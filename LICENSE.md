License     {#pageLicense}
=======

PIPER software framework and application
========================================

Copyright (C) 2017 UCBL-Ifsttar, KTH, FITT, CEESAR, INRIA, U Southampton
(details to be added by library and file, order by partner number)

The PIPER Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

The PIPER Framework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

A full contributors list will be added soon.

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).

PIPER documentation
===================

The documentation is released under the GNU FDL 1.3 license.

PIPER data
==========

Data provided along with the PIPER software (e.g. CCTAnthro database) is 
distributed under the CC-BY-4.0 license (see files for details).

