/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <Python.h>

#include <cstdlib>
#include <iostream>
#include <string>
#include <list>

#include <gtest/gtest.h>

#include <QCoreApplication>
#include <QProcessEnvironment>
#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QTextDocument>

#include <common/init.h>
#include <common/logging.h>
#include <common/helper.h>

void gtestMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static QTextDocument document;
    switch (type) {
    case QtCriticalMsg:
    case QtFatalMsg:
        document.setHtml(msg);
        ADD_FAILURE_AT(context.file, context.line) << document.toPlainText().toStdString();
    }
}

using namespace piper;

static std::string piperTestDir = std::string(PIPER_SHARE_DIR)+"/test";

// a function to run a batch script and report any run time error
void runBatchTest(std::string const& batchFile) {
    pInfo() << piper::INFO << "Start batch test: " << batchFile;
    QString scriptFile = QString::fromStdString(piperTestDir+"/"+batchFile);
    ASSERT_TRUE(QFileInfo::exists(scriptFile)) << "scriptFile: " << scriptFile.toStdString();        
    EXPECT_EQ(0, piper::runPythonScript(scriptFile));
}

TEST(selfTest, selfTest_01) {
    runBatchTest("selfTest_01.py");
}

// macro to add an integration test
#define PIPER_INTEGRATION_TEST(NAME) \
    TEST(integrationTest, NAME) { \
        runBatchTest(std::string(#NAME)+".py"); \
    }

PIPER_INTEGRATION_TEST(target)

PIPER_INTEGRATION_TEST(child_import_save_export)
PIPER_INTEGRATION_TEST(child_positioning_physics)
PIPER_INTEGRATION_TEST(child_scaling_parameters)

PIPER_INTEGRATION_TEST(ghbm_import_save_export)

PIPER_INTEGRATION_TEST(viva_import_save_export)

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    piper::LoggingParameter::instance().setVerboseLevel(piper::LoggingParameter::VerboseLevel::VERBOSE);
    piper::LoggingParameter::instance().addMessageHandler(gtestMessageHandler);

    Py_SetProgramName(argv[0]);
    piper::init();
    std::atexit(piper::cleanup);
    piper::sofaInit();

    QDir piperTestQDir(QString::fromStdString(piperTestDir));
    pDebug() << "Test script directory: " << piperTestQDir.canonicalPath();
    ::testing::InitGoogleTest(&argc, argv);

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!env.contains("PIPER_MODEL_PATH")) {
        std::cerr << "ERROR: Missing PIPER_MODEL_PATH environment variable" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    // to access model files
    if (!QFileInfo::exists(env.value("PIPER_MODEL_PATH"))) {
        std::cerr << "ERROR: PIPER_MODEL_PATH: " << env.value("PIPER_MODEL_PATH").toStdString() << " does not exists" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    pDebug() << "PIPER_MODEL_PATH: " << env.value("PIPER_MODEL_PATH");

    return RUN_ALL_TESTS();
}

