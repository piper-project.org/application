# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 

## \addtogroup groupTestAPI Batch test API
## \{

## if \a b is false, add a failure to googletest with \a message
def expect_true(b, message="Test failed"):
    if not b:
        caller = inspect.getframeinfo(inspect.stack()[1][0])
        _add_failure_at(caller.filename, caller.lineno, message);

## if \a value is different from \a expectedValue, add a failure to googletest with \a message
def expect_eq(expectedValue, value, message="Test failed"):
    if not expectedValue == value:
        caller = inspect.getframeinfo(inspect.stack()[1][0])
        _add_failure_at(caller.filename, caller.lineno, "{0}!={1} - ".format(expectedValue, value) + message);

## \}


