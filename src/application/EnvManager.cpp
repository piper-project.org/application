/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <utility>


#include <tinyxml/tinyxml2.h>

#include "common/message.h"
#include "common/logging.h"
#include "common/XMLValidationProcess.h"

//#include "hbm/IdGenerator.h"

#include <fstream>
#include <sstream>

#include "EnvManager.h"
#include <QTextStream>
#include <QFileInfo>
#include <QProcess>
#include <QCoreApplication>
#include <QtQuick>

namespace piper {
    namespace envmanager {
        using namespace hbm;


        TableEnvModel::TableEnvModel( const std::map<std::string, std::string>& mapvalues, QObject *parent): QAbstractListModel (parent) {
	        construct( mapvalues);
        }

        int TableEnvModel::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return 2;
        }

        int TableEnvModel::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
	        return items.count();
        }

        QVariant TableEnvModel::data(const QModelIndex &index, int role) const {
            switch (role) {
                case NameRole: return items[index.row()]->m_name;
                case ValueRole:
                default:
                    return items[index.row()]->m_file;
            }
        }

        QVariantMap TableEnvModel::get(int idx) const {
	        QVariantMap map;
	        foreach(int k, roleNames().keys()) {
		        map[roleNames().value(k)] = data(index(idx, 0), k);
	        }
	        return map;
        }

        QHash<int, QByteArray> TableEnvModel::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[ValueRole] = "ValueRole";

            return roles;
        }

        bool TableEnvModel::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
                case NameRole: items[index.row()]->m_name = value.toString(); break;
                case ValueRole: items[index.row()]->m_file = value.toString(); break;
            }
            emit dataChanged(index, index);
            return true;
        }


        void TableEnvModel::construct( const std::map<std::string, std::string>& mapvalues) {
	        items.clear();
	        for (auto it=mapvalues.begin(); it!=mapvalues.end(); ++it) {
		        //std::cout << it->first << " - " << it->second << std::endl;
		        items.append( new ModelParameterItem(QString::fromStdString(it->first), QUrl( QString::fromStdString( it->second))));
	        }
	        this->beginResetModel();
	        this->endResetModel();
        }

        Qt::ItemFlags TableEnvModel::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
        }

        void TableEnvModel::clear() {
	        items.clear();
	        this->beginResetModel();
	        this->endResetModel();
        }


        EnvManager::EnvManager(): m_tablemodel( new TableEnvModel( )), m_count(0), m_defaultname("Env_0")
        {
            connect(&Context::instance(), &Context::envChanged, this, &EnvManager::update);
            update(); 
        }


        void EnvManager::updateDefaultName() {
	        std::ostringstream convert;   
	        convert << m_count;
	        std::string defaultname="Env_" + convert.str();
	        m_defaultname=QString::fromStdString(defaultname);
        }

        void EnvManager::update()
        {
	        m_tablemodel->clear();
	        m_mapfile.clear();
            EnvironmentModels& env=Context::instance().project().environment();
	        std::vector<std::string> list = env.getListNames();
	        for(std::vector<std::string>::const_iterator it=list.begin(); it!=list.end(); ++it) {
		        m_mapfile[*it]=env.getEnvironmentModel( *it)->file();
		        //std::cout << env.getEnvironmentModel(*it).name() << std::endl;
		        if (it->size()>4 && it->size()<6) {
			        std::string cur=*it;
			        if (cur.substr(0, 4) == "Env_") {
				        int numb;
				        std::istringstream ( cur.substr(4, cur.size()-4) ) >> numb;
				        if (numb>=m_count) {
					        m_count=numb+1;
					        updateDefaultName();
					        emit defaultNameChanged();
				        }
			        }
		        }
	        }

	        m_tablemodel->construct( m_mapfile);

        }

        QString EnvManager::defaultName( ) const {
	        return m_defaultname;
        }

        void EnvManager::addEnvModel(const QString& name, const QUrl& envmodelFile, const QUrl& formatRuleFile, const QString& lengthUnit) {
	        if (!name.isEmpty() && !envmodelFile.isEmpty()) {
                Context::instance().addenvModel(name.toStdString(), envmodelFile.toLocalFile().toStdString(),
                    formatRuleFile.toLocalFile().toStdString(), units::LengthUnit::strToUnit(lengthUnit.toStdString()));
                // TODO message to log ?
		        emit defaultNameChanged();
	        }
        }

        void EnvManager::delEnvModel(const QString& name) {
	        if (!name.isEmpty()) {
                Context::instance().removeEnvModel(name.toStdString());
	        }
        }

        TableEnvModel* EnvManager::getModel(  )  {	
	        return m_tablemodel;
        }

        void EnvManager::setScale(const QString& name, double x, double y, double z)
        {
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    model->envmodel().setScale(x, y, z);
                    emit Context::instance().envScaled(name, x, y, z);
                }
            }
        }

        void EnvManager::setTranslation(const QString& name, double x, double y, double z)
        {
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    model->envmodel().setTranslation(x, y, z);
                    emit Context::instance().envTranslated(name, x, y, z);
                }
            }
        }

        void EnvManager::setRotation(const QString& name, double x, double y, double z)
        {
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    model->envmodel().setRotation(x, y, z);
                    emit Context::instance().envRotated(name, x, y, z);
                }
            }
        }

        QVariantList EnvManager::getScale(const QString& name)
        {
            QVariantList scale;
            scale.push_back(1);
            scale.push_back(1);
            scale.push_back(1);
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    const Coord& sc = model->envmodel().getScale();
                    scale[0] = sc[0];
                    scale[1] = sc[1];
                    scale[2] = sc[2];
                }
            }
            return scale;
        }

        QVariantList EnvManager::getTranslation(const QString& name)
        {
            QVariantList translation;
            translation.push_back(0);
            translation.push_back(0);
            translation.push_back(0);
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    const Coord& tr = model->envmodel().getTranslation();
                    translation[0] = tr[0];
                    translation[1] = tr[1];
                    translation[2] = tr[2];
                }
            }
            return translation;
        }

        QVariantList EnvManager::getRotation(const QString& name)
        {
            QVariantList rotation;
            rotation.push_back(0);
            rotation.push_back(0);
            rotation.push_back(0);
            if (!name.isEmpty())
            {
                EnvironmentModelPtr model = Context::instance().project().environment().getEnvironmentModel(name.toStdString());
                if (model != NULL)
                {
                    const Coord& rot = model->envmodel().getRotation();
                    rotation[0] = rot[0];
                    rotation[1] = rot[1];
                    rotation[2] = rot[2];
                }
            }
            return rotation;
        }

        void EnvManager::envVisible(const QString& name, bool visible)
        {
            emit Context::instance().envVisibleChanged(name, visible);
        }

    }
}
