// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0
import Qt.labs.folderlistmodel 2.1

import Piper 1.0

Dialog {
    id: root
    title: "Run a python script"
    standardButtons: StandardButton.Open | StandardButton.Cancel
    visible: false

    Component.onCompleted: {width=500;height=500}

    FolderListModel {
        id: standardScriptsModel
        folder: Qt.resolvedUrl("file:"+context.shareDirPath()+"/python/scripting")
        nameFilters: [ "*.py" ]
        showDirs: false
        showFiles: true
        sortField : FolderListModel.Time
    }

    FolderListModel {
        id: customScriptsModel
        folder: settings.customScriptFolder
        nameFilters: [ "*.py" ]
        showDirs: false
        showFiles: true
        sortField : FolderListModel.Time
    }

    Component {
        id: scriptTableView

        TableView {
            id: root
            anchors.fill: parent
            model: scriptModel
            headerVisible : false
            alternatingRowColors : true
            selectionMode : SelectionMode.SingleSelection

            TableViewColumn {
                role: "fileName"
            }

            Connections {
                target: selection
                onSelectionChanged : root.scriptSelectionChanged(selection)
            }
//            Label {
//                anchors.centerIn: parent
//                text: "No script available"
//                visible: (0 === root.model.count) // [TL] do not know why, it does not update
//            }

            signal scriptSelectionChanged(var selection);
        }
    }

    GridLayout {
        anchors.fill: parent
//        anchors.right: parent.right
        columns: 3

        // standard scripts
        Label {
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            text: "Standard:"
        }
        Loader {
            id: standardScriptTableViewLoader
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.columnSpan: 2

            property var scriptModel: standardScriptsModel
            sourceComponent: scriptTableView
            ToolTip {
                text: "Select a standard python script"
            }
        }
        Connections {
            target: standardScriptTableViewLoader.item
            onScriptSelectionChanged: {
                if (0 === selection.count)
                    return
                customScriptTableViewLoader.item.selection.clear();
                selection.forEach( function(rowIndex) {
                    pythonScript.setPathUrl(standardScriptsModel.get(rowIndex, "fileURL"));
                    root.update();
                } );
            }
        }

        // custom scripts
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Custom:"
        }
        TextField {
            Layout.fillWidth: true
            readOnly: true
            text: settings.customScriptFolder
            ToolTip {
                text: "Custom script folder"
            }
        }
        Button {
            Layout.alignment: Qt.AlignTop | Qt.AlignRight
            tooltip: "Select your custom python script folder"
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectCustomScriptFolderDialog.open()
        }
        Loader {
            id: customScriptTableViewLoader
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.row: 2
            Layout.column: 1
            Layout.columnSpan: 2

            property var scriptModel: customScriptsModel
            sourceComponent: scriptTableView
            ToolTip {
                text: "Select a custom python script"
            }
        }
        Connections {
            target: customScriptTableViewLoader.item
            onScriptSelectionChanged: {
                if (0 === selection.count)
                    return
                standardScriptTableViewLoader.item.selection.clear();
                selection.forEach( function(rowIndex) {
                    pythonScript.setPathUrl(customScriptsModel.get(rowIndex, "fileURL"));
                    root.update();
                } );
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            text: "Description:"
        }
        TextArea {
            id: pythonScriptDescriptionText
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.minimumHeight: 40
            readOnly: true
            function update() {
                text = pythonScript.description();
                if (0 == pythonScriptDescriptionText.text.length)
                    text = "No description available";
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Arguments:"
        }
        TextField {
            id: pythonScriptArgsText
            Layout.fillWidth: true
            Layout.columnSpan: 2
            placeholderText: "Script arguments"
        }
    }

    function update() {
        pythonScriptDescriptionText.update();
        pythonScriptArgsText.text = pythonScript.args().join(" ");
    }

    onVisibleChanged: {
        if (visible) update();
    }

    onAccepted: {
        var args;
        if (pythonScriptArgsText.text.length > 0)
            args = pythonScriptArgsText.text.split(" ");
        else
            args = [];
        pythonScript.setArgs(args);
        scripting.runScript();
    }

    Settings {
        id: settings
        category: "application"
        property url recentScriptFolder
        property url customScriptFolder
    }

    FileDialog {
        id: selectCustomScriptFolderDialog
        title: qsTr("Select a folder containing python scripts...")
        selectFolder: true

        onVisibleChanged: {
            if (visible && context.doFolderExist(settings.customScriptFolder))
                folder = settings.customScriptFolder
        }
        onAccepted: {
            settings.customScriptFolder = folder;
            customScriptsModel.folder = folder;
        }
    }
}
