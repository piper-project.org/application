// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0

Dialog {
    title: "Import Geometric model"
    standardButtons: StandardButton.Ok | StandardButton.Cancel
    visible: false

//    property url modelFolderPath
    property url modelRulesFilePath
//    property alias modelLengthUnit : lengthUnitChooser.currentText

    Settings {
        id: settings
        property url recentObjImportFolder
    }

    Component.onCompleted: {width=500;}

    GridLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        columns: 3

		Label {
            Layout.alignment: Qt.AlignRight
            text: "Model description file:"
        }
        TextField {
            id: modelRulesFileText
            Layout.fillWidth: true
            placeholderText: qsTr("Select model description file")
        }
        Button {
            tooltip: qsTr("Select model description file")
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectModelRulesDialog.open()
        }

        Label {
            Layout.alignment: Qt.AlignRight
            text: "Model Folder:"
        }
        TextField {
            id: modelFolderText
            Layout.fillWidth: true
            placeholderText: qsTr("Select model folder")
        }
        Button {
            tooltip: qsTr("Select model folder")
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectModelDialog.open()
		}
//        Label {
//            Layout.alignment: Qt.AlignRight
//            text: "Model units:"
//        }
//		LengthUnitChooser {
//			id: lengthUnitChooser
//		}
    }

    FileDialog {
        id: selectModelRulesDialog
        title: qsTr("Select model description...")
        nameFilters: ["pmr (*.pmr)"]
        onAccepted: {
            modelRulesFilePath = selectModelRulesDialog.fileUrl
            modelRulesFileText.text = selectModelRulesDialog.fileUrl
        }
    }

    FileDialog 
	{
        id: selectModelDialog
        title: qsTr("Select model folder...")
        selectFolder: true
        onAccepted: 
		{
			settings.recentObjImportFolder = folder
            modelFolderText.text = selectModelDialog.fileUrl
        }
        onVisibleChanged:
        {
            if(visible)
                if(context.doFolderExist(settings.recentObjImportFolder))
					folder = settings.recentObjImportFolder
        }
    }


}
