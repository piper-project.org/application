// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQml 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import Qt.labs.settings 1.0

import Piper 1.0
import piper.scripting 1.0

import SofaScene 1.0
import SofaBasics 1.0
import SofaApplication 1.0

ApplicationWindow {

    id: applicationWindow
    title: "Piper "+Qt.application.version+" - "+projectFilePath
    visible: true
    x:settingsWindow.x
    y:settingsWindow.y
    width: settingsWindow.width
    height: settingsWindow.height

    property int itemMargin: 10
    property string currentModuleName

    property string projectFilePath: {
        if (context.projectFile.toString().length === 0) "new project";
        else context.projectFile
    }

    function updateApplicationWindowTitle() {
        applicationWindow.title = "Piper "+Qt.application.version+" | "+applicationWindow.currentModuleName+" Module";
    }

    /*
     * Sofa simulation
     */
    property var sofaScene: SofaScene { asynchronous: true }
    /** Keep track of the up to date sofa scene
      \todo: move this code in a seperate component and instanciate it only in sofa based modules
      */
    QtObject {
        id: sofaSceneUpToDate
        // current sofa scene is ready for module \a moduleName
        property string moduleName: ""

        signal reloadNeeded()
        signal loaded()

        function reset() {
            if (moduleName !== applicationWindow.currentModuleName) {
                moduleName = "";
                if (!sofaModuleLoader.visible) // in that case, we can free GUI loader
                    sofaModuleLoader.source="";
            }
            else {
                sofaSceneUpToDate.moduleName = "";
                reloadNeeded();
            }
        }
        function cancelSceneLoading() {
            sofaScene.source = "";
            loaded();
        }

        onLoaded: {
            sofaSceneUpToDate.moduleName = applicationWindow.currentModuleName;
            context.setNotBusy();
            context.busyTaskCancelled();
        }
    }
    Connections {
        target: context
        onModelChanged: sofaSceneUpToDate.reset()
        onModelUpdated: sofaSceneUpToDate.reset()
        onMetadataChanged: sofaSceneUpToDate.reset()

    }
    Connections {
        target: moduleParameter
        onValueChanged: {
            if (0 === sofaSceneUpToDate.moduleName.length)
                return;
            var paramKey;
            if (physScalingAction.text === sofaSceneUpToDate.moduleName) {
                paramKey = "PhysScale"
                if (-1 !== key.indexOf(paramKey))
                    sofaSceneUpToDate.reset();
            }
            else {
                if (physPositioningInteractiveAction.text === sofaSceneUpToDate.moduleName)
                    paramKey = "PhysPosiInter"
                else if (physPositioningDeformationAction.text === sofaSceneUpToDate.moduleName)
                    paramKey = "PhysPosiDefo"
                if (-1 !== key.indexOf(paramKey) || -1 !== key.indexOf("PhysPosi_"))
                    sofaSceneUpToDate.reset();
            }
        }
    }

    // used when going directly from the body section module to the kriging module, it indicates the name of the control points source & target to be
    // selected at the init of the kriging module
    property string krigingAutoLoaControlPointsName: "none"
    // used when going directly from the interactive positioning module to the positioning deformation module
    // tells the deformation module to start directly
    property bool physPosiDeformAutoLoadTarget: false

    Component.onCompleted: {
        SofaApplication.defaultInteractorName= "MoveCamera";
        context.projectFileChanged.connect(recentProjects.onProjectOpened);
        scripting.init();
        checkAction.trigger();
    }

    Component.onDestruction: {
        settingsWindow.x = applicationWindow.x
        settingsWindow.y = applicationWindow.y
        settingsWindow.width = applicationWindow.width
        settingsWindow.height = applicationWindow.height
    }

     ModelHistoryNameDialog {
         id: renameModelHistoryDialog
         title: qsTr("Rename the current model")
         text: context.currentModel
         onAccepted: context.renameCurrentModel(text)
         onVisibleChanged: text = context.currentModel
     }

    Settings {
        id: settingsWindow
        category: "applicationWindow"
        property int x
        property int y
        property int width: 800
        property int height: 600
    }

    Action {
        id: newAction
        text: qsTr("New")
        tooltip: qsTr("New Empty Project")
        shortcut: StandardKey.New
        onTriggered: {
            context.newEmptyProject();
            checkAction.trigger();
        }
    }
    
    Action {
        id: openAction
        text: qsTr("Open")
        tooltip: qsTr("Open Project")
        iconSource: "qrc:///icon/document-open.png"
        shortcut: StandardKey.Open
        onTriggered: openDialog.open()
        enabled: !context.isBusy
    }

    Action {
        id: openRecentAction
        onTriggered: {            
            context.operationFinished.connect(importHBMmodelDialog.postImport);
            context.openProject(source.text.toString())
        }
    }

    Action {
        id: clearRecentProjectsAction
        text: qsTr("Clear recent")
        tooltip: qsTr("Clear recent projects")
        onTriggered: recentProjects.clear()
    }

    Settings {
        id: settings
        category: "application"

        property url recentImportHBMmodelFolder
        property url recentModelFolder
        property url recentFEModelExportFolder
        property url recentTargetFolder
        property alias recentProjects: recentProjects.projects
        property alias recentScriptsArgs: scripting.recentScriptsArgsJson
    }

    Action {
        id: loadCtrlPtSrcAction
        text: qsTr("Load CtrlPtSrc")
        tooltip: qsTr("Load Source Control Point")
        onTriggered: openCtrlPtSrc.open()
    }

    Action {
        id: loadCtrlPtDestAction
        text: qsTr("Load CtrlPtDest")
        tooltip: qsTr("Load Target Control Point")
        onTriggered: openCtrlPtDest.open()
    }
    Action {
        id: loadCtrlPtWeightAction
        text: qsTr("Load CtrlPtWeight")
        tooltip: qsTr("Load Control Point Weight")
        onTriggered: openCtrlPtWeight.open()
    }

	Action {
		id: targetInformationAction
		text: qsTr("Target not loaded")
		//text.bold: true
		iconSource: "qrc:///icon/redCircle.png"
	}

    QtObject {
        id: recentProjects
        property int maxRecentProjects: 5
        property string projects: "" // TODO: build that string with JSON

        function onProjectOpened() {
            if (context.projectFile.toString()==="")
                return
            var projectsList = projects.split(";")
            if (projectsList.length===1 && projectsList[0].length===0)
                projectsList=[]
            var projectString = Qt.resolvedUrl(context.projectFile)
            var i=projectsList.indexOf(projectString)
            if (i!==-1) {
                projectsList.splice(i,1)
            }
            projectsList.unshift(projectString)
            projectsList=projectsList.slice(0,maxRecentProjects)
            projects=projectsList.join(";")
        }

        function clear() {
            projects = ""
        }

        onProjectsChanged: {
            updateRecentProjectsMenu()
        }

        function updateRecentProjectsMenu() {
            var projectsList = projects.split(";")
            menuRecentProjects.clear()

            for (var i=0; i<projectsList.length; i++) {
                if (projectsList[i].length === 0)
                    continue;
                var item = menuRecentProjects.addItem(projectsList[i])
                item.action=openRecentAction
            }
            menuRecentProjects.addSeparator()
            menuRecentProjects.addItem( "clear" ).action=clearRecentProjectsAction
        }

        Component.onCompleted:  {
            updateRecentProjectsMenu();
        }

        Component.onDestruction: {
            settings.recentProjects=projects
        }

    }

    Action {
        id: saveAction
        text: qsTr("Save")
        tooltip: "Save project"
        shortcut: StandardKey.Save
        iconSource: "qrc:///icon/document-save.png"
        enabled: !context.isBusy
        onTriggered: {
            if (context.projectFile.toString()==="")
                saveAsAction.trigger()
            else
                context.saveProject(context.projectFile.toString())
        }
    }

    Action {
        id: saveAsAction
        text: qsTr("Save as")
        iconSource: "qrc:///icon/document-save.png"
        onTriggered: saveAsDialog.open()
    }

    Action {
        id: importHBMmodelAction
        text: qsTr("Import HBM model")
        tooltip: qsTr("Import a human body model described in a model rule file (*.pmr)")
        onTriggered: importHBMmodelDialog.open()
    }

    Action {
        id: importTargetAction
        enabled: context.hasModel
        text: qsTr("Import targets")
        tooltip: qsTr("Import targets from file and replace current target list")
        onTriggered: importTargetDialog.open()
    }
    Action {
        id: clearTargetAction
        text: "&Clear target"
        tooltip: "Remove all targets from the application target list"
        onTriggered: context.clearTarget()
    }

    Action {
        id: importenvAction
        enabled: context.hasModel
        text: qsTr("Environment models")
        tooltip: qsTr("Manage finite element environment models")
        onTriggered: {
            if (importEnvModelDialog.visible)
                importEnvModelDialog.hide()
            else
                importEnvModelDialog.show()
        }
    }
    
    Action {
        id: exportFEmodelAction
        enabled: context.hasModel
        text: qsTr("Export HBM to FE files")
        tooltip: qsTr("Export a human body finite element model copying/updating its original FE files")
        onTriggered: exportDialog.open();
    }

    Action {
        id: exportLandmarksAction
        enabled: context.hasModel
        text: qsTr("Export landmarks coordinates")
        tooltip: qsTr("Export landmarks coordinates in a simple ascii file")
        onTriggered: exportLandmarks.open()
    }

    Action {
        id: exportEntityNodesAction
        enabled: context.hasModel
        text: qsTr("Export nodes coordinates")
        tooltip: qsTr("Export nodes coordinates with original id in a simple ascii files (one per entity)")
        onTriggered: exportNodes.open()
    }
    
    Action {
        id: exportTargetAction
        text: qsTr("Export targets")
        tooltip: qsTr("Export targets to file")
        onTriggered: exportTargetDialog.open()
    }
    
    Action {
        id: quitAction
        text: qsTr("Quit")
        shortcut: StandardKey.Quit
        onTriggered: Qt.quit();
    }

    Action {
        id: moduleParameterAction
        text: "Parameters"
        tooltip: "Edit modules parameters"
        iconSource: "qrc:///icon/configure.png"
        enabled: !context.isBusy
        onTriggered: moduleParameterDialog.open()
    }

    Action {
        id: aboutAction
        text: qsTr("About")
        onTriggered: aboutDialog.open()
    }

    Action {
        id: showLogPanelAction
        shortcut: "F3" // TODO make it context: Qt.ApplicationShortcut
        text: "Log Panel"
        tooltip: "show/hide log panel (F3)"
        checkable: true
        checked: logMessage.visible
        onTriggered: {
            logMessage.visible = !logMessage.visible;
        }
    }

    Action {
        id: showUserGuideActionHTML
        text: qsTr("HTML User Guide")
        shortcut: "F1"
        onTriggered: context.showUserGuideHTML()
    }

	Action {
        id: showUserGuideActionPDF
        text: qsTr("PDF User Guide")
        shortcut: "F2"
        onTriggered: context.showUserGuidePDF()
    }

    Action {
        id: renameCurrentModelAction
        text: "Rename current model"
        onTriggered: {
            renameModelHistoryDialog.open()
        }
    }

    Action {
        id: reloadFEmeshAction
        text: "Reload FE mesh"
        onTriggered: {
            context.reloadFEMesh()
        }
    }

    /*
     * Scripting
     */
    PythonScript {
        id: pythonScript
    }
    QtObject {
        id: scripting
        property int maxRecentScripts: 5
        property int maxRecentScriptStringSize: 50
        property string recentScriptsArgsJson: ""
        property var recentScriptsArgs: []

        function init() {
            try {
                recentScriptsArgs = JSON.parse(recentScriptsArgsJson);
            }
            catch(err) {
                recentScriptsArgs = [];
            }
            updateRecentScriptMenu();
        }

        function runRecentScript(index) {
            if (!(index < recentScriptsArgs.length)) {
                console.warn("scripting.runRecentScript: invalid index:",index,"with recent scripts list:",recentScriptsArgs);
                return;
            }
            var scriptArgs = recentScriptsArgs[index];
            pythonScript.setPathUrl(scriptArgs[0]);
            pythonScript.setArgs( (scriptArgs.length>1 ? scriptArgs.slice(1,scriptArgs.length) : []) );
            runScript();
        }

        function runScript() {
            pythonScript.runAsynchronous();

            // TODO update the recent scripts only if script was successfull
            var thisScriptArg = [Qt.resolvedUrl(pythonScript.pathUrl())].concat(pythonScript.args());
            var i = recentScriptsArgs.map(function(sa) {return sa[0]}).indexOf(thisScriptArg[0]);
            if (i!==-1) {
                recentScriptsArgs.splice(i,1);
            }
            recentScriptsArgs.unshift(thisScriptArg)
            recentScriptsArgs = recentScriptsArgs.slice(0, maxRecentScripts);
            recentScriptsArgsJson = JSON.stringify(recentScriptsArgs);

            updateRecentScriptMenu();
        }

        function clear() {
            recentScriptsArgs = [];
            recentScriptsArgsJson = JSON.stringify(recentScriptsArgs);
            updateRecentScriptMenu();
        }

        function updateRecentScriptMenu() {
            menuScripting.clear();
            var item = menuScripting.addItem(runScriptAction.text);
            item.action = runScriptAction;
            item = menuScripting.addItem(runMostRecentScriptAction.text);
            item.action = runMostRecentScriptAction;
            menuScripting.addSeparator();

            for (var i=0; i<recentScriptsArgs.length; i++) {
                if (recentScriptsArgs[i].length === 0)
                    continue;
                var scriptPath = recentScriptsArgs[i][0];
				
                if (scriptPath.length > maxRecentScriptStringSize)
                    scriptPath = scriptPath.substring(scriptPath.length-maxRecentScriptStringSize, scriptPath.length);
                item = menuScripting.addItem(i+": "+scriptPath);
                item.action = runRecentScriptAction;
            }
            if (0 < recentScriptsArgs.length) {
                menuScripting.addSeparator();
                item = menuScripting.addItem(clearRecentScriptAction.text);
                item.action = clearRecentScriptAction;
            }
        }
    }

    Action {
        id: runScriptAction
        text: "Run script..."
        tooltip: "Run a python script"
        shortcut: "Ctrl+Shift+R"
        iconSource: "qrc:///icon/text-x-python.png"
        enabled: !context.isBusy
        onTriggered: runScriptDialog.open()
    }

    Action {
        id: runMostRecentScriptAction
        text: "Re-run script"
        tooltip: "Re-run the last script"
        shortcut: "Ctrl+R"
        enabled: scripting.recentScriptsArgs.length > 0

        onTriggered: scripting.runRecentScript(0);
    }

    Action {
        id: runRecentScriptAction
        onTriggered: scripting.runRecentScript(parseInt(source.text.split(":")[0]));
    }

    Action {
        id: clearRecentScriptAction
        text: "Clear"
        onTriggered: scripting.clear();
    }

    function setVisibleModuleLoader(name) {
        if ("sofa" === name) {
            defaultModuleLoader.visible = false;
            sofaModuleLoader.visible = true;
            defaultModuleLoader.source = ""; // unload the previous module GUI
        }
        else {
            defaultModuleLoader.visible = true;
            sofaModuleLoader.visible = false;
            if ("" === sofaSceneUpToDate.moduleName) // in that case no need to keep GUI loaded
                sofaModuleLoader.source = "";
        }
    }

    // default function to load a module GUI
    function defaultLoadModule(moduleAction) {
        if (applicationWindow.currentModuleName != moduleAction.text)
        {
            contextVtkDisplay.RemoveNonpersistentActors();
            setVisibleModuleLoader("default");
            applicationWindow.currentModuleName = moduleAction.text;
            defaultModuleLoader.setSource(moduleAction.moduleSource);
        }
    }

    function sofaLoadModule(moduleAction) {
        contextVtkDisplay.RemoveNonpersistentActors();
        setVisibleModuleLoader("sofa");
        applicationWindow.currentModuleName = moduleAction.text;
        sofaModuleLoader.setSource(moduleAction.moduleSource);
//        if (applicationWindow.currentModuleName !== sofaSceneUpToDate.moduleName) {
//            sofaModuleLoader.setSource(moduleAction.moduleSource);
//        }
//        else
//            context.logInfo("Simulation already loaded")
    }

    ExclusiveGroup {
        id: moduleGroup
        ModuleAction {
            id: checkAction
            checkable: true
            text: "Check"
            letter: "C"
            color: "white"
            shortcut: "Ctrl+Alt+C"
            tooltip: "Check the current model"
            moduleSource: "qrc:///module/check/Check.qml"
            onTriggered: defaultLoadModule(checkAction);
        }
		ModuleAction {
            id: childAction
            checkable: true
            text: "Child"
            letter: "C"
            color: "magenta"
            //shortcut: "Ctrl+Alt+C"
            tooltip: "Perform custom scaling on child model"
            moduleSource: "qrc:///module/child/Child.qml"
            onTriggered: defaultLoadModule(childAction);
        }
        ModuleAction {
            id: anthroPersoAction
            checkable: true
            text: "Anthro"
            letter: "A"
			color: "gold"
			shortcut: "Ctrl+Alt+A"
            tooltip: "Compute personalization of the current model based on anthropometric data"
            moduleSource: "qrc:///module/anthropometry/AnthroPersoTool.qml"
            onTriggered: defaultLoadModule(anthroPersoAction);
        }
        ModuleAction {
            id: krigingAction
            checkable: true
            text: "Kriging"
            letter: "K"
            color: "limegreen"
            moduleSource: "qrc:///module/kriging/KrigingModule.qml"
            tooltip: "Perform transformation of the current model by the kriging interpolation"
            onTriggered: defaultLoadModule(krigingAction);
        }
        ModuleAction {
            id: physPositioningInteractiveAction
            checkable: true
            text: "Pre"
            letter: "P"
            tooltip: "Physics based Interactive Positioning"
			color: "darkorange"
            shortcut: "Ctrl+Alt+P"
            moduleSource: "qrc:///module/sofaPositioning/sofaPositioning.qml"
            onTriggered: {
                sofaLoadModule(physPositioningInteractiveAction);
            }    
        }
        ModuleAction {
            id: physPositioningDeformationAction
            checkable: true
            text: "Fine"
			color: "darkorange"
			letter: "F"
            tooltip: qsTr("Physics based Fine Positioning")
            shortcut: "Ctrl+Alt+F"
            moduleSource: "qrc:///module/sofaPositioning/sofaDeforming.qml"
            onTriggered: sofaLoadModule(physPositioningDeformationAction)
        }
        ModuleAction {
            id: scalingParameterAction
            checkable: true
            text: "Param"
			letter: "P"
			color: "limegreen"
            tooltip: "Scaling Parameter"
            moduleSource: "qrc:///module/scalingParameter/scalingParameter.qml"
            onTriggered : defaultLoadModule(scalingParameterAction)
        }
        ModuleAction {
            id: meshoptimizingAction
            checkable: true
            text: "Smooth"
            letter: "S"
			color: "deepskyblue"
            tooltip: "Mesh Optimization"
            moduleSource: "qrc:///module/meshOptimizing/meshOptimizing.qml"
            onTriggered: defaultLoadModule(meshoptimizingAction)
        }
        ModuleAction {
            id: bodysectionModuleAction
            checkable: true
            text: "Scal. Const."
            letter: "S"
			color: "gold"
			shortcut: "Ctrl+Alt+S"
            tooltip: "Scaling constraints on HBM"
            moduleSource: "qrc:///module/BodySectionPersonalizing/BodySectionPersonalizing.qml"
            onTriggered: defaultLoadModule(bodysectionModuleAction);
        }
        ModuleAction {
            id: contourDeformationAction
            checkable: true
            text: "Contour"
			letter: "C"
			color: "limegreen"
            tooltip: "Contour"
            moduleSource: "qrc:///module/contourDeformation/contourDeformation.qml"
            onTriggered: defaultLoadModule(contourDeformationAction)
        }
        ModuleAction {
            id: physScalingAction
            checkable: true
            text: "Shape"
            letter: "S"
            tooltip: "Physics based Interactive Shaping/Scaling"
            color: "limegreen"
            moduleSource: "qrc:///module/sofaScaling/sofaScaling.qml"
            onTriggered: {
                sofaLoadModule(physScalingAction);
            }
        }
        ModuleAction {
            id: contourPersonalizationAction
            checkable: true
            text: "Perso"
            letter: "P"
            color: "limegreen"
            tooltip: "Contour Personalization"
            moduleSource: "qrc:///module/contourPersonalization/ContourPersonalization.qml"
            onTriggered: defaultLoadModule(contourPersonalizationAction)
        }
    }

            
    menuBar: MenuBar {
        Menu {
            enabled: !context.isBusy
            title: qsTr("&Project")
            MenuItem { action: newAction }
            MenuItem { action: openAction }

            Menu {
                id: menuRecentProjects
                title: qsTr("&Recent projects...")
            }
            MenuSeparator { }
            MenuItem { action: saveAction }
            MenuItem { action: saveAsAction }
            MenuSeparator { }
            Menu {
                id: menuImport
                title: qsTr("Import...")
                MenuItem { action: importHBMmodelAction }
                MenuItem { action: importMetadataAction }
            }
            //MenuItem { action: importFEmodelAction }
            Menu {
                id: menuExport
                title: qsTr("Export...")
                MenuItem { action: exportFEmodelAction }
                MenuItem { action: exportMetadataAction }
                MenuSeparator { }
                MenuItem { action: exportLandmarksAction }
                MenuItem { action: exportEntityNodesAction }

            }
            //MenuItem { action: exportFEmodelAction }
            MenuSeparator { }
            MenuItem { action: importenvAction }
            MenuItem { action: moduleParameterAction }
            MenuSeparator { }
            MenuItem { action: quitAction }
        }
        Menu {
            enabled: !context.isBusy
            title: "&Target"
            MenuItem { action: clearTargetAction }
            MenuItem { action: importTargetAction }
            MenuItem { action: exportTargetAction }
			MenuSeparator { }
			MenuItem { action: targetInformationAction }	
			onAboutToShow: {
                if (context.isTargetLoaded()) {
                    targetInformationAction.text = project.targetSize() + " targets"
                    targetInformationAction.iconSource= "qrc:///icon/greenCircle.png"

				}
                else {
                    targetInformationAction.text = "No target"
                    targetInformationAction.iconSource= "qrc:///icon/redCircle.png"
                }
			}
        }
        Menu {
            id: menuHistory
            enabled: !context.isBusy && context.hasModel
            title: qsTr("History")
            MenuItem { action: renameCurrentModelAction }
            MenuItem { action: reloadFEmeshAction }
            MenuSeparator { }
            Instantiator {
                id: instHistory
                model: context.historylist
                delegate: MenuItem {
                    text: modelData
//                    enabled: {
//                        console.log("i am here")
//                        if (modelData===context.currentModel) {
//                            enabled=true}
//                        else {
//                            enabled=false}
//                    }
                    onTriggered: {
                        context.setCurrentModel(modelData)
                    }
                }
                onObjectAdded: menuHistory.insertItem(index, object)
                onObjectRemoved: menuHistory.removeItem(object)
            }
            style: MenuStyle { //ttrying to make bold the current model
                itemDelegate {
                    label: Label {
                        font.bold: styleData.text==context.currentModel ? true : false
                        text: styleData.text
                    }
                }
            }
        }
        Menu {
            enabled: !context.isBusy
            title: qsTr("&Modules")
            MenuItem { action: checkAction }
            MenuSeparator { }
            MenuItem { action: anthroPersoAction }
            MenuItem { action: bodysectionModuleAction }
            MenuSeparator { }
            MenuItem { action: physPositioningInteractiveAction }
            MenuItem { action: physPositioningDeformationAction }
            MenuSeparator { }
            MenuItem { action: meshoptimizingAction }
			MenuSeparator { }
            MenuItem { action: krigingAction }
            MenuItem { action: contourDeformationAction }
            MenuItem { action: physScalingAction }
			MenuItem { action: scalingParameterAction }
			MenuSeparator { }
			MenuItem { action: childAction }
        }
        Menu {
            id: menuScripting
            enabled: !context.isBusy
            title: qsTr("&Scripting")
            // menu updated by updateRecentScriptMenu()
        }

        Menu {
            enabled: !context.isBusy
            title: qsTr("&Help")
            MenuItem { action: aboutAction }
            MenuItem { action: showUserGuideActionHTML }
			MenuItem { action: showUserGuideActionPDF }
        }
    }

    Component {
       id: moduleCategoryLabel
       Rectangle {
           Layout.fillWidth: true
           implicitHeight: label.implicitHeight
           color: "steelblue"
           Label {
               id: label
               anchors.left: parent.left
               anchors.leftMargin: 2
               font.italic: false
               font.bold: true
               text: labelText
           }
       }
    }

    RowLayout {
        anchors.fill: parent

        ColumnLayout {
            id: leftPanel
            Layout.fillHeight: true
            Layout.maximumWidth: 110
            anchors.top: parent.top 
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            GridLayout {
                id: iconGrid
                Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                columnSpacing: 0
                columns: 3
                ToolButton { action: openAction }
                ToolButton { action: saveAction }
                ToolButton { action: moduleParameterAction }
                ToolButton { action: runScriptAction }
            }

            GridLayout {
                id: modulesGrid
                Layout.fillWidth: true
                anchors.top: iconGrid.bottom
                anchors.bottom: logPanelButton.top
                anchors.topMargin: 5
                columns: 2
                Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Pre-Process" }
                ModuleButton {
                    Layout.fillWidth: true
                    action: checkAction
                }
                Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Scale" }
				 ModuleButton {
                    Layout.fillWidth: true
                    action: anthroPersoAction
                }
				ModuleButton {
                    Layout.fillWidth: true
                    action: bodysectionModuleAction
                }
                Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Position" }

                ModuleButton {
                    Layout.fillWidth: true
                    action: physPositioningInteractiveAction
                }
                ModuleButton {
                    Layout.fillWidth: true
                    action: physPositioningDeformationAction
                }
                Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Post-Process" }
                ModuleButton {
                    Layout.fillWidth: true
                    action: meshoptimizingAction
                }
				Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Tools" }
                ModuleButton {
                    Layout.fillWidth: true
                    action: krigingAction
                }
                ModuleButton {
                    Layout.fillWidth: true
                    action: contourDeformationAction
                }
                ModuleButton {
                    Layout.fillWidth: true
                    action: physScalingAction
                    BetaLabel { }
                }
				ModuleButton {
                    Layout.fillWidth: true
                    action: scalingParameterAction
                }
				Loader { sourceComponent: moduleCategoryLabel
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    property string labelText: "Custom" }
				ModuleButton {
                    Layout.fillWidth: true
                    action: childAction
                }
            }

            Button {
                id: logPanelButton
                anchors.bottom: leftPanel.bottom
                anchors.left: leftPanel.left
                anchors.right: leftPanel.right
                action: showLogPanelAction
            }
        }

        SplitView {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            Layout.fillHeight: true
            Layout.fillWidth: true
            orientation: Qt.Vertical
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumHeight: 400
                enabled: !context.isBusy
                Loader {
                    id: defaultModuleLoader
                    anchors.fill: parent
                }
                Loader {
                    id: sofaModuleLoader
                    anchors.fill: parent
                }
            }
            TextArea {
                id: logMessage
                anchors.left: parent.left
                anchors.right: parent.right
                Layout.fillWidth: true
                Layout.minimumHeight: 100
                enabled: true
                textFormat: TextEdit.RichText
                font.family: "Courier" // monospace font to allow for some "ascii-art" logging
                frameVisible: true
                readOnly: true
                property bool nbsp: false // when switched to true, all spaces will be replaced by non-breaking spaces (for ascii art)
                text: ""
                function scroll_to_bottom() {
                    logMessage.__verticalScrollBar.value = logMessage.__verticalScrollBar.maximumValue
                    return;
                }
                function onImportantMessageReceived() {
                    visible=true;
                }
                function clear() {
                    logMessage.cursorPosition = 0;
                    logMessage.text = "";
                }

                Component.onCompleted: {
                    context.importantMessageReceived.connect(logMessage.onImportantMessageReceived);
                    onHeightChanged.connect(logMessage.scroll_to_bottom);
                }
                Connections {
                    target: context
                    onNewLogMessage : {
                        if (logMessage.nbsp)
                            message = message.replace(/ /g, "\xa0")
                        logMessage.append(message);
                        logMessage.scroll_to_bottom();
                    }
                    onSetLogNBSP : {
                        logMessage.nbsp = value
                    }
                }

                ColumnLayout {
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.topMargin: 10
					anchors.rightMargin: 20
                    width: 100
                    ComboBox {
                        Layout.fillWidth: true
                        model: ListModel {
                            ListElement {text: "Default" }
                            ListElement {text: "Verbose"}
                            ListElement {text: "Very Verbose"}
                        }
                        currentIndex: context.verboseLevel()
                        onCurrentIndexChanged : {context.setVerboseLevel(currentIndex);}
                    }
                    Button {
                        Layout.fillWidth: true
						anchors.rightMargin: 20
						anchors.topMargin: 10
                        text: "clear"
                        onClicked: logMessage.clear()
                    }

                    states: State {
                        name: "closing";
                        when: {
                            logPanelButton.pressed == true;
                        }
                        PropertyChanges {
                            target: logMessage;
                            height: 0;
                        }
                    }

                    transitions: Transition {
                        from: ""; to: "closing"; reversible: true
                        ParallelAnimation {
                            NumberAnimation{ properties: "height"; duration: 500; easing.type: Easing.InOutQuad; }
                        }
                    }
                }
            }//TextArea
        }//SplitView
    }

    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        width: 100
        height: width
        running: context.isBusy

        Button {
            id: cancelOperationButton
            anchors.centerIn: parent
            visible: context.busyCancelVisible
            text: "Cancel"
            onClicked: {
                context.onCancelTaskButtonClicked();
            }
            enabled: !context.busyTaskCancelRequested
        }
        Connections {
            target: context
            onBusyTaskCancelRequestedChanged: {
                if (busyTaskCancelRequested)
                    context.logInfo("Cancel requested...");
                else
                    context.logInfo("Cancelled");
            }
        }
    }

    statusBar: StatusBar {
        RowLayout {
            width: parent.width
            spacing: 20
            Label { text: "Module: "+applicationWindow.currentModuleName }
            RowLayout {
                spacing: 0
                Label { text: "Project: " }
                Label {
                    id: projectStatus
                    text: projectFilePath
                    onTextChanged: {
                        if (context.projectFile.toString().length == 0) projectStatus.font.italic = true;
                        else projectStatus.font.italic = false;
                    }
                }
            }
            Label { text: "Current Model: "+context.currentModel }
            Item { Layout.fillWidth: true }
            Label { text: "Memory usage: "+ context.memoryUsage + " MB" }
        }
    }

    ImportExportMetadataDialog {
        id: importExportMetadataDialog
        onAccepted: {
            if (importExportMetadataDialog.operation === "import")
                context.loadMetadata(importExportMetadataDialog.metadataFilePath,
                                     importExportMetadataDialog.metadataType.anthropometry,
                                     importExportMetadataDialog.metadataType.entities,
                                     importExportMetadataDialog.metadataType.landmarks,
                                     importExportMetadataDialog.metadataType.genericmeta,
                                     importExportMetadataDialog.metadataType.contacts,
                                     importExportMetadataDialog.metadataType.joints,
                                     importExportMetadataDialog.metadataType.point,
                                     importExportMetadataDialog.metadataType.hbmParameter)
            else
                context.saveMetadata(importExportMetadataDialog.metadataFilePath,
                                     importExportMetadataDialog.metadataType.anthropometry,
                                     importExportMetadataDialog.metadataType.entities,
                                     importExportMetadataDialog.metadataType.landmarks,
                                     importExportMetadataDialog.metadataType.genericmeta,
                                     importExportMetadataDialog.metadataType.contacts,
                                     importExportMetadataDialog.metadataType.joints,
                                     importExportMetadataDialog.metadataType.controlpoint,
                                     importExportMetadataDialog.metadataType.hbmParameter)
        }
    }

    ImportEnvModelDialog {
        id: importEnvModelDialog
    }


    ModuleParameter {
        id: moduleParameterDialog
    }

    FileDialog {
        id: importHBMmodelDialog
        title: qsTr("Select HBM model...")
        nameFilters: ["*.pmr"]
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentImportHBMmodelFolder))
                folder = settings.recentImportHBMmodelFolder;
        }
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentImportHBMmodelFolder = folder;
            context.operationFinished.connect(importHBMmodelDialog.postImport);
            context.importModel(importHBMmodelDialog.fileUrl);
        }
        function postImport() {
            context.operationFinished.disconnect(importHBMmodelDialog.postImport);
            checkAction.trigger();
        }
    }

    FileDialog {
        id: openDialog
        title: qsTr("Open Project...")
        nameFilters: ["Project files (*.ppj)"]
		onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentModelFolder))
				folder = settings.recentModelFolder;
		}
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentModelFolder = folder;
            context.operationFinished.connect(importHBMmodelDialog.postImport);
            context.openProject(openDialog.fileUrl)
        }
    }

    Action {
        id: importMetadataAction
        enabled: context.hasModel
        text: qsTr("Import Piper Metadata")
        tooltip: qsTr("Import a Piper metadata")
        onTriggered: {
            importExportMetadataDialog.operation="import"
            importExportMetadataDialog.open()
        }
    }

    FileDialog {
        id: importTargetDialog
        title: qsTr("Import Target...")
        nameFilters: ["Target files (*.ptt)"]
		onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentTargetFolder))
				folder = settings.recentTargetFolder;
				
		}
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
			    settings.recentTargetFolder = folder;
            context.loadTarget(importTargetDialog.fileUrl)
        }
    }

    FileDialog {
        id: exportTargetDialog
        title: qsTr("Export Target...")
        selectExisting: false
        nameFilters: ["Target files (*.ptt)"]
		onVisibleChanged: {
            if(visible && context.doFolderExist(settings.recentTargetFolder))
				folder = settings.recentTargetFolder;
		}
        onAccepted: {
            context.saveTarget(exportTargetDialog.fileUrl)
        }
    }

    FileDialog {
        id: saveAsDialog
        title: qsTr("Save project as...")
        selectExisting: false
        nameFilters: ["Project files (*.ppj)"]
        onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentModelFolder))
				folder = settings.recentModelFolder;
		}
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
			    settings.recentModelFolder = folder;
            context.saveProject(saveAsDialog.fileUrl)
        }
    }
    
    FileDialog {
        id: exportDialog
        title: qsTr("Export FE model - Select the destination folder")
        selectMultiple: true
        selectExisting: true
        selectFolder: true
        onVisibleChanged:
		{
            if(visible && context.doFolderExist(settings.recentFEModelExportFolder))
                folder = settings.recentFEModelExportFolder;
		}
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentFEModelExportFolder = folder;
            context.exportModel(exportDialog.fileUrl)
        }
    }

    Action {
        id: exportMetadataAction
        enabled: context.hasModel
        text: qsTr("Export Piper metadata")
        tooltip: qsTr("Export Piper metadata")
        onTriggered: {
            importExportMetadataDialog.operation="export"
            importExportMetadataDialog.open();
        }
    }

    FileDialog {
        id: exportLandmarks
        title: qsTr("Export Landmarks - Select output file")
        selectExisting: false
        nameFilters: ["Landmark files (*.txt)"]
        onAccepted: {
            context.exportLandmarks(exportLandmarks.fileUrl);
        }
    }

    FileDialog {
        id: exportNodes
        title: qsTr("Export entities nodes - Select output folder")
        selectExisting: false
        selectFolder : true
        onAccepted: {
            context.exportEntityNodes(exportNodes.folder);
        }
    }

    RunScriptDialog {
        id: runScriptDialog        
    }
    
    MessageDialog {
        id: aboutDialog
        icon: StandardIcon.Information
        title: "About"
        text: "PIPER application\n"
              +"version "+Qt.application.version+"\n"
              +"compiled on: "+context.versionDate()+" - git hash: "+context.versionGitHash()+"\n\n"
              +"licence GPL v2 or later (see License file for more info)\n"
              +"\n"
        standardButtons: StandardButton.Close
    }
}


