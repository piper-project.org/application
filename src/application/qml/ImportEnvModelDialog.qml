// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0
import Piper 1.0
import VtkQuick 1.0

import piper.EnvManager 1.0

ModuleToolWindow {
    id: root
    title: "Environment models"

    property alias envFilePath: selectEnvModelDialog.fileUrl
	property alias formatRulesFilePath : formatRulesChooser.filePath
    property alias modelEnvLengthUnit : lengthUnitChooser.currentText
        
    Settings {
        id: settings // TODO: the settings from main.qml should be used here, this is a workaround for windows
        category: "application"
        property url recentEnvModelFolder
    }

    Component.onCompleted: {
        adjustWindowSizeToContent()
    }
    
    ColumnLayout{
        anchors.fill: parent
		enabled: !context.isBusy                 
		
	    EnvManager {
		    id: myEnvManager
		    onDefaultNameChanged: envNameTest.text=myEnvManager.defaultName()
        }
	    // table view of current environment files
	    TableView {
		    id: listEnvfiles
		    Layout.fillWidth: true
		    height: 150
		    selectionMode:SelectionMode.SingleSelection
		    model: myEnvManager.getModel()
            Component.onCompleted: {
                implicitWidth = nameColumn.width + valueColumn.width + visibleColumn.width + deleteColumn.width
            }
            onCurrentRowChanged: {
                if (currentRow === -1)
                    transformHint.visible = true
                else
                {
                    transformHint.visible = false
                    var scale = myEnvManager.getScale(model.get(currentRow).NameRole)
                    var translation = myEnvManager.getTranslation(model.get(currentRow).NameRole)
                    var rotation = myEnvManager.getRotation(model.get(currentRow).NameRole)
                    scaleX.value = scale[0]
                    scaleY.value = scale[1]
                    scaleZ.value = scale[2]
                    transX.value = translation[0]
                    transY.value = translation[1]
                    transZ.value = translation[2]
                    rotX.value = rotation[0]
                    rotY.value = rotation[1]
                    rotZ.value = rotation[2]
                }
            }
            TableViewColumn{
                id: deleteColumn
                role: "DeleteRole"
                title: "Delete"
                width: 40 // enough for a single button
                movable: false
                resizable: true
                delegate : Button{
                    text: qsTr('Delete')
                    onClicked: {
                        if (styleData.row === listEnvfiles.currentRow) listEnvfiles.currentRow = -1
                        myEnvManager.delEnvModel(listEnvfiles.model.get(styleData.row).NameRole)
                    }
                }
            }
		    //itemDelegate: editableDelegate
		    TableViewColumn {
			    id: nameColumn
			    role: "NameRole"
			    title: "Name"
			    movable: false
			    resizable: true
		    } // TableViewColumn
		    TableViewColumn {
			    id: valueColumn
			    role: "ValueRole"
			    title: "File"
			    movable: false
			    resizable: true
		    } // TableViewColumn
            TableViewColumn{
                id: visibleColumn
                role: "VisibleRole"
                title: "Visible"
                width: 40 // enough for a single checkbox
                movable: false
                resizable: true
                delegate : CheckBox{
                    checked: true
                    onCheckedChanged: myEnvManager.envVisible(listEnvfiles.model.get(styleData.row).NameRole, checked);
                }
            }
		    Menu { 
                id: contextMenu
			    MenuItem {
				    text: qsTr('Delete')
				    action: deleteAction
			    }
		    }
		    rowDelegate: Item {
                height: 20
                Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }//anchors
				    height: parent.height
				    color: styleData.selected ? 'lightblue' : 'white'
				    MouseArea {
					    anchors.fill: parent
					    acceptedButtons: Qt.RightButton
					    propagateComposedEvents: true
					    onReleased: {
						    if (typeof styleData.row === 'number') {
							    listEnvfiles.currentRow = styleData.row
							    if (mouse.button === Qt.RightButton) { // never true
								    contextMenu.popup()
							    }//if
						    }//if
						    mouse.accepted = false
					    }//onReleased
				    }//MouseArea
			    }//Rectangle
		    }//rowDelegate
	    } // TableView
    
        GroupBox
        {
            Layout.fillWidth: true
            title: "Load new environment"
            GridLayout {
                anchors.left: parent.left
                anchors.right: parent.right
                columns: 3
		        // name and file of the current selection
	            Label {
                    Layout.alignment: Qt.AlignLeft
                    text: "Name:"
			        Layout.columnSpan: 1
		        }
                Label {
                    Layout.alignment: Qt.AlignLeft
                    text: "File:"
			        //Layout.fillWidth: true
			        Layout.columnSpan: 2
		        }
                TextField {
                    id: envNameTest
			        text: myEnvManager.defaultName()
                    //Layout.fillWidth: true
                    placeholderText: qsTr("name env.")
                }
		        TextField {
                    id: envFileText
                    Layout.fillWidth: true
                    placeholderText: qsTr("Select environment model file")
                }
                Button {
                    tooltip: qsTr("Select environment model file")
                    iconSource: "qrc:///icon/document-open.png"
                    onClicked: selectEnvModelDialog.open()
                }
    	        FileDialog {
    		        id: selectEnvModelDialog
    		        title: qsTr("Select environment model file...")
                    nameFilters: ["LS-Dyna (*.dyn *.k *.key)", "Pamcrash (*.pc *.inc)", "*.*"]
                    onVisibleChanged: {
                        if(visible && context.doFolderExist(settings.recentEnvModelFolder))
                            folder = settings.recentEnvModelFolder;
                    }
    	            onAccepted: {
    	                if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                            settings.recentEnvModelFolder = folder;
    			        envFileText.text = selectEnvModelDialog.fileUrl
    		        }
    	        }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Format rules file:"
                }
		        FormatRulesChooser {
                    id: formatRulesChooser
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Model units:"
                }
		        LengthUnitChooser {
			        id: lengthUnitChooser
		        }
		        // add current env model selection
                Button {
			        id: addbutton
                    action: addEnvModelAction
			        Layout.columnSpan: 3
			        Layout.fillWidth: true
                }
		        Action {
		            id: addEnvModelAction
		            text: qsTr("add environment model file")
		            onTriggered: {
				        myEnvManager.addEnvModel( envNameTest.text, envFilePath, formatRulesFilePath, modelEnvLengthUnit)
				        envFileText.text=""
			        }
		        }
		        Action {
		            id: deleteAction
		            text: qsTr("delete environment model file")
		            onTriggered: {
				        myEnvManager.delEnvModel( myEnvManager.m_tablemodel.get( myEnvManager.m_tablemodel.currentIndex).NameRole)
                        listEnvfiles.currentRow = -1
			        }
		        }
            }
        }
        GroupBox
        {
            id: tranformGroupBox
            Layout.fillWidth: true
            title : "Transform environment"
            enabled: listEnvfiles.currentRow != -1

            property var transNbDecimals : {"mm": 0, "cm": 1, "dm": 2, "m": 3}
            property var lengthUnits : {"mm": 1000, "cm": 100, "dm": 10, "m": 1} // to convert length units

            GridLayout
            {
                columns: 6
                anchors.left: parent.left
                anchors.right: parent.right

                Label {
                    Layout.columnSpan: 6
                    id: transformHint
                    text: "Select an environment in the table above to transform it"
                    color: "red"
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Scale X:"
                }
                SpinBox {
                    id: scaleX
                    stepSize: 0.1
                    decimals : 2
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    value: 1.
                    onValueChanged : myEnvManager.setScale(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, scaleX.value, scaleY.value, scaleZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Scale Y:"
                }
                SpinBox {
                    id: scaleY
                    stepSize: 0.1
                    decimals : 2
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    value: 1.
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setScale(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, scaleX.value, scaleY.value, scaleZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Scale Z:"
                }
                SpinBox {
                    id: scaleZ
                    stepSize: 0.1
                    decimals: 2
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    value: 1.
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setScale(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, scaleX.value, scaleY.value, scaleZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Translate X:"
                }
                SpinBox {
                    id: transX
                    stepSize: 5e-3 * tranformGroupBox.lengthUnits[context.modelLengthUnit] // 5mm
                    suffix: context.modelLengthUnit
                    decimals : tranformGroupBox.transNbDecimals[context.modelLengthUnit]
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setTranslation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, transX.value, transY.value, transZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Translate Y:"
                }
                SpinBox {
                    id: transY
                    stepSize: 5e-3 * tranformGroupBox.lengthUnits[context.modelLengthUnit] // 5mm
                    suffix: context.modelLengthUnit
                    decimals : tranformGroupBox.transNbDecimals[context.modelLengthUnit]
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setTranslation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, transX.value, transY.value, transZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Translate Z:"
                }
                SpinBox {
                    id: transZ
                    stepSize: 5e-3 * tranformGroupBox.lengthUnits[context.modelLengthUnit] // 5mm
                    suffix: context.modelLengthUnit
                    decimals : tranformGroupBox.transNbDecimals[context.modelLengthUnit]
                    minimumValue: -Infinity
                    maximumValue: Infinity
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setTranslation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, transX.value, transY.value, transZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Rotate X:"
                }
                SpinBox {
                    id: rotX
                    stepSize: 5
                    suffix: '°'
                    decimals : 0
                    minimumValue: -360
                    maximumValue: 360
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setRotation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, rotX.value, rotY.value, rotZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Rotate Y:"
                }
                SpinBox {
                    id: rotY
                    stepSize: 5
                    suffix: '°'
                    decimals : 0
                    minimumValue: -360
                    maximumValue: 360
                    implicitWidth: scaleX.width
                    onValueChanged : myEnvManager.setRotation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, rotX.value, rotY.value, rotZ.value)
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text: "Rotate Z:"
                }
                SpinBox {
                    id: rotZ
                    stepSize: 5
                    suffix: '°'
                    decimals : 0
                    minimumValue: -360
                    maximumValue: 360
                    implicitWidth: scaleX.width
                    onValueChanged: myEnvManager.setRotation(listEnvfiles.model.get(listEnvfiles.currentRow).NameRole, rotX.value, rotY.value, rotZ.value)
                }
            }
        }
        GroupBox
        {
            title: "Visual properties"
            RowLayout
            {
                Label {
                    text: "Environment opacity"
                }
                SpinBox{
                    stepSize: 0.1
                    decimals: 2
                    minimumValue: 0
                    maximumValue: 1
                    value: 1
                    onValueChanged : context.envOpacityChanged(value)
                }
            }
        }
    }//ColumnLayout		
}//ModuleToolWindow
