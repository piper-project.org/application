// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0
import piper.MetaEditor 1.0
import VtkQuick 1.0

GroupBox {
    title: "Metadata Editor - EXPERIMENTAL"
    GridLayout {
        anchors.centerIn: parent
        columns: 7
        ModuleToolWindowButton {
            iconSource: "qrc:///icon/editor-entity.png"
            tooltip: "Entity Editor"
            toolWindow: editorEntityWindow
        }
        ModuleToolWindowButton {
            iconSource: "qrc:///icon/editor-landmark.png"
            tooltip: "Landmark Editor"
            toolWindow: editorLandmarkWindow
            onClicked: {
                if (checked) {
                    contextMetaManager.setDisplayModeActive(
                                4, true) // turn on landmark display
                } else {
                    contextMetaManager.setDisplayModeActive(
                                4, false) // turn off landmark display
                }
            }
        }
        ModuleToolWindowButton {
            iconSource: "qrc:///icon/editor-namedmetadata.png"
            tooltip: "NamedMetadata Editor"
            toolWindow: editorGenericMetadataWindow
        }
        ModuleToolWindowButton {
            Layout.row: 0
            Layout.column: 3
            iconSource: "qrc:///icon/editor-joint.png"
            tooltip: "Joint Editor"
            toolWindow: editorJointWindow
            onClicked: {
                if (checked) {
                    contextMetaManager.setDisplayModeActive(
                                64, true) // turn on axes display
                } else {
                    contextMetaManager.setDisplayModeActive(
                                64, false) // turn off axes display
                }
            }
        }
   /*     ModuleToolWindowButton {
            iconSource: "qrc:///icon/editor-control-point.png"
            tooltip: "Control Point Editor"
            toolWindow: editorControlPointWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(32, true); // turn on point display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(32, false); // turn off point display
                }
            }
        }*/
        ModuleToolWindowButton {
            visible: false
            Layout.row: 0
            Layout.column: 4
            iconSource: "qrc:///icon/editor-frame.png"
            tooltip: "Frame Editor"
            toolWindow: editorFrameWindow
        }
    }
    EditorEntityWindow {
        id: editorEntityWindow
    }
    EditorLandmarkWindow {
        id: editorLandmarkWindow
    }
    EditorGenericMetadataWindow {
        id: editorGenericMetadataWindow
    }
    EditorFrameWindow {
        id: editorFrameWindow
    }
    EditorJointWindow {
        id: editorJointWindow
    }
 //   EditorControlPointWindow {
   //     id: editorControlPointWindow
    //}
}
