// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Controls.Styles 1.2
import Piper 1.0

ModuleToolWindow
{
    id: exportMetadataWindow
    title: "Export Metadata"
    minimumHeight : 675
    minimumWidth : 620
    property int nb: 0;

    ColumnLayout
    {
        id:grid
        property int recHeightRow1: 300
        property int recWidthRow1: 300
        property int recHeightRow2: 300
        property int recWidthRow2: 300
        anchors.leftMargin: 7
        anchors.topMargin: 7
        width: parent.width
        Layout.fillHeight: true
        RowLayout
        {
            width: parent.width

            // Entity display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Entity"
                    ColumnLayout
                    {
                        Label{ text:"Entity select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    entityTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    entityTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(entityTableView.selection.contains(i))
                                        {
                                            entityTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            entityTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:listModel
                        }
                        TableView
                        {
                            id: entityTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: listModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getEntityList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    listModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Entities"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    entityTableView.getList()
                                }
                            }
                            Connections
                            {
                                target: entityTableView.selection
                                onSelectionChanged :
                                {
                                    var listNames = [];
                                    for (var i = 0; i < entityTableView.model.count; i++)
                                    {
                                        if(entityTableView.selection.contains(i)){
                                            listNames.push(entityTableView.model.get(i).role);
                                        }
                                    }
                                    myMetaEditor.setEntityExportList(listNames);
                                }
                            }
                        }
                    }
                }
            }

            //Landmarks display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Landmark"
                    ColumnLayout
                    {
                        Label{ text:"Landmark select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    landmarkTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    landmarkTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(landmarkTableView.selection.contains(i))
                                        {
                                            landmarkTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            landmarkTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:landmarkListModel
                        }
                        TableView
                        {
                            id: landmarkTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: landmarkListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getLandmarkList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    landmarkListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Landmarks"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    landmarkTableView.getList()
                                }
                            }
                            Connections
                            {
                                target: landmarkTableView.selection
                                onSelectionChanged :
                                {
                                    var listNames = [];
                                    for (var i = 0; i < landmarkTableView.model.count; i++)
                                    {
                                        if(landmarkTableView.selection.contains(i)){
                                            listNames.push(landmarkTableView.model.get(i).role);
                                        }
                                    }
                                    myMetaEditor.setLandmarkExportList(listNames);
                                }
                            }
                        }
                    }
                }
            }
        }
        // Second Row

        RowLayout
        {
            // GenericMetadata display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"GenericMetadata"
                    ColumnLayout
                    {
                        Label{ text:"GenericMetadata select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    genericMetadataTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    genericMetadataTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(genericMetadataTableView.selection.contains(i))
                                        {
                                            genericMetadataTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            genericMetadataTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id: genericMetadataListModel
                        }
                        TableView
                        {
                            id: genericMetadataTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: genericMetadataListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getGenericMetadataList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    genericMetadataListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "GenericMetadata"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    genericMetadataTableView.getList()
                                }
                            }
                            Connections
                            {
                                target: genericMetadataTableView.selection
                                onSelectionChanged :
                                {
                                    var listNames = [];
                                    for (var i = 0; i < genericMetadataTableView.model.count; i++)
                                    {
                                        if(genericMetadataTableView.selection.contains(i)){
                                            listNames.push(genericMetadataTableView.model.get(i).role);
                                        }
                                    }
                                    myMetaEditor.setGenericMetadataExportList(listNames);
                                }
                            }
                        }
                    }
                }
            }

            // Joint display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Joints"
                    ColumnLayout
                    {
                        Label{ text:"Joint select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    jointTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    jointTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(jointTableView.selection.contains(i))
                                        {
                                            jointTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            jointTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id: jointListModel
                        }
                        TableView
                        {
                            id: jointTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: jointListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getJointList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    jointListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Joints"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    jointTableView.getList()
                                }

                            }
                            Connections
                            {
                                target: jointTableView.selection
                                onSelectionChanged :
                                {
                                    var listNames = [];
                                    for (var i = 0; i < jointTableView.model.count; i++)
                                    {
                                        if(jointTableView.selection.contains(i)){
                                            listNames.push(jointTableView.model.get(i).role);
                                        }
                                    }
                                    myMetaEditor.setJointExportList(listNames);
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            id: buttonsColumn
            ToolButton { action: saveMetadataAction }
            FileDialog {
                id: saveAsDialog
                title: qsTr("Save Metadata as...")
                selectExisting: false
                nameFilters: ["Metadata files (*.xml)"]
                onAccepted: {
                     myMetaEditor.saveMetadataBusyIndicator(saveAsDialog.fileUrl)
                }
            }
            Action {
                id: saveMetadataAction
                text: qsTr("Save Metadata as")
                iconSource: "qrc:///icon/document-save.png"
                onTriggered: saveAsDialog.open()
            }
        }
    }
}
