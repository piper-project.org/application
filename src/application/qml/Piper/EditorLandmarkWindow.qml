// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import Piper 1.0

ModuleToolWindow {
    id: mainWindow
    title: "Landmark Editor"

    minimumHeight: 460
    minimumWidth: 375

    property var landmarkList: []
    property string lmrkType: ""

    property string create: ""

    property int oldIndex: 0

    onCreateChanged: {
        console.log("Create changed : " + create)
        var vals = contextMetaManager.getLandmarkList()
        if (create !== "") {
            landmarkName.text = create
            var found = false
            for (var i = 0; i < vals.length; i++)
                if (vals[i] === create) {
                    found = true
                    break
                }
            console.log("Create found : " + found)
            if (found == true) {
                editLandmarkRadioButton.checked = true
                searchTextField.text = create
                var type = contextMetaManager.getTypeOfLandmark(create)
                if (type === "POINT") {
                    landmarkType.currentIndex = 2
                    //landmarkType.model = ["POINT", "SPHERE", "BARYCENTER"]
                    //moveLandmark.enabled = true
                } else if (type === "BARYCENTER") {
                    landmarkType.currentIndex = 1
                    //landmarkType.model = ["BARYCENTER", "POINT", "SPHERE"]
                    //moveLandmark.enabled = false
                } else if (type === "SPHERE") {
                    landmarkType.currentIndex = 0
                    //landmarkType.model = ["SPHERE", "POINT", "BARYCENTER"]
                    //moveLandmark.enabled = false
                }
            } else {
                createLandmarkRadioButton.checked = true
            }
        }
    }

    MessageDialog {
        id: messageDialog
        visible: false
        title: "Alert"
        text: " text for message dialog "
        onAccepted: {

        }
    }

    MessageDialog {
        id: dialogRemove
        title: "Remove?"
        icon: StandardIcon.Question
        text: "Are sure you want to remove the selected Landmark?"
        standardButtons: StandardButton.Yes | StandardButton.No
        visible: false
        onYes: {
            for (var i = 0; i < listModel.count; i++) {
                if (tableView.selection.contains(i)) {
                    myMetaEditor.removeLandmark(listModel.get(i).role)
                }
            }
            tableView.selection.clear()
            updateLandmark.enabled = false
            removeLandmark.enabled = false
        }
    }

    MessageDialog {
        id: dialogLandmarkChange
        title: "Change Landmark type?"
        icon: StandardIcon.Question
        text: "Are sure you want to change the landmark type?"
        detailedText: "Changing landmark type would change the contraints involved.\n"
        standardButtons: StandardButton.Yes | StandardButton.No
        visible: false
        onYes: {
            switch (landmarkType.currentIndex) {
            case 0:
                landmarkTypeHint.text = "Select atleast 5 nodes."
                break
            case 1:
                landmarkTypeHint.text = "Select atleast 1 node."
                break
            case 2:
                landmarkTypeHint.text = "Select only 1 node."
                //breakription.message = "A Centroid of section"
                break
            }
            oldIndex = landmarkType.currentIndex
        }
        onNo: {
            landmarkType.currentIndex = oldIndex
        }
    }

    ColumnLayout {
        anchors {
            top: title.bottom
            left: parent.left
            right: parent.right
        }
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        ListModel {
            id: listModel
        }

        GroupBox {
            title: "Action"

            RowLayout {
                ExclusiveGroup {
                    id: tabPositionGroup
                }
                RadioButton {
                    text: "Create Landmark"
                    id: createLandmarkRadioButton
                    checked: true
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if (checked === true) {
                            landmarkName.visible = true
                            searchTextField.visible = false
                            tableView.visible = false
                            typeSelection.visible = true
                            landmarkTypeHint.visible = true
                            pickersColumn.visible = true
                            addLandmark.visible = true
                            updateLandmark.visible = false
                            removeLandmark.visible = false
                            copyLandmark.visible = false
                            copyLandmarkLabel.visible = false
                            copyLandmarkName.visible = false
                        }
                    }
                }
                RadioButton {
                    text: "Edit Landmark"
                    id: editLandmarkRadioButton
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if (checked === true) {
                            landmarkName.visible = false
                            searchTextField.visible = true
                            tableView.visible = true
                            typeSelection.visible = true
                            landmarkTypeHint.visible = true
                            pickersColumn.visible = true
                            addLandmark.visible = false
                            updateLandmark.visible = true
                            removeLandmark.visible = true
                            copyLandmark.visible = false
                            copyLandmarkLabel.visible = false
                            copyLandmarkName.visible = false
                        }
                    }
                }
                RadioButton {
                    visible: false
                    text: "Copy Landmark"
                    id: copyLandmarkRadioButton
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if (checked === true) {
                            landmarkName.text = ""
                            landmarkName.visible = true
                            searchTextField.visible = true
                            tableView.visible = true
                            typeSelection.visible = false
                            landmarkTypeHint.visible = false
                            pickersColumn.visible = false
                            addLandmark.visible = false
                            updateLandmark.visible = false
                            removeLandmark.visible = false
                            copyLandmark.visible = true
                            copyLandmarkLabel.visible = true
                            copyLandmarkName.visible = true
                        }
                    }
                }
            }
        }

        RowLayout {


            Label {
                id: copyLandmarkLabel
                text: "Landmark to be copied : "
                visible: false
                onVisibleChanged: {
                    landmarkLabel.visible = copyLandmarkLabel.visible
                }
            }
            TextField {
                id: copyLandmarkName
                placeholderText: "Select a landmark from the table"
                visible: false
                readOnly: true
                Layout.fillWidth: true
            }
        }

        RowLayout {

            Label {
                id: landmarkLabel
                text: "New Landmark name : "
                visible: false
            }

            TextField {
                id: landmarkName
                placeholderText: qsTr("Enter New Landmark Name")
                Layout.fillWidth: true

                onTextChanged: {

                    if (landmarkName.text.length <= 0) {
                        addLandmark.enabled = false
                        buttonHint.visible = false
                        pickersColumn.enabled = false
                        copyLandmark.enabled = false
                    } else {
                        pickersColumn.enabled = true
                        for (var i = 0; i < tableView.model.count; i++) {
                            if (landmarkName.text.toUpperCase(
                                        ) === tableView.model.get(
                                        i).role.toUpperCase()) {
                                addLandmark.enabled = false
                                copyLandmark.enabled = false
                                buttonHint.visible = false
                                tableView.selection.select(i)
                                break
                            } else {
                                addLandmark.enabled = true
                                buttonHint.visible = false
                                if (tableView.selection.count > 0) {
                                    copyLandmark.enabled = true
                                }

                            }
                        }
                    }
                }
            }
        }

        TextField {
            id: searchTextField
            placeholderText: qsTr("Search")
            Layout.row: 1
            Layout.column: 0
            Layout.fillWidth: true
            visible: false
            onTextChanged: {
                tableView.getList(searchTextField.text)
            }
        }

        TableView {
            id: tableView
            alternatingRowColors: false
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            model: listModel
            visible: false

            property variant vals
            property variant types

            onRowCountChanged: {
                if (tableView.rowCount === 1) {
                    updateLandmark.enabled = true
                    removeLandmark.enabled = true
                } else {
                    updateLandmark.enabled = false
                    removeLandmark.enabled = false
                }
            }

            function processClickedItem(index) {

                if (tableView.selection.contains(index)) {
                    contextMetaManager.landmarkDisplay(true, listModel.get(
                                                           index).role, false)
                } else {
                    contextMetaManager.landmarkDisplay(false, listModel.get(
                                                           index).role, false)
                }

                if (tableView.selection.contains(index)) {
                   // myMetaEditor.selectLandmarkActors(true, listModel.get(index).role)

                    updateLandmark.enabled = true
                    removeLandmark.enabled = true

//                    if (listModel.get(index).type === "POINT") {
//                        moveLandmark.enabled = true
//                    } else {
//                        moveLandmark.enabled = false
//                    }
                    oldIndex = landmarkType.find(listModel.get(index).type)
                    landmarkType.currentIndex = landmarkType.find(
                                listModel.get(index).type)
                    landmarkName.text = listModel.get(index).role
                    copyLandmarkName.text = listModel.get(index).role
                } else {
                    //myMetaEditor.selectLandmarkActors(false, listModel.get(index).role)
                }
                if (copyLandmarkRadioButton.checked === true)
                    landmarkName.text = ""
            }

            TableViewColumn {
                id: ccRole
                role: "role"
                title: "Landmarks"
                width: 230
            }
            TableViewColumn {
                id: ccType
                role: "type"
                title: "Type"
                width: 80
            }

            onClicked: processClickedItem(row)

            function getList(query) {
                listModel.clear()
                vals = contextMetaManager.getLandmarkList()
                types = contextMetaManager.getLandmarkTypeList()
                for (var i = 0; i < vals.length; i++) {
                    if (!query) {
                        listModel.append({
                                             role: vals[i],
                                             type: types[i]
                                         })
                    } else {
                        if (vals[i].toUpperCase().indexOf(
                                    searchTextField.text.toUpperCase(
                                        )) !== -1) {
                            listModel.append({
                                                 role: vals[i],
                                                 type: types[i]
                                             })
                        }
                    }
                }
            }
            Connections {
                target: context
                onMetadataChanged: {
                    tableView.getList()
                }
            }
            Connections
            {
                target: context
                onVisDataLoaded:
                {
                    tableView.getList();
                }
            }
            Component.onCompleted: {
                getList()
                ccType.width = tableView.width - ccRole.width - 5
            }
        }

        RowLayout {
            id: typeSelection
            Layout.fillWidth: true
            Label {
                text: "Select Type : "
            }
            ComboBox {
                id: landmarkType
                Layout.fillWidth: true
                currentIndex: -1

                model: ["SPHERE", "BARYCENTER", "POINT"]
                onCurrentTextChanged: {
                     if (editLandmarkRadioButton.checked===true && currentIndex != oldIndex
                            && landmarkType.currentIndex !== landmarkType.find(
                                listModel.get(tableView.currentRow).type)) {
                        dialogLandmarkChange.open()
                    }
                    if (currentText!="POINT"){
                        //moveLandmark.enabled = false
                        boxNodePickerButton.enabled = true
                        rubberbandElementPicker.enabled = true
                        singleElementPicker.enabled = true
                        boxElementPicker.enabled = true
                    }
                    else{
                        //moveLandmark.enabled = true
                        boxNodePickerButton.enabled = false
                        rubberbandElementPicker.enabled = false
                        singleElementPicker.enabled = false
                        boxElementPicker.enabled = false
                    }
                }
                onCurrentIndexChanged: {
                    lmrkType = model[currentIndex]
                }
            }

            Button {
                visible: false
                id: typeDescription
                property string message: ""
                iconSource: "qrc:///icon/information-icon.png"
                onClicked: {
                    messageDialog.text = typeDescription.message
                    messageDialog.visible = true
                }
            }
        }
        Label {
            id: landmarkTypeHint
            text: "Please select nodes to define the landmark"
            font.pixelSize: 10
            color: "red"
        }

        ExclusiveGroup {
            id: group
        }
        Label {
            id: hintLabel
            visible: false
        }
        GroupBox {
            title: "Pickers"
            id: pickersColumn
            enabled: false
            ColumnLayout{
                RowLayout{
                    Layout.fillWidth: true
                    GridLayout {
                        id: pickerButtons
                        ExclusiveGroup{
                            id : pickerGroup
                        }
                        Label {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            text: "Single Pick"
                            Layout.row: 0
                            Layout.column: 1
                        }
                        Label {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            text: "Rubber Band"
                            Layout.row: 0
                            Layout.column: 2
                        }
                        Label {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            text: "Box"
                            Layout.row: 0
                            Layout.column: 3
                        }
//                        Label {
//                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
//                            text: "Move"
//                            Layout.row: 0
//                            Layout.column: 4
//                        }
                        Label {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            text: "Node"
                            Layout.row: 1
                            Layout.column: 0
                        }
                        Button {
                            id : singleNodePicker
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            Layout.row: 1
                            Layout.column: 1
                            tooltip: "Node Picker"
                            checkable: true
                            iconSource: "qrc:///icon/picker-node.png"
                            exclusiveGroup: pickerGroup
                            onClicked: {
                                if (checked) {
                                    pickerButtons.uncheckPickerButtons()
                                    selectingNodesOfSelectedElements.enabled = false
                                    hintLabel.text = contextVtkDisplay.SetPickingType(0)
                                } else
                                    hintLabel.text = contextVtkDisplay.SetPickingType(
                                                -1)
                            }
                        }
                        Button {
                            id:boxNodePickerButton
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            Layout.row: 1
                            Layout.column: 3
                            tooltip: "Box Node Picker"
                            checkable: true
                            iconSource: "qrc:///icon/box-node.png"
                            exclusiveGroup: pickerGroup
                            onClicked: {
                                if (checked) {
                                    pickerButtons.uncheckPickerButtons()
                                    selectingNodesOfSelectedElements.enabled = false
                                    hintLabel.text = contextVtkDisplay.SetPickingType(7)
                                } else
                                    hintLabel.text = contextVtkDisplay.SetPickingType(
                                                -1)
                            }
                        }
//                        Button {
//                            id: moveLandmark
//                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
//                            Layout.row: 1
//                            Layout.column: 4
//                            iconSource: "qrc:///icon/drag-landmark.png"
//                            tooltip: "Move Landmark"
//                            exclusiveGroup: pickerGroup
//                            checkable: true
//                            enabled: false
//                            onClicked: {
//                                if (checked) {
//                                    pickerButtons.uncheckPickerButtons()
//                                    hintLabel.text = contextVtkDisplay.SetPickingType(9)
//                                    selectingNodesOfSelectedElements.enabled = false
//                                } else
//                                    hintLabel.text = contextVtkDisplay.SetPickingType(
//                                                -1)
//                            }
//                        }
                        Label {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            text: "Element"
                            Layout.row: 2
                            Layout.column: 0
                        }
                        Button
                        {
                            id : singleElementPicker
                            exclusiveGroup: pickerGroup
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            Layout.row: 2
                            Layout.column : 1
                            tooltip: "Element Picker"
                            checkable: true
                            iconSource: "qrc:///icon/picker-element.png"
                            onClicked:
                            {
                                if (checked)
                                {
                                    hintLabel.text = contextVtkDisplay.SetPickingType(2);
                                    selectingNodesOfSelectedElements.enabled = true
                                }
                                else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                            }
                        }
                        Button
                        {
                            id : rubberbandElementPicker
                            exclusiveGroup: pickerGroup
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            Layout.row: 2
                            Layout.column : 2
                            tooltip: "Rubberband Element Picker"
                            checkable: true
                            iconSource: "qrc:///icon/rubberband-element.png"
                            onClicked:
                            {
                                if (checked)
                                {
                                    hintLabel.text = contextVtkDisplay.SetPickingType(5);
                                    selectingNodesOfSelectedElements.enabled = true
                                }
                                else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                            }
                        }
                        Button
                        {
                            id : boxElementPicker
                            exclusiveGroup: pickerGroup
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            Layout.row: 2
                            Layout.column : 3
                            tooltip: "Box Element Picker"
                            checkable: true
                            iconSource: "qrc:///icon/box-element.png"
                            onClicked:
                            {
                                selectingNodesOfSelectedElements.enabled = true
                                if (checked)
                                {
                                    hintLabel.text = contextVtkDisplay.SetPickingType(8);
                                    selInsideBoxButton.enabled = true
                                    deselInsideBoxButton.enabled = true
                                }
                                else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                            }
                        }
                        // uncheck all the buttons in the pickerButtons panel
                        function uncheckPickerButtons() {
                            for (var i = 0; i < pickerButtons.children.length; i++) {
                                if (pickerButtons.children[i].checkable)
                                    pickerButtons.children[i].checked = false
                            }
                        }
                    }
                }
                RowLayout{
                    Layout.fillWidth: true
                    Button
                    {
                        id : selectingNodesOfSelectedElements
                        enabled: false
                        text: "    Select nodes of selected elements    "
                        onClicked: contextVtkDisplay.SelectNodesByElements(false)
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Button {
                id: addLandmark
                enabled: false
                text: "Create"
                tooltip: "Create the new landmark"
                onClicked: {
                    console.log("Creating landmark : "+landmarkName.text+" Type : "+landmarkType.currentText)
                    myMetaEditor.getSelectedNodeIDsBusyIndicator(
                                landmarkName.text, landmarkType.currentText, 0)
                }
            }
            Button {
                id: updateLandmark
                enabled: false
                visible: false
                text: "Update"
                tooltip: "Update Landmark"
                onClicked: {
                    for (var i = 0; i < listModel.count; i++) {
                        if (tableView.selection.contains(i)) {
                            myMetaEditor.getSelectedNodeIDsBusyIndicator(
                                        listModel.get(i).role,
                                        landmarkType.currentText, 0, 1)
                            tableView.selection.clear()
                            updateLandmark.enabled = false
                            removeLandmark.enabled = false
                        }
                    }
                }
            }
            Button {
                id: removeLandmark
                enabled: false
                visible: false
                text: "Remove"
                tooltip: "Remove Landmark"
                onClicked: {
                    dialogRemove.open()
                }
            }
        }
        Label {
            visible: false
            id: buttonHint
            text: "Landmark name already exists."
            font.pixelSize: 10
            color: "red"
        }
        RowLayout {
            Button {
                id: copyLandmark
                enabled: false
                visible: false
                text: "Copy"
                tooltip: "Copy"
                onClicked: {
                    myMetaEditor.getSelectedNodeIDsBusyIndicator(
                                landmarkName.text, mainWindow.lmrkType, 0)
                }
            }
        }
    }
}
