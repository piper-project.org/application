// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
    title: "Clipping Plane Options"
    minimumWidth: 250

    ColumnLayout {
        RowLayout {
            GroupBox {
                title: "Activate Global Clipping"

                RowLayout {
                    ExclusiveGroup {
                        id: useGlobalClippingGroup
                    }
                    RadioButton {
                        text: "Activate"
                        id: activateGlobalClipping
                        exclusiveGroup: useGlobalClippingGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                clippilngTools.enabled = true
                                contextVtkDisplay.UseGlobalClipping(true);
                                contextVtkDisplay.Render();
                            }
                        }
                    }
                    RadioButton {
                        text: "Deactivate"
                        id: deactivateGlobalClipping
                        checked: true
                        exclusiveGroup: useGlobalClippingGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                clippilngTools.enabled = false
                                contextVtkDisplay.UseGlobalClipping(false);
                                contextVtkDisplay.Render();
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            GroupBox {
                title: "Tools"
                id: clippilngTools
                enabled: false

                GroupBox {
                    title: "Choose Clipping Plane"
                    ColumnLayout{
                        RowLayout {
                            ExclusiveGroup {
                                id: clippingPlaneGroup
                            }
                            RadioButton {
                                text: "X"
                                exclusiveGroup: clippingPlaneGroup
                                id: radioButtonX
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(1, 0, 0);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                            RadioButton {
                                text: "-X"
                                exclusiveGroup: clippingPlaneGroup
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(-1, 0, 0);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                            RadioButton {
                                text: "Y"
                                exclusiveGroup: clippingPlaneGroup
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(0, 1, 0);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                            RadioButton {
                                text: "-Y"
                                exclusiveGroup: clippingPlaneGroup
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(0, -1, 0);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                            RadioButton {
                                text: "Z"
                                exclusiveGroup: clippingPlaneGroup
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(0, 0, 1);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                            RadioButton {
                                text: "-Z"
                                exclusiveGroup: clippingPlaneGroup
                                onCheckedChanged: {
                                    if (checked === true) {
                                        contextVtkDisplay.SetGlobalClipPlaneNormal(0, 0, -1);
                                        contextVtkDisplay.Render();
                                    }
                                }
                            }
                        }
                        RowLayout{
                            Label { text: "Distance:" }
                            SpinBox {
                                id: distance
                                decimals: 3
                                stepSize: 0.1
                                minimumValue: -99999999
                                maximumValue: 99999999
                                value: 0
                                onValueChanged: {
                                    contextVtkDisplay.SetGlobalClipPlanePosition(distance.value);
                                    contextVtkDisplay.Render();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted : {
        radioButtonX.checked = true
    }
}
