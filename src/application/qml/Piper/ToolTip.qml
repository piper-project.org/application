// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4

MouseArea {
    id: root
    anchors.fill: parent
    hoverEnabled:true

    property string text: "empty tooltip"
    property int textWidth : 150

    // display delay in ms
    property int delay: 1000
    property int xOffset: 0
    property int yOffset: 20

    onEntered: d.init()
    onExited: d.release();

    onPressed: {
        d.release();
        mouse.accepted = false;
    }

    onWheel: {
        d.release();
        wheel.accepted = false;
    }

    Component {
        id: toolTipComponent

        Rectangle {
            implicitWidth: text.contentWidth
            implicitHeight: text.contentHeight

            color: "lightgoldenrodyellow"
            border.width: 1
            border.color: "black"
            z: 9999
            TextArea {
                id: text
                width: root.textWidth
                height: contentHeight
                backgroundVisible: false
                frameVisible: false
                wrapMode: TextEdit.WordWrap
                textFormat: TextEdit.AutoText
                verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                text: root.text
            }
        }
    }

    Timer {
        id: timer
        running: false
        repeat: false
        interval: root.delay
        onTriggered: {
            var parentWindow = context.itemWindow(root);
            var pos = root.mapToItem(parentWindow.contentItem, mouseX + root.xOffset, mouseY + root.yOffset);
            d.toolTip = toolTipComponent.createObject(parentWindow.contentItem, {x: pos.x, y: pos.y});

            // make sure the tool tip fits in its parent window
            d.toolTip.x = d.toolTip.x + Math.max(0, Math.min(pos.x, parentWindow.width - d.toolTip.width)) - pos.x;
            d.toolTip.y = d.toolTip.y + Math.max(0, Math.min(pos.y, parentWindow.height - d.toolTip.height)) - pos.y;
        }
    }

    QtObject {
        id: d

        property Item toolTip

        function init() {
            timer.start()
        }
        function release() {
            timer.stop();
            if(toolTip)
                toolTip.destroy();
        }
    }

}
