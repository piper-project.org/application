// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQml 2.2
import QtQuick 2.5
import QtQuick.Window 2.2
import Qt.labs.settings 1.0

Window {
    id: root
    visible: false
    modality: Qt.NonModal
    flags: Qt.Tool
    color: "lightgray"
    contentItem.enabled: !context.isBusy

    property int widthMargin: ("windows" === Qt.platform.os) ? 20 : 0
    property int heightMargin: ("windows" === Qt.platform.os) ? 20 : 0


    /** bind the window position to the moduleLoader position
     * so as to initialize the tool window in the top-left corner of the moduleLoader
     * \todo find a smarter way to position tool window ?
     */
    x: settings.x
    y: settings.y

    Settings {
        id: settings
        property int x: applicationWindow.x+0.5*applicationWindow.width-root.width
        property int y: applicationWindow.y+0.8*applicationWindow.height-root.height
        Component.onCompleted: {
            category = applicationWindow.currentModuleName + "_" + root.title.replace(" ","_");
        }
    }

    Component.onDestruction: {
        settings.x = root.x;
        settings.y = root.y;
    }

    /// set the window size to the implicit size of its content
    function adjustWindowSizeToContent() {
        width = contentItem.children[0].implicitWidth + widthMargin;
        height = contentItem.children[0].implicitHeight + heightMargin;
    }

    /// fix the window size to its current size
    function setCurrentWindowSizeFixed() {
        minimumWidth = maximumWidth = width;
        minimumHeight = maximumHeight = height;        
    }

    /// set the minimum window size to its current size
    function setCurrentWindowSizeMinimum() {
        minimumWidth=width;
        minimumHeight=height;
    }

    /// adjustWindowSizeToContent that will work for windows on which setCurrentWindowSizeFixed was called before. The window will stay fixed afterwards
    function adjustWindowSizeToContent_forFixed() {
        minimumWidth = maximumWidth = contentItem.children[0].implicitWidth + widthMargin;
        minimumHeight = maximumHeight = contentItem.children[0].implicitHeight + heightMargin;
        width = minimumWidth
        height = minimumHeight
    }

    onVisibleChanged: {
        // break the above binding to remember window user position when it is shown/hidden again
        if (settings.y < 0)
            settings.y = applicationWindow.y+0.8*applicationWindow.height-root.height
        else
            settings.y=settings.y;
        if (settings.x < 0)
            settings.x = applicationWindow.x+0.5*applicationWindow.width-root.width
        else
            settings.x=settings.x;
        // make sure window will not render off the screen and be unavailable to drag back (at least part of the title bar must be clickable
        // but getting size of the title bar is not trivial to do platform-independently, so let's just use some very small margins
        if (y < 10) y = 10;
        if (x < -width + 2) x = -width + 2;
    }

    onActiveFocusItemChanged: {
        // make inactive window transluscent
        if (activeFocusItem!=null) opacity=1.0;
        else opacity=0.9;
    }

}
