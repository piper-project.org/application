// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

RowLayout {
    id: root
    property string highConfidenceText: "High confidence"
    property string mediumConfidenceText: "Medium confidence"
    property string lowConfidenceText: "Low confidence"

    property int confidence: 0

    Image {
        id: image
        sourceSize.width: 24
        sourceSize.height: 24
        fillMode: Image.Stretch
    }
    Label {
        id: label
    }

    function update() {
        if (0 === root.confidence) {
            image.source = "qrc:///icon/greenCircle.png";
            label.text = highConfidenceText;
        }
        else if (1 == root.confidence) {
            image.source = "qrc:///icon/orangeCircle.png";
            label.text = mediumConfidenceText;
        }
        else if  (2 == root.confidence) {
            image.source = "qrc:///icon/redCircle.png";
            label.text = lowConfidenceText;
        }
        else
            console.warn("Invalid confidence value", root.confidence);
    }

    onConfidenceChanged: update()

    Component.onCompleted: update()

}
