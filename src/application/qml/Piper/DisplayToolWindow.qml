// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.1

import "../metaEditor"

import Piper 1.0

ModuleToolWindow 
{
    title: "Display Control"
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb_entities: 0;
    property int nb_landmarks: 0;
    property int nb_frames: 0;

    Rectangle 
    {
        implicitWidth:1000
        implicitHeight:220
        Layout.fillWidth: true
        height:childrenRect.height

        ColumnLayout 
        {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop

            RowLayout
            {
                GroupBox 
                {
                    RowLayout 
                    {
                        CheckBox 
                        {
                             text: qsTr("Entities")
                             checked: true
                             onClicked: 
                             {
                                if (checked == true)
                                {
                                    entityBox.visible = true
                                    allEntitiesSelection();
                                    contextMetaManager.setDisplayModeActive(2, true); // turn on entity display
                                }
                                else
                                {
                                    entityBox.visible = false
                                    noneEntitiesSelection();
                                    contextMetaManager.setDisplayModeActive(2, false); // turn off entity display
                                }
                            }
                        }
                        CheckBox 
                        {
                            text: qsTr("Landmarks")
                            onClicked: 
                            {
                               if (checked == true)
                               {
                                   landmarkBox.visible = true
                                   contextMetaManager.setDisplayModeActive(4, true); // turn on landmark display
                                   //landmarksTableView.selection.selectAll();
                                   //contextMetaManager.landmarkDisplay(1, 'all');
                               }
                               else
                               {
                                   landmarkBox.visible = false
                                   landmarksTableView.selection.clear();
                                   contextMetaManager.landmarkDisplay(0, 'none', false);
                                   contextMetaManager.setDisplayModeActive(4, false); // turn off landmark display
                               }
                           }
                        }
                        CheckBox 
                        {
                             text: qsTr("Frames")
                             onClicked: 
                             {
                                if (checked == true)
                                {
                                    frameBox.visible = true
                                    allFramesSelection()
                                    framesTableView.selection.selectAll();
                                    contextMetaManager.setDisplayModeActive(16, true); // turn on axes display
                                }
                                else
                                {
                                    frameBox.visible = false
                                    noneFramesSelection()
                                    framesTableView.selection.clear();
                                    contextMetaManager.setDisplayModeActive(16, false); // turn off axes display
                                }
                            }
                        }
                        CheckBox 
                        {
                             text: qsTr("Contours")
                             onClicked: 
                             {
                                if (checked == true)
                                {
                                    contourBox.visible = true
                                    contextMetaManager.setDisplayModeActive(8, true); // turn on points display to see landmark points
                                    contextMetaManager.setDisplayModeActive(2, true); // turn on entity display - polylines currently added as entities, maybe it is a good idea to create a new mode for it
                                }
                                else
                                {
                                    contourBox.visible = false
                                    contextMetaManager.setDisplayModeActive(8, false); // turn off points display
                                }
                            }
                        }
                    }
                }
                GroupBox 
                {
                    RowLayout 
                    {
                        CheckBox 
                        {
                             text: qsTr("Full Model")
                             checked: false
                             onClicked: 
                             {
                                if (checked == true)
                                {
                                    contextMetaManager.setFullModelOpacity(fullModelOpacity.value);
                                    fullModelOpacity.enabled = true
                                    contextMetaManager.setDisplayModeActive(1, true); // turn on full mesh
                                }
                                else
                                {
                                    fullModelOpacity.enabled = false
                                        contextMetaManager.setDisplayModeActive(1, false); // turn off full mesh
                                }
                            }
                        }
                        Label
                        {
                            text:"Opacity"
                        }
                        SpinBox 
                        {
                            id: fullModelOpacity;
                            enabled: false
                            decimals: 2
                            minimumValue: 0.00
                            maximumValue: 1.00
                            stepSize: 0.10
                            value: 1.00
                            onValueChanged: 
                            {
                                contextMetaManager.setFullModelOpacity(fullModelOpacity.value);
                            }
                        }
                    }
                }
            }
            RowLayout 
            {
                id: displayTools
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true

                GroupBox
                {
                    id: entityBox
                    title: "Entities"
                    visible: false
                    RowLayout 
                    {
                        Layout.alignment: Qt.AlignTop
                        ColumnLayout 
                        {
                            id: buttonsColumn
                            width:childrenRect.width
                            ColumnLayout 
                            {
                                id: entitiesButtonsColumn
                                width:childrenRect.width
                                Button 
                                {
                                    id: clearSelectionBtn
                                    text: "None"
                                    tooltip: "Clear Selection"
                                    onClicked: noneEntitiesSelection();
                                }
                                Button 
                                {
                                    id: selectAllBtn
                                    text: "All"
                                    tooltip: "select all"
                                    onClicked: allEntitiesSelection();
                                }
                                Button 
                                {
                                    id: invertSelectionBtn
                                    text: "Invert"
                                    tooltip: "Invert Selection"
                                    onClicked: invertEntitiesSelection();
                                }
                            }
                        }
                        ListModel
                        {
                            id:entitiesList
                        }
                        TableView 
                        {
                            id: entitiesTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: entitiesList
                            property variant vals;
                            function getList() 
                            {
                                vals = contextMetaManager.getEntityList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    entitiesList.append({entity: vals[i]});
                                    nb_entities++;
                                }
                                entitiesTableView.selection.selectAll();
                            }
                            function toggleEntityDisplay(index)
                            {
                                if(entitiesTableView.selection.contains(index))
                                {
                                   contextMetaManager.entityDisplay(1, entitiesList.get(index).entity);
                                }
                                else 
                                {
                                   contextMetaManager.entityDisplay(0, entitiesList.get(index).entity);
                                }
                            }
                            TableViewColumn 
                            {
                                role: "entity"
                                title: "Entities"
                            }
                            onClicked:toggleEntityDisplay(row);
                            Component.onCompleted: 
                            {
                                getList();
                                entitiesTableView.selection.selectAll();
                            }
                        }
                    }
                }
                GroupBox
                {
                    id: landmarkBox
                    title: "Landmarks"
                    visible: false
                    RowLayout 
                    {
                        Layout.alignment: Qt.AlignTop
                        ColumnLayout
                        {
                            id: landmarkButtonsColumn
                            width:childrenRect.width
                            ColumnLayout 
                            {
                                id: landmarksButtonsColumn
                                width:childrenRect.width
                                Button 
                                {
                                    text: "none"
                                    tooltip: "clear selection"

                                    onClicked: 
                                    {
                                        landmarksTableView.selection.clear();
                                        contextMetaManager.landmarkDisplay(0, 'all', false);
                                    }
                                }
                                Button 
                                {
                                    text: "all"
                                    tooltip: "select all"
                                    onClicked: 
                                    {
                                        landmarksTableView.selection.selectAll();
                                        contextMetaManager.landmarkDisplay(1, 'all', false);
                                    }
                                }

                                Button 
                                {
                                    text: "invert"
                                    tooltip: "invert selection"
                                    onClicked: 
                                    {
                                        invertLandmarksSelection()
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:landmarksList
                        }
                        TableView 
                        {
                            id: landmarksTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: landmarksList
                            property variant vals;

                            function getList() 
                            {
                                vals = contextMetaManager.getLandmarkList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    landmarksList.append({landmark: vals[i]});
                                    nb_landmarks++;
                                }
                                landmarksTableView.selection.selectAll();
                            }
                            function togglelandmarkDisplay(index) 
                            {
                                if(landmarksTableView.selection.contains(index))
                                {
                                   contextMetaManager.landmarkDisplay(1, landmarksList.get(index).landmark, false);
                                }
                                else 
                                {
                                   contextMetaManager.landmarkDisplay(0, landmarksList.get(index).landmark, false);
                                }
                            }
                            TableViewColumn 
                            {
                                role: "landmark"
                                title: "Landmarks"
                            }
                            onClicked:togglelandmarkDisplay(row);

                            Component.onCompleted: 
                            {
                                getList();
                                landmarksTableView.selection.selectAll();
                            }
                        }
                    }
                }
                GroupBox
                {
                    id: frameBox
                    title: "Frames"
                    visible: false
                    RowLayout 
                    {
                        Layout.alignment: Qt.AlignTop
                        ColumnLayout 
                        {
                            width:childrenRect.width
                            ColumnLayout
                            {
                                width:childrenRect.width
                                Button 
                                {
                                    text: "none"
                                    tooltip: "clear selection"
                                    onClicked: 
                                    {
                                        framesTableView.selection.clear();
                                        noneFramesSelection();
                                    }
                                }
                                Button 
                                {
                                    text: "all"
                                    tooltip: "select all"
                                    onClicked: 
                                    {
                                        framesTableView.selection.selectAll()
                                        allFramesSelection();
                                    }
                                }
                                Button 
                                {
                                    text: "invert"
                                    tooltip: "invert selection"
                                    onClicked: 
                                    {
                                        invertFramesSelection()
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:framesList
                        }

                        TableView 
                        {
                            id: framesTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: framesList
                            property variant vals;

                            function getList() 
                            {
                                vals = contextMetaManager.getFrameList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    framesList.append({frame: vals[i]});
                                    nb_frames++;
                                }
                            }
                            function toggleFrameDisplay(index)
                            {
                                if(framesTableView.selection.contains(index))
                                {
                                   contextMetaManager.frameDisplay(1, framesList.get(index).frame);
                                }
                                else 
                                {
                                   contextMetaManager.frameDisplay(0, framesList.get(index).frame);
                                }
                            }
                            TableViewColumn 
                            {
                                role: "frame"
                                title: "Frames"
                            }

                            onClicked:toggleFrameDisplay(row);
                            Component.onCompleted: 
                            {
                                getList();
                            }
                        }
                    }
                }
                GroupBox
                {
                    id: contourBox
                    title: "Contours"
                    visible: false
                    Layout.alignment: Qt.AlignTop
                    height:childrenRect.height
                    RowLayout 
                    {
                        Rectangle 
                        {
                            Layout.fillWidth: true
                            height:childrenRect.height
                            ColumnLayout 
                            {
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignTop
                                RowLayout 
                                {
                                    Layout.alignment: Qt.AlignTop
                                    Layout.fillWidth: true

                                    ColumnLayout 
                                    {
                                        width:childrenRect.width
                                        ColumnLayout 
                                        {
                                            enabled:true
                                            width:childrenRect.width
                                            
                                            Action 
                                            {
                                                id: openContourAction
                                                text: qsTr("Open Contour file")
                                                tooltip: qsTr("Open contour file")
                                                iconSource: "qrc:///icon/document-open.png"
                                                onTriggered: openContourDialog.open()
                                            }
                                            ToolButton { action: openContourAction }
                                            FileDialog {
                                                id: openContourDialog
                                                title: qsTr("Open Contour File...")
                                                nameFilters: ["Contour files (*.txt *.xml)"]
                                                onAccepted: {
                                                    contextMetaManager.openContour(openContourDialog.fileUrl);
                                                    contourTools.enabled = true;
                                                    listView.visible =true
                                                }
                                            }
                                            Action {
                                                id: saveConotourAction
                                                text: qsTr("Save Contour as")
                                                iconSource: "qrc:///icon/document-save.png"
                                                onTriggered: saveAsDialog.open()
                                            }
                                            ToolButton { action: saveConotourAction }
                                            FileDialog {
                                                id: saveAsDialog
                                                title: qsTr("Save contour as...")
                                                selectExisting: false
                                                nameFilters: ["Contour files (*.xml)"]
                                                onAccepted: {
                                                    contextMetaManager.saveContour(saveAsDialog.fileUrl)
                                                }
                                            }
                                            
                                        }
                                    }
                                    RowLayout
                                    {
                                        id: contourTools
                                        enabled: false
                                        ListModel 
                                        {
                                            id: contourBRList
                                            ListElement 
                                            {
                                                nb: 1
                                                contour: "Right_Foot"
                                            }
                                            ListElement {
                                                nb: 2
                                                contour: "Left_Foot"
                                            }
                                            ListElement {
                                                nb: 3
                                                contour: "Right_Ankle_to_Calf"
                                            }
                                            ListElement {
                                                nb: 4
                                                contour: "Left_Ankle_to_Calf"
                                            }
                                            ListElement {
                                                nb: 5
                                                contour: "Right_Knee_lower"
                                            }
                                            ListElement {
                                                nb: 6
                                                contour: "Left_Knee_lower"
                                            }
                                            ListElement {
                                                nb: 7
                                                contour: "Right_Knee_upper"
                                            }
                                            ListElement {
                                                nb: 8
                                                contour: "Left_Knee_upper"
                                            }
                                            ListElement {
                                                nb: 9
                                                contour: "Right_Thigh"
                                            }
                                            ListElement {
                                                nb: 10
                                                contour: "Left_Thigh"
                                            }
                                            ListElement {
                                                nb: 11
                                                contour: "Hip"
                                            }
                                            ListElement {
                                                nb: 12
                                                contour: "Abdomen"
                                            }
                                            ListElement {
                                                nb: 13
                                                contour: "Thorax"
                                            }
                                            ListElement {
                                                nb: 14
                                                contour: "Neck"
                                            }
                                            ListElement {
                                                nb: 15
                                                contour: "Head"
                                            }
                                            ListElement {
                                                nb: 16
                                                contour: "Right_Palm"
                                            }
                                            ListElement {
                                                nb: 17
                                                contour: "Left_Palm"
                                            }
                                            ListElement {
                                                nb: 18
                                                contour: "Right_Forearm"
                                            }
                                            ListElement {
                                                nb: 19
                                                contour: "Left_Forearm"
                                            }
                                            ListElement {
                                                nb: 20
                                                contour: "Right_Elbow_lower"
                                            }
                                            ListElement {
                                                nb: 21
                                                contour: "Left_Elbow_lower"
                                            }
                                            ListElement {
                                                nb: 22
                                                contour: "Right_Elbow_upper"
                                            }
                                            ListElement {
                                                nb: 23
                                                contour: "Left_Elbow_upper"
                                            }
                                            ListElement {
                                                nb: 24
                                                contour: "Right_Biceps"
                                            }
                                            ListElement {
                                                nb: 25
                                                contour: "Left_Biceps"
                                            }
                                            ListElement {
                                                nb: 26
                                                contour: "Right_biceps_to_shoulder"
                                            }
                                            ListElement {
                                                nb: 27
                                                contour: "Left_biceps_to_shoulder"
                                            }
                                            ListElement {
                                                nb: 28
                                                contour: "Hip_right"
                                            }
                                            ListElement {
                                                nb: 29
                                                contour: "Hip_left"
                                            }
                                            ListElement {
                                                nb: 30
                                                contour: "Keypoints"
                                            }
                                            ListElement {
                                                nb: 31
                                                contour: "Right_Abdomen"
                                            }
                                            ListElement {
                                                nb: 32
                                                contour: "Left_Abdomen"
                                            }
                                            ListElement {
                                                nb: 33
                                                contour: "Right_Thorax"
                                            }
                                            ListElement {
                                                nb: 34
                                                contour: "Left_Thorax"
                                            }
                                        }

                                        TableView 
                                        {
                                            id : listView
                                            visible: false
                                            Layout.minimumHeight: 100
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true
                                            enabled: true
                                            clip: true
                                            focus: false
                                            property variant vals;
                                            function processClickedItem(index) 
                                            {
                                                listView2.visible = true
                                                contoursColumn.title =  contourBRList.get(index).contour + " Contours"
                                                vals = contextMetaManager.getContourList(contourBRList.get(index).nb);
                                                contoursList.clear()
                                                for(var i =0; i<vals.length; i++)
                                                {
                                                    contoursList.append({nb: i+1,contour: vals[i]});
                                                }
                                            }
                                            TableViewColumn 
                                            {
                                                role: "nb"
                                                title: "No."
                                                width: 20
                                            }
                                            TableViewColumn 
                                            {
                                                  role: "enabled"
                                                  width:20
                                                  delegate: CheckBox 
                                                  {
                                                     checked:true
                                                     onClicked: 
                                                     {
                                                         if(checked == true)
                                                         {
                                                             contextMetaManager.contourBodyRegionDisplay(1,model.nb - 1)
                                                         }
                                                         else
                                                         {
                                                             contextMetaManager.contourBodyRegionDisplay(0,model.nb - 1)
                                                         }
                                                     }
                                                 }
                                            }
                                            TableViewColumn 
                                            {
                                                role: "contour"
                                                title: "Contour Body Regions"
                                            }
                                            model: contourBRList
                                            headerVisible:true
                                            onClicked:processClickedItem(row)
                                        }
                                        ListModel
                                        {
                                            id:contoursList
                                        }
                                        TableView 
                                        {
                                            id: listView2
                                            visible: false
                                            Layout.minimumHeight: 100
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true
                                            clip: true
                                            focus: false
                                            headerVisible:true
                                            model: contoursList
                                            property variant vals;
                                            function processClickedItem2(index) 
                                            {
                                                listView3.visible = true
                                                contourPtsColumn.title = "Contour Points of "+ contoursList.get(index).contour
                                                vals = contextMetaManager.getContourList(listView.currentRow,index);
                                                contourPointsList.clear()
                                                for(var i =0; i<vals.length; i++)
                                                {
                                                    contourPointsList.append({contour: vals[i]});
                                                }
                                            }

                                            TableViewColumn 
                                            {
                                                role: "nb"
                                                title: "No."
                                                width: 20
                                            }
                                            TableViewColumn 
                                            {
                                                 role: "enabled"
                                                 width:20
                                                 delegate: CheckBox 
                                                 {
                                                 checked:true
                                                 onClicked: 
                                                 {
                                                     if(checked == true)
                                                     {
                                                         contextMetaManager.contourDisplay(1,listView.currentRow,model.nb - 1)
                                                     }
                                                     else
                                                     {
                                                         contextMetaManager.contourDisplay(0,listView.currentRow,model.nb - 1)
                                                     }
                                                 }
                                              }
                                           }
                                            TableViewColumn 
                                            {
                                                id:contoursColumn
                                                role: "contour"
                                                title: "Contours"
                                            }
                                            onClicked:processClickedItem2(row)
                                        }

                                        ListModel
                                        {
                                            id:contourPointsList
                                        }

                                        TableView 
                                        {
                                            id: listView3
                                            visible: false
                                            Layout.minimumHeight: 100
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true
                                            clip: true
                                            focus: false
                                            headerVisible:true
                                            selectionMode:SelectionMode.MultiSelection
                                            model: contourPointsList

                                            function processClickedItem3(index) 
                                            {
                                                if(listView3.selection.contains(index))
                                                {
                                                    contextMetaManager.contourPointDisplay(1,listView.currentRow,listView2.currentRow,index)
                                                }
                                                else 
                                                {
                                                   contextMetaManager.contourPointDisplay(0,listView.currentRow,listView2.currentRow,index)
                                                }
                                            }

                                            TableViewColumn 
                                            {
                                                id:contourPtsColumn
                                                role: "ValueRole"
                                                title: "Contour Points"
                                                movable: false
                                                resizable: false
                                            } // TableViewColumn
                                            onClicked:processClickedItem3(row)
                                        }
                                        /*
                                        GroupBox
                                        {
                                            title:"Contour Line Options"
                                            RowLayout
                                            {
                                                Label
                                                { text:"Width"}
                                                SpinBox 
                                                { 
                                                    id: lineWidth;
                                                    decimals: 2
                                                    minimumValue: 0
                                                    maximumValue: 999
                                                    stepSize: 1
                                                    value: 3.00
                                                    onValueChanged: 
                                                    {
                                                        updateLine()
                                                    }
                                                }
                                            }
                                        }*/
                                    }
                                }
                            }
                        }
                        function clipping(){
                            contextMetaManager.setClipAxis(3);
                            contextMetaManager.clipPlane();
                        }

                        function slicing(){
                            contextMetaManager.setSliceAxis(3);
                            contextMetaManager.slicePlane();
                        }

                        function updateLine(){
                            contextMetaManager.updateContourLine(lineWidth.value)
                        }

                    }
                }
                /*
                GroupBox {
                    id:elementBox
                    title: "Element"

                    ColumnLayout {
                        CheckBox {id: barsChkBx;  text: "1d Bars"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("bars",checked)
                            }
                        }
                        CheckBox {id: trianglesChkBx;  text: "2d Triangles"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("triangles",checked)
                            }
                        }
                        CheckBox {id: quadsChkBx;  text: "2d Quads"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("quads",checked)
                            }
                        }
                        CheckBox {id: tetraChkBx;  text: "3d Tetrahedra"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("tetrahedra",checked)
                            }
                        }
                        CheckBox {id: hexaChkBx; text: "3d Hexahedra"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("hexahedra",checked)
                            }
                        }
                        CheckBox {id: pentaChkBx; text: "3d Pentahedra"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("pentahedra",checked)
                            }
                        }
                        CheckBox { id: pyramidsChkBx; text: "3d Pyramids"; checked: true;
                            onClicked:
                            {
                                contextMetaManager.setElementTypeActivated("pyramids",checked)
                            }
                        }
                    }
                }*/
            }
        }
        Component.onCompleted : { // by default, turn on entities
            entityBox.visible = true
            allEntitiesSelection();
        }
    }

    function invertEntitiesSelection()
    {
        for(var i=0;i<nb_entities;i++)
        {
            if(entitiesTableView.selection.contains(i))
            {
                entitiesTableView.selection.deselect(i)
                contextMetaManager.entityDisplay(0, entitiesList.get(i).entity);
            }
            else
            {
                entitiesTableView.selection.select(i)
                contextMetaManager.entityDisplay(1, entitiesList.get(i).entity);
            }
        }
    }

    function noneEntitiesSelection()
    {
        entitiesTableView.selection.clear();
        for(var i=0;i<nb_entities;i++)
        {
            contextMetaManager.entityDisplay(0, entitiesList.get(i).entity);
        }
    }

    function allEntitiesSelection()
    {
        entitiesTableView.selection.selectAll();
        for(var i=0;i<nb_entities;i++)
        {
            contextMetaManager.entityDisplay(1, entitiesList.get(i).entity);
        }
    }

    function invertLandmarksSelection()
    {
        for(var i=0;i<nb_landmarks;i++)
        {
            if(landmarksTableView.selection.contains(i))
            {
                landmarksTableView.selection.deselect(i)
                contextMetaManager.landmarkDisplay(0, landmarksList.get(i).landmark, false);
            }
            else
            {
                landmarksTableView.selection.select(i)
                contextMetaManager.landmarkDisplay(1, landmarksList.get(i).landmark, false);
            }
        }
    }

    function invertFramesSelection()
    {
        for(var i=0;i<nb_frames;i++)
        {
            if(framesTableView.selection.contains(i))
            {
                framesTableView.selection.deselect(i)
                contextMetaManager.frameDisplay(0, framesList.get(i).frame);
            }
            else
            {
                framesTableView.selection.select(i)
                contextMetaManager.frameDisplay(1, framesList.get(i).frame);
            }
        }
    }

    function noneFramesSelection()
    {
        for(var i=0;i<nb_frames;i++)
        {
            contextMetaManager.frameDisplay(0, framesList.get(i).frame);
        }
    }

    function allFramesSelection()
    {
        for(var i=0;i<nb_frames;i++)
        {
            contextMetaManager.frameDisplay(1, framesList.get(i).frame);
        }
    }
}
