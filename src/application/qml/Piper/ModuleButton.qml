// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

Button {
    Component {
        id: moduleButtonStyle
        ButtonStyle {
            background : Rectangle {
                Layout.fillWidth: true
                border.width: 0
                color: control.checked ? "steelblue" : "transparent"
            }
            label: ColumnLayout {
                spacing: 1
                Image {
                    Layout.alignment: Qt.AlignHCenter
                    source: control.iconSource
                    sourceSize.width: 36
                    Label {
                        anchors.centerIn: parent
                        text: control.action.letter
                        color: control.action.color
                        font.pixelSize: 24
                        font.bold: true
                    }
                }
                Label {
                    Layout.alignment: Qt.AlignHCenter
                    text: control.text
                    color: control.checked ? "white" : "black"
                }
            }
        }
    }

    activeFocusOnPress: true
    style: moduleButtonStyle
    iconSource: "qrc:///icon/module_background.png"
}
