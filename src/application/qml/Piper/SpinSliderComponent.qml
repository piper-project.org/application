// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

ColumnLayout {
    id: mySpinSlider
    property real incValue
    property int minValue
    property int maxValue
    property int decimals
    property string title
    property alias updateValueWhileDragging: mySlider.updateValueWhileDragging

    property double valueComponent
    property bool mutex: true

    onValueComponentChanged: {
        if (mutex)
        {
            mutex = false
            mySpin.value = valueComponent
            mySlider.value = valueComponent
            valueFinalized(valueComponent)
            mutex = true
        }
    }
    
    signal valueFinalized(real value);

    Layout.fillWidth : true


    Label {
        id: myTitle
        Layout.fillWidth : true
        Layout.fillHeight : true
        text: title
        horizontalAlignment: TextInput.AlignHCenter
    }
    RowLayout {
        id: myRow
        SpinBox  {
            id: mySpin
            Layout.fillWidth : true
            Layout.fillHeight : true
            width: mySpinSlider.width*0.25
            stepSize: mySpinSlider.incValue
            maximumValue: mySpinSlider.maxValue
            minimumValue: mySpinSlider.minValue
            decimals: mySpinSlider.decimals
            onValueChanged: {
                if (mutex) // send this signal only if user is not currently dragging the slider
                {
                    mutex = false
                    mySpinSlider.valueComponent = value
                    mySlider.value = value
                    mySpinSlider.valueFinalized(mySpinSlider.valueComponent)
                    mutex = true
                }
            }

        }
        Slider {
            id: mySlider
            Layout.fillWidth : true
            Layout.fillHeight : true
            width: mySpinSlider.width*0.25
            stepSize: mySpinSlider.incValue
            maximumValue: mySpinSlider.maxValue
            minimumValue: mySpinSlider.minValue
            updateValueWhileDragging: false
            onPressedChanged: {
                if (!mySlider.pressed && mutex) // send this signal only if user is not currently dragging the slider
                {
                    mutex = false
                    mySpinSlider.valueComponent = value
                    mySpin.value = value
                    mySpinSlider.valueFinalized(mySpinSlider.valueComponent)
                    mutex = true
                }
            }
            onValueChanged: {
                if (!mySlider.pressed && mutex) // send this signal only if user is not currently dragging the slider
                {
                    mutex = false
                    mySpinSlider.valueComponent = value
                    mySpin.value = value
                    mySpinSlider.valueFinalized(mySpinSlider.valueComponent)
                    mutex = true
                }
            }
            MouseArea {
                anchors.fill: parent
                onWheel: {
                    // do nothing
                }
                onPressed: {
                    // propogate to slider
                    mouse.accepted = false;
                }
                onReleased: {
                    // propogate to slider
                    mouse.accepted = false;
                }
            }
        }
    }
}
