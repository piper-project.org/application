// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Frame Display Options"

    property int nb: 0;

    ColumnLayout
    {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop
        anchors.centerIn : parent
        anchors.fill : parent
        RowLayout
        {
            width:childrenRect.width
            Button
            {
                text: "none"
                tooltip: "clear selection"
                onClicked:
                {
                    tableView.selection.clear();
                    noneSelection();
                }
            }
            Button
            {
                text: "all"
                tooltip: "select all"
                onClicked:
                {
                    allSelection();
                }
            }
            Button
            {
                text: "invert"
                tooltip: "invert selection"
                onClicked:
                {
                    invertSelection()
                }
            }
        }
        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.MultiSelection
            model: listModel
            property variant vals;
            onRowCountChanged:          //Every time the nb of elements in the list change
            {
                allSelection()
            }

            function getList()
            {
                listModel.clear();
                nb = 0;
                vals = contextMetaManager.getFrameList();
                for(var i =0; i<vals.length; i++)
                {
                    listModel.append({role: vals[i]});
                    nb++;
                }
            }
            TableViewColumn
            {
                role: "role"
                title: "Frames"
            }
            Connections
            {
                target: tableView.selection
                onSelectionChanged :
                {
                    var listNames = [];
                    var listVisible = [];
                    for (var i = 0; i < tableView.model.count; i++)
                    {
                        listNames.push(tableView.model.get(i).role);
                        listVisible.push(tableView.selection.contains(i));
                    }
                    contextMetaManager.frameDisplayList(listNames, listVisible);
                }
            }

            Component.onCompleted:
            {
                getList();
            }
            Connections{
                target: context
                onModelChanged : {
                    tableView.getList();
                }
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeMinimum();
    }

    function invertSelection()
    {
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            if (tableView.selection.contains(i))
            {
                listVisible.push(false);
                tableView.selection.deselect(i)
            }
            else
            {
                listVisible.push(true);
                tableView.selection.select(i)
            }
        }
        contextMetaManager.frameDisplayList(listNames, listVisible);
    }

    function noneSelection()
    {
        tableView.selection.clear();
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            listVisible.push(false);
        }
        contextMetaManager.frameDisplayList(listNames, listVisible);
    }

    function allSelection()
    {
        var listNames = [];
        var listVisible = [];
        if (tableView.model.count > 0)
        {
            tableView.selection.selectAll();
            tableView.focus = true;
            for (var i = 0; i < tableView.model.count; i++)
            {
                listNames.push(tableView.model.get(i).role);
                listVisible.push(true);
            }
        }
        contextMetaManager.frameDisplayList(listNames, listVisible);
    }
}
