// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Contour Editor"

    minimumHeight : 200
    minimumWidth : 250
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;

    ColumnLayout
    {
        width: parent.width
        Layout.fillHeight: true
        RowLayout
        {
            width: parent.width
            Button
            {
                id: makeContour
                iconSource: "qrc:///module/iitd/icon/contour_make.png"
                tooltip: "Make Contour"
                onClicked:
                {
                    myMetaEditor.makeContour();
                }
            }
        }
        GroupBox
        {
            title:"Add Contour"
            ColumnLayout
            {
                Label{ text:"Body Region"}
                ComboBox
                {
                    currentIndex: 0
                    model: ListModel
                    {
                        id: cbItems
                        ListElement {text: "Right Foot";bodyRegion: "1";}
                        ListElement {text: "Left Foot";bodyRegion: "2";}
                        ListElement {text: "Right Ankle to Calf";bodyRegion: "3";}
                        ListElement {text: "Left Ankle to Calf";bodyRegion: "4";}
                        ListElement {text: "Right Knee lower";bodyRegion: "5";}
                        ListElement {text: "Left Knee lower";bodyRegion: "6";}
                        ListElement {text: "Right Knee upper";bodyRegion: "7";}
                        ListElement {text: "Left Knee upper";bodyRegion: "8";}
                        ListElement {text: "Right Thigh";bodyRegion: "9";}
                        ListElement {text: "Left Thigh";bodyRegion: "10";}
                        ListElement {text: "Hip";bodyRegion: "11";}
                        ListElement {text: "Abdomen";bodyRegion: "12";}
                        ListElement {text: "Thorax";bodyRegion: "13";}
                        ListElement {text: "Neck";bodyRegion: "14";}
                        ListElement {text: "Head";bodyRegion: "15";}
                        ListElement {text: "Right Palm";bodyRegion: "16";}
                        ListElement {text: "Left Palm";bodyRegion: "17";}
                        ListElement {text: "Right Forearm";bodyRegion: "18";}
                        ListElement {text: "Left Forearm";bodyRegion: "19";}
                        ListElement {text: "Right Elbow lower";bodyRegion: "20";}
                        ListElement {text: "Left Elbow lower";bodyRegion: "21";}
                        ListElement {text: "Right Elbow upper";bodyRegion: "22";}
                        ListElement {text: "Left Elbow upper";bodyRegion: "23";}
                        ListElement {text: "Right Biceps";bodyRegion: "24";}
                        ListElement {text: "Left Biceps";bodyRegion: "25";}
                        ListElement {text: "Right biceps to shoulder";bodyRegion: "26";}
                        ListElement {text: "Left biceps to shoulder";bodyRegion: "27";}
                        ListElement {text: "Hip right";bodyRegion: "28";}
                        ListElement {text: "Hip left";bodyRegion: "29";}
                        ListElement {text: "Keypoints";bodyRegion: "30";}
                        ListElement {text: "Right Abdomen";bodyRegion: "31";}
                        ListElement {text: "Left Abdomen";bodyRegion: "32";}
                        ListElement {text: "Right Thorax";bodyRegion: "33";}
                        ListElement {text: "Left Thorax";bodyRegion: "34";}
                    }
                    width: 200
                    //onCurrentIndexChanged: contextMetaManager.setBodyRegion(currentIndex+1)
                }
                Button
                {
                    id: addContour
                    text: "Add Contour"
                    tooltip: "Add Contour"
                    onClicked:
                    {
                        contextMetaManager.displayModel();
                    }
                }
            }
        }
    }
}
