// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Frame Editor"

    minimumHeight : 200
    minimumWidth : 250
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;

    ExclusiveGroup
    {
        id: group
    }

    ColumnLayout
    {
        width: parent.width
        ColumnLayout
        {
            width: parent.width
            GroupBox
            {
                title:"Create a Frame"
                ColumnLayout
                {
                    width: parent.width
                    Layout.fillHeight: true

                    RowLayout
                    {
                        width: parent.width
                        Button
                        {
                            text: "Select 3 Nodes"
                            tooltip: "Select 3 Nodes"
                            checkable: true
                            exclusiveGroup: group
                            onClicked:
                            {
                                if (checked)
                                {
                                    contextVtkDisplay.SetPickingType(0);
                                    framesMake.enabled = true
                                }
                                else
                                    contextVtkDisplay.SetPickingType(-1);
                            }
                        }
                    }
                    RowLayout
                    {
                        width: parent.width
                        Button
                        {
                            id: framesMake
                            enabled: false
                            text: "Create a Frame"
                            tooltip: "Create a Frame"
                            checkable: true
                            exclusiveGroup: group
                            //iconSource: "qrc:///module/iitd/icon/frame_make.png"
                            onClicked:
                            {
                                //myMetaEditor.makeFrames(framesNumb.value);
                                myMetaEditor.makeFrame();
                            }
                        }
                    }
                }
            }
        }
        ColumnLayout
        {
            width:parent.width
            GroupBox
            {
                title: "Instructions"
                ColumnLayout
                {
                    width:parent.width
                    Label
                    {
                        width:parent.width
                        text:"1) Select 3 points to create a Frame"
                    }
                    Label
                    {
                        width:parent.width
                        text:"2) Click on Create frame"
                    }
                }
            }
        }
    }
}
