// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Landmarks Display Options"

    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;

    property bool visualizeAsPoints: false;



    ColumnLayout
    {
        anchors.fill : parent
        anchors.centerIn: parent

        GroupBox {
            title: "Display Type"

            RowLayout {
                ExclusiveGroup {
                    id: landmarkDisplayTypeGroup
                }
                RadioButton {
                    text: "Landmark Points"
                    checked: false
                    exclusiveGroup: landmarkDisplayTypeGroup
                    onCheckedChanged: {
                        if (checked === true) {
                           visualizeAsPoints = true;
                        }else{
                           visualizeAsPoints = false;
                        }

                        var listNames = [];
                        var listVisible = [];
                        for (var i = 0; i < tableView.model.count; i++)
                        {
                            listNames.push(tableView.model.get(i).role);
                            if (tableView.selection.contains(i))
                            {
                                listVisible.push(true);
                            }
                            else
                            {
                                listVisible.push(false);
                            }
                        }
                        contextMetaManager.landmarkDisplayList(listNames, listVisible, visualizeAsPoints);
                    }
                }
                RadioButton {
                    text: "Landmark Positions"
                    checked: true
                    exclusiveGroup: landmarkDisplayTypeGroup
                }
            }
        }

        TextField {
            id: searchTextField
            placeholderText: qsTr("Search")
            Layout.fillWidth: true
            onTextChanged: {
                tableView.getList(searchTextField.text)
            }
        }

        RowLayout
        {
            width:childrenRect.width
            Button
            {
                text: "None"
                tooltip: "clear selection"
                onClicked:
                {
                    noneSelection();
                }
            }
            Button
            {
                text: "All"
                tooltip: "select all"
                onClicked:
                {
                    allSelection();
                }
            }
            Button
            {
                text: "Invert"
                tooltip: "invert selection"
                onClicked:
                {
                    invertSelection()
                }
            }
        }

        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.MultiSelection
            model: listModel
            property variant vals;
            property variant types;
//            onRowCountChanged:          //Every time the nb of elements in the list change
//            {
//                allSelection()
//            }
            function getList(query) {
                listModel.clear()
                vals = contextMetaManager.getLandmarkList()
                types = contextMetaManager.getLandmarkTypeList()
                for (var i = 0; i < vals.length; i++) {
                    if (!query) {
                        listModel.append({
                                             role: vals[i],
                                             type: types[i]
                                         })
                    } else {
                        if (vals[i].toUpperCase().indexOf(
                                    searchTextField.text.toUpperCase(
                                        )) !== -1) {
                            listModel.append({
                                                 role: vals[i],
                                                 type: types[i]
                                             })
                        }
                    }
                }
            }
//            function getList()
//            {
//                listModel.clear();
//                nb = 0;
//                vals = contextMetaManager.getLandmarkList();
//                types = contextMetaManager.getLandmarkTypeList();
//                for(var i =0; i<vals.length; i++)
//                {
//                    listModel.append({role: vals[i], type:types[i]});
//                    nb++;
//                }
//            }
            function toggleDisplay(index)
            {
                if(tableView.selection.contains(index))
                {
                    contextMetaManager.landmarkDisplay(true, listModel.get(index).role, false);
                }
                else
                {
                    contextMetaManager.landmarkDisplay(false, listModel.get(index).role, false);
                }
            }
            TableViewColumn
            {
                role: "role"
                title: "Landmarks"
            }
            TableViewColumn
            {
                role: "type"
                title: "Type"
            }
            Connections
            {
                target: tableView.selection
                onSelectionChanged :
                {
                    var listNames = [];
                    var listVisible = [];
                    for (var i = 0; i < tableView.model.count; i++)
                    {
                        listNames.push(tableView.model.get(i).role);
                        listVisible.push(tableView.selection.contains(i));
                    }
                    contextMetaManager.landmarkDisplayList(listNames, listVisible, visualizeAsPoints);
                }
            }

            Component.onCompleted:
            {
                getList();
            }
            Connections
            {
                target: context
                onVisDataLoaded:
                {
                    tableView.getList();
                }
            }
            Connections {
                target: context
                onMetadataChanged:  {
                    tableView.getList()
                }
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeMinimum();
    }

    function invertSelection()
    {
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            if (tableView.selection.contains(i))
            {
                listVisible.push(false);
                tableView.selection.deselect(i)
            }
            else
            {
                listVisible.push(true);
                tableView.selection.select(i)
            }
        }
        contextMetaManager.landmarkDisplayList(listNames, listVisible, visualizeAsPoints);
    }

    function noneSelection()
    {
        tableView.selection.clear();
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            listVisible.push(false);
        }
        contextMetaManager.landmarkDisplayList(listNames, listVisible, visualizeAsPoints);
    }

    function allSelection()
    {
        var listNames = [];
        var listVisible = [];
        if (tableView.model.count > 0)
        {
            tableView.selection.selectAll();
            tableView.focus = true;
            for (var i = 0; i < tableView.model.count; i++)
            {
                listNames.push(tableView.model.get(i).role);
                listVisible.push(true);
            }
        }
        contextMetaManager.landmarkDisplayList(listNames, listVisible, visualizeAsPoints);
    }

}
