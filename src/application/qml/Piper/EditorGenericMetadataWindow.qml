// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
    title: "Named Metadata Editor"

    property string create: ""
    minimumHeight: 375
    minimumWidth: 250
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0

    onCreateChanged: {
        addElementSetName.text = create
    }

    ExclusiveGroup {
        id: group
    }

    GroupBox {
        ColumnLayout {
            width: parent.width
            Layout.fillHeight: true

            GroupBox {
                title: "Action"

                RowLayout {
                    ExclusiveGroup {
                        id: tabPositionGroup
                    }
                    RadioButton {
                        text: "Create"
                        id: createLandmarkRadioButton
                        checked: true
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                createGMD.visible = true
                                searchTextField.visible = false
                                table.visible = false
                                addElements.visible = true
                                updateGenericMetadata.visible = false
                                removeElementSet.visible = false
                            }
                        }
                    }
                    RadioButton {
                        text: "Edit"
                        id: editLandmarkRadioButton
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                createGMD.visible = false
                                searchTextField.visible = true
                                table.visible = true
                                addElements.visible = false
                                updateGenericMetadata.visible = true
                                removeElementSet.visible = true
                            }
                        }
                    }
                }
            }

            RowLayout {
                id: createGMD

                Label {
                    text: "Name : "
                }
                TextField {
                    id: addElementSetName
                    placeholderText: qsTr("Named Metadata Name")
                }
            }

            TextField {
                id: searchTextField
                visible: false
                placeholderText: qsTr("Search")
                Layout.row: 1
                Layout.column: 0
                Layout.fillWidth: true
                onTextChanged: {
                    tableView.getList(searchTextField.text)
                }
            }

            RowLayout {
                id: table
                visible: false

                ListModel {
                    id: listModel
                }

                TableView {
                    id: tableView
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible: true
                    selectionMode: SelectionMode.MultiSelection
                    model: listModel
                    property variant vals

                    function toggleDisplay(index) {
                        if (tableView.selection.contains(index)) {
                            contextMetaManager.genericMetadataDisplay(
                                        1, listModel.get(index).role)
                        } else {
                            contextMetaManager.genericMetadataDisplay(
                                        0, listModel.get(index).role)
                        }
                    }

                    TableViewColumn {
                        role: "elementSets"
                        title: "GenericMetadata"
                    }
                    onClicked: toggleDisplay(row)
                    function getList(query) {
                        listModel.clear()
                        nb = 0
                        vals = contextMetaManager.getGenericMetadataList()
                        for (var i = 0; i < vals.length; i++) {
                            if (!query) {
                                listModel.append({
                                                     role: vals[i]
                                                 })
                            } else {
                                if (vals[i].toUpperCase().indexOf(
                                            searchTextField.text.toUpperCase(
                                                )) !== -1) {
                                    listModel.append({
                                                         role: vals[i]
                                                     })
                                }
                            }
                        }
                    }
                    Connections {
                        target: context
                        onMetadataChanged: {
                            tableView.getList()
                        }
                    }
                    Component.onCompleted: {
                        getList()
                    }
                }
            }

            RowLayout {
                GroupBox {
                    title: "Pickers"
                    ColumnLayout {
                        id: entityByElements
                        GridLayout {
                            id: pickerButtons
                            Label {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                text: "Entity"
                                Layout.row: 0
                                Layout.column: 1
                            }
                            Label {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                text: "Element"
                                Layout.row: 0
                                Layout.column: 2
                            }
                            Label {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                text: "Single Pick"
                                Layout.row: 1
                                Layout.column: 0
                            }
                            Label {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                text: "Rubber Band"
                                Layout.row: 2
                                Layout.column: 0
                            }
                            Label {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                text: "Box"
                                Layout.row: 3
                                Layout.column: 0
                            }
                            Button {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                Layout.row: 1
                                Layout.column: 1
                                tooltip: "Entity Picker"
                                checkable: true
                                iconSource: "qrc:///icon/picker-entity.png"
                                onClicked: {
                                    if (checked) {
                                        pickerButtons.uncheckPickerButtons()
                                        checked = true
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    3)
                                    } else
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    -1)
                                }
                            }
                            Button {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                Layout.row: 1
                                Layout.column: 2
                                tooltip: "Element Picker"
                                checkable: true
                                iconSource: "qrc:///icon/picker-element.png"
                                onClicked: {
                                    if (checked) {
                                        pickerButtons.uncheckPickerButtons()
                                        checked = true
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    2)
                                    } else
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    -1)
                                }
                            }
                            Button {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                Layout.row: 2
                                Layout.column: 2
                                tooltip: "Rubberband Element Picker"
                                checkable: true
                                iconSource: "qrc:///icon/rubberband-element.png"
                                onClicked: {
                                    if (checked) {
                                        pickerButtons.uncheckPickerButtons()
                                        checked = true
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    5)
                                    } else
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    -1)
                                }
                            }
                            Button {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                Layout.row: 3
                                Layout.column: 2
                                tooltip: "Box Element Picker"
                                checkable: true
                                iconSource: "qrc:///icon/box-element.png"
                                onClicked: {
                                    if (checked) {
                                        pickerButtons.uncheckPickerButtons()
                                        checked = true
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    8)
                                    } else
                                        hintLabel.text = contextVtkDisplay.SetPickingType(
                                                    -1)
                                }
                            }
                            // uncheck all the buttons in the pickerButtons panel
                            function uncheckPickerButtons() {
                                for (var i = 0; i < pickerButtons.children.length; i++) {
                                    if (pickerButtons.children[i].checkable)
                                        pickerButtons.children[i].checked = false
                                }
                            }
                        }
                    }
                }
            }

            RowLayout {

                Button {
                    id: addElements
                    visible: true
                    text: "Add"
                    tooltip: "Add"
                    onClicked: {
                        myMetaEditor.getSelectedElementIDs(
                                    addElementSetName.text, 1)
                    }
                }
                Button {
                    id: updateGenericMetadata
                    visible: false
                    text: "Update"
                    tooltip: "Update GenericMetadata"
                    onClicked: {
                        for (var i = 0; i < listModel.count; i++) {
                            if (tableView.selection.contains(i)) {
                                myMetaEditor.getSelectedElementIDsBusyIndicator(
                                            tableView.get(i).role, 1, 1)
                            }
                        }
                    }
                }
                Button {
                    id: removeElementSet
                    visible: false
                    text: "Remove"
                    tooltip: "Remove GenericMetadata"
                    onClicked: {
                        for (var i = 0; i < listModel.count; i++) {
                            if (tableView.selection.contains(i)) {
                                myMetaEditor.removeGenericMetadata(
                                            listModel.get(i).role)
                            }
                        }
                    }
                }
            }

            ColumnLayout {
                CheckBox {
                    id: checkboxSeeHint
                    checked: false
                    text: "See hints"
                    onCheckedChanged: {
                        hintLabel.visible = checked
                        if (checked) {
                            hintLabel.text = hintLabel.text // to refresh the label if it was invisible...don't ask me why, but this way it works
                            adjustWindowSizeToContent()
                        }
                    }
                }
            }
            GroupBox {
                title: "Hints for pickers"
                Label {
                    id: hintLabel
                    text: "No picker selected"
                }
            }
        }
    }
}
