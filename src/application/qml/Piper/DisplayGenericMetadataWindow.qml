// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Named Metadata Display Options"

    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;
    ColumnLayout
    {
        anchors.fill : parent
        anchors.centerIn: parent
        RowLayout
        {
            width:childrenRect.width
            Button
            {
                id: clearSelectionBtn
                text: "None"
                tooltip: "Clear Selection"
                onClicked: noneSelection();
            }

            Button
            {
                id: selectAllBtn
                text: "All"
                tooltip: "select all"
                onClicked: allSelection();
            }
            Button
            {
                id: invertSelectionBtn
                text: "Invert"
                tooltip: "Invert Selection"
                onClicked: invertSelection();
            }
        }
        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.MultiSelection
            model: listModel
            property variant vals;
            onRowCountChanged:
            {
                allSelection()
            }
            function getList()
            {
                listModel.clear();
                nb = 0;
                vals = contextMetaManager.getGenericMetadataList();
                for(var i =0; i<vals.length; i++)
                {
                    listModel.append({role: vals[i]});
                    nb++;
                }
            }
            function toggleDisplay(index)
            {
                if(tableView.selection.contains(index))
                {
                    contextMetaManager.genericMetadataDisplay(1, listModel.get(index).role);
                }
                else
                {
                    contextMetaManager.genericMetadataDisplay(0, listModel.get(index).role);
                }
            }
            TableViewColumn
            {
                role: "role"
                title: "Named Metadata"
            }
            onClicked:toggleDisplay(row);

            Component.onCompleted:
            {
                getList();
            }
            Connections{
                target: context
                onVisDataLoaded : {
                    tableView.getList();
                }
                onMetadataChanged:  {
                    tableView.getList()
                }
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeMinimum();
    }

    function invertSelection()
    {
        for(var i=0;i<nb;i++)
        {
            if(tableView.selection.contains(i))
            {
                tableView.selection.deselect(i)
                contextMetaManager.genericMetadataDisplay(0, listModel.get(i).role);
            }
            else
            {
                tableView.selection.select(i)
                contextMetaManager.genericMetadataDisplay(1, listModel.get(i).role);
            }
        }
    }

    function noneSelection()
    {
        tableView.selection.clear();
        for(var i=0;i<nb;i++)
        {
            contextMetaManager.genericMetadataDisplay(0, listModel.get(i).role);
        }
    }

    function allSelection()
    {
        if (tableView.model.count > 0)
        {
            tableView.selection.selectAll();
            tableView.focus = true;

            for (var i = 0; i < nb; i++)
            {
                contextMetaManager.genericMetadataDisplay(1, listModel.get(i).role);
            }
        }
    }
}
