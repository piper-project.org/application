// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0



GroupBox
{
    title: "Display"
    GridLayout
    {
        anchors.centerIn: parent
        columns: 7
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-entity.png"
            tooltip: "Entity"
            toolWindow: displayEntityWindow
        }
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-landmark.png"
            tooltip: "Landmark"
            toolWindow: displayLandmarkWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(4, true); // turn on landmark display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(4, false); // turn off landmark display
                }
            }
        }
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-namedmetadata.png"
            tooltip: "Named Metadata"
            toolWindow: displayGenericMetadataWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(8, true); // turn on genericMetadata display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(8, false); // turn off genericMetadata display
                }
            }
        }
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-frame.png"
            tooltip: "Frame"
            toolWindow: displayFrameWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(64, true); // turn on axes display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(64, false); // turn off axes display
                }
            }
        }
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-joint.png"
            tooltip: "Joint"
            toolWindow: displayJointWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(64, true); // turn on axes display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(64, false); // turn off axes display
                }
            }
        }
        ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-contour.png"
            tooltip: "Contour"
            toolWindow: displayContourWindow
            visible : false
        }
  /*      ModuleToolWindowButton
        {
            iconSource: "qrc:///icon/display-control-point.png"
            tooltip: "Control Points"
            toolWindow: displayControlPointWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(32, true); // turn on point display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(32, false); // turn off point display
                }
            }
        }*/
        /*
        ModuleToolWindowButton
        {
            visible: false
            Layout.row: 2
            Layout.column : 1
            iconSource: "qrc:///icon/display-target.png"
            tooltip: "Target"
            toolWindow: displayTargetWindow
            onClicked: {
                if(checked)
                {
                    contextMetaManager.setDisplayModeActive(32, true); // turn on point display
                }
                else
                {
                    contextMetaManager.setDisplayModeActive(32, false); // turn off point display
                }
            }
        }*/
//        ModuleToolWindowButton
//        {
//            Layout.row: 2
//            Layout.column : 2
//            iconSource: "qrc:///icon/display-config.png"
//            tooltip: "Display Configurations"
//            toolWindow: displayConfigurationsWindow
//            onClicked: {
//                if(checked)
//                {
//                    contextMetaManager.setDisplayModeActive(4, true); // turn on landmark display
//                }
//                else
//                {
//                    contextMetaManager.setDisplayModeActive(4, false); // turn off landmark display
//                }
//            }
//        }
	} 

	//DISPLAY OPTIONS
	DisplayEntityWindow
	{
		id: displayEntityWindow
	}
	DisplayLandmarkWindow
	{
		id: displayLandmarkWindow
	}
	DisplayGenericMetadataWindow
	{
		id: displayGenericMetadataWindow
	}
	DisplayFrameWindow
	{
		id: displayFrameWindow
	}
	DisplayJointWindow
	{
		id: displayJointWindow
	}
	DisplayContourWindow
	{
		id: displayContourWindow
	}
//	DisplayControlPointWindow
	//{
		//id: displayControlPointWindow
	//}
	DisplayTargetWindow
	{
		id: displayTargetWindow
	}
	DisplayConfigurationsWindow
	{
		id: displayConfigurationsWindow
	}
}


