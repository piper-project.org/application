// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

GroupBox {
    id: root
    title: "Mouse tool"
    enabled: context.hasModel
    Layout.preferredWidth: helpLabelWidth

    property var mouseToolActionArray: []
    property int mouseToolIconGridColumns: 4
    property int helpLabelWidth: 120

    ExclusiveGroup {
        id: mouseToolExclusiveGroup
        onCurrentChanged: {
            if (null === current) return;
            leftHelpLabel.text = current.leftButtonHelp;
            middleHelpLabel.text = current.middleButtonHelp;
            rightHelpLabel.text = current.rightButtonHelp;
        }
    }

    Component {
        id: mouseInteractorToolButton
        MouseInteractorToolButton { }
    }

    Component {
        id: mouseToolHelpLabel
        Label { }
    }

    ColumnLayout {
        GridLayout {
            id: mouseToolButtonRoot
            columns: mouseToolIconGridColumns
            Layout.fillWidth: true
        }
        CheckBox {
            id: doShowHelp
            Layout.alignment: Qt.AlignRight
            text: "Show help"
        }
        ColumnLayout {
            visible: doShowHelp.checked
            Layout.fillWidth: true
            ColumnLayout {
                spacing: 0
                Label {
                    text: "left:"
                    font.italic: true
                }
                Label {
                    id: leftHelpLabel
                    Layout.preferredWidth: helpLabelWidth
                    wrapMode: Text.Wrap
                }
            }
            ColumnLayout {
                spacing: 0
                Label {
                    text: "middle:"
                    font.italic: true
                }
                Label {
                    id: middleHelpLabel
                    Layout.preferredWidth: helpLabelWidth
                    wrapMode: Text.Wrap
                }
            }
            ColumnLayout {
                spacing: 0
                Label {
                    text: "right:"
                    font.italic: true
                }
                Label {
                    id: rightHelpLabel
                    Layout.preferredWidth: helpLabelWidth
                    wrapMode: Text.Wrap
                }
            }
        }

    }


    Component.onCompleted: {
        for (var i=0; i<mouseToolActionArray.length; ++i) {
            var mouseAction = mouseToolActionArray[i];
            mouseAction.exclusiveGroup = mouseToolExclusiveGroup;
            mouseInteractorToolButton.createObject(mouseToolButtonRoot, {"action": mouseAction});
        }
    }
}
