// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.2

RowLayout {
    property Component content : Label {text:"No content"}
    property Component toolbar : Label {text:"No toolbar"}

    property var allToolWindows: []
    property var savedVisibleToolWindows: []

    onVisibleChanged: {
        if (visible) {
            // restore visible state
            for (var i=0; i<savedVisibleToolWindows.length; ++i)
                savedVisibleToolWindows[i].show();
            savedVisibleToolWindows = [];
        }
        else {
            // save visible state
            savedVisibleToolWindows = [];
            for (var i=0; i<allToolWindows.length; ++i)
                if (allToolWindows[i].visible) {
                    savedVisibleToolWindows.push(allToolWindows[i]);
                    allToolWindows[i].hide();
                }
        }
    }

    Loader {
        Layout.fillWidth: true
        Layout.fillHeight: true
        sourceComponent: content
    }

    Loader {
        Layout.rightMargin: 5
        Layout.minimumWidth: 80
        Layout.fillHeight: true
        anchors.top: parent.top
        anchors.topMargin: 5
        sourceComponent: toolbar
    }
}


