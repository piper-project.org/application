// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import Qt.labs.folderlistmodel 2.1

ComboBox {

    id: root

    property url folderUrl
    property url filePath

    currentIndex: 0

    ListModel {
        id: myListModel
    }

    FolderListModel {
        id: folderModel
        folder: root.folderUrl
        nameFilters: ["*.m"]
        showDirs: false

        onCountChanged: {
            myListModel.clear()
            myListModel.append({"text" : "Select..."})
            for(var i=0; i<folderModel.count; ++i)
                myListModel.append({"text" : String(folderModel.get(i, "fileName"))})
            myListModel.append({"text" : "Other..."})
        }
    }


    model: myListModel

    Component.onCompleted: {
        rootPath = "file:/"+context.applicationDirPath()
        setFilePath();
    }

    function setFilePath() {
        if (currentText.substr(0,7) === "file://") // user defined absolute path
            filePath = currentText
        else if (currentIndex > 0) // format rules provided with the piper application
            filePath = "file:" + folderModel.folder + currentText;
        else
            filePath="";
    }

    function addRulesFilePath(path) {
        model.insert(model.count-1, {text: path.toString()})
    }

    onCurrentIndexChanged: {
        if (currentIndex==model.count-1)
            selectOctaveScriptDialog.open();
        else
            setFilePath();
    }

    FileDialog {
        id: selectOctaveScriptDialog
        title: qsTr("Select Octave script file...")
        nameFilters: ["script (*.m)"]
        onAccepted: {
            filePath = selectOctaveScriptDialog.fileUrl
            root.addRulesFilePath(selectOctaveScriptDialog.fileUrl);
        }
        onRejected: {
            root.currentIndex=0;
        }
    }
}
