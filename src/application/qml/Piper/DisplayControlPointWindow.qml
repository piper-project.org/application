// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Control Points Display Options"

    minimumHeight : 200
    minimumWidth : 250
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;

    ColumnLayout
    {
        anchors.fill : parent
        anchors.centerIn: parent
        RowLayout
        {
            width:childrenRect.width
            Button
            {
                text: "None"
                tooltip: "clear selection"
                onClicked:
                {
                    noneSelection();
                }
            }
            Button
            {
                text: "All"
                tooltip: "select all"
                onClicked:
                {
                    allSelection();
                }
            }
            Button
            {
                text: "Invert"
                tooltip: "invert selection"
                onClicked:
                {
                    invertSelection()
                }
            }
        }
        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.MultiSelection
            model: listModel
            property variant vals;
            property variant roles;
            property variant numbers;
            onRowCountChanged:          //Every time the nb of elements in the list change
            {
                allSelection()
            }
            function getList()
            {
                listModel.clear();
                nb = 0;
                vals = contextMetaManager.getControlPointList();
                roles = contextMetaManager.getControlPointRoleList();
                numbers = contextMetaManager.getControlPointNumberList();
                for(var i =0; i<vals.length; i++)
                {
                    listModel.append({name: vals[i], role: roles[i], number: numbers[i]});
                    nb++;
                }
            }
            TableViewColumn
            {
                role: "name"
                title: "Control Points"
            }
            TableViewColumn
            {
                role: "role"
                title: "Roles"
            }
            TableViewColumn
            {
                role: "number"
                title: "Number"
            }
            Connections
            {
                target: tableView.selection
                onSelectionChanged :
                {
                    var listNames = [];
                    var listVisible = [];
                    for (var i = 0; i < tableView.model.count; i++)
                    {
                        listNames.push(tableView.model.get(i).name);
                        listVisible.push(tableView.selection.contains(i));
                    }
                    contextMetaManager.controlPointDisplayList(listNames, listVisible, false);
                }
            }

            Component.onCompleted:
            {
                getList();
            }
            Connections
            {
                target: context
                onVisDataLoaded:
                {
                    tableView.getList();
                }
            }
            Connections {
                target: context
                onMetadataChanged:  {
                    tableView.getList()
                }
            }
        }
    }

    function invertSelection()
    {
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            if (tableView.selection.contains(i))
            {
                listVisible.push(false);
                tableView.selection.deselect(i)
            }
            else
            {
                listVisible.push(true);
                tableView.selection.select(i)
            }
        }
        contextMetaManager.controlPointDisplayList(listNames, listVisible, false);
    }

    function noneSelection()
    {
        tableView.selection.clear();
        var listNames = [];
        var listVisible = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            listVisible.push(false);
        }
        contextMetaManager.controlPointDisplayList(listNames, listVisible, false);
    }

    function allSelection()
    {
        var listNames = [];
        var listVisible = [];
        if (tableView.model.count > 0)
        {
            tableView.selection.selectAll();
            tableView.focus = true;
            for (var i = 0; i < tableView.model.count; i++)
            {
                listNames.push(tableView.model.get(i).role);
                listVisible.push(true);
            }
        }
        contextMetaManager.controlPointDisplayList(listNames, listVisible, false);
    }

}
