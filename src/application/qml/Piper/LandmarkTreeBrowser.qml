// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1
import QtQml.Models 2.2

import AnatomicTreeModel 1.0
import AnatomyDB 1.0
import Piper 1.0

ColumnLayout
{
    id: main
    property int selectMode : SelectionMode.SingleSelection
    property bool visualizeSelected : false // setting this to true will make the treeview toggle visibility of selected entities in the contextMetaManager
    property string defaultEntity : "" // entity that will be selected upon (re)loading the data if single selection is turned on (selectMode == SelectionMode.SingleSelection
    property string treeViewTitle : "AnatomyDB Landmarks"
    property alias treeView : treeView
    property alias treeModel : entityTreeModel
    property alias searchText: searchTree.searchText
    property string database : ""

    property bool joints: false

    property var anatomyDBLandmarks : []
    property var ansurLandmarks : []

    property var anatomyDBJoints : []

    onDatabaseChanged: {

        if(root.database==="AnatomyDB"){
            console.log("Joints = "+main.joints)
            if ( main.joints==true){
                console.log("Calling the updateEntityList() for joints")
                entityTreeModel.updateEntityList()
            }
            if (anatomyDBLandmarks.length===0 && main.joints===false)
                entityTreeModel.updateEntityList()

            treeView.model = entityTreeModel.model
            itemSelectionModel.model = entityTreeModel.model
            searchTree.treeModel = entityTreeModel
        }else if(root.database==="Ansur" || root.database==="none" ){
            if((ansurLandmarks.length===0 && main.joints===false) || (anatomyDBJoints.length===0 && main.joints==true) )
                entityTreeModel2.updateEntityList()

            treeView.model = entityTreeModel2.model
            itemSelectionModel.model = entityTreeModel2.model
            searchTree.treeModel = entityTreeModel2
        }

    }

    onJointsChanged: {
        entityTreeModel.updateEntityList();
    }

    RowLayout
    {

        Layout.fillWidth: true
        visible: (selectMode != SelectionMode.SingleSelection && selectMode != SelectionMode.NoSelection) // make it visible only if it is possible to select more than one item
        Button
        {
            id: clearSelectionBtn
            text: "None"
            tooltip: "Clear Selection"
            onClicked: treeView.selection.clear()
        }
        Button
        {
            id: selectAllBtn
            text: "All"
            tooltip: "select all"
            onClicked: treeView.selection.selectAll()
        }
        Button
        {
            id: invertSelectionBtn
            text: "Invert"
            tooltip: "Invert Selection"
            onClicked: treeView.selection.invertSelection()
        }
    }

    AnatomicTreeModel {
        id: entityTreeModel

        function  updateEntityList() {
            console.log("updateEntityList() called ")
            if(main.joints==false){
                console.log("updateEntityList() called for the landmarks ")
                var regions = []
                var entities
                if(main.database=="AnatomyDB"){
                    
                    if(main.anatomyDBLandmarks.length === 0){
                        console.log("Fetching anatomy DB landmarks")
                        main.anatomyDBLandmarks = contextMetaManager.getAnatomyDBLandmarksList();
                        console.log(main.anatomyDBLandmarks.length + " Anatomy DB landmarks fecthed")
                    }
                    entities = main.anatomyDBLandmarks
                  
                }
                if(main.database=="Ansur")
                    entities = [ ];
                if(main.database=="none")
                    entities = [ ];
                qdataList = regions.concat(entities);
            }else{
                console.log("updateEntityList() called for the joints ")
                var regions = []
                var entities
                if(main.database=="AnatomyDB"){
                     
                    if(main.anatomyDBJoints.length === 0){
                        console.log("Fetching anatomy DB joints")
                        main.anatomyDBJoints = contextMetaManager.getAnatomyDBJointList();
                        console.log(main.anatomyDBJoints.length + " Anatomy DB joints fecthed")
                    }
                    entities = main.anatomyDBJoints
                     
                }
                if(main.database=="Ansur")
                    entities = [ ];
                if(main.database=="none")
                    entities = [ ];
                qdataList = regions.concat(entities);
            }
        }
    }

    AnatomicTreeModel {
        id: entityTreeModel2

        function  updateEntityList() {
            if(main.joints==false){
                var regions = []
                var entities
                if(main.database=="AnatomyDB"){
                     
                    if(main.anatomyDBLandmarks.length === 0){
                        console.log("Fetching anatomy DB ladnmarks")
                        main.anatomyDBLandmarks = contextMetaManager.getAnatomyDBLandmarksList();
                        console.log(main.anatomyDBLandmarks.length + " Anatomy DB landmarks fecthed")
                    }
                    entities = main.anatomyDBLandmarks
                     
                }
                if(main.database=="Ansur")
                    entities = [ ];
                if(main.database=="none")
                    entities = [ ];
                qdataList = regions.concat(entities);
            }else{
                var regions = []
                var entities
                if(main.database=="AnatomyDB"){
                     
                    if(main.anatomyDBJoints.length === 0){
                        console.log("Fetching anatomy DB joints")
                        main.anatomyDBJoints = contextMetaManager.getAnatomyDBJointList();
                        console.log(main.anatomyDBJoints.length + " Anatomy DB joints fecthed")
                    }
                    entities = main.anatomyDBJoints
                    
                }
                if(main.database=="Ansur")
                    entities = [ ];
                if(main.database=="none")
                    entities = [ ];
                qdataList = regions.concat(entities);
            }
        }
    }

    ItemSelectionModel
            {
                id: itemSelectionModel
                //model: entityTreeModel.model

                function selectAll() {
                    var allIndexes=model.getAllIndexes();
                    for (var i=0; i<allIndexes.length; ++i)
                        select(allIndexes[i], ItemSelectionModel.Select);
                }

                function invertSelection() {
                    var allIndexes = model.getLeafIndexes(); // only toggle if it is a leaf - toggling parents will select/deselect all children making the button useless
                    for (var i=0; i<allIndexes.length; ++i) {
                        select(allIndexes[i], ItemSelectionModel.Toggle);
                    }
                }

                function selectSingleEntity(entityName) {
                    var list = model.getLeafIndexes()
                    searchTree.searchField.search.text = main.anatomyDBLandmarks[treeView.currentIndex]
                    for (var i = 0; i < list.length; i++)
                    {
                        if (model.data(list[i]) === qsTr(entityName))
                        {
                            setCurrentIndex(list[i], ItemSelectionModel.Select);
                            treeView.focus = true;
                            break;
                        }
                    }
                }

                onSelectionChanged: {
                    //searchTree.searchField.search.text = main.anatomyDBLandmarks[treeView.currentIndex]
                //    if(treeView.currentIndex !== -1){
                    if(selected.length !==0 || deselected.length !==0){
                        //console.log("!")
                        for (var i=0; i<selected.length; ++i) {
                            var childrenIndexes = model.getChildrenIndexes(selected[i].topLeft, false);
                            for (var j=0; j<childrenIndexes.length; ++j)
                                select(childrenIndexses[j], ItemSelectionModel.Select);
                        }
                        for (var i=0; i<deselected.length; ++i) {
                            var childrenIndexes = model.getChildrenIndexes(deselected[i].topLeft, false);
                            for (var j=0; j<childrenIndexes.length; ++j)
                                select(childrenIndexes[j], ItemSelectionModel.Deselect);
                        }
                   }
                }
            }


    RowLayout{

                    Label
                    {
                        text: "Name : "
                    }


        SearchTree {
                    id: searchTree

                    Layout.fillWidth: true

                    height: 30
                    //radius: 5

                    treeModel: entityTreeModel
                    treeView: treeView

                    displayNavigationArrows : false
                    useButtonForSearching: true

                    Component.onCompleted: this.forceActiveFocus()

        }
    }

    TreeView {
        id: treeView
        visible : true
        Layout.fillWidth: true
        Layout.fillHeight : true



        model: entityTreeModel.model

            function selectSingleEntity(entityName) {
                var list = model.getLeafIndexes()
                for (var i = 0; i < list.length; i++)
                {
                    if (model.data(list[i]) === qsTr(entityName))
                    {
                        setCurrentIndex(list[i], ItemSelectionModel.Select);
                        treeView.focus = true;
                        break;
                    }
                }
            }


        selectionMode: selectMode
        selection:itemSelectionModel

//        Connections
//        {
//            target: context
//            onModelChanged : {
//                entityTreeModel.updateEntityList();
//                if (selectMode != SelectionMode.SingleSelection && selectMode != SelectionMode.NoSelection)
//                    treeView.selection.selectAll();
//                else if (selectMode == SelectionMode.SingleSelection)
//                    treeView.selection.selectSingleEntity(defaultEntity);
//            }
//        }

        itemDelegate: Label {
            font.bold: searchTree.matchIndex(styleData.index)
            text: styleData.value
        }

        TableViewColumn {
                role: "Name"
                title: treeViewTitle
        }
    }
}
