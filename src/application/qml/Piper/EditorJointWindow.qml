// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Joint Editor"

    id:jointEditor;

    minimumHeight : 400
    minimumWidth : 250
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;
    property variant entities;
    property variant frames;

    GroupBox{

        ListModel{
            id: typeList
            ListElement { text: "SOFT";}
            ListElement { text: "HARD";}
        }

        ListModel
        {
            id:entityList
        }
        ListModel
        {
            id:frameList
        }
        ListModel
        {
            id:entity2List
        }
        ListModel
        {
            id:frame2List
        }
        ColumnLayout{

            GroupBox {
                title: "Action"

                RowLayout {
                    ExclusiveGroup {
                        id: tabPositionGroup
                    }
                    RadioButton {
                        text: "Create Joint"
                        id: createLandmarkRadioButton
                        checked: true
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                jointName.visible = true
                                searchTextField.visible = false
                                table.visible = false
                                addJointButton.visible = true
                                updateJoint.visible = false
                                removeJoint.visible = false
                            }
                        }
                    }
                    RadioButton {
                        text: "Edit Joint"
                        id: editLandmarkRadioButton
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                jointName.visible = false
                                searchTextField.visible = true
                                table.visible = true
                                addJointButton.visible = false
                                updateJoint.visible = true
                                removeJoint.visible = true
                            }
                        }
                    }
                }
            }


            TextField
            {
                id:jointName;
                placeholderText: qsTr("Joint Name")
            }

            TextField {
                id: searchTextField
                placeholderText: qsTr("Search")
                Layout.row: 1
                Layout.column: 0
                Layout.fillWidth: true
                visible: false
                onTextChanged: {
                    tableView.getList(searchTextField.text)
                }
            }

            RowLayout
            {
                id: table
                visible: false

                ListModel
                {
                    id: listModel
                }

                TableView
                {
                    id: tableView
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible:true
                    selectionMode:SelectionMode.SingleSelection
                    model: listModel

                    function getList(query)
                    {
                        listModel.clear();
                        nb = 0;
                        var vals = contextMetaManager.getJointList();
                        var e1 = contextMetaManager.getJointEntity1List();
                        var e2 = contextMetaManager.getJointEntity2List();
                        var e1f = contextMetaManager.getJointEntity1FrameList();
                        var e2f = contextMetaManager.getJointEntity2FrameList();
                        for (var i = 0; i < vals.length; i++) {
                            if (!query) {
                                listModel.append({role: vals[i], entity1: e1[i], frame1: e1f[i], entity2: e2[i], frame2: e2f[i]});
                                nb++;
                            } else {
                                if (vals[i].toUpperCase().indexOf(searchTextField.text.toUpperCase()) !== -1) {
                                    listModel.append({role: vals[i], entity1: e1[i], frame1: e1f[i], entity2: e2[i], frame2: e2f[i]});
                                    nb++;
                                }
                            }
                        }
                        for(var i =0; i<vals.length; i++)
                        {
                            listModel.append({role: vals[i], entity1: e1[i], frame1: e1f[i], entity2: e2[i], frame2: e2f[i]});
                            nb++;
                        }
                    }
                    TableViewColumn
                    {
                        width: 100
                        role: "role"
                        title: "Joints"
                    }
                    TableViewColumn
                    {
                        width: 70
                        role: "entity1"
                        title: "Entity 1"
                    }
                    TableViewColumn
                    {
                        width: 20
                        role: "frame1"
                        title: "F1"
                    }
                    TableViewColumn
                    {
                        width: 70
                        role: "entity2"
                        title: "Entity 2"
                    }
                    TableViewColumn
                    {
                        width: 20
                        role: "frame2"
                        title: "F2"
                    }

                    Component.onCompleted:
                    {
                        getList();
                    }
                    Connections {
                        target: context
                        onMetadataChanged:  {
                            tableView.getList()
                        }
                    }
                    onClicked:{
                        updateJoint.enabled = true;
                        jointName.text = listModel.get(row).role;
                        entity1Combo.currentIndex = entity1Combo.find(listModel.get(row).entity1)
                        entity1FrameCombo.currentIndex = entity1FrameCombo.find(listModel.get(row).frame1)
                        entity2Combo.currentIndex = entity2Combo.find(listModel.get(row).entity2)
                        entity2FrameCombo.currentIndex = entity2FrameCombo.find(listModel.get(row).frame2)
                    }
                }
            }

            GridLayout{
                Label
                {
                    Layout.alignment : Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Constrained DOF Type"
                    Layout.row: 0
                    Layout.column: 0
                }
                Label
                {
                    Layout.alignment : Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Entity 1"
                    Layout.row: 1
                    Layout.column: 0
                }
                Label
                {
                    Layout.alignment : Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Entity 1 Frame"
                    Layout.row: 2
                    Layout.column: 0
                }
                Label
                {
                    Layout.alignment : Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Entity 2"
                    Layout.row: 3
                    Layout.column: 0
                }
                Label
                {
                    Layout.alignment : Qt.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Entity 2 Frame"
                    Layout.row: 4
                    Layout.column: 0
                }
                ComboBox {
                    id: typeCombo
                    currentIndex: 0
                    model: typeList
                    width: 200
                    Layout.row: 0
                    Layout.column: 1
                }
                ComboBox {
                    id: entity1Combo
                    currentIndex: 0
                    model: entityList
                    width: 200
                    Layout.row: 1
                    Layout.column: 1
                    Component.onCompleted:
                    {
                        jointEditor.getEntityList();
                    }
                    onCurrentIndexChanged: {
                        if(entity1Combo.currentIndex != 0 && entity1FrameCombo.currentIndex && entity2Combo.currentIndex && entity2FrameCombo.currentIndex){
                            addJointButton.enabled = true;
                        }
                    }
                }
                ComboBox {
                    id: entity1FrameCombo
                    currentIndex: 0
                    model: frame2List
                    width: 200
                    Layout.row: 2
                    Layout.column: 1
                    Component.onCompleted:
                    {
                        jointEditor.getFrameList();
                    }
                    onCurrentIndexChanged: {
                        if(entity1Combo.currentIndex != 0 && entity1FrameCombo.currentIndex && entity2Combo.currentIndex && entity2FrameCombo.currentIndex){
                            addJointButton.enabled = true;
                        }
                    }
                }
                ComboBox {
                    id: entity2Combo
                    currentIndex: 0
                    model: entity2List
                    width: 200
                    Layout.row: 3
                    Layout.column: 1
                    Component.onCompleted:
                    {
                        jointEditor.getEntity2List();
                    }
                    onCurrentIndexChanged: {
                        if(entity1Combo.currentIndex != 0 && entity1FrameCombo.currentIndex && entity2Combo.currentIndex && entity2FrameCombo.currentIndex){
                            addJointButton.enabled = true;
                        }
                    }
                }
                ComboBox {
                    id: entity2FrameCombo
                    currentIndex: 0
                    model: frame2List
                    width: 200
                    Layout.row: 4
                    Layout.column: 1
                    Component.onCompleted:
                    {
                        jointEditor.getFrame2List();
                    }
                    onCurrentIndexChanged: {
                        if(entity1Combo.currentIndex != 0 && entity1FrameCombo.currentIndex && entity2Combo.currentIndex && entity2FrameCombo.currentIndex){
                            addJointButton.enabled = true;
                        }
                    }
                }
            }

            RowLayout{

                Button
                {
                    id: addJointButton
                    visible: true
                    text: "Add"
                    tooltip: "Add Joint"
                    enabled: false
                    onClicked:
                    {
                        if(jointName.text != ""){
                            myMetaEditor.addJoint(jointName.text, entityList.get(typeCombo.currentIndex).text, entityList.get(entity1Combo.currentIndex).text, frameList.get(entity1FrameCombo.currentIndex).text, entity2List.get(entity2Combo.currentIndex).text, frame2List.get(entity2FrameCombo.currentIndex).text)
                        }
                    }
                }

                Button
                {
                    id: updateJoint
                    visible: false
                    text: "Update"
                    tooltip: "Update Joint"
                    enabled: false
                    onClicked:
                    {
                        if(jointName.text != ""){
                            myMetaEditor.addJoint(jointName.text, entityList.get(typeCombo.currentIndex).text, entityList.get(entity1Combo.currentIndex).text, frameList.get(entity1FrameCombo.currentIndex).text, entity2List.get(entity2Combo.currentIndex).text, frame2List.get(entity2FrameCombo.currentIndex).text, 1)
                        }
                    }
                }

                Button{
                    id: removeJoint
                    visible: false
                    text: "Remove"
                    tooltip: "Remove Joint"
                    onClicked:{
                         for(var i=0; i<listModel.count; i++){
                            if(tableView.selection.contains(i))
                            {
                                myMetaEditor.removeJoint(listModel.get(i).role);
                            }
                         }
                    }
                }

            }



        }
    }

    function getEntityList()
    {
        entityList.clear();
        nb = 0;
        entities = contextMetaManager.getEntityList();
        for(var i =0; i<entities.length; i++)
        {
            if(i==0){
                entityList.append({text: ""});
            }
            entityList.append({text: entities[i]});
            nb++;
        }
    }
    function getFrameList()
    {
        frameList.clear();
        nb = 0;
        frames = contextMetaManager.getFrameList();
        for(var i =0; i<frames.length; i++)
        {
            if(i==0){
                frameList.append({text: ""});
            }
            frameList.append({text: String(frames[i])});
            nb++;
        }
    }
    function getEntity2List()
    {
        entity2List.clear();
        nb = 0;
        entities = contextMetaManager.getEntityList();
        for(var i =0; i<entities.length; i++)
        {
            if(i==0){
                entity2List.append({text: ""});
            }
            entity2List.append({text: entities[i]});
            nb++;
        }
    }
    function getFrame2List()
    {
        frame2List.clear();
        nb = 0;
        frames = contextMetaManager.getFrameList();
        for(var i =0; i<frames.length; i++)
        {
            if(i==0){
                frame2List.append({text: ""});
            }
            frame2List.append({text: String(frames[i])});
            nb++;
        }
    }
}
