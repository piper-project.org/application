// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1

import Piper 1.0
import AnatomicTreeModel 1.0
import AnatomyDB 1.0

ModuleToolWindow
{
    title: "Entity Editor"
    id: entityEditor

    minimumHeight : 450
    minimumWidth : 250

    property int nb: 0;
    property string selectedNode
    property variant selectedList
    property  variant map
    property variant selectedObject
    property string newName

    property string create: ""

    onCreateChanged: { entityName.text = create  }
    
    ListModel{  id:anatomyEntityList }

    GroupBox{


        ColumnLayout{

            width: parent.width
            Layout.fillHeight: true


            GroupBox {
                title: "Action"

                RowLayout {
                    ExclusiveGroup {
                        id: tabPositionGroup
                    }
                    RadioButton {
                        text: "Create Entity"
                        id: createLandmarkRadioButton
                        checked: true
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                entityName.visible = true
                                addElementSet.visible = true
                                updateEntity.visible = false
                                removeElementSet.visible = false
                                table.visible = false
                                searchTextField.visible = false
                            }
                        }
                    }
                    RadioButton {
                        text: "Edit Entity"
                        id: editLandmarkRadioButton
                        exclusiveGroup: tabPositionGroup
                        onCheckedChanged: {
                            if (checked === true) {
                                entityName.visible = false
                                addElementSet.visible = false
                                updateEntity.visible = true
                                removeElementSet.visible = true
                                table.visible = true
                                searchTextField.visible = true
                            }
                        }
                    }
                }
            }


            TextField{
                id:entityName;
                placeholderText: qsTr("Entity Name")
                Layout.row: 0
                Layout.column : 0
            }

            TextField {
                id: searchTextField
                placeholderText: qsTr("Search")
                Layout.row: 1
                Layout.column: 0
                Layout.fillWidth: true
                visible: false
                onTextChanged: {
                    tableView.getList(searchTextField.text)
                }
            }

            RowLayout
            {
                id : table
                visible: false
                ListModel
                {
                    id:listModel
                }
                TableView
                {
                    id: tableView
                    alternatingRowColors: false
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible: true
                    selectionMode: SelectionMode.SingleSelection
                    model: listModel

                    property variant vals;

                    function processClickedItem(index)
                    {
                        if(tableView.selection.contains(index))
                        {
                            myMetaEditor.selectActor(1, listModel.get(index).role);
                        }
                        else
                        {
                            myMetaEditor.selectActor(0, listModel.get(index).role);
                        }
                    }
                    function getList(query) {
                        listModel.clear()
                        vals = contextMetaManager.getEntityList()
                        for (var i = 0; i < vals.length; i++) {
                            if (!query) {
                                listModel.append({ role: vals[i] })
                            } else {
                                if (vals[i].toUpperCase().indexOf(searchTextField.text.toUpperCase()) !== -1) {
                                    listModel.append({role: vals[i]})
                                }
                            }
                        }
                    }
                    Connections {
                        target: context
                        onMetadataChanged:  {
                            tableView.getList()
                        }
                    }
                    Component.onCompleted:
                    {
                        getList();
                    }


                    TableViewColumn
                    {
                        role: "role"
                        title: "New Entities"
                    }
                    onClicked:processClickedItem(row)
                }
            }

           GroupBox{
               title: "Pickers"

               ColumnLayout{

                   RowLayout{

                      visible: false
                      ExclusiveGroup { id: radio2 }

                      RadioButton {
                          text: "Select elements interactively"
                          checked: true
                          exclusiveGroup: radio2
                          onClicked: {
                              entitiesByGroup.visible = false
                              entitySelectionArea.visible = false
                              entityByElements.visible = true
                          }
                      }

                      RadioButton {
                          visible: false
                          id:addGroupOfEntities
                          text: "Form a Group of entities"
                          exclusiveGroup: radio2
                          property int count

                          function buildTree(){
                              objModel.clear()
                              count = 0
                              var node =  entityEditor.map["Root"]
                              if (entityEditor.map[node] != null) {
                                  var mapdata = '';
                                  for (var prop  in entityEditor.map) {
                                    mapdata += prop + ' : ' + entityEditor.map[prop]+' No of Children = '+entityEditor.map[prop].length+';\n';
                                    console.log( mapdata )
                                    mapdata= ''
                                  }
                                  objModel.append({role: qsTr(node), "level": 0, "subNode": [], "childCount": entityEditor.map[node].length, "selected":false, "id":0, "parent_id":0})
                                  var parentNode = objModel.get(0)
                                  for(var i=0; i<entityEditor.map[node].length ; i++) {
                                      console.log(entityEditor.map[node][i])
                                      addNode( parentNode , qsTr(entityEditor.map[node][i]) , 1, i )
                                  }
                              }
                          }

                          function addNode( parentNode , node , level , position ){
                              //console.log(node)
                              var childCount
                              //console.log("Leaf Node")
                              if(map[node]==null){
                                  //console.log("Leaf Node")
                                  childCount = 0
                              }
                              else
                                  childCount = map[node].length
                              parentNode.subNode.append({role: qsTr(node), "level": level, "subNode": [], "childCount":childCount , "selected":false, "id":++count, "parent_id":parentNode.id})
                              var current = parentNode.subNode.get(position)
                              //console.log(current.role+"\t Level = "+current.level+"\t child count = "+current.childCount)
                              var i=0
                              for(i; i<childCount ; i++) {
                                  //console.log(map[node][i])
                                  addNode( current , qsTr(map[node][i]) , level+1 ,i )
                              }

                          }
                          onClicked: {
                              entityByElements.visible = false
                              entitiesByGroup.visible = true
                              entitySelectionArea.visible = true
                              entityEditor.selectedList= []
                              displaySelectedList.text = ""
                              entityEditor.map = {"Root":"Entities", "Entities":["Predefined","Group of entities"]}
                              entityEditor.map["Predefined"] = contextMetaManager.getEntityList();
                              entityEditor.map["Group of entities"] = []
                              buildTree()
                              adjustWindowSizeToContent(20, 20)
                          }
                      }

                   }

                   ColumnLayout{
                       id: entityByElements
                       GridLayout
                       {
                           id: pickerButtons
                           Label
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               text: "Entity"
                               Layout.row: 0
                               Layout.column: 1

                           }
                           Label
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               text: "Element"
                               Layout.row: 0
                               Layout.column: 2

                           }
                           Label
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               text: "Single Pick"
                               Layout.row: 1
                               Layout.column: 0
                           }
                           Label
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               text: "Rubber Band"
                               Layout.row: 2
                               Layout.column: 0
                           }
                           Label
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               text: "Box"
                               Layout.row: 3
                               Layout.column: 0
                           }
                           Button
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               Layout.row: 1
                               Layout.column : 1
                               tooltip: "Entity Picker"
                               checkable: true
                               iconSource: "qrc:///icon/picker-entity.png"
                               onClicked:
                               {
                                   if (checked)
                                   {
                                       pickerButtons.uncheckPickerButtons();
                                       checked = true;
                                       hintLabel.text = contextVtkDisplay.SetPickingType(3);
                                   }
                                   else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                               }
                           }
                           Button
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               Layout.row: 1
                               Layout.column : 2
                               tooltip: "Element Picker"
                               checkable: true
                               iconSource: "qrc:///icon/picker-element.png"
                               onClicked:
                               {
                                   if (checked)
                                   {
                                       pickerButtons.uncheckPickerButtons();
                                       checked = true;
                                       hintLabel.text = contextVtkDisplay.SetPickingType(2);
                                   }
                                   else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                               }
                           }
                           Button
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               Layout.row: 2
                               Layout.column : 2
                               tooltip: "Rubberband Element Picker"
                               checkable: true
                               iconSource: "qrc:///icon/rubberband-element.png"
                               onClicked:
                               {
                                   if (checked)
                                   {
                                       pickerButtons.uncheckPickerButtons();
                                       checked = true;
                                       hintLabel.text = contextVtkDisplay.SetPickingType(5);
                                   }
                                   else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                               }
                           }
                           Button
                           {
                               Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                               Layout.row: 3
                               Layout.column : 2
                               tooltip: "Box Element Picker"
                               checkable: true
                               iconSource: "qrc:///icon/box-element.png"
                               onClicked:
                               {
                                   if (checked)
                                   {
                                       pickerButtons.uncheckPickerButtons();
                                       checked = true;
                                       hintLabel.text = contextVtkDisplay.SetPickingType(8);
                                   }
                                   else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                               }
                           }						   
                           // uncheck all the buttons in the pickerButtons panel
                           function uncheckPickerButtons()
                           {
                               for (var i = 0; i < pickerButtons.children.length; i++)
                               {
                                   if (pickerButtons.children[i].checkable)
                                       pickerButtons.children[i].checked = false;
                               }
                           }
                       }
                       ColumnLayout
                       {
                           CheckBox
                           {
                               id: checkboxSeeHint
                               checked: false
                               text: "See hints"
                               onCheckedChanged:
                               {
                                   hintLabel.visible = checked
                                   if (checked)
                                   {
                                       hintLabel.text = hintLabel.text // to refresh the label if it was invisible...don't ask me why, but this way it works
                                       adjustWindowSizeToContent();
                                   }
                               }
                           }
                           Label
                           {
                               id: hintLabel
                               text : "No picker is active."
                               visible: checkboxSeeHint.checked
                               Layout.fillWidth: true
                               onContentWidthChanged :
                               {
                                   adjustWindowSizeToContent();
                               }
                           }
                       }
                    }


           }           
        }




       RowLayout{
           Button
           {
               id: addElementSet
               text: "Add"
               tooltip: "Create Entity"
               onClicked:{
                   if(entityName.text != ""){
                       myMetaEditor.getSelectedElementIDsBusyIndicator(entityName.text, 0);
                       listModel.append({role: entityName.text});
                   }
               }
           }
           Button
           {
               id: updateEntity
               visible: false
               text: "Update"
               tooltip: "Update Entity"
               onClicked:{
                    for(var i=0; i<listModel.count; i++){
                       if(tableView.selection.contains(i))
                       {
                           myMetaEditor.getSelectedElementIDsBusyIndicator( listModel.get(i).role, 0, 1);
                       }
                    }
               }
           }
           Button{
               id: removeElementSet
               visible: false
               text: "Remove"
               tooltip: "Remove Entity"
               onClicked:{
                    for(var i=0; i<listModel.count; i++){
                       if(tableView.selection.contains(i))
                       {
                           myMetaEditor.removeEntity(listModel.get(i).role);
                       }
                    }
               }
           }
       }



    }
    }
}

