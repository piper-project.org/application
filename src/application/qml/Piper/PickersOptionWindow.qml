// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.1
import QtQml 2.2
import Qt.labs.settings 1.0

ModuleToolWindow
{
    title: "Selection"
    id: root
    width : childrenRect.width + 20
    height : childrenRect.height + 20

    Settings {
        id: settings
        category: "selection"

        property url recentExportFolder
    }

    GridLayout
    {
        anchors.fill: parent
        ColumnLayout
        {
            Layout.row: 0
            Layout.column : 0
            Layout.alignment : Qt.AlignTop | Qt.AlignHCenter
            id: mainLayout
            GroupBox
            {
                title:"Pickers"
                GridLayout
                {
                    id: pickerButtons
                    Label
                    {
                        Layout.preferredWidth: longestLabel.width
                        Layout.alignment : Qt.AlignVCenter | Qt.AlignHCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Entity"
                        Layout.row: 0
                        Layout.column: 1
                    }
                    Label 
                    {
                        Layout.preferredWidth : longestLabel.width
                        Layout.alignment : Qt.AlignVCenter | Qt.AlignHCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Node"
                        Layout.row: 0
                        Layout.column: 2
                    }
                    Label 
                    {
                        Layout.preferredWidth: longestLabel.width
                        Layout.alignment : Qt.AlignVCenter | Qt.AlignHCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Element"
                        Layout.row: 0
                        Layout.column: 3
                    }
                    Label 
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        id: longestLabel
                        text: "Landmark"
                        Layout.row: 0
                        Layout.column: 4
                    }
                    Label 
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.preferredWidth: longestLabel.width
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Frame"
                        Layout.row: 0
                        Layout.column: 5

                    }
                    Label
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.preferredWidth: longestLabel.width
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: "Point"
                        Layout.row: 0
                        Layout.column: 6

                    }
                    Label 
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        text: "Single pick"
                        Layout.row: 1
                        Layout.column: 0

                    }
                    Label 
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        text: "Rubber band (surface only)"
                        Layout.row: 2
                        Layout.column: 0

                    }
                    Label 
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        text: "Box"
                        Layout.row: 3
                        Layout.column: 0
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column: 1
                        iconSource: "qrc:///icon/picker-entity.png"
                        tooltip: "Entity Picker"
                        checkable: true
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(3)
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column : 2
                        tooltip: "Node Picker"
                        checkable: true
                        iconSource: "qrc:///icon/picker-node.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(0);
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }      
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column : 3
                        tooltip: "Element Picker"
                        checkable: true
                        iconSource: "qrc:///icon/picker-element.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(2);
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column : 4
                        iconSource: "qrc:///icon/picker-landmark.png"
                        tooltip : "Landmark picker"
                        checkable : true
                        onClicked :
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(4)
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column: 5
                        iconSource: "qrc:///icon/picker-frame.png"
                        tooltip: "Frame Picker"
                        checkable: true
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(3) // right now, frame picking is the same as entity picking
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 2
                        Layout.column : 3
                        tooltip: "Rubberband Element Picker"
                        checkable: true
                        iconSource: "qrc:///icon/rubberband-element.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(5);
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 3
                        Layout.column : 2
                        tooltip: "Box Node Picker"
                        checkable: true
                        iconSource: "qrc:///icon/box-node.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(7);
                                checked = true;
                                selInsideBoxButton.enabled = true
                                deselInsideBoxButton.enabled = true
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }      
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 3
                        Layout.column : 3
                        tooltip: "Box Element Picker"
                        checkable: true
                        iconSource: "qrc:///icon/box-element.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(8);
                                checked = true;
                                selInsideBoxButton.enabled = true
                                deselInsideBoxButton.enabled = true
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Button
                    {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                        Layout.row: 1
                        Layout.column : 6
                        tooltip: "Point Picker"
                        checkable: true
                        iconSource: "qrc:///icon/picker-node.png"
                        onClicked:
                        {
                            if (checked)
                            {
                                hintLabel.text = contextVtkDisplay.SetPickingType(6);
                                checked = true;
                            }
                            else hintLabel.text = contextVtkDisplay.SetPickingType(-1);
                        }
                    }
                    Connections
                    {
                        target: contextVtkDisplay
                        onPickTargetChanged : pickerButtons.uncheckPickerButtons()
                    }
                    // uncheck all the buttons in the pickerButtons panel
                    function uncheckPickerButtons()
                    {
                        selInsideBoxButton.enabled = false
                        deselInsideBoxButton.enabled = false
                        for (var i = 0; i < pickerButtons.children.length; i++)
                        {
                            if (pickerButtons.children[i].checkable)
                                pickerButtons.children[i].checked = false;
                        }
                    }
                }
            }
            GroupBox
            {
                title:"Selection filters"
                anchors.left: parent.left
                anchors.right : parent.right
                GridLayout
                {
                    anchors.horizontalCenter : parent.horizontalCenter
                    Button
                    {
                        Layout.row: 0
                        Layout.column : 0
                        Layout.fillWidth: true
                        text: "Select nodes of selected elements"
                        onClicked: contextVtkDisplay.SelectNodesByElements(false)
                    }
                    Button
                    {
                        Layout.row: 1
                        Layout.column : 0
                        Layout.fillWidth: true
                        text: "Select elements of selected nodes"
                        onClicked : contextVtkDisplay.SelectElementsByNodes()
                    }
                    Button
                    {
                        Layout.row: 2
                        Layout.column : 0
                        Layout.fillWidth: true
                        text: "Deselect nodes of selected elements"
                        onClicked : contextVtkDisplay.SelectNodesByElements(true)
                    }
                    Button
                    {
                        Layout.row: 0
                        Layout.column : 1
                        Layout.fillWidth: true
                        text: "Invert nodes selection"
                        onClicked : contextVtkDisplay.InvertMeshNodesSelection()
                    }
                    Button
                    {
                        Layout.row: 1
                        Layout.column : 1
                        Layout.fillWidth: true
                        text: "Invert element selection"
                        onClicked : contextVtkDisplay.InvertCellsSelection()
                    }
                    Button
                    {
                        id: deselectBones
                        Layout.row: 2
                        Layout.column : 1
                        Layout.fillWidth: true
                        text: "Deselect bone elements"
                        onClicked: context.DeselectBoneElements()
                    }
                    Button
                    {
                        Layout.row: 3
                        Layout.column : 1
                        Layout.fillWidth: true
                        id: exportSelect
                        text: "Export selected elements"
                        tooltip: "Works only on the full model; extracts each connected group of selected elements as a .vtu file"
                        onClicked: fileDialog.open()
                    }
                }
            }
        }
        ColumnLayout
        {
            Layout.row: 0
            Layout.column : 1
            Layout.alignment : Qt.AlignTop | Qt.AlignHCenter		

            GroupBox
            {	
                title:"Selection boxes options"
                ColumnLayout
                {
                    id: boxesManagement
                    CheckBox
                    {
                        id: checkboxVisualizeBoxes
                        checked : true
                        text: "Draw selection boxes"           
                        onClicked :  contextVtkDisplay.optionVisualizeBoxes = checkboxVisualizeBoxes.checked
                        Component.onCompleted : checked = contextVtkDisplay.optionVisualizeBoxes
                        Connections
                        {
                            target: contextVtkDisplay
                            onOptionVisualizeBoxesChanged : checkboxVisualizeBoxes.checked = contextVtkDisplay.optionVisualizeBoxes
                        }
                    }
                    Button
                    {
                        text: "Remove all boxes"
                        onClicked:
                        {
                            contextVtkDisplay.ClearSelectionBoxes()
                            contextVtkDisplay.Refresh()
                        }
                    }
                    RowLayout
                    {
                        Button
                        {
                            text: "  Select next box  "
                            onClicked:
                            {
                                var selID = contextVtkDisplay.SelectNextSelectionBox()
                                if (selID != -1)
                                {
                                    selectedBoxLabel.text = "Selected box #" + selID
                                    contextVtkDisplay.Render()
                                    operationOnSelectedBox.enabled = true
                                }
                                else operationOnSelectedBox.enabled = false
                            }
                        }
                        Button
                        {
                            text: "Select previous box"
                            onClicked:
                            {
                                var selID = contextVtkDisplay.SelectPrevSelectionBox()
                                if (selID != -1)
                                {
                                    selectedBoxLabel.text = "Selected box #" + selID
                                    contextVtkDisplay.Render()
                                    operationOnSelectedBox.enabled = true
                                }
                                else operationOnSelectedBox.enabled = false
                            }
                        }
                        Label
                        {

                            id: selectedBoxLabel
                            text: "No box selected"
                        }
                    }
                    GroupBox
                    {
                        id: operationOnSelectedBox
                        enabled: false
                        anchors.horizontalCenter : boxesManagement.horizontalCenter
                        title: "Selected box options"
                        ColumnLayout
                        {
                            Button
                            {
                                Layout.fillWidth: true
                                text: "Remove selected box"
                                onClicked:
                                {
                                    contextVtkDisplay.RemoveSelectedBox()
                                    contextVtkDisplay.Refresh()
                                    selectedBoxLabel.text = "No box selected"
                                    operationOnSelectedBox.enabled = false
                                }
                            }
                            Button
                            {
                                Layout.fillWidth: true
                                text: "Select inside of selected box"
                                id: selInsideBoxButton
                                onClicked:
                                {
                                    exportSelect.visible= true
                                    if (contextVtkDisplay.SelectBySelectedBox(false)) // is false in case the box is no longer valid
                                        contextVtkDisplay.Render()
                                    else
                                    {
                                        selectedBoxLabel.text = "No box selected"
                                        operationOnSelectedBox.enabled = false
                                    }
                                }
                            }
                            Button
                            {
                                Layout.fillWidth: true
                                text: "Deselect inside of selected box"
                                id: deselInsideBoxButton
                                onClicked:
                                {
                                    exportSelect.visible= false
                                    if (contextVtkDisplay.SelectBySelectedBox(true))
                                        contextVtkDisplay.Render()
                                    else
                                    {
                                        selectedBoxLabel.text = "No box selected"
                                        operationOnSelectedBox.enabled = false
                                    }
                                }
                            }                            
                        }
                    }
                    Button
                    {
                        Layout.fillWidth: true
                        text: " Build boxes around selected elements "
                        onClicked : contextVtkDisplay.BuildBoxesBySelCells()
                    }
                    Button
                    {
                        Layout.fillWidth: true
                        text: " Build boxes around selected nodes "
                        onClicked : contextVtkDisplay.BuildBoxesBySelPoints()
                    }
                }
            }
			GroupBox
            {			
                title:"Selection by plane"
				anchors.left: parent.left
				anchors.right : parent.right

				ColumnLayout
				{
					GridLayout
					{
						Label
						{
							Layout.row: 0
							Layout.column : 0
							text: "Reference point: "
						}
						Label
						{
							Layout.row: 0
							Layout.column : 1
							text: "X: "
						}
						TextField
						{
							id: planeRefPointX
							Layout.row: 0
							Layout.column : 2
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "0"
						}
						Label
						{
							text: "Y: "
							Layout.row: 0
							Layout.column : 3
						}
						TextField
						{
							id: planeRefPointY
							Layout.row: 0
							Layout.column : 4
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "0"
						}
						Label
						{
							text: "Z: "
							Layout.row: 0
							Layout.column : 5
						}
						TextField
						{
							id: planeRefPointZ
							Layout.row: 0
							Layout.column : 6
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "0"
						}
						Label
						{
							text: "Normal: "
							Layout.row: 1
							Layout.column : 0
						}
						Label
						{
							text: "X: "
							Layout.row: 1
							Layout.column : 1
						}
						TextField
						{
							id: planeNormalX
							Layout.row: 1
							Layout.column : 2
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "1"
						}
						Label
						{
							text: "Y: "
							Layout.row: 1
							Layout.column : 3
						}
						TextField
						{
							id: planeNormalY
							Layout.row: 1
							Layout.column : 4
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "0"
						}
						Label
						{
							text: "Z: "
							Layout.row: 1
							Layout.column : 5
						}
						TextField
						{
							id: planeNormalZ
							Layout.row: 1
							Layout.column : 6
							validator: DoubleValidator {notation:DoubleValidator.ScientificNotation}
							implicitWidth: 35
							text: "0"
						}
					}				
					CheckBox
					{
					    id: drawPlaneCheckbox
					    checked : false
					    text: "Draw selection plane"    
					    Layout.row: 0
					    Layout.column: 1
					    onCheckedChanged:
					    {
					        if (checked)
					        {
					            contextVtkDisplay.VisualizePlane(parseFloat(planeRefPointX.text), parseFloat(planeRefPointY.text),parseFloat(planeRefPointZ.text),
								     parseFloat(planeNormalX.text), parseFloat(planeNormalY.text), parseFloat(planeNormalZ.text), "Selection Plane")
					        }
					        else
					        {
					            contextVtkDisplay.RemoveActor("Selection Plane", true)
					            contextVtkDisplay.Refresh()
					        }
					    }
					}
                    GridLayout
                    {
						Label
						{
							Layout.row: 0
							Layout.column: 0
							text: "Select Elements"
						}
                        Button
                        {
							id: planeSelElePositive
							text: "+"
							tooltip: "Select elements in direction of plane normal"
							Layout.row: 0
							Layout.column: 1
							implicitWidth: 35
                            onClicked:
                            {
								contextVtkDisplay.SelectElementsByPlane(parseFloat(planeRefPointX.text), parseFloat(planeRefPointY.text),parseFloat(planeRefPointZ.text),
								 parseFloat(planeNormalX.text), parseFloat(planeNormalY.text), parseFloat(planeNormalZ.text), true)
                            }
                        }
						Button
                        {
							id: planeSelEleNegative
							text: "-"
							tooltip: "Select elements in the direction opposite to the plane normal"
							Layout.row: 0
							Layout.column: 2
							implicitWidth: 35
                            onClicked:
                            {
								contextVtkDisplay.SelectElementsByPlane(parseFloat(planeRefPointX.text), parseFloat(planeRefPointY.text),parseFloat(planeRefPointZ.text),
								 parseFloat(planeNormalX.text), parseFloat(planeNormalY.text), parseFloat(planeNormalZ.text), false)
                            }
                        }
						Label
						{
							Layout.row: 1
							Layout.column: 0
							text: "Select Nodes"
						}
                        Button
                        {
							id: planeSelNodePositive
							text: "+"
							tooltip: "Select nodes in direction of plane normal"
							Layout.row: 1
							Layout.column: 1
							implicitWidth: 35
                            onClicked:
                            {
								contextVtkDisplay.SelectNodesByPlane(parseFloat(planeRefPointX.text), parseFloat(planeRefPointY.text),parseFloat(planeRefPointZ.text),
								 parseFloat(planeNormalX.text), parseFloat(planeNormalY.text), parseFloat(planeNormalZ.text), true)
                            }
                        }
						Button
                        {
							id: planeSelNodeNegative
							text: "-"
                            tooltip: "Select nodes in the direction opposite to the plane normal"
							Layout.row: 1
							Layout.column: 2
							implicitWidth: 35
                            onClicked:
                            {
								contextVtkDisplay.SelectNodesByPlane(parseFloat(planeRefPointX.text), parseFloat(planeRefPointY.text),parseFloat(planeRefPointZ.text),
								 parseFloat(planeNormalX.text), parseFloat(planeNormalY.text), parseFloat(planeNormalZ.text), false)
                            }
                        }
                    }
                }
            }
        }
        ColumnLayout
        {
            Layout.row: 1
            Layout.columnSpan: 2
            CheckBox
            {
                id: checkboxSeeHint
                checked: false
                text: "See picker hints"
                onCheckedChanged:
                {
                    hintLabel.visible = checked
                    if (checked)
                    {
                        hintLabel.text = hintLabel.text // to refresh the label if it was invisible...don't ask me why, but this way it works
                    }
					root.adjustWindowSizeToContent();
                }
            }
            Label
            {
                id: hintLabel
                text : "No picker is active."
                visible: checkboxSeeHint.checked
                Layout.fillWidth: true
                onContentWidthChanged :
                {
                    root.adjustWindowSizeToContent();
                }
            }
        }
    }
    
    FileDialog 
    {
        id: fileDialog
        title: "Export selection to folder"
        selectFolder: true
        selectMultiple: false
        selectExisting: true
        onAccepted: {
            settings.recentExportFolder = folder;
            context.ExportSelectedElements(folder)
        }
        onVisibleChanged: {            
            if(visible)
            {
                if(context.doFolderExist(settings.recentExportFolder))
                    folder = settings.recentExportFolder;
            }
        }
    }  
    Component.onCompleted:
    {
        adjustWindowSizeToContent();
    }

	onVisibleChanged:
	{
		if (visible === false)
        {
		    contextVtkDisplay.RemoveActor("Selection Plane", true);
		    drawPlaneCheckbox.checked = false
            contextVtkDisplay.Refresh()
        }
	}
}

