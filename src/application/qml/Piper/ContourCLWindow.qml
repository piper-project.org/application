// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2

import SofaBasics 1.0
import SofaWidgets 1.0

import piper.MetaEditor 1.0
import VtkQuick 1.0
import piper.MetaViewManager 1.0
import "../check"
import Piper 1.0

ModuleToolWindow {
    id: main
    title: "Contour CL Editor"
    minimumHeight: 500
    minimumWidth: 900

    property string selectedNode: ""
    property string selectedNodeType

    onSelectedNodeChanged: {
        nodeDescriptionText.text = "Description for " + selectedNode
    }

    ListModel {
        id: predefinedLandmarksList
    }
    ListModel {
        id: anatomyDBLandmarksList
    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        ColumnLayout {

            Button {
                id: loadContourCLButton
                text: "Load ContourCL file"
                onClicked: {
                    openContourCLXMLDialog.open()
                }
            }

            Button {
                id: refreshCLDisplayButton
                text: "Refresh ContourCL Display"
                enabled: false
                onClicked: {
                    myMetaEditor.ui_loadContourCLXml(
                                openContourCLXMLDialog.fileUrl)
                }
            }

            FileDialog {
                id: openContourCLXMLDialog
                title: qsTr("Open ContourCL xml File...")
                nameFilters: ["Contour files (*.xml)"]
                onAccepted: {
                    console.log(openContourCLXMLDialog.fileUrl.toString())
                    if (myMetaEditor.ui_loadContourCLXml(
                                    openContourCLXMLDialog.fileUrl)) {                       
                        getMap.enabled = true
                        refreshCLDisplayButton.enabled = true
                    } else {
                        messageDialog.text = "Invalid contourCL xml file!"
                        messageDialog.visible = true
                    }
                }
            }

            Button {
                id: getMap
                text: "Display ContourCL Hierarchy"
                enabled: false
                property variant map
                property variant vals
                property variant anatomyDBlandmarks

                onClicked: {
                    objModel.clear()

                    map = contextMetaManager.ui_getContourCLTree()
                    var node = map["Root"]
                    if (map[node] !== null) {
                        objModel.append({
                                            role: qsTr(node),
                                            level: 0,
                                            subNode: [],
                                            childCount: map[node].length,
                                            selected: false
                                        })
                        var parentNode = objModel.get(0)
                        var i = 0
                        for (i; i < map[node].length; i++) {
                            console.log(map[node][i])
                            addNode(parentNode, qsTr(map[node][i]), 1, i)
                        }
                        vals = contextMetaManager.getLandmarkList()
                        predefinedLandmarksList.clear()
                        for (var i = 0; i < vals.length; i++) {
                            predefinedLandmarksList.append({
                                                               role: vals[i]
                                                           })
                        }
                        anatomyDBlandmarks = contextMetaManager.ui_getAnatomyDBLandmarks()
                        anatomyDBLandmarksList.clear()
                        for (var i = 0; i < vals.length; i++) {
                            if (anatomyDBlandmarks[i] === "")
                                continue
                            anatomyDBLandmarksList.append({
                                                              role: anatomyDBlandmarks[i]
                                                          })
                        }
                        generateContoursButton.enabled = true
                        generateFullContoursButton.enabled = true
                    }
                }

                MessageDialog {
                    id: messageDialog
                    title: "Alert"
                    text: ""
                    onAccepted: {

                    }
                    Component.onCompleted: visible = false
                }

                function addNode(parentNode, node, level, position) {
                    //console.log(node)
                    var childCount
                    //console.log("Leaf Node")
                    if (map[node] == null) {
                        console.log("Leaf Node")
                        childCount = 0
                    } else
                        childCount = map[node].length
                    parentNode.subNode.append({
                                                  role: qsTr(node),
                                                  level: level,
                                                  subNode: [],
                                                  childCount: childCount,
                                                  selected: false
                                              })
                    var current = parentNode.subNode.get(position)
                    //console.log(current.role+"\t Level = "+current.level+"\t child count = "+current.childCount)
                    var i = 0
                    for (i; i < childCount; i++) {
                        //console.log(map[node][i])
                        addNode(current, qsTr(map[node][i]), level + 1, i)
                    }
                }
            }

            Rectangle {
                width: 300
                height: 450
                color: "white"

                ListModel {
                    id: objModel
                }

                Component {
                    id: objRecursiveDelegate
                    Column {
                        id: objRecursiveColumn
                        clip: true
                        MouseArea {
                            width: objRow.implicitWidth
                            height: objRow.implicitHeight

                            function selectNode(name, current) {
                                if (current.role == name) {
                                    current.selected = !current.selected
                                    return current
                                } else if (current.childCount == 0)
                                    return null
                                else {
                                    var i = 1
                                    var child
                                    var node
                                    for (i; i <= current.childCount; i++) {
                                        child = current.subNode.get(i - 1)
                                        node = selectNode(name, child)
                                        if (node == null) {
                                            continue
                                        }
                                        if (node.role == name) {
                                            return node
                                        }
                                    }
                                }
                                return null
                            }

                            function deselectAllNodes(current) {
                                current.selected = false

                                var i = 1
                                var child
                                var node
                                for (i; i <= current.childCount; i++) {
                                    child = current.subNode.get(i - 1)
                                    node = deselectAllNodes(child)
                                }

                                return null
                            }

                            onClicked: {
                                for (var i = 1; i < parent.children.length - 1; ++i) {
                                    parent.children[i].visible = !parent.children[i].visible
                                }
                                var name = model.role
                                main.selectedNode = name
                                if (model.level % 2 == 0) {
                                    main.selectedNodeType = "body_region"
                                    tangentMagnitude.visible = false
                                    nodeDescriptionText.text
                                            = contextMetaManager.ui_getBRDescription(
                                                main.selectedNode)
                                    circumference.visible = true
                                    circumference.type = "bodyRegion"
                                    circumInfoView.getList()
                                    ctrlPtLandmarks.visible = true
                                    skin.visible = true
                                    skinTableView.getList()
                                    splineInfo.visible = false
                                    splLandmark.visible = false
                                    //console.log("Skingm : "+contextMetaManager.ui_getSkingm(main.selectedNode))
                                    pointSetView.getList()
                                } else {
                                    main.selectedNodeType = "joint"
                                    tangentMagnitude.visible = true
                                    nodeDescriptionText.text
                                            = contextMetaManager.ui_getJointDescription(
                                                main.selectedNode)
                                    circumference.visible = true
                                    circumference.type = "joint"
                                    circumInfoView.getList()
                                    ctrlPtLandmarks.visible = false
                                    skin.visible = false
                                    splineInfo.visible = true
                                    splLandmark.visible = true
                                    splineInfoView.getList()
                                    splLandmarkListView.getList()
                                }

                                var current = objModel.get(0)
                                var root = objModel.get(0)
                                deselectAllNodes(root)
                                console.log(selectNode(name, current).role)
                            }
                            onDoubleClicked: {

                                //                            console.log(model.role)
                                //                            console.log(model.level )
                                //                            console.log(model.subNode )
                                //                            row.color = "steelblue"
                            }
                            Component.onCompleted: {
                                for (var i = 1; i < parent.children.length - 1; ++i) {
                                    parent.children[i].visible = !parent.children[i].visible
                                }
                            }
                            Rectangle {
                                id: row
                                color: model.selected == true ? "#b3ccff" : "transparent"
                                height: 14
                                width: 300
                                Row {
                                    id: objRow
                                    Item {
                                        height: 1
                                        width: model.level * 20
                                    }
                                    Text {
                                        text: {
                                            (objRecursiveColumn.children.length
                                             > 2 ? objRecursiveColumn.children[1].visible ? qsTr("-  ") : qsTr("+ ") : qsTr("   ")) + model.role
                                        }
                                        color: model.level % 2 == 0 ? "black" : "blue"
                                        //font { bold: true; pixelSize: 14 }
                                    }
                                }
                            }
                        }
                        Repeater {
                            model: subNode
                            delegate: objRecursiveDelegate
                        }
                    }
                }

                ColumnLayout {
                    anchors.fill: parent
                    ListView {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        model: objModel
                        delegate: objRecursiveDelegate
                    }
                }
            }

            GroupBox {
                title: " Description : "
                Text {
                    id: nodeDescriptionText
                    width: 300
                    text: "Please select an element in the tree"
                    wrapMode: Text.WordWrap
                    onTextChanged: {
                        if (text == "") {
                            text = "No description available"
                        }
                    }
                }
            }

            GroupBox {
                ColumnLayout {
                    RowLayout {
                        Label {
                            visible: false
                            text: "Model Opacity"
                        }
                        SpinBox {
                            visible: false
                            id: fullModelOpacity
                            decimals: 2
                            minimumValue: 0.00
                            maximumValue: 1.00
                            stepSize: 0.10
                            value: 1.00
                            onValueChanged: {

                                //  contextMetaManager.setOpacity(fullModelOpacity.value);
                            }
                        }
                    }
                    Button {
                        id: generateContoursButton
                        text: "Generate contours for selected body region/joint"
                        enabled: false
                        onClicked: {
                            if (main.selectedNode !== "")
                                myMetaEditor.ui_showContoursForCLElement(
                                            main.selectedNode,
                                            main.selectedNodeType)
                        }
                    }
                    Button {
                        id: generateFullContoursButton
                        text: "Generate contours for the whole body"
                        enabled: false
                        onClicked: {
                            if (main.selectedNode !== "")
                                myMetaEditor.generateContBusyIndicator(1)
                        }
                    }
                    RowLayout {
                        Label {
                            text: "No. of Contours : "
                        }
                        SpinBox {
                            decimals: 0
                            stepSize: 1
                            value: 8
                            onValueChanged: {

                            }
                        }
                    }
                }
            }
        }

        ColumnLayout {

            anchors.top: main.top

            CollapsibleGroupBox {
                Layout.fillWidth: true
                id: ctrlPtLandmarks
                title: "Control Point Landmarks"
                visible: false
                ColumnLayout {
                    Layout.fillWidth: true
                    GroupBox {
                        id: ctrlPtLandmarksDescription
                        visible: false
                        title: "Description"
                        Text {
                            id: ctrlPtLandmarksDescriptionText
                            text: "Description for control point landmarks "
                        }
                    }

                    RowLayout{
                        GroupBox{
                            Row{

                                Button {
                                    property ModuleToolWindow toolWindow
                                    text: "Change location of selected landmark"
                                    onClicked: {
                                        toolWindow = editorLandmarkWindow
                                        toolWindow.create = pointSetList.get(
                                                    pointSetView.currentRow).landmark
                                        toolWindow.show()
                                    }
                                }

                                Button {
                                    text: "Remove selected landmark"
                                    onClicked: {
                                        console.log("Removing "+pointSetList.get(pointSetView.currentRow).landmark)
                                        contextMetaManager.ui_removeCtrlPtLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                        pointSetView.getList()
                                    }
                                }

                                Button{
                                    text: "Add landmark"
                                    onClicked: {
                                          addLandmarkDialog.func = "ctrlPtLandmark"
                                          addLandmarkDialog.makeComboBoxVisible(false)
                                          addLandmarkDialog.open()
                                    }
                                }
                            }
                        }
                        GroupBox{
                            Row{
                                Button{
                                    text:"Move Up"
                                    onClicked:{
                                        if(pointSetView.currentRow != 0){
                                            console.log(pointSetView.currentRow)
                                            contextMetaManager.ui_moveUpCtrlLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                            pointSetView.getList()
                                            pointSetView.selection.deselect(pointSetView.currentRow)
                                            pointSetView.currentRow -= 1
                                            pointSetView.selection.select(pointSetView.currentRow)
                                        }
                                    }
                                }
                                Button{
                                    text:"Move Down"
                                    onClicked:{
                                         if(pointSetView.currentRow != pointSetView.rowCount-1){
                                             console.log(pointSetView.currentRow)
                                             contextMetaManager.ui_moveDownCtrlLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                             pointSetView.getList()
                                             pointSetView.selection.deselect(pointSetView.currentRow)
                                             pointSetView.currentRow += 1
                                             pointSetView.selection.select(pointSetView.currentRow)

                                         }
                                    }
                                }
                            }
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        ListModel {
                            id: pointSetList
                        }

                        Rectangle {
                            height: 120
                            width: 600
                            TableView {
                                id: pointSetView
                                height: parent.height
                                width: parent.width
                                //clip: true
                                focus: false
                                headerVisible: true
                                selectionMode: SelectionMode.SingleSelection
                                model: pointSetList
                                property variant vals
                                property variant descList
                                property int index: -1

                                function getList() {
                                    pointSetList.clear()
                                    vals = contextMetaManager.ui_getCtrlPtLandmarks(
                                                main.selectedNode)
                                    descList = contextMetaManager.ui_getCplLandmarksDesc(
                                                main.selectedNode)
                                    console.log(" Description list for Cpl landmarks of "
                                                + main.selectedNode
                                                + " ; Length = " + descList.length)
                                    for (var i = 0; i < descList.length; i++)
                                        console.log(descList[i])
                                    var desc
                                    if (vals.length < 2) {
                                        ctrlPtLandmarksDescriptionText.text
                                                = "Please add new control point landmarks"
                                    } else if (vals.length == 2) {
                                        ctrlPtLandmarksDescriptionText.text
                                                = "This is a linear body region "
                                    } else {
                                        ctrlPtLandmarksDescriptionText.text = "The " + vals.length + " control point landmarks are used to create a spline for the body region"
                                    }

                                    for (var i = 0; i < vals.length; i++) {

                                        if (getMap.anatomyDBlandmarks.indexOf(
                                                    vals[i]) != -1)
                                            desc = contextMetaManager.ui_getLandmarkDescriptionFromAnatomyDB(
                                                        vals[i])
                                        else if (i < descList.length)
                                            desc = descList[i]
                                        if (desc == "")
                                            desc = "No description available"
                                        pointSetList.append({
                                                                landmark: vals[i],
                                                                defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined" : "Undefined",
                                                                                                                description: desc
                                                            })
                                    }
                                }

                                function processClickedItem(row) {
                                    index = row
                                    console.log(pointSetList.get(
                                                    index).landmark)
                                }

                                TableViewColumn {
                                    role: "landmark"
                                    title: "Landmarks"
                                    width: 150
                                }
                                TableViewColumn {
                                    role: "defined"
                                    title: "Defined"
                                    width: 60
                                }
                                TableViewColumn {
                                    id: pointSetViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width: 390
                                }

                                onClicked: processClickedItem(row)

                                Component.onCompleted: {

                                }
                            }
                        }
                    }
                }
            }

            CollapsibleGroupBox {
                Layout.fillWidth: true
                id: skin
                title: "Skin Entities"
                visible: false
                ColumnLayout {
                    Layout.fillWidth: true
                    GroupBox {
                        id: skinDesc
                        visible: false
                        title: "Description"
                        Text {
                            id: skinDescriptionText
                            text: ""
                        }
                    }

                    RowLayout{
                        Button {
                            property ModuleToolWindow toolWindow
                            text: "Edit the selected skin entity"
                            onClicked: {
                                toolWindow = editorEntityWindow
                                toolWindow.create = skinList.get(
                                            skinTableView.currentRow).skinEntity
                                toolWindow.show()
                            }
                        }
                        Button {
                            text : "Change skin entity for body region"
                            onClicked: {
                                addEntityDialog.open()
                            }
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        ListModel {
                            id: skinList
                        }

                        Rectangle {
                            height: 60
                            width: 600

                            TableView {
                                id: skinTableView
                                height: parent.height
                                width: parent.width
                                focus: false
                                headerVisible: true
                                selectionMode: SelectionMode.SingleSelection
                                model: skinList
                                property variant vals
                                property variant descList
                                property int index: -1

                                function getList() {
                                    skinList.clear()
                                    var skinEntityName = contextMetaManager.ui_getSkingm(
                                                main.selectedNode)
                                    if (skinEntityName !== "")
                                        skinList.append({
                                                            skinEntity: skinEntityName,
                                                            description: "No description available"
                                                        })
                                    //                                    var val = []
                                    //                                    val.append(contextMetaManager.ui_getSkingm(main.selectedNode))
                                    //                                    vals = val
                                    //                                    var descList = []
                                    //                                    for(var i =0; i<descList.length; i++)
                                    //                                        console.log(descList[i])
                                    //                                    var desc= ""
                                    //                                    for(var i =0; i<vals.length; i++)
                                    //                                    {
                                    //                                       if(desc == "")
                                    //                                             desc = "No description available"
                                    //                                        skinList.append({skinEntity: vals[i] ,  description : desc })
                                    //                                    }
                                }

                                function processClickedItem(row) {
                                    index = row
                                    console.log(skinList.get(index).skinEntity)
                                }

                                TableViewColumn {
                                    role: "skinEntity"
                                    title: "Skin Entity"
                                    width: 120
                                }
                                TableViewColumn {
                                    id: skinDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width: 480
                                }

                                onClicked: processClickedItem(row)

                                Component.onCompleted: {

                                }
                            }
                        }
                    }
                }
            }

            CollapsibleGroupBox {
                Layout.fillWidth: true
                id: splineInfo
                title: "Spline information"
                visible: false
                ColumnLayout {
                    Layout.fillWidth: true


                    RowLayout{
                        GroupBox{
                            Row{
                                Button {
                                    property ModuleToolWindow toolWindow
                                    text: "Change location of selected landmark"
                                    onClicked: {
                                        toolWindow = editorLandmarkWindow
                                        toolWindow.create = splineInfoList.get(
                                                    splineInfoView.currentRow).spline
                                        toolWindow.show()
                                    }
                                }
                                Button {
                                    text: "Remove selected landmark"
                                    onClicked: {
                                        console.log("Removing "+splineInfoList.get(splineInfoView.currentRow).spline)
                                        contextMetaManager.ui_removeSplineInfo( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                        splineInfoView.getList()
                                    }
                                }
                                Button{
                                    id:addSplineInfoButton
                                    text: "Add landmark "
                                    onClicked: {
                                        addLandmarkDialog.func = "spline"
                                        addLandmarkDialog.makeComboBoxVisible(false)
                                        addLandmarkDialog.open()
                                    }
                                }
                            }
                        }
                        GroupBox{
                            Row{
                                Button{
                                    text:"Move Up"
                                    onClicked:{
                                        if(splineInfoView.currentRow != 0){
                                            console.log(splineInfoView.currentRow)
                                            contextMetaManager.ui_moveUpSplineLandmark( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                            splineInfoView.getList()
                                            splineInfoView.selection.deselect(splineInfoView.currentRow)
                                            splineInfoView.currentRow -= 1
                                            splineInfoView.selection.select(splineInfoView.currentRow)
                                        }
                                    }
                                }
                                Button{
                                    text:"Move Down"
                                    onClicked:{
                                         if(splineInfoView.currentRow != splineInfoView.rowCount-1){
                                             console.log(splineInfoView.currentRow)
                                             contextMetaManager.ui_moveDownSplineLandmark( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                             splineInfoView.getList()
                                             splineInfoView.selection.deselect(splineInfoView.currentRow)
                                             splineInfoView.currentRow += 1
                                             splineInfoView.selection.select(splineInfoView.currentRow)

                                         }
                                    }
                                }
                            }
                        }
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        ListModel {
                            id: splineInfoList
                        }

                        Rectangle {
                            height: 120
                            width: 600
                            TableView {
                                id: splineInfoView
                                height: parent.height
                                width: parent.width
                                focus: false
                                headerVisible: true
                                selectionMode: SelectionMode.SingleSelection
                                model: splineInfoList
                                property variant vals
                                property int index

                                function getList() {
                                    splineInfoList.clear()
                                    vals = contextMetaManager.ui_getSplineInfo(
                                                main.selectedNode)
                                    var type = ["p0", "p1", "t1", "t2"]
                                    var desc = contextMetaManager.ui_getSplineDesc(
                                                main.selectedNode)
                                    for (var i = 0; i < vals.length; i++) {
                                        if (desc[i] == "")
                                            desc = "No description available"
                                        splineInfoList.append({
                                                                  type: type[i],
                                                                  spline: vals[i],
                                                                  defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined" : "Undefined",
                                                                                                                  description: desc[i]
                                                              })
                                    }
                                }

                                function processClickedItem(row) {
                                    index = row
                                }

                                TableViewColumn {
                                    id: type
                                    role: "type"
                                    title: " "
                                    resizable: false
                                    width: 30
                                }

                                TableViewColumn {
                                    role: "spline"
                                    title: "Spline Info"
                                    width: 110
                                }
                                TableViewColumn {
                                    role: "defined"
                                    title: "Defined"
                                    width: 60
                                }
                                TableViewColumn {
                                    id: splineInfoViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width: 400
                                }

                                onClicked: processClickedItem(row)

                                Component.onCompleted: {

                                }
                            }
                        }
                    }
                }
            }

            CollapsibleGroupBox {
                Layout.fillWidth: true
                id: tangentMagnitude
                title: "Tangent information"
                visible: false
                ColumnLayout {
                    Layout.fillWidth: true

                    GroupBox {
                        ColumnLayout {
                            RowLayout {
                                Label {
                                    text: "Tangent 1 magnitude  : "
                                }
                                SpinBox {
                                    decimals: 0
                                    stepSize: 1
                                    value: 8
                                    onValueChanged: {

                                    }
                                }
                            }
                            RowLayout {
                                Label {
                                    text: "Tangent 2 magnitude  : "
                                }
                                SpinBox {
                                    decimals: 0
                                    stepSize: 1
                                    value: 8
                                    onValueChanged: {

                                    }
                                }
                            }
                        }
                    }
                }
            }

                CollapsibleGroupBox {
                    Layout.fillWidth: true
                    id: splLandmark
                    visible: false
                    //title: "Special Landmarks"
                    title: "Axes landmarks "
                    ColumnLayout {

                        Layout.fillWidth: true

                        RowLayout{
                            GroupBox{
                                Row{

                                    Button {
                                        property ModuleToolWindow toolWindow
                                        text: "Change location of selected landmark"
                                        onClicked: {
                                            toolWindow = editorLandmarkWindow
                                            toolWindow.create = splLandmarkList.get(
                                                        splLandmarkListView.currentRow).spl_landmark
                                            toolWindow.show()
                                        }
                                    }
                                    Button {
                                        text: "Remove selected landmark"
                                        onClicked: {
                                            console.log("Removing "+splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark)
                                            if (splLandmarkList.get(splLandmarkListView.currentRow).type === "Flexion")
                                                contextMetaManager.ui_removeFlexionAxesLandmarks( main.selectedNode, splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark  )
                                            else if (splLandmarkList.get(splLandmarkListView.currentRow).type === "Twisting")
                                                contextMetaManager.ui_removeTwistAxesLandmarks( main.selectedNode, splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark  )
                                            splLandmarkListView.getList()
                                        }
                                    }
                                    Button{
                                        id:addSplLandmarkButton
                                        text: "Add landmark "
                                        onClicked: {
                                              addLandmarkDialog.func = "axis"
                                              addLandmarkDialog.makeComboBoxVisible(true)
                                              addLandmarkDialog.open()
                                        }
                                    }
                                }
                            }
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            ListModel {
                                id: splLandmarkList
                            }

                            Rectangle {
                                height: 120
                                width: 600

                                TableView {
                                    id: splLandmarkListView
                                    width: parent.width
                                    height: parent.height
                                    focus: false
                                    headerVisible: true
                                    selectionMode: SelectionMode.SingleSelection
                                    model: splLandmarkList
                                    property variant vals
                                    property variant descList
                                    property int index

                                    function getList() {
                                        splLandmarkList.clear()
                                        vals = contextMetaManager.ui_getFlexionAxesLandmarks(
                                                    main.selectedNode)
                                        descList = contextMetaManager.ui_getFlexionAxesLandmarksDesc(
                                                    main.selectedNode)
                                        for (var i = 0; i < vals.length; i++) {
                                            var desc = descList[i]
                                            if (desc == "")
                                                desc = "No description available "
                                            splLandmarkList.append({
                                                                       spl_landmark: vals[i],
                                                                       type: "Flexion",
                                                                       defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined" : "Undefined",
                                                                                                                       description: desc
                                                                   })
                                        }
                                        vals = []
                                        vals = contextMetaManager.ui_getTwistAxesLandmarks(
                                                    main.selectedNode)
                                        descList = contextMetaManager.ui_getTwistAxesLandmarksDesc(
                                                    main.selectedNode)
                                        for (var i = 0; i < vals.length; i++) {
                                            var desc = descList[i]
                                            if (desc == "")
                                                desc = "No description available "
                                            splLandmarkList.append({
                                                                       spl_landmark: vals[i],
                                                                       type: "Twisting",
                                                                       defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined" : "Undefined",
                                                                                                                       description: desc
                                                                   })
                                        }
                                    }

                                    function processClickedItem(row) {
                                        index = row
                                    }

                                    TableViewColumn {
                                        role: "spl_landmark"
                                        //title: "SpecialLandmarks"
                                        title: "Flexion axes landmarks"
                                        width: 120
                                    }
                                    TableViewColumn {
                                        role: "type"
                                        title: "Type"
                                        width: 70
                                    }
                                    TableViewColumn {
                                        role: "defined"
                                        title: "Defined"
                                        width: 60
                                    }
                                    TableViewColumn {
                                        id: splLandmarkViewDescription
                                        visible: true
                                        role: "description"
                                        title: "Description"
                                        width: 350
                                    }

                                    onClicked: processClickedItem(row)
                                }
                            }
                        }
                    }
                }

                CollapsibleGroupBox {
                    Layout.fillWidth: true
                    width: parent.width
                    id: circumference
                    title: "Circumference Location Landmark"
                    visible: false

                    property string type: "bodyRegion"

                    function getJointCircumference() {
                        circumValue.text = contextMetaManager.ui_getJointCircumference(
                                    main.selectedNode)
                        circumValue.color = "black"
                        if (circumValue.text == "") {
                            circumValue.text = " NO VALUE DEFINED "
                            circumValue.color = "red"
                        }
                        circumference.type = "joint"
                    }

                    ColumnLayout {

                        Layout.fillWidth: true



                        RowLayout{
                            GroupBox{
                                Row{
                                    Button {
                                        property ModuleToolWindow toolWindow
                                        text: "Change location of selected landmark"
                                        onClicked: {
                                            toolWindow = editorLandmarkWindow
                                            toolWindow.create = circumInfoList.get(
                                                        circumInfoView.currentRow).circumference
                                            toolWindow.show()
                                        }
                                    }
                                    Button {
                                        text: "Remove selected landmark"
                                        onClicked: {
                                            console.log("Removing "+circumInfoList.get(circumInfoView.currentRow).circumference)
                                            if(circumference.type === "bodyRegion")
                                                contextMetaManager.ui_removeBRCircumference( main.selectedNode, circumInfoList.get(circumInfoView.currentRow).circumference  )
                                            else
                                                contextMetaManager.ui_removeJointCircumference( main.selectedNode, circumInfoList.get(circumInfoView.currentRow).circumference  )
                                            circumInfoView.getList()
                                        }
                                    }
                                    Button{
                                        text: "Add landmark "
                                        onClicked: {
                                              addLandmarkDialog.func = "circum"
                                              addLandmarkDialog.makeComboBoxVisible(false)
                                              addLandmarkDialog.open()
                                        }
                                    }
                                }
                            }
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            ListModel {
                                id: circumInfoList
                            }

                            Rectangle {
                                height: 60
                                width: 600

                                TableView {
                                    id: circumInfoView
                                    height: parent.height
                                    width: parent.width
                                    focus: false
                                    headerVisible: true
                                    selectionMode: SelectionMode.SingleSelection
                                    model: circumInfoList
                                    property variant vals
                                    property variant descList
                                    property int index

                                    function getList() {
                                        circumInfoList.clear()
                                        if (circumference.type == "bodyRegion") {
                                            vals = contextMetaManager.ui_getBRCircumference(
                                                        main.selectedNode)
                                            descList = contextMetaManager.ui_getBRCircumDesc(
                                                        main.selectedNode)
                                        } else {
                                            vals = contextMetaManager.ui_getJointCircumference(
                                                        main.selectedNode)
                                            descList = contextMetaManager.ui_getJointCircumDesc(
                                                        main.selectedNode)
                                        }

                                        for (var i = 0; i < vals.length; i++) {
                                            var desc
                                            if (getMap.anatomyDBlandmarks.indexOf(
                                                        vals[i]) != -1)
                                                desc = contextMetaManager.ui_getLandmarkDescriptionFromAnatomyDB(
                                                            vals[i])
                                            else if (i < descList.length)
                                                desc = descList[i]
                                            if (desc == "")
                                                desc = "No description available"
                                            circumInfoList.append({
                                                                      circumference: vals[i],
                                                                      defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined" : "Undefined",
                                                                                                                      description: desc
                                                                  })
                                        }
                                    }

                                    function processClickedItem(row) {
                                        index = row
                                    }

                                    TableViewColumn {
                                        role: "circumference"
                                        title: "Circumference Location Landmarks"
                                        width: 120
                                    }
                                    TableViewColumn {
                                        role: "defined"
                                        title: "Defined"
                                        width: 60
                                    }
                                    TableViewColumn {
                                        id: circumInfoViewDescription
                                        visible: true
                                        role: "description"
                                        title: "Description"
                                        width: 420
                                    }

                                    onClicked: processClickedItem(row)

                                    Component.onCompleted: {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        EditorEntityWindow {
            id: editorEntityWindow
        }
        EditorLandmarkWindow {
            id: editorLandmarkWindow
        }
        EditorGenericMetadataWindow {
            id: editorGenericMetadataWindow
        }

        Dialog {
            visible: false
            width: 400
            id: addEntityDialog

            contentItem:  ColumnLayout{
                ListModel
                {
                    id:list_Model1
                }
                TextField{
                    id: searchField
                    placeholderText: qsTr("Search")
                    Layout.fillWidth: true
                    onTextChanged: {
                        entityTableView.getList(searchField.text)
                    }
                }
                TableView
                {
                    id: entityTableView
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible:true
                    selectionMode:SelectionMode.SingleSelections
                    model: list_Model1
                    property variant vals;
                    function getList(query) {
                        list_Model1.clear()
                        vals = contextMetaManager.getEntityList()
                        for (var i = 0; i < vals.length; i++) {
                            if (!query) {
                                list_Model1.append({ role: vals[i]})
                            } else {
                                if (vals[i].toUpperCase().indexOf(
                                            searchField.text.toUpperCase(
                                                )) !== -1) {
                                    list_Model1.append({ role: vals[i]})
                                }
                            }
                        }
                    }
                    TableViewColumn
                    {
                        role: "role"
                        title: "Entities"
                    }
                    Component.onCompleted:
                    {
                        getList();
                    }
                    Connections
                    {
                        target: context
                        onVisDataLoaded:
                        {
                            entityTableView.getList();
                        }
                    }
                    Connections {
                        target: context
                        onMetadataChanged:  {
                            entityTableView.getList()
                        }
                    }
                }
                Button{
                    text: "Change entity"
                    Layout.fillWidth: true
                    onClicked: {
                        contextMetaManager.ui_setSkingm( main.selectedNode ,list_Model1.get(entityTableView.currentRow).role)
                        skinTableView.getList()
                        addEntityDialog.close()
                    }
                }
            }
        }

        Dialog {
            visible: false
            width: 400
            id: addLandmarkDialog
            title: "Select Landmark"

            property string func: ""

            function makeComboBoxVisible(val){
                axisComboBox.visible  = val
            }

            contentItem:  ColumnLayout{

                ComboBox {
                    width: 400
                    id : axisComboBox
                    visible: false
                    model: [ "Flexion/Extention", "Twisting"]
                }

                TextField{
                    id: searchTextField
                    placeholderText: qsTr("Search")
                    Layout.fillWidth: true
                    onTextChanged: {
                        tableView.getList(searchTextField.text)
                    }
                }

                ListModel{
                    id:list_Model
                }

                TableView {
                    id: tableView
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible:true
                    selectionMode:SelectionMode.SingleSelection
                    model: list_Model
                    property variant vals;
                    property variant types;

                    function getList(query) {
                        list_Model.clear()
                        vals = contextMetaManager.getLandmarkList()
                        types = contextMetaManager.getLandmarkTypeList()
                        for (var i = 0; i < vals.length; i++) {
                            if (!query) {
                                list_Model.append({
                                                     role: vals[i],
                                                     type: types[i]
                                                 })
                            } else {
                                if (vals[i].toUpperCase().indexOf(
                                            searchTextField.text.toUpperCase(
                                                )) !== -1) {
                                    list_Model.append({
                                                         role: vals[i],
                                                         type: types[i]
                                                     })
                                }
                            }
                        }
                    }
                    TableViewColumn
                    {
                        role: "role"
                        title: "Landmarks"
                        width: 300
                    }
                    TableViewColumn
                    {
                        role: "type"
                        title: "Type"
                        width: 100
                    }
                    Component.onCompleted:{
                        getList();
                    }
                    Connections{
                        target: context
                        onVisDataLoaded:
                        {
                            tableView.getList();
                        }
                        onMetadataChanged:  {
                            tableView.getList()
                        }
                    }
                }

                Button{
                    text: "Add selected landmark"
                    Layout.fillWidth: true
                    onClicked: {
                        if( tableView.selection.count !== 0 ){
                            switch(addLandmarkDialog.func){
                                case "ctrlPtLandmark" : {
                                    contextMetaManager.ui_addCtrlPtLandmark( main.selectedNode ,list_Model.get(tableView.currentRow).role , "")
                                    pointSetView.getList()
                                    break;
                                }
                                case "spline" : {
                                    contextMetaManager.ui_addSplineInfo( main.selectedNode ,list_Model.get(tableView.currentRow).role)
                                    splineInfoView.getList()
                                    break;
                                }
                                case "axis" : {
                                    if(axisComboBox.currentIndex === 0)
                                        contextMetaManager.ui_addFlexionAxesLandmark( main.selectedNode ,list_Model.get(tableView.currentRow).role )
                                    else if (axisComboBox.currentIndex === 1)
                                        contextMetaManager.ui_addTwistAxesLandmark( main.selectedNode ,list_Model.get(tableView.currentRow).role )
                                    splLandmarkListView.getList()
                                    axisComboBox.visible = false
                                    break;
                                }
                                case "circum" : {
                                    if (circumference.type == "bodyRegion")
                                        contextMetaManager.ui_addBRCircumference( main.selectedNode ,list_Model.get(tableView.currentRow).role )
                                    else
                                        contextMetaManager.ui_addJointCircumference( main.selectedNode ,list_Model.get(tableView.currentRow).role )
                                    circumInfoView.getList()
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

