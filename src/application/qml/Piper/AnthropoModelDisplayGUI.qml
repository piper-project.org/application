// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5

import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import QtQml.Models 2.2

import Piper 1.0

import piper.AnthropoModelDisplay 1.0

GroupBox {
    property AnthropoModelDisplay anthropoModelDisplay

    title: "Display"

    function refresh() {
        anthropoModelDisplay.displaySourceSegment(displaySourceSegment.checked, false)
        anthropoModelDisplay.displaySourceSection(displaySourceSection.checked, false)
        anthropoModelDisplay.displaySourceControlPointsSegment(displaySourceControlPoints.checked && displaySourceSegment.checked, false)
        anthropoModelDisplay.displaySourceControlPointsSection(displaySourceControlPoints.checked && displaySourceSection.checked, false)
        anthropoModelDisplay.displayTargetSegment(displayTargetSegment.checked, false)
        anthropoModelDisplay.displayTargetSection(displayTargetSection.checked, false)
        anthropoModelDisplay.displayTargetControlPointsSegment(displayTargetControlPoints.checked && displayTargetSegment.checked, false)
        anthropoModelDisplay.displayTargetControlPointsSection(displayTargetControlPoints.checked && displayTargetSection.checked, false)
        anthropoModelDisplay.displayFreeLandmarksControlPoints((displaySourceControlPoints.checked && (displaySourceSegment.checked || displaySourceSection.checked)),(displayTargetControlPoints.checked && (displayTargetSegment.checked || displayTargetSection.checked)), false)
        anthropoModelDisplay.updateHighlight(true)
    }

    function clearAnthropoModelDisplay() {
        anthropoModelDisplay.clearAnthropoModelDisplay(false)
        refresh()
    }

    ColumnLayout {
        anchors.margins : itemMargin
		/*
        RowLayout {
            anchors.margins : itemMargin
            GroupBox {
                title: "Source"
                ColumnLayout {
                    anchors.margins : itemMargin
                    CheckBox {
                        id: displaySourceSection
                        text: qsTr("Body Sections")
                        Layout.fillWidth: true
                        checked: true
                        onClicked: {
                            refresh()
                        }
                    }
                    CheckBox {
                        id: displaySourceSegment
                        text: qsTr("Body Segments")
                        Layout.fillWidth: true
                        checked: true
                        onClicked: {
                            refresh()
                        }
                    }
                    CheckBox {
                        id: displaySourceControlPoints
                        text: qsTr("Control Points")
                        Layout.fillWidth: true
                        checked: true
                        onClicked: {
                            refresh()
                        }
                    }
                }
            }
            GroupBox {
                title: "Target"
                ColumnLayout {
                    anchors.margins : itemMargin
                    CheckBox {
                        id: displayTargetSection
                        text: qsTr("Body sections")
                        Layout.fillWidth: true
                        checked: false
                        onClicked: {
                            refresh()
                        }
                    }
                    CheckBox {
                        id: displayTargetSegment
                        text: qsTr("Body Segments")
                        Layout.fillWidth: true
                        checked: false
                        onClicked: {
                            refresh()
                        }
                    }
                    CheckBox {
                        id: displayTargetControlPoints
                        text: qsTr("Control Points")
                        Layout.fillWidth: true
                        checked: false
                        onClicked: {
                            refresh()
                        }
                    }
                }
            }
        }
		*/
		RowLayout
		{
			ColumnLayout
			{
				Label
				{
					text: "Body Sections"
					Layout.row: 0
				}
				Label
				{
					text: "Body Segments"
					Layout.row: 1
				}
				Label
				{
					text: "Control Points"
					Layout.row: 2
				}
			}
			ColumnLayout
			{
				GroupBox
				{
					title: "Source"
					flat: true
					ColumnLayout 
					{
						anchors.margins : itemMargin
						CheckBox 
						{
							id: displaySourceSection
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
						CheckBox 
						{
							id: displaySourceSegment
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
						CheckBox 
						{
							id: displaySourceControlPoints
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
					}
				}
			}
			ColumnLayout
			{
				GroupBox
				{
					title: "Target"
					flat: true
					ColumnLayout 
					{
						anchors.margins : itemMargin
						CheckBox 
						{
							id: displayTargetSection
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
						CheckBox 
						{
							id: displayTargetSegment
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
						CheckBox 
						{
							id: displayTargetControlPoints
							Layout.row: 0
							checked: true
							onClicked: {
								refresh()
							}
						}
					}
				}
			}
		}
    }

}
