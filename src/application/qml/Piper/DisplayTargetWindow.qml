// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Target Display Options"

    //Component.onCompleted: setCurrentWindowSizeFixed()
    ColumnLayout
    {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop
        anchors.centerIn : parent
        anchors.fill : parent
        RowLayout
        {
            width:childrenRect.width
            Button
            {
                text: "none"
                tooltip: "clear selection"
                onClicked:
                {
                    tableView.selection.clear();
                    noneSelection();
                }
            }
            Button
            {
                text: "all"
                tooltip: "select all"
                onClicked:
                {
                    allSelection();
                }
            }
            Button
            {
                text: "invert"
                tooltip: "invert selection"
                onClicked:
                {
                    invertSelection()
                }
            }
        }
        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.MultiSelection
            model: listModel
            onRowCountChanged:          //Every time the nb of elements in the list change
            {
                allSelection()
            }

            function getList()
            {
                listModel.clear();
                var vals = contextMetaManager.getTargetList();
                var types = contextMetaManager.getTargetTypeList();
                for(var i =0; i<vals.length; i++)
                {
                    listModel.append({target: vals[i], type: types[i]});
                }
            }
            TableViewColumn
            {
                role: "target"
                title: "Targets"
                width:200
            }
            TableViewColumn
            {
                role: "type"
                title: "Types"
                width:100
            }

            Connections
            {
                target: tableView.selection
                onSelectionChanged :
                {
                    var listNames = [];
                    var listVisible = [];
                    var listType = [];
                    for (var i = 0; i < tableView.model.count; i++)
                    {
                        listNames.push(tableView.model.get(i).target);
                        listType.push(tableView.model.get(i).type);
                        listVisible.push(tableView.selection.contains(i));
                    }
                    contextMetaManager.targetDisplayList(listNames, listVisible, listType);
                }
            }

            Component.onCompleted:
            {
                getList();
                allSelection();
            }
            Connections
            {
                target: context
                onVisDataLoaded:
                {
                    tableView.getList();
                }
                onTargetChanged: {
                    tableView.getList();
                }
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeMinimum();
    }

    function invertSelection()
    {
        var listNames = [];
        var listVisible = [];
        var listType = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).target);
            listType.push(tableView.model.get(i).type);
            if (tableView.selection.contains(i))
            {
                listVisible.push(false);
                tableView.selection.deselect(i)
            }
            else
            {
                listVisible.push(true);
                tableView.selection.select(i)
            }
        }
        contextMetaManager.targetDisplayList(listNames, listVisible, listType);
    }

    function noneSelection()
    {
        tableView.selection.clear();
        var listNames = [];
        var listVisible = [];
        var listType = [];
        for (var i = 0; i < tableView.model.count; i++)
        {
            listNames.push(tableView.model.get(i).role);
            listType.push(tableView.model.get(i).type);
            listVisible.push(false);
        }
        contextMetaManager.targetDisplayList(listNames, listVisible, listType);
    }

    function allSelection()
    {
        var listNames = [];
        var listVisible = [];
        var listType = [];
        if (tableView.model.count > 0)
        {
            tableView.selection.selectAll();
            tableView.focus = true;
            for (var i = 0; i < tableView.model.count; i++)
            {
                listNames.push(tableView.model.get(i).role);
                listType.push(tableView.model.get(i).type);
                listVisible.push(true);
            }
        }
        contextMetaManager.targetDisplayList(listNames, listVisible, listType);
    }
}
