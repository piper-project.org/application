// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import Piper 1.0

ModuleToolWindow {
    id: mainWindow
    title: "Control Point Editor"

    minimumHeight: 460
    minimumWidth: 400

    property var controlPointList: []
    property string lmrkType: ""

    property string create: ""

    property int oldIndex: 0

    onCreateChanged: {
        console.log("Create changed : " + create)
        var vals = contextMetaManager.getControlPointList()
        if (create !== "") {
            controlPointName.text = create
            var found = false
            for (var i = 0; i < vals.length; i++)
                if (vals[i] === create) {
                    found = true
                    break
                }
            console.log("Create found : " + found)
            if (found == true) {
                editControlPointRadioButton.checked = true
                searchTextField.text = create
                var type = contextMetaManager.getTypeOfControlPoint(create)
                if (type === "POINT") {
                    controlPointType.currentIndex = 2
                    moveControlPoint.enabled = true
                } else if (type === "BARYCENTER") {
                    controlPointType.currentIndex = 1
                    moveControlPoint.enabled = false
                } else if (type === "SPHERE") {
                    controlPointType.currentIndex = 0
                    moveControlPoint.enabled = false
                }
            } else {
                createControlPointRadioButton.checked = true
            }
        }
    }

    MessageDialog {
        id: messageDialog
        visible: false
        title: "Alert"
        text: " text for message dialog "
        onAccepted: {

        }
    }

    MessageDialog {
        id: dialogRemove
        title: "Remove?"
        icon: StandardIcon.Question
        text: "Are sure you want to remove the selected Control Point?"
        standardButtons: StandardButton.Yes | StandardButton.No
        visible: false
        onYes: {
            for (var i = 0; i < listModel.count; i++) {
                if (tableView.selection.contains(i)) {
                    myMetaEditor.removeControlPoint(listModel.get(i).role)
                }
            }
            tableView.selection.clear()
            updateControlPoint.enabled = false
            removeControlPoint.enabled = false
        }
    }

    MessageDialog {
        id: dialogControlPointChange
        title: "Change ControlPoint type?"
        icon: StandardIcon.Question
        text: "Are sure you want to change the controlPoint type?"
        detailedText: "Changing controlPoint type would change the contraints involved.\n"
        standardButtons: StandardButton.Yes | StandardButton.No
        visible: false
        onYes: {
            switch (controlPointType.currentIndex) {
            case 0:
                controlPointTypeHint.text = "Select atleast 5 nodes."
                break
            case 1:
                controlPointTypeHint.text = "Select atleast 1 node."
                break
            case 2:
                controlPointTypeHint.text = "Select only 1 node."
                breakription.message = "A Centroid of section"
                break
            }
            oldIndex = controlPointType.currentIndex
        }
        onNo: {
            controlPointType.currentIndex = oldIndex
        }
    }

    ColumnLayout {
        anchors {
            top: title.bottom
            left: parent.left
            right: parent.right
        }
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        ListModel {
            id: listModel
        }

        GroupBox {
            title: "Action"

            RowLayout {
                ExclusiveGroup {
                    id: tabPositionGroup
                }
                RadioButton {
                    text: "Create ControlPoint"
                    id: createControlPointRadioButton
                    checked: true
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if (checked === true) {
                            controlPointName.visible = true
                            searchTextField.visible = false
                            tableView.visible = false
                            typeSelection.visible = true
                            controlPointTypeHint.visible = true
                            pickersColumn.visible = true
                            addControlPoint.visible = true
                            updateControlPoint.visible = false
                            removeControlPoint.visible = false
                            copyControlPoint.visible = false
                            copyControlPointLabel.visible = false
                            copyControlPointName.visible = false
                        }
                    }
                }
                RadioButton {
                    text: "Edit ControlPoint"
                    id: editControlPointRadioButton
                    exclusiveGroup: tabPositionGroup
                    onCheckedChanged: {
                        if (checked === true) {
                            controlPointName.visible = false
                            searchTextField.visible = true
                            tableView.visible = true
                            typeSelection.visible = true
                            controlPointTypeHint.visible = true
                            pickersColumn.visible = true
                            addControlPoint.visible = false
                            updateControlPoint.visible = true
                            removeControlPoint.visible = true
                            copyControlPoint.visible = false
                            copyControlPointLabel.visible = false
                            copyControlPointName.visible = false
                        }
                    }
                }
            }
        }

        RowLayout {

            Label {
                id: copyControlPointLabel
                text: "ControlPoint to be copied : "
                visible: false
                onVisibleChanged: {
                    controlPointLabel.visible = copyControlPointLabel.visible
                }
            }
            TextField {
                id: copyControlPointName
                placeholderText: "Select a controlPoint from the table"
                visible: false
                readOnly: true
                Layout.fillWidth: true
            }
        }

        RowLayout {

            Label {
                id: controlPointLabel
                text: "New ControlPoint name : "
                visible: false
            }

            TextField {
                id: controlPointName
                placeholderText: qsTr("Enter New ControlPoint Name")
                Layout.fillWidth: true

                onTextChanged: {

                    if (controlPointName.text.length <= 0) {
                        addControlPoint.enabled = false
                        buttonHint.visible = false
                        pickersColumn.enabled = false
                        copyControlPoint.enabled = false
                    } else {
                        if(createControlPointRadioButton.checked){
                            addControlPoint.enabled = true
                            buttonHint.visible = false
                            pickersColumn.enabled = true
                        }else{
                            pickersColumn.enabled = true
                            for (var i = 0; i < tableView.model.count; i++) {
                                if (controlPointName.text.toUpperCase(
                                            ) === tableView.model.get(
                                            i).role.toUpperCase()) {
                                    addControlPoint.enabled = false
                                    copyControlPoint.enabled = false
                                    buttonHint.visible = false
                                    tableView.selection.select(i)
                                    break
                                } else {
                                    addControlPoint.enabled = true
                                    buttonHint.visible = false
                                    if (tableView.selection.count > 0) {
                                        copyControlPoint.enabled = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        TextField {
            id: searchTextField
            placeholderText: qsTr("Search")
            Layout.row: 1
            Layout.column: 0
            Layout.fillWidth: true
            visible: false
            onTextChanged: {
                tableView.getList(searchTextField.text)
            }
        }

        TableView {
            id: tableView
            alternatingRowColors: false
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            model: listModel
            visible: false

            property variant vals
            property variant types
            property variant numbers

            function processClickedItem(index) {

                if (tableView.selection.contains(index)) {
                    updateControlPoint.enabled = true
                    removeControlPoint.enabled = true
                } else {
                    updateControlPoint.enabled = false
                    removeControlPoint.enabled = false
                }
            }

            TableViewColumn {
                id: ccRole
                role: "role"
                title: "ControlPoints"
                width: 230
            }
            TableViewColumn {
                id: ccType
                role: "type"
                title: "Role"
                width: 80
            }
            TableViewColumn
            {
                id: ccNumber
                role: "number"
                title: "Number"
                width: 80
            }

            onClicked: processClickedItem(row)

            function getList(query) {
                listModel.clear()
                vals = contextMetaManager.getControlPointList()
                types = contextMetaManager.getControlPointRoleList()
                numbers = contextMetaManager.getControlPointNumberList()

                for (var i = 0; i < vals.length; i++) {
                    if (!query) {
                        listModel.append({
                                             role: vals[i],
                                             type: types[i],
                                             number: numbers[i]
                                         })
                    } else {
                        if (vals[i].toUpperCase().indexOf(
                                    searchTextField.text.toUpperCase(
                                        )) !== -1) {
                            listModel.append({
                                                 role: vals[i],
                                                 type: types[i],
                                                 number: numbers[i]
                                             })
                        }
                    }
                }
            }
            Connections {
                target: context
                onMetadataChanged:  {
                    tableView.getList()
                }
            }
        }

        RowLayout {
            id: typeSelection
            Layout.fillWidth: true
            Label {
                text: "Select Role : "
            }
            ComboBox {
                id: controlPointType
                Layout.fillWidth: true
                currentIndex: -1
                model: ["NONE", "SOURCE", "TARGET"]
            }

            Button {
                visible: false
                id: typeDescription
                property string message: ""
                iconSource: "qrc:///icon/information-icon.png"
                onClicked: {
                    messageDialog.text = typeDescription.message
                    messageDialog.visible = true
                }
            }
        }
        Label {
            id: controlPointTypeHint
            text: ""
            font.pixelSize: 10
            color: "red"
        }

        ExclusiveGroup {
            id: group
        }
        Label {
            id: hintLabel
            visible: false
        }
        RowLayout {
            ColumnLayout {
                GroupBox {
                    title: "Pickers"
                    id: pickersColumn
                    enabled: false
                    ColumnLayout {
                        RowLayout {
                            Layout.fillWidth: true
                            GridLayout {
                                id: pickerButtons
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Single Pick"
                                    Layout.row: 0
                                    Layout.column: 1
                                }
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Rubber Band"
                                    Layout.row: 0
                                    Layout.column: 2
                                }
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Box"
                                    Layout.row: 0
                                    Layout.column: 3
                                }
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Move"
                                    Layout.row: 0
                                    Layout.column: 4
                                }
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Node"
                                    Layout.row: 1
                                    Layout.column: 0
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 1
                                    Layout.column: 1
                                    tooltip: "Node Picker"
                                    checkable: true
                                    iconSource: "qrc:///icon/picker-node.png"
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            checked = true
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        0)
                                        } else
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                    }
                                }
                                Button {
                                    id: boxNodePickerButton
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 1
                                    Layout.column: 3
                                    tooltip: "Box Node Picker"
                                    checkable: true
                                    iconSource: "qrc:///icon/box-node.png"
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            checked = true
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        7)
                                        } else
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                    }
                                }
                                Button {
                                    id: moveControlPoint
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 1
                                    Layout.column: 4
                                    iconSource: "qrc:///icon/drag-control-point.png"
                                    tooltip: "Move ControlPoint"
                                    checkable: true
                                    enabled: false
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        9)
                                            checked = true
                                            moveControlPointOptions.visible = true
                                        } else {
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                            moveControlPointOptions.visible = false
                                        }
                                    }
                                }
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Element"
                                    Layout.row: 2
                                    Layout.column: 0
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 2
                                    Layout.column: 1
                                    tooltip: "Element Picker"
                                    checkable: true
                                    iconSource: "qrc:///icon/picker-element.png"
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        2)
                                            checked = true
                                        } else
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                    }
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 2
                                    Layout.column: 2
                                    tooltip: "Rubberband Element Picker"
                                    checkable: true
                                    iconSource: "qrc:///icon/rubberband-element.png"
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        5)
                                            checked = true
                                        } else
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                    }
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 2
                                    Layout.column: 3
                                    tooltip: "Box Element Picker"
                                    checkable: true
                                    iconSource: "qrc:///icon/box-element.png"
                                    onClicked: {
                                        if (checked) {
                                            pickerButtons.uncheckPickerButtons()
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        8)
                                            checked = true
                                            selInsideBoxButton.enabled = true
                                            deselInsideBoxButton.enabled = true
                                        } else
                                            hintLabel.text = contextVtkDisplay.SetPickingType(
                                                        -1)
                                    }
                                }
                                // uncheck all the buttons in the pickerButtons panel
                                function uncheckPickerButtons() {
                                    contextVtkDisplay.RemoveNonpersistentActors(
                                                )
                                    for (var i = 0; i < pickerButtons.children.length; i++) {
                                        if (pickerButtons.children[i].checkable)
                                            pickerButtons.children[i].checked = false
                                    }
                                }
                            }
                        }
                        RowLayout {
                            Layout.fillWidth: true
                            Button {
                                text: "    Select nodes of selected elements    "
                                onClicked: contextVtkDisplay.SelectNodesByElements(
                                               false)
                            }
                        }
                    }
                }
            }
            ColumnLayout {
                GroupBox {
                    title: "Move ControlPoint Options"
                    id: moveControlPointOptions
                    visible: false
                    ColumnLayout {
                        RowLayout {
                            Layout.fillWidth: true
                            GridLayout {
                                id: optionsGrid
                                Label {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    text: "Plane"
                                    Layout.row: 0
                                    Layout.column: 0
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 0
                                    Layout.column: 1
                                    tooltip: "Plot XY Plane"
                                    checkable: true
                                    iconSource: "qrc:///icon/plane-xy.png"
                                    onClicked: {
                                        if (checked) {
                                            optionsGrid.uncheckOptionsButtons()
                                            for (var i = 0; i < listModel.count; i++) {
                                                if (tableView.selection.contains(
                                                            i)) {
                                                    myMetaEditor.plotControlPointPlane(
                                                                listModel.get(
                                                                    i).role, 2)
                                                }
                                            }
                                        }
                                    }
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 0
                                    Layout.column: 2
                                    tooltip: "Plot YZ Plane"
                                    checkable: true
                                    iconSource: "qrc:///icon/plane-yz.png"
                                    onClicked: {
                                        if (checked) {
                                            optionsGrid.uncheckOptionsButtons()
                                            for (var i = 0; i < listModel.count; i++) {
                                                if (tableView.selection.contains(
                                                            i)) {
                                                    myMetaEditor.plotControlPointPlane(
                                                                listModel.get(
                                                                    i).role, 0)
                                                }
                                            }
                                        }
                                    }
                                }
                                Button {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 0
                                    Layout.column: 3
                                    tooltip: "Plot ZX Plane"
                                    checkable: true
                                    iconSource: "qrc:///icon/plane-zx.png"
                                    onClicked: {
                                        if (checked) {
                                            optionsGrid.uncheckOptionsButtons()
                                            for (var i = 0; i < listModel.count; i++) {
                                                if (tableView.selection.contains(
                                                            i)) {
                                                    myMetaEditor.plotControlPointPlane(
                                                                listModel.get(
                                                                    i).role, 1)
                                                }
                                            }
                                        }
                                    }
                                }
                                Button {
                                    visible: false
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 1
                                    Layout.column: 1
                                    tooltip: "Plot Sphere"
                                    checkable: true
                                    iconSource: "qrc:///icon/picker-node.png"
                                    onClicked: {
                                        if (checked) {
                                            optionsGrid.uncheckOptionsButtons()
                                            for (var i = 0; i < listModel.count; i++) {
                                                if (tableView.selection.contains(
                                                            i)) {
                                                    myMetaEditor.plotControlPointSphere(
                                                                listModel.get(
                                                                    i).role)
                                                }
                                            }
                                        }
                                    }
                                }
                                Button {
                                    visible: false
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                                    Layout.row: 1
                                    Layout.column: 2
                                    tooltip: "Plot Line"
                                    checkable: true
                                    iconSource: "qrc:///icon/picker-node.png"
                                    onClicked: {
                                        if (checked) {
                                            optionsGrid.uncheckOptionsButtons()
                                            for (var i = 0; i < listModel.count; i++) {
                                                if (tableView.selection.contains(
                                                            i)) {
                                                    myMetaEditor.plotControlPointLine(
                                                                listModel.get(
                                                                    i).role)
                                                }
                                            }
                                        }
                                    }
                                }
                                // uncheck all the buttons in the pickerButtons panel
                                function uncheckOptionsButtons() {
                                    contextVtkDisplay.RemoveNonpersistentActors(
                                                )
                                    for (var i = 0; i < optionsGrid.children.length; i++) {
                                        if (optionsGrid.children[i].checkable)
                                            optionsGrid.children[i].checked = false
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Button {
                id: addControlPoint
                enabled: false
                text: "Create"
                tooltip: "Create the new Control Point"
                onClicked: {
                    myMetaEditor.getSelectedNodeIDsBusyIndicator(controlPointName.text, controlPointType.currentText, 1)
                }
            }
            Button {
                id: updateControlPoint
                enabled: false
                visible: false
                text: "Update"
                tooltip: "Update ControlPoint"
                onClicked: {
                    for (var i = 0; i < listModel.count; i++) {
                        if (tableView.selection.contains(i)) {
                            myMetaEditor.getSelectedNodeIDsBusyIndicator(listModel.get(i).role, controlPointType.currentText, 1, 1)
                            tableView.selection.clear()
                            updateControlPoint.enabled = false
                            removeControlPoint.enabled = false
                        }
                    }
                }
            }
            Button {
                id: removeControlPoint
                enabled: false
                visible: false
                text: "Remove"
                tooltip: "Remove ControlPoint"
                onClicked: {
                    dialogRemove.open()
                }
            }
        }
        Label {
            visible: false
            id: buttonHint
            text: "ControlPoint name already exists."
            font.pixelSize: 10
            color: "red"
        }
    }
}
