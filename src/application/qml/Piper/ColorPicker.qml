// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Controls.Styles 1.2

Button
{
    id: colorPicker
    property string titleBarValue
    property color colorSelected
    property Item item
    text: "Change Color"
    tooltip: "Color picker"

    ColorDialog {
        title: colorPicker.titleBarValue
        id: colorDialog
        onAccepted: {
            colorPicker.colorSelected  = colorDialog.color
        }
        Component.onCompleted: visible = false
    }

    onClicked:
    {
        colorDialog.open()

    }
    style: ButtonStyle {
        background: Rectangle {
            color: colorDialog.color
        }
    }
}
