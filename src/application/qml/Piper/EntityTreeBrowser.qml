// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1
import QtQml.Models 2.2

import AnatomicTreeModel 1.0
import AnatomyDB 1.0
import Piper 1.0

ColumnLayout
{

    id:root
    property int selectMode : SelectionMode.MultiSelection
    property int nbSelected : 0
    property var itemSelected : []
    property var indexNewList : 0

    property bool visualizeSelected : false // setting this to true will make the treeview toggle visibility of selected entities in the contextMetaManager
    property string defaultEntity : "" // entity that will be selected upon (re)loading the data if single selection is turned on (selectMode == SelectionMode.SingleSelection
    property string treeViewTitle : "Name"
    property alias treeView : treeView
    property alias treeModel : entityTreeModel
	property alias clearButton: clearSelectionBtn

    property bool anatomyDB : false

    RowLayout
    {
        id: selectionButtons

        Layout.fillWidth: true
        visible: (selectMode != SelectionMode.SingleSelection && selectMode != SelectionMode.NoSelection) // make it visible only if it is possible to select more than one item
        Button
        {
            id: clearSelectionBtn
            text: "None"
            tooltip: "Clear Selection"
            onClicked:{
				treeView.selection.clear()
				clearSelection()
			}
        }
        Button
        {
            id: selectAllBtn
            text: "All"
            tooltip: "select all"
            onClicked: treeView.selection.selectAll()
        }
        Button
        {
            id: invertSelectionBtn
            text: "Invert"
            tooltip: "Invert Selection"
            onClicked: treeView.selection.invertSelection()
        }
    }

	function clearSelection()
	{
		itemSelected= []; // remove all objects from itemSelected
		nbSelected=0; // set counter of selected elements to 0
	}

    AnatomicTreeModel {
        id: entityTreeModel

        function  updateEntityList() {
            var regions = ["Head","Body_proper", "Right_upper_limb", "Left_upper_limb", "Right_lower_limb", "Left_lower_limb"]

            var entities
            if (!root.anatomyDB)
                entities = contextMetaManager.getEntityList();
            else
                entities = contextMetaManager.getAnatomyDBEntityList();
            qdataList = regions.concat(entities);
        }       

        function  updateTreeWithCustomList(dataList)
        {
            qdataList = dataList;
        }
    }
    SearchTree {
        id: searchTree

        Layout.fillWidth: true

        treeModel: entityTreeModel
        treeView: treeView

        Component.onCompleted: this.forceActiveFocus()
    }
    TreeView {
        id: treeView

        Layout.fillWidth: true
        Layout.fillHeight : true

        model: entityTreeModel.model

        selectionMode: selectMode
        selection: ItemSelectionModel
        {
            model: entityTreeModel.model

            function selectAll() {
                var allIndexes=model.getAllIndexes();
                for (var i=0; i<allIndexes.length; ++i)
                    select(allIndexes[i], ItemSelectionModel.Select);
            }

            function invertSelection() {
                var allIndexes = model.getLeafIndexes(); // only toggle if it is a leaf - toggling parents will select/deselect all children making the button useless
                for (var i=0; i<allIndexes.length; ++i) {
                    select(allIndexes[i], ItemSelectionModel.Toggle);
                }
            }

            function selectSingleEntity(entityName) {
                var list = model.getLeafIndexes()
                for (var i = 0; i < list.length; i++)
                {
                    if (model.data(list[i]) === qsTr(entityName))
                    {
                        setCurrentIndex(list[i], ItemSelectionModel.Select);
                        treeView.focus = true;
                        break;
                    }
                }
            }

			function clearSelection()
			{
				itemSelected= []; // remove 1 object at location 'u' from itemSelected
				nbSelected=0; // decrease counter of selected elements
			}

            onSelectionChanged: {
                for (var i = 0; i < selected.length; ++i) {
                    if (visualizeSelected)
                        contextMetaManager.entityDisplay(1, model.data(selected[i].topLeft));
                    // select children if any
                    var childrenIndexes = model.getChildrenIndexes(selected[i].topLeft, false);
                    for (var j = 0; j < childrenIndexes.length; ++j)
                        select(childrenIndexes[j], ItemSelectionModel.Select);

                    itemSelected[nbSelected] = model.data(selected[i].topLeft);
                    nbSelected++;
                }
                for (var i = 0; i < deselected.length; ++i) {
                    if (visualizeSelected)
                        contextMetaManager.entityDisplay(0, model.data(deselected[i].topLeft));
                    var childrenIndexes = model.getChildrenIndexes(deselected[i].topLeft, false);
                    for (var j = 0; j < childrenIndexes.length; ++j)
                        select(childrenIndexes[j], ItemSelectionModel.Deselect);
                    for (var u = 0; u < nbSelected; ++u)
                    {
                        if (model.data(deselected[i].topLeft) === itemSelected[u])
                        {
                            itemSelected.splice(u, 1); // remove 1 object at location 'u' from itemSelected
                            nbSelected--; // decrease counter of selected elements
                            break;
                        }
                    }
                }
            }
        }
        Connections
        {
            target: context
            onModelChanged : {
                // reset selection
                nbSelected = 0;
                itemSelected = [];
                entityTreeModel.updateEntityList();
                if (selectMode === SelectionMode.SingleSelection || defaultEntity !== "")
                    treeView.selection.selectSingleEntity(defaultEntity);
                else if (selectMode !== SelectionMode.SingleSelection && selectMode !== SelectionMode.NoSelection)
                    treeView.selection.selectAll();
            }
        }


        itemDelegate: Label {
            font.bold: searchTree.matchIndex(styleData.index)
            text: styleData.value
        }

        TableViewColumn {
                role: "Name"
                title: treeViewTitle
        }

		function setDataList(dataList)
		{
			entityTreeModel.updateTreeWithCustomList(dataList);
		}

		function getSelectedItems()
		{
			return itemSelected;
		}
    }
}
