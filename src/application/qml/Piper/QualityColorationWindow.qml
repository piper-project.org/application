// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.meshOptimizing 1.0
import "../../module/meshOptimizing" // TJ - I don't know what is the correct way to include this without using the direct path like this, if somebody knows, please change it

ModuleToolWindow 
{
    title: "Element quality (by VTK)"
    width : childrenRect.width + 20
    height : childrenRect.height + 20

    Component.onCompleted: {        
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }
    onClosing:
    {
        contextVtkDisplay.HighlightElementsByScalarsOff("Quality")
        contextVtkDisplay.Render()
    }

    onVisibleChanged: {
        if (visible == false) {
            contextVtkDisplay.HighlightElementsByScalarsOff("Quality")
            contextVtkDisplay.Render()
        }
    }
    ColumnLayout 
    {
        anchors.fill: parent
        anchors.margins : 10
        GroupBox 
        {
            title: "Quality settings"
            ColumnLayout 
            {
                GridLayout 
                {
                    RowLayout 
                    {                    
                        Layout.row: 0
                        Layout.column: 0
                        Label
                        { 
                            text:"Choose metric function: "
                        }
                        ComboBox 
                        {
                            id: chooseMetricFunction
                            model:  ["Aspect Frobenius","Min Angle","Max Angle","Condition","Scaled Jacobian","Relative Size Squared","Shape","Shape and Size", "Distortion", "Area", "Edge Ratio", "Aspect Ratio", "Radius Ratio", "Volume"]
                            currentIndex: 4
                            onActivated:
                            {
                                chooseMetricFunction.currentIndex=index
                            }
                        }
                    }
                    RowLayout 
                    {                    
                        Layout.row: 0
                        Layout.column: 1
                        anchors.right: parent.right
                        Label
                        { 
                            text:" Alpha: "
                        }
                        SpinBox
                        {
                            id: alphaValue; 
                            decimals: 2
                            minimumValue: 0
                            maximumValue: 1
                            stepSize: 0.1
                            value: 1
                            enabled: true
                        }
                    }
					RowLayout
					{
					    Layout.row: 1
					    Layout.columnSpan: 2
						Layout.fillWidth: true
						CheckBox 
						{
							id: range1Chkbx; 
							text: "range 1"; 
							checked: true; 
						}
						SpinBox 
						{ 
							id: range1Lbx; 
							value: -1
							decimals: 2
							minimumValue: -999999999
							maximumValue: range1Ubx.value
                            implicitWidth: range2Lbx.width
							stepSize: 0.1
						}
						Label
						{
							text:" to "
						}
						SpinBox 
						{ 
							id: range1Ubx; 
							decimals: 2
                            minimumValue : range1Lbx.value
							maximumValue : range3Lbx.value    
                            implicitWidth: range2Lbx.width
							stepSize: 0.1
							value: 0.0
							onValueChanged:
							{
								range2Lbx.value = value
							}
						}
						ComboBox 
						{
							id: range1colorA
							Layout.maximumWidth: 80
							model:  [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 0
							onActivated:
							{
								range1colorA.currentIndex=index
							}
						}
						ComboBox 
						{
							id: range1colorB
							Layout.maximumWidth: 80
							model:  [" ","Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 0
							onActivated:
							{
								range1colorB.currentIndex=index
							}
						}
					}
					RowLayout
					{
					    Layout.row: 2
					    Layout.columnSpan: 2
						Layout.fillWidth: true
						CheckBox 
						{
							id: range2Chkbx;
							text: "range 2"; 
							checked: true; 
						}
						SpinBox 
						{ 
							id: range2Lbx; 
							value: 0.0
							decimals: 2
							stepSize: 0.1
							minimumValue : -999999999
							maximumValue : 999999999    
							enabled: false
						}
						Label
						{
							text:" to "
						}
						SpinBox 
						{ 
							id: range2Ubx; 
							value: 0.5
							decimals: 2
							stepSize: 0.1
							minimumValue : -999999999
							maximumValue : 999999999    
							enabled: false
						}
						ComboBox 
						{
							id: range2colorA
							Layout.maximumWidth: 80
							model: [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 1
							onActivated:
							{
								range2colorA.currentIndex=index
							}
						}
						ComboBox 
						{
							id: range2colorB
							Layout.maximumWidth: 80
							model: [ " " ,"Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 0
							onActivated:
							{
								range2colorB.currentIndex=index
							}
						}
					}
					RowLayout
					{
					    Layout.row: 3
					    Layout.columnSpan: 2
						Layout.fillWidth: true
						CheckBox 
						{
							id: range3Chkbx; 
							text: "range 3"; 
							checked: true; 
						}
						SpinBox 
						{ 
							id: range3Lbx; 
							value: 0.5
							decimals: 2
							stepSize: 0.1
							minimumValue : range1Ubx.value
							maximumValue : range3Ubx.value   
                            implicitWidth: range2Lbx.width
							onValueChanged : 
							{
								range2Ubx.value = value;
							}
						}
						Label
						{
							text:" to "
						}
						SpinBox 
						{ 
							id: range3Ubx; 
							value: 1.0
							decimals: 2
							minimumValue : range3Lbx.value
							maximumValue : 999999999   
                            implicitWidth: range2Lbx.width
							stepSize: 0.1
						}
						ComboBox 
						{
							id: range3colorA
							Layout.maximumWidth: 80
							model: [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 2
							onActivated:
							{
								range3colorA.currentIndex=index
							}
						}
						ComboBox 
						{
							id: range3colorB
							Layout.maximumWidth: 80
							model: [" " ,"Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
							currentIndex: 0
							activeFocusOnPress : true
							onActivated:
							{
								range3colorB.currentIndex=index
							}
						}
					}
                    RowLayout 
                    {                    
                        Layout.row: 4
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        Label
                        { 
                            text:"Select elements by quality: "
                        }
                        CheckBox
                        {
                            id: selectRange1 
                            checked: true
                            text: "Range 1"
                        }
                        CheckBox
                        {
                            id: selectRange2 
                            checked: false
                            text: "Range 2"
                        }
                        CheckBox
                        {
                            id: selectRange3
                            checked: false
                            text: "Range 3"
                        }                               
                        Button
                        {
                            id: buttonSelectByQuality
                            action : actionSelectByQuality
		                    tooltip: "Selects cells based on currently computed quality - this will not cause quality to be recomputed"
                        }
                    }
					RowLayout
					{
					    Layout.row: 5
					    Layout.columnSpan: 2
						Button 
						{
							text: "Display"
							tooltip: "Compute and display mesh quality"
							onClicked: 
							{
								getLUTParametersForQuality();
								indexOfMetricUsedForComputeQuality = chooseMetricFunction.currentIndex;
								actionSelectByQuality.enabled = true;
							}
						}
						Button
						{
							id: applySaveQuality
							action: applySaveQualityAction
						}
					}
                }
            }
        }
        ColumnLayout
        {
            CheckBox
            {
                id: checkboxRelative
                checked : myMeshOptimizing.optionQualUseBase
                onClicked : myMeshOptimizing.optionQualUseBase = checked
                text: "Use relative quality - difference between this and baseline model"
            }
            BaseModelSelectGroupBox
            {
                enabled: checkboxRelative.checked
                Layout.fillWidth: true
                myMeshoptimizing: myMeshOptimizing
                onBaseModelChanged:
                {
                    adjustWindowSizeToContent();
                }
            }
        }
    }
    property variant indexOfMetricUsedForComputeQuality: 100
	property variant ctfParamsArray: [[]]

    //Return an array to c++ with all the quality parameters setled in the GUI by the user
    function getLUTParametersForQuality()
    {

        var nbRanges = 0;

        //Count how many ranges determined by the user and send the data to c++
        if(range1Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range1colorA.currentText, range1colorB.currentText, range1Lbx.value, range1Ubx.value];
            nbRanges = nbRanges + 1;
        }
        if(range2Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range2colorA.currentText, range2colorB.currentText, range2Lbx.value, range2Ubx.value];
            nbRanges = nbRanges + 1;
        }
        if(range3Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range3colorA.currentText, range3colorB.currentText, range3Lbx.value, range3Ubx.value];
            nbRanges = nbRanges + 1;
        }
        if (nbRanges > 0)
            myMeshOptimizing.QualityComputeAndVisualisation(ctfParamsArray, chooseMetricFunction.currentIndex, alphaValue.value)

		ctfParamsArray= [[]]
    }
    MeshOptimizing
    {
        id: myMeshOptimizing
    }
    
    Action 
    {
        id: applySaveQualityAction
        text: qsTr("Save Quality")
        onTriggered: saveQualityDialog.open()
    }

    FileDialog 
    {
        id: saveQualityDialog
        title: qsTr("Save quality as...")
        selectExisting: false
        nameFilters: ["Quality files (*.csv)"]
        onAccepted: myMeshOptimizing.exportQualityFile(saveQualityDialog.fileUrl, indexOfMetricUsedForComputeQuality)
    }

    Action 
    {
        id: actionSelectByQuality
        text: qsTr("Select")
        enabled: false
        onTriggered: 
        {      
            var parArray = [[]];
            var nbRanges = 0;
            parArray[0] = [1, -1] // by default use a range that will cause all to deselect
            //Count how many ranges determined by the user and send the data to c++
            if (selectRange1.checked)
                parArray[nbRanges++] = [range1Lbx.value, range1Ubx.value];
            if (selectRange2.checked)
                parArray[nbRanges++] = [range2Lbx.value, range2Ubx.value];
            if (selectRange3.checked)
                parArray[nbRanges] = [range3Lbx.value, range3Ubx.value];

            myMeshOptimizing.SelectByQuality(parArray)
        }
    }
 
}

       
