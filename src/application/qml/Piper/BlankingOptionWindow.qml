// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.1
import QtQml 2.2

ModuleToolWindow
{
    title: "Blanking"
    id: root
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : 10
        GroupBox
        {
            title:"Blank elements"
            GridLayout
            {
                anchors.centerIn: parent        
                Button
                {
                    Layout.row: 0
                    Layout.columnSpan : 2
                    text: "Blank selected elements"
                    anchors.horizontalCenter : parent.horizontalCenter      
                    onClicked:
                    {
                        context.BlankSelectedElements()
                    }
                }
                Button
                {
                    id: buttonUndoLast
                    Layout.row: 1
                    Layout.column : 0
                    text: "Undo last"
                    enabled: contextVtkDisplay.GetBlankLevel() > 0
                    onClicked:
                    {
                        context.Unblank(false)
                    }
                    Connections
                    {
                        target: contextVtkDisplay
                        onBlankLevelChanged: buttonUndoLast.enabled = contextVtkDisplay.GetBlankLevel() > 0
                    }
                }
                Button
                {
                    id: buttonUndoAll
                    Layout.row: 1
                    Layout.column : 1
                    text: "Undo all "
                    enabled : contextVtkDisplay.GetBlankLevel() > 0
                    onClicked:
                    {
                        context.Unblank(true)
                    }
                    Connections
                    {
                        target: contextVtkDisplay
                        onBlankLevelChanged: buttonUndoAll.enabled = contextVtkDisplay.GetBlankLevel() > 0
                    }
                }
            }
        }
    }
    Component.onCompleted:
    {
        adjustWindowSizeToContent();
    }
}

