// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

GroupBox {
    title: "Viewer settings"
    flat: true
    GridLayout {
        anchors.centerIn: parent
        columns: 4
        Button {
            Layout.row: 0
            Layout.column : 0
            iconSource: "qrc:///icon/reset-camera.png"
            tooltip: "Reset Camera"
            onClicked: {
                contextVtkDisplay.ResetCamera()
            }
        }
        Button{ 
            Layout.row: 0
            Layout.column : 1
            id: buttonUseParallel
            iconSource: "qrc:///icon/parallel-projection.png"
            tooltip: "Toggle use parallel projection"
            checkable: true
            onClicked: contextVtkDisplay.useParallelProjection = buttonUseParallel.checked
            Component.onCompleted: checked = contextVtkDisplay.useParallelProjection
            Connections {
                target: contextVtkDisplay
                onUseParallelProjectionChanged: buttonUseParallel.checked
                                                = contextVtkDisplay.useParallelProjection
            }
        }
        Button {
            Layout.row: 0
            Layout.column : 2
            id: buttonPickCenter
            tooltip: "Click on a surface of a mesh to set the clicked point as camera rotation center"
            checkable: true
            iconSource: "qrc:///icon/rotation-center.png"
            onClicked: {
                if (checked) {
                    contextVtkDisplay.SetPickingType(-1)
                    checked = true // setting pick type will reset it
                    contextVtkDisplay.PickCameraFocus()
                } else
                    contextVtkDisplay.SetPickingType(-1)
            }
            Connections {
                target: contextVtkDisplay
                onPickTargetChanged: buttonPickCenter.checked = false
            }
        }
        Button{
            Layout.row: 1
            Layout.column : 0
            iconSource: "qrc:///icon/reset-camera-x.png"
            tooltip: "Set Camera in X Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(0)
            }
        }
        Button {
            Layout.row: 1
            Layout.column : 1
            iconSource: "qrc:///icon/reset-camera-y.png"
            tooltip: "Set Camera in Y Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(1)
            }
        }
        Button {
            Layout.row: 1
            Layout.column : 2
            iconSource: "qrc:///icon/reset-camera-z.png"
            tooltip: "Set Camera in Z Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(2)
            }
        }
        Button{
            Layout.row: 2
            Layout.column : 0
            iconSource: "qrc:///icon/reset-camera-negx.png"
            tooltip: "Set Camera in X Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(3)
            }
        }
        Button {
            Layout.row: 2
            Layout.column : 1
            iconSource: "qrc:///icon/reset-camera-negy.png"
            tooltip: "Set Camera in Y Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(4)
            }
        }
        Button {
            Layout.row: 2
            Layout.column : 2
            iconSource: "qrc:///icon/reset-camera-negz.png"
            tooltip: "Set Camera in Z Direction"
            onClicked: {
                contextVtkDisplay.SetCamera(5)
            }
        }
        Button {
            Layout.row: 3
            Layout.column : 0
            id: buttonRenderNormals
            iconSource: "qrc:///icon/element-normal.png"
            tooltip: "Toggle see element normals"
            checkable: true
            checked: buttonRenderNormals.checked = contextVtkDisplay.renderNormals
            onClicked: context.ShowNormals(buttonRenderNormals.checked)
            Component.onCompleted: checked = contextVtkDisplay.renderNormals
            Connections {
                target: contextVtkDisplay
                onRenderNormalsChanged: buttonRenderNormals.checked
                                        = contextVtkDisplay.renderNormals
            }
        }
        Button {
            Layout.row: 3
            Layout.column : 1
            id: buttonRenderEdges
            iconSource: "qrc:///icon/element-edges.png"
            tooltip: "Toggle see element edges"
            checkable: true
            onClicked: contextVtkDisplay.renderEdges = buttonRenderEdges.checked
            Component.onCompleted: checked = contextVtkDisplay.renderEdges
            Connections {
                target: contextVtkDisplay
                onRenderEdgesChanged: buttonRenderEdges.checked = contextVtkDisplay.renderEdges
            }
        }
        Button {
            Layout.row: 3
            Layout.column : 2
            id: buttonDisplayOriginAxis
            iconSource: "qrc:///icon/origin-axis.png"
            tooltip: "Toggle see axes"
            checkable: true
            onClicked: {
                contextMetaManager.displayOriginAxis(
                            buttonDisplayOriginAxis.checked)
                if (checked) {
                    contextMetaManager.setDisplayModeActive(
                                64, true) // turn on axes display
                }
            }

            Component.onCompleted: checked = contextVtkDisplay.renderEdges
        }
        ModuleToolWindowButton{
            Layout.row: 3
            Layout.column : 3
            iconSource: "qrc:///icon/clip.png"
            tooltip: "Clipping Plane"
            toolWindow: clippingPlaneWindow
        }
    }
    ClippingPlaneWindow {
        id: clippingPlaneWindow
    }
}
