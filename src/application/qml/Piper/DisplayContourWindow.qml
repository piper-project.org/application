// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow 
{
    title: "Editor Options"
    
    minimumHeight : 200
    minimumWidth : 800
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb_entities: 0;
    property int nb_landmarks: 0;
    property int nb_frames: 0;

    Component.onCompleted: adjustWindowSizeToContent()

    ExclusiveGroup
    {
        id: group
    }

    SplitView 
    {
        anchors.fill: parent
        orientation: Qt.Horizontal
        Rectangle 
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            height:childrenRect.height
            ColumnLayout 
            {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                RowLayout 
                {
                    id: displayTools
                    Layout.alignment: Qt.AlignTop
                    Layout.fillWidth: true

                    GroupBox
                    {
                        id: contourBox
                        title: "Contours"
                        Layout.alignment: Qt.AlignTop
                        RowLayout 
                        {
                            Rectangle 
                            {
                                Layout.fillWidth: true
                                height:childrenRect.height
                                ColumnLayout 
                                {
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignTop
                                    RowLayout 
                                    {
                                        Layout.alignment: Qt.AlignTop
                                        Layout.fillWidth: true
                                        ColumnLayout 
                                        {
                                            width:childrenRect.width
                                            ColumnLayout 
                                            {
                                                enabled:true
                                                width:childrenRect.width
                                                Action 
                                                {
                                                    id: openContourAction
                                                    text: qsTr("Open Contour file")
                                                    tooltip: qsTr("Open contour file")
                                                    iconSource: "qrc:///icon/document-open.png"
                                                    onTriggered: openContourDialog.open()
                                                }
                                                ToolButton 
                                                {
                                                    action: openContourAction 
                                                }
                                                FileDialog 
                                                {
                                                    id: openContourDialog
                                                    title: qsTr("Open Contour File...")
                                                    nameFilters: ["Contour files (*.txt *.xml)"]
                                                    onAccepted: 
                                                    {
                                                        contextMetaManager.openContour(openContourDialog.fileUrl);
                                                        contourTools.enabled = true;
                                                        listView.visible =true
                                                    }
                                                }
                                                Action 
                                                {
                                                    id: saveConotourAction
                                                    text: qsTr("Save Contour as")
                                                    iconSource: "qrc:///icon/document-save.png"
                                                    onTriggered: saveAsDialog.open()
                                                }
                                                ToolButton 
                                                { 
                                                    action: saveConotourAction 
                                                }
                                                FileDialog 
                                                {
                                                    id: saveAsDialog
                                                    title: qsTr("Save contour as...")
                                                    selectExisting: false
                                                    nameFilters: ["Contour files (*.xml)"]
                                                    onAccepted: 
                                                    {
                                                        contextMetaManager.saveContour(saveAsDialog.fileUrl)
                                                    }
                                                }
                                            }
                                        }
                                        RowLayout
                                        {
                                            id: contourTools
                                            enabled: false
                                            ListModel 
                                            {
                                                id: contourBRList
                                                ListElement 
                                                {
                                                    nb: 1
                                                    contour: "Right_Foot"
                                                }
                                                ListElement 
                                                {
                                                    nb: 2
                                                    contour: "Left_Foot"
                                                }
                                                ListElement 
                                                {
                                                    nb: 3
                                                    contour: "Right_Ankle_to_Calf"
                                                }
                                                ListElement 
                                                {
                                                    nb: 4
                                                    contour: "Left_Ankle_to_Calf"
                                                }
                                                ListElement 
                                                {
                                                    nb: 5
                                                    contour: "Right_Knee_lower"
                                                }
                                                ListElement 
                                                {
                                                    nb: 6
                                                    contour: "Left_Knee_lower"
                                                }
                                                ListElement 
                                                {
                                                    nb: 7
                                                    contour: "Right_Knee_upper"
                                                }
                                                ListElement 
                                                {
                                                    nb: 8
                                                    contour: "Left_Knee_upper"
                                                }
                                                ListElement 
                                                {
                                                    nb: 9
                                                    contour: "Right_Thigh"
                                                }
                                                ListElement 
                                                {
                                                    nb: 10
                                                    contour: "Left_Thigh"
                                                }
                                                ListElement 
                                                {
                                                    nb: 11
                                                    contour: "Hip"
                                                }
                                                ListElement 
                                                {
                                                    nb: 12
                                                    contour: "Abdomen"
                                                }
                                                ListElement 
                                                {
                                                    nb: 13
                                                    contour: "Thorax"
                                                }
                                                ListElement 
                                                {
                                                    nb: 14
                                                    contour: "Neck"
                                                }
                                                ListElement 
                                                {
                                                    nb: 15
                                                    contour: "Head"
                                                }
                                                ListElement 
                                                {
                                                    nb: 16
                                                    contour: "Right_Palm"
                                                }
                                                ListElement 
                                                {
                                                    nb: 17
                                                    contour: "Left_Palm"
                                                }
                                                ListElement 
                                                {
                                                    nb: 18
                                                    contour: "Right_Forearm"
                                                }
                                                ListElement 
                                                {
                                                    nb: 19
                                                    contour: "Left_Forearm"
                                                }
                                                ListElement 
                                                {
                                                    nb: 20
                                                    contour: "Right_Elbow_lower"
                                                }
                                                ListElement 
                                                {
                                                    nb: 21
                                                    contour: "Left_Elbow_lower"
                                                }
                                                ListElement 
                                                {
                                                    nb: 22
                                                    contour: "Right_Elbow_upper"
                                                }
                                                ListElement 
                                                {
                                                    nb: 23
                                                    contour: "Left_Elbow_upper"
                                                }
                                                ListElement 
                                                {
                                                    nb: 24
                                                    contour: "Right_Biceps"
                                                }
                                                ListElement 
                                                {
                                                    nb: 25
                                                    contour: "Left_Biceps"
                                                }
                                                ListElement 
                                                {
                                                    nb: 26
                                                    contour: "Right_biceps_to_shoulder"
                                                }
                                                ListElement 
                                                {
                                                    nb: 27
                                                    contour: "Left_biceps_to_shoulder"
                                                }
                                                ListElement 
                                                {
                                                    nb: 28
                                                    contour: "Hip_right"
                                                }
                                                ListElement 
                                                {
                                                    nb: 29
                                                    contour: "Hip_left"
                                                }
                                                ListElement 
                                                {
                                                    nb: 30
                                                    contour: "Keypoints"
                                                }
                                                ListElement 
                                                {
                                                    nb: 31
                                                    contour: "Right_Abdomen"
                                                }
                                                ListElement 
                                                {
                                                    nb: 32
                                                    contour: "Left_Abdomen"
                                                }
                                                ListElement 
                                                {
                                                    nb: 33
                                                    contour: "Right_Thorax"
                                                }
                                                ListElement 
                                                {
                                                    nb: 34
                                                    contour: "Left_Thorax"
                                                }
                                            }
                                            TableView  
                                            {
                                                id : listView
                                                Layout.minimumHeight: 100
                                                Layout.fillHeight: true
                                                Layout.fillWidth: true
                                                enabled: true
                                                clip: true
                                                focus: false
                                                property variant vals;
                                                function processClickedItem(index) 
                                                {
                                                    listView2.visible = true
                                                    contoursColumn.title =  contourBRList.get(index).contour + " Contours"
                                                    vals = contextMetaManager.getContourList(contourBRList.get(index).nb);
                                                    contoursList.clear()
                                                    for(var i =0; i<vals.length; i++)
                                                    {
                                                        contoursList.append({nb: i+1,contour: vals[i]});
                                                    }
                                                }
                                                TableViewColumn 
                                                {
                                                    role: "nb"
                                                    title: "No."
                                                    width: 20
                                                }
                                                TableViewColumn 
                                                {
                                                    role: "enabled"
                                                    width:20
                                                    delegate: CheckBox 
                                                    {
                                                        checked:true
                                                        onClicked: 
                                                        {
                                                            if(checked)
                                                            {
                                                                contextMetaManager.contourBodyRegionDisplay(1,model.nb - 1)
                                                            }
                                                            else
                                                            {
                                                                contextMetaManager.contourBodyRegionDisplay(0,model.nb - 1)
                                                            }
                                                        }
                                                    }
                                                }
                                                TableViewColumn 
                                                {
                                                    role: "contour"
                                                    title: "Contour Body Regions"
                                                }
                                                model: contourBRList
                                                headerVisible:true
                                                onClicked:processClickedItem(row)
                                            }
                                            ListModel
                                            {
                                                id:contoursList
                                            }
                                            TableView 
                                            {
                                                id: listView2
                                                Layout.minimumHeight: 100
                                                Layout.fillHeight: true
                                                Layout.fillWidth: true
                                                clip: true
                                                focus: false
                                                headerVisible:true
                                                model: contoursList
                                                property variant vals;
                                                function processClickedItem2(index) 
                                                {
                                                    listView3.visible = true
                                                    contourPtsColumn.title = "Contour Points of "+ contoursList.get(index).contour
                                                    vals = contextMetaManager.getContourList(listView.currentRow,index);
                                                    contourPointsList.clear()
                                                    for(var i =0; i<vals.length; i++)
                                                    {
                                                        contourPointsList.append({contour: vals[i]});
                                                    }
                                                }
                                                TableViewColumn 
                                                {
                                                    role: "nb"
                                                    title: "No."
                                                    width: 20
                                                }
                                                TableViewColumn 
                                                {
                                                    role: "enabled"
                                                    width:20
                                                    delegate: CheckBox 
                                                    {
                                                        checked:true
                                                        onClicked: 
                                                        {
                                                            if(checked == true)
                                                            {
                                                                contextMetaManager.contourDisplay(1,listView.currentRow,model.nb - 1)
                                                            }
                                                            else
                                                            {
                                                                contextMetaManager.contourDisplay(0,listView.currentRow,model.nb - 1)
                                                            }
                                                        }
                                                    }
                                            }
                                            TableViewColumn 
                                            {
                                                id:contoursColumn
                                                role: "contour"
                                                title: "Contours"
                                            }
                                            onClicked:processClickedItem2(row)
                                            }

                                        ListModel
                                        {
                                            id:contourPointsList
                                        }

                                        TableView 
                                        {
                                            id: listView3
                                            Layout.minimumHeight: 100
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true
                                            clip: true
                                            focus: false
                                            headerVisible:true
                                            selectionMode:SelectionMode.MultiSelection
                                            model: contourPointsList
                                            function processClickedItem3(index) 
                                            {
                                                if(listView3.selection.contains(index))
                                                {
                                                    contextMetaManager.contourPointDisplay(1,listView.currentRow,listView2.currentRow,index)
                                                }
                                                else 
                                                {
                                                    contextMetaManager.contourPointDisplay(0,listView.currentRow,listView2.currentRow,index)
                                                }
                                            }
                                            TableViewColumn 
                                            {
                                                id:contourPtsColumn
                                                role: "ValueRole"
                                                title: "Contour Points"
                                                movable: false
                                                resizable: false
                                            } // TableViewColumn
                                            onClicked:processClickedItem3(row)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }//SplitView 


    }
}
