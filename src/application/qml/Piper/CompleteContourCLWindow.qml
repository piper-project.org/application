// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Create Missing Metadata for Contour Deformation Module"

    minimumHeight : 250
    minimumWidth : 750
    //Component.onCompleted: setCurrentWindowSizeFixed()

    MessageDialog{
        id:messageDialog
        visible: false
    }

    ColumnLayout
    {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop
        anchors.centerIn : parent
        anchors.fill : parent

        ListModel
        {
            id:listModel
        }



        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.SingleSelection
            model: listModel


            function getList()
            {
                listModel.clear();
                var vals = contextMetaManager.ui_getUndefinedContourCLElements()
                for(var elem in vals)
                {
                        listModel.append({element: elem, type:vals[elem][0] , description:vals[elem][2], parent: vals[elem][1]});
                }
            }
            TableViewColumn
            {
                role: "element"
                title: "ContourCL element"
                width:200
            }
            TableViewColumn
            {
                role: "type"
                title: "Type"
                width:100
            }
            TableViewColumn
            {
                role: "description"
                title: "Description"
                width:300
            }
            TableViewColumn
            {
                role: "parent"
                title: "Body region/Joint"
                width:300
            }
            TableViewColumn
            {
                visible: false
                role: "apply"
                title: "CREATE"
                width:50
                delegate: Button
                {
                    text:"Create"
                    //checkable: true
                    onClicked:
                    {
                        console.log("Selected element = "+(tableView.row.element))//+" Selected type = "+(this.tableView.row).type)
                    }
                }
            }
            Connections
            {
                target: tableView.selection
                onSelectionChanged :
                {
//                    var listNames = [];
//                    var listVisible = [];
//                    var listType = [];
//                    for (var i = 0; i < tableView.model.count; i++)
//                    {
//                        listNames.push(tableView.model.get(i).target);
//                        listType.push(tableView.model.get(i).type);
//                        listVisible.push(tableView.selection.contains(i));
//                    }
//                    contextMetaManager.targetDisplayList(listNames, listVisible, listType);
                }
            }

            Component.onCompleted:
            {
                getList();

            }
            Connections
            {
                target: context
                onVisDataLoaded:{
                    tableView.getList();
                }
                onMetadataChanged:{
                    tableView.getList();
                }
            }
        }
        GroupBox{
            ColumnLayout{
                Button{
                    property ModuleToolWindow toolWindow
                    text:"Create object"
                    onClicked:
                    {
                        var type = listModel.get(tableView.currentRow).type
                        type = type.split(" ")
                        type = type[type.length-1]
                        if(type=="Landmark")
                            toolWindow = editorLandmarkWindow
                        else if(type=="Entity")
                            toolWindow = editorEntityWindow
                        else if(type=="Metadata")
                            toolWindow =  editorGenericMetadataWindow
                        toolWindow.create = listModel.get(tableView.currentRow).element
                        toolWindow.show()
                    }
                }
            }
        }
    }

    EditorEntityWindow
    {
        id: editorEntityWindow
    }
    EditorLandmarkWindow
    {
        id: editorLandmarkWindow
    }
    EditorGenericMetadataWindow
    {
        id: editorGenericMetadataWindow
    }


}
