// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2

import Piper 1.0

ColumnLayout{
	Layout.fillWidth: true
	VtkViewerCommonTools
	{
		id: vtkViewerCommonTools
	}

	ModuleToolWindowButton 
	{
		id: detachUserToolsButton
		Layout.fillWidth: false
		height: 20
		width: 20
		anchors.right: vtkViewerCommonTools.right
		anchors.top: vtkViewerCommonTools.top
		anchors.topMargin: -3
		anchors.rightMargin: 1
		iconSource: "qrc:///icon/pinIcon_atached.png"
		toolWindow: vtkViewerCommonToolsWindow
		tooltip: qsTr("Detach the viewer options from the right panel")
		onClicked:
		{
			if(checked)
			{
				vtkViewerCommonTools.visible = false
				iconSource= "qrc:///icon/pinIcon_detached.png"
				tooltip= qsTr("Atach the viewer options from the right panel")
			}
			else
			{
				vtkViewerCommonTools.visible = true
				iconSource= "qrc:///icon/pinIcon_atached.png"
				tooltip= qsTr("Detach the viewer options from the right panel")
			}
		}
	}
	VtkViewerCommonToolsWindow
	{
		id: vtkViewerCommonToolsWindow
		onClosing:
		{
			if(close.accepted=== true)
			{
				vtkViewerCommonTools.visible = true
				detachUserToolsButton.iconSource= "qrc:///icon/pinIcon_atached.png"
				detachUserToolsButton.tooltip= qsTr("Detach the viewer options from the right panel")
			}
		}
	}
}
