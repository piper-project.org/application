// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1
import QtQml.Models 2.2

import Piper 1.0

ModuleToolWindow 
{
    title: "Entity Display Options"
    property bool isInited: false

    ColumnLayout
    {
        anchors.fill: parent
        EntityTreeBrowser
        {
            anchors.fill: parent
            id: entityBrowser
            visualizeSelected : true
        } 
        RowLayout {
            CheckBox 
            {
                id: checkVisEnt
                text: qsTr("Visualize entities -    ")
                checked: contextVtkDisplay.IsDisplayModeActive(2)
                onClicked: 
                {
                    if (checked == true)
                    {
                        contextMetaManager.setEntitiesOpacity(entityOpacity.value);
                        entityOpacity.enabled = true
                        contextMetaManager.setDisplayModeActive(2, true); // turn on entities
                    }
                    else
                    {
                        entityOpacity.enabled = false
                        contextMetaManager.setDisplayModeActive(2, false); // turn off entities
                    }
                }
            }
            Label{text:"opacity:"}
            SpinBox {
                id: entityOpacity
                enabled : contextVtkDisplay.IsDisplayModeActive(2)
                decimals: 2
                minimumValue: 0.00
                maximumValue: 1.00
                stepSize: 0.10
                value: 1.00
                onValueChanged: {
                    contextMetaManager.setEntitiesOpacity(entityOpacity.value);
                }
            }
        }
        RowLayout {
            CheckBox 
            {
                id: checkVisFullModel
                text: qsTr("Visualize full Model - ")
                checked: contextVtkDisplay.IsDisplayModeActive(1)
                onClicked: 
                {
                    if (checked == true)
                    {
                        contextMetaManager.setFullModelOpacity(fullModelOpacity.value);
                        fullModelOpacity.enabled = true
                        contextMetaManager.setDisplayModeActive(1, true); // turn on full mesh
                    }
                    else
                    {
                        fullModelOpacity.enabled = false
                        contextMetaManager.setDisplayModeActive(1, false); // turn off full mesh
                    }
                }
            }
            Label{text:"opacity:"}
            SpinBox {
                id: fullModelOpacity
                enabled : contextVtkDisplay.IsDisplayModeActive(1)
                decimals: 2
                minimumValue: 0.00
                maximumValue: 1.00
                stepSize: 0.10
                value: 1.00
                onValueChanged: {
                    contextMetaManager.setFullModelOpacity(fullModelOpacity.value);
                }
            }
        }   
        Connections
        {
            target: contextVtkDisplay
            onDisplayModeChanged:
            {
                fullModelOpacity.enabled = contextVtkDisplay.IsDisplayModeActive(1);
                checkVisFullModel.checked = contextVtkDisplay.IsDisplayModeActive(1);
                entityOpacity.enabled = contextVtkDisplay.IsDisplayModeActive(2);
                checkVisEnt.checked = contextVtkDisplay.IsDisplayModeActive(2);
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        height=500;
        setCurrentWindowSizeMinimum();
    }

    function initialize() {
        if (!isInited)
        {
            entityBrowser.treeModel.updateEntityList();
            entityBrowser.treeView.selection.selectAll();
            isInited = true
        }
    }
    Connections {
        target: context
        onVisDataLoaded: {
            entityBrowser.treeModel.updateEntityList();
            entityBrowser.treeView.selection.selectAll();
            isInited = true
        }
        onMetadataChanged:  {
            entityBrowser.treeModel.updateEntityList();
            entityBrowser.treeView.selection.selectAll();
            isInited = true
        }
    }

}
