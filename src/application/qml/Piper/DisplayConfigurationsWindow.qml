// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Controls.Styles 1.2
import Piper 1.0

ModuleToolWindow
{
    id: displayConfigurationsWindow
    title: "Display Configurations"
    property int nb: 0;

    ColumnLayout
    {
        id:grid
        property int recHeightRow1: 300
        property int recWidthRow1: 300
        property int recHeightRow2: 300
        property int recWidthRow2: 300
        anchors.leftMargin: 7
        anchors.topMargin: 7
        width: parent.width
        Layout.fillHeight: true
        RowLayout
        {
            width: parent.width

            // Entity display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Entity"
                    ColumnLayout
                    {
                        Label{ text:"Entity select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    entityTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    entityTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(entityTableView.selection.contains(i))
                                        {
                                            entityTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            entityTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:listModel
                        }
                        TableView
                        {
                            id: entityTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: listModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getEntityList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    listModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Entities"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections
                            {
                                target: context
                                onVisDataLoaded:
                                {
                                    entityTableView.getList();
                                }
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    entityTableView.getList()
                                }
                            }
                        }
                        RowLayout{
                            ColorPicker{
                                id: colorEntity
                                titleBarValue: "Select entity color"
                                onColorSelectedChanged: {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(entityTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorEntity.colorSelected, listModel.get(i).role, alphaEntity.value)
                                        }
                                    }
                                }
                            }
                            Label
                            {
                                text:" Alpha: "
                            }
                            SpinBox
                            {
                                id: alphaEntity;
                                decimals: 2
                                minimumValue: 0
                                maximumValue: 1
                                stepSize: 0.1
                                value: 0.80
                                enabled: true
                                onValueChanged:{
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(entityTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorEntity.colorSelected, listModel.get(i).role, alphaEntity.value)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Landmarks display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Landmark"
                    ColumnLayout
                    {
                        Label{ text:"Landmark select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    landmarkTableView.focus = true;
                                    landmarkTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    landmarkTableView.focus = true;
                                    landmarkTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    landmarkTableView.focus = true;
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(landmarkTableView.selection.contains(i))
                                        {
                                            landmarkTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            landmarkTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id:landmarkListModel
                        }
                        TableView
                        {
                            id: landmarkTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: landmarkListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getLandmarkList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    landmarkListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Landmarks"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections
                            {
                                target: context
                                onVisDataLoaded:
                                {
                                    landmarkTableView.getList();
                                }
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    landmarkTableView.getList()
                                }
                            }
                            Connections
                            {
                                target: landmarkTableView.selection
                                onSelectionChanged :
                                {
                                    var listNames = [];
                                    var listVisible = [];
                                    for (var i = 0; i < landmarkTableView.model.count; i++)
                                    {
                                        listNames.push(landmarkTableView.model.get(i).role);
                                        listVisible.push(landmarkTableView.selection.contains(i));
                                    }
                                    contextMetaManager.landmarkDisplayList(listNames, listVisible, false);
                                }
                            }
                        }
                        RowLayout{
                            ColorPicker{
                                id: colorLandmark
                                titleBarValue: "Select landmark color"
                                onColorSelectedChanged: {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(landmarkTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorLandmark.colorSelected, landmarkListModel.get(i).role, alphaLandmark.value)
                                        }
                                    }
                                }
                            }
                            Label
                            {
                                text:" Alpha: "
                            }
                            SpinBox
                            {
                                id: alphaLandmark;
                                decimals: 2
                                minimumValue: 0
                                maximumValue: 1
                                stepSize: 0.1
                                value: 0.80
                                enabled: true
                                onValueChanged:{
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(landmarkTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorLandmark.colorSelected, landmarkListModel.get(i).role, alphaLandmark.value)
                                        }
                                    }
                                }
                            }
                        }
                        RowLayout{
                            Label
                            {
                                text:" Radius: "
                            }
                            SpinBox
                            {
                                id: radiusLandmark;
                                decimals: 2
                                minimumValue: 0
                                stepSize: 0.1
                                value: 3.00
                                enabled: true
                                onValueChanged:{
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(landmarkTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeRadius(landmarkListModel.get(i).role, radiusLandmark.value)
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        // Second Row

        RowLayout
        {
            // GenericMetadata display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"GenericMetadata"
                    ColumnLayout
                    {
                        Label{ text:"GenericMetadata select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    genericMetadataTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    genericMetadataTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(genericMetadataTableView.selection.contains(i))
                                        {
                                            genericMetadataTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            genericMetadataTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id: genericMetadataListModel
                        }
                        TableView
                        {
                            id: genericMetadataTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: genericMetadataListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getGenericMetadataList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    genericMetadataListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "GenericMetadata"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections
                            {
                                target: context
                                onVisDataLoaded:
                                {
                                    genericMetadataTableView.getList();
                                }
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    genericMetadataTableView.getList()
                                }
                            }
                        }
                        RowLayout{
                            ColorPicker{
                                id: colorGenericMetadata
                                titleBarValue: "Select GenericMetadata color"
                                onColorSelectedChanged: {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(genericMetadataTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorGenericMetadata.colorSelected, genericMetadataListModel.get(i).role, alphaGenericMetadata.value)
                                        }
                                    }
                                }
                            }
                            Label
                            {
                                text:" Alpha: "
                            }
                            SpinBox
                            {
                                id: alphaGenericMetadata;
                                decimals: 2
                                minimumValue: 0
                                maximumValue: 1
                                stepSize: 0.1
                                value: 0.80
                                enabled: true
                                onValueChanged:{
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(genericMetadataTableView.selection.contains(i))
                                        {
                                            contextMetaManager.changeColor(colorGenericMetadata.colorSelected, genericMetadataListModel.get(i).role, alphaGenericMetadata.value)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Frame display configurations
            Rectangle
            {
                height:grid.recHeightRow1; width:grid.recWidthRow1
                color:"transparent"
                GroupBox
                {
                    height:parent.height; width:parent.width
                    title:"Frame"
                    ColumnLayout
                    {
                        Label{ text:"Frame select"}
                        RowLayout
                        {
                            width:childrenRect.width
                            Button
                            {
                                text: "None"
                                tooltip: "Clear selection"
                                onClicked:
                                {
                                    frameTableView.selection.clear();
                                }
                            }
                            Button
                            {
                                text: "All"
                                tooltip: "Select all"
                                onClicked:
                                {
                                    frameTableView.selection.selectAll();
                                }
                            }
                            Button
                            {
                                text: "Invert"
                                tooltip: "Invert selection"
                                onClicked:
                                {
                                    for(var i=0;i<nb;i++)
                                    {
                                        if(frameTableView.selection.contains(i))
                                        {
                                            frameTableView.selection.deselect(i)
                                        }
                                        else
                                        {
                                            frameTableView.selection.select(i)
                                        }
                                    }
                                }
                            }
                        }
                        ListModel
                        {
                            id: frameListModel
                        }
                        TableView
                        {
                            id: frameTableView
                            Layout.minimumHeight: 100
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            clip: true
                            focus: false
                            headerVisible:true
                            selectionMode:SelectionMode.MultiSelection
                            model: frameListModel
                            property variant vals;
                            function getList()
                            {
                                vals = contextMetaManager.getFrameList();
                                for(var i =0; i<vals.length; i++)
                                {
                                    frameListModel.append({role: vals[i]});
                                    nb++;
                                }
                            }
                            TableViewColumn
                            {
                                role: "role"
                                title: "Frames"
                            }

                            Component.onCompleted:
                            {
                                getList();
                            }
                            Connections
                            {
                                target: context
                                onVisDataLoaded:
                                {
                                    frameTableView.getList();
                                }
                            }
                            Connections {
                                target: context
                                onMetadataChanged:  {
                                    frameTableView.getList()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeMinimum();
    }
}
