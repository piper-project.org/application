// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0



Dialog {
    title: "Import FE model"
    standardButtons: StandardButton.Ok | StandardButton.Cancel
    visible: false

    property alias formatRulesFilePath : formatRulesChooser.filePath
    property alias modelRulesFilePath : selectModelRulesDialog.fileUrl
    property alias modelFilePath : selectModelDialog.fileUrl
    property alias modelLengthUnit : lengthUnitChooser.currentText

    Component.onCompleted: {width=500;}

    Settings {
        id: settings // TODO: the settings from main.qml should be used here, this is a workaround for windows
        category: "application"

        property url recentFEModelImportFolder
    }

    GridLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        columns: 3
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Format rules file:"
        }
        FormatRulesChooser {
            id: formatRulesChooser
            Layout.fillWidth: true
            Layout.columnSpan: 2
        }

		Label {
            Layout.alignment: Qt.AlignRight
            text: "Model description file:"
        }
        TextField {
            id: modelRulesFileText
            Layout.fillWidth: true
            placeholderText: qsTr("Select model description file")
        }
        Button {
            tooltip: qsTr("Select model description file")
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectModelRulesDialog.open()
        }

        Label {
            Layout.alignment: Qt.AlignRight
            text: "Model file:"
        }
        TextField {
            id: modelFileText
            Layout.fillWidth: true
            placeholderText: qsTr("Select model file")
        }
        Button {
            tooltip: qsTr("Select model file")
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectModelDialog.open()
        }

        Label {
            Layout.alignment: Qt.AlignRight
            text: "Model units:"
        }
        LengthUnitChooser {
            id: lengthUnitChooser
        }
    }

    FileDialog {
        id: selectModelRulesDialog
        title: qsTr("Select model description...")
        nameFilters: ["pmr (*.pmr)"]
        onVisibleChanged: {
            if(visible)
				if(context.doFolderExist(settings.recentFEModelImportFolder))
					folder = settings.recentFEModelImportFolder;
        }
        onAccepted: {
            settings.recentFEModelImportFolder = folder;
            modelRulesFileText.text = selectModelRulesDialog.fileUrl
        }
    }

    FileDialog {
        id: selectModelDialog
        title: qsTr("Select model...")
        nameFilters: ["LS-Dyna (*.dyn *.k *.key)", "Pamcrash (*.pc *.inc)", "*.*"]
        onVisibleChanged: {
            if(visible)
				if(context.doFolderExist(settings.recentFEModelImportFolder))
					folder = settings.recentFEModelImportFolder;
        onAccepted: {
            settings.recentFEModelImportFolder = folder;
            modelFileText.text = selectModelDialog.fileUrl
        }
    }
}
