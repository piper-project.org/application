// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0



Dialog {
    id: root
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    property alias metadataType : settings
    property url metadataFilePath
    property string operation : "import"

    Component.onCompleted: {
        if (operation === "import")
            root.title =  "Import metadata"
        else
            root.title =  "Export metadata"
    }

    onOperationChanged: {
        if (operation === "import")
            root.title =  "Import metadata"
        else
            root.title =  "Export metadata"
    }

    Settings {
        id: settings // TODO: the settings from main.qml should be used here, this is a workaround for windows
        category: "application"

        property url recentMetadataFolder
        property bool anthropometry
        property bool entities
        property bool landmarks
        property bool controlpoint
        property bool genericmeta
        property bool joints
        property bool contacts
        property bool hbmParameter

    }
    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        RowLayout {
            Layout.fillWidth: true
            CheckBox {
                id: check_all
                text: "all metadata"
                checked: (settings.anthropometry && settings.entities && settings.landmarks &&
                          settings.controlpoint && settings.genericmeta && settings.joints && settings.contacts && settings.hbmParameter)
                onClicked: {
                    check_anthropometry.checked=checked
                    check_entities.checked=checked
                    check_landmarks.checked=checked
                    check_controlpoint.checked=checked
                    check_joints.checked=checked
                    check_contacts.checked=checked
                    check_hbmParameter.checked=checked
                    check_genericmeta.checked=checked
                }

            }
            CheckBox {
                id: check_anthropometry
                text: "anthropometry"
                checked: settings.anthropometry
                onCheckedChanged: settings.anthropometry = checked
            }
            CheckBox {
                id: check_entities
                text: "entities"
                checked: settings.entities
                onCheckedChanged: settings.entities = checked
            }
            CheckBox {
                id: check_landmarks
                text: "landmarks"
                checked: settings.landmarks
                onCheckedChanged: settings.landmarks = checked
            }
            CheckBox {
                id: check_controlpoint
                text: "controlpoint"
                checked: settings.controlpoint
                onCheckedChanged: settings.controlpoint = checked
            }
            CheckBox {
                id: check_joints
                text: "joints"
                checked: settings.joints
                onCheckedChanged: settings.joints = checked
            }
            CheckBox {
                id: check_contacts
                text: "contacts"
                checked: settings.contacts
                onCheckedChanged: settings.contacts = checked
            }
            CheckBox {
                id: check_hbmParameter
                text: "hbmParameter"
                checked: settings.hbmParameter
                onCheckedChanged: settings.hbmParameter = checked
            }
            CheckBox {
                id: check_genericmeta
                text: "named metadata"
                checked: settings.genericmeta
                onCheckedChanged: settings.genericmeta = checked
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Label {
                Layout.alignment: Qt.AlignRight
                text: "Metadata file:"
            }
            TextField {
                id: metadataFileText
                Layout.fillWidth: true
                placeholderText: qsTr("Select metadata file (*.pmr)")
            }
            Button {
                tooltip: qsTr("Metadata file (*.pmr)")
                iconSource: "qrc:///icon/document-open.png"
                onClicked: {
                    if (operation === "import")
                        metadataImportDialog.open()
                    else
                        metadataExportDialog.open()
                }
            }
        }
    }

    FileDialog {
        id: metadataExportDialog
        title: qsTr("export metadata ...")
        selectExisting: false
        nameFilters: ["Metadata files (*.pmr)"]
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentMetadataFolder))
                folder = settings.recentMetadataFolder;
        }
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentMetadataFolder = folder;
            metadataFileText.text = metadataExportDialog.fileUrl
            metadataFilePath = metadataExportDialog.fileUrl
        }
    }

    FileDialog {
        id: metadataImportDialog
        title: qsTr("import metadata ...")
        selectExisting: true
        nameFilters: ["Metadata files (*.pmr)"]
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentMetadataFolder))
                folder = settings.recentMetadataFolder;
        }
        onAccepted: {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentMetadataFolder = folder;
            metadataFileText.text = metadataImportDialog.fileUrl
            metadataFilePath = metadataImportDialog.fileUrl
        }
    }
}
