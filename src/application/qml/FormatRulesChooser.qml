// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

ComboBox {
    id: root

    property url filePath

    currentIndex: 0
    model: ListModel { // TODO: get the available format rules using Qt.labs.folderlistmodel in Qt5.5
        ListElement {text: "Formatrules_LSDyna_Fixed.pfr"}
        ListElement {text: "Formatrules_LSDyna_Separator.pfr"}
        ListElement {text: "Formatrules_PamCrash.pfr"}
        ListElement {text: "Other..."}
    }

    Component.onCompleted: {setFilePath();}

    function setFilePath() {
        if (currentText.substr(0,7) === "file://") // user defined absolute path
            filePath = currentText
        else // format rules provided with the piper application
            filePath = "file:///" + applicationDirPath + "/" + currentText;
    }

    function addRulesFilePath(path) {
        model.insert(model.count-1, {text: path.toString()})
    }

    onCurrentIndexChanged: {
        if (currentIndex==model.count-1)
            selectFormatRulesDialog.open();
        else
            setFilePath();
    }

    FileDialog {
        id: selectFormatRulesDialog
        title: qsTr("Select piper format parser...")
        nameFilters: ["pfr (*.pfr)"]
        onAccepted: {
            filePath = selectFormatRulesDialog.fileUrl
            root.addRulesFilePath(selectFormatRulesDialog.fileUrl);
        }
		onRejected: {
			root.currentIndex=0;
		}
    }
}
