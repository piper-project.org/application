// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import Piper 1.0

Dialog {
    id: root
    title: "Modules Parameters"
    standardButtons: StandardButton.Close | StandardButton.RestoreDefaults | StandardButton.Help
    visible: false

    height: 750
    width: 400

    property int helpLabelWidth : 300
    property var allHelpLabels: []

    Component {
        id: helpLabel
        Label {
            Layout.preferredWidth: root.helpLabelWidth
            Layout.column: 2
            visible: false
            wrapMode: Text.Wrap
        }
    }

    function initParameterHelp(layoutId, helpList, indexOffset) {
        var i=0;
        for (; i<helpList.length; ++i) {
            var labelItem = helpLabel.createObject(layoutId, {"text": helpList[i], "Layout.row":indexOffset+i});
            allHelpLabels.push(labelItem);
        }
        return i+indexOffset;
    }

    onReset: {
        context.setDefaultModuleParameter();
        tabPosition.item.initParameterValue();
        tabKriging.item.initParameterValue();
    }

    onHelp: {
        for (var i=0; i<allHelpLabels.length;  ++i) {
            allHelpLabels[i].visible = !allHelpLabels[i].visible;
        }
    }

    // TODO: present data to the user in a more convenient unit
    TabView {
        id: rootLayout
        anchors.fill: parent

        Tab {
            id: tabPosition
            title: "Position"
            active: true

            property var help_param_PhysPosi : [
                "Maximum number of simulation iterations (auto stop mode)",
                "Stop simulation when velocity is less than (auto stop mode)",
                "Voxel size for geometry rasterization in the 3D grid",
                "Enable voxel size scaling with HBM height",
                "HBM reference height to scale the voxel size",
                "Number of vertices considered for each collision (0 to keep them all)",
                "Add an offset to collision computation (bone, capsule, ligament)",
                "Number of affine frames sampled per articular capsule",
                "Enable the distance constraints between the patella and the tibial tuberosity",
                "Stiffness value for the fixed degrees of freedom of the <i>soft</i> robotic joints" ]

            property var help_param_PhysPosiInter : [
                "Enable bones collisions",
                "Enable frames and collisions for articular capsules and ligaments",
                "Reduce the number of voxels by merging them in <i>k</i> by <i>k</i> coarse voxel",
                "Density of affine frames spread uniformely in the flesh",
                "Enable custom affine frames defined in metadata",
                "Must be on to be able to update HBM nodes position",
                "Default target stiffness, can be set for each constraint",
                "Default spine target stiffness, can be changed in the module"
            ]

            property var help_param_PhysPosiDefo : [
                "Enable bones collisions",
                "Enable frames and collisions for articular capsules and ligaments",
                "Density of affine frames spread uniformely in the flesh",
                "Enable custom affine frames defined in metadata",
                "Default target stiffness, can be changed in the module"
            ]

            ScrollView {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 5
                function initParameterValue() {
                    param_PhysPosi_autoStopNbIterationMax.value = moduleParameter.PhysPosi_autoStopNbIterationMax;
                    param_PhysPosi_autoStopVelocity.text = moduleParameter.PhysPosi_autoStopVelocity.toExponential();
                    param_PhysPosi_voxelSize.value = moduleParameter.PhysPosi_voxelSize;
                    param_PhysPosi_scaleVoxelSize.checked = moduleParameter.PhysPosi_scaleVoxelSize;
                    param_PhysPosi_scaleVoxelSizeRefHeight.text = moduleParameter.PhysPosi_scaleVoxelSizeRefHeight;

                    param_PhysPosi_boneCollisionNbVertex.value = moduleParameter.PhysPosi_boneCollisionNbVertex;
                    param_PhysPosi_boneCollisionOffset.text = moduleParameter.PhysPosi_boneCollisionOffset.toExponential();
                    param_PhysPosi_nbAffinePerCapsule.value = moduleParameter.PhysPosi_nbAffinePerCapsule
                    param_PhysPosi_tibiaPatellaConstraintEnabled.checked = moduleParameter.PhysPosi_tibiaPatellaConstraintEnabled;
                    param_PhysPosi_softJointStiffness.text = moduleParameter.PhysPosi_softJointStiffness.toExponential();

                    param_PhysPosiInter_boneCollisionEnabled.checked = moduleParameter.PhysPosiInter_boneCollisionEnabled;
                    param_PhysPosiInter_capsuleEnabled.checked = moduleParameter.PhysPosiInter_capsuleEnabled
                    param_PhysPosiInter_voxelCoarsening.value = moduleParameter.PhysPosiInter_voxelCoarsening;
                    param_PhysPosiInter_affineDensity.value = moduleParameter.PhysPosiInter_affineDensity;
                    param_PhysPosiInter_customAffine.checked = moduleParameter.PhysPosiInter_customAffine;
                    param_PhysPosiInter_modelUpdateEnabled.checked = moduleParameter.PhysPosiInter_modelUpdateEnabled;
                    param_PhysPosiInter_targetDefaultStiffness.setValue(moduleParameter.PhysPosiInter_targetDefaultStiffness);
                    param_PhysPosiInter_spineTargetDefaultStiffness.setValue(moduleParameter.PhysPosiInter_spineTargetDefaultStiffness);

                    param_PhysPosiDefo_boneCollisionEnabled.checked = moduleParameter.PhysPosiDefo_boneCollisionEnabled;
                    param_PhysPosiDefo_capsuleEnabled.checked = moduleParameter.PhysPosiDefo_capsuleEnabled
                    param_PhysPosiDefo_affineDensity.value = moduleParameter.PhysPosiDefo_affineDensity;
                    param_PhysPosiDefo_customAffine.checked = moduleParameter.PhysPosiDefo_customAffine;
                    param_PhysPosiDefo_targetDefaultStiffness.setValue(moduleParameter.PhysPosiDefo_targetDefaultStiffness);
                }
                GridLayout {
                    id: layout_param_position
                    columns: 3
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 0
                        Layout.columnSpan: 3
                        font.bold: true
                        text: "Common"
                    }
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 1
                        text: "Max iterations:"
                    }
                    SpinBox {
                        id: param_PhysPosi_autoStopNbIterationMax
                        minimumValue: 0
                        maximumValue: 10000
                        decimals: 0
                        stepSize: 10
                        onValueChanged: { moduleParameter.PhysPosi_autoStopNbIterationMax=value; }
                    }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 2
                         text: "Stop Velocity (m/s):"
                     }
                     TextField {
                         id: param_PhysPosi_autoStopVelocity
                         implicitWidth: 50
                         readOnly: false
                         validator: DoubleValidator {bottom: 0; top:1; notation:DoubleValidator.ScientificNotation}
                         onTextChanged: {
                             if (acceptableInput)
                                 moduleParameter.PhysPosi_autoStopVelocity = parseFloat(text);
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 3
                         text: "Voxel size:"
                     }
                     SpinBox {
                         id: param_PhysPosi_voxelSize
                         minimumValue: 0
                         maximumValue: 0.1
                         decimals: 3
                         stepSize: 0.001 // 1 mm
                         suffix: "m"
                         onValueChanged: { moduleParameter.PhysPosi_voxelSize=value; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 4
                         text: "Scale voxel:"
                     }
                     Switch {
                        id: param_PhysPosi_scaleVoxelSize
                        onCheckedChanged: { moduleParameter.PhysPosi_scaleVoxelSize = checked; }
                     }
                     Label {
                         enabled: param_PhysPosi_scaleVoxelSize.checked
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 5
                         text: "Scale reference (m):"
                     }
                     TextField {
                         id: param_PhysPosi_scaleVoxelSizeRefHeight
                         enabled: param_PhysPosi_scaleVoxelSize.checked
                         implicitWidth: 50
                         readOnly: false
                         validator: DoubleValidator {bottom: 0; top:3; notation:DoubleValidator.StandardNotation}
                         onTextChanged: {
                             if (acceptableInput)
                                 moduleParameter.PhysPosi_scaleVoxelSizeRefHeight = parseFloat(text);
                         }
                     }

                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 6
                         text: "Collision vertices:"
                     }
                     SpinBox {
                         id: param_PhysPosi_boneCollisionNbVertex
                         minimumValue: 0
                         maximumValue: 10000
                         decimals: 0
                         stepSize: 1
                         value: moduleParameter.PhysPosi_boneCollisionNbVertex
                         onValueChanged: { moduleParameter.PhysPosi_boneCollisionNbVertex = parseInt(value); }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 7
                         text: "Collision offset (m):"
                     }
                     TextField {
                         id: param_PhysPosi_boneCollisionOffset
                         implicitWidth: 50
                         readOnly: false
                         validator: DoubleValidator {bottom: 0; top: 1; notation: DoubleValidator.ScientificNotation}
                         onTextChanged: {
                             if (acceptableInput)
                                 moduleParameter.PhysPosi_boneCollisionOffset = parseFloat(text);
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 8
                         text: "Frames per capsule:"
                     }
                     SpinBox {
                         id: param_PhysPosi_nbAffinePerCapsule
                         minimumValue: 1
                         maximumValue: 100
                         decimals: 0
                         stepSize: 1
                         value: moduleParameter.PhysPosi_nbAffinePerCapsule
                         onValueChanged: { moduleParameter.PhysPosi_nbAffinePerCapsule = parseInt(value); }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 9
                         text: "Tibia/Patella constraint:"
                     }
                     Switch {
                         id: param_PhysPosi_tibiaPatellaConstraintEnabled
                         onCheckedChanged: { moduleParameter.PhysPosi_tibiaPatellaConstraintEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 10
                         text: "Soft joint stiffness:"
                     }
                     StiffnessInput {
                         id: param_PhysPosi_softJointStiffness
                         onValueChanged: {
                             moduleParameter.PhysPosi_softJointStiffness = value;
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 11
                         Layout.columnSpan: 3
                         font.bold: true
                         text: "Pre-Position"
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 12
                         text: "Bone collision:"
                     }
                     Switch {
                         id: param_PhysPosiInter_boneCollisionEnabled
                         onCheckedChanged: { moduleParameter.PhysPosiInter_boneCollisionEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 13
                         text: "Articular capsule:"
                     }
                     Switch {
                         id: param_PhysPosiInter_capsuleEnabled
                         onCheckedChanged: { moduleParameter.PhysPosiInter_capsuleEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 14
                         text: "Voxel coarsening:"
                     }
                     SpinBox {
                         id: param_PhysPosiInter_voxelCoarsening
                         minimumValue: 1
                         maximumValue: 10
                         decimals: 0
                         stepSize: 1
                         value:1
                         onValueChanged: { moduleParameter.PhysPosiInter_voxelCoarsening=value; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 15
                         text: "Affine density:"
                     }
                     SpinBox {
                         id: param_PhysPosiInter_affineDensity
                         minimumValue: 0
                         maximumValue: 10000
                         stepSize: 100
                         suffix: "/m3"
                         onValueChanged: { moduleParameter.PhysPosiInter_affineDensity = value; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 16
                         text: "Custom affine:"
                     }
                     Switch {
                         id: param_PhysPosiInter_customAffine
                         onCheckedChanged: { moduleParameter.PhysPosiInter_customAffine = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 17
                         text: "Nodes update:"
                     }
                     Switch {
                         id: param_PhysPosiInter_modelUpdateEnabled
                         onCheckedChanged: { moduleParameter.PhysPosiInter_modelUpdateEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 18
                         text: "Target stiffness:"
                     }
                     StiffnessInput {
                         id: param_PhysPosiInter_targetDefaultStiffness
                         onValueChanged: {
                             moduleParameter.PhysPosiInter_targetDefaultStiffness = value;
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 19
                         text: "Spine stiffness:"
                     }
                     StiffnessInput {
                         id: param_PhysPosiInter_spineTargetDefaultStiffness
                         onValueChanged: {
                             moduleParameter.PhysPosiInter_spineTargetDefaultStiffness = value;
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 20
                         Layout.columnSpan: 3
                         font.bold: true
                         text: "Fine-Position"
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 21
                         text: "Bone collision:"
                     }
                     Switch {
                         id: param_PhysPosiDefo_boneCollisionEnabled
                         onCheckedChanged: { moduleParameter.PhysPosiDefo_boneCollisionEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 22
                         text: "Articular capsule:"
                     }
                     Switch {
                         id: param_PhysPosiDefo_capsuleEnabled
                         onCheckedChanged: { moduleParameter.PhysPosiDefo_capsuleEnabled = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 23
                         text: "Affine density:"
                     }
                     SpinBox {
                         id: param_PhysPosiDefo_affineDensity
                         minimumValue: 0
                         maximumValue: 10000
                         stepSize: 100
                         suffix: "/m3"
                         onValueChanged: { moduleParameter.PhysPosiDefo_affineDensity = value; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 24
                         text: "Custom affine:"
                     }
                     Switch {
                         id: param_PhysPosiDefo_customAffine
                         onCheckedChanged: { moduleParameter.PhysPosiDefo_customAffine = checked; }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 25
                         text: "Target stiffness:"
                     }
                     StiffnessInput {
                         id: param_PhysPosiDefo_targetDefaultStiffness
                         onValueChanged: {
                             moduleParameter.PhysPosiDefo_targetDefaultStiffness = value;
                         }
                     }
                     Item {
                         Layout.row: 26
                         Layout.fillHeight: true
                     }
                     Component.onCompleted: {
                         var paramLayoutIndex = 1;
                         paramLayoutIndex = initParameterHelp(layout_param_position, help_param_PhysPosi, paramLayoutIndex);
                         paramLayoutIndex += 1;
                         paramLayoutIndex = initParameterHelp(layout_param_position, help_param_PhysPosiInter, paramLayoutIndex);
                         paramLayoutIndex += 1;
                         paramLayoutIndex = initParameterHelp(layout_param_position, help_param_PhysPosiDefo, paramLayoutIndex);
                     }
                }
            }
        }

        Tab {
            id: tabKriging
            title: "Kriging"
            active: true
            property var help_param_Kriging : [
                "Default nugget to assign to all points when doing kriging - note that individual tools might override this setting e.g. by automatic calculation of the nugget",
                "Maximum amount of control points per box (box will be split if it has more)",
                "Overlap of split boxes (0 = no overlap, 0.49 = the two boxes will share 98% of volume)",
                "If turned on, kriging will interpolate the displacement of source and target points, otherwise it will use the position of the points. Displacement should be preferred when the deformation consists only of \"fluctuation\", i.e. only small-scale non-rigid deformation. When using geodesic distance, displacement is always interpolated, regardless of this setting.",
                "Drift part represents the rigid, affine part of the transformation created by Kriging. Automatically turned off if displacement is used as the interpolated quality.",
                "Biharmonic Distance by Yaron Lipman, Raif Rustamov, Thomas Funkhouser, published in ACM Transactions on Graphics 29, issue 3, 2010",
                "Biharmonic Distance by Yaron Lipman, Raif Rustamov, Thomas Funkhouser, published in ACM Transactions on Graphics 29, issue 3, 2010",
                "Interpolation between Green and Biharmonic computed as a (1-w(i,j)) * Green(i,j) + w(i,j) * Biharmonic(i,j), where w(i,k) = Biharmonic(i, j) / maxBiharmonic",
                "Geodesics in Heat: A New Approach to Computing Distance Based on Heat Flow by Keenan Crane and Clarisse Weischedel and Max Wardetzky, published in ACM Transactions on Graphics 32, issue 5, 2013.",
                "Lower precision reduces computation time, but if it is too low, some artifact can appear.\nThis parameter (PREC_P) is used to compute the precision (P) in conjuction with number of points in the mesh (NP) as: P = PREC / NP.\nThere are limits set to the result P: 10 < P < (NP - 1), therefore, at some point, decreasing the precision will not further decrease computation time. Does not influence Geodesic By Heat."
            ]

            ScrollView 
            {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 5
                function initParameterValue() {
                    param_Kriging_defaultNugget.text = moduleParameter.Kriging_defaultNugget;
                    param_Kriging_splitThresholdCP.text = moduleParameter.Kriging_splitThresholdCP;
                    param_Kriging_splitBoxOverlap.sliderValue = moduleParameter.Kriging_splitBoxOverlap;
                    param_Kriging_geoPrecision.text = moduleParameter.Kriging_geoPrecision;
                    param_Kriging_interpDisplacement.checked = moduleParameter.Kriging_interpolateDisplacement
                    param_Kriging_useDrift.checked = moduleParameter.Kriging_useDrift
                    var geoType = moduleParameter.Kriging_geoType;
                    if (geoType == 0)
                        param_Kriging_geoGreen.checked = true
                    else if (geoType == 1)
                        param_Kriging_geoBiharmonic.checked = true
                    else if (geoType == 2)
                        param_Kriging_geoBiharmonicGreen.checked = true
                    else if (geoType == 3)
                        param_Kriging_geoHeat.checked = true
                }
                GridLayout
                {
                    id: layout_param_kriging
                    columns: 3
                    ExclusiveGroup { id: geodesicTypes }
                    Label
                    {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 0
                        text : qsTr("Default nugget:")
                    }
                    TextField
                    {
                        id: param_Kriging_defaultNugget
                        horizontalAlignment : Text.AlignLeft
                        textColor: acceptableInput ? "black" : "red"
                        validator : DoubleValidator{ top:0; notation:DoubleValidator.ScientificNotation }
                        onTextChanged:
                        {
                            if (acceptableInput)
                                moduleParameter.Kriging_defaultNugget = parseFloat(text);
                            else
                            {
                                var a = parseFloat(text)
                                if (a == a) // NaN is not equal to itself so this is apparently the only reliable way to test for NaN http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
                                    text = -a
                                else text = 0
                            }
                        }
                    }
                    Label
                    {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 1
                        text : "Maximum control points per box:"
                    }
                    TextField
                    {
                        id: param_Kriging_splitThresholdCP
                        validator: IntValidator {bottom: 0; }
                        onTextChanged:
                        {
                            if (acceptableInput)
                                moduleParameter.Kriging_splitThresholdCP = parseInt(text);
                        }
                    }
                    LabeledSlider
                    {
                        id : param_Kriging_splitBoxOverlap
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 2
                        Layout.columnSpan : 2
                        sliderMaxValue: 0.49
                        sliderMinValue: 0.0
                        sliderStep: 0.01
                        sliderText: "Overlap of split boxes:"
                        sliderValue:  moduleParameter.Kriging_splitBoxOverlap
                        onSliderValueModified:
                        {
                            moduleParameter.Kriging_splitBoxOverlap = sliderValue
                        }
                    }
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 3
                        text: "Interpolate displacement:"
                    }
                    Switch {
                        id: param_Kriging_interpDisplacement
                        onCheckedChanged: { 
                            moduleParameter.Kriging_interpolateDisplacement = checked; 
                            if (checked)
                                param_Kriging_useDrift.checked = false; // interpolation by displacement requires drift to be off
                        }
                    }
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 4
                        text: "Use drift:"
                    }
                    Switch {
                        id: param_Kriging_useDrift
                        onCheckedChanged: { 
                            moduleParameter.Kriging_useDrift = checked; 
                            if (checked)
                                param_Kriging_interpDisplacement.checked = false; // cannot use drift with displacement interpolation
                        }
                    }
                    Label
                    {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 5
                        text : "Geodesic distance:"
                    }
                    RadioButton {
                        id: param_Kriging_geoGreen
                        Layout.row: 5
                        Layout.column: 1
                        text: "Green's"
                        exclusiveGroup: geodesicTypes
                        onCheckedChanged: if (checked) moduleParameter.Kriging_geoType = 0
                    }
                    RadioButton {
                        id: param_Kriging_geoBiharmonic
                        Layout.row: 6
                        Layout.column: 1
                        text: "Biharmonic"
                        exclusiveGroup: geodesicTypes
                        onCheckedChanged: if (checked) moduleParameter.Kriging_geoType = 1
                    }
                    RadioButton {
                        id: param_Kriging_geoBiharmonicGreen
                        Layout.row: 7
                        Layout.column: 1
                        text: "Biharmonic-Green combined"
                        exclusiveGroup: geodesicTypes
                        onCheckedChanged: if (checked) moduleParameter.Kriging_geoType = 2
                    }
                    RadioButton {
                        id: param_Kriging_geoHeat
                        Layout.row: 8
                        Layout.column: 1
                        text: "Geodesic by heat"
                        exclusiveGroup: geodesicTypes
                        onCheckedChanged: {
                            param_Kriging_geoPrecision.enabled = !checked                            
                            if (checked) moduleParameter.Kriging_geoType = 3
                        }
                    }
                    Label
                    {
                        Layout.alignment: Qt.AlignLeft
                        Layout.row: 9
                        text : "Topological distance precision:"
                    }
                    TextField
                    {
                        id: param_Kriging_geoPrecision
                        validator: DoubleValidator {bottom: 0.01; }
                        onTextChanged :
                        {
                            if (acceptableInput)
                                moduleParameter.Kriging_geoPrecision = parseFloat(text);
                        }
                    }
                    Component.onCompleted: {
                        initParameterHelp(layout_param_kriging, help_param_Kriging, 0);
                    }
                }
            }
        }

        Tab {
            id: tabShape
            title: "Shape"
            active: true

            property var help_param_Shape : [
                "Number of point handles sampled on the skin.",
                "Default stiffness for point handle targets.",
                "Skin triangular FEM Young Modulus."
            ]

            ScrollView {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 5

                function initParameterValue() {
                    param_PhysScale_nbPointHandles.value = moduleParameter.PhysScale_nbPointHandles;
                    param_PhysScale_targetDefaultStiffness.setValue(moduleParameter.PhysScale_targetDefaultStiffness);
                    param_PhysScale_skinYoungModulus.setValue(moduleParameter.PhysScale_skinYoungModulus);
                }

                GridLayout
                {
                    id: layout_param_shape
                     columns: 3
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 0
                         text : "Points handle:"
                     }
                     SpinBox {
                         id: param_PhysScale_nbPointHandles
                         minimumValue: 10
                         maximumValue: 10000
                         decimals: 0
                         stepSize: 100
                         value: moduleParameter.PhysScale_nbPointHandles
                         onValueChanged: { moduleParameter.PhysScale_nbPointHandles = parseInt(value); }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 1
                         text : "Default target stiffness:"
                     }
                     StiffnessInput {
                         id: param_PhysScale_targetDefaultStiffness
                         onValueChanged: {
                             moduleParameter.PhysScale_targetDefaultStiffness = value;
                         }
                     }
                     Label {
                         Layout.alignment: Qt.AlignLeft
                         Layout.row: 2
                         text : "Skin young modulus:"
                     }
                     StiffnessInput {
                         id: param_PhysScale_skinYoungModulus
                         onValueChanged: {
                             moduleParameter.PhysScale_skinYoungModulus = value;
                         }
                     }
                }
                Component.onCompleted: {
                    initParameterHelp(layout_param_shape, help_param_Shape, 0);
                }
            }
        }
    }
    onVisibleChanged: {
        if (visible) {
            tabPosition.item.initParameterValue();
            tabKriging.item.initParameterValue();
            tabShape.item.initParameterValue();
        }
    }
}
