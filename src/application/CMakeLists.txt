project(piper-app)

find_package(Qt5 5.6 COMPONENTS Core Widgets Qml Quick Concurrent REQUIRED)
set(CMAKE_AUTOMOC ON)

find_package(Boost COMPONENTS system filesystem thread REQUIRED) # thread added since it is necessary at install time
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS}) # should not be required ${Boost_LIBRARY_DIR}, according to the doc

find_package(PythonLibs 2.7 EXACT REQUIRED)
include_directories(${PYTHON_INCLUDE_PATH})

find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

include_directories(.)

add_subdirectory(common)
add_subdirectory(test)

set(HEADER_FILES
    module/check/Check.h
    module/anthropometry/AnthropometryModule.h
    module/kriging/Kriging.h
	module/kriging/KrigingModule.h    
    module/kriging/RegisterSurfaces.h
    module/scalingParameter/ScalingParameter.h
    module/meshOptimizing/meshOptimizing.h
    module/bodyDimTargetGen/BodyDimTargetGen.h
    EnvManager.h
    QTargetLoader.h
    module/BodySectionPersonalizing/BodySectionPersonalizing.h
    module/BodySectionPersonalizing/AnthropoModelDisplay.h
    module/BodySectionPersonalizing/AnthropoTree.h
    module/BodySectionPersonalizing/AnthropoTreeItem.h
    module/contourDeformation/ContourDeformation.h
    module/contourDeformation/ContourGen.h
    module/contourDeformation/TargetSynth.h
    module/contourPersonalization/ContourPersonalization.h
)

set(SOURCE_FILES
    main.cpp
    module/check/Check.cpp
    module/anthropometry/AnthropometryModule.cpp
    module/kriging/Kriging.cpp
    module/kriging/KrigingModule.cpp    
    module/kriging/RegisterSurfaces.cpp
    module/scalingParameter/ScalingParameter.cpp
    module/meshOptimizing/meshOptimizing.cpp
    module/bodyDimTargetGen/BodyDimTargetGen.cpp
    EnvManager.cpp
    QTargetLoader.cpp
    module/BodySectionPersonalizing/BodySectionPersonalizing.cpp
    module/BodySectionPersonalizing/AnthropoModelDisplay.cpp
    module/BodySectionPersonalizing/AnthropoTree.cpp
    module/BodySectionPersonalizing/AnthropoTreeItem.cpp
    module/contourDeformation/ContourDeformation.cpp
    module/contourDeformation/ContourGen.cpp
    module/contourDeformation/TargetSynth.cpp
	module/contourPersonalization/ContourPersonalization.cpp
)

set(QML_FILES
    qml/main.qml
    qml/ImportExportMetadataDialog.qml
    qml/FormatRulesChooser.qml
    qml/LengthUnitChooser.qml
    qml/RunScriptDialog.qml
    qml/ModuleAction.qml
#
# Piper qml module
#
    qml/Piper/ModelHistoryNameDialog.qml
    qml/Piper/ModuleButton.qml
    qml/Piper/ModuleLayout.qml
    qml/Piper/ModuleToolWindow.qml
    qml/Piper/ModuleToolWindowButton.qml
    qml/Piper/MouseInteractorToolButton.qml
    qml/Piper/DefaultVtkViewer.qml
    qml/Piper/LabeledSlider.qml
    qml/Piper/PickersOptionWindow.qml
    qml/Piper/BlankingOptionWindow.qml
    qml/Piper/DisplayGroupBox.qml
    qml/Piper/DisplayContourWindow.qml
    qml/Piper/DisplayEntityWindow.qml
    qml/Piper/DisplayFrameWindow.qml
    qml/Piper/DisplayGenericMetadataWindow.qml
    qml/Piper/DisplayLandmarkWindow.qml
    qml/Piper/DisplayJointWindow.qml
    qml/Piper/DisplayTargetWindow.qml
    qml/Piper/DisplayConfigurationsWindow.qml
    qml/Piper/DisplayControlPointWindow.qml
    qml/Piper/EditorContourWindow.qml
    qml/Piper/EditorEntityWindow.qml
    qml/Piper/EditorFrameWindow.qml
    qml/Piper/EditorGenericMetadataWindow.qml
    qml/Piper/EditorLandmarkWindow.qml
    qml/Piper/EditorJointWindow.qml
    qml/Piper/ExportMetadataWindow.qml
    qml/Piper/ContourCLWindow.qml
    qml/Piper/ExportMetadataWindow.qml
    qml/Piper/CompleteContourCLWindow.qml
    qml/Piper/ColorPicker.qml
    qml/Piper/QualityColorationWindow.qml
    qml/Piper/EntityTreeBrowser.qml
    qml/Piper/LandmarkTreeBrowser.qml
    qml/Piper/OctaveScriptChooser.qml
    qml/Piper/DispSettingsGroupBox.qml
    qml/Piper/VtkViewerCommonTools.qml
    qml/Piper/VtkViewerCommonToolsWindow.qml
    qml/Piper/DetachableVtkViewerCommonTools.qml
    qml/Piper/SpinSliderComponent.qml
    qml/Piper/StiffnessInput.qml
    qml/Piper/ToolTip.qml
    qml/Piper/TargetInfo.qml
    qml/Piper/MouseToolSelection.qml
    qml/Piper/ClippingPlaneWindow.qml
    qml/Piper/AnthropoModelDisplayGUI.qml
    qml/Piper/BetaLabel.qml
    qml/Piper/ConfidenceLabel.qml
    qml/Piper/GroupBoxNoTitle.qml
    qml/Piper/MetaEditorGroupBox.qml
    qml/Piper/qmldir
#
# PiperSofa qml module
#
    qml/PiperSofa/ReloadButton.qml
    qml/PiperSofa/EnvironmentUpdater.qml
    qml/PiperSofa/qmldir

    module/check/Check.qml
	module/child/Child.qml
#    module/check/SetOctaveFilesWindow.qml
#
# physics positioning
#   
    module/sofaPositioning/sofaPositioning.qml
    module/sofaPositioning/sofaDeforming.qml
#    module/sofaPositioning/sofaPositioningFEM.qml
    module/sofaPositioning/dof.js
    module/sofaPositioning/DofController.qml
    module/sofaPositioning/FixedBoneToolWindow.qml
    module/sofaPositioning/JointToolWindow.qml
    module/sofaPositioning/LandmarkToolWindow.qml
    module/sofaPositioning/FrameToolWindow.qml
    module/sofaPositioning/SpineToolWindow.qml
    module/sofaPositioning/SpineToolWindowForm.ui.qml
    module/sofaPositioning/ControlToolWindow.qml
    module/sofaPositioning/VisualizationToolWindow.qml
    module/sofaPositioning/TargetToolWindow.qml
    module/sofaPositioning/DeformationTargetToolWindow.qml
    module/sofaPositioning/TargetLoad.qml
    module/sofaPositioning/UpdateModel.qml
    module/sofaPositioning/PredictorToolWindow.qml
#
# Physics scaling
#
    module/sofaScaling/sofaScaling.qml
    module/sofaScaling/SkinNodeToolWindow.qml
    module/sofaScaling/ControlToolWindow.qml
    module/sofaScaling/DisplayToolWindow.qml
    module/sofaScaling/UpdateModel.qml

    module/kriging/Krigingdeformation.qml
    module/kriging/KrigingModule.qml
	module/kriging/TargetPoints.qml
    module/kriging/SourcePoints.qml
    module/kriging/RegisterSurfaces.qml
	module/kriging/ImportControlPointsDialog.qml
    module/kriging/ComboTableComponent.qml
    module/kriging/ComboTableTargetComponent.qml
    module/scalingParameter/scalingParameter.qml
    module/scalingParameter/ValuesTable.qml
    module/meshOptimizing/meshOptimizing.qml
    module/bodyDimTargetGen/BodyDimTargetGenDialog.qml
    qml/ImportEnvModelDialog.qml
    qml/ModuleParameter.qml
    module/BodySectionPersonalizing/AnthropoComponent.qml
    module/BodySectionPersonalizing/AnthropoModelTree.qml
    module/BodySectionPersonalizing/BodySectionPersonalizing.qml
    module/BodySectionPersonalizing/PersoSourceTargetDialog.qml
    module/BodySectionPersonalizing/ComboTableComponent.qml
    module/BodySectionPersonalizing/DimensionTypeSection.qml
    module/BodySectionPersonalizing/DimensionTypeSegment.qml
    module/BodySectionPersonalizing/ControlPointsNameDialog.qml
    module/BodySectionPersonalizing/TargetDimension.qml
    module/BodySectionPersonalizing/BodyDimension.qml
    module/BodySectionPersonalizing/BodySection.qml
    module/BodySectionPersonalizing/BodySegment.qml
    module/contourDeformation/contourDeformation.qml	
    module/contourDeformation/PersonalizationWindow.qml
    module/contourDeformation/RepositioningWindow.qml
    module/contourPersonalization/ContourPersonalization.qml
    module/contourPersonalization/TargetsWindow.qml	
    module/contourPersonalization/ContourCLWindow.qml
    module/meshOptimizing/MesquiteOptionWindow.qml
    module/meshOptimizing/CreaseDetectionOptionWindow.qml
    module/meshOptimizing/SurfaceSmoothingOptionWindow.qml
    module/meshOptimizing/KrigingInABoxOptionWindow.qml
    module/meshOptimizing/BaseModelSelectGroupBox.qml    
	module/meshOptimizing/LocalAvgSmoothWindow.qml
    module/anthropometry/AnthroPerso.qml
    module/anthropometry/AnthroPersoTool.qml
    module/anthropometry/AnthroPersoChild.qml
    module/anthropometry/AnthroPersoAdultDeformation.qml
)

set(SOFA_SCENES
    module/sofaPositioning/scenePreparePositioning.py
    module/sofaPositioning/scenePositioning.py
    module/sofaPositioning/scenePrepareDeforming.py
#    module/sofaPositioning/scenePositioningFEM.py
    module/sofaPositioning/interactors.py
    module/sofaPositioning/sceneCommon.py
    module/empty.scn
    module/sofaScaling/sceneScaling.py
)

set(QRC_FILES
    resource.qrc
)

find_package(SofaSimulation REQUIRED)
find_package(SofaPython REQUIRED)

get_filename_component(SOFA_INSTALL_PREFIX "${SofaSimulation_DIR}/../../.." ABSOLUTE)
message(STATUS "Found Sofa in ${SOFA_INSTALL_PREFIX}")

qt5_add_resources(RESOURCE_FILES ${QRC_FILES})

add_executable(piper ${HEADER_FILES} ${SOURCE_FILES} ${QRC_FILES} ${RESOURCE_FILES} ${QML_FILES})
target_link_libraries(piper tinyxml2 ${Boost_LIBRARIES} ${PYTHON_LIBRARIES} ${VTK_LIBRARIES} ${Mesquite_LIBRARIES} Qt5::Core Qt5::Widgets Qt5::Qml Qt5::Quick SofaSimulationCommon SofaPiper SofaPython hbm pipercommon KrigingDeformation anatomyDB meshoptimizer BodyDimDB contours AnthropoModel vtkPIPERFilters)

if(SWIG_FOUND AND PYTHONLIBS_FOUND)
    add_dependencies(piper _app _hbm _sofa _anatomyDB) # cmake variables ${SWIG_MODULE_hbm_REAL_NAME} shoud be used, but are not defined here
endif()

install(TARGETS piper RUNTIME DESTINATION bin) # BUNDLE DESTINATION . COMPONENT Runtime - I think it is ony for macosx bundle

file(GLOB PIPER_DTD_PATH
        "${CMAKE_SOURCE_DIR}/../share/*.dtd" "${CMAKE_SOURCE_DIR}/../share/*.ent")
get_filename_component(PIPER_DTD_PATH "${PIPER_DTD_PATH}" ABSOLUTE)
file(GLOB FORMAT_RULES_PATH
        "${CMAKE_SOURCE_DIR}/../share/formatrules/*.pfr") 
get_filename_component(FORMAT_RULES_PATH "${FORMAT_RULES_PATH}" ABSOLUTE)

# copy octave scripts to the build dir
file(COPY ${PIPER_OCTAVE_SCRIPT_PATH} DESTINATION "${PIPER_BUILD_SHARE_DIR}/octave")

# copy python scripts to the build dir
file(COPY ${PIPER_PYTHON_SCRIPT_PATH} DESTINATION "${PIPER_BUILD_SHARE_DIR}/python")

# install octave scripts
install(DIRECTORY ${PIPER_OCTAVE_SCRIPT_PATH} DESTINATION share/octave)

# install python scripts
install(DIRECTORY ${PIPER_PYTHON_SCRIPT_PATH} DESTINATION share/python)

if(NOT MSVC_IDE)
    # sofa scene: copy to build tree
    foreach(SCENE ${SOFA_SCENES})
        get_filename_component(TARGET_NAME ${SCENE} NAME)
        add_custom_target(copy_${TARGET_NAME}
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${SCENE} ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
            VERBATIM
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            SOURCES ${SCENE}
            COMMENT "Copy sofa scene: ${SCENE} to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}"
        )
        add_dependencies(piper copy_${TARGET_NAME})
    endforeach(SCENE ${SOFA_SCENES})
    # sofa scene: install
    file(COPY ${PIPER_DTD_PATH} DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
    file(COPY ${FORMAT_RULES_PATH} DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
endif(NOT MSVC_IDE)

if(MSVC_IDE)
    add_dependencies(piper copyAnatomyDb)
	add_dependencies(piper copyBodyDimDB)
endif()

install(FILES ${SOFA_SCENES} DESTINATION bin)
install(FILES ${PIPER_DTD_PATH} DESTINATION bin)
install(FILES ${FORMAT_RULES_PATH} DESTINATION bin)

#installing xmllint
if(WIN32)
	INSTALL(DIRECTORY "${XMLLINT_DIR}/" DESTINATION bin COMPONENT Runtime)
endif(WIN32)


#copying DTD and SOFA SCENES + DLLS to VS directory to ease debugging
if(MSVC_IDE)
    SET(FILES_TOCOPY ${PIPER_DTD_PATH} ${FORMAT_RULES_PATH})
    foreach( FILES_TOCOPY_i ${FILES_TOCOPY})
        ADD_CUSTOM_COMMAND(
            TARGET piper
            POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ${FILES_TOCOPY_i}
            "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIGURATION>/"
            )
    endforeach(FILES_TOCOPY_i)
	set(SOFA_SCENES_FULLPATH)
    foreach(sofa_scenes_i ${SOFA_SCENES})
	    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${sofa_scenes_i})
		    LIST(APPEND SOFA_SCENES_FULLPATH ${CMAKE_CURRENT_SOURCE_DIR}/${sofa_scenes_i})
	    endif()
    endforeach()
    foreach(SCENE ${SOFA_SCENES_FULLPATH})
		get_filename_component(TARGET_NAME ${SCENE} NAME)
		add_custom_target(copy_${TARGET_NAME}
			COMMAND ${CMAKE_COMMAND} -E copy_if_different ${SCENE} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIGURATION>/"
			WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
			SOURCES ${SCENE}
			COMMENT "Copy sofa scene: ${SCENE}"
		)
		add_dependencies(piper copy_${TARGET_NAME})
    endforeach(SCENE ${SOFA_SCENES_FULLPATH})
endif(MSVC_IDE)


if(WIN32)
    #setting MSVC project properties for Environment variable (to ease setting project debugging environment)
    if(MSVC_IDE)
        foreach(CONFIGURATION ${CMAKE_CONFIGURATION_TYPES})
            file(COPY "${XMLLINT_DIR}/xmllint.exe" DESTINATION "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CONFIGURATION}/")
            set (PATH_WINDOWS $ENV{PATH})
            STRING(REPLACE ";" "\\;" PATH_WINDOWS "${PATH_WINDOWS}")
            set (PATH_WINDOWS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CONFIGURATION}/\;${XMLLINT_DIR}\;${Mesquite_BINARY_DIR}\;${Boost_LIBRARY_DIRS}\;${VTK_INSTALL_PREFIX}/bin\;${_qt5Core_install_prefix}/bin\;${SOFA_ROOT}/bin/;${OCTAVE_ROOT_DIR}/bin/;${PATH_WINDOWS};") #escape semicolon in the string, otherwise only the first path from the semicolon separated list would be taken into account
            if (QASSISTANT_EXECUTABLE)
                file(COPY ${QASSISTANT_EXECUTABLE} DESTINATION "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CONFIGURATION}/")
            endif (QASSISTANT_EXECUTABLE)
        endforeach()
        SET(USERFILE_ENVIRONMENT "PATH=${PATH_WINDOWS}/;
            QML_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
            QML2_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
            QT_QPA_PLATFORM_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/platforms
            QT_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/
            SOFA_ROOT=${SOFA_ROOT}
            PYTHONPATH=${PIPER_PACKAGE_DIR}/Python27/
            PYTHONHOME=${PIPER_PACKAGE_DIR}/Python27/
            PYTHONUSERBASE=${PIPER_PACKAGE_DIR}/Python27/")
		
        # Configure the template file
        SET(USER_FILE piper.vcxproj.user)
        SET(OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/${USER_FILE})
        CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/piper.vcxproj.user.in ${USER_FILE} @ONLY)
    else()
        SET(USERFILE_ENVIRONMENT "SET PATH=%PATH%;${Mesquite_BINARY_DIR}/;${meshoptimizer_BINARY_DIR}/$(CONFIGURATION)/;${PYTHON_DIR};${PYTHON_DIR}/DLLs/;${SOFA_ROOT}/bin/;${tinyxml2_BINARY_DIR}/;${sofaTools_BINARY_DIR};${hbm_BINARY_DIR}/;${anatomyDB_BINARY_DIR}/$(CONFIGURATION)/;${piper-common_BINARY_DIR}/;${XMLLINT_DIR};${QT_LIBRARY_DIRS};${QT_LIBRARY_DIR};${QT_BINARY_DIR};${_qt5Core_install_prefix}/bin;${Boost_LIBRARY_DIRS};${Boost_LIBRARY_DIR};${VTK_INSTALL_PREFIX}/bin;
        SET QML_IMPORT_PATH=${_qt5Core_install_prefix}/qml/ 
        SET QML2_IMPORT_PATH=${_qt5Core_install_prefix}/qml/ 
        SET QT_QPA_PLATFORM_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/platforms 
        SET QT_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/
        SET PYTHONPATH=${PYTHON_DIR}/
        SET PYTHONHOME=${PYTHON_DIR}/
        SET PYTHONUSERBASE=${PYTHON_DIR}/")
        CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/setenv.bat.in ${CMAKE_BINARY_DIR}/setenv.bat @ONLY)
        file(COPY "${XMLLINT_DIR}/xmllint.exe" DESTINATION "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/")
    endif()
endif()

# dependencies installation

if(WIN32)
    include(${CMAKE_SOURCE_DIR}/cmake/installWindows.cmake)
elseif(UNIX)
    include(${CMAKE_SOURCE_DIR}/cmake/installUnix.cmake)
endif(WIN32)
