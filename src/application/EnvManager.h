/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "common/Context.h"
#include <QObject>
#include <QString>
#include <QUrl>
#include <QtCore>
#include <QtWidgets>


namespace piper {
    namespace envmanager {
        
    struct ModelParameterItem {
        ModelParameterItem(QString const& name, QUrl const& file): m_name(name), m_file(file) {}
        QString m_name;
        QUrl m_file;
    };

    class TableEnvModel : public QAbstractListModel 
    {
        Q_OBJECT
    public:
	    TableEnvModel( QObject *parent = 0): QAbstractListModel (parent) {}
        TableEnvModel( const std::map<std::string, std::string>& mapvalues, QObject *parent = 0);
	    //implement abstract method of QAbstractTableModel
	    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	    Q_INVOKABLE QVariantMap get(int idx) const;
	    int rowCount(const QModelIndex &parent) const;
	    int columnCount(const QModelIndex &parent) const;
	    QHash<int, QByteArray> roleNames() const;
	    enum Roles {
            NameRole = Qt::UserRole + 1,
            ValueRole
	    };
	    bool setData(const QModelIndex &index, const QVariant &value, int role);
	    Q_INVOKABLE bool setData(int row, int column, const QVariant value)
	    {
		    int role = Qt::UserRole + 1 + column;
		    return setData(index(row,0), value, role);
	    }
	    Qt::ItemFlags flags(const QModelIndex &index) const;


	    void construct( const std::map<std::string, std::string>& mapvalues);
	    void clear();
        QList<ModelParameterItem*> items;
    };



    class EnvManager : public QObject
    {
        Q_OBJECT
	    Q_PROPERTY(TableEnvModel* m_tablemodel READ getModel)

    public:
        EnvManager();
	    Q_INVOKABLE TableEnvModel* getModel( ) ;

	    Q_INVOKABLE QString defaultName( ) const ; 

	    Q_INVOKABLE void addEnvModel(const QString& name, const QUrl& envmodelFile, const QUrl& formatRuleFile, const QString& lengthUnit);
	    Q_INVOKABLE void delEnvModel( const QString& name); 
        
        /// <summary>
        /// Sets the scale transformation factors for a given environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <param name="x">Scaling factor for x-axis.</param>
        /// <param name="y">Scaling factor for y-axis.</param>
        /// <param name="z">Scaling factor for z-axis.</param>
        Q_INVOKABLE void setScale(const QString& name, double x, double y, double z);

        /// <summary>
        /// Sets the translation distances for a given environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <param name="x">Translation distance along x-axis.</param>
        /// <param name="y">Translation distance along y-axis.</param>
        /// <param name="z">Translation distance along z-axis.</param>
        Q_INVOKABLE void setTranslation(const QString& name, double x, double y, double z);

        /// <summary>
        /// Sets the rotation for a given environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <param name="x">Rotation around x-axis, in degrees.</param>
        /// <param name="y">Rotation around y-axis, in degrees.</param>
        /// <param name="z">Rotation around z-axis, in degrees.</param>
        Q_INVOKABLE void setRotation(const QString& name, double x, double y, double z);

        /// <summary>
        /// The scale transformation of a specified environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <returns>3 scale factors - for X, Y and Z axis on indices 0, 1, 2 respectively of the returned array.</returns>
        Q_INVOKABLE QVariantList getScale(const QString& name);

        /// <summary>
        /// The scale transformation of a specified environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <returns>3 translation distances - for X, Y and Z axis on indices 0, 1, 2 respectively of the returned array.</returns>
        Q_INVOKABLE QVariantList getTranslation(const QString& name);

        /// <summary>
        /// The scale transformation of a specified environment model.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <returns>3 rotation angles - around X, Y and Z axis on indices 0, 1, 2 respectively of the returned array. In degrees.</returns>
        Q_INVOKABLE QVariantList getRotation(const QString& name);

        /// <summary>
        /// Changes the visibility of a specified environment model in the Context's VtkDisplay.
        /// </summary>
        /// <param name="name">The name of the environment model.</param>
        /// <param name="visible">If set to <c>true</c>, the specified model will be visible.</param>
        Q_INVOKABLE void envVisible(const QString& name, bool visible);

    public slots:
	    void update();

    signals:
        void defaultNameChanged();
        void modelChanged();
	    void datachanged();

    private:
        QString m_info;

	    std::map<std::string,std::string> m_mapfile;
	    TableEnvModel* m_tablemodel;
	    int m_count;
	    QString m_defaultname;
	    void updateDefaultName();

    };

    }
}

Q_DECLARE_METATYPE(piper::envmanager::TableEnvModel*)
