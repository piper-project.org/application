// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2

import SofaApplication 1.0
import SofaInteractors 1.0
import SofaManipulators 1.0

import Piper 1.0
import "dof.js" as DofConstant

ModuleToolWindow {
    id: root
    title: "Spine Controller"

    // current selected spline control point
    property int currentSplineCtrlpIndex: -1

    property Action interactor: Action {
        checkable: true
        checked: false
        enabled: false
        iconSource: "qrc:/icon/cursor.png"
        text: "S"
        tooltip: "Spine interactor"
        property string leftButtonHelp: "click on a spline control point and move it by dragging the arrows"
        property string middleButtonHelp: cameraInteractor.middleButtonHelp
        property string rightButtonHelp: cameraInteractor.rightButtonHelp
        onToggled: {
            sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "setShowCtrlp", checked);
            currentSplineCtrlpIndex=-1;
            if (checked)
                SofaApplication.interactorComponent = selectSplineCtrlp;
        }
    }

    property Component selectSplineCtrlp: UserInteractor_MoveCamera {
        hoverEnabled: true

        function init() {
            moveCamera_init();

            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                var selectableParticle = sofaViewer.pickParticleWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["spine_ctrlp"]);
                if(selectableParticle && selectableParticle.sofaComponent) {
                    sofaScene.selectedComponent = selectableParticle.sofaComponent;
                    root.currentSplineCtrlpIndex = selectableParticle.particleIndex;
                }
                else {
                    var selectable = sofaViewer.pickObjectWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["manipulator"]);
                    if(selectable && selectable.manipulator) {
                        sofaScene.selectedManipulator = selectable.manipulator;
                        if(sofaScene.selectedManipulator.mousePressed)
                            sofaScene.selectedManipulator.mousePressed(mouse, sofaViewer);
                        if(sofaScene.selectedManipulator.mouseMoved)
                            setMouseMovedMapping(sofaScene.selectedManipulator.mouseMoved);
                    }
                    else {
                        sofaScene.selectedComponent = null;
                        sofaScene.selectedManipulator = null;
                        root.currentSplineCtrlpIndex = -1;
                    }
                }
            });

            addMouseReleasedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                if(sofaScene.selectedManipulator && sofaScene.selectedManipulator.mouseReleased)
                    selectedManipulator.mouseReleased(mouse, sofaViewer);
                sofaScene.selectedManipulator = null;
                setMouseMovedMapping(null);
            });
        }
    }

    Component {
        id: manipulatorComponent

        Manipulator {
            id: manipulator

            property var sofaScene
            property int index: -1

            Manipulator3D_Translation {
                id: tx
                visible: manipulator.visible
                axis: "x"
                onPositionChanged: manipulator.position = position;
            }

            Manipulator3D_Translation {
                id: ty
                visible: manipulator.visible
                axis: "y"
                onPositionChanged: manipulator.position = position;
            }

            Manipulator3D_Translation {
                id: tz
                visible: manipulator.visible
                axis: "z"
                onPositionChanged: manipulator.position = position;
            }

            function getPosition() {
                var positionArray = sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "getCtrlpPosition", index);
                position = Qt.vector3d(positionArray[0], positionArray[1], positionArray[2]);
            }

            function setPosition() {
                sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "setCtrlpPosition", index, [position.x, position.y, position.z]);
            }

            Component.onCompleted: getPosition()

            onVisibleChanged: {
                if (visible) getPosition();
            }

            onPositionChanged: setPosition()
        }
    }

    function init() {
        SofaApplication.addInteractor("Select spline knot", selectSplineCtrlp);
        form.stiffness.setValue(moduleParameter.PhysPosiInter_spineTargetDefaultStiffness);
    }

    function setDefaultVertebraSelection() {
        vertebraTableView.selection.clear();
        vertebraTableView.selection.select(0);
        vertebraTableView.selection.select(13);
        vertebraTableView.selection.select(23);
    }

    function updateVertebraSelectedText() {
        form.selectedVertebraText.text="";
        vertebraTableView.selection.forEach( function(rowIndex) {form.selectedVertebraText.insert(form.selectedVertebraText.length, vertebraList.get(rowIndex)["vertebra"]+" ");} );
    }

    SpineToolWindowForm {
        id: form
        anchors.fill: parent

        cameraInteractorButton.action: cameraInteractor
        spineInteractorButton.action: root.interactor
        addButton.enabled: vertebraTableView.selection.count >= 2 && !removeButton.enabled
        removeButton.enabled: false

        ListModel {
            id: vertebraList
        }

        function updateDistance() {
            var distance = sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "getSpineDistanceToTarget");
            form.cumDistanceItem.text = distance[0].toFixed(DofConstant.dofNbDecimals[context.modelLengthUnit]-1)
            form.maxDistanceItem.text = distance[1].toFixed(DofConstant.dofNbDecimals[context.modelLengthUnit]-1)
        }
        Connections {
            target: sofaScene
            onStepEnd : form.updateDistance()
            onReseted : form.updateDistance()
        }

        selectVertebra.onClicked: selectVertebraDialog.open()

        addButton.onClicked: {
            var initPoints = [];
            vertebraTableView.selection.forEach( function(rowIndex) {initPoints.push(vertebraList.get(rowIndex)["vertebra"]);} );
            var controllerCreated = sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "addController", initPoints);
            if (!controllerCreated) return;
            form.updateDistance();
            sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "setCompliance", 1./stiffness.value);
            for(var i = 0; i < sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "getNbCtrlp"); ++i) {
                sofaScene.addManipulator(manipulatorComponent.createObject(root, {objectName:"spineCtrlp", sofaScene: sofaScene, index: i, visible: Qt.binding(function () {return this.index === root.currentSplineCtrlpIndex;})}));
            }
            removeButton.enabled=true;
            interactor.enabled=true;
        }

        removeButton.onClicked: {
            sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "removeController");
            currentSplineCtrlpIndex=-1;
            var removedManipulators = sofaScene.removeManipulatorByName("spineCtrlp");
            for(var i = 0; i < removedManipulators.length; ++i)
                removedManipulators[i].destroy();
            removeButton.enabled=false;
            if (interactor.checked)
                cameraInteractor.trigger()
            interactor.enabled=false
        }

        defaultButton.onClicked: setDefaultVertebraSelection()


        stiffness.onValueChanged: {
            if (sofaScene.ready)
                sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "setCompliance", 1./stiffness.value);
        }
    }

    Dialog {
        id: selectVertebraDialog
        title: "Select spline control points"
        standardButtons: StandardButton.Close
        visible: false
        TableView {
            id: vertebraTableView
            anchors.fill: parent
            TableViewColumn {
                role: "vertebra"
                title: "Vertebra"
            }
            alternatingRowColors: true
            selectionMode: SelectionMode.MultiSelection
            headerVisible: false
        }
    }

    Connections {
        target: vertebraTableView.selection
        onSelectionChanged: updateVertebraSelectedText()
    }

    Component.onCompleted: {
        var vertebra = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11", "T12", "L1", "L2", "L3", "L4", "L5"]; // TODO display only vertebra available in the current HBM
        for(var i = 0; i<vertebra.length; ++i) {
            vertebraList.append({"vertebra": vertebra[i]})
        }
        vertebraTableView.model = vertebraList;
        setDefaultVertebraSelection();
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }


}
