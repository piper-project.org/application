// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQml 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0

import SofaBasics 1.0
import SofaScene 1.0

import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Display"

    function init() {
        sofaScene.sofaPythonInteractor.call("displayController", "setShowSkinOpacity", skinOpacity.value);
        // start with capsules mesh displayed only if they are simulated
        if (physPositioningInteractiveAction.text === currentModuleName)
            capsuleCheckBox.checked = moduleParameter.PhysPosiInter_capsuleEnabled;
        else
            capsuleCheckBox.checked = moduleParameter.PhysPosiDefo_capsuleEnabled;
    }

    function setLandmarkVisible(visible) {
        landmarkItem.checked = visible;
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed()
    }

    Settings {
        id: settings
        category: "sofaViewer"
        property double skinOpacity: 0.3
    }

    Component.onDestruction: {
        settings.skinOpacity = skinOpacity.value;
    }

    ColumnLayout {
        GroupBox {
            Layout.fillWidth: true
            title: "Entity"
            GridLayout {
                columns: 2
                CheckBox {
                    text: "Bones"
                    partiallyCheckedEnabled: true
                    checkedState: Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowBones", checkedState)
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide bones mesh"
                    }
                }
                CheckBox {
                    id: capsuleCheckBox
                    text: "Capsule"
                    partiallyCheckedEnabled: true
                    checkedState:  Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShow", "capsule", checkedState);
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide capsules mesh"
                    }
                }
                CheckBox {
                    text: "Ligament"
                    partiallyCheckedEnabled: true
                    checkedState: Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShow", "ligament", checkedState);
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide ligaments mesh"
                    }
                }
                CheckBox {
                    text: "Environment"
                    checked: true
                    onCheckedChanged: sofaScene.setDataValue("@/environment.activated", checked);
                    ToolTip {
                        text: "Show/Hide environment mesh"
                    }
                }
                CheckBox {
                    text: "Skin"
                    partiallyCheckedEnabled: true
                    checkedState: Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShow", "skin", checkedState);
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide skin mesh"
                    }
                }
                RowLayout {
                    Label { text: "opacity:" }
                    SpinBox {
                        id: skinOpacity
                        minimumValue: 0
                        maximumValue: 1
                        decimals: 1
                        stepSize: 0.1
                        value: settings.skinOpacity
                        onValueChanged: {
                            if (sofaScene.ready)
                                sofaScene.sofaPythonInteractor.call("displayController", "setShowSkinOpacity", value);
                        }
                        ToolTip {
                            text: "Set skin opacity (0:transparent, 1:opaque)"
                        }
                    }
                }
            }
        }
        GroupBox {
            Layout.fillWidth: true
            title: "Model"
            GridLayout {
                columns: 2
                CheckBox {
                    text: "Anatomical frames"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowAnatomicalFrames", checked)
                    ToolTip {
                        text: "Show/Hide ISB anatomical frames computed from the landmarks"
                    }
                }
                CheckBox {
                    text: "Joint frames"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowJointsOffset", checked)
                    ToolTip {
                        text: "Show/Hide the robotic joint frames (2 frames per joint)"
                    }
                }
                CheckBox {
                    text: "World frames"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowWorldFrames", checked)
                    ToolTip {
                        text: "Show/Hide the world origin frame"
                    }
                }
                CheckBox {
                    id: landmarkItem
                    text: "Landmarks"
                    checked: false
                    onCheckedChanged: sofaScene.sofaPythonInteractor.call("displayController", "setShowLandmarks", checked)
                    ToolTip {
                        text: "Show/Hide the available landmarks"
                    }
                }

            }
        }
        GroupBox {
            Layout.fillWidth: true
            title: "Simulation"
            GridLayout {
                columns: 2
                CheckBox {
                    text: "Bone frames"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowBonesRigids", checked)
                    ToolTip {
                        text: "Show/Hide all bones rigid frame (inertia frame)"
                    }
                }
                CheckBox {
                    text: "Soft tissue affines"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowSofTissueAffines", checked)
                    ToolTip {
                        text: "Show/Hide all affine frames (sampled in the flesh, custom frames, capsule frames)"
                    }
                }
                CheckBox {
                    text: "Contact violation"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowSlidingContact", checked)
                    ToolTip {
                        text: "Show/Hide line segments representing sliding contacts distance violation"
                    }
                }
                CheckBox {
                    text: "Normals"
                    checked: false
                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowNormals", checked)
                    ToolTip {
                        text: "Show/Hide nodes normal of the displayed meshes"
                    }
                }

//                CheckBox {
//                    text: "Integration points"
//                    checked: false
//                    onClicked: sofaScene.sofaPythonInteractor.call("displayController", "setShowGaussPoint", checked)
//                }
            }
        }
        ClipPlane {
            property var unitValue: {"m":1., "dm":0.1, "cm":0.01, "mm": 0.001} // TODO make s.t. reusable and derived from c++
            property var unitValueNbDecimal: {"m":3, "dm":2, "cm":1, "mm": 0} // TODO check available units
            clipPlaneComponentPath: "/clipPlane"
            distanceMinMax: 2./unitValue[context.modelLengthUnit]
            distanceNbDecimals: unitValueNbDecimal[context.modelLengthUnit]
        }
    }
}
