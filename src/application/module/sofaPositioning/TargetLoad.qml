// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQml 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import SofaBasics 1.0
import SofaScene 1.0

import Piper 1.0
import piper.QTargetLoader 1.0

GroupBox {
    title: "Load"

    Layout.minimumWidth: 300

    property QTargetLoader targetLoader

    GridLayout {
        width: parent.width
        columns: 2
        Button {
            action: importTargetAction
            text: "Import targets..."
        }
        TargetInfo {
            Layout.row: 1
        }
        Button {
            id: loadButton
            Layout.alignment: Qt.AlignRight
            text: "Load"
            tooltip: "Load the current target list into the simulation"
            enabled: (0 !== project.targetSize())
            onClicked: targetLoader.process("Default")
            Connections {
                target: context
                onTargetChanged: {
                    loadButton.enabled = (0 !== project.targetSize());
                }
            }
        }
    }
}

//GroupBox {
//    title: "Load"

//    property QTargetLoader targetLoader

//    ListModel {
//        id: targetListModel
//        ListElement {
//            text: "Default"
//        }
//    }

//    GridLayout {
//        width: parent.width
//        columns: 2
//        ComboBox {
//            model: targetListModel
//        }

//        Button {
//            Layout.alignment: Qt.AlignRight
//            text: "Load"
//            tooltip: "Load target list"
//            onClicked: targetLoader.process("Default") // TODO: selection of a target list by name
//        }
//    }
//}
