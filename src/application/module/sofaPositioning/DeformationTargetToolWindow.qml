// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

import piper.QTargetLoader 1.0
import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Target"

    property QTargetLoader targetLoader: QTargetLoader {
        id: sofaDeformationTargetLoader
        property int nbTargets
        onStarted: { nbTargets=0; }
        onFixedBone:
        {
            var success = sofaScene.sofaPythonInteractor.call("fixedBonesInteractor", "setBoneFixed", boneName, true);
            if (success) {
                context.logDebug("Fixed bone target added: "+boneName);
                ++nbTargets;
            }
            else
                context.logWarning("Fixed bone target failed: "+boneName);
        }
        onNewLandmarkTarget :
        {
            var controllerCreated = sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "addController", targetName, landmarkName, mask);
            if (controllerCreated) {
                var it=0;
                for (var i=0; i< mask.length; ++i)
                    if (mask[i]===1) {
                        sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "setTarget", targetName, i, value[it]);
                        it+=1;
                    }
                sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "setCompliance", targetName, 1./moduleParameter.PhysPosiDefo_targetDefaultStiffness);
                context.logDebug("Landmark target added: "+targetName);
                ++nbTargets;
            }
            else
                context.logWarning("Landmark target failed: "+targetName);
        }
        onNewJointTarget :
        {
            var controllerCreated = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "addJointController", jointName);
            if (controllerCreated) {
                var it=0;
                var mask = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "getMask", jointName);
                for (var i=0; i< mask.length; ++i)
                    if (mask[i]===1) {
                        sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "setTarget", jointName, i, value[it]);                        
                        it+=1;
                    }
                sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "setCompliance", jointName, 1./moduleParameter.PhysPosiDefo_targetDefaultStiffness);

                context.logDebug("Joint target added: "+targetName);
                ++nbTargets;
            }
            else
                context.logWarning("Joint target failed: "+targetName);
        }
        onNewFrameToFrameTarget :
        {
            var controllerCreated = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "addController", targetName, frameSource, frameTarget, isRelative, mask);
            if (controllerCreated) {
                var it=0;
                for (var i=0; i< mask.length; ++i)
                    if (mask[i]===1) {
                        sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "setTarget", targetName, i, value[it]);
                        it+=1;
                    }
                sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "setCompliance", targetName, 1./moduleParameter.PhysPosiDefo_targetDefaultStiffness);
                context.logDebug("Frame target added: "+targetName);
                ++nbTargets;
            }
            else
                context.logWarning("Frame target failed: "+targetName);
        }
        onFinished :
        {
            context.logInfo("Loaded "+nbTargets+" targets");
            errorValue.update();
        }
    }

    ColumnLayout {
        TargetLoad {
            Layout.fillWidth: true
            targetLoader: sofaDeformationTargetLoader
        }
        GridLayout {
            columns: 2
            Label {
                Layout.alignment: Qt.AlignRight
                text: "Target error:"
            }
            TextField {
                id: errorValue
                readOnly: true
                text: "N/A"
                function update() {
                    text = sofaScene.sofaPythonInteractor.call("targetInteractor", "getAllTargetsCumulatedError");
                }
                ToolTip {
                    visible: "N/A" !== errorValue.text
                    text: "Accumulated targets error in arbitrary unit"
                }
                ToolTip {
                    visible: "N/A" === errorValue.text
                    text: "Value available only with loaded targets"
                }
                Connections {
                    target: sofaScene
                    onStepEnd : errorValue.update()
                    onReseted : errorValue.update()
                }
            }

            Label {
                Layout.alignment: Qt.AlignRight
                text: "Target stiffness:"
            }
            StiffnessInput {
                id: stiffness
                Layout.fillWidth: true
                onValueChanged: {
                    if (sofaScene.ready)
                        sofaScene.sofaPythonInteractor.call("targetInteractor", "setAllTargetsCompliance", 1./value);
                }
                Component.onCompleted: {
                    setValue(moduleParameter.PhysPosiDefo_targetDefaultStiffness);
                }
            }
        }
        Button {
            Layout.alignment: Qt.AlignRight
            text:"Remove All"
            tooltip: "Remove all targets from the current simulation"
            onClicked: {
                if (!sofaScene.ready) return;
                sofaScene.sofaPythonInteractor.call("fixedBonesInteractor", "setNoneFixed");
                sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "removeAllControllers");
                sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "removeAllControllers");
                sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "removeAllControllers");
                context.logInfo("Removed all targets");
                errorValue.text="N/A"
            }
        }
    }
    //TODO: remove all current targets

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }

}
