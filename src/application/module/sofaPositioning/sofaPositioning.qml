// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4

import SofaBasics 1.0
import SofaWidgets 1.0
import SofaScene 1.0
import SofaApplication 1.0

import Piper 1.0
import PiperSofa 1.0

ModuleLayout {

    id: root

    property bool scenePreparation: true

    Shortcut {
        sequence: "F4"
        onActivated: controlToolWindow.sofaSceneAnimateAction.trigger()
    }

    Action {
        id: cameraInteractor
        checkable: true
        checked: true
        tooltip: "Camera only"
        property string leftButtonHelp: "double-click to set the rotation center"
        property string middleButtonHelp: "click to pan view, wheel to zoom"
        property string rightButtonHelp: "click to rotate view"
        onTriggered: {
            SofaApplication.interactorComponent = SofaApplication.interactorComponentMap[SofaApplication.defaultInteractorName];
        }
    }

    Action {
        id: startDeformation
        text: "Fine-Position"
        tooltip: "Physics based fine positioning on the current position"
        iconSource: "qrc:/icon/arrow-right.png"
        onTriggered: {
            sofaScene.sofaPythonInteractor.call("targetInteractor", "createAllBoneTargets");
            physPositioningDeformationAction.trigger();
            physPosiDeformAutoLoadTarget = true;
        }
    }

    Action {
        id: reloadModule
        onTriggered: physPositioningInteractiveAction.trigger()
    }

    EnvironmentUpdater {id: envUpdater}
    Connections {
        target: context
        onEnvChanged: envUpdater.onEnvChanged()
        onEnvTranslated: envUpdater.onEnvTranslated(name, x, y, z)
        onEnvRotated: envUpdater.onEnvRotated(name, x, y, z)
        onEnvScaled: envUpdater.onEnvScaled(name, x, y, z)
        onEnvOpacityChanged: envUpdater.onEnvOpacityChanged(currentOpacity);
    }
    Connections {
        target: sofaScene
        onReseted: envUpdater.onSceneReseted()
    }

    function loadSimulation() {
        if (!context.hasModel) {
            sofaScene.source = "";
            sofaSceneUpToDate.loaded();
            return;
        }
        else {
            context.setBusy();
            context.busyCancelVisible = true;
            var sceneFile = "file:scenePreparePositioning.py"
            scenePreparation=true;
            if (Qt.resolvedUrl(sofaScene.source) === sceneFile) {
                sofaScene.reload()
            }
            else
                sofaScene.source = sceneFile;
            cameraInteractor.trigger();
        }
    }

    function initGui() {
        fixedBoneToolWindow.init();
        landmarkToolWindow.init()
        jointToolWindow.init();
        frameToolWindow.init();
        spineToolWindow.init();
        predictorToolWindow.init();
        visualizationToolWindow.init();
        cameraInteractor.trigger();
    }

    Component.onCompleted: {
        loadSimulation();
        visible = true;
    }
    Component.onDestruction: { }

    Connections {
        target: sofaScene
        onStatusChanged: {
            if (!sofaScene.ready)
                return;
            if (!context.hasModel)
                return;
            if ("" === sofaScene.source)
                return;
            if (context.busyTaskCancelRequested) {
                sofaSceneUpToDate.cancelSceneLoading();
                return;
            }

            if (scenePreparation) {
                scenePreparation = false;
                sofaScene.source = "file:scenePositioning.py";
            }
            else {
                initGui();
                // back to the default value
                scenePreparation = true;
                sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "setAllNodesActivated", false);
                sofaSceneUpToDate.loaded();
            }
        }
    }

    // [TL] TODO optimal solution, just send one event, but QML does not find the updatePosition() function !?
    // update manipulator position during simulation
//    Connections {
//        target: sofaScene
//        onStepEnd : {
//            if (null !== sofaScene.selectedManipulator)
//                sofaScene.selectedManipulator.updatePosition();
//        }
//    }

    content: DynamicSplitView {
        id: dynamicSplitView
        anchors.fill: parent
        uiId: 2000
        reloadSavedViews: false
        sourceComponent: Component {
            DynamicContent {
                defaultContentName: "SofaViewer"
                sourceDir: "qrc:/SofaWidgets"
                properties: {"culling": true, "defaultCameraOrthographic": true, "highlightIfFocused": false, "hideBusyIndicator": true }
            }
        }
    }

    toolbar: ColumnLayout {

        anchors.right: parent.right
        anchors.left: parent.left

        Button {
            Layout.fillWidth: true
            action: controlToolWindow.sofaSceneAnimateAction
        }

        MouseToolSelection {
            Layout.fillWidth: true
            mouseToolActionArray: [
                cameraInteractor,
                fixedBoneToolWindow.interactor,
                landmarkToolWindow.interactor,
//                frameToolWindow.interactor,
                spineToolWindow.interactor
            ]
        }

        ModuleToolWindowButton {
            text: "\&Control"
            shortcutChar: "C"
            toolWindow: controlToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Display"
            shortcutChar: "D"
            toolWindow: visualizationToolWindow
        }
        ModuleToolWindowButton {
            text: "Fixed \&bones"
            shortcutChar: "B"
            toolWindow: fixedBoneToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Joint"
            shortcutChar: "J"
            toolWindow: jointToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Landmark"
            shortcutChar: "L"
            toolWindow: landmarkToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Frame"
            shortcutChar: "F"
            toolWindow: frameToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Spine"
            shortcutChar: "S"
            toolWindow: spineToolWindow
        }
        ModuleToolWindowButton {
            text: "\&Predictors"
            shortcutChar: "P"
            toolWindow: predictorToolWindow
            tooltip: "Statistical predictors for positioning"
        }

        ModuleToolWindowButton {
            text: "\&Target"
            shortcutChar: "T"
            toolWindow: targetToolWindow
        }
        Item {
            Layout.fillHeight: true
        }
        UpdateModel {
            enabled: moduleParameter.PhysPosiInter_modelUpdateEnabled
            Layout.fillWidth: true
        }
        Button {
            Layout.fillWidth: true
            action: startDeformation
        }
        RowLayout {
            Button {
                Layout.fillWidth: true
                text: "Scripting"
                tooltip: "Use scripting to deform your model"
                action: runScriptAction
            }
            Switch {
                id: runScriptSwitch
                checked: controlToolWindow.autoRunScript
                onClicked: {
                    controlToolWindow.autoRunScript = checked;
                }
                ToolTip {
                    text: "Run most recent python script after each simulation step"
                }
                Connections {
                    target: controlToolWindow
                    onAutoRunScriptChanged: {
                        runScriptSwitch.checked = controlToolWindow.autoRunScript;
                    }
                }
            }
        }

        ReloadButton {
            Layout.fillWidth: true
            action: reloadModule
        }
    }

    ControlToolWindow {
        id: controlToolWindow
        Component.onCompleted: allToolWindows.push(controlToolWindow);
    }
    VisualizationToolWindow {
        id: visualizationToolWindow
        Component.onCompleted: allToolWindows.push(visualizationToolWindow);
    }
    FixedBoneToolWindow {
        id: fixedBoneToolWindow
        Component.onCompleted: allToolWindows.push(fixedBoneToolWindow);
    }
    LandmarkToolWindow {
        id: landmarkToolWindow
        Component.onCompleted: allToolWindows.push(landmarkToolWindow);
    }
    JointToolWindow {
        id: jointToolWindow
        Component.onCompleted: allToolWindows.push(jointToolWindow);
    }
    FrameToolWindow {
        id: frameToolWindow
        Component.onCompleted: allToolWindows.push(frameToolWindow);
    }
    SpineToolWindow {
        id: spineToolWindow
        Component.onCompleted: allToolWindows.push(spineToolWindow);
    }
    PredictorToolWindow {
        id:predictorToolWindow
        frameToolWindow: frameToolWindow
        Component.onCompleted: allToolWindows.push(predictorToolWindow);
    }
    TargetToolWindow {
        id: targetToolWindow
        Component.onCompleted: allToolWindows.push(targetToolWindow);
    }

}
