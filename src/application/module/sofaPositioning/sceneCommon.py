# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import numpy as np

import Sofa
import SofaPython.units
import SofaPython.Tools
import Compliant.StructuralAPI
import ContactMapping.StructuralAPI
import Anatomy.Tools
import Anatomy.Constraint

import SofaPiper.sml
import SofaPiper.tools

import piper.anatomyDB
import piper.app
import piper.sofa

SofaPython.Tools.meshLoader = SofaPiper.tools.meshLoader
Anatomy.Tools.getFirstVertex = SofaPiper.tools.getFirstVertex
SofaPython.mass.RigidMassInfo.setFromMesh = SofaPiper.tools.RigidMassInfo_setFromMesh
Compliant.StructuralAPI.GenericRigidJoint.PositionController = Anatomy.Constraint.ProgressivePositionController

model=None
scene=None
jointControllers = dict()
landmarkControllers = dict()
frameControllers = dict()
controllableFrames = dict() # frameName -> Compliant.Frame.Frame or Compliant.StructuralAPI.RigidBody
controllableLandmarks = dict() # landmarkName -> Compliant.StructuralAPI.RigidBody.MappedPoint or Compliant.StructuralAPI.RigidBody

spine = None # to constrain the shape of the spine
spineReferencePoints=dict() # reference point mapped under vertebra
allNodesList = list() # sofa nodes which contains all nodes from model

currentModule = None # PhysPosiInter or PhysPosiDefo

def dofToTargetIndex(dofIndex, mask):
    """from a dofIndex and the mask, get the target index
     ex: dofIndex=4, mask=[0,0,0,1,1,1]
     targetIndex is 1              ^
    """
    if not mask[dofIndex]: return None
    targetIndex=-1
    for dofCurrentIndex, dofMask in enumerate(mask):
        if dofMask: targetIndex+=1
        if dofCurrentIndex==dofIndex: return targetIndex
    return None

def init(moduleName) :
    global simulationController, jointController, landmarkController, frameController
    simulationController=None
    jointController=None
    landmarkController=None
    frameController = None
    global currentModule
    currentModule = moduleName
    jointControllers.clear()
    frameControllers.clear()
    controllableFrames.clear()
    SofaPython.units.setLocalUnits(length = piper.hbm.unitToStr(piper.sofa.project().model().metadata().lengthUnit()))
    # fill frame factory with landmarks
    # used for anatomical frames and anatomical joints
    piper.anatomyDB.FrameFactory.instance().landmark().clear()
    ff = piper.anatomyDB.FrameFactory.instance()
    for landmark in piper.sofa.project().model().metadata().landmarks().values():
        try:
            ff.landmark().add(landmark.name(), landmark.position(piper.sofa.project().model().fem()))
        except BaseException, e:
            piper.app.logDebug(str(e)+" - ignored")
    global model
    model = SofaPiper.sml.Model(doPaintBoneCapsule = piper.sofa.project().moduleParameter().getBool(currentModule, "capsuleEnabled"))
    if piper.sofa.project().moduleParameter().getBool(currentModule, "capsuleEnabled"):
        model.addSoftTissueContact({"capsule"}, doCollision="all")
        model.addSoftTissueContact({"ligament"}, doCollision="insert")
    if piper.sofa.project().moduleParameter().getBool(currentModule, "boneCollisionEnabled"):
        model.addBoneCollision()
    model.addAnatomicalJoint()
    global scene
    scene = None
    global allNodesList
    allNodesList = list()

def findBoneRigid(boneList):
    """ @return the bone rigid which exists in the model
    """
    rigid=None
    for bone in boneList:
        if bone in scene.rigids:
            rigid = scene.rigids[bone]
            break
    return rigid

def findLandmarkRigid(landmark):
    """ @return the bone rigid to which the landmark belongs
    DEPRECATED
    """
    return findBoneRigid(piper.anatomyDB.getLandmarkBoneList(landmark))

def addVisualStyle():
    for bone in scene.rigids.values(): # special case, bones can have several visuals
        for visual in bone.visuals.values():
            visual.node.createObject("VisualStyle", name="visualStyle", displayFlags="hideWireframe")
    for solid in model.getSolidsByTags({"skin", "ligament", "capsule"}):
        for visual in scene.visuals[solid.id].values():
            visual.node.createObject("VisualStyle", name="visualStyle", displayFlags="hideWireframe")

def addLandmarks():
    for (name,landmark) in piper.sofa.project().model().metadata().landmarks().iteritems():
        entity = None
        if piper.anatomyDB.exists(name) and piper.anatomyDB.isLandmark(name):
            landmarkBoneList = piper.anatomyDB.getLandmarkBoneList(name)
            if len(landmarkBoneList) > 0:
                entity = findBoneRigid(landmarkBoneList)
        mappedPoint=None
        if entity is None:
            # default is to attach the landmark to the flesh
            piper.app.logWarning("No anatomical entity found to attach landmark {0} - attached to the flesh".format(name))
            mappedPoint = scene.addMappedPoint(name,
                                 np.array(landmark.position(piper.sofa.project().model().fem())).reshape(3),
                                 map(lambda s: s.id, model.getSolidsByTags({"skin"})) )
        else :
            # attach the landmark to a bone, simple RigidMapping under the bone rigid
            mappedPoint = entity.addAbsoluteMappedPoint(name, np.array(landmark.position(piper.sofa.project().model().fem())).reshape(3))
        mappedPoint.dofs.tags = "landmark" # for landmark picking
        mappedPoint.dofs.drawMode = 1 # to draw a sphere
        mappedPoint.dofs.showColor = SofaPython.Tools.listToStr(scene.param.showLandmarkColor)
        mappedPoint.dofs.showObjectScale = SofaPython.units.length_from_SI(scene.param.showLandmarkScale)
        controllableLandmarks[name] = mappedPoint
    piper.app.logInfo("{0} anatomical landmarks added".format(len(controllableLandmarks)))
    if len(controllableLandmarks): piper.app.logDebug(", ".join(controllableLandmarks.keys()))

def setBoneVisualTag():
    scene.param.color["selected"] = [.8,.0,.0,1]
    for boneSolid in scene.model.getSolidsByTags({"bone"}):
        for visual in scene.rigids[boneSolid.id].visuals.itervalues():
            visual.model.tags = "bone"

def addControllableFrameBones():
    """ add bones inertia frame as controllableFrames """
    for id,rigid in scene.rigids.items():
        frameUIName = "B_"+model.solids[id].name
        controllableFrames[frameUIName] = rigid

def addControllableFrameWorld():
    """ add a world reference frame in [0,0,0,0,0,0,1] """
    controllableFrames["W_Origin"] = Compliant.StructuralAPI.RigidBody(scene.node, "Origin")
    controllableFrames["W_Origin"].setManually([0,0,0,0,0,0,1])
    controllableFrames["W_Origin"].dofs.showObjectScale = 2*SofaPython.units.length_from_SI(scene.param.showRigidScale)
    controllableFrames["W_Origin"].setFixed()

def addAnatomicalFrames():
    """add anatomical frames to the scene"""

    # create anatomical bone frames
    # get ISB Bone Coordinate System (BCS) and Joint  Coordinate System (JCS) frames
    ff = piper.anatomyDB.FrameFactory.instance()
    frameList = piper.anatomyDB.getSubClassOfFromBibliographyList("Frame", "ISB_BCS")
    frameList += piper.anatomyDB.getSubClassOfFromBibliographyList("Frame", "ISB_JCS")
    addedFrames = list()
    for frameName in frameList :
        try:
            framePosition = ff.computeFrame(frameName)
        except Exception as e:
            continue
        else:
            # to which bone belongs this frame ?
            partOfList = piper.anatomyDB.getPartOfSubClassList(frameName, "Bone", True);
            if not len(partOfList):
                piper.app.logWarning("Unknown frame {0} - ignored".format(frameName))
                continue
            rigid = findBoneRigid(partOfList)
            if rigid is None:
                piper.app.logWarning("No entity found for frame {0} - ignored".format(frameName))
                continue
            frameRigid = rigid.addAbsoluteOffset(frameName, framePosition.toVec().flatten().tolist())
            frameRigid.mapping.applyRestPosition=True
            frameRigid.dofs.showObjectScale = SofaPython.units.length_from_SI(scene.param.showRigidScale)
            frameRigid.boneName = model.solids[piper.anatomyDB.getReferenceName(rigid.node.name)].name # retrieve the rigid name from the model
            controllableFrames[frameName] = frameRigid

            addedFrames.append(frameName)
    piper.app.logInfo("{0} anatomical frames added".format(len(addedFrames)))
    if len(addedFrames): piper.app.logDebug(", ".join(addedFrames))

#
# TODO: better way to define spine, using joint centers (or ISB JCS if available ?) instead of vertebrae centers
#
def addSpineReferencePoints():
    global spine
    spine = None
    spineReferencePoints.clear()
    vertebraList = list() # used for log debug
    allInput=""
    allIndexPairs=""
    i=0

    for name,rigid in scene.rigids.iteritems():
        if piper.anatomyDB.isEntityPartOf(name, "Vertebral_column", True):
            refName = piper.anatomyDB.getReferenceName(name)
            vertebraList.append(name)
            spineReferencePoints[refName] = rigid
            if spine is None:
                spine = ContactMapping.StructuralAPI.ContactObject()
                spine.node = rigid.node.createChild("spine")
            else :
                rigid.node.addChild(spine.node)
            allInput += '@'+rigid.node.getPathName()+" "
            allIndexPairs += str(i) + " 0 "
            i+=1
    if spine is None:
        piper.app.logInfo("No vertebra found")
    else:
        piper.app.logInfo("{0} vertebra found".format(len(spineReferencePoints))) # TODO FIXME
        piper.app.logDebug("Vertebra: {0}".format(vertebraList))
    if not spine is None :
        spine.dofs = spine.node.createObject("MechanicalObject", template="Vec3")
        spine.dofs.showObject = False
        spine.dofs.showObjectScale = SofaPython.units.length_from_SI(0.005)
        spine.dofs.drawMode = 1
        spine.node.createObject('SubsetMultiMapping', template = "Rigid3,Vec3", name="mapping", input = allInput , output = '@./', indexPairs = allIndexPairs, applyRestPosition=True )

def setupCollisions():
    for c in scene.model.getSurfaceLinksByTags({"ignoreInitialPenetration"}):
        scene.collisionContacts[c.id].ignoreInitialPenetratedVertex()
        if "symmetric" in c.tags:
            scene.collisionContacts[c.id+"_inverted"].ignoreInitialPenetratedVertex()

def addCustomAffines():
    hbmModel = piper.sofa.project().model()
    for name, metadata in hbmModel.metadata().genericmetadatas().iteritems() :
        nameList = name.split("__")
        if len(nameList) == 2 and "custom_affine" == nameList[0]:
            if not piper.anatomyDB.exists(nameList[1]):
                piper.app.logWarning("Unknown entity {0} - custom_affine metadata ignored".format(nameList[1]))
                continue
            entityRefName = piper.anatomyDB.getReferenceName(nameList[1])
            if not entityRefName in model.solids:
                piper.app.logWarning("Entity {0} not in model - custom_affine metadata ignored".format(nameList[1]))
                continue
            coordList = []
            for nodeId in metadata.getNodeIdVec(hbmModel.fem()):
                coord = hbmModel.fem().getNode(nodeId).get().flatten()
                coordList.append(coord)
            scene.param.samplingAdditionalPosition[entityRefName] = coordList

def getVoxelSize():
    voxelSize = piper.sofa.project().moduleParameter().getDouble("PhysPosi","voxelSize")
    if piper.sofa.project().moduleParameter().getBool("PhysPosi", "scaleVoxelSize"):
        anthropoHeight = piper.sofa.project().model().metadata().anthropometry().height
        if not anthropoHeight.isDefined():
            piper.app.logWarning("Scaled voxel size requested, but HBM height is not defined, using unscaled voxel size")
            return voxelSize
        scale = SofaPython.units.length_to_SI(anthropoHeight.value()) / piper.sofa.project().moduleParameter().getDouble("PhysPosi","scaleVoxelSizeRefHeight")
        return scale * voxelSize
    else:
        return voxelSize
