// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

import Piper 1.0

ColumnLayout {
    property alias defaultButton: defaultButton
    property alias addButton: addButton
    property alias removeButton: removeButton
    property alias spineInteractorButton: spineInteractorButton
    property alias cameraInteractorButton: cameraInteractorButton
    property alias selectVertebra: selectVertebra
    property alias selectedVertebraText: selectedVertebraText
    property alias stiffness: stiffness
    property alias cumDistanceItem: cumDistanceItem
    property alias maxDistanceItem: maxDistanceItem
    RowLayout {
        Layout.fillWidth: true
        Label {text: "Spline control points:"}

        Item {
            id: item1
            Layout.fillWidth: true
        }

        Button {
            id: defaultButton
            text: qsTr("Default")
        }

    }

    RowLayout {
        id: rowLayout2

        TextField {
            id: selectedVertebraText
            width: 80
            height: 20
            readOnly: true
            Layout.fillWidth: true
            font.pixelSize: 12
        }

        Button {
            id: selectVertebra
            text: qsTr("...")
            Layout.preferredWidth: 20
            tooltip: "Select spline control points..."
        }
    }

    RowLayout {
        id: rowLayout3
        width: 100
        height: 100

        Label {
            id: label1
            text: "Error ("+context.modelLengthUnit+"):"
        }

        TextField {
            id: cumDistanceItem
            text: "0"
            Layout.preferredWidth: 60
            ToolTip {
                text: "Vertebrae cumulated distance to the target spline"
            }
        }

        TextField {
            id: maxDistanceItem
            width: 60
            text: "0"
            Layout.preferredWidth: 60
            ToolTip {
                text: "Vertebrae maximum distance to the target spline"
            }
        }
    }

    RowLayout {
        id: rowLayout

        Label {
            id: label
            text: "Stiffness:"
            horizontalAlignment: Text.AlignRight
        }
        StiffnessInput {
            id: stiffness
            ToolTip {
                text: "Spine target stiffness"
            }
        }
    }


    RowLayout {
        id: rowLayout1
        Layout.fillWidth: true
        MouseInteractorToolButton {
            id: cameraInteractorButton
        }
        MouseInteractorToolButton {
            id: spineInteractorButton
        }
        Item {
            id: item2
            Layout.fillWidth: true
        }

        Button {
            id: addButton
            tooltip: "Add spine controller"
            iconSource: "qrc:///icon/list-add.png"
        }

        Button {
            id: removeButton
            tooltip: "Remove spine controller"
            iconSource: "qrc:///icon/list-remove.png"
        }

    }





}
