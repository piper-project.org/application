// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQml 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

import SofaScene 1.0

import Piper 1.0

ModuleToolWindow {
    id: root
    
    title: "Joint Controller"

    property var controlledJointList: []
    property var jointNames: []
    
    property var dofControllerComponent: Qt.createComponent("qrc:///module/sofaPositioning/DofController.qml");

    function jointControllerDofId(jointName,i) {
        var id = "joint"+jointName+String(i)+"Input";
        id = id.replace('-','_'); //a qml id cannot contain a '-'
        return id;
    }
    
    function init() {
        jointsListModel.clear();
        jointNames = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "getJointIds");
        for (var i=0; i < jointNames.length; i++) {
            jointsListModel.append({text: jointNames[i]})
        }
        addControllerButton.updateEnabled();
    }

    function addController(jointName, initialValue) {
        var controllerCreated = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "addJointController", jointName);
        if (!controllerCreated)
            return false;
        var mask = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "getMask", jointName);

        var _initialValue = initialValue;
        if (typeof _initialValue === 'undefined')
            _initialValue = sofaScene.sofaPythonInteractor.call("jointControllerInteractor", "getTarget", jointName);

        var controller = dofControllerComponent.createObject(jointsControllerParent, {
                                                                 "controllerName": jointName,
                                                                 "sofaPythonInteractorName": "jointControllerInteractor",
                                                                 "mask": mask,
                                                                 "initialValue": _initialValue,
                                                                 "defaultStiffness": moduleParameter.PhysPosiInter_targetDefaultStiffness,
                                                                 "enableShowDofsButton": true
                                                             } );
        controller.removed.connect(onControllerRemoved);
        controlledJointList.push(jointName);
        addControllerButton.updateEnabled();
        return true;
    }

    function onControllerRemoved(jointName) {
        controlledJointList.splice(controlledJointList.indexOf(jointName),1);
        addControllerButton.updateEnabled();
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        width = 500;
        setCurrentWindowSizeMinimum();
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        GroupBox {
            Layout.fillWidth: true
            title: "Add joint controller"
            RowLayout {
                width: parent.width
                ComboBox {
                    id: jointsCombo
                    Layout.minimumWidth: 200
                    Layout.fillWidth: true
                    editable: true
                    model: ListModel {
                        id: jointsListModel
                    }
                    onCurrentIndexChanged: addControllerButton.updateEnabled()
                    onEditTextChanged: addControllerButton.updateEnabled()
                }
                Button {
                    id: addControllerButton
                    anchors.right: parent.right
                    iconSource: "qrc:///icon/list-add.png"
                    tooltip:"Add a controller for the joint"
                    enabled: false
                    onClicked: {
                        var jointName = jointsCombo.currentText;
                        addController(jointName);
                    }

                    function updateEnabled() {
                        enabled = (jointsListModel.count>0 && jointsCombo.currentIndex>=0
                                   && (-1 !== jointNames.indexOf(jointsCombo.editText))
                                   && controlledJointList.indexOf(jointsCombo.currentText) ==-1);
                    }
                }
            }
        }
        ScrollView {
            Layout.minimumHeight: 100
            Layout.minimumWidth: 300
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                id: jointsControllerParent
                width: root.width
            }
        }
    }
}
