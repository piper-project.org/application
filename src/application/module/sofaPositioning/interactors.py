# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import sys
import math
import numpy as np
import numpy.linalg as la

import Sofa
import SofaPython.Tools
import SofaPython.units
import Anatomy.Constraint
import ContactMapping.StructuralAPI

import piper.app
import piper.sofa

import sceneCommon

userTargetCompliance = 1e-8
spineTargetCompliance = 5e-3
spineTargetErrorMin = 0.002 # m
progressiveTargetStepTranslation = 0.005 # m
progressiveTargetStepRotation = math.radians(5) # degree

class SimulationControlInteractor(Sofa.PythonScriptController) :

#    doUpdateBoneCollisionActivation = False # TODO : make those instance variables
#    boneCollisionActivated = None

#    def onLoaded(self,node): # looks like it is never called...
#        piper.app.logDebug("loaded")
#        return 0

#    def onBeginAnimationStep(self, dt):
#        if SimulationControlInteractor.doUpdateBoneCollisionActivation:
#            for contact in sceneCommon.scene.collisionContacts.values():
#                contact.node.activated = SimulationControlInteractor.boneCollisionActivated
#            SimulationControlInteractor.doUpdateBoneCollisionActivation = False

    def createGraph(self, node):
        self.node = node

    def setCollision(self, activated):
        for contact in sceneCommon.scene.collisionContacts.values():
            contact.node.activated = activated
#        SimulationControlInteractor.doUpdateBoneCollisionActivation = True
#        SimulationControlInteractor.boneCollisionActivated = activated

    def setCollisionNbVertex(self, nbVertex):
        for contact in sceneCommon.scene.collisionContacts.values():
            if not contact.closestKeepNSmallestEngine is None:
                contact.closestKeepNSmallestEngine.keepNSmallest = int(nbVertex)

    def setCollisionCompliance(self, compliance):
        for contact in sceneCommon.scene.collisionContacts.values():
            for obj in contact.node.getObjects():
                if obj.name == "constraint":
                    contact.node.removeObject(obj)
                    break
            contact.compliance.compliance = compliance
            contact.compliance.reinit()

    def checkCollision(self):
        violated = dict()
        maxViolatedName = None
        maxViolatedDistance = 0
        for contact in sceneCommon.scene.collisionContacts.itervalues():
            for d in contact.dofs.position:
                if len(d) < 0 : # the simulation has not yet been started, no value
                    continue
                if d[0] < 0 :
                    nameSplit = contact.node.name.split("__")
                    name = nameSplit[1]+"/"+nameSplit[2]
                    if not name in violated:
                        violated[name] = 0
                    if d[0] < violated[name]:
                        violated[name] = d[0]
                    if d[0] < maxViolatedDistance:
                        maxViolatedDistance = d[0]
                        maxViolatedName = name
        if len(violated) > 0:
            message = "Violated collisions: "
            unitStr = piper.hbm.unitToStr(piper.sofa.project().model().metadata().lengthUnit())
            for name, d in violated.iteritems():
                m = "{0} ({1:.2f} {2}), ".format(name, math.fabs(d), unitStr)
                if name == maxViolatedName:
                    m = "<b>"+m+"</b>"
                message += m
            piper.app.logInfo(message)
        else:
            piper.app.logInfo("No violated collision")

    def setSolverStabilization(self, activated):
        if (activated):
            sceneCommon.scene.root.getObject("odesolver").stabilization="post-stabilization assembly"
        else:
            sceneCommon.scene.root.getObject("odesolver").stabilization="pre-stabilization"

    def setAllNodesActivated(self,activated):
        for node in sceneCommon.allNodesList:
            node.activated = activated

    def isAllNodesActivated(self):
        if 0 == len(sceneCommon.allNodesList):
            return False
        return sceneCommon.allNodesList[0].activated

    def setAutoStop(self, autoStop):
        self.autoStop = autoStop

    def getNbIteration(self):
        return self.autoStopLastNbIteration

    def getMaxCartesianVelocity(self):
        return SofaPython.units.velocity_to_SI(math.sqrt(self._getMaxCartesianVelocity()))

    def _getMaxCartesianVelocity(self):
        getMaxCartesianVelocity2Args=dict()
        if "dofRigid" in sceneCommon.scene.nodes and not sceneCommon.scene.nodes["dofRigid"] is None:
            getMaxCartesianVelocity2Args["rigidDof"] = sceneCommon.scene.nodes["dofRigid"].getObject("dofs")
        if "dofAffine" in sceneCommon.scene.nodes and not sceneCommon.scene.nodes["dofAffine"] is None:
            getMaxCartesianVelocity2Args["affineDof"] = sceneCommon.scene.nodes["dofAffine"].getObject("dofs")
        return Anatomy.Tools.getMaxCartesianVelocity2(**getMaxCartesianVelocity2Args)

    def setAutoStopMaxCartesianVelocity(self, velocity):
        self.autoStopMaxCartesianVelocity2 = math.pow(SofaPython.units.velocity_from_SI(velocity), 2)

    def _stopSimulation(self):
        self.autoStopNbIteration = 0
        self.rootNode.animate = False

    def onLoaded(self,node):
        self.rootNode = node
        self.autoStop = False
        self.autoStopNbIteration = 0
        self.autoStopLastNbIteration = 0
        self.autoStopNbIterationMax = piper.sofa.project().moduleParameter().getUInt("PhysPosi","autoStopNbIterationMax")
        self.setAutoStopMaxCartesianVelocity(piper.sofa.project().moduleParameter().getDouble("PhysPosi","autoStopVelocity"))

    def onEndAnimationStep(self,dt):
        if not self.autoStop:
            return
        self.autoStopNbIteration += 1
        self.autoStopLastNbIteration = self.autoStopNbIteration
        if not self.autoStopNbIteration > 1: # at least one iteration
            return
        if self.autoStopNbIteration >= self.autoStopNbIterationMax:
            self._stopSimulation()
            return
        if  self._getMaxCartesianVelocity() < self.autoStopMaxCartesianVelocity2:
            self._stopSimulation()
            return

    def updateModelNodes(self):
        self.node.sendGUIEvent("","exportFullModel","");
        self.node.sendGUIEvent("","exportEntities","");

class DisplayQmlInteractor(Sofa.PythonScriptController) :

    # Qt GUI enum constant
    Qt_UnChecked=0
    Qt_PartiallyChecked=1
    Qt_Checked=2

    defaultEnvColor= [1., 1., 1., 1.]

    def createGraph(self,node):
        self.rootNode = node
        self.envNode = None
        self.envTransformation = dict() # workaround a strange behavior of the sofa reset()

    def initGraph(self,node):
        self.updateEnvironment()
        # initialize show counter
        for frame in sceneCommon.controllableFrames.values():
            frame.showCount = 0
        for joint in sceneCommon.scene.joints.values():
            joint.showCount = 0

    def setShowBonesRigids(self, show):
        for bone in sceneCommon.scene.rigids.values():
            bone.showCount += 1 if show else -1
            bone.dofs.showObject = (bone.showCount>0)

    def setShowSofTissueAffines(self, show):
        if "dofAffine" in sceneCommon.scene.nodes and not sceneCommon.scene.nodes["dofAffine"] is None:
            sceneCommon.scene.nodes["dofAffine"].getObject("dofs").showObject = show

    def setShowLandmarks(self, show):
        for name,landmark in sceneCommon.controllableLandmarks.items():
            landmark.dofs.showObject = show

    def setShowAnatomicalFrames(self, show):
        for name,frame in sceneCommon.controllableFrames.items():
            if not "W_" in name and not "B_" in name:
                frame.showCount += 1 if show else -1
                frame.dofs.showObject = (frame.showCount>0)

    def setShowWorldFrames(self, show):
        for name,frame in sceneCommon.controllableFrames.items():
            if "W_" in name:
                frame.showCount += 1 if show else -1
                frame.dofs.showObject = (frame.showCount>0)

    def setShowJointsOffset(self, show):
        for joint in sceneCommon.scene.joints.values():
            joint.showCount += 1 if show else -1
            joint.node.getParents()[0].getObject("dofs").showObject = (joint.showCount>0)
            joint.node.getParents()[1].getObject("dofs").showObject = (joint.showCount>0)

    def setShowGaussPoint(self, show):
        for behavior in sceneCommon.scene.behaviors.values():
            behavior.sampler.showSamples = show

    def setShowBones(self, show): # special case, bones can have several visuals
        for bone in sceneCommon.scene.rigids.values():
            for visual in bone.visuals.values():
                visual.node.activated = (show != DisplayQmlInteractor.Qt_UnChecked)
                if (show != DisplayQmlInteractor.Qt_UnChecked):
                    visual.node.propagatePositionAndVelocity()
                visual.node.getObject("visualStyle").displayFlags = "hideWireframe" if (show != DisplayQmlInteractor.Qt_PartiallyChecked) else "showWireframe"

    def setShow(self, tag, show):
        for solid in sceneCommon.model.getSolidsByTags({tag}):
            for visual in sceneCommon.scene.visuals[solid.id].values():
                visual.node.activated = (show != DisplayQmlInteractor.Qt_UnChecked)
                if (show != DisplayQmlInteractor.Qt_UnChecked):
                    visual.node.propagatePositionAndVelocity()
                visual.node.getObject("visualStyle").displayFlags = "hideWireframe" if (show != DisplayQmlInteractor.Qt_PartiallyChecked) else "showWireframe"

    def setShowSkinOpacity(self, opacity):
        for solid in sceneCommon.model.getSolidsByTags({"skin"}):
            for visual in sceneCommon.scene.visuals[solid.id].values():
                color = solid.getValueByTag(sceneCommon.scene.param.color)
                color[3] = opacity
                visual.node.getObject("model").setColor(color[0], color[1], color[2], color[3])

    def setShowSlidingContact(self, show):
        for slidingContact in sceneCommon.scene.slidingContacts.values():
            slidingContact.node.getObject("visualStyle").displayFlags="showMapping" if show else "hideMapping"

    def setShowNormals(self, show):
        self.rootNode.getObject("VisualStyle").displayFlags = "showNormals" if show else "hideNormals"

    def updateEnvironment(self):
        if not self.envNode is None:
            self.envNode.detachFromGraph()
        self.envTransformation.clear()
        self.envNode = self.rootNode.createChild("environment")
        for envName in piper.sofa.project().environment().getListNames():
            self.envTransformation[envName] = dict()
            myEnvNode = self.envNode.createChild(envName)
            myEnvNode.createObject("PiperMeshLoader", name="loader", environmentName=envName)
            myEnvNode.createObject("MechanicalObject", name="dofs", src="@loader")
            mappingNode = myEnvNode.createChild("mapping")
            mappingNode.createObject("OglModel", name="visual", src="@../loader", color = SofaPython.Tools.listToStr(DisplayQmlInteractor.defaultEnvColor))
            mappingNode.createObject("IdentityMapping", name="mapping")
        self.envNode.init()

    def updateAllEnvironmentPosition(self):
        for envName, envTrans in self.envTransformation.iteritems():
            if "translation" in envTrans:
                t = envTrans["translation"]
                self.setEnvironmentTranslation(envName, t[0], t[1], t[2])
            if "rotation" in envTrans:
                r = envTrans["rotation"]
                self.setEnvironmentRotation(envName, r[0], r[1], r[2])
            if "scale" in envTrans:
                s = envTrans["scale"]
                self.setEnvironmentScale(envName, s[0], s[1], s[2])

    def setEnvironmentTranslation(self, name, x, y, z):
        loader = self.envNode.getObject(name+"/loader")
        self.envTransformation[name]["translation"] = [x,y,z]
        if not loader is None:
            loader.translation = [x,y,z]
            loader.reinit()

    def setEnvironmentRotation(self, name, rx, ry, rz):
        loader = self.envNode.getObject(name+"/loader")
        self.envTransformation[name]["rotation"] = [rx,ry,rz]
        if not loader is None:
            loader.rotation = [rx,ry,rz]
            loader.reinit()

    def setEnvironmentScale(self, name, sx, sy, sz):
        if sx==0 or sy==0 or sz==0: # it leads to invalid transformation matrix
            return
        self.envTransformation[name]["scale"] = [sx,sy,sz]
        loader = self.envNode.getObject(name+"/loader")
        if not loader is None:
            loader.scale3d = [sx,sy,sz]
            loader.reinit()

    def setEnvironmentOpacity(self, alpha):
        for myEnvNode in self.envNode.getChildren():
            visual = myEnvNode.getObject("mapping/visual")
            visual.setColor(DisplayQmlInteractor.defaultEnvColor[0], DisplayQmlInteractor.defaultEnvColor[1], DisplayQmlInteractor.defaultEnvColor[2], alpha)

class FixedBonesQmlInteractor(Sofa.PythonScriptController) :

    def getBoneIds(self):
        bones=[]
        for solid in sceneCommon.model.getSolidsByTags({"bone"}):
            bones.append(solid.id)
        return sorted(bones)

    def getBoneNames(self):
        bones=[]
        for solid in sceneCommon.model.getSolidsByTags({"bone"}):
            bones.append(solid.name)
        return sorted(bones)

    def setBoneFixed(self, boneName, isFixed):
        boneId = piper.anatomyDB.getReferenceName(boneName)
        if not boneId in sceneCommon.scene.rigids:
            piper.app.logWarning("Bone {0} does not exists - ignored".format(boneId))
            return False
        sceneCommon.scene.rigids[boneId].setFixed(isFixed)
        color = None
        if (isFixed):
            color = sceneCommon.scene.param.color["selected"]
        else:
            color = sceneCommon.scene.param.color["bone"]
        for visual in sceneCommon.scene.rigids[boneId].visuals.itervalues():
            visual.model.setColor(color[0],color[1],color[2],color[3])
        return True

    def setNoneFixed(self):
        for boneId in sceneCommon.scene.rigids.keys():
            self.setBoneFixed(boneId, False)


class JointControllersQmlInteractor(Sofa.PythonScriptController) :

    def getJointIds(self):
        return sorted(sceneCommon.model.genericJoints.keys())

    def getMask(self, jointName):
        """ @return the controler mask, 1 for controlled dof, which is the negation of the joint mask
        """
        return [int(not m) for m in sceneCommon.scene.joints[jointName].mask]

    def getCurrentPosition(self, jointName):
        return sceneCommon.scene.joints[jointName].dofs.position[0]

    def addJointController(self, jointName):
        if jointName in sceneCommon.jointControllers:
            piper.app.logWarning("Joint {0} already controlled - ignored".format(jointName))
            return False
        if not jointName in sceneCommon.scene.joints:
            piper.app.logWarning("Joint {0} does not exist - ignored".format(jointName))
            return False
        controller = sceneCommon.scene.joints[jointName].addGenericPositionController(compliance=userTargetCompliance, isCompliance=False)
        controller.progressiveTarget.step = progressiveTargetStepRotation # TODO these are value for controlled angles
        controller.node.init()
        controller.joint = sceneCommon.scene.joints[jointName] # keep track of the controlled joint
        sceneCommon.jointControllers[jointName] = controller
        return True

    def removeController(self, jointName):
        if not jointName in sceneCommon.jointControllers:
            piper.app.logWarning("Joint {0} not controlled - ignored".format(jointName))
            return False
        sceneCommon.jointControllers[jointName].node.detachFromGraph()
        del sceneCommon.jointControllers[jointName]

    def removeAllControllers(self):
        for jointName in sceneCommon.jointControllers.keys():
            self.removeController(jointName)

    def getControllerNames(self):
        return sceneCommon.jointControllers.keys()

    def getTarget(self, jointName):
        return sceneCommon.jointControllers[jointName].progressiveTarget.target

    def setTarget(self, jointName, dofIndex, value):
        if not jointName in sceneCommon.jointControllers:
            return False
        targetOrig=sceneCommon.jointControllers[jointName].progressiveTarget.target
        target=list()
        for t in targetOrig:
            target+=t
        target[sceneCommon.dofToTargetIndex(int(dofIndex), self.getMask(jointName))]=value # int(dofIndex) workaround for dofIndex beeing a float
        sceneCommon.jointControllers[jointName].setTarget(target)
        return True

    def setCompliance(self, jointName, compliance) :
        sceneCommon.jointControllers[jointName].compliance.compliance = compliance
        sceneCommon.jointControllers[jointName].compliance.reinit()

    def setShowControlledFrames(self, name, show):
        joint = sceneCommon.jointControllers[name].joint
        joint.showCount += 1 if show else -1
        joint.node.getParents()[0].getObject("dofs").showObject = (joint.showCount>0)
        joint.node.getParents()[1].getObject("dofs").showObject = (joint.showCount>0)



class LandmarkControllersQmlInteractor(Sofa.PythonScriptController) :

    def onLoaded(self,node):
        self.landmarkCtrlp = dict() # landmark ctrlp as tuple (ComputeMaskedPosition, MechanicalObjects) which are used for picking
        self.showCtrlp = False

    def getLandmarkNames(self):
        return sorted(sceneCommon.controllableLandmarks.keys())

#    def getFrameNames(self): # not used for now, landmarks are controlled in the world reference frame
#        return sorted(sceneCommon.controllableFrames.keys())

    def addController(self, name, landmarkName, mask):
        # TODO check if the requested controller already exists !
        controller = Anatomy.Constraint.PointTranslation(name, sceneCommon.controllableLandmarks[landmarkName], mask, compliance=userTargetCompliance, isCompliance=False, progressive=True)
        controller.landmarkName = landmarkName
        controller.progressiveTarget.step = SofaPython.units.length_from_SI(progressiveTargetStepTranslation)

        # landmark target control point
        landmarkTargetCtrlpNode = controller.node.createChild("landmarkTargetCtrlp")
        engine = landmarkTargetCtrlpNode.createObject("ComputeMaskedPosition", template="Vec3,Vec1", name="computeTargetFullPosition",
                                             position="@../../dofs.position",
                                             mask="@../mask/mapping.dofs", maskPosition="@../mask/target/target.target")
        dofs = landmarkTargetCtrlpNode.createObject(
            "MechanicalObject", template="Vec3", name=name, tags="landmark_ctrlp",
            position="@computeTargetFullPosition.output",
            drawMode=1, showObjectScale=SofaPython.units.length_from_SI(sceneCommon.scene.param.showLandmarkScale*1.1), showColor="0 1 0 1", showObject=self.showCtrlp
        )
        controller.node.init()
        landmarkTargetCtrlpNode.init()

        sceneCommon.landmarkControllers[name] = controller
        self.landmarkCtrlp[name] = (engine, dofs)
        return True

    def removeController(self, name):
        if not name in sceneCommon.landmarkControllers:
            piper.app.logWarning("Landmark {0} not controlled - not removed".format(name))
            return False
        sceneCommon.landmarkControllers[name].detachFromGraph()
        del sceneCommon.landmarkControllers[name]
        del self.landmarkCtrlp[name]
        return True

    def getControllerNames(self):
        return sceneCommon.landmarkControllers.keys()

    def removeAllControllers(self):
        for name in sceneCommon.landmarkControllers.keys():
            self.removeController(name)

    def getCurrentPosition(self, name):
        return sceneCommon.landmarkControllers[name].node.getParents()[0].getObject("dofs").position[0]

    def getMask(self, name):
        return sceneCommon.landmarkControllers[name].mask

    def getTarget(self, name):
        return sceneCommon.landmarkControllers[name].getTarget()

    def setTarget(self, name, dofIndex, value):
        target=sceneCommon.landmarkControllers[name].getTarget()
        target[sceneCommon.dofToTargetIndex(int(dofIndex), sceneCommon.landmarkControllers[name].mask)]=value # int(dofIndex) workaround for dof beeing a float
        sceneCommon.landmarkControllers[name].setTarget(target)

    def setCompliance(self, name, compliance) :
        sceneCommon.landmarkControllers[name].compliance.compliance = compliance
        sceneCommon.landmarkControllers[name].compliance.reinit()

    def setShowCtrlp(self, show):
        self.showCtrlp = show
        for ctrlp in self.landmarkCtrlp.itervalues():
            ctrlp[1].showObject=show

    def getCtrlpPosition(self, name):
        self.landmarkCtrlp[name][0].updateIfDirty()
        return self.landmarkCtrlp[name][1].position[0]

class FrameControllersQmlInteractor(Sofa.PythonScriptController) :

    def initGraph(self,node):
        self.boneNames = list()
        for bone in sceneCommon.model.getSolidsByTags({"bone"}):
            self.boneNames.append(bone.name)
        self.boneNames.sort()
        self.boneToFrame = dict()
        for frameName, frameRigid in sceneCommon.controllableFrames.iteritems():
            if piper.anatomyDB.isEntitySubClassOf(frameName, "Frame"):
                if not frameRigid.boneName in self.boneToFrame:
                    self.boneToFrame[frameRigid.boneName] = list()
                self.boneToFrame[frameRigid.boneName].append(frameName)


    def getFrameNames(self, getBoneInertiaFrame=True):
        if getBoneInertiaFrame:
            return sorted(sceneCommon.controllableFrames.keys())
        else:
            frameNames = list()
            for name in sceneCommon.controllableFrames.iterkeys():
                if "B_" != name[0:2]:
                    frameNames.append(name)
            return sorted(frameNames)

    def getBoneNames(self):
        return self.boneNames

    def getBoneFrameNames(self, boneName):
        if boneName in self.boneToFrame:
            return self.boneToFrame[boneName]
        else :
            return list()

    def addController(self, name, frame1, frame2, isRelative, mask):
        if not frame1 in sceneCommon.controllableFrames:
            return False
        if not frame2 in sceneCommon.controllableFrames:
            return False
        if name in sceneCommon.frameControllers:
            return False

        controller = Anatomy.Constraint.RigidRigidTransformation(
                       name, sceneCommon.controllableFrames[frame1],
                       sceneCommon.controllableFrames[frame2], mask,
                       compliance=userTargetCompliance, isCompliance=False, progressive=True, isRelative=bool(isRelative))
        if controller.isRelative: # init dynamically created nodes
            for frame in [controller.frame1, controller.frame2]:
                frame.showCount = 0
                frame.dofs.showObjectScale = SofaPython.units.length_from_SI(sceneCommon.scene.param.showRigidScale)
                frame.node.init()
        controller.frame1Name = frame1
        controller.frame2Name = frame2
        controller.progressiveTarget.step = SofaPython.Tools.listToStr([SofaPython.units.length_from_SI(progressiveTargetStepTranslation), progressiveTargetStepRotation])
        controller.node.init()
        sceneCommon.frameControllers[name] = controller

        return True

    def setShowControlledFrames(self, name, show):
        controller = sceneCommon.frameControllers[name]
        controller.frame1.showCount += 1 if show else -1
        controller.frame1.dofs.showObject = (controller.frame1.showCount>0)
        controller.frame2.showCount += 1 if show else -1
        controller.frame2.dofs.showObject = (controller.frame2.showCount>0)

    def setCompliance(self, name, compliance) :
        sceneCommon.frameControllers[name].setCompliance(compliance)

    def removeController(self, name):
        if not name in sceneCommon.frameControllers:
            piper.app.logWarning("Frame {0} not controlled - not removed".format(name))
            return False
        sceneCommon.frameControllers[name].detachFromGraph() # TODO should be doable in the __del__ of Constraint.RigidRigidTransformation
        del sceneCommon.frameControllers[name]

    def removeAllControllers(self):
        for name in sceneCommon.frameControllers.keys():
            self.removeController(name)

    def getControllerNames(self):
        return sceneCommon.frameControllers.keys()

    def getCurrentPosition(self, name):
        return sceneCommon.frameControllers[name].getCurrentPosition()

    def getMask(self, name):
        return sceneCommon.frameControllers[name].mask

    def getTarget(self, name):
        return list(sceneCommon.frameControllers[name].getTarget())

    def setTarget(self, name, dofIndex, value):
        target=list(sceneCommon.frameControllers[name].getTarget())
        target[sceneCommon.dofToTargetIndex(int(dofIndex), sceneCommon.frameControllers[name].mask)]=value # int(dofIndex) workaround for dof beeing a float
        sceneCommon.frameControllers[name].setTarget(target)

class SpineControllerQmlInteractor(Sofa.PythonScriptController) :
    def onLoaded(self,node):
        self.splineEngine = None # the engine which compute the spline
        self.ctrlpNode = None # Node which contains ctrlp dof and topology to pick and display them
        self.spline = None # contact object for the spline
        self.contact = None # The contact to constrain the spine on the spline
        self.curvatureCtrlp = dict() # the control points which control the curvature, to ensure C1 continuity of the spline
        self.referenceCtrlp = dict() # bezier segments end points, to ensure C1 continuity and intuitive interactive manipulation

    def addController(self, initPoints):
        if sceneCommon.spine is None:
            piper.app.logError("No spine defined on the model")
            return False
        self.spline = ContactMapping.StructuralAPI.ContactObject()
        self.curvatureCtrlp.clear()
        self.referenceCtrlp.clear()

        self.spline.node = sceneCommon.scene.node.createChild("spineSpline")
        ctrlp = list()
        for pt in initPoints:
            refName = piper.anatomyDB.getReferenceName(pt)
            if not refName in sceneCommon.spineReferencePoints:
                piper.app.logWarning("Missing vertebra {0}".format(pt))
                continue
            ctrlp.append(sceneCommon.spineReferencePoints[refName].dofs.position[0][0:3])

        self.splineEngine = self.spline.node.createObject("SplineEngine", template="Vec3", name="spline", useBezier=True, initSpline=SofaPython.Tools.listListToStr(ctrlp), ds=0.01)
        self.spline.model = self.spline.node.createObject("MechanicalObject", template="Vec3", name="dofs", position="@spline.position", reset_position="@spline.position")
        self.spline.topology = self.spline.node.createObject("MeshTopology", name="topology", position="@dofs.position", edges="@spline.edges", drawEdges=True)
        self.spline.node.createObject("VisualStyle", displayFlags="hideBehaviorModels")
        self.spline.node.createObject("FixedConstraint", fixAll=True)
        # initialize curvatureCtrlp, spline is a sequence of degree 3 Bezier curves
        ctrlpDisplayEdges=list()
        self.referenceCtrlp[0]=[1]
        ctrlpDisplayEdges+=[0,1]
        for i in range(2,(len(ctrlp)-2)*3,3):
            self.curvatureCtrlp[i]=i+2
            self.curvatureCtrlp[i+2]=i
            ctrlpDisplayEdges+=[i,i+1]
            ctrlpDisplayEdges+=[i+1,i+2]
            self.referenceCtrlp[i+1]=[i,i+2]
        self.referenceCtrlp[2+(len(ctrlp)-2)*3+1]=[2+(len(ctrlp)-2)*3]
        ctrlpDisplayEdges+=[2+(len(ctrlp)-2)*3,2+(len(ctrlp)-2)*3+1]

        # MechanicalObject to pick the spline control points
        self.ctrlpNode = self.spline.node.createChild("spineSplineCtrlp")
        self.ctrlpNode.createObject("MechanicalObject", template="Vec3", name="ctrlp", tags="spine_ctrlp", position="@../spline.controlPoints", reset_position="@../spline.controlPoints", drawMode=1, showObjectScale=SofaPython.units.length_from_SI(sceneCommon.scene.param.showLandmarkScale), showColor="0 1 0 1")
        self.ctrlpNode.createObject("MeshTopology", name="topology", position="@ctrlp.position", edges=SofaPython.Tools.listToStr(ctrlpDisplayEdges)) # just to display bezier curve controls

        # create the constraint between the vertebra centers and the spline
        self.contact = ContactMapping.StructuralAPI.Contact("spine_posture", sceneCommon.spine, self.spline, compliance=5e-3, isCompliance=False, restLength=0., useTangentPlaneProjectionMethod=False, rejectBorder=True, isObject2Polyline=True)
        self.contact.closestPoints.drawMode=1
        # replace the UniformaCompliance by a LinearDiagonalCompliance, TODO clean that
        differenceNode = self.contact.node.getChild("difference")
        differenceNode.removeObject(self.contact.compliance)
        self.contact.compliance = differenceNode.createObject("LinearDiagonalCompliance", isCompliance=False, complianceMin=spineTargetCompliance, errorMin=SofaPython.units.length_from_SI(spineTargetErrorMin))
        self.spline.node.init()
        self.contact.node.init()
        return True

    def removeController(self):
        # TODO remove node
#        self.contact.detachFromFraph()
        self.contact.node.detachFromGraph()
        self.contact = None
        self.spline.node.detachFromGraph()
        self.spline = None
        self.splineEngine = None
        self.spline = None
        self.contact = None
        self.curvatureCtrlp.clear()
        self.referenceCtrlp.clear()

    def getSpineDistanceToTarget(self):
        """ return the [cumDistance, maxDistance] of the model spine to the target spline
            cumDistance is the sum of the individual distance of each vertebra to the spine
        """
        if self.contact is None:
            return [0,0]
        cumDistance = 0
        maxDistance = 0
        for pos in self.contact.node.getChild("difference").getObject("dofs").position:
            d = la.norm(pos)
            cumDistance += d
            if d > maxDistance:
                maxDistance = d
        return [cumDistance, maxDistance]

    def setCompliance(self, compliance):
        if self.contact is None:
            return
        self.contact.compliance.complianceMin = compliance
        self.contact.compliance.reinit()

    def getNbCtrlp(self):
        if self.splineEngine is None:
            return 0
        return len(self.splineEngine.controlPoints)

    def setShowCtrlp(self, show):
        if self.ctrlpNode is None:
            return
        self.ctrlpNode.getObject("ctrlp").showObject=show
        self.ctrlpNode.getObject("topology").drawEdges=show

    def setCtrlpPosition(self, index, position):
        if self.splineEngine is None:
            return
        allCtrlp = self.splineEngine.controlPoints
        if index in self.curvatureCtrlp:
            # align the curvature ctrlp keeping his distance to the mid point
            # so that the curve stays C1
            iRefCtrlp = (index+self.curvatureCtrlp[index])/2
            refCtrlp = np.asarray(allCtrlp[iRefCtrlp])
            modifiedCtrlp = np.asarray(position)
            curvatureCtrlp = np.asarray(allCtrlp[self.curvatureCtrlp[index]])
            u = refCtrlp-modifiedCtrlp
            u/= la.norm(u)
            allCtrlp[self.curvatureCtrlp[index]] = (refCtrlp + la.norm(curvatureCtrlp-refCtrlp)*u).tolist()
        if index in self.referenceCtrlp:
            # move curvature ctrlp accordingly
            # so that the curve stays C1
            u = np.asarray(position) - np.asarray(allCtrlp[index])
            for iCtrlp in self.referenceCtrlp[index]:
                allCtrlp[iCtrlp] = (np.asarray(allCtrlp[iCtrlp])+u).tolist()
        allCtrlp[index]=position
        self.splineEngine.controlPoints=SofaPython.Tools.listListToStr(allCtrlp)

    def getCtrlpPosition(self, index):
        if self.splineEngine is None:
            return
        return self.splineEngine.controlPoints[index]

class SpinePredictorQmlInteractor(Sofa.PythonScriptController) :

    def onLoaded(self,node):
        self.rootNode = node
        self.polyline = None
        self.contact = None
        self.displayNode = None
        self.showTargetType = "absolute"

    def addController(self):
        if sceneCommon.spine is None:
            piper.app.logError("Human body model has no spine, spine predictor controller cannot be created")
            return False
        pelvicSkeletonRefName = piper.anatomyDB.getReferenceName("Pelvic_skeleton")
        if not pelvicSkeletonRefName in sceneCommon.scene.rigids:
            piper.app.logError("Human body model has no pelvic skeleton, spine predictor controller cannot be created")
            return False
        self.pelvicSkeletonRigid = sceneCommon.scene.rigids[pelvicSkeletonRefName]
        self.polyline = ContactMapping.StructuralAPI.ContactObject()
        self.polyline.node = self.pelvicSkeletonRigid.node.createChild("spinePredictorPolyline")
        self.polyline.model = self.polyline.node.createObject("MechanicalObject", template="Vec3", name="dofs")
        self.polyline.topology = self.polyline.node.createObject("MeshTopology", name="topology", position="@dofs.position", drawEdges=False)
        self.mapping = self.polyline.node.createObject("RigidMapping", name="mapping", globalToLocalCoords=True)
        self.polyline.node.createObject("VisualStyle", displayFlags="hideBehaviorModels")
        # create the constraint between the vertebra centers and the spline
        self.contact = ContactMapping.StructuralAPI.Contact("spine_posture", sceneCommon.spine, self.polyline, compliance=5e-3, restLength=0, useTangentPlaneProjectionMethod=False, rejectBorder=True, isObject2Polyline=True)
        self.contact.closestPoints.drawMode = 0

        # replace the UniformaCompliance by a LinearDiagonalCompliance, TODO clean that
        differenceNode = self.contact.node.getChild("difference")
        differenceNode.removeObject(self.contact.compliance)
        self.contact.compliance = differenceNode.createObject("LinearDiagonalCompliance", isCompliance=False, complianceMin=spineTargetCompliance, errorMin=SofaPython.units.length_from_SI(spineTargetErrorMin))
        self.polyline.node.init()
        self.contact.node.init()

        # to display raw result
        self.displayNode = self.rootNode.createChild("spinePredictorDisplay")
        self.displayNode.createObject("MeshTopology", name="topology", drawEdges=True)

        self.updateShowTarget()

        return True

    def removeController(self):
        if not self.contact is None:
            self.contact.node.detachFromGraph()
            self.contact=None
        if not self.polyline is None:
            self.polyline.node.detachFromGraph()
            self.polyline = None
        if not self.displayNode is None:
            self.displayNode.detachFromGraph()
            self.displayNode = None

    def getSpineDistanceToTarget(self):
        """ return the [cumDistance, maxDistance] of the model spine to the target spline
            cumDistance is the sum of the individual distance of each vertebra to the spine
        """
        if self.contact is None:
            return [0,0]
        cumDistance = 0
        maxDistance = 0
        for pos in self.contact.node.getChild("difference").getObject("dofs").position:
            d = la.norm(pos)
            cumDistance += d
            if d > maxDistance:
                maxDistance = d
        return [cumDistance, maxDistance]


    def setCompliance(self, compliance):
        if self.contact is None:
            return
        self.contact.compliance.complianceMin = compliance
        self.contact.compliance.reinit()

    def generateCSFile(self, CSFilePath):
        nbFrames = 0
        with open(CSFilePath, 'w') as CSFile:
            CSFile.write("# Generated by Piper - module phys-posi \n")
            for name,frame in sceneCommon.controllableFrames.iteritems():
                if piper.anatomyDB.exists(name) and piper.anatomyDB.isEntityFromBibliography(name, "ISB_BCS") and ( piper.anatomyDB.isEntityPartOf(name, "Vertebral_column", True) or "Pelvic_frame" == name ):
                    rotMat = SofaPython.Quaternion.to_matrix(frame.dofs.position[0][3:])
                    wp2name = name.split('_')[0] # TODO workaround octave script does not use anatomyDB names
                    if "Pelvic" == wp2name : # TODO workaround octave script uses Sacrum instead of pelvis
                        self.PelvicCurrent = frame.dofs.position[0]
                        wp2name = "Sacrum"
                    if "L5" == wp2name : # TODO hack to keep current L5 frame to transform polyline back to that frame
                        self.L5Current = frame.dofs.position[0]
                    CSFile.write("{0} {1} {2} {3} {4} \n".format(
                        wp2name,
                        " ".join(map(str, frame.dofs.position[0][0:3])),
                        " ".join(map(str, rotMat[:,0])),
                        " ".join(map(str, rotMat[:,1])),
                        " ".join(map(str, rotMat[:,2])) ))
                    nbFrames+=1
        if 25 != nbFrames:
            piper.app.logError("Spine predictor: missing {0} vertebra and/or pelvic frame".format(25-nbFrames))
            return False
        return True

    def generateGravityFile(self, gravityFilePath):
        metadata = piper.sofa.project().model().metadata()
        with open(gravityFilePath, 'w') as gravityFile:
            gravityFile.write("# Generated by Piper - module phys-posi \n")
            if metadata.isGravityDefined():
                g = metadata.gravity().flatten()
                gravityFile.write("{0} {1} {2}\n".format(g[0], g[1], g[2]))
            else :
                gravityFile.write("0 0 0\n")
        return True


    def setPolyline(self, CSFilePath, polylineFilePath):
        self.getL5Target(CSFilePath)
        TL5target = np.identity(4) # construct transformation matrix
        TL5target[0:3,3] = self.L5Target[0:3]
        TL5target[0:3,0:3] = SofaPython.Quaternion.to_matrix(self.L5Target[3:])
        TL5targetInv = la.inv(TL5target)
        TL5current = np.identity(4) # construct transformation matrix
        TL5current[0:3,3] = self.L5Current[0:3]
        TL5current[0:3,0:3] = SofaPython.Quaternion.to_matrix(self.L5Current[3:])

        polyline = []
        absolutePolyline = []
        edges = []
        T = TL5current.dot(TL5targetInv) # global transformation to apply
        coordH = np.empty((4)) # homogeneous coordinate
        coordH[3] = 1
        with open(polylineFilePath, 'r') as polylineFile:
            for i,pointData in enumerate(polylineFile):
                coordH[:3] = map(float, pointData.split(','))
                absolutePolyline.append(coordH[:3].tolist())
                coordH = T.dot(coordH)
                polyline.append(coordH[:3].tolist())
                if i>0:
                    edges.append([i-1,i])
        self.mapping.initialPoints=""
        self.polyline.model.resize(len(polyline))
        self.polyline.model.position = SofaPython.Tools.listListToStr(polyline)
        self.mapping.reinit()
        self.polyline.topology.edges = SofaPython.Tools.listListToStr(edges)

        absoluteTopology = self.displayNode.getObject("topology")
        absoluteTopology.position = SofaPython.Tools.listListToStr(absolutePolyline)
        absoluteTopology.edges = SofaPython.Tools.listListToStr(edges)

    def getL5Target(self, CSFilePath):
        with open(CSFilePath, 'r') as CSFile:
            for CS in CSFile:
                if "L5" in CS:
                    values = CS.split(' ')
                    values.pop(0) # pop frame name
                    t = np.empty((3)) # origin
                    for i in range(3):
                        t[i] = values.pop(0)
                    R = np.empty((3,3)) # orientation
                    for i in range(3):
                        for j in range(3):
                            R[j,i] = values.pop(0)
                    self.L5Target =  np.concatenate(( t, np.array(SofaPython.Quaternion.from_matrix(R)) ))

    def setShowTarget(self, type="simulation"):
        # type in {"simulation", "absolute"}
        self.showTargetType = type
        self.updateShowTarget()

    def updateShowTarget(self):
        if self.polyline is None or self.contact is None or self.displayNode is None:
            return
        if "simulation" == self.showTargetType:
            self.polyline.topology.drawEdges = True
            self.contact.closestPoints.drawMode = 1
            self.displayNode.getObject("topology").drawEdges = False
        elif "absolute" == self.showTargetType:
            self.polyline.topology.drawEdges = False
            self.contact.closestPoints.drawMode = 0
            self.displayNode.getObject("topology").drawEdges = True
        else:
            print "ERROR: SpinePredictorQmlInteractor.setShowTarget: invalid type", self.showTargetType

    def getPelvisOrientation(self, CSFilePath):
        pelvisOrientation = None
        with open(CSFilePath, 'r') as CSFile:
            for CS in CSFile:
                if "Sacrum" in CS:
                    values = CS.split(' ')
                    values.pop(0) # pop frame name
                    for i in range(3): # pop origin
                        values.pop(0)
                    R = np.empty((3,3))
                    for i in range(3):
                        for j in range(3):
                            R[j,i] = values.pop(0)
                    pelvisOrientation = SofaPython.Quaternion.quatToRotVec( SofaPython.Quaternion.from_matrix(R) ).tolist() # maybe not the shortest path
                elif "L5" in CS:
                    values = CS.split(' ')
                    values.pop(0) # pop frame name
                    t = np.empty((3)) # origin
                    for i in range(3):
                        t[i] = values.pop(0)
                    R = np.empty((3,3)) # orientation
                    for i in range(3):
                        for j in range(3):
                            R[j,i] = values.pop(0)
                    self.L5Target =  np.concatenate(( t, np.array(SofaPython.Quaternion.from_matrix(R)) ))
        return pelvisOrientation

    def getConfidenceValue(self, confidenceFilePath):
        with open(confidenceFilePath, 'r') as confidenceFile:
            return int(confidenceFile.readline().strip()) - 1 # start counting at 0

