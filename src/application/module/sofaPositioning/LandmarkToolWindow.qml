// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0

import SofaScene 1.0
import SofaApplication 1.0
import SofaInteractors 1.0
import SofaManipulators 1.0

import Piper 1.0

import "dof.js" as DofConstant

ModuleToolWindow {
    id: root

    title: "Landmark Controller"

    property var dofControllerComponent: Qt.createComponent("qrc:///module/sofaPositioning/DofController.qml");
    property var landmarkNames: []

    // current selected spline control point
    property string currentLandmarkCtrlpName: ""

    property Action interactor: Action {
        checkable: true
        checked: false
        iconSource: "qrc:/icon/cursor.png"
        text: "L"
        tooltip: "Landmark interactor"
        property string leftButtonHelp: "click on a landmark to select it, click on a target to move it by dragging the arrows"
        property string middleButtonHelp: cameraInteractor.middleButtonHelp
        property string rightButtonHelp: cameraInteractor.rightButtonHelp
        onToggled: {
            sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "setShowCtrlp", checked);
            currentLandmarkCtrlpName="";
            if (checked)
                SofaApplication.interactorComponent = selectLandmarkCtrlp;
            visualizationToolWindow.setLandmarkVisible(checked);
        }
    }

    property Component selectLandmarkCtrlp: UserInteractor_MoveCamera {
        hoverEnabled: true

        function init() {
            moveCamera_init();

            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                if (null !== sofaScene.selectedComponent) {
                    // if a component is selected, try to catch one of its manipulator
                    var selectable = sofaViewer.pickObjectWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["manipulator"]);
                    if(selectable && selectable.manipulator) {
                        sofaScene.selectedManipulator = selectable.manipulator;
                        if(sofaScene.selectedManipulator.mousePressed)
                            sofaScene.selectedManipulator.mousePressed(mouse, sofaViewer);
                        if(sofaScene.selectedManipulator.mouseMoved)
                            setMouseMovedMapping(sofaScene.selectedManipulator.mouseMoved);
                        return;
                    }
                }
                // try to catch a control point
                var selectableParticle = sofaViewer.pickParticleWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["landmark_ctrlp"]);
                if(selectableParticle && selectableParticle.sofaComponent) {
                    sofaScene.selectedComponent = selectableParticle.sofaComponent;
                    root.show();
                    root.currentLandmarkCtrlpName = selectableParticle.sofaComponent.name();
                }
                else {
                    sofaScene.selectedComponent = null;
                    sofaScene.selectedManipulator = null;
                    root.currentLandmarkCtrlpName = "";
                    // try to catch a landmark to select it in the landmark controller window
                    selectableParticle = sofaViewer.pickParticleWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["landmark"]);
                    if(selectableParticle && selectableParticle.sofaComponent) {
                        var pathNameSplit = selectableParticle.sofaComponent.getPathName().split("/");
                        root.selectLandmark(pathNameSplit[pathNameSplit.length-2]);
                    }
                }
            });

            addMouseReleasedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                if(sofaScene.selectedManipulator && sofaScene.selectedManipulator.mouseReleased)
                    selectedManipulator.mouseReleased(mouse, sofaViewer);
                sofaScene.selectedManipulator = null;
                setMouseMovedMapping(null);
            });
        }

    }

    function removeManipulator(name) {
        if (null !== sofaScene.selectedManipulator && name === sofaScene.selectedManipulator.objectName) {
            // kind of impossible case, just in case
            sofaScene.selectedManipulator = null;
        }
        if (name === root.currentLandmarkCtrlpName) {
            sofaScene.selectedComponent = null;
            root.currentLandmarkCtrlpName = "";
        }
        var manip = sofaScene.removeManipulatorByName(name);
        manip[0].destroy();
    }

    Component {
        id: manipulatorComponent

        Manipulator {
            id: manipulator

            property var sofaScene
            property string name: ""
            property string allAxis: "xyz"
            property DofController dofController

//            visible: manipulator.landmarkName===root.currentLandmarkCtrlpName

            Manipulator3D_Translation {
                id: tx
                visible: manipulator.visible && (-1 !== manipulator.allAxis.indexOf(axis))
                axis: "x"
                onPositionChanged: {
                    manipulator.position = position;
                    if (-1 !== manipulator.allAxis.indexOf(axis)) {
                        dofController.setValue(0, position.x);
                    }
                }
            }

            Manipulator3D_Translation {
                id: ty
                visible: manipulator.visible && (-1 !== manipulator.allAxis.indexOf(axis))
                axis: "y"
                onPositionChanged: {
                    manipulator.position = position;
                    if (-1 !== manipulator.allAxis.indexOf(axis))
                        dofController.setValue(1, position.y);
                }
            }

            Manipulator3D_Translation {
                id: tz
                visible: manipulator.visible && (-1 !== manipulator.allAxis.indexOf(axis))
                axis: "z"
                onPositionChanged: {
                    manipulator.position = position;
                    if (-1 !== manipulator.allAxis.indexOf(axis))
                        dofController.setValue(2, position.z);
                }
            }

            function getPosition() {
                var positionArray = sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "getCtrlpPosition", name);
                position = Qt.vector3d(positionArray[0], positionArray[1], positionArray[2]);
            }

            function updatePosition() {
                if (visible) getPosition();
            }

            Component.onCompleted: getPosition()

            onVisibleChanged: {
                if (visible) getPosition();
            }
        }

    }

    function updateEnabled() {
        // TODO: prevent from adding twice the same controller

        // enable/disable add button
        if (landmarkListModel.count>0
//                && frameListModel.count>0
//                && frameCombo.currentIndex>=0
                && landmarkCombo.currentIndex>=0
                && (-1 !== landmarkNames.indexOf(landmarkCombo.editText))
                && (dofXCheckBox.checked || dofYCheckBox.checked || dofZCheckBox.checked ) )
            addControllerButton.enabled=true;
        else
            addControllerButton.enabled=false;

        //TODO disable if a dof is already controlled
    }

    function selectLandmark(landmark) {
        var index = landmarkCombo.find(landmark);
        if (-1 !== index)
            landmarkCombo.currentIndex = index;
    }

    ListModel {
        id: landmarkListModel
    }

//    ListModel {
//        id: frameListModel
//    }

    function init() {
        landmarkListModel.clear();
        landmarkNames = sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "getLandmarkNames");
        for (var i=0; i < landmarkNames.length; i++) {
            landmarkListModel.append({text: landmarkNames[i]});
        }

//        var frameNames = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getFrameNames");
////                console.log("FrameController "+frameNames);
//        for (i=0; i < frameNames.length; i++) {
//            frameListModel.append({text: frameNames[i]});
//        }

        root.updateEnabled();
    }

    function addController(landmark, mask, initialValue) {

        var allAxis="";
        for (var k=0; k< mask.length; ++k) {
            allAxis += mask[k]===1?DofConstant.dofLabel[k]:"";
        }
        var controllerName = landmark+"_"+allAxis;

        var controllerCreated = sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "addController", controllerName, landmark, mask);
        if (!controllerCreated)
            return false;

        var _initialValue = initialValue;
        if (typeof _initialValue === 'undefined')
            _initialValue = sofaScene.sofaPythonInteractor.call("landmarkControllerInteractor", "getTarget", controllerName);

        var myController = dofControllerComponent.createObject(landmarkControllerParent, {
                                                                   "controllerName": controllerName,
                                                                   "sofaPythonInteractorName": "landmarkControllerInteractor",
                                                                   "mask": mask,
                                                                   "initialValue": _initialValue,
                                                                   "defaultStiffness": moduleParameter.PhysPosiInter_targetDefaultStiffness
                                                               } );
        var myManipulator = manipulatorComponent.createObject(root, {objectName:controllerName, sofaScene: sofaScene, name: controllerName, allAxis: allAxis, dofController:myController, visible: Qt.binding(function () {return this.name === root.currentLandmarkCtrlpName;})})
        myController.valueChangedByUser.connect(myManipulator.updatePosition); // to update manipulator position
        sofaScene.onStepEnd.connect(myManipulator.updatePosition); // [TL] TODO sub optimal solution, send too many events
        sofaScene.onReseted.connect(myManipulator.updatePosition);
        sofaScene.addManipulator(myManipulator);
        myController.removed.connect(root.removeManipulator);

        return true;
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        width = 500;
        setCurrentWindowSizeMinimum();
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        GroupBox {
            Layout.fillWidth: true
            title: "Add landmark controller"
            ColumnLayout {
                width:parent.width
                RowLayout {
                    Layout.fillWidth: true
//                    ComboBox {
//                        id: frameCombo
//                        Layout.fillWidth: true
//                        //                tooltip: "Select frame"
//                        model: frameListModel
//                        onCurrentIndexChanged : root.updateEnabled()
//                    }
                    ComboBox {
                        id: landmarkCombo
                        Layout.fillWidth: true
                        //                tooltip: "Select landmark"
                        editable: true
                        model: landmarkListModel
                        onCurrentIndexChanged : root.updateEnabled()
                        onEditTextChanged: root.updateEnabled()
                    }
                    Button {
                        id: addControllerButton
                        iconSource: "qrc:///icon/list-add.png"
                        tooltip:"Add landmark controller"
                        enabled: false
                        onClicked: {
                            var mask = [];
                            mask = mask.concat([dofXCheckBox.checked?1:0]);
                            mask = mask.concat([dofYCheckBox.checked?1:0]);
                            mask = mask.concat([dofZCheckBox.checked?1:0]);

                            addController(landmarkCombo.currentText, mask);
                        }
                    }
                }
                RowLayout {
                    CheckBox {
                        id: dofXCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "x"
                                color: DofConstant.dofLabelColor[0]
                            }
                        }
                    }
                    CheckBox {
                        id: dofYCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "y"
                                color: DofConstant.dofLabelColor[1]
                            }
                        }
                    }
                    CheckBox {
                        id: dofZCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "z"
                                color: DofConstant.dofLabelColor[2]
                            }
                        }
                    }
                }
            }
        }
        ScrollView {
            Layout.minimumHeight: 100
            Layout.minimumWidth: 300
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                id: landmarkControllerParent
                width: root.width
            }
        }
        RowLayout {
            Layout.alignment: Qt.AlignLeft
            MouseInteractorToolButton {
                action: cameraInteractor
            }
            MouseInteractorToolButton {
                action: interactor
            }
        }
    }

}
