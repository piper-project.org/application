# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import shutil

import Sofa
import SofaPython.units
import SofaPython.mass
import SofaPython.Tools
import SofaPiper.sml
import SofaPiper.tools
import Compliant.sml
import Anatomy.sml.rigidAffine

import piper.app
import piper.hbm
import piper.sofa

import sceneCommon

Compliant.StructuralAPI.geometric_stiffness = 2
Compliant.sml.printLog = False
Anatomy.sml.rigidAffine.printLog = False

def createSceneAndController(rootNode):
    piper.app.logStart("Loading physics based Pre-Position...")

    sceneCommon.init("PhysPosiInter")

    rootNode.createObject("RequiredPlugin", name="SofaPiper")

    tmpDir = piper.app.tempDirectoryPath(sceneCommon.currentModule)
    shutil.rmtree(tmpDir, ignore_errors=True)

    sceneCommon.scene = Anatomy.sml.rigidAffine.ScenePrepare(rootNode, sceneCommon.model, tmpDir) # the prepare scene is just a local variable

    param = sceneCommon.scene.param # shortcut
#   lazy parameters
    sceneCommon.scene.param.voxelSize = sceneCommon.getVoxelSize() # m
    sceneCommon.scene.param.coarseningFactor = piper.sofa.project().moduleParameter().getUInt("PhysPosiInter","voxelCoarsening")
    param.nodeDensity["default"] = piper.sofa.project().moduleParameter().getUInt("PhysPosiInter","affineDensity") # m^-3
    param.numberOfNodes["capsule"] = piper.sofa.project().moduleParameter().getUInt("PhysPosi","nbAffinePerCapsule")

    sceneCommon.scene.param.autoConnectSkin = not sceneCommon.model.isSkinMultiEntities # specific connections are set in the model

    if piper.sofa.project().moduleParameter().getBool(sceneCommon.currentModule, "customAffine"):
        sceneCommon.addCustomAffines()

    param.softTissueTags={"skin"}
    param.mappedObjectTags=set()
    if piper.sofa.project().moduleParameter().getBool(sceneCommon.currentModule, "capsuleEnabled"):
        param.softTissueTags.update({"ligament","capsule"})
    else:
        param.mappedObjectTags.update({"ligament","capsule"})

    sceneCommon.scene.param.GPDensity = 1
    sceneCommon.scene.param.GPType = "332"

    param.sampleSurfaceRatherThanVolume = {"capsule"}
    sceneCommon.scene.param.samplingMethod='local'

    sceneCommon.scene.param.exportShapeFunction = True

    sceneCommon.scene.param.showRigid=True
    sceneCommon.scene.param.showRigidScale=0.05

    sceneCommon.scene.param.showAffine=True
    sceneCommon.scene.param.showAffineScale=0.02

    sceneCommon.scene.param.showRigidVertex = True

    sceneCommon.scene.createScene()

    return rootNode

def bwdInitGraph(node):
    if piper.app.Context.instance().busyTaskCancelRequested():
        return node
    sceneCommon.scene.export()
    return node
