// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import Piper 1.0

import "dof.js" as DofConstant

GroupBox {
    id : root
    title : controllerName

    property var controllerName
    property var sofaPythonInteractorName
    property var mask
    property var initialValue
    property double defaultStiffness
    property var dofComponents: ({})
    property var currentPosition
    property bool enableShowDofsButton: false

    signal valueChangedByUser()
    signal removed(string controllerName)

    Layout.fillWidth: true

    // remove this controller
    function remove() {
        sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "removeController", controllerName);
        root.destroy();
        root.removed(controllerName);
    }

    function updateCurrentPosition() {
        root.currentPosition = sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "getCurrentPosition", controllerName);
    }

    Connections {
        target: sofaScene
        onStepEnd : updateCurrentPosition() // TODO optimize when not visible
        onReseted : updateCurrentPosition()
    }

    RowLayout {
        width: parent.width
        GridLayout {
            id: dofComponentContainer
            flow: GridLayout.TopToBottom
            rows: 2
        }
        Item {
            Layout.fillWidth: true
        }
        GridLayout {
            id: constraintToolContainer
            columns: 2
            Label {
                text: "k:"
                Layout.alignment: Qt.AlignRight
            }
            StiffnessInput {
                id: stiffness
                onValueChanged: {
                    sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "setCompliance", controllerName, 1./value);
                }
                ToolTip {
                    text: "Constraint stiffness"
                }
            }
            Button {
                Layout.row: 1
                Layout.column: 1
                iconSource: "qrc:///icon/list-remove.png"
                tooltip: "Remove this controller"
                anchors.right: parent.right
                onClicked: remove()
            }
        }
    }

    Component {
        id: dofComponent
        RowLayout {

            spacing: 2

            property int index
            property var initialValue

            function isAngularDof() {return index>2;}

            function setValue(value) {
                if (undefined === value) return;
                setValue.scriptEdit = true;
                valueSpinBox.value = isAngularDof() ? Math.round(value*180/Math.PI) : value
                setValue.scriptEdit = false;
            }

            Label {
                color: DofConstant.dofLabelColor[parent.index]
                text: DofConstant.dofLabel[parent.index]+":"
            }
            SpinBox {
                id: valueSpinBox
                implicitWidth: DofConstant.itemWidth
                suffix: isAngularDof() ? "°" : context.modelLengthUnit
                minimumValue: isAngularDof() ? -360 : -Infinity
                maximumValue: isAngularDof() ? 360 : Infinity
                stepSize: isAngularDof() ? 5 : 5e-3 * DofConstant.lengthUnits[context.modelLengthUnit]
                decimals: isAngularDof() ? 1 : DofConstant.dofNbDecimals[context.modelLengthUnit]
//                value: isAngularDof() ? Math.round(parent.initialValue*180/Math.PI) : parent.initialValue
                onValueChanged: {
                    sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "setTarget", controllerName, parent.index, isAngularDof() ? value*Math.PI/180 : value);
                    if (!setValue.scriptEdit) valueChangedByUser();
                }
                Component.onCompleted: {
                    setValue(parent.initialValue);
                    // necessary when the initial value is not the current simulation value (target loading)
                    sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "setTarget", controllerName, parent.index, isAngularDof() ? value*Math.PI/180 : value);
                }
            }
            ToolTip {
                text: "Target value"
            }
        }
    }

    Component {
        id: dofCurrentComponent
        RowLayout {

            spacing: 2

            property int index
            property int targetIndex

            property bool isAngularDof: DofConstant.isAngularDof(index)
            function updateCurrentValue() {
                currentValue.text = (isAngularDof ? (root.currentPosition[index]*180/Math.PI).toFixed(0) : root.currentPosition[index].toFixed(DofConstant.dofNbDecimals[context.modelLengthUnit]));
            }
            Label {
                text: DofConstant.dofLabel[parent.index]+":"
            }
            TextField {
                id: currentValue
                readOnly: true
                implicitWidth: DofConstant.itemWidth
                ToolTip {
                    text: "Current value"
                }
            }
            Connections {
                target: root
                onCurrentPositionChanged : updateCurrentValue()
            }
        }
    }

    Component {
        id: showFramesButton
        CheckBox {
            text: "Frames"
            checked: false
            onCheckedChanged: {
                sofaScene.sofaPythonInteractor.call(sofaPythonInteractorName, "setShowControlledFrames", controllerName, checked);
            }
            ToolTip {
                text: "Show/Hide constrained frames"
            }
        }
    }

    Component.onCompleted: {
        var it=0;
        for (var i=0 ; i < mask.length ; i++) {
            if (mask[i]===1) {
                root.dofComponents[i]=dofComponent.createObject(dofComponentContainer, {
                                                                    "index": i,
                                                                    "initialValue": initialValue[it] } );
                dofCurrentComponent.createObject(dofComponentContainer, {
                                                     "index": i,
                                                     "targetIndex": it } );
                it+=1;                
            }
        }
        if (enableShowDofsButton)
            showFramesButton.createObject(constraintToolContainer, {
                                            "Layout.row":1,
                                            "Layout.column": 0 });
        stiffness.setValue(root.defaultStiffness);
        updateCurrentPosition();
    }

    function setValue(dofIndex, value) {
        if (dofIndex in root.dofComponents)
            root.dofComponents[dofIndex].setValue(value);
        else
            console.log("WARNING: "+controllerName+" does not control dof index "+dofIndex);
    }
}
