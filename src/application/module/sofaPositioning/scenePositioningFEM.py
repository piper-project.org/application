# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import Sofa
import SofaPython.Tools

import SofaPython.Tools
import Flexible.API
import Compliant.StructuralAPI
import Anatomy.sml
import SofaPiper.tools

import piper.common

import sceneCommon

Compliant.StructuralAPI.geometric_stiffness = 2

def createSceneAndController(rootNode):
    piper.common.logStart("Loading physics based positioning (FEM)...")

    sceneCommon.init()

    rootNode.createObject("RequiredPlugin", name="SofaPiper")
    rootNode.createObject('RequiredPlugin', name='Anatomy')

    sceneCommon.scene = Anatomy.sml.SceneFEM(rootNode, sceneCommon.model)

    sceneCommon.scene.param.voxelSize= piper.common.Context.instance().moduleParameter().getFloat("PhysPosiDefo","hexahedronSize") # m

    sceneCommon.scene.param.coarseningFactor=1
    sceneCommon.scene.param.showHexa =True
    sceneCommon.scene.param.behaviorLaw = 'hooke'
    sceneCommon.scene.param.gridType='branching'
#    sceneCommon.scene.param.contactCompliance=1E-5

    sceneCommon.scene.param.GPType = "332"

#    sceneCommon.scene.param.jointCompliance=1e-10

    sceneCommon.scene.createScene()

    rootNode.findData('gravity').value='0 0 0'

#    rootNode.createObject('CompliantImplicitSolver', name='odesolver', stabilization=1, neglecting_compliance_forces_in_geometric_stiffness=False)
    rootNode.createObject('CompliantPseudoStaticSolver', name='odesolver', iterations=1)
    rootNode.createObject('SequentialSolver', name='numsolver', iterations=250, precision=1e-14, iterateOnBilaterals=True)
    rootNode.createObject('LDLTResponse', name='response')

    rootNode.createObject('PythonScriptController', name="fixedBonesInteractor", filename="interactors.py", classname="FixedBonesQmlInteractor")
    rootNode.createObject('PythonScriptController', name="jointControllerInteractor", filename="interactors.py", classname="JointControllersQmlInteractor")
    rootNode.createObject('PythonScriptController', name="frameControllerInteractor", filename="interactors.py", classname="FrameControllersQmlInteractor")

    sceneCommon.addAnatomicalFrames()
    sceneCommon.addControllableFrameBones()
    sceneCommon.addControllableFrameWorld()

    return rootNode

    # all model nodes, to be exported
    sceneCommon.allNodes = sceneCommon.scene.nodes["FEM"].createChild("allNodes")
    # export all nodes mapped on flesh
    modelNode = sceneCommon.allNodes.createChild("model_allNodes")
    modelNode.createObject("PiperMeshLoader", name="loader", loadAllNodes=True)
    allNodesMO = modelNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=True)
    modelNode.createObject("BranchingCellOffsetsFromPositions", template="BranchingImageUC", name="cell", position ="@allNodes.position", src="@"+SofaPython.Tools.getObjectPath(sceneCommon.scene.labelImage.branchingImage), labels=SofaPython.Tools.listToStr(sceneCommon.scene.skinLabels), useGlobalIndices=True)
    Flexible.API.insertLinearMapping(modelNode,
        dofAffineNode=sceneCommon.scene.nodes["FEM"], cell="@cell.cell" ) # position=allNodesMO,
    modelNode.createObject("PiperExporter", template="Vec3", name="exporter", guiEventName="exportFullModel", positions="@allNodes.position", positionsId="@loader.positionId")

    # export entities nodes
    # TODO: for bones, use a RigidMapping under each bone Rigid3
    for solid in sceneCommon.model.solids.values():
        if not "bone" in solid.tags:
            continue
        print solid.id+"_allNodes"
        boneNode = sceneCommon.allNodes.createChild(solid.id+"_allNodes")
        SofaPiper.tools.meshLoader(boneNode, filename=solid.id, name="loader")
        allNodesMO = boneNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=True)
        boneNode.createObject("BranchingCellOffsetsFromPositions", template="BranchingImageUC", name="cell", position ="@allNodes.position",
            src="@"+SofaPython.Tools.getObjectPath(sceneCommon.scene.labelImage.branchingImage),
            labels=SofaPython.Tools.listToStr(sceneCommon.scene.entitiesProp.entities[solid.id].allLabels()), useGlobalIndices=True)
        Flexible.API.insertLinearMapping(boneNode,
            dofAffineNode=sceneCommon.scene.nodes["FEM"], cell="@cell.cell" ) # position=allNodesMO,
        boneNode.createObject("PiperExporter", template="Vec3", name="exporter", guiEventName="exportEntities", positions="@allNodes.position", positionsId="@loader.positionId")

    sceneCommon.addAnatomicalFrames()
    sceneCommon.addControllableFrameBones()
    sceneCommon.addControllableFrameWorld()

    return rootNode

def bwdInitGraph(node):
    boneNoHexaVertex=list()
    for solid in sceneCommon.model.solidsByTag["bone"]:
        if not solid.id in sceneCommon.scene.rigids:
            print "[scenePositioningFEM] WARNING: "+solid.name+" is not a bone"
            continue
        print solid.name, len(sceneCommon.scene.rigids[solid.id].dofsHexa.findData('position').value)
        if not len(sceneCommon.scene.rigids[solid.id].dofsHexa.findData('position').value):
            boneNoHexaVertex.append(solid.name)
    if len(boneNoHexaVertex):
        piper.common.logWarning("The following bones does not have any hexehedron vertex")
        piper.common.logWarning(", ".join(boneNoHexaVertex))
        piper.common.logWarning("Try to reduce the 'hexahedron size' in Module Parameters")
    piper.common.logDone("")
    sys.stdout.flush()
    return node
