// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQml 2.2
import QtQml.Models 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

import SofaScene 1.0
import SofaApplication 1.0
import SofaInteractors 1.0

import Piper 1.0
import AnatomicTreeModel 1.0
import AnatomyDB 1.0

ModuleToolWindow {
    id: root
    title: "Fixed bones"

    property Action interactor: Action {
        checkable: true
        checked: false
        iconSource: "qrc:/icon/cursor.png"
        text: "B"
        tooltip: "Fixed bone interactor"
        property string leftButtonHelp: "click on a bone to (un)fix it"
        property string middleButtonHelp: cameraInteractor.middleButtonHelp
        property string rightButtonHelp: cameraInteractor.rightButtonHelp
        onToggled: {
            if (checked)
                SofaApplication.interactorComponent = selectFixBone;
        }
    }

    property Component selectFixBone: UserInteractor_MoveCamera {
        hoverEnabled: true

        function init() {
            moveCamera_init();

            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                var selectable = sofaViewer.pickObjectWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["bone"]);
                if(selectable && selectable.sofaComponent) {
                    var pathNameSplit = selectable.sofaComponent.getPathName().split("/");
                    root.toggleBoneFixed(pathNameSplit[2]);
                }
            });

        }

    }

    function init() {
        entityTreeModel.updateEntityList();
    }


    function _setBoneFixed(boneName, toggle) { // [TL] looks like qml javascript does not support default argument value
        var index = entityTreeModel.model.findIndex(boneName, true);
        if (0 === index.length) {
            context.logWarning("Fixed bone controller: unknown bone: "+boneName+" - ignored");
            return false;
        }
        if (1 < index.length) {
            context.logWarning("Fixed bone controller: several bones with name: "+boneName+" - ignored");
            return false;
        }
        if (toggle)
            fixedBonesView.selection.select(index[0], ItemSelectionModel.Toggle);
        else
            fixedBonesView.selection.select(index[0], ItemSelectionModel.Select);
        return true;
    }

    function setBoneFixed(boneName) {
        return _setBoneFixed(boneName, false);
    }

    function toggleBoneFixed(boneName) {
        return _setBoneFixed(boneName, true);
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        height = 500;
        setCurrentWindowSizeMinimum();
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        RowLayout {
            Layout.fillWidth: true
            Button {
                text: "All"
                tooltip: "Select all bones"
                onClicked: {
                    fixedBonesView.selection.selectAll();
                }
            }
            Button {
                text: "None"
                tooltip: "Deselect all bones"

                onClicked: {
                    fixedBonesView.selection.clear();
                }
            }
            Button {
                text: "Invert"
                tooltip: "Invert current selection"

                onClicked: {
                    fixedBonesView.selection.invertSelection();
                }
            }
        }

        AnatomicTreeModel {
            id: entityTreeModel

            property var bones: []

            function  updateEntityList() {
                var regions = ["Head","Body_proper", "Right_upper_limb", "Left_upper_limb", "Right_lower_limb", "Left_lower_limb"]
                bones = sofaScene.sofaPythonInteractor.call("fixedBonesInteractor", "getBoneNames");
                qdataList = regions.concat(bones);
            }
        }

        SearchTree {
            id: searchTree

            Layout.fillWidth: true

            treeModel: entityTreeModel
            treeView: fixedBonesView

            Component.onCompleted: this.forceActiveFocus()
        }

        TreeView {
            id: fixedBonesView

            Layout.fillWidth: true
            Layout.fillHeight : true

            model: entityTreeModel.model

            selectionMode: SelectionMode.MultiSelection
            selection: ItemSelectionModel
            {
                model: entityTreeModel.model

                function selectAll() {
                    var allIndexes=model.getAllIndexes();
                    for (var i=0; i<allIndexes.length; ++i)
                        select(allIndexes[i], ItemSelectionModel.Select);
                }

                function invertSelection() {
                    var allIndexes = model.getLeafIndexes(); // only toggle if it is a leaf - toggling parents will select/deselect all children making the button useless
                    for (var i=0; i<allIndexes.length; ++i) {
                        select(allIndexes[i], ItemSelectionModel.Toggle);
                    }
                }

                onSelectionChanged: {
                    for (var i=0; i<selected.length; ++i) {
                        var bone = model.data(selected[i].topLeft);
                        if (entityTreeModel.bones.indexOf(bone)!==-1)
                            sofaScene.sofaPythonInteractor.call("fixedBonesInteractor", "setBoneFixed", bone, true);
                        // select children if any
                        var childrenIndexes = model.getChildrenIndexes(selected[i].topLeft, false);
                        for (var j=0; j<childrenIndexes.length; ++j)
                            select(childrenIndexes[j], ItemSelectionModel.Select);
                    }
                    for (var i=0; i<deselected.length; ++i) {
                        var bone = model.data(deselected[i].topLeft);
                        if (entityTreeModel.bones.indexOf(bone)!==-1)
                            sofaScene.sofaPythonInteractor.call("fixedBonesInteractor", "setBoneFixed", bone, false);
                        // deselect children if any
                        var childrenIndexes = model.getChildrenIndexes(deselected[i].topLeft, false);
                        for (var j=0; j<childrenIndexes.length; ++j)
                            select(childrenIndexes[j], ItemSelectionModel.Deselect);
                    }
                }
            }

            itemDelegate: Label {
                font.bold: searchTree.matchIndex(styleData.index)
                text: styleData.value
            }

            TableViewColumn {
                    role: "Name"
                    title: "Bone"
            }
        }
        RowLayout {
            MouseInteractorToolButton {
                action: cameraInteractor
            }
            MouseInteractorToolButton {
                action: interactor
            }
        }
    }
}
