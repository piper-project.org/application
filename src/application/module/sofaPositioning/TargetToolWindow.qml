// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

import piper.QTargetLoader 1.0
import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Target"

    QTargetLoader {
        id: targetLoader
        property int nbTarget
        onStarted: { nbTarget=0; }
        onFixedBone: {
            var success = fixedBoneToolWindow.setBoneFixed(boneName);
            if (success) {
                context.logDebug("Fixed bone target added: "+boneName);
                ++nbTarget;
            }
            else
                context.logWarning("Fixed bone target failed: "+boneName);
        }
        onNewLandmarkTarget: {
            var success = landmarkToolWindow.addController(landmarkName, mask, value);
            if (success) {
                context.logDebug("Landmark target added: "+targetName);
                ++nbTarget;
            }
            else
                context.logWarning("Landmark target failed: "+targetName);
        }
        onNewJointTarget : {
            var success = jointToolWindow.addController(jointName, value);
            if (success) {
                context.logDebug("Joint target added: "+targetName);
                ++nbTarget;
            }
            else
                context.logWarning("Joint target failed: "+targetName);
        }
        onNewFrameToFrameTarget: {
            var controllerGui = frameToolWindow.addController(frameSource, frameTarget, isRelative, mask, value);
            if (null !== controllerGui) {
                context.logDebug("Frame target added: "+targetName);
                ++nbTarget;
            }
            else
                context.logWarning("Frame target failed: "+targetName);
        }
        onFinished: { context.logInfo("Loaded "+nbTarget+" targets"); }

    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }

    ColumnLayout {
        GroupBox {
            Layout.fillWidth: true
            title: "Create"
            GridLayout {
                width: parent.width
                columns: 2
                ExclusiveGroup {
                    id: targetTypeSelection
                }
                RadioButton {
                    id: targetUserDefined
                    text: "User defined targets"
                    checked: true
                    exclusiveGroup: targetTypeSelection
                }
                RadioButton {
                    id: targetAllLandmarks
                    text: "All Landmarks"
                    checked: false
                    exclusiveGroup: targetTypeSelection
                }
                RadioButton {
                    id: targetAllBones
                    text: "All bones position"
                    checked: false
                    exclusiveGroup: targetTypeSelection
                }

                //                                RadioButton {
                //                                    id: targetJointsAll
                //                                    text: "All joints"
                //                                    checked: false
                //                                }
                Button {
                    Layout.alignment: Qt.AlignRight
                    text: "Create"
                    tooltip: "Create target list over the current one"
                    onClicked: {
                        if (targetUserDefined.checked)
                            sofaScene.sofaPythonInteractor.call("targetInteractor", "createUserTargets")
                        else if (targetAllBones.checked)
                            sofaScene.sofaPythonInteractor.call("targetInteractor", "createAllBoneTargets")
                        else if (targetAllLandmarks.checked)
                            sofaScene.sofaPythonInteractor.call("targetInteractor", "createAllLandmarkTargets")
                        context.targetChanged();
                    }
                }
            }

        }
        TargetLoad {
            Layout.fillWidth: true
            targetLoader: targetLoader
        }
    }
}

