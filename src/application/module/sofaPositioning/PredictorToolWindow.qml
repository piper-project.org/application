// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import Piper 1.0
import piper.predictors 1.0
import piper.proxy 1.0

import "dof.js" as DofConstant

ModuleToolWindow {
    id: root

    title: "Predictors"

    property FrameToolWindow frameToolWindow: null // used to control the pelvis orientation
    property DofController pelvicOrientationController: null // the pelvis DofController

    function init() {
        stiffness.setValue(moduleParameter.PhysPosiInter_spineTargetDefaultStiffness);
    }

    function addPelvicOrientationController() {
        pelvicOrientationController = frameToolWindow.addController("W_Origin", "Pelvic_frame", false, [0,0,0,1,1,1]);
        if (null === pelvicOrientationController) {
            context.logWarning("Pelvic orientation controller could not be added, turning off alignment on gravity. Are Pelvic_frame required landmarks defined ?");
            alignOnGravity.checked = false;
            return;
        }
        context.logInfo("Pelvic orientation controller added");
        if (!project.isGravityDefined())
            context.logWarning("No gravity defined in the model, gravity taken as world Z vector");
    }

    function removePelvicOrientationController() {
        if (null !== pelvicOrientationController) {
            pelvicOrientationController.remove();
            pelvicOrientationController = null;
            context.logInfo("Pelvic orientation controller removed");
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }

    ColumnLayout {
        width: parent.width
        height: parent.height

        GroupBox {
            title: "Spine"
            checkable: true
            checked: false

            SpinePredictor {
                id: spinePredictor
            }

            onCheckedChanged: {
                if (null === frameToolWindow) return;
                if (checked) {
                    sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "addController");
                    if (alignOnGravity.checked)
                        addPelvicOrientationController();
                }
                else {
                    sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "removeController");
                    removePelvicOrientationController();
                }
            }

            GridLayout {
                columns: 2
                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Posture <i>A</i>:"
                    textFormat: Text.StyledText
                }
                ComboBox {
                    id: sourcePosture
                    model: spinePredictor.postureNames()
                    currentIndex: 5

                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Posture <i>B</i>:"
                    textFormat: Text.StyledText
                }
                ComboBox {
                    id: targetPosture
                    model: spinePredictor.postureNames()
                    currentIndex: 4
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Target posture:"
                }
                RowLayout {
                    Slider {
                        id: postureValue
                        minimumValue: -0.2
                        maximumValue: 1.2
                        stepSize: 0.1
                        value: 0.5
                    }
                    Label {
                        text: postureValue.value.toFixed(1)
                    }
                    ToolTip {
                        text: "alpha value in [-0.2,1.2] to select a target posture in [<i>A</i>, <i>B</i>] "
                    }
                }

                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Lateral flexion:"
                }
                RowLayout {
                    Label {
                        Layout.alignment: Qt.AlignRight
                        text:"thoraco-lumbar (°):"
                    }
                    TextField {
                        id: latFlexTL
                        validator: DoubleValidator {bottom: -32; top: 32; notation:DoubleValidator.StandardNotation}
                        textColor: acceptableInput ? "black" : "red"
                        text: "0"
                        ToolTip {
                            text: "thoraco-lumbar lateral flexion in [-32, 32]"
                        }
                    }
                    Label {
                        Layout.alignment: Qt.AlignRight
                        text:"cervical (°):"
                    }
                    TextField {
                        id: latFlexC
                        validator: DoubleValidator {bottom: -8; top: 8; notation:DoubleValidator.StandardNotation}
                        textColor: acceptableInput ? "black" : "red"
                        text: "0"
                        ToolTip {
                            text: "cervical lateral flexion in [-8, 8]"
                        }
                    }
                }

                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Align on gravity:"
                }
                RowLayout {
                    MetadataQmlProxy {
                        id: metadata
                    }
                    Component {
                        id: gravityVector
                        TextField {
                            property var defaultGravity : [0,0,1]
                            implicitWidth: 30
                            readOnly: false
                            validator: DoubleValidator {bottom: -1e6; top: 1e6; notation:DoubleValidator.StandardNotation}
                            textColor: acceptableInput ? "black" : "red"
                            text: (3 === metadata.gravity.length) ? metadata.gravity[index] : defaultGravity[index]
                            onTextChanged: {
                                if (acceptableInput) {
                                    var g = (3 === metadata.gravity.length) ? metadata.gravity : defaultGravity;
                                    g[index] = parseFloat(text);
                                    metadata.gravity = g;
                                }
                            }
                            ToolTip {
                                text: "g_" + DofConstant.dofLabel[index]
                            }
                        }
                    }
                    Switch {
                        id: alignOnGravity
                        checked: true
                        onCheckedChanged: {
                            if (checked)
                                addPelvicOrientationController();
                            else
                                removePelvicOrientationController();
                        }
                        ToolTip {
                            text: "Add a frame controller to adapt the orientation of the pelvis to the target posture"
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    RowLayout {
                        spacing : 2
                        Label {
                            text: "g="
                            ToolTip {
                                text: "Gravity vector direction (norm does not matter)"
                            }
                        }
                        Loader {
                            property int index: 0
                            sourceComponent: gravityVector
                        }
                        Loader {
                            property int index: 1
                            sourceComponent: gravityVector
                        }
                        Loader {
                            property int index: 2
                            sourceComponent: gravityVector
                        }
                    }
                }
                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Stiffness:"
                }
                RowLayout {
                    StiffnessInput {
                        id: stiffness
                        onValueChanged: {
                            if (sofaScene.ready)
                                sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "setCompliance", 1./value);
                        }
                        ToolTip {
                            text: "Spine predictor target stiffness"
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    RowLayout {
                        id: distanceItem
                        function updateDistance() {
                            var distance = sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "getSpineDistanceToTarget");
                            cumDistanceItem.text = distance[0].toFixed(DofConstant.dofNbDecimals[context.modelLengthUnit]-1)
                            maxDistanceItem.text = distance[1].toFixed(DofConstant.dofNbDecimals[context.modelLengthUnit]-1)
                        }
                        Connections {
                            target: sofaScene
                            onStepEnd : distanceItem.updateDistance()
                            onReseted : distanceItem.updateDistance()
                        }

                        Label {
                            text: "error ("+context.modelLengthUnit+"):"
                        }
                        TextField {
                            id: cumDistanceItem
                            implicitWidth: 60
                            readOnly: true
                            text: "0"
                            ToolTip {
                                text: "Vertebrae cumulated distance to the target spline"
                            }
                        }
                        TextField {
                            id: maxDistanceItem
                            implicitWidth: 60
                            readOnly: true
                            text: "0"
                            ToolTip {
                                text: "Vertebrae maximum distance to the target spline"
                            }
                        }
                    }
                }

                Label {
                    Layout.alignment: Qt.AlignRight
                    text:"Target display:"
                }
                RowLayout {
                    ExclusiveGroup { id: targetDisplayType }
                    RadioButton {
                        text: "Absolute spline"
                        checked: true
                        exclusiveGroup: targetDisplayType
                        onCheckedChanged: {
                            if (checked)
                                sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "setShowTarget", "absolute");
                        }
                        ToolTip {
                            text: "Absolute target spline as computed by the predictor"
                        }
                    }
                    RadioButton {
                        text: "Simulation spline"
                        exclusiveGroup: targetDisplayType
                        onCheckedChanged: {
                            if (checked)
                                sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "setShowTarget", "simulation");
                        }
                        ToolTip {
                            text: "Simulation target spline attached to the pelvis as used by the simulation"
                        }
                    }
                }
                Item {
                    Layout.columnSpan: 2
                    width: 10
                    height: 10
                }

                RowLayout {
                    Layout.columnSpan: 2

                    ConfidenceLabel {
                        id: confidenceLabel
                        highConfidenceText: "High confidence prediction"
                        mediumConfidenceText: "Medium confidence prediction"
                        lowConfidenceText: "Low confidence prediction"
                        Layout.alignment: Qt.AlignLeft
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Button {
                        Layout.alignment: Qt.AlignRight
                        text: "Update"
                        tooltip: "Update the target spine posture"
                        enabled: sourcePosture.currentIndex !== targetPosture.currentIndex && latFlexC.acceptableInput && latFlexTL.acceptableInput
                        onClicked: {
                            var inputCSFilePath = context.tempDirectoryPath("PhysPosiInter")+"/input_CS.dat";
                            var inputGravityFilePath = context.tempDirectoryPath("PhysPosiInter")+"/input_gravity.dat";
                            if (sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "generateCSFile", inputCSFilePath)
                                    && sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "generateGravityFile", inputGravityFilePath) ) {
                                // context.octaveProcessPtr(),
                                spinePredictor.startCompute(inputCSFilePath, inputGravityFilePath, sourcePosture.currentIndex, targetPosture.currentIndex, postureValue.value, parseFloat(latFlexTL.text), parseFloat(latFlexC.text), context.tempDirectoryPath("PhysPosiInter")+"/", alignOnGravity.checked);
                            }
                        }
                        Connections {
                            target: spinePredictor
                            onComputeFinished: {
                                sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "setPolyline",
                                                                    context.tempDirectoryPath("PhysPosiInter")+"/CS.dat",
                                                                    context.tempDirectoryPath("PhysPosiInter")+"/splinePoints.dat");
                                distanceItem.updateDistance();
                                confidenceLabel.confidence = sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "getConfidenceValue",
                                                                                                 context.tempDirectoryPath("PhysPosiInter")+"/sp_reliability.dat");
                                if (null !== pelvicOrientationController) {
                                    var pelvisRotVec = sofaScene.sofaPythonInteractor.call("spinePredictorQmlInteractor", "getPelvisOrientation", context.tempDirectoryPath("PhysPosiInter")+"/CS.dat");
                                    for (var i=0; i<3; ++i)
                                        pelvicOrientationController.setValue(i+3, pelvisRotVec[i]);
                                    context.logInfo("Pelvic orientation target updated");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
