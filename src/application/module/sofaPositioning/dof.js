// TODO move this to PiperSofa

var dofLabel = ["x","y","z","rx","ry","rz"];
var dofLabelColor = ["red","green","blue","red","green","blue"];
var dofNbDecimals = {"mm": 1, "cm": 2, "dm": 3, "m": 4}
var lengthUnits = {"mm": 1000, "cm": 100, "dm": 10, "m": 1}
var itemWidth = 80

function isAngularDof(index) {
    return index>2;
}
