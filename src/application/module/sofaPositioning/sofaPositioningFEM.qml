// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2

import SofaBasics 1.0
import SofaTools 1.0
import SofaWidgets 1.0
import Scene 1.0
import SofaApplication 1.0

import piper.QTargetLoader 1.0

Item {

    id: sofaPositioningFEM

    property var viewers: SofaApplication.viewers // TODO: Move this somewhere else ?

    Component.onCompleted: {
        var sceneFile = "file:scenePositioningFEM.py"
        if (Qt.resolvedUrl(sofaScene.source) === sceneFile)
            sofaScene.reload()
        else
            sofaScene.source = sceneFile;
        visible = true;
    }
    Component.onDestruction: {
        sofaScene.source = "file:empty.scn";
    }
    Action {
        id: loadTargetListAction
        text: "Load target list"
        onTriggered: {
            targetLoader.nbLoadedTargets=0;
            targetLoader.process("Default") // TODO: selection of a target list by name
        }
    }
    QTargetLoader {
        id: targetLoader
        property int nbLoadedTargets: 0 // TODO: move this to the c++ class
        onFixedBone: {
            console.log("targetLoader: onFixedBone", boneName);
            sofaScene.pythonInteractor.call("fixedBonesInteractor", "setBoneFixed", boneName, true);
            nbLoadedTargets+=1;
        }
        onNewJointTarget : {
            console.log("targetLoader: onNewJointTarget: "+jointName);
            var controllerCreated = sofaScene.pythonInteractor.call("jointControllerInteractor", "addJointController", jointName);
            nbLoadedTargets+=1;
            //                                        if (controllerCreated)
            //                                            jointController.addController(jointName);
        }
        onJointTargetValue : {
//            console.log("targetLoader: onJointTargetValue: "+jointName+" "+dofIndex+" "+value);
            sofaScene.pythonInteractor.call("jointControllerInteractor", "setJointTarget", jointName, dofIndex, value);
            //                                        jointController.setTargetValue(jointName, dofIndex, value);
        }
        onNewFrameToFrameTarget : {
            console.log("targetLoader: onNewFrameToFrameTarget: "+targetName+" "+frameSource+" "+frameTarget);
            sofaScene.pythonInteractor.call("frameControllerInteractor", "addController", targetName, frameSource, frameTarget, mask);
            nbLoadedTargets+=1;
        }
        onFrameToFrameTargetValue : {
            sofaScene.pythonInteractor.call("frameControllerInteractor", "setTarget", targetName, dofIndex, value);
        }

        onFinished : {
            context.logInfo("Loaded "+nbLoadedTargets+" targets");
        }
    }

    Connections {
        target: sofaScene
        onStatusChanged: {
            if (sofaScene.status === Scene.Ready) {
//                var boneIds = sofaScene.pythonInteractor.call("fixedBonesInteractor", "getBoneIds");
//                fixedBonesListModel.clear();
//                for (var i=0; i<boneIds.length; i++)
//                    fixedBonesListModel.append({"id":boneIds[i]});
//                fixedBonesTableView.resizeColumnsToContents();
                // hide all nodes at startup
                sofaScene.setDataValue("@/piper/image/merge/label/dof/FEM/allNodes.activated", false);
                updateModelButton.enabled = false;
                if (physPosiDeformAutoLoadTarget) {
                    loadTargetListAction.trigger();
                    physPosiDeformAutoLoadTarget=false;
                }
            }
        }
    }

    // TODO: Move this somewhere else ?
//    Connections {
//        target: sofaScene
//        onStepEnd: {
//            for (var i = 0; i < viewers.length; i++) {
//                if(viewers[i].saveVideo) {
//                    var folder = viewers[i].folderToSaveVideo;
//                    viewers[i].saveVideoInFile(folder,i);
//                }
//            }
//        }
//    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        DynamicSplitView {
            id: dynamicSplitView
            Layout.minimumWidth: 400
            Layout.fillWidth: true
            Layout.fillHeight: true
            uiId: 2000
            sourceComponent: Component {
                DynamicContent {
                    defaultContentName: "Viewer"
                    sourceDir: "qrc:/SofaWidgets"
                    properties: {"scene": sofaScene, "culling": true, "defaultCameraOrthographic": true }
                }
            }
        }

        Rectangle {
            clip: true
            color: "lightgrey"
            Layout.minimumWidth: 300
            Layout.fillHeight: true
            ColumnLayout {
                width: parent.width
                CollapsibleGroupBox {
                    Layout.fillWidth: true
                    title: "Control"
                    ColumnLayout {
                        width: parent.width
                        RowLayout {
                            width: parent.width
                            Button {
                                id: positioningButton
                                text: "Positioning"
                                tooltip: "Start/Stop the positioning process"
                                checkable: true
                                onCheckedChanged: {
                                    if(sofaScene) {
                                        sofaScene.play = positioningButton.checked;
                                        fpsDisplay.enabled = positioningButton.checked;
                                    }
                                }

                                Connections {
                                    target: sofaScene
                                    ignoreUnknownSignals: true
                                    onPlayChanged: {
                                        positioningButton.checked = sofaScene.play;
                                    }
                                }
                            }
                            Button {
                                id: resetButton
                                text: "Reset"
                                tooltip: "Reset the positioning"
                                onClicked: {
                                    if(sofaScene)
                                        sofaScene.reset();
                                }
                            }
                            FPSDisplay {
                                id: fpsDisplay
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignRight
                                enabled: false
                                horizontalAlignment: Text.AlignHRight
                                verticalAlignment: Text.AlignVCenter
                            }
                        }
                        RowLayout {
                            CheckBox {
                                text: "Compute all nodes"
                                checked: false
                                onClicked: {
                                    sofaScene.setDataValue("@/piper/image/merge/label/dof/FEM/allNodes.activated", checked);
//                                    sofaScene.setDataValue("@/piper/dofAffine/dof/allNodes.activated", checked); // TODO get the path from scene.nodes["dof"]
                                    if (!sofaScene.playing)
                                        sofaScene.step(); // needed to update nodes positions
                                    updateModelButton.enabled = checked;
                                }
                            }
                            Button {
                                id: updateModelButton
                                text: "Update Model"
                                enabled: true
                                onClicked: {
                                    sofaScene.sendGUIEvent("","exportFullModel","");
                                    sofaScene.sendGUIEvent("","exportEntities","");
                                    context.logDone("Model updated");
                                }
                            }
                        }
                    }
                }

//                CollapsibleGroupBox {
//                    Layout.fillWidth: true
//                    title: "Fixed bones"
//                    ColumnLayout {
//                        width: parent.width
//                        RowLayout {
//                            Button {
//                                text: "All"
//                                tooltip: "Select all bones"
//                                onClicked: {
//                                    fixedBonesTableView.selection.selectAll();
//                                }
//                            }
//                            Button {
//                                text: "None"
//                                tooltip: "Deselect all bones"
//                                onClicked: {
//                                    fixedBonesTableView.selection.clear();
//                                }
//                            }
//                        }
//                        TableView {
//                            id: fixedBonesTableView
//                            Layout.fillWidth: true
//                            Layout.minimumHeight: 150
//                            selectionMode: SelectionMode.MultiSelection
//                            headerVisible: false
//                            model: ListModel {
//                                id: fixedBonesListModel
//                            }
//                            TableViewColumn {
//                                title: "Bone"
//                                role: "id"
//                                width: fixedBonesTableView.viewport.width
//                                resizable: false
//                            }
//                            Connections {
//                                target: fixedBonesTableView.selection
//                                onSelectionChanged: {
//                                    for (var i=0; i<fixedBonesListModel.count; i++) {
////                                        console.log(i+" "+fixedBonesListModel.get(i)["id"]+" "+fixedBonesTableView.selection.contains(i))
//                                        sofaScene.pythonInteractor.call("fixedBonesInteractor", "setBoneFixed", fixedBonesListModel.get(i)["id"], fixedBonesTableView.selection.contains(i));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                JointController {
//                    id: jointController
//                    width: parent.width
//                }
//                Connections {
//                    target: sofaScene
//                    onStatusChanged: {
//                        if (sofaScene.status === Scene.Ready) {
//                            jointController.init();
//                        }
//                    }
//                }
//                FrameController {
//                    id: frameController
//                    width: parent.width
//                }
//                Connections {
//                    target: sofaScene
//                    onStatusChanged: {
//                        if (sofaScene.status === Scene.Ready) {
//                            frameController.init();
//                        }
//                    }
//                }
                CollapsibleGroupBox {
                    Layout.fillWidth: true
                    title: "Visualisation"
                    ColumnLayout {
                        width: parent.width
//                        GroupBox {
//                            Layout.fillWidth: true
//                            title: "Entity"
//                            GridLayout {
//                                columns: 2
//                                CheckBox {
//                                    text: "Bones"
//                                    checked: true
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowBones", checked)
//                                }
//                                CheckBox {
//                                    text: "Skin"
//                                    checked: true
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowSkin", checked)
//                                }
//                                CheckBox {
//                                    text: "Environment"
//                                    checked: true
//                                    onClicked: sofaScene.setDataValue("@/environment.activated", checked);
//                                }
//                            }
//                        }
//                        GroupBox {
//                            Layout.fillWidth: true
//                            title: "Model"
//                            GridLayout {
//                                columns: 2
//                                CheckBox {
//                                    text: "Anatomical frames"
//                                    checked: false
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowAnatomicalFrames", checked)
//                                }
//                                CheckBox {
//                                    text: "Joint frames"
//                                    checked: false
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowJointsOffset", checked)
//                                }
//                                CheckBox {
//                                    text: "World frames"
//                                    checked: false
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowWorldFrames", checked)
//                                }
//                            }
//                        }
                        GroupBox {
                            Layout.fillWidth: true
                            title: "Simulation"
                            GridLayout {
                                columns: 2
//                                CheckBox {
//                                    text: "Bone frames"
//                                    checked: false
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowBonesRigids", checked)
//                                }
                                CheckBox {
                                    text: "Embedding hexahedra"
                                    checked: true
                                    onClicked: sofaScene.setDataValue("@/piper/image/merge/label/dof/FEM/VisuHexa.activated", checked);
                                }
//                                CheckBox {
//                                    text: "Sliding contacts"
//                                    checked: false
//                                    onClicked: sofaScene.pythonInteractor.call("displayInteractor", "setShowSlidingContact", checked)
//                                }
                            }
                        }
                    }
                }
                CollapsibleGroupBox {
                    id: targetLoadSave
                    implicitWidth: 0
                    Layout.fillWidth: true

                    title: "Target"
                    RowLayout {
                        Button {
                            action: loadTargetListAction
                        }
                    }
                }
            }
        }
    }
}
