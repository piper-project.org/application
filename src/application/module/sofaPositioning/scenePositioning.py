# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import os.path
import math

import numpy as np
import numpy.linalg as la

import Sofa
import SofaPython.units
import SofaPython.Tools
import SofaPython.Quaternion as Quat
import SofaPiper.sml
import SofaPiper.tools
import Anatomy.sml.rigidAffine
import Anatomy.Constraint
import Flexible.API
import Compliant.StructuralAPI
import Compliant.sml
import ContactMapping.StructuralAPI
import ContactMapping.sml

import piper.app
import piper.hbm
import piper.sofa

import sceneCommon

Compliant.StructuralAPI.geometric_stiffness = 2
Compliant.sml.printLog = False
ContactMapping.sml.printLog = False
Anatomy.sml.rigidAffine.printLog = False


def createSceneAndController(rootNode):
    piper.app.logDebug("Loading simulation...")

    tmpDir = piper.app.tempDirectoryPath(sceneCommon.currentModule)
    if not os.path.exists(tmpDir):
        print "[sofaPositionning] ERROR: run prepare first"
        return

    rootNode.createObject("RequiredPlugin", name="Anatomy")
    rootNode.createObject("RequiredPlugin", name="ContactMapping")
    rootNode.createObject("RequiredPlugin", name="SofaPiper")
    rootNode.createObject("VisualStyle", displayFlags="showVisualModels hideCollisionModels showBehaviorModels")
    rootNode.createObject('ClipPlane', name="clipPlane", normal="1 0 0", position="0 0 0", active=False)

    sceneCommon.scene = Anatomy.sml.rigidAffine.SceneSimu(rootNode, sceneCommon.model, tmpDir)
    param = sceneCommon.scene.param # shortcut

    param.softTissueTags={"skin"}
    param.mappedObjectTags=set()
    if piper.sofa.project().moduleParameter().getBool(sceneCommon.currentModule, "capsuleEnabled"):
        param.softTissueTags.update({"ligament","capsule"})
    else:
        param.mappedObjectTags.update({"ligament","capsule"})

    param.jointIsComplianceByTag["default"] = True
    param.jointComplianceByTag["default"] = 1e-10
    param.jointIsComplianceByTag["soft"]=False
    param.jointComplianceByTag["soft"] = 1./piper.sofa.project().moduleParameter().getDouble("PhysPosi","softJointStiffness")

    param.parallel = False
    param.contactCompliance = 1e-4 # s^2/kg
    param.slidingContactRejectBoundary = True

    if True:
        param.loadShapeFunction = True
    else:
        param.loadShapeFunction = False
    param.showRigid=False
    param.showRigidScale=0.05
    param.showOffset=False
    param.showOffsetScale=0.04
    param.showAffine=False
    param.showAffineScale=0.02
    param.showGP=False
    param.showGPScale=0.005
    param.showLandmarkColor = [0.7,0.7,0.,1.]
    param.showLandmarkScale = 0.01

    param.color["skin"]=[1.,0.75,0.75,0.3]

    param.collisionCompliance = 1e-10
    param.collisionOffset = piper.sofa.project().moduleParameter().getDouble("PhysPosi", "boneCollisionOffset")
    param.collisionNbClosestVertex = piper.sofa.project().moduleParameter().getUInt("PhysPosi", "boneCollisionNbVertex")

    sceneCommon.scene.createScene()

    global simulationController, jointController, landmarkController, frameController
    simulationController = rootNode.createObject('PythonScriptController', name="simulationControlInteractor", filename="interactors.py",classname="SimulationControlInteractor").instance()
    rootNode.createObject('PythonScriptController', name="displayController", filename="interactors.py", classname="DisplayQmlInteractor")
    rootNode.createObject('PythonScriptController', name="targetInteractor", filename=__file__, classname="TargetQmlInteractor")

    # user target controllers
    rootNode.createObject('PythonScriptController', name="fixedBonesInteractor", filename="interactors.py", classname="FixedBonesQmlInteractor")
    # make those controllers available globally in the python interpreter, usefull for scripting
    jointController = rootNode.createObject('PythonScriptController', name="jointControllerInteractor", filename="interactors.py", classname="JointControllersQmlInteractor").instance()
    landmarkController = rootNode.createObject('PythonScriptController', name="landmarkControllerInteractor", filename="interactors.py", classname="LandmarkControllersQmlInteractor").instance()
    frameController = rootNode.createObject('PythonScriptController', name="frameControllerInteractor", filename="interactors.py", classname="FrameControllersQmlInteractor").instance()
    rootNode.createObject('PythonScriptController', name="spineControllerInteractor", filename="interactors.py", classname="SpineControllerQmlInteractor")

    # predictors
    rootNode.createObject('PythonScriptController', name="spinePredictorQmlInteractor", filename="interactors.py", classname="SpinePredictorQmlInteractor")

#    rootNode.createObject("OrderIndependentTransparencyManager") # TODO add this component only above some openGL version

#    rootNode.createObject('CompliantImplicitSolver', name='odesolver', stabilization="pre-stabilization", neglecting_compliance_forces_in_geometric_stiffness=False)
    rootNode.createObject('CompliantPseudoStaticSolver', name='odesolver', iterations=1, stabilization="pre-stabilization", neglecting_compliance_forces_in_geometric_stiffness=False)
#    rootNode.createObject('LDLTSolver', name='numsolver', regularization=1e-16)
#    rootNode.createObject('MinresSolver', name='numsolver', iterations=250, precision=1e-14)
    rootNode.createObject('SequentialSolver', name='numsolver', iterations=250, precision=1e-14, iterateOnBilaterals=True)
    rootNode.createObject('LDLTResponse', name='response')
    rootNode.gravity= [0, 0, 0]

    # display of sliding contacts via DistanceToMeshMultiMapping
    for slidingContact in sceneCommon.scene.slidingContacts.values():
        slidingContact.node.createObject('VisualStyle', name="visualStyle", displayFlags="hideMapping")

#    if not sceneCommon.scene.nodes["dofRigid"] is None:
#        sceneCommon.scene.nodes["dofRigid"].createObject('UniformVelocityDampingForceField', template="Rigid3", name="damping", dampingCoefficient=SofaPython.units.damping_from_SI(1))
#    if not sceneCommon.scene.nodes["dofAffine"] is None:
#        sceneCommon.scene.nodes["dofAffine"].createObject('UniformVelocityDampingForceField', template="Affine", name="damping", dampingCoefficient=SofaPython.units.damping_from_SI(1))

    if sceneCommon.currentModule=="PhysPosiDefo" or piper.sofa.project().moduleParameter().getBool("PhysPosiInter","modelUpdateEnabled") :
        skinLabels = sceneCommon.scene.entitiesProp.getSolidLabelsByTag(sceneCommon.model, "skin")
        # all model nodes, to be exported
        allNodesNode = sceneCommon.scene.nodes["dof"].createChild("allNodes")
        sceneCommon.allNodesList.append(allNodesNode)
        # export all nodes mapped on flesh
        modelNode = allNodesNode.createChild("model_allNodes")
        modelNode.createObject("PiperMeshLoader", name="loader", loadAllNodes=True)
        allNodesMO = modelNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=False)
        modelNode.createObject("BranchingCellOffsetsFromPositions", template="BranchingImageUC", name="cell", position ="@allNodes.position", src=sceneCommon.scene.labelImage.getImagePath(), labels=SofaPython.Tools.listToStr(skinLabels))
        Flexible.API.insertLinearMapping(modelNode,
            dofRigidNode=sceneCommon.scene.nodes["dofRigid"], dofAffineNode=sceneCommon.scene.nodes["dofAffine"], cell="@cell.cell", isMechanical=False ) # position=allNodesMO,
        modelNode.createObject("PiperExporter", template="Vec3", name="exporter_model", guiEventName="exportFullModel", positions="@allNodes.position", positionsId="@loader.positionId")

        # export entities nodes
        # TODO: avoid computing twice the nodes coordinates (once in model_allNodes and then in each entity)
        for solid in sceneCommon.model.getSolidsByTags(param.softTissueTags):
            # piper entities might be grouped in a single sml solid, each piper entity is a sml mesh in that case
            for mesh in solid.mesh:
                piperEntityName = mesh.name
                entityNode = allNodesNode.createChild(piperEntityName+"_allNodes")
                entityNode.createObject('PiperMeshLoader', name="loader", entityNames=piperEntityName)
                allNodesMO = entityNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=True)
                entityNode.createObject("BranchingCellOffsetsFromPositions", template="BranchingImageUC", name="cell", position ="@allNodes.position",
                    src=sceneCommon.scene.labelImage.getImagePath(),
                    labels=SofaPython.Tools.listToStr(sceneCommon.scene.entitiesProp.entities[solid.id].allLabels()) )
                Flexible.API.insertLinearMapping(entityNode,
                    dofRigidNode=sceneCommon.scene.nodes["dofRigid"], dofAffineNode=sceneCommon.scene.nodes["dofAffine"], cell="@cell.cell", isMechanical=False ) # position=allNodesMO,
                entityNode.createObject("PiperExporter", template="Vec3", name="exporter_"+solid.id, guiEventName="exportEntities", positions="@allNodes.position", positionsId="@loader.positionId")
        for solid in sceneCommon.model.getSolidsByTags({"bone"}):
            # piper bone and cartilage entities might be grouped in a single sml solid, each piper entity is a sml mesh in that case
            rigidNode = sceneCommon.scene.rigids[solid.id].node
            for mesh in solid.mesh:
                piperEntityName = mesh.name
                entityNode = rigidNode.createChild(piperEntityName+"_allNodes")
                sceneCommon.allNodesList.append(entityNode)
                entityNode.createObject("PiperMeshLoader", name="loader", entityNames=piperEntityName)
                entityNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=True)
                entityNode.createObject("RigidMapping", name="mapping", globalToLocalCoords=True, mapForces=False, mapConstraints=False, mapMasses=False)
                entityNode.createObject("PiperExporter", template="Vec3", name="exporter_"+mesh.name, guiEventName="exportEntities", positions="@allNodes.position", positionsId="@loader.positionId")

    sceneCommon.addVisualStyle()
    sceneCommon.setBoneVisualTag()
    sceneCommon.addAnatomicalFrames()
    sceneCommon.addLandmarks()
    sceneCommon.addControllableFrameBones()
    sceneCommon.addControllableFrameWorld()
    sceneCommon.addSpineReferencePoints()
    sceneCommon.setupCollisions()
    if piper.sofa.project().moduleParameter().getBool("PhysPosi","tibiaPatellaConstraintEnabled") :
        addTibialTuberosityPatellaDistanceConstraint()

    return rootNode

def bwdInitGraph(node):
    piper.app.logInfo("Size of the system - rigids: {0} | affines: {1}".format(
        0 if sceneCommon.scene.nodes["dofRigid"] is None else len(sceneCommon.scene.nodes["dofRigid"].getObject("dofs").position),
        0 if sceneCommon.scene.nodes["dofAffine"] is None else len(sceneCommon.scene.nodes["dofAffine"].getObject("dofs").position),
    ))
    piper.app.logDone("")
    return node

def addTibialTuberosityPatellaDistanceConstraint():
    for side in ["Left", "Right"]:
        tuberosityName = side+"_tibial_tuberosity"
        patellaName = side+"_patella"
        if tuberosityName in sceneCommon.controllableLandmarks and patellaName in sceneCommon.scene.rigids:
            piper.app.logDebug("Add {0} tibial tuberosity / patella distance constraint".format(side.lower()))

            patellaCoGNode = sceneCommon.scene.rigids[patellaName].node.createChild("PatellaCoG") # to have Patella CoG
            patellaCoGNode.createObject("MechanicalObject", template="Vec3", name="dofs")
            patellaCoGNode.createObject("SubsetMultiMapping", template="Rigid3,Vec3", name="mapping", input="@../", output="@./", indexPairs="0 0")

            tuberosityNode = sceneCommon.controllableLandmarks[tuberosityName].node #shortcut
            extremitiesNode = patellaCoGNode.createChild("TibialTuberosityPatellaDistance")
            tuberosityNode.addChild(extremitiesNode)
            extremitiesNode.createObject("MechanicalObject", template="Vec3", name="dofs")
            extremitiesNode.createObject("SubsetMultiMapping", template="Vec3,Vec3", name="mapping", input="@"+patellaCoGNode.getPathName()+" @"+tuberosityNode.getPathName(), output = "@./", indexPairs="0 0 1 0")
            distanceNode = extremitiesNode.createChild("distance")
            distanceNode.createObject("MechanicalObject", template="Vec1", name="dofs")
            distanceNode.createObject("EdgeSetTopologyContainer", name="edge", position="@../dofs.position", edges="0 1")
            distanceNode.createObject("DistanceMapping", name="mapping", applyRestPosition=True)
            distanceNode.createObject("UniformCompliance", name="compliance", isCompliance=False, compliance=1e-10)
        else:
            piper.app.logWarning("Missing {0} tibial tuberosity or patella - skipped distance constraint".format(side.lower()))

class TargetQmlInteractor(Sofa.PythonScriptController) :

    def createUserTargets(self):
        piper.sofa.project().target().clear()
        nbSavedTargets = 0
        # fixed bones
        piper.sofa.project().target().clear()
        for boneName, bone in sceneCommon.scene.rigids.iteritems():
            if not bone.fixedConstraint is None:
                target = piper.hbm.FixedBoneTarget()
                target.setName("fixed_"+boneName)
                target.bone = boneName
                piper.sofa.project().target().fixedBone.append(target)
                nbSavedTargets += 1
        # landmarks
        piper.sofa.project().target().landmark.clear()
        for controllerName,controller in sceneCommon.landmarkControllers.iteritems():
            target = piper.hbm.LandmarkTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName(controllerName)
            target.landmark = controller.landmarkName
            dofValue = dict()
            targetValue = controller.getTarget()
            targetIndex=0
            for dofIndex,dof in enumerate(controller.mask):
                if dof:
                    dofValue[dofIndex]=targetValue[targetIndex]
                    targetIndex+=1
            target.setValue(piper.hbm.RigidTransformationTargetValueCont(dofValue))
            piper.sofa.project().target().landmark.append(target)
            nbSavedTargets += 1
        # joints
        piper.sofa.project().target().joint.clear()
        for jointName,controller in sceneCommon.jointControllers.iteritems():
            target = piper.hbm.JointTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName(jointName)
            target.joint=jointName
            dofValue = dict()
            targetIndex=0
            for dofIndex,dof in enumerate(sceneCommon.scene.joints[jointName].mask):
                if not dof:
                    dofValue[dofIndex]=controller.progressiveTarget.target[targetIndex][0]
                    targetIndex+=1
            target.setValue(piper.hbm.RigidTransformationTargetValueCont(dofValue))
            piper.sofa.project().target().joint.append(target)
            nbSavedTargets += 1
        # frames
        piper.sofa.project().target().frameToFrame.clear()
        for frameToFrameName,controller in sceneCommon.frameControllers.iteritems():
            target = piper.hbm.FrameToFrameTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName(frameToFrameName)
            target.isRelative = controller.isRelative
            target.frameSource = controller.frame1Name
            target.frameTarget = controller.frame2Name
            dofValue = dict()
            targetValue = controller.getTarget()
            targetIndex=0
            for dofIndex,dof in enumerate(controller.mask):
                if dof:
                    dofValue[dofIndex]=targetValue[targetIndex]
                    targetIndex+=1
            target.setValue(piper.hbm.RigidTransformationTargetValueCont(dofValue))
            piper.sofa.project().target().frameToFrame.append(target)
            nbSavedTargets += 1
        piper.app.logInfo("{0} targets created".format(nbSavedTargets))

    def createAllBoneTargets(self):
        piper.sofa.project().target().clear()
        nbSavedTargets = 0
        for name, bone in sceneCommon.scene.rigids.iteritems():
            target = piper.hbm.FrameToFrameTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName("W_Origin_to_B_"+name)
            target.isRelative = False
            target.frameSource = "W_Origin"
            target.frameTarget = "B_"+name
            dofs = list(bone.dofs.position[0][0:3]) + list(Quat.quatToRotVec(Quat.normalized(bone.dofs.position[0][3:])))
            dofValue = dict()
            for index in range(len(dofs)):
                dofValue[index]=dofs[index]
            target.setValue(piper.hbm.RigidTransformationTargetValueCont(dofValue))
            piper.sofa.project().target().add(target)
            nbSavedTargets += 1
        piper.app.logInfo("{0} targets created".format(nbSavedTargets))

    def createAllJointTargets(self):
        piper.sofa.project().target().clear()
        nbSavedTargets = 0
        for name, joint in sceneCommon.scene.joints.iteritems():
            target = piper.hbm.JointTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName(name)
            target.joint=name
            dofValue = dict()
            for index,dof in enumerate(joint.mask):
                if not dof:
                    dofValue[index]=joint.dofs.position[0][index]
            target.setValue(piper.hbm.RigidTransformationTargetValueCont(dofValue))
            piper.sofa.project().target().joint.append(target)
            nbSavedTargets += 1
        piper.app.logInfo("{0} targets created".format(nbSavedTargets))

    def createAllLandmarkTargets(self):
        piper.sofa.project().target().clear()
        nbSavedTargets = 0
        for landmarkName, landmark in sceneCommon.controllableLandmarks.iteritems():
            target = piper.hbm.LandmarkTarget()
            target.setUnit(piper.sofa.project().model().metadata().lengthUnit())
            target.setName(landmarkName)
            target.landmark = landmarkName
            dofValue = dict()
            for i in range(3):
                dofValue[i] = landmark.dofs.position[0][i]
            target.setValue(dofValue)
            piper.sofa.project().target().landmark.append(target)
            nbSavedTargets += 1
        piper.app.logInfo("{0} targets created".format(nbSavedTargets))

    def getAllTargetsCumulatedError(self):
        if len(sceneCommon.jointControllers)>0 or len(sceneCommon.landmarkControllers)>0:
            return "N/A"
        cumErr = 0
        for controller in sceneCommon.frameControllers.values():
            if len(controller.progressiveTarget.target[0])!=7:
                return "N/A"
            cumErr += la.norm( np.asarray(controller.getCurrentPosition()) - np.asarray(controller.getTarget()) )
        return "{0:f}".format(cumErr)

    def setAllTargetsCompliance(self, compliance):
        i=0
        for controller in sceneCommon.landmarkControllers.values():
            controller.compliance.compliance = compliance
            controller.compliance.reinit()
            i+=1
        for controller in sceneCommon.frameControllers.values():
            controller.setCompliance(compliance)
            i+=1
        for controller in sceneCommon.jointControllers.values():
            controller.compliance.compliance = compliance
            controller.compliance.reinit()
            i+=1
        piper.app.logDebug("Updated {0} targets stiffness".format(i))

