// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0

import SofaScene 1.0
import SofaApplication 1.0
//import SofaInteractors 1.0
//import SofaManipulators 1.0

import Piper 1.0

import "dof.js" as DofConstant

ModuleToolWindow {
    id: root

    title: "Frame Controller"

    property var dofControllerComponent: Qt.createComponent("qrc:///module/sofaPositioning/DofController.qml");
    property var frameNames: []
    property var boneNames: []
    property var boneFrameNames: []


    function frameControllerDofId(controllerName,i) {
        var id = "frame_"+controllerName+String(i)+"Input";
        id = id.replace('-','_'); //a qml id cannot contain a '-';
        return id;
    }

    function onTargetChanged(controllerName, index, value) {
        sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "setTarget", controllerName, index, value);
    }

    function updateEnabled() {
        // TODO: prevent from adding twice the same controller

        // enable/disable add button
        if (referenceFrameListModel.count>0
                && frame1Combo.currentIndex>=0 && entity2Combo.currentIndex>=0 && frame2Combo.currentIndex>=0
                && (-1 !== frameNames.indexOf(frame1Combo.editText))
                && (-1 !== boneNames.indexOf(entity2Combo.editText))
                && (-1 !== boneFrameNames.indexOf(frame2Combo.editText))
                && (dofXCheckBox.checked || dofYCheckBox.checked || dofZCheckBox.checked
                  || dofRxCheckBox.checked || dofRyCheckBox.checked || dofRzCheckBox.checked ) )
            addControllerButton.enabled=true;
        else
            addControllerButton.enabled=false;

        //TODO disable if a dof is already controlled
    }

    ListModel {
        id: referenceFrameListModel
        function updateList(showBoneInertiaFrame) {
            referenceFrameListModel.clear();
            frameNames = [];
            if (sofaScene.ready)
                frameNames = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getFrameNames", showBoneInertiaFrame);
            for (var i=0; i < frameNames.length; i++) {
                referenceFrameListModel.append({text: frameNames[i]});
            }
        }
    }

    ListModel {
        id: boneListModel
        function updateList() {
            boneListModel.clear();
            boneNames = []
            if (sofaScene.ready)
                boneNames = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getBoneNames");
            for (var i=0; i < boneNames.length; i++) {
                boneListModel.append({text: boneNames[i]});
            }
        }
    }

    ListModel {
        id: boneFrameListModel
        function updateList(boneName) {
            boneFrameListModel.clear()
            boneFrameNames = [];
            if (sofaScene.ready)
                boneFrameNames = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getBoneFrameNames", boneName);
            boneFrameNames.unshift("Relative");
            for (var i=0; i < boneFrameNames.length; i++) {
                boneFrameListModel.append({text: boneFrameNames[i]});
            }
        }
    }

    function init() {
        referenceFrameListModel.updateList(showBoneInertiaFrame.checked);
        boneListModel.updateList();
//        SofaApplication.addInteractor("Select frame", selectFrameManipulator);
        root.updateEnabled();
    }

    function addController(frameReference, frame, isRelative, mask, initialValue) {
        var controllerName = frameReference+"_to_"+frame;
        for (var k=0; k< mask.length; ++k) {
            controllerName += mask[k]===1?"_"+DofConstant.dofLabel[k]:"";
        }

        var controllerCreated = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "addController", controllerName, frameReference, frame, isRelative, mask);
        if (!controllerCreated)
            return null;

        var _initialValue = initialValue;
        if (typeof _initialValue === 'undefined')
            _initialValue = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getTarget", controllerName);

        var controllerGui = dofControllerComponent.createObject(frameControllerParent, {
                                                                    "controllerName": controllerName,
                                                                    "sofaPythonInteractorName": "frameControllerInteractor",
                                                                    "mask": mask,
                                                                    "initialValue": _initialValue,
                                                                    "defaultStiffness": moduleParameter.PhysPosiInter_targetDefaultStiffness,
                                                                    "enableShowDofsButton": true
                                                                } );
//        if ("W_Origin"===frameReference && 1===mask[0] && 1===mask[1] && 1===mask[2] && 1===mask[3] && 1===mask[4] && 1===mask[5]) // TODO limited manipulator
//            sofaScene.addManipulator(manipulatorComponent.createObject(root, {objectName:controllerName, sofaScene: sofaScene, controllerName:controllerName, controllerGui:controllerGui, controllerMask:mask}));
        return controllerGui;
    }

//    property Component selectFrameManipulator: UserInteractor_MoveCamera {
//        hoverEnabled: true

//        function init() {
//            moveCamera_init();

//            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
//                var selectable = sofaViewer.pickObjectWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["manipulator"]);
//                if(selectable && selectable.manipulator) {
//                sofaScene.selectedManipulator = selectable.manipulator;
//                    if(sofaScene.selectedManipulator.mousePressed)
//                        sofaScene.selectedManipulator.mousePressed(mouse, sofaViewer);
//                    if(sofaScene.selectedManipulator.mouseMoved)
//                        setMouseMovedMapping(sofaScene.selectedManipulator.mouseMoved);
//                }
//                else {
//                    sofaScene.selectedComponent = null;
//                    sofaScene.selectedManipulator = null;
//                    root.currentSplineCtrlpIndex = -1;
//                }
//            });

//            addMouseReleasedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
//                if(sofaScene.selectedManipulator && sofaScene.selectedManipulator.mouseReleased)
//                    selectedManipulator.mouseReleased(mouse, sofaViewer);
//                sofaScene.selectedManipulator = null;
//                setMouseMovedMapping(null);
//            });
//        }
//    }

//    property Action interactor: Action {
//        checkable: true
//        checked: false
//        enabled: true
//        iconSource: "qrc:/icon/cursor.png"
//        text: "F"
//        tooltip: "Frame interactor"
//        exclusiveGroup: interactorExclusiveGroup
//        onToggled: {
//            if (checked)
//                SofaApplication.interactorComponent = selectFrameManipulator;
//        }
//    }

//    Component {
//        id: manipulatorComponent

//        Manipulator {
//            id: manipulator

//            property var sofaScene
//            property var controllerName
//            property var controllerGui
//            property var controllerMask

//            visible: interactor.checked

//            Manipulator3D_Translation {
//                id: tx
//                visible: manipulator.visible
//                axis: "x"
//                onPositionChanged: manipulator.position = position;
//            }

//            Manipulator3D_Translation {
//                id: ty
//                visible: manipulator.visible
//                axis: "y"
//                onPositionChanged: manipulator.position = position;
//            }

//            Manipulator3D_Translation {
//                id: tz
//                visible: manipulator.visible
//                axis: "z"
//                onPositionChanged: manipulator.position = position;
//            }

////            Manipulator3D_Rotation {
////                id: rx
////                visible: manipulator.visible
////                axis: "x"
////                onOrientationChanged: manipulator.orientation = orientation;
////            }

////            Manipulator3D_Rotation {
////                id: ry
////                visible: manipulator.visible
////                axis: "y"
////                onOrientationChanged: manipulator.orientation = orientation;
////            }

////            Manipulator3D_Rotation {
////                id: rz
////                visible: manipulator.visible
////                axis: "z"
////                onOrientationChanged: manipulator.orientation = orientation;
////            }

//            function getPositionAndOrientation() {
//                var positionArray = sofaScene.sofaPythonInteractor.call("frameControllerInteractor", "getCurrentPosition", controllerName);
//                position = Qt.vector3d(positionArray[0], positionArray[1], positionArray[2]);
//                orientation = SofaApplication.quaternionFromRotationVec(Qt.vector3d(positionArray[3], positionArray[4], positionArray[5]));
//            }

////            function setPosition() {
////                sofaScene.sofaPythonInteractor.call("spineControllerInteractor", "setCtrlpPosition", index, [position.x, position.y, position.z]);
////            }

//            Component.onCompleted: {
//                getPositionAndOrientation();
//            }

//            onVisibleChanged: {
//                if (visible) getPositionAndOrientation();
//            }

////            onPositionChanged: setPosition()

////            Connections {
////                target: manipulator.controllerGui
////                onRemoved: {sofaScene.removeManipulatorByName(manipulator.controllerName);}
////            }
//        }
//    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        width = 500;
        setCurrentWindowSizeMinimum();
    }

    Component.onDestruction: {
        settings.showBoneInertiaFrame = showBoneInertiaFrame.checked;
    }

    Settings {
        id: settings
        category: "frameToolWindows"
        property bool showBoneInertiaFrame: false
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        GroupBox {
            Layout.fillWidth: true
            title: "Add frame to frame controller"
            ColumnLayout {
                width:parent.width

                GridLayout {
                    Layout.fillWidth: true
                    columns: 5
                    // row 1
                    Label {
                        text: "Reference frame"
                    }
                    Label {
                        Layout.column:2
                        text: "Entity"
                    }
                    Label {
                        text: "Entity frame"
                    }
                    // row 2
                    ComboBox {
                        id: frame1Combo
                        Layout.row:1
                        Layout.fillWidth: true
                        editable: true
                        model: referenceFrameListModel
                        onCurrentIndexChanged : root.updateEnabled()
                        onEditTextChanged: root.updateEnabled()
                    }
                    Label {text: "to"}
                    ComboBox {
                        id: entity2Combo
                        Layout.fillWidth: true
                        editable: true
                        model: boneListModel
                        onCurrentIndexChanged : {
                            boneFrameListModel.updateList(currentText);
                            root.updateEnabled();
                        }
                        onEditTextChanged: {
                            boneFrameListModel.updateList(currentText);
                            root.updateEnabled();
                        }
                    }
                    ComboBox {
                        id: frame2Combo
                        Layout.fillWidth: true
                        editable: true
                        model: boneFrameListModel
                        onCurrentIndexChanged : root.updateEnabled()
                        onEditTextChanged: root.updateEnabled()
                        onModelChanged: {
                            currentIndex = 0;
                            if (model.count > 0)
                                editText = model.get(currentIndex).text;
                            else
                                editText = ""
                        }
                        ToolTip {
                            text: "If <em>Relative</em> is selected, transformation from reference to entity is relative in the reference frame"
                        }
                    }
                    Button {
                        id: addControllerButton
                        iconSource: "qrc:///icon/list-add.png"
                        tooltip:"Add frame controller from <em>reference</em> to <em>entity</em>"
                        enabled: false
                        onClicked: {
                            var mask = [];
                            mask = mask.concat([dofXCheckBox.checked?1:0]);
                            mask = mask.concat([dofYCheckBox.checked?1:0]);
                            mask = mask.concat([dofZCheckBox.checked?1:0]);
                            mask = mask.concat([dofRxCheckBox.checked?1:0]);
                            mask = mask.concat([dofRyCheckBox.checked?1:0]);
                            mask = mask.concat([dofRzCheckBox.checked?1:0]);

                            if (frame2Combo.currentIndex == 0)
                                addController(frame1Combo.currentText, "B_"+entity2Combo.currentText, true, mask);
                            else
                                addController(frame1Combo.currentText, frame2Combo.currentText, false, mask);
                        }
                    }

                    CheckBox {
                        id: showBoneInertiaFrame
                        text: "Bone inertia"
                        checked: settings.showBoneInertiaFrame
                        onCheckedChanged: referenceFrameListModel.updateList(checked)
                        ToolTip {
                            text: "Show/Hide bone inertia frames computed by the simulation"
                        }
                    }

                }
                RowLayout {
                    CheckBox {
                        id: dofXCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "x"
                                color: DofConstant.dofLabelColor[0]
                            }
                        }
                    }
                    CheckBox {
                        id: dofYCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "y"
                                color: DofConstant.dofLabelColor[1]
                            }
                        }
                    }
                    CheckBox {
                        id: dofZCheckBox
                        onCheckedChanged: root.updateEnabled()
                        style: CheckBoxStyle {
                            label: Label {
                                text: "z"
                                color: DofConstant.dofLabelColor[2]
                            }
                        }
                    }
                    CheckBox {
                        id: dofRxCheckBox
                        onCheckedChanged: {
                            dofRyCheckBox.checked=checked;
                            dofRzCheckBox.checked=checked;
                            root.updateEnabled();
                        }
                        style: CheckBoxStyle {
                            label: Label {
                                text: "rx"
                                color: DofConstant.dofLabelColor[3]
                            }
                        }
                    }
                    CheckBox {
                        id: dofRyCheckBox
                        onCheckedChanged: {
                            dofRxCheckBox.checked=checked;
                            dofRzCheckBox.checked=checked;
                            root.updateEnabled();
                        }
                        style: CheckBoxStyle {
                            label: Label {
                                text: "ry"
                                color: DofConstant.dofLabelColor[4]
                            }
                        }
                    }
                    CheckBox {
                        id: dofRzCheckBox
                        onCheckedChanged: {
                            dofRxCheckBox.checked=checked;
                            dofRyCheckBox.checked=checked;
                            root.updateEnabled();
                        }
                        style: CheckBoxStyle {
                            label: Label {
                                text: "rz"
                                color: DofConstant.dofLabelColor[5]
                            }
                        }
                    }
                }
            }
        }
        ScrollView {
            Layout.minimumHeight: 100
            Layout.minimumWidth: 300
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                id: frameControllerParent
                width: root.width
            }
        }
    }
}
