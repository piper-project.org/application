// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2
import QtQml 2.2

import SofaBasics 1.0
import SofaScene 1.0

import Piper 1.0

ModuleToolWindow {
    title: "Control"

    property var timeStepValues: [1.,0.1,0.01,0.003,0.001]
    property alias autoRunScript: runScriptSwitch.checked

    property Action sofaSceneAnimateAction : Action {
        text: "Positioning"
        tooltip: "Start/Stop the positioning process (F4)"
        checkable: true
        onToggled: {
            if(sofaScene) {
                sofaScene.animate = checked;
                fpsDisplay.enabled = checked;
            }
        }
    }

    Connections {
        target: sofaScene
        onAnimateChanged: {
            sofaSceneAnimateAction.checked = sofaScene.animate;
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }

    GridLayout {
        columns: 3
        // simu
        Button {
            action: sofaSceneAnimateAction
        }
        FPSItem {
            id: fpsDisplay
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignRight
            enabled: false
            horizontalAlignment: Text.AlignHRight
            verticalAlignment: Text.AlignVCenter
        }
        Button {
            id: resetButton
            text: "Reset"
            tooltip: "Reset to the initial position"
            Layout.alignment: Qt.AlignRight
            onClicked: {
                if(sofaScene)
                    sofaScene.reset();
            }
        }
        // time step
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Time step"
        }
        SpinBox {
            id: dtSpinBox
            decimals: 4
            minimumValue: 0.0001
            maximumValue: 1
            stepSize: 0.1
            value: 0.1
            suffix: "s"
            onValueChanged: {
                if(sofaScene) sofaScene.dt = value;
            }
        }
        ComboBox {
            implicitWidth: 80
            Layout.fillWidth: true
            model: timeStepValues
            currentIndex: 2
            onCurrentIndexChanged: {
                dtSpinBox.value = timeStepValues[currentIndex];
                dtSpinBox.stepSize = Math.min(model[currentIndex], 0.1);
            }
        }
        // auto stop
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Auto stop"
        }
        Switch {
            id: positioningAutoStop
            Layout.columnSpan: 2
            checked: false
            enabled: !sofaScene.animate
            onCheckedChanged: {
                if (sofaScene.ready)
                    sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "setAutoStop", checked);
            }
        }
        Connections {
            target: sofaScene
            onAnimateChanged: {
                if (positioningAutoStop.checked && sofaScene.animate)
                    context.logStart("Positioning");
                if (positioningAutoStop.checked && !sofaScene.animate)
                    context.logDone(" - iterations: "
                                    + sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "getNbIteration")
                                    + " - max dof velocity: "
                                    + sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "getMaxCartesianVelocity")
                                    + "m/s"
                                    );
            }
        }
        // bone and capsule collision
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Collision"
        }
        Switch {
            id: collisionSwitch
            ToolTip {
                text: "Activate/Deactivate collision (bone and capsule)"
            }
            onCheckedChanged: {
                if (collisionInfo.checked)
                    collisionInfo.checked = false;
                if (sofaScene.ready)
                    sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "setCollision", checked);
            }
        }
        SpinBox {
            id: collisionNbVertex
            minimumValue: 0
            maximumValue: 10000
            decimals: 0
            stepSize: 1
            onValueChanged: {
                if (sofaScene.ready)
                    sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "setCollisionNbVertex", value);
            }
            ToolTip {
                text: "Number of vertices considered for each collision (0 to keep them all)"
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Run script"
        }
        Switch {
            id: runScriptSwitch
            Layout.columnSpan: 2
            function callMostRecentScript() {
                if (!scripting.recentScriptsArgs.length > 0) {
                    context.logError("No recent script to be run at each simulation step");
                    return;
                }
                var scriptArgs = scripting.recentScriptsArgs[0];
                pythonScript.setPathUrl(scriptArgs[0]);
                pythonScript.setArgs( (scriptArgs.length>1 ? scriptArgs.slice(1,scriptArgs.length) : []) );
                pythonScript.run();
            }

            onCheckedChanged: {
                if (checked) {
                    sofaScene.stepEnd.connect(callMostRecentScript);
                }
                else {
                    sofaScene.stepEnd.disconnect(callMostRecentScript);
                }
            }
            ToolTip {
                text: "Run most recent python script after each simulation step"
            }
        }

        Button {
            id: collisionInfo
            checkable: true
            enabled: collisionSwitch.checked
            text: "Info"
            tooltip: "Enable/disable collision information in log (bone and capsule)"
            function callCheck() {
                sofaScene.sofaPythonInteractor.call("simulationControlInteractor", "checkCollision");
                context.importantMessageReceived();
            }
            onCheckedChanged: {
                if (checked) {
                    callCheck();
                    sofaScene.stepEnd.connect(callCheck);
                }
                else
                    sofaScene.stepEnd.disconnect(callCheck);
            }
            Component.onDestruction: {
                if (checked)
                    sofaScene.stepEnd.disconnect(callCheck);
            }
        }

        Component.onCompleted: {
            collisionNbVertex.value = moduleParameter.PhysPosi_boneCollisionNbVertex;
            var collisionEnabled;
            if (physPositioningInteractiveAction.text === currentModuleName)
                collisionEnabled = moduleParameter.PhysPosiInter_boneCollisionEnabled || moduleParameter.PhysPosiInter_capsuleEnabled;
            else
                collisionEnabled = moduleParameter.PhysPosiDefo_boneCollisionEnabled || moduleParameter.PhysPosiDefo_capsuleEnabled;
            collisionSwitch.checked = collisionEnabled;
            collisionSwitch.enabled = collisionEnabled;
            collisionNbVertex.enabled = collisionEnabled;
        }
    }
}
