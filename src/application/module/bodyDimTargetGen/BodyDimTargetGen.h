/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QObject>
#include <QString>
#include <qstringlist.h>
#include <QUrl>
#include <bodyDimDB/BodyDimDB.h>
#include <memory>
#include "hbm/target.h"

namespace piper {
namespace BodyDimTargetGen {
/** @brief Body Dimension Personalizing.
 *
 * @author Thomas Dupeux @date 2015
 */
class BodyDimTargetGen : public QObject
{
    Q_OBJECT

public:
    BodyDimTargetGen();

    Q_PROPERTY(QString info READ info NOTIFY infoChanged)
    Q_INVOKABLE QStringList SubjectsName();
    Q_INVOKABLE QStringList variablesName();
    Q_INVOKABLE QVariant subjectHasVariable(QString subject, QString variable);
    Q_INVOKABLE QStringList equationsName(QString subject);
    Q_INVOKABLE int equationNBVariables(QString subject, QString equation);
    Q_INVOKABLE QVariant equationHasVariable(QString subject, QString equation, QString variable);
    Q_INVOKABLE QList<double > variableRange(QString subject, QString equation, QString variable);
    Q_INVOKABLE QString variableUnit(QString subject, QString equation, QString variable);


    

    QString info() const;
    
	Q_INVOKABLE void generateTarget(QString const& subjectType, QVariant const & use_percentile, QVariant const & isTransformAbsolute, QVariant const & isPosSeated, QString const& equationName, QStringList const& valuesNameVect, QVariant const& valuesQList, QVariant const& initialValuesQList );
    Q_INVOKABLE void cancelPreviousTargetGeneration();
    Q_INVOKABLE void loadDB(QUrl filename,bool silentSuccess=false);
  /*  Q_INVOKABLE void loadCtrlPtDest(QUrl filename);
    Q_INVOKABLE void loadCtrlPtNugget(QUrl filename);
	*/
public slots:
    void bodyDimTargetGen();

signals:
    void infoChanged();
    void modelChanged();
    void bodyDimDBChanged();


private:
    QString m_info;
    std::shared_ptr<BodyDimDB::BodyDimDB > _bodyDb;
    BodyDimDB::DataBaseReader _reader;
    std::map<std::string, double>* _lastTargetDimensionResult;
    QStringList _subjectsName;
    QStringList _variablesName;
    QStringList _equationsName;
    std::list<piper::hbm::AnthropometricDimensionTarget> _oldTargetList;
};

}
} // namespace piper
