// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
//import piper.BodyDimTargetGen 1.0
import QtQuick.Dialogs 1.2


//import Qt.labs.settings 1.0


Dialog {
    // BodyDimPers {
    //    id: myBodyDimPers
    // }
    title: "Body Dimensions Target Generator"
    standardButtons:  StandardButton.Cancel | StandardButton.Ok | StandardButton.Apply 
    visible: false
    height: 500
    width : 300

	 property string subject:"child"
	 property string equation:"age"
	 property var values:[]
	 property var valuesName:[]
	 property var initialValues:[]
	 property var subjectsName:[]
	 property var subjectsWidget:[]
	 property var variablesName:[]
	 property var variablesWidget:[]
	 property var mywidget;


 	Component.onCompleted: {
        width = 500;
        createGUI();
        context.modelChanged.connect(resetProperties);  
        myBodyDimTargetGen.bodyDimDBChanged.connect(resetProperties)
	}

    Component.onDestruction: {
        
    }

    onApply: {
        mywidget.generateTarget();
    }

    onAccepted:{
        mywidget.generateTarget();
    }

    onRejected:{
        myBodyDimTargetGen.cancelPreviousTargetGeneration();
    }

        property int margin: 10

    Rectangle {

        Layout.fillWidth: true
        height:250
        ColumnLayout {
            id:widgetLayout
        }
    }
 

        function subjectRadioBtnWidget(index,name)
        {
            var widget = "";
            widget+='        RadioButton {id: subjectBtn'+String(index)+'\n';
            widget+='           text: "'+name+'"\n';
            widget+='           exclusiveGroup: radioGroup \n';
            if(index === 0){
                widget+='           checked:true\n';
            }
            widget+='           onClicked:{\n';
            widget+='               subject="'+name+'"\n';
            var equationsName = myBodyDimTargetGen.equationsName(name);
            var setVarChecked = false;
            for(var i=0;i<variablesName.length;i++){
                if(myBodyDimTargetGen.subjectHasVariable(name,variablesName[i])){
                    widget+='               varLayout'+String(i)+'.enabled=true\n';
                    if(!setVarChecked){
                        for(var k=0 ; k<variablesName.length;k++){
                            widget+='               varBtn'+String(k)+'.enabled=true\n';
                            if(k!=i)
                                widget+='                   varBox'+String(k)+'.enabled=false\n';
                            else
                                widget+='                   varBox'+String(k)+'.enabled=true\n';
                        }
                        
                        widget+='               varBtn'+String(i)+'.checked=true\n';
                        setVarChecked=true;
                    }
                }
                else{
                    widget+='               varLayout'+String(i)+'.enabled=false\n';
                }
            }    						
            widget+='               allVarsChkbx.checked=false\n';
            

            for(var j=0;j<equationsName.length;j++){
                for(var i=0;i<variablesName.length;i++){
                    if(myBodyDimTargetGen.equationHasVariable(name,equationsName[j],variablesName[i]) && myBodyDimTargetGen.equationNBVariables(name,equationsName[j]) === 1) {
                        var range = myBodyDimTargetGen.variableRange(name,equationsName[j],variablesName[i]);
                        widget+='               varSlider'+String(i)+'.value='+String((range[1]+range[0])/2)+'\n';
                        widget+='               varSlider'+String(i)+'.minimumValue='+String(range[0])+'\n';
                        widget+='               varSlider'+String(i)+'.maximumValue='+String(range[1])+'\n';
                        widget+='               varSlider'+String(i)+'.stepSize='+String(((range[1]-range[0])/1000).toExponential(0))+'\n';
                        var unitLabel = myBodyDimTargetGen.variableUnit(name,equationsName[j],variablesName[i]);
                        widget+='               varUnitLabel'+String(i)+'.text="'+unitLabel+'"\n';
                        widget+='               varNameLabel'+String(i)+'.text="'+equationsName[j]+'"\n';
                    }
                }
            }
            widget+='           }\n';
            widget+='       }\n';
            return widget;
        }

        function variableSliderWidget(index,name){
            var widget = "";
            widget+='       RowLayout{\n';
            widget+='           id:varLayout'+String(index)+'\n';
            widget+='           GroupBox{ title:" "\n id:varBox'+String(index)+'\n';
            if(index === 0){
                widget+='           enabled:true\n';
            }
            else
                widget+='               enabled:false\n';
            widget+='               RowLayout{\n';
            widget+='                   Label{\n';
            widget+='                       id:varNameLabel'+String(index)+'\n';
            widget+='                       text:"'+name+'"\n';
            widget+='                   }\n';
            widget+='                   Slider {\n';
            widget+='                       id: varSlider'+String(index)+'\n';


            widget+='                       width:150\n';
 
            var equationsName = myBodyDimTargetGen.equationsName(myBodyDimTargetGen.SubjectsName()[0]);
            var unitLabel;
            for(var j=0;j<equationsName.length;j++){
                if(myBodyDimTargetGen.equationHasVariable(myBodyDimTargetGen.SubjectsName()[0],equationsName[j],variablesName[index]) && myBodyDimTargetGen.equationNBVariables(myBodyDimTargetGen.SubjectsName()[0],equationsName[j]) === 1) {
                    var range = myBodyDimTargetGen.variableRange(myBodyDimTargetGen.SubjectsName()[0],equationsName[j],variablesName[index]);
                    widget+='                       value:'+String((range[1]+range[0])/2)+'\n';
                    widget+='                       minimumValue:'+String(range[0])+'\n';
                    widget+='                       maximumValue:'+String(range[1])+'\n';
                    unitLabel = myBodyDimTargetGen.variableUnit(myBodyDimTargetGen.SubjectsName()[0],equationsName[j],variablesName[index]);
                    widget+='                       stepSize: '+String(((range[1]-range[0])/1000).toExponential(0))+'\n';
                }
            }



            
            widget+='                       updateValueWhileDragging :true\n';
            widget+='                       onValueChanged:{\n';
            widget+='                           if(varTxtField'+String(index)+' != null){\n';
            widget+='                               varTxtField'+String(index)+'.text=varSlider'+String(index)+'.value}\n';
            widget+='                           }\n';
            widget+='                       }\n';
            widget+='                       TextField{\n';
            widget+='                           id: varTxtField'+String(index)+'\n';
            //displayText :
            widget+='                           maximumLength :6\n';
            widget+='                           readOnly : false\n';
            widget+='                           selectByMouse :true\n';
            widget+='                           width:30\n';
            widget+='                           text:varSlider'+String(index)+'.value\n';
            widget+='                           activeFocusOnPress:true\n';
            widget+='                           onEditingFinished:{\n';
            widget+='                               varSlider'+String(index)+'.value=varTxtField'+String(index)+'.text\n';
            widget+='                           }\n';
            widget+='                       }\n';


            widget+='                       Label{id: varUnitLabel'+String(index)+' ; text:"'+unitLabel+'"}\n';


            widget+='                   }\n';								
            widget+='               }\n';
            widget+='               RadioButton {id: varBtn'+String(index)+'; text: ""; exclusiveGroup: activeVariable ;\n';
            if(index === 0){
                widget+='                   checked:true\n';
            }
            widget+='                   onClicked:{\n';
            for(var i=0 ; i<variablesName.length;i++){
                if(i !== index)
                    widget+='                       varBox'+String(i)+'.enabled=false\n';
                else
                    widget+='                       varBox'+String(i)+'.enabled=true\n';
            }
            widget+='                   }\n';
            widget+='               }\n';
            widget+='           }\n';
           
            return widget;
        }

        function createGUIWidget(){
            var widget=""

            subjectsName = myBodyDimTargetGen.SubjectsName();
            variablesName = myBodyDimTargetGen.variablesName();

            widget+='import QtQuick 2.0\n';
            widget+='import QtQuick.Controls 1.2\n';
            widget+='import QtQuick.Layouts 1.0\n';

            widget+='ColumnLayout {\n';
            widget+='GroupBox {title: " "\n';
            widget+='    Layout.fillWidth: true\n';
            widget+='    RowLayout {\n';
            widget+='        anchors.fill: parent\n';
            widget+='        ExclusiveGroup { id: radioGroup }\n';

            for(var i=0;i<subjectsName.length;i++){
                widget+=subjectRadioBtnWidget(i, subjectsName[i])
            }
            
            widget+='               CheckBox {id: percentileChkbx; text: "percentile"; checked: false; \n';
            widget+='                   onClicked:\n';
            widget+='                       {\n';
            widget+='                       } \n';
            widget+='                   enabled:false\n';
            widget+='               }\n';
            widget+='               CheckBox {id: seatedChkbx; text: "seated posture"; checked: true; \n';
            widget+='                   onClicked:\n';
            widget+='                       {\n';
            widget+='                       }\n';
            widget+='                   enabled:false\n';
            widget+='               }\n';
            widget+='               CheckBox {id: relativeChkbx; text: "relative scale"; checked: false; \n';
            widget+='                   onClicked:\n';
            widget+='                       {\n';
            widget+='                   } \n';
            widget+='               enabled:false\n';
            widget+='           }\n';

            widget+='    }\n';
            widget+='}\n';
            widget+='GroupBox {title: " "\n';
            widget+='    ExclusiveGroup { id: activeVariable }\n';
            widget+='    ColumnLayout{\n';
            
            for(var i=0;i<variablesName.length;i++){
                widget+=variableSliderWidget(i, variablesName[i])
            }
                       
            widget+='RowLayout{\n';
            widget+='    anchors.right: parent.right\n';
            widget+='    CheckBox {id: allVarsChkbx; text: "all"; checked: false; \n';
            widget+='        anchors.right: parent.right\n';
            widget+='        onClicked:\n';
            widget+='            {\n';

            for(var i=0;i<variablesName.length;i++){
                widget+='    if(myBodyDimTargetGen.subjectHasVariable(subject,"'+variablesName[i]+'")){\n';
                widget+='varBox'+String(i)+'.enabled=allVarsChkbx.checked\n';
                widget+='varBtn'+String(i)+'.enabled=!allVarsChkbx.checked\n';
                widget+='            } \n';
            }
            widget+='    }\n';
            widget+='    }\n';
            widget+='}\n';

            widget+='       }\n';
            widget+='       }\n';
            widget+='function generateTarget(){\n';
            widget+='   if(allVarsChkbx.checked){\n';
            
            for(var j=0;j<subjectsName.length;j++){
                var equationsName = myBodyDimTargetGen.equationsName(subjectsName[j]);
                widget+='       if(subjectBtn'+String(j)+'.checked){\n';

                    for(var k=0;k<equationsName.length;k++){
                        if(myBodyDimTargetGen.equationNBVariables(subjectsName[j],equationsName[k])>1) {
                            widget+='           equation = "'+equationsName[k]+'";\n';
                            widget+='           valuesName=[];\n';
                            widget+='           values=[];\n';
                            for(var i=0;i<variablesName.length;i++){

                                if(myBodyDimTargetGen.equationHasVariable(subjectsName[j],equationsName[k],variablesName[i])){
                                    widget+='           valuesName.push("'+variablesName[i]+'");\n';
                                    widget+='           values.push(varSlider'+String(i)+'.value);\n';
                                }
                            }
                            widget+='           initialValues=[];\n';
                        }
                    }
                    widget+='       }\n';// subjectBtn
                }
            widget+='   }\n';// allVarsChkbx
            

            
            widget+='   else{\n';// allVarsChkbx
            for(var j=0;j<subjectsName.length;j++){
                var equationsName = myBodyDimTargetGen.equationsName(subjectsName[j]);
                widget+='       if(subjectBtn'+String(j)+'.checked){\n';
                for(var i=0;i<variablesName.length;i++){
                    widget+='           if(varBox'+String(i)+'.enabled){\n';
            
                    for(var k=0;k<equationsName.length;k++){
                        if(myBodyDimTargetGen.equationHasVariable(subjectsName[j],equationsName[k],variablesName[i]) && myBodyDimTargetGen.equationNBVariables(subjectsName[j],equationsName[k]) === 1) {
                            widget+='               equation = "'+equationsName[k]+'";\n';
                            widget+='               valuesName = ["'+variablesName[i]+'"];\n';
                            widget+='               values = [varSlider'+String(i)+'.value];\n';
                            widget+='               initialValues=[];\n';
                        }
                    }
                    widget+='           }\n'; //varBox
                }
                widget+='       }\n';//subjectBtn
            }

            widget+='   }\n';// allVarsChkbx
                             
            widget+='myBodyDimTargetGen.generateTarget(subject,percentileChkbx.checked, !relativeChkbx.checked,seatedChkbx.checked,equation,valuesName,values, initialValues );\n';
            widget+='}\n';
            widget+='       }\n';
            //console.log(widget);
            return widget
        }

        function createGUI()
        {
            mywidget=Qt.createQmlObject(createGUIWidget(), widgetLayout, "mywidget");
        }

        function clearGUI()
        {
            mywidget.destroy();
        } 

        function resetProperties(){
            clearGUI();
            createGUI();
        }
}
