/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "BodyDimTargetGen.h"

#include <QDebug>
#include <QTextStream>
#include <QFileInfo>
#include <QProcess>
#include <QCoreApplication>
#include <utility>

#include <tinyxml/tinyxml2.h>

#include "common/message.h"
#include "common/logging.h"
#include "common/Context.h"
#include "common/XMLValidationProcess.h"

//#include "bodyDimPers/bodyDimPersPiperInterface.h"

#include "kriging/KrigingPiperInterface.h"
#include "hbm/Metadata.h"
#include "hbm/Landmark.h"


namespace piper {
    namespace BodyDimTargetGen {
        using namespace hbm;

        BodyDimTargetGen::BodyDimTargetGen()
        {
            //connect(&Context::instance(), &Context::modelChanged, this, &BodyDimTargetGen::bodyDimTargetGen);
            connect(&Context::instance(), &Context::projectFileChanged, this, &BodyDimTargetGen::bodyDimTargetGen);
            bodyDimTargetGen();
        }

        QString BodyDimTargetGen::info() const
        {
            return m_info;
        }

        void BodyDimTargetGen::bodyDimTargetGen()
        {
            _bodyDb = std::make_shared<BodyDimDB::BodyDimDB>();
            if (!Context::instance().project().bodyDimDBFileName().empty()){
                loadDB(QUrl::fromLocalFile(QString::fromStdString(Context::instance().project().bodyDimDBFileName())));
            }
            else
            {
                loadDB(QUrl::fromLocalFile(QCoreApplication::applicationDirPath() + "/" + "gebod_dimdata.xml"),true);
                Context::instance().project().setBodyDimDBFileName(std::string(""));  //avoid storing database filename in piperproject as the one used is the default one in the application directory, therefore not relative to the project
            }
        }

        void BodyDimTargetGen::generateTarget(QString const& subjectType, QVariant const & use_percentile, QVariant const & isTransformAbsolute, QVariant const & isPosSeated, QString const& equationName, QStringList const& valuesNameVect, QVariant const& valuesQList, QVariant const& initialValuesQList){

            m_info.clear();
            //pInfo() << START << QStringLiteral("Generating dimensions ") ;

            //emit infoChanged();

            if (!_reader.isDbLoaded()){
                pCritical() << "[BodyDimTargetGen] - No DataBase - make sure that a DataBase is successfuly loaded." ;
                emit infoChanged();
                return;
            }

            std::vector<std::pair<std::string, double > > valuesVect;
            std::vector<std::pair<std::string, double > > initialValuesVect;

            for (int i = 0; i < valuesNameVect.size(); i++){
                if (i < valuesNameVect.size() && i < valuesQList.toList().size())
                    valuesVect.push_back(std::make_pair(valuesNameVect.at(i).toStdString(), valuesQList.toList().at(i).toDouble()));
                if (i < valuesNameVect.size() && i < initialValuesQList.toList().size())
                    initialValuesVect.push_back(std::make_pair(valuesNameVect.at(i).toStdString(), initialValuesQList.toList().at(i).toDouble()));
            }

            _bodyDb->_conv.outputLengthUnit = units::LengthUnit::unitToStr(Context::instance().project().model().metadata().lengthUnit());
            _bodyDb->_conv.outputWeightUnit = std::string("kg"); //TODOThomasD : ajouter ces unit au modele HBM
            _bodyDb->_conv.outputAgeUnit = std::string("month"); //TODOThomasD : ajouter ces unit au modele HBM

            _lastTargetDimensionResult = nullptr;
            //subject type exists in db
            if (_bodyDb->m_subjects.find(subjectType.toStdString()) != _bodyDb->m_subjects.end()){ //TODOThomasD : use tolower when creating db and here
                BodyDimDB::BodyDimDB::Subject &subject = _bodyDb->m_subjects.at(subjectType.toStdString());
                if (use_percentile.toBool()){
                    //TODOThomasD : compute values out of percentile - see how gebod does
                    //and replace the values vec with it ?
                }
                else{
                    //maybe do nothing ?
                }
                //equation exists in database
                if (subject.m_equations.find(equationName.toStdString()) != subject.m_equations.end()){
                    //TODOThomasD : make smthg easier for multiple variables
                    BodyDimDB::BodyDimDB::Equation &equation = subject.m_equations.at(equationName.toStdString());

                    if (isTransformAbsolute.toBool()){
                        equation.computeDimensions(valuesVect);
                    }
                    else
                    {
                        equation.computeDimensions(initialValuesVect, valuesVect);
                    }
                    _lastTargetDimensionResult = &equation.m_dimensionTargetResult;
                }
                else
                    pCritical() << "[BodyDimTargetGen] BodyDimension DataBase does not contain input equation " + equationName ;
            }
            else
                pCritical() << "[BodyDimTargetGen] BodyDimension DataBase does not contain input subject " + subjectType ;

            //save old target list for local cancel
            std::list<piper::hbm::AnthropometricDimensionTarget> tempOldTargetList = Context::instance().project().target().anthropometricDimension;

            Context::instance().project().target().anthropometricDimension.clear();
            for (auto itDim = _lastTargetDimensionResult->begin(); itDim != _lastTargetDimensionResult->end(); ++itDim){
                if ((*itDim).first == "AGE") {
                    AgeTarget agetarget;
                    agetarget.setName((*itDim).first);
                    try{
                        agetarget.setValue((*itDim).second);
                        agetarget.setUnit(units::AgeUnit::strToUnit(_bodyDb->_conv.outputAgeUnit));
                        agetarget.convertToUnit(Context::instance().project().model().metadata().ageUnit());
                    }
                    catch (std::exception e) {
                        pWarning() << QString::fromStdString("Cannot add dimension " + (*itDim).first + " with value " + std::to_string((*itDim).second) + ", error : ") << e.what();
                        emit infoChanged();
                    }
                    Context::instance().project().target().age.push_back(agetarget);
                }
                else {
                    AnthropometricDimensionTarget target;
                    target.setName((*itDim).first);
                    try{
                        target.setValue((*itDim).second);
                        target.setUnit(units::LengthUnit::strToUnit(_bodyDb->_conv.outputLengthUnit));
                        target.convertToUnit(Context::instance().project().model().metadata().lengthUnit());
                    }
                    catch (std::exception e) {
                        pWarning() << QString::fromStdString("Cannot add dimension " + (*itDim).first + " with value " + std::to_string((*itDim).second) + ", error : ") << e.what();
                        emit infoChanged();
                    }
                    Context::instance().project().target().anthropometricDimension.push_back(target);
                }
            }

            // add thigh length = KNEE_HEIGHT_SEATED - BUTTOCK_DEPTH
            double thigh_length_value = _lastTargetDimensionResult->at("KNEE_HEIGHT_SEATED") - _lastTargetDimensionResult->at("BUTTOCK_DEPTH");
            AnthropometricDimensionTarget target;
            target.setName("THIGH_LENGTH");
            target.setValue(thigh_length_value);
            target.setUnit(units::LengthUnit::strToUnit(_bodyDb->_conv.outputLengthUnit));
            target.convertToUnit(Context::instance().project().model().metadata().lengthUnit());
            Context::instance().project().target().anthropometricDimension.push_back(target);

            bool targetWasUpdated = Context::instance().project().target().anthropometricDimension.size() != tempOldTargetList.size();
            if (!targetWasUpdated){
                auto oldIt = tempOldTargetList.begin();
                for (auto it = Context::instance().project().target().anthropometricDimension.begin(); it != Context::instance().project().target().anthropometricDimension.end(); ++oldIt, ++it){
                    targetWasUpdated = ((*it).name().compare((*oldIt).name()) != 0) || ((*it).value()!= (*oldIt).value());
                    if (targetWasUpdated){ break; }
                }
            }

            if (targetWasUpdated){
                _oldTargetList = tempOldTargetList;
                //    subjectType,
                //       valuesNameVect,
                //      valuesQList,
                QString res;

                for (int i = 0; i < valuesVect.size(); ++i){
                    if (i != 0){ res.append(", "); }
                    res.append(QString::fromStdString(valuesVect[i].first) + " = " + QString::fromStdString(std::to_string(valuesVect[i].second)) + variableUnit(subjectType, equationName, QString::fromStdString(valuesVect[i].first)));
                }
                pInfo() << INFO << "Generated target dimensions for target : " + subjectType + " (" + res + ")" ;
                emit infoChanged();
            }

            return;
        }

        void BodyDimTargetGen::cancelPreviousTargetGeneration(){
            if (!_oldTargetList.empty()){
                Context::instance().project().target().anthropometricDimension.clear();
                Context::instance().project().target().anthropometricDimension = _oldTargetList;
                _oldTargetList.clear();
                pInfo() << INFO << QStringLiteral("Cancel generating target dimensions and restored previous targets.") ;
            }
        }

        void BodyDimTargetGen::loadDB(QUrl filename,bool silentSuccess){
            //m_info.clear();
            QTextStream out(&m_info, QIODevice::WriteOnly);

            _bodyDb->clear();

            try{
                _reader.read(filename.toLocalFile().toStdString(), *_bodyDb);
            }
            catch (std::exception e){
                out << FAILEDMSG << e.what() << endl;
                pCritical() << QStringLiteral("Error while loading database file ") << filename.toLocalFile() << " : " << e.what() ;
                emit infoChanged();
                return;
            }

            if (_reader.isDbLoaded()){
                Context::instance().project().setBodyDimDBFileName(filename.toLocalFile().toStdString());
                if (!silentSuccess){
                    pInfo() << INFO << " BodyDimDB : " + filename.toLocalFile() + " successfully loaded" ;
                    emit infoChanged();
                }
            }
            emit bodyDimDBChanged();
        }

        QStringList  BodyDimTargetGen::SubjectsName(){
            _subjectsName.clear();
            if (_reader.isDbLoaded()) {
                for (auto it = _bodyDb->m_subjects.begin(); it != _bodyDb->m_subjects.end(); ++it) {
                    _subjectsName.push_back(QString::fromStdString((*it).first));
                }
            }
            return _subjectsName;

        }


        QStringList BodyDimTargetGen::variablesName(){
            _variablesName.clear();
            if (_reader.isDbLoaded()) {
                for (auto subjectIt = _bodyDb->m_subjects.begin(); subjectIt != _bodyDb->m_subjects.end(); ++subjectIt) {
                    for (auto equationIt = (*subjectIt).second.m_equations.begin(); equationIt != (*subjectIt).second.m_equations.end(); ++equationIt) {
                        for (auto variableIt = (*equationIt).second.m_variables.begin(); variableIt != (*equationIt).second.m_variables.end(); ++variableIt) {
                            if (!_variablesName.contains(QString::fromStdString((*variableIt).first)))
                                _variablesName.push_back(QString::fromStdString((*variableIt).first));
                        }
                    }
                }
            }
            return _variablesName;
        }

        QVariant BodyDimTargetGen::subjectHasVariable(QString subject, QString variable){
            if (_reader.isDbLoaded()) {
                if (_bodyDb->m_subjects.find(subject.toStdString()) != _bodyDb->m_subjects.end()){
                    for (auto equationIt = _bodyDb->m_subjects.at(subject.toStdString()).m_equations.begin(); equationIt != _bodyDb->m_subjects.at(subject.toStdString()).m_equations.end(); ++equationIt) {
                        if ((*equationIt).second.m_variables.find(variable.toStdString()) != (*equationIt).second.m_variables.end()){
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        QStringList BodyDimTargetGen::equationsName(QString subject){
            _equationsName.clear();
            if (_reader.isDbLoaded()) {
                if (_bodyDb->m_subjects.find(subject.toStdString()) != _bodyDb->m_subjects.end()){
                    for (auto it = _bodyDb->m_subjects.at(subject.toStdString()).m_equations.begin(); it != _bodyDb->m_subjects.at(subject.toStdString()).m_equations.end(); ++it){
                        _equationsName.push_back(QString::fromStdString((*it).first));
                    }
                }
            }
            return _equationsName;
        }

        int BodyDimTargetGen::equationNBVariables(QString subject, QString equation){
            if (_reader.isDbLoaded()) {
                if (_bodyDb->m_subjects.find(subject.toStdString()) != _bodyDb->m_subjects.end()){
                    if (_bodyDb->m_subjects.at(subject.toStdString()).m_equations.find(equation.toStdString()) != _bodyDb->m_subjects.at(subject.toStdString()).m_equations.end()){
                        return (int)(_bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.size());
                    }
                }

            }
            return 0;
        }
        QVariant BodyDimTargetGen::equationHasVariable(QString subject, QString equation, QString variable){
            if (_reader.isDbLoaded()) {
                if (_bodyDb->m_subjects.find(subject.toStdString()) != _bodyDb->m_subjects.end()){
                    if (_bodyDb->m_subjects.at(subject.toStdString()).m_equations.find(equation.toStdString()) != _bodyDb->m_subjects.at(subject.toStdString()).m_equations.end()){
                        return _bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.find(variable.toStdString()) != _bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.end();
                    }
                }
            }
            return false;
        }

        QList<double > BodyDimTargetGen::variableRange(QString subject, QString equation, QString variable){
            QList<double> range;
            _bodyDb->_conv.outputLengthUnit = units::LengthUnit::unitToStr(Context::instance().project().model().metadata().lengthUnit());
            _bodyDb->_conv.outputWeightUnit = std::string("kg"); //TODOThomasD : ajouter ces unit au modele HBM et libunit
            _bodyDb->_conv.outputAgeUnit = std::string("month"); //TODOThomasD : ajouter ces unit au modele HBM et libunit

            if (equationHasVariable(subject, equation, variable).toBool()){
                double rangemin = _bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.at(variable.toStdString())._range._min;
                double rangemax = _bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.at(variable.toStdString())._range._max;
                rangemin = _bodyDb->_conv.toOutputUnit(_bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.at(variable.toStdString())._unit, rangemin);
                rangemax = _bodyDb->_conv.toOutputUnit(_bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.at(variable.toStdString())._unit, rangemax);
                range.push_back(rangemin);
                range.push_back(rangemax);
                
            }
            
            return range;
        }

        QString BodyDimTargetGen::variableUnit(QString subject, QString equation, QString variable){
            if (equationHasVariable(subject, equation, variable).toBool()){
                return QString::fromStdString(_bodyDb->_conv.getCorrespondingOutputUnit(_bodyDb->m_subjects.at(subject.toStdString()).m_equations.at(equation.toStdString()).m_variables.at(variable.toStdString())._unit));
            }
            return QString("");
        }
    

    }
}
