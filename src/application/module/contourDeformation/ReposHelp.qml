// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
        id:root
        title: "Positioning module UI information"
        minimumHeight : 600
        minimumWidth : 900


                   TextArea {
                       id:textArea
                       anchors.fill: parent
                       text:
                           " Load ContourCL file :\nIt loads the contourCL metadata. It is required for generation of contours around the body and the information required to reposition the model at a specified joint by giving a specific angle wih respect to the current position of the model.\nIn current piper application contourCL is the yellow highlighted line segment. It is basically made up by joining the anatomical landmarks of the human body.\n " +
                           "\nLoad spline infomation (for hip positioning) File icon :\nUser can externally load the spline information for the hip region if the user wants to perform the repositioning at hip joint. User needs to load the file at following path :  {model_path}/GHBM/metadata/contours_for_hip/Contour_Input.txt. \n " +
                           "\nModule Opacity :\nIt controls the opacity of human body model.\n " +
                           "\nContour CL opacity :\nIt controls the opacity of contourCL and generated contours. \n " +
                           "\nRepositioning Parameters :\nThis section takes input from the user about the joint at which he wants to do the positioning, the type of movement, the side(left/right)\nand the angle(with respect to the current position).\n " +
                           "\n\tSelect joint :\n\tSelect the joint from a list of joints- Knee, Ankle, Hip, Neck, Elbow or Wrist \n" +
                           "\n\tSelect side :\n\tLeft or Right Side\n" +
                           "\n\tSelect type :\n\tType of movement- Flexion/Extension, Inversion/Aversion, Lateral Rotation or Lateral Flexion etc. \n" +
                           "\n\tAngle :\n\tAngle must be given with respect to the current position. eg. A value of 10 degree at LEFT KNEE joint shall do a flexion of knee by an angle of 10 degree. \n" +
                           "\nGenerate Contours :\nContours are typically closed non-intersecting curves like circle, ellipse or a polyline enveloping the HBM. In some cases they can also be open cubic splines.\nThe contour modification routines shall modify them accordingly.
    \nTypically, the contours should not be on the skin but slightly bigger and away from surface of mesh. Generate contour will create these contours.
   \n" +
                           "\nDeform Contours :\nContours will be deformed by the input angle at the selected joint. \n" +
                           "\nReposition :\nThe model will be repositioned. \n"+
                           "\nUse Landmark Switch : If the positioning is to be done using the landmark targets import the target file from the file menu and click on this switch to start the positioning through landmark targets\n"+
                           "\nStart Positioning Using Targets : Click on this button to compute the input required to do the positioning.\n"+
                           "\nProcess Next Target : Click on this button to continue on to the next target for positioning till a prompt of positioning complete comes up.\n"
                   }


}
