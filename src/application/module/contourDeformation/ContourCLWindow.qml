// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2

//import Piper 1.0

import SofaBasics 1.0
import SofaWidgets 1.0

import piper.MetaEditor 1.0
import VtkQuick 1.0
import piper.MetaViewManager 1.0
import "../check"
import Piper 1.0

ModuleToolWindow 
{
    id:main
    title: "Contour CL Window"
    minimumHeight : 500
    minimumWidth : 900
    property string selectedNode

    onSelectedNodeChanged: {
        nodeDescriptionText.text = "Description for "+selectedNode
    }

    ListModel{
        id: predefinedLandmarksList
    }
    ListModel{
        id: anatomyDBLandmarksList
    }



    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal



        ColumnLayout{

            Button {
               id: getMap
               text: "Display ContourCL Hierarchy"
               property  variant map
               property variant vals
               property variant anatomyDBlandmarks

               onClicked: {
                   objModel.clear()
                   //console.log("ui_getContourCLTree() called")
                   map = myContourDeformation.ui_getContourCLTree();
                   var node =  map["Root"]
                   if (map[node] != null) {
//                   var mapdata = '';
//                   for (var prop  in map) {
//                     mapdata += prop + ' : ' + map[prop]+' No of Children = '+map[prop].length+';\n';
//                     console.log( mapdata )
//                     mapdata= ''
//                   }
                  // console.log("\n##########################################\n")
                           objModel.append({role: qsTr(node), "level": 0, "subNode": [], "childCount": map[node].length, "selected":false})
                           var parentNode = objModel.get(0)
                           //console.log(parentNode.role+"\t Level = "+parentNode.level+"\t child count = "+parentNode.childCount)
                           var i=0
                           for(i; i<map[node].length ; i++) {
                               console.log(map[node][i])
                               addNode( parentNode , qsTr(map[node][i]) , 1, i )
                           }

                           vals = contextMetaManager.getLandmarkList();

                           predefinedLandmarksList.clear()
                           //predefinedLandmarksList.append({role: "__dummy_landmark__"})
                           for(var i =0; i<vals.length; i++){
                             predefinedLandmarksList.append({role: vals[i]});
                           }

                           anatomyDBlandmarks = myContourDeformation.ui_getAnatomyDBLandmarks()
                           anatomyDBLandmarksList.clear()
                           //console.log("=======================================================")
                           for(var i =0; i<vals.length; i++){
                             if(anatomyDBlandmarks[i] == "")
                                 continue
                             anatomyDBLandmarksList.append({role: anatomyDBlandmarks[i]});
                            // console.log(anatomyDBlandmarks[i]+"\t"+myContourDeformation.ui_getLandmarkDescriptionFromAnatomyDB(anatomyDBlandmarks[i]))
                           }
                   }else{
                        messageDialog.text = "Please load the ContourCL in the Reposition window "
                        messageDialog.visible = true
                   }

               }
               MessageDialog {
                   id: messageDialog
                   title: "Alert"
                   text: ""
                   onAccepted: {

                   }
                   Component.onCompleted: visible = false
               }

               function addNode( parentNode , node , level , position ){
                   //console.log(node)
                   var childCount
                   //console.log("Leaf Node")
                   if(map[node]==null){
                       console.log("Leaf Node")
                       childCount = 0
                   }
                   else
                       childCount = map[node].length
                   parentNode.subNode.append({role: qsTr(node), "level": level, "subNode": [], "childCount":childCount , "selected":false})
                   var current = parentNode.subNode.get(position)
                   //console.log(current.role+"\t Level = "+current.level+"\t child count = "+current.childCount)
                   var i=0
                   for(i; i<childCount ; i++) {
                       //console.log(map[node][i])
                       addNode( current , qsTr(map[node][i]) , level+1 ,i )
                   }

               }


            }

            Rectangle {
               width: 300
               height: 500
               color: "white"



               ListModel {
                  id: objModel
               }

               Component {
                  id: objRecursiveDelegate
                  Column {
                     id: objRecursiveColumn
                     clip: true
                     MouseArea {
                        width: objRow.implicitWidth
                        height: objRow.implicitHeight

                        function selectNode(name,current){
                            if (current.role==name){
                                current.selected = !current.selected
                                return current
                            }
                            else if (current.childCount == 0)
                                    return null
                            else  {
                                var i=1
                                var child
                                var node
                                for (i; i <= current.childCount; i++) {
                                     child = current.subNode.get(i-1)
                                     node = selectNode(name,child)
                                     if (node == null){
                                         continue
                                     }
                                     if (node.role == name ){
                                         return node
                                     }
                                }
                            }
                            return null
                        }

                        function deselectAllNodes(current){
                            current.selected = false

                                var i=1
                                var child
                                var node
                                for (i; i <= current.childCount; i++) {
                                     child = current.subNode.get(i-1)
                                     node = deselectAllNodes(child)
                                }

                            return null
                        }

                        onClicked: {
                           for(var i = 1; i < parent.children.length - 1; ++i) {
                              parent.children[i].visible = !parent.children[i].visible
                           }
                           var name = model.role
                           main.selectedNode = name
                           if(model.level%2==0){
                               nodeDescriptionText.text = myContourDeformation.ui_getBRDescription(main.selectedNode)
                               circumference.visible = true
                               circumference.type = "bodyRegion"
                               circumInfoView.getList()
                               ctrlPtLandmarks.visible = true
                               splineInfo.visible = false
                               splLandmark.visible= false
                               pointSetView.getList()
                           }

                           else{
                               nodeDescriptionText.text = myContourDeformation.ui_getJointDescription(main.selectedNode)
                               circumference.visible = true
                               circumference.type = "joint"
                               circumInfoView.getList()
                               ctrlPtLandmarks.visible = false
                               splineInfo.visible = true
                               splLandmark.visible= true
                               splineInfoView.getList()
                               splLandmarkListView.getList()

                           }

                           var current = objModel.get(0)
                           var root = objModel.get(0)
                           deselectAllNodes(root)
                           console.log(selectNode(name,current).role)
                        }
                        onDoubleClicked:{
//                            console.log(model.role)
//                            console.log(model.level )
//                            console.log(model.subNode )
//                            row.color = "steelblue"
                        }
                        Component.onCompleted: {
                            for(var i = 1; i < parent.children.length - 1; ++i) {
                               parent.children[i].visible = !parent.children[i].visible
                            }
                        }
                        Rectangle{
                            id:row
                            color: model.selected == true ? "#b3ccff"  : "transparent"
                            height: 14
                            width:300
                            Row {
                               id: objRow
                               Item {
                                  height: 1
                                  width: model.level * 20
                               }
                               Text {
                                  text:{
                                      (objRecursiveColumn.children.length > 2 ?
                                           objRecursiveColumn.children[1].visible ?
                                           qsTr("-  ") : qsTr("+ ") : qsTr("   ")) + model.role}
                                  color: model.level % 2  == 0 ? "black" : "blue"
                                  //font { bold: true; pixelSize: 14 }
                               }
                            }
                        }
                     }
                     Repeater {
                        model: subNode
                        delegate: objRecursiveDelegate
                     }
                  }



               }

               ColumnLayout {
                  anchors.fill: parent
                  ListView {
                     Layout.fillHeight: true
                     Layout.fillWidth: true
                     model: objModel
                     delegate: objRecursiveDelegate
                  }



                    }

                }

                GroupBox{
                    title: " Description : "
                    Text{
                        id: nodeDescriptionText
                        width: 300
                        text : "Please select an element in the tree"
                        wrapMode: Text.WordWrap
                        onTextChanged: {
                            if(text==""){
                                text = "No description available"
                            }
                        }
                    }
                }

                GroupBox{
                    ColumnLayout{
                        RowLayout{
                            Label{text:"Model Opacity"}
                            SpinBox {
                                id: fullModelOpacity;
                                decimals: 2
                                minimumValue: 0.00
                                maximumValue: 1.00
                                stepSize: 0.10
                                value: 1.00
                                onValueChanged: {
                                    myContourDeformation.setOpacity(fullModelOpacity.value);
                                }
                            }
                        }

                    }
                }
            }

            ColumnLayout{

                anchors.top: main.top


                CollapsibleGroupBox{
                    Layout.fillWidth: true
                    id:ctrlPtLandmarks
                    title: "Control Point Landmarks"
                    visible:false
                    ColumnLayout{
                        RowLayout{
                            GroupBox{
                                visible: false
                                Row{
                                    Button{
                                        text:"Display Landmarks"
                                    }

                                    Button{
                                        text:"Preview Contours"
                                    }
                                    CheckBox {
                                           text: qsTr("Show description")
                                           checked: false
                                           onCheckedChanged: {
                                                if(checked==true){
                                                    ctrlPtLandmarksDescription.visible = true
                                                    pointSetViewDescription.visible = true
                                                    pointSetViewDescription.resizeToContents()

                                                }else{
                                                    ctrlPtLandmarksDescription.visible = false
                                                    pointSetViewDescription.visible = false
                                                }
                                           }
                                    }
                                }
                            }
                            GroupBox{
                                id:ctrlPtLandmarksDescription
                                visible: false
                                title: "Description"
                                Text{
                                    id:ctrlPtLandmarksDescriptionText
                                    text: "Description for control point landmarks "
                                }
                            }
                        }

                        RowLayout{
                            GroupBox{
                                Row{
                                    Button{
                                        text: "Add landmark"
                                        onClicked: {
                                              addNewCtrlLandmark.visible = true
                                        }

                                    }
                                     Button{
                                        visible: false
                                        text: "Edit "
                                        onClicked: {
                                               editCtrlLandmark.visible = true
                                        }
                                    }
                                    Button {
                                        text: "Remove "
                                        onClicked: {
                                            console.log("Removing "+pointSetList.get(pointSetView.currentRow).landmark)
                                            myContourDeformation.ui_removeCtrlPtLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                            pointSetView.getList()
                                        }
                                    }

                                }
                            }
                            GroupBox{
                                Row{
                                    Button{
                                        text:"Move Up"
                                        onClicked:{
                                            if(pointSetView.currentRow != 0){
                                                console.log(pointSetView.currentRow)
                                                myContourDeformation.ui_moveUpCtrlLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                                pointSetView.getList()
                                                pointSetView.selection.deselect(pointSetView.currentRow)
                                                pointSetView.currentRow -= 1
                                                pointSetView.selection.select(pointSetView.currentRow)
                                            }
                                        }
                                    }
                                    Button{
                                        text:"Move Down"
                                        onClicked:{
                                             if(pointSetView.currentRow != pointSetView.rowCount-1){
                                                 console.log(pointSetView.currentRow)
                                                 myContourDeformation.ui_moveDownCtrlLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark  )
                                                 pointSetView.getList()
                                                 pointSetView.selection.deselect(pointSetView.currentRow)
                                                 pointSetView.currentRow += 1
                                                 pointSetView.selection.select(pointSetView.currentRow)

                                             }
                                        }
                                    }
                                }
                            }

                            Button{
                                iconSource:  "qrc:///icon/information-icon.png"
                                onClicked: {
                                      messageDialog.text = ctrlPtLandmarksDescriptionText.text
                                      messageDialog.visible = true


                                }
                            }

                        }


                        GroupBox{
                            id: addNewCtrlLandmark
                            visible: false
                            ColumnLayout{
                                RowLayout{
                                   ExclusiveGroup { id: ctrlPtLndmrkGroup }
                                           RadioButton {
                                               text: "Add a pre-defined landmark"
                                               checked: true
                                               exclusiveGroup: ctrlPtLndmrkGroup
                                               onCheckedChanged:
                                               {
                                                    addPredefinedLandmark.visible = true
                                                    addAnatomyDBLandmark.visible = false

                                               }
                                           }
                                           RadioButton {
                                               text: "Add an Anatomy DB landmark"
                                               exclusiveGroup: ctrlPtLndmrkGroup
                                               onClicked: {
                                                       addPredefinedLandmark.visible =  false
                                                       addAnatomyDBLandmark.visible = true
                                                   }
                                           }
                                }
                                RowLayout
                                {
                                    id : addPredefinedLandmark
                                    visible: true
                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addNewCtrlLandmarkName
                                        model: predefinedLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }
                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {

                                            if(addSplineInfoName.currentIndex != -1)
                                                myContourDeformation.ui_addCtrlPtLandmark( main.selectedNode, predefinedLandmarksList.get(addNewCtrlLandmarkName.currentIndex).role, "No description available")
                                            pointSetView.getList()
                                            addNewCtrlLandmark.visible = false
                                            addPredfinedLandmark.visible = false

                                        }
                                    }
                                }
                                RowLayout
                                {
                                    visible: false
                                    id : addAnatomyDBLandmark
                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addAnatomyDBLandmarkName
                                        model: anatomyDBLandmarksList
                                        currentIndex: -1
                                    }
                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            var desc = myContourDeformation.ui_getLandmarkDescriptionFromAnatomyDB(anatomyDBLandmarksList.get(addAnatomyDBLandmarkName.currentIndex).role)
                                            if(addSplineInfoName.currentIndex != -1)
                                                myContourDeformation.ui_addCtrlPtLandmark(main.selectedNode, anatomyDBLandmarksList.get(addAnatomyDBLandmarkName.currentIndex).role, desc)
                                            pointSetView.getList()
                                            addNewCtrlLandmark.visible = false
                                            addAnatomyDBLandmark.visible = false

                                        }
                                    }
                                }
                            }
                        }

                        RowLayout
                        {
                            visible: false
                            id : editCtrlLandmark
                            Label
                            {
                                text: "Edit Landmark : "
                            }
                            TextField
                            {
                                id:editPointSetName;
                                placeholderText: qsTr(" Name ")
                            }
                            Button
                            {

                                text: "Add"
                                tooltip: "Add"
                                onClicked:
                                {
                                    console.log("Editing "+pointSetList.get(pointSetView.currentRow).landmark)
                                    if(editPointSetName.text!=""  )
                                        myContourDeformation.ui_editCtrlPtLandmark( main.selectedNode, pointSetList.get(pointSetView.currentRow).landmark ,editPointSetName.text)
                                    pointSetView.getList()
                                    editCtrlLandmark.visible = false

                                }
                            }
                        }
                        RowLayout
                        {
                            ListModel
                            {
                                id:pointSetList
                            }

                            TableView
                            {
                                id: pointSetView
                                Layout.minimumHeight: 50
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                clip: true
                                focus: false
                                headerVisible:true
                                selectionMode:SelectionMode.SingleSelection
                                model: pointSetList
                                width: ctrlPtLandmarks.width
                                property variant vals;
                                property variant descList;
                                property  int  index : -1



                                function getList()
                                {
                                    pointSetList.clear();
                                    vals = myContourDeformation.ui_getCtrlPtLandmarks(main.selectedNode)
                                    descList =  myContourDeformation.ui_getCplLandmarksDesc(main.selectedNode)
                                    console.log(" Description list for Cpl landmarks of "+main.selectedNode+" ; Length = "+descList.length)
                                    for(var i =0; i<descList.length; i++)
                                        console.log(descList[i])
                                    var desc
                                    if(vals.length<2){
                                        ctrlPtLandmarksDescriptionText.text = "Please add new control point landmarks"
                                    }else if(vals.length==2){
                                        ctrlPtLandmarksDescriptionText.text = "This is a linear body region "
                                    }else{
                                        ctrlPtLandmarksDescriptionText.text = "The "+vals.length+" control point landmarks are used to create a spline for the body region"
                                    }

                                    for(var i =0; i<vals.length; i++)
                                    {

                                        if (getMap.anatomyDBlandmarks.indexOf(vals[i]) != -1)
                                            desc = myContourDeformation.ui_getLandmarkDescriptionFromAnatomyDB(vals[i])
                                        else if(i< descList.length)
                                            desc = descList[i]
                                        if(desc == "")
                                             desc = "No description available"
                                        pointSetList.append({landmark: vals[i] , defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined"  : "Undefined"  , description : desc })
                                    }
                                }

                                function processClickedItem(row)
                                {
                                     index = row
                                     console.log(pointSetList.get(index).landmark)
                                }

                                TableViewColumn
                                {
                                    role: "landmark"
                                    title: "Landmarks"
                                }
                                TableViewColumn
                                {
                                    role: "defined"
                                    title: "Defined"
                                }
                                TableViewColumn
                                {
                                    id:pointSetViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width:200

                                }

                                onClicked:processClickedItem(row)

                                Component.onCompleted:
                                {

                                }
                            }
                        }

                    }
                }

                CollapsibleGroupBox{
                    Layout.fillWidth: true
                    id:splineInfo
                    title: "Spline information"
                    visible:false
                    ColumnLayout{

                        RowLayout{
                            visible: false
                            GroupBox{
                                Row{
                                    Button{
                                        text:"Display Landmarks"
                                    }

                                    Button{
                                        text:"Preview Contours"
                                    }
                                    CheckBox {
                                           text: qsTr("Show description")
                                           checked: false
                                           onCheckedChanged: {
                                               if(checked==true) {
                                                    splineInfoDescription.visible = true
                                                    splineInfoViewDescription.visible = true
                                                    splineInfoViewDescription.resizeToContents()
                                               }else{
                                                    splineInfoDescription.visible = false
                                                    splineInfoViewDescription.visible = false
                                               }

                                           }
                                    }
                                }
                            }
                            GroupBox{
                                id:splineInfoDescription
                                visible: false
                                title: "Description"
                                Text{
                                    id:splineInfoDescriptionText
                                    text: "p0 landmark defines the starting point of the spline\np1 landmark defines the terminating point of the spline\nt1 landmark is used to define the tangent at the starting point\nt2 landmark is used to define the tangent at the terminating point of the spline"
                                }
                            }

                        }

                       RowLayout{
                           GroupBox{
                               Row{
                                   Button{
                                       id:addSplineInfoButton
                                       text: "Add landmark "
                                       onClicked: {
                                             addNewSplineInfo.visible = true
                                       }
                                   }
                                   Button{
                                       visible: false
                                       text: "Edit "
                                       onClicked: {
                                              editSplineInfo.visible = true
                                       }
                                   }
                                   Button {
                                       text: "Remove "
                                       onClicked: {
                                           console.log("Removing "+splineInfoList.get(splineInfoView.currentRow).spline)
                                           myContourDeformation.ui_removeSplineInfo( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                           splineInfoView.getList()
                                       }
                                   }

                               }
                           }
                           GroupBox{
                               Row{
                                   Button{
                                       text:"Move Up"
                                       onClicked:{
                                           if(splineInfoView.currentRow != 0){
                                               console.log(splineInfoView.currentRow)
                                               myContourDeformation.ui_moveUpSplineLandmark( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                               splineInfoView.getList()
                                               splineInfoView.selection.deselect(splineInfoView.currentRow)
                                               splineInfoView.currentRow -= 1
                                               splineInfoView.selection.select(splineInfoView.currentRow)
                                           }
                                       }
                                   }
                                   Button{
                                       text:"Move Down"
                                       onClicked:{
                                            if(splineInfoView.currentRow != splineInfoView.rowCount-1){
                                                console.log(splineInfoView.currentRow)
                                                myContourDeformation.ui_moveDownSplineLandmark( main.selectedNode, splineInfoList.get(splineInfoView.currentRow).spline  )
                                                splineInfoView.getList()
                                                splineInfoView.selection.deselect(splineInfoView.currentRow)
                                                splineInfoView.currentRow += 1
                                                splineInfoView.selection.select(splineInfoView.currentRow)

                                            }
                                       }
                                   }
                               }
                           }
                           Button{
                               iconSource:  "qrc:///icon/information-icon.png"
                               onClicked: {
                                     messageDialog.text = splineInfoDescriptionText.text
                                     messageDialog.visible = true


                               }
                           }
                       }

                        GroupBox{
                            id : addNewSplineInfo
                            visible: false
                            ColumnLayout{
                                RowLayout{
                                   ExclusiveGroup { id: splineInfoGroup }
                                           RadioButton {
                                               text: "Add a pre-defined landmark"
                                               checked: true
                                               exclusiveGroup: splineInfoGroup
                                               onCheckedChanged:
                                               {
                                                    addNewPredefinedSplineLandmark.visible = true
                                                    addNewSplineLandmarkFromAnatomyDB.visible = false

                                               }
                                           }
                                           RadioButton {
                                               text: "Add an Anatomy DB landmark"
                                               exclusiveGroup: splineInfoGroup
                                               onClicked: {
                                                       addNewPredefinedSplineLandmark.visible =  false
                                                       addNewSplineLandmarkFromAnatomyDB.visible = true
                                                   }
                                           }
                                }

                                RowLayout
                                {
                                    visible: true
                                    id:addNewPredefinedSplineLandmark

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addSplineInfoName
                                        model: predefinedLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }
                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            if(addSplineInfoName.currentIndex != -1)
                                                myContourDeformation.ui_addSplineInfo(main.selectedNode,predefinedLandmarksList.get(addSplineInfoName.currentIndex).role )
                                            splineInfoView.getList()
                                            addNewSplineInfo.visible = false

                                        }
                                    }
                                }
                                RowLayout
                                {
                                    visible: false
                                    id : addNewSplineLandmarkFromAnatomyDB

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        visible: true
                                        id:addNewSplineFromAnatomyDBName
                                        model: anatomyDBLandmarksList
                                        property string landmark
                                        currentIndex: -1

                                    }
                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            if(addNewSplineFromAnatomyDBName.currentIndex != -1)
                                                myContourDeformation.ui_addSplineInfo(main.selectedNode,anatomyDBLandmarksList.get(addNewSplineFromAnatomyDBName.currentIndex).role)
                                            splineInfoView.getList()
                                            addNewSplineInfo.visible = false

                                        }
                                    }
                                }

                            }
                        }
                        RowLayout
                        {
                            ListModel
                            {
                                id:splineInfoList
                            }

                            TableView
                            {
                                id: splineInfoView
                                Layout.minimumHeight: 50
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                clip: true
                                focus: false
                                headerVisible:true
                                selectionMode:SelectionMode.SingleSelection
                                model: splineInfoList
                                property variant vals;
                                property  int  index



                                function getList()
                                {
                                    splineInfoList.clear();
                                    vals = myContourDeformation.ui_getSplineInfo(main.selectedNode);
                                    var type = ["p0","p1","t1","t2"]
                                    if(vals.length<4)
                                        addSplineInfoButton.enabled = true
                                    else
                                        addSplineInfoButton.enabled = false
                                    var desc = myContourDeformation.ui_getSplineDesc(main.selectedNode)
                                    for(var i =0; i<vals.length; i++)
                                    {
                                        if(desc[i] == "")
                                             desc = "No description available"
                                        splineInfoList.append({type:type[i], spline: vals[i] , defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined"  : "Undefined" , description :  desc[i]  })
                                    }
                                }

                                function processClickedItem(row)
                                {
                                     index = row
                                }

                                TableViewColumn
                                {
                                    id:type
                                    role: "type"
                                    title: " "
                                    resizable: false
                                    width: 20
                                }

                                TableViewColumn
                                {
                                    role: "spline"
                                    title: "Spline Info"
                                }
                                TableViewColumn
                                {
                                    role: "defined"
                                    title: "Defined"
                                }
                                TableViewColumn
                                {
                                    id:splineInfoViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width:200

                                }

                                onClicked:processClickedItem(row)

                                Component.onCompleted:
                                {

                                }
                            }
                        }

                    }
                }
                CollapsibleGroupBox{
                    Layout.fillWidth: true
                    id:splLandmark
                    visible:false
                    //title: "Special Landmarks"
                    title: "Axes landmarks "
                    ColumnLayout{

                        RowLayout{
                            visible: false
                            GroupBox{
                                Row{
                                    Button{
                                        text:"Display Landmarks"
                                    }

                                    Button{
                                        text:"Preview Contours"
                                    }
                                    CheckBox {
                                           text: qsTr("Show description")
                                           checked: false
                                           onCheckedChanged: {
                                                if(checked==true){
                                                    splLandmarkDescription.visible = true
                                                    splLandmarkViewDescription.visible = true
                                                    splLandmarkViewDescription.resizeToContents()
                                                }else{
                                                    splLandmarkDescription.visible = false
                                                    splLandmarkViewDescription.visible = false
                                                }
                                           }
                                    }
                                }
                            }
                            GroupBox{
                                id:splLandmarkDescription
                                visible: false
                                title: "Description"
                                Text{
                                    id:splLandmarkDescriptionText
                                    text: "These landmarks are used to define the flexion axis of the joint"
                                }
                            }
                        }

                        RowLayout{
                            GroupBox{
                                Row{
                                    Button{
                                        id:addSplLandmarkButton
                                        text: "Add landmark "

                                        onClicked: {
                                              addNewSplLandmark.visible = true

                                        }
                                    }

                                    Button {
                                        text: "Remove landmark"
                                        onClicked: {
                                            console.log("Removing "+splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark)
                                            //myContourDeformation.ui_removeSplLandmark( main.selectedNode, splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark  )
                                            myContourDeformation.ui_removeFlexionAxesLandmarks( main.selectedNode, splLandmarkList.get(splLandmarkListView.currentRow).spl_landmark  )
                                            splLandmarkListView.getList()
                                        }
                                    }


                                }
                            }

                            Button{
                                iconSource:  "qrc:///icon/information-icon.png"
                                onClicked: {
                                      messageDialog.text = splLandmarkDescriptionText.text
                                      messageDialog.visible = true


                                }
                            }
                        }



                        GroupBox{
                            id:addNewSplLandmark
                            visible: false
                            ColumnLayout{
                                RowLayout{
                                   ExclusiveGroup { id: splLndmrkGroup }
                                           RadioButton {
                                               text: "Add a pre-defined landmark"
                                               checked: true
                                               exclusiveGroup: splLndmrkGroup
                                               onCheckedChanged:
                                               {
                                                    addNewPredefinedSplLandmark.visible = true
                                                    addNewSplLandmarkFromAnatomyDB.visible = false

                                               }
                                           }
                                           RadioButton {
                                               text: "Add an Anatomy DB landmark"
                                               exclusiveGroup: splLndmrkGroup
                                               onClicked: {
                                                       addNewPredefinedSplLandmark.visible =  false
                                                       addNewSplLandmarkFromAnatomyDB.visible = true
                                                   }
                                           }
                                }

                                RowLayout
                                {
                                    visible: true
                                    id : addNewPredefinedSplLandmark

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addNewSplLandmarkName
                                        model: predefinedLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }


                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            var desc  = "No description available"
                                            if(addNewSplLandmarkName.currentIndex != -1 )
                                                myContourDeformation.ui_addFlexionAxesLandmark(main.selectedNode, predefinedLandmarksList.get(addNewSplLandmarkName.currentIndex).role, desc)
                                                //myContourDeformation.ui_addSplLandmark(main.selectedNode, predefinedLandmarksList.get(addNewSplLandmarkName.currentIndex).role, desc)
                                            splLandmarkListView.getList()
                                            addNewSplLandmark.visible = false

                                        }
                                    }
                                }

                                RowLayout
                                {
                                    visible: false
                                    id : addNewSplLandmarkFromAnatomyDB

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addNewSplLandmarkFromAnatomyDBName
                                        model: anatomyDBLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }


                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            var desc = myContourDeformation.ui_getLandmarkDescriptionFromAnatomyDB(anatomyDBLandmarksList.get(addNewSplLandmarkFromAnatomyDBName.currentIndex).role)
                                            if(addNewSplLandmarkFromAnatomyDBName.currentIndex != -1 )
                                                myContourDeformation.ui_addSplLandmark(main.selectedNode, anatomyDBLandmarksList.get(addNewSplLandmarkFromAnatomyDBName.currentIndex).role,desc )
                                            splLandmarkListView.getList()
                                            addNewSplLandmark.visible = false

                                        }
                                    }
                                }
                            }
                        }



                        RowLayout
                        {
                            ListModel
                            {
                                id:splLandmarkList
                            }

                            TableView
                            {
                                id: splLandmarkListView
                                Layout.minimumHeight: 50
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                clip: true
                                focus: false
                                headerVisible:true
                                selectionMode:SelectionMode.SingleSelection
                                model: splLandmarkList
                                property variant vals;
                                property variant descList
                                property  int  index

                                function getList()
                                {
                                    splLandmarkList.clear();
                                    vals = myContourDeformation.ui_getSplLandmarks(main.selectedNode);
                                    //vals = myContourDeformation.ui_getFlexionAxesLandmarks(main.selectedNode);
                                    console.log("Flex axes landmarks : "+vals)
                                    //descList = myContourDeformation.ui_getSplLandmarksDesc(main.selectedNode)
                                    descList = myContourDeformation.ui_getFlexionAxesLandmarksDesc(main.selectedNode)
                                    for(var i =0; i<vals.length; i++)
                                    {
                                        var desc = descList[i]
                                        if(desc == "")
                                            desc = "No description available "
                                        splLandmarkList.append({spl_landmark: vals[i] , defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined"  : "Undefined" , description :  desc  })
                                    }
                                }

                                function processClickedItem(row)
                                {
                                     index = row
                                }

                                TableViewColumn
                                {
                                    role: "spl_landmark"
                                    //title: "SpecialLandmarks"
                                    title:"Flexion axes landmarks"
                                }
                                TableViewColumn
                                {
                                    role: "defined"
                                    title: "Defined"
                                }
                                TableViewColumn
                                {
                                    id:splLandmarkViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width:200

                                }

                                onClicked:processClickedItem(row)

                                Component.onCompleted:
                                {

                                }
                            }
                        }

                    }
                }

                CollapsibleGroupBox{
                    Layout.fillWidth: true
                    width: parent.width
                    id:circumference
                    title: "Circumference Location Landmark"
                    visible:false

                    property string type : "bodyRegion"



                    function getJointCircumference(){
                        circumValue.text = myContourDeformation.ui_getJointCircumference(main.selectedNode)
                        circumValue.color = "black"
                        if(circumValue.text==""){
                            circumValue.text = " NO VALUE DEFINED "
                            circumValue.color = "red"
                        }
                        circumference.type= "joint"

                    }

                    ColumnLayout{

                        RowLayout{
                            visible: false
                            GroupBox{
                                Row{
                                    Button{
                                        text:"Display Landmarks"
                                    }

                                    Button{
                                        text:"Preview Contours"
                                    }
                                    CheckBox {
                                           text: qsTr("Show description")
                                           checked: false
                                           onCheckedChanged: {
                                                if(checked==true){
                                                    circumDescription.visible = true
                                                    circumInfoViewDescription.visible = true
                                                    circumInfoViewDescription.resizeToContents()
                                                }else{
                                                    circumDescription.visible = false
                                                    circumInfoViewDescription.visible = false
                                                }
                                           }
                                    }
                                }
                            }
                            GroupBox{
                                id:circumDescription
                                visible: false
                                title: "Description"
                                Text{
                                    id:circumDescriptionText
                                    text: "Circumference landmarks are used to define the location of the circumference\nof a body region or a joint"
                                }
                            }
                        }

                        RowLayout{
                            GroupBox{
                                Row{
                                    Button{
                                        text: "Add a landmark "
                                        onClicked: {
                                              addNewCirInfo.visible = true
                                        }
                                    }

                                    Button {
                                        text: "Remove "
                                        onClicked: {
                                            console.log("Removing "+circumInfoList.get(circumInfoView.currentRow).circumference)
                                            if(circumference.type=="bodyRegion")
                                                myContourDeformation.ui_removeBRCircumference( main.selectedNode, circumInfoList.get(circumInfoView.currentRow).spl_landmark  )
                                            else
                                                myContourDeformation.ui_removeJointCircumference( main.selectedNode, circumInfoList.get(circumInfoView.currentRow).spl_landmark  )
                                            circumInfoView.getList()
                                        }
                                    }
                                }
                            }
                            Button{
                                iconSource:  "qrc:///icon/information-icon.png"
                                onClicked: {
                                      messageDialog.text = circumDescriptionText.text
                                      messageDialog.visible = true


                                }
                            }
                        }



                        GroupBox{
                            id:addNewCirInfo
                            visible: false
                            ColumnLayout{
                                RowLayout{
                                   ExclusiveGroup { id: cirInfoGroup }
                                           RadioButton {
                                               text: "Add a pre-defined landmark"
                                               checked: true
                                               exclusiveGroup: cirInfoGroup
                                               onCheckedChanged:
                                               {
                                                    addNewCircumLandmark.visible = true
                                                    addNewCircumLandmarkFromAnatomyDB.visible = false

                                               }
                                           }
                                           RadioButton {
                                               text: "Add an Anatomy DB landmark"
                                               exclusiveGroup: cirInfoGroup
                                               onClicked: {
                                                       addNewCircumLandmark.visible =  false
                                                       addNewCircumLandmarkFromAnatomyDB.visible = true
                                                   }
                                           }
                                }

                                RowLayout
                                {
                                    visible: true
                                    id : addNewCircumLandmark

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addNewCircumLandmarkName
                                        model: predefinedLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }


                                    Button{
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            if(addNewCircumLandmarkName.currentIndex != -1 )
                                                if(circumference.type=="bodyRegion")
                                                    myContourDeformation.ui_addBRCircumference(main.selectedNode, predefinedLandmarksList.get(addNewCircumLandmarkName.currentIndex).role)
                                                else
                                                    myContourDeformation.ui_addJointCircumference(main.selectedNode, predefinedLandmarksList.get(addNewCircumLandmarkName.currentIndex).role)
                                            circumInfoView.getList()
                                            addNewCirInfo.visible = false
                                        }
                                    }
                                }

                                RowLayout{
                                    visible: false
                                    id : addNewCircumLandmarkFromAnatomyDB

                                    Label{
                                        text: " Select a landmark : "
                                    }
                                    ComboBox{
                                        id:addNewCircumLandmarkFromAnatomyDBName
                                        model: anatomyDBLandmarksList
                                        property string landmark
                                        currentIndex: -1
                                        onCurrentIndexChanged: {

                                        }
                                    }


                                    Button
                                    {
                                        text: "Add"
                                        tooltip: "Add"
                                        onClicked:
                                        {
                                            if(addNewCircumLandmarkFromAnatomyDBName.currentIndex != -1 )
                                                if(circumference.type=="bodyRegion")
                                                    myContourDeformation.ui_addBRCircumference(main.selectedNode, anatomyDBLandmarksList.get(addNewCircumLandmarkFromAnatomyDBName.currentIndex).role)
                                                else
                                                    myContourDeformation.ui_addJointCircumference(main.selectedNode, anatomyDBLandmarksList.get(addNewCircumLandmarkFromAnatomyDBName.currentIndex).role)
                                            circumInfoView.getList()
                                            addNewCirInfo.visible = false

                                        }
                                    }
                                }
                            }
                        }

                        RowLayout
                        {
                            width: parent.width
                            ListModel
                            {
                                id:circumInfoList
                            }

                            TableView
                            {
                                id: circumInfoView
                                Layout.minimumHeight: 50
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                width: parent.width
                                clip: true
                                focus: false
                                headerVisible:true
                                selectionMode:SelectionMode.SingleSelection
                                model: circumInfoList
                                property variant vals
                                property variant descList
                                property  int  index



                                function getList()
                                {
                                    circumInfoList.clear();
                                    if(circumference.type=="bodyRegion"){
                                        vals =  myContourDeformation.ui_getBRCircumference(main.selectedNode)
                                        descList = myContourDeformation.ui_getBRCircumDesc(main.selectedNode)
                                    }
                                    else{
                                        vals =  myContourDeformation.ui_getJointCircumference(main.selectedNode)
                                        descList = myContourDeformation.ui_getJointCircumDesc(main.selectedNode)
                                    }

                                    for(var i =0; i<vals.length; i++)
                                    {
                                        var desc
                                        if (getMap.anatomyDBlandmarks.indexOf(vals[i]) != -1)
                                            desc = myContourDeformation.ui_getLandmarkDescriptionFromAnatomyDB(vals[i])
                                        else if(i< descList.length)
                                            desc = descList[i]
                                        if(desc == "")
                                             desc = "No description available"
                                        circumInfoList.append({circumference: vals[i] , defined: (getMap.vals.indexOf(vals[i]) != -1) ? "Defined"  : "Undefined" , description : desc  })
                                    }
                                }

                                function processClickedItem(row)
                                {
                                     index = row
                                }

                                TableViewColumn
                                {
                                    role: "circumference"
                                    title: "Circumference Location Landmarks"
                                }
                                TableViewColumn
                                {
                                    role: "defined"
                                    title: "Defined"
                                }
                                TableViewColumn
                                {
                                    id:circumInfoViewDescription
                                    visible: true
                                    role: "description"
                                    title: "Description"
                                    width:200

                                }

                                onClicked:processClickedItem(row)

                                Component.onCompleted:
                                {

                                }
                            }
                        }





                    }




                }


           }



     }
}




