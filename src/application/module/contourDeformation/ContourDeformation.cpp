/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ContourDeformation.h"
#include "lib/hbm/VtkSelectionTools.h"
#include "common/helper.h"

#define LEFT_KNEE_FLEXION_MIN 
#define LEFT_KNEE_FLEXION_MAX
#define RIGHT_KNEE_FLEXION_MIN
#define RIGHT_KNEE_FLEXION_MAX
#define LEFT_FOOT_FLEXION_MIN
#define LEFT_FOOT_FLEXION_MAX
#define RIGHT_FOOT_FLEXION_MIN
#define RIGHT_FOOT_FLEXION_MAX
#define LEFT_HIP_FLEXION_MIN
#define LEFT_HIP_FLEXION_MAX
#define RIGHT_HIP_FLEXION_MIN
#define RIGHT_HIP_FLEXION_MAX
#define LEFT_ELBOW_FLEXION_MIN
#define LEFT_ELBOW_FLEXION_MAX
#define RIGHT_ELBOW_FLEXION_MIN
#define RIGHT_ELBOW_FLEXION_MAX

using namespace std;
using namespace piper::contours;
using namespace piper::hbm;
using namespace Eigen;

namespace piper {
	namespace contourdeformation {

#pragma region Display
		ContourDeformation::ContourDeformation() :
            main_repo(QCoreApplication::applicationDirPath().toStdString())
        {
			tmpPath = tempDirectoryPath("contourdeform", true);
		}

		ContourDeformation::~ContourDeformation() { }

		void ContourDeformation::displayModel(){
			modelDisplay(3);

            Context::instance().display().SetObjectOpacity("full_model", 0);
		}
#pragma endregion

#pragma region UICalls

		/// <summary>
		/// Switch between Entity/Full Model Display
		/// </summary>
		/// <param name="type">The type of model wich has to be shown. 0: FULL_MESH, 1: ENTITIES</param>
		void ContourDeformation::modelDisplay(int type){
			switch (type){
			case 0:
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::FULL_MESH);
				break;
			case 1:
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::ENTITIES);
				break;
			case 2:
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::LANDMARKS);
				break;
			case 3:
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::ALL);
				break;
			default:
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::FULL_MESH);
				break;
			}
			refreshDisplay();
		}

		void ContourDeformation::displayModel(int value){
			if (value == 0){
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::ALL);
			}
			else{
				Context::instance().display().SetDisplayMode(DISPLAY_MODE::ENTITIES);
			}
			refreshDisplay();
		}

		void ContourDeformation::setOpacity(float value){
			Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::ENTITIES, value);
			Context::instance().display().Refresh();
		}

		void ContourDeformation::setCLOpacity(float value){
			Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::GEBOD, value);
			Context::instance().display().Refresh();
		}

		void ContourDeformation::deformContoursBusyIndicator()
		{
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::ui_deformContours, this), false, false, false, false, false);
		}

		void ContourDeformation::ui_deformContours(){
			pInfo() << START << "Contour Deformation starts";
			deformContours();
			pskeleton->UpdateContoursListForBR();
			//refreshDisplay();
		}

		void ContourDeformation::reposBusyIndicator()
		{
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::ui_perfomDelaunnay, this), false, false, false, false, false);
		}

		void ContourDeformation::ui_perfomDelaunnay(){

			performDelaunnay();
			refreshDisplay();
			DisplaySkeleton();

			/*			if (!Context::instance().project().model().empty()) {
			m_meta.cont_det.contour_map = contour_map;
			m_meta.m_hbm_contours.sort_all();
			//Draw All Contours
			}*/
		}

		void ContourDeformation::ui_applyState(){
			applyState();
		}

		void ContourDeformation::ui_setModifiedState(int n){
			if (n == 1){
				MODIFIED_STATE = true;
			}
			else{
				MODIFIED_STATE = false;
			}
			refreshDisplay();
		}

		void ContourDeformation::ui_setReposFunc(int n){
			reposFunc = n;
		}
		void ContourDeformation::ui_setReposType(int n){
			reposType = n;
		}
		void ContourDeformation::ui_setReposSide(int n){
			reposSide = n;
		}
		void ContourDeformation::ui_setReposAngle(double n){
			reposAngle = n;
		}
		double ContourDeformation::ui_getReposAngle(){
			return reposAngle;
		}
		void ContourDeformation::ui_setReposMap(bool mapping){
			MAPPING_REPOSITIONING = mapping;
		}
		void ContourDeformation::ui_setPersMap(bool mapping){
			MAPPING_PERSONALIZATION = mapping;
		}

		void ContourDeformation::personalizeBusyIndicator(const QUrl& bodyData, double a1, double a2, double a3, double a4, double a5, double a6, double
			a7, double a8, double a9, double a10, double a11, double a12, double
			a13, double a14, double a15, double a16, double a17, double a18, double

			b1, double b2, double b3, double b4, double b5, double b6, double
			b7, double b8, double b9, double b10, double b11, double b12, double
			b13, double b14, double b15, double b16, double b17){

			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::ui_personalize, this, bodyData, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12,
				a13, a14, a15, a16, a17, a18, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17), false, false, false, false, false);

		}

		void ContourDeformation::ui_personalize(const QUrl& bodyData, double a1, double a2, double a3, double a4, double a5, double a6,
			double a7, double a8, double a9, double a10, double a11, double a12,
			double a13, double a14, double a15, double a16, double a17, double a18,

			double b1, double b2, double b3, double b4, double b5, double b6,
			double b7, double b8, double b9, double b10, double b11, double b12,
			double b13, double b14, double b15, double b16, double b17)
		{
			pInfo() << START << QStringLiteral("Personalization started !");

			std::vector<double> vals;

			std::string bodyDataFile = bodyData.toLocalFile().toStdString();

			vals.push_back(a1); vals.push_back(a2); vals.push_back(a3); vals.push_back(a4); vals.push_back(a5); vals.push_back(a6);
			vals.push_back(a7); vals.push_back(a8); vals.push_back(a9); vals.push_back(a10); vals.push_back(a11); vals.push_back(a12);
			vals.push_back(a13); vals.push_back(a14); vals.push_back(a15); vals.push_back(a16); vals.push_back(a17); vals.push_back(a18);

			vals.push_back(b1); vals.push_back(b2); vals.push_back(b3); vals.push_back(b4); vals.push_back(b5); vals.push_back(b6);
			vals.push_back(b7); vals.push_back(b8); vals.push_back(b9); vals.push_back(b10); vals.push_back(b11); vals.push_back(b12);
			vals.push_back(b13); vals.push_back(b14); vals.push_back(b15); vals.push_back(b16); vals.push_back(b17);

			personalize(vals, bodyDataFile);

			refreshDisplay();

			pInfo() << DONE << QStringLiteral("Personalization complete !") << endl;
		}

		bool ContourDeformation::ui_skeletonDisplay(int visible){
			if (visible == 1){
				pInfo() << INFO << QStringLiteral("Show Skeleton") << endl;
				return(DisplaySkeleton());
			}
			else{
				pInfo() << INFO << QStringLiteral("Hide Skeleton") << endl;
				HideSkeleton();
			}
			return false;
		}

		QString ContourDeformation::getSeletedContourCLElement(){
			std::vector < std::string > string = Context::instance().display().GetSelectedActorNames();
			std::cout << "\nSelected actors : ";
			for (unsigned int i = 0; i < string.size(); i++)
				std::cout << "\t" << string.at(i);
			return QString::fromStdString(string.at(0));

		}

		void ContourDeformation::generateContBusyIndicator(int visible)
		{
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::ui_contourDisplay, this, visible), false, false, false, false, false);
		}

		void ContourDeformation::ui_contourDisplay(int visible){

			if (visible == 1){
				pskeleton = ContourGen::getInstance();
				pskeleton->ComputeContours();
			}
			else{
				pskeleton = ContourGen::getInstance();
				pskeleton->HideContoursListForBR();
			}
		}

#pragma endregion

		void ContourDeformation::openContour(std::string const& contourFile){
			QUrl m_contourFile;
			m_contourFile = QUrl::fromLocalFile(QString::fromStdString(contourFile));

			m_meta.m_hbm_contours = main_repo.readContour(contourFile, &m_meta.cont_det.contour_map, &vec_spline_new);

			if (!Context::instance().project().model().empty()) {
				m_meta.m_hbm_contours.sort_all();

				//Draw All Contours		
				//				m_meta.m_hbm_contours.sort_all();
				//			asdfg << m_meta.cont_det.contour_map.size() << std::endl;
				for (int body_region = 0; body_region < maxBodyRegions; body_region++){
					int i = 0;
					std::vector<piper::hbm::Contour*> bodyRegion = getBodyRegion(m_meta, body_region);
					std::vector<vtkSmartPointer<vtkActor>> contourSet;
					for (std::vector<piper::hbm::Contour*>::iterator it = bodyRegion.begin(); it != bodyRegion.end(); ++it){
						int j = 0;
						int contouroriginstr = bodyRegion.at(i)->contid;
						if (bodyRegion.at(i)->contour_points.size()){

							// Create a vtkPoints object and store the points in it
							vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

							for (std::vector<piper::hbm::NodePtr>::iterator itt = bodyRegion.at(i)->contour_points.begin(); itt != bodyRegion.at(i)->contour_points.end(); ++itt){
								double pt[3] = { bodyRegion.at(i)->contour_points.at(j)->getCoordX(), bodyRegion.at(i)->contour_points.at(j)->getCoordY(), bodyRegion.at(i)->contour_points.at(j)->getCoordZ() };
								points->InsertNextPoint(pt);
								j++;
							}

							//Join Last Point to First to close the Contour
							double pt[3] = { bodyRegion.at(i)->contour_points.at(0)->getCoordX(), bodyRegion.at(i)->contour_points.at(0)->getCoordY(), bodyRegion.at(i)->contour_points.at(0)->getCoordZ() };
							points->InsertNextPoint(pt);

							std::stringstream name;
							name << "contour_BR_" << body_region << "_contourNo_" << i; // I don't know if this name is actually unique, please check /TJ
							auto actor = draw.polyLine(points);
							Context::instance().display().AddActor(actor, name.str(), false, DISPLAY_MODE::ENTITIES); // maybe some other display mode?
							contourSet.push_back(actor);
						}
						i++;
					}
				}
				refreshDisplay();
			}
		}

		/// <summary>
		/// Switch case to access a specific Contour Body Region vector from a given integer
		/// </summary>
		std::vector<piper::hbm::Contour*> ContourDeformation::getBodyRegion(piper::hbm::Metadata& m_meta, int body_region){
			std::vector<piper::hbm::Contour*> bodyRegion;
			if (!Context::instance().project().model().empty()) {
				switch (body_region){
				case 0:		bodyRegion = m_meta.m_hbm_contours.Right_Foot;	break;
				case 1:		bodyRegion = m_meta.m_hbm_contours.Left_Foot;	break;
				case 2:		bodyRegion = m_meta.m_hbm_contours.Right_Ankle_to_Calf;	break;
				case 3:		bodyRegion = m_meta.m_hbm_contours.Left_Ankle_to_Calf;	break;
				case 4:		bodyRegion = m_meta.m_hbm_contours.Right_Knee_lower;	break;
				case 5:		bodyRegion = m_meta.m_hbm_contours.Left_Knee_lower;	break;
				case 6:		bodyRegion = m_meta.m_hbm_contours.Right_Knee_upper;	break;
				case 7:		bodyRegion = m_meta.m_hbm_contours.Left_Knee_upper;	break;
				case 8:		bodyRegion = m_meta.m_hbm_contours.Right_Thigh;	break;
				case 9:		bodyRegion = m_meta.m_hbm_contours.Left_Thigh;	break;
				case 10:	bodyRegion = m_meta.m_hbm_contours.Hip;	break;
				case 11:	bodyRegion = m_meta.m_hbm_contours.Abdomen;	break;
				case 12:	bodyRegion = m_meta.m_hbm_contours.Thorax;	break;
				case 13:	bodyRegion = m_meta.m_hbm_contours.Neck;	break;
				case 14:	bodyRegion = m_meta.m_hbm_contours.Head;	break;
				case 15:	bodyRegion = m_meta.m_hbm_contours.Right_Palm;	break;
				case 16:	bodyRegion = m_meta.m_hbm_contours.Left_Palm;	break;
				case 17:	bodyRegion = m_meta.m_hbm_contours.Right_Forearm;	break;
				case 18:	bodyRegion = m_meta.m_hbm_contours.Left_Forearm;	break;
				case 19:	bodyRegion = m_meta.m_hbm_contours.Right_Elbow_lower;	break;
				case 20:	bodyRegion = m_meta.m_hbm_contours.Left_Elbow_lower;	break;
				case 21:	bodyRegion = m_meta.m_hbm_contours.Right_Elbow_upper;	break;
				case 22:	bodyRegion = m_meta.m_hbm_contours.Left_Elbow_upper;	break;
				case 23:	bodyRegion = m_meta.m_hbm_contours.Right_Biceps;	break;
				case 24:	bodyRegion = m_meta.m_hbm_contours.Left_Biceps;	break;
				case 25:	bodyRegion = m_meta.m_hbm_contours.Right_biceps_to_shoulder;	break;
				case 26:	bodyRegion = m_meta.m_hbm_contours.Left_biceps_to_shoulder;	break;
				case 27:	bodyRegion = m_meta.m_hbm_contours.Hip_right;	break;
				case 28:	bodyRegion = m_meta.m_hbm_contours.Hip_left;	break;
				case 29:	bodyRegion = m_meta.m_hbm_contours.Keypoints;	break;
				case 30:	bodyRegion = m_meta.m_hbm_contours.Right_Abdomen;	break;
				case 31:	bodyRegion = m_meta.m_hbm_contours.Left_Abdomen;	break;
				case 32:	bodyRegion = m_meta.m_hbm_contours.Right_Thorax;	break;
				case 33:	bodyRegion = m_meta.m_hbm_contours.Left_Thorax;	break;
				}
			}
			return bodyRegion;
		}

		void ContourDeformation::saveContour(std::string const& contourFile){}

		void ContourDeformation::readContourFile(string const& contourFile){
			QUrl m_contourFile; ///< current contour file
			pInfo() << START << "Open contour: " << m_contourFile.path();
			m_contourFile = QUrl::fromLocalFile(QString::fromStdString(contourFile));
			Metadata& m_meta = Context::instance().project().model().metadata();

			m_meta.m_hbm_contours = main_repo.readContour(contourFile, &m_meta.cont_det.contour_map, &vec_spline_new);
			std::ofstream ght("ght.txt");
			ght << "cont map size" << m_meta.cont_det.contour_map.size() << std::endl;
			pInfo() << QStringLiteral("Checking contour...read from contours lib") << endl;
			if (!Context::instance().project().model().empty()) {
				m_meta.cont_det.contour_map = contour_map;
				m_meta.m_hbm_contours.sort_all();
				pInfo() << DONE;
				pInfo() << INFO << QStringLiteral("Contour reading complete !") << endl;
			}
		}

		QVariantMap ContourDeformation::ui_getContourCLTree(){
			Metadata& m_meta = Context::instance().project().model().metadata();
			//m_meta.ctrl_line.printSkele();
			QVariantMap map;
			QList<QString> list, childBr;
			ContourCLbr* br;
			if (m_meta.ctrl_line.root)
				br = m_meta.ctrl_line.root;
			else
				return map;
			stack<ContourCLbr*> bodyRegions;
			map.insert("Root", QString::fromStdString(br->name));
			bodyRegions.push(br);
			br = bodyRegions.top();
			bodyRegions.pop();
			list.clear();
			for (unsigned int i = 0; i < br->childrenjts.size(); i++){
				list.append(QString::fromStdString(br->childrenjts.at(i)->name));
				childBr.clear();
				childBr.append(QString::fromStdString(br->childrenjts.at(i)->childbodyregion->name));
				bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
				map.insert(QString::fromStdString(br->childrenjts.at(i)->name), QVariant(childBr));
			}
			map.insert(QString::fromStdString(br->name), QVariant(list));

			while (!bodyRegions.empty()){
				br = bodyRegions.top();
				bodyRegions.pop();
				//std::cout << "\nBody reigion : \""+br->name+"\" ";
				list.clear();
				for (unsigned int i = 0; i < br->childrenjts.size(); i++){
					list.append(QString::fromStdString(br->childrenjts.at(i)->name));
					//std::cout << "\t\"" + br->childrenjts.at(i)->name + "\"";
					childBr.clear();
					if (br->childrenjts.at(i)->childbodyregion){
						childBr.append(QString::fromStdString(br->childrenjts.at(i)->childbodyregion->name));
						bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
						map.insert(QString::fromStdString(br->childrenjts.at(i)->name), QVariant(childBr));
					}
				}
				map.insert(QString::fromStdString(br->name), QVariant(list));
			}

			return map;
		}

		ContourCLbr* ContourDeformation::getBodyRegion(string name){
			//std::cout << "\n getBodyRegion called for " + name;
			Metadata& m_meta = Context::instance().project().model().metadata();
			ContourCLbr* br = m_meta.ctrl_line.root;
			stack<ContourCLbr*> bodyRegions;
			bodyRegions.push(br);
			while (!bodyRegions.empty()){
				br = bodyRegions.top();
				bodyRegions.pop();
				if (br->name == name)
					return br;
				for (unsigned int i = 0; i < br->childrenjts.size(); i++)
					if (br->childrenjts.at(i)->childbodyregion)
						bodyRegions.push(br->childrenjts.at(i)->childbodyregion);

			}
			return NULL;
		}

		ContourCLj* ContourDeformation::getJoint(string name){
			//std::cout << "\n getJoint called for " + name;
			Metadata& m_meta = Context::instance().project().model().metadata();
			ContourCLbr* br = m_meta.ctrl_line.root;
			stack<ContourCLbr*> bodyRegions;
			bodyRegions.push(br);
			while (!bodyRegions.empty()){
				br = bodyRegions.top();
				bodyRegions.pop();
				for (unsigned int i = 0; i < br->childrenjts.size(); i++){
					if (br->childrenjts.at(i)->name == name)
						return br->childrenjts.at(i);
					if (br->childrenjts.at(i)->childbodyregion)
						bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
				}
			}
			//std::cout << "\t ****Returning NULL";
			return NULL;
		}

		void ContourDeformation::computeJtAngles(std::string name){
			ContourCLj* tempj = getJoint(name);
			if (tempj != NULL){
				std::vector<std::string> landmarkvec;
				if (tempj->flexionaxeslandmarks.size() == 2 && m_meta.hasLandmark(tempj->flexionaxeslandmarks.at(0)) && m_meta.hasLandmark(tempj->flexionaxeslandmarks.at(1)) && m_meta.hasLandmark(tempj->joint_splinet1) && m_meta.hasLandmark(tempj->joint_splinet2)){
					landmarkvec.push_back(tempj->flexionaxeslandmarks.at(0));
					landmarkvec.push_back(tempj->flexionaxeslandmarks.at(1));
					double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, tempj->joint_splinet1, tempj->joint_splinet2);
					tempj->jointangle = angle;
				}
			}
		}

		void ContourDeformation::computeBrAngles(std::string name){
			ContourCLbr* tempbr = getBodyRegion(name);
			if (tempbr != NULL){
				if (tempbr->dof1.size() == 2 && m_meta.hasLandmark(tempbr->dof1.at(0)) && m_meta.hasLandmark(tempbr->dof1.at(1)) && m_meta.hasLandmark(tempbr->reference_landmark.at(0)) && m_meta.hasLandmark(tempbr->reference_landmark.at(1))){
					std::vector<std::string> landmarkvec;
					landmarkvec.push_back(tempbr->dof1.at(0));
					landmarkvec.push_back(tempbr->dof1.at(1));
					double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, tempbr->reference_landmark.at(0), tempbr->reference_landmark.at(1));
					tempbr->brangle = angle;
				}
			}
		}

		bool ContourDeformation::ui_getSignOfAngle(QString parent){
			return signOfAngle(parent.toStdString());
		}

		bool ContourDeformation::signOfAngle(std::string name){
			if (name == "Neck"){
				computeBrAngles(name);
				ContourCLbr* tempbr = getBodyRegion(name);
				hbm::LandmarkTarget l_target = m_tsynth.target_lanmk;
				std::vector<std::string> landmarkvec;
				landmarkvec.push_back(tempbr->dof1.at(0));
				landmarkvec.push_back(tempbr->dof1.at(1));
				double absolutefinalangle = comptargets.computeTargetAngle(m_meta, fem1, landmarkvec, l_target, tempbr->reference_landmark.at(0));
				double diff = absolutefinalangle - tempbr->brangle;
				if (diff < 0)
					return false;
				else{
					return true;
				}
			}
			else{
				computeJtAngles(name);

				ContourCLj* pjoint = m_tsynth.joint_pointer;
				hbm::LandmarkTarget l_target = m_tsynth.target_lanmk;

				std::vector<std::string> landmarkvec;
				landmarkvec.push_back(pjoint->flexionaxeslandmarks.at(0));
				landmarkvec.push_back(pjoint->flexionaxeslandmarks.at(1));

				double absolutefinalangle = comptargets.computeTargetAngle(m_meta, fem1, landmarkvec, l_target, pjoint->joint_splinet1);
				double diff = absolutefinalangle - pjoint->jointangle;
				if (diff < 0)
					return false;
				else{
					return true;
				}

			}
		}

		QList<QString> ContourDeformation::ui_getCtrlPtLandmarks(QString name){
			//std::cout << "\n ui_getCtrlPtLandmarks called for "+name.toStdString();
			QList<QString> list;
			ContourCLbr* br = getBodyRegion(name.toStdString());
			//std:cout << "\n" << br->name <<" body region selected";
			for (unsigned int i = 0; i < br->cplandmarks.size(); i++)
				list.append(QString::fromStdString(br->cplandmarks.at(i)));
			return list;
		}

		void ContourDeformation::ui_addCtrlPtLandmark(QString bodyRegion, QString name, QString desc){
			//std::cout << "\n ui_addCtrlPtLandmark called for " + bodyRegion.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			//std:cout << "\n" << br->name << " body region selected";
			br->cplandmarks.push_back(name.toStdString());
			br->cplandmarks_info.push_back(desc.toStdString());
		}

		void ContourDeformation::ui_editCtrlPtLandmark(QString bodyRegion, QString name, QString newName){
			//std::cout << "\n ui_editCtrlPtLandmark called for " + bodyRegion.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			//std:cout << "\n" << br->name << " body region selected";
			std::replace(br->cplandmarks.begin(), br->cplandmarks.end(), name.toStdString(), newName.toStdString());
		}

		void ContourDeformation::ui_removeCtrlPtLandmark(QString bodyRegion, QString name){
			//std::cout << "\n ui_removeCtrlPtLandmark called for " + bodyRegion.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			string landmark = name.toStdString();
			string desc;
			//std:cout << "\n" << br->name << " body region selected";
			for (unsigned int i = 0; i < br->cplandmarks.size(); i++)
				if (br->cplandmarks.at(i) == landmark)
					desc = br->cplandmarks_info.at(i);
			br->cplandmarks.erase(std::remove(br->cplandmarks.begin(), br->cplandmarks.end(), name.toStdString()), br->cplandmarks.end());
			br->cplandmarks_info.erase(std::remove(br->cplandmarks_info.begin(), br->cplandmarks_info.end(), desc), br->cplandmarks_info.end());
		}

		void ContourDeformation::ui_moveUpCtrlLandmark(QString bodyRegion, QString name){
			//std::cout << "\n ui_moveUpCtrlLandmark called for " + bodyRegion.toStdString();
			string landmark = name.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			std::vector<std::string> cplandmarks = br->cplandmarks;
			for (unsigned int i = 1; i < br->cplandmarks.size(); i++){
				if (br->cplandmarks.at(i) == landmark){
					br->cplandmarks.at(i) = br->cplandmarks.at(i - 1);
					br->cplandmarks.at(i - 1) = landmark;
					std::string info_temp = br->cplandmarks_info.at(i - 1);
					br->cplandmarks_info.at(i - 1) = br->cplandmarks_info.at(i);
					br->cplandmarks_info.at(i) = info_temp;
					return;
				}
			}
		}

		void ContourDeformation::ui_moveDownCtrlLandmark(QString bodyRegion, QString name){
			//std::cout << "\n ui_moveDownCtrlLandmark called for " + bodyRegion.toStdString();
			string landmark = name.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			std::vector<std::string> cplandmarks = br->cplandmarks;
			for (unsigned int i = 0; i < br->cplandmarks.size() - 1; i++){
				if (br->cplandmarks.at(i) == landmark){
					br->cplandmarks.at(i) = br->cplandmarks.at(i + 1);
					br->cplandmarks.at(i + 1) = landmark;
					std::string info_temp = br->cplandmarks_info.at(i + 1);
					br->cplandmarks_info.at(i + 1) = br->cplandmarks_info.at(i);
					br->cplandmarks_info.at(i) = info_temp;
					return;
				}
			}
		}

		QList<QString> ContourDeformation::ui_getBRCircumference(QString name){
			//std::cout << "\n ui_getBRCircumference called for " + name.toStdString();
			ContourCLbr* br = getBodyRegion(name.toStdString());
			QList<QString> list;
			//std:cout << "\n" << br->name << " body region selected";
			for (unsigned int i = 0; i < br->circumfvec.size(); i++)
				list.append(QString::fromStdString(br->circumfvec.at(i)));
			return list;

		}
		void ContourDeformation::ui_addBRCircumference(QString br, QString newValue){
			//std::cout << "\n ui_setBRCircumference called for " + br.toStdString();
			ContourCLbr* b = getBodyRegion(br.toStdString());
			//std:cout << "\n" << b->name << " body region selected\n";
			b->circumfvec.push_back(newValue.toStdString());
		}
		void ContourDeformation::ui_removeBRCircumference(QString bodyRegion, QString name){
			//std::cout << "\n ui_removeCtrlPtLandmark called for " + bodyRegion.toStdString();
			ContourCLbr* br = getBodyRegion(bodyRegion.toStdString());
			for (unsigned int i = 0; i < br->circumfvec_info.size(); i++){
				if (br->circumfvec.at(i) == name.toStdString())
					br->circumfvec_info.at(i) = "";
			}
			br->circumfvec.erase(std::remove(br->circumfvec.begin(), br->circumfvec.end(), name.toStdString()), br->circumfvec.end());
		}

		QList<QString> ContourDeformation::ui_getJointCircumference(QString name){
			//std::cout << "\n\n ui_getSplLandmarks called for " + name.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(name.toStdString());
			for (unsigned int i = 0; i < joint->joint_circum_vec.size(); i++)
				list.append(QString::fromStdString(joint->joint_circum_vec.at(i)));
			return list;
		}

		void ContourDeformation::ui_addJointCircumference(QString name, QString newValue){
			ContourCLj* joint = getJoint(name.toStdString());
			joint->joint_circum_vec.push_back(newValue.toStdString());
			joint->joint_circum_vec_info.push_back("");
		}

		void ContourDeformation::ui_removeJointCircumference(QString j, QString name){
			ContourCLj* joint = getJoint(j.toStdString());
			for (unsigned int i = 0; i < joint->joint_circum_vec.size(); i++){
				if (joint->joint_circum_vec.at(i) == name.toStdString())
					joint->joint_circum_vec_info.at(i) = "";
			}
			joint->joint_circum_vec.erase(std::remove(joint->joint_circum_vec.begin(), joint->joint_circum_vec.end(), name.toStdString()), joint->joint_circum_vec.end());
		}

		QList<QString> ContourDeformation::ui_getFlexionAxesLandmarks(QString name){
			QList<QString> list;
			ContourCLj* joint = getJoint(name.toStdString());
			for (unsigned int i = 0; i < joint->flexionaxeslandmarks.size(); i++){
				list.append(QString::fromStdString(joint->flexionaxeslandmarks.at(i)));
			}
			return list;
		}

		void ContourDeformation::ui_addFlexionAxesLandmark(QString name, QString newValue){
			ContourCLj* joint = getJoint(name.toStdString());
			joint->flexionaxeslandmarks.push_back(newValue.toStdString());
			joint->flexionaxeslandmarks_info.push_back("");
		}

		void ContourDeformation::ui_removeFlexionAxesLandmarks(QString j, QString name){
			ContourCLj* joint = getJoint(j.toStdString());
			for (unsigned int i = 0; i < joint->flexionaxeslandmarks.size(); i++){
				if (joint->flexionaxeslandmarks.at(i) == name.toStdString())
					//std::string removeString = joint->flexionaxeslandmarks_info.at(i);
					joint->flexionaxeslandmarks_info.erase(joint->flexionaxeslandmarks_info.begin() + i);
			}
			joint->flexionaxeslandmarks.erase(std::remove(joint->flexionaxeslandmarks.begin(), joint->flexionaxeslandmarks.end(), name.toStdString()), joint->flexionaxeslandmarks.end());
		}



		QList<QString> ContourDeformation::ui_getSplLandmarks(QString name){
			//std::cout << "\n\n ui_getSplLandmarks called for " + name.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(name.toStdString());
			if (joint){
				for (unsigned int i = 0; i < joint->dof1_target.size(); i++)
					list.append(QString::fromStdString(joint->dof1_target.at(i)));
				for (unsigned int i = 0; i < joint->dof1_ref.size(); i++)
					list.append(QString::fromStdString(joint->dof1_ref.at(i)));
				for (unsigned int i = 0; i < joint->dof1_normal.size(); i++)
					list.append(QString::fromStdString(joint->dof1_normal.at(i)));

				for (unsigned int i = 0; i < joint->dof2_target.size(); i++)
					list.append(QString::fromStdString(joint->dof2_target.at(i)));
				for (unsigned int i = 0; i < joint->dof2_ref.size(); i++)
					list.append(QString::fromStdString(joint->dof2_ref.at(i)));
				for (unsigned int i = 0; i < joint->dof2_normal.size(); i++)
					list.append(QString::fromStdString(joint->dof2_normal.at(i)));
				
				for (unsigned int i = 0; i < joint->dof3_target.size(); i++)
					list.append(QString::fromStdString(joint->dof3_target.at(i)));
				for (unsigned int i = 0; i < joint->dof3_ref.size(); i++)
					list.append(QString::fromStdString(joint->dof3_ref.at(i)));
				for (unsigned int i = 0; i < joint->dof3_normal.size(); i++)
					list.append(QString::fromStdString(joint->dof3_normal.at(i)));
			}
			ContourCLbr* br = getBodyRegion(name.toStdString());
			if (br){
				for (unsigned int i = 0; i < br->dof1_target.size(); i++)
					list.append(QString::fromStdString(br->dof1_target.at(i)));
				for (unsigned int i = 0; i < br->dof1_ref.size(); i++)
					list.append(QString::fromStdString(br->dof1_ref.at(i)));
				for (unsigned int i = 0; i < br->dof1_normal.size(); i++)
					list.append(QString::fromStdString(br->dof1_normal.at(i)));

				for (unsigned int i = 0; i < br->dof2_target.size(); i++)
					list.append(QString::fromStdString(br->dof2_target.at(i)));
				for (unsigned int i = 0; i < br->dof2_ref.size(); i++)
					list.append(QString::fromStdString(br->dof2_ref.at(i)));
				for (unsigned int i = 0; i < br->dof2_normal.size(); i++)
					list.append(QString::fromStdString(br->dof2_normal.at(i)));

				for (unsigned int i = 0; i < br->dof3_target.size(); i++)
					list.append(QString::fromStdString(br->dof3_target.at(i)));
				for (unsigned int i = 0; i < br->dof3_ref.size(); i++)
					list.append(QString::fromStdString(br->dof3_ref.at(i)));
				for (unsigned int i = 0; i < br->dof3_normal.size(); i++)
					list.append(QString::fromStdString(br->dof3_normal.at(i)));
			}
			return list;
		}

		void ContourDeformation::ui_addSplLandmark(QString jt, QString name, QString desc){
			//std::cout << "\n\n ui_addSplLandmark called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->special_landmark1.size() == 0){
				joint->special_landmark1 = name.toStdString();
				if (joint->special_landmarks_vec_info.size() > 0)
					joint->special_landmarks_vec_info.at(0) = desc.toStdString();
				else
					joint->special_landmarks_vec_info.push_back(desc.toStdString());
				return;
			}
			if (joint->special_landmark2.size() == 0){
				joint->special_landmark2 = name.toStdString();
				if (joint->special_landmarks_vec_info.size() > 1)
					joint->special_landmarks_vec_info.at(1) = desc.toStdString();
				else
					joint->special_landmarks_vec_info.push_back(desc.toStdString());
			}
		}

		void ContourDeformation::ui_editSplLandmark(QString jt, QString name, QString newName){
			//std::cout << "\n\n ui_editSplLandmark called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->special_landmark1 == name.toStdString())
				joint->special_landmark1 = newName.toStdString();
			if (joint->special_landmark2 == name.toStdString())
				joint->special_landmark2 = newName.toStdString();

		}

		void ContourDeformation::ui_removeSplLandmark(QString jt, QString name){
			//std::cout << "\n\n ui_removeSplLandmark called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->special_landmark1 == name.toStdString()){
				joint->special_landmark1 = string();
				joint->special_landmarks_vec_info.at(0) = string();
			}
			if (joint->special_landmark2 == name.toStdString()){
				joint->special_landmark2 = string();
				joint->special_landmarks_vec_info.at(0) = string();
			}
		}

		QList<QString> ContourDeformation::ui_getSplineInfo(QString name){
			//std::cout << "\n\n ui_getSplineInfo called for " + name.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(name.toStdString());
			if (joint->joint_splinep0.size() != 0)
				list.append(QString::fromStdString(joint->joint_splinep0));
			if (joint->joint_splinep1.size() != 0)
				list.append(QString::fromStdString(joint->joint_splinep1));
			if (joint->joint_splinet1.size() != 0)
				list.append(QString::fromStdString(joint->joint_splinet1));
			if (joint->joint_splinet2.size() != 0)
				list.append(QString::fromStdString(joint->joint_splinet2));
			return list;
		}

		void ContourDeformation::ui_addSplineInfo(QString jt, QString name){
			//std::cout << "\n\n ui_addSplineInfo called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->joint_splinep0 == ""){
				joint->joint_splinep0 = name.toStdString();
				return;
			}
			if (joint->joint_splinep1 == ""){
				joint->joint_splinep1 = name.toStdString();
				return;
			}
			if (joint->joint_splinet1 == ""){
				joint->joint_splinet1 = name.toStdString();
				return;
			}
			if (joint->joint_splinet2 == "")
				joint->joint_splinet2 = name.toStdString();
		}

		void ContourDeformation::ui_editSplineInfo(QString jt, QString name, QString newName){
			//std::cout << "\n\n ui_editSplineInfo called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->joint_splinep0 == name.toStdString())
				joint->joint_splinep0 = newName.toStdString();
			if (joint->joint_splinep1 == name.toStdString())
				joint->joint_splinep1 = newName.toStdString();
			if (joint->joint_splinet1 == name.toStdString())
				joint->joint_splinet1 = newName.toStdString();
			if (joint->joint_splinet2 == name.toStdString())
				joint->joint_splinet2 = newName.toStdString();

		}

		void ContourDeformation::ui_removeSplineInfo(QString jt, QString name){
			//std::cout << "\n\n ui_removeSplLandmark called for " + jt.toStdString();
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (joint->joint_splinep0 == name.toStdString()){
				joint->joint_splinep0 = string();
				joint->spline_vec_info.at(0) = "";
			}
			if (joint->joint_splinep1 == name.toStdString()){
				joint->joint_splinep1 = string();
				joint->spline_vec_info.at(1) = "";
			}
			if (joint->joint_splinet1 == name.toStdString()){
				joint->joint_splinet1 = string();
				joint->spline_vec_info.at(2) = "";
			}
			if (joint->joint_splinet2 == name.toStdString()){
				joint->joint_splinet2 = string();
				joint->spline_vec_info.at(3) = "";
			}
		}

		void ContourDeformation::ui_moveUpSplineLandmark(QString jt, QString name){
			string landmark = name.toStdString();
			string temp;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (landmark == joint->joint_splinep1){
				temp = joint->joint_splinep1;
				joint->joint_splinep1 = joint->joint_splinep0;
				joint->joint_splinep0 = temp;
				temp = joint->spline_vec_info.at(0);
				joint->spline_vec_info.at(0) = joint->spline_vec_info.at(1);
				joint->spline_vec_info.at(1) = temp;
				return;
			}
			if (landmark == joint->joint_splinet1){
				temp = joint->joint_splinet1;
				joint->joint_splinet1 = joint->joint_splinep1;
				joint->joint_splinep1 = temp;
				temp = joint->spline_vec_info.at(2);
				joint->spline_vec_info.at(2) = joint->spline_vec_info.at(1);
				joint->spline_vec_info.at(1) = temp;
				return;
			}
			if (landmark == joint->joint_splinet2){
				temp = joint->joint_splinet1;
				joint->joint_splinet1 = joint->joint_splinet2;
				joint->joint_splinet2 = temp;
				temp = joint->spline_vec_info.at(2);
				joint->spline_vec_info.at(2) = joint->spline_vec_info.at(3);
				joint->spline_vec_info.at(3) = temp;
				return;
			}
		}

		void ContourDeformation::ui_moveDownSplineLandmark(QString jt, QString name){
			string landmark = name.toStdString();
			string temp;
			ContourCLj* joint = getJoint(jt.toStdString());
			if (landmark == joint->joint_splinep0){
				temp = joint->joint_splinep1;
				joint->joint_splinep1 = joint->joint_splinep0;
				joint->joint_splinep0 = temp;
				temp = joint->spline_vec_info.at(0);
				joint->spline_vec_info.at(0) = joint->spline_vec_info.at(1);
				joint->spline_vec_info.at(1) = temp;
				return;
			}
			if (landmark == joint->joint_splinep1){
				temp = joint->joint_splinet1;
				joint->joint_splinet1 = joint->joint_splinep1;
				joint->joint_splinep1 = temp;
				temp = joint->spline_vec_info.at(2);
				joint->spline_vec_info.at(2) = joint->spline_vec_info.at(1);
				joint->spline_vec_info.at(1) = temp;
				return;
			}
			if (landmark == joint->joint_splinet1){
				temp = joint->joint_splinet1;
				joint->joint_splinet1 = joint->joint_splinet2;
				joint->joint_splinet2 = temp;
				temp = joint->spline_vec_info.at(2);
				joint->spline_vec_info.at(2) = joint->spline_vec_info.at(3);
				joint->spline_vec_info.at(3) = temp;
				return;
			}
		}

		QString ContourDeformation::ui_getLandmarkDescriptionFromAnatomyDB(QString name){
			return QString::fromStdString(anatomydb::getEntityDescription(name.toStdString()));
			return QString::fromStdString("");
		}

		QList<QString> ContourDeformation::ui_getAnatomyDBLandmarks(){
			QList<QString> list;
			std::vector<std::string> landmarkList;
			anatomydb::getLandmarkList(landmarkList);
			for (unsigned int i = 0; i < landmarkList.size(); i++)
				list.append(QString::fromStdString(landmarkList.at(i)));
			return list;
		}

		void ContourDeformation::ui_addbrToTargetLandmark(QString landmarkName, QString brSelected){
			string landmark = landmarkName.toStdString();
			string bodyregion = brSelected.toStdString();
			if (!Context::instance().project().target().empty()) {
				cout << INFO << "Targets :" << endl;
				TargetList const& targetList = Context::instance().project().target();
				for (hbm::LandmarkTarget const& target : targetList.landmark)
					if (landmark == target.name()){
						std::cout << landmark << "\t" << bodyregion;

						ContourCLj* foundjoint;
						foundjoint = m_meta.ctrl_line.findparentJoint(bodyregion);
						if (foundjoint == nullptr){
							std::cout << "no joint corresponds to this information" << std::endl;
						}
						else{
							std::cout << foundjoint->name;
						}
					}
			}
			else
				cout << INFO << "No target" << endl;

		}

		void ContourDeformation::ui_mapBRToTargetLandmark(QString landmarkName, QString brSelected){
			string landmark = landmarkName.toStdString();
			string bodyregion = brSelected.toStdString();
			if (!Context::instance().project().target().empty()) {
				//cout << INFO << "\nTargets :" << endl;
				TargetList const& targetList = Context::instance().project().target();
				for (hbm::LandmarkTarget const& target : targetList.landmark)
					if (landmark == target.name()){
						std::map<unsigned int, double> coordMap = target.value();

						// Mapping the target landmark with body region
						comptargets.targetlandmark_map[landmark] = bodyregion;
						//comptargets.targetlandmarkconatiner_map[target] = bodyregion;  HAVING BUILD ERRORS Because of this line

						//  Finding the parent joint 
						ContourCLj* foundjoint;
						string joint;
						if (!m_meta.ctrl_line.root)
							return;
						foundjoint = m_meta.ctrl_line.findparentJoint(bodyregion);
						if (foundjoint == nullptr){
						}
						else{
							joint = foundjoint->name;
						}
						//Finding the angle


						std::vector<std::string> landmarkvec;
						landmarkvec.push_back(foundjoint->flexionaxeslandmarks.at(0));
						landmarkvec.push_back(foundjoint->flexionaxeslandmarks.at(1));

						double absolutefinalangle = comptargets.computeTargetAngle(m_meta, fem1, landmarkvec, target, foundjoint->joint_splinet1);
						//						double angle = absolutefinalangle - foundjoint->jointangle;
						double absoluteangle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, foundjoint->joint_splinet2, foundjoint->joint_splinet1);
						foundjoint->jointangle = absoluteangle;
						double angle = absolutefinalangle - foundjoint->jointangle;

						std::cout << "\n Calling computeTargetAngle for : " << landmark;
						//						double angle = comptargets.computeTargetAngle(m_meta, fem1, landmarkvec, target, landmark);
						double deform_angle = angle;
						std::cout << "\n Angle : " << angle;
						// Setting values before using deformContours()
						std::vector<double> valueVec;
						if (joint == "kneel" || joint == "kneer"){
							valueVec.push_back(1);// ui_setReposFunc(1);


						}
						if (joint == "anklel" || joint == "ankler"){
							valueVec.push_back(2);//ui_setReposFunc(2);


						}
						if (joint == "hipl" || joint == "hipr")
							valueVec.push_back(3);//ui_setReposFunc(3);
						if (joint == "cervical_joint"){
							valueVec.push_back(4);//ui_setReposFunc(4);
							valueVec.push_back(0);//ui_setReposSide(0);
						}
						if (joint == "elbowl" || joint == "elbowr"){
							valueVec.push_back(5);//ui_setReposFunc(5);


						}
						if (joint == "wristl" || joint == "wristr")
							valueVec.push_back(6);//ui_setReposFunc(6);

						valueVec.push_back(0);// flexion   ui_setReposType(0);

						if (joint.back() == 'l')
							valueVec.push_back(0);//ui_setReposSide(0);
						if (joint.back() == 'r')
							valueVec.push_back(1);//ui_setReposSide(1);

						valueVec.push_back(angle);//ui_setReposAngle(angle);
						valueVec.push_back(deform_angle);//ui_setReposAngle(angle);

						// Saving the values in the map 
						comptargets.targetlandmark_reposmap[landmark] = valueVec;
						valueVec = comptargets.targetlandmark_reposmap[landmark];
					}
			}
			else
				cout << INFO << "No target" << endl;

		}

		QList<double> ContourDeformation::ui_getTargetLandmarkValues(QString landmarkTarget){
			QList<double> list;
			string landmark = landmarkTarget.toStdString();
			std::vector<double> valueVec;
			valueVec = comptargets.targetlandmark_reposmap[landmark];
			for (int j = 0; j < valueVec.size(); j++){
				list.append(valueVec.at(j));
			}
			return list;
		}

		void ContourDeformation::ui_deformContoursForLandmarkTarget(QString landmarkTarget){
			string landmark = landmarkTarget.toStdString();
			std::vector<double> valueVec;
			valueVec = comptargets.targetlandmark_reposmap[landmark];
			ui_setReposFunc(valueVec.at(0));
			ui_setReposType(valueVec.at(1));
			ui_setReposSide(valueVec.at(2));
			ui_setReposAngle(valueVec.at(4));
			deformContours();
			refreshDisplay();
		}

		void ContourDeformation::useLandmarkTargetBusyIndicator(){
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::ui_useLandmarkTargets, this), false, false, false, false, true);
		}

		void ContourDeformation::ui_useLandmarkTargets()
		{
			// pInfo();
			//pInfo() << START << "Angle computation from targets ";
			//TargetSynth tsynth;
			m_tsynth.ComputeJointAngleFromTarget();
			//pInfo() << DONE << "Angle computation from targets ";
		}

		//When processing done
		QList<QString> ContourDeformation::ui_GetBRandAngleForLandmarkTarget(){
			QList<QString> list;
			std::pair<int, std::string> brangle = m_tsynth.GetBRandAngle();
			ContourCLj* tempj = m_meta.ctrl_line.findparentJoint(brangle.second);

			//Populate the UI : first value is the parent joint name , the second value is the type of movement , third value is the joint angle
			list.append(QString::fromStdString(tempj->name));
			list.append(QString::fromStdString(std::to_string(0))); // Setting default value as flexion for time being 
			list.append(QString::fromStdString(std::to_string(brangle.first)));

			return list;
		}



		QString ContourDeformation::ui_getBRDescription(QString name){
			ContourCLbr* br = getBodyRegion(name.toStdString());
			return QString::fromStdString(br->description);
		}

		QString ContourDeformation::ui_getJointDescription(QString jt){
			ContourCLj* joint = getJoint(jt.toStdString());
			return QString::fromStdString(joint->joint_description);
		}

		QList<QString> ContourDeformation::ui_getBRCircumDesc(QString name){
			QList<QString> list;
			ContourCLbr* br = getBodyRegion(name.toStdString());
			for (unsigned int i = 0; i < br->circumfvec_info.size(); i++)
				list.append(QString::fromStdString(br->circumfvec_info.at(i)));
			return list;
		}

		QList<QString> ContourDeformation::ui_getJointCircumDesc(QString jt){
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			//for (unsigned int i = 0; i<joint->cir)
			return list;
		}

		QList<QString> ContourDeformation::ui_getCplLandmarksDesc(QString name){
			std::cout << "\nDescriptions for Cpl Landmark of " << name.toStdString();
			QList<QString> list;
			ContourCLbr* br = getBodyRegion(name.toStdString());
			for (unsigned int i = 0; i < br->cplandmarks_info.size(); i++){
				if (br->cplandmarks_info.at(i).size() != 0)
					list.append(QString::fromStdString(br->cplandmarks_info.at(i)));
				else
					list.append(QString::fromStdString(""));
			}
			return list;
		}

		QList<QString> ContourDeformation::ui_getSplLandmarksDesc(QString jt){
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			for (unsigned int i = 0; i < joint->special_landmarks_vec_info.size(); i++)
				if (joint->special_landmarks_vec_info.at(i).size() != 0)
					list.append(QString::fromStdString(joint->special_landmarks_vec_info.at(i)));
				else
					list.append(QString::fromStdString(""));
			return list;
		}

		QList<QString> ContourDeformation::ui_getFlexionAxesLandmarksDesc(QString jt){
			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			for (unsigned int i = 0; i < joint->flexionaxeslandmarks_info.size(); i++)
				if (joint->flexionaxeslandmarks_info.at(i).size() != 0)
					list.append(QString::fromStdString(joint->flexionaxeslandmarks_info.at(i)));
				else
					list.append(QString::fromStdString(""));
			return list;
		}

		QList<QString> ContourDeformation::ui_getSplineDesc(QString jt){

			QList<QString> list;
			ContourCLj* joint = getJoint(jt.toStdString());
			//std::cout << "\n ui_getSplineDesc called for " << jt.toStdString() << "\t size : " << joint->spline_vec_info.size() << "\t";
			for (unsigned int i = 0; i < joint->spline_vec_info.size(); i++)
				if (joint->spline_vec_info.at(i).size() != 0)
					list.append(QString::fromStdString(joint->spline_vec_info.at(i)));
				else
					list.append(QString::fromStdString(""));
			return list;
		}



		void ContourDeformation::refreshDisplay(){
			fem1.updateVTKRepresentation();

			if (MAPPING_REPOSITIONING == true){
				pskeleton->ForceContourGen();
			}
			Context::instance().display().Refresh();
		}


		void ContourDeformation::deformContours(){

			double flexion_angle = reposAngle;
			//			MAPPING_REPOSITIONING = true;

			switch (reposFunc){

				// Knee 
			case 1:{
					   // Flexion
					   switch (reposSide){
						   // Left
					   case 0:{
								 
									  pInfo() << "Left Knee Repositioning starts ";

#ifdef CONTOUR_INPUT
									  main_repo.Left_Leg_Repositioning(flexion_angle, fem1,
										  m_meta, tmpPath);
#endif
#ifdef CONTOUR_INPUT_SPLINE
									  main_repo.Left_Knee_Spline_Repositioning(flexion_angle, fem1, m_meta);
#endif

									  reposAngle = flexion_angle;
									  main_repo.Left_Knee_Spline_RepositioningcontourCL(flexion_angle, fem1, m_meta, tmpPath);

									  //							std::string joint_name = "kneel";
									  //							main_repo.repositionContours(flexion_angle, fem1, m_meta, tmpPath, joint_name);

									  enableReposButton = true;
									  hip_process = true;
								  

								  break;

					   }
						   // Right
                       case 1:{
                                  pInfo() << "Right Knee Repositioning starts ";

								 
#ifdef CONTOUR_INPUT
									  main_repo.Right_Leg_Repositioning(flexion_angle, fem, m_meta, &m_meta.m_hbm_contours);
									  SId nid_content_right_leg;
									  if (m_meta.hasGenericmetadata("right_flesh_leg"))
									  {
										  VId idgroupe = m_meta.genericmetadata("right_flesh_leg").get_groupNode();
										  for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
											  nid_content_right_leg.insert(fem.get<GroupNode>(*it)->get().begin(), fem.get<GroupNode>(*it)->get().end());
									  }
									  main_repo.Delaunay_Right_Leg_Repositioning(&nid_content_right_leg, fem1, m_meta, MAPPING_REPOSITIONING);
#endif

									  
									  reposAngle = flexion_angle;
									  main_repo.Right_Knee_Spline_RepositioningcontourCL(flexion_angle, fem1, m_meta, tmpPath);
									  enableReposButton = true;
									  //						main_repo.Delaunay_Right_Leg_Repositioning(&nid_content_right_leg, fem1, m_meta, MAPPING_REPOSITIONING);

									  //						pInfo() << "Right Knee Repositioning ends for angle " << flexion_angle;
									  hip_process = true;


								  


								  break;

					   }
					   }
					   break;
			}

				// Ankle 
			case 2:{
					   switch (reposType){
						   //Flexion 
					   case 0:{
								  switch (reposSide){
									  //Left
								  case 0:{


											 if (!m_meta.hasEntity("Skin_of_right_free_lower_limb")){
												 pCritical() << "Missing Skin_of_right_free_lower_limb entity so contours can't be craeted on left free lower limb";
											 }
											 else{
												 pInfo() << "Left Foot Repositioning starts for angle " << flexion_angle << endl;
												 std::vector<std::string> landmarkvec;

												 reposAngle = flexion_angle;
												 main_repo.leftFootReposContourCl(flexion_angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
												 hip_process = true;
											 }


											 break;
								  }
									  //Right
								  case 1:{
											 if (!m_meta.hasGenericmetadata("Flesh_right_foot"))
											 {
												 pCritical() << "Missing Flesh_right_foot genericmetadata";
											 }
											 else{

												 reposAngle = flexion_angle;
												 pInfo() << "Right Foot Repositioning starts for angle " << flexion_angle << endl;
												 main_repo.rightFootReposContourCl(flexion_angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
												 hip_process = true;
                                                 
                                             }
                                             
											 break;
								  }
								  }
								  break;
					   }

						   // twisting
					   case 2:{
								  switch (reposSide){
									  //Left
								  case 0:{
											 if (!m_meta.hasGenericmetadata("Flesh_left_foot"))
											 {
												 pCritical() << "Missing Flesh_left_foot genericmetadata";
											 }
											 else{

												 pInfo() << "Left Foot Repositioning starts for angle " << flexion_angle << endl;
												 main_repo.leftFootTwistContourCl(flexion_angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
												 hip_process = true;


											 }

											 break;
								  }
									  //right
								  case 1:{
											 if (!m_meta.hasGenericmetadata("Flesh_right_foot"))
											 {
												 pCritical() << "Missing Flesh_right_foot genericmetadata";
											 }
											 else{

												 pInfo() << "Right Foot twisting starts for angle " << flexion_angle << endl;
												 main_repo.rightFootTwistContourCl(flexion_angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
												 hip_process = true;


											 }
											 break;
								  }
								  }
								  break;
					   }
					   }
					   break;
			}

				// Hip
			case 3:{
					   switch (reposType){
						   //Flexion 
					   case 0:{
								  switch (reposSide){
									  //Left
								  case 0:{
												 main_repo.hipcontourcorrection(fem1, m_meta);

												 VId sidnodes, tempselector;
												 VId hip_test;
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;
												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
												 pInfo() << "size of hip :" << sidnodes.size();
												 pInfo() << "size of leg :" << tempselector.size();
												 ////-11.9441, -107.935, -13.8835, -27.0299, -105.086, -59.8361, 8.68459, -111.163, -90.4454, 60.4249, -122.947, -81.2431
												 // std::vector<hbm::NodePtr>
												 NodePtr p1 = std::make_shared<Node>();
												 p1->setCoord(-11.941, 0,-13.8835);

                                                 NodePtr p2 = std::make_shared<Node>();
												 p2->setCoord(-27.023, 0, -59.8361);

                                                 NodePtr p3 = std::make_shared<Node>();
												 p3->setCoord(8.68, 0,-111.16);

                                                 NodePtr p4 = std::make_shared<Node>();
												 p4->setCoord(60.4249, 0, -81.2461);
												 vec_spline_new.clear();
												 vec_spline_new.push_back(p1);
												 vec_spline_new.push_back(p2);
												 vec_spline_new.push_back(p3);
												 vec_spline_new.push_back(p4);
												 main_repo.ReposContourHip2(flexion_angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
                                                 
												 enableReposButton = true;
											 //Hip
											 break;
								  }
									  //Right
								  case 1:{
											 if (!m_meta.hasLandmark("flexion_axis_hip_a"))
											 {
												 pCritical() << "Missing flexion_axis_hip_a landmark";
											 }
											 else if (!m_meta.hasLandmark("flexion_axis_hip_b"))
											 {
												 pCritical() << "Missing flexion_axis_hip_b landmark";
											 }
											 else if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set genericmetadata";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip genericmetadata";
											 }
											 else if (!m_meta.hasEntity(m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata)){
												 pCritical() << "Missing " << m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata << " entity so contours can't be craeted on left free lower limb";
											 }
											 else{
												 pInfo() << "Right Hip Repositioning starts for angle " << flexion_angle;

												 std::vector<std::string> landmarkvec;
												 landmarkvec.push_back(m_meta.ctrl_line.root->childrenjts.at(1)->flexionaxeslandmarks.at(0));
												 landmarkvec.push_back(m_meta.ctrl_line.root->childrenjts.at(1)->flexionaxeslandmarks.at(1));

												 //							double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, "torso_proximal", "thighr_distal");

												 double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, m_meta.ctrl_line.root->childrenjts.at(1)->joint_splinet1, m_meta.ctrl_line.root->childrenjts.at(1)->joint_splinet2);
												 m_meta.ctrl_line.root->childrenjts.at(1)->jointangle = angle;

												 pInfo() << "current angle between femur axis and axis passing through center of saccrum and cuccyx is :" << angle << " The max and min angle between the axes can be 180 and 100 degree respectively.";
												 pInfo() << "User needs to input the amount of flexion or extension which he wants with respect to current position.";

												 pInfo() << "Absolute angle" << angle;
												 double angle_limit = angle + flexion_angle;
												 pInfo() << "target absolute angle: " << angle_limit;

												 if (angle_limit > 190){
													 flexion_angle = 190 - angle;
													 pInfo() << "But as it exceeds the max angle limit by which one can do the flexion at hip joint. The max angle limit is 190 degree so the maximum flexion which can occur from the current position is " << flexion_angle;
												 }
												 else if (angle_limit < 135){
													 flexion_angle = 135 - angle;
													 pInfo() << "But as it exceeds the min angle limit by which one can do the flexion at hip joint. The min angle limit is 135 degree angle between so the minimum flexion which can occur from the current position is " << flexion_angle;
												 }


												 reposAngle = flexion_angle;

												 pInfo() << "Right Hip Repositioning ends for angle " << flexion_angle;
												 enableReposButton = true;
											 }
											 break;
								  }
								  }
								  break;
					   }

						   //Abduction
					   case 1:{
								  switch (reposSide){
									  //Left
								  case 0:{
											 if (!m_meta.hasLandmark("flexion_axis_hip_a"))
											 {
												 pCritical() << "Missing flexion_axis_hip_a landmark";
											 }
											 else if (!m_meta.hasLandmark("flexion_axis_hip_b"))
											 {
												 pCritical() << "Missing flexion_axis_hip_b landmark";
											 }
											 else if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set genericmetadata";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip genericmetadata";
											 }
											 else if (!m_meta.hasEntity(m_meta.ctrl_line.root->childrenjts.at(1)->childbodyregion->skingenericmetadata)){
												 pCritical() << "Missing " << m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata << " entity so contours can't be craeted on left free lower limb";
											 }
											 else{



												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;
												 enableReposButton = true;


											 }

											 break;
								  }
									  //Right
								  case 1:{
											 if (!m_meta.hasLandmark("flexion_axis_hip_a"))
											 {
												 pCritical() << "Missing flexion_axis_hip_a landmark";
											 }
											 else if (!m_meta.hasLandmark("flexion_axis_hip_b"))
											 {
												 pCritical() << "Missing flexion_axis_hip_b landmark";
											 }
											 else if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set genericmetadata";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip genericmetadata";
											 }
											 else if (!m_meta.hasEntity(m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata)){
												 pCritical() << "Missing " << m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata << " entity so contours can't be craeted on left free lower limb";
											 }
											 else{
												 pInfo() << "Right Hip Abduction starts for angle " << flexion_angle;

												 enableReposButton = true;

											 }

											 break;
								  }
								  }
								  break;
					   }

						   // twisting
					   case 2:{
								  switch (reposSide){
									  //Left
								  case 0:{
											 if (!m_meta.hasLandmark("flexion_axis_hip_a"))
											 {
												 pCritical() << "Missing flexion_axis_hip_a landmark";
											 }
											 else if (!m_meta.hasLandmark("flexion_axis_hip_b"))
											 {
												 pCritical() << "Missing flexion_axis_hip_b landmark";
											 }
											 else if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set genericmetadata";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip genericmetadata";
											 }
											 else if (!m_meta.hasEntity(m_meta.ctrl_line.root->childrenjts.at(1)->childbodyregion->skingenericmetadata)){
												 pCritical() << "Missing " << m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata << " entity so contours can't be craeted on left free lower limb";
											 }
											 else{
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;

												 enableReposButton = true;

											 }

											 break;
								  }
									  //right
								  case 1:{
											 if (!m_meta.hasLandmark("flexion_axis_hip_a"))
											 {
												 pCritical() << "Missing flexion_axis_hip_a landmark";
											 }
											 else if (!m_meta.hasLandmark("flexion_axis_hip_b"))
											 {
												 pCritical() << "Missing flexion_axis_hip_b landmark";
											 }
											 else if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set genericmetadata";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip genericmetadata";
											 }
											 else if (!m_meta.hasEntity(m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata)){
												 pCritical() << "Missing " << m_meta.ctrl_line.root->childrenjts.at(2)->childbodyregion->skingenericmetadata << " entity so contours can't be craeted on left free lower limb";
											 }
											 else{
												 pInfo() << "Right Hip Twisting starts for angle " << flexion_angle;


												 enableReposButton = true;

											 }

											 break;
								  }
								  }
								  break;
					   }
					   }
					   break;
			}

				// Neck 
			case 4:{
					   pInfo() << INFO << " Neck " << endl;

					   switch (reposType){
						   //Flexion 
					   case 0:{
								  pInfo() << "Flexion / Extension of Cervical Region";

								  if (!m_meta.hasEntity("Atlas"))
								  {
									  pCritical() << "Missing Atlas entity";
								  }
								  else if (!m_meta.hasEntity("Axis"))
								  {
									  pCritical() << "Missing Axis entity";
								  }
								  else if (!m_meta.hasEntity("Third_cervical_vertebra"))
								  {
									  pCritical() << "Missing Third_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fourth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fourth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fifth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fifth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Sixth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Sixth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Seventh_cervical_vertebra"))
								  {
									  pCritical() << "Missing Seventh_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("First_thoracic_vertebra"))
								  {
									  pCritical() << "Missing First_thoracic_vertebra entity";
								  }
								  else{

									  /*						std::vector<std::string> landmarkvec;
									  landmarkvec.push_back();
									  landmarkvec.push_back(m_meta.ctrl_line.root->childrenjts.at(1)->flexionaxeslandmarks.at(1));
									  pInfo() << m_meta.ctrl_line.root->childrenjts.at(1)->name;

									  //							double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, "torso_proximal", "thighr_distal");
									  double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, m_meta.ctrl_line.root->childrenjts.at(1)->joint_splinet1, m_meta.ctrl_line.root->childrenjts.at(1)->joint_splinet2);
									  m_meta.ctrl_line.root->childrenjts.at(1)->jointangle = angle;;
									  pInfo() << "Absolute angle" << angle;
									  //							flexion_angle = 58.328 - flexion_angle;
									  double angle_limit = angle + flexion_angle;


									  if (angle_limit > 180){
									  flexion_angle = 180 - angle;
									  }
									  else if (angle_limit < 100){
									  flexion_angle = 100 - angle;
									  }

									  pInfo() << "current angle between femur axis and axis passing through center of saccrum and cuccyx is :" << angle << " The max and min angle between the axes can be 180 and 100 degree respectively.";
									  pInfo() << "User needs to input the amount of flexion or extension which he wants with respect to current position.";

									  pInfo() << "Absolute angle" << angle;


									  pInfo() << "Absolute angle" << angle;
									  double angle_limit = angle + flexion_angle;
									  pInfo() << "target absolute angle: " << angle_limit;

									  if (angle_limit > 180){
									  flexion_angle = 180 - angle;
									  pInfo() << "But as it exceeds the max angle limit by which one can do the flexion at hip joint. The max angle limit is 126 degree so the maximum flexion which can occur from the current position is " << flexion_angle;
									  }
									  else if (angle_limit < 100){
									  flexion_angle = 100 - angle;
									  pInfo() << "But as it exceeds the min angle limit by which one can do the flexion at hip joint. The min angle limit is 81 degree angle between so the minimum flexion which can occur from the current position is " << flexion_angle;
									  }


									  reposAngle = flexion_angle;


									  */
									  //						std::cout << m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->dof1_info.at(0);



									  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
									  vector<NodePtr > cstore;  /// contains centroids of vertebrae
									  reposAngle = flexion_angle;


									  pDebug() << "flexion angle" << flexion_angle;
									  //main_repo.LumbarLateral(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
									  //						main_repo.ThoraxMotion(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);

//									  main_repo.CervicalVertebraeEntityFlexion2(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
									  main_repo.Neck_head_flexion(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
							         // main_repo.Delaunay_neck_twisting(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

									  //						angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(0), m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(1));
									  //						pInfo() << " Final Absolute angle" << angle;

									  pInfo() << "Vertebrae and skull moved by " << flexion_angle << " around an axis passing through the centroid of T1 in segital plane";
									  pInfo() << "Contours moved by " << flexion_angle << " around an axis passing through the centroid of T1 in segital plane";
									  enableReposButton = true;

								  }

								  break;
					   }

						   //Lateral Rotation(twisting)
					   case 2:{
								  pInfo() << " Lateral Rotation ";
								  if (!m_meta.hasEntity("Atlas"))
								  {
									  pCritical() << "Missing Atlas entity";
								  }
								  else if (!m_meta.hasEntity("Axis"))
								  {
									  pCritical() << "Missing Axis entity";
								  }
								  else if (!m_meta.hasEntity("Third_cervical_vertebra"))
								  {
									  pCritical() << "Missing Third_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fourth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fourth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fifth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fifth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Sixth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Sixth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Seventh_cervical_vertebra"))
								  {
									  pCritical() << "Missing Seventh_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("First_thoracic_vertebra"))
								  {
									  pCritical() << "Missing First_thoracic_vertebra entity";
								  }
								  else{

									  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
									  vector<NodePtr > cstore;  /// contains centroids of vertebrae
									  /// neck repositioning through delaunnay


//									  main_repo.CervicalVertebraeTwist(flexion_angle, &cstore, &all_nodes, fem1, m_meta, tmpPath);
									  main_repo.CervicalVertebraeTwist12(flexion_angle, &cstore, &all_nodes, fem1, m_meta, tmpPath);
									  main_repo.Delaunay_neck_twisting(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay

									  enableReposButton = true;

								  }
								  break;
					   }

						   // Lateral Flexion  
					   case 1:{
								  if (!m_meta.hasEntity("Atlas"))
								  {
									  pCritical() << "Missing Atlas entity";
								  }
								  else if (!m_meta.hasEntity("Axis"))
								  {
									  pCritical() << "Missing Axis entity";
								  }
								  else if (!m_meta.hasEntity("Third_cervical_vertebra"))
								  {
									  pCritical() << "Missing Third_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fourth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fourth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Fifth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Fifth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Sixth_cervical_vertebra"))
								  {
									  pCritical() << "Missing Sixth_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("Seventh_cervical_vertebra"))
								  {
									  pCritical() << "Missing Seventh_cervical_vertebra entity";
								  }
								  else if (!m_meta.hasEntity("First_thoracic_vertebra"))
								  {
									  pCritical() << "Missing First_thoracic_vertebra entity";
								  }
								  else{
									  reposAngle = flexion_angle;

                                      pInfo() << "Neck Lateral Flexion of Contours start for angle " << flexion_angle;
									  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
									  vector<NodePtr > cstore;  /// contains centroids of vertebrae
									  main_repo.CervicalVertebraeEntityLateral2(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
									  main_repo.Delaunay_neck_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay
									  enableReposButton = true;
/*									  pInfo() << "lateral flexion starting";
									  std::vector<std::string> landmarkvec;
									  landmarkvec.push_back(m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->dof3.at(0));

									  landmarkvec.push_back(m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->dof3.at(1));
									  pInfo() << m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->dof3.at(0) << "   " << m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->dof3.at(1);
									  pInfo() << m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(2) << "   " << m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(3);
									  double angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(2), m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(3));

									  pInfo() << "current angle between the center of the inferior plate of T1 and C2 is :" << angle << " The max and min angle can be 35 and 20 degree respectively.";
									  pInfo() << "User needs to input the amount of lateral flexion or extension which he wants with respect to current position.";

									  pInfo() << "Absolute angle" << angle;

									  double angle_limit = angle + flexion_angle;
									  pInfo() << "lateral flexion angle processing";
									  if (angle_limit > 20){
										  flexion_angle = 20 - angle;
										  pInfo() << "But as it exceeds the max angle limit by which one can do the lateral flexion at cervical joint. The max angle limit is 20.00 degree so the maximum extension which can occur from the current position is " << flexion_angle;
									  }
									  else if (angle_limit < -20){
										  flexion_angle = -20 - angle;
										  pInfo() << "But as it exceeds the min angle limit by which one can do the lateral flexion at cervical joint. The min angle limit is -20.00 degree angle between so the maximum flexion which can occur from the current position is " << flexion_angle;
									  }

									  reposAngle = flexion_angle;

									  pInfo() << "Neck Lateral Flexion of Contours start";
									  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
									  vector<NodePtr > cstore;  /// contains centroids of vertebrae
									  pInfo() << "angle" << flexion_angle;
									  main_repo.CervicalVertebraeEntityLateral2(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
									  angle = comptargets.computeJointAngle(m_meta, fem1, landmarkvec, m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(2), m_meta.ctrl_line.root->childrenjts.at(0)->childbodyregion->childrenjts.at(0)->childbodyregion->reference_landmark.at(3));
									  pInfo() << "Final Absolute angle" << angle;
									  enableReposButton = true;
									  */
								  }

								  break;
					   }
					   }
					   break;
			}

				// Elbow
			case 5:{
					   switch (reposType){
						   //Flexion 
					   case 0:{
								  switch (reposSide){
									  //Left
								  case 0:{

											 if (!m_meta.hasGenericmetadata("Left_arm_Flesh"))
											 {
												 pCritical() << "Missing Left_arm_Flesh genericmetadata";
											 }
											 else{
                                                 pInfo() << "Left Elbow Flexion of Contours starts for angle " << flexion_angle;
												 std::vector<std::string> landmarkvec;

												 /*
												 pInfo() << "current angle between femur axis and axis passing through center of saccrum and cuccyx is :" << angle << " The max and min angle between the axes can be 180 and 100 degree respectively.";
												 pInfo() << "User needs to input the amount of flexion or extension which he wants with respect to current position.";

												 pInfo() << "Absolute angle" << angle;
												 //							flexion_angle = 58.328 - flexion_angle;
												 double angle_limit = angle + flexion_angle;


												 if (angle_limit > 180){
												 flexion_angle = 180 - angle;
												 pInfo() << "But as it exceeds the max angle limit by which one can do the flexion at hip joint. The max angle limit is 126 degree so the maximum flexion which can occur from the current position is " << flexion_angle;

												 }
												 else if (angle_limit < 100){
												 flexion_angle = 100 - angle;
												 pInfo() << "But as it exceeds the min angle limit by which one can do the flexion at hip joint. The min angle limit is 81 degree angle between so the minimum flexion which can occur from the current position is " << flexion_angle;

												 }

												 */

												 double angle = flexion_angle;
												 reposAngle = flexion_angle;
												 main_repo.LeftElbowContourCL(angle, fem1, m_meta, tmpPath);
//												 main_repo.LeftElbowBoneRepos(angle, fem1, fem, m_meta);   /// Bone Repositioning
												 main_repo.LeftElbowBoneRepos12(angle, fem1, fem, m_meta);   /// Bone Repositioning
												 enableReposButton = true;
											 }
											 break;
								  }
									  //Right
								  case 1:{
											 if (!m_meta.hasGenericmetadata("Right_arm_Flesh"))
											 {
												 pCritical() << "Missing Right_arm_Flesh genericmetadata";
											 }
                                             else{
                                                 pInfo() << "Right Elbow Flexion of Contours starts for angle " << flexion_angle;

												 double angle = flexion_angle;
//												 main_repo.RightElbowBoneRepos(angle, fem1, fem, m_meta);   /// Bone Repositioning
												 main_repo.RightElbowBoneRepos12(angle, fem1, fem, m_meta);   /// Bone Repositioning
												 main_repo.RightElbowContourCL(angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
											 }
											 break;
								  }
								  }
								  break;
					   }

						   // twisting
					   case 2:{
								  pInfo() << " Twisting ";
								  switch (reposSide){
									  //Left
								  case 0:{
											 if (!m_meta.hasGenericmetadata("Left_arm_Flesh"))
											 {
												 pCritical() << "Missing Left_arm_Flesh genericmetadata";
											 }
											 else{
												 double angle = flexion_angle;
//												 main_repo.LeftElbowTwist(angle, fem, fem1, m_meta);
												 main_repo.LeftElbowTwistContourCL(angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
											 }
											 break;
								  }
									  //right
								  case 1:{

											 if (!m_meta.hasGenericmetadata("Right_arm_Flesh"))
											 {
												 pCritical() << "Missing Right_arm_Flesh genericmetadata";
											 }
											 else{

												 double angle = flexion_angle;

												 main_repo.RightElbowTwist(angle, fem, fem1, m_meta);

												 main_repo.RightElbowTwistContourCL(angle, fem1, m_meta, tmpPath);
												 enableReposButton = true;
											 }

											 break;
								  }
								  }
								  break;
					   }
					   }
					   break;
			}

			case 6:	{
						pInfo() << INFO << " Wrist ";
						switch (reposSide){
						case 0:{
								   pInfo() << "Left " << endl;

								   if (!m_meta.hasGenericmetadata("Left_hand_flesh")){
									   pInfo() << ERRORMSG << "Missing Left_hand_flesh genericmetadata" << endl;
								   }
								   else{
									   double angle = flexion_angle;
									   main_repo.LeftWristFlexion(angle, fem1, fem, m_meta);   /// Bone Repositioning
									   main_repo.LeftWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									   enableReposButton = true;
								   }


								   break;
						}
						case 1:{
								   pInfo() << "Right " << endl;
								   if (!m_meta.hasGenericmetadata("Right_hand_flesh")){
									   pInfo() << ERRORMSG << "Missing Right_hand_flesh genericmetadata" << endl;
								   }
								   else{
									   double angle = flexion_angle;
									   main_repo.RightWristFlexion(angle, fem1, fem, m_meta);   /// Bone Repositioning
									   main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									   enableReposButton = true;
								   }
						}
						}
						break;
			}

			case 7:{
					   // Thorax
					   switch (reposType){

					   case 0:{
								  // Flexion
								  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
								  vector<NodePtr > cstore;  /// contains centroids of vertebrae
								  main_repo.ThoraxMotion(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
//								  main_repo.ThoraxLateral(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
								  enableReposButton = true;
								  break;
					   }
					   case 1:{
								  // Lateral Flexion
								  //func

						   vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
						   vector<NodePtr > cstore;  /// contains centroids of vertebrae
						   main_repo.ThoraxLateral(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
						   enableReposButton = true;

								  break;
					   }
					   case 2:{
								  // Twisting
								  break;
					   }
					   }
					   break;
			}

			case 8:{
					   // Lumbar
					   switch (reposType){

					   case 0:{
								  // Flexion
								  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
								  vector<NodePtr > cstore;  /// contains centroids of vertebrae

								  main_repo.LumbarMotion12(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
//								  main_repo.hipcontourcorrection(fem1, m_meta);
								  enableReposButton = true;
								  break;
					   }
					   case 1:{
								  // Lateral Flexion
								  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
								  vector<NodePtr > cstore;  /// contains centroids of vertebrae

								  main_repo.LumbarLateral(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
								  enableReposButton = true;
								  break;
					   }
					   case 2:{
								  // Twisting
								  vector<vector<NodePtr> > all_nodes; ///all vertebrae nodes
								  vector<NodePtr > cstore;  /// contains centroids of vertebrae

								  main_repo.LumbarTwist(flexion_angle, &cstore, &all_nodes, fem1, fem, m_meta, tmpPath);
								  enableReposButton = true;
								  break;
					   }
					   }
					   break;
			}

			case 9:{
					   // Shoulder
					   switch (reposType){

					   case 0:{
								  // Flexion
								  switch (reposSide){
								  case 0:{
											 // Flexion
											 switch (reposSide){
											 case 0:{
														/*pInfo() << "Left Shoulder Flexion " << endl;
														if (!m_meta.hasLandmark("glenohumeral_rotation_c")){
														pInfo() << ERRORMSG << "Missing glenohumeral_rotation_c lanbdmark" << endl;
														}
														else if (!m_meta.hasLandmark("glenohumeral_rotation_flexion")){
														pInfo() << ERRORMSG << "Missing glenohumeral_rotation_flexion landmark" << endl;
														}
														else{*/
														double angle = flexion_angle;

														std::cout << "shoulder flexionj starts:" << std::endl;
														main_repo.shoulderFlexion(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
														//										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
														enableReposButton = true;
														//}

														// Left 
														break;
											 }
											 case 1: {
														 // Right
												 double angle = flexion_angle;

												 std::cout << "shoulder flexionj starts:" << std::endl;
												 main_repo.rightShoulderFlexion(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
												 //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
												 enableReposButton = true;
														 break;
											 }
											 }
											 break;
								  }
								  case 1: {
											  // Right

									  double angle = flexion_angle;

//									  main_repo.rightShoulderFlexion(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
									  //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									  enableReposButton = true;

											  break;
								  }
								  }
								  break;
					   }

					   case 1:{
								  // Lateral Flexion

								  switch (reposSide){
								  case 0:{
											 // Left 
										  double angle = flexion_angle;

											std::cout << "shoulder flexionj starts:" << std::endl;
											  main_repo.shoulderLateralFlexion(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
											  //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
											enableReposButton = true;

									  
											 break;
								  }
								  case 1: {
											  // Right

									  double angle = flexion_angle;

									  std::cout << "shoulder flexionj starts:" << std::endl;
									  main_repo.rightShoulderLateralFlexion(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
									  //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									  enableReposButton = true;
											  break;
								  }
								  }

								  break;
					   }

					   case 2:{
								  // Twisting

								  switch (reposSide){
								  case 0:{
											 // Left 
									  double angle = flexion_angle;

									  std::cout << "shoulder flexionj starts:" << std::endl;
									  main_repo.shoulderTwist(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
									  //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									  enableReposButton = true;
									  //}
											 break;
								  }
								  case 1: {
											  // Right
									  double angle = flexion_angle;

									  std::cout << "shoulder flexionj starts:" << std::endl;
									  main_repo.rightShoulderTwist(angle, fem1, fem, m_meta, tmpPath);   /// Bone Repositioning
									  //										main_repo.RightWristFlexionContourCL(angle, fem1, m_meta, tmpPath);  ///main repo
									  enableReposButton = true;
											  break;
								  }
								  }
								  break;
					   }

					   }
					   break;
			}

			}

		}



		void ContourDeformation::performDelaunnay(){
			double flexion_angle = reposAngle;
			switch (reposFunc){

			case 1:{
					   // Knee
					   switch (reposType){

					   case 0:{
								  // Flexion
								  switch (reposSide){

								  case 0:{
											 if (m_meta.hasGenericmetadata("Flesh_left_leg")){
												 pInfo() << "Perform Delaunnay : Flexion of left Knee";

												 pDebug() << "Contour temp dir path " << tmpPath;
												 main_repo.Delaunay_Left_Leg_Repositioning(fem1, m_meta,
													 MAPPING_REPOSITIONING, tmpPath);
											 }
											 else{
												 pCritical() << "Left_full_leg genericmetadata is not prsent ";
											 }

											 break;
								  }

								  case 1:{
											 pInfo() << "Flexion of right Knee";
											 if (m_meta.hasGenericmetadata("Right_full_leg")){
												 pInfo() << "Perform Delaunnay : Flexion of right Knee";
												 main_repo.DelaunayRightLegFlexion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }
											 else{
												 pCritical() << "Right_full_leg genericmetadata is not prsent ";
											 }

											 break;
								  }

								  }
								  break;
					   }

					   }
					   break;
			}

			case 2:{
					   // Ankle
					   switch (reposType){

					   case 0:{
								  //Flexion 
								  switch (reposSide){

								  case 0:{
                                             main_repo.Delaunay_Left_Foot_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 
											 break;
								  }

								  case 1:{


									  main_repo.Delaunay_Right_Foot_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											 break;
								  }

								  }
								  break;
					   }

					   case 2:{
								  //Twisting
								  switch (reposSide){

								  case 0:{

									  main_repo.Delaunay_Left_Foot_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
								 
											 break;
								  }

								  case 1:{


									  main_repo.Delaunay_Right_Foot_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);



											 break;
								  }

								  }
								  break;
					   }


					   }
					   break;
			}

			case 3:{
					   // Hip 
					   float angle = flexion_angle;



					   switch (reposType){

					   case 0:{
								  //Flexion
								  switch (reposSide){

								  case 0:{
									  
											 VId sidnodes, tempselector;
											 VId hip_test;
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;
												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
												 pInfo() << "size of hip :" << sidnodes.size();
												 pInfo() << "size of leg :" << tempselector.size();
												 ////-11.9441, -107.935, -13.8835, -27.0299, -105.086, -59.8361, 8.68459, -111.163, -90.4454, 60.4249, -122.947, -81.2431
												 // std::vector<hbm::NodePtr>
												/* NodePtr p1(new Node);
												 p1->getCoordX() = -11.941;
												 p1->getCoordY() = 0; 
												 p1->getCoordZ() = -13.8835;
												 
												 NodePtr p2(new Node);
												 p2->getCoordX() = -27.023;	 p2->getCoordY() = 0; p2->getCoordZ() = -59.8361;

												 NodePtr p3(new Node);
												 p3->getCoordX() = 8.68;	 p3->getCoordY() = 0; p3->getCoordZ() = -111.16;

												 NodePtr p4(new Node);
												 p4->getCoordX() = 60.4249;	 p4->getCoordY() = 0; p4->getCoordZ() = -81.2461;
												 vec_spline_new.clear();
												 vec_spline_new.push_back(p1);
												 vec_spline_new.push_back(p2);
												 vec_spline_new.push_back(p3);
												 vec_spline_new.push_back(p4);
												 main_repo.ReposContourHip2(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
												*/
												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);

												 pInfo() << "Left Hip Repositioning ends for angle " << flexion_angle;
											 
									  std::cout << "finished";
											 break;
								  }

								  case 1:{

											 VId sidnodes, tempselector;
											 VId hip_test;
											 if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set entity";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip entity";
											 }
											 else{
												 pInfo() << "Right Hip Repositioning starts for angle " << flexion_angle;

												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg_hip").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());

												 main_repo.ReposContourHipRightFlexion(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);

												 pInfo() << "Right Hip Repositioning ends for angle " << flexion_angle;
											 }
											 break;
								  }

								  }
								  break;
					   }

					   case 1:{
								  //Abduction
								  switch (reposSide){

								  case 0:{
											 VId sidnodes, tempselector;
											 VId hip_test;
											 if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set entity";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip entity";
											 }
											 else{
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;

												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg").getGMNodesIds(fem, tempselector);
												 pInfo() << "size of hip :" << sidnodes.size();
												 pInfo() << "size of leg :" << tempselector.size();
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
//												 main_repo.ReposContourHipAbductionLeft(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);

//												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);
//
												 pInfo() << "Left Hip Repositioning ends for angle " << flexion_angle;
											 }
											 break;
								  }

								  case 1:{
											 VId sidnodes, tempselector;
											 VId hip_test;
											 if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set entity";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip entity";
											 }
											 else{
												 pInfo() << "Right Hip Repositioning starts for angle " << flexion_angle;

												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg_hip").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
												 main_repo.ReposContourHipAbductionRight(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);

												 pInfo() << "Right Hip Abduction ends for angle " << flexion_angle;
											 }

											 break;
								  }

								  }
								  break;
					   }

					   case 2:{
								  //Twisting
								  switch (reposSide){

								  case 0:{
											 pInfo() << "Twisting of left Hip";
											 VId sidnodes, tempselector;
											 VId hip_test;
											 if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set entity";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip entity";
											 }
											 else{
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;

												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg_hip").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
												 main_repo.ReposContourHipTwistLeft(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);
												 pInfo() << "Left Hip Twisting ends for angle " << flexion_angle;

											 }
											 break;
								  }

								  case 1:{
											 pInfo() << "Twisting of right Hip";
											 VId sidnodes, tempselector;
											 VId hip_test;
											 if (!m_meta.hasGenericmetadata("hip_part_set"))
											 {
												 pCritical() << "Missing hip_part_set entity";
											 }
											 else if (!m_meta.hasGenericmetadata("lower_full_leg_hip"))
											 {
												 pCritical() << "Missing lower_full_leg_hip entity";
											 }
											 else{
												 pInfo() << "Left Hip Repositioning starts for angle " << flexion_angle;

												 m_meta.genericmetadata("hip_part_set").getGMNodesIds(fem, sidnodes);
												 m_meta.genericmetadata("lower_full_leg_hip").getGMNodesIds(fem, tempselector);
												 hip_test.insert(hip_test.end(), tempselector.begin(), tempselector.end());
												 hip_test.insert(hip_test.end(), sidnodes.begin(), sidnodes.end());
												 main_repo.ReposContourHipRightTwisting(angle, vec_spline_new, sidnodes, tempselector, fem, fem1, &m_meta.map_mod_coord, m_meta, tmpPath);
												 main_repo.DelaunayPersonalizationHip(&hip_test, fem1, &m_meta.map_mod_coord, MAPPING_REPOSITIONING, tmpPath);

												 pInfo() << "Left Hip Twisting ends for angle " << flexion_angle;
											 }
											 break;
								  }

								  }
								  break;
					   }

					   }
					   break;
			}

			case 4:{
					   // Neck
					   switch (reposType){

					   case 0:{
								  //Flexion
								  pInfo() << "Flexion of neck";
								  main_repo.Delaunay_neck_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay

//								  main_repo.Delaunay_neck_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
//								  main_repo.Delaunay_neck_twisting(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
								  break;
					   }

					   case 2:{
								  //Twisting
								  if (!m_meta.hasGenericmetadata("Flesh_Head_Neck"))
								  {
									  pCritical() << "Missing Flesh_Head_Neck genericmetadata";
								  }
								  else{
									  pInfo() << "Twisting of neck";
									  main_repo.Delaunay_neck_Repositioning(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay

//									  main_repo.Delaunay_neck_twisting(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay
								  }
								  break;
					   }

					   case 1:{
								  //Lateral Flexion
								  if (!m_meta.hasGenericmetadata("Flesh_Head_Neck"))
								  {
									  pCritical() << "Missing Flesh_Head_Neck genericmetadata";
								  }
								  else{
									  pInfo() << "Lateral Flexion of neck";
//									  main_repo.Delaunay_neck_lateral(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);   /// neck repositioning through delaunnay
								  }
								  break;
					   }

					   }
					   break;
			}

			case 5:{
					   // Elbow
					   switch (reposType){

					   case 0:{
								  //Flexion
								  switch (reposSide){

								  case 0:{
												 pInfo() << "Flexion of left Elbow";
//												 main_repo.DelaunayLeftElbow(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
												 main_repo.DelaunayLeftElbow12(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											 break;
								  }

								  case 1:{
											 if (!m_meta.hasGenericmetadata("Right_arm_Flesh"))
											 {
												 pCritical() << "Missing Right_arm_Flesh genericmetadata";
											 }
											 else{
												 pInfo() << "Flexion of Right Elbow";
//												 main_repo.DelaunayRightElbow(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
												 main_repo.DelaunayRightElbow12(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }

											 break;
								  }

								  }
								  break;
					   }

					   case 2:{
								  // Twisting
								  switch (reposSide){

								  case 0:{
											 if (!m_meta.hasGenericmetadata("Left_arm_Flesh"))
											 {
												 pCritical() << "Missing Left_arm_Flesh genericmetadata";
											 }
											 else{
												 pInfo() << "Twisting of left Elbow";
												 main_repo.DelaunayLeftElbowTwist(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }
											 break;
								  }

								  case 1:{
											 if (!m_meta.hasGenericmetadata("Right_arm_Flesh"))
											 {
												 pCritical() << "Missing Right_arm_Flesh genericmetadata";
											 }
											 else{
												 pInfo() << "Twisting of right Elbow";
												 main_repo.DelaunayRightElbowTwist(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }
											 break;
								  }

								  }
								  break;
					   }

					   }
					   break;
			}

			case 6:{
					   // Wrist
					   switch (reposType){

					   case 0:{
								  // Flexion
								  switch (reposSide){

								  case 0:{
											 if (!m_meta.hasGenericmetadata("Left_hand_flesh")){
												 pInfo() << ERRORMSG << "Missing Left_hand_flesh genericmetadata" << endl;
											 }
											 else{
												 pInfo() << "Flexion of left Wrist";
												 main_repo.DelaunayLeftWrist(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }


											 break;
								  }

								  case 1:{

											 if (!m_meta.hasGenericmetadata("Right_hand_flesh")){
												 pInfo() << ERRORMSG << "Missing Right_hand_flesh genericmetadata" << endl;
											 }
											 else{
												 pInfo() << "Flexion of right Wrist ";
												 main_repo.DelaunayRightWrist(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 }

											 break;
								  }

								  }
								  break;
					   }

					   case 1:{
								  // Lateral Flexion
								  switch (reposSide){

								  case 0:{
											 pInfo() << "Lateral Flexion of left Wrist";

											 break;
								  }

								  case 1:{
											 pInfo() << "Lateral Flexion of right Wrist ";

											 break;
								  }

								  }
								  break;
					   }

					   }
					   break;
			}

			case 7:{
					   // Thorax
					   switch (reposType){

					   case 0:{
								  // Flexion
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

								  break;
					   }
					   case 1:{
								  // Lateral Flexion
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
								  break;
					   }
					   case 2:{
								  // Twisting
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

								  break;
					   }
					   }
					   break;
			}

			case 8:{
					   // Lumbar
					   switch (reposType){

					   case 0:{
								  // Flexion
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

								  break;
					   }
					   case 1:{
								  // Lateral Flexion
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
								  break;
					   }
					   case 2:{
								  // Twisting
								  main_repo.Delaunay_lumbarmotion(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
								  break;
					   }
					   }
					   break;
			}

			case 9:{
					   // Shoulder
					   switch (reposType){

					   case 0:{
								  // Flexion
								  switch (reposSide){
								  case 0:{
											 // Left
											 main_repo.Delaunay_Shoulder(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											 break;
								  }
								  case 1: {
											  // Right
											 main_repo.Delaunay_Shoulder_right(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											  break;
								  }
								  }
								  break;
					   }

					   case 1:{
								  // Lateral Flexion

								  switch (reposSide){
								  case 0:{
											 // Left 
									  main_repo.Delaunay_Shoulder(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											 break;
								  }
								  case 1: {
											  // Right
									  main_repo.Delaunay_Shoulder_right(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);
											  break;
								  }
								  }

								  break;
					   }

					   case 2:{
								  // Twisting

								  switch (reposSide){
								  case 0:{
											 // Left 
											  main_repo.Delaunay_Shoulder(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											 break;
								  }
								  case 1: {
											  // Right
									  main_repo.Delaunay_Shoulder_right(fem1, m_meta, MAPPING_REPOSITIONING, tmpPath);

											  break;
								  }
								  }
								  break;
					   }

					   }
					   break;
			}

			}
		}




		void ContourDeformation::applyState(){

			//mod_elt_bar.clear();
			//mod_vtriangles.clear();
			//mod_vquadtriangles.clear();

			FEModel const& fem = Context::instance().project().model().fem(); //shortcut
			FEModel& fem1 = Context::instance().project().model().fem(); //shortcut

			Metadata& m_meta = Context::instance().project().model().metadata(); //shortcut

			double cordiX, cordiY, cordiZ;
			sidnode = m_meta.setids;
			for (auto iter = sidnode.begin(); iter != sidnode.end(); iter++){
				cordiX = fem1.get<Node>(*iter)->getCoordX();
				cordiY = fem1.get<Node>(*iter)->getCoordY();
				cordiZ = fem1.get<Node>(*iter)->getCoordZ();

				double u1 = m_meta.map_mod_coord[*iter].modx;
				double u2 = m_meta.map_mod_coord[*iter].mody;
				double u3 = m_meta.map_mod_coord[*iter].modz;
				double a1, a2, a3;

				if (!MODIFIED_STATE){

					NodePtr newnodem;;
					Id idm = newnodem->getId(); //piper id attributed to the new node
					newnodem->setCoord(u1, u2, u3);

					m_meta.map_mod_coord[*iter].modx = fem1.get<Node>(*iter)->getCoordX();
					m_meta.map_mod_coord[*iter].mody = fem1.get<Node>(*iter)->getCoordY();
					m_meta.map_mod_coord[*iter].modz = fem1.get<Node>(*iter)->getCoordZ();

					a1 = newnodem->getCoordX();
					a2 = newnodem->getCoordY();
					a3 = newnodem->getCoordZ();

					fem1.get<Node>(*iter)->setCoord(a1, a2, a3);
				}
			}
			m_meta.setids.clear();
			m_meta.map_mod_coord.clear();
		}

		void ContourDeformation::personalize(std::vector<double> vals, std::string bodyDataFile){
			//std::cout << "personalize called with bodydata file url = "+bodyDataFile;
			FEModel const& fem = Context::instance().project().model().fem(); //shortcut
			FEModel& fem1 = Context::instance().project().model().fem(); //shortcut

			Metadata& m_meta = Context::instance().project().model().metadata(); //shortcut

			pInfo() << "Personalization starts";
			pInfo() << "size " << m_meta.cont_det.contour_map.size();

			main_repo.Personalization(fem, &m_meta.m_hbm_contours, m_meta.cont_det, vals, m_meta, bodyDataFile);

			//	act.Personalization2(&sidnode, fem, fem1, m_meta, &contour_poly, &m_meta.m_hbm_contours, contour_plane_new, &m_meta.map_mod_coord, m_meta.cont_det, vals);
			std::set<Id> totnodes;
			Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();
			for (Metadata::EntityCont::const_iterator it = entities.begin(); it != entities.end(); ++it) {
				Entity const& entity = Context::instance().project().model().metadata().entity(it->second.name());
				std::set<Id> sidnodes; //stores node ids in a set
				sidnodes = repo.getnodesbypart(entity, sidnodes, fem);
				totnodes.insert(sidnodes.begin(), sidnodes.end());
			}
			cout << totnodes.size() << std::endl;

			sidnode = totnodes;

			main_repo.DelaunayPersonalization(&sidnode, fem1, &m_meta.map_mod_coord, MAPPING_PERSONALIZATION);
			//	act.delaunaypersona(flexion_angle, &sidnode, fem, fem1, m_meta, &contour_poly, &m_meta.m_hbm_contours, contour_plane_new, &m_meta.map_mod_coord, MAPPING_PERSONALIZATION);
			m_meta.setids.clear();
			//	m_meta.setids = totnodes;
			contour_poly.clear();
			m_meta.m_hbm_contours.clear();
			contour_plane_new.clear();
			pInfo() << "Parallelization ends";

			MODIFIED_STATE = true;
		}


		bool ContourDeformation::ui_validateBodyDataFile(const QUrl& modelFile){
			std::string filename = modelFile.toLocalFile().toStdString();
			ifstream infile(filename);
			if (infile.good())
			{
				string sLine;
				getline(infile, sLine);
				infile.close();
				if (sLine == "PERSONALIZATION-DATA"){
					pInfo() << "Valid BodyData file." << endl;
					return true;
				}
				else{
					pInfo() << "The file selected is not a BodyData file." << endl;
					return false;
				}
			}
			else
				return false;
		}

		bool ContourDeformation::ui_validateFile(const QUrl& modelFile, QString starttext){
			std::string filename = modelFile.toLocalFile().toStdString();
			ifstream infile(filename);
			if (infile.good())
			{
				string sLine;
				getline(infile, sLine);
				infile.close();
				if (sLine == starttext.toStdString()){
					pInfo() << filename << " loaded successfully." << endl;
					return true;
				}
				else{
					pInfo() << "The file selected is not a BodyData file." << endl;
					return false;
				}
			}
			else
				return false;
		}


		QVariantMap ContourDeformation::ui_populateGebodPercentileMap(const QUrl& modelFile){
			QVariantMap map;
			std::vector<std::string> keys;
			QList<double> values[4];
			//QVariantMap map;
			std::cout << "\n\nui_populateGebodPercentileMap called !";
			std::string filename = modelFile.toLocalFile().toStdString();
			std::cout << filename << std::endl;
			std::string line;
			std::ifstream infile(filename.c_str());
			bool readingData = false;
			int lineCount = 0;
			while (std::getline(infile, line))
			{
				if (line == "*")
					break;
			}
			std::getline(infile, line);
			std::istringstream iss(line);
			do{
				std::string sub;
				iss >> sub;
				keys.push_back(sub);
			} while (iss);
			while (std::getline(infile, line))
			{
				double d1, d2, d3;// , d4;
				std::istringstream iss2(line);
				iss2 >> d1 >> d2 >> d3;// >> d4;
				std::cout << d1 << "\t" << d2 << "\t" << d3 << "\t" << endl;// << d4 << endl;
				values[0].append(d1);
				values[1].append(d2);
				values[2].append(d3);
				//values[4].append(d4);
				if (line == "#")	break;
			}
			map.insert(QString::fromStdString(keys.at(0)), QVariant::fromValue(values[0]));
			map.insert(QString::fromStdString(keys.at(1)), QVariant::fromValue(values[1]));
			map.insert(QString::fromStdString(keys.at(2)), QVariant::fromValue(values[2]));
			//map.insert(QString::fromStdString(keys.at(3)), QVariant::fromValue(values[3]));
			infile.close();
			return map;
		}

		bool ContourDeformation::ui_loadContourCLXml(const QUrl& modelFile){
			m_meta.ctrl_line.undefinedCLElements.clear();
			std::string file = modelFile.toLocalFile().toStdString();
			pskeleton = ContourGen::getInstance();
			pskeleton->Init();  // set display mode and refresh actors
			//Context::instance().performAsynchronousOperation(std::bind(&AnthropometryModule::doCreateTargets, this, userInputArray), false, false, false, false, false);
			if (main_repo.is_file_exist(file.c_str())){
                if (pskeleton->ReadContourCL(file) == tinyxml2::XML_SUCCESS)
				{
					pskeleton->ComputeSkeleton();
					//Mark All regions as unprocessed when contours are loaded
					//                    m_tsynth.markUnprocessed(m_meta.ctrl_line.root);
				}
                else
                {
                    pCritical() << "file parsing error";
                    return false;
                }
			}
			else{
				pCritical() << "file does not exist";
                return false;
			}

			if (m_meta.hasEntity("Skin_of_neck") && m_meta.hasEntity("Skin_of_head"))
				createSkinHeadNeck();
            return true;
		}

		void ContourDeformation::ui_loadAnthropometicTargetFile(const QUrl& modelFile){
			std::string type, name, value;
			std::string file = modelFile.toLocalFile().toStdString();
			std::cout << " ui_loadAnthropometicTargetFile() called for file : " << file << std::endl;
			tinyxml2::XMLDocument xmlDoc;
			tinyxml2::XMLError eResult = xmlDoc.LoadFile(file.c_str());
			if (eResult == tinyxml2::XML_SUCCESS){
				tinyxml2::XMLNode * pRoot = xmlDoc.FirstChild();
				tinyxml2::XMLElement * pElement = pRoot->FirstChildElement("targetList");
				while (pElement != NULL){
					pElement = pRoot->FirstChildElement("target");
					type = pElement->Attribute("type");
					name = pElement->Attribute("name");
					value = pElement->Attribute("value");
					if (type == "AnthropometricDimension")
						std::cout << "Target : " << name << "\t Value : " << value << std::endl;
					pElement = pElement->NextSiblingElement("target");
				}
			}
			else{
				std::cout << "Something went wrong here !";
			}
		}

		///Parse and Display vtk based skeleton
		bool ContourDeformation::DisplaySkeleton()
		{
			bool ret_status = false;
			pskeleton = ContourGen::getInstance();
			pskeleton->Init();
			ret_status = pskeleton->ComputeSkeleton();
			return ret_status;
		}

		///Hide vtk based skeleton
		void ContourDeformation::HideSkeleton()
		{
			pskeleton = ContourGen::getInstance();
			pskeleton->RemoveSkeletonActors();
		}

		void ContourDeformation::setScale(float value, QString CircumElement)
		{
			pskeleton = ContourGen::getInstance();
			pskeleton->SetScalingValue(value, CircumElement.toStdString());
		}

		QVariantMap ContourDeformation::ui_setGebodVals(){
			QVariantMap map;
			if (Context::instance().project().target().anthropometricDimension.size() > 0){
				for (auto it = Context::instance().project().target().anthropometricDimension.begin(); it != Context::instance().project().target().anthropometricDimension.end(); ++it){

					//replacing '-' with '_' in target names
					std::string name = (*it).name();
					std::replace(name.begin(), name.end(), '-', '_');

					map.insert(QString::fromUtf8(name.c_str()), QString::number((*it).value()));
				}
			}
			else
				pInfo() << "Target dimensions not generated";
			return map;
		}

		QVariantMap ContourDeformation::ui_setTargetVals(){
			return targetMap;
		}

		std::string ContourDeformation::convertUnits(tinyxml2::XMLElement* element){
			tinyxml2::XMLElement* units = element->FirstChildElement("name")->NextSiblingElement("units");
			if (units->Attribute("length")){
				std::string type = units->Attribute("length");
				if (type.compare("mm") == 0){
					float value = std::stof(units->NextSiblingElement("value")->GetText());
					return std::to_string(value / 1000);
				}
				else{
					return units->NextSiblingElement("value")->GetText();
				}
			}
			else{
				return units->NextSiblingElement("value")->GetText();
			}
		}

		//		ACROMION_HT – SCYE_CIRC_OVER_ACROMION / pi



		void ContourDeformation::openTargetFile(std::string const& filePath){
			if (!Context::instance().project().model().empty()) {
				map<std::string, std::string> targets;
				targets["NECK_CIRC_OVER_LARYNX"] = "NECK_CIRCUMFERENCE";
				targets["ARMCIRCBCPS_FLEX"] = "BICEPS_CIRCUMFERENCE";
				targets["ELBOW_CIRC_EXTENDED"] = "ELBOW_CIRCUMFERENCE";
				targets["FOREARM_CIRC_FLEXED"] = "FOREARM_CIRCUMFERENCE";
				targets["WRIST_CIRC_STYLION"] = "WRIST_CIRCUMFERENCE";
				targets["THIGH_CIRC_DISTAL"] = "UPPER_LEG_CIRCUMFERENCE";
				targets["THIGH_CIRC_PROXIMAL"] = "THIGH_CIRCUMFERENCE";
				targets["KNEE_CIRC"] = "KNEE_CIRCUMFERENCE";
				targets["CALF_CIRC"] = "CALF_CIRCUMFERENCE";
				targets["ANKLE_CIRC"] = "ANKLE_CIRCUMFERENCE";
				targets["WEIGHT"] = "WEIGHT";
				targets["STATURE"] = "STANDING_HEIGHT";
				targets["ACROMION_HT"] = "SHOULDER_HEIGHT";
				targets["AXILLA_HT"] = "ARMPIT_HEIGHT";
				targets["WAIST_HT_OMPHALION"] = "WAIST_HEIGHT";
				targets["SITTING_HT"] = "SEATED_HEIGHT";
				targets["HEAD_LNTH"] = "WRIST_TO_INDEX_FINGER_LNTH";
				targets["HEAD_BRTH"] = "HEAD_BREADTH";
				targets["HEAD_CIRC"] = "HEAD_TO_CHIN_HEIGHT";
				targets["BIACROMIAL_BRTH"] = "SHOULDER_BREADTH";
				targets["CHEST_DEPTH"] = "CHEST_DEPTH";
				targets["CHEST_BRTH"] = "CHEST_BREADTH";
				targets["WAIST_DEPTH_OMPHALION"] = "WAIST_DEPTH";
				targets["WAIST_BRTH_OMPHALION"] = "WAIST_BREADTH";
				targets["BUTT_DEPTH"] = "BUTTOCK_DEPTH";
				targets["HIP_BRTH"] = "HIP_BREADTH_STANDING";
				targets["SHOULDER_ELBOW_LNTH"] = "SHOULDER_TO_ELBOW_LENGTH";
				targets["FOREARM_HAND_LENTH"] = "FOREARM_HAND_LENGTH";
				targets["KNEE_HT___SITTING"] = "KNEE_HEIGHT_SEATED";
				targets["LATERAL_MALLEOUS_HT"] = "ANKLE_HEIGHT_OUTSIDE";
				targets["FOOT_BRTH"] = "FOOT_BREADTH";
				targets["FOOT_LNTH"] = "FOOT_LENGTH";
				targets["HAND_BRTH_AT_METACARPALE"] = "HAND_BREADTH";
				targets["HAND_LNTH"] = "HAND_LENGTH";
				targets["HAND_CIRC_AT_METACARPALE"] = "HAND_DEPTH";
				targets["SCYE_CIRC_OVER_ACROMION"] = "ACR1";
				targets["WAIST_HT_UMBILICUS_SITTING"] = "ACR2";
				targets["AB_EXT_DEPTH_SIT"] = "ACR4";
				targets["CALF_HT"] = "ACR5";
				targets["NECK_TO_BUSTPOINT_LNTH"] = "ACR6";
				targets["SHOULDER_LNTH"] = "ACR7";
				pInfo() << START << QStringLiteral("Loading Targets ...");
				FEModel& m_fem = Context::instance().project().model().fem();
				tinyxml2::XMLDocument model;
				model.LoadFile(filePath.c_str());
				if (model.Error()) {
					std::stringstream str;
                    str << "Failed to load model file: " << filePath << std::endl << model.ErrorStr() << std::endl;
					throw std::runtime_error(str.str().c_str());
				}

				tinyxml2::XMLElement* target;
				target = model.FirstChildElement("piper-target")->FirstChildElement("targetList")->FirstChildElement("target");
				std::ofstream newmap("newmap.txt");
				while (target != nullptr) {
					tinyxml2::XMLElement* element = target->FirstChildElement("name");
					std::string name = element->GetText();
					std::replace(name.begin(), name.end(), ' ', '_');//replacing space with '_' in target names
					std::replace(name.begin(), name.end(), '-', '_');//replacing '-' with '_' in target names
					std::string value = convertUnits(target);
					if (targets.find(name) == targets.end()) {
					}
					else{
						targetMap.insert(QString::fromUtf8(targets[name].c_str()), QString::fromUtf8(value.c_str()));
						newmap << targets[name].c_str() << "  " << value.c_str() << std::endl;
					}
					target = target->NextSiblingElement("target");
				}


				std::cout << "************************target maps***************************" << std::endl;
				for (std::map<string, string>::iterator it = targets.begin(); it != targets.end(); ++it){
					std::cout << it->first << " => " << it->second << '\n';
				}

				std::cout << "************************targetMap for QVarient***************************" << std::endl;
				for (QVariantMap::const_iterator iter = targetMap.begin(); iter != targetMap.end(); ++iter) {
					std::cout << iter.key().toStdString() << " -  " << iter.value().toDouble() << std::endl;
				}


				pInfo() << DONE;
			}
		}

		double ContourDeformation::ui_getJtAngle(QString name){
			ContourCLj* tempj = getJoint(name.toStdString());
			computeJtAngles(name.toStdString());
			if (tempj != NULL)
				return tempj->jointangle;
			else
				return -900;
		}
		double ContourDeformation::ui_getBrAngle(QString name){
			ContourCLbr* tempbr = getBodyRegion(name.toStdString());
			computeBrAngles(name.toStdString());
			if (tempbr != NULL)
				return tempbr->brangle;
			else
				return -900;
		}

		bool ContourDeformation::ui_getEnableReposButton(){ return enableReposButton; }
		void ContourDeformation::ui_setEnableReposButton(bool value){ enableReposButton = value; }

		bool ContourDeformation::ui_getHipProcess(){
			pInfo() << hip_process;
			return hip_process;
		}

		void ContourDeformation::ui_markUnprocessed(){
			if (m_meta.ctrl_line.root != nullptr)
				m_tsynth.markUnprocessed(m_meta.ctrl_line.root);
		}

		//MODEL HISTORY
		void ContourDeformation::doSetResultinHistory(std::string const& historyName) {
			HumanBodyModel& hbmref = Context::instance().project().model();
			m_nodes = piper::hbm::Nodes(hbmref.fem().getNodes());
			pInfo() << START << "Save model in history ";
			FEModel& femodel = Context::instance().project().model().fem(); //shortcut
			Context::instance().addNewHistory(QString::fromStdString(historyName));
			Context::instance().display().RemoveNonpersistentActors();
			// copy new nodes coordinates in the current model in history
			if (m_nodes.size() > 0) {
				for (auto it = m_nodes.begin(); it != m_nodes.end(); ++it)
					femodel.getNode((*it)->getId()).set((*it)->get());
			}
			femodel.updateVTKRepresentation();
			std::string curmodel = Context::instance().currentModel().toStdString();
			pInfo() << DONE << "Model after Contour deformation is saved in history as " << curmodel;
			emit Context::instance().modelUpdated();
		}

		void ContourDeformation::setResultinHistory(QString const& historyName) {
			void(*setResultinHistoryWrapper)(piper::contourdeformation::ContourDeformation*, void (piper::contourdeformation::ContourDeformation::*)(std::string const&), std::string const&)
				= &piper::wrapClassMethodCall < piper::contourdeformation::ContourDeformation, void(piper::contourdeformation::ContourDeformation::*)(std::string const&), std::string const& >;

			Context::instance().performAsynchronousOperation(std::bind(setResultinHistoryWrapper,
				this, &piper::contourdeformation::ContourDeformation::doSetResultinHistory, historyName.toStdString()),
				false, false, false, false, false);
		}

		void ContourDeformation::ui_drawSpline(double p0x, double p0y, double p0z, double p1x, double p1y,
            double p1z, double t1x, double t1y, double t1z, double t2x, double t2y, double t2z){
			std::cout << "ui_drawSpline called ";
			ContourGen* pskeleton = ContourGen::getInstance();
			pskeleton->Init();
			MatrixXd splineMatrix(4, 3);
			splineMatrix << p0x, p0y, p0z,
				p1x, p1y, p1z,
				t1x, t1y, t1z,
				t2x, t2y, t2z;
			std::cout << "Calling drawSpline";
			pskeleton->drawSpline(splineMatrix);

		}

		void ContourDeformation::ui_initializeTargetsBusyIndicator(){
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::initializeTargets, this ), false, false, false, false, false);
		}
		
		void ContourDeformation::initializeTargets(){
			m_tsynth.init();
			processNextTarget();
		}
		
		void ContourDeformation::ui_processNextTargetBusyIndicator(){
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::processNextTarget, this), false, false, false, false, false);
		}

		void ContourDeformation::processNextTarget(){
			int currentElementIndex = m_tsynth.currentElementStatus.first;
			std::string currentElement = m_tsynth.CLElementVector.at(currentElementIndex);
			// Checking if all the available joints/body regions have been processed
			if (currentElementIndex >= m_tsynth.CLElementVector.size()){
				m_tsynth.reposParameters = { -1.0, -1.0, -1.0, -1.0, -1.0, -1.0 };
				return;
			}
			// Calculating next set of repositioning parameters
			m_tsynth.calculateReposParameters();
			// Updating the status of next repositioning to be processed
			int currentDofIndex = m_tsynth.reposOrder[currentElement].at(m_tsynth.currentElementStatus.second);
			if (currentDofIndex+1 < m_tsynth.reposOrder[currentElement].size())
				m_tsynth.updateElementStatus(currentElementIndex, currentDofIndex+1);
			else{
				m_tsynth.updateElementStatus(currentElementIndex+1, 0);
			}
			// Checking if the calculated angle is less than the threshold, if yes then moving on to the next repositioning 
			double threshold_angle = 3.0;
			double angleCalculated = fabs(m_tsynth.reposParameters.at(3));
			if (angleCalculated < threshold_angle)
				processNextTarget();
			else
				reposAngle = m_tsynth.reposParameters.at(3);
		}

		QList<double> ContourDeformation::ui_getReposParameters(){
			QList<double> list;
			std::vector<double> reposParams = m_tsynth.getReposParameters();
			for (int i = 0; i < reposParams.size(); i++)
				list.append(reposParams.at(i));
			return list;
		}

		void ContourDeformation::ui_TestTargetAngleComputation(int currentElementIndex, int currentDofIndex){
			//std::cout << "\n ui_TestTargetAngleComputation called !" << currentElementIndex << "\t" << currentDofIndex;
			Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::TestTargetAngleComputation, this, currentElementIndex, currentDofIndex), false, false, false, false, false);

		}
		
		void ContourDeformation::TestTargetAngleComputation(int currentElementIndex, int currentDofIndex){
			//std::cout << "\nTestTargetAngleComputation called !" << currentElementIndex << "\t" << currentDofIndex;
			m_tsynth.hardcodingStuff();
			m_tsynth.updateElementStatus(currentElementIndex, currentDofIndex );
			m_tsynth.calculateReposParameters();
        }


		void ContourDeformation::createSkinHeadNeck(){
			std::cout << "\n Creating the Skin_head_neck entity";
			Entity& head = m_meta.entity("Skin_of_head");
			Entity& neck = m_meta.entity("Skin_of_neck");

			VId vid1D, vid2D, vid3D;

			std::vector <std::string> selectedActorNames = { "Skin_of_head", "Skin_of_neck" };
			SId sId1D, sId2D, sId3D;
			if (selectedActorNames.size() > 0){
				for (auto it = selectedActorNames.begin(); it != selectedActorNames.end(); ++it){
					VId ge1D = m_meta.entity(*it).get_groupElement1D();
					VId ge2D = m_meta.entity(*it).get_groupElement2D();
					VId ge3D = m_meta.entity(*it).get_groupElement3D();

					for (auto it = ge1D.begin(); it != ge1D.end(); ++it)
						sId1D.insert(fem.getGroupElements1D(*it).get().begin(), fem.getGroupElements1D(*it).get().end());
					for (auto it = ge2D.begin(); it != ge2D.end(); ++it)
						sId2D.insert(fem.getGroupElements2D(*it).get().begin(), fem.getGroupElements2D(*it).get().end());
					for (auto it = ge3D.begin(); it != ge3D.end(); ++it)
						sId3D.insert(fem.getGroupElements3D(*it).get().begin(), fem.getGroupElements3D(*it).get().end());
				}

				for (auto it = sId1D.begin(); it != sId1D.end(); ++it)
					vid1D.push_back(*it);
				for (auto it = sId2D.begin(); it != sId2D.end(); ++it)
					vid2D.push_back(*it);
				for (auto it = sId3D.begin(); it != sId3D.end(); ++it)
					vid3D.push_back(*it);
			}
            MetaEditor metaEditor;
			metaEditor.addEntity("Skin_head_neck", vid1D, vid2D, vid3D, 0);
		}

		void ContourDeformation::createLandmarkTargetFile(){
			if (!Context::instance().project().model().empty()) {
				FEModel& m_fem = Context::instance().project().model().fem();
				Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();
				string name;
				ofstream file;
				file.open("new_target_file.ptt");
				file << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
				file << "<piper-target version=\"0.6\">\n";
				file << "\t<targetList>\n";
				for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it) {
					std::string name = it->second.name();
					Coord coorda = m_meta.landmark(it->second.name()).position(m_fem);
					file << "\t\t<target type=\"Landmark\">\n";
					file << "\t\t\t<name>" << name << "</name>\n";
					file << "\t\t\t<units length=\"mm\"/>\n";
					file << "\t\t\t<value dof=\"x\">"<< coorda[0] <<"</value>\n";
					file << "\t\t\t<value dof=\"y\">" << coorda[1] << "</value>\n";
					file << "\t\t\t<value dof=\"z\">" << coorda[2] << "</value>\n";
					file << "\t\t\t<landmark>" << name << "</landmark>\n";
					file << "\t\t</target>\n";
				}
				file << "\t</targetList>\n";
				file << "</piper-target>";
				file.close();
			}
		}
	}
}

