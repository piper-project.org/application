// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
       id : root
       minimumHeight : 600
       minimumWidth : 900
       title : " Personalization module UI information "

       TextArea {
                       id:textArea
                       anchors.fill: parent
                       text:"\t\t\t\tPersonalization module UI information "+
                           "\nLoad Personalization Contours : "+
                           "\nUser needs to load the contours for personalization. The path for the Personalization_Contours is {model}\GHBM\metadata\contour_input_for_personalization"+
                           "\n\nLoad Body Data : "+
                            "\nLoad BodyData.txt file from the path : {model}\GHBM\metadata\BodyData\ to enable the personalization process"+
                            "\n\nLoad Anthropometric Target File : "+
                            "\nLoad the target file generated from the anthropometric module, it contain the values corresponding to ANSUR body dimensions\nLoad this file when you need to do the personalization using the target ANSUR body dimensions "+
                            "\n\nGEBOD target generation : "+
                            "\nThe GEBOD target generation button opens up a window which helps in generating the 35 GEBOD parameter values by adjusting the height, weight and age of a chlid,female or male human body model through regression"+
                            "\n\nOptions :"+
                            "\nAt present the user can populate the 35 GEBOD dimension values by selecting among 5th,50th,75th or 95th percentile or by selecting the generated body dimensions or by using the ANSUR target dimensions "+
                            "\n\nPersonalize : "+
                            "\nPersonalize button will start the personalization for the full GHBMC model for the given 35 parameter values"+
                            "\n\n\n\t\t\t\tGEBOD Personalization: Body Dimension Descriptions "+
                            "\n\nWEIGHT:\n  " + "Subject stands on scales (nude or wearing lightweight undergarments) with feet parallel and weight distributed equally on both feet." +
                           "\n\nSTANDING HEIGHT:\n  " + "Subject stands erect, head in the Frankfort plane, heels together, and weight distributed equally on both feet.
With the arm of the anthropometer firmly.
64 MADYMO Utilities Manual Release 7.5 touching the scalp, measure the vertical distance from the standing surface to the top of the head." +
                           "\n\nSHOULDER HEIGHT:\n  " + "Subject stands erect looking straight ahead, heels together, and
weight distributed equally on both feet. With an anthropometer, measure the vertical
distance from the standing surface to the right acromial landmark.  " +
                           "\n\nARMPIT HEIGHT:\n  " + "Measurement derived by subtracting Scye Circumference divided by π. from Shoulder Height (see above).
Scye Circumference: Subject stands erect looking straight ahead. The right arm is abducted sufficiently to allow placement of a
tape into the axilla. With a tape passing through the axilla, over the anterior and posterior vertical scye landmarks and over the right acromial landmark, measure the
circumference of the scye. The axillary tissue is not compressed. " +
                           "\n\nWAIST HEIGHT:\n  " + "Subject stands erect, his head in the Frankfort plane.Using an
anthropometer, measure the distance from the standing surface to the omphalion
landmark. The subject must not pull in his stomach." +
                           "\n\nSEATED HEIGHT:\n  " + "Subject sits erect, head in the Frankfort plane, upper arms hanging
relaxed, forearms and hands extended forward horizontally. With the anthropometer
arm firmly touching the scalp, measure the vertical distance from the sitting surface to
the top of the head. " +
                           "\n\nHEAD LENGTH:\n  " + "Subject sits. With a spreading calliper, measure in the midsagittal
plane the maximum length of the head between the glabella landmark and the occiput." +
                           "\n\nHEAD BREADTH:\n  " + "Subject sits. With a spreading calliper measure the maximum
horizontal breadth of the head above the level of the ears." +
                           "\n\nHEAD TO CHIN HEIGHT:\n  " + "Subject stands under the headboard looking straight ahead.
The headboard is adjusted so that its vertical and horizontal planes are in firm contact
with the back and the top of the head. Positioning the head in the Frankfort plane and
using the special gauge, measure the vertical distance from the horizontal plane to the
menton landmark." +
                           "\n\nNECK CIRCUMFERENCE:\n  " + "Subject sits erect, head in the Frankfort plane. A piece of
dental tape is placed around the neck, passing over all four neck landmarks. The
measurer marks off with her thumbnail a length of tape corresponding to the subject’s
neck circumference, and then measures this tape segment with a standard tape." +
                           "\n\nSHOULDER BREADTH:\n  " + " Subject sits erect looking straight ahead, upper arms hanging
relaxed, forearms and hands extended forward horizontally. With a beam calliper,
measure the distance between the acromial landmarks." +
                           "\n\nCHEST DEPTH:\n  " + "H: Subject stands erect looking straight ahead, heels together, and weight
distributed equally on both feet. With a beam calliper, measure the horizontal depth of
the trunk at the level of the bustpoint landmarks. The reading is made at the point of
maximum quiet inspiration. 65 Release 7.5 MADYMO Utilities Manual" +
                           "\n\nCHEST BREADTH:\n  " + "Subject stands erect looking straight ahead with arms slightly
abducted. With a beam calliper, measure the horizontal distance across the trunk at the
level of the bustpoint landmarks. " +
                           "\n\nWAIST DEPTH:\n  " + "Subject stands erect looking straight ahead, arms at sides heels
together, and weight distributed equally on both feet. With a beam calliper, measure the
horizontal depth of the trunk at the level of the waist landmarks. The reading is made
at the point of maximum quiet inspiration. The subject must not pull in her stomach. " +
                           "\n\nWAIST BREADTH:\n  " + "Subject stands erect looking straight ahead with arms slightly
abducted. With a beam calliper, measure the horizontal breadth across the trunk at the
level of the waist landmarks." +
                           "\n\nBUTTOCK DEPTH:\n  " + "Subject stands erect, heels together and weight distributed equally
on both feet. With a beam calliper, measure the horizontal depth of the trunk at the level
of the buttock landmark. " +
                           "\n\nHIP BREADTH, STANDING:\n  " + "Subject stands erect, heels together and weight
distributed equally on both feet. With a beam calliper, measure the maximum horizontal
breadth of the hips.  " +
                           "\n\nSHOULDER TO ELBOW LENGTH:\n  " + "Subject stands erect looking straight ahead and
with arms relaxed. With a beam calliper held parallel to the long axis of the right upper
arm, measure the distance from the acromial landmark to the radiale landmark. " +
                           "\n\nFOREARM-HAND LENGTH:\n  " + "H: Measurement derived by summing Radiale- Stylion
Length and Hand Length. Radiale-Stylion Length: Subject stands erect with arms
relaxed. With a beam calliper held parallel to the long axis of the right forearm, measure
the distance from the radiale landmark to the stylion landmark. Hand Length: Subject
sits, right forearm and hand raised with palm up. The fingers are together and straight
but not hyper-extended. With the bar of a sliding calliper parallel to the long axis of the
hand, measure the distance from the wrist landmark to the dactylion. " +
                           "\n\nBICEPS CIRCUMFERENCE:\n  " + " Subject stands with right arm slightly abducted. With a
tape held in a plane perpendicular to the long axis of the upper arm, measure the
circumference of the arm at the level of the biceps landmark.  " +
                           "\n\nELBOW CIRCUMFERENCE:\n  " + " Subject stands, right upper arm raised so that its long
axis is horizontal, elbow flexed 90 degrees, fist tightly clenched and biceps strongly
contracted. With a tape passing over the tip and through the crotch of the elbow,
measure the circumference of the elbow. " +
                           "\n\nFOREARM CIRCUMFERENCE:\n  " + "Subject stands erect with right arm slightly abducted
and hand relaxed. With a tape held in a plane perpendicular to the long axis of the
forearm, measure the circumference of the arm at the level of the forearm landmark." +
                           "\n\nWRIST CIRCUMFERENCE:\n  " + "Subject stands with right arm slightly abducted. With a
tape held in a plane perpendicular to the long axis of the forearm and hand, measure the
circumference of the wrist at the level of the stylion landmark. 66 MADYMO Utilities
Manual Release 7.5  " +
                           "\n\nKNEE HEIGHT, SEATED:\n  " + "Subject sits with his feet resting on a surface adjusted so
that the knees are bent at about right angles. Using an anthropometer, measure the
vertical distance from the footrest surface to the suprapatella landmark on the right
knee." +
                           "\n\nTHIGH CIRCUMFERENCE:\n  " + "Subject stands erect, heels approximately 10 cm apart,
and weight distributed equally on both feet. With a tape held in a plane perpendicular
to the long axis of the right thigh measure the circumference of the thigh at the level of
the lowest point on the gluteal furrow. Where the furrow is deeply indented, the
measurement is made just distal to the furrow. " +
                           "\n\nUPPER LEG CIRCUMFERENCE:\n  " + "Measurement derived by summing the Thigh
Circumference (see above) and the Knee Circumference (see below) and dividing the
sum by two to obtain the average" +
                           "\n\nKNEE CIRCUMFERENCE:\n  " + "Subject stands erect, heels approximately 10 cm apart,
and weight distributed equally on both feet. With a tape held in a plane perpendicular
to the long axis of the right leg, measure the circumference of the knee at the level of
the mid patella landmark. The subject must not tense her knee during the measurement.  " +
                           "\n\nCALF CIRCUMFERENCE:\n  " + "Subject stands erect, heels approximately 10 cm apart, and
weight distributed equally on both feet. With a tape held in a plane perpendicular to the
long axis of the right lower leg, measure the circumference of the calf at the level of the
calf landmark.  " +
                           "\n\nANKLE CIRCUMFERENCE:\n  " + "Subject stands erect with weight distributed equally on
both feet. With a tape held in a plane perpendicular to the long axis of the right lower
leg, measure the circumference of the leg at the level of the ankle landmark.  " +
                           "\n\nANKLE HEIGHT, OUTSIDE:\n " + "Subject stands with weight distributed equally on both
feet. With the special measuring block, measure the vertical distance from the standing
surface to the lateral malleolus landmark on the right leg. " +
                           "\n\nFOOT BREADTH:\n  " + "Subject stands erect, right foot in the measuring box, left foot on a
board of equal height, and weight distributed equally. The right foot is positioned so
that its long axis is parallel to the side of the box, the heel touches the rear of the box,
and the medial metatarsal-phalangeal joint touches the widest part of the foot, measure
on the scale of the box the breadth of the foot.  " +
                           "\n\nFOOT LENGTH:\n  " + "H: Subject stands erect, right foot in the measuring box, left foot on a
board of equal height, and weight distributed equally. The right foot is positioned so
that its long axis is parallel to the side of the box, the heel touches the rear of the box,
and the medial metatarsal-phalangeal joint touches the side of the box. With the
measuring block touching the tip of the most protruding toe, measure on the scale of
the box the length of the foot. " +
                           "\n\nHAND BREADTH:\n  " + "Subject sits with right hand resting on a table, palm up, fingers
extended and together. The thumb is held away from the hand. Using the sliding caliper,
measure the maximum breadth from Metacarpale II to Metacarpale V. 67 Release 7.5
MADYMO Utilities Manual " +
                           "\n\nHAND LENGTH:\n  " + "Subject sits with his right hand resting flat on a table, palm up,
fingers extended and together. With the bar of the sliding caliper parallel to the long
axis of the hand, measure the distance from the wrist landmark to the tip of the finger." +
                           "\n\nHAND DEPTH:\n  " + "Subject.s right hand is held palm down with fingers extended and
together, narrow profile towards the measurerer. Maintaining light pressure on the
spreading caliper, measure the thickness of the hand at the metacarpalphalangeal joint
of the third finger. ";



                   }

               }

