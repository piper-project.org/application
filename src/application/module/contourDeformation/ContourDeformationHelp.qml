// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {

    id:root
    minimumHeight : 350
    minimumWidth : 750


    ColumnLayout{
        height : 350
        width:750

        RowLayout{

            Label{
                text:"Window : "
            }
            ComboBox {
                width : 500
                model: [ "Positioning", "Personalization" ]
                currentIndex: -1
                onCurrentIndexChanged: {

                    if(currentText=="Positioning"){
                         textArea.text = "Steps to position the model using the contour deformation module:\n\n"+
                         "\t1. Open up PIPER and load the GHBMC model.\n"+
                         "\t2. Open Repositioning window by clicking on the \"Repos\" button on the right panel.\n"+
                         "\t3. Click on the Load Contour CL button and select the ContourCl.xml file from \GHBM\metadata\contourCL folder \n"+
                         "\t4. Select the joint to be re-positioned. **(Hip repositioning requires contour file to be loaded first)\n"+
                         "\t5. Select the side.\n"+
                         "\t6. Select the type of movement intended.\n"+
                         "\t7. Enter the angle.\n"+
                         "\t8. Click on the \"Generate contours\" button to create the contours and wait for the contours to be generated.\n"+
                         "\t9. Click on the \"Deform Contours\" button to deform the contours and wait for the controus to be deformed.\n"+
                         "\t10. Click on the \"Reposition\" button to move the model to the new position and wait for the positioning to be completed.\n"+
                         "\t13. Repeat steps from 4 through 10 to position the model in any other way.\n";

                    }
                    if(currentText=="Personalization"){
                         textArea.text = "Steps to personalize the model using the contour deformation module:\n\n"+
                         "\t1. Open up PIPER and load the GHBMC model.\n"+
                         "\t2. Open Personalization window by clicking on the \"Personalization\" button on the right panel.\n"+
                         "\t3. Click on the \"Load personalization contours\" button and the select the Personalization_Input.txt file to load\n\t   and the create the personalizaiton contours.\n"+
                         "\t4. Click on the \"Load body data file\" button and the select the BodyData.txt file to load the personalization ratios \n"+
                         "\t5. Choose a percentile from the option tab or GEBOD body dimension targets or generate the ANSUR targets.\n"+
                         "\t6. Click on the start button to personalize the model.\n"

                    }
                    if(currentText=="MetaEditor"){
                        textArea.text = "Steps to position the model using the contour deformation module when metadata required for repositioning is not present:\n\n"+
                        "\t1. Open up PIPER and load the GHBMC model.\n"+
                        "\t2. Open Repositioning window by clicking on the \"Repos\" button on the right panel.\n"+
                        "\t3. Click on the Load Contour CL button and select the ContourCl.xml file from \GHBM\metadata\MetaEditor\contourCL folder \n"+
                        "\t4. An alert will come up that required metadata by ContourCL is not present.(say Entity/Landmark)\n"+
                        "\t4. Create the required Entity through MetaEditor. Click on the CompleteContourCL button in MetaEditor to create the contourCL missing metadatas. Now go back to ContourDeformation Module and Load the ContourCL again. \n"+
                        "\t5. Select the joint to be re-positioned. **(Hip repositioning requires contour file to be loaded first)\n"+
                        "\t6. Select the side.\n"+
                        "\t7. Select the type of movement intended.\n"+
                        "\t8. Enter the angle.\n"+
                        "\t9. Click on the \"Generate contours\" button to create the contours and wait for the contours to be generated.\n"+
                        "\t10. Click on the \"Deform Contours\" button to deform the contours and wait for the controus to be deformed.\n"+
                        "\t11. Click on the \"Reposition\" button to move the model to the new position and wait for the positioning to be completed.\n"+
                        "\t12. Repeat steps from 4 through 10 to position the model in any other way.\n";

                    }


                }
            }

        }

        Rectangle{
            height : 350
            width: 750
            color : "transparent"
             TextArea {
                    id:textArea
                    anchors.fill : parent
                    text:"Steps to position the model using the contour deformation module:\n\n"+
                         "\t1. Open up PIPER and load the GHBMC model.\n"+
                         "\t2. Open Repositioning window by clicking on the \"Repos\" button on the right panel.\n"+
                         "\t3. Click on the Load Contour CL button and select the ContourCl.xml file from \GHBM\metadata\contourCL folder \n"+
                         "\t4. Select the joint to be re-positioned. **(Hip repositioning requires contour file to be loaded first)\n"+
                         "\t5. Select the side.\n"+
                         "\t6. Select the type of movement intended.\n"+
                         "\t7. Enter the angle.\n"+
                         "\t8. Click on the \"Generate contours\" button to create the contours and wait for the contours to be generated.\n"+
                         "\t9. Click on the \"Deform Contours\" button to deform the contours and wait for the controus to be deformed.\n"+
                         "\t10. Click on the \"Reposition\" button to move the model to the new position and wait for the positioning to be completed.\n"+
                         "\t11. Repeat steps from 4 through 10 to position the model in any other way.\n";


             }
       }





       }

}
