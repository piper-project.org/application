// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow 
{
    id : root
    title: "GEBOD Personalization"
    
    minimumHeight : 500
    minimumWidth : 890

    property bool contoursLoaded: false
    property bool ratiosLoaded: false

    property double minHeight:1.69
    property double minWeight:691.2



    property var ratios: []

    onContoursLoadedChanged: {
        if(contoursLoaded==true && ratiosLoaded==true){
            optionsGroupBox.enabled = true
            targetGroupBox.enabled = true
        }
    }
    onRatiosLoadedChanged: {
        if(contoursLoaded==true && ratiosLoaded==true){
            optionsGroupBox.enabled = true
            targetGroupBox.enabled = true
        }
    }

    MessageDialog{
        id: messageDialog
        visible: false
        title: ""
        text: ""
    }

    RowLayout{
            Layout.alignment: Qt.AlignTop
            width: 100

            ColumnLayout{
            Layout.alignment: Qt.AlignTop

            GroupBox{
                visible: false
                Layout.fillWidth: true
                title: "Load Target Flie"

                ColumnLayout {
                    width:childrenRect.width
                    Action {
                        id: openTargetAction
                        text: qsTr("Open Target file")
                        tooltip: qsTr("Open Target file")
                        iconSource: "qrc:///icon/document-open.png"
                        onTriggered: openTargetDialog.open()
                    }
                    ToolButton { action: openTargetAction }
                    FileDialog {
                        id: openTargetDialog
                        title: qsTr("Open Target File...")
                        nameFilters: ["Target files (*.ptt)"]
                        onAccepted: {
                            myContourDeformation.openTargetFile(openTargetDialog.fileUrl);
                            targetVals.enabled = true
                            targetValsHint.visible = false
                        }
                    }
                }

            }

                GroupBoxNoTitle {
//                    height: parent.height ; width: parent.width
                    ColumnLayout{
                        anchors.centerIn: parent
                        Button{
                            id: loadPersonalizationContoursButton
                            text:"Load Personalization Contours"
                            onClicked: {
                                openContourDialog.open()
                            }
                        }
                        FileDialog {
                            id: openContourDialog
                            title: qsTr("Open Contour File...")
                            nameFilters: ["Contour files (*.txt)"]
                            onAccepted: {
                                if(myContourDeformation.ui_validateFile(openContourDialog.fileUrl,"*POINT")){
                                    myContourDeformation.ui_openContour(openContourDialog.fileUrl);
                                    contextMetaManager.openContour(openContourDialog.fileUrl);
                                    myContourDeformation.ui_setPersMap(true)
                                    root.contoursLoaded = true
                                }else{
                                    messageDialog.text = "Invalid Personalization Contour File";
                                    messageDialog.visible = true;
                                }
                            }
                        }

                        Button{
                            text:"Load Body Data"
                            onClicked:{
                                openBodyDataDialog.open()
                            }
                        }
                        FileDialog {
                            id: openBodyDataDialog
                            title: qsTr("Open Body Data File...")
                            nameFilters: ["Body Data (*.txt )"]
                            onAccepted: {
                                if(myContourDeformation.ui_validateFile(openBodyDataDialog.fileUrl,"PERSONALIZATION-DATA")){
                                    root.ratiosLoaded = true
                                    console.log("BodyData.txt loaded")
                                }
                            }
                        }

                    }
                }

                GroupBoxNoTitle{
                    id : targetGroupBox
                    enabled: false
                    ColumnLayout{
                        anchors.centerIn: parent
                        Button{
                            text:"Load Anthropometric Target file"
                            onClicked:{
                                openAnthropometricDimensionTargetFileDialog.open()
                            }
                        }
                        FileDialog {
                            id: openAnthropometricDimensionTargetFileDialog
                            title: qsTr("Open Anthropometric Target File...")
                            nameFilters: ["Target files (*.ptt )"]
                            onAccepted: {
                                if(myContourDeformation.ui_validateFile(openAnthropometricDimensionTargetFileDialog.fileUrl,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
                                    myContourDeformation.openTargetFile(openAnthropometricDimensionTargetFileDialog.fileUrl);
                                    targetVals.enabled = true
                                targetValsHint.visible = false
                                }else{
                                    messageDialog.text = "Invalid Anthropometric target File";
                                    messageDialog.visible = true;
                                }

                            }
                        }




                    }
                }






                GroupBox{
                id:optionsGroupBox
                enabled: false

                title:"Options"
                    ColumnLayout{
                        ExclusiveGroup { id: persVals }
                        RadioButton {id: five; text: "5th Percentile"; exclusiveGroup: persVals ;property int a: 0;
                            onClicked:{
                                applyButton.enabled = true
                                a1.text = "691.2";	a2.text = "1.671";	a3.text = "1.361";	a4.text = "1.212";	a5.text = "0.9929";	a6.text = "0.8907";
                                a7.text = "0.196";	a8.text = "0.1548";	a9.text = "0.223";	a10.text = "0.3768";	a11.text = "0.3953";	a12.text = "0.239";
                                a13.text = "0.3188";	a14.text = "0.2178";	a15.text = "0.2983";	a16.text = "0.2326";	a17.text = "0.3398";	a18.text = "0.3095";

                                b1.text = "0.467";	b2.text = "0.309";	b3.text = "0.3016";	b4.text = "0.2746";	b5.text = "0.1706";	b6.text = "0.5213";
                                b7.text = "0.5718";	b8.text = "0.4726";	b9.text = "0.3725";	b10.text = "0.3629";	b11.text = "0.2174";	b12.text = "0.1283";
                                b13.text = "0.0942";	b14.text = "0.2569";	b15.text = "0.0861";	b16.text = "0.1822";	b17.text = "0.0269";
                            }
                        }
                        RadioButton {id: fifty; text: "50th Percentile"; exclusiveGroup: persVals;property int a: 1;
                            onClicked:{
                                applyButton.enabled = true
                                a1.text = "772.1";	a2.text = "1.773";	a3.text = "1.452";	a4.text = "1.298";	a5.text = "1.065";	a6.text = "0.9319";
                                a7.text = "0.1987";	a8.text = "0.156";	a9.text = "0.2277";	a10.text = "0.3835";	a11.text = "0.4073";	a12.text = "0.2452";
                                a13.text = "0.3278";	a14.text = "0.2231";	a15.text = "0.3096";	a16.text = "0.2397";	a17.text = "0.3526";	a18.text = "0.3297";

                                b1.text = "0.494";	b2.text = "0.3151";	b3.text = "0.3125";	b4.text = "0.2815";	b5.text = "0.1759";	b6.text = "0.5576";
                                b7.text = "0.5882";	b8.text = "0.488";	b9.text = "0.3869";	b10.text = "0.3719";	b11.text = "0.2241";	b12.text = "0.1372";
                                b13.text = "0.0977";	b14.text = "0.2703";	b15.text = "0.0889";	b16.text = "0.191";	b17.text = "0.0276";
                            }
                        }
                        RadioButton {id: seventyfive; text: "75th Percentile"; exclusiveGroup: persVals;property int a: 2;
                            onClicked:{
                                applyButton.enabled = true
                                a1.text = "805.2";	a2.text = "1.815";	a3.text = "1.489";	a4.text = "1.333";	a5.text = "1.094";	a6.text = "0.9488";
                                a7.text = "0.1998";	a8.text = "0.1565";	a9.text = "0.2297";	a10.text = "0.3862";	a11.text = "0.4122";	a12.text = "0.2478";
                                a13.text = "0.3315";	a14.text = "0.2252";	a15.text = "0.3142";	a16.text = "0.2426";	a17.text = "0.3579";	a18.text = "0.338";

                                b1.text = "0.505";	b2.text = "0.3176";	b3.text = "0.3169";	b4.text = "0.2844";	b5.text = "0.178";	b6.text = "0.5725";
                                b7.text = "0.595";	b8.text = "0.4943";	b9.text = "0.3927";	b10.text = "0.3756";	b11.text = "0.2269";	b12.text = "0.1408";
                                b13.text = "0.0991";	b14.text = "0.2758";	b15.text = "0.0901";	b16.text = "0.1946";	b17.text = "0.0279";
                            }
                        }
                        RadioButton {id: nintyfive; text: "95th Percentile"; exclusiveGroup: persVals;property int a: 3;
                            onClicked:{
                                applyButton.enabled = true
                                a1.text = "853";	a2.text = "1.875";	a3.text = "1.543";	a4.text = "1.384";	a5.text = "1.137";	a6.text = "0.9731";
                                a7.text = "0.2014";	a8.text = "0.1572";	a9.text = "0.2325";	a10.text = "0.3901";	a11.text = "0.4193";	a12.text = "0.2515";
                                a13.text = "0.33669";	a14.text = "0.2283";	a15.text = "0.3208";	a16.text = "0.2468";	a17.text = "0.3655";	a18.text = "0.3499";

                                b1.text = "0.521";	b2.text = "0.3212";	b3.text = "0.3233";	b4.text = "0.2884";	b5.text = "0.1811";	b6.text = "0.5939";
                                b7.text = "0.6047";	b8.text = "0.5034";	b9.text = "0.4012";	b10.text = "0.3809";	b11.text = "0.2308";	b12.text = "0.1461";
                                b13.text = "0.1012";	b14.text = "0.2836";	b15.text = "0.0917";	b16.text = "0.1998";	b17.text = "0.0283";
                            }
                        }
                        RadioButton {id: geBodVals; text: "GEBOD target values"; exclusiveGroup: persVals;property int a: 4;
                            visible:true
                            property variant vals

                            onClicked:{
                                applyButton.enabled = true
                                vals = myContourDeformation.ui_setGebodVals();
                                var count = 0
                                for(var elem in vals ){
                                    if(elem == "WEIGHT"){
                                        vals[elem] = ""+parseFloat(vals[elem]) * 9.8
                                        count  = 1
                                    }
                                    else
                                        vals[elem] = ""+parseFloat(vals[elem]) * 0.001
                                    console.log(vals[elem])
                                }
                                if(count!==0){
                                    if(parseFloat(vals.WEIGHT) < root.minWeight || parseFloat(vals.STANDING_HEIGHT) < root.minHeight){
                                        messageDialog.text = "Please select the parameter values such that they lie above the 5th percentile values of the human body model.\nPlease generate the the GEBOD target values again from the Anthropometry module.  "
                                        messageDialog.visible = true
                                        applyButton.enabled = false
                                    }else{
                                        applyButton.enabled = true
                                        a1.text = vals.WEIGHT;	a2.text = vals.STANDING_HEIGHT;	a3.text = vals.SHOULDER_HEIGHT;	a4.text = vals.ARMPIT_HEIGHT;	a5.text = vals.WAIST_HEIGHT;	a6.text = vals.SEATED_HEIGHT;
                                        a7.text = vals.HEAD_LENGTH;	a8.text = vals.HEAD_BREADTH;	a9.text = vals.HEAD_TO_CHIN_HEIGHT;	a10.text = vals.NECK_CIRCUMFERENCE;	a11.text = vals.SHOULDER_BREADTH;	a12.text = vals.CHEST_DEPTH;
                                        a13.text = vals.CHEST_BREADTH;	a14.text = vals.WAIST_DEPTH;	a15.text = vals.WAIST_BREADTH;	a16.text = vals.BUTTOCK_DEPTH;	a17.text = vals.HIP_BREADTH_STANDING;	a18.text = vals.SHOULDER_TO_ELBOW_LENGTH;

                                        b1.text = vals.FOREARM_HAND_LENGTH;	b2.text = vals.BICEPS_CIRCUMFERENCE;	b3.text = vals.ELBOW_CIRCUMFERENCE;	b4.text = vals.FOREARM_CIRCUMFERENCE;	b5.text = vals.WRIST_CIRCUMFERENCE;	b6.text = vals.KNEE_HEIGHT_SEATED;
                                        b7.text = vals.THIGH_CIRCUMFERENCE;	b8.text = vals.UPPER_LEG_CIRCUMFERENCE;	b9.text = vals.KNEE_CIRCUMFERENCE;	b10.text = vals.CALF_CIRCUMFERENCE;	b11.text = vals.ANKLE_CIRCUMFERENCE;	b12.text = vals.ANKLE_HEIGHT_OUTSIDE;
                                        b13.text = vals.FOOT_BREADTH;	b14.text = vals.FOOT_LENGTH;	b15.text = vals.HAND_BREADTH;	b16.text = vals.HAND_LENGTH;	b17.text = vals.HAND_DEPTH;
                                    }
                                }else{
                                    messageDialog.text = "Please generate GEBOD target values from the Anthropometry module first."
                                    messageDialog.visible = true
                                }
                            }
                        }
                        RadioButton {id: targetVals; text: "Anthropometric target values"; exclusiveGroup: persVals;property int a: 4;  enabled: false;
                            property variant vals;
                            onClicked:{
                                applyButton.enabled = true
                                vals = myContourDeformation.ui_setTargetVals();

                                for(var e in vals){
                                    console.log(e + "," + vals[e])
                                }
                                vals.ARMPIT_HEIGHT = ""+( parseFloat(vals.SHOULDER_HEIGHT)-parseFloat(vals.ACR1)/3.14 );
                                vals.HEAD_LENGTH = "" + ( parseFloat(vals.HAND_LENGTH) );
                                vals.HEAD_TO_CHIN_HEIGHT = "" + ( parseFloat(vals.ACR2));
//                                vals.SHOULDER_BREADTH = "" + (parseFloat(vals.CHEST_BREADTH));
//                                vals.CHEST_DEPTH = "" + (parseFloat(vals.WAIST_DEPTH));
                                vals.BUTTOCK_DEPTH = "" + (parseFloat(vals.WAIST_DEPTH));
                                vals.WAIST_DEPTH = "" + (parseFloat(vals.ACR2));
                                vals.WAIST_BREADTH = "" + (parseFloat(vals.ACR4));
                                vals.SHOULDER_TO_ELBOW_LENGTH = "" + ( parseFloat(vals.SHOULDER_TO_ELBOW_LENGTH) - parseFloat(vals.ACR7)/2 );
                                vals.KNEE_HEIGHT_SEATED = "" + (parseFloat(vals.FOREARM_HAND_LENGTH));

                                if(parseFloat(vals.WEIGHT) < root.minWeight || parseFloat(vals.STANDING_HEIGHT) < root.minHeight){
                                    messageDialog.text = "Please select the parameter values such that they lie above the 5th percentile values of the human body model."
                                    messageDialog.visible = true
                                    applyButton.enabled = false
                                }

                                    applyButton.enabled = true
                                    a1.text = vals.WEIGHT;	a2.text = vals.STANDING_HEIGHT;	a3.text = vals.SHOULDER_HEIGHT;	a4.text = vals.ARMPIT_HEIGHT;	a5.text = vals.WAIST_HEIGHT;	a6.text = vals.SEATED_HEIGHT;
                                    a7.text = vals.HEAD_LENGTH;	a8.text = vals.HEAD_BREADTH;	a9.text = vals.HEAD_TO_CHIN_HEIGHT;	a10.text = vals.NECK_CIRCUMFERENCE;	a11.text = vals.SHOULDER_BREADTH;	a12.text = vals.CHEST_DEPTH;
                                    a13.text = vals.CHEST_BREADTH;	a14.text = vals.WAIST_DEPTH;	a15.text = vals.WAIST_BREADTH;	a16.text = vals.BUTTOCK_DEPTH;	a17.text = vals.HIP_BREADTH_STANDING;	a18.text = vals.SHOULDER_TO_ELBOW_LENGTH;

                                    b1.text = vals.FOREARM_HAND_LENGTH;	b2.text = vals.BICEPS_CIRCUMFERENCE;	b3.text = vals.ELBOW_CIRCUMFERENCE;	b4.text = vals.FOREARM_CIRCUMFERENCE;	b5.text = vals.WRIST_CIRCUMFERENCE;	b6.text = vals.KNEE_HEIGHT_SEATED;
                                    b7.text = vals.THIGH_CIRCUMFERENCE;	b8.text = vals.UPPER_LEG_CIRCUMFERENCE;	b9.text = vals.KNEE_CIRCUMFERENCE;	b10.text = vals.CALF_CIRCUMFERENCE;	b11.text = vals.ANKLE_CIRCUMFERENCE;	b12.text = vals.ANKLE_HEIGHT_OUTSIDE;
                                    b13.text = vals.FOOT_BREADTH;	b14.text = vals.FOOT_LENGTH;	b15.text = vals.HAND_BREADTH;	b16.text = vals.HAND_LENGTH;	b17.text = vals.HAND_DEPTH;
                                }


                        }
                        Label{
                            id: targetValsHint;
                            text: "Load Target File"
                            font.pixelSize: 10
                            color: "red"
                        }

                        CheckBox {id: persMap;  text: "Pre-processing"; checked: false; visible:false;
                            onClicked:
                            {
                                myContourDeformation.ui_setPersMap(checked)
                            }
                        }
                        Button {
                            id: applyButton
                            text: "Personalize"
                            tooltip: "Start personalization"
                            enabled: false
                            onClicked: {
                                    loadPersonalizationContoursButton.enabled = false
                                    root.ratiosLoaded = false
                                    optionsGroupBox.enabled = false

                                    myContourDeformation.personalizeBusyIndicator( openBodyDataDialog.fileUrl ,a1.text,	a2.text,	a3.text,	a4.text,	a5.text,	a6.text,
                                                          a7.text,	a8.text,	a9.text,	a10.text,	a11.text,	a12.text,
                                                          a13.text, a14.text,	a15.text,	a16.text,	a17.text,	a18.text,

                                                          b1.text,	b2.text,	b3.text,	b4.text,	b5.text,	b6.text,
                                                          b7.text,	b8.text,	b9.text,	b10.text,	b11.text,	b12.text,
                                                          b13.text,	b14.text,	b15.text,	b16.text,	b17.text
                                                          );

                            }
                        }

                }
            }



                ModuleToolWindowButton{
                    text:"\&Help"
                    toolWindow: personalizationHelpWindow
                    shortcutChar: "H"
                }


        }



        GroupBox{
            id:parametersGroupBox
            enabled: false
            title:"Parameters"
            GridLayout
            {
                id: entitiesButtonsColumn
                enabled:true
                Label {
                    text: "WEIGHT"
                    Layout.row: 0
                    Layout.column : 0
                }
                Label {
                    text: "753.19 N"
                    Layout.row: 0
                    Layout.column : 1
                }
                TextField { id:a1;
                    objectName: "textInput";
                    placeholderText: qsTr("0")
                    Layout.row: 0
                    Layout.column : 2
                }
                Label {
                    text: "STANDING HEIGHT"
                    Layout.row: 1
                    Layout.column : 0
                }
                Label {
                    text: "1.7977 m"
                    Layout.row: 1
                    Layout.column : 1
                }
                TextField { id:a2;
                    placeholderText: qsTr("0")
                    Layout.row: 1
                    Layout.column : 2
                }
                Label {
                    text: "SHOULDER HEIGHT"
                    Layout.row: 2
                    Layout.column : 0
                }
                Label {
                    text: "1.4896 m"
                    Layout.row: 2
                    Layout.column : 1
                }
                TextField { id:a3;
                    placeholderText: qsTr("0")
                    Layout.row: 2
                    Layout.column : 2
                }
                Label {
                    text: "ARMPIT HEIGHT"
                    Layout.row: 3
                    Layout.column : 0
                }
                Label {
                    text: "1.3111 m"
                    Layout.row: 3
                    Layout.column : 1
                }
                TextField { id:a4;
                    placeholderText: qsTr("0")
                    Layout.row: 3
                    Layout.column : 2
                }
                Label {
                    text: "WAIST HEIGHT"
                    Layout.row: 4
                    Layout.column : 0
                }
                Label {
                    text: "1.0925 m"
                    Layout.row: 4
                    Layout.column : 1
                }
                TextField { id:a5;
                    placeholderText: qsTr("0")
                    Layout.row: 4
                    Layout.column : 2
                }
                Label {
                    text: "SEATED HEIGHT"
                    Layout.row: 5
                    Layout.column : 0
                }
                Label {
                    text: "0.9357 m"
                    Layout.row: 5
                    Layout.column : 1
                }
                TextField { id:a6;
                    placeholderText: qsTr("0")
                    Layout.row: 5
                    Layout.column : 2
                }
                Label {
                    text: "HEAD LENGTH"
                    Layout.row: 6
                    Layout.column : 0
                }
                Label {
                    text: "0.1970 m"
                    Layout.row: 6
                    Layout.column : 1
                }
                TextField { id:a7;
                    placeholderText: qsTr("0")
                    Layout.row: 6
                    Layout.column : 2
                }
                Label {
                    text: "HEAD BREADTH"
                    Layout.row: 7
                    Layout.column : 0
                }
                Label {
                    text: "0.1595 m"
                    Layout.row: 7
                    Layout.column : 1
                }
                TextField { id:a8;
                    placeholderText: qsTr("0")
                    Layout.row: 7
                    Layout.column : 2
                }
                Label {
                    text: "HEAD TO CHIN HEIGHT"
                    Layout.row: 8
                    Layout.column : 0
                }
                Label {
                    text: "0.2351 m"
                    Layout.row: 8
                    Layout.column : 1
                }
                TextField { id:a9;
                    placeholderText: qsTr("0")
                    Layout.row: 8
                    Layout.column : 2
                }
                Label {
                    text: "NECK CIRCUMFERENCE"
                    Layout.row: 9
                    Layout.column : 0
                }
                Label {
                    text: "0.3835 m"
                    Layout.row: 9
                    Layout.column : 1
                }
                TextField { id:a10;
                    placeholderText: qsTr("0")
                    Layout.row: 9
                    Layout.column : 2
                }
                Label {
                    text: "SHOULDER BREADTH"
                    Layout.row: 10
                    Layout.column : 0
                }
                Label {
                    text: "0.3172 m"
                    Layout.row: 10
                    Layout.column : 1
                }
                TextField { id:a11;
                    placeholderText: qsTr("0")
                    Layout.row: 10
                    Layout.column : 2
                }
                Label {
                    text: "CHEST DEPTH"
                    Layout.row: 11
                    Layout.column : 0
                }
                Label {
                    text: "0.2432 m"
                    Layout.row: 11
                    Layout.column : 1
                }
                TextField { id:a12;
                    placeholderText: qsTr("0")
                    Layout.row: 11
                    Layout.column : 2
                }
                Label {
                    text: "CHEST BREADTH"
                    Layout.row: 12
                    Layout.column : 0
                }
                Label {
                    text: "0.3638 m"
                    Layout.row: 12
                    Layout.column : 1
                }
                TextField { id:a13;
                    placeholderText: qsTr("0")
                    Layout.row: 12
                    Layout.column : 2
                }
                Label {
                    text: "WAIST DEPTH"
                    Layout.row: 13
                    Layout.column : 0
                }
                Label {
                    text: "0.2284 m"
                    Layout.row: 13
                    Layout.column : 1
                }
                TextField { id:a14;
                    placeholderText: qsTr("0")
                    Layout.row: 13
                    Layout.column : 2
                }
                Label {
                    text: "WAIST BREADTH"
                    Layout.row: 14
                    Layout.column : 0
                }
                Label {
                    text: "0.2705 m"
                    Layout.row: 14
                    Layout.column : 1
                }
                TextField { id:a15;
                    placeholderText: qsTr("0")
                    Layout.row: 14
                    Layout.column : 2
                }
                Label {
                    text: "BUTTOCK DEPTH"
                    Layout.row: 15
                    Layout.column : 0
                }
                Label {
                    text: "0.2473 m"
                    Layout.row: 15
                    Layout.column : 1
                }
                TextField { id:a16;
                    placeholderText: qsTr("0")
                    Layout.row: 15
                    Layout.column : 2
                }
                Label {
                    text: "HIP BREADTH, STANDING"
                    Layout.row: 16
                    Layout.column : 0
                }
                Label {
                    text: "0.3578 m"
                    Layout.row: 16
                    Layout.column : 1
                }
                TextField { id:a17;
                    placeholderText: qsTr("0")
                    Layout.row: 16
                    Layout.column : 2
                }
                Label {
                    text: "SHOULDER TO ELBOW LENGTH"
                    Layout.row: 17
                    Layout.column : 0
                }
                Label {
                    text: "0.3597 m"
                    Layout.row: 17
                    Layout.column : 1
                }
                TextField { id:a18;
                    placeholderText: qsTr("0")
                    Layout.row: 17
                    Layout.column : 2
                }
                Label {
                    text: "FOREARM-HAND LENGTH"
                    Layout.row: 0
                    Layout.column : 3
                }
                Label {
                    text: "0.2759 m"
                    Layout.row: 0
                    Layout.column : 4
                }
                TextField { id:b1;
                    placeholderText: qsTr("0")
                    Layout.row: 0
                    Layout.column : 5
                }
                Label {
                    text: "BICEPS CIRCUMFERENCE"
                    Layout.row: 1
                    Layout.column : 3
                }
                Label {
                    text: "0.3316 m"
                    Layout.row: 1
                    Layout.column : 4
                }
                TextField { id:b2;
                    placeholderText: qsTr("0")
                    Layout.row: 1
                    Layout.column : 5
                }
                Label {
                    text: "ELBOW CIRCUMFERENCE"
                    Layout.row: 2
                    Layout.column : 3
                }
                Label {
                    text: "0.2692 m"
                    Layout.row: 2
                    Layout.column : 4
                }
                TextField { id:b3;
                    placeholderText: qsTr("0")
                    Layout.row: 2
                    Layout.column : 5
                }
                Label {
                    text: "FOREARM CIRCUMFERENCE"
                    Layout.row: 3
                    Layout.column : 3
                }
                Label {
                    text: "0.2808 m"
                    Layout.row: 3
                    Layout.column : 4
                }
                TextField { id:b4;
                    placeholderText: qsTr("0")
                    Layout.row: 3
                    Layout.column : 5
                }
                Label {
                    text: "WRIST CIRCUMFERENCE"
                    Layout.row: 4
                    Layout.column : 3
                }
                Label {
                    text: "0.1817 m"
                    Layout.row: 4
                    Layout.column : 4
                }
                TextField { id:b5;
                    placeholderText: qsTr("0")
                    Layout.row: 4
                    Layout.column : 5
                }
                Label {
                    text: "KNEE HEIGHT, SEATED"
                    Layout.row: 5
                    Layout.column : 3
                }
                Label {
                    text: "0.4972 m"
                    Layout.row: 5
                    Layout.column : 4
                }
                TextField { id:b6;
                    placeholderText: qsTr("0")
                    Layout.row: 5
                    Layout.column : 5
                }
                Label {
                    text: "THIGH CIRCUMFERENCE"
                    Layout.row: 6
                    Layout.column : 3
                }
                Label {
                    text: "0.6094 m"
                    Layout.row: 6
                    Layout.column : 4
                }
                TextField { id:b7;
                    placeholderText: qsTr("0")
                    Layout.row: 6
                    Layout.column : 5
                }
                Label {
                    text: "UPPER LEG CIRCUMFERENCE"
                    Layout.row: 7
                    Layout.column : 3
                }
                Label {
                    text: "0.5007 m"
                    Layout.row: 7
                    Layout.column : 4
                }
                TextField { id:b8;
                    placeholderText: qsTr("0")
                    Layout.row: 7
                    Layout.column : 5
                }
                Label {
                    text: "KNEE CIRCUMFERENCE"
                    Layout.row: 8
                    Layout.column : 3
                }
                Label {
                    text: "0.3922 m"
                    Layout.row: 8
                    Layout.column : 4
                }
                TextField { id:b9;
                    placeholderText: qsTr("0")
                    Layout.row: 8
                    Layout.column : 5
                }
                Label {
                    text: "CALF CIRCUMFERENCE"
                    Layout.row: 9
                    Layout.column : 3
                }
                Label {
                    text: "0.3826 m"
                    Layout.row: 9
                    Layout.column : 4
                }
                TextField { id:b10;
                    placeholderText: qsTr("0")
                    Layout.row: 9
                    Layout.column : 5
                }
                Label {
                    text: "ANKLE CIRCUMFERENCE"
                    Layout.row: 10
                    Layout.column : 3
                }
                Label {
                    text: "0.2257 m"
                    Layout.row: 10
                    Layout.column : 4
                }
                TextField { id:b11;
                    placeholderText: qsTr("0")
                    Layout.row: 10
                    Layout.column : 5
                }
                Label {
                    text: "ANKLE HEIGHT, OUTSIDE"
                    Layout.row: 11
                    Layout.column : 3
                }
                Label {
                    text: "0.1022 m"
                    Layout.row: 11
                    Layout.column : 4
                }
                TextField { id:b12;
                    placeholderText: qsTr("0")
                    Layout.row: 11
                    Layout.column : 5
                }
                Label {
                    text: "FOOT BREADTH"
                    Layout.row: 12
                    Layout.column : 3
                }
                Label {
                    text: "0.0977 m"
                    Layout.row: 12
                    Layout.column : 4
                }
                TextField { id:b13;
                    placeholderText: qsTr("0")
                    Layout.row: 12
                    Layout.column : 5
                }
                Label {
                    text: "FOOT LENGTH"
                    Layout.row: 13
                    Layout.column : 3
                }
                Label {
                    text: "0.2703 m"
                    Layout.row: 13
                    Layout.column : 4
                }
                TextField { id:b14;
                    placeholderText: qsTr("0")
                    Layout.row: 13
                    Layout.column : 5
                }
                Label {
                    text: "HAND BREADTH"
                    Layout.row: 14
                    Layout.column : 3

                }
                Label {
                    text: "0.0889 m"
                    Layout.row: 14
                    Layout.column : 4
                }
                TextField { id:b15;
                    placeholderText: qsTr("0")
                    Layout.row: 14
                    Layout.column : 5
                }
                Label {
                    text: "HAND LENGTH"
                    Layout.row: 15
                    Layout.column : 3
                }
                Label {
                    text: "0.191 m"
                    Layout.row: 15
                    Layout.column : 4
                }
                TextField { id:b16;
                    placeholderText: qsTr("0")
                    Layout.row: 15
                    Layout.column : 5
                }
                Label {
                    text: "HAND DEPTH"
                    Layout.row: 16
                    Layout.column : 3
                }
                Label {
                    text: "0.0276 m"
                    Layout.row: 16
                    Layout.column : 4
                }
                TextField { id:b17;
                    placeholderText: qsTr("0")
                    Layout.row: 16
                    Layout.column : 5
                }
            }
        }
        PersonalizationHelp{
            id:personalizationHelpWindow
        }
}
    Connections {

        target: context

         onModelChanged: {
             root.contoursLoaded = false
             root.ratiosLoaded =  false
             loadPersonalizationContoursButton.enabled = true
             targetGroupBox.enabled = false
             optionsGroupBox.enabled = false
             parametersGroupBox.enabled = false
             applyButton.enabled = false

         }

    }
}
