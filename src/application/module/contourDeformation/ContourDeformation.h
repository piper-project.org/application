/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURDEFORMATION_H
#define PIPER_CONTOURDEFORMATION_H

#include "common/Context.h"
#include "contours/SpinePredictor.h"
#include "contours/ComputeTarget.h"
#include "ContourGen.h"
#include "TargetSynth.h"
#include "anatomyDB/query.h"

namespace piper {
	namespace contourdeformation {

		class ContourDeformation : public QObject
		{
			Q_OBJECT
		public:
			ContourDeformation();
			~ContourDeformation();
			Q_INVOKABLE void displayModel();
			Q_INVOKABLE QString getSeletedContourCLElement();
			Q_INVOKABLE bool ui_skeletonDisplay(int visible);
			Q_INVOKABLE void ui_contourDisplay(int visible);
			Q_INVOKABLE void generateContBusyIndicator(int visible);
			Q_INVOKABLE void deformContoursBusyIndicator();
			Q_INVOKABLE void reposBusyIndicator();
			Q_INVOKABLE void personalizeBusyIndicator(const QUrl& bodyData, double a1, double a2, double a3, double a4, double a5, double a6, double
				a7, double a8, double a9, double a10, double a11, double a12, double
				a13, double a14, double a15, double a16, double a17, double a18, double

				b1, double b2, double b3, double b4, double b5, double b6, double
				b7, double b8, double b9, double b10, double b11, double b12, double
				b13, double b14, double b15, double b16, double b17);
			Q_INVOKABLE void setResultinHistory(QString const& historyName);

			bool DisplaySkeleton();
			void HideSkeleton();

			/*
			refernce for getjtangle
			ContourCLj* tempj = getJoint("asd");
			computeJtAngles("kneel");
			pInfo() << "computeJointANgle func result" << tempj->jointangle;
			*/
			/*
			ContourCLbr* tempbr = getBodyRegion("Neck");
			computeBrAngles("Neck");
			pInfo() << "computeBrAngle result " << tempbr->brangle;
			*/

			bool enableReposButton = false;
			hbm::ContourCLbr* getBodyRegion(string);
			hbm::ContourCLj* getJoint(string);
			string tmpPath;
			void computeJtAngles(std::string name);
			void computeBrAngles(std::string name);
			bool signOfAngle(std::string name);
			bool hip_process = false;

			public slots:
			void ui_openContour(const QUrl& modelFile) {
				//					openContour(modelFile.toLocalFile().toStdString());
				Context::instance().performAsynchronousOperation(std::bind(&ContourDeformation::openContour, this, modelFile.toLocalFile().toStdString()), false, false, false, false, false);
			}
			void ui_SaveContour(const QUrl& modelFile) { saveContour(modelFile.toLocalFile().toStdString()); }

			void ui_deformContours();
			void ui_perfomDelaunnay();
			void ui_applyState();
			void ui_setModifiedState(int n);
			void ui_setReposFunc(int n);
			void ui_setReposType(int n);
			void ui_setReposSide(int n);
			void ui_setReposAngle(double n);
			double ui_getReposAngle();
			bool ui_getEnableReposButton();
			void ui_setEnableReposButton(bool);
			QVariantMap ui_setGebodVals();
			QVariantMap ui_setTargetVals();

			bool ui_getHipProcess();
			double ui_getJtAngle(QString name);
			double ui_getBrAngle(QString name);

			QList<QString> ui_GetBRandAngleForLandmarkTarget();
			void ui_markUnprocessed();
			void ui_useLandmarkTargets();

			bool ui_loadContourCLXml(const QUrl& modelFile);


			bool ui_validateBodyDataFile(const QUrl& modelFile);
			bool ui_validateFile(const QUrl& modelFile, QString  starttext);


			QVariantMap ui_populateGebodPercentileMap(const QUrl& modelFile);


			QVariantMap ui_getContourCLTree();

			void ui_loadAnthropometicTargetFile(const QUrl& modelFile);

			QList<QString> ui_getCtrlPtLandmarks(QString);
			void ui_addCtrlPtLandmark(QString, QString, QString);
			void ui_editCtrlPtLandmark(QString, QString, QString);
			void ui_removeCtrlPtLandmark(QString, QString);
			void ui_moveUpCtrlLandmark(QString, QString);
			void ui_moveDownCtrlLandmark(QString, QString);

			QList<QString> ui_getBRCircumference(QString);
			void ui_addBRCircumference(QString, QString);
			void ui_removeBRCircumference(QString, QString);

			QList<QString> ui_getJointCircumference(QString);
			void ui_addJointCircumference(QString, QString);
			void ui_removeJointCircumference(QString, QString);

			QList<QString> ui_getSplLandmarks(QString);
			void ui_addSplLandmark(QString, QString, QString);
			void ui_removeSplLandmark(QString, QString);
			void ui_editSplLandmark(QString, QString, QString);

			QList<QString> ui_getFlexionAxesLandmarks(QString);
			void ui_addFlexionAxesLandmark(QString, QString);
			void ui_removeFlexionAxesLandmarks(QString, QString);

			QList<QString> ui_getSplineInfo(QString);
			void ui_addSplineInfo(QString, QString);
			void ui_editSplineInfo(QString, QString, QString);
			void ui_removeSplineInfo(QString, QString);
			void ui_moveUpSplineLandmark(QString, QString);
			void ui_moveDownSplineLandmark(QString, QString);

			QString ui_getLandmarkDescriptionFromAnatomyDB(QString);
			QList<QString> ui_getAnatomyDBLandmarks();

			QString ui_getBRDescription(QString);
			QString ui_getJointDescription(QString);
			QList<QString> ui_getBRCircumDesc(QString);
			QList<QString> ui_getJointCircumDesc(QString);

			QList<QString> ui_getCplLandmarksDesc(QString);
			QList<QString> ui_getSplLandmarksDesc(QString);
			QList<QString> ui_getFlexionAxesLandmarksDesc(QString);
			QList<QString> ui_getSplineDesc(QString);

			void useLandmarkTargetBusyIndicator();

			void ui_addbrToTargetLandmark(QString, QString);
			void ui_mapBRToTargetLandmark(QString, QString);
			QList<double> ui_getTargetLandmarkValues(QString);
			void ui_deformContoursForLandmarkTarget(QString);

			void ui_setReposMap(bool mapping);
			void ui_setPersMap(bool mapping);

			void ui_personalize(const QUrl& bodyData, double a1, double a2, double a3, double a4, double a5, double a6, double
				a7, double a8, double a9, double a10, double a11, double a12, double
				a13, double a14, double a15, double a16, double a17, double a18, double

				b1, double b2, double b3, double b4, double b5, double b6, double
				b7, double b8, double b9, double b10, double b11, double b12, double
				b13, double b14, double b15, double b16, double b17);



			void ui_drawSpline(double, double, double, double, double, double, double, double, double, double, double, double);




			void displayModel(int value);
			void setOpacity(float value);
			void setCLOpacity(float value);
			void setScale(float value, QString CircumElement);

			void modelDisplay(int type);
			void openTargetFile(const QUrl& modelFile) { openTargetFile(modelFile.toLocalFile().toStdString()); }

			bool ui_getSignOfAngle(QString parent);

			void ui_initializeTargetsBusyIndicator();
			void ui_processNextTargetBusyIndicator();
			void initializeTargets();
			void processNextTarget();
			QList<double> ui_getReposParameters();
			//QList<QString> ui_getCurrentJointAndDOF();

			void ui_TestTargetAngleComputation(int currentElement, int currentDof);
			void TestTargetAngleComputation(int currentElement, int currentDof);


			void createSkinHeadNeck();

			void createLandmarkTargetFile();

		signals:
			void displayChanged();

		private:
			contours::ContourGen* pskeleton = NULL;
			void openTargetFile(std::string const& filePath);
			TargetSynth m_tsynth;
			void doSetResultinHistory(std::string const& historyName);
			piper::hbm::Nodes m_nodes;
		protected:
			void openContour(std::string const& modelFile);
			void saveContour(std::string const& modelFile);



			void readContourFile(std::string const& contourFile);

			void refreshDisplay();
			void deformContours();
			void performDelaunnay();
			void personalize(std::vector<double> vals, std::string bodyDataFile);
			void applyState();

			bool MODIFIED_STATE = false;

			std::vector<piper::hbm::NodePtr> vec_node;
			std::vector<piper::hbm::NodePtr> vec_node_new;
			std::vector<piper::hbm::NodePtr> vec_spline_new;
			std::map<int, piper::hbm::Plane*> contour_plane;
			std::map<int, piper::hbm::Plane*> contour_plane_new;
			std::map<int, piper::hbm::Contour*> contour_map;
			std::vector<piper::hbm::Contour*> vec_circle_cont;
			std::vector<piper::hbm::Contour*> vec_spline_cont;
			std::vector<piper::hbm::Contour*> vec_poly_cont;
			std::vector<unsigned int> frames;

			std::vector<piper::hbm::Contour*> contour_poly;
			std::set<piper::hbm::Id> sidnode;

			int reposFunc = 1, reposType = 0, reposSide = 0;
			bool MAPPING_REPOSITIONING = false, MAPPING_PERSONALIZATION = false;
			double reposAngle = 30.00;

			piper::contours::ProcessingFunc repo;
			piper::contours::ComputeTarget comptargets;
			piper::hbm::HBM_contours m_hbm_contours;
			piper::hbm::contour_detail cont_det;
			piper::hbm::Plane* m_plane;
			piper::contours::MainRepositioning main_repo;
			piper::contours::ContoursHbmInterface c_interface;
			piper::contours::ParserContourFile parser;
			piper::contours::SpinePredictor sprdict;
			hbm::FEModel const& fem = Context::instance().project().model().fem();
			hbm::FEModel& fem1 = Context::instance().project().model().fem();
			hbm::Metadata& m_meta = Context::instance().project().model().metadata();
			std::vector<piper::hbm::Contour*> getBodyRegion(piper::hbm::Metadata& m_meta, int body_region);

			VtkDisplayHighlighters draw;

			int maxBodyRegions = 34;

			QVariantMap targetMap;


			std::map<std::string, int> anthro_targets;
			void matchANSURGebodTargets();
			std::string convertUnits(tinyxml2::XMLElement* element);
		};

	}
}
#endif
