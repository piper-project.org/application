/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "TargetSynth.h"

#include "anatomyDB/query.h"
#include "common/Context.h"

#include <vtkLine.h>

using namespace Eigen;
using namespace std;
using namespace piper::hbm;

namespace piper {
    namespace contourdeformation {

    TargetSynth::TargetSynth()
    {
    }

    void TargetSynth::ComputeJointAngleFromTarget()
    {
        ComputeImportantTargets();
    }

    double TargetSynth::GetAxesToPointDistance(Coord a1, Coord a2, Coord l)
    {
        double p[3],lineP0[3],lineP1[3];

        lineP0[0] = a1[0];
        lineP0[1] = a1[1];
        lineP0[2] = a1[2];

        lineP1[0]= a2[0];
        lineP1[1]= a2[1];
        lineP1[2]= a2[2];


        p[0] = l[0];
        p[1] = l[1];
        p[2] = l[2];

        double dist = vtkLine::DistanceToLine(p, lineP0, lineP1);
        return dist;

    }


   double TargetSynth::GetAxesToPointDistance2(Coord a1, Coord a2, Coord l)
    {
         Coord v = a2 - a1;
         Coord w = l - a1;
         Coord d(0,0,0);

         double c1 = w.dot(v);
         double c2;
         if(!(v.isApprox(Coord::Zero())))
         {

            c2 = v.dot(v);
            double b = c1 / c2;
            Coord Pb = a1 + b * v;
            d = l-Pb;

         }
         return d.squaredNorm();
    }

    string TargetSynth::GetFarthestTargetFromAxes(vector <string> axis, vector <string> l_mark_targets)
    {
        FEModel& m_fem = Context::instance().project().model().fem();
        Metadata& m_meta = Context::instance().project().model().metadata();

        Coord a1 = m_meta.landmark(axis[0]).position(m_fem);
        Coord a2 = m_meta.landmark(axis[1]).position(m_fem);


        double old_dist,new_dist;
        old_dist = 0;
        new_dist = 0;
        string fTarget;

        for(auto it = l_mark_targets.begin(); it != l_mark_targets.end(); it++)
        {
           if(*it == "Head_center_of_left_femur" || *it == "Head_center_of_right_femur") //Some issue with distacne computation with this landmark
               continue;
           Coord l = m_meta.landmark(*it).position(m_fem);
           pDebug() << "Landmark  " << *it ;
           new_dist = GetAxesToPointDistance2(a1,a2,l);
           pDebug() << "new_dist  " << new_dist;
           if(new_dist > old_dist)
           {
               fTarget = *it;
               old_dist = new_dist;
           }

        }

        return fTarget;

    }

     std::vector<NodePtr> TargetSynth::ComputeFrameTargets(vector<pair<double*,double*>> axes,vector<NodePtr> imp_targets)
     {
         vector<NodePtr> frameTargets;
         /*

         for(auto it = axes.begin(); it != axes.end(); it++)
         {
            NodePtr ftarget = GetFarthestTargetFromAxes(it->first,it->second,imp_targets);
            frameTargets.push_back(ftarget);

         }

         */

        return frameTargets;
     }


     void TargetSynth::ComputeImportantTargets()
     {


        //Get a list of transformed landmarks
        m_trans_lmarks = GetTransformedLandmarks();

        ComputeTargetEntityMap(m_trans_lmarks);
        LogBRTargets(m_bone_targets);

        ComputeBREntityMap();
        LogBRTargets(m_br_bones);

         //LogBRTargets();

         //LevelTraverseContourCL();

         m_resultantAngleBr.first = 0;
         Metadata& m_meta = Context::instance().project().model().metadata();
         traverseCL(m_meta.ctrl_line.root);


     }



     Matrix3d TargetSynth::GetFrame(vector <string> l_mark, bool isTarget)
     {

         FEModel& m_fem = Context::instance().project().model().fem();
         Metadata& m_meta = Context::instance().project().model().metadata();

         //Get Origin
         Coord  nodeorigin ;
        //Get First dir
        Coord  nodefirstdir;

        //Get Second point in plane
        Coord  nodeseconddir;

         if(isTarget)
         {


             TargetList const& targetList = Context::instance().project().target();
             for (hbm::LandmarkTarget const& target : targetList.landmark)
             {
                 if(target.landmark == l_mark[0] )
                 {
                     //Get target coords
                     map<unsigned int, double> t_coord_map = target.value();

                     for(auto it = t_coord_map.begin(); it != t_coord_map.end(); it++)
                         nodeorigin << it->second;
                 }
                 else if(target.landmark == l_mark[1] )
                 {
                     //Get target coords
                     map<unsigned int, double> t_coord_map = target.value();

                     for(auto it = t_coord_map.begin(); it != t_coord_map.end(); it++)
                         nodefirstdir << it->second;
                 }
                 else if(target.landmark == l_mark[2] )
                 {
                     //Get target coords
                     map<unsigned int, double> t_coord_map = target.value();

                     for(auto it = t_coord_map.begin(); it != t_coord_map.end(); it++)
                         nodeseconddir << it->second;
                 }
             }




         }

         else
         {
             //Get Origin
            nodeorigin = m_meta.landmark(l_mark[0]).position(m_fem);

            //Get First dir
            nodefirstdir = m_meta.landmark(l_mark[1]).position(m_fem);

            //Get Second point in plane
            nodeseconddir = m_meta.landmark(l_mark[2]).position(m_fem);


         }



        Vector3d First = nodefirstdir - nodeorigin;
        Vector3d Second = nodeseconddir - nodeorigin;

        //Get Third Direction
        Vector3d Third = First.cross(Second);

        //Get Second Dir
        Second = Third.cross(First);

        //Normalize
        First.normalize();
        Second.normalize();
        Third.normalize();

        Matrix3d coordFrame(3,3);
        coordFrame << First[0],First[1],First[2]
                   , Second[0],Second[1],Second[2]
                   ,Third[0],Third[1],Third[2];

        std::cout << coordFrame;

        return coordFrame;


     }

      //Compute a map of BR and corresponding targets, using AnatomyDB
     void TargetSynth::ComputeTargetEntityMap(std::vector<std::string>& targetList)
     {
         vector<std::string> hierarchy;


         //Populate a map of BR having corresponding targets
        for(auto it = targetList.begin(); it != targetList.end(); it++)
         {
             pDebug() << *it;
             if(anatomydb::isLandmark(*it))
             {

                hierarchy = anatomydb::getLandmarkBoneList(*it);
                //Take immediate parent
                string parent_bony_ent = hierarchy[0];

                //Get all the targets of a BR together
                m_bone_targets[parent_bony_ent].push_back(*it);
                pDebug() << parent_bony_ent;
             }
         }
     }

     void TargetSynth::ComputeBREntityMap()
     {
         vector<std::string> hierarchy;

         for(auto it = m_bone_targets.begin(); it != m_bone_targets.end(); it++)
         {
             //Get BR hierarchy
             hierarchy = anatomydb::getPartOfSubClassList(it->first,"Region");

             if(hierarchy.size() > 0)
             {
                 //Take immediate parent and push it to map, may be this is not corresponding to contourCL ??
                 m_br_bones[hierarchy[0]].push_back(it->first);
             }
         }
     }

     void TargetSynth::LogBRTargets( map<string,vector<string>> mymap)
     {

         for(auto it = mymap.begin(); it != mymap.end(); it++)
         {
             pair< string, vector <string> > currentbr = *it;

             pDebug() << currentbr.first ;
             for(auto it2 = currentbr.second.begin(); it2 != currentbr.second.end(); it2++)
             {
                 pDebug() <<"    " << *it2;

             }


         }

     }

     //API to get Landmarks belonging to a BR
    vector < string > TargetSynth::GetBRLandmarks(string brname)
    {
        std::vector<std::string> bone_list;
        std::vector<std::string> landmark_list;

        bone_list = anatomydb::getSubPartOfList(brname,"Bone");
        pDebug() << "Bones for " << brname;
        LogVec(bone_list);


        for(auto it = bone_list.begin(); it != bone_list.end(); it++)
        {
            //Get list of bones for BR
            std::vector<std::string> l_list = anatomydb::getSubPartOfList(*it,"Landmark");

            //Get list of landmarks for all the bones of BR
            for(auto itr = l_list.begin(); itr != l_list.end(); itr++)
                landmark_list.push_back(*itr);

        }

        return landmark_list;
    }


     void TargetSynth::LogVec(std::vector<std::string>& logvec)
     {

         for(auto it = logvec.begin(); it != logvec.end(); it++)
         {
             pDebug() << *it ;

         }


     }

     string TargetSynth::GetAnatomyBR(string brname)
     {
         string db_name;
        /*
             "pelvis"
        "thorax"
       "neck"
         "head"
              */
             if(brname == "Left_arm")
                 db_name = "Left_arm";
             else if(brname == "Neck")
                 db_name = "Neck";
             else if(brname == "Left_forearm" )
                db_name = "Skeleton_of_left_forearm";
             else if(brname == "Left_hand" )
                db_name = "Skeleton_of_left_hand";
             else if (brname ==  "Right_arm" )
                db_name = "Right_arm";
             else if(brname ==  "Right_forearm" )
                db_name = "Skeleton_of_right_forearm";
             else if(brname == "Right_hand" )
                db_name = "Skeleton_of_right_hand";
             else if(brname == "Left_thigh" )
                db_name = "Left_free_lower_limb";
             else if(brname == "Left_leg" )
                db_name = "Skeleton_of_left_leg";
             else if(brname == "Left_foot" )
                db_name = "Left_foot";
             else if(brname == "Right_thigh" )
                db_name = "Right_free_lower_limb";
             else if(brname == "Right_leg" )
                db_name = "Skeleton_of_right_leg";
             else if(brname == "Right_foot" )
                db_name = "Right_foot";

         return db_name;

     }

     void TargetSynth::LevelTraverseContourCL()
     {
         Metadata& m_meta = Context::instance().project().model().metadata();
         std::queue<ContourCLbr*> myqueue;
         myqueue.push(m_meta.ctrl_line.root);
         while (myqueue.size() != 0)
         {
             ContourCLbr* current_br = myqueue.front();
             myqueue.pop();
             pDebug() << "Body Region: " << current_br->name ;
             for (int i = 0; i < current_br->childrenjts.size(); i++)
             {
                 //Get the spline type joint skeleton
                 std::string name(current_br->childrenjts.at(i)->name);
                 myqueue.push(current_br->childrenjts.at(i)->childbodyregion);
             }
         }

     }

     int TargetSynth::Process(ContourCLbr* root)
     {
         FEModel& m_fem = Context::instance().project().model().fem();
         Metadata& m_meta = Context::instance().project().model().metadata();
         TargetList const& targetList = Context::instance().project().target();
         piper::contours::ComputeTarget comptargets;
         int absolutefinalangle = 0;

         //Get the landmarks
         string brname = GetAnatomyBR(root->name);
         std::vector < std::string > landmark_list = GetBRLandmarks(brname);
         LogVec(landmark_list);

         pDebug() << " ========================================= ";
         pDebug() << " Get transformed targets for " << root->name;
         //Filter them
         std::vector < std::string > tran_llist = GetTransformedLandmarksofBR(landmark_list);
         LogVec(tran_llist);
         if(!tran_llist.empty())
         {


             //The axes of rotation is located at parent joint
             ContourCLj* p_joint;
             p_joint = m_meta.ctrl_line.findparentJoint(root->name);
             if (p_joint == nullptr){
                 pDebug() << "no joint corresponds to this information";
                 return 0;
             }
             else{
                 pDebug() << p_joint->name;
             }

             //Only one target for this BR
             if(tran_llist.size() == 1)
             {
                 pDebug() << "Its case one";
                 //Compute flexion angle of rotation
                 if(!(p_joint->flexionaxeslandmarks.empty()))
                 {

                     //Get target coords of landmark
                     hbm::LandmarkTarget ftarget;
                     for (hbm::LandmarkTarget const& target : targetList.landmark)
                     {
                         if(target.landmark == tran_llist[0])
                         {
                            ftarget = target;
                         }
                     }

					 target_lanmk = ftarget;
					 joint_pointer = p_joint;

                    pDebug() << "Using " <<  tran_llist[0] << "for angle computation of " << root->name ;
					absolutefinalangle = comptargets.computeSignedTargetAngle(m_meta, m_fem, p_joint->flexionaxeslandmarks,
                                                                              ftarget,tran_llist[0]);
                    pDebug() << "Angle: "<< absolutefinalangle;


                    if(abs(absolutefinalangle) > 1)//atleast 1 degree ??
                    {
                        m_resultantAngleBr = make_pair(absolutefinalangle,root->name);
						
						
                    }


                 }
                 else
                 {
                     pWarning() << "Flexion axes not defined for " << root->name;

                 }


             }
             else if(tran_llist.size() == 2)
             {
                 pDebug() << "Its case two";
                 //Compute  angle of rotation
                 if(!(p_joint->flexionaxeslandmarks.empty()))
                 {
                     string t_landmark = GetFarthestTargetFromAxes(p_joint->flexionaxeslandmarks,tran_llist);
                     //Get target coords of landmark
                     hbm::LandmarkTarget ftarget;
                     for (hbm::LandmarkTarget const& target : targetList.landmark)
                     {
                         if(target.landmark == t_landmark)
                         {
                            ftarget = target;
                         }
                     }

					 target_lanmk = ftarget;
					 joint_pointer = p_joint;


                     pDebug() << "Using " <<  t_landmark << "for angle computation of " << root->name;
					 absolutefinalangle = comptargets.computeSignedTargetAngle(m_meta, m_fem, p_joint->flexionaxeslandmarks,
                                                                               ftarget,t_landmark);
                     pDebug() << "Angle: "<< absolutefinalangle;

                     if(abs(absolutefinalangle) > 1)//atleast 1 degree ??
                     {
                         m_resultantAngleBr = make_pair(absolutefinalangle,root->name);
                     }

                 }
                 else
                 {
                     pWarning() << "Flexion axes not defined for " << root->name;
                 }

             }
             else if(tran_llist.size() >= 3)
             {
                 vector < string > axis;
                 pDebug() << "Its case three ";
                 if(!(p_joint->flexionaxeslandmarks.empty()) ||
                         (root->name == "Neck" && !(root->dof1.empty())))
                 {
                     string t_landmark;
                     if(root->name == "Neck")
                     {
                         axis =root->dof1;
                         t_landmark = GetFarthestTargetFromAxes(axis,tran_llist);
                     }
                     else
                     {
                         axis = p_joint->flexionaxeslandmarks;
                         t_landmark = GetFarthestTargetFromAxes(axis,tran_llist);
                     }

                     //Get target coords of landmark
                     hbm::LandmarkTarget ftarget;
                     for (hbm::LandmarkTarget const& target : targetList.landmark)
                     {
                         if(target.landmark == t_landmark)
                         {
                            ftarget = target;
                         }
                     }
					 target_lanmk = ftarget;
					 joint_pointer = p_joint;


                     pDebug() << "Using " <<  t_landmark << "for angle computation of " << root->name ;
					 absolutefinalangle = comptargets.computeSignedTargetAngle(m_meta, m_fem, axis,
                                                                               ftarget,t_landmark);
                     pDebug() << "Angle: "<< absolutefinalangle;
                     if(abs(absolutefinalangle) > 1)//atleast 1 degree ??
                     {
                         m_resultantAngleBr = make_pair(absolutefinalangle,root->name);
                     }

                 }
                 else
                 {
                     pWarning() << "Flexion axes not defined for " << root->name;

                 }

             }


         }
         else
         {
             pDebug() << " No transformed targets for " << root->name;

         }

         root->Isprocessed = true;
         return absolutefinalangle;

     }


     void TargetSynth::markUnprocessed(ContourCLbr* root)
     {
         if (root)
         {
             pDebug() <<"Marking unprocessed  " << root->name ;
             root->Isprocessed = false;

             // First traverse the left subtree
             if(root->childrenjts.size() >= 1)
             markUnprocessed(root->childrenjts.at(0)->childbodyregion);


             // Traverse middle subtree
             if(root->childrenjts.size() >= 2)
             markUnprocessed(root->childrenjts.at(1)->childbodyregion);

             // Finally Traverse the right subtree
             if(root->childrenjts.size() >= 3)
             markUnprocessed(root->childrenjts.at(2)->childbodyregion);
         }
     }


     // A recursive function to traverse Ternary Search Tree
     void TargetSynth::traverseCL(ContourCLbr* root)
     {
         if (root)
         {
             pDebug() << "Body Region: " << root->name;
             pDebug() << "root->Isprocessed : " << root->Isprocessed ;

             string brname = GetAnatomyBR(root->name);
             //string brname = root->name;
             if(!brname.empty() && brname != "thorax"
                     && brname != "pelvis" && brname != "head"
                     && root->Isprocessed == false)//thorax not supported yet
             {
                 if(abs(Process(root)) > 1)//Dont process further if angle comes
                     return;
             }



             // First traverse the left subtree
             if(root->childrenjts.size() >= 1 && m_resultantAngleBr.first == 0)
             traverseCL(root->childrenjts.at(0)->childbodyregion);


             // Traverse middle subtree
             if(root->childrenjts.size() >= 2 && m_resultantAngleBr.first == 0)
             traverseCL(root->childrenjts.at(1)->childbodyregion);

             // Finally Traverse the right subtree
             if(root->childrenjts.size() >= 3 && m_resultantAngleBr.first == 0)
             traverseCL(root->childrenjts.at(2)->childbodyregion);
         }
     }




     //Get transformed landmarks based o whether they are changes, and also above a threshold
     std::vector<std::string> TargetSynth::GetTransformedLandmarks()
     {
         FEModel& m_fem = Context::instance().project().model().fem();
         Metadata& m_meta = Context::instance().project().model().metadata();
         string l_name;
         std::vector<std::string> transformed_landmarks;

         double threshold = 2.0f;
         string current_unit = units::LengthUnit::unitToStr(Context::instance().project().model().metadata().lengthUnit());
         if(current_unit == "mm") // Add other cases for other units
             threshold = 2.0f;


         //Init anatomyDB
         anatomydb::init();

         if (!Context::instance().project().target().empty())
         {

             TargetList const& targetList = Context::instance().project().target();
             for (hbm::LandmarkTarget const& target : targetList.landmark)
             {
                 l_name = target.landmark;

                 if(anatomydb::isLandmark(l_name))
                 {

                     //Get initial coord of landmark
                     Coord  i_coord = m_meta.landmark(l_name).position(m_fem);

                     //Get target coords
                     map<unsigned int, double> t_coord_map = target.value();
                     Coord t_coord;
                     for(auto it = t_coord_map.begin(); it != t_coord_map.end(); it++)
                         t_coord << it->second;

                     Coord cord_diff = i_coord-t_coord;
                     //Check if not equal, then push to transformed target list
                   if (!(cord_diff).isApprox(Coord::Zero()) &&  (cord_diff.norm() > threshold))
                   {
                       transformed_landmarks.push_back(l_name);

                   }

                 }
             }


         }
         else
         {
             pCritical() << "Target List Empty";

         }
         return transformed_landmarks;
     }



     //Check if the landmark is present in the list of transformed-thresholded landmarks
     std::vector<std::string> TargetSynth::GetTransformedLandmarksofBR(std::vector<std::string> landmark_list)
     {
         std::vector<std::string> transformed_BR_landmarks;
         for(auto it = landmark_list.begin(); it != landmark_list.end(); it++)
         {
             for(auto it2 = m_trans_lmarks.begin(); it2 != m_trans_lmarks.end(); it2++)
             {
                 if(*it2 == *it)
                 transformed_BR_landmarks.push_back(*it);
             }

         }

         return transformed_BR_landmarks;
     }


     /// Case when, one landmark is present, we assume it is flexion only
 /*    vector < double > TargetSynth::FindAxesRotationAngle(vector <string> axes1, vector <string> axes2,
                                               vector <string> axes3,vector <string> l_mark)
     {
         Matrix3d frame_i = GetFrame(l_mark,false);
         Matrix3d frame_f = GetFrame(l_mark,true);

         Quaterniond qq = QuaternionRot(frame_i.row(0), frame_i.row(1), frame_i.row(2),
                                        frame_f.row(0), frame_f.row(1), frame_f.row(2));

         Vector3d euler = qq.toRotationMatrix().eulerAngles(2, 1, 0);
          double yaw_abduction = euler[0];
          double pitch_flexion= euler[1];
          double roll_twist = euler[2];

		 return euler;
     }
	 */


     double TargetSynth::FindPrimaryRotationAngle(vector <string> axes_landmark, string l_mark)
     {
          FEModel& m_fem = Context::instance().project().model().fem();
          Metadata& m_meta = Context::instance().project().model().metadata();


          //Get axes coords
          Coord  axes[2];
          axes[0] = m_meta.landmark(axes_landmark[0]).position(m_fem);
          axes[1] = m_meta.landmark(axes_landmark[1]).position(m_fem);

          //std::cout <<" axes[0] " <<  axes[0] << std::endl;
          //std::cout <<"axes[1] " << axes[1] << std::endl;

          Coord origin = (axes[0] + axes[1] )/2;
          Coord normal = axes[1] - axes[0];
          normal.normalize();

          //std::cout <<"origin " << origin << std::endl;
          //std::cout <<"normal " << normal << std::endl;

          //Get initial coord of landmark
          Coord  i_coord = m_meta.landmark(l_mark).position(m_fem);

          //std::cout <<"i_coord " << i_coord << std::endl;



          //Get target coords of landmark
          Coord t_coord;
          TargetList const& targetList = Context::instance().project().target();
          for (hbm::LandmarkTarget const& target : targetList.landmark)
          {
              if(target.landmark == l_mark)
              {
                  //Get target coords
                  map<unsigned int, double> t_coord_map = target.value();

                  for(auto it = t_coord_map.begin(); it != t_coord_map.end(); it++)
                      t_coord << it->second;
              }
          }


          //std::cout <<"t_coord " << t_coord << std::endl;

          vtkSmartPointer<vtkPlane> plane =
                vtkSmartPointer<vtkPlane>::New();
            plane->SetOrigin(origin[0], origin[1], origin[2]);
            plane->SetNormal(normal[0], normal[1], normal[2]);


#if 0

            vtkSmartPointer<vtkPlaneSource> planeSource =
               vtkSmartPointer<vtkPlaneSource>::New();
             planeSource->SetCenter(origin[0], origin[1], origin[2]);
             planeSource->SetNormal(normal[0], normal[1], normal[2]);
             planeSource->Update();

             vtkPolyData* pplane = planeSource->GetOutput();

             // Create a mapper and actor
               vtkSmartPointer<vtkPolyDataMapper> mapper =
                 vtkSmartPointer<vtkPolyDataMapper>::New();

               mapper->SetInputData(pplane);

               vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
               //vtkSmartPointer<vtkActor> actor =
                 //vtkSmartPointer<vtkActor>::New();
               actor->SetMapper(mapper);

               //actor->GetProperty()->SetDiffuseColor(r, g, b);
               //actor->SetName(name.str());

               Context::instance().display().AddActor(actor,l_mark, false, DISPLAY_MODE::GEBOD, true, false);

            //Context::instance().display().AddVtkPointSet( planeSource->GetOutput(),l_mark,false,DISPLAY_MODE::GEBOD);
            //Context::instance().display().SetObjectColor(l_mark, 1.0 , 1.0 ,0 ,0.8);
            //Context::instance().display().SetObjectWidth(l_mark,2.0);
#endif

            double initial[3] = {i_coord[0], i_coord[1], i_coord[2]};
            double final[3] = {t_coord[0],t_coord[1],t_coord[2]};

            double d_origin[3] = {origin[0], origin[1], origin[2]};
            double d_normal[3] = {normal[0], normal[1], normal[2]};
            double projected_i[3];
            double projected_f[3];
            double projected_o[3];

            plane->ProjectPoint(initial, d_origin, d_normal, projected_i);
            plane->ProjectPoint(final, d_origin, d_normal, projected_f);
            plane->ProjectPoint(d_origin, d_origin, d_normal, projected_o);

            Coord proj_i,proj_f,proj_o;
            proj_i << projected_i[0],projected_i[1],projected_i[2] ;
            //std::cout <<"proj_i " << proj_i << std::endl;


            proj_f << projected_f[0],projected_f[1],projected_f[2]  ;
            //std::cout <<"proj_f " << proj_f << std::endl;

            proj_o << projected_o[0],projected_o[1],projected_o[2]  ;
            //std::cout <<"proj_o " << proj_o << std::endl;

            Coord vec_i = proj_i - proj_o;
            Coord vec_f = proj_f - proj_o;
            vec_i.normalize();
            vec_f.normalize();

            //std::cout <<"vec_i " << vec_i << std::endl;

            //std::cout <<"vec_f " << vec_f << std::endl;

            //std::cout << "dot prod "<< vec_i.dot(vec_f);


            double angle;
            if(vec_i.isApprox(Coord::Zero()) || vec_i.isApprox(Coord::Zero()))
                    angle = 0;
            else
                angle = acos(vec_i.dot(vec_f));


            return angle*57.29578;//return in degree

     }



     /// Case when, three or more than three landmarks present
     /// Determine rotation quaternion from coordinate system 1 (vectors
     /// x1, y1, z1) to coordinate system 2 (vectors x2, y2, z2)
     /// This code is derived from https://goo.gl/Ezm6Kh
     /// And is based on paper https://goo.gl/cjVWem
     Quaterniond TargetSynth::QuaternionRot(Vector3d x1, Vector3d y1, Vector3d z1,
                               Vector3d x2, Vector3d y2, Vector3d z2) {

         Matrix3d M = x1*x2.transpose() + y1*y2.transpose() + z1*z2.transpose();

         Matrix4d N;
         N << M(0,0)+M(1,1)+M(2,2)   ,M(1,2)-M(2,1)          , M(2,0)-M(0,2)         , M(0,1)-M(1,0),
              M(1,2)-M(2,1)          ,M(0,0)-M(1,1)-M(2,2)   , M(0,1)+M(1,0)         , M(2,0)+M(0,2),
              M(2,0)-M(0,2)          ,M(0,1)+M(1,0)          ,-M(0,0)+M(1,1)-M(2,2)  , M(1,2)+M(2,1),
              M(0,1)-M(1,0)          ,M(2,0)+M(0,2)          , M(1,2)+M(2,1)         ,-M(0,0)-M(1,1)+M(2,2);

         EigenSolver<Matrix4d> N_es(N);
         Vector4d::Index maxIndex;
         N_es.eigenvalues().real().maxCoeff(&maxIndex);

         Vector4d ev_max = N_es.eigenvectors().col(maxIndex).real();

         Quaterniond quat(ev_max(0), ev_max(1), ev_max(2), ev_max(3));
         quat.normalize();

         return quat;
     }


      std::pair<int,std::string> TargetSynth::GetBRandAngle()
      {
            return m_resultantAngleBr;
      }

	  std::vector<double> TargetSynth::getReposParameters(){
		  return reposParameters;
	  }

	  hbm::LandmarkTarget TargetSynth::getLandmarkTargetObject(std::string targetname){
		  LandmarkTarget targetObject;
		  if (!Context::instance().project().target().empty()){
			  TargetList const& targetList = Context::instance().project().target();
			  for (hbm::LandmarkTarget const& target : targetList.landmark)
				  if (target.landmark == targetname){
					  targetObject = target;
					  return targetObject;
				  }
		  }
		  return targetObject;
	  }

	  Coord TargetSynth::getLandmarkTargetCoordinates(std::string target){
		  Coord coords = {0,0,0};
		  map<unsigned int, double> t_coord_map = getLandmarkTargetObject(target).value();
		  if (t_coord_map.size() != 3)
			  return coords;
		  coords = { t_coord_map[0], t_coord_map[1], t_coord_map[2] };
		  return coords;
	  }

	  void TargetSynth::init(){
		  currentElementStatus = std::make_pair(0, 0);
		  hardcodingStuff();
		  initializeCLElementVector();
	  }

	  void TargetSynth::initializeCLElementVector(){
		  // Pre order Traveral of the contourCL tree
		  
	  }

	  void TargetSynth::hardcodingStuff(){
		  // Harcoding the reposFunc values used when deformContours() and performDelaunay() are called 
		  reposFuncMap["thorax"] = 7;
		  reposFuncMap["lumbar"] = 8;
		  reposFuncMap["Neck"] = 4;
		  reposFuncMap["shoulderl"] = 9;
		  reposFuncMap["elbowl"] = 5;
		  reposFuncMap["wristl"] = 6;
		  reposFuncMap["shoulderr"] = 9;
		  reposFuncMap["elbowr"] = 5;
		  reposFuncMap["wristr"] = 6;
		  reposFuncMap["hipl"] = 3;
		  reposFuncMap["kneel"] = 1 ;
		  reposFuncMap["anklel"] = 2 ;
		  reposFuncMap["hipr"] = 3;
		  reposFuncMap["kneer"] = 1 ;
		  reposFuncMap["ankler"] = 2; 
		  // Harcoding the reposSide values used when deformContours() and performDelaunay() are called , 0 for left , 1 for right
		  reposSideMap["thorax"] = 0;
		  reposSideMap["lumbar"] = 0;
		  reposSideMap["Neck"] = 0;
		  reposSideMap["shoulderl"] = 0;
		  reposSideMap["elbowl"] = 0;
		  reposSideMap["wristl"] = 0;
		  reposSideMap["shoulderr"] = 1;
		  reposSideMap["elbowr"] = 1;
		  reposSideMap["wristr"] = 1;
		  reposSideMap["hipl"] = 0;
		  reposSideMap["kneel"] = 0;
		  reposSideMap["anklel"] = 0;
		  reposSideMap["hipr"] = 1;
		  reposSideMap["kneer"] = 1;
		  reposSideMap["ankler"] = 1;
		  // Hardcoding the order of processing of joints/bodyregions, from the parent to children nodes, this can be generated by running a DFS on the CL tree
		  CLElementVector = { "lumbar", "thorax", "Neck", "shoulderl", "elbowl", "wristl", "shoulderr", "elbowr", "wristr", "hipl", "kneel", "anklel", "hipr", "kneer", "ankler" };
		  //                    0         1         2          3            4          5         6         7         8        9       10         11     12       13       14
		  // Hardcoding the order of processing the different degrees of freedom as a vector flexion=0 ->twisting=1 (if applicable for joint/br )-> lateral-flexion/abduction=2 (if applicable)  
		  reposOrder["lumbar"] = { 0, 1, 2 };
		  reposOrder["thorax"] = { 0, 1};
		  reposOrder["Neck"] = { 0, 1, 2 };
		  reposOrder["shoulderl"] = { 0, 1, 2 };
		  reposOrder["elbowl"] = { 0, 1 };
		  reposOrder["wristl"] = { 0 };
		  reposOrder["shoulderr"] = { 0, 1, 2 };
		  reposOrder["elbowr"] = { 0, 1 };
		  reposOrder["wristr"] = { 0 };
		  reposOrder["hipl"] = { 0, 1, 2 };
		  reposOrder["kneel"] = { 0 };
		  reposOrder["anklel"] = { 0, 1 };
		  reposOrder["hipr"] = { 0, 1, 2 };
		  reposOrder["kneer"] = { 0 };
		  reposOrder["ankler"] = { 0, 1 };
		  
	  }

	  void TargetSynth::updateElementStatus(int newCurrentElement, int currentDof){
		  std::cout << "\n\n updateElementStatus called ; new status =  "<< newCurrentElement << "," << currentDof;
		  this->currentElementStatus = std::make_pair(newCurrentElement, currentDof);
	  }

	  // Calculates the repos parameters for the current Cl br/joint 
	  void  TargetSynth::calculateReposParameters(){
		  FEModel& m_fem = Context::instance().project().model().fem();
		  Metadata& m_meta = Context::instance().project().model().metadata();
		  piper::contours::ComputeTarget comptargets;
		  		  
		  double reposFunc, reposType, reposSide, reposPossibleAngle=0, reposComputedAngle=0, reposAngleLimit=0, angle=0, angleTarget=0, currentAngle=0, angleLimitMin=0, angleLimitMax=0;
		  
		  int elementIndex = this->currentElementStatus.first;
		  int currentDofIndex = this->currentElementStatus.second;

		  std::string currentElement = this->CLElementVector.at(elementIndex);
		  string reference1, reference2, normal1, normal2, target1, target2;
		  
		  std::cout << "\ncalculateReposParameters() called for " << currentElement << " & dof = ";

		  reposFunc = reposFuncMap[currentElement];
		  reposSide = reposSideMap[currentElement];
				  
		  if (m_meta.ctrl_line.getBodyRegion(currentElement)){
			  ContourCLbr* br = m_meta.ctrl_line.getBodyRegion(currentElement);
			  switch (currentDofIndex){
				  //Flexion
				  case 0:{
					  std::cout << "Flexion-Extension";
					  std::cout << "\nVector sizes : reference = " << br->dof1_ref.size() << ", target = " << br->dof1_target.size() << ", normal = " << br->dof1_normal.size();
					  reposType = 0;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (br->dof1_ref.size() < 2 || br->dof1_target.size() < 2 || br->dof1_normal.size() < 2)
						  return;
					  angleLimitMin = br->dof1_limit_min;
					  angleLimitMax = br->dof1_limit_max;
					  reference1 = br->dof1_ref.at(0); 	  reference2 = br->dof1_ref.at(1);
					  normal1 = br->dof1_normal.at(0); 	  normal2 = br->dof1_normal.at(1); 
					  target1 = br->dof1_target.at(0);    target2 = br->dof1_target.at(1);
					  break;
				  }
				  //Lateral Flexion OR Abduction 
				  case 2:{
					  std::cout << "Lateral Flexion OR Abduction ";
					  std::cout << "\nVector sizes : reference = " << br->dof3_ref.size() << ", target = " << br->dof3_target.size() << ", normal = " << br->dof3_normal.size();
					  reposType = 1;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (br->dof3_ref.size() < 2 || br->dof3_target.size() < 2 || br->dof3_normal.size() < 2)
						  return;
					  angleLimitMin = br->dof3_limit_min;
					  angleLimitMax = br->dof3_limit_max;
					  reference1 = br->dof3_ref.at(0); 	  reference2 = br->dof3_ref.at(1);
					  normal1 = br->dof3_normal.at(0); 	  normal2 = br->dof3_normal.at(1);
					  target1 = br->dof3_target.at(0);    target2 = br->dof3_target.at(1);
					  break;
				  }
				  //Twisting
				  case 1:{
					  std::cout << "Twisting";
					  std::cout << "\nVector sizes : reference = " << br->dof2_ref.size() << ", target = " << br->dof2_target.size() << ", normal = " << br->dof2_normal.size();
					  reposType = 2;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (br->dof2_ref.size() < 2 || br->dof2_target.size() < 2 || br->dof2_normal.size() < 2)
						  return;
					  angleLimitMin = br->dof2_limit_min;
					  angleLimitMax = br->dof2_limit_max;
					  reference1 = br->dof2_ref.at(0); 	  reference2 = br->dof2_ref.at(1);
					  normal1 = br->dof2_normal.at(0); 	  normal2 = br->dof2_normal.at(1);
					  target1 = br->dof2_target.at(0);    target2 = br->dof2_target.at(1);
					  break;
				  }
			  }
		  }
		  else if(m_meta.ctrl_line.getJoint(currentElement)){
			  ContourCLj* joint = m_meta.ctrl_line.getJoint(currentElement);
			  switch (currentDofIndex){
				  //Flexion
				  case 0:{
					  std::cout << "Flexion-Extension";
					  std::cout << "\nVector sizes : reference = " << joint->dof1_ref.size() << ", target = " << joint->dof1_target.size() << ", normal = " << joint->dof1_normal.size();
					  reposType = 0;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (joint->dof1_ref.size() < 2 || joint->dof1_target.size() < 2 || joint->dof1_normal.size() < 2)
						  return;
					  angleLimitMin = joint->dof1_limit_min;
					  angleLimitMax = joint->dof1_limit_max;
					  reference1 = joint->dof1_ref.at(0); 	  reference2 = joint->dof1_ref.at(1);
					  normal1 = joint->dof1_normal.at(0); 	  normal2 = joint->dof1_normal.at(1);
					  target1 = joint->dof1_target.at(0);    target2 = joint->dof1_target.at(1);
					  break;
				  }
				  //Lateral Flexion OR Abduction 
				  case 2:{
					  std::cout << "Lateral Flexion OR Abduction ";
					  std::cout << "\nVector sizes : reference = " << joint->dof3_ref.size() << ", target = " << joint->dof3_target.size() << ", normal = " << joint->dof3_normal.size();
					  reposType = 1;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (joint->dof3_ref.size() < 2 || joint->dof3_target.size() < 2 || joint->dof3_normal.size() < 2)
						  return;
					  angleLimitMin = joint->dof3_limit_min;
					  angleLimitMax = joint->dof3_limit_max;
					  reference1 = joint->dof3_ref.at(0); 	  reference2 = joint->dof3_ref.at(1);
					  normal1 = joint->dof3_normal.at(0); 	  normal2 = joint->dof3_normal.at(1);
					  target1 = joint->dof3_target.at(0);    target2 = joint->dof3_target.at(1);
					  break;
				  }
				  //Twisting
				  case 1:{
					  std::cout << "Twisting";
					  std::cout << "\nVector sizes : reference = " << joint->dof2_ref.size() << ", target = " << joint->dof2_target.size() << ", normal = " << joint->dof2_normal.size();
					  reposType = 2;
					  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
					  // if the landmark information is not mentioned in CL, skip all calculations for this movement and move on to the next
					  if (joint->dof2_ref.size() < 2 || joint->dof2_target.size() < 2 || joint->dof2_normal.size() < 2)
						  return;
					  angleLimitMin = joint->dof2_limit_min;
					  angleLimitMax = joint->dof2_limit_max;
					  reference1 = joint->dof2_ref.at(0); 	  reference2 = joint->dof2_ref.at(1);
					  normal1 = joint->dof2_normal.at(0); 	  normal2 = joint->dof2_normal.at(1);
					  target1 = joint->dof2_target.at(0);    target2 = joint->dof2_target.at(1);
					  break;
				  }
			  }
		  }
		  
		  std::vector<std::string> landmarks = { normal1, normal2, target1, target2, reference1, reference2 };
		  bool flag = true;
		  for (int i = 0; i < landmarks.size(); i++)
			  if (!m_meta.hasLandmark(landmarks.at(i))){
				  flag = false;
			  }
		  if (!flag){
			  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, reposAngleLimit };
			  return;
		  }
		
		  std::vector<std::string> reference_landmarks = { reference1, reference2 };
		  std::vector<std::string> nomral_vector_landmarks = { normal1, normal2 };
		  std::vector<std::string> target_landmarks = { target1, target2 };

		  std::vector<Coord> reference_landmarks_coords, nomral_vector_landmarks_coords, target_landmarks_initial_coords, target_landmarks_final_coords;

		  reference_landmarks_coords = { m_meta.landmark(reference1).position(m_fem), m_meta.landmark(reference2).position(m_fem) };
		  nomral_vector_landmarks_coords = { m_meta.landmark(normal1).position(m_fem), m_meta.landmark(normal2).position(m_fem) };
		  target_landmarks_initial_coords = { m_meta.landmark(target1).position(m_fem), m_meta.landmark(target2).position(m_fem) };
		  
		  currentAngle = calculateAngle(target_landmarks_initial_coords, reference_landmarks_coords, nomral_vector_landmarks_coords);
		  
		  reference_landmarks_coords = { getLandmarkTargetCoordinates(reference1), getLandmarkTargetCoordinates(reference2) };
		  nomral_vector_landmarks_coords = { getLandmarkTargetCoordinates(normal1), getLandmarkTargetCoordinates(normal2) };
		  target_landmarks_final_coords = { getLandmarkTargetCoordinates(target1), getLandmarkTargetCoordinates(target2) };

		  angleTarget = calculateAngle(target_landmarks_final_coords, reference_landmarks_coords, nomral_vector_landmarks_coords);
		  
		  std::cout << std::endl << " Target angle = " << angleTarget << "\t Current angle = " << currentAngle << reposComputedAngle;
		  reposComputedAngle = angleTarget - currentAngle;
		  // If the angle limits for the max and min angle are equal then they are not defined in the contour Cl, hence just calculating the angle without cosidering the limits
		  if (angleLimitMax == angleLimitMin){
			  reposPossibleAngle = reposComputedAngle;
			  reposAngleLimit = -999;
		  }
		  // IF the target angle exceeds the max limit
		  else if ( angleTarget > angleLimitMax){
			  reposPossibleAngle = angleLimitMax - currentAngle;
			  reposAngleLimit = angleLimitMax;
		  }
		  // IF the target angle is lower than the min limit
		  else if (angleTarget < angleLimitMin){
			  reposPossibleAngle = angleLimitMin - currentAngle;
			  reposAngleLimit = angleLimitMin;
		  }
		  // If the target angle is within range
		  else{
			  reposPossibleAngle = reposComputedAngle;
		  }
		  
		  reposParameters = { reposFunc, reposType, reposSide, reposPossibleAngle, reposComputedAngle, currentAngle, reposAngleLimit };
	  }

	  double TargetSynth::calculateAngle(std::vector<Coord> vector1pts, std::vector<Coord> vector2pts, std::vector<Coord> normalpts){
		  double vector1proj[3], vector2proj[3];
		  double vector1[3] = { vector1pts.at(1)[0] - vector1pts.at(0)[0], vector1pts.at(1)[1] - vector1pts.at(0)[1], vector1pts.at(1)[2] - vector1pts.at(0)[2] };
		  double vector2[3] = { vector2pts.at(1)[0] - vector2pts.at(0)[0], vector2pts.at(1)[1] - vector2pts.at(0)[1], vector2pts.at(1)[2] - vector2pts.at(0)[2] };
		  double normalVec[3] = { normalpts.at(1)[0] - normalpts.at(0)[0], normalpts.at(1)[1] - normalpts.at(0)[1], normalpts.at(1)[2] - normalpts.at(0)[2] };
		  double origin[3] = { normalVec[0] / 2, normalVec[1] / 2, normalVec[2] / 2 };
		  vtkMath::Normalize(normalVec);
		  vtkMath::Normalize(vector2);
		  vtkMath::Normalize(normalVec);
		  double normal[3] = { normalVec[0], normalVec[1], normalVec[2] };
		  vtkPlane::ProjectVector(vector1, origin, normal, vector1proj);
		  vtkPlane::ProjectVector(vector2, origin, normal, vector2proj);
		  double angle = vtkMath::DegreesFromRadians(vtkMath::AngleBetweenVectors(vector1proj, vector2proj));
		  angle = roundf(angle * 100) / 100;
		  return angle;
	  }
    
	}

}
