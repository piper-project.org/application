// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow
{
    title: "Assign Body Regions For Landmark Targets"

    minimumHeight : 200
    minimumWidth : 350
    //Component.onCompleted: setCurrentWindowSizeFixed()
    property int nb: 0;
    ColumnLayout
    {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop
        anchors.centerIn : parent
        anchors.fill : parent
        RowLayout
        {
            visible: false
            width:childrenRect.width
            Button
            {
                text: "none"
                tooltip: "clear selection"
                onClicked:
                {
                    tableView.selection.clear();
                    noneSelection();
                }
            }
            Button
            {
                text: "all"
                tooltip: "select all"
                onClicked:
                {
                    allSelection();
                }
            }
            Button
            {
                text: "invert"
                tooltip: "invert selection"
                onClicked:
                {
                    invertSelection()
                }
            }

        }
        ListModel
        {
            id:listModel
        }
        TableView
        {
            id: tableView
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            focus: false
            headerVisible:true
            selectionMode:SelectionMode.SingleSelection
            model: listModel
            property variant vals;
            onRowCountChanged:          //Every time the nb of elements in the list change
            {
                //allSelection()
            }

            function getList()
            {
                listModel.clear();
                nb = 0;
                vals = contextMetaManager.getTargetList();
                var types = contextMetaManager.getTargetTypeList();
                for(var i =0; i<vals.length; i++)
                {
                    listModel.append({target: vals[i] , joint:"Select a body region"/*, type: types[i]*/});
                    nb++;
                }
                tableView.selection.clear();
            }
            function toggleDisplay(index)
            {
                if(tableView.selection.contains(index))
                {
                    contextMetaManager.targetDisplay(1, listModel.get(index).role);
                }
                else
                {
                    contextMetaManager.targetDisplay(0, listModel.get(index).role);
                }
            }
            TableViewColumn
            {
                role: "target"
                title: "Landmark Targets"
                width:200
            }

            TableViewColumn
            {
                id:bRColumn
                role: "apply"
                title: "Body Regions"
                width:100
                property var bodyRegions : [ "pelvis", "thorax", "neck", "upperarml", "forearml", "handl", "upperarmr", "forearmr", "handr", "tighl", "ankletocalfl", "footl", "tighr", "ankletocalfr", "footr" ]
                property var joints : ["pelvic_joint", "cervical_joint", "head", "shoulderl", "elbowl", "wristl", "shoulderr", "elbowr", "wristr", "hipl", ,"kneel", "anklel", "hipr", ,"kneer", "ankler"]
                delegate:  ComboBox
                {
                    id:brChoices
                    model: [ "pelvis", "thorax", "neck", "upperarml", "forearml", "handl", "upperarmr", "forearmr", "handr", "tighl", "ankletocalfl", "footl", "tighr", "ankletocalfr", "footr", "pelvic_joint", "cervical_joint", "head", "shoulderl", "elbowl", "wristl", "shoulderr", "elbowr", "wristr", "hipl", "kneel", "anklel", "hipr", "kneer", "ankler" ]
                    currentIndex: -1

                    onCurrentIndexChanged:{
                        if(tableView.currentRow != -1 ){
                            console.log(listModel.get(tableView.currentRow).target+"\t"+currentText)
                            myContourDeformation.ui_addbrToTargetLandmark(listModel.get(tableView.currentRow).target,currentText)
                            tableView.selection.clear()
                            tableView.currentRow = -1
                        }
                        else{
                            messageDialog.visible = true
                        }
                    }

                }
            }
//        MessageDialog {
//            id: messageDialog
//            title: "Alert"
//            text: "Please first select a landmark listed in the table"
//            onAccepted: {
//            }
//            Component.onCompleted: visible = false
//        }

            TableViewColumn
            {
                role: "joint"
                title: "Parent joint"
                width:200
            }
            onClicked:{
                //brChoices.enabled = true

            }

            Component.onCompleted:
            {
                getList();
            }
            Connections{
                target: context
                onModelChanged : {
                    tableView.getList();
                }
            }
        }

        GroupBox{
            RowLayout{
                Button{
                    text:"Decide body regions from Anatomy DB"
                }
                Button{
                    text:"Choose body region graphically for selected landmark"
                }
            }
        }
        GroupBox{
            ColumnLayout{
                RowLayout{
                    Button{
                        text:"Display all landmarks"
                    }
                    Button{
                        text:"Display unmapped landmarks"
                    }

                }
                Button{
                    text:"Display ContourCL & Body Regions"
                }
            }
        }

    }

    function invertSelection()
    {
        for(var i=0;i<nb;i++)
        {
            if(tableView.selection.contains(i))
            {
                tableView.selection.deselect(i)
                contextMetaManager.targetDisplay(0, listModel.get(i).role);
            }
            else
            {
                tableView.selection.select(i)
                contextMetaManager.targetDisplay(1, listModel.get(i).role);
            }
        }
    }

    function noneSelection()
    {
        tableView.selection.clear();
        for(var i=0;i<nb;i++)
        {
            contextMetaManager.targetDisplay(0, listModel.get(i).role);
        }
    }

    function allSelection()
    {
        tableView.selection.selectAll();
        tableView.focus = true;
        for(var i=0;i<nb;i++)
        {
            contextMetaManager.targetDisplay(1, listModel.get(i).role);
        }
    }
}
