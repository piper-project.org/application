/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef TARGETSYNTH_H
#define TARGETSYNTH_H

/** @brief 	A class for determining joint repositioning angles from targets
*
* @author Sukhraj Singh @date 2016
*/

//eigen includes
#include <Eigen/Dense>
#include <Eigen/Geometry>

//hbm includes
#include "contours/ComputeTarget.h"


namespace piper {
    namespace contourdeformation {
        class TargetSynth {

        public:
            void ComputeJointAngleFromTarget();
            std::pair<int,std::string> GetBRandAngle();
            void markUnprocessed(hbm::ContourCLbr* root);

            TargetSynth();

			hbm::LandmarkTarget target_lanmk;
			hbm::ContourCLj* joint_pointer;

			// currentElementStatus : the first attribute tells the current position in the vector 
			// the second attribute tell how many degrees of freedom have been processes of the current BR/Joint
			std::pair<int, int> currentElementStatus;
			// 
			std::map<std::string, std::vector<int> > elementStatusMap;

			// This function is used to change the current br/joint and the current degree of freedom
			void updateElementStatus(int, int);

			// This vector holds the order of BR/joints that need to be repositioned 
			std::vector<std::string> CLElementVector;

			// This vector holds the currently calculated values of reposFunc, reposType, reposSide, computedAngle , reposAngle and angleLimit for the current Cl element repositioning
			std::vector<double> reposParameters;

			//std::pair<std::string, std::string> currentJointAndDOF;
			
			// This function calculates 
			void calculateReposParameters();

			// this function initializes values of currentElementStatus, CLElementVector, degreeOfFreedomForElement
			void init();
			void hardcodingStuff();
			void initializeCLElementVector();

			// this functions return the reposFunc, reposType, reposSide, computedAngle and reposAngle for the current Cl element repositioning
			std::vector<double> getReposParameters();

			// Hardcoded maps for reposFunc & side
			std::map<std::string, int> reposFuncMap;
			std::map<std::string, int> reposSideMap;

			std::map<std::string, std::vector<int>> reposOrder;

			hbm::LandmarkTarget getLandmarkTargetObject(std::string target);
			hbm::Coord getLandmarkTargetCoordinates(std::string target);

			
			double calculateAngle(std::vector<hbm::Coord> vector1pts, std::vector<hbm::Coord> vector2pts, std::vector<hbm::Coord> normalpts);

        private:

            int Process(hbm::ContourCLbr *root);

            //Computes distance of a point from a joint axes
            double GetAxesToPointDistance(hbm::Coord a1, hbm::Coord a2, hbm::Coord l);

            double GetAxesToPointDistance2(hbm::Coord a1, hbm::Coord a2, hbm::Coord l);

            //Gets farthest target from current provided axes
            std::string GetFarthestTargetFromAxes( std::vector <std::string> axis, std::vector <std::string> l_mark_targets);

            //Get relevant targets for frames.
            std::vector<hbm::NodePtr> ComputeFrameTargets(std::vector<std::pair<double*, double*>> axes, std::vector<hbm::NodePtr> imp_targets);

            std::vector<std::string> GetTransformedLandmarks();

            void ComputeTargetEntityMap(std::vector<std::string>& targetList);

            void ComputeBREntityMap();

            Eigen::Matrix3d GetFrame(std::vector < std::string > l_mark, bool isTarget);

            void ComputeImportantTargets();

            void CreateFrameFromTargets();

            std::vector < std::string > GetBRLandmarks(std::string brname);

            std::string GetAnatomyBR(std::string brname);

            void LogBRTargets(std::map<std::string,std::vector<std::string> > mymap);

            void LogVec(std::vector<std::string>& logvec);

            void LevelTraverseContourCL();

            void traverseCL(hbm::ContourCLbr* root);

           std::vector<std::string> GetTransformedLandmarksofBR(std::vector<std::string> landmark_list);

           double FindPrimaryRotationAngle(std::vector<std::string> axes, std::string l_mark);

           std::vector < double > FindAxesRotationAngle(std::vector <std::string> axes1, std::vector <std::string> axes2,
                                                     std::vector <std::string> axes3, std::vector<std::string> l_mark);
           Eigen::Quaterniond QuaternionRot(Eigen::Vector3d x1, Eigen::Vector3d y1, Eigen::Vector3d z1,
                                      Eigen::Vector3d x2, Eigen::Vector3d y2, Eigen::Vector3d z2);

		   


        private:
            std::map<std::string,std::vector<std::string>> m_bone_targets;
            std::map<std::string,std::vector<std::string>> m_br_bones;
            std::vector<std::string> m_trans_lmarks;
            std::pair<int,std::string> m_resultantAngleBr;
            std::string m_nextbr;

        };

    }
}











#endif // TARGETSYNTH_H
