/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ContourGen.h"

#include <vtkParametricSpline.h>
#include <vtkParametricFunctionSource.h>
#include <vtkLineSource.h>

//For Gebod Circums
#include <vtkCutter.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkCenterOfMass.h>

#define JOINT_CONTOUR_COUNT 8
#define DEFAULT_CONTOUR_COUNT 10
#define THIGH_CONTOUR_COUNT 15
#define CALF_CONTOUR_COUNT 18
#define FOOT_CONTOUR_COUNT 20
#define SKIP_CONTOUR 4
#define THORAX_CONTOUR_COUNT 18
#define NECK_CONTOUR_COUNT 20
#define HIP_CONTOUR_COUNT 12

using namespace std;
using namespace piper::hbm;
using namespace Eigen;

namespace piper {
	namespace contours {

	//Intialize the instance
	bool ContourGen::IsCreated = false;
	ContourGen* ContourGen::m_ContourGen = NULL;

    ContourGen::ContourGen() : main_repo(QCoreApplication::applicationDirPath().toStdString())
	{

	}

	void ContourGen::Init()
	{
		Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::ENTITIES | DISPLAY_MODE::GEBOD | DISPLAY_MODE::AXES | DISPLAY_MODE::POINTS | DISPLAY_MODE::LANDMARKS);
		//Context::instance().display().SetPickableDisplayMode(DISPLAY_MODE::GEBOD);
		//vtkSmartPointer<vtkMousePickStylePIPER> style = vtkMousePickStylePIPER::SafeDownCast(Context::instance().display().GetRenderWindow()->GetInteractor()->GetInteractorStyle());
		//style->SetPickingType(SELECTION_TARGET::ENTITIES, false, false);

		//Do cleanup
		RemoveSkeletonActors();

		//Remove VTK Actors for contours.
		RemoveContourActors();

		IsContourCreated = false;

	}


	//http://www.codeproject.com/Articles/1921/Singleton-Pattern-its-implementation-with-C
	ContourGen* ContourGen::getInstance()
	{
		if (!IsCreated)
		{
			m_ContourGen = new ContourGen();
			IsCreated = true;
			return m_ContourGen;
		}
		else
		{
			return m_ContourGen;
		}
	}


	ContourGen::~ContourGen()
	{

	}



	///Compute the joint skeleton, which is a spline
	void ContourGen::ComputeJointSkeleton(std::string name)
	{

		vector<double> curve_point;

		curve_point = ComputePointsFromBezier(bezier_ctrl_pts, 0.0);
		double p0[3] = { curve_point[0], curve_point[1], curve_point[2] };

		curve_point = ComputePointsFromBezier(bezier_ctrl_pts, 0.35);
		double p1[3] = { curve_point[0], curve_point[1], curve_point[2] };

		curve_point = ComputePointsFromBezier(bezier_ctrl_pts, 0.65);
		double p2[3] = { curve_point[0], curve_point[1], curve_point[2] };


		curve_point = ComputePointsFromBezier(bezier_ctrl_pts, 1.0);
		double p3[3] = { curve_point[0], curve_point[1], curve_point[2] };

		// Create a vtkPoints object and store the points in it
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		points->InsertNextPoint(p0);
		points->InsertNextPoint(p1);
		points->InsertNextPoint(p2);
		points->InsertNextPoint(p3);

		vtkSmartPointer<vtkParametricSpline> spline =
			vtkSmartPointer<vtkParametricSpline>::New();
		spline->SetPoints(points);

		vtkSmartPointer<vtkParametricFunctionSource> functionSource =
			vtkSmartPointer<vtkParametricFunctionSource>::New();
		functionSource->SetParametricFunction(spline);
		functionSource->Update();

		//concatenate _splineelem to the bodyregion
		std::stringstream jname;
		jname << name << "_splineelem";
		pDebug() << jname.str();
		Context::instance().display().AddVtkPointSet(functionSource->GetOutput(), jname.str(), false, DISPLAY_MODE::GEBOD);
		Context::instance().display().SetObjectColor(jname.str(), 1.0, 1.0, 0, 0.8);
		Context::instance().display().SetObjectWidth(jname.str(), 3);
		CLActors.push_back(jname.str());
		jname.str(std::string());
		jname.clear();
	}



    tinyxml2::XMLError ContourGen::ReadContourCL(string file)
	{
		std::cout << std::endl << "ContourCL Parsing starts";
        tinyxml2::XMLError status = parsecontcl.readContourCLxml(m_fem, m_meta, file.c_str());
		std::cout << std::endl << "Parsing status " << status;
		return status;
	}

	bool ContourGen::ComputeSkeleton()
	{

		//Populate the VTK DS from the intitalized hierarchy
		std::cout << "Populate VTK constructs for contourCL";

		PopulateVTKConstructs(m_fem, m_meta);

		//Refresh the display
		Context::instance().display().Refresh();
		std::cout << "\nCompute skeleton completed !!!";
		return true;
	}



	void ContourGen::ComputeContours()
	{
		//Init hierarchy
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();

		if (IsContourCreated == false)
		{

			//clear buffers
			ClearPushedContours();

			//Populate the VTK DS from the intitalized hierarchy
			CreateContoursFromSkeleton(m_fem, m_meta);

			//Uncomment following code to test contour for particular BR or Joint
			//GetContoursforJoint("kneel");
			//GetContourforBR("Neck");

			IsContourCreated = true;
		}
		else
		{
			HideContoursListForBR(0.8);

		}

		//Refresh the display
		Context::instance().display().Refresh();
	}


	void ContourGen::ForceContourGen()
	{
		//Remove VTK Actors for contours.
		RemoveContourActors();

		//Write here call to clear Pushed ContourPoints
		ClearPushedContours();

		//Set the status to false
		IsContourCreated = false;

		//Compute contours.
		ComputeContours();

	}


	///Compute bezier control points from hermite curve
	void ContourGen::ComputeBezierCurve(MatrixXd spline_matrix)
	{
		bezier_ctrl_pts = transformObj.convertSplineToBezier(spline_matrix);
	}


	void ContourGen::GetSplineTangent(MatrixXd bezier_ctrl_pts, double u, double* normal)
	{

		vector<double> curve_point1, curve_point2;
		curve_point1 = ComputePointsFromBezier(bezier_ctrl_pts, u + 0.01);
		curve_point2 = ComputePointsFromBezier(bezier_ctrl_pts, u - 0.01);

		normal[0] = curve_point1[0] - curve_point2[0];
		normal[1] = curve_point1[1] - curve_point2[1];
		normal[2] = curve_point1[2] - curve_point2[2];

	}


	///Compute the point for a bezier curve from a given u value
	std::vector<double> ContourGen::ComputePointsFromBezier(MatrixXd bezier_ctrl_pts, double u)
	{
		double mum1, mum13, mu3;
		VectorXd temp_vector(3);
		vector<double> p, p1, p2, p3, p4;

		temp_vector = bezier_ctrl_pts.row(0);
		p1.resize(temp_vector.size());
		VectorXd::Map(&p1[0], temp_vector.size()) = temp_vector;

		temp_vector = bezier_ctrl_pts.row(1);
		p2.resize(temp_vector.size());
		VectorXd::Map(&p2[0], temp_vector.size()) = temp_vector;

		temp_vector = bezier_ctrl_pts.row(2);
		p3.resize(temp_vector.size());
		VectorXd::Map(&p3[0], temp_vector.size()) = temp_vector;

		temp_vector = bezier_ctrl_pts.row(3);
		p4.resize(temp_vector.size());
		VectorXd::Map(&p4[0], temp_vector.size()) = temp_vector;


		mum1 = 1.0f - u;
		mum13 = mum1 * mum1 * mum1;
		mu3 = u*u*u;
		p.resize(3);
		p[0] = mum13*p1[0] + 3 * u*mum1*mum1*p2[0] + 3 * u*u*mum1*p3[0] + mu3*p4[0];
		p[1] = mum13*p1[1] + 3 * u*mum1*mum1*p2[1] + 3 * u*u*mum1*p3[1] + mu3*p4[1];
		p[2] = mum13*p1[2] + 3 * u*mum1*mum1*p2[2] + 3 * u*u*mum1*p3[2] + mu3*p4[2];

		return(p);

	}

	void ContourGen::AddEndPointSphere(double* p, std::string lin_endname)
	{

		vtkSmartPointer<VtkDisplayActor> pointActor = draw.point(p[0], p[1], p[2], 3);
		Context::instance().display().AddActor(pointActor, lin_endname, false, DISPLAY_MODE::POINTS, true, false);
	}

	void ContourGen::AddLctrlptSphere(NodePtr p, std::string lin_endname)
	{
		double ptcord[3];
		ptcord[0] = p->getCoordX();
		ptcord[1] = p->getCoordY();
		ptcord[2] = p->getCoordZ();
		vtkSmartPointer<VtkDisplayActor> pointActor = draw.point(ptcord[0], ptcord[1], ptcord[2], 3);
		Context::instance().display().AddActor(pointActor, lin_endname, false, DISPLAY_MODE::POINTS, true, false);
	}


	void ContourGen::AddLinearSkeletonElement(double* p0, double* p1, std::string name)
	{
		vtkSmartPointer<vtkLineSource> lineSource =
			vtkSmartPointer<vtkLineSource>::New();

		lineSource->SetPoint1(p0);
		lineSource->SetPoint2(p1);
		lineSource->Update();

		std::stringstream liname;
		//concatenate _linelem to the bodyregion
		liname << name << "_linelem";
		Context::instance().display().AddVtkPointSet(lineSource->GetOutput(), liname.str(), false, DISPLAY_MODE::GEBOD);
		Context::instance().display().SetObjectColor(liname.str(), 1.0, 1.0, 0, 0.8);
		Context::instance().display().SetObjectWidth(liname.str(), 5);
		CLActors.push_back(liname.str());
		liname.str(std::string());
		liname.clear();

	}


	void ContourGen::AddCLbrElement(vector<NodePtr> lctrlpts, std::string name)
	{

		// Create a vtkPoints object and store the points in it
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		if (lctrlpts.size() > 1){
			if (lctrlpts.size() > 2){
				for (auto it = lctrlpts.begin(); it != lctrlpts.end(); it++){
					double p[3];
					NodePtr newnode;
					newnode = *it;
					p[0] = newnode->getCoordX();
					p[1] = newnode->getCoordY();
					p[2] = newnode->getCoordZ();
					points->InsertNextPoint(p);
				}

				vtkSmartPointer<vtkParametricSpline> spline =
					vtkSmartPointer<vtkParametricSpline>::New();
				spline->SetPoints(points);

				vtkSmartPointer<vtkParametricFunctionSource> functionSource =
					vtkSmartPointer<vtkParametricFunctionSource>::New();
				functionSource->SetParametricFunction(spline);
				functionSource->Update();

				//concatenate _splineelem to the bodyregion
				std::stringstream brname;
				brname << name << "_splineelem";
				Context::instance().display().AddVtkPointSet(functionSource->GetOutput(), brname.str(), false, DISPLAY_MODE::GEBOD);
				Context::instance().display().SetObjectColor(brname.str(), 1.0, 1.0, 0, 0.8);
				Context::instance().display().SetObjectWidth(brname.str(), 5);
				brname.str(std::string());
				brname.clear();
			}
			else{
				vtkSmartPointer<vtkLineSource> lineSource =
					vtkSmartPointer<vtkLineSource>::New();
				if (name == "Neck"){
					pDebug() << "entered clbrelement func";
				}
				for (auto it = lctrlpts.begin(); it != lctrlpts.end(); it++){
					double p[3];
					NodePtr newnode;
					newnode = *it;
					p[0] = newnode->getCoordX();
					p[1] = newnode->getCoordY();
					p[2] = newnode->getCoordZ();
					points->InsertNextPoint(p);
				}
				if (name == "Neck"){
					pDebug() << "pushed the coordinates into points";
				}

				lineSource->SetPoints(points);
				//				lineSource->SetPoint2(p1);
				lineSource->Update();

				std::stringstream liname;
				//concatenate _linelem to the bodyregion
				liname << name << "_linelem";
				if (name == "Neck"){
					pDebug() << "cancatinating lineem";
				}
				Context::instance().display().AddVtkPointSet(lineSource->GetOutput(), liname.str(), false, DISPLAY_MODE::GEBOD);
				Context::instance().display().SetObjectColor(liname.str(), 1.0, 1.0, 0, 0.8);
				Context::instance().display().SetObjectWidth(liname.str(), 5);
				liname.str(std::string());
				liname.clear();
				if (name == "Neck"){
					pDebug() << "end of add cl br element function";
				}

			}
		}
	}


	MatrixXd ContourGen::GetHermiteSpline(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br, int iter)
	{
		Coord p0, p1, z1, z2;
		Coord tant1, tant2;
		if (!current_br->childrenjts.at(iter)->joint_splinep0.empty() &&
			!current_br->childrenjts.at(iter)->joint_splinep1.empty() &&
			!current_br->childrenjts.at(iter)->joint_splinet1.empty() &&
			!current_br->childrenjts.at(iter)->joint_splinet2.empty()
			)
		{
			if (m_meta.hasLandmark(current_br->childrenjts.at(iter)->joint_splinep0))
//				p0 = m_meta.landmark(current_br->childrenjts.at(iter)->joint_splinep0).position(m_fem);
				p0 = process_func.getKeyPointCoord(m_fem, m_meta, current_br->childrenjts.at(iter)->joint_splinep0);
			else
				std::cout << std::endl << "landmark not found !!!!!";
			if (m_meta.hasLandmark(current_br->childrenjts.at(iter)->joint_splinep1))
//				p1 = m_meta.landmark(current_br->childrenjts.at(iter)->joint_splinep1).position(m_fem);
				p1 = process_func.getKeyPointCoord(m_fem, m_meta, current_br->childrenjts.at(iter)->joint_splinep1);
			else
				std::cout << std::endl << "landmark not found !!!!!";
			if (m_meta.hasLandmark(current_br->childrenjts.at(iter)->joint_splinet1))
//				z1 = m_meta.landmark(current_br->childrenjts.at(iter)->joint_splinet1).position(m_fem);
				z1 = process_func.getKeyPointCoord(m_fem, m_meta, current_br->childrenjts.at(iter)->joint_splinet1);
			else
				std::cout << std::endl << "landmark not found !!!!!";
			if (m_meta.hasLandmark(current_br->childrenjts.at(iter)->joint_splinet2))
//				z2 = m_meta.landmark(current_br->childrenjts.at(iter)->joint_splinet2).position(m_fem);
				z2 = process_func.getKeyPointCoord(m_fem, m_meta, current_br->childrenjts.at(iter)->joint_splinet2);
			else
				std::cout << std::endl << "landmark not found !!!!!";
		}

		if (current_br->childrenjts.at(iter)->spline_tangent.size() == 2){
			tant1 = (p0 - z1) / current_br->childrenjts.at(iter)->spline_tangent.at(1); //(p0-z1)/1.5;
			tant2 = (z2 - p1) / current_br->childrenjts.at(iter)->spline_tangent.at(0);  //(z2 - p1)/1.2;
		}
		else{
			std::cout << std::endl << "No tangent magnitudes specified for " << current_br->childrenjts.at(iter)->name;
			tant1 = p0 - z1;
			tant2 = z2 - p1;
		}

		MatrixXd spline_matrix(4, 3);
		spline_matrix << p0(0), p0(1), p0(2),
			p1(0), p1(1), p1(2),
			tant1(0), tant1(1), tant1(2),
			tant2(0), tant2(1), tant2(2);

		if (current_br->childrenjts.at(iter)->name == "kneel"){
			std::ofstream kneelspline("kneelspline.txt");
			kneelspline << spline_matrix;
		}

		return(spline_matrix);
	}





	vtkSmartPointer<vtkPoints> ContourGen::ScaleCircumference(vtkSmartPointer<vtkPoints> cutter_points,
		double scaling_factor, double Origin[3])
	{

		vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();

		int NumPts = cutter_points->GetNumberOfPoints();

		double p[3];
		double translate_p[3], scale_p[3], scaled_p[3];
		for (int i = 0; i < NumPts; i++)
		{
			cutter_points->GetPoint(i, p);

			//Translate to the local coordinate space
			translate_p[0] = p[0] - Origin[0];
			translate_p[1] = p[1] - Origin[1];
			translate_p[2] = p[2] - Origin[2];

			//Scale the coords now
			scale_p[0] = translate_p[0] * scaling_factor;
			scale_p[1] = translate_p[1] * scaling_factor;
			scale_p[2] = translate_p[2] * scaling_factor;


			//Translate to WC
			scaled_p[0] = scale_p[0] + Origin[0];
			scaled_p[1] = scale_p[1] + Origin[1];
			scaled_p[2] = scale_p[2] + Origin[2];

			//Push to updated set of points
			scaled_cutter_points->InsertNextPoint(scaled_p);

		}

		return scaled_cutter_points;
	}

	//Set the scaling value for the circumelements.
	void ContourGen::SetScalingValue(double scale_value, std::string circum_element)
	{
		ScalingValue = scale_value;
		scalingregion = circum_element;
		std::string name;



		if (circum_element == "THIGH_CIRCUMFERENCE")
			name = "thighr_circumelem";
		else if (circum_element == "CALF_CIRCUMFERENCE")
			name = "ankletocalfr_circumelem";
		else if (circum_element == "FOREARM_CIRCUMFERENCE")
			name = "forearmr_circumelem";

		if (std::find(CircumActors.begin(), CircumActors.end(), name) != CircumActors.end())
		{
            boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();

			auto it = actors->find(name);
			if (it != actors->end())
			{
				vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
				Context::instance().display().SelectActor(it->second, APPEND_ALL, false);


				//Compute the center of mass
				vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter =
					vtkSmartPointer<vtkCenterOfMass>::New();

				centerOfMassFilter->SetInputData(inputPolyData);
				centerOfMassFilter->SetUseScalarsAsWeights(false);
				centerOfMassFilter->Update();
				double center[3];
				centerOfMassFilter->GetCenter(center);


				vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
				scaled_cutter_points = ScaleCircumference(inputPolyData->GetPoints(), ScalingValue, center);
				inputPolyData->SetPoints(scaled_cutter_points);
			}
			else
			{
				pWarning() << "Actor " << name << "not present";
			}


		}
		else
			pWarning() << "Element not present";
	}


	///Creates Contour for a provided body region
	vtkSmartPointer<vtkPolyData> ContourGen::CreateContourForBr(vtkUnstructuredGrid* PolyDataBR, double Origin[],
		double Normal[], int ContourCount, double range[])
	{
		// Create a plane to cut
		vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
		plane->SetOrigin(Origin);
		plane->SetNormal(Normal);


		// Create cutter
		vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();

		//Set cutting function- Here a plane
		cutter->SetCutFunction(plane);

		//Set Cutter Input data
		cutter->SetInputData(PolyDataBR);

		//Set Cutter bounds
		//cutter->SetNumberOfContours(1);
		cutter->GenerateValues(ContourCount, range);
		cutter->Update();

		vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
		polyDataOut->DeepCopy(cutter->GetOutput());

		return polyDataOut;
	}

	///Compute contours for a joint
	void ContourGen::ComputeContourForJoint(ContourCLbr* current_br, int index)
	{
		//TODO: Find necessary contours required for a joint
		double no_of_contours = JOINT_CONTOUR_COUNT;

		pDebug() << current_br->childrenjts.at(index)->name;
		//Get the Model Actor
		std::string BodyRegion;

		pDebug() << "skingenericmetadata : " << current_br->skingenericmetadata;
		BodyRegion = current_br->skingenericmetadata;

		if (!BodyRegion.empty())
		{

            boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();

			auto it = actors->find(BodyRegion);
			if (it != actors->end())
			{

				vtkUnstructuredGrid *inputPolyData = vtkUnstructuredGrid::SafeDownCast(it->second->GetOriginalDataSet());

				double range[2], Normal[3];
				vector<double> curve_point;
				range[0] = -1.0;
				range[1] = 1.0;

				curve_point = ComputePointsFromBezier(bezier_ctrl_pts, 0.0);
				double old_p[3] = { curve_point[0], curve_point[1], curve_point[2] };


				for (double contour_count = 1; contour_count <= no_of_contours; contour_count++)
				{

					curve_point = ComputePointsFromBezier(bezier_ctrl_pts, contour_count / no_of_contours);


					double curr_p[3] = { curve_point[0], curve_point[1], curve_point[2] };

					//Normal[0] = curr_p[0] - old_p[0];
					//Normal[1] = curr_p[1] - old_p[1];
					//Normal[2] = curr_p[2] - old_p[2];
					GetSplineTangent(bezier_ctrl_pts, contour_count / no_of_contours, Normal);
					double uval = contour_count / no_of_contours;

					// Create a plane to cut
					vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
					plane->SetOrigin(curr_p);
					plane->SetNormal(Normal);


					// Create cutter
					vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();

					//Set cutting function- Here a plane
					cutter->SetCutFunction(plane);

					//Set Cutter Input data
					cutter->SetInputData(inputPolyData);

					//Set Cutter bounds
					cutter->GenerateValues(1, range);
					cutter->Update();

					vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
					polyDataOut->DeepCopy(cutter->GetOutput());


					// Lets get the cutter points
					vtkSmartPointer<vtkPoints> cutter_points =
						vtkSmartPointer<vtkPoints>::New();
					cutter_points = polyDataOut->GetPoints();


					vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
					scaled_cutter_points = ScaleCircumference(cutter_points, SCALING_FACTOR, curr_p);

					//Update the cutter points
					polyDataOut->SetPoints(scaled_cutter_points);

					//Push the contour to skeleton Data structure
					cutter_points = polyDataOut->GetPoints();
					//concatenate _contourelem and its no. to the jointname
					std::stringstream jname;
					jname << current_br->childrenjts.at(index)->name << "_contourelem" << contour_count;
					Context::instance().display().AddVtkPointSet(polyDataOut, jname.str(), false, DISPLAY_MODE::GEBOD);
					Context::instance().display().SetObjectColor(jname.str(), 1.0, 0.5, 0, 0.8);
					Context::instance().display().SetObjectWidth(jname.str(), 3);
					PushGeneratedJointContours(cutter_points, current_br, index, jname.str(), uval);
					ContourActors.push_back(jname.str());
					jname.str(std::string());
					jname.clear();


					memcpy(old_p, curr_p, 3 * sizeof(double));
				}
			}
			else
			{
				pWarning() << "Actor " << BodyRegion << "not present";
			}
		}
	}


	void ContourGen::CreateContoursListForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		double Normal[3];
		double Proximal[3], Distal[3];
		Coord proximal, distal, normal;
		int number_of_contours = DEFAULT_CONTOUR_COUNT;

		pDebug() << current_br->name << " " << current_br->cplandmarks.size();
		if (current_br->childrenjts.size() == 0)
			pDebug() << "==================== Its the leaf bodyregion =======================";


		if (current_br->cplandmarks.size() > 0)
		{
			//Contours for linear BR
			if (current_br->cplandmarks.size() == 2 &&
				!current_br->cplandmarks.at(0).empty() &&
				!current_br->cplandmarks.at(1).empty())
			{
				if (m_meta.hasLandmark(current_br->cplandmarks.at(0)))
//					distal = m_meta.landmark(current_br->cplandmarks.at(0)).position(m_fem);
					distal = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(0));
				else
					std::cout << std::endl << "landmark not found !!!!!";

				if (m_meta.hasLandmark(current_br->cplandmarks.at(1)))
//					proximal = m_meta.landmark(current_br->cplandmarks.at(1)).position(m_fem);
					proximal = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(1));
				else
					std::cout << std::endl << "landmark not found !!!!!";

				normal = proximal - distal;
				double distance = normal.norm();

				Normal[0] = normal(0);
				Normal[1] = normal(1);
				Normal[2] = normal(2);

				Proximal[0] = proximal(0);
				Proximal[1] = proximal(1);
				Proximal[2] = proximal(2);

				Distal[0] = distal(0);
				Distal[1] = distal(1);
				Distal[2] = distal(2);


				//Get the Model Actor
				std::string BodyRegion;
				BodyRegion = current_br->skingenericmetadata;
				pDebug() << BodyRegion;
				if (current_br->name == "Left_thigh" || current_br->name == "Right_thigh")
					number_of_contours = THIGH_CONTOUR_COUNT;
				else if (current_br->name == "Right_leg" || current_br->name == "Left_leg")
					number_of_contours = CALF_CONTOUR_COUNT;
				else if (current_br->name == "Right_foot" || current_br->name == "Left_foot")
					number_of_contours = FOOT_CONTOUR_COUNT;
				else
					number_of_contours = DEFAULT_CONTOUR_COUNT;

				pDebug() << current_br->name << "name cont:" << number_of_contours;

				if (!BodyRegion.empty())
				{

                    boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();

					auto it = actors->find(BodyRegion);
					if (it != actors->end())
					{


						vtkUnstructuredGrid *inputPolyData = vtkUnstructuredGrid::SafeDownCast(it->second->GetOriginalDataSet());

						double range[2];
						range[0] = -distance / 2;
						range[1] = distance / 2;

						double Location[3];
						double translate[3];
						vtkSmartPointer<vtkPolyData> polydataCopy = vtkSmartPointer<vtkPolyData>::New();

						for (int contour_no = 0; contour_no <= number_of_contours; contour_no++)
						{

							//Linear interpolation of contour location between proximal and distal landmark
							Location[0] = Proximal[0] * (static_cast<double> (contour_no) / static_cast<double> (number_of_contours)) +
								Distal[0] * (static_cast<double>(number_of_contours - contour_no) / static_cast<double> (number_of_contours));
							Location[1] = Proximal[1] * (static_cast<double>(contour_no) / static_cast<double>(number_of_contours)) +
								Distal[1] * (static_cast<double>(number_of_contours - contour_no) / static_cast<double>(number_of_contours));
							Location[2] = Proximal[2] * (static_cast<double>(contour_no) / static_cast<double> (number_of_contours)) +
								Distal[2] * (static_cast<double> (number_of_contours - contour_no) / static_cast<double> (number_of_contours));


							if ((current_br->name == "Right_hand" || current_br->name == "Left_hand") && contour_no > SKIP_CONTOUR) //Skip intersection, just copy contour
							{
								for (int i = 0; i < 7; i++)
								{
									//Cut location
									translate[0] = 0.6*(Proximal[0] - Distal[0]) + i*((Proximal[0] - Distal[0]) / 10);
									translate[1] = 0.6*(Proximal[1] - Distal[1]) + i*((Proximal[1] - Distal[1]) / 10);
									translate[2] = 0.6*(Proximal[2] - Distal[2]) + i*((Proximal[2] - Distal[2]) / 10);

									contour_no = contour_no + i;
									ExtendContour(polydataCopy, contour_no, translate, current_br, 3.0);
								}

							}


							if (contour_no == number_of_contours)
							{

								if (current_br->name == "Right_foot" || current_br->name == "Left_foot")
								{
									for (int i = 0; i < 1; i++)
									{
										//Cut location
										translate[0] = 1.2*(Proximal[0] - Distal[0]) + i*((Proximal[0] - Distal[0]) / 10);
										translate[1] = 1.2*(Proximal[1] - Distal[1]) + i*((Proximal[1] - Distal[1]) / 10);
										translate[2] = 1.2*(Proximal[2] - Distal[2]) + i*((Proximal[2] - Distal[2]) / 10);

										contour_no = contour_no + i;
										ExtendContour(polydataCopy, contour_no, translate, current_br);
									}

								}

							}
							
							if (current_br->name == "thorax"){
								pDebug() << "i of cont for thorax" << number_of_contours;
							}


							vtkSmartPointer<vtkPolyData> cutteroutput = CreateContourForBr(inputPolyData, Location, Normal, 1, range);




							// Lets get the cutter points
							vtkSmartPointer<vtkPoints> cutter_points =
								vtkSmartPointer<vtkPoints>::New();
							cutter_points = cutteroutput->GetPoints();


							double scaling_factor = 1.05;
							double center[3];
							center[0] = cutteroutput->GetCenter()[0];
							center[1] = cutteroutput->GetCenter()[1];
							center[2] = cutteroutput->GetCenter()[2];
							vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
							scaled_cutter_points = ScaleCircumference(cutter_points, scaling_factor, center);

							//Update the cutter points
							cutteroutput->SetPoints(scaled_cutter_points);

							//Take the copy of contour, which needs to be extended
							// It first contour of a body region as of now
							if (contour_no == 0)
								polydataCopy->DeepCopy(cutteroutput);

							//concatenate _circumelem to the bodyregion
							std::stringstream name;
							name << current_br->name << "_contourelem" << contour_no;


							//Push the contour to skeleton Data structure
							cutter_points = cutteroutput->GetPoints();



							Context::instance().display().AddVtkPointSet(cutteroutput, name.str(), false, DISPLAY_MODE::GEBOD);
							Context::instance().display().SetObjectColor(name.str(), 1.0, 0.2, 0, 0.8);
							Context::instance().display().SetObjectWidth(name.str(), 3);
							PushGeneratedContours(cutter_points, current_br, contour_no, name.str());
							ContourActors.push_back(name.str());
							name.str(std::string());
							name.clear();
						}
					}
					else
					{
						pWarning() << "Actor " << BodyRegion << "not present";
					}
				}
			}
		} // Contours for spline type BR
		if (current_br->cplandmarks.size() >= 4)
		{
			CreateContourForSplineBR(m_fem, m_meta, current_br);

		}

	}



	//Gets the contour for a particular br name provided
	void ContourGen::GetContourforBR(std::string br_name)
	{
		//Init hierarchy
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();

		std::queue<ContourCLbr*> local;
		local.push(m_meta.ctrl_line.root);

		while (local.size() != 0)
		{
			ContourCLbr* current_br = local.front();
			local.pop();

			//Create contour only for the provided br_name
			if (br_name == current_br->name)
				CreateContoursListForBR(m_fem, m_meta, current_br);

			//Push Next skeleton element
			for (int i = 0; i < current_br->childrenjts.size(); i++)
			{
				local.push(current_br->childrenjts.at(i)->childbodyregion);
			}

		}

	}



	//Gets the contour for a particular joint name provided
	void ContourGen::GetContourforJoint(std::string j_name)
	{
		//Init hierarchy
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();

		std::queue<ContourCLbr*> local;
		local.push(m_meta.ctrl_line.root);

		while (local.size() != 0)
		{
			ContourCLbr* current_br = local.front();
			local.pop();

			for (int i = 0; i < current_br->childrenjts.size(); i++)
			{
				if (j_name == current_br->childrenjts.at(i)->name)
				{
					MatrixXd spline_matrix = GetHermiteSpline(m_fem, m_meta, current_br, i);
					//Init the Bezier Curve
					ComputeBezierCurve(spline_matrix);
					ComputeContourForJoint(current_br, i);
				}

				local.push(current_br->childrenjts.at(i)->childbodyregion);
			}

		}

	}



	void ContourGen::CreateContourForChildElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		std::queue<ContourCLbr*> contour_iter;
		contour_iter.push(current_br);

		pDebug() << "current_br = " << current_br->name;
		pDebug() << "contour_iter.size() = " << contour_iter.size();

		while (contour_iter.size() != 0)
		{
			ContourCLbr* br = contour_iter.front();
			contour_iter.pop();

			CreateContoursListForBR(m_fem, m_meta, br);
			for (int j = 0; j < br->childrenjts.size(); j++)
			{
				pDebug() << "current_br->childrenjts.at(j)->name = " << current_br->childrenjts.at(j)->name;
				MatrixXd spline_matrix = GetHermiteSpline(m_fem, m_meta, br, j);
				//Init the Bezier Curve
				ComputeBezierCurve(spline_matrix);
				ComputeContourForJoint(br, j);

				contour_iter.push(br->childrenjts.at(j)->childbodyregion);
			}
		}
	}



	//Gets all the contours parent and childrn for a given joint
	void ContourGen::GetContoursforJoint(std::string j_name)
	{
		//Init hierarchy
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();

		std::queue<ContourCLbr*> local;
		local.push(m_meta.ctrl_line.root);

		while (local.size() != 0)
		{
			ContourCLbr* current_br = local.front();
			local.pop();

			for (int i = 0; i < current_br->childrenjts.size(); i++)
			{
				if (j_name == current_br->childrenjts.at(i)->name)
				{
					CreateContourForChildElement(m_fem, m_meta, current_br);

				}

				local.push(current_br->childrenjts.at(i)->childbodyregion);
			}

		}

	}


	/// Push the points got from thr VTK cutter to the contour data structures.
	void ContourGen::PushGeneratedContours(vtkSmartPointer<vtkPoints> cutter_points, ContourCLbr* current_br, double contourcnt, std::string name)
	{

		double p[3];
		Contour* contour = new Contour();

		int NumPts = cutter_points->GetNumberOfPoints();

		//		current_br->brcont.push_back(contour);

		for (int i = 0; i < NumPts; i++)
		{
			NodePtr node(new Node);
			cutter_points->GetPoint(i, p);
			node->setCoord(p[0], p[1], p[2]);
			contour->mcontour[name].push_back(node);
			contour->contour_points.push_back(node);
		}



		current_br->brcont.push_back(contour);

#ifdef DEBUGGING
		if (current_br->name == "Left_foot"){
			std::ofstream finally("finally.txt");
			for (int k = 0; k < current_br->brcont.size(); k++){
				for (int j = 0; j < current_br->brcont.at(k)->contour_points.size(); j++){
					finally << current_br->brcont.at(k)->contour_points.at(j)->getCoordX() << "  " << current_br->brcont.at(k)->contour_points.at(j)->getCoordY() << "  " << current_br->brcont.at(k)->contour_points.at(j)->getCoordZ() << std::endl;
				}
			}
		}
#endif

		}
	/// Push the points got from thr VTK cutter to the joint contour data structures.
	void ContourGen::PushGeneratedJointContours(vtkSmartPointer<vtkPoints> cutter_points, ContourCLbr* current_br, int index, std::string name, double uval)
	{

		double p[3];
		Contour* contour = new Contour();
		int NumPts = cutter_points->GetNumberOfPoints();
		for (int i = 0; i < NumPts; i++)
		{
			NodePtr node(new Node);
			cutter_points->GetPoint(i, p);
			node->setCoord(p[0], p[1], p[2]);
			contour->mcontour[name].push_back(node);
			contour->contour_points.push_back(node);
			contour->uvalue = uval;
		}
		current_br->childrenjts.at(index)->jointcont.push_back(contour);

#ifdef DEBUGGING
		if (current_br->childrenjts.at(index)->name == "kneel"){
			std::ofstream finallyj("finallyj.k");
			std::ofstream f2("f2.txt");
			f2 << "tot size: " << current_br->childrenjts.at(index)->jointcont.size() << std::endl;
			finallyj << "*NODE";
			int knum = 0;
			for (int k = 0; k < 1; k++){
				for (int j = 0; j < current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.size(); j++){
					//					finallyj << std::endl << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordX() << "  " << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordY() << "  " << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordZ() << std::endl;
					finallyj << std::endl << setw(8) << knum++ << setw(16) << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordX() << setw(16) << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordY() << setw(16) << current_br->childrenjts.at(index)->jointcont.at(k)->contour_points.at(j)->getCoordZ();
				}
			}
			finallyj << std::endl << "*END";
		}
#endif

		}



	void ContourGen::GebodCircumElements(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br, int curr_el)
	{
		double Origin[3], Normal[3];
		int number_of_contours = 1;



		hbm::SId gnode = m_conthbm.getLandmarkNodeids(m_fem, m_meta, current_br->circumf);
		std::set<Id>::iterator it;
		std::cout << std::endl << "size " << gnode.size() << std::endl;
		if (gnode.size() > 0)
		{
			for (it = gnode.begin(); it != gnode.end(); ++it)
			{
				NodePtr iter = m_fem.get<Node>(*it);
				Origin[0] = iter->getCoordX();
				Origin[1] = iter->getCoordY();
				Origin[2] = iter->getCoordZ();
			}


			if (!current_br->cplandmarks.at(0).empty() &&
				!current_br->cplandmarks.at(1).empty())
			{
				//Get Proximal Landmark
				Coord prox;
				if (m_meta.hasLandmark(current_br->cplandmarks.at(0)))
//					prox = m_meta.landmark(current_br->cplandmarks.at(0)).position(m_fem);
					prox = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(0));
				else
					std::cout << std::endl << "landmark not found !!!!!";


				//Get Distal Landmark
				Coord dist;
				if (m_meta.hasLandmark(current_br->cplandmarks.at(1)))
//					dist = m_meta.landmark(current_br->cplandmarks.at(1)).position(m_fem);
					dist = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(1));
				else
					std::cout << std::endl << "landmark not found !!!!!";


				//get normal
				Coord norm = prox - dist;

				Normal[0] = norm(0);
				Normal[1] = norm(1);
				Normal[2] = norm(2);
			}



			//Get the Model Actor
			std::string BodyRegion;
			BodyRegion = current_br->skingenericmetadata;
			pDebug() << BodyRegion;

			if (!BodyRegion.empty())
			{
                boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();

				auto it = actors->find(BodyRegion);
				if (it != actors->end())
				{
					vtkUnstructuredGrid *inputPolyData = vtkUnstructuredGrid::SafeDownCast(it->second->GetOriginalDataSet());

					//Get Data Bounds Here
					double minBound[3];
					minBound[0] = inputPolyData->GetBounds()[0];
					minBound[1] = inputPolyData->GetBounds()[2];
					minBound[2] = inputPolyData->GetBounds()[4];

					double maxBound[3];
					maxBound[0] = inputPolyData->GetBounds()[1];
					maxBound[1] = inputPolyData->GetBounds()[3];
					maxBound[2] = inputPolyData->GetBounds()[5];

					double center[3];
					center[0] = inputPolyData->GetCenter()[0];
					center[1] = inputPolyData->GetCenter()[1];
					center[2] = inputPolyData->GetCenter()[2];

					double distanceMin = sqrt(vtkMath::Distance2BetweenPoints(minBound, center));
					double distanceMax = sqrt(vtkMath::Distance2BetweenPoints(maxBound, center));

					double range[2];
					range[0] = -distanceMin;
					range[1] = distanceMax;

					vtkSmartPointer<vtkPolyData> cutteroutput = CreateContourForBr(inputPolyData, Origin, Normal, number_of_contours, range);


					// Lets get the cutter points
					vtkSmartPointer<vtkPoints> cutter_points =
						vtkSmartPointer<vtkPoints>::New();
					cutter_points = cutteroutput->GetPoints();


					//Scale circumference elements
					double scaling_factor = ScalingValue;
					vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
					scaled_cutter_points = ScaleCircumference(cutter_points, scaling_factor, Origin);

					//Update the cutter points
					cutteroutput->SetPoints(scaled_cutter_points);



					//concatenate _circumelem to the bodyregion
					std::stringstream name;
					name << current_br->name << "_circumelem";
					Context::instance().display().AddVtkPointSet(cutteroutput, name.str(), false, DISPLAY_MODE::GEBOD);
					Context::instance().display().SetObjectColor(name.str(), 1.0, 1.0, 0, 0.8);
					Context::instance().display().SetObjectWidth(name.str(), 3);
					CircumActors.push_back(name.str());
					name.str(std::string());
					name.clear();

				}
				else
				{
					pWarning() << "Actor " << BodyRegion << "not present";
				}
			}

		}
	}




	void ContourGen::CreateContourForSplineBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		// Create a vtkPoints object and store the points in it
		pDebug() << current_br->name;
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();


		for (int i = 0; i < current_br->cplandmarks.size(); i++)
		{
			if (!current_br->cplandmarks.at(i).empty())
			{
				Coord point;
				if (m_meta.hasLandmark(current_br->cplandmarks.at(i)))
//					point = m_meta.landmark(current_br->cplandmarks.at(i)).position(m_fem);
					point = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(i));
				else
					std::cout << std::endl << "landmark not found !!!!!";

				double p[3];
				p[0] = point(0);
				p[1] = point(1);
				p[2] = point(2);
				points->InsertNextPoint(p);
			}

		}

		vtkSmartPointer<vtkParametricSpline> spline =
			vtkSmartPointer<vtkParametricSpline>::New();
		spline->SetPoints(points);



		//TODO: Find necessary contours required for a joint
		double no_of_contours = JOINT_CONTOUR_COUNT;
		if (current_br->name == "Head")
			no_of_contours = 16;
		else if (current_br->name == "lumbar")
			no_of_contours = THORAX_CONTOUR_COUNT;
		else if (current_br->name == "Neck")
			no_of_contours = NECK_CONTOUR_COUNT;

		pDebug() << current_br->name;

		//Get the Model Actor
		std::string BodyRegion;
		BodyRegion = current_br->skingenericmetadata;

		if (!BodyRegion.empty())
		{
            boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
			auto it = actors->find(BodyRegion);
			if (it != actors->end())
			{
				vtkUnstructuredGrid *inputPolyData = vtkUnstructuredGrid::SafeDownCast(it->second->GetOriginalDataSet());

				double range[2], Normal[3];

				range[0] = -1.0;
				range[1] = 1.0;

				double u[3];
				u[0] = 0.0;
				double old_p[3];
				double Du[9];
				double curr_p[3], curr_p1[3], curr_p2[3];
				double translate[3];


				spline->Evaluate(u, old_p, Du);
				vtkSmartPointer<vtkPolyData> polydataCopy = vtkSmartPointer<vtkPolyData>::New();


				if (current_br->name == "thorax"){
					for (double contour_count = 0; contour_count <= no_of_contours; contour_count++)
					{
						u[0] = contour_count / no_of_contours;

						spline->Evaluate(u, curr_p, Du);

						//Cut normal
						u[0] = u[0] + 0.01;
						spline->Evaluate(u, curr_p1, Du);
						u[0] = u[0] - 0.01;
						spline->Evaluate(u, curr_p2, Du);

						Normal[0] = curr_p1[0] - curr_p2[0];
						Normal[1] = curr_p1[1] - curr_p2[1];
						Normal[2] = curr_p1[2] - curr_p2[2];

						//for Head only
						if (contour_count == no_of_contours && current_br->name == "Head")
						{
							//Cut location
							translate[0] = 10.0*(curr_p[0] - old_p[0]);
							translate[1] = 10.0*(curr_p[1] - old_p[1]);
							translate[2] = 10.0*(curr_p[2] - old_p[2]);


							ExtendContour(polydataCopy, contour_count, translate, current_br);
							break;
						}


						// Create a plane to cut
						vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
						plane->SetOrigin(curr_p);
						plane->SetNormal(Normal);


						// Create cutter
						vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();

						//Set cutting function- Here a plane
						cutter->SetCutFunction(plane);

						//Set Cutter Input data
						cutter->SetInputData(inputPolyData);

						//Set Cutter bounds
						cutter->GenerateValues(1, range);
						cutter->Update();

						vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
						polyDataOut->DeepCopy(cutter->GetOutput());


						// Lets get the cutter points
						vtkSmartPointer<vtkPoints> cutter_points =
							vtkSmartPointer<vtkPoints>::New();
						cutter_points = polyDataOut->GetPoints();


						vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
						scaled_cutter_points = ScaleCircumference(cutter_points, SCALING_FACTOR, curr_p);

						//Update the cutter points
						polyDataOut->SetPoints(scaled_cutter_points);

						//Take the copy of data
						polydataCopy->DeepCopy(polyDataOut);

						//Push the contour to skeleton Data structure
						cutter_points = polyDataOut->GetPoints();


						std::stringstream name;
						name << current_br->name << "_contourelem" << contour_count;


						Context::instance().display().AddVtkPointSet(polyDataOut, name.str(), false, DISPLAY_MODE::GEBOD);
						Context::instance().display().SetObjectColor(name.str(), 1.0, 0.5, 0, 0.8);
						Context::instance().display().SetObjectWidth(name.str(), 3);

						PushGeneratedContours(cutter_points, current_br, contour_count, name.str());

						ContourActors.push_back(name.str());
						name.str(std::string());
						name.clear();


						memcpy(old_p, curr_p, 3 * sizeof(double));
					}
				}
				else{

					for (double contour_count = 1; contour_count <= no_of_contours; contour_count++)
					{
						u[0] = contour_count / no_of_contours;

						spline->Evaluate(u, curr_p, Du);

						//Cut normal
						u[0] = u[0] + 0.01;
						spline->Evaluate(u, curr_p1, Du);
						u[0] = u[0] - 0.01;
						spline->Evaluate(u, curr_p2, Du);

						Normal[0] = curr_p1[0] - curr_p2[0];
						Normal[1] = curr_p1[1] - curr_p2[1];
						Normal[2] = curr_p1[2] - curr_p2[2];

						//for Head only
						if (contour_count == no_of_contours && current_br->name == "Head")
						{
							//Cut location
							translate[0] = 10.0*(curr_p[0] - old_p[0]);
							translate[1] = 10.0*(curr_p[1] - old_p[1]);
							translate[2] = 10.0*(curr_p[2] - old_p[2]);

							ExtendContour(polydataCopy, contour_count, translate, current_br);
							break;
						}


						// Create a plane to cut
						vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
						plane->SetOrigin(curr_p);
						plane->SetNormal(Normal);


						// Create cutter
						vtkSmartPointer<vtkCutter> cutter = vtkSmartPointer<vtkCutter>::New();

						//Set cutting function- Here a plane
						cutter->SetCutFunction(plane);

						//Set Cutter Input data
						cutter->SetInputData(inputPolyData);

						//Set Cutter bounds
						cutter->GenerateValues(1, range);
						cutter->Update();

						vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
						polyDataOut->DeepCopy(cutter->GetOutput());


						// Lets get the cutter points
						vtkSmartPointer<vtkPoints> cutter_points =
							vtkSmartPointer<vtkPoints>::New();
						cutter_points = polyDataOut->GetPoints();


						vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
						scaled_cutter_points = ScaleCircumference(cutter_points, SCALING_FACTOR, curr_p);

						//Update the cutter points
						polyDataOut->SetPoints(scaled_cutter_points);

						//Take the copy of data
						polydataCopy->DeepCopy(polyDataOut);

						//Push the contour to skeleton Data structure
						cutter_points = polyDataOut->GetPoints();

						std::stringstream name;
						name << current_br->name << "_contourelem" << contour_count;


						Context::instance().display().AddVtkPointSet(polyDataOut, name.str(), false, DISPLAY_MODE::GEBOD);
						Context::instance().display().SetObjectColor(name.str(), 1.0, 0.5, 0, 0.8);
						Context::instance().display().SetObjectWidth(name.str(), 3);

						PushGeneratedContours(cutter_points, current_br, contour_count, name.str());

						ContourActors.push_back(name.str());
						name.str(std::string());
						name.clear();

						memcpy(old_p, curr_p, 3 * sizeof(double));
					}
				}

			}
			else
			{
				pWarning() << "Actor " << BodyRegion << "not present";
			}
		}
	}

	void ContourGen::ExtendContour(vtkSmartPointer<vtkPolyData> polyDataOut, int contour_count, double* curr_p,
		ContourCLbr* current_br, double scaling_factor)
	{


		//Push the contour to skeleton Data structure
		vtkSmartPointer<vtkPoints> cutter_points =
			vtkSmartPointer<vtkPoints>::New();

		vtkSmartPointer<vtkPolyData> polyDataOutScaled =
			vtkSmartPointer<vtkPolyData>::New();

		polyDataOutScaled->DeepCopy(polyDataOut);

		cutter_points = polyDataOutScaled->GetPoints();
		double center[3];
		center[0] = polyDataOutScaled->GetCenter()[0];
		center[1] = polyDataOutScaled->GetCenter()[1];
		center[2] = polyDataOutScaled->GetCenter()[2];
		vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
		scaled_cutter_points = ScaleCircumference(cutter_points, scaling_factor, center);

		//Update the cutter points
		polyDataOutScaled->SetPoints(scaled_cutter_points);


		pDebug() << polyDataOutScaled->GetNumberOfPoints();


		vtkSmartPointer<vtkTransform> translation =
			vtkSmartPointer<vtkTransform>::New();

		translation->Translate(curr_p[0], curr_p[1], curr_p[2]);

		vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter =
			vtkSmartPointer<vtkTransformPolyDataFilter>::New();
		transformFilter->SetInputData(polyDataOutScaled);
		transformFilter->SetTransform(translation);
		transformFilter->Update();

		cutter_points = transformFilter->GetOutput()->GetPoints();


		std::stringstream name;
		name << current_br->name << "_contourelem" << contour_count;
		Context::instance().display().AddVtkPointSet(transformFilter->GetOutput(), name.str(), false, DISPLAY_MODE::GEBOD);
		Context::instance().display().SetObjectColor(name.str(), 1.0, 0.5, 0, 0.8);
		Context::instance().display().SetObjectWidth(name.str(), 3);
		PushGeneratedContours(cutter_points, current_br, contour_count, name.str());
		ContourActors.push_back(name.str());
		name.str(std::string());
		name.clear();

	}


	void ContourGen::CreateSplineForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		pDebug() << current_br->name;
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		Coord point;

		for (int i = 0; i < current_br->cplandmarks.size(); i++)
		{
			if (!current_br->cplandmarks.at(i).empty())
			{
				if (m_meta.hasLandmark(current_br->cplandmarks.at(i)))
//					point = m_meta.landmark(current_br->cplandmarks.at(i)).position(m_fem);
					point = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(i));
				else
					std::cout << std::endl << current_br->cplandmarks.at(i) << " landmark not found !!!";
				double p[3];
				p[0] = point(0);
				p[1] = point(1);
				p[2] = point(2);
				points->InsertNextPoint(p);
			}

		}


		vtkSmartPointer<vtkParametricSpline> spline =
			vtkSmartPointer<vtkParametricSpline>::New();
		spline->SetPoints(points);

		vtkSmartPointer<vtkParametricFunctionSource> functionSource =
			vtkSmartPointer<vtkParametricFunctionSource>::New();
		functionSource->SetParametricFunction(spline);
		functionSource->Update();

		//concatenate _splineelem to the bodyregion
		std::stringstream brname;
		brname << current_br->name << "_splineelem";
		pDebug() << "Spline Names " << brname.str();
		Context::instance().display().AddVtkPointSet(functionSource->GetOutput(), brname.str(), false, DISPLAY_MODE::GEBOD);
		Context::instance().display().SetObjectColor(brname.str(), 1.0, 1.0, 0, 0.8);
		Context::instance().display().SetObjectWidth(brname.str(), 3);
		CLActors.push_back(brname.str());
		brname.str(std::string());
		brname.clear();

	}

	void ContourGen::CreateSphereForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		double landmark[3];
		Coord prox;
        //Get Proximal Landmark
		//std::cout << std::endl << "{{{{{{{{{{{1";
        if ( !current_br->cplandmarks.at(0).empty())
        {
			if (m_meta.hasLandmark(current_br->cplandmarks.at(0)))
//				prox = m_meta.landmark(current_br->cplandmarks.at(0)).position(m_fem);
				prox = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(0));
			else
				std::cout << std::endl << current_br->cplandmarks.at(0) << " landmark not present!\n";
			//std::cout << std::endl << "{{{{{{{{{{{2";
            landmark[0] = prox(0);
            landmark[1] = prox(1);
            landmark[2] = prox(2);

            std::stringstream endname;
            endname << current_br->name;
			//std::cout << std::endl << "{{{{{{{{{{{3";
            AddEndPointSphere(landmark,endname.str());
			//std::cout << std::endl << "{{{{{{{{{{{4";
			CLActors.push_back(endname.str());
			//std::cout << std::endl << "{{{{{{{{{{{5";
            endname.str( std::string() );
            endname.clear();
        }
		std::stringstream endname;
		endname << current_br->name;
		//AddEndPointSphere(landmark,endname.str());
		CLActors.push_back(endname.str());

	}

	void ContourGen::ComputeLinearElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		double proximal[3], distal[3];
		Coord prox;
		Coord dist;
		if (current_br->cplandmarks.size() > 0){
			//Contours for linear BR
			if (!current_br->cplandmarks.at(0).empty() && !current_br->cplandmarks.at(1).empty())
			{
				if (m_meta.hasLandmark(current_br->cplandmarks.at(0)))
//					prox = m_meta.landmark(current_br->cplandmarks.at(0)).position(m_fem);
					prox = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(0));
				else
					std::cout << std::endl << "landmark not found !!!";
				if (m_meta.hasLandmark(current_br->cplandmarks.at(1)))
//					dist = m_meta.landmark(current_br->cplandmarks.at(1)).position(m_fem);
					dist = process_func.getKeyPointCoord(m_fem, m_meta, current_br->cplandmarks.at(1));
				else
					std::cout << std::endl << "landmark not found !!!";
				proximal[0] = prox(0);
				proximal[1] = prox(1);
				proximal[2] = prox(2);
				distal[0] = dist(0);
				distal[1] = dist(1);
				distal[2] = dist(2);

				std::stringstream endname;
				endname << current_br->name << "_proximal";
				AddEndPointSphere(proximal, endname.str());
				CLActors.push_back(endname.str());
				endname.str(std::string());
				endname.clear();
				endname << current_br->name << "_distal";
				AddEndPointSphere(distal, endname.str());
				CLActors.push_back(endname.str());
				AddLinearSkeletonElement(proximal, distal, current_br->name);

				//Get Proximal Landmark
				SId newnode = m_conthbm.getLandmarkNodeids(m_fem, m_meta, current_br->cplandmarks.at(0));
				std::set<Id>::iterator it;
				for (it = newnode.begin(); it != newnode.end(); ++it)
				{
					NodePtr iter = m_fem.get<Node>(*it);
					proximal[0] = iter->getCoordX();
					proximal[1] = iter->getCoordY();
					proximal[2] = iter->getCoordZ();
				}
				newnode = m_conthbm.getLandmarkNodeids(m_fem, m_meta, current_br->cplandmarks.at(1));
				for (it = newnode.begin(); it != newnode.end(); ++it)
				{
					NodePtr iter = m_fem.get<Node>(*it);
					distal[0] = iter->getCoordX();
					distal[1] = iter->getCoordY();
					distal[2] = iter->getCoordZ();
				}

				endname << current_br->name << "_proximal";
				//AddEndPointSphere(proximal,endname.str());
				CLActors.push_back(endname.str());
				endname.str(std::string());
				endname.clear();
				endname << current_br->name << "_distal";
				//AddEndPointSphere(distal,endname.str());
				CLActors.push_back(endname.str());
				AddLinearSkeletonElement(proximal, distal, current_br->name);
			}
		}
	}



	void ContourGen::ComputeBodyRegionElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, ContourCLbr* current_br)
	{
		std::vector<NodePtr> lctrlpts;

		for (auto it = current_br->cplandmarks.begin(); it != current_br->cplandmarks.end(); ++it){
			SId nodeset = m_conthbm.getLandmarkNodeids(m_fem, m_meta, *it);
			std::stringstream endname;
			endname << *it << "_pt";
			AddLctrlptSphere(m_fem.get<Node>(*nodeset.begin()), endname.str());
			endname.str(std::string());
			endname.clear();
			lctrlpts.push_back(m_fem.get<Node>(*nodeset.begin()));
		}
		for (auto it = lctrlpts.begin(); it != lctrlpts.end(); ++it){
			NodePtr newnode;
			newnode = *it;
		}
		AddCLbrElement(lctrlpts, current_br->name);

	}



    void ContourGen::PopulateVTKConstructs(hbm::FEModel& m_fem, hbm::Metadata& m_meta)
    {
		   std::cout << "\n-------------PopulateVTKConstructs  called !!";
			std::queue<ContourCLbr*> local;
            local.push(m_meta.ctrl_line.root);
			while (local.size() != 0){
                ContourCLbr* current_br = local.front();
                local.pop();
                std::cout <<std::endl << "\n---------Body Region: " << current_br->name ;
				//std::cout <<std::endl << "--------------1";
                if(current_br->cplandmarks.size() == 1)
                    CreateSphereForBR(m_fem, m_meta, current_br);
				//std::cout << std::endl << "<<<<<<<<1.1";
				if(current_br->cplandmarks.size() == 2)
                    ComputeLinearElement(m_fem, m_meta, current_br);
				//std::cout << std::endl << "<<<<<<<<1.2";
				if(current_br->cplandmarks.size() == 3)
					cout <<std::endl << "Unsupported Landmark size ! ";
                if(current_br->cplandmarks.size() >= 4)
                    CreateSplineForBR(m_fem, m_meta, current_br);
				//std::cout << std::endl << "<<<<<<<<1.3";
				//std::cout << std::endl << "--------------2";
				for (int i = 0; i < current_br->childrenjts.size(); i++){
                    
					MatrixXd spline_matrix = GetHermiteSpline(m_fem, m_meta, current_br, i);
					//std::cout << std::endl << "--------------3";
                    //Init the Bezier Curve
                    ComputeBezierCurve(spline_matrix);
					//std::cout << std::endl << "--------------4";
                    //Get the spline type joint skeleton
                    std::string name(current_br->childrenjts.at(i)->name);
					if (!current_br->childrenjts.at(i)->joint_splinep0.empty())
						ComputeJointSkeleton(name);
					//std::cout << std::endl << "--------------5";
//                    DisplayAxes(m_fem, m_meta, current_br->childrenjts.at(i));


  //                  GebodFrames(m_fem, m_meta, current_br, i);

                    //Get and Display the Gebod Circumelements
//                    GebodCircumElements(m_fem, m_meta, current_br,i);

                    local.push(current_br->childrenjts.at(i)->childbodyregion);
                }
            }

    }

    void ContourGen::DisplayAxes(FEModel& m_fem, Metadata& m_meta, ContourCLj* p_joint)
    {
        double fL1[3],fL2[3];
        double tL1[3],tL2[3];

         if(!(p_joint->flexionaxeslandmarks.empty()))
         {
             //Two landmarks define a flexion axes
             if ( !p_joint->flexionaxeslandmarks.at(0).empty() &&
                 !p_joint->flexionaxeslandmarks.at(1).empty())
             {
				 Coord l1, l2;
				 if (m_meta.hasLandmark(p_joint->flexionaxeslandmarks.at(0)))
//					 l1 = m_meta.landmark(p_joint->flexionaxeslandmarks.at(0)).position(m_fem);
					 l1 = process_func.getKeyPointCoord(m_fem, m_meta, p_joint->flexionaxeslandmarks.at(0));
				 else
					 std::cout << std::endl << "landmark not found !!!!!";
				 

				 if (m_meta.hasLandmark(p_joint->flexionaxeslandmarks.at(1)))
//					 l2 = m_meta.landmark(p_joint->flexionaxeslandmarks.at(1)).position(m_fem);
					 l2 = process_func.getKeyPointCoord(m_fem, m_meta, p_joint->flexionaxeslandmarks.at(1));
				 else
					 std::cout << std::endl << "landmark not found !!!!!";
                 

                 fL1[0] = l1(0);
                 fL1[1] = l1(1);
                 fL1[2] = l1(2);

                 fL2[0] = l2(0);
                 fL2[1] = l2(1);
                 fL2[2] = l2(2);

                 std::stringstream endname;
                 endname << p_joint->name << "_fl1";
                 AddEndPointSphere(fL1,endname.str());
                 CLActors.push_back(endname.str());
                 endname.str( std::string() );
                 endname.clear();
                 endname << p_joint->name << "_fl2";
                 AddEndPointSphere(fL2,endname.str());
                 CLActors.push_back(endname.str());
                 AddLinearSkeletonElement(fL1,fL2,p_joint->name);
             }

         }

         if(!(p_joint->twistaxeslandmarks.empty()))
         {
			 Coord l1, l2;
			 if (m_meta.hasLandmark(p_joint->twistaxeslandmarks.at(0)))
//				 l1 = m_meta.landmark(p_joint->twistaxeslandmarks.at(0)).position(m_fem);
				 l1 = process_func.getKeyPointCoord(m_fem, m_meta, p_joint->twistaxeslandmarks.at(0));
			 else
				 std::cout << std::endl << "landmark not found !!!!!";
             
			 if (m_meta.hasLandmark(p_joint->twistaxeslandmarks.at(1)))
//				 l2 = m_meta.landmark(p_joint->twistaxeslandmarks.at(1)).position(m_fem);
				 l2 = process_func.getKeyPointCoord(m_fem, m_meta, p_joint->twistaxeslandmarks.at(1));
			 else
				 std::cout << std::endl << "landmark not found !!!!!";

             

             tL1[0] = l1(0);
             tL1[1] = l1(1);
             tL1[2] = l1(2);

             tL2[0] = l2(0);
             tL2[1] = l2(1);
             tL2[2] = l2(2);

             std::stringstream endname;
             endname << p_joint->name << "_tl1";
             AddEndPointSphere(tL1,endname.str());
             CLActors.push_back(endname.str());
             endname.str( std::string() );
             endname.clear();
             endname << p_joint->name << "_tl2";
             AddEndPointSphere(tL2,endname.str());
             CLActors.push_back(endname.str());
             AddLinearSkeletonElement(tL1,tL2,p_joint->name);

         }

    }

    void ContourGen::CreateContoursFromSkeleton(hbm::FEModel& m_fem, hbm::Metadata& m_meta)
    {
        std::queue<ContourCLbr*> local;
        local.push(m_meta.ctrl_line.root);

        while (local.size() != 0)
        {
            ContourCLbr* current_br = local.front();
            local.pop();
            //Get and Display get contour
            CreateContoursListForBR(m_fem, m_meta, current_br);
            for (int i = 0; i < current_br->childrenjts.size(); i++)
            {

                MatrixXd spline_matrix = GetHermiteSpline(m_fem, m_meta, current_br, i);

                //Init the Bezier Curve
                ComputeBezierCurve(spline_matrix);

                ComputeContourForJoint(current_br,i);

                local.push(current_br->childrenjts.at(i)->childbodyregion);


			}
		}

	}


	void ContourGen::ClearPushedContours()
	{
		Metadata& m_meta = Context::instance().project().model().metadata();
		std::queue<ContourCLbr*> local;
		local.push(m_meta.ctrl_line.root);

		while (local.size() != 0)
		{
			ContourCLbr* current_br = local.front();
			local.pop();

			//Clear BR Contour
			current_br->brcont.clear();

			for (int i = 0; i < current_br->childrenjts.size(); i++)
			{
				current_br->childrenjts.at(i)->jointcont.clear();
				local.push(current_br->childrenjts.at(i)->childbodyregion);
			}
		}

	}




	void ContourGen::RemoveSkeletonActors()
	{
		for (auto it = CLActors.begin(); it != CLActors.end(); ++it)
		{
			std::string name = *it;
			Context::instance().display().RemoveActor(name);

		}

		for (auto it = CircumActors.begin(); it != CircumActors.end(); ++it)
		{
			std::string name = *it;
			Context::instance().display().RemoveActor(name);

		}

		CLActors.clear();
		CircumActors.clear();

		Context::instance().display().Refresh();

	}

	void ContourGen::RemoveContourActors()
	{
		for (auto it = ContourActors.begin(); it != ContourActors.end(); ++it)
		{
			std::string name = *it;
			Context::instance().display().RemoveActor(name);

		}

		ContourActors.clear();
		Context::instance().display().Refresh();
	}



	void ContourGen::HideContoursListForBR(float opacity)
	{

		for (auto it = ContourActors.begin(); it != ContourActors.end(); ++it)
		{
			std::string name = *it;
            Context::instance().display().SetObjectOpacity(name, opacity);
		}

		Context::instance().display().Refresh();

	}



	///Function for reloading all the contours from the control line.
	void ContourGen::UpdateContoursListForBR()
	{
		std::queue<ContourCLbr*> local;
		local.push(m_meta.ctrl_line.root);

        boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
		while (local.size() != 0)
		{
			ContourCLbr* current_br = local.front();
			local.pop();

			for (int i = 0; i < current_br->brcont.size(); i++)
			{
				Contour* contour = current_br->brcont[i];
				double point[3];

				//get all the nodes for a particular contour
				for (auto it2 = contour->mcontour.begin(); it2 != contour->mcontour.end(); ++it2)
				{
					//Get the node
					std::vector<NodePtr> vnode = contour->mcontour[it2->first];
					vtkSmartPointer<vtkPoints> updated_point = vtkSmartPointer<vtkPoints>::New();
					for (int j = 0; j < vnode.size(); j++)
					{
						point[0] = vnode[j]->getCoordX();
						point[1] = vnode[j]->getCoordY();
						point[2] = vnode[j]->getCoordZ();
						//Create updated points
						updated_point->InsertNextPoint(point);

					}
					auto it = actors->find(it2->first);
					if (it != actors->end())
					{

						vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
						inputPolyData->SetPoints(updated_point);

					}
					else
					{
						pWarning() << "Actor " << it2->first << "not present";
					}

				}

			}

			for (int i = 0; i < current_br->childrenjts.size(); i++)
			{

				for (int k = 0; k < current_br->childrenjts.at(i)->jointcont.size(); k++)
				{
					Contour* contour = current_br->childrenjts.at(i)->jointcont[k];
					double point[3];

					//get all the nodes for a particular contour
					for (auto it2 = contour->mcontour.begin(); it2 != contour->mcontour.end(); ++it2)
					{
						//Get the node
						std::vector<NodePtr> vnode = contour->mcontour[it2->first];
						vtkSmartPointer<vtkPoints> updated_point = vtkSmartPointer<vtkPoints>::New();
						for (int j = 0; j < vnode.size(); j++)
						{
							point[0] = vnode[j]->getCoordX();
							point[1] = vnode[j]->getCoordY();
							point[2] = vnode[j]->getCoordZ();
							//Create updated points
							updated_point->InsertNextPoint(point);

						}

						auto it = actors->find(it2->first);
						if (it != actors->end())
						{
							vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
							inputPolyData->SetPoints(updated_point);
						}
						else
						{
							pWarning() << "Actor " << it2->first << "not present";
						}

					}


				}


				local.push(current_br->childrenjts.at(i)->childbodyregion);

			}
		}
		Context::instance().display().Render();

	}

	void ContourGen::drawSpline(Eigen::MatrixXd splineMatrix){
		std::cout << "\ncalling ComputeBezierCurve ";
		ComputeBezierCurve(splineMatrix);
		int i = rand() % 1000;
		stringstream convert;
		convert << i;
		string actor_name = convert.str();
		std::cout << "\ncalling ComputeJointSkeleton ";
		ComputeJointSkeleton(actor_name);
		//Refresh the display
		Context::instance().display().Refresh();
	}

	//Set the scaling value for the contour.
	void ContourGen::scaleContour(double scale_value, std::string name)
	{
		ScalingValue = scale_value;
		scalingregion = name;

		if (std::find(ContourActors.begin(), ContourActors.end(), name) != ContourActors.end())
		{

            boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();

			auto it = actors->find(name);
			if (it != actors->end())
			{
				vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
				Context::instance().display().SelectActor(it->second, APPEND_ALL, false);

				//Compute the center of mass
				vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter =
					vtkSmartPointer<vtkCenterOfMass>::New();

				centerOfMassFilter->SetInputData(inputPolyData);
				centerOfMassFilter->SetUseScalarsAsWeights(false);
				centerOfMassFilter->Update();
				double center[3];
				centerOfMassFilter->GetCenter(center);

				vtkSmartPointer<vtkPoints> scaled_cutter_points = vtkSmartPointer<vtkPoints>::New();
				scaled_cutter_points = ScaleCircumference(inputPolyData->GetPoints(), ScalingValue, center);
				inputPolyData->SetPoints(scaled_cutter_points);
				Context::instance().display().Refresh();
			}
			else
			{
				pWarning() << "Actor " << name << "not present";
			}
		}
		else
			pWarning() << "Element not present";
	}

	std::vector< std::string > ContourGen::getContourActors(){
		return ContourActors;
	}

	}

}

