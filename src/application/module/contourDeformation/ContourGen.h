/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOUR_CONTOURGEN_H
#define PIPER_CONTOUR_CONTOURGEN_H
/** @brief 	A class for visualization of HBM Skeleton.
	*
	* @author Sukhraj Singh @date 2016
	*/
#include "common/Context.h"

#include "contours/ParserContourFile.h"

#include "contours/ParseContourCL.h"
#include "contours/main_repositioning.h"

namespace piper {
    namespace contours {
        //namespace contourdeformation {
        class ContourGen {
        public:
            ~ContourGen();
            static ContourGen* getInstance();
            void Init();
            tinyxml2::XMLError ReadContourCL(std::string file);
            bool ComputeSkeleton();
            void RemoveSkeletonActors();
            void SetScalingValue(double scale_value, std::string circum_element);
            void ComputeContours();

            void GetContourforBR(std::string br_name);
            void GetContoursforJoint(std::string j_name);
            void GetContourforJoint(std::string j_name);

            void UpdateContoursListForBR();

            void ForceContourGen();

            void DisplayAxes(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLj* p_joint);

            void HideContoursListForBR(float opacity = 0);
            ProcessingFunc process_func;
            //Init hierarchy
            hbm::FEModel& m_fem = Context::instance().project().model().fem();
            hbm::Metadata& m_meta = Context::instance().project().model().metadata();
            MainRepositioning main_repo;

            void drawSpline(Eigen::MatrixXd splineMatrix);


            void scaleContour(double scale_value, std::string circum_element);
            std::vector< std::string > getContourActors();
            double SCALING_FACTOR = 1.10; //Scale circumference elements, as of now 10%.

        private:
            //Singleton class for contours
            static bool IsCreated;
            static ContourGen *m_ContourGen;
            ContourGen();

            VtkDisplayHighlighters draw;

            Eigen::MatrixXd bezier_ctrl_pts;
            SpaceTransform transformObj;

            void ComputeJointSkeleton(std::string name);
            void ComputeBezierCurve(Eigen::MatrixXd spline_matrix);
            std::vector<double> ComputePointsFromBezier(Eigen::MatrixXd bezier_ctrl_pts, double u);
            Eigen::MatrixXd GetHermiteSpline(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br, int iter);
            void AddLinearSkeletonElement(double* p0, double* p1, std::string name);
            void PushGeneratedJointContours(vtkSmartPointer<vtkPoints> cutter_points, hbm::ContourCLbr* current_br, int index, std::string name, double uval);
            void PushGeneratedContours(vtkSmartPointer<vtkPoints> cutter_points, hbm::ContourCLbr *temp1, double contcount, std::string name);

            //@Aditya
            void AddCLbrElement(std::vector<hbm::NodePtr>, std::string name);

            void AddEndPointSphere(double* p, std::string lin_endname);

            //@Aditya
            void AddLctrlptSphere(hbm::NodePtr p, std::string lin_endname);

            void PopulateVTKConstructs(hbm::FEModel& m_fem, hbm::Metadata& m_meta);
            void CreateContoursFromSkeleton(hbm::FEModel& m_fem, hbm::Metadata& m_meta);
            void GebodCircumElements(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br, int curr_el);
            void GebodCircumElementsjoints(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLj* currentj, int curr_el);
            void GebodFrames(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr *current_br, int iter);
            void CreateContoursListForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr *current_br);
            vtkSmartPointer<vtkPoints> ScaleCircumference(vtkSmartPointer<vtkPoints> cutter_points,
                double scaling_factor, double Origin[3]);
            vtkSmartPointer<vtkPoints> ScaleLinearElement(vtkSmartPointer<vtkPoints> points);
            vtkSmartPointer<vtkPolyData> CreateContourForBr(vtkUnstructuredGrid* PolyDataBR, double Origin[],
                double Normal[], int ContourCount, double range[]);
            void ComputeContourForJoint(hbm::ContourCLbr *current_br, int index);

            vtkSmartPointer<vtkAxesActor> CreateLocalFrame(double axes_first[], double axes_second[], double axes_center[]);
            void ComputeLinearElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);
            void CreateSphereForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);
            void CreateSplineForBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);
            void CreateContourForSplineBR(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);

            void ExtendContour(vtkSmartPointer<vtkPolyData> polyDataOut, int count, double *curr_p, hbm::ContourCLbr *current_br, double scaling_factor = 1.25);

            //@Aditya
            void ComputeBodyRegionElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);


            void CreateContourForChildElement(hbm::FEModel& m_fem, hbm::Metadata& m_meta, hbm::ContourCLbr* current_br);

            void GetSplineTangent(Eigen::MatrixXd bezier_ctrl_pts, double u, double* normal);


            void RemoveContourActors();
            void ClearPushedContours();

            ParserContourFile parser;
            ContoursHbmInterface m_conthbm;

            //Contour Parser
            ParseContourCL parsecontcl;

            std::vector< std::string > ContourActors;
            std::vector< std::string > CircumActors;
            std::vector< std::string > CLActors;

            bool IsContourCreated;
            double ScalingValue = 1.0;
            std::string scalingregion;
        };
    }
}

//}
#endif//PIPER_CONTOUR_ContourGen_H
