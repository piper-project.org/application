// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Repositioning"
    minimumHeight : 400
    minimumWidth : 530

    property  bool targetParametersGiven : false
    property bool contoursLoaded : false
    property int functionTag: -1
    property int lastJointMoved : -1

    ListModel {
        id: model1
        ListElement {
            contour: "Knee"
        }
        ListElement {
            contour: "Ankle"
        }
        ListElement {
            contour: "Hip"
        }
        ListElement {
            contour: "Neck"
        }
       ListElement {
            contour: "Elbow"
        }
       ListElement {
            contour: "Wrist"
        }
    }


    ColumnLayout{
        anchors.fill: parent
        anchors.margins : 10


        RowLayout{
            GroupBoxNoTitle{
                id : groupBox
                ColumnLayout{
                    anchors.centerIn: parent
                    Button {
                        id:loadContourCLButton
                        text :"Load ContourCL file"
                        onClicked: {
                            openContourCLXMLDialog.open()
                        }
                    }
                    Button {
                        visible: false
                        text :"Load Spline Info"
                        onClicked: {
                            openContourDialog.open()
                        }
                    }


                    FileDialog {
                        id: openContourCLXMLDialog
                        title: qsTr("Open ContourCL xml File...")
                        nameFilters: ["Contour files (*.xml)"]
                        onAccepted: {
                            console.log(openContourCLXMLDialog.fileUrl.toString());
                            root.contoursLoaded = false
                            if(myContourDeformation.ui_loadContourCLXml(openContourCLXMLDialog.fileUrl))
                            {
                                useTargets.enabled = true
                                fullModelOpacity.value = 0.2
                                repositioningParametersGroupBox.enabled = true
                                myContourDeformation.ui_setReposAngle(0.0);
                                // Performing pre-processing at all times
                                myContourDeformation.ui_setReposMap(true)
                                angle.value = 0.0
                                messageDialog.text = "ContourCL File Loaded."
                                var vals = contextMetaManager.ui_getUndefinedContourCLElements()
                                var complete = true
                                var message = "The following metadata are missing , please create them in the metaeditor for proper functioning :\n\nMETADATA\t\t\t\tTYPE\n"
                                for(var elem in vals){
                                    if(elem ==  "")
                                        elem = "Missing name"+"\t"+vals[elem][0]+"\n"
                                    else
                                        message += elem+"\t"+vals[elem][0]+"\n"
                                    complete = false
                                }
                                if(complete==false){
                                    messageDialog.text = message
                                    messageDialog.visible = true
                                }
                                if( contextMetaManager.getIsModelChanged() === false){
                                    contextMetaManager.setIsModelChanged(true)
                                    myContourDeformation.setResultinHistory("Init_ContourDeformation")
                                }
                            }else{
                                   messageDialog.text = "Invalid contourCL xml file!"
                                   messageDialog.visible = true
                            }
                        }
                    }
                    FileDialog {
                        id: openContourDialog
                        title: qsTr("Open Contour File...")
                        nameFilters: ["Contour files (*.txt *.xml)"]
                        onAccepted: {
                            if(myContourDeformation.ui_validateFile(openContourDialog.fileUrl,"*POINT")){
                                myContourDeformation.ui_openContour(openContourDialog.fileUrl);
                                root.contoursLoaded = true
                            }else{
                                messageDialog.text = "Invalid hip Contours File";
                                messageDialog.visible = true;
                            }

                        }
                    }




                    RowLayout{
                        id : useTargets
                        enabled: false
                        Switch{
                            id: useTargetSwitch
                            checked :false
                            onCheckedChanged: {
                                if(checked == true){                             
                                    //processLandmarkTargets.visible = true
                                    startPositioningUsingTargets.visible = true
                                    startPositioningUsingTargets.enabled = true
                                    processNextTarget.visible = true
                                    processNextTarget.enabled = false
                                    generateFullBodyContours.enabled = false
                                    root.enableReposParameters(false)
                                    root.setReposJointLabel(0)
                                    root.setReposSideLabel(0)
                                    root.setReposTypeLabel(0)
                                    root.setReposAngleLabel(0)
                                }else{
                                    generateFullBodyContours.enabled = true
                                    root.setReposJointLabel(-1)
                                    root.setReposSideLabel(-1)
                                    root.setReposTypeLabel(-1)
                                    root.setReposAngleLabel(-999)
                                    reposButton.enabled = false
                                    deformContoursButton.enabled = false
                                    root.enableReposParameters(true)
                                    processLandmarkTargets.visible = false
                                    startPositioningUsingTargets.visible = false
                                    processNextTarget.visible = false

                                }
                            }
                        }
                        Label {
                            text : "Use Landmark Tagets"
                        }
                    }
                }
            }
                
            Rectangle{
                height:groupBox.height; width:175
                color: "transparent"
                ColumnLayout{
                    GroupBoxNoTitle{
                            height:parent.height;
                        ColumnLayout{
                                RowLayout{
                                    Label{text:"Model Opacity : "}
                                    SpinBox {
                                        id: fullModelOpacity;
                                        decimals: 2
                                        minimumValue: 0.00
                                        maximumValue: 1.00
                                        stepSize: 0.10
                                        value: 1.00
                                        onValueChanged: {
                                            myContourDeformation.setOpacity(fullModelOpacity.value);
                                        }
                                    }
                                }
                                RowLayout{
                                    Label{text:"ContourCL Opacity : "}
                                    SpinBox {
                                        id: clOpacity;
                                        decimals: 2
                                        minimumValue: 0.00
                                        maximumValue: 1.00
                                        stepSize: 0.10
                                        value: 1.00
                                        onValueChanged: {
                                            myContourDeformation.setCLOpacity(clOpacity.value);
                                        }
                                    }
                                }
                        }
                    }
                }
            }


        }


        RowLayout{
            id: myReposTabContent;
            //enabled: false;


        GroupBox{
            id:repositioningParametersGroupBox
            title:"Repositioning parameters"
            enabled:false
            property int joint

            ColumnLayout{

            Button{
                id: processLandmarkTargets
                visible: false
                text:"Process landmark targets"
                onClicked: {
                    if(root.checkIfLandmarkTargetsArePresent()){
//                         root.functionTag = 1
//                         myContourDeformation.useLandmarkTargetBusyIndicator();
//                         processLandmarkTargets.visible = false
//                         startPositioningUsingTargets.visible = false
//                         generateFullBodyContours.enabled = true
//                         //continueProcessingLandmarkTargets.visible = true
//                         processNextTarget.visible = true
//                         useTargetSwitch.enabled = false
                    }else{
                        messageDialog.text = "No landmark targets found !\nPlease import a landmark target file from the \"Target\" tab in the menu"
                        messageDialog.visible = true
                    }

                }
            }

            Button{
                id: startPositioningUsingTargets
                visible: false
                text:"Start Positioning Using Targets"
                onClicked: {
                    if(root.checkIfLandmarkTargetsArePresent()){
                         root.functionTag = 11
                         myContourDeformation.ui_initializeTargetsBusyIndicator();
                         //startPositioningUsingTargets.visible = false
                         startPositioningUsingTargets.enabled = false
                         generateFullBodyContours.enabled = true
                         processNextTarget.visible = true
                         processNextTarget.enabled = true
                         useTargetSwitch.enabled = false
                         processNextTarget.enabled = false
                    }else{
                        messageDialog.text = "No landmark targets found !\nPlease import a landmark target file from the \"Target\" tab in the menu"
                        messageDialog.visible = true
                    }

                }
            }

            RowLayout{
                id:selectReposFunc
                Label{
                    id: reposJointLabel
                    text : "Joint : "


                }
                ComboBox {
                    id:jointComboBox
                    model: [ "Knee" , "Ankle" , "Hip" , "Neck" , "Elbow" , "Wrist", "Thorax", "Lumbar", "Shoulder"  ]
                    currentIndex: -1
                    onCurrentIndexChanged: {
                        selectReposSide.enabled = true
                        myContourDeformation.ui_setReposFunc(currentIndex+1);
                        angleInfoButton.joint = currentIndex+1;
                        switch(currentText){
                            case "Knee":
                                typeComboBox.model = [ "Flexion/Extension" ]
                                selectReposSide.enabled = true
                                break;
                            case "Ankle":
                                typeComboBox.model = [ "Flexion/Extension", "Inversion/Aversion" ]
                                selectReposSide.enabled = true
                                break;
                            case "Hip":
                                typeComboBox.model = [ "Flexion/Extension", "Abduction/Adduction","Internal/External Rotation" ]
                                selectReposSide.enabled = true
                                break;
                            case "Neck":
                                typeComboBox.model = [ "Flexion/Extension", "Lateral Rotation", "Lateral Flexion" ]
                                selectReposSide.enabled = false
                                break;
                            case "Elbow":
                                typeComboBox.model = [ "Flexion/Extension", "Pronation/Supination" ]
                                selectReposSide.enabled = true
                                break;
                            case "Wrist":
                                typeComboBox.model = [ "Flexion/Extension" ]
                                selectReposSide.enabled = true
                                break;
                            case "Thorax":
                                typeComboBox.model = [ "Flexion/Extension", "Lateral Flexion"]
                                selectReposSide.enabled = false
                                break;
                            case "Lumbar":
                                typeComboBox.model = [ "Flexion/Extension", "Lateral Flexion", "Twisting" ]
                                selectReposSide.enabled = false
                                break;
                            case "Shoulder":
                                typeComboBox.model = [ "Flexion/Extension", "Abduction/Adduction", "Twisting" ]
                                selectReposSide.enabled = true
                                break;
                        }
                    }
                }
            }

            RowLayout{
                    id:selectReposSide
                    enabled: false
                    Label{
                        id: reposSideLabel
                        text : "Side  : "
                    }
                    ComboBox {
                        id:sideComboBox
                        model: [ "Left" , "Right"  ]
                        currentIndex: -1
                        onCurrentIndexChanged: {
                            selectReposType.enabled = true
                            angleInfoButton.side = currentIndex
                            myContourDeformation.ui_setReposSide(currentIndex);

                        }
                    }
            }


            RowLayout{
                    id:selectReposType
                    enabled: false
                    Label{
                        id : reposTypeLabel
                        text : "Type : "
                    }
                    ComboBox {
                        id:typeComboBox
                        model: [ "Flexion/Extension" ]
                        currentIndex: -1
                        onCurrentIndexChanged: {
                            angleInfoButton.type = currentIndex
                            switch(currentText){
                                case "Flexion/Extension":
                                    myContourDeformation.ui_setReposType(0)
                                    break;
                                case "Abduction/Adduction":
                                    myContourDeformation.ui_setReposType(1)
                                    break;
                                case "Lateral Flexion":
                                    myContourDeformation.ui_setReposType(1)
                                    break;
                                case "Internal/External Rotation":
                                    myContourDeformation.ui_setReposType(2)
                                    break;
                                case "Twisting":
                                    myContourDeformation.ui_setReposType(2)
                                    break;
                                case "Lateral Rotation":
                                    myContourDeformation.ui_setReposType(2)
                                    break;
                                case "Pronation/Supination":
                                    myContourDeformation.ui_setReposType(2)
                                    break;
                                case "Inversion/Aversion":
                                    myContourDeformation.ui_setReposType(2)
                                    break;

                            }
                        }
                    }
                }

                RowLayout{
                        Label{ id: angleLabel ; text:"Angle :       "}

                        SpinBox {
                            id: angle;
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: 0
                            onValueChanged: {
                                myContourDeformation.ui_setReposAngle(angle.value);
                            }
                        }
                        Button{
                            id : angleInfoButton
                            iconSource:  "qrc:///icon/information-icon.png"
                            property int joint
                            property int side
                            property int type
                            property double angleComputed
                            property double deformAngle
                            property double min_angle
                            property double max_angle
                            onClicked: {
                                root.generateTextForAngleInfo(joint,type,side,angleComputed,deformAngle,min_angle,max_angle)
                                messageDialog.visible = true
                            }
                        }
                    }


                    ColumnLayout{
                        Connections {

                            target: context

                            onOperationFinished:  {
                                // Check if targets are present when opening the model, if they are present switching to target based positioing UI directly
                                if(root.functionTag === 0 && root.checkIfLandmarkTargetsArePresent()===true && useTargetSwitch.checked === false){
                                    useTargetSwitch.checked = true
                                    root.functionTag = -1
                                }
                                //  for reposBusyIndicator()
                                if(root.functionTag === 4){
                                    var timeString = new Date().toLocaleTimeString(Qt.locale(), Locale.ShortFormat);
                                    var text = Date.fromLocaleTimeString(Qt.locale(), timeString, Locale.ShortFormat);
                                    var string = "Contour_deformaiton_"+text
                                    root.functionTag = 5
                                    myContourDeformation.setResultinHistory(string)
                                    root.functionTag = -1
                                }
                                // for ui_initializeTargetsBusyIndicator and processNextTarget()
                                if(root.functionTag === 11 ) {
                                    var reposFunc, reposType, reposSide, possibleAngle, computedAngle, angleLimit,currentAbsoulteAngle
                                    var vals = myContourDeformation.ui_getReposParameters()
                                    root.enableReposParameters(false)
                                    reposFunc  = parseInt(vals[0])
                                    jointComboBox.currentIndex = reposFunc-1
                                    myContourDeformation.ui_setReposFunc(reposFunc);
                                    reposType  = parseInt(vals[1])
                                    typeComboBox.currentIndex = reposType
                                    myContourDeformation.ui_setReposType(reposType)
                                    reposSide  = parseInt(vals[2])
                                    sideComboBox.currentIndex = reposSide
                                    myContourDeformation.ui_setReposSide(reposSide);
                                    possibleAngle = parseFloat(vals[3])
                                    angle.value = possibleAngle
                                    myContourDeformation.ui_setReposAngle(possibleAngle);
                                    computedAngle = parseFloat(vals[4])
                                    currentAbsoulteAngle = parseFloat(vals[5])
                                    angleLimit = parseFloat(vals[6])
                                    if(computedAngle !== possibleAngle){
                                        messageDialog.text = " Current angle of the joint = "+currentAbsoulteAngle
                                        if(angleLimit!==-999)
                                            if(angleLimit < currentAbsoulteAngle)
                                                messageDialog.text += "\n Minimum possible angle = "+angleLimit
                                            else
                                                messageDialog.text += "\n Minimum possible angle = "+angleLimit
                                        messageDialog.text += "\n Angle computed from target = "+computedAngle
                                        messageDialog.text += "\n Continuing positioning by angle "+possibleAngle
                                        messageDialog.visible = true
                                    }else if(possibleAngle > 1 || possibleAngle <-1 ){
                                        reposJointLabel.text = "Joint : "+jointComboBox.model[reposFunc-1]
                                        reposTypeLabel.text = "Type : "+typeComboBox.model[reposType]
                                        setReposSideLabel(reposSide+1)
                                        setReposAngleLabel(possibleAngle)
                                        console.log(" Positioning parameters : reposFunc = "+ reposFunc+" reposType = "+reposType+" reposSide = "+ reposSide+" possibleAngle = "+ possibleAngle+" computedAngle = "+computedAngle+" angleLimit = "+angleLimit)
                                        messageDialog.text =   " Joint to be positioned : "+jointComboBox.model[reposFunc-1]
                                        messageDialog.text += "\n Type of movement       : "+typeComboBox.model[reposType]
                                        messageDialog.text += "\n Angle computed         : "+angle.value
                                        messageDialog.visible = true
                                    }else if( possibleAngle < 1 && possibleAngle>-1 ){
                                        console.log(" Positioning Complete")
                                        messageDialog.text = " Positioning complete !"
                                        messageDialog.visible = true
                                        generateFullBodyContours.enabled = false
                                        useTargetSwitch.enabled = true
                                        root.setReposJointLabel(0)
                                        root.setReposSideLabel(0)
                                        root.setReposTypeLabel(0)
                                        root.setReposAngleLabel(0)
                                    }
                                    root.functionTag = -1
                                }
                                // for useLandmarkTargetBusyIndicator()
                                if(root.functionTag === 1 ) {
                                    var joint, type, side=0, angl
                                    var vals = myContourDeformation.ui_GetBRandAngleForLandmarkTarget()
                                    type  = parseInt(vals[1])
                                    angl = parseFloat(vals[2])
                                    if(angl!==0){
                                        console.log("ui_GetBRandAngleForLandmarkTarget called : "+vals[0]+"\t"+vals[2])
                                        if( vals[0] === "kneel" || vals[0] === "kneer" )
                                            joint = 1
                                        else if ( vals[0] === "anklel" || vals[0] === "ankler" )
                                            joint = 2
                                        else if ( vals[0] === "hipl" || vals[0] === "hipr" )
                                            joint = 3
                                        else if ( vals[0] === "cervical_joint" )
                                            joint = 4
                                        else if ( vals[0] === "elbowl" ||  vals[0] === "elbowr")
                                            joint = 5
                                        else if ( vals[0] === "wristl" ||  vals[0] === "wristr")
                                            joint = 6

                                        root.lastJointMoved = joint
                                        generateFullBodyContours.joint = joint
                                        if( vals[0].charAt(vals[0].length - 1) === "l")
                                            side = 0
                                        else if (vals[0].charAt(vals[0].length - 1) === "r")
                                            side = 1
                                        root.setReposJointLabel(joint)
                                        root.setReposTypeLabel(type+1)
                                        root.setReposSideLabel(side+1)
                                        root.setReposAngleLabel(angl)
                                        myContourDeformation.ui_setReposFunc(joint)
                                        myContourDeformation.ui_setReposSide(side)
                                        myContourDeformation.ui_setReposType(type)
                                        myContourDeformation.ui_setReposAngle(angl)
                                        angleInfoButton.joint = joint
                                        angleInfoButton.type = type
                                        angleInfoButton.side = side
                                        angleInfoButton.angleComputed = angl
                                        root.functionTag = -1

                                    }
                                    else{
                                        messageDialog.text = " Positioning complete !"
                                        messageDialog.visible = true
                                        generateFullBodyContours.enabled = false
                                        useTargetSwitch.enabled = true
                                        root.setReposJointLabel(0)
                                        root.setReposSideLabel(0)
                                        root.setReposTypeLabel(0)
                                        root.setReposAngleLabel(0)
                                    }
                                }
                            }
                            onTargetChanged: {
                                console.log("New targets loaded.\nPlease click on the \"Process Landmark Targets\" button again.")
                                useTargetSwitch.checked = true
                                //processLandmarkTargets.enabled = true
                               // processLandmarkTargets.visible = true
                                startPositioningUsingTargets.visible = true
                                useTargetSwitch.enabled = true
                                //continueProcessingLandmarkTargets.visible = false
                                processNextTarget.visible = false
                                generateFullBodyContours.enabled = false
                                deformContoursButton.enabled = false
                                reposButton.enabled = false
                                root.lastJointMoved = -1
                                root.functionTag = 0
                                myContourDeformation.ui_markUnprocessed()
                            }
                            onModelChanged: {
                                console.log("New model loaded")
                                repositioningParametersGroupBox.enabled = false
                                useTargetSwitch.enabled = true
                                //processLandmarkTargets.enabled = true
                                //processLandmarkTargets.visible = false
                                startPositioningUsingTargets.visible = false
                                //continueProcessingLandmarkTargets.visible = false
                                continueProcessingLandmarkTargets.enabled = false
                                processNextTarget.visible = false
                                processNextTarget.enabled = false
                                useTargetSwitch.checked = false
                                root.enableReposParameters(true)
                                generateFullBodyContours.enabled = true
                                deformContoursButton.enabled = false
                                reposButton.enabled = false
                                root.lastJointMoved = -1
                                root.targetParametersGiven = false
                                root.contoursLoaded  = false
                                root.functionTag = 0
                                myContourDeformation.ui_markUnprocessed()
                            }
                        }

                        Button{
                            id: generateFullBodyContours
                            text: qsTr("Generate contours")
                            Layout.fillWidth: true
                            property int joint: -1
                            onClicked: {
                                console.log("jointComboBox.currentIndex = "+jointComboBox.currentIndex)
                                console.log("typeComboBox.currentIndex = "+typeComboBox.currentIndex)
                                console.log("sideComboBox.currentIndex = "+sideComboBox.currentIndex)
                                root.functionTag = 2
                                myContourDeformation.generateContBusyIndicator(1);
//                                if( jointComboBox.currentText == "Hip" || (useTargetSwitch.checked==true && generateFullBodyContours.joint ==3) ){
//                                    if(root.contoursLoaded === false){
//                                        messageDialog.text = "Hip contours are not loaded, please load the hip contours and click on \"Generate contours\" button again"
//                                        messageDialog.visible = true
//                                    }else if(myContourDeformation.ui_getHipProcess()==true){
//                                        messageDialog.text = "Hip positioning is not possible now."
//                                        messageDialog.visible = true
//                                    }else{
//                                        deformContoursButton.enabled = true
//                                        if(useTargetSwitch.checked==true)
//                                            generateFullBodyContours.enabled = false
//                                    }
//                                } else{
                                    deformContoursButton.enabled = true
                                    if(useTargetSwitch.checked==true)
                                        generateFullBodyContours.enabled = false
//                                }

                        }
                      }


                            Button {
                                id : deformContoursButton
                                text: "Deform Contours"
                                tooltip: "Deform Contours"
                                enabled: false
                                Layout.fillWidth: true
                                onClicked: {
                                    deformContoursButton.enabled = false
                                    root.functionTag = 3
                                    myContourDeformation.deformContoursBusyIndicator()
                                    reposButton.enabled = true
                                    root.enableReposParameters(false)
                                    useTargetSwitch.enabled = false
                                    if(useTargetSwitch.checked===false)
                                        useTargetSwitch.enabled = false
                                }

                            }


                    }
                    Button {
                        id:reposButton
                        Layout.fillWidth: true
                        enabled: false
                        text: "Reposition"
                        tooltip: "Reposition"
                        onClicked: {
                            angle.value = myContourDeformation.ui_getReposAngle()
                            if( myContourDeformation.ui_getEnableReposButton() === true){
                                root.functionTag = 4
                                myContourDeformation.reposBusyIndicator()
//                                if(typeComboBox.currentText != "Flexion/Extension" )
//                                    myContourDeformation.reposBusyIndicator()
//                                else{
//                                    console.log("\n Calling ui_repositionContours with "+jointComboBox.currentText+"\t"+typeComboBox.currentText)
//                                    myContourDeformation.ui_repositionContours()
//                                }
                            }else{
                                messageDialog.text = "Please first create the metadata required to do positioning at the selected joint."
                                messageDialog.visible = true
                            }
                            myContourDeformation.ui_setEnableReposButton(false)
                            if(useTargetSwitch.checked === false)
                                useTargetSwitch.enabled = true
                            root.enableReposParameters(true)
                            reposButton.enabled = false
                            if(useTargetSwitch.checked === true){
                                root.setReposJointLabel(0)
                                root.setReposSideLabel(0)
                                root.setReposTypeLabel(0)
                                root.setReposAngleLabel(0)
                                continueProcessingLandmarkTargets.enabled = true
                                processNextTarget.enabled = true
                            }

                        }
                    }

                    Button{
                        id: continueProcessingLandmarkTargets
                        Layout.fillWidth: true
                        enabled: false
                        visible: false
                        text:"Continue"
                        onClicked: {

                                 root.functionTag = 1
                                 myContourDeformation.useLandmarkTargetBusyIndicator()
                                 //processLandmarkTargets.enabled = false
                                 generateFullBodyContours.enabled = true
                                 continueProcessingLandmarkTargets.enabled = false
                                 if(useTargetSwitch.checked===false)
                                    useTargetSwitch.enabled = false

                        }
                    }
                    Button{
                        id: processNextTarget
                        Layout.fillWidth: true
                        enabled: false
                        visible: false
                        text:"Process Next Target"
                        onClicked: {
                                 root.functionTag = 11
                                 myContourDeformation.ui_processNextTargetBusyIndicator()
                                 generateFullBodyContours.enabled = true
                                 //processNextTarget.enabled = false
                                 if(useTargetSwitch.checked===false)
                                    useTargetSwitch.enabled = false
                                 processNextTarget.enabled = false

                        }
                    }
                }
            }
        }

        ModuleToolWindowButton
        {
            text:"\&Help"
            toolWindow: reposHelpWindow
            shortcutChar: "H"
        }

        GroupBoxNoTitle{
            visible: false
            id : splineMaker

            property var coordinates: [0,0,0]

            RowLayout{
                anchors.centerIn: parent

                GroupBoxNoTitle{
                    ColumnLayout{
                        anchors.centerIn: parent
                        Row{

                            Label{
                                text: "Single node picker : "
                            }

                            Button{
                                Layout.fillWidth: true
                                tooltip: "Node Picker"
                                iconSource: "qrc:///icon/picker-node.png"
                                onClicked:{
                                    contextVtkDisplay.SetPickingType(0);
                                }
                            }
                        }
                        Button{
                            text: "Copy coordinates of the selected node"
                            Layout.fillWidth: true
                            onClicked: {
//                                var a = ( Math.random() * 100 ) % 100
//                                var b = ( Math.random() * 100 ) % 100
//                                var c = ( Math.random() * 100 ) % 100
//                                splineMaker.coordinates = [a,b,c]
                                var list = contextMetaManager.getCoordinatesOfSelectedNode()
                                console.log("Coordinates : "+list)
                                //splineMaker.coordinates
                            }
                        }
                        Button{
                            text: "Draw spline"
                            Layout.fillWidth: true
                            onClicked: {
                                myContourDeformation.ui_drawSpline(p0x.value, p0y.value, p0z.value,p1x.value, p1y.value, p1z.value,t1x.value, t1y.value, t1z.value,t2x.value, t2y.value, t2z.value)
                            }
                        }
                        Button{
                            text: "Generate contours"
                            Layout.fillWidth: true
                            onClicked: {
                                //
                            }
                        }
                    }
                }

                ColumnLayout{
                    RowLayout{
                        Label{ text:" Tangent magnitude 1 = "}
                        SpinBox { id : t1mag;  decimals: 2; minimumValue: 0.01; stepSize: 0.1; value: 1 }
                        Label{ text:" Tangent magnitude 2 = "}
                        SpinBox { id : t2mag;  decimals: 2; minimumValue: 0.01; stepSize: 0.1; value: 1 }
                    }

                    RowLayout{
                        Label{ text:" P0 : "}
                        RowLayout{
                            Label{ text:" x = "}
                            SpinBox { id : p0x; decimals: 0; minimumValue: 0; stepSize: 1; value: 0 }
                            Label{ text:" y = "}
                            SpinBox { id : p0y; decimals: 0; minimumValue: 0; stepSize: 1; value: 0 }
                            Label{ text:" z = "}
                            SpinBox { id : p0z; decimals: 0; minimumValue: 0; stepSize: 1; value: 0 }
                            Button{
                                text : "Paste"
                                Layout.fillWidth: true
                                onClicked: {
                                    p0x.value = splineMaker.coordinates[0]
                                    p0y.value = splineMaker.coordinates[1]
                                    p0z.value = splineMaker.coordinates[2]
                                }
                            }
                        }
                    }
                    RowLayout{
                        Label{ text:" P1 : "}
                        RowLayout{
                            Label{ text:" x = "}
                            SpinBox {
                                id : p1x
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Label{ text:" y = "}
                            SpinBox {
                                id : p1y
                                decimals: 0; stepSize: 1; value: 0
                            }
                            Label{ text:" z = "}
                            SpinBox {
                                id : p1z
                               decimals: 0; stepSize: 1; value: 0

                            }
                            Button{
                                text : "Paste"
                                Layout.fillWidth: true
                                onClicked: {
                                    p1x.value = splineMaker.coordinates[0]
                                    p1y.value = splineMaker.coordinates[1]
                                    p1z.value = splineMaker.coordinates[2]
                                }
                            }
                        }
                    }
                    RowLayout{
                        Label{ text:" T1 : "}
                        RowLayout{
                            Label{ text:" x = "}
                            SpinBox {
                                id : t1x
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Label{ text:" y = "}
                            SpinBox {
                                id : t1y
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Label{ text:" z = "}
                            SpinBox {
                                id : t1z
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Button{
                                text : "Paste"
                                Layout.fillWidth: true
                                onClicked: {
                                    t1x.value = splineMaker.coordinates[0]
                                    t1y.value = splineMaker.coordinates[1]
                                    t1z.value = splineMaker.coordinates[2]
                                }
                            }
                        }
                    }
                    RowLayout{
                        Label{ text:" T2 : "}
                        RowLayout{
                            Label{ text:" x = "}
                            SpinBox {
                                id : t2x
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Label{ text:" y = "}
                            SpinBox {
                                id : t2y
                                decimals: 0; stepSize: 1; value: 0

                            }
                            Label{ text:" z = "}
                            SpinBox {
                                id : t2z
                                decimals: 0; minimumValue: 0; stepSize: 1; value: 0

                            }
                            Button{
                                text : "Paste"
                                Layout.fillWidth: true
                                onClicked: {
                                    t2x.value = splineMaker.coordinates[0]
                                    t2y.value = splineMaker.coordinates[1]
                                    t2z.value = splineMaker.coordinates[2]
                                }
                            }
                        }
                    }
                }

            }
        }
        GroupBoxNoTitle{
            visible: false
            ColumnLayout{
                anchors.centerIn: parent
                    Button{
                        text: "Create current target file ";
                        onClicked: {
                            myContourDeformation.createLandmarkTargetFile()
                        }
                    }
                    RowLayout{
                        Label{text:"CL element : "}
                        SpinBox {
                            id: currentCLelement;
                            decimals: 0
                            minimumValue: 0
                            maximumValue: 15
                            stepSize: 1
                            value: 0
                        }
                    }
                    RowLayout{
                        Label{text:"DOF : "}
                        SpinBox {
                            id: currentDOF
                            decimals: 0
                            minimumValue: 0
                            maximumValue: 15
                            stepSize: 1
                            value: 0
                        }
                    }
                    Button{
                        text : "Calculate Target Angle"
                        Layout.fillWidth: true
                        onClicked: {
                            console.log("Calling ui_TestTargetAngleComputation for CL Element NO -"+currentCLelement.value+" DOF - "+currentDOF.value  )
                            myContourDeformation.ui_TestTargetAngleComputation(currentCLelement.value, currentDOF.value)
                        }
                    }
            }
        }
    }

    MessageDialog {
        id: messageDialog
        visible: false
        title: "Alert"
        text: " text for message dialog "
        onAccepted: {
        }
    }

    ReposHelp{
        id:reposHelpWindow
    }


    Component.onCompleted: {
        adjustWindowSizeToContent()
    }

    function generateTextForAngleInfo(joint,type,side,angleComputed,deformAngle,min_angle,max_angle){
            var string = "The input angle is with respect to the current position\n"
            switch(joint){
                //Knee

                case 1:
                     if(side==0)
                        var angle = myContourDeformation.ui_getJtAngle("kneel")
                     else
                        var angle = myContourDeformation.ui_getJtAngle("kneer")
                     messageDialog.text = string + "\nThe flexion/extension angle at the knee joint is defined as the angle observed when the diphysis axis of the femur and the dipysis axis of the tibia are projected onto the sagital plane
                                                    \nThe current orientation of the model the angle is "+angle+" degrees.\n Please input an angle by which you want to move the knee joint relative to the current position\nFor example a value of 10 degree shall make the FE angle "+(angle+10)

                     break

               //Ankle

                case 2:
                    if(side==0)
                       var angle = myContourDeformation.ui_getJtAngle("anklel")
                    else
                       var angle = myContourDeformation.ui_getJtAngle("ankler")
                    if(type==0)
                         messageDialog.text = string + "\nThe flexion/extension angle at the ankle joint is defined as the angle observed when the diphysis axis of the tibia and the dipysis axis of the second metatarsal are projected onto the sagital plane
                                                    \nThe current orientation of the model the angle is "+angle+" degrees.\n Please input an angle by which you want to move the ankle joint relative to the current position\nFor example a value of 10 degree shall make the FE angle "+(angle+10)
                    else
                         messageDialog.text = string + "\nThe inversion/aversion angle at the ankle joint is defined as the angle about the axis passing through the anterior point on upper plate of the talus bone and the posterior point on lower plate of the calcenous bone"

                        break

                // Hip

                case 3:
                    if(type==0){
                        if(side==0)
                           var angle = myContourDeformation.ui_getJtAngle("hipl")
                        else
                           var angle = myContourDeformation.ui_getJtAngle("hipr")
                         messageDialog.text = string + " \nThe flexion/extension angle at the hip joint is defined as the angle observed when the diphysis axis of the femur and the axis passing through the sacrum and coccyx of the pelvic griddle are projected onto the sagital plane
                         \nThe current orientation of the model the angle is "+angle+" degrees.\n Please input an angle by which you want to move the hip joint relative to the current position\nFor example a value of 10 degree shall make the FE angle "+(angle+10)
                   }
                   if(type==2)
                        messageDialog.text = string + "The medial/lateral roatation at the hip joint is defined as the angle about the axis passing through the greater trochanter of femur and the center point of the anterior surface of patela"
                    if(type==1)
                        messageDialog.text = string + " \nThe flexion/extension angle at the hip joint is defined as the angle observed when the diphysis axis of the femur and the axis passing through the sacrum and coccyx of the pelvic griddle are projected onto the frontal plane
                        \n Please input an angle by which you want to move the hip joint relative to the current position."

                    break

                // Neck

                case 4:
                    if(type==0){
                        var angle = myContourDeformation.ui_getBrAngle("Neck")
                        messageDialog.text = string + " \nThe flexion/extension angle at the cervical joint is defined as the angle observed between center lower plate of T1 vertebra and the center lower plate of the C2 vertebra projected onto the segital plane.
                      \nThe current orientation of the model the angle is "+angle+" degrees.\n Please input an angle by which you want to move the cervical joint relative to the current position\nFor example a value of 10 degree shall make the FE angle "+(angle+10)
                    }
                    if(type==2)
                        messageDialog.text = string + "The lateral-rotation angle in cervical region is defined as the angle between an axis passing throught the anterior and posterior side of inferior plates of T1 and an axis passing throught the anterior and posterior side of inferior plates of C2 projected onto the lateral plane."
                    if(type==1)
                        messageDialog.text = string + " \nThe flexion/extension angle at the cervical joint is defined as the angle observed between center lower plate of T1 vertebra and the center lower plate of the C2 vertebra projected onto the frontal plane.
                      \n Please input an angle by which you want to move the ankle joint relative to the current position\n"

                    break

                // Elbow

                case 5:
                    if (type == 0){
                        if(side==0)
                           var angle = myContourDeformation.ui_getJtAngle("elbowl")
                        else
                           var angle = myContourDeformation.ui_getJtAngle("elbowr")
                         messageDialog.text = string + " \nThe flexion/extension angle at the elbow joint is defined as the angle observed when the diphysis axis of the radius and the axis passing through the humerus bone are projected onto the sagital plane
                         \nThe current orientation of the model the angle is "+angle+" degrees.\n Please input an angle by which you want to move the elbow joint relative to the current position\nFor example a value of 10 degree shall make the FE angle "+(angle+10)
                    }
                    else
                        messageDialog.text = string + "The pronation/supination angle at the elbow joint is defined as the angle about the axis passing through the diphysis axis of radius bone at it's proximal location and diphysis axis of the ulna bone at its distal location "

                    break

                // Wrist

                case 6:
                    if (type == 0)
                        messageDialog.text = string + "The flexion/extension angle at the wrist joint is defined as the angle between the radius axis and the axis passing through the third distal phalange projected on to the segital plane."

                    break

            }
    }

    function setReposJointLabel(jointNo){
        if(jointNo==-1){
            reposJointLabel.font.bold = false
            jointComboBox.visible = true
            reposJointLabel.text = "Select Joint :"
            return
        }else{
            jointComboBox.visible = false
            reposJointLabel.font.bold = true
            var str = "Joint : "
            switch(jointNo){
                case 0:
                    reposJointLabel.text = str
                    break
                case 1:
                    reposJointLabel.text = str+"Knee"
                    break
                case 2:
                    reposJointLabel.text = str+"Ankle"
                    break
                case 3:
                    reposJointLabel.text = str+"Hip"
                    break
                case 4:
                    reposJointLabel.text = str+"Neck"
                    break
                case 5:
                    reposJointLabel.text = str+"Elbow"
                    break
                case 6:
                    reposJointLabel.text = str+"Wrist"
                    break
                case 7:
                    reposJointLabel.text = str+"Lumbar"
                    break
                case 6:
                    reposJointLabel.text = str+"Thorax"
                    break
                case 6:
                    reposJointLabel.text = str+"Shoulder"
                    break


            }
        }

    }

    function setReposTypeLabel(type){
            if(type==-1){
                reposTypeLabel.font.bold = false
                typeComboBox.visible = true
                reposTypeLabel.text = "Select Type  :"
                return
            }else{
                typeComboBox.visible = false
                reposTypeLabel.font.bold = true
                var str = "Type : "
                var typeList= []
                switch(jointComboBox.currentIndex){
                    case 0:
                        typeComboBox.model = [ "Flexion/Extension" ]
                        break;
                    case 1:
                        typeList = [ "Flexion/Extension", "Inversion/Aversion" ]
                        break;
                    case 2:
                        typeList = [ "Flexion/Extension", "Abduction/Adduction","Internal/External Rotation" ]
                        break;
                    case 3:
                        typeList = [ "Flexion/Extension", "Lateral Rotation", "Lateral Flexion" ]
                        break;
                    case 4:
                        typeList = [ "Flexion/Extension", "Pronation/Supination" ]
                        break;
                    case 5:
                        typeList = [ "Flexion/Extension" ]
                        break;
                    case 6:
                        typeList = [ "Flexion/Extension", "Lateral Flexion"]
                        break;
                    case 7:
                        typeList = [ "Flexion/Extension", "Lateral Flexion", "Twisting" ]
                        break;
                    case 8:
                        typeList = [ "Flexion/Extension", "Abduction/Adduction", "Twisting" ]
                        break;
              }
              if(type===0)
                  reposTypeLabel.text = str
              else{
                  type -= 1
                  reposTypeLabel.text = str+typeList[type]
              }
          }
    }

    function setReposSideLabel(side){
        if(side==-1){
            reposSideLabel.font.bold = false
            sideComboBox.visible = true
            reposSideLabel.text = "Select Side :"
            return
        }else{
            reposSideLabel.font.bold = true
            sideComboBox.visible = false
            var str = "Side : "
            switch(side){
                case 0:
                    reposSideLabel.text = str
                    break
                case 1:
                    reposSideLabel.text = str+"Left"
                    break
                case 2:
                    reposSideLabel.text = str+"Right"
                    break

            }
        }
    }

    function setReposAngleLabel(angleValue){
        if(angleValue==-999){
            angleLabel.font.bold = false
            angle.visible = true
            angleLabel.text = "Angle :"
            return
        }else{
            angleLabel.font.bold = true
            angle.visible = false
            if(angleValue==0)
                angleLabel.text = "Angle : "
            else{
                angleLabel.text = "Angle : "+angleValue.toFixed(2)
            }
        }
    }

    function enableReposParameters(flag){
        jointComboBox.enabled = flag
        sideComboBox.enabled =flag
        typeComboBox.enabled = flag
        angle.enabled = flag
    }

    function checkIfLandmarkTargetsArePresent(){
        var vals = contextMetaManager.getTargetTypeList()
        for (var i=0 ; i<vals.length; i++ )
            if(vals[i] === "Landmark")
                return true

        return false
    }



}


