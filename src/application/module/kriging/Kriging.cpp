/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Kriging.h"

#include <functional>

#include "common/Context.h"

#include "vtkPIPERFilters/vtkSurfaceDistance.h"

#include <vtkPointData.h>
#include <vtkColorTransferFunction.h>


namespace piper {
    namespace kriging {
        using namespace hbm;
        
        Kriging::Kriging()
            : m_kriging(std::make_shared<IntermediateTargetsInterface>()),
            m_krigingdisplay(std::make_shared<KrigingDisplay>())
        {
            m_kriging->setRefHBM(&Context::instance().project().model());
            m_krigingdisplay->init(m_kriging->getKrigingInterfaceEuclidSkin(), m_kriging->getKrigingInterfaceEuclidBones(), &Context::instance().display());

            connect(this, &Kriging::willPerformKriging, this, &Kriging::updateModuleParameters);
            std::function<void(std::string const&)> infoLog = [&](std::string const& msg) { Context::instance().logInfo(msg); };
            std::function<void(std::string const&)> warnLog = [&](std::string const& msg) { Context::instance().logWarning(msg); };
            m_kriging->setMessageFunctions(infoLog, warnLog);
        }

        Kriging::Kriging(std::shared_ptr<kriging::KrigingDisplay> display)
            : m_kriging(std::make_shared<IntermediateTargetsInterface>()),
            m_krigingdisplay(display)
        {
            m_kriging->setRefHBM(&Context::instance().project().model());
            m_krigingdisplay->init(m_kriging->getKrigingInterfaceEuclidSkin(), m_kriging->getKrigingInterfaceEuclidBones(), &Context::instance().display());

            connect(this, &Kriging::willPerformKriging, this, &Kriging::updateModuleParameters);
            std::function<void(std::string const&)> infoLog = [&](std::string const& msg) { Context::instance().logInfo(msg); };
            std::function<void(std::string const&)> warnLog = [&](std::string const& msg) { Context::instance().logWarning(msg); };
            m_kriging->setMessageFunctions(infoLog, warnLog);
        }

        Kriging::~Kriging() {
            m_nodes.clear();
        }

        void Kriging::setOperationSignals(bool nuggetComputed, bool decimationComputed, bool transformedCPs) {
            if (decimationComputed)
                connect(&Context::instance(), &Context::operationFinished, this, &Kriging::isDecimationComputedChanged, Qt::ConnectionType::DirectConnection);
            if (nuggetComputed || decimationComputed) // if decimation is recomputed, nugget will change as well
                connect(&Context::instance(), &Context::operationFinished, this, &Kriging::isNuggetComputedChanged, Qt::ConnectionType::DirectConnection);
            if (transformedCPs)
                connect(&Context::instance(), &Context::operationFinished, this, &Kriging::transformedCPs, Qt::ConnectionType::DirectConnection);
            connect(&Context::instance(), &Context::operationFinished, this, &Kriging::cleanupOperationSignals, Qt::ConnectionType::DirectConnection);
        }

        void Kriging::cleanupOperationSignals() {
            disconnect(&Context::instance(), &Context::operationFinished, this, &Kriging::isNuggetComputedChanged);
            disconnect(&Context::instance(), &Context::operationFinished, this, &Kriging::isDecimationComputedChanged);
            disconnect(&Context::instance(), &Context::operationFinished, this, &Kriging::cleanupOperationSignals);
            disconnect(&Context::instance(), &Context::operationFinished, this, &Kriging::transformedCPs);

        }
        
        void Kriging::updateModuleParameters()
        {
            m_kriging->setGeoDistancePrecision(
                Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_GEODESIC_PRECISION));
            m_kriging->setSplitBoxOverlap(
                Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_OVERLAP));
            m_kriging->setGeodesicDistanceType((SurfaceDistance)Context::instance().project().moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_GEODESIC_TYPE));
            m_kriging->setInterpolateDisplacement(Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT));
            m_kriging->setUseDrift(Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT));
        }

        void Kriging::resetKrigingTargets()
        {
            m_transformedCPs = NULL;
            toggleBonePreview(false);
            toggleSkinPreview(false);
            m_kriging->resetKrigingInterface();
            m_krigingdisplay->setBonePreviewValid(false);
            m_krigingdisplay->setSkinPreviewValid(false);
            emit targetsReset();
            emit isDecimationComputedChanged();
            emit isNuggetComputedChanged();
        }

        void Kriging::resetKrigingSources()
        {
            m_transformedCPs = NULL;
            m_kriging->sourcesReset();
            resetKrigingTargets();
            emit sourcesReset();
        }

        void Kriging::doSetResultinHistory(std::string const& historyName) {
            pInfo() << START << "Save model in history ";
            FEModel& femodel = Context::instance().project().model().fem(); //shortcut
            Context::instance().addNewHistory(QString::fromStdString(historyName));
            m_krigingdisplay->setBonePreviewValid(false);
            m_krigingdisplay->setSkinPreviewValid(false);
            // copy new nodes coordinates in the current model in history
            femodel.getNodes() = m_nodes;
            femodel.updateVTKRepresentation();
            pInfo() << DONE << "Model after Kriging deformation is saved in history as " << Context::instance().currentModel().toStdString();
            //emit Context::instance().modelUpdated();
        }

        void Kriging::setResultinHistory(QString const& historyName) {
            void(*setResultinHistoryWrapper)(piper::kriging::Kriging*, void (piper::kriging::Kriging::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall < piper::kriging::Kriging, void(piper::kriging::Kriging::*)(std::string const&), std::string const& > ;

            setOperationSignals(true, true, m_transformedCPsSend);
            m_transformedCPsSend = false;
            Context::instance().performAsynchronousOperation(std::bind(setResultinHistoryWrapper,
                this, &piper::kriging::Kriging::doSetResultinHistory, historyName.toStdString()),
                false, true, true, false, false);
        }

        void Kriging::krige(bool useKrigingWIntermediates) {
            void(*krigeWrapper)(piper::kriging::Kriging*, void (piper::kriging::Kriging::*)(bool), bool)
                = &piper::wrapClassMethodCall < piper::kriging::Kriging, void (piper::kriging::Kriging::*)(bool) , bool> ;
            Context::instance().performAsynchronousOperation(std::bind(krigeWrapper, this, &piper::kriging::Kriging::doKrige, useKrigingWIntermediates),
                false, false, false, false, false);
        }


        void Kriging::doKrige(bool useKrigingWIntermediates) {
            pInfo() << START << "Applying kriging to the model.";

            HumanBodyModel& hbmref = Context::instance().project().model();
            if (hbmref.empty()){
                pCritical() << QStringLiteral("No model loaded, aborting.");
                return;
            }

            emit willPerformKriging();

            if (useKrigingWIntermediates)
            {
                m_transformedCPs = NULL;
                m_kriging->scaleHBMDenseWIntermediates(&m_nodes,
                    Context::instance().project().moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP),
                    Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT));
            }
            else
            {
                m_transformedCPs = vtkSmartPointer<vtkPoints>::New();
                // copy fem nodes
                m_kriging->scaleSparseAllCPs(&m_nodes,
                    Context::instance().project().moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP), m_transformedCPs);
                m_transformedCPsSend = true;
            }

            pInfo() << DONE;
            emit this->deformed();
        }
        
        void Kriging::toggleSkinPreview(bool visible)
        {
            if (visible) 
                updateModuleParameters();// if kriging parameters has changed, skin might become invalid
            if ((m_kriging->getIsSkinValid() || !visible) && m_krigingdisplay->getSkinPreviewValid())
            {
                m_krigingdisplay->visibleSkinPreview(visible, false, false);
                // turn off nuggets and points - if they are supposed to be turned on, it will be done from the qml right after this call
                if (visible) // don't bother if it's not visible
                {
                    visualizeCtrlPointsOffInterm(true, false);
                    visualizeNuggetsOffInterm(true, false);
                }
                else // if visible, render is called in visualizeNuggetsOffInterm / visualizeCtrlPointsOffInterm
                    Context::instance().display().Render();
            }
            else if (visible)
            {
                void(*transformSkinWrapper)(piper::kriging::Kriging*,
                    void (piper::kriging::Kriging::*)())
                    = &piper::wrapClassMethodCall < piper::kriging::Kriging,
                    void(piper::kriging::Kriging::*)() >;

                setOperationSignals(true, true, false);
                Context::instance().performAsynchronousOperation(std::bind(transformSkinWrapper,
                    this, &piper::kriging::Kriging::transformSkin),
                    false, false, false, false, false);
            }
        }

        void Kriging::transformSkin()
        {
            pInfo() << START << "Creating intermediate target - skin";
            emit willPerformKriging();
            m_kriging->scaleSkin();
            m_krigingdisplay->SetSkinPreview(m_kriging->getScaledSkin());
            m_krigingdisplay->visibleSkinPreview(true, true, false);
            pInfo() << DONE;
        }

        void Kriging::toggleBonePreview(bool visible)
        {
            if (visible)
                updateModuleParameters();// if kriging parameters has changed, bones might become invalid
            if ((m_kriging->getIsBonesValid() || !visible) && m_krigingdisplay->getBonePreviewValid())
            {
                m_krigingdisplay->visibleBonePreview(visible, false, false);
                // turn off nuggets and points - if they are supposed to be turned on, it will be done from the qml right after this call
                if (visible) // don't bother if it's not visible
                {
                    visualizeCtrlPointsOffInterm(false, true);
                    visualizeNuggetsOffInterm(false, true);
                }
                else // if visible, render is called in visualizeNuggetsOffInterm / visualizeCtrlPointsOffInterm
                    Context::instance().display().Render();
            }
            else if (visible)
            {
                void(*transformBonesWrapper)(piper::kriging::Kriging*,
                    void (piper::kriging::Kriging::*)())
                    = &piper::wrapClassMethodCall < piper::kriging::Kriging,
                    void(piper::kriging::Kriging::*)() >;

                setOperationSignals(true, true, false);
                Context::instance().performAsynchronousOperation(std::bind(transformBonesWrapper,
                    this, &piper::kriging::Kriging::transformBones),
                    false, false, false, false, false);
            }
        }

        void Kriging::transformBones()
        {
            pInfo() << START << "Creating intermediate target - bones";
            emit willPerformKriging();
            m_kriging->scaleBones();
            m_krigingdisplay->SetBonePreview(m_kriging->getScaledBones());
            m_krigingdisplay->visibleBonePreview(true, true, false);
            pInfo() << DONE;
        }
        
        void Kriging::setIntermediateKrigingTarget(bool useSkin, bool useBones)
        {
            m_kriging->setUseIntermediateBones(useBones);
            m_kriging->setUseIntermediateSkin(useSkin);
            resetKrigingTargets();
        }

        bool Kriging::useGeodesicSkin()
        {
            return m_kriging->getUseGeodesicSkin();
        }
        
        bool Kriging::decimationUseDisplacement()
        {
            return m_kriging->getDecimationUseDisplacement();
        }

        bool Kriging::decimationUseHomogenous()
        {
            return m_kriging->getDecimationUseHomogenous();
        }

        unsigned int Kriging::decimationGridSizeX()
        {
            return m_kriging->getDecimationGridSize();
        }

        double Kriging::decimationRadius()
        {
            return m_kriging->getDecimationRadius();
        }

        double Kriging::decimationDeviation()
        {
            return m_kriging->getDecimationDeviation();
        }

        double Kriging::nuggetBoneWeight()
        {
            return m_kriging->getNuggetBoneWeight();
        }

        double Kriging::nuggetRadius()
        {
            return m_kriging->getNuggetRadius();
        }


        double Kriging::nuggetScaleSkin()
        {
            return m_kriging->getNuggetScaleSkin();
        }

        double Kriging::nuggetScaleBones()
        {
            return m_kriging->getNuggetScaleBones();
        }

        void Kriging::setNuggetScaleSkin(double scale)
        {
            if (m_kriging->setNuggetScaleSkin(scale))
                emit isNuggetComputedChanged();
        }

        void Kriging::setNuggetScaleBones(double scale)
        {
            if (m_kriging->setNuggetScaleBones(scale))
                emit isNuggetComputedChanged();
        }

        bool Kriging::useDefaultNugget()
        {
            return m_kriging->getUseDefaultNugget();
        }

        bool Kriging::isNuggetComputed()
        {
            return m_kriging->getIsNuggetComputed();
        }

        bool Kriging::isDecimationComputed()
        {
            return m_kriging->getIsDecimationComputed();
        }

        bool Kriging::fixBones()
        {
            return m_kriging->getFixBones();
        }

        void Kriging::setFixBones(bool fix)
        {
            if (m_kriging->setFixBones(fix))
            {
                toggleBonePreview(false);
                emit boneTargetReset();
                emit isNuggetComputedChanged(); // nuggets and decimation are no longer valid
                emit isDecimationComputedChanged();
            }
        }

        bool Kriging::useDomains()
        {
            return m_kriging->getUseIntermediateDomains();
        }

        void Kriging::setUseDomains(bool useDomains)
        {
            if (m_kriging->setUseIntermediateDomains(useDomains))
            {
                emit isNuggetComputedChanged(); // nuggets and decimation are no longer valid
                emit isDecimationComputedChanged();
            }
        }
        
        void Kriging::setDecimationUseDisplacement(bool use)
        {
            if (m_kriging->setDecimationUseDisplacement(use))
            {
                emit isDecimationComputedChanged();
                emit isNuggetComputedChanged(); // if decimation changes, so do nuggets
            }
        }

        void Kriging::setDecimationUseHomogenous(bool use)
        {
            if (m_kriging->setDecimationUseHomogenous(use))
            {
                emit isDecimationComputedChanged();
                emit isNuggetComputedChanged(); // if decimation changes, so do nuggets
            }
        }

        void Kriging::setDecimationGridSizeX(unsigned int sizeX)
        {
            if (m_kriging->setDecimationGridSize(sizeX))
            {
                emit isDecimationComputedChanged();
                emit isNuggetComputedChanged(); // if decimation changes, so do nuggets
            }
        }

        void Kriging::setDecimationRadius(double radius)
        {
            if (m_kriging->setDecimationRadius(radius))
            {
                emit isDecimationComputedChanged();
                emit isNuggetComputedChanged(); // if decimation changes, so do nuggets
            }
        }

        void Kriging::setDecimationDeviation(double deviation)
        {
            if (m_kriging->setDecimationDeviation(deviation))
            {
                emit isDecimationComputedChanged();
                emit isNuggetComputedChanged(); // if decimation changes, so do nuggets
            }
        }

        void Kriging::setNuggetRadius(double radius)
        {
            if (m_kriging->setNuggetRadius(radius))
                emit isNuggetComputedChanged();
        }

        void Kriging::setUseDefaultNugget(bool useDefault)
        {
            m_kriging->setUseDefaultNugget(useDefault);
            emit isNuggetComputedChanged();
        }

        void Kriging::setNuggetBoneWeight(const double &weight)
        {
            if (m_kriging->setNuggetBoneWeight(weight))
                emit isNuggetComputedChanged();
        }

        void Kriging::setUseGeodesicSkin(bool usegeodesic)
        {
            m_kriging->setUseGeodesicSkin(usegeodesic);
        }

        void Kriging::setUseSkinThreshold(double threshold)
        {
            if (m_kriging->setUseSkinThreshold(threshold))
                resetKrigingSources();
        }

        void Kriging::setUseBonesThreshold(double threshold)
        {
            if (m_kriging->setUseBonesThreshold(threshold))
                resetKrigingSources();
        }

        void Kriging::decimateCtrlPoints()
        {
            void(*deciWrapper)(piper::kriging::Kriging*,
                void (piper::kriging::Kriging::*)())
                = &piper::wrapClassMethodCall < piper::kriging::Kriging,
                void(piper::kriging::Kriging::*)() >;

            emit willPerformKriging();
            setOperationSignals(false, true, false);
            Context::instance().performAsynchronousOperation(std::bind(deciWrapper,
                this, &piper::kriging::Kriging::doDecimateCtrlPoints),
                false, false, false, false, false);
        }

        void Kriging::doDecimateCtrlPoints()
        {
            pInfo() << START << "Decimating control points of intermediate targets";
            m_kriging->decimateCtrlPoints();
            pInfo() << DONE;
        }

        void Kriging::computeIntermNuggets()
        {
            void(*nuggetWrapper)(piper::kriging::Kriging*,
                void (piper::kriging::Kriging::*)())
                = &piper::wrapClassMethodCall < piper::kriging::Kriging,
                void(piper::kriging::Kriging::*)() >;

            emit willPerformKriging();
            setOperationSignals(true, true, false);
            Context::instance().performAsynchronousOperation(std::bind(nuggetWrapper,
                this, &piper::kriging::Kriging::doComputeIntermediatesNuggets),
                false, false, false, false, false);
        }

        void Kriging::doComputeIntermediatesNuggets()
        {
            pInfo() << START << "Computing nuggets on intermediate targets";
            m_kriging->computeNuggetsInterm();
            pInfo() << DONE;
        }

        void Kriging::visualizeNuggetsOffInterm(bool skin, bool bones)
        {
            if (skin)
                Context::instance().display().HighlightElementsByScalarsOff(m_krigingdisplay->previewSkinName);
            if (bones)
            {
                auto bonePrevMeshes = m_krigingdisplay->GetBonePreview();
                for (auto &cur : *bonePrevMeshes)
                    Context::instance().display().HighlightElementsByScalarsOff(cur.first);
            }
            Context::instance().display().Render();
        }

        void Kriging::visualizeNuggetsInterm(bool skin, bool bones)
        {
            if (skin)
            {
                vtkSmartPointer<vtkPolyData> skinmesh = m_krigingdisplay->GetSkinPreview();
                if (skinmesh)
                {
                    m_kriging->createNuggetMapSkin(skinmesh);
                    vtkSmartPointer<vtkDoubleArray> map = vtkDoubleArray::SafeDownCast(skinmesh->GetPointData()->GetArray(_PIPER_NUGGET_MAP));
                    if (map)
                    {
                        double minVal = DBL_MAX;
                        for (vtkIdType i = 0; i < map->GetNumberOfTuples(); i++)
                        {
                            if (map->GetValue(i) < minVal)
                                minVal = map->GetValue(i);
                        }
                        pInfo() << INFO << "Visualizing automated nugget on skin intermediate target, highest (negative) nugget: " << minVal;
                        vtkSmartPointer<vtkColorTransferFunction> ctf = vtkSmartPointer<vtkColorTransferFunction>::New();
                        ctf->AddRGBPoint(minVal, 1, 0.3, 0.3); // min (highest variance allowed) -> Red
                        ctf->AddRGBPoint(0, 0.3, 0.3, 1); // 0 -> Blue
                        ctf->AddRGBPoint(1, 1, 1, 1); // points with no nugget white
                        Context::instance().display().UpdateActor(m_krigingdisplay->previewSkinName);
                        Context::instance().display().HighlightElementsByScalars(_PIPER_NUGGET_MAP, true, m_krigingdisplay->previewSkinName, ctf);
                    }
                }
            }
            if (bones)
            {
                vtkSmartPointer<vtkDoubleArray> map = m_kriging->createNuggetMapBones();
                if (map)
                {
                    double minVal = DBL_MAX;
                    for (vtkIdType i = 0; i < map->GetNumberOfTuples(); i++)
                    {
                        if (map->GetValue(i) < minVal)
                            minVal = map->GetValue(i);
                    }
                    vtkSmartPointer<vtkColorTransferFunction> ctf = vtkSmartPointer<vtkColorTransferFunction>::New();
                    ctf->AddRGBPoint(minVal, 1, 0.3, 0.3); // min (highest variance allowed) -> Red
                    ctf->AddRGBPoint(0, 0.3, 0.3, 1); // 0 -> Blue
                    ctf->AddRGBPoint(1, 1, 1, 1); // points with no nugget white
                    pInfo() << INFO << "Visualizing automated nugget on bones intermediate targets, highest (negative) nugget: " << minVal;
                    auto bonePrevMeshes = m_krigingdisplay->GetBonePreview();
                    for (auto &cur : *bonePrevMeshes)
                    {
                        // use SetScalars over AddArray to mark them on the parent unstructured grid 
                        // (cur.second, which is not directly input of the actor's mapper) as active so they stay highlighted even when other array is added through AddArray
                        cur.second->GetPointData()->SetScalars(map);
                        Context::instance().display().UpdateActor(cur.first);
                        Context::instance().display().HighlightElementsByScalars(_PIPER_NUGGET_MAP, true, cur.first, ctf);
                    }
                }
            }
            Context::instance().display().Render();
        }

        void Kriging::visualizeCtrlPointsOffInterm(bool skin, bool bones)
        {
            if (skin)
                Context::instance().display().HighlightPointsOff(m_krigingdisplay->previewSkinName);
            if (bones)
            {
                auto bonePrevMeshes = m_krigingdisplay->GetBonePreview();
                for (auto &cur : *bonePrevMeshes)
                    Context::instance().display().HighlightPointsOff(cur.first);
            }
            Context::instance().display().Render();
        }

        void Kriging::visualizeCtrlPointsInterm(bool skin, bool bones)
        {
            if (skin)
            {
                vtkSmartPointer<vtkPolyData> skinmesh = m_krigingdisplay->GetSkinPreview();
                if (skinmesh)
                {
                    m_kriging->createDecimationMapSkin(skinmesh);
                    Context::instance().display().UpdateActor(m_krigingdisplay->previewSkinName);
                    Context::instance().display().HighlightPoints(m_krigingdisplay->previewSkinName, false,
                        Context::instance().display().POINTHIGHLIGHT_RADIUS / Context::instance().display().POINTHIGHLIGH_SMALL_FACTOR, false, _PIPER_DECIMATION_MAP);
                }
            }
            if (bones)
            {
                vtkSmartPointer<vtkBitArray> map = m_kriging->createDecimationMapBones();
                auto bonePrevMeshes = m_krigingdisplay->GetBonePreview();
                for (auto &cur : *bonePrevMeshes)
                {
                    cur.second->GetPointData()->AddArray(map);
                    Context::instance().display().UpdateActor(cur.first);
                    Context::instance().display().HighlightPoints(cur.first, false,
                        Context::instance().display().POINTHIGHLIGHT_RADIUS / Context::instance().display().POINTHIGHLIGH_SMALL_FACTOR, false, _PIPER_DECIMATION_MAP);
                }
            }
            Context::instance().display().Refresh();
        }
    }
}
