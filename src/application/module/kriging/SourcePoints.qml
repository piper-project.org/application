// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import Qt.labs.folderlistmodel 2.1

import Piper 1.0
import piper.Kriging 1.0

ModuleToolWindow
{
    title: "Source Points"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

    property KrigingModule _kriging
    property int sizeWidth
    property alias tableHeight: controlpoints.tableHeight

    property var rootPath: ""

    Settings {
        id: settings
        category: "krigingSrcPoints"

        property url recentFolder
        property url recentExportFolder
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : itemMargin
        Button {
            text: qsTr("Import Source Points")
            tooltip: qsTr("Select and import source control points file")
            Layout.fillWidth : true
            width: sizeWidth
            action: importControlPointsAction
        }

        ComboTableComponent {
            id: controlpoints
            tableHeight: tableHeight
            totalWidth: sizeWidth
            Layout.fillWidth : true
            internalComboList: _kriging.availableControlPoints
            internalTableModel: _kriging.controlPointsSource
            onAddInTable: _kriging.addControlPointsSource(comboItem)
            onSetCurrentItem: _kriging.setCurrentControlPointSource(tableItem)
            onRemoveCurrentItem: _kriging.removeControlPointsSource(tableItem)
            onHideShowCurrentItem: _kriging.toggleVisibilityCPSource(tableItem)
            onResetKrigingParameters: _kriging.resetCPSetParameters(tableItem)
            onExportPoints: 
            {
                exportPointsDialog.curPointsName = tableItem
                exportPointsDialog.open()
            }
            Connections
            {
                target: _kriging
                onCurrentControlPointsSourceChanged: {
                    controlpoints.indexTable = _kriging.controlPointsSource.get(_kriging.currentControlPoints)
                }
            }
    
            function setAsSkin(item, as_skin)
            {
                _kriging.setAsSkinGlobal(item, as_skin)
            }

            function setAsBones(item, as_bones)
            {
                _kriging.setAsBonesGlobal(item, as_bones)
            }

            function setNugget(item, nugget)
            {
                _kriging.setCPSetNugget(item, nugget)
            }
        }

    }

    ImportControlPointsDialog {
        id: importControlPointsDialog
        onAccepted: {
            settings.recentFolder = importControlPointsDialog.folder;
            _kriging.loadCtrlPtSrc(importControlPointsDialog.selectedFilePath, importControlPointsDialog.name)
        }
        onVisibleChanged:
        {
            if(visible)
            {
                importControlPointsDialog.name = _kriging.getValidControlPointSetName()
                if(context.doFolderExist(settings.recentFolder))
                    importControlPointsDialog.folder = settings.recentFolder;
            }
        }
    }

    Action{
        id: importControlPointsAction
        text: qsTr("Import Control Points")
        onTriggered : importControlPointsDialog.open()
    }

    FileDialog 
    {
        id: exportPointsDialog
        property string curPointsName: ""
        selectExisting: false
        onAccepted: {
            settings.recentExportFolder = folder;
            _kriging.exportControlPointsSource(curPointsName, fileUrl, false);
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentExportFolder))
                    folder = settings.recentExportFolder;
            }
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent(20, 20)
        importControlPointsDialog._kriging = _kriging

        rootPath = "file:/"+context.applicationDirPath
    }
}


