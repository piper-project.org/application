// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQml 2.2

import piper.Kriging 1.0
import Piper 1.0

ModuleToolWindow
{
    title: "Register Surfaces - EXPERIMENTAL"
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin
    id: window
    
    
    property string loadedSource
    property string loadedTarget
    property string loadedLandmarks

    Settings {
        id: settings
        category: "registerSurfaces"

        property url recentSourceFolder
        property url recentTargetFolder
        property url recentExportFolder
        property url recentLandmarksFolder
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : 10
        GroupBox
        {            
            title:"Input"
            Layout.fillWidth : true
            GridLayout
            {
                Layout.fillWidth : true
                Button
                {
                    Layout.fillWidth : true
                    id: buttonLoadSource
                    Layout.row: 0
                    Layout.column : 0
                    action: loadSourceAction
                }
                Label
                {
                    id: labelLoadSource
                    Layout.row: 0
                    Layout.column : 1
                    text: "No source loaded"
                    Connections
                    {
                        target: registrationInterface
                        onSourceLoadedChanged: {
                            if (registrationInterface.sourceLoaded)
                            {
                                labelLoadSource.text = loadedSource
                                if (registrationInterface.targetLoaded)
                                {
                                    registrationInterface.CheckInputValid()
                                }
                                adjustWindowSizeToContent_forFixed()
                            }
                            else
                            {
                                labelLoadSource.text = "No source loaded"
                                registerAction.enabled = false
                                labelMeshCheck.text = ""
                            }
                        }
                    }
                }
                Button
                {
                    Layout.fillWidth : true
                    id: buttonLoadTarget
                    Layout.row: 1
                    Layout.column : 0
                    action: loadTargetAction
                }
                Label
                {
                    id: labelLoadTarget
                    Layout.row: 1
                    Layout.column : 1
                    text: "No target loaded"
                    Connections
                    {
                        target: registrationInterface
                        onTargetLoadedChanged: {
                            if (registrationInterface.targetLoaded)
                            {
                                labelLoadTarget.text = loadedTarget
                                if (registrationInterface.sourceLoaded)
                                {
                                    registrationInterface.CheckInputValid()
                                }
                                adjustWindowSizeToContent_forFixed()
                            }
                            else
                            {
                                labelLoadTarget.text = "No target loaded"
                                registerAction.enabled = false
                                labelMeshCheck.text = ""
                            }
                        }
                    }
                }
                Button
                {
                    Layout.fillWidth : true
                    id: buttonLoadLandmarks
                    Layout.row: 2
                    Layout.column : 0
                    action: loadLandmarksAction
                }
                Label
                {
                    id: labelLoadLandmarks
                    Layout.row: 2
                    Layout.column : 1
                    text: "No landmarks loaded (landmarks are optional)"
                    Connections
                    {
                        target: registrationInterface
                        onSourceLoadedChanged: {
                            labelLoadLandmarks.text = "No landmarks loaded (landmarks are optional)" // whenever source is reloaded, landmarks are reset
                            if (registrationInterface.sourceLoaded)
                            {
                                loadLandmarksAction.enabled = true
                            }
                            else
                            {
                                loadLandmarksAction.enabled = false
                            }
                            adjustWindowSizeToContent_forFixed()
                        }
                        onLandmarksLoadedChanged: {
                            if (registrationInterface.landmarksLoaded)
                            {
                                labelLoadLandmarks.text = loadedLandmarks
                            }
                            else
                            {
                                labelLoadLandmarks.text = "No landmarks loaded (landmarks are optional)"
                            }
                            adjustWindowSizeToContent_forFixed()
                        }
                    }
                }
            }
        }
        GroupBox
        {            
            title:"Parameters"
            Layout.fillWidth : true
            GridLayout
            {
                Layout.fillWidth : true
                Label
                {
                    Layout.row: 0
                    Layout.column : 0
                    text: "Number of iterations:"
                }
                TextField
                {
                    id: textFieldNoIter
                    Layout.row: 0
                    Layout.column : 1
                    validator: IntValidator {bottom: 1; }
                    font.pointSize : 9
                    horizontalAlignment : Text.AlignRight
                    Layout.fillWidth : true
                    onTextChanged: {
                        if (acceptableInput)
                            registrationInterface.SetNoIterations(parseInt(text))
                        else
                            text = "20"
                    }
                }
                Label
                {
                    Layout.row: 1
                    Layout.column : 0
                    text: "Curvature precision (recommended < 10):"
                }
                TextField
                {
                    id: textFieldHistBuckets
                    Layout.row: 1
                    Layout.column : 1
                    validator: IntValidator {bottom: 2; }
                    font.pointSize : 9
                    horizontalAlignment : Text.AlignRight
                    Layout.fillWidth : true
                    onTextChanged: {
                        if (acceptableInput)
                            registrationInterface.SetHistBuckets(parseInt(text))
                        else
                            text = "5"
                    }
                }
                Label
                {
                    Layout.row: 2
                    Layout.column : 0
                    text: "Post-process smoothing iterations:"
                }
                TextField
                {
                    id: textFieldPostProcess
                    Layout.fillWidth : true
                    Layout.row: 2
                    Layout.column : 1
                    validator: IntValidator {bottom: 0; }
                    font.pointSize : 9
                    horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            registrationInterface.SetPostProcessSmoothing(parseInt(text))
                        else
                            text = "2"
                    }
                }
                Label {
                    Layout.alignment: Qt.AlignLeft
                    Layout.row: 3
                    Layout.column: 0
                    text: "Rigid registration only"
                }
                Switch
                {
                    Layout.row: 3
                    Layout.column: 1
                    checked: false
                    onCheckedChanged: {
                        registrationInterface.SetRigidRegistration(checked)
                        if (checked)
                            textFieldPostProcess.enabled = false
                        else
                            textFieldPostProcess.enabled = true
                    }
                }
            }
        }
        Button
        {
            id: buttonRegister
            Layout.fillWidth: true
            action: registerAction
            Connections
            {
                target: registrationInterface
                onMeshesCompatibleChanged: {
                    if (registrationInterface.meshesCompatible)
                        registerAction.enabled = true
                    else
                        registerAction.enabled = false
                }
            }
        }
    }

    Item
    {
        RegisterSurfacesIntrfc 
        {
            id: registrationInterface
        }
    }

    Component.onCompleted: {
        textFieldHistBuckets.text = "5"
        textFieldNoIter.text = "20"
        textFieldPostProcess.text = "2"
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed()
    }

    Action{
        id: loadSourceAction
        text: qsTr("Load Source Mesh")
        onTriggered : loadSourceDialog.open()
    }

    FileDialog 
    {
        id: loadSourceDialog
        title: "Load Source Mesh"
        nameFilters: [ "Alias Wavefront Object (*.obj)", "All files (*)" ]
        onAccepted: {
            settings.recentSourceFolder = folder;
            registrationInterface.LoadSourceOBJ(fileUrl); //TODO - implement
            loadedSource = fileUrl
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentSourceFolder))
                    folder = settings.recentSourceFolder;
            }
        }
    }

    Action{
        id: loadTargetAction
        text: qsTr("Load Target Mesh")
        onTriggered : loadTargetDialog.open()
    }

    FileDialog 
    {
        id: loadTargetDialog
        title: "Load Target Mesh"
        nameFilters: [ "Alias Wavefront Object (*.obj)", "All files (*)" ]
        onAccepted: {
            settings.recentTargetFolder = folder;
            registrationInterface.LoadTargetOBJ(fileUrl); //TODO - implement
            loadedTarget = fileUrl
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentTargetFolder))
                    folder = settings.recentTargetFolder;
            }
        }
    }

    Action{
        id: loadLandmarksAction
        enabled: false
        text: qsTr("Load Landmarks")
        onTriggered : loadLandmarksDialog.open()
    }

    FileDialog 
    {
        id: loadLandmarksDialog
        title: "Load Landmarks"
        nameFilters: [ "All files (*), Text file (*.txt)" ]
        onAccepted: {
            settings.recentLandmarksFolder = folder;
            registrationInterface.LoadSourceLandmarks(fileUrl);
            loadedLandmarks = fileUrl
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentLandmarksFolder))
                    folder = settings.recentLandmarksFolder;
            }
        }
    }

    Action{
        id: registerAction
        enabled: false
        text: qsTr("Register Meshes")
        onTriggered : registerDialog.open()
    }

    FileDialog 
    {
        id: registerDialog
        title: "Export registered mesh as..."
        nameFilters: [ "Alias Wavefront Object (*.obj)", "All files (*)" ]
        selectExisting: false
        onAccepted: {
            settings.recentExportFolder = folder;
            registrationInterface.PerformRegistration(fileUrl) // TODO - implement
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentExportFolder))
                    folder = settings.recentExportFolder;
            }
        }
    }
}


