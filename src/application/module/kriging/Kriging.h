/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef _PIPER_KRIGING_H
#define _PIPER_KRIGING_H

#include "common/KrigingDisplay.h"
#include "kriging/IntermediateTargetsInterface.h"

#include <QtCore>

namespace piper {
namespace kriging {
    
    /// <summary>
    /// GUI interface for transformation by kriging
    /// </summary>
    /// <author>
    /// Thomas Dupeux, Tomas Janak
    /// </author>
    /// <seealso cref="QObject" />
    class Kriging : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(bool useGeodesicSkin READ useGeodesicSkin WRITE setUseGeodesicSkin NOTIFY useGeodesicSkinChanged)
            Q_PROPERTY(double nuggetBoneWeight READ nuggetBoneWeight WRITE setNuggetBoneWeight NOTIFY nuggetBoneWeightChanged)
            Q_PROPERTY(double nuggetScaleSkin READ nuggetScaleSkin WRITE setNuggetScaleSkin NOTIFY nuggetScaleSkinChanged)
            Q_PROPERTY(double nuggetScaleBones READ nuggetScaleBones WRITE setNuggetScaleBones NOTIFY nuggetScaleBonesChanged)
            Q_PROPERTY(bool useDefaultNugget READ useDefaultNugget WRITE setUseDefaultNugget NOTIFY useDefaultNuggetChanged)
            Q_PROPERTY(bool isNuggetComputed READ isNuggetComputed NOTIFY isNuggetComputedChanged)
            Q_PROPERTY(bool isDecimationComputed READ isDecimationComputed NOTIFY isDecimationComputedChanged)
            Q_PROPERTY(bool decimationUseDisplacement READ decimationUseDisplacement WRITE setDecimationUseDisplacement NOTIFY decimationUseDisplacementChanged)
            Q_PROPERTY(bool decimationUseHomogenous READ decimationUseHomogenous WRITE setDecimationUseHomogenous NOTIFY decimationUseHomogenousChanged)
            Q_PROPERTY(double decimationRadius READ decimationRadius WRITE setDecimationRadius NOTIFY decimationRadiusChanged)
            Q_PROPERTY(double decimationDeviation READ decimationDeviation WRITE setDecimationDeviation NOTIFY decimationDeviationChanged)
            Q_PROPERTY(unsigned int decimationGridSizeX READ decimationGridSizeX WRITE setDecimationGridSizeX NOTIFY decimationGridSizeXChanged)
            Q_PROPERTY(double nuggetRadius READ nuggetRadius WRITE setNuggetRadius NOTIFY nuggetRadiusChanged)
            Q_PROPERTY(bool fixBones READ fixBones WRITE setFixBones NOTIFY fixBonesChanged)
            Q_PROPERTY(bool useDomains READ useDomains WRITE setUseDomains NOTIFY useDomainsChanged)

    public:
        Kriging();        
        /// <summary>
        /// Initializes a new instance of the <see cref="Kriging"/> class.
        /// </summary>
        /// <param name="display">The display component that will be used to display previews.</param>
        Kriging(std::shared_ptr<kriging::KrigingDisplay> display);
        ~Kriging();
        
        /// <summary>
        /// Sets the display component for visualizing kriging related actors.
        /// </summary>
        /// <param name="display">The display component that will be used to display previews.</param>
        void setDisplay(std::shared_ptr<kriging::KrigingDisplay> display) { this->m_krigingdisplay = display; }
        std::shared_ptr<kriging::KrigingDisplay> getDisplay() { return m_krigingdisplay; }
        
        /// <summary>
        /// After a transformation is finished, this will contain transformed control points, BUT only works for the kriging without intermediate targets.
        /// </summary>
        /// <returns>Transformed control points in the same order as they were provided to the kriging interface.</returns>
        vtkSmartPointer<vtkPoints> getTransformedCPs() { return m_transformedCPs; }

        /// <summary>
        /// Performs kriging transformation of the HBM.
        /// </summary>
        /// <param name="useKrigingWIntermediates">If set to <c>true</c>, intermediate targets (bones and skin) will be used for kriging,
        /// i.e. first skin and bones will be transformed separately and then used as a target to do transform the entire mesh.</param>
        Q_INVOKABLE void krige(bool useKrigingWIntermediates);
        Q_INVOKABLE void setResultinHistory(QString const& historyName);
        
        /// <summary>
        /// Sets whether bones or skin or both should be used as itermediate targets. Relevant only when "kriging with intermediate targets" is used.
        /// </summary>
        /// <param name="useSkin">If set to <c>true</c>, skin will be used as an intermediate target. If set to <c>false</c> and
        /// useBones is also set to <c>false</c>, both will be used as a target, i.e. the option will be ignored, 
        /// since at least something has to be used as a target. If useKrigingWIntermediates is <c>false</c>, this option is ignored.</param>
        /// <param name="useBones">If set to <c>true</c>, bones will be used as a target for smart kriging. If set to <c>false</c> and
        /// useSkin is also set to <c>false</c>, both will be used as a target, i.e. the option will be ignored, 
        /// since at least something has to be used as a target. If useKrigingWIntermediates is <c>false</c>, this option is ignored.</param>
        Q_INVOKABLE void setIntermediateKrigingTarget(bool useSkin, bool useBones);
        // visualizes nuggets on the preview meshes. the meshes must be created before calling this, otherwise it will have no effect
        Q_INVOKABLE void visualizeNuggetsInterm(bool skin, bool bones);
        Q_INVOKABLE void visualizeNuggetsOffInterm(bool skin, bool bones);
        Q_INVOKABLE void visualizeCtrlPointsInterm(bool skin, bool bones);
        Q_INVOKABLE void visualizeCtrlPointsOffInterm(bool skin, bool bones);
        Q_INVOKABLE void toggleSkinPreview(bool visible);
        Q_INVOKABLE void toggleBonePreview(bool visible);
        Q_INVOKABLE void setUseGeodesicSkin(bool geoSkin);
        Q_INVOKABLE void setDecimationRadius(double radius);
        Q_INVOKABLE void setDecimationDeviation(double deviation);
        Q_INVOKABLE void setDecimationUseDisplacement(bool use);
        Q_INVOKABLE void setDecimationUseHomogenous(bool use);
        Q_INVOKABLE void setDecimationGridSizeX(unsigned int sizeX);
        Q_INVOKABLE void setNuggetRadius(double radius);        
        Q_INVOKABLE void setNuggetScaleSkin(double scale);
        Q_INVOKABLE void setNuggetScaleBones(double scale);
        Q_INVOKABLE void setUseDefaultNugget(bool useDefault);
        Q_INVOKABLE void computeIntermNuggets();
        Q_INVOKABLE void decimateCtrlPoints();
        Q_INVOKABLE void setNuggetBoneWeight(const double &weight);
        Q_INVOKABLE void setUseSkinThreshold(double threshold);
        Q_INVOKABLE void setUseBonesThreshold(double threshold);
        Q_INVOKABLE void setFixBones(bool fix);
        Q_INVOKABLE void setUseDomains(bool useDomains);

        Q_INVOKABLE double getUseSkinThreshold() { return m_kriging->getUseSkinThreshold();}
        Q_INVOKABLE double getUseBonesThreshold() { return m_kriging->getUseBonesThreshold(); }
                
        /// <summary>
        /// Returns the IntermediateTargetsInterface that is responsible of all kriging transformations.
        /// </summary>
        /// <returns>Reference to the interface.</returns>
        std::shared_ptr<IntermediateTargetsInterface> getKrigingInterface() { return m_kriging; }
        
    public slots:
        bool useGeodesicSkin();
        double nuggetBoneWeight();
        double decimationRadius();
        double decimationDeviation();
        bool decimationUseDisplacement();
        bool decimationUseHomogenous();
        unsigned int decimationGridSizeX();
        double nuggetRadius();
        double nuggetScaleSkin();
        double nuggetScaleBones();
        bool useDefaultNugget();
        bool isNuggetComputed();
        bool isDecimationComputed();
        bool fixBones();
        bool useDomains();
        // invalidates the anthropoModel's kriging interface, should be called if target is changed in some way (anthropoModel is not a Qobject so it can't have the slot itself)
        void resetKrigingTargets();
        // resets the kriging interface including the source control points, calls resetKrigingInterface in the end
        void resetKrigingSources();
        void updateModuleParameters();

    signals:
                        
        // this signal must always be emitteded right before any kriging transformation by the interface (getKrigingInterface) is done. 
        // you can use this to hook up functions that update control points etc. from outside of the Kriging class if needed.
        void willPerformKriging();
        void deformed();
        

        /// <summary>
        /// This signal will be emitted after the model has been deformed along with source control points
        /// </summary>
        /// <seealso cref=getTransformedCPs />        
        void transformedCPs();

        void useGeodesicSkinChanged();
        void nuggetBoneWeightChanged();
        void decimationRadiusChanged();
        void decimationDeviationChanged();
        void decimationUseDisplacementChanged();
        void decimationUseHomogenousChanged();
        void decimationGridSizeXChanged();
        void nuggetRadiusChanged();
        void useDefaultNuggetChanged();
        void nuggetScaleSkinChanged();
        void nuggetScaleBonesChanged();
        void fixBonesChanged();
        void targetsReset();
        void boneTargetReset();
        void sourcesReset();
        void isDecimationComputedChanged();
        void isNuggetComputedChanged();
        void useDomainsChanged();
    private:
        /// <summary>
        /// Calls the scaleSkin of anthropomodel and adds the result as a preview to the associated anthropo display.
        /// </summary>
        void transformSkin();

        /// <summary>
        /// Calls the scaleBones of anthropomodel and adds the result as a preview to the associated anthropo display.
        /// </summary>
        void transformBones();
        void doComputeIntermediatesNuggets();
        void doDecimateCtrlPoints();
        void setOperationSignals(bool nuggetComputed, bool decimationComputed, bool transformedCPs);
        void cleanupOperationSignals();

        std::shared_ptr<IntermediateTargetsInterface> m_kriging;
        piper::hbm::Nodes m_nodes;
        vtkSmartPointer<vtkPoints> m_transformedCPs = NULL; // after transformation will contain transformed control points
        std::shared_ptr<kriging::KrigingDisplay> m_krigingdisplay;

        void doKrige(bool useKrigingWIntermediates);
        void doSetResultinHistory(std::string const& historyName);

        // if kriging deformed CPs, we need to send the transformedCPs signal - but do it only if the model ends up being stored in history
        bool m_transformedCPsSend = false; 
    };


}
} // namespace piper


#endif
