/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef _PIPER_REGISTERSURFACES_H
#define _PIPER_REGISTERSURFACES_H

#include "hbm/HumanBodyModel.h"
#include "vtkPIPERFilters/vtkSurfaceRegistrationPIPER.h"

#include "common/VtkErrorObserver.h"

#include <QtCore>

namespace piper {

// For now registration is included as a tool to facilitate kriging -> part of the kriging namespace. 
// But perhaps if there are more applications for it and it is more developed it could be a module of its own.
namespace kriging {
    
    /// <summary>
    /// GUI interface for surface registration
    /// </summary>
    /// <author>
    /// Tomas Janak
    /// </author>
    class RegisterSurfaces : public QObject
    {
        Q_OBJECT

            Q_PROPERTY(bool meshesCompatible READ meshesCompatible NOTIFY meshesCompatibleChanged)
            Q_PROPERTY(bool sourceLoaded READ sourceLoaded NOTIFY sourceLoadedChanged)
            Q_PROPERTY(bool targetLoaded READ targetLoaded NOTIFY targetLoadedChanged)
            Q_PROPERTY(bool landmarksLoaded READ landmarksLoaded NOTIFY landmarksLoadedChanged)

    public:
        RegisterSurfaces();
        ~RegisterSurfaces();

        /// <summary>
        /// Loads the source mesh and notifies the GUI with info about the mesh if loading was succesful.
        /// If the target mesh was already loaded, performs check if they are compatible (i.e. have similar number of vertices) and notifies GUI.
        /// </summary>
        /// <param name="filepath">Full path to the mesh in obj format.</param>
        Q_INVOKABLE void LoadSourceOBJ(QUrl const& filepath);
        
        /// <summary>
        /// Loads the landmarks for the source mesh. The source mesh must already be set, otherwise will do nothing.
        /// </summary>
        /// <param name="filepath">Path to a text file with lines formatted as "IDsource IDtarget". For each
        /// point in the source mesh with id IDsource, the point with IDtarget from the target mesh
        /// will be always matched as the closest. Lines with IDsource higher than the number of points in the mesh will be ignored.</param>
        Q_INVOKABLE void LoadSourceLandmarks(QUrl const& filepath);
        
        /// <summary>
        /// Loads the target mesh and notifies the GUI with info about the mesh if loading was succesful.
        /// If the source mesh was already loaded, performs check if they are compatible (i.e. have similar number of vertices) and notifies GUI.
        /// </summary>
        /// <param name="filepath">Full path to the mesh in obj format.</param>
        Q_INVOKABLE void LoadTargetOBJ(QUrl const& filepath);

        /// <summary>
        /// Performs the registration on the previously loaded meshes (<seealso cref="loadSourceMesh" /> <seealso cref="loadTargetMesh" />).
        /// Writes the result as obj mesh to a specified file.
        /// </summary>
        /// <param name="filepath">Full path to the file to be used to write the result.</param>
        Q_INVOKABLE void PerformRegistration(QUrl const& filepath);

        // checks if both source and target are loaded and are compatible for registration
        // sets the meshesCompatible and sends the meshesCompatibleChanged signal when done
        Q_INVOKABLE void CheckInputValid();
        
        /// <summary>
        /// Sets the number of iterations done by the registration filter.
        /// </summary>
        /// <param name="noIter">Number of iterations for the registration process.</param>
        Q_INVOKABLE void SetNoIterations(int noIter);

        /// <summary>
        /// Number of histogram buckets used for the surface descriptors of the registration filter.
        /// </summary>
        /// <param name="noBuckets">Must be at least 2.</param>
        Q_INVOKABLE void SetHistBuckets(int noBuckets);

        /// <summary>
        /// Sets if the registration should be rigid or non-rigid.
        /// </summary>
        /// <param name="useRigid">If set to true, registration will not defrom the source mesh, only rigidly transform.</param>
        Q_INVOKABLE void SetRigidRegistration(bool useRigid);

        /// <summary>
        /// The registered mesh is smoothed after each iteration of the registration process, this parameter says how much. Only for non-rigid registration.
        /// </summary>
        /// <param name="noIter">Number of iterations of the vtkWindowedSincPolyDataFilter applied after each registration iteration.</param>
        Q_INVOKABLE void SetPostProcessSmoothing(int noIter);
        
    public slots:
        bool meshesCompatible();
        bool sourceLoaded();
        bool targetLoaded();
        bool landmarksLoaded();

    signals:

        void meshesCompatibleChanged();
        void sourceLoadedChanged();
        void targetLoadedChanged();
        void landmarksLoadedChanged();

    private:
                
        void doLoadSourceOBJ(std::string const& filepath);
        void doLoadSourceLandmarks(std::string const& filepath);
        // port = 0 == source mesh, port = 1 == target mesh
        // requires (port == 0 || port == 1)
        void doLoadOBJMesh(std::string const& filepath, int port);
        void doPerformRegistration(std::string const& filepath);
        
        vtkSmartPointer<vtkSurfaceRegistrationPIPER> m_registration;
        vtkSmartPointer<VtkErrorObserver>  m_errorObserver;

        vtkIdType inputNpts[3]; // index 0 for source, 1 for target, 2 for landmarks. Contains number of points or 0 if the meshes have not been loaded
        bool areMeshesCompatible = false;
    };


}
} // namespace piper


#endif
