/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef _PIPER_KRIGINGMODULE_H
#define _PIPER_KRIGINGMODULE_H

#include "hbm/HumanBodyModel.h"
#include "common/KrigingDisplay.h"
#include "kriging/IntermediateTargetsInterface.h"
#include "Kriging.h"

#include <QtCore>

namespace piper {
namespace kriging {

    // default values for the as_skin/as_bones global parameters
    static const double KRIGMODULE_DEF_AS_BONESGLOBAL = 1;
    static const double KRIGMODULE_DEF_AS_SKINGLOBAL = 0;

    struct ModeItemSource {      

        /// <summary>
        /// Initializes a new record of a control point set.
        /// </summary>
        /// <param name="name">The name of the control point set.</param>
        /// <param name="n">The number of control points.</param>
        /// <param name="as_bones">The association parameter for bones, 0-1. Use negative value to express that per-point values are used - this global value will be ignored.</param>
        /// <param name="as_skin">The association parameter for skin, 0-1. Use negative value to express that per-point values are used - this global value will be ignored.</param>
        /// <param name="nugget">The global nugget for the set. Must be negative if valid, NaN if not defined,
        /// or positive if it should not be used - when per-point nuggets are defined for the set.</param>
        ModeItemSource(QString const& name, size_t const& n, double as_bones, double as_skin, double nugget)
            : m_name(name), m_nb(n), m_as_bones(as_bones), m_as_skin(as_skin), m_nugget(nugget) {}
        QString m_name;
        size_t m_nb;
        double m_as_skin, m_as_bones, m_nugget;
    };

    class ModelTableSource : public QAbstractListModel
    {
        Q_OBJECT
    public:
        ModelTableSource(QObject *parent = 0) : QAbstractListModel(parent) {}
        ModelTableSource(std::map<std::string, ModeItemSource> const& listnames, QObject *parent = 0);
        //implement abstract method of QAbstractTableModel
        Q_INVOKABLE QVariant data(const QModelIndex &index, int role) const;
        Q_INVOKABLE QString const get(int index) const { return items.at(index).m_name; }
        Q_INVOKABLE QVariant const get(QString const& value) const;
        int rowCount(const QModelIndex &parent) const;
        int columnCount(const QModelIndex &parent) const;
        QHash<int, QByteArray> roleNames() const;
        enum Roles {
            NameRole = Qt::UserRole + 1,
            NbRole,
            AsSkinRole,
            AsBonesRole,
            Nugget
        };
        bool setData(const QModelIndex &index, const QVariant &value, int role);
        Q_INVOKABLE bool setData(int row, int column, const QVariant value)
        {
            int role = Qt::UserRole + 1 + column;
            return setData(index(row, 0), value, role);
        }
        Qt::ItemFlags flags(const QModelIndex &index) const;

        Q_INVOKABLE QVariant getAsSkin(const QVariant &index) const;
        Q_INVOKABLE QVariant getAsBones(const QVariant &index) const;
        Q_INVOKABLE QVariant getNugget(const QVariant &index) const;


        void construct(std::map<std::string, ModeItemSource> const& listnames);

        void clear();
        //private:
        QList<ModeItemSource> items;
    };

    struct ModeItemTarget {
        ModeItemTarget(QString const& name, size_t const& n) : m_name(name), m_nb(n) {}
        ModeItemTarget(QString const& name, size_t const& n, QString const& nameassoc) : m_name(name), m_nb(n), m_assoc(nameassoc) {}
        QString m_name;
        size_t m_nb;
        QString m_assoc;
    };

    class ModelTableTarget : public QAbstractListModel
    {
        Q_OBJECT
    public:
        ModelTableTarget(QObject *parent = 0) : QAbstractListModel(parent) {}
        ModelTableTarget(std::map<std::string, size_t> const& listnames, std::map<std::string, std::string> const& list_assoc, QObject *parent = 0);
        //implement abstract method of QAbstractTableModel
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        Q_INVOKABLE QString const get(int index) const { return items.at(index)->m_name; }
        Q_INVOKABLE QVariant const get(QString const& value) const;
        Q_INVOKABLE QString const getSource(int index) const;
        int rowCount(const QModelIndex &parent) const;
        int columnCount(const QModelIndex &parent) const;
        QHash<int, QByteArray> roleNames() const;
        enum Roles {
            NameRole = Qt::UserRole + 1,
            NbRole,
            AssocRole,
        };
        bool setData(const QModelIndex &index, const QVariant &value, int role);
        Q_INVOKABLE bool setData(int row, int column, const QVariant value)
        {
            int role = Qt::UserRole + 1 + column;
            return setData(index(row, 0), value, role);
        }
        Qt::ItemFlags flags(const QModelIndex &index) const;


        void construct(std::map<std::string, size_t> const& listnames, std::map<std::string, std::string> const& list_assoc);

        void clear();
        //private:
        QList<ModeItemTarget*> items;
    };

    /// <summary>
    /// GUI interface for transformation by kriging
    /// </summary>
    /// <author>
    /// Thomas Dupeux, Tomas Janak
    /// </author>
    /// <seealso cref="QObject" />
    class KrigingModule : public QObject
    {
        Q_OBJECT
            Q_PROPERTY(QString currentControlPoints READ currentControlPoints NOTIFY currentControlPointsSourceChanged)
            Q_PROPERTY(QStringList availableControlPoints READ availableControlPoints NOTIFY availableControlPointsChanged)
            Q_PROPERTY(ModelTableSource* controlPointsSource READ controlPointsSource NOTIFY controlPointsSourceChanged)

            Q_PROPERTY(QStringList currentListSourcePoints READ currentListSourcePoints NOTIFY currentListSourcePointsChanged)

            Q_PROPERTY(QString currentTargetPoints READ currentTargetPoints NOTIFY currentTargetPointsChanged)
            Q_PROPERTY(QStringList availableTargetPoints READ availableTargetPoints NOTIFY availableTargetPointsChanged)
            Q_PROPERTY(ModelTableTarget* targetPoints READ targetPoints NOTIFY targetPointsChanged)


            Q_PROPERTY(piper::kriging::Kriging* kriging READ kriging)

    public:
        KrigingModule();
        ~KrigingModule();
        
        Q_INVOKABLE void loadCtrlPtSrc(QUrl filename, QString const& name);
        Q_INVOKABLE void loadTargetPoint(QUrl filename, QString const& name);

        Q_INVOKABLE QString currentControlPoints() const;
        Q_INVOKABLE QStringList availableControlPoints() const;
        Q_INVOKABLE ModelTableSource* controlPointsSource() const;
        Q_INVOKABLE void addControlPointsSource(QString const& name);
        Q_INVOKABLE void removeControlPointsSource(QString const& name);
        Q_INVOKABLE void setCurrentControlPointSource(QString const& name);
        Q_INVOKABLE void toggleVisibilityCPSource(QString const& name);

        Q_INVOKABLE QStringList currentListSourcePoints() const;

        Q_INVOKABLE QString currentTargetPoints() const;
        Q_INVOKABLE QStringList availableTargetPoints() const;
        Q_INVOKABLE ModelTableTarget* targetPoints() const;
        Q_INVOKABLE void addTargetPoints(QString const& name);
        Q_INVOKABLE void removeTargetPoints(QString const& name);
        Q_INVOKABLE void toggleVisibilityCPTarget(QString const& name);
        Q_INVOKABLE void setCurrentTargetPoints(QString const& name);
        // sets the as_skin, as_bones and nugget to default values (for nugget, that means to use the global nugget set through Module Parameters)
        Q_INVOKABLE void resetCPSetParameters(QString const& name);

        Q_INVOKABLE QString getValidControlPointSetName() const;
        Q_INVOKABLE QString getValidTargetPointName() const;

        Q_INVOKABLE void setAssoSourceTarget(QString const& target, QString const& source);
        Q_INVOKABLE void removeAssoSourceTarget(QString const& target);

        //Q_INVOKABLE void InitModuleDisplayWithEmptyModel();
        Q_INVOKABLE void InitModuleData(QString const& initdata = "none");
        Q_INVOKABLE void generateTargetPoints(QUrl  scriptpath);
                
        /// <summary>
        /// Exports the specified source control points set to a file.
        /// </summary>
        /// <param name="controlPointName">The name of the control point set. If it is empty, the current control points will be exported. 
        /// If no control points are current, nothing will be exported.</param>
        /// <param name="filepath">The path to the file to which the control points will be written.</param>
        /// <param name="exportIDs">If set to <c>true</c>, the file will be in a four column format - ID in first, coordinates in the next three.
        /// If this is set to <c>false</c>, there will be only coordinates. If a control point set has both FE nodes AND free nodes as control points,
        /// the free nodes will be assigned ascending IDs starting from the max+1, where max is the maximal ID among those of the FE nodes.
        /// If there are only free nodes, they will be assigned IDs starting from 1 (not 0).</param>
        Q_INVOKABLE void exportControlPointsSource(QString const& controlPointName, QUrl const& filepath, bool exportIDs);

        /// <summary>
        /// Exports the specified target control points set to a file.
        /// </summary>
        /// <param name="controlPointName">The name of the control point set. If it is empty, nothing will be exported.</param>
        /// <param name="filepath">The path to the file to which the control points will be written.</param>
        /// <param name="exportIDs">If set to <c>true</c>, the file will be in a four column format - ID in first, coordinates in the next three.
        /// The nodes will be assigned IDs starting from 1 (not 0).</param>
        Q_INVOKABLE void exportControlPointsTarget(QString const& controlPointName, QUrl const& filepath, bool exportIDs);
        void doGenerateTargetPoints(std::string const& scriptpath);

        Q_INVOKABLE void setAsSkinGlobal(QString const& name, QVariant const& value);
        Q_INVOKABLE void setAsBonesGlobal(QString const& name, QVariant const& value);
        Q_INVOKABLE void setCPSetNugget(QString const& name, QVariant const& value);
        
        /// <summary>
        /// Builds the table data for filling the GUI component with current control points sets. 
        /// The construction must be done withing the main thread -> use only these two methods triggered by appropriate signals.
        /// </summary>
        void reconstructSourceTable();
        void reconstructTargetTable();
        
        void doLoadCtrlPtSrc(std::string const& filename, std::string const& name);
        void doLoadTargetPoint(std::string const& filename, std::string const& name);
        void setOperationSignals(bool initModuleGUI, bool sourcePointsTableUpdated);


    public slots:
        void updateTargets();
        void reset();
        kriging::Kriging *kriging();

    signals:

        void availableControlPointsChanged();
        void controlPointsSourceChanged();
        void currentControlPointsSourceChanged();

        void currentListSourcePointsChanged();
        
        void availableTargetPointsChanged();
        void targetPointsChanged();
        void currentTargetPointsChanged();

    private:
        Kriging m_kriging;
        
        ModelTableSource* model_controlPointsSource;
        std::map<std::string, ModeItemSource> m_controlPointsSource;
        std::string m_currentControlPointSource;

        ModelTableTarget* model_targetPoints;
        std::map<std::string, size_t> m_targetPoints;
        std::string m_currentTargetPoints;

        std::map<std::string, std::string> m_assoSourceTargetPoints;
        
        void setUpKrigingSystem();
        void checkControlsourcePoints();
        void checkTargetPoints();

        void collectControlPoints(piper::hbm::InteractionControlPoint& source, piper::hbm::InteractionControlPoint& target) const;
        // loads the deformed control points from the kriging interface and create new set of control points that are the deformed ones
        void createDeformedCPs();
        void initModuleGUI();
        void exportControlPointsSource(std::string const& controlPointSource, QString const& filepath);
    };


}
} // namespace piper

Q_DECLARE_METATYPE(piper::kriging::ModelTableSource*)
Q_DECLARE_METATYPE(piper::kriging::ModelTableTarget*)


#endif
