// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import Piper 1.0
import piper.Kriging 1.0

ModuleToolWindow
{
    title: "Kriging Deformation"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

    property Kriging krigingIntrfc
    property bool allowIntermediate // set to false to hide the "kriging with intermediate targets" panel

    onAllowIntermediateChanged:
    {
        smartKrigingOptions.visible = allowIntermediate
        //adjustWindowSizeToContent_forFixed() prints out some strange warnings, even though it works correctly, so let's just do it this way...
        minimumWidth = 0
        maximumWidth = 999999;
        minimumHeight = 0
        maximumHeight = 999999;       
        adjustWindowSizeToContent()
        setCurrentWindowSizeFixed()
    }

    onKrigingIntrfcChanged:
    {
        usedSkinComp.valueComponent = krigingIntrfc.getUseSkinThreshold()
        usedBoneComp.valueComponent = krigingIntrfc.getUseBonesThreshold()
        checkUseDeciDisp.checked = krigingIntrfc.decimationUseDisplacement
        checkDeciUseHomogenous.checked = krigingIntrfc.decimationUseHomogenous
        useDomainDecomposition.checked = krigingIntrfc.useDomains
        visControlPointsCheckbox.enabled = Qt.binding(function() { return krigingIntrfc.isDecimationComputed })
        visNuggetCheckbox.enabled = Qt.binding(function() { return krigingIntrfc.isNuggetComputed })
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : itemMargin

        GroupBox
        {
            Layout.fillWidth: true
            title: qsTr("Control points decimation settings")
            ColumnLayout
            {
                CheckBox
                {
                    id: checkDeciUseHomogenous
                    text: qsTr("Use homogenous decimation")
                    // tooltip: qsTr("Ensures homogenous distribution of control points within the bounding box of all the control points. If decimation by displacement is enabled, this will be done after it")
                    checked: false
                    onCheckedChanged:
                    {
                        krigingIntrfc.decimationUseHomogenous = checked
                        deciHomogenous.enabled = checked
                    }
                }
                GridLayout
                {      
                    id: deciHomogenous
                    enabled: false
                    columns: 2
                    Label
                    {
                        Layout.row: 0
                        Layout.column: 0
                        id: deciGridSizeX
                        text : qsTr("Max. control points along X axis: ")
                    }
                    TextField
                    {
                        //tooltip: qsTr("The bounding box of the control points will be divided into regular cubical grid, with this amount of cells per axis X. Each cell will be assigned at most one CP (closest to center), rest will be decimated")
                        id: paramkrigingIntrfc_decimationGridSizeX
                        Layout.row : 0
                        Layout.column : 1
                        validator : IntValidator{ bottom: 0; }
                        font.pointSize : 9
                        horizontalAlignment : Text.AlignRight
                        text: qsTr("40")
                        onTextChanged :
                            {
                                if (acceptableInput && krigingIntrfc)
                                    krigingIntrfc.decimationGridSizeX = parseInt(text);
                            }
                    }
                }
                CheckBox
                {
                    id: checkUseDeciDisp
                    text: qsTr("Use decimation based on relative displacement")
                 //   tooltip: qsTr("Control points that do not significantly contribute to the transformation field will be not used as control points in order to save computation time")
                    checked: false
                    onCheckedChanged:
                    {
                        krigingIntrfc.decimationUseDisplacement = checked
                        deciByDisplacement.enabled = checked
                    }
                }
                GridLayout
                {      
                    id: deciByDisplacement
                    columns: 2
                    enabled: false
                    Label
                    {
                        Layout.row: 0
                        Layout.column: 0
                        id: deciRadius
                        text : qsTr("Influence radius of points: ")
                    }
                    TextField
                    {
                        //tooltip: qsTr("Points around each point within this radius will be used to compute the overall displacement of the tested point; if the point has similar movement, it is insignificant and will be decimated")
                        id: paramkrigingIntrfc_decimationRadius
                        Layout.row : 0
                        Layout.column : 1
                        validator : DoubleValidator{ bottom: 0; }
                        font.pointSize : 9
                        horizontalAlignment : Text.AlignRight
                        text: qsTr("30")
                        onTextChanged :
                        {
                            if (acceptableInput && krigingIntrfc)
                                krigingIntrfc.decimationRadius = parseFloat(text);
                        }
                    }
                    Label
                    {
                        Layout.row: 1
                        Layout.column : 0
                        id: deciDeviation
                        text : qsTr("Max. deviation from average displacement: ")
                    }
                    TextField
                    {
                        Layout.row: 1
                        Layout.column : 1
                        id: paramkrigingIntrfc_decimationDeviation
                        //tooltip : qsTr("Point will be decimated if the deviation is lower than this threshold. The average is computed in the vicinity of the point based on the influence radius parameter")
                        validator : DoubleValidator{ bottom: 0; }
                        font.pointSize : 9
                        horizontalAlignment : Text.AlignRight
                        text: qsTr("0.15")
                        onTextChanged :
                        {
                            if (acceptableInput && krigingIntrfc)
                                krigingIntrfc.decimationDeviation = parseFloat(text);
                        }
                    }
                }
            }
        }
        CheckBox
        {
            id: useDomainDecomposition
            text : qsTr("Use domain decomposition")
            //     tooltip: qsTr("If set and the model contains the named metadata "***_decomposition", the model will be transformed per domain separately")
            Layout.fillWidth: true
            onCheckedChanged: krigingIntrfc.setUseDomains(checked)
        }
        RowLayout
        {
            CheckBox
            {
                id: switchIntermediates
                text: "Use intermediate skin and bones targets"
                Layout.fillWidth: true
                onCheckedChanged: allowIntermediate = checked
            }
        }
        GroupBox
        {
            id: smartKrigingOptions
            title : qsTr("Kriging with Intermediate targets")
            visible: allowIntermediate
            ColumnLayout
            {
                GroupBox
                {
                    Layout.fillWidth: true
                    id: skinIntermediateOptions
                    title : qsTr("Intermediate skin target")
                    GridLayout
                    {                 
                        columns: 2
                        CheckBox
                        {
                            Layout.columnSpan: 2
                            checked: false
                            text: qsTr("Use topology-aware distance")
                         //   tooltip: qsTr("When transforming the skin, distance determines the area of influence for each point. Using topology-aware distance will ensure that points that are close to each other in terms of euclidean distance won't affect each other if they are not also close topologically.")
                            Layout.fillWidth: true
                            onCheckedChanged :
                            {
                                krigingIntrfc.useGeodesicSkin = checked
                                if (skinPreviewCheckbox.checked)
                                    krigingIntrfc.toggleSkinPreview(skinPreviewCheckbox.checked)
                            }
                        }
                        SpinSliderComponent {
                            id: usedSkinComp
                            title: "Used for Skin threshold"
                            Layout.fillWidth : true
                            incValue: 0.1
                            maxValue: 1.0
                            minValue: 0.0
                            decimals: 1
                            updateValueWhileDragging: true
                            onValueFinalized: {
                                krigingIntrfc.setUseSkinThreshold( value)
                            }
                        }  
                        CheckBox
                        {
                            Layout.columnSpan: 2
                            id: skinPreviewCheckbox
                            checked: false
                            text: qsTr("Display intermediate skin target")
                            Layout.fillWidth: true
                            onCheckedChanged :
                            {
                                krigingIntrfc.toggleSkinPreview(checked)
                                // enforce update of nugget and CP visualization if they are computed - otherwise toggleBonePreview will send neccessary signals to do the recomputation and updates
                                if (checked && visNuggetCheckbox.checked && krigingIntrfc.isNuggetComputed)
                                    krigingIntrfc.visualizeNuggetsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                                if (checked && visControlPointsCheckbox.checked && krigingIntrfc.isDecimationComputed)
                                    krigingIntrfc.visualizeCtrlPointsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                            }
                            Connections
                            {
                                target: krigingIntrfc
                                onTargetsReset: skinPreviewCheckbox.checked = false
                            }
                        }
                    }
                }    
                GroupBox
                {
                    Layout.fillWidth: true
                    id: boneIntermediateOptions
                    title : qsTr("Intermediate bones target")
                    GridLayout
                    {
                        columns: 2
                        SpinSliderComponent {
                            id: usedBoneComp
                            title: "Used for Bone threshold"
                            Layout.fillWidth : true
                            incValue: 0.1
                            maxValue: 1.0
                            minValue: 0.0
                            decimals: 1
                            updateValueWhileDragging: true
                            onValueFinalized: {
                                krigingIntrfc.setUseBonesThreshold( value)
                            }
                        }                  
                        CheckBox
                        {
                            Layout.columnSpan: 2
                            checked: false
                            text: qsTr("Fix bones")
                            Layout.fillWidth: true
                            onCheckedChanged :
                            {
                                krigingIntrfc.fixBones = checked
                                // if bones are fixed, they must be used as intermediate targets -> make sure it is and disable turning it off
                                if (checked)
                                {
                                    applyKrigingBonesTarget.checked = true
                                    applyKrigingBonesTarget.enabled = false 
                                }
                                else
                                {
                                    applyKrigingBonesTarget.enabled = true
                                }
                            }
                        }
                        CheckBox
                        {
                            Layout.columnSpan: 2
                            id: bonePreviewCheckbox
                            checked: false
                            text: qsTr("Display intermediate bones target")
                            Layout.fillWidth: true
                            onCheckedChanged :
                            {
                                krigingIntrfc.toggleBonePreview(checked)
                                // enforce update of nugget and CP visualization if they are computed - otherwise toggleBonePreview will send neccessary signals to do the recomputation and updates
                                if (checked && visNuggetCheckbox.checked && krigingIntrfc.isNuggetComputed)
                                {
                                    krigingIntrfc.visualizeNuggetsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                                }
                                if (checked && visControlPointsCheckbox.checked && krigingIntrfc.isDecimationComputed)
                                {
                                    krigingIntrfc.visualizeCtrlPointsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                                }
                            }
                            Connections
                            {
                                target: krigingIntrfc
                                onTargetsReset: bonePreviewCheckbox.checked = false
                                onBoneTargetReset: bonePreviewCheckbox.checked = false
                            }
                        }
                    }
                }
                GroupBox
                {
                    Layout.fillWidth: true
                    title: qsTr("Control points decimation")
                    GridLayout
                    {      
				        Button
				        {
				            Layout.row: 0
				            action: decimateCtrlPointsAction
				            tooltip: qsTr("Control points on intermediate targets will be decimated based on the decimation settings above. This will be finally done regardless of whether you press this button or not, this is only so that you can see the decimation result on the preview.")
				            Layout.fillWidth: true
				        }
                        CheckBox
                        {
                            Layout.row: 1
                            id: visControlPointsCheckbox
                            //tooltip: qsTr("The preview models will be colored based on the nugget assigned to them")
                            text : qsTr("Visualize control points on intermediate targets")
                            Layout.fillWidth: true
                            onCheckedChanged:
                            {                
                                if (krigingIntrfc)
                                {
                                    if (checked)
                                    {
                                        if (krigingIntrfc.isDecimationComputed)
                                            krigingIntrfc.visualizeCtrlPointsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                                        else
                                            krigingIntrfc.decimateCtrlPoints() // will cause a change of the isDecimationComputed which will cause an update of visualization
                                    }
                                    else
                                        krigingIntrfc.visualizeCtrlPointsOffInterm(true, true)
                                }
                            }
                            onEnabledChanged: // caused by update of isDecimationComputed -> refresh visualization
                            {
                                if (enabled && krigingIntrfc.isDecimationComputed && checked)
                                    krigingIntrfc.visualizeCtrlPointsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)                        
                                else
                                    krigingIntrfc.visualizeCtrlPointsOffInterm(true, true)
                            }
                        } 
                    }
                }
                CheckBox
                {
                    text: qsTr("Use default nugget on all control points")
                    checked: true
                    onCheckedChanged:
                    {
                        krigingIntrfc.useDefaultNugget = checked
                        nuggetGroupBox.enabled = !checked
                    }
                }
                GroupBox
                {
                    id: nuggetGroupBox
                    Layout.fillWidth: true
                    title: qsTr("Automatic nugget - EXPERIMENTAL")
                    GridLayout
                    {
                        columns: 2
                        Label
                        {
                            Layout.row: 0
                            Layout.column: 0
                            id: nuggetRadius
                            text : qsTr("Influence radius of points: ")
                        }
                        TextField
                        {
                            //tooltip: qsTr("Points around each point within this radius will be used to compute the overall displacement of the tested point; if the point has similar movement, it will have low nugget")
                            id: paramkrigingIntrfc_nuggetRadius
                            Layout.row : 0
                            Layout.column : 1
                            validator : DoubleValidator{ bottom: 0; }
                            font.pointSize : 9
                            horizontalAlignment : Text.AlignRight
                            text: qsTr("30")
                            onTextChanged :
                                {
                                    if (acceptableInput && krigingIntrfc)
                                        krigingIntrfc.nuggetRadius = parseFloat(text);
                                }
                        }
                        Label
				        {
                            Layout.row: 1
                            Layout.column : 0
					        text : qsTr("Skin scale (multiplies the nugget on skin points): ")
				        }
				        TextField
				        {
                            Layout.row: 1
                            Layout.column : 1
					        id: paramkrigingIntrfc_nuggetScaleSkin
                            validator : DoubleValidator{ bottom: 0; }
					        font.pointSize : 9
                            horizontalAlignment : Text.AlignRight
                            text: qsTr("1")
                            onTextChanged :
                            {
                                if (acceptableInput && krigingIntrfc)
                                    krigingIntrfc.nuggetScaleSkin = parseFloat(text);
                            }
				        }
                        Label
				        {
                            Layout.row: 2
                            Layout.column : 0
					        text : qsTr("Bones scale (multiplies the nugget on bones points): ")
				        }
				        TextField
				        {
                            Layout.row: 2
                            Layout.column : 1
					        id: paramkrigingIntrfc_nuggetScaleBones
                            validator : DoubleValidator{ bottom: 0; }
					        font.pointSize : 9
                            horizontalAlignment : Text.AlignRight
                            text: qsTr("1")
                            onTextChanged :
                            {
                                if (acceptableInput && krigingIntrfc)
                                    krigingIntrfc.nuggetScaleBones = parseFloat(text);
                            }
				        }
                        LabeledSlider
                        {
                            //tooltip: qsTr("The importance of bone/skin, i.e. which should be respected more. At 0.5, both are treated equally, 1 = bones are assigned 0 nugget (are perfectly respected) while skin is assigned high nugget, i.e. all discontinuities in the deformation field are treated by moving the skin; if 0 = the other way around.")
                            id : paramkrigingIntrfc_nuggetBoneWeight
                            Layout.row: 3
					        sliderMaxValue: 1
					        sliderMinValue: 0.0
					        sliderStep: 0.05
                            sliderText: "Bone-skin nugget distribution ratio:"
                            sliderValue: 0.5
                            onSliderValueModified:
                            {
                                if (krigingIntrfc)
                                    krigingIntrfc.nuggetBoneWeight = sliderValue
                            }
                        }
                        Button
                        {
                            Layout.row: 4
                            action: computeNuggetsAction
                            tooltip: qsTr("Computes nuggets on control points of intermediate targets based on deviation from (local) mean of displacement - points with large deviation will be assigned high nugget (red color), small deviation low nugget (blue). White color is used for points that are not control points (were decimated).")
                            Layout.fillWidth: true
                        }
                        CheckBox
                        {
                            Layout.row: 5
                            id: visNuggetCheckbox
                            //tooltip: qsTr("The preview models will be colored based on the nugget assigned to them")
                            text : qsTr("Visualize nuggets on intermediate targets")
                            Layout.fillWidth: true
                            onCheckedChanged:
                            {                
                                if (krigingIntrfc)
                                {
                                    if (checked)
                                    {
                                        if (krigingIntrfc.isNuggetComputed)
                                            krigingIntrfc.visualizeNuggetsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)
                                        else
                                            krigingIntrfc.computeIntermNuggets() // will cause a change of the isNuggetComputed which will cause an update of visualization
                                    }
                                    else
                                        krigingIntrfc.visualizeNuggetsOffInterm(true, true)
                                }
                            }
                            onEnabledChanged: // caused by update of isNuggetComputed -> refresh visualization
                            {
                                if (enabled && krigingIntrfc.isNuggetComputed && checked)
                                    krigingIntrfc.visualizeNuggetsInterm(skinPreviewCheckbox.checked, bonePreviewCheckbox.checked)                            
                                else
                                    krigingIntrfc.visualizeNuggetsOffInterm(true, true)
                            }
                        }
                    }
                }
                CheckBox
                {
                    id: applyKrigingSkinTarget
                    checked: true
                    text: qsTr("Use skin as intermediate target")
               //     tooltip: qsTr("If set, skin will first be transformed based on the sections and then used as a target for the final kriging step, i.e. everything is transformed according to the skin")
                    Layout.fillWidth: true
                    onCheckedChanged: krigingIntrfc.setIntermediateKrigingTarget(applyKrigingSkinTarget.checked, applyKrigingBonesTarget.checked)
                }
                CheckBox
                {
                    id: applyKrigingBonesTarget
                    checked: true
                    text : qsTr("Use bones as intermediate target")
               //     tooltip: qsTr("If set, bones will first be transformed based on the segments and then used as a target for the final kriging step, i.e. everything is transformed according to the bones")
                    Layout.fillWidth: true
                    onCheckedChanged: krigingIntrfc.setIntermediateKrigingTarget(applyKrigingSkinTarget.checked, applyKrigingBonesTarget.checked)
                }
            }
        }
        Button
        {
            id: applyKrigingButton
            action: applyKrigingAction
            Layout.fillWidth: true
        }
    }

    Action {
        id: applyKrigingAction
        text: "Apply Kriging"
        shortcut: StandardKey.Refresh
        onTriggered: {
            krigingIntrfc.krige(allowIntermediate)
        }
    }

    Action {
        id: computeNuggetsAction 
        text: "Compute nuggets on skin and bones"
        onTriggered: {
            krigingIntrfc.computeIntermNuggets()
        }
    }
    
    Action {
        id: decimateCtrlPointsAction 
        text: "Decimate control points"
        onTriggered: {
            krigingIntrfc.decimateCtrlPoints()
        }
    }

    Component.onCompleted: {
        nuggetGroupBox.enabled = false
        switchIntermediates.checked = allowIntermediate
        adjustWindowSizeToContent()
        setCurrentWindowSizeFixed()
    }

    Connections {
        target: krigingIntrfc
        onDeformed:
        {
            myModelValidator.text = context.getValidHistoryName("Model_Scaling")
            myModelValidator.open()
        }
    }

    ModelHistoryNameDialog {
        id: myModelValidator
        onAccepted : krigingIntrfc.setResultinHistory(text)
    }
}


