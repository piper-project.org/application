// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2


ColumnLayout {
    width:parent.width
    id: myComboTable
    property variant internalComboList
    property variant internalTableModel
    property int tableHeight
    property int totalWidth

    signal addInTable(string  comboItem)
    signal setCurrentItem(string tableItem)
    signal removeCurrentItem(string tableItem)
    signal hideShowCurrentItem(string tableItem)
    signal exportPoints(string tableItem)
    signal resetKrigingParameters(string tableItem)
    signal parametersReset()

    property real indexTable: -1

    onIndexTableChanged: {
        myTable.selection.clear()
        if (indexTable === -1) {
            myTable.currentRow = indexTable
        }
        else
            myTable.selection.select(indexTable, indexTable)
    }


    height: parent.height

    ComboBox {
        id: myCombo
        Layout.preferredWidth: totalWidth
        Layout.fillWidth : true
        model: parent.internalComboList
        onCurrentIndexChanged: {
            if (currentIndex >0) {
                myComboTable.addInTable(myCombo.currentText)
            }
            myCombo.currentIndex=0
        }
    }

    TableView {
        id: myTable
        Layout.preferredHeight: tableHeight
        Layout.preferredWidth: totalWidth
        Layout.fillWidth : true
        Layout.fillHeight : true
        anchors.top: myCombo.bottom
        anchors.bottom: myComboTable.bottom
        model: parent.internalTableModel
        selectionMode: SelectionMode.SingleSelection
        currentRow: parent.indexTable
        TableViewColumn {
            role: "NameRole"
            title: "Name"
            movable: false
            resizable: true
        }
        TableViewColumn {
            role: "NbRole"
            title: "Count"
            movable: false
            resizable: false
        }
        TableViewColumn {
            title: "as_skin"
            movable: false
            resizable: true
            width: 60 // enough for 0-1 number
            delegate : TextField{
                validator: DoubleValidator {bottom: 0; top:1; notation:DoubleValidator.ScientificNotation}
                textColor: acceptableInput ? "black" : "red"
                onTextChanged: {
                    if (acceptableInput)
                    {
                        myComboTable.setAsSkin(myTable.model.get(styleData.row), text)
                        myTable.model.setData(styleData.row, styleData.column, text);
                    }
                }
                Component.onCompleted: 
                {
                     initAsSkin()
                }
                Connections
                {
                    target: myComboTable
                    onParametersReset: initAsSkin()
                }
                function initAsSkin()
                {
                    var a = parseFloat(myTable.model.getAsSkin(styleData.row))
                    if (a < 0)
                    {
                        enabled = false
                        text = "Per-Point"
                    }
                    text = a
                }
            }
        }
        TableViewColumn {
            title: "as_bones"
            movable: false
            resizable: true
            width: 60 // enough for 0-1 number
            delegate : TextField{
                validator: DoubleValidator {bottom: 0; top:1; notation:DoubleValidator.ScientificNotation}
                textColor: acceptableInput ? "black" : "red"
                onTextChanged: {
                    if (acceptableInput)
                    {
                        myComboTable.setAsBones(myTable.model.get(styleData.row), text)
                        myTable.model.setData(styleData.row, styleData.column, text);
                    }
                }
                Component.onCompleted: 
                {
                    initAsBones()
                }
                Connections
                {
                    target: myComboTable
                    onParametersReset: initAsBones()
                }
                function initAsBones(){
                    var a = parseFloat(myTable.model.getAsBones(styleData.row))
                    if (a < 0)
                    {
                        enabled = false
                        text = "Per-Point"
                    }
                    text = a
                }
            }
        }
        TableViewColumn {
            title: "nugget"
            movable: false
            resizable: true
            width: 60 
            delegate : TextField{
                validator : DoubleValidator{ top:0; notation:DoubleValidator.ScientificNotation }
                textColor: acceptableInput ? "black" : "red"
                onTextChanged: {
                    // if user gives a positive number as nugget, make it negative
                    var a = parseFloat(text)
                    if (acceptableInput || a == a)// NaN is not equal to itself so this is apparently the only reliable way to test for NaN http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
                    {
                        if (a > 0)
                            text = -a
                        myComboTable.setNugget(myTable.model.get(styleData.row), text)
                        myTable.model.setData(styleData.row, styleData.column, text);
                    }
                }
                Component.onCompleted: 
                {
                    initNugget()
                }
                Connections
                {
                    target: myComboTable
                    onParametersReset: initNugget()
                }
                function initNugget(){
                    var nug = parseFloat(myTable.model.getNugget(styleData.row))
                    if (nug != nug) // NaN is not equal to itself so this is apparently the only reliable way to test for NaN http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
                        text = "Global"
                    else
                    {
                        if (nug > 0)
                        {
                            enabled = false
                            text = "Per-Point"
                        }
                        else 
                            text = nug
                    }
                }
            }
        }
        Menu {
            id: contextMenu
            MenuItem {
                action: removeAction
            }
            MenuItem {
                action: hideAction
            }
            MenuItem {
                action: exportAction
            }
            MenuItem {
                action: resetParamAction
            }
        }
        rowDelegate: Item {
            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }//anchors
                height: parent.height
                color: styleData.selected ? 'lightblue' : 'white'
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.RightButton
                    propagateComposedEvents: true
                    onReleased: {
                        if (typeof styleData.row === 'number') {
                            myTable.currentRow = styleData.row
                            if (mouse.button === Qt.RightButton) { // never true
                                contextMenu.popup()
                            }//if
                        }//if
                        mouse.accepted = false
                    }//onReleased
                }//MouseArea
            }//Rectangle
        }//rowDelegate
        onCurrentRowChanged:  {
            parent.indexTable = currentRow
            if (currentRow > -1)
                myComboTable.setCurrentItem(model.get(currentRow))
        }
    }

    Action {
        id: removeAction
        text: qsTr("Remove")
        onTriggered: {
            myComboTable.removeCurrentItem(myTable.model.get(myTable.currentRow))
            myTable.selection.clear()
        }
    }

    Action {
        id: hideAction
        text: qsTr("Hide/Show")
        onTriggered: {
            myComboTable.hideShowCurrentItem(myTable.model.get(myTable.currentRow))
        }
    }

    Action {
        id: exportAction
        text: qsTr("Export")
        onTriggered: {
            myComboTable.exportPoints(myTable.model.get(myTable.currentRow))
        }
    }

    Action {
        id: resetParamAction
        text: qsTr("Default parameters")
        onTriggered: {
            myComboTable.resetKrigingParameters(myTable.model.get(myTable.currentRow))
            myComboTable.parametersReset()
        }
    }
}

