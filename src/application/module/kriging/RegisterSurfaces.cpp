/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "RegisterSurfaces.h"
#include "common/Context.h"

#include <vtkOBJReader.h>
// TODO - OBJ writer is not in VTK 7.0.0, this file is copy-pasted from VTK 8.x. Once PIPER is ported to a newer version of VTK, use obj writer directly from that
#include "vtkPIPERFilters/vtkOBJWriter.h"


namespace piper {
    namespace kriging {
        using namespace hbm;
        
     
        RegisterSurfaces::RegisterSurfaces()
        {
            m_registration = vtkSmartPointer<vtkSurfaceRegistrationPIPER>::New();
            m_errorObserver = vtkSmartPointer<VtkErrorObserver>::New();
            m_registration->AddObserver(vtkCommand::ErrorEvent, m_errorObserver);
            m_registration->AddObserver(vtkCommand::WarningEvent, m_errorObserver);
            inputNpts[0] = inputNpts[1] = inputNpts[2] = 0;
        }

        RegisterSurfaces::~RegisterSurfaces() { }

        bool RegisterSurfaces::sourceLoaded()
        {
            return inputNpts[0] > 0;
        }

        bool RegisterSurfaces::targetLoaded()
        {
            return inputNpts[1] > 0;
        }

        bool RegisterSurfaces::landmarksLoaded()
        {
            return inputNpts[2] > 0;
        }

        void RegisterSurfaces::PerformRegistration(QUrl const& filepath)
        {
            void(*wrap)(piper::kriging::RegisterSurfaces*, void (piper::kriging::RegisterSurfaces::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall < piper::kriging::RegisterSurfaces, void (piper::kriging::RegisterSurfaces::*)(std::string const&), std::string const&>;
            Context::instance().performAsynchronousOperation(std::bind(wrap, this, &piper::kriging::RegisterSurfaces::doPerformRegistration, filepath.toLocalFile().toStdString()),
                false, false, false, false, false);
        }

        void RegisterSurfaces::doPerformRegistration(std::string const& filepath)
        {
            if (sourceLoaded() && targetLoaded())
            {
                pInfo() << START << "Registering meshes...";
                m_registration->Update();
                vtkPolyData *ret = m_registration->GetOutput();
                if (!m_errorObserver->GetError())
                {
                    vtkSmartPointer<vtkOBJWriter> objw = vtkSmartPointer<vtkOBJWriter>::New();
                    vtkSmartPointer<VtkErrorObserver>  writingErrorObserver = vtkSmartPointer<VtkErrorObserver>::New();
                    objw->AddObserver(vtkCommand::ErrorEvent, writingErrorObserver);
                    objw->AddObserver(vtkCommand::WarningEvent, writingErrorObserver);
                    objw->SetFileName(filepath.c_str());
                    objw->SetInputData(ret);
                    objw->Write();
                    if (writingErrorObserver->GetError())
                        return;
                    pInfo() << DONE;
                }
                else
                {
                    pCritical() << "Registration failed: " << m_errorObserver->GetErrorMessage();
                    m_errorObserver->Clear(); // to reset error flags for if we run it again
                }
            }
            else
            {
                pCritical() << "Either source or target were not loaded, registration cannot be performed.";
            }
        }

        void RegisterSurfaces::LoadSourceOBJ(QUrl const& filepath)
        {
            void(*wrap)(piper::kriging::RegisterSurfaces*, void (piper::kriging::RegisterSurfaces::*)(std::string const&, int), std::string const&, int)
                = &piper::wrapClassMethodCall < piper::kriging::RegisterSurfaces, void (piper::kriging::RegisterSurfaces::*)(std::string const&, int), std::string const&, int>;
            Context::instance().performAsynchronousOperation(std::bind(wrap, this, &piper::kriging::RegisterSurfaces::doLoadOBJMesh, filepath.toLocalFile().toStdString(), 0),
                false, false, false, false, false);
        }

        void RegisterSurfaces::LoadTargetOBJ(QUrl const& filepath)
        {
            void(*wrap)(piper::kriging::RegisterSurfaces*, void (piper::kriging::RegisterSurfaces::*)(std::string const&, int), std::string const&, int)
                = &piper::wrapClassMethodCall < piper::kriging::RegisterSurfaces, void (piper::kriging::RegisterSurfaces::*)(std::string const&, int), std::string const&, int>;
            Context::instance().performAsynchronousOperation(std::bind(wrap, this, &piper::kriging::RegisterSurfaces::doLoadOBJMesh, filepath.toLocalFile().toStdString(), 1),
                false, false, false, false, false);
        }

        void RegisterSurfaces::doLoadOBJMesh(std::string const& filepath, int port)
        {
            pInfo() << START << "Loading mesh...";
            vtkSmartPointer<vtkOBJReader> objr = vtkSmartPointer<vtkOBJReader>::New();
            vtkSmartPointer<VtkErrorObserver>  errorObserver = vtkSmartPointer<VtkErrorObserver>::New();
            objr->AddObserver(vtkCommand::ErrorEvent, errorObserver);
            objr->AddObserver(vtkCommand::WarningEvent, errorObserver);
            objr->SetFileName(filepath.c_str());
            objr->Update();

            if (errorObserver->GetError())
                return;
            auto mesh = objr->GetOutput();
            inputNpts[port] = mesh->GetNumberOfPoints();
            m_registration->SetInputData(port, mesh);
            if (port == 0)
            {
                sourceLoadedChanged();
                m_registration->SetControlPoints(vtkSmartPointer<vtkIdTypeArray>::New());
            }
            else if (port == 1)
                targetLoadedChanged();
            pInfo() << DONE;
        }

        void RegisterSurfaces::LoadSourceLandmarks(QUrl const& filepath)
        {
            void(*wrap)(piper::kriging::RegisterSurfaces*, void (piper::kriging::RegisterSurfaces::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall < piper::kriging::RegisterSurfaces, void (piper::kriging::RegisterSurfaces::*)(std::string const&), std::string const&>;
            Context::instance().performAsynchronousOperation(std::bind(wrap, this, &piper::kriging::RegisterSurfaces::doLoadSourceLandmarks, filepath.toLocalFile().toStdString()),
                false, false, false, false, false);
        }

        void RegisterSurfaces::doLoadSourceLandmarks(std::string const& filepath)
        {
            if (inputNpts[0] > 0)
            {
                pInfo() << START << "Loading landmarks...";
                vtkSmartPointer<vtkIdTypeArray> cps = vtkSmartPointer<vtkIdTypeArray>::New();
                cps->SetNumberOfValues(inputNpts[0]);
                cps->FillComponent(0, -1);
                try
                {
                    std::ifstream cpFile;
                    cpFile.open(filepath);
                    if (cpFile.is_open())
                    {
                        std::string myString;
                        vtkIdType IDsource, IDtarget;
                        while (!cpFile.eof())
                        {
                            getline(cpFile, myString);
                            std::istringstream iMyString(myString);
                            iMyString >> IDsource >> IDtarget;
                            if (IDsource >= inputNpts[0] || IDsource < 0)
                                pWarning() << "Source ID " << IDsource << " in the landmarks file is invalid, will be ignored.";
                            else // we don't need to check the target ID validity - vtkSurfaceRegistrationPIPER will do that itself
                                cps->SetValue(IDsource, IDtarget);
                        }
                        cpFile.close();
                    }
                    else
                        pWarning() << "Could not open landmarks file " << filepath << ", landmarks will not be used for registration.";
                }
                catch (std::exception e)
                {
                    pWarning() << "Could not open landmarks file " << filepath << ". Encountered error: " << e.what();
                    return;
                }
                m_registration->SetControlPoints(cps);
                inputNpts[2] = cps->GetNumberOfTuples();
                landmarksLoadedChanged();
                pInfo() << DONE;
            }
            else
                pCritical() << "Source mesh must be loaded before loading landmarks.";            
        }

        void RegisterSurfaces::CheckInputValid()
        {
            pInfo() << "Checking registration input...";
            areMeshesCompatible = m_registration->CheckInputValidity();
            if (!m_errorObserver->GetError() && !m_errorObserver->GetWarning())
                pInfo() << "Meshes can be registered.";
            // else the error was printed by the errorObserver
            m_errorObserver->Clear();
            meshesCompatibleChanged();
        }

        bool RegisterSurfaces::meshesCompatible()
        {
            return areMeshesCompatible;
        }

        void RegisterSurfaces::SetNoIterations(int noIter)
        {
            m_registration->SetNumberOfIterations(noIter);
        }

        void RegisterSurfaces::SetHistBuckets(int noBuckets)
        {
            m_registration->SetHistogramBuckets(noBuckets);
        }

        void RegisterSurfaces::SetRigidRegistration(bool useRigid)
        {
            m_registration->SetRigidOnly(useRigid);
        }

        void RegisterSurfaces::SetPostProcessSmoothing(int noIter)
        {
            m_registration->SetPostProcessSmooth(noIter);
        }
    }
}