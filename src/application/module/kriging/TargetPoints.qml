// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.Kriging 1.0

ModuleToolWindow
{
    title: "Target Points"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

    property KrigingModule _kriging
    property int sizeWidth
    property alias tableHeight: targetpoints.tableHeight

    Settings {
        id: settings
        category: "krigingTrgtPoints"

        property url recentFolder
        property url recentExportFolder
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : itemMargin
        Button {
            text: qsTr("Import Target Points")
            tooltip: qsTr("Import Target Points")
            Layout.fillWidth : true
            width: sizeWidth
            action: importTargetPointsAction
        }

        ComboTableTargetComponent {
            id: targetpoints
            tableHeight: tableHeight
            totalWidth: sizeWidth
            Layout.fillWidth : true
            internalComboList: _kriging.availableTargetPoints
            internalTableModel: _kriging.targetPoints
            internalComboSourceListModel: _kriging.currentListSourcePoints
            onAddInTable: _kriging.addTargetPoints( comboItem)
            onSetCurrentItem: _kriging.setCurrentTargetPoints( tableItem)
            onRemoveCurrentItem: _kriging.removeTargetPoints( tableItem)
            onSetAsso: _kriging.setAssoSourceTarget(tableItem, sourceComboItem)
            onRemoveAsso: _kriging.removeAssoSourceTarget(tableItem)
            onHideShowCurrentItem: _kriging.toggleVisibilityCPTarget(tableItem)
            onExportPoints: 
            {
                exportPointsDialog.curPointsName = tableItem
                exportPointsDialog.open()
            }
            Connections
            {
                target: _kriging
                onCurrentTargetPointsChanged: {
                    targetpoints.indexTable = _kriging.targetPoints.get(_kriging.currentTargetPoints)
                }
            }
        }
    }

    ImportControlPointsDialog {
        id: importTargetPointsDialog
        onAccepted: {
            settings.recentFolder = importTargetPointsDialog.folder;
            _kriging.loadTargetPoint(importTargetPointsDialog.selectedFilePath, importTargetPointsDialog.name)
        }
        onVisibleChanged: {            
            if(visible)
            {
                importTargetPointsDialog.name = _kriging.getValidTargetPointName()
                if(context.doFolderExist(settings.recentFolder))
                    importTargetPointsDialog.folder = settings.recentFolder;
            }
        }
    }

    FileDialog 
    {
        id: exportPointsDialog
        property string curPointsName: ""
        selectExisting: false
        onAccepted: {
            settings.recentExportFolder = folder;
            _kriging.exportControlPointsTarget(curPointsName, fileUrl, false);
        }
        onVisibleChanged:
        {
            if(visible)
            {
                if(context.doFolderExist(settings.recentExportFolder))
                    folder = settings.recentExportFolder;
            }
        }
    }
    Action{
        id: importTargetPointsAction
        text: qsTr("Import Target Points")
        onTriggered : importTargetPointsDialog.open()
    }

    Component.onCompleted: {
        adjustWindowSizeToContent(20, 20)
        importTargetPointsDialog._kriging = _kriging
    }
}


