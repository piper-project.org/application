// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2

import Piper 1.0
import piper.Kriging 1.0

Dialog {
    title: "Import 3D control Points"
    id: windowControlPoints
    visible: false

    property url selectedFilePath
    property alias name: setname.text
    property KrigingModule _kriging
    property alias folder: selectControlPointDialog.folder

    Component.onCompleted: {
        height= 120;
        width=500;
    }

    GridLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        columns: 3
        Label {
            text: "Name of control point set:"
            Layout.alignment: Qt.AlignRight
            Layout.columnSpan: 1
        }
        TextField {
            id: setname
            Layout.fillWidth: true
            Layout.columnSpan: 2
            text: _kriging.getValidControlPointSetName()
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Control Point file:"
        }
        TextField {
            id: selectedFilePathText
            Layout.fillWidth: true
            placeholderText: qsTr("Select Control Point file")
        }
        Button {
            tooltip: qsTr("Select Control Point file")
            iconSource: "qrc:///icon/document-open.png"
            onClicked: selectControlPointDialog.open()
        }
    }

    FileDialog {
        id: selectControlPointDialog
        title: qsTr("Select Control Point file...")
        nameFilters: ["files (*.*)"]
        onAccepted: {
            var pathString = selectControlPointDialog.fileUrl.toString();
            selectedFilePath = pathString
            selectedFilePathText.text = pathString
            // suggest naming the control points as the file name
            setname.text = pathString.substring(pathString.lastIndexOf("/") + 1, pathString.lastIndexOf(".")); 
        }
    }

}

