/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "KrigingModule.h"

#include <utility>
#include <functional>

#include "common/Context.h"
#include "common/XMLValidationProcess.h"
#include "common/KrigingTargetGenerator.h"

#include "kriging/KrigingPiperInterface.h"
#include "kriging/CtrlPtLoader.h"
#include "hbm/Helper.h"

#include "common/OctaveProcess.h"
#include "common/helper.h"


namespace piper {
    namespace kriging {
        using namespace hbm;


        ModelTableSource::ModelTableSource(std::map<std::string, ModeItemSource> const& listnames, QObject *parent) : QAbstractListModel(parent) {
            construct(listnames);
        }

        int ModelTableSource::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return 5;
        }

        int ModelTableSource::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return items.count();
        }

        QVariant ModelTableSource::data(const QModelIndex &index, int role) const {
            switch (role) {
                case NbRole:
                    return (unsigned int)items[index.row()].m_nb;
                case AsBonesRole:
                    return (double)items[index.row()].m_as_bones;
                case AsSkinRole:
                    return (double)items[index.row()].m_as_skin;
                case Nugget:
                    return (double)items[index.row()].m_nugget;
                case NameRole:
                default:
                    return items[index.row()].m_name;
            }
        }

        QVariant ModelTableSource::getAsSkin(const QVariant &index) const {
            int i = index.toInt();
            if (i >= 0 && i < items.size())
            {
                double val = (double)items[index.toInt()].m_as_skin;
                if (!std::isnan(val))
                    return val;
            }
            return KRIGING_DEF_AS_SKINGLOBAL;
        }


        QVariant ModelTableSource::getAsBones(const QVariant &index) const {
            int i = index.toInt();
            if (i >= 0 && i < items.size())
            {
                double val = (double)items[index.toInt()].m_as_bones;
                if (!std::isnan(val))
                    return val;
            }
            return KRIGING_DEF_AS_BONESGLOBAL;
        }

        QVariant ModelTableSource::getNugget(const QVariant &index) const {
            int i = index.toInt();
            if (i >= 0 && i < items.size())
                return (double)items[index.toInt()].m_nugget;
            return NAN; // if no nugget is defined, specify it as NAN
        }

        QHash<int, QByteArray> ModelTableSource::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[NbRole] = "NbRole";
            roles[AsSkinRole] = "AsSkinRole";
            roles[AsBonesRole] = "AsBonesRole";
            roles[Nugget] = "Nugget";
            return roles;
        }

        bool ModelTableSource::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
                case NameRole: items[index.row()].m_name = value.toString(); break;
                case NbRole: items[index.row()].m_nb = value.toInt(); break;
                case AsBonesRole: items[index.row()].m_as_bones = value.toDouble(); break;
                case AsSkinRole: items[index.row()].m_as_skin = value.toDouble(); break;
                case Nugget: items[index.row()].m_nugget = value.toDouble(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void ModelTableSource::construct(std::map<std::string, ModeItemSource> const& listnames) {
            items.clear();
            this->beginResetModel();
            for (auto &cur : listnames)
                items.append(cur.second);
            this->endResetModel();
        }
        
        Qt::ItemFlags ModelTableSource::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
        }

        void ModelTableSource::clear() {
            items.clear();
            this->beginResetModel();
            this->endResetModel();
        }

        QVariant const ModelTableSource::get(QString const& value) const {
            int count = 0;
            for (auto const& cur : items) {
                if (cur.m_name == value)
                    return count;
                else count++;
            }
            return -1;
        }

        ModelTableTarget::ModelTableTarget(std::map<std::string, size_t> const& listnames,
            std::map<std::string, std::string> const& list_assoc, QObject *parent) : QAbstractListModel(parent) {
            construct(listnames, list_assoc);
        }

        int ModelTableTarget::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return 3;
        }

        int ModelTableTarget::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return items.count();
        }

        QVariant ModelTableTarget::data(const QModelIndex &index, int role) const {
            switch (role) {
            case NameRole:
                return items[index.row()]->m_name;
            default:
                return items[index.row()]->m_name;
            case NbRole:
                return (unsigned int)items[index.row()]->m_nb;
            case AssocRole:
                return items[index.row()]->m_assoc;
            }
        }

        QHash<int, QByteArray> ModelTableTarget::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[NbRole] = "NbRole";
            roles[AssocRole] = "AssocRole";
            return roles;
        }

        bool ModelTableTarget::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
            case NameRole: items[index.row()]->m_name = value.toString(); break;
            case NbRole: items[index.row()]->m_nb = value.toInt(); break;
            case AssocRole: items[index.row()]->m_assoc = value.toString(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void ModelTableTarget::construct(std::map<std::string, size_t> const& listnames, std::map<std::string, std::string> const& list_assoc) {
            items.clear();
            this->beginResetModel();
            if (listnames.size() > 0) {
                for (auto const& cur : listnames) {
                    if (list_assoc.find(cur.first) == list_assoc.end())
                        items.append(new ModeItemTarget(QString::fromStdString(cur.first), cur.second));
                    else
                        items.append(new ModeItemTarget(QString::fromStdString(cur.first), cur.second, QString::fromStdString(list_assoc.at(cur.first))));
                }
            }
            this->endResetModel();
        }


        Qt::ItemFlags ModelTableTarget::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
        }

        void ModelTableTarget::clear() {
            items.clear();
            this->beginResetModel();
            this->endResetModel();
        }

        QVariant const ModelTableTarget::get(QString const& value) const {
            int count = 0;
            for (auto const& cur : items) {
                if (cur->m_name == value)
                    return count;
                else count++;
            }
            return -1;
        }

        QString const ModelTableTarget::getSource(int index) const {
            if (items.at(index)->m_assoc.isEmpty())
                return QString("Select");
            else
                return items.at(index)->m_assoc;
        }


        KrigingModule::KrigingModule()
            : model_controlPointsSource(new ModelTableSource())
            , m_currentControlPointSource("")
            , model_targetPoints(new ModelTableTarget())
            , m_currentTargetPoints("")
        {
            connect(&Context::instance(), &Context::modelChanged, this, &KrigingModule::reset);
            connect(&Context::instance(), &Context::targetChanged, this, &KrigingModule::updateTargets);

            connect(this, &KrigingModule::controlPointsSourceChanged, this, &KrigingModule::availableControlPointsChanged);
            connect(this, &KrigingModule::controlPointsSourceChanged, this, &KrigingModule::currentListSourcePointsChanged);
            connect(this, &KrigingModule::controlPointsSourceChanged, this, &KrigingModule::reconstructSourceTable);

            connect(this, &KrigingModule::targetPointsChanged, this, &KrigingModule::availableTargetPointsChanged);
            connect(this, &KrigingModule::targetPointsChanged, this, &KrigingModule::reconstructTargetTable);

            m_kriging.getKrigingInterface()->setRefHBM(&Context::instance().project().model());

            connect(&m_kriging, &Kriging::willPerformKriging, this, &KrigingModule::setUpKrigingSystem,
                Qt::ConnectionType::DirectConnection);
            connect(&m_kriging, &Kriging::transformedCPs, this, &KrigingModule::createDeformedCPs);
        }

        KrigingModule::~KrigingModule() {
            if (model_controlPointsSource != nullptr) {
                model_controlPointsSource->clear();
                delete model_controlPointsSource;
            }
            if (model_targetPoints != nullptr) {
                model_targetPoints->clear();
                delete model_targetPoints;
            }
        }

        Kriging *KrigingModule::kriging()
        {
            return &m_kriging;
        }

        void KrigingModule::reset()
        {
            m_currentControlPointSource = "";
            m_currentTargetPoints = "";
            m_kriging.getKrigingInterface()->setRefHBM(&Context::instance().project().model());
            m_kriging.getDisplay()->reset();
        }

        void KrigingModule::InitModuleData(QString const& initdata) {
            m_currentControlPointSource = "";
            m_currentTargetPoints = "";
            checkControlsourcePoints();
            checkTargetPoints();
            if (initdata != "none") {
                addControlPointsSource(initdata);
                addTargetPoints(initdata);
                setAssoSourceTarget(initdata, initdata);
            }
        }

        void KrigingModule::updateTargets() {
            checkTargetPoints();
        }

        void KrigingModule::checkControlsourcePoints() {
            if (!Context::instance().hasModel()) {
                m_controlPointsSource.clear();
                return;
            }
            if (m_controlPointsSource.size() > 0) {
                // chekck it exists in metadata
                std::vector<std::string> removed, removed_asso;
                for (auto const& cur : m_controlPointsSource) 
                {
                    InteractionControlPoint *s = Context::instance().project().model().metadata().interactionControlPoint(cur.first);
                    if (s == NULL || s->getControlPointRole() == InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET)
                        removed.push_back(cur.first);
                }
                if (removed.size() > 0) {
                    for (auto const& cur : removed) {
                        m_controlPointsSource.erase(cur);
                        pWarning() << "Control points " << cur << " has been removed from current control points selection as they do not exist in the model!";
                        if (cur == m_currentControlPointSource)
                            setCurrentControlPointSource("");
                    }
                    for (auto const& cur : removed) {
                        for (auto const& curasso : m_assoSourceTargetPoints) {
                            if (curasso.second == cur)
                                removed_asso.push_back(curasso.first);
                        }
                    }
                    if (removed_asso.size() > 0) {
                        for (auto const& cur : removed_asso) {
                            m_assoSourceTargetPoints.erase(cur);
                        }
                    }
                }
                model_controlPointsSource->construct(m_controlPointsSource);
            }
        }

        void KrigingModule::checkTargetPoints() {
            if (!Context::instance().hasModel()) {
                m_targetPoints.clear();
                m_assoSourceTargetPoints.clear();
                return;
            }
            if (m_targetPoints.size() > 0) {
                std::set<std::string> targetname;
                std::vector<std::string> removed;
                for (auto const& cur : Context::instance().project().target().controlPoint)
                    targetname.insert(cur.name());
                for (auto const& cur : m_targetPoints) 
                {
                    InteractionControlPoint *t = Context::instance().project().model().metadata().interactionControlPoint(cur.first);
                    if (targetname.find(cur.first) == targetname.end() && 
                        (t == NULL || t->getControlPointRole() == InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE)) 
                    {
                        removed.push_back(cur.first);
                        pWarning() << "Target Control points " << cur.first << " has been removed from current target control points selection as they do not exist in the model!";
                        setCurrentTargetPoints("");
                    }
                }
                if (removed.size() > 0) {
                    for (auto const& cur : removed) {
                        m_targetPoints.erase(cur);
                        if (m_assoSourceTargetPoints.find(cur) != m_assoSourceTargetPoints.end())
                            m_assoSourceTargetPoints.erase(cur);
                    }
                }
                emit this->targetPointsChanged();
            }
        }

        void KrigingModule::doLoadCtrlPtSrc(std::string const& filename, std::string const& name) {
            if (Context::instance().project().model().empty()){
                pInfo() << INFO << "No Model - make sure that a model has been opened and that the \"Check\" module runs successfully.";
                pCritical() << QStringLiteral("while loading control points : no  model.");
                return;
            }

            pInfo() << START << "Load control points (" << name << "): " << filename;

            CtrlPtLoader myLoader;
            if (myLoader.loadData(filename))
            {
                HumanBodyModel& hbmref = Context::instance().project().model();
                Metadata::InteractionControlPointCont & ctrlPtCont = hbmref.metadata().interactionControlPoints();

                //store data in a ctrlptset
                InteractionControlPoint ctrlPt;
                ctrlPt.setName(name);
                ctrlPt.setControlPointRole(InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE);
                myLoader.getCoordinates(ctrlPt.getControlPt3dCont());

                if (hbmref.metadata().addInteractionControlPoint(ctrlPt)) {
                    addControlPointsSource(QString::fromStdString(name));
                    pInfo() << DONE << " loaded source control points: " << name << " with " <<
                        Context::instance().project().model().metadata().interactionControlPoint(name)->getNbControlPt() << " control points.";
                }
                else {
                    pCritical() << "Control points with name " << name << " already exist in metadata.";
                }
            }
            else
                pCritical() << "Failed reading from " << filename << ".";
        }


        void KrigingModule::loadCtrlPtSrc(QUrl filename, QString const& name){
            if (filename.toLocalFile().toStdString().size() > 0) {
                setOperationSignals(true, true);
                void(*loadCtrlPtSrcWrapper)(piper::kriging::KrigingModule*, void (piper::kriging::KrigingModule::*)(std::string const&, std::string const&), 
                    std::string const&, std::string const&)
                    = &piper::wrapClassMethodCall < piper::kriging::KrigingModule,
                    void(piper::kriging::KrigingModule::*)(std::string const&, std::string const&), std::string const&, std::string const& >;

                Context::instance().performAsynchronousOperation(std::bind(loadCtrlPtSrcWrapper, this,
                    &piper::kriging::KrigingModule::doLoadCtrlPtSrc, filename.toLocalFile().toStdString(), name.toStdString()),
                    false, false, false, false, false);
            }
        }

        void KrigingModule::doLoadTargetPoint(std::string const& filename, std::string const& name) {
            if (Context::instance().project().model().empty()){
                pInfo() << INFO << "No Model - make sure that a model has been opened and that the \"Check\" module runs successfully.";
                pCritical() << QStringLiteral("while loading target points : no  model.");
                return;
            }
            pInfo() << START << "Load target points (" << name << "): " << filename;

            CtrlPtLoader myLoader;
            if (myLoader.loadData(filename))
            {
                //store data in a ctrlptset
                std::vector<hbm::Coord> vcoord;
                myLoader.getCoordinates(vcoord);
                // define target
                ControlPoint addtarget;
                addtarget.setName(name);
                addtarget.setSubsetName(name);
                addtarget.setValue(vcoord);
                addtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
                Context::instance().project().target().add(addtarget);


                addTargetPoints(QString::fromStdString(name));

                pInfo() << SUCCESS << "loaded target points: " << name << " with " << vcoord.size() << " points.";
            }
            else
                pCritical() << "Failed reading from " << filename << ".";

        }

        void  KrigingModule::loadTargetPoint(QUrl filename, QString const& name){
            if (filename.toLocalFile().toStdString().size() > 0) {
                setOperationSignals(true, false);
                void(*loadTargetPointWrapper)(piper::kriging::KrigingModule*, void (piper::kriging::KrigingModule::*)(std::string const&, std::string const&), 
                    std::string const&, std::string const&)
                    = &piper::wrapClassMethodCall < piper::kriging::KrigingModule, 
                    void(piper::kriging::KrigingModule::*)(std::string const&, std::string const&), std::string const&, std::string const& >;

                Context::instance().performAsynchronousOperation(std::bind(loadTargetPointWrapper,
                    this, &piper::kriging::KrigingModule::doLoadTargetPoint, filename.toLocalFile().toStdString(), name.toStdString()),
                    false, false, false, false, false);
            }
        }
        
        QString KrigingModule::getValidControlPointSetName() const {
            Metadata::InteractionControlPointCont & ctrlPtCont = Context::instance().project().model().metadata().interactionControlPoints();
            std::string trame("ControlPoints");
            std::string validname(trame);
            std::vector<std::string> vnames;
            if (ctrlPtCont.size() > 0) {
                for (auto const& cur : ctrlPtCont)
                    vnames.push_back(cur.first);
            }
            getValidName(trame, vnames);
            return QString::fromStdString(trame);
        }

        QString KrigingModule::getValidTargetPointName() const {
            piper::hbm::TargetList const& target = Context::instance().project().target();
            std::string trame("TargetPoints");
            std::vector<std::string> vnames;
            for (auto const& cur : target.controlPoint)
                vnames.push_back(cur.name());
            getValidName(trame, vnames);
            return QString::fromStdString(trame);
        }

        QString KrigingModule::currentControlPoints() const {
            return QString::fromStdString(m_currentControlPointSource);
        }

        QString KrigingModule::currentTargetPoints() const {
            return QString::fromStdString(m_currentTargetPoints);
        }

        QStringList KrigingModule::availableControlPoints() const {
            Metadata::InteractionControlPointCont & ctrlPtCont = Context::instance().project().model().metadata().interactionControlPoints();
            QStringList list;
            list.append("Select");
            if (ctrlPtCont.size() > 0) {
                for (auto const& cur : ctrlPtCont) {
                    if (cur.second.getControlPointRole() != InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET
                        && m_controlPointsSource.find(cur.first) == m_controlPointsSource.end())
                        list.append(QString::fromStdString(cur.first));
                }
            }
            return list;
        }

        QStringList KrigingModule::currentListSourcePoints() const {
            QStringList list;
            list.append("Select");
            for (auto const& cur : m_controlPointsSource)
                list.append(cur.second.m_name);
            return list;
        }

        QStringList KrigingModule::availableTargetPoints() const {
            QStringList list;
            list.append("Select");
            Metadata::InteractionControlPointCont & ctrlPtCont = Context::instance().project().model().metadata().interactionControlPoints();
            if (ctrlPtCont.size() > 0) {
                for (auto const& cur : ctrlPtCont) {
                    if (cur.second.getControlPointRole() != InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE
                        && m_targetPoints.find(cur.first) == m_targetPoints.end())
                        list.append(QString::fromStdString(cur.first));
                }
            }
            for (auto const& cur : Context::instance().project().target().controlPoint) {
                if (m_targetPoints.find(cur.name()) == m_targetPoints.end() 
                    && !list.contains(QString::fromStdString(cur.name())))
                    list.append(QString::fromStdString(cur.name()));
            }
            return list;
        }

        ModelTableSource* KrigingModule::controlPointsSource() const {
            return model_controlPointsSource;
        }

        ModelTableTarget* KrigingModule::targetPoints() const {
            return model_targetPoints;
        }

        void KrigingModule::addControlPointsSource(QString const& name) {
            InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
            if (set)
            {                
                ModeItemSource m = ModeItemSource(name, set->getNbControlPt(),
                    set->getAssociationBones().size() == set->getNbControlPt() ? -1 : set->getAssociationBonesGlobal(),
                    set->getAssociationSkin().size() == set->getNbControlPt() ? -1 : set->getAssociationSkinGlobal(),
                    set->getWeight().size() == set->getNbControlPt() ? 1 : set->getGlobalWeight());
                m_controlPointsSource.insert({ name.toStdString(), m });
                m_kriging.getDisplay()->displayControlPointsSource(name.toStdString(), true, true);
                setCurrentControlPointSource(name);
                emit this->controlPointsSourceChanged();
            }
        }

        void KrigingModule::reconstructSourceTable()
        {
            model_controlPointsSource->construct(m_controlPointsSource);
        }

        void KrigingModule::reconstructTargetTable()
        {
            model_targetPoints->construct(m_targetPoints, m_assoSourceTargetPoints);
        }

        void KrigingModule::addTargetPoints(QString const& name) {
            size_t n = 0;
            for (auto const& cur : Context::instance().project().target().controlPoint) {
                if (name.toStdString() == cur.name()) {
                    n = cur.value().size();
                    break;
                }
            }
            if (n == 0) // look for the set within the control poitns, not among targets
            {
                InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
                if (set)
                    n = set->getNbControlPt();
            }
            m_targetPoints[name.toStdString()] = n;
            m_kriging.getDisplay()->displayControlPointsTarget(name.toStdString(), true, true);
            setCurrentTargetPoints(name);
            emit this->targetPointsChanged();
        }

        void KrigingModule::removeControlPointsSource(QString const& name) {
            m_controlPointsSource.erase(name.toStdString());
            for (auto & cur : m_assoSourceTargetPoints) 
            {
                if (cur.second == name.toStdString())
                {
                    m_kriging.getDisplay()->removeAsso(cur.first, false);
                    m_assoSourceTargetPoints.erase(cur.first);
                }
            }
            m_kriging.getDisplay()->removeControlPointsSource(name.toStdString(), true);

            setCurrentControlPointSource("");
            emit this->controlPointsSourceChanged();
            emit this->targetPointsChanged();
        }

        void KrigingModule::removeTargetPoints(QString const& name) {
            m_targetPoints.erase(name.toStdString());
            if (m_assoSourceTargetPoints.find(name.toStdString()) != m_assoSourceTargetPoints.end())
                m_assoSourceTargetPoints.erase(name.toStdString());
            m_kriging.getDisplay()->removeControlPointsTarget(name.toStdString(), true);
            setCurrentTargetPoints("");
            emit this->targetPointsChanged();
        }

        void KrigingModule::toggleVisibilityCPSource(QString const& name)
        {
            m_kriging.getDisplay()->displayControlPointsSource(name.toStdString(),
                !m_kriging.getDisplay()->GetCPSourceVisible(name.toStdString()), false);
            Context::instance().display().Render();

        }

        void KrigingModule::toggleVisibilityCPTarget(QString const& name)
        {
            m_kriging.getDisplay()->displayControlPointsTarget(name.toStdString(),
                !m_kriging.getDisplay()->GetCPTargetVisible(name.toStdString()), false);
            Context::instance().display().Render();
        }

        void KrigingModule::resetCPSetParameters(QString const& name)
        {
            InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
            if (set)
            {
                set->setGlobalAssociationBones(KRIGMODULE_DEF_AS_BONESGLOBAL);
                set->setGlobalAssociationSkin(KRIGMODULE_DEF_AS_SKINGLOBAL);
                set->setGlobalWeight(NAN); // marks that the global nugget (set through Module Parameters) should be used
                auto &entry = m_controlPointsSource.find(name.toStdString());
                if (entry != m_controlPointsSource.end())
                {
                    int index = model_controlPointsSource->get(name).toInt();
                    model_controlPointsSource->items[index].m_as_bones = KRIGMODULE_DEF_AS_BONESGLOBAL;
                    model_controlPointsSource->items[index].m_as_skin = KRIGMODULE_DEF_AS_SKINGLOBAL;
                    model_controlPointsSource->items[index].m_nugget = NAN;
                    entry->second.m_as_bones = KRIGMODULE_DEF_AS_BONESGLOBAL;
                    entry->second.m_as_skin = KRIGMODULE_DEF_AS_SKINGLOBAL;
                    entry->second.m_nugget = NAN;
                }
                m_kriging.resetKrigingSources();
            }
        }

        void KrigingModule::setCurrentControlPointSource(QString const& name) {
            if (m_currentControlPointSource != name.toStdString()) {
                m_currentControlPointSource = name.toStdString();
                m_kriging.getDisplay()->HighlightControlPointsSource(m_currentControlPointSource);
                emit this->currentControlPointsSourceChanged();
            }
        }

        void KrigingModule::setCurrentTargetPoints(QString const& name) {
            if (m_currentTargetPoints != name.toStdString()) {
                m_currentTargetPoints = name.toStdString();
                m_kriging.getDisplay()->HighlightControlPointsTarget(m_currentTargetPoints);
                emit this->currentTargetPointsChanged();
            }
        }

        void KrigingModule::setAssoSourceTarget(QString const& target, QString const& source){
            // first remove existing association if there is some
            if (m_assoSourceTargetPoints.find(target.toStdString()) != m_assoSourceTargetPoints.end())
                m_kriging.getDisplay()->removeAsso(target.toStdString(), false);
            m_assoSourceTargetPoints[target.toStdString()] = source.toStdString();
            m_kriging.getDisplay()->displayAsso(target.toStdString(), true, source.toStdString());
            emit this->targetPointsChanged();
        }

        void KrigingModule::removeAssoSourceTarget(QString const& target) {
            if (m_assoSourceTargetPoints.find(target.toStdString()) != m_assoSourceTargetPoints.end()) {
                m_assoSourceTargetPoints.erase(target.toStdString());
                m_kriging.getDisplay()->removeAsso(target.toStdString(), true);
            }
            emit this->targetPointsChanged();
        }

        void KrigingModule::setUpKrigingSystem() {
            InteractionControlPoint sources, targets;
            collectControlPoints(sources, targets);

            m_kriging.getKrigingInterface()->setControlPoints(sources, targets);
        }

        void KrigingModule::collectControlPoints(InteractionControlPoint& source, InteractionControlPoint& target) const 
        {
            Context::instance().project().collectControlPoints(source, target, m_assoSourceTargetPoints);
            
        }

        void KrigingModule::createDeformedCPs()
        {
            piper::hbm::FEModel const& fem = Context::instance().project().model().fem();
            piper::hbm::Metadata & metadata = Context::instance().project().model().metadata();
            piper::hbm::TargetList const& targetlist = Context::instance().project().target();
            vtkSmartPointer<vtkPoints> transCPs = m_kriging.getTransformedCPs();
            if (transCPs == NULL || transCPs->GetNumberOfPoints() == 0)
                return;
            vtkIdType currentCP = 0;
            // use the history model name to identify at which transformation the given points were created
            std::string modelName = Context::instance().project().model().history().getCurrent();
            for (auto const& cur : m_assoSourceTargetPoints) // concatenate all matched pairs of control points into one set of control points
            {
                InteractionControlPoint * set = metadata.interactionControlPoint(cur.second);
                if (set)
                {
                    for (auto const& curset : targetlist.controlPoint)
                    {
                        if (curset.name() == cur.first) // find the matched target
                        {
                            if (curset.value().size() == set->getNbControlPt())
                            {
                                std::stringstream newName;
                                newName << cur.second << "->" << modelName;
                                // ensure unique name
                                int counter = 1;
                                while (metadata.hasInteractionControlPoint(newName.str()))
                                {
                                    newName.clear();
                                    newName.str("");
                                    newName << cur.second << "->" << modelName << "_" << counter++;
                                }
                                // the nodes coming from FE ids are first, add them
                                VId const& feIDs = set->getGroupNodeId();
                                VId transFeIDs(feIDs);
                                currentCP += feIDs.size();
                                InteractionControlPoint transformed(newName.str(), transFeIDs);
                                // now update the coord container
                                std::vector<Coord> &cont = set->getControlPt3dCont();
                                std::vector<Coord> &contTrans = transformed.getControlPt3dCont();
                                for (size_t i = 0; i < cont.size(); i++)
                                {
                                    double *point = transCPs->GetPoint(currentCP++);
                                    contTrans.push_back(Coord(point[0], point[1], point[2]));
                                }
                                if (metadata.addInteractionControlPoint(transformed)) 
                                    addControlPointsSource(QString::fromStdString(transformed.name()));
                            }
                            break;
                        }
                    }
                }
            }
            emit this->controlPointsSourceChanged();
        }
        
        void KrigingModule::setOperationSignals(bool initModuleGUI, bool sourcePointsTableUpdated)
        {
            disconnect(&Context::instance(), &Context::operationFinished, 0, 0);
            if (initModuleGUI)
                connect(&Context::instance(), &Context::operationFinished, this, &KrigingModule::initModuleGUI);
            if (sourcePointsTableUpdated)
                connect(&Context::instance(), &Context::operationFinished, this, &KrigingModule::reconstructSourceTable);
        }

        void KrigingModule::initModuleGUI() {
            emit this->currentControlPointsSourceChanged();
            emit this->currentTargetPointsChanged();
            emit this->controlPointsSourceChanged();
            emit this->targetPointsChanged();
        }

        void KrigingModule::generateTargetPoints(QUrl  scriptpath) {
            if (scriptpath.toLocalFile().toStdString().size() > 0) {
                QDir scriptDir(piper::octaveScriptDirectoryPath());
                QString scriptrelativepath = scriptDir.relativeFilePath(scriptpath.adjusted(QUrl::NormalizePathSegments).toLocalFile().replace("file:/", ""));
                //std::cout << piper::octaveScriptDirectoryPath().toStdString() << std::endl;
                //std::cout << scriptpath.adjusted(QUrl::NormalizePathSegments).toLocalFile().replace("file:/", "").toStdString() << std::endl;
                //std::cout << scriptrelativepath.toStdString() << std::endl;

                doGenerateTargetPoints(scriptrelativepath.toStdString());
                //setOperationSignals(true);
                //void(*generateTargetPointsWrapper)(piper::kriging::Kriging*, void (piper::kriging::Kriging::*)(std::string const&), std::string const&)
                //    = &piper::wrapClassMethodCall < piper::kriging::Kriging, void(piper::kriging::Kriging::*)(std::string const&), std::string const& >;

                //Context::instance().performAsynchronousOperation(std::bind(generateTargetPointsWrapper, this,
                //    &piper::kriging::Kriging::doGenerateTargetPoints, scriptrelativepath.toStdString()),
                //    false, false, false, false, false);
            }
        }

        void KrigingModule::doGenerateTargetPoints(std::string const& scriptpath) {

            std::string ageUnit;

            pInfo() << START << "Apply Octave script to generate target control points from current set of source control points.";

            // export control point in basic csv format

            QString filepath = QString::fromStdString(piper::tempDirectoryPath() + "/controlpointsSource.txt");
            exportControlPointsSource(QString::fromStdString(m_currentControlPointSource), QUrl::fromLocalFile(filepath), true);



            QString outputFilePath = QString::fromStdString(piper::tempDirectoryPath() + "/controlpointsTarget.txt");
            QFileInfo infofiletarget(outputFilePath);
            QUrl urlfiletarget = QUrl::fromLocalFile(filepath);
            QFile filetarget(urlfiletarget.path());
            if (infofiletarget.exists())
                filetarget.remove();

            KrigingTargetGenerator myGenerator;

            myGenerator.compute(QString::fromStdString(scriptpath),
                QString::fromStdString(m_currentControlPointSource),
                filepath, outputFilePath);

            // read target in outputfile and make association
            if (!infofiletarget.exists()) {
                pCritical() << "Target control point file is not created: can not be loaded!";
                return;
            }
            std::string source = m_currentControlPointSource;
            std::string target = m_currentControlPointSource + "_target";
            doLoadTargetPoint(outputFilePath.toStdString(), target);
            setAssoSourceTarget(QString::fromStdString(target), QString::fromStdString(source));

            pInfo() << DONE;
        }

        void KrigingModule::exportControlPointsTarget(QString const& controlPointName, QUrl const& filepath, bool exportIDs)
        {
            std::string cpName = controlPointName.toStdString();
            if (cpName == "") {
                pCritical() << "No control points are selected: points not exported!";
                return;
            }
            piper::hbm::HumanBodyModel& hbm = Context::instance().project().model();
            piper::hbm::TargetList const& targetlist = Context::instance().project().target();
            for (auto const& curset : targetlist.controlPoint)
            {
                if (curset.name() == cpName) // find the matched target
                {
                    QString path = filepath.toLocalFile();
                    QFileInfo infofile(path);
                    QFile file(filepath.path());
                    if (infofile.exists())
                        file.remove();
                    //Stream declaration to write in a csv file
                    ofstream myfile;
                    //create the csv input file in the piper temp folder
                    myfile.open(path.toStdString());

                    std::vector<Coord> const& set = curset.value();
                    if (exportIDs)
                    {
                        int count = 1; // give IDs to the non-FE control points after the FE ones
                        for (auto const& coord : set)
                            myfile << count++ << " " << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                    }
                    else
                    {
                        for (auto const& coord : set)
                            myfile << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                    }
                    myfile.close();
                    return;
                }
            }
            pCritical() << "Control point set \"" << cpName << "\" not found.";
        }

        void KrigingModule::exportControlPointsSource(QString const& controlPointName, QUrl const& filepath, bool exportIDs)
        {
            std::string cpName = controlPointName.toStdString();
            if (cpName == "")
                cpName = m_currentControlPointSource;
            if (cpName == "") {
                pCritical() << "No control points are selected: points not exported!";
                return;
            }
            piper::hbm::HumanBodyModel& hbm = Context::instance().project().model();
            InteractionControlPoint *set = hbm.metadata().interactionControlPoint(cpName);
            if (set)
            {
                QString path = filepath.toLocalFile();
                QFileInfo infofile(path);
                QFile file(filepath.path());
                if (infofile.exists())
                    file.remove();
                //Stream declaration to write in a csv file
                ofstream myfile;
                //create the csv input file in the piper temp folder
                myfile.open(path.toStdString());

                VId vid = set->getGroupNodeId();
                int maxID = 0;
                for (auto const& id : vid)
                {
                    Coord const& coord = hbm.fem().getNode(id).get();
                    if (exportIDs)
                    {
                        IdKey idfe;
                        std::string key;
                        hbm.fem().getInfoId<Node>(id, key, idfe);
                        myfile << idfe.id << " " << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                        if (idfe.id > maxID)
                            maxID = idfe.id;
                    }
                    else
                        myfile << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                }
                VCoord const& vcoord = set->getControlPt3dCont();
                if (exportIDs)
                {
                    maxID++; // give IDs to the non-FE control points after the FE ones
                    for (auto const& coord : vcoord)
                        myfile << maxID++ << " " << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                }
                else
                {
                    for (auto const& coord : vcoord)
                        myfile << coord[0] << " " << coord[1] << " " << coord[2] << std::endl;
                }

                myfile.close();
            }
            else
                pCritical() << "Control point set \"" << cpName << "\" not found.";
        }

        void KrigingModule::setAsSkinGlobal(QString const& name, QVariant const& value)
        {
            InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
            if (set)
            {
                double as_skin = value.toDouble();
                if (set->getAssociationSkinGlobal() != as_skin) // only update if this actually changes something
                {
                    set->setGlobalAssociationSkin(as_skin);
                    auto entry = m_controlPointsSource.find(name.toStdString());
                    if (entry != m_controlPointsSource.end())
                    {
                        model_controlPointsSource->items[model_controlPointsSource->get(name).toInt()].m_as_skin = as_skin;
                        entry->second.m_as_skin = as_skin;
                    }
                    m_kriging.resetKrigingSources();
                }
            }
        }

        void KrigingModule::setAsBonesGlobal(QString const& name, QVariant const& value)
        {
            InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
            if (set)
            {
                double as_bones = value.toDouble();
                if (set->getAssociationBonesGlobal() != as_bones) // only update if this actually changes something
                {
                    set->setGlobalAssociationBones(as_bones);
                    auto entry = m_controlPointsSource.find(name.toStdString());
                    if (entry != m_controlPointsSource.end())
                    {
                        model_controlPointsSource->items[model_controlPointsSource->get(name).toInt()].m_as_bones = as_bones;
                        entry->second.m_as_bones = as_bones;
                    }
                    m_kriging.resetKrigingSources();
                }
            }
        }

        void KrigingModule::setCPSetNugget(QString const& name, QVariant const& value)
        {
            InteractionControlPoint *set = Context::instance().project().model().metadata().interactionControlPoint(name.toStdString());
            if (set)
            {
                double nugget = value.toDouble();
                if (set->getGlobalWeight() != nugget) // only update if this actually changes something
                {
                    set->setGlobalWeight(nugget);
                    auto entry = m_controlPointsSource.find(name.toStdString());
                    if (entry != m_controlPointsSource.end())
                    {
                        model_controlPointsSource->items[model_controlPointsSource->get(name).toInt()].m_nugget = nugget;
                        entry->second.m_nugget = nugget;
                    }
                    m_kriging.resetKrigingSources();
                }
            }
        }
    }
}
