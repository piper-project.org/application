// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2

import piper.Kriging 1.0
import Piper 1.0
import VtkQuick 1.0

ModuleLayout
{
    id: root
    anchors.fill: parent

    content: DefaultVtkViewer { }

    toolbar:
    ColumnLayout
    {
        anchors.fill: parent

        GroupBox
        {
            id: moduletools
            anchors.margins: itemMargin
            Layout.fillWidth: true
            title: qsTr("Control Points")
            anchors.top: parent.top

            Connections
            {
                target: context
                onModelUpdated: contextVtkDisplay.Refresh();
            }

            ColumnLayout
            {
                anchors.fill: parent
                ModuleToolWindowButton
                {
                    id: controlPointsButton
                    text:"\&Source Points"
                    toolWindow: sourcePoints
                    shortcutChar: "C"
                    checkable: true
                }

                ModuleToolWindowButton
                {
                    id: targetPointsButton
                    text:"\&Target Points"
                    toolWindow: targetPoints
                    shortcutChar: "T"
                    checkable: true
                }
            }
        }

        ModuleToolWindowButton {
            id: registrationButton
            text:"Register Surfaces"
            shortcutChar: 'R'
            toolWindow: registersurfaces
        }

        ModuleToolWindowButton {
            id: krigingdeformationButton
            text:"Perform Deformation"
            shortcutChar: 'P'
            toolWindow: krigingdeformation
        }

        
        TargetPoints
        {
            id: targetPoints
            sizeWidth: 500
            tableHeight: 180
        }

        SourcePoints
        {
            id: sourcePoints
            sizeWidth: 500
            tableHeight: 180
        }
        Item
        {
            KrigingModule
            {
                id: krigingmodule
            }
            Component.onCompleted: {
                sourcePoints._kriging = krigingmodule
                targetPoints._kriging = krigingmodule
                if (krigingAutoLoaControlPointsName !== "none") {
                    controlPointsButton.checked=true
                    targetPointsButton.checked=true
                }
                krigingmodule.InitModuleData(krigingAutoLoaControlPointsName);
                krigingAutoLoaControlPointsName="none"
            }
            Connections
            {
                target: context
                onVisDataLoaded: {
                    if (krigingAutoLoaControlPointsName !== "none") {
                        controlPointsButton.checked=true
                        targetPointsButton.checked=true
                    }
                    krigingmodule.InitModuleData(krigingAutoLoaControlPointsName);
                    krigingAutoLoaControlPointsName="none"
                }
            }
        }        
		ColumnLayout
		{
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 10
			DetachableVtkViewerCommonTools
			{
			}
        }

        Krigingdeformation{
            id: krigingdeformation
            allowIntermediate: false
		}

        RegisterSurfaces
        {
            id: registersurfaces
        }

        Component.onCompleted:
        {
            krigingdeformation.krigingIntrfc = krigingmodule.kriging
        }
    }

}
