// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0




ColumnLayout {
    width:parent.width
    id: myComboTable
    property variant internalComboList
    property variant internalTableModel
    property variant internalComboSourceListModel
    property int tableHeight
    property int totalWidth

    signal addInTable(string  comboItem)
    signal setCurrentItem(string tableItem)
    signal removeCurrentItem(string tableItem)
    signal setAsso(string  tableItem, string  sourceComboItem)
    signal removeAsso(string  tableItem)
    signal hideShowCurrentItem(string tableItem)
    signal exportPoints(string tableItem)

    property real indexTable: -1

    onIndexTableChanged: {
        myTable.selection.clear()
        if (indexTable === -1) {
            myTable.currentRow = indexTable
        }
        else
            myTable.selection.select(indexTable, indexTable)
    }

    height: parent.height

    ComboBox {
        id: myCombo
        Layout.preferredWidth: totalWidth
        Layout.fillWidth : true
        model: parent.internalComboList
        onCurrentIndexChanged: {
            if (currentIndex >0) {
                myComboTable.addInTable(myCombo.currentText)
            }
            myCombo.currentIndex=0
        }
    }

    TableView {
        id: myTable
        Layout.preferredHeight: tableHeight
        Layout.preferredWidth: totalWidth
        Layout.fillWidth : true
        Layout.fillHeight : true
        anchors.top: myCombo.bottom
        anchors.bottom: myComboTable.bottom
        model: parent.internalTableModel
        headerVisible: true
        selectionMode: SelectionMode.SingleSelection
        currentRow: parent.indexTable
        TableViewColumn {
            role: "NameRole"
            title: "Name"
            movable: false
            resizable: true
        }
        TableViewColumn {
            role: "NbRole"
            title: "Count"
            movable: false
            resizable: false
        }
        TableViewColumn {
            role: "AssocRole"
            title: "Source"
            movable: false
            resizable: true
            delegate: Item {
                ComboBox {
                    id: sourceCombo
                    enabled: false
                    anchors.fill:parent
                    model: internalComboSourceListModel
                    Component.onCompleted: {
                        currentIndex= find(myTable.model.getSource(styleData.row))
                        if (myTable.currentRow > -1 && myTable.currentRow === styleData.row)
                            sourceCombo.enabled=true
                    }
                    onActivated: {
                        if (index === 0) {
                            removeAsso(myTable.model.get(myTable.currentRow))
                        }
                        else if (index > 0) {
                            setAsso(myTable.model.get(myTable.currentRow), sourceCombo.textAt(index))
                        }
                    }
                    Connections {
                        target: myTable
                        onCurrentRowChanged: {
                            if (myTable.currentRow > -1 && myTable.currentRow === styleData.row)
                                sourceCombo.enabled=true
                            else
                                sourceCombo.enabled=false
                        }
                    }
                }
            }
        }
        onCurrentRowChanged:  {
            parent.indexTable = currentRow
            if (currentRow > -1)
                myComboTable.setCurrentItem(model.get(currentRow))
        }

        Menu { 
            id: contextMenu
            MenuItem {
                action: removeAction
            }
            MenuItem {
                action: hideAction
            }
            MenuItem {
                action: exportAction
            }
        }
        rowDelegate: Item {
            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }//anchors
                height: parent.height
                color: styleData.selected ? 'lightblue' : 'white'
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.RightButton
                    propagateComposedEvents: true
                    onReleased: {
                        if (typeof styleData.row === 'number') {
                            myTable.currentRow = styleData.row
                            if (mouse.button === Qt.RightButton) { // never true
                                contextMenu.popup()
                            }//if
                        }//if
                        mouse.accepted = false
                    }//onReleased
                }//MouseArea
            }//Rectangle
        }//rowDelegate
    }
    Action {
        id: removeAction
        text: qsTr("Remove")
        onTriggered: {
            myComboTable.removeCurrentItem(myTable.model.get(myTable.currentRow))
            myTable.selection.clear()
        }
    }

    Action {
        id: hideAction
        text: qsTr("Hide/Show")
        onTriggered: {
            myComboTable.hideShowCurrentItem(myTable.model.get(myTable.currentRow))
        }
    }

    Action {
        id: exportAction
        text: qsTr("Export")
        onTriggered: {
            myComboTable.exportPoints(myTable.model.get(myTable.currentRow))
        }
    }
}

