/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "common/Context.h"
#include "hbm/Metadata.h"

#include <QObject>
#include <QString>

namespace piper {

/** @brief Check module.
 *
 * @author Thomas Lemaire @date 2014
 * \todo check for duplicated landmarks and entities (synonym)
 * \todo check for duplicated entities (part of)
 * \todo make highlight word case agnostic
 */
class Check : public QObject
{
    Q_OBJECT

public:
    Check();
    QString info() const;

public slots:
    void check(bool anatomicalFrameOption, bool verbose);
    void setDirty() {m_isDirty=true;}
    void highlight(QString word);

signals:
    void infoAppend(QString message);
    void infoChanged(QString message);

private:

    void doCheck(bool anatomicalFrameOption, bool verbose);
    /// publish info string in small chuck to work around a display bug
    void publishInfo(QString info);

    QString m_info;
    QString m_searchedWord;
    bool m_isDirty;

};


} // namespace piper
