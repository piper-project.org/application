/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Check.h"

#include <set>
#include <functional>

#include <QDebug>
#include <QString>
#include <QRegularExpression>
#include <QTextStream>
#include <QFileInfo>
#include <QFile>
#include <QProcess>
#include <QCoreApplication>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include "common/helper.h"
#include <tinyxml/tinyxml2.h>
#include "common/message.h"
#include "common/logging.h"
#include "common/Project.h"
#include "common/XMLValidationProcess.h"
#include "common/Context.h"

#include "hbm/Contour.h"
#include "common/OctaveProcess.h"

#include "anatomyDB/query.h"
#include "anatomyDB/FrameFactory.h"

namespace piper {

using namespace hbm;

Check::Check() :
    m_isDirty(true)
{
    connect(&Context::instance(), &Context::modelChanged,  this, &Check::setDirty);
    connect(&Context::instance(), &Context::targetChanged, this, &Check::setDirty);
    connect(&Context::instance(), &Context::envChanged,    this, &Check::setDirty);
}

QString Check::info() const
{
    return m_info;
}

void Check::check(bool anatomicalFrameOption, bool verbose)
{
    if (!m_isDirty) return;
    void (*doCheckWrapper)(piper::Check*, void (Check::*)(bool, bool), bool, bool)
            = &piper::wrapClassMethodCall<piper::Check, void (piper::Check::*)(bool, bool), bool, bool>;
    Context::instance().performAsynchronousOperation(std::bind(doCheckWrapper, this,
                    &Check::doCheck, anatomicalFrameOption, verbose),
                    false, false, false, false, false);
}

void Check::doCheck(bool anatomicalFrameOption, bool verbose)
{
    pInfo() << START <<  QStringLiteral("Checking project...");
    m_info.clear();
    QTextStream out(&m_info, QIODevice::WriteOnly);
    hbm::HumanBodyModel & model = Context::instance().project().model(); // TODO: should be a const reference
    if (!model.empty()) {
        QFileInfo source(QString::fromStdString(model.metadata().sourcePath()));
        if (source.exists()) {
            std::string format = model.metadata().getNameFormat();
            out << SUCCESS << "Source model [" << QString::fromStdString(format) << "]: " << source.filePath() << endl;
        }
        else
            out << FAILEDMSG <<  "Source model: " << source.filePath() << " does not exist" << endl;

        // units
        out << INFO << "Units -"
            << " length: " << QString::fromStdString(units::LengthUnit::unitToStr(model.metadata().lengthUnit()))
            << " weight: " << QString::fromStdString(units::MassUnit::unitToStr(model.metadata().massUnit()))
            << " age: " << QString::fromStdString(units::AgeUnit::unitToStr(model.metadata().ageUnit())) << endl;

        if (model.metadata().isGravityDefined()) {
            Coord const& g = model.metadata().gravity();
            out << INFO << "Gravity: [" << g(0) << "," << g(1) << "," << g(2) << "]" << endl; // TODO [TL] find a way to send Eigen::Matrix to the log, add the proper operator in logging.h
        }
        else
            out << INFO << "Gravity: undefined " << endl; // [TL] TODO shouldn't it be a warning ?

        // hbm anthropo
        if (model.metadata().anthropometry().age.isDefined())
            out << INFO << "Age : " << model.metadata().anthropometry().age.value() << " " << QString::fromStdString(units::AgeUnit::unitToStr(model.metadata().ageUnit())) << endl;
        else
            out << INFO << "Hbm Age is not defined." << endl;
        if (model.metadata().anthropometry().weight.isDefined())
            out << INFO << "Hbm Weight : " << model.metadata().anthropometry().weight.value() << " " << QString::fromStdString(units::MassUnit::unitToStr(model.metadata().massUnit())) << endl;
        else
            out << INFO << "Hbm Weight is not defined." << endl;
        if (model.metadata().anthropometry().height.isDefined())
            out << INFO << "Hbm Height : " << model.metadata().anthropometry().height.value() << " " << QString::fromStdString(units::LengthUnit::unitToStr(model.metadata().lengthUnit())) << endl;
        else
            out << INFO << "Hbm Height is not defined." << endl;

        // build a set of entities ids to ease subsequent checks
        std::set<unsigned int> entitiesId;

        // entities
        std::list<std::string> boneList;
        std::list<std::string> skinList;
        std::list<std::string> otherEntityList;
        std::list<std::string> unknownEntityList;
        std::list<std::string> envelopIssueEntityList;
        for(auto const& name_entity : model.metadata().entities()) {
            if (!anatomydb::exists(name_entity.first) || !anatomydb::isAnatomicalEntity(name_entity.first))
                unknownEntityList.push_back(name_entity.first);
            else {
                entitiesId.insert(anatomydb::getEntityId(name_entity.first));
                if (anatomydb::isEntitySubClassOf(name_entity.first, "Bone"))
                    boneList.push_back(name_entity.first);
                else if (anatomydb::isEntitySubClassOf(name_entity.first, "Skin"))
                    skinList.push_back(name_entity.first);
                else
                    otherEntityList.push_back(name_entity.first);
            }
        }
        if (!boneList.empty()) {
            out << INFO << "Entities - Bones (" << boneList.size() << "): ";
            for (std::string const& name: boneList)
                out << QString::fromStdString(name) << ", ";
            out << endl;
        }
        if (!skinList.empty()) {
            out << INFO << "Entities - Skin (" << skinList.size() << "): ";
            for (std::string const& name: skinList)
                out << QString::fromStdString(name) << ", ";
            out << endl;
        }
        else
            out << WARNING << "No skin is defined in the model" << endl;
        if (!otherEntityList.empty()) {
            out << INFO << "Entities - Others (" << otherEntityList.size() << "): ";
            for (std::string const& name: otherEntityList)
                out << QString::fromStdString(name) << ", ";
            out << endl;
        }
        if (!unknownEntityList.empty()) {
            out << WARNING << "Entities - Unknown (" << unknownEntityList.size() << "): ";
            for (std::string entity : unknownEntityList )
                out << QString::fromStdString(entity) << ", ";
            out << endl;
        }

        if (verbose) {
            if (boneList.size() > 0) {
                for (auto const& bone : boneList) {
                    model.metadata().entity(bone).getEnvelopElements(model.fem());
                    if (!model.metadata().entity(bone).isEnvelopValid)
                        envelopIssueEntityList.push_back(bone);
                }
            }
            if (!envelopIssueEntityList.empty()) {
                out << WARNING << "Entities with envelop issue (" << envelopIssueEntityList.size() << "): ";
                for (std::string entity : envelopIssueEntityList)
                    out << QString::fromStdString(entity) << ", ";
                out << endl;
            }
        }

        // landmarks
        out << INFO << "Landmarks ("<< model.metadata().landmarks().size() << "): ";
        std::list<std::string> unknownLandmarkList;
        std::list<std::string> landmarkWithNoEntity;
        Metadata::LandmarkCont const& landmarks = model.metadata().landmarks();
        for(Metadata::LandmarkCont::const_iterator it = landmarks.begin() ; it != landmarks.end() ; ++it) {
            out << QString::fromStdString(it->second.name()) << ", ";
            if (!anatomydb::exists(it->second.name()) || !anatomydb::isLandmark(it->second.name()))
                unknownLandmarkList.push_back(it->second.name());
            else {
                std::vector<std::string> entityVec = anatomydb::getLandmarkBoneList(it->second.name());
                bool hasEntity = false;
                for (std::string entity : entityVec) {
                    if (entitiesId.count(anatomydb::getEntityId(entity))>0) {
                        hasEntity = true;
                        break;
                    }
                }
                if (!hasEntity)
                    landmarkWithNoEntity.push_back(it->second.name());
            }
        }
        out << endl;
        if (!unknownLandmarkList.empty()) {
            out << WARNING << "Unknown landmarks: ";
            for (std::string landmark : unknownLandmarkList)
                out << QString::fromStdString(landmark) << ", ";
            out << endl;
        }
        if (!landmarkWithNoEntity.empty()) {
            out << WARNING << "Model has no entity defined for landmarks: ";
            for (std::string landmark : landmarkWithNoEntity)
                out << QString::fromStdString(landmark) << ", ";
            out << endl;
        }
        // TODO: check landmarks: sphere 3+nodeId / nodeId exists

        if (anatomicalFrameOption) {
            bool haveAnatomicalFrame=false;
            // anatomical frames
            out << INFO << "Available anatomical frames: ";
            int warningOccured = false; // flag to add endl char when necessary
            anatomydb::LandmarkCont & landmark = anatomydb::FrameFactory::instance().landmark();
            landmark.clear();
            for (auto const& name_landmark: model.metadata().landmarks()) {
                try {
                    landmark.add(name_landmark.second.name(), name_landmark.second.position(model.fem()));
                }
                catch(std::runtime_error const& e) {
                    out << endl << WARNING << e.what();
                    warningOccured = true;
                }
            }
            std::string message;
            for(std::string const& frameName : anatomydb::getSubClassOfFromBibliographyList("Frame","ISB_BCS")) {
                if (anatomydb::FrameFactory::instance().canComputeFrame(frameName, message)) {
                    if (warningOccured)
                        out << endl;
                    warningOccured = false;
                    out << QString::fromStdString(frameName) << ", ";
                    haveAnatomicalFrame=true;
                }
                else if (verbose) {
                    out << endl << WARNING << QString::fromStdString(message);
                    warningOccured = true;
                }
            }
            if (!haveAnatomicalFrame) {
                if (warningOccured)
                    out << endl;
                out << "None";
            }
            out << endl;
        }

		// joints
        if (model.metadata().joints().size() >0 ) {
            out << INFO << "Joints (" << model.metadata().joints().size() << "): ";
            bool warningOccured = false; // flag to add endl char when necessary
            for (auto const& id_joint: model.metadata().joints()) {
                if (warningOccured)
                    out << endl;
                warningOccured = false;
                out << QString::fromStdString( id_joint.second.name());
                if (hbm::BaseJointMetadata::ConstrainedDofType::SOFT == id_joint.second.getConstrainedDofType())
                    out << " (SOFT)";
                out << ", ";
                if (model.metadata().entities().find(id_joint.second.getEntity1()) == model.metadata().entities().end()) {
                    out << endl << WARNING << "Entity " << QString::fromStdString(id_joint.second.getEntity1()) << " does not exist";
                    warningOccured=true;
                }
                if (model.metadata().entities().find(id_joint.second.getEntity2()) == model.metadata().entities().end()) {
                    out << endl << WARNING << "Entity " << QString::fromStdString(id_joint.second.getEntity2()) << " does not exist";
                    warningOccured=true;
                }
            }
        }
        else
            out << INFO << "No joint";
        out << endl;

        // anatomical joints
        if (model.metadata().anatomicalJoints().size() >0 ) {
            out << INFO << "Anatomical Joints (" << model.metadata().anatomicalJoints().size() << "): ";
            bool warningOccured = false; // flag to add endl char when necessary
            for (auto const& id_joint: model.metadata().anatomicalJoints()) {
                if (warningOccured)
                    out << endl;
                warningOccured = false;
                out << QString::fromStdString( id_joint.second.name()) << ", ";
                if (!anatomydb::exists(id_joint.second.name()) || !anatomydb::isEntitySubClassOf(id_joint.second.name(),"Joint")) {
                    out << endl << WARNING << "Unknown anatomical joint " << QString::fromStdString(id_joint.second.name());
                    warningOccured=true;
                }
                else {
                    // TODO need for a recursive version of getSubPartOfList for this check
//                    for (std::string const& entity: anatomydb::getSubPartOfList(id_joint.second.name(), "Bone"))
//                        if (entitiesId.count(anatomydb::getEntityId(entity))==0) {
//                            out << endl << WARNING
//                                << "Anatomical joint: " << QString::fromStdString(id_joint.second.name())
//                                << " missing entity " << QString::fromStdString(entity) ;
//                            warningOccured=true;
//                        }
                    if (anatomicalFrameOption) {
                        std::string message;
                        for(std::string const& frame : anatomydb::getSubPartOfList(id_joint.second.name(), "Frame"))
                            if (!anatomydb::FrameFactory::instance().canComputeFrame(frame, message)) {
                                out << endl << WARNING
                                    << "Anatomical joint: " << QString::fromStdString(id_joint.second.name())
                                    << QString::fromStdString(message);
                                warningOccured=true;
                            }
                    }
                }
            }
        }
        else
            out << INFO << "No anatomical joint";
        out << endl;

        if (anatomicalFrameOption) {
            anatomydb::FrameFactory::instance().landmark().clear();
        }

        // contacts
        if (model.metadata().contacts().size() > 0) {
            bool warningOccured = false; // flag to add endl char when necessary
            out << INFO << "Contacts (" << model.metadata().contacts().size() << "): ";
            const Metadata::ContactCont& contacts = model.metadata().contacts();
            for (Metadata::ContactCont::const_iterator it = contacts.begin() ; it != contacts.end() ; ++it) {
                if (warningOccured)
                    out << endl;
                warningOccured = false;
                out << QString::fromStdString( it->second.name()) << " (" << QString::fromStdString(hbm::EntityContactType_str[(unsigned int)it->second.type()]) << ":" <<
                    QString::fromStdString(it->second.entity1()) << " - " <<
                    QString::fromStdString(it->second.entity2()) << "), ";
                if (model.metadata().entities().find(it->second.entity1()) == model.metadata().entities().end()) {
                    out << endl << WARNING << "Entity " << QString::fromStdString(it->second.entity1()) << " does not exist";
                    warningOccured=true;
                }
                else if (hbm::ID_UNDEFINED != it->second.group1()) {
                    if (!model.fem().getGroupNodes().isValidId(it->second.group1())) {
                        out << endl << WARNING << "Group " << it->second.group1() << " does not exist";
                        warningOccured=true;
                    }
                    else {
                        hbm::VId entityNodeId = model.metadata().entity(it->second.entity1()).getEntityNodesIds(model.fem());
    //                    pDebug() << it->second.entity1() << " " << it->second.group1() << " " << model.fem().infoId<hbm::GroupNode>(it->second.group1());
                        for (hbm::Id nodeId: model.fem().getGroupNode(it->second.group1()).get()) {
    //                        pDebug() << nodeId << " " << model.fem().infoId<hbm::Node>(nodeId);
                            if (std::find(entityNodeId.begin(), entityNodeId.end(), nodeId) == entityNodeId.end()) {
                                out << endl << WARNING << "Node " << nodeId << " does not belong to entity " << QString::fromStdString(it->second.entity1());
                                warningOccured=true;
                            }
                        }
                    }
                }
                if (model.metadata().entities().find(it->second.entity2()) == model.metadata().entities().end()) {
                    out << endl << WARNING << "Entity " << QString::fromStdString(it->second.entity2()) << " does not exist";
                    warningOccured=true;
                }
                else if (hbm::ID_UNDEFINED != it->second.group2()) {
                    if (!model.fem().getGroupNodes().isValidId(it->second.group2())) {
                        out << endl << WARNING << "Group " << it->second.group2() << " does not exist";
                        warningOccured=true;
                    }
                    else {
                        hbm::VId entityNodeId = model.metadata().entity(it->second.entity2()).getEntityNodesIds(model.fem());
    //                    pDebug() << it->second.entity2() << " " << it->second.group2() << " " << model.fem().infoId<hbm::GroupNode>(it->second.group2());
                        for (hbm::Id nodeId: model.fem().getGroupNode(it->second.group2()).get()) {
    //                        pDebug() << nodeId << " " << model.fem().infoId<hbm::Node>(nodeId);
                            if (std::find(entityNodeId.begin(), entityNodeId.end(), nodeId) == entityNodeId.end()) {
                                out << endl << WARNING << "Node " << nodeId << " does not belong to entity " << QString::fromStdString(it->second.entity2());
                                warningOccured=true;
                            }
                        }
                    }
                }
            }
        }
        else
            out << INFO << "No contact";
        out << endl;

        // Named Metadata
        if (model.metadata().genericmetadatas().size() > 0) {
            const Metadata::GenericMetadataCont& genericMetadatas = model.metadata().genericmetadatas();
            out << INFO << "Named metadata (" << genericMetadatas.size() << "): ";
            for (Metadata::GenericMetadataCont::const_iterator it = genericMetadatas.begin() ; it != genericMetadatas.end() ; ++it) {
                out << QString::fromStdString( it->second.name());
                if (verbose) {
                    out << "(" << it->second.getNodeIdVec(model.fem()).size() << ")";
                }
                out << ", ";
            }
        }
        else
            out << INFO << "No named metadata";
        out << endl;

        // Control points
        out << INFO << "ControlPoints: ";

        Metadata::InteractionControlPointCont & ctrlPts = model.metadata().interactionControlPoints();
        for(auto it = ctrlPts.begin() ; it != ctrlPts.end() ; ++it) {
            out << "[name : " <<
                QString::fromStdString(it->second.name()) << ", role: " <<
                QString(it->second.getControlPointRoleStr().c_str()) << ", total number of control points: "
                << it->second.getNbControlPt() << ", ";
            if (!std::isnan(it->second.getGlobalWeight()))
                out << "global weight " << it->second.getGlobalWeight() << " set;";
            else if (it->second.getWeight().size() > 0)
                out << " individual weights set; ";
            else
                out << " no weights set;";
            out << "], ";
        }
        // TODO: check landmarks: sphere 3+nodeId / nodeId exists
        out << endl;

        //hbm paremters
        out << INFO << "HBM Parameters (" << model.metadata().hbmParameters().size() << "): ";
        const Metadata::HbmParameterCont& params = model.metadata().hbmParameters();

        for (Metadata::HbmParameterCont::const_iterator it = params.begin(); it != params.end(); ++it) {
            out << QString::fromStdString(it->second.name()) << ", ";
        }
        out << endl;

        // body sections
        size_t nb_segments = Context::instance().project().anthropoModel().listAnthropoBodySegment().size();
        size_t nb_sections = Context::instance().project().anthropoModel().listAnthropoBodySection().size();
        size_t nb_dims = Context::instance().project().anthropoModel().listAnthropoBodyDimension().size();
        out << INFO << "Anthropometric Model :" << " Number Body Segments: " << nb_segments << " | Number Body Sections: " << nb_sections << " | Number Body Dimensions: " << nb_dims << endl;;

        // FE model
        out << INFO << "FE model: " << endl;
        FEModel const& fem = model.fem(); //shortcut
        out << "Nb nodes: " << fem.getNbNode()
            << " | elements1D: " << fem.getNbElement1D()
            << " | elements2D: " << fem.getNbElement2D()
            << " | elements3D: " << fem.getNbElement3D()
            << " |" << endl;
        out << "Nb groups of  nodes: " << fem.getNbGroupNode()
            << " | elements1D: " << fem.getNbGroupElements1D()
            << " | elements2D: " << fem.getNbGroupElements2D()
            << " | elements3D: " << fem.getNbGroupElements3D()
            << " | groups: " << fem.getNbGroupGroups()
            << " |" << endl;
        if (fem.getNbFrames() > 0) {
            out << "Nb frames: " << fem.getNbFrames()
            << " |" << endl;
        }



        // Environement
        hbm::EnvironmentModels const& environment = Context::instance().project().environment(); // TODO: should be a const reference
        std::vector<std::string> list_envmodels = environment.getListNames();
        if (list_envmodels.size() > 0) {
            out << INFO << "Environment models: " <<  endl;
            for (std::vector<std::string>::const_iterator it=list_envmodels.begin(); it!=list_envmodels.end(); ++it) 
            {
                EnvironmentModelPtr model = environment.getEnvironmentModel(*it);
                out << QString::fromStdString(model->name()) << " "
                    << QString::fromStdString(model->file()) << " ";
                FEModel const& env = model->envmodel();
                out << "(Nb nodes: " << env.getNbNode()
                    << " | elements: " << env.getNbElement1D() + env.getNbElement2D() + env.getNbElement3D()
                    << " | Unit length: " << QString::fromStdString(units::LengthUnit::unitToStr(model->lengthUnit()))
                    << " )" << endl;
            }
        }
        else
            out << INFO << "No environment"  << endl;
    }
    else
        out << INFO << "No model" << endl;

    // Target
    if (!Context::instance().project().target().empty()) {
        out << INFO << "Targets : [";
        TargetList const& targetList = Context::instance().project().target();
        //
        out << "anthropometricDimension (" << targetList.anthropometricDimension.size() << ") ";
        if (verbose)
            for (hbm::AnthropometricDimensionTarget const& target : targetList.anthropometricDimension)
                out << " - " << QString::fromStdString(target.name()) << "(" << target.value() << ")";
        out << "] [";
        //
        out << "fixedBone (" << targetList.fixedBone.size() << ") ";
        if (verbose)
            for (hbm::FixedBoneTarget const& target : targetList.fixedBone)
                out << " - " << QString::fromStdString(target.name());
        out << "] [";
        //
        out << "landmark (" << targetList.landmark.size() << ") ";
        if (verbose)
            for (hbm::LandmarkTarget const& target : targetList.landmark)
                out << " - " << QString::fromStdString(target.name());
        out << "] [";
        //
        out << "joint (" << targetList.joint.size() << ") ";
        if (verbose)
            for (hbm::JointTarget const& target : targetList.joint)
                out << " - " << QString::fromStdString(target.name());
        out << "] [";
        //
        out << "frameToFrame (" << targetList.frameToFrame.size() << ") ";
        if (verbose)
            for (hbm::FrameToFrameTarget const& target : targetList.frameToFrame)
                out << " - " << QString::fromStdString(target.name());
        out << "] [";
        //
        out << "scalingparameter (" << targetList.scalingparameter.size() << ") ";
        if (verbose)
            for (hbm::ScalingParameterTarget const& target : targetList.scalingparameter)
                out << " - " << QString::fromStdString(target.name()) << "(" << target.value().size()  << " values)";
        out << "] [";
        //
        out << "controlPoint (" << targetList.controlPoint.size() << ") ";
        if (verbose)
            for (hbm::ControlPoint const& target : targetList.controlPoint)
                out << " - " << QString::fromStdString(target.name()) << "(" << target.value().size() << ")";
        out << "]";
        //
        if (!targetList.age.empty())
            out << " [Age - " << QString::fromStdString(targetList.age.front().name()) << "(" << targetList.age.front().value() << ")]";
        if (!targetList.weight.empty())
            out << " [Weight - " << QString::fromStdString(targetList.weight.front().name()) << "(" << targetList.weight.front().value() << ")]";
        if (!targetList.height.empty())
            out << " [Height - " << QString::fromStdString(targetList.height.front().name()) << "(" << targetList.height.front().value() << ")]";

        out << endl;
    }
    else
        out << INFO << "No target" << endl;

    // print out the info by smaller parts because the TextArea seems to be unable to handle long lines
    publishInfo(m_info);
    m_isDirty = false;

    pInfo() << DONE;
}

void Check::highlight(QString word)
{
    bool doHighlight = true;
    if (0 == word.length())
        doHighlight = false;
    static QString nbsp("nbsp"); // workaround for html non breaking space entity
    if (-1 != nbsp.indexOf(word))
        doHighlight = false;
    if (!doHighlight) {
        publishInfo(m_info); // necessary to reset the display
        return;
    }

    QString highlightedInfo = m_info;
    highlightedInfo.replace(QRegularExpression(word + "(?!([^<]+)?>)"), "<font style=\"background-color: yellow\">" + word + "</font>");
    publishInfo(highlightedInfo);
}

void Check::publishInfo(QString info)
{
    static const int chunkSize = 2048;
    // infoAppend appends on a new line, continue splitting on <br/> to prevent html tags from being split on 2 lines
    QStringList splitted = info.split("<br/>");
    infoChanged("");
    for (QString s : splitted) {
        if (s.size() < chunkSize)
            infoAppend((s + "<br/>"));
        else {
            int current_index = 0;
            while (current_index < s.size()) {
                int next_index = s.indexOf(",", current_index+chunkSize);
                if (-1 == next_index)
                    next_index = current_index+chunkSize;
                else
                    next_index += 1; // infoAppend appends on a new line, to keep the ',' on the same line
                infoAppend(s.mid(current_index, next_index-current_index));
                current_index=next_index;
            }
        }
    }
}

}
