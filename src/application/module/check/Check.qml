// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0
import piper.Check 1.0
import VtkQuick 1.0

import "../sofaDisplay"

ModuleLayout 
{
    id: root
    anchors.fill: parent

    Check 
    {
        id: myCheck
    }
    Component.onCompleted: 
    {
        visible = true;
    }
    Component 
    {
        id: diagnosticContent
        TextArea 
        {
            id: checkLog
            anchors.fill: parent
            textFormat: TextEdit.RichText
            readOnly: true
            text: ""
            Connections {
                target: myCheck
                onInfoAppend : {
                    checkLog.append(message);
                    // i don't know how to set it to the end of the log without getting qml warnings about cursor position out of range after highlighting is done
                    // now the scrollview is scrolled down while the text is not after initialization, but at least there are no warnings...feel free to fix it / TJ
                    checkLog.cursorPosition = 0;
                }
                onInfoChanged: {
                    checkLog.text = message;
                    checkLog.cursorPosition = 0;
                }
            }
        }
    }
    Component 
    {
        id: diagnosticToolbox
        ColumnLayout {
            Action {
                id: refreshAction
                text: qsTr("Refresh")
                shortcut: StandardKey.Refresh
                onTriggered: {
                    myCheck.setDirty();
                    myCheck.check(anatomicalFrameOption.checked, verboseOption.checked);
                    myCheck.highlight(highlightText.text);
                }
            }
            Connections {
                target: context
                onOperationFinished: {
                    myCheck.check(anatomicalFrameOption.checked, verboseOption.checked);
                    myCheck.highlight(highlightText.text);
                }
            }

            TextField {
                id: highlightText
                Layout.fillWidth: true
                placeholderText: "search"
                onTextChanged: myCheck.highlight(text)
                ToolTip {
                    text: "Highlight the searched term in the diagnostic"
                }
            }
            CheckBox {
                id: anatomicalFrameOption
                text: "Anatomical frame"
                onCheckedChanged: refreshAction.trigger()
                ToolTip {
                    text: "List the anatomical frames that can be computed with the available landmarks"
                }
            }
            CheckBox {
                id: verboseOption
                text: "Verbose"
                onCheckedChanged: refreshAction.trigger()
                ToolTip {
                    text: "Output even more information"
                }
            }

            Component.onCompleted:
            {
                refreshAction.trigger();
            }
        }
    }

    Component {
        id: viewerContent
        DefaultVtkViewer { }
    }

    Component 
    {
        id: viewerToolbox
	
		DetachableVtkViewerCommonTools
		{
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 10
		}
		
    }
    
    content: diagnosticContent
    toolbar:
    ColumnLayout 
    {
        anchors.fill: parent

        GroupBox 
        {
            id: groupBox1
            Layout.fillWidth: true
            title: qsTr("Mode")

            ColumnLayout 
            {
                id: columnLayout1
                anchors.top: parent.top
                width: parent.width

                ExclusiveGroup 
                { 
                    id: displayModeGroup 
                }
              
                Button 
                {
                    id: buttonDiagnostic
                    Layout.fillWidth: true
                    text: qsTr("Diagnostic")
                    checkable: true
                    checked: true
                    exclusiveGroup: displayModeGroup
                    onCheckedChanged: 
                    {
                        if (checked) 
                        {
                            root.content = diagnosticContent
                            subModuleControl.sourceComponent = diagnosticToolbox
                            subModuleControl.anchors.bottom = undefined // reset the positioning used for viewerTools
                        }
                    }
                }
                Button 
                {
                    id: button3dView
                    Layout.fillWidth: true
                    text: qsTr("	  3D View    ")
                    checkable: true
                    exclusiveGroup: displayModeGroup
                    onCheckedChanged: 
                    {
                        if (checked) 
                        {
                            root.content = viewerContent
                            subModuleControl.sourceComponent = viewerToolbox
                            subModuleControl.anchors.bottom = subModuleControl.parent.bottom
                        }
                    }
                }
            }
        }
        Loader 
        {
            id: subModuleControl
            Layout.fillWidth: true
            sourceComponent: diagnosticToolbox
        }
        Item {
            Layout.fillHeight: true
        }    
    }
}
