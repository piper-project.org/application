/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QObject>
#include <QString>
#include <qlistwidget.h>


namespace piper {

/** @brief Display module.
 *
 * @author Thomas Dupeux @date 2015
 */
class SofaDisplay : public QObject
{
    Q_OBJECT

   

public:
    SofaDisplay();

    Q_INVOKABLE QStringList  SofaEntities(QObject* sofaScene);
    Q_INVOKABLE QStringList const elementTypes();
    Q_INVOKABLE void setSaveQualityPath(QObject* sofaScene,const QUrl& url);
    Q_INVOKABLE void setElementTypeActivated(QObject* sofaScene, const QString& elemType, const QVariant& state);
    Q_INVOKABLE void setItemActivated(QObject* sofaScene,QString const & item,QVariant const & state);
    Q_INVOKABLE void setItemProperty(QObject* sofaScene,QString const & item,QString const & property,QVariant const & value);
    Q_INVOKABLE void setQualityActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setQualityLightActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setQualityAlphaActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setElementDisplayActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setEdgesActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setWriteToFileActivated(QObject* sofaScene,QVariant const & state);
    Q_INVOKABLE void setEntitiesActivated(QObject* sofaScene,QVariant const & state) ;
    Q_INVOKABLE void setFullModelActivated(QObject* sofaScene,QVariant const & state) ;
    Q_INVOKABLE void setModelAndEntityItemProperty(QObject* sofaScene,QString const & item,QString const & property,QVariant const & value);
    Q_INVOKABLE void reinitModelAndEntityItem(QObject* sofaScene,QString const & item);
    Q_INVOKABLE void reinitItem(QObject* sofaScene,QString const & item);

    Q_INVOKABLE void setStepNeeded(QObject* sofaScene);
    Q_INVOKABLE void stepIfNeeded(QObject* sofaScene, QString const & component);

public slots:
  //  QString test(QObject* sofaScene);

signals:
    void testSig();

private:
    int itEntities;
    QStringList _sofaEntities;
    QStringList qelementTypes;

    bool setSofaComponentProperty(QObject* sofaScene,QString const& componentPath,QString const & property,QVariant const & state);
    bool getSofaComponentProperty(QObject* sofaScene,QString const& componentPath,QString const & property,QVariant & state);

    bool doReinitItem(QObject* sofaScene,QString const & componentPath);
    std::map<std::string, bool> componentsNeedingSofaStep;

    bool isSceneLoaded(QObject* sofaScene);
};


} // namespace piper
