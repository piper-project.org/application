// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

ScrollView {
    id: displayOptionsTab

    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
    Layout.fillWidth: true

    Component.onCompleted: {
        sofaScene.statusChanged.connect(updateListViewModelNSelection)
        sofaScene.statusChanged.connect(resetDisplayOptionsMenus)
	}

    Rectangle {
        Layout.fillWidth: true
        height:childrenRect.height

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            Rectangle {

                Layout.fillWidth: true
                height:childrenRect.height
                       
                GroupBox {
                    title: " "
                    Layout.fillWidth: true
                    // anchors.fill: parent
                    RowLayout {
                     //   anchors.fill: parent
                        ExclusiveGroup { id: radioGroup }

                        RadioButton {id: entitiesBtn; text: "Entities"; exclusiveGroup: radioGroup ; 
                            onClicked:{
                                mySofaDisplay.setEntitiesActivated(sofaScene,true)
                                setSelectedEntitiesActivated()
                                mySofaDisplay.setFullModelActivated(sofaScene,false)
                                listView.enabled = true
                                entitiesButtonsColumn.enabled = true
                                mySofaDisplay.stepIfNeeded(sofaScene, "entities")
                            }
                        }
                        RadioButton {id: fullModelBtn; text: "FullModel"; exclusiveGroup: radioGroup; checked: true ; 
                            onClicked:{
                                mySofaDisplay.setEntitiesActivated(sofaScene,false)
                                mySofaDisplay.setFullModelActivated(sofaScene,true)
                                listView.enabled = false
                                entitiesButtonsColumn.enabled = false
                                mySofaDisplay.stepIfNeeded(sofaScene, "fullModel")
                            }
                        }
                         CheckBox { id: displayQualityChkBx ; text: "DisplayQuality"; checked: false; 
                            onClicked:
                            {
                                mySofaDisplay.setQualityActivated(sofaScene,checked)
                                mySofaDisplay.setElementDisplayActivated(sofaScene,!checked)
                                mySofaDisplay.setItemProperty(sofaScene,"legend","activated",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"quality")

                            } 
                        }
                        CheckBox {id: edgesChkBx; text: "DisplayEdges"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setEdgesActivated(sofaScene,checked)
                            } 
                        }

                    }
                }
            }
		    RowLayout {
			    id: displayTools
			    Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true
                //anchors.fill: parent

                ColumnLayout {
                    id: buttonsColumn
                  //  anchors.left: parent.left
                    width:childrenRect.width
                    ColumnLayout {
                        id: entitiesButtonsColumn
                        // anchors.fill: parent
                        enabled:false
                        width:childrenRect.width

		                Button {
			                id: clearSelectionBtn
			        //        anchors.top: top
			         //       anchors.right: right
			                text: "none"
			                tooltip: "clear selection"
	  
			                onClicked: {
				                listView.selection.clear()
                                mySofaDisplay.setEntitiesActivated(sofaScene,false)
			                }
		                }

		                Button {
		                    id: selectAllBtn
		            //        anchors.top: top
		            //        anchors.right: right
		                    text: "all"
		                    tooltip: "select all"
	  
			                onClicked: {
				                listView.selection.selectAll()
                                mySofaDisplay.setEntitiesActivated(sofaScene,true)                       
                                mySofaDisplay.stepIfNeeded(sofaScene,"all")
			                }
		                }
		
		                Button {
		                    id: invertSelectionBtn
		            //        anchors.top: top
		            //        anchors.right: right
		                    text: "invert"
		                    tooltip: "invert selection"
	  
		                    onClicked: {
                                invertNSetSelectedEntitiesActivated()
		                    }
		                }
                    }

	            }

			    TableView  {
				    id : listView

				    Layout.minimumHeight: 100
                    Layout.fillHeight: true
				    Layout.fillWidth: true
                  //  anchors.right: elementBox.left
                   // anchors.top: parent.top
                    //anchors.bottom: parent.bottom
				    //anchors.left: buttonsColumn.right
                    enabled: false
				    clip: true
				    model: mySofaDisplay.SofaEntities(sofaScene)
				    focus: false
					

				    function activateItem(index) {
                        mySofaDisplay.setItemActivated(sofaScene,listView.model[index],true)
                        mySofaDisplay.stepIfNeeded(sofaScene,listView.model[index])
				    }
				    function processClickedItem(index) {
					    if(listView.selection.contains(index)){
    						mySofaDisplay.setItemActivated(sofaScene,listView.model[index],true)
                            mySofaDisplay.stepIfNeeded(sofaScene,listView.model[index])
                        }
					    else {
						   mySofaDisplay.setItemActivated(sofaScene,listView.model[index],false)
					    }
				    }

				    Component.onCompleted: {
                        if(listView.model.length>0)
                            listView.selection.selectAll()
				    }
					TableViewColumn {
					    role: "text"
					    title: "Entities"
					   // width: 200
				    }
                    //contentHeader :
                    //headerDelegate :
                    headerVisible:true
                //       sortIndicatorVisible:true
				    selectionMode:SelectionMode.MultiSelection
				    selection.onSelectionChanged :{
				    }
				    onClicked:processClickedItem(row)        
				    onDoubleClicked:processClickedItem(row)        
			    }

                GroupBox {
                    id:elementBox
                    title: "Element"
                  //  anchors.right: items.left

                    ColumnLayout {
                       // anchors.fill: parent
                        CheckBox {id: barsChkBx;  text: "1d Bars"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"bars",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"bars")
                            }
                        }
                        CheckBox {id: trianglesChkBx;  text: "2d Triangles"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"triangles",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"triangles")
                            }
                        }
                        CheckBox {id: quadsChkBx;  text: "2d Quads"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"quads",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"quads")
                            } 
                        }
                        CheckBox {id: tetraChkBx;  text: "3d Tetrahedra"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"tetrahedra",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"tetrahedra")
                            }
                        }
                        CheckBox {id: hexaChkBx; text: "3d Hexahedra"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"hexahedra",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"hexahedra")
                            }
                        }
                        CheckBox {id: pentaChkBx; text: "3d Pentahedra"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"pentahedra",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"pentahedra")
                            }
                        }
                        CheckBox { id: pyramidsChkBx; text: "3d Pyramids"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setElementTypeActivated(sofaScene,"pyramids",checked)
                                if(checked)
                                    mySofaDisplay.stepIfNeeded(sofaScene,"pyramids")
                            }
                        }
                    }
                }

                GroupBox {
                    id:items
                    title: "items"
                  //  anchors.right: parent.right
                    ColumnLayout {
                   //     anchors.fill: parent
                        CheckBox { id: ctrlPtChkBx; text: "ControlPoints"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setItemActivated(sofaScene,"controlPoints",checked)
                            }
                        }
                        CheckBox { id: landmarkChkBx; text: "Landmarks"; checked: true; 
                            onClicked:
                            {
                                mySofaDisplay.setItemActivated(sofaScene,"landmarks",checked)
                            }
                        }
                    }
                }

            } 
        }
	}

    


    function updateListViewModelNSelection(newstatus) {
	    if(sofaScene && sofaScene.status == 1){
	        listView.model=mySofaDisplay.SofaEntities(sofaScene);
            if(listView.model.length>0)
                listView.selection.selectAll()
	    }
    }

    function setSelectedEntitiesActivated(){
        if(sofaScene && sofaScene.status == 1){
            for(var i=0;i<listView.model.length;i++){ // ou voir listView.selection.forEach(callback)
		        if(listView.selection.contains(i)){
			        mySofaDisplay.setItemActivated(sofaScene,listView.model[i], true)
                    mySofaDisplay.stepIfNeeded(sofaScene,listView.model[i])
			        }
		        else{
                    mySofaDisplay.setItemActivated(sofaScene,listView.model[i], false)
		        }
            }
        }
    }

    function invertNSetSelectedEntitiesActivated(){
        if(sofaScene && sofaScene.status == 1){
            for(var i=0;i<listView.model.length;i++){ // ou voir listView.selection.forEach(callback) 
			    if(listView.selection.contains(i)){
				    listView.selection.deselect(i)
                    mySofaDisplay.setItemActivated(sofaScene,listView.model[i], false)
				    }
			    else{
				    listView.selection.select(i)
                    mySofaDisplay.setItemActivated(sofaScene,listView.model[i], true)
                    mySofaDisplay.stepIfNeeded(sofaScene,listView.model[i])
			    }
		    }
        }
    }

    function resetDisplayOptionsMenus(){
        if(sofaScene && sofaScene.status == 1){
            listView.enabled = false
            entitiesButtonsColumn.enabled = false
            entitiesBtn.checked=false;
            fullModelBtn.checked=true;
            displayQualityChkBx.checked=false;
            edgesChkBx.checked=true;
            barsChkBx.checked=true;
            trianglesChkBx.checked=true;
            quadsChkBx.checked=true;
            tetraChkBx.checked=true;
            hexaChkBx.checked=true;
            pentaChkBx.checked=true; 
            pyramidsChkBx.checked=true;
            ctrlPtChkBx.checked=true;
            landmarkChkBx.checked=true;
        }
    }
}
