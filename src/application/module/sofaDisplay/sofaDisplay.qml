// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import SofaBasics 1.0
import SofaWidgets 1.0


import piper.SofaDisplay 1.0

Item {

 SofaDisplay {
		id: mySofaDisplay
	}

	layer.enabled: true

	Component.onCompleted: {
        sofaScene.asynchronous = true;

        if (Qt.resolvedUrl(sofaScene.source) === "file:sceneDisplay.py")
			sofaScene.reload()
        else
			sofaScene.source = "file:sceneDisplay.py";
		visible = true;
        sofaScene.statusChanged.connect(resetScene)
        context.modelChanged.connect(resetSofa)
	}
    Component.onDestruction: {
        sofaScene.source = "file:empty.scn";
    }

	SplitView {
		id: splitViewDisplayTools
		anchors.fill: parent
		orientation: Qt.Vertical

        Rectangle {

            Layout.fillWidth: true
            height:250
                   
            TabView {
                id: tabView
                anchors.fill: parent
           //     anchors.margins: UI.margin
            //    tabPosition: UI.tabPosition
                Layout.fillWidth: true
                Layout.minimumHeight: 250
                Layout.preferredHeight: 250
                Tab {
                    title: "Display Options"
                    Layout.fillWidth: true
                    //  anchors.fill: parent
                     id: tab1
                    DisplayOptionsTab {
                        id: myDisplayOptionsTab
                    }
                }
                Tab {
                    title: "Tools"
                    Layout.fillWidth: true
                      //           anchors.fill: parent

                    DisplayToolsTab {
                    id: myDisplayToolsTab
                    }
                }
                Tab {
                    title: "Quality Options"
                    Layout.fillWidth: true
                        //         anchors.fill: parent
                    QualityOptionsTab {
                    id: myQualityOptionsTab
                    }
                }

            }
        }

	    RowLayout {
		    DynamicSplitView {
			    id: dynamicSplitView
			    anchors.fill: parent
			    uiId: 1
			    sourceComponent: Component {
				    DynamicContent {
                        defaultContentName: "SofaViewer"
					    sourceDir: "qrc:/SofaWidgets"
                        properties: {"scene": sofaScene, "culling": false, "defaultCameraOrthographic": true, "hideBusyIndicator": true}
				    }
			    }
		    }
	    }
}
   

     FileDialog {
        id: saveQualityDialog
        title: qsTr("save quality in directory")
		selectMultiple: false
        selectExisting: true
		selectFolder: true
        onAccepted: {
            mySofaDisplay.setSaveQualityPath(sofaScene,saveQualityDialog.fileUrl)
            mySofaDisplay.setWriteToFileActivated(sofaScene,true)
            sofaScene.reset() //reset de t=0 ; writestate enregistre sinon a chaque iteration ; ici on set volontairement des valeurs de time et period permetant de n'enregistrer que a la premiere iteration
            sofaScene.step()
            mySofaDisplay.setWriteToFileActivated(sofaScene,false)
            mySofaDisplay.setSaveQualityPath(sofaScene,"") //hack : will call reinitcomponent on writestate that will close the fileHandle 
            context.logInfo("Saved quality metric values to "+saveQualityDialog.fileUrl);
        }
    }

     function resetScene(){
         if(sofaScene && sofaScene.ready) {
            mySofaDisplay.setWriteToFileActivated(sofaScene,false)
            mySofaDisplay.setEntitiesActivated(sofaScene,false)
            mySofaDisplay.setFullModelActivated(sofaScene,true)
            mySofaDisplay.setQualityActivated(sofaScene,false)
            mySofaDisplay.setElementDisplayActivated(sofaScene,true)
            mySofaDisplay.setEdgesActivated(sofaScene,true)
            mySofaDisplay.setQualityLightActivated(sofaScene,false)
            //TODO sofaViewer.culling=false
            //  mySofaDisplay.setModelAndEntityItemProperty(sofaScene,"displayElement/visual","cullFace",0)
            //   sofaScene.reset();
        }

     }
     function resetSofa(){
       
        if (sofaScene && Qt.resolvedUrl(sofaScene.source) === "file:sceneDisplay.py"){
            sofaScene.reload()}
     }
}
