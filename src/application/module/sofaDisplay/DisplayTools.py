# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 

def insertTopologyViewer(parentNode, topologyKind, triangulate, createSubelements, color, linkToLoaderName):
    toponode=parentNode.createChild(topologyKind)
    if topologyKind == "bars":
        topologyKind = "edges"
    kwArgsMeshLoader={"name":"submesh","piperMeshLoaderPath":linkToLoaderName,topologyKind:"@../loader."+topologyKind, "triangulate":triangulate, "createSubelements":createSubelements}
    toponode.createObject("PiperMeshLoader", **kwArgsMeshLoader)
    toponode.createObject('MechanicalObject',template="Vec3",name='meshDof')#empty MO because ScaledJacobianMapping Needs one but is hacked to get position from piperloader #,position="@loader.position")
  #  kwArgs={"name":"subtopo","position":"@submesh.position",topologyKind:"@submesh."+topologyKind}

  #  if topologyKind == "triangles" :
  #      toponode.createObject('TriangleSetTopologyContainer', **kwArgs)
  #  elif topologyKind == "quads":
  ##      toponode.createObject('QuadSetTopologyContainer', **kwArgs)
   # elif topologyKind == "tetrahedra":
   #     toponode.createObject('TetrahedronSetTopologyContainer', **kwArgs)
   # elif topologyKind == "hexahedra":
   #     toponode.createObject('HexahedronSetTopologyContainer', **kwArgs)
   # elif topologyKind == "edges":
   #     toponode.createObject('EdgeSetTopologyContainer', **kwArgs)
   # else:
   #     toponode.createObject('MeshTopology', **kwArgs)
    if topologyKind == "edges":
        displayEdge=toponode.createChild("displayEdge")
        displayElement=toponode.createChild("displayElement")
        displayElement.createObject("PiperOglModel", name="visual", piperMeshLoaderPath=toponode.getPathName()+"/submesh",  lineWidth="1", lineSmooth=False, updateNormals=False, handleDynamicTopology=False, useExtPos=True, useExtEdges=True, useExtNormals=True, color=color)#edges='@../submesh.edges',
        

    else:
        displayEdge=toponode.createChild("displayEdge")
        displayEdge.createObject("PiperOglModel", name="visual", piperMeshLoaderPath=toponode.getPathName()+"/submesh",  lineWidth="1", lineSmooth=False, updateNormals=False, handleDynamicTopology=False, useExtPos=True, useExtEdges=True, useExtNormals=False) #edges='@../submesh.edges',
        displayElement=toponode.createChild("displayElement")
        displayElement.createObject("PiperOglModel", name="visual", piperMeshLoaderPath=toponode.getPathName()+"/submesh", color=color, updateNormals=False, handleDynamicTopology=False, useExtPos=True, useExtTriangles=True, useExtQuads=True, useExtNormals=True) #triangles="@../submesh.triangles", quads="@../submesh.quads", 


    
    return toponode

def insertQualityMetric(parentNode,metric,topologyKind):
    parentTopoNode = parentNode.getChild(topologyKind)
    if parentTopoNode is not None:
        metricNode = parentTopoNode.createChild("QualityMetricCompute")
        metricNode.createObject('MechanicalObject',template="Vec1",name='quality')
        if  topologyKind is not "pentahedra" and topologyKind is not "pyramids" and  topologyKind is not "bars" :
            metricNode.createObject('ScaledJacobianMapping',template="Vec3,Vec1", name="scaledJacobianMapping",  qualityMetric=metric,  elementKindStr=topologyKind,  input="@../meshDof",  output="@./", extPiperMeshLoaderPath=parentNode.getPathName()+"/loader")

    return None

def insertQualityMetricVisu(parentNode,topologyKind):
    parentTopoNode = parentNode.getChild(topologyKind)
    if parentTopoNode is not None:
        displayQuality = parentTopoNode.createChild("displayQuality")
        displayQuality.createObject('ColorMap', name="ColorMap1", paletteSize="256", colorScheme="Green", showLegend="0")
        displayQuality.createObject('ColorMap', name="ColorMap2", paletteSize="256", colorScheme="Red", showLegend="0")
        displayQuality.createObject('ColorMap', name="ColorMap3", paletteSize="256", colorScheme="BlueInv", showLegend="0")     
        displayQuality.createObject('DataDisplayTopo', template="ExtVec3f",name="dataDisplayTopo", maximalRange="0", hasUserRange="1", hasSuppColorMap="1", userRange="-1 0.2 0.5 1", hasAlpha="0", CullBackFace="0", drawWithLight="0", userAlpha="1 1 1" ,cellData='@../QualityMetricCompute/quality.position', colorMapName2="ColorMap2", colorMapName3="ColorMap3", extPiperMeshLoaderPath=parentTopoNode.getPathName()+"/submesh", elementKindStr=topologyKind)
   #     displayQuality.createObject('IdentityMapping',template="Vec3,ExtVec3f", name="identityMap0",  input="@/fullModel/meshDof", output="@./")

    return None

def insertWriteQualityToFile(parentNode,topologyKind):
    parentTopoNode = parentNode.getChild(topologyKind)
    if parentTopoNode is not None:
        qualityMetricNode = parentTopoNode.getChild("QualityMetricCompute")
        writeToFileNode = qualityMetricNode.createChild("writeToFile")
        writeToFileNode.createObject('WriteState', input="@../",filename="", time="-0.001", period="999999")
        writeToFileNode.activated = True

    return None
