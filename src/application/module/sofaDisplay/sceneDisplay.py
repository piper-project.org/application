# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import os
import sys
sys.path.append(os.path.dirname( os.path.abspath( __file__ ) )+"/../lib/python2.7/site-packages")

import colorsys


import Sofa
import SofaPython.Tools
import piper.sofa
import piper.hbm
import piper.app
from DisplayTools import (insertTopologyViewer,insertQualityMetric,insertQualityMetricVisu,insertWriteQualityToFile)

class ColorFactory:
    """Simple palette generator using the HSV color space"""

    def __init__(self, nbColors=10, s=1., v=1.):
        self.palette=list()
        for i in range(0, nbColors):
            self.palette.append(colorsys.hsv_to_rgb(float(i)/nbColors, s, v))

    def getColor(self, i):
        return self.palette[i%len(self.palette)]

def createScene(rootNode):
    print "[SOFA] create scene sofaDisplay"

    model = piper.sofa.project().model()
    colors = ColorFactory()
    elementKindsList={"triangles","quads","tetrahedra","hexahedra","pentahedra","pyramids","bars"}

    rootNode.createObject("RequiredPlugin", name="SofaPiper")
    rootNode.createObject("RequiredPlugin", name="Anatomy")
    rootNode.createObject('ClipPlane', name="Clip", normal="0 -1 0", position="0 -50 0", active=False)
 
    if not model.empty():
        #loading fullmodel to sofascene
        fullModel = rootNode.createChild("fullModel")
        fullModel.createObject("PiperMeshLoader", name="loader", loadModel=True, triangulate=False, createSubelements=False)
        i=0
        for elementKind in elementKindsList:
            color=SofaPython.Tools.listToStr(colors.getColor(i))
            insertTopologyViewer(fullModel,elementKind,False,True,color,"fullModel/loader")
            insertQualityMetric(fullModel,0,elementKind)
            insertQualityMetricVisu(fullModel,elementKind)
            insertWriteQualityToFile(fullModel,elementKind)
            i=i+1

        i=0

        #loading entities to sofa scene
        for entity in model.metadata().entities().values():
            #print "Entity:", entity.name()
            entityNode = rootNode.createChild(entity.name())
            entityNode.createObject("PiperMeshLoader", name="loader", entityName=entity.name(), triangulate=False, createSubelements=False)
            color=SofaPython.Tools.listToStr(colors.getColor(i))

            for elementKind in elementKindsList:
                insertTopologyViewer(entityNode,elementKind,False,True,color,entity.name()+"/loader")
                insertQualityMetric(entityNode,0,elementKind)
                insertQualityMetricVisu(entityNode,elementKind)
            i=i+1
        
        #loading controlpoints to sofascene
        controlPointsNode = rootNode.createChild("controlPoints")
        for ctrlPt in model.metadata().interactionControlPoints().values():
            if ctrlPt.hasId():
                ctrlPtId = ctrlPt.id()
                ctrlPtNode = controlPointsNode.createChild("ctrlPt"+str(ctrlPtId))
                ctrlPtNode.createObject("PiperMeshLoader", name="loader", controlPointId=ctrlPtId, triangulate=False)
                ctrlPtNode.createObject('MechanicalObject', template='Vec3', name='ctrlPtPos', showObject='true' , position="@loader.position", showColor=SofaPython.Tools.listToStr(colors.getColor(i)), drawMode='1', showObjectScale='3')
                i=i+1

        #loading landmarks to sofascene
        landmarksNode = rootNode.createChild("landmarks")
        for landMark in model.metadata().landmarks().values():
            lmName = landMark.name()
            lmNode = landmarksNode.createChild(lmName)
            lmNode.createObject("PiperMeshLoader", name="loader", landmarkName=lmName, triangulate=False)
            lmNode.createObject('MechanicalObject', template='Vec3', name='landMarkPos', showObject='true' , position="@loader.position", showColor=SofaPython.Tools.listToStr(colors.getColor(i)), drawMode='1', showObjectScale='3')
            i=i+1
        
        legendNode = rootNode.createChild("legend")
        legendNode.createObject('ColorMap', name="ColorMap1", paletteSize="256", colorScheme="Red", showLegend="0")
        legendNode.createObject('ColorMap', name="ColorMap2", paletteSize="256", colorScheme="Green", showLegend="0")
        legendNode.createObject('ColorMap', name="ColorMap3", paletteSize="256", colorScheme="BlueInv", showLegend="0")   

    return rootNode
