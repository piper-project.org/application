/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "SofaDisplay.h"

#include <QDebug>
#include <QTextStream>
#include <QFileInfo>
#include <QProcess>
#include <QCoreApplication>

#include <tinyxml/tinyxml2.h>
#include "common/Context.h"
#include "hbm/Metadata.h"
#include <QQmlProperty>
#include <QMetaObject>
#include <QVariant>


namespace piper {

using namespace hbm;

bool SofaDisplay::isSceneLoaded(QObject* sofaScene)
{
    return sofaScene!=nullptr
            && QQmlProperty::read(sofaScene, "source").toString().compare("file:sceneDisplay.py")==0 // the display scene is the current scene
            && QQmlProperty::read(sofaScene, "status").toInt()==1; //verifies that the sofascene is ready (return code 0 null, 1 ready, 2 loading, 3 error)
}

SofaDisplay::SofaDisplay():itEntities(0)
{
    qelementTypes.push_back("triangles");qelementTypes.push_back("quads");qelementTypes.push_back("tetrahedra");
    qelementTypes.push_back("hexahedra");qelementTypes.push_back("pentahedra");qelementTypes.push_back("pyramids");qelementTypes.push_back("bars");
}

 QStringList  SofaDisplay::SofaEntities(QObject* sofaScene){
    _sofaEntities.clear();

    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();
 
             for(Metadata::EntityCont::const_iterator it = entities.begin() ; it != entities.end() ; ++it) {
                    QVariant returnedValue;
 
                    QString dataToLookFor="@/"+QString::fromStdString(it->second.name())+".name";    
                    QMetaObject::invokeMethod(sofaScene, "onDataValue",Q_RETURN_ARG(QVariant, returnedValue),Q_ARG(QString, dataToLookFor));
                    if(!returnedValue.isNull())
                        _sofaEntities.push_back(returnedValue.toString());
             }
        }
    }
    return _sofaEntities;
 }


 QStringList const SofaDisplay::elementTypes(){
     return qelementTypes;
 }

 void SofaDisplay::setSaveQualityPath(QObject* sofaScene,const QUrl& url){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)) {
            for(int i=0;i<qelementTypes.size();i++)
            {
                QString value = url.toLocalFile()+"/"+qelementTypes[i]+".txt";
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/QualityMetricCompute/writeToFile/WriteState";
                if(setSofaComponentProperty(sofaScene,componentPath,QString("filename"),value))
                     QMetaObject::invokeMethod(sofaScene, "reinitComponent",Q_ARG(QString, componentPath));
            }
        }
    }
 }

 void SofaDisplay::reinitModelAndEntityItem(QObject* sofaScene,QString const & item){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/"+item;
                doReinitItem(sofaScene,componentPath);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/"+item;
                    doReinitItem(sofaScene,componentPath);
                } 
            }
        }
    }

 }

  void SofaDisplay::reinitItem(QObject* sofaScene,QString const & item){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)){
                QString componentPath="@/"+item;
                doReinitItem(sofaScene,componentPath);

            
        }
    }

 }
    

void SofaDisplay::setElementTypeActivated(QObject* sofaScene, const QString& elemType, const QVariant& state){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)){
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();
            QString componentPath="@/fullModel/"+elemType;
            setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
            for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+elemType;
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
            }
        }
    }
}

void SofaDisplay::setItemActivated(QObject* sofaScene,QString const & item,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
         if(isSceneLoaded(sofaScene)){
            QString componentPath="@/"+item;
            setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
        }
    }
}

void SofaDisplay::setItemProperty(QObject* sofaScene,QString const & item,QString const & property,QVariant const & value){
    if (!Context::instance().project().model().empty()) {
        if(isSceneLoaded(sofaScene)){
        QString componentPath="@/"+item;
        setSofaComponentProperty(sofaScene,componentPath,property,value);
        }
    }
}

void SofaDisplay::setQualityActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)){
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/displayQuality";
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/displayQuality";
                    setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                }
            }
        }
    }
}


    
void SofaDisplay::setQualityLightActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)){
             Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/displayQuality/dataDisplayTopo";
                setSofaComponentProperty(sofaScene,componentPath,QString("drawWithLight"),state);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/displayQuality/dataDisplayTopo";
                    setSofaComponentProperty(sofaScene,componentPath,QString("drawWithLight"),state);
                } 
            }
        }
    }
}

void SofaDisplay::setQualityAlphaActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if(isSceneLoaded(sofaScene)){
             Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/displayQuality/dataDisplayTopo";
                setSofaComponentProperty(sofaScene,componentPath,QString("hasAlpha"),state);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/displayQuality/dataDisplayTopo";
                    setSofaComponentProperty(sofaScene,componentPath,QString("hasAlpha"),state);
                } 
            }
        }
    }
}

void SofaDisplay::setElementDisplayActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/displayElement";
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/displayElement";
                    setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                } 
            }
        }
    }
}

void SofaDisplay::setEdgesActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

               for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/displayEdge";
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/displayEdge";
                    setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
                } 
            }
        }
    }
}

void SofaDisplay::setWriteToFileActivated(QObject* sofaScene,QVariant const & state){
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/QualityMetricCompute/writeToFile";
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
            }
        }
    }
}

void SofaDisplay::setEntitiesActivated(QObject* sofaScene,QVariant const & state) {
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                QString componentPath="@/"+QString::fromStdString(it->second.name());
                setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
            }         
        }
    }
}

void SofaDisplay::setFullModelActivated(QObject* sofaScene,QVariant const & state) {
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            QString componentPath="@/fullModel";
            setSofaComponentProperty(sofaScene,componentPath,QString("activated"),state);
        }
    }
}


bool SofaDisplay::setSofaComponentProperty(QObject* sofaScene,QString const& componentPath,QString const & property,QVariant const & state){
    QVariant returnedValue;
    QString dataToLookFor=componentPath+".name";    
    QMetaObject::invokeMethod(sofaScene, "onDataValue",Q_RETURN_ARG(QVariant, returnedValue),Q_ARG(QString, dataToLookFor));

    if(!returnedValue.isNull()){
        QString dataToModify=componentPath+"."+property; 
        QMetaObject::invokeMethod(sofaScene, "onSetDataValue",Q_ARG(QString, dataToModify),Q_ARG(QVariant, state));
        return true;
    }
    else return false;
}

bool SofaDisplay::getSofaComponentProperty(QObject* sofaScene,QString const& componentPath,QString const & property,QVariant & state){
    QVariant returnedValue;
    QString dataToLookFor=componentPath+".name";    
    QMetaObject::invokeMethod(sofaScene, "onDataValue",Q_RETURN_ARG(QVariant, returnedValue),Q_ARG(QString, dataToLookFor));

    if(!returnedValue.isNull()){
        QString dataToInspect=componentPath+"."+property; 
        QMetaObject::invokeMethod(sofaScene, "onDataValue",Q_RETURN_ARG(QVariant,state),Q_ARG(QString, dataToInspect));
        return true;
    }
    else return false;
}

bool SofaDisplay::doReinitItem(QObject* sofaScene,QString const& componentPath){
    QVariant returnedValue;
    QString dataToLookFor=componentPath+".name";    
    QMetaObject::invokeMethod(sofaScene, "onDataValue",Q_RETURN_ARG(QVariant, returnedValue),Q_ARG(QString, dataToLookFor));

    if(!returnedValue.isNull()){
        QMetaObject::invokeMethod(sofaScene, "reinitComponent",Q_ARG(QString, componentPath));
        return true;
    }
    else return false;
}

void SofaDisplay::setModelAndEntityItemProperty(QObject* sofaScene, QString const & item, QString const & property, QVariant const & value){
    if (!Context::instance().project().model().empty()) {
 
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i]+"/"+item;
                setSofaComponentProperty(sofaScene,componentPath,property,value);
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name())+"/"+qelementTypes[i]+"/"+item;
                    setSofaComponentProperty(sofaScene,componentPath,property,value);
                } 
            }
        }
    }
}

void SofaDisplay::setStepNeeded(QObject* sofaScene){
    componentsNeedingSofaStep["fullModel"]=true;
    componentsNeedingSofaStep["entities"]=true;
    componentsNeedingSofaStep["quality"]=true;
    for(int i=0; i<_sofaEntities.size();++i)
        componentsNeedingSofaStep[_sofaEntities[i].toStdString()]=true;
    for(int i=0; i<qelementTypes.size();++i)
        componentsNeedingSofaStep[qelementTypes[i].toStdString()]=true;

    if (!Context::instance().project().model().empty()) {
        if (isSceneLoaded(sofaScene)) {
            Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

             QVariant activated=QVariant::fromValue<bool>(false);

            for(int i=0;i<qelementTypes.size();i++)
            {
                QString componentPath="@/fullModel/"+qelementTypes[i];
                if(getSofaComponentProperty(sofaScene,componentPath,"activated",activated) && activated.toBool()){
                    componentsNeedingSofaStep[qelementTypes[i].toStdString()]=false;
                }
            }

            if(getSofaComponentProperty(sofaScene,"@/fullModel/"+qelementTypes[0]+"/displayQuality","activated",activated) && activated.toBool()){
                componentsNeedingSofaStep["quality"]=false;
            }

            if(getSofaComponentProperty(sofaScene,"@/fullModel","activated",activated) && activated.toBool()){
                componentsNeedingSofaStep["fullModel"]=false;
            }
             else {
                componentsNeedingSofaStep["entities"]=false;
                for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                    QString componentPath="@/"+QString::fromStdString(it->second.name());
                    if(getSofaComponentProperty(sofaScene,componentPath,"activated",activated) && activated.toBool()){
                        componentsNeedingSofaStep[it->second.name()]=false;
                    }
                }

             }
        }
    }
}
void SofaDisplay::stepIfNeeded(QObject* sofaScene, QString const & component){
        if (!Context::instance().project().model().empty()) {
            if (isSceneLoaded(sofaScene)) {
                Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();

                if(component.compare("all")==0){
                    for(auto it = entities.begin() ; it != entities.end() ; ++it) {
                        stepIfNeeded(sofaScene,QString::fromStdString((*it).second.name()));
                    }
                }
                else{
                    if(componentsNeedingSofaStep.find(component.toStdString()) != componentsNeedingSofaStep.end() && componentsNeedingSofaStep[component.toStdString()]){
                        QMetaObject::invokeMethod(sofaScene, "step");
                        componentsNeedingSofaStep[component.toStdString()]=false;
                    }
                }
            }
             
        }
}


}
