// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import piper.VtkDisplay 1.0
import piper.Check 1.0
import Piper 1.0


ModuleToolWindow 
{
    title: "Quality Control"
    Layout.fillWidth: true

    Component.onCompleted: setCurrentWindowSizeFixed()
    onClosing: vtkDisplay.HighlightElementsByScalarsOff("Quality")

    ScrollView 
    {
        //horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        Layout.fillWidth: true
        implicitWidth:555
        implicitHeight:200

        Rectangle 
        {
            Layout.fillWidth: true
            height:childrenRect.height
            anchors.fill: parent
            ColumnLayout 
            {
                Layout.fillWidth: true
                height:childrenRect.height
                GroupBox 
                {
                    title: " "
                    Layout.fillWidth: true
                    ColumnLayout
                    {
                        RowLayout 
                        {
                            CheckBox 
                            {
                                id: lightChkbx; 
                                text: "draw quality with lights"; 
                                checked: false; 
                            }
                            CheckBox 
                            {
                                id: alphaChkbx;
                                text: "draw quality with transparency";
                                checked: false; 
                            }
                        }
                        Button 
                        {
		                    id: saveQualityToFile
		                    text: "save Quality to File"
		                    tooltip: "save Quality to File"
	  	                    onClicked: 
                            {
                                saveQualityDialog.open()
		                    }
		                }
                    }
                }
                RowLayout 
                {
                    Layout.fillWidth: true
                    height:childrenRect.height
                    GroupBox 
                    {
                        title: " "
                        Layout.fillWidth: true
                        //anchors.fill: parent
                        RowLayout 
                        {
                            ColumnLayout 
                            {
                                RowLayout 
                                {
                                    Label
                                    { 
                                        text:"Choose metric function to use :"
                                    }
                                    ComboBox 
                                    {
                                        id: chooseMetricFunction
                                        width: 300
                                        model:  ["Aspect Frobenius","Min Angle","Max Angle","Condition","ScaledJacobian","Relative Size Squared","Shape","Shape and Size","Distortion", "Area", "Edge Ratio", "Aspect Ratio", "Radius Ratio"]
                                        currentIndex: 4
                                        onActivated:
                                        {
                                            chooseMetricFunction.currentIndex=index
                                        }
                                    }
                                    Label
                                    { 
                                        text:" Alpha: "
                                    }
                                    SpinBox 
                                    { 
                                        id: alphaValue; 
                                        decimals: 2
                                        minimumValue: 0
                                        maximumValue: 1
                                        stepSize: 0.1
                                        value: 1
                                        enabled: true
                                    }
                                }
                                RowLayout 
                                {
                                    CheckBox 
                                    {
                                        id: range1Chkbx; 
                                        text: "range1"; 
                                        checked: true; 
                                    }
                                    Label
                                    { 
                                        text:" ["
                                    }
                                    SpinBox 
                                    { 
                                        id: range1Lbx; 
                                        value: -1
                                        decimals: 2
                                        minimumValue: -99
                                        maximumValue: range1Ubx.value-0.01
                                        stepSize: 0.1
                                    }
                                    SpinBox 
                                    { 
                                        id: range1Ubx; 

                                        decimals: 2
                                        minimumValue:range1Lbx.value+0.01
                                        maximumValue:range3Lbx.value            
                                        stepSize: 0.1
                                        value: 0.0
                                        onEditingFinished:
                                        {
                                            range2Lbx.value = value
                                        }
                                        onValueChanged:
                                        {
                                            range2Lbx.value = value
                                        }
                                    }
                                    Label
                                    { 
                                        text:"[ "
                                    }
                                    ComboBox 
                                    {
                                        id: range1colorA
                                        width: 100
                                        model:  [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 0
                                        onActivated:
                                        {
                                            range1colorA.currentIndex=index
                                        }
                                    }
                                    ComboBox 
                                    {
                                        id: range1colorB
                                        width: 100
                                        model:  [" ","Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 0
                                        onActivated:
                                        {
                                            range1colorB.currentIndex=index
                                        }
                                    }
                         

                                  /*  CheckBox 
                                    {
                                        id: range1LegendChkbx; 
                                        text: "Show Legend"; 
                                        checked: false; 
                                        onClicked:
                                        {
                                            if(checked)
                                            {
                                                range2Offset = range2Offset + 30;
                                                range3Offset = range3Offset + 30;
                                            }
                                            else
                                            {
                                                range2Offset = range2Offset - 30;
                                                range3Offset = range3Offset - 30;
                                            }
                                        }
                                    }*/
                                }
                                RowLayout 
                                {
                                    CheckBox 
                                    {
                                        id: range2Chkbx;
                                        text: "range2"; 
                                        checked: true; 
                                    }
                                    Label
                                    { 
                                        text:" ["
                                    }
                                    SpinBox 
                                    { 
                                        id: range2Lbx; 
                                        value: 0.0
                                        decimals: 2
                                        stepSize: 0.1
                                        minimumValue: -99
                                        maximumValue: 99
                                        enabled: false
                                    }
                                   SpinBox 
                                    { 
                                        id: range2Ubx; 
                                        value: 0.5
                                        decimals: 2
                                        //minimumValue: -99
                                        //maximumValue: range1Ubx.value-0.01
                                        stepSize: 0.1
                                        enabled: false
                                    }
                                    Label
                                    { 
                                        text:"] "
                                    }
                                    ComboBox 
                                    {
                                        id: range2colorA
                                        width: 100
                                        model: [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 1
                                        onActivated:
                                        {
                                            range2colorA.currentIndex=index
                                        }
                                    }
                                    ComboBox 
                                    {
                                        id: range2colorB
                                        width: 100
                                        model: [ " " ,"Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 0
                                        onActivated:
                                        {
                                            range2colorB.currentIndex=index
                                        }
                                    }
                                    /*
                                    Label
                                    { 
                                        text:"Alpha:"
                                    }
                                    SpinBox 
                                    { 
                                        id: alpha2bx; 
                                        decimals: 2
                                        minimumValue: 0
                                        maximumValue: 1
                                        stepSize: 0.1
                                        value: 1
                                        enabled: false
                                        onValueChanged : 
                                        {
                                            updateAlpha()
                                        }
                                    }
                                    CheckBox 
                                    {
                                        id: range2LegendChkbx; 
                                        text: "Show Legend"; 
                                        checked: false; 
                                        onClicked:
                                        {
                                            if(checked)
                                                range3Offset = range3Offset + 30;
                                            else
                                                range3Offset = range3Offset - 30;
                                        }
                                    }*/
                                }
                                RowLayout 
                                {
                                    CheckBox 
                                    {
                                        id: range3Chkbx; 
                                        text: "range3"; 
                                        checked: true; 
                                    }
                                    Label
                                    { 
                                        text:" ]"
                                    }
                                    SpinBox 
                                    { 
                                        id: range3Lbx; 
                                        //width: 100
                                        value: 0.5
                                        anchors.horizontalCenter: range2Lbx.center
                                        decimals: 2
                                        minimumValue: range1Ubx.value
                                        maximumValue: range3Ubx.value-0.01  
                                        stepSize: 0.1
                                        onEditingFinished:
                                        {
                                            range2Ubx.value = value;
                                        }
                                        onValueChanged : 
                                        {
                                            range2Ubx.value = value;
                                        }
                                    }
                                    Label
                                    { 
                                        text:" "
                                    }
                                    SpinBox 
                                    { 
                                        id: range3Ubx; 
                                        value: 1.0
                                        width: 500
                                        decimals: 2
                                        anchors.centerIn: range3Ubx
                                        minimumValue: range3Lbx.value+0.01
                                        //maximumValue: range1Ubx.value-0.01
                                        stepSize: 0.1
                                    }
                                    Label
                                    { 
                                        text:"] "
                                    }

                                    ComboBox 
                                    {
                                        id: range3colorA
                                        width: 100
                                        model: [ "Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 2
                                        onActivated:
                                        {
                                            range3colorA.currentIndex=index
                                        }
                                    }
                                    ComboBox 
                                    {
                                        id: range3colorB
                                        width: 100
                                        model: [" " ,"Red","Green","Blue","Cyan","Magenta","Yellow","Black","White"]
                                        currentIndex: 0
                                        activeFocusOnPress : true
                                        onActivated:
                                        {
                                            range3colorB.currentIndex=index
                                        }
                                    }/*
                                    Label
                                    { 
                                        text:"Alpha:"
                                    }
                                    SpinBox 
                                    { 
                                        id: alpha3bx; 
                                        decimals: 2
                                        minimumValue: 0
                                        maximumValue: 1
                                        stepSize: 0.1
                                        value: 1
                                        enabled: false
                                        onValueChanged : 
                                        {
                                            updateAlpha()
                                        }
                                    }
                                    CheckBox 
                                    {
                                        id: range3LegendChkbx;
                                        text: "Show Legend";
                                        checked: false; 
                                    }*/
                                }
                            }
                            Button 
                            {
		                        text: "ok"
		                        tooltip: "update range values"
                                onClicked: 
                                {
                                    onClicked: getLUTParametersForQuality()
		                        }
		                    }
                        }
                    }

                    /*
                    GroupBox 
                    {
                        Layout.fillWidth: true
                        title: "Quality Metric"
                        ColumnLayout 
                        {
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "1d Bars"
                                    enabled: false
                                }
                                ComboBox
                                {
                                    id: barQualityMetric
                                    width: 200
                                    enabled: false
                                    model: ["-"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "2d Triangles"
                                    enabled: true
                                }
                                ComboBox 
                                {
                                    id: trianglesQualityMetric
                                    width: 200
                                    enabled: true
                                    model: ["ScaledJacobian","ShapeNSize","Warpage","Aspect ratio","Skew","Shape Factor","Jacobian","min angle","max angle", "side length"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "2d Quads"
                                    enabled: true
                                }
                                ComboBox
                                {
                                    id: quadsQualityMetric
                                    width: 200
                                    enabled: true
                                    model: ["ScaledJacobian","ShapeNSize","Warpage","Aspect ratio","Skew","Jacobian","min angle","max angle", "side length"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "3d Tetrahedra"
                                    enabled: true
                                }
                                ComboBox 
                                {
                                    id: tetrasQualityMetric
                                    width: 200
                                    enabled: true
                                    model: ["ScaledJacobian","ShapeNSize","Warpage","Aspect ratio","Skew","Tet collapse","Vol skew","Vol aspect ratio","Tri faces min angle", "Tri faces max angle", "side length"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "3d Pentahedra"
                                    enabled: false
                                }
                                ComboBox
                                {
                                    id: pentasQualityMetric
                                    width: 200
                                    enabled: false
                                    model: ["-"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout 
                            {
                                TextField
                                {
                                    text: "3d Hexahedra"
                                    enabled: true
                                }
                                ComboBox 
                                {
                                    id: hexasQualityMetric
                                    width: 200
                                    enabled: true
                                    model: ["ScaledJacobian","ShapeNSize","Warpage","Aspect ratio","Skew","Quad faces min angle", "Quad faces max angle", "Jacobian","Tri faces min angle", "Tri faces max angle", "side length"]
                                    currentIndex: 0
                                }
                            }
                            RowLayout
                            {
                                TextField
                                {
                                    text: "3d Pyramids"
                                    enabled: false
                                }
                                ComboBox 
                                {
                                    id: pyramidsQualityMetric
                                    width: 200
                                    enabled: false
                                    model: ["-"]
                                    currentIndex: 0
                                }
                            }
                        }
                    }*/

                }
            }
        }
    }

    property variant ctfParamsArray: [[]]

    //Return an array to c++ with all the quality parameters setled in the GUI by the user
    function getLUTParametersForQuality()
    {
        var nbRanges = 0;

        //Count how many ranges determined by the user and send the data to c++
        if(range1Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range1colorA.currentText, range1colorB.currentText, range1Lbx.value, range1Ubx.value];
            nbRanges = nbRanges + 1;
        }
        if(range2Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range2colorA.currentText, range2colorB.currentText, range2Lbx.value, range2Ubx.value];
            nbRanges = nbRanges + 1;
        }
        if(range3Chkbx.checked)
        {
            ctfParamsArray[nbRanges] = [nbRanges, range3colorA.currentText, range3colorB.currentText, range3Lbx.value, range3Ubx.value];
            nbRanges = nbRanges + 1;
        }
        vtkDisplay.QualityComputeAndVisualisation(ctfParamsArray, chooseMetricFunction.currentIndex, alphaValue.value)
    }   
}

       
