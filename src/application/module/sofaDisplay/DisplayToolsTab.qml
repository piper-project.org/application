// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

ScrollView {
    id: displayToolsTab
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
    Layout.fillWidth: true

     Component.onCompleted: {
        sofaScene.statusChanged.connect(resetToolsMenus)
        sofaScene.statusChanged.connect(updateSofa)
	}
    Rectangle {
        Layout.fillWidth: true
        height:childrenRect.height
            GroupBox {
            Layout.fillWidth: true
            title: "Clipping plane"
            ColumnLayout{
                  GroupBox{
                      title: " "
                     RowLayout{
                        Button {
			                id: clipPlaneBtn
			                text: "clipPlane"
                            tooltip: "activate ClippingPlane"
                            checkable : true
                            checked: false
                            onClicked: {
				                mySofaDisplay.setItemProperty(sofaScene, "Clip","active",clipPlaneBtn.checked)
			                }
		                }
                        Button {
			                id: clipPlaneUpdateBtn
			                text: "update"
                            tooltip: "update ClipPlane normal/position"
                            onClicked: {
				                updateSofa()
			                }
		                }
                     }
                  }

                RowLayout{
                    GroupBox{
                        title:"Normal"
                        RowLayout{
                            Label{ text:"X"}
                        SpinBox { id: normalX; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: 0
                            onValueChanged: {
                                updateSofa()    
                            }
                        }
                        Label{ text:"Y"}
                        SpinBox { id: normalY; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: -1
                            onValueChanged: {
                                updateSofa()  
                            }
                        }
                        Label{ text:"Z"}
                        SpinBox { id: normalZ; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: 0
                            onValueChanged: {
                                updateSofa()  
                            }
                        }
                    }
                    }
                    GroupBox{
                        title:"Position"
                        RowLayout{
                        Label{ text:"X"}
                        SpinBox { id: posX; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: 0
                            onValueChanged:{
                                updateSofa()  
                            }
                        }
                        Label{ text:"Y"}
                        SpinBox { id: posY; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: -50
                            onValueChanged:{
                                updateSofa()  
                            }
                        }
                        Label{ text:"Z"}
                        SpinBox { id: posZ; 
                            decimals: 2
                            minimumValue: -999
                            maximumValue: 999
                            stepSize: 1
                            value: 0
                            onValueChanged:{
                                updateSofa()  
                            }
                        }
                    }
                }
                }
                }
            }
        }

    function resetToolsMenus(){
      clipPlaneBtn.checked = false
    }

    function updateSofa(){
        mySofaDisplay.setItemProperty(sofaScene,"Clip","normal",[[[normalX.value,normalY.value,normalZ.value]]])
        mySofaDisplay.setItemProperty(sofaScene,"Clip","position",[[[posX.value,posY.value,posZ.value]]])
        sofaScene.step()
    }


} 

