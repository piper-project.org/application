// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

import Piper 1.0
import piper.ScalingParameter 1.0
import VtkQuick 1.0

ModuleLayout
{
    id: root
    content: DefaultVtkViewer {}
    toolbar:ColumnLayout {
        anchors.fill: parent

        ExclusiveGroup
        {
            id: moduletoolsGroup
        }


        ColumnLayout {
            anchors.top: parent.top
            Layout.fillWidth: true
            GroupBox
            {
                id: inputmodule
                title: qsTr("Target")
                anchors.top: parent.top
                Layout.fillWidth: true
                ColumnLayout {
                    anchors.fill: parent
                    Button {
                        id: targetProjectButton
                        Layout.fillWidth: true
                        action: targetProjectAction
                    }
                    Button {
                        id: targetFileButton
                        Layout.fillWidth: true
                        action: targetFileAction
                    }
                    Button {
                        id: targetResetButton
                        Layout.fillWidth: true
                        action: targetResetAction
                    }
                    Button {
                        id: saveTargetButton
                        Layout.fillWidth: true
                        action: saveTargetAction
                    }
                }
            }
            ModuleToolWindowButton {
                id: valueTableButton
                text:"Scale Factors"
                tooltip: "Show scale factor table"
                toolWindow: valuesTable
            }
            Button {
                id: applyButton
                Layout.fillWidth: true
                action: applyScalingParameter
            }
        }
        Action {
            id: targetResetAction
            text: qsTr("Reset")
            tooltip: "Reset Target scale factors (set value = 1.0)"
            onTriggered: myScalingParameter.resetTarget()
        }
        Action {
            id: applyScalingParameter
            text: qsTr("Apply Target")
            tooltip: "Apply Target scale factors to parameters"
            onTriggered: {
                myModelValidator.text = context.getValidHistoryName("Model_Parameter_Scaled")
                myModelValidator.open()
            }
        }
        Action {
            id: targetProjectAction
            text: qsTr("Target from Project")
            tooltip: "Load target scale factors"
            onTriggered: myScalingParameter.loadTargetfromContext()
        }
        Action {
            id: targetFileAction
            text: qsTr("Target from File")
            tooltip: "Load target scale factors from file:\n parametername1 value1\n parametername2 value2 ...\n "
            onTriggered: openScalingValues.open()
        }
        Action {
            id: saveTargetAction
            text: qsTr("Save Target")
            tooltip: "Save targets in project"
            onTriggered: myScalingParameter.saveTargettoContext()
        }
        FileDialog {
            id: openScalingValues
            title: qsTr("Open text file with target scale factors ...")
            nameFilters: ["Scaling Values files (*.txt)"]
            onAccepted: {
                myScalingParameter.loadTargetfromFile(openScalingValues.fileUrl)
            }
        }
        ModelHistoryNameDialog {
            id: myModelValidator
            onAccepted: myScalingParameter.setResultinHistory(text)
        }
//        Connections {
//            target: myScalingParameter
//            onModelScaled:
//            {
//                myModelValidator.text = context.getValidHistoryName("Model_Parameter_Scaled")
//                myModelValidator.open()
//            }
//        }
        ValuesTable {
            id: valuesTable
        }

        Item {
            ScalingParameter {
                id: myScalingParameter
            }
            Component.onCompleted: {
                valuesTable._myScalingParameter = myScalingParameter
            }
        }

    }
}
