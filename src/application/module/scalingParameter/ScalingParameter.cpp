/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ScalingParameter.h"

#include "common/message.h"
#include "common/logging.h"

#include "hbmscalingparameter/HbmScalingParameter.h"
#include "hbm/HumanBodyModel.h"

//#include <QString>
//#include <QTextStream>
//#include <QFileInfo>
//#include <QProcess>
//#include <QCoreApplication>
//#include <QtQuick>



namespace piper {
    namespace scalingparameter {
using namespace hbm;


TableParameterModel::TableParameterModel(std::map<std::string, std::vector<double>>& listnames, QObject *parent) : QAbstractListModel(parent) {
	construct( listnames);
}

TableParameterModel::~TableParameterModel()
{
    clear();
}

int TableParameterModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return 2;
}

int TableParameterModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
	return items.count();
}

QVariant TableParameterModel::data(const QModelIndex &index, int role) const {
    switch (role) {
        case NameRole: return items[index.row()]->m_name;
        case ValueRole:
        default:
            return items[index.row()]->m_scalefactor;
    }
}

QHash<int, QByteArray> TableParameterModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[NameRole] = "NameRole";
    roles[ValueRole] = "ValueRole";

    return roles;
}

bool TableParameterModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    switch (role) {
        case NameRole: items[index.row()]->m_name = value.toString(); break;
        case ValueRole: {
            items[index.row()]->m_scalefactor = value.toDouble();
            m_mapvalues.at(items[index.row()]->m_name.toStdString())[0] = value.toDouble();
            break;
        }
    }
    emit dataChanged(index, index);
    return true;
}

void TableParameterModel::construct( std::map<std::string, std::vector<double>> mapvalues) {
    for (auto item : items)
        delete item;
	items.clear();
    m_mapvalues = mapvalues;
    this->beginResetModel();
    for (auto it = mapvalues.begin(); it != mapvalues.end(); ++it) {
		items.append(new ModelParameterItem(QString::fromStdString(it->first), it->second[0]));
	}
	this->endResetModel();
}

Qt::ItemFlags TableParameterModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.column() > 0)
        flags |= Qt::ItemIsEditable;
    return flags;
}

void TableParameterModel::clear() {
    for (auto item : items)
        delete item;
	items.clear();
	this->beginResetModel();
	this->endResetModel();
}


ScalingParameter::ScalingParameter(): m_tablemodel( new TableParameterModel( ))
{
    connect(&Context::instance(), &Context::modelChanged, this, &ScalingParameter::doInitscaling);

    doInitscaling();
}

ScalingParameter::~ScalingParameter()
{
    delete m_tablemodel;
}

void ScalingParameter::doInitscaling()
{
    std::map<std::string, std::vector<double>> new_scalefactor;
    Metadata::HbmParameterCont& hbmparam = Context::instance().project().model().metadata().hbmParameters();
    for (Metadata::HbmParameterCont::iterator it = hbmparam.begin(); it != hbmparam.end(); ++it) {
        if (m_scalefactor.find(it->first) != m_scalefactor.end())
            new_scalefactor[it->first] = m_scalefactor[it->first];
        else
        {
            std::vector<double> newval;
            newval.push_back(1.0);
            new_scalefactor[it->first] = newval;
        }
    }
    m_scalefactor = new_scalefactor;
    m_tablemodel->clear();
    m_tablemodel->construct(m_scalefactor);
}
//
//void ScalingParameter::setOperationSignals(bool modelScaled)
//{
//    disconnect(&Context::instance(), &Context::operationFinished, this, &ScalingParameter::modelScaled);
//    if (modelScaled)
//        connect(&Context::instance(), &Context::operationFinished, this, &ScalingParameter::modelScaled);
//}

TableParameterModel* ScalingParameter::getModel(  )  {	
	return m_tablemodel;
}

void ScalingParameter::setResultinHistory(QString const& historyName) {
    void(*setResultinHistoryWrapper)(piper::scalingparameter::ScalingParameter*, void (piper::scalingparameter::ScalingParameter::*)(std::string const&), std::string const&)
        = &piper::wrapClassMethodCall < piper::scalingparameter::ScalingParameter, void(piper::scalingparameter::ScalingParameter::*)(std::string const&), std::string const& >;

    Context::instance().performAsynchronousOperation(std::bind(setResultinHistoryWrapper,
        this, &piper::scalingparameter::ScalingParameter::doSetResultinHistory, historyName.toStdString()),
        false, true, true, false, false);
}

void ScalingParameter::doSetResultinHistory(std::string const& historyname) {
    pInfo() << START << "Save model in history ";
    FEModel& femodel = Context::instance().project().model().fem(); //shortcut
    Context::instance().addNewHistory(QString::fromStdString(historyname));
    std::string curmodel = Context::instance().currentModel().toStdString();
    doApplyScaling();
    pInfo() << DONE << "Model parameter scaling is saved in history as " << curmodel;
    //emit Context::instance().modelUpdated();

}

//void ScalingParameter::applyScaling() {
//    setOperationSignals( true);
//    void(*applyScalingWrapper)(piper::scalingparameter::ScalingParameter*, void (piper::scalingparameter::ScalingParameter::*)())
//        = &piper::wrapClassMethodCall < piper::scalingparameter::ScalingParameter, void (piper::scalingparameter::ScalingParameter::*)() >;
//    Context::instance().performAsynchronousOperation(std::bind(applyScalingWrapper, this, &piper::scalingparameter::ScalingParameter::doApplyScaling),
//        false, false, false, false, false);
//}
//
//
void ScalingParameter::doApplyScaling(){
    pInfo() << START << QStringLiteral("Apply Scaling values to parameters.");
    HumanBodyModel& hbm = Context::instance().project().model();
    TargetList target;
    for (auto it = m_tablemodel->m_mapvalues.begin(); it != m_tablemodel->m_mapvalues.end(); ++it) {
        hbm::ScalingParameterTarget newtarget;
        newtarget.setName(it->first);
        std::vector<double> newval;
        newtarget.setValue(it->second);
        target.add(newtarget);
    }
    //apply targets
    hbmscalingparameter::applyScalingParameterTarget(hbm, target);
    pInfo() << DONE << "Model parameter are scaled." ;
}

void ScalingParameter::loadTargetfromFile(QUrl const& filename) {
    if (filename.toLocalFile().toStdString().size() > 0) {
        void(*loadTargetScaleFactorWrapper)(piper::scalingparameter::ScalingParameter*, void (piper::scalingparameter::ScalingParameter::*)(std::string const&), std::string const&)
            = &piper::wrapClassMethodCall < piper::scalingparameter::ScalingParameter, void(piper::scalingparameter::ScalingParameter::*)(std::string const&), std::string const& >;

        Context::instance().performAsynchronousOperation(std::bind(loadTargetScaleFactorWrapper,
            this, &piper::scalingparameter::ScalingParameter::doLoadTargetfromFile, filename.toLocalFile().toStdString()),
            false, false, false, false, false);
    }
}

void ScalingParameter::doLoadTargetfromFile(std::string const& filename) {
	std::ifstream myFile;
    myFile.open(filename);
    if(!myFile.is_open()){
        std::cerr<<" unable to open "<<filename <<std::endl;
        return;
    }
 
	std::string name;
    double value;
    std::vector<double> vvalue;
    std::string myString;
    while(!myFile.eof())
    {
        getline(myFile, myString);
        std::stringstream elem(myString.c_str());
        if (elem >> name){
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);
            vvalue.clear();
            while (elem >> value)
                vvalue.push_back(value);
            if (m_scalefactor.find(name) != m_scalefactor.end())
                m_scalefactor[name] = vvalue;
            else
                pWarning() << " Parameter ( " << QString::fromStdString(name) << " ) is not defined in metadata of the model. Target scale factor value can not be applied" ;
        }
    }
	myFile.close();
    m_tablemodel->construct(m_scalefactor);
    return;
}

void ScalingParameter::loadTargetfromContext() {
    std::list<piper::hbm::ScalingParameterTarget>const& targetlist = Context::instance().project().target().scalingparameter;
    for (auto const& cur : targetlist) {
        if (m_scalefactor.find(cur.name()) != m_scalefactor.end())
            m_scalefactor[cur.name()] = cur.value();
        else
            pWarning() << " For Target value ( " << QString::fromStdString(cur.name()) << " ), no parameter with same name is defined in metadata.";
    }
    m_tablemodel->construct(m_scalefactor);
    return;


}

void ScalingParameter::saveTargettoContext() const {
    piper::hbm::TargetList& targetlist = Context::instance().project().target();
    targetlist.scalingparameter.clear();
    for (auto it = m_tablemodel->m_mapvalues.begin(); it != m_tablemodel->m_mapvalues.end(); ++it) {
        if (it->second[0] != 1.0) {
            piper::hbm::ScalingParameterTarget newTarget;
            newTarget.setName(it->first);
            newTarget.setValue(it->second);
            targetlist.add(newTarget);
        }
    }
    emit Context::instance().targetChanged();
}

void ScalingParameter::resetTarget() {
    for (auto & cur : m_scalefactor) {
        cur.second.clear();
        cur.second.push_back(1.0);
    }
    m_tablemodel->clear();
    m_tablemodel->construct(m_scalefactor);
}


}

}
