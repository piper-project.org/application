/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "common/Context.h"

#include <QObject>
#include <QString>
#include <QUrl>
#include <QtCore>
#include <QtWidgets>


namespace piper {
namespace scalingparameter {

struct ModelParameterItem {
    ModelParameterItem(QString name, double scalefactor): m_name(name), m_scalefactor(scalefactor) {}
    QString m_name;
    double m_scalefactor;
};

class TableParameterModel : public QAbstractListModel 
{
    Q_OBJECT
public:
	TableParameterModel( QObject *parent = 0): QAbstractListModel (parent) {}
    TableParameterModel(std::map<std::string, std::vector<double>>& listnames, QObject *parent = 0);
    ~TableParameterModel();
	//implement abstract method of QAbstractTableModel
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
	QHash<int, QByteArray> roleNames() const;
	enum Roles {
        NameRole = Qt::UserRole + 1,
        ValueRole
	};
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	Q_INVOKABLE bool setData(int row, int column, const QVariant value)
	{
		int role = Qt::UserRole + 1 + column;
		return setData(index(row,0), value, role);
	}
	Qt::ItemFlags flags(const QModelIndex &index) const;


	void construct( std::map<std::string, std::vector<double>> mapvalues);
	void clear();
	//private:
    QList<ModelParameterItem*> items;
    std::map<std::string, std::vector<double>> m_mapvalues;
};



class ScalingParameter : public QObject
{
    Q_OBJECT
        Q_PROPERTY(TableParameterModel* m_tablemodel READ getModel NOTIFY modelChanged)
public:
    ScalingParameter();
    ~ScalingParameter();
	Q_INVOKABLE TableParameterModel* getModel( ) ;
    
    //Q_INVOKABLE void applyScaling();
    Q_INVOKABLE void loadTargetfromFile(QUrl const& filename);
    Q_INVOKABLE void loadTargetfromContext();
    Q_INVOKABLE void saveTargettoContext() const;
    Q_INVOKABLE void resetTarget();
    Q_INVOKABLE void setResultinHistory(QString const& historyName);
    void doLoadTargetfromFile(std::string const&  filename);

public slots:
void doInitscaling();

signals:
    void modelChanged();
private:
    std::map<std::string, std::vector<double>> m_scalefactor;
    TableParameterModel* m_tablemodel;

    //void setOperationSignals(bool modelScaled);
    void doApplyScaling();
    void doSetResultinHistory(std::string const& historyname);

};

}
}

Q_DECLARE_METATYPE(piper::scalingparameter::TableParameterModel*)
