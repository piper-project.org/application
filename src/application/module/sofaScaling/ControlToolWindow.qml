// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2
import QtQml 2.2

import SofaBasics 1.0
import SofaScene 1.0

import Piper 1.0

ModuleToolWindow {
    title: "Control"
    property Action sofaSceneAnimateAction : Action {
        text: "Scaling"
        tooltip: "Start/Stop the scaling process (F4)"
        checkable: true
        onToggled: {
            if(sofaScene) {
                sofaScene.animate = checked;
                fpsDisplay.enabled = checked;
            }
        }
    }

    function init() {
//        fixedBonesSwitch.checked = true;
        handleStiffness.setValue(moduleParameter.PhysScale_targetDefaultStiffness);
        skinYoungModulus.setValue(moduleParameter.PhysScale_skinYoungModulus);
    }

    Connections {
        target: sofaScene
        onAnimateChanged: {
            sofaSceneAnimateAction.checked = sofaScene.animate;
        }
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed();
    }

    GridLayout {
        columns: 3
        // simu
        Button {
            action: sofaSceneAnimateAction
        }
        FPSItem {
            id: fpsDisplay
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignRight
            enabled: false
            horizontalAlignment: Text.AlignHRight
            verticalAlignment: Text.AlignVCenter
        }
        Button {
            id: resetButton
            text: "Reset"
            tooltip: "Reset to the initial position"
            Layout.alignment: Qt.AlignRight
            onClicked: {
                if(sofaScene)
                    sofaScene.reset();
            }
        }
//        Label {
//            text: "Fixed bones"
//        }
//        Switch {
//            id: fixedBonesSwitch
//            Layout.columnSpan: 2
//            checked: false
//            enabled: !sofaScene.animate
//            onCheckedChanged: {
//                sofaScene.sofaPythonInteractor.call("simulationController", "setBonesFixed", checked);
//            }
//        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Handle stiffness:"
        }
        StiffnessInput {
            id: handleStiffness
            Layout.columnSpan: 2
            onValueChanged: {
                if (sofaScene.ready)
                    sofaScene.sofaPythonInteractor.call("skinNodeController", "setCompliance", 1./value);
            }
            ToolTip {
                text: "Point handle target stiffness"
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Skin young modulus:"
        }
        StiffnessInput {
            id: skinYoungModulus
            Layout.columnSpan: 2
            onValueChanged: {
                if (sofaScene.ready)
                    sofaScene.sofaPythonInteractor.call("simulationController", "setSkinYoungModulus", value);
            }
            ToolTip {
                text: "Skin triangle FEM young modulus"
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Error ("+context.modelLengthUnit+"):"
        }
        RowLayout {
            id: errorItem
            Layout.columnSpan: 2

            property var errorNbDecimals : {"mm": 1, "cm": 2, "dm": 3, "m": 4}

            function updateDistance() {
                var distance = sofaScene.sofaPythonInteractor.call("skinNodeController", "getTargetError");
                cumDistanceItem.text = distance[0].toFixed(errorNbDecimals[context.modelLengthUnit]);
                maxDistanceItem.text = distance[1].toFixed(errorNbDecimals[context.modelLengthUnit]);
            }
            Connections {
                target: sofaScene
                onStepEnd : errorItem.updateDistance()
                onReseted : errorItem.updateDistance()
            }

            TextField {
                id: cumDistanceItem
                implicitWidth: 60
                readOnly: true
                text: "0"
                ToolTip {
                    text: "Point handles cumulated error"
                }
            }
            TextField {
                id: maxDistanceItem
                implicitWidth: 60
                readOnly: true
                text: "0"
                ToolTip {
                    text: "Point handles maximum error"
                }
            }
        }
//        Label {
//            Layout.alignment: Qt.AlignRight
//            text: "Scale skeleton:"
//        }
//        RowLayout {
//            id: scaleSkeleton
//            Layout.columnSpan: 2
//            function setSkeletonScale() {
//                sofaScene.sofaPythonInteractor.call("simulationController", "setSkeletonScale", sx.value, sy.value, sz.value);
//            }
//            SpinBox {
//                id: sx
//                minimumValue: 0.1
//                maximumValue: 10
//                decimals: 2
//                stepSize: 0.1
//                value: 1
//                onValueChanged: scaleSkeleton.setSkeletonScale()
//            }
//            SpinBox {
//                id: sy
//                minimumValue: 0.1
//                maximumValue: 10
//                decimals: 2
//                stepSize: 0.1
//                value: 1
//                onValueChanged: scaleSkeleton.setSkeletonScale()
//            }
//            SpinBox {
//                id: sz
//                minimumValue: 0.1
//                maximumValue: 10
//                decimals: 2
//                stepSize: 0.1
//                value: 1
//                onValueChanged: scaleSkeleton.setSkeletonScale()
//            }

//        }
    }
}
