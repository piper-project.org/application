# Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv Paruchuri,
# Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT); Thomas
# Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-Ifsttar)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import numpy.linalg as la

import Sofa
import SofaPython.Tools
from SofaPython.Tools import listToStr
import SofaPython.units
import LinearSubspace.API

import SofaPiper.sml
import SofaPiper.tools

import piper.app
import piper.hbm
import piper.sofa

SofaPython.Tools.meshLoader = SofaPiper.tools.meshLoader
#SofaPython.mass.RigidMassInfo.setFromMesh = SofaPiper.tools.RigidMassInfo_setFromMesh

skinColor = [1.,0.75,0.75,1.0]
defaultTargetStiffness = 1e5
defaultYoungModulus = 1e5
skinThickness = 0.002 # m

def createSceneAndController(rootNode):
    global sfMesh, lsDof, skinCollisionNode, skinVisualNode, boneVisualNode, allNodesNode
    piper.app.logStart("Loading physics Scaling...")

    SofaPython.units.setLocalUnits(length = piper.hbm.unitToStr(piper.sofa.project().model().metadata().lengthUnit()))
    model = SofaPiper.sml.Model(doAddJoint=False, doAddContact=False, doAddCapsule=False, doAddLigament=False)

    rootNode.createObject("RequiredPlugin", name="SofaPiper")
    rootNode.createObject("RequiredPlugin", name="Flexible")
    rootNode.createObject("RequiredPlugin", name="Compliant")
    rootNode.createObject("RequiredPlugin", name="LinearSubspace")
    rootNode.createObject("RequiredPlugin", name="SofaTetGen")
    rootNode.createObject('ClipPlane', name="clipPlane", normal="1 0 0", position="0 0 0", active=False)
    rootNode.gravity= [0, 0, 0]

    rootNode.createObject("PythonScriptController", name="simulationController", filename=__file__, classname="SimulationController")
    rootNode.createObject("PythonScriptController", name="displayController", filename=__file__, classname="DisplayController")
    rootNode.createObject("PythonScriptController", name="skinNodeController", filename=__file__, classname="SkinNodeController")

    rootNode.createObject("EulerImplicitSolver")
    rootNode.createObject("CGLinearSolver", iterations="25", tolerance="1e-05", threshold="1e-05")

    dataNode = rootNode.createChild("data")
    lsNode = rootNode.createChild("ls")

    sfMesh = LinearSubspace.API.ShapeFunctionMesh(lsNode)

    hbmModel = piper.sofa.project().model()
    skinEntities = []
    for solid in model.getSolidsByTags({"skin"}):
        for mesh in solid.mesh:
            skinEntities.append(mesh.name)
    skinMeshLoader = SofaPython.Tools.meshLoader(dataNode, listToStr(skinEntities))
    skinMeshClosing = dataNode.createObject("MeshClosingEngine", name="closing", inputPosition="@"+skinMeshLoader.getPathName()+".position", inputTriangles="@"+skinMeshLoader.getPathName()+".triangles", inputQuads="@"+skinMeshLoader.getPathName()+".quads")
    visualNode = dataNode.createChild("visual")
    visualNode.createObject("VisualModel", name="skinBase", src="@../closing", color="0 1 0 0.3")
    sfMesh.addEnvelop(solid.id, skinMeshClosing)
    for solid in model.getSolidsByTags({"bone"}):
        for mesh in solid.mesh:
            sfMesh.addInnerPoints(solid.id, SofaPython.Tools.meshLoader(dataNode, mesh.name))

    tetrahedrizer = sfMesh.addTetGenTetrahedrization()
    tetrahedrizer.display_constaints = True

    lsDof = LinearSubspace.API.LSDof(sfMesh)
    # add all bone nodes to a single affine
    boneIndices = lsDof.node.createObject("GenerateIndicesRangeEngine", template="Vec3", name="boneIndices", startIndexAsVectorSize="@"+skinMeshClosing.getPathName()+".position", sizeAsVectorSize = "@"+sfMesh.mergeInnerPoints.getPathName()+".position")
    lsDof.addAffineFromExternalRoi(boneIndices.getPathName()+".indices")
    lsDof.addPointFromEnvelopSampler( piper.sofa.project().moduleParameter().getUInt("PhysScale", "nbPointHandles") )
    lsDof.addMechanicalObjects()

    pointDof = lsDof.pointNode.getObject("dof")
    pointDof.tags="skin"
    pointDof.showObject = False
    pointDof.showObjectScale = SofaPython.units.length_from_SI(0.005)
    pointDof.showColor="1 0 0 1"
    pointDof.drawMode = 1

    lsDof.affineNode.createObject("FixedConstraint", name="fixedConstraint")

    sf = LinearSubspace.API.ShapeFunction(sfMesh, lsDof)
    sf.addJaconsonShapeFunction(perform_tests=False)

    skinCollisionNode = lsDof.createChild("skinCollision")
    skinCollisionNode.createObject("MeshTopology", name="topology", src="@"+skinMeshLoader.getPathName())
    skinCollisionNode.createObject("MechanicalObject", template="Vec3", name="dof", position="@"+skinMeshLoader.getPathName()+".position")
    skinCollisionNode.createObject("TriangleFEMForceField", name="ff", thickness = SofaPython.units.length_from_SI(skinThickness), youngModulus=SofaPython.units.elasticity_from_SI(defaultYoungModulus))
    skinCollisionNode.createObject("UniformMass", name="mass", totalmass=10) # TODO compute the mass based on the volume of the skin
    lsDof.insertLSMapping(skinCollisionNode, isMechanical=True)

    skinVisualNode = skinCollisionNode.createChild("skinVisual")
    skinVisualNode.createObject("VisualStyle", name="visualStyle", displayFlags="hideWireframe")
    skinVisualNode.createObject("VisualModel", name="skinVisual", src="@"+sfMesh.getEnvelopObject().getPathName(), color=listToStr(skinColor))
    skinVisualNode.createObject("IdentityMapping")

    boneCollisionNode = lsDof.createChild("boneCollision")
    boneCollisionNode.createObject("MechanicalObject", template="Vec3", name="dof", position="@"+sfMesh.mergeInnerPoints.getPathName()+".position")
    lsDof.insertLSMapping(boneCollisionNode, isMechanical=False)
    boneVisualNode = boneCollisionNode.createChild("boneVisual")
    boneVisualNode.createObject("VisualStyle", name="visualStyle", displayFlags="hideWireframe")
    boneVisualNode.createObject("VisualModel", name="skinVisual", src="@"+sfMesh.mergeInnerPoints.getPathName(), color="1 1 1 1")
    boneVisualNode.createObject("IdentityMapping")

    allNodesNode = lsDof.createChild("allNodes")
    allNodesNode.createObject("PiperMeshLoader", name="loader", loadAllNodes=True)
    allNodesNode.createObject("MechanicalObject", name="allNodes", src="@loader", showObject=True)
    allNodesNode.createObject("PiperExporter", template="Vec3", name="exporter", guiEventName="exportFullModel", positions="@allNodes.position", positionsId="@loader.positionId")
    lsDof.insertLSMapping(allNodesNode, isMechanical=False)

def bwdInitGraph(node):
    piper.app.logDebug("Generated {0} tetrahedron".format(sfMesh.topology.getNbTetrahedra()))
    piper.app.logInfo("Size of the system - affine: {0} | points: {1}".format(1, len(lsDof.pointNode.getObject("dof").position)))
    piper.app.logDone("")
    return node

class SimulationController(Sofa.PythonScriptController) :

    def createGraph(self, node):
        self.bonesFixedConstraint = None

    def setBonesFixed(self, isFixed):
        if isFixed and self.bonesFixedConstraint is None:
            self.bonesFixedConstraint = lsDof.affineNode.createObject("FixedConstraint", warning=False, name="fixedConstraint")
            self.bonesFixedConstraint.init()
        elif not isFixed and not self.bonesFixedConstraint is None:
            lsDof.affineNode.removeObject(self.bonesFixedConstraint)
            self.bonesFixedConstraint=None

    def setAllNodesActivated(self,activated):
        allNodesNode.activated = activated

    def setSkinYoungModulus(self, youngModulus):
        ff = skinCollisionNode.getObject("ff")
        ff.youngModulus = SofaPython.units.elasticity_from_SI(youngModulus)
        ff.reinit()

    def setSkeletonScale(self, sx, sy, sz):
        piper.app.logDebug("scale skeleton {0} {1} {2}".format(sx, sy, sz))
        affineDof = lsDof.affineNode.getObject("dof")
        pos = affineDof.position
        pos[0][3] = sx
        pos[0][7] = sy
        pos[0][11] = sz
        affineDof.position = pos

class DisplayController(Sofa.PythonScriptController) :
    # TODO remove duplicated code with sofa positioning modules

    # Qt GUI enum constant
    Qt_UnChecked=0
    Qt_PartiallyChecked=1
    Qt_Checked=2

    defaultEnvColor= [1., 1., 1., 1.]

    def createGraph(self, node):
        self.rootNode = node
        self.envNode = None
        self.envTransformation = dict() # workaround a strange behavior of the sofa reset()

    def initGraph(self,node):
        self.updateEnvironment()

    def setShowReferenceSkin(self, show):
        visualNode = SofaPython.Tools.getNode(self.rootNode,"/data/visual")
        visualNode.activated = show

    def setShowSkin(self, show):
        skinVisualNode.activated = (show != DisplayController.Qt_UnChecked)
        if (show != DisplayController.Qt_UnChecked):
            skinVisualNode.propagatePositionAndVelocity()
            skinVisualNode.getObject("visualStyle").displayFlags = "hideWireframe" if (show != DisplayController.Qt_PartiallyChecked) else "showWireframe"

    def setSkinOpacity(self, opacity):
        color = skinColor
        color[3] = opacity
        skinVisualNode.getObject("skinVisual").setColor(color[0], color[1], color[2], color[3])

    def setShowBones(self, show):
        boneVisualNode.activated = (show != DisplayController.Qt_UnChecked)
        if (show != DisplayController.Qt_UnChecked):
            boneVisualNode.propagatePositionAndVelocity()
            boneVisualNode.getObject("visualStyle").displayFlags = "hideWireframe" if (show != DisplayController.Qt_PartiallyChecked) else "showWireframe"

    def setShowPointHandles(self, show):
        lsDof.pointNode.getObject("dof").showObject = show

    def setShowTetrahedron(self, show):
        sfMesh.topology.drawTetrahedra = show

    def updateEnvironment(self):
        if not self.envNode is None:
            self.envNode.detachFromGraph()
        self.envTransformation.clear()
        self.envNode = self.rootNode.createChild("environment")
        for envName in piper.sofa.project().environment().getListNames():
            self.envTransformation[envName] = dict()
            myEnvNode = self.envNode.createChild(envName)
            myEnvNode.createObject("PiperMeshLoader", name="loader", environmentName=envName)
            myEnvNode.createObject("MechanicalObject", name="dofs", src="@loader")
            mappingNode = myEnvNode.createChild("mapping")
            mappingNode.createObject("OglModel", name="visual", src="@../loader", color = SofaPython.Tools.listToStr(DisplayController.defaultEnvColor))
            mappingNode.createObject("IdentityMapping", name="mapping")
        self.envNode.init()

    def updateAllEnvironmentPosition(self):
        for envName, envTrans in self.envTransformation.iteritems():
            if "translation" in envTrans:
                t = envTrans["translation"]
                self.setEnvironmentTranslation(envName, t[0], t[1], t[2])
            if "rotation" in envTrans:
                r = envTrans["rotation"]
                self.setEnvironmentRotation(envName, r[0], r[1], r[2])
            if "scale" in envTrans:
                s = envTrans["scale"]
                self.setEnvironmentScale(envName, s[0], s[1], s[2])

    def setEnvironmentTranslation(self, name, x, y, z):
        loader = self.envNode.getObject(name+"/loader")
        self.envTransformation[name]["translation"] = [x,y,z]
        if not loader is None:
            loader.translation = [x,y,z]
            loader.reinit()

    def setEnvironmentRotation(self, name, rx, ry, rz):
        loader = self.envNode.getObject(name+"/loader")
        self.envTransformation[name]["rotation"] = [rx,ry,rz]
        if not loader is None:
            loader.rotation = [rx,ry,rz]
            loader.reinit()

    def setEnvironmentScale(self, name, sx, sy, sz):
        if sx==0 or sy==0 or sz==0: # it leads to invalid transformation matrix
            return
        self.envTransformation[name]["scale"] = [sx,sy,sz]
        loader = self.envNode.getObject(name+"/loader")
        if not loader is None:
            loader.scale3d = [sx,sy,sz]
            loader.reinit()

    def setEnvironmentOpacity(self, alpha):
        for myEnvNode in self.envNode.getChildren():
            visual = myEnvNode.getObject("mapping/visual")
            visual.setColor(DisplayQmlInteractor.defaultEnvColor[0], DisplayQmlInteractor.defaultEnvColor[1], DisplayQmlInteractor.defaultEnvColor[2], alpha)


class SkinNodeController(Sofa.PythonScriptController) :
    def initGraph(self,node):
        self.skinNodeControllers = dict()
        self.compliance = 1. / defaultTargetStiffness

    def addController(self, index, targetValue):
        piper.app.logDebug("Add controller on handle {0}".format(index))
        node = lsDof.pointNode.createChild("node_{0}".format(index))
        node.createObject("MechanicalObject", template="Vec3", name="dof")
        node.createObject("DifferenceFromTargetMapping", name="mapping", indices=str(index), targets=listToStr(targetValue))
        node.createObject("UniformCompliance", name="compliance", compliance=self.compliance, isCompliance=False)
        node.init()
        self.skinNodeControllers[index] = node

    def removeController(self, index):
        piper.app.logDebug("Remove controller on handle {0}".format(index))
        self.skinNodeControllers[index].detachFromGraph()
        del self.skinNodeControllers[index]

    def setTarget(self, index, value):
        self.skinNodeControllers[index].getObject("mapping").targets = listToStr(value)

    def getTarget(self, index):
        return self.skinNodeControllers[index].getObject("mapping").targets[0]

    def getCurrentPosition(self, index):
        return lsDof.pointNode.getObject("dof").position[index]

    def setCompliance(self, compliance) :
        self.compliance = compliance
        for controller in self.skinNodeControllers.itervalues():
            uc = controller.getObject("compliance")
            uc.compliance = compliance
            uc.reinit()

    def getTargetError(self):
        cumError = 0
        maxError = 0
        for controller in self.skinNodeControllers.itervalues():
            pos = controller.getObject("dof").position
            d = la.norm(pos)
            cumError += d
            if d > maxError:
                maxError = d
        return [cumError, maxError]


