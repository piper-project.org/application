// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

import SofaScene 1.0
import SofaApplication 1.0
import SofaInteractors 1.0
import SofaManipulators 1.0

import Piper 1.0

ModuleToolWindow {
    id: root

    title: "Skin Node Controller"

    property var controlledHandlesList: []

    property Action interactor: Action {
        checkable: true
        checked: false
        iconSource: "qrc:/icon/cursor.png"
        text: "P"
        tooltip: "Point handle interactor"
        property string leftButtonHelp: "click on a point handle (red) to add a target, click again to remove it, click on a target to move it by dragging the arrows"
        property string middleButtonHelp: cameraInteractor.middleButtonHelp
        property string rightButtonHelp: cameraInteractor.rightButtonHelp
        onToggled: {
            if (checked)
                SofaApplication.interactorComponent = skinNodeInteractor;
            displayToolWindow.setPointHandlesVisible(checked);
        }
    }

    property Component skinNodeInteractor: UserInteractor_MoveCamera {
        hoverEnabled: true

        function init() {
            moveCamera_init();

            addMousePressedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                // try to catch a manipulator
                var selectableManipulator = sofaViewer.pickObjectWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["manipulator"]);
                if(selectableManipulator && selectableManipulator.manipulator) {
                    sofaScene.selectedManipulator = selectableManipulator.manipulator;
                    if(sofaScene.selectedManipulator.mousePressed)
                        sofaScene.selectedManipulator.mousePressed(mouse, sofaViewer);
                    if(sofaScene.selectedManipulator.mouseMoved)
                        setMouseMovedMapping(sofaScene.selectedManipulator.mouseMoved);
                    return;
                }

                // try to catch a skin handle
                var selectableParticle = sofaViewer.pickParticleWithTags(Qt.point(mouse.x + 0.5, mouse.y + 0.5), ["skin"]);
                if(selectableParticle && selectableParticle.sofaComponent) {
                    sofaScene.selectedComponent = selectableParticle.sofaComponent;
                    if (-1 === controlledHandlesList.indexOf(selectableParticle.particleIndex))
                        addController(selectableParticle.particleIndex);
                    else
                        removeController(selectableParticle.particleIndex);
                    return;
                }
            });

            addMouseReleasedMapping(Qt.LeftButton, function(mouse, sofaViewer) {
                if(sofaScene.selectedManipulator && sofaScene.selectedManipulator.mouseReleased)
                    selectedManipulator.mouseReleased(mouse, sofaViewer);
                sofaScene.selectedManipulator = null;
                setMouseMovedMapping(null);
            });
        }

    }

    function removeManipulator(index) {
        if (null !== sofaScene.selectedManipulator && index.toString() === sofaScene.selectedManipulator.objectName) {
            // kind of impossible case, just in case
            sofaScene.selectedManipulator = null;
        }
        var manip = sofaScene.removeManipulatorByName(index.toString());
        manip[0].destroy();
    }

    Component {
        id: manipulatorComponent

        Manipulator {
            id: manipulator

            property var sofaScene
            property int index: -1

            Manipulator3D_Translation {
                id: tx
                visible: manipulator.visible
                axis: "x"
                onPositionChanged: {
                    manipulator.position = position;
                }
            }

            Manipulator3D_Translation {
                id: ty
                visible: manipulator.visible
                axis: "y"
                onPositionChanged: {
                    manipulator.position = position;
                }
            }

            Manipulator3D_Translation {
                id: tz
                visible: manipulator.visible
                axis: "z"
                onPositionChanged: {
                    manipulator.position = position;
                }
            }

            function updatePosition() {
                var positionArray = sofaScene.sofaPythonInteractor.call("skinNodeController", "getTarget", index);
                position = Qt.vector3d(positionArray[0], positionArray[1], positionArray[2]);
            }

            Component.onCompleted: updatePosition()

            onVisibleChanged: {
                if (visible) updatePosition();
            }

            onPositionChanged: {
                sofaScene.sofaPythonInteractor.call("skinNodeController", "setTarget", index, [position.x,position.y,position.z]);
            }
        }
    }

    function addController(index) { // initialValue
        var currentPosition = sofaScene.sofaPythonInteractor.call("skinNodeController", "getCurrentPosition", index);
        sofaScene.sofaPythonInteractor.call("skinNodeController", "addController", index, currentPosition);
        var myManipulator = manipulatorComponent.createObject(root, {objectName:index.toString(), sofaScene: sofaScene, index: index, visible: Qt.binding(function () {return root.interactor.checked;})})
        sofaScene.addManipulator(myManipulator);
        controlledHandlesList.push(index);
    }

    function removeController(index) {
        sofaScene.sofaPythonInteractor.call("skinNodeController", "removeController", index);
        removeManipulator(index);
        controlledHandlesList.splice(controlledHandlesList.indexOf(index),1);
    }

//    function init() {
//        stiffness.setValue(moduleParameter.PhysScale_targetDefaultStiffness);
//    }

//    GridLayout {
//        columns: 2
//        Label {
//            Layout.alignment: Qt.AlignRight
//            text: "Stiffness:"
//        }
//        StiffnessInput {
//            id: stiffness
//            onValueChanged: {
//                if (sofaScene.ready)
//                    sofaScene.sofaPythonInteractor.call("skinNodeController", "setCompliance", 1./value);
//            }
//            ToolTip {
//                text: "Skin nodes target stiffness"
//            }
//        }
//    }
}
