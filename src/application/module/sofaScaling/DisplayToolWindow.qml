// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQml 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import SofaBasics 1.0
import SofaScene 1.0

import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Display"

    function init() {
        sofaScene.sofaPythonInteractor.call("displayController", "setSkinOpacity", skinOpacity.value);
        referenceSkinCheckBox.checked = false;
    }

    function setPointHandlesVisible(visible) {
        pointHandlesCheckBox.checked = visible;
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed()
    }

    Settings {
        id: settings
        category: "sofaScalingViewer"
        property double skinOpacity: 1.0
    }

    Component.onDestruction: {
        settings.skinOpacity = skinOpacity.value;
    }

    ColumnLayout {
        GroupBox {
            Layout.fillWidth: true
            title: "Entity"
            GridLayout {
                columns: 2
                CheckBox {
                    text: "Bones"
                    partiallyCheckedEnabled: true
                    checkedState: Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowBones", checkedState)
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide bones mesh"
                    }
                }
                CheckBox {
                    text: "Environment"
                    checked: true
                    onCheckedChanged: sofaScene.setDataValue("@/environment.activated", checked);
                    ToolTip {
                        text: "Show/Hide environment mesh"
                    }
                }
                CheckBox {
                    id: referenceSkinCheckBox
                    text: "Reference skin"
                    checked: true
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowReferenceSkin", checked)
                    }
                    ToolTip {
                        text: "Show/Hide the original skin"
                    }
                }
                CheckBox {
                    Layout.row: 2
                    text: "Skin"
                    partiallyCheckedEnabled: true
                    checkedState: Qt.Checked
                    onCheckedStateChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowSkin", checkedState);
                    }
                    ToolTip {
                        text: "Show/Wireframe/Hide skin mesh"
                    }
                }
                RowLayout {
                    Label { text: "opacity:" }
                    SpinBox {
                        id: skinOpacity
                        minimumValue: 0
                        maximumValue: 1
                        decimals: 1
                        stepSize: 0.1
                        value: settings.skinOpacity
                        onValueChanged: {
                            if (sofaScene.ready)
                                sofaScene.sofaPythonInteractor.call("displayController", "setSkinOpacity", value);
                        }
                        ToolTip {
                            text: "Set skin opacity (0:transparent, 1:opaque)"
                        }
                    }
                }
            }
        }
        GroupBox {
            Layout.fillWidth: true
            title: "Simulation"
            GridLayout {
                columns: 2
                CheckBox {
                    id: pointHandlesCheckBox
                    text: "Point handles"
                    checked: false
                    onCheckedChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowPointHandles", checked)
                    }
                    ToolTip {
                        text: "Show/Hide simulation point degrees of freedom"
                    }
                }
                CheckBox {
                    text: "Tetrahedron"
                    checked: false
                    onCheckedChanged: {
                        sofaScene.sofaPythonInteractor.call("displayController", "setShowTetrahedron", checked)
                    }
                    ToolTip {
                        text: "Show/Hide generated tetrahedron used to compute skinning weights"
                    }
                }
            }
        }
        ClipPlane {
            property var unitValue: {"m":1., "dm":0.1, "cm":0.01, "mm": 0.001} // TODO make s.t. reusable and derived from c++
            property var unitValueNbDecimal: {"m":3, "dm":2, "cm":1, "mm": 0} // TODO check available units
            clipPlaneComponentPath: "/clipPlane"
            distanceMinMax: 2./unitValue[context.modelLengthUnit]
            distanceNbDecimals: unitValueNbDecimal[context.modelLengthUnit]
        }
    }
}
