/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ContourPersonalization.h"

#include <vtkTransformPolyDataFilter.h>
#include <vtkCenterOfMass.h>
#include <vtkMapper.h>

using namespace std;
using namespace piper::hbm;

namespace piper {
	namespace contours {

		ContourPersonalization::ContourPersonalization() :
			main_repo(QCoreApplication::applicationDirPath().toStdString()),
			dmp(QCoreApplication::applicationDirPath().toStdString())
		{}

		ContourPersonalization::~ContourPersonalization() { }

		void ContourPersonalization::displayModel(){
			Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::ENTITIES);
		}

#pragma region UICalls

		bool ContourPersonalization::loadContourCLXml(const QUrl& modelFile){
			m_meta.ctrl_line.undefinedCLElements.clear();
			std::string file = modelFile.toLocalFile().toStdString();
			pskeleton = ContourGen::getInstance();
			pskeleton->Init();  // set display mode and refresh actors
			if (main_repo.is_file_exist(file.c_str())){
				if (pskeleton->ReadContourCL(file) == tinyxml2::XML_SUCCESS)
				{
					pskeleton->ComputeSkeleton();
					getDimensions();
					loadTargets();
				}
                else
                {
                    pCritical() << "file parsing error";
                    return false;
                }
			}
			else{
				pCritical() << "file does not exist";
                return false;
			}
            return true;
		}

		void ContourPersonalization::setOpacity(float value){
			Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::ENTITIES, value);
			Context::instance().display().Refresh();
		}

		void ContourPersonalization::setCLOpacity(float value){
			Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::GEBOD, value);
			Context::instance().display().Refresh();
		}

		void ContourPersonalization::generateContBusyIndicator(int visible)
		{
			Context::instance().performAsynchronousOperation(std::bind(&ContourPersonalization::contourDisplay, this, visible), false, false, false, false, false);
		}

		void ContourPersonalization::contourDisplay(int visible){
			if (visible == 1){
				pskeleton = ContourGen::getInstance();
				pskeleton->ComputeContours();
				getContourActorList();
				saveInitialContourData();
				emit Context::instance().metadataChanged();
			}
			else{
				pskeleton = ContourGen::getInstance();
				pskeleton->HideContoursListForBR();
			}
		}

		void ContourPersonalization::scaleBRContours(float scale_value, QString name){
			resetContours(); // Reset contours before scaling
			std::string contour_name = name.toStdString();
			std::string br_name = getContourBR(contour_name);
			changeContourBRCircumference(scale_value, br_name, contour_name);
		}

		void ContourPersonalization::scaleTargetContourBR(){
			if (!Context::instance().project().model().empty()) {
				if (contourBRCircumferenceAnsurValues.size() > 0){
					for (auto const& dim : contourBRCircumferenceAnsurValues) {
						double br_circum = getContourBRCircumference(dim.first);
						if (br_circum > 0){
							double target_circum = dim.second;
							if (target_circum){
								double scale = target_circum / br_circum;
								changeContourBRCircumference(scale, dim.first);
							}
						}
					}
				}
			}
		}

		void ContourPersonalization::resetContours(){
			pskeleton = ContourGen::getInstance();
			pskeleton->UpdateContoursListForBR();
			final_contour_data.clear();
		}

		void ContourPersonalization::saveBRContours(QString name, bool is_br_name){
			std::vector<Node> contour_nodes;
			std::string br_name;

			if (!is_br_name){
				br_name = getContourBR(name.toStdString());
			}
			else{
				br_name = name.toStdString();
			}

			std::vector<std::string> contour_names = getBRContours(br_name);
			for (auto const& contour_name : contour_names) {
				for (auto const& node : getContourActorNodes(contour_name)) {
					contour_nodes.push_back(node);
				}
			}

			final_contour_data[br_name] = contour_nodes;
		}

		void ContourPersonalization::personalizeBusyIndicator(QString name, bool is_br_name, bool is_translate, bool refresh)
		{
			if (!Context::instance().project().model().empty()) {
				Context::instance().performAsynchronousOperation(std::bind(&ContourPersonalization::personalize, this, name, is_br_name, is_translate, refresh), false, false, false, false, false);
			}
		}

		void ContourPersonalization::personalize(QString name, bool is_br_name, bool is_translate, bool refresh){
			if (!Context::instance().project().model().empty()) {
				saveBRContours(name, is_br_name);

				std::string br_name;
				if (!is_br_name){
					br_name = getContourBR(name.toStdString());
				}
				else{
					br_name = name.toStdString();
				}

				std::string file_name = "perso_file";
				repo.write_delaunay_input_file_3(file_name + "_initial.k", true, initial_contour_data[br_name]);
				repo.write_delaunay_input_file_3(file_name + "_final.k", true, final_contour_data[br_name]);

				std::vector<NodePtr> nodes_vector;
				std::vector<std::string> contour_names = getBRContours(br_name);

				int i = 0;
				for (auto const& contour : contour_names) {
					if (i > 2 && i <= contour_names.size()){
						getNodesWithinBounds(contour_names.at(i - 1), contour_names.at(i), &nodes_vector);
					}
					i++;
				}

				//Remove duplicate nodes
				set<NodePtr> s(nodes_vector.begin(), nodes_vector.end());
				nodes_vector.assign(s.begin(), s.end());

				// Translation of all child body regions
				if (is_translate){
					translateContourBRChildren(br_name);
				}

				std::vector<point> vec_point;
				vec_point = dmp.call_delaunay(file_name + "_initial.node", file_name + "_final.node", nodes_vector);

				for (int i = 0; i < vec_point.size(); i++){
					NodePtr p = fem.get<Node>(vec_point.at(i).point_id);
					double x = vec_point.at(i).p_x;
					double y = vec_point.at(i).p_y;
					double z = vec_point.at(i).p_z;
					p->setCoord(x, y, z);
				}
				vec_point.clear();

				if (refresh){
					refreshDisplay();
				}
			}
		}

		void ContourPersonalization::personalizeTargetsBusyIndicator()
		{
			if (!Context::instance().project().model().empty()) {
				Context::instance().performAsynchronousOperation(std::bind(&ContourPersonalization::personalizeTargets, this), false, false, false, false, false);
			}
		}

		void ContourPersonalization::personalizeTargets(){
			pInfo() << START << "Personalization Started";
			if (!Context::instance().project().model().empty()) {
				std::vector<std::string> updatedContourBR;
				std::vector<std::string> translatedContourBR;
				for (auto const& dim : contourBRCircumferenceAnsur) {
					updatedContourBR.push_back(dim.first);
				}
				for (auto const& dim : contourBRLengthAnsur) {
					updatedContourBR.push_back(dim.first);
					translatedContourBR.push_back(dim.first);
				}

				//Removing Duplicates
				set<std::string> s(updatedContourBR.begin(), updatedContourBR.end());
				updatedContourBR.assign(s.begin(), s.end());

				// Personalize
				for (auto const& br_name : updatedContourBR) {
					bool translate = std::find(translatedContourBR.begin(), translatedContourBR.end(), br_name) != translatedContourBR.end();
					personalize(QString::fromStdString(br_name), true, translate, false);
				}
				refreshDisplay();
			}
			pInfo() << DONE;
		}

		double ContourPersonalization::contourBRLength(QString br_name){
			std::vector<std::string> contours;
			contours = getBRContours(br_name.toStdString());
			if (contours.size() >= 2){
				string start_contour = contours.at(0);
				string end_contour = contours.at(contours.size() - 1);
				double distance = distance2BetweenContourActors(start_contour, end_contour);
				return distance;
			}
			else{
				return 0.0;
			}
		}

		void ContourPersonalization::scaleContourBRLength(float scale, QString br_name, bool reset){
			if (reset){
				resetContours(); // Reset contours before scaling
			}
			std::vector<std::string> contours = getBRContours(br_name.toStdString());

			if (contours.size() >= 2){
				string contour1, contour2;
				contour1 = contours.at(0);
				contour2 = contours.at(contours.size() - 1);

				double initial_length = distance2BetweenContourActors(contour1, contour2);

				double source_point[3], target_point[3];
				centerOfMass(contour1, source_point);
				centerOfMass(contour2, target_point);

				double final_length = initial_length * scale;

				double distance = final_length - initial_length;

				vtkVector3d total_translation;
				translatePointOnLine(source_point, target_point, distance, total_translation);
				contourBRTotalTranslation[br_name.toStdString()] = total_translation;

				double unit_distance_initial = initial_length / contours.size();
				double unit_distance_final = final_length / contours.size();


				int i = 0;
				for (auto const& value : contours) {
					double translate[3];
					translate[0] = total_translation(0) * i / contours.size();
					translate[1] = total_translation(1) * i / contours.size();
					translate[2] = total_translation(2) * i / contours.size();
					translateContourActor(value, translate);
					i++;
				}
			}
		}

		void ContourPersonalization::scaleTargetContourBRLength(){
			if (!Context::instance().project().model().empty()) {
				if (contourBRLengthAnsurValues.size() > 0){
					for (auto const& dim : contourBRLengthAnsurValues) {
						double br_circum = getContourBRLength(dim.first);
						if (br_circum > 0){
							double target_circum = dim.second;
							if (target_circum){
								double scale = target_circum / br_circum;
								scaleContourBRLength(scale, QString::fromStdString(dim.first), false);
							}
						}
					}
				}
			}
		}

		void ContourPersonalization::loadTargets() {
			pInfo() << START << "Load anthropometricDimension targets";
			// Fill data
			contourBRCircumferenceAnsurValues.clear();
			contourBRLengthAnsurValues.clear();
			//Fill Circumference
			for (auto const& dim : contourBRCircumferenceAnsur) {
				bool flag = false;
				for (hbm::AnthropometricDimensionTarget const& bodysectiondimtarget : Context::instance().project().target().anthropometricDimension) {
					if (bodysectiondimtarget.name() == dim.second){
						contourBRCircumferenceAnsurValues[dim.first] = bodysectiondimtarget.value();
						flag = true;
					}
				}
				if (!flag){
					contourBRCircumferenceAnsurValues[dim.first] = 0;
				}
			}
			//Fill Length
			for (auto const& dim : contourBRLengthAnsur) {
				bool flag = false;
				for (hbm::AnthropometricDimensionTarget const& bodysectiondimtarget : Context::instance().project().target().anthropometricDimension) {
					if (bodysectiondimtarget.name() == dim.second){
						contourBRLengthAnsurValues[dim.first] = bodysectiondimtarget.value();
						flag = true;
					}
				}
				if (!flag){
					contourBRLengthAnsurValues[dim.first] = 0;
				}
			}
			emit Context::instance().metadataChanged();
			pInfo() << DONE;
		}
#pragma endregion
#pragma region Tables
		QList<QString> ContourPersonalization::getContourActorList(){
			contourActorList.clear();
			contourCircumferenceList.clear();
			std::string br_name;
			if (!Context::instance().project().model().empty()) {
				if (!Context::instance().project().model().empty()) {
					pskeleton = ContourGen::getInstance();
					std::vector< std::string > contourActors = pskeleton->getContourActors();
					for (auto const& value : contourActors) {
						contourCircumferenceList.append(getContourCircumference(value));
						contourActorList.append(QString::fromStdString(value));
						std::string new_br = getContourBR(value);
						if (new_br != br_name){
							contourBRList.append(QString::fromStdString(new_br));
							br_name = new_br;
						}
					}
				}
			}
			return contourActorList;
		}

		QList<QString> ContourPersonalization::getContourBRList()
		{
			if (!Context::instance().project().model().empty()) {
				return contourBRList;
			}
			else{
				QList<QString> datalist;
				return datalist;
			}
		}

		QList<double> ContourPersonalization::getContourCircumferenceList(){
			return contourCircumferenceList;
		}

		QList<double> ContourPersonalization::getContourBRCircumferenceList()
		{
			QList<double> datalist;
			if (!Context::instance().project().model().empty()) {
				for (auto const& dim : contourBRCircumferenceAnsur) {
					datalist.append(getContourBRCircumference(dim.first));
				}
			}
			return datalist;
		}

		QList<QString> ContourPersonalization::getContourBRCircumferenceTargetList(bool get_name)
		{
			QList<QString> datalist;
			if (!Context::instance().project().model().empty()) {
				for (auto const& dim : contourBRCircumferenceAnsur) {
					std::string val;
					if (get_name){
						val = dim.first;
					}
					else{
						val = dim.second;
					}
					datalist.append(QString::fromStdString(val));
				}
			}
			return datalist;
		}

		QList<double> ContourPersonalization::getContourBRCircumferenceTargetValueList()
		{
			QList<double> datalist;
			if (!Context::instance().project().model().empty()) {
				if (contourBRCircumferenceAnsurValues.size() > 0){
					for (auto const& dim : contourBRCircumferenceAnsurValues) {
						datalist.append(dim.second);
					}
				}
				else{
					for (auto const& dim : contourBRCircumferenceAnsur) {
						datalist.append(0);
					}
				}
			}
			return datalist;
		}

		QList<double> ContourPersonalization::getContourBRLengthList()
		{
			QList<double> datalist;
			if (!Context::instance().project().model().empty()) {
				for (auto const& dim : contourBRLengthAnsur) {
					datalist.append(getContourBRLength(dim.first));
				}
			}
			return datalist;
		}

		QList<QString> ContourPersonalization::getContourBRLengthTargetList(bool get_name)
		{
			QList<QString> datalist;
			if (!Context::instance().project().model().empty()) {
				for (auto const& dim : contourBRLengthAnsur) {
					std::string val;
					if (get_name){
						val = dim.first;
					}
					else{
						val = dim.second;
					}
					datalist.append(QString::fromStdString(val));
				}
			}
			return datalist;
		}

		QList<double> ContourPersonalization::getContourBRLengthTargetValueList()
		{
			QList<double> datalist;
			if (!Context::instance().project().model().empty()) {
				if (contourBRLengthAnsurValues.size() > 0){
					for (auto const& dim : contourBRLengthAnsurValues) {
						datalist.append(dim.second);
					}
				}
				else{
					for (auto const& dim : contourBRLengthAnsur) {
						datalist.append(0);
					}
				}
			}
			return datalist;
		}
#pragma endregion
#pragma region Functions
		void ContourPersonalization::saveInitialContourData(){
			for (auto const& value : contourBRList) {
				std::vector<Node> contour_nodes;
				std::vector<std::string> contour_names = getBRContours(value.toStdString());
				for (auto const& contour_name : contour_names) {
					for (auto const& node : getContourActorNodes(contour_name)) {
						contour_nodes.push_back(node);
					}
					// Storing as vtkPoints
					vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
					new_points = getContourActorPoints(contour_name);
					initial_contour_points[contour_name] = new_points;
				}
				initial_contour_data[value.toStdString()] = contour_nodes;
			}
		}

		void ContourPersonalization::refreshDisplay(){
			fem1.updateVTKRepresentation();
			pskeleton->ComputeSkeleton();
			pskeleton->ForceContourGen();
			Context::instance().display().Refresh();
			saveInitialContourData();
			emit Context::instance().metadataChanged();
		}

		void ContourPersonalization::getNodesWithinBounds(std::string contour1, std::string contour2, std::vector<piper::hbm::NodePtr>* nodes_vector){
			std::vector<piper::hbm::Node> nodes;
			vtkSmartPointer<vtkPoints> vtk_points1 = vtkSmartPointer<vtkPoints>::New();
			vtk_points1 = initial_contour_points[contour1];
			vtkSmartPointer<vtkPoints> vtk_points2 = vtkSmartPointer<vtkPoints>::New();
			vtk_points2 = initial_contour_points[contour2];
			// Appending the two vtkPoints
			int NumPts = vtk_points2->GetNumberOfPoints();
			for (int i = 0; i < NumPts; i++)
			{
				double p[3];
				vtk_points2->GetPoint(i, p);
				vtk_points1->InsertNextPoint(p);
			}
			double bounds[6];
			vtk_points1->GetBounds(bounds);

			double delta[3] = { 0, 0, 0 };
			piper::hbm::Nodes m_nodes = fem1.getNodes();
			for (auto const& node : m_nodes) {
				double p[3] = { node->getCoordX(), node->getCoordY(), node->getCoordZ() };
				if (vtkMath::PointIsWithinBounds(p, bounds, delta)){
					nodes_vector->push_back(node);
				}
			}
		}

		void ContourPersonalization::scaleContour(float scale_value, QString name){
			pskeleton = ContourGen::getInstance();
			pskeleton->scaleContour(scale_value, name.toStdString());
		}

		std::string ContourPersonalization::getContourBR(std::string name){
			std::string delimiter = "_contourelem";
			std::string br_name = name.substr(0, name.find(delimiter));
			return br_name;
		}

		std::vector<std::string> ContourPersonalization::getBRContours(std::string name){
			std::vector<std::string> contours;
			pskeleton = ContourGen::getInstance();
			std::vector< std::string > contourActors = pskeleton->getContourActors();

			for (auto const& value : contourActors) {
				if (value.find(name) != std::string::npos){
					contours.push_back(value);
				}
			}
			return contours;
		}

		std::vector<piper::hbm::Node> ContourPersonalization::getContourActorNodes(std::string name){
			std::vector<piper::hbm::Node> nodes;
			boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
			auto it = actors->find(name);
			if (it != actors->end())
			{
				vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
				polyDataOut->DeepCopy(it->second->GetMapper()->GetInput());
				for (vtkIdType i = 0; i < polyDataOut->GetNumberOfPoints(); i++){
					double p[3];
					polyDataOut->GetPoint(i, p);
					Node node;
					node.setCoord(p[0], p[1], p[2]);
					nodes.push_back(node);
				}
			}
			return nodes;
		}

		vtkSmartPointer<vtkPoints> ContourPersonalization::getContourActorPoints(std::string name){
			boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
			auto it = actors->find(name);
			if (it != actors->end())
			{
				vtkSmartPointer<vtkPolyData> polyDataOut = vtkSmartPointer<vtkPolyData>::New();
				polyDataOut->DeepCopy(it->second->GetMapper()->GetInput());
				vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
				new_points = polyDataOut->GetPoints();
				return new_points;
			}
			else{
				vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
				return new_points;
			}
		}

		void ContourPersonalization::centerOfMass(std::string actor_name, double center[3]){
			boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
			auto it = actors->find(actor_name);
			if (it != actors->end())
			{
				vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
				//Compute the center of mass
				vtkSmartPointer<vtkCenterOfMass> centerOfMassFilter =
					vtkSmartPointer<vtkCenterOfMass>::New();
				centerOfMassFilter->SetInputData(inputPolyData);
				centerOfMassFilter->SetUseScalarsAsWeights(false);
				centerOfMassFilter->Update();
				centerOfMassFilter->GetCenter(center);
			}
		}

		double ContourPersonalization::distance2BetweenContourActors(std::string contour1, std::string contour2){
			double contour1_center[3], contour2_center[3];
			centerOfMass(contour1, contour1_center);
			centerOfMass(contour2, contour2_center);

			// Find the squared distance between the points.
			double squaredDistance = vtkMath::Distance2BetweenPoints(contour1_center, contour2_center);

			// Take the square root to get the Euclidean distance between the points.
			double distance = sqrt(squaredDistance);

			return distance;
		}

		void ContourPersonalization::translatePointOnLine(double source_point[3], double target_point[3], double distance, vtkVector3d &translate){
			double v[3];

			//Calculate the vector fron source_point to target_point
			v[0] = target_point[0] - source_point[0];
			v[1] = target_point[1] - source_point[1];
			v[2] = target_point[2] - source_point[2];

			//Calculate the length
			double mag = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);

			// normalize the vector to unit length
			v[0] /= mag;
			v[1] /= mag;
			v[2] /= mag;

			//Calculate the new vector
			double new_point[3];
			new_point[0] = source_point[0] + v[0] * (mag + distance);
			new_point[1] = source_point[1] + v[1] * (mag + distance);
			new_point[2] = source_point[2] + v[2] * (mag + distance);

			translate.Set(new_point[0] - target_point[0], new_point[1] - target_point[1], new_point[2] - target_point[2]);
		}

		void ContourPersonalization::translateContourActor(std::string name, double translate[3]){
			boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *actors = Context::instance().display().GetDisplayActors();
			auto it = actors->find(name);
			if (it != actors->end())
			{
				vtkPolyData *inputPolyData = vtkPolyData::SafeDownCast(it->second->GetOriginalDataSet());
				Context::instance().display().SelectActor(it->second, APPEND_ALL, false);

				vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
				translation->Translate(translate[0], translate[1], translate[2]);

				vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
				transformFilter->SetInputData(inputPolyData);
				transformFilter->SetTransform(translation);
				transformFilter->Update();

				vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
				new_points = transformFilter->GetOutput()->GetPoints();

				inputPolyData->SetPoints(new_points);
			}
			Context::instance().display().Refresh();
		}

		void ContourPersonalization::getContourBRChildren(std::string br_name, std::vector<std::string>* body_regions){
			Metadata& m_meta = Context::instance().project().model().metadata();

			ContourCLbr* contourBR = m_meta.ctrl_line.getBodyRegion(br_name);
			if (contourBR->childrenjts.size() > 0){
				body_regions->push_back(contourBR->childrenjts.at(0)->name);
				if (contourBR->childrenjts.at(0)->childbodyregion){
					body_regions->push_back(contourBR->childrenjts.at(0)->childbodyregion->name);
					getContourBRChildren(contourBR->childrenjts.at(0)->childbodyregion->name, body_regions);
				}
			}
		}

		void ContourPersonalization::translateContourBRChildren(std::string br_name){
			std::vector<std::string> contour_names = getBRContours(br_name);
			std::vector<std::string> child_brs;
			getContourBRChildren(br_name, &child_brs);

			//pushing contours of all contourBR into one vector
			std::vector<std::string> all_contours;

			//pushing the last contour of selected contourBR
			std::vector<std::string> contours = getBRContours(br_name);
			all_contours.push_back(contours.at(contours.size() - 1));

			//Pushing contours of contourBR children
			for (auto const& br : child_brs) {
				contours = getBRContours(br);
				for (auto const& contour : contours) {
					all_contours.push_back(contour);
				}
			}

			// get nodes of all contourBR			
			std::vector<NodePtr> br_nodes;
			int i = 0;
			for (auto const& contour : all_contours) {
				if (i > 0 && i <= all_contours.size()){
					getNodesWithinBounds(all_contours.at(i - 1), all_contours.at(i), &br_nodes);
				}
				i++;
			}

			vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
			for (auto const& node : br_nodes) {
				double p[3] = { node->getCoordX(), node->getCoordY(), node->getCoordZ() };
				points->InsertNextPoint(p);
			}
			vtkSmartPointer<vtkPolyData> inputPolyData = vtkSmartPointer<vtkPolyData>::New();
			inputPolyData->SetPoints(points);

			vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
			vtkVector3d &total_translation = contourBRTotalTranslation[br_name];
			translation->Translate(total_translation.GetData());
			vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
			transformFilter->SetInputData(inputPolyData);
			transformFilter->SetTransform(translation);
			transformFilter->Update();

			vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
			new_points = transformFilter->GetOutput()->GetPoints();
			i = 0;
			for (auto const& node : br_nodes) {
				NodePtr p = fem.get<Node>(node->getId());
				double pt[3];
				new_points->GetPoint(i, pt);
				p->setCoord(pt[0], pt[1], pt[2]);
				i++;
			}

			br_nodes.clear();
		}

		double ContourPersonalization::getContourCircumference(std::string contour_name){
			vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
			points = getContourActorPoints(contour_name);
			int NumPts = points->GetNumberOfPoints();
			double total_distance = 0;
			for (int i = 0; i < NumPts; i++)
			{
				double p1[3], p2[3];
				if (i == NumPts - 1){
					points->GetPoint(i, p1);
					points->GetPoint(0, p2);
				}
				else{
					points->GetPoint(i, p1);
					points->GetPoint(i + 1, p2);
				}
				// Find the squared distance between the points.
				double squaredDistance = vtkMath::Distance2BetweenPoints(p1, p2);
				// Take the square root to get the Euclidean distance between the points.
				double distance = sqrt(squaredDistance);
				total_distance += distance;
			}
			// Getting original circumference by decreasing by scaling factor
			total_distance = total_distance / (pskeleton->SCALING_FACTOR * 10);
			return total_distance;
		}

		double ContourPersonalization::getContourBRCircumference(std::string br_name){
			auto br_contours = getBRContours(br_name);
			double circum = 0;
			if (br_contours.size() >= 2){
				circum = getContourCircumference(br_contours.at(std::floor(br_contours.size() / 2)));
			}
			return circum;
		}

		double ContourPersonalization::getContourBRLength(std::string br_name){
			auto contours = getBRContours(br_name);
			double length = 0;
			if (contours.size() >= 2){
				string contour1, contour2;
				contour1 = contours.at(0);
				contour2 = contours.at(contours.size() - 1);
				length = distance2BetweenContourActors(contour1, contour2);
			}
			return length;
		}

		void ContourPersonalization::doLoadTargetsFromFile(std::string const& targetFile)
		{
			Context::instance().project().loadTarget(targetFile);
			loadTargets();
		}

		void ContourPersonalization::getDimensions()
		{
			contourBRDimensions.clear();

			std::queue<ContourCLbr*> local;
			local.push(m_meta.ctrl_line.root);

			while (local.size() != 0)
			{
				ContourCLbr* current_br = local.front();
				local.pop();
				contourBRDimensions[current_br->name] = current_br->dimensions;
				//Push Next skeleton element
				for (int i = 0; i < current_br->childrenjts.size(); i++)
				{
					local.push(current_br->childrenjts.at(i)->childbodyregion);
				}
			}

			// Filling respective maps
			contourBRCircumferenceAnsur.clear();
			contourBRCircumferenceGebod.clear();
			contourBRLengthAnsur.clear();
			contourBRLengthGebod.clear();
			for (auto const& br : contourBRDimensions) {
				for (auto const& dim : br.second) {
					if (dim.first == "CIRCUMFERENCE"){
						for (auto const& src : dim.second) {
							if (src.first == "ANSUR"){
								contourBRCircumferenceAnsur[br.first] = src.second;
							}
							else if (src.first == "GEBOD"){
								contourBRCircumferenceGebod[br.first] = src.second;
							}
						}
					}
					else if (dim.first == "LENGTH"){
						for (auto const& src : dim.second) {
							if (src.first == "ANSUR"){
								contourBRLengthAnsur[br.first] = src.second;
							}
							else if (src.first == "GEBOD"){
								contourBRLengthGebod[br.first] = src.second;
							}
						}
					}
				}
			}
			emit Context::instance().metadataChanged();
		}

		void ContourPersonalization::changeContourBRCircumference(double scale_value, std::string br_name, std::string contour_name){
			std::vector<std::string> contours = getBRContours(br_name);
			if (contours.size() >= 2){
				if (contour_name == ""){
					contour_name = contours.at(std::floor(contours.size() / 2));
				}
				ptrdiff_t pos = std::find(contours.begin(), contours.end(), contour_name) - contours.begin();

				float factor = (scale_value - 1) / pos;
				float max_factor = (scale_value - 1) / (contours.size() - pos - 1);

				int i = 0;
				for (auto const& value : contours) {
					if (i <= pos){
						float scl = 1 + (factor * i);
						scaleContour(scl, QString::fromStdString(value));
					}
					else{
						float scl = 1 + (max_factor * (contours.size() - i - 1));
						scaleContour(scl, QString::fromStdString(value));
					}
					i++;
				}
			}
		}
#pragma endregion
	}
}

