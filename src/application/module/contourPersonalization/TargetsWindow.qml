// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.0
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import Piper 1.0

ModuleToolWindow {
    id: root
    title: "Target Based Personalization"
    minimumHeight: 400
    minimumWidth: 810
    property int nb: 0

    MessageDialog {
        id: messageDialog
        visible: false
        title: "Alert"
        text: " text for message dialog "
        onAccepted: {

        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.centerIn: parent
        ColumnLayout {
            RowLayout {
                Button {
                    id: loadContourCLButton
                    text: "Load ContourCL file"
                    Layout.fillWidth: true
                    onClicked: {
                        openContourCLXMLDialog.open()
                    }
                }
                Button {
                    id: generateFullBodyContours
                    text: qsTr("Generate contours")
                    Layout.fillWidth: true
                    enabled: false
                    onClicked: {
                        myContourPersonalization.generateContBusyIndicator(1)
                        targetImport.enabled = true
                    }
                }
                Button {
                    id: targetImport
                    enabled: false
                    Layout.fillWidth: true
                    action: importTargetAction
                }
                FileDialog {
                    id: openContourCLXMLDialog
                    title: qsTr("Open ContourCL xml File...")
                    nameFilters: ["Contour files (*.xml)"]
                    onAccepted: {
                        console.log(openContourCLXMLDialog.fileUrl.toString())
                        if (myContourPersonalization.loadContourCLXml(
                                        openContourCLXMLDialog.fileUrl)) {                            
                            fullModelOpacity.value = 0.2
                            generateFullBodyContours.enabled = true
                            messageDialog.text = "ContourCL File Loaded."
                            var vals = contextMetaManager.ui_getUndefinedContourCLElements()
                            var complete = true
                            var message = "The following metadata are missing , please create them in the metaeditor for proper functioning :\n\nMETADATA\t\t\t\tTYPE\n"
                            for (var elem in vals) {
                                if (elem == "")
                                    elem = "Missing name" + "\t" + vals[elem][0] + "\n"
                                else
                                    message += elem + "\t" + vals[elem][0] + "\n"
                                complete = false
                            }
                            if (complete == false) {
                                messageDialog.text = message
                                messageDialog.visible = true
                            }
                            if (contextMetaManager.getIsModelChanged(
                                        ) === false) {
                                contextMetaManager.setIsModelChanged(true)
                            }
                        } else {
                            messageDialog.text = "Invalid contourCL xml file!"
                            messageDialog.visible = true
                        }
                    }
                }
            }
        }

        ColumnLayout {
            id: tools
            enabled: false

            ColumnLayout {
                ListModel {
                    id: listModel
                }
                TableView {
                    id: tableView
                    Layout.minimumHeight: 100
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    focus: false
                    headerVisible: true
                    selectionMode: SelectionMode.SingleSelection
                    model: listModel
                    property variant values
                    property variant targets
                    property variant dims
                    property variant names
                    function getList() {
                        listModel.clear()

                        //Lengths
                        nb = 0
                        names = myContourPersonalization.getContourBRLengthTargetList(
                                    true)
                        dims = myContourPersonalization.getContourBRLengthList()
                        targets = myContourPersonalization.getContourBRLengthTargetList()
                        values = myContourPersonalization.getContourBRLengthTargetValueList()
                        for (var i = 0; i < names.length; i++) {
                            listModel.append({
                                                       name: names[i],
                                                       type: "LENGTH",
                                                       dim: dims[i],
                                                       target: targets[i],
                                                       val: values[i]
                                                   })
                            nb++
                        }

                        //Circumferences
                        nb = 0
                        names = myContourPersonalization.getContourBRCircumferenceTargetList(true)
                        dims = myContourPersonalization.getContourBRCircumferenceList()
                        targets = myContourPersonalization.getContourBRCircumferenceTargetList()
                        values = myContourPersonalization.getContourBRCircumferenceTargetValueList()
                        for (var i = 0; i < names.length; i++) {
                            listModel.append({
                                                       name: names[i],
                                                       type: "CIRCUMFERENCE",
                                                       dim: dims[i],
                                                       target: targets[i],
                                                       val: values[i]
                                                   })
                            nb++
                        }
                    }
                    TableViewColumn {
                        role: "name"
                        title: "Body Regions"
                    }
                    TableViewColumn {
                        role: "type"
                        title: "Type"
                    }
                    TableViewColumn {
                        role: "dim"
                        title: "Dimensions"
                    }
                    TableViewColumn {
                        role: "target"
                        title: "Targets"
                    }
                    TableViewColumn {
                        role: "val"
                        title: "Values"
                    }
                    Connections {
                        target: tableView.selection
                        onSelectionChanged: {
                            toolsGroupBox.enabled = true
                        }
                    }
                    Component.onCompleted: {
                        getList()
                    }
                    Connections {
                        target: context
                        onMetadataChanged: {
                            tableView.getList()
                            tools.enabled = true
                        }
                    }
                }
                RowLayout {
                    Layout.fillWidth: true
                    GroupBox {
                        id: toolsGroupBox
                        Layout.fillWidth: true
                        ColumnLayout {
                            RowLayout {
                                Button {
                                    text: "Preview"
                                    Layout.fillWidth: true
                                    onClicked: {
                                        myContourPersonalization.resetContours()
                                        myContourPersonalization.scaleTargetContourBR()
                                        myContourPersonalization.scaleTargetContourBRLength()
                                        personalizeButton.enabled = true
                                    }
                                }
                                Button {
                                    text: "Reset Contours"
                                    Layout.fillWidth: true
                                    onClicked: {
                                        myContourPersonalization.resetContours()
                                        personalizeButton.enabled = false
                                    }
                                }
                                Button {
                                    id: personalizeButton
                                    enabled: false
                                    text: "Personalize"
                                    Layout.fillWidth: true
                                    onClicked: {
                                        myContourPersonalization.personalizeTargetsBusyIndicator()
                                    }
                                }
                            }
                        }
                    }
                }
            }

            RowLayout {
                GroupBox {
                    ColumnLayout {
                        RowLayout {
                            CheckBox {
                                id: checkVisFullModel
                                text: qsTr("Display Contours")
                                checked: true
                                onClicked: {
                                    if (checked == true) {
                                        myContourPersonalization.generateContBusyIndicator(
                                                    1)
                                    } else {
                                        myContourPersonalization.generateContBusyIndicator(
                                                    0)
                                    }
                                }
                            }
                        }
                    }
                }
                GroupBox {
                    ColumnLayout {
                        RowLayout {
                            Label {
                                text: "Model Opacity : "
                            }
                            SpinBox {
                                id: fullModelOpacity
                                decimals: 2
                                minimumValue: 0.00
                                maximumValue: 1.00
                                stepSize: 0.10
                                value: 1.00
                                onValueChanged: {
                                    myContourPersonalization.setOpacity(
                                                fullModelOpacity.value)
                                }
                            }
                        }
                    }
                }
                GroupBox {
                    ColumnLayout {
                        RowLayout {
                            Label {
                                text: "ContourCL Opacity : "
                            }
                            SpinBox {
                                id: clOpacity
                                decimals: 2
                                minimumValue: 0.00
                                maximumValue: 1.00
                                stepSize: 0.10
                                value: 1.00
                                onValueChanged: {
                                    myContourPersonalization.setCLOpacity(
                                                clOpacity.value)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Action {
        id: importTargetAction
        enabled: context.hasModel
        text: qsTr("Load targets from file")
        tooltip: qsTr("Import targets from file")
        onTriggered: importTargetDialog.open()
    }
    FileDialog {
        id: importTargetDialog
        title: qsTr("Import target...")
        nameFilters: ["Target files (*.ptt)"]
        onAccepted: myContourPersonalization.loadTargetsFromFile(
                        importTargetDialog.fileUrl)
    }
}
