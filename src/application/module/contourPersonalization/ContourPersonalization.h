/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURPERSONALIZATION_H
#define PIPER_CONTOURPERSONALIZATION_H

#include "../contourDeformation/ContourGen.h"
#include <vtkVector.h>

namespace piper {
	namespace contours {
		/** @brief 	A class for Contour based Personalization.
		*
		* @author Sachiv Paruchuri @date 2016
		*/
		class ContourPersonalization : public QObject
		{
			Q_OBJECT
		public:
			ContourPersonalization();
			~ContourPersonalization();
			Q_INVOKABLE void displayModel();

			public slots:

			/// <summary>
			/// Parse the contourCL file and load the respective data structures.
			/// </summary>
			/// <param name="modelFile">XML file path</param>
			/// <returns>Returns Boolean indicating the validity of the contourCL file <c>false</c>: Invalid contourCL file, <c>true</c>: Valid contourCL file.</returns>
			bool loadContourCLXml(const QUrl& modelFile);

			/// <summary>
			/// Set opacity of the model.
			/// </summary>
			/// <param name="float">Float indicating the opacity of the model.</param>
			void setOpacity(float value);

			/// <summary>
			/// Set opacity of the contourCL.
			/// </summary>
			/// <param name="float">Float indicating the opacity of the contourCL.</param>
			void setCLOpacity(float value);

			/// <summary>
			/// Busy Indicator of contourDisplay function.
			/// </summary>
			/// <param name="visible">Integer indicating the visibility of contourCL. <0>: Remove contourCL from display, <1>: Create contourCl.</param>
			void generateContBusyIndicator(int visible);

			/// <summary>
			/// Create the contourCL in the display.
			/// </summary>
			/// <param name="visible">Integer indicating the visibility of contourCL. <0>: Remove contourCL from display, <1>: Create contourCl.</param>
			void contourDisplay(int visible);

			/// <summary>
			/// Change the circumference of Contour Actors of indicated Contour Body Region.
			/// </summary>
			/// <param name="scale_value">Float indicatinf the scale to which the circumference needs to be updated.</param>
			/// <param name="name">QString indicating the name of the Contour Body Region.</param>
			void scaleBRContours(float scale_value, QString name);

			/// <summary>
			/// Change the circumference of Contour Body Regions from Target Values.
			/// </summary>
			void scaleTargetContourBR();

			/// <summary>
			/// Reset contour Actors to initial state.
			/// </summary>
			void resetContours();

			/// <summary>
			/// Save Contour Actor data.
			/// </summary>
			/// <param name="contour_name">QString indicating the name of the Contour Actor or Body Region</param>
			/// <param name="is_br_name">Boolean indicating if Contour Actor name of Body Region name. <c>flase</c>: Contour Actor name, <c>true</c>: Contour Body Region name.</param>
			void saveBRContours(QString contour_name, bool is_br_name = false);

			/// <summary>
			/// Busy Indicator for personalize function
			/// </summary>
			/// <param name="name">QString indicating the name of the Contour Actor or Body Region</param>
			/// <param name="is_br_name">Boolean indicating if Contour Actor name of Body Region name. <c>flase</c>: Contour Actor name, <c>true</c>: Contour Body Region name.</param>
			/// <param name="is_translate">Boolean indicating if Translation or Scaling is to be performed. <c>flase</c>: Scaling, <c>true</c>: Translation.</param>
			/// <param name="refresh">Boolean indicating if display has to be refreshed. <c>flase</c>: do not Refresh the display, <c>true</c>: Refresh the display.</param>
			void personalizeBusyIndicator(QString name, bool is_br_name = false, bool is_translate = false, bool refresh = true);

			/// <summary>
			/// Perform custom personalization
			/// </summary>
			/// <param name="name">QString indicating the name of the Contour Actor or Body Region</param>
			/// <param name="is_br_name">Boolean indicating if Contour Actor name of Body Region name. <c>flase</c>: Contour Actor name, <c>true</c>: Contour Body Region name.</param>
			/// <param name="is_translate">Boolean indicating if Translation or Scaling is to be performed. <c>flase</c>: Scaling, <c>true</c>: Translation.</param>
			/// <param name="refresh">Boolean indicating if display has to be refreshed. <c>flase</c>: do not Refresh the display, <c>true</c>: Refresh the display.</param>
			void personalize(QString contour_name, bool is_br_name = false, bool is_translate = false, bool refresh = true);

			/// <summary>
			/// Busy Indicator for personalizeTargets function
			/// </summary>
			void personalizeTargetsBusyIndicator();

			/// <summary>
			/// Perform target based personalization
			/// </summary>
			void personalizeTargets();

			/// <summary>
			/// Get the length of the Contour Body Region
			/// </summary>
			/// <param name="br_name">QString indicating the name of the Contour Body Region</param>
			double contourBRLength(QString br_name);

			/// <summary>
			/// Change the length of the indicated Contour Body Region
			/// </summary>
			/// <param name="scale">Float indicating the scale to which the length needs to be updated.</param>
			/// <param name="br_name">QString indicating the name of the Contour Body Region</param>
			/// <param name="refresh">Boolean indicating if contours needs to be reset. <c>flase</c>: do not reset, <c>true</c>: reset.</param>
			void scaleContourBRLength(float scale, QString br_name, bool reset = true);

			/// <summary>
			/// Change the length of the Contour Body Regions from given target values.
			/// </summary>
			void scaleTargetContourBRLength();

			// TABLES

			/// <summary>
			/// Get Names of Contour Actors
			/// </summary>
			/// <returns>QList of QSting containing Contour Actor Names</returns>
			QList<QString> getContourActorList();

			/// <summary>
			/// Get Names of Contour Body Regions
			/// </summary>
			/// <returns>QList of QSting containing Contour Body Region Names</returns>
			QList<QString> getContourBRList();

			/// <summary>
			/// Get Circumferences of Contour Actors
			/// </summary>
			/// <returns>QList of double containing Contour Actor Circumferences</returns>
			QList<double> getContourCircumferenceList();


			/// <summary>
			/// Get Circumferences of Contour Body Regions
			/// </summary>
			/// <returns>QList of double containing  Contout Body Region Circumferences</returns>
			QList<double> getContourBRCircumferenceList();

			/// <summary>
			/// Get Names of Contour Body Region Circumference Target Names
			/// </summary>
			/// <returns>QList of QSting containing Contour Body Region Circumference Target Names</returns>
			QList<QString> getContourBRCircumferenceTargetList(bool get_name = false);

			/// <summary>
			/// Get Names of Contour Body Region Circumference Target Values
			/// </summary>
			/// <returns>QList of double containing Contour Body Region Circumference Target Values</returns>
			QList<double> getContourBRCircumferenceTargetValueList();


			/// <summary>
			/// Get Names of Contour Body Region Length Values
			/// </summary>
			/// <returns>QList of double containing Contour Body Region Length Values</returns>
			QList<double> getContourBRLengthList();

			/// <summary>
			/// Get Names of Contour Body Region Length Target Names
			/// </summary>
			/// <returns>QList of QSting containing Contour Body Region Length Target Names</returns>
			QList<QString> getContourBRLengthTargetList(bool get_name = false);

			/// <summary>
			/// Get Values of Contour Body Region Length Target Values
			/// </summary>
			/// <returns>QList of double containing Contour Body Region Length Target Values</returns>
			QList<double> getContourBRLengthTargetValueList();


			// TARGETS

			/// <summary>
			/// Load the targets from instance
			/// </summary>
			Q_INVOKABLE void loadTargets();

			/// <summary>
			/// Import a target from file, stores it in project's target list
			/// <param name="targetFile">QUrl of path of file.</param>
			/// </summary>
			Q_INVOKABLE void loadTargetsFromFile(const QUrl& targetFile) { doLoadTargetsFromFile(targetFile.toLocalFile().toStdString()); };

		private:
			/// <summary>
			/// Save the Initial Contour Actors data
			/// </summary>
			void saveInitialContourData();

			/// <summary>
			/// Refresh VTK Display
			/// </summary>
			void refreshDisplay();

			/// <summary>
			/// Get all the nodes withing the indicated Contour Actors			
			/// </summary>
			/// <param name="contour1">Name of the starting Contour Actor.</param>
			/// <param name="contour2">Name of the ending Contour Actor.</param>
			/// <param name="nodes_vector">Vector of NodePtr filled with nodes within the indicated contours.</param>
			void getNodesWithinBounds(std::string contour1, std::string contour2, std::vector<piper::hbm::NodePtr>* nodes_vector);

			/// <summary>
			/// Update Contour Actor circumference by indicated scale value.			
			/// </summary>
			/// <param name="scale_value">Float indicating the scale to which the circumference needs to be updated.</param>
			/// <param name="name">Name of the Contour Actor.</param>
			void scaleContour(float scale_value, QString name);

			/// <summary>
			/// Get Contour Body Region Name of the indicated Contour Actor name.
			/// </summary>
			/// <param name="name">String indicating the name of Contour Actor.</param>
			/// <returns>String of the name of the Contour Body Region.</returns>
			std::string getContourBR(std::string name);

			/// <summary>
			/// Get Contour Actor names of the indicated Contour Body Region name.
			/// </summary>
			/// <param name="name">String indicating the name of Contour Body Region.</param>
			/// <returns>Vector of string indicating the names of the Contour Actors.</returns>
			std::vector<std::string> getBRContours(std::string name);

			/// <summary>
			/// Get Nodes of the indicated Contour Actor name.
			/// </summary>
			/// <param name="name">String indicating the name of Contour Actor.</param>
			/// <returns>Vector of Node indicating the nodes in a Contour Actor.</returns>
			std::vector<piper::hbm::Node> getContourActorNodes(std::string name);

			/// <summary>
			/// Get vtkPoints of the indicated Contour Actor name.
			/// </summary>
			/// <param name="name">String indicating the name of Contour Actor.</param>
			/// <returns>vtkPoints indicating the nodes in a Contour Actor.</returns>
			vtkSmartPointer<vtkPoints> getContourActorPoints(std::string name);

			/// <summary>
			/// Get distance between the Centre of Mass of two Contour Actors.
			/// </summary>
			/// <param name="contour1">String indicating the name of the starting Contour Actor.</param>
			/// <param name="contour2">String indicating the name of the ending Contour Actor.</param>
			/// <returns>double indicating distance between the Centre of Mass of two Contour Actors.</returns>
			double distance2BetweenContourActors(std::string contour1, std::string contour2);

			/// <summary>
			/// Get center of mass Contour Actor.
			/// </summary>
			/// <param name="actor_name">String indicating the name of the Contour Actor.</param>
			/// <param name="center">double[3] indicating the coordinates of the Contour Actor.</param>
			void centerOfMass(std::string actor_name, double center[3]);

			/// <summary>
			/// Translate the ending point on the given line defined by two points at a given distance.
			/// </summary>
			/// <param name="p1">double[3] indicating the coordinates of the starting point of the line.</param>
			/// <param name="p2">double[3] indicating the coordinates of the ending point of the line.</param>
			/// <param name="distance">double indicating the distance from staring point at which the ending point needs to be translated.</param>
			/// <param name="translate">vtkVector3d indicating the coordinates of the translated point.</param>
			void translatePointOnLine(double p1[3], double p2[3], double distance, vtkVector3d &translate);

			/// <summary>
			/// Translate Contour Actor.
			/// </summary>
			/// <param name="translate">double[3] indicating the translation parameters.</param>
			void translateContourActor(std::string name, double translate[3]);

			/// <summary>
			/// Translate the Children of the indicated Contour Body Region.
			/// </summary>
			/// <param name="br_name">string indicating the Contour Body Region.</param>
			void translateContourBRChildren(std::string br_name);

			/// <summary>
			/// Get the names of Children of the indicated Contour Body Region.
			/// </summary>
			/// <param name="br_name">string indicating the name of Contour Body Region.</param>
			/// <param name="br_name">vector of string indicating the Contour Body Region Children Names.</param>
			void getContourBRChildren(std::string br_name, std::vector<std::string>* body_regions);

			/// <summary>
			/// Get the circumference of Contour Actor.
			/// </summary>
			/// <param name="contour_name">string indicating the name of Contour Actor.</param>
			/// <returns>double indicating the Contour Actor circumference.</returns>
			double getContourCircumference(std::string contour_name);

			/// <summary>
			/// Get the circumference of Contour Body Region.
			/// </summary>
			/// <param name="br_name">string indicating the name of Contour Body Region.</param>
			/// <returns>double indicating the Contour Body Region circumference.</returns>
			double getContourBRCircumference(std::string br_name);

			/// <summary>
			/// Get the Length of Contour Body Region.
			/// </summary>
			/// <param name="br_name">string indicating the name of Contour Body Region.</param>
			/// <returns>double indicating the Contour Body Region Length.</returns>
			double getContourBRLength(std::string br_name);

			/// <summary>
			/// Fill the vectors of the ANSUR dimensions.
			/// </summary>
			void getDimensions();

			/// <summary>
			/// Change the circumference of Contour Body Region
			/// </summary>
			/// <param name="scale_value">double indicating the scale to which the circumference needs to be changed.</param>
			/// <param name="br_name">string indicating the Contour Body Region Name.</param>
			/// <param name="contour_name">Contour Actor name.</param>
			void changeContourBRCircumference(double scale_value, std::string br_name, std::string contour_name = "");

			// TARGETS

			/// <summary>
			/// Import a target from file, stores it in project's target list
			/// <param name="targetFile">QUrl of path of file.</param>
			/// </summary>
			void doLoadTargetsFromFile(std::string const& targetFile);

		protected:

			hbm::FEModel const& fem = Context::instance().project().model().fem();
			hbm::FEModel& fem1 = Context::instance().project().model().fem();
			hbm::Metadata& m_meta = Context::instance().project().model().metadata();

			std::map<std::string, std::vector<hbm::Node>> initial_contour_data, final_contour_data;
			std::map<std::string, vtkSmartPointer<vtkPoints>> initial_contour_points;

			MainRepositioning main_repo;

			ContourGen* pskeleton = NULL;

			ProcessingFunc repo;
			DelaunnayFunc dmp;

			QList<QString> contourActorList;
			QList<double> contourCircumferenceList;
			QList<QString> contourBRList;

			std::map<std::string, vtkVector3d> contourBRTotalTranslation;

			std::map<std::string, std::map<std::string, std::map<std::string, std::string>>> contourBRDimensions;
			std::map<std::string, std::string> contourBRCircumferenceAnsur;
			std::map<std::string, double> contourBRCircumferenceAnsurValues;
			std::map<std::string, std::string> contourBRCircumferenceGebod;
			std::map<std::string, std::string> contourBRLengthAnsur;
			std::map<std::string, double> contourBRLengthAnsurValues;
			std::map<std::string, std::string> contourBRLengthGebod;
		};
	}
}
#endif
