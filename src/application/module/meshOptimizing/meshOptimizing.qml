// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.1
import Piper 1.0
import piper.meshOptimizing 1.0

import VtkQuick 1.0

ModuleLayout 
{
    id: root
    anchors.fill: parent
    
    Component 
    {
        id: mesquiteTableContent
        Rectangle 
        {
            Layout.minimumWidth: 400
            Layout.fillHeight: true
            Layout.fillWidth: true
		    anchors.fill: parent
		    id:rect            
            TableView 
            {
                id: tablemesquite
                Layout.minimumWidth: rect.width
                Layout.fillHeight: true
                Layout.fillWidth: true
                anchors.fill: rect
                selectionMode:SelectionMode.NoSelection
                visible: true
                //itemDelegate: editableDelegate
                TableViewColumn 
                {
                    id: partidmes
                    role: "Part Id"
                    title: "Part Id"
                    movable: false
                    resizable: true
                } // TableViewColumn
                TableViewColumn 
                {
                    id: partnamemes
                    role: "Part Name"
                    title: "Part Name"
                    movable: false
                    resizable: true
                } // TableViewColumn
                TableViewColumn 
                {
                    id: invbefore
                    role: "Nb Elements Inverted before Optimization"
                    title: "Nb Elements Inverted before"
                    movable: false
                    resizable: false
                } // TableViewColumn
                TableViewColumn 
                {
                    id: meanrationbefore
                    role: "MeanRatio before Optimization"
                    title: "MeanRatio before"
                    movable: false
                    resizable: false
                } // TableViewColumn
                TableViewColumn 
                {
                    id: invafter
                    role: "Nb Elements Inverted after Optimization"
                    title: "Nb Elements Inverted after"
                    movable: false
                    resizable: false
                    visible: false
                } // TableViewColumn
                TableViewColumn 
                {
                    id: meanrationafter
                    role: "MeanRatio after Optimization"
                    title: "MeanRatio after"
                    movable: false
                    resizable: false
                    visible: false
                } // TableViewColumn
                Component.onCompleted : tablemesquite.model = Qt.binding(function() { return myMeshoptimizing.m_tablemodel })
                Connections
                {
                    target: myMeshoptimizing
                    onOptimStatusChanged:
                    {
                        meanrationafter.visible = myMeshoptimizing.optimStatus
                        invafter.visible = myMeshoptimizing.optimStatus
                    }
                }
            } // TableView Mesquite
        }//Rectangle         
    }
    Component 
    {
        id: surfSmoothTableContent
        Rectangle 
        {
            Layout.minimumWidth: 400
            Layout.fillHeight: true
            Layout.fillWidth: true
		    anchors.fill: parent
		    id:rect            
		    TableView 
            {
			    id: tablesurfsmooth
			    Layout.minimumWidth : rect.width
			    Layout.fillHeight : true
			    Layout.fillWidth : true
			    anchors.fill : rect
			    selectionMode : SelectionMode.NoSelection
			    model : myMeshoptimizing.m_tablemodel
			    visible : true
				    //itemDelegate: editableDelegate
			    TableViewColumn
                {
				    id: partidsurfsmooth
				    role: "Part Id"
				    title: "Part Id"
				    movable: false
				    resizable: true
			    } // TableViewColumn
			    TableViewColumn
                {
				    id: partnamesurfsmooth
				    role : "Part Name"
				    title : "Part Name"
				    movable : false
				    resizable : true
			    } // TableViewColumn
		    } // TableView surfsmooth
        }//Rectangle                 
    }
    Component
    {
        id: diagnosticToolbox
        ColumnLayout
        {

        }
    }
    Component {
        id: viewerContent
        DefaultVtkViewer { }
    }

    Component 
    {
        id: viewerToolbox
        
		DetachableVtkViewerCommonTools
		{
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 10
		}
    }
    content: viewerContent
    toolbar:
    ColumnLayout 
    {
        anchors.fill: parent
        GroupBox 
        {
            id: groupBox1
            Layout.fillWidth: true
            title: qsTr("Mode")
            ColumnLayout 
            {
                id: columnLayout1
                anchors.fill: parent

                ExclusiveGroup 
                { 
                    id: displayModeGroup 
                }
                Button 
                {
                    Layout.fillWidth: true
                    id: buttonDiagnostic
                    anchors.right: parent.right
                    anchors.left: parent.left
                    text: qsTr("Information grid")
                    checkable: true
                    checked: false
                    exclusiveGroup: displayModeGroup
                    onCheckedChanged: 
                    {
                        if (checked) 
                        {
                            myMeshoptimizing.optionVisualizationOn = false
                            if (myMeshoptimizing.metric === qsTr("Mesquite")) {
                                root.content = mesquiteTableContent
                            }
                            else {
                                root.content = surfSmoothTableContent
                            }
                            subModuleControl.sourceComponent = diagnosticToolbox
                            subModuleControl.anchors.bottom = undefined // reset the positioning used for viewerTools
                        }
                    }
                }
                Button 
                {
                    Layout.fillWidth: true
                    id: button3dView
                    anchors.right: parent.right
                    anchors.left: parent.left
                    text: qsTr("3D view")
                    checkable: true
                    checked: true
                    exclusiveGroup: displayModeGroup
                    onCheckedChanged: 
                    {
                        if (checked) 
                        {
                            root.content = viewerContent
                            subModuleControl.sourceComponent = viewerToolbox
                            subModuleControl.anchors.bottom = subModuleControl.parent.bottom
                        }
                    }
                    Connections 
                    {
                        target: context
                        onVisDataLoaded: 
                        {
                            myMeshoptimizing.optionVisualizationOn = true // only switch this on after the VtkDisplay is ready
                        }
                    }
                }
            }
        }
        GroupBox 
        {
            id: groupBoxMethods
            Layout.fillWidth: true
            title: qsTr("Method")
            ColumnLayout 
            {
                anchors.right: parent.right
                anchors.left: parent.left
                ModuleToolWindowButton 
                {
                    text:"\&Mesquite"
                    toolWindow: mesquiteOptionWindow
                    shortcutChar: "M"
                    checkable: true
                    exclusiveGroup: exclusiveButtonGroup
                    onCheckedChanged : {
                        if (checked) {
                            myMeshoptimizing.metric = qsTr("Mesquite")
                            if (myMeshoptimizing.optionVisualizationOn === false) {
                                root.content = mesquiteTableContent
                            }
                        }
                    }
                }
                ModuleToolWindowButton 
                {
                    text:"\&Crease Detection"
                    toolWindow: creaseDetOptionWindow
                    shortcutChar: "C"
                    checkable: true
                    exclusiveGroup: exclusiveButtonGroup
                    onCheckedChanged : {
                        if (checked) {
                            myMeshoptimizing.metric = qsTr("Crease detection")
                            if (myMeshoptimizing.optionVisualizationOn === false) {
                                root.content = surfSmoothTableContent
                            }
                        }
                    }
                }                
                ModuleToolWindowButton 
                {
                    id: surfSmoothButton
                    text:"\&Smooth Surface"
                    toolWindow: surfSmoothOptionWindow
                    shortcutChar: "S"
                    checkable: true
                    exclusiveGroup: exclusiveButtonGroup
                    onCheckedChanged : {
                        if (checked) {
                            myMeshoptimizing.metric = qsTr("Surface smoothing")
                            myMeshoptimizing.optionTaubinFIR = true
                            myMeshoptimizing.optionKrigBox = false
                            myMeshoptimizing.optionMoveAvg = false
                            if (myMeshoptimizing.optionVisualizationOn === false) {
                                root.content = surfSmoothTableContent
                            }
                        }
                    }
                }  
                GroupBox
                {
                    Layout.fillWidth: true
                    title: qsTr("Smooth Transformation")
                    ColumnLayout
                    {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        ModuleToolWindowButton 
                        {
                            text:"\&Kriging in a box"
                            toolWindow: krigingBoxOptionWindow
                            shortcutChar: "K"
                            checkable: true
                            exclusiveGroup: exclusiveButtonGroup
                            onCheckedChanged : {
                                if (checked) {
                                    myMeshoptimizing.metric = qsTr("Surface smoothing")
                                    myMeshoptimizing.optionKrigBox = true
                                    myMeshoptimizing.optionTaubinFIR = false
                                    myMeshoptimizing.optionMoveAvg = false
                                    if (myMeshoptimizing.optionVisualizationOn === false) {
                                        root.content = surfSmoothTableContent
                                    }
                                }
                            }
                        }    
                        ModuleToolWindowButton 
                        {
                            text:"\&Moving average"
                            toolWindow: localAvgOptionWindow
                            shortcutChar: "A"
                            checkable: true
                            exclusiveGroup: exclusiveButtonGroup
                            onCheckedChanged : {
                                if (checked) {
                                    myMeshoptimizing.metric = qsTr("Surface smoothing")
                                    myMeshoptimizing.optionKrigBox = false
                                    myMeshoptimizing.optionTaubinFIR = false
                                    myMeshoptimizing.optionMoveAvg = true
                                    if (myMeshoptimizing.optionVisualizationOn === false) {
                                        root.content = surfSmoothTableContent
                                    }
                                }
                            }
                        }
                    }
                }
                ExclusiveGroup 
                {
                    id: exclusiveButtonGroup
                }
            }
        }
        Loader 
        {
            id: subModuleControl
            Layout.fillWidth: true
            sourceComponent: viewerToolbox
            anchors.bottom: parent.bottom
        }

        Item
        {
            Layout.fillHeight: true
        }
    }
    MesquiteOptionWindow
    {
        id: mesquiteOptionWindow
    }
    CreaseDetectionOptionWindow
    {
        id: creaseDetOptionWindow
    }
    SurfaceSmoothingOptionWindow
    {
        id: surfSmoothOptionWindow
    }
    KrigingInABoxOptionWindow
    {
        id: krigingBoxOptionWindow
    }    
    LocalAvgSmoothWindow
    {
        id: localAvgOptionWindow
    }   

    Item
    {
        MeshOptimizing 
        {
            id: myMeshoptimizing
        }
        Component.onCompleted: {
            mesquiteOptionWindow.myMeshoptimizingMes = myMeshoptimizing
            creaseDetOptionWindow.myMeshoptimizingSurfSmooth = myMeshoptimizing
            surfSmoothOptionWindow.myMeshoptimizingSurfSmooth = myMeshoptimizing
            krigingBoxOptionWindow.myMeshoptimizingSurfSmooth = myMeshoptimizing
            localAvgOptionWindow.myMeshoptimizingSurfSmooth = myMeshoptimizing
        }
    }
}
