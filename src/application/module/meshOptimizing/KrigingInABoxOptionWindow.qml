// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.meshOptimizing 1.0

import VtkQuick 1.0


ModuleToolWindow 
{
    title: "Smooth transformation using kriging in a box"
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin
    property MeshOptimizing myMeshoptimizingSurfSmooth

    ColumnLayout 
    {
        anchors.fill: parent
        anchors.margins : 10
        

        BaseModelSelectGroupBox
        {
            Layout.fillWidth: true
            myMeshoptimizing: myMeshoptimizingSurfSmooth
            onBaseModelChanged:
            {
                adjustWindowSizeToContent();
            }
        }
        GroupBox
        {
            id: chooseEntOpt
            title: "Choose entities to use as targets"
            Layout.fillWidth : true
            Layout.fillHeight : true
            EntityTreeBrowser
            {
                anchors.fill: parent
                anchors.margins : 10
                id: entityBrowser
                defaultEntity : "Skin"
                treeViewTitle : "Select target entities"
            }
        }
        GroupBox
        {
		    id: rectKrigingOpt
            Layout.fillWidth: true
            title: "Kriging in a box options"

            GridLayout
            {
                id: krigingOptions
                Layout.fillWidth: true

                CheckBox
                {
                    Layout.row: 0
                    Layout.columnSpan : 2
                    id: checkboxSkinNuggetByDistance
                    text : qsTr("Set nuggets of skin control points based on distance to bones")                 
                    onCheckedChanged : 
                    {
                        internalSkinNodeNuggetInput.enabled = !checked
                        myMeshoptimizingSurfSmooth.setSmoothOptionBool(qsTr("computeSkinNuggetByDistanceToBone"), checked)
                    }
                }
                Text
                {
                    Layout.row: 1
                    Layout.column : 0
    		        id: internalSkinNodeNugget
    		        text : qsTr("Nugget to assign to skin control points: ")
    		        fontSizeMode : Text.Fit;
                    minimumPixelSize: 9;
    		        horizontalAlignment : Text.AlignLeft
    	        }
                TextField
                {
                    Layout.row: 1
                    Layout.column : 1
    		        id: internalSkinNodeNuggetInput
                    validator : DoubleValidator{ top:0; notation:DoubleValidator.ScientificNotation }
    		        font.pointSize : 9
    		        horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("nuggetForSkin"), parseFloat(text))
                        else
                        {
                            var a = parseFloat(text)
                            if (a == a) // NaN is not equal to itself so this is apparently the only reliable way to test for NaN http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
                                text = -a
                            else text = 0
                        }
                    }
    	        }
                Text
                {
                    Layout.row: 2
                    Layout.column : 0
    		        id: internalBoneNodeNugget
    		        text : qsTr("Nugget to assign to bone control points: ")
    		        fontSizeMode : Text.Fit;
                    minimumPixelSize: 9;
    		        horizontalAlignment : Text.AlignLeft
    	        }
                TextField
                {
                    Layout.row: 2
                    Layout.column : 1
    		        id: internalBoneNodeNuggetInput
                    validator : DoubleValidator{ top:0; notation:DoubleValidator.ScientificNotation }
    		        font.pointSize : 9
    		        horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("nuggetForBones"), parseFloat(text))
                        else
                        {
                            var a = parseFloat(text)
                            if (a == a) // NaN is not equal to itself so this is apparently the only reliable way to test for NaN http://adripofjavascript.com/blog/drips/the-problem-with-testing-for-nan-in-javascript.html
                                text = -a
                            else text = 0
                        }
                    }
    	        }
                CheckBox
                {
                    id: checkboxVisualizeBoxes       
                    Layout.row: 3
                    Layout.columnSpan : 2  
                    checked : true
                    text: "See selection boxes"         
                    onClicked :  contextVtkDisplay.optionVisualizeBoxes = checkboxVisualizeBoxes.checked
                    Component.onCompleted : checked = contextVtkDisplay.optionVisualizeBoxes
                    Connections
                    {
                        target: contextVtkDisplay
                        onOptionVisualizeBoxesChanged : checkboxVisualizeBoxes.checked = contextVtkDisplay.optionVisualizeBoxes
                    }
                }
            }
        } //rectSurfSmoothOpt
		Button
		{
            Layout.fillWidth : true
            id: smoothByKriging
            action : smoothByKrigingAction
        }
    }
    Component.onCompleted: {
        entityBrowser.treeModel.updateEntityList();
        entityBrowser.treeView.selection.selectSingleEntity("Skin");
        myMeshoptimizingSurfSmooth.metric = qsTr("Surface smoothing")
        myMeshoptimizingSurfSmooth.optionTaubinFIR = false
        myMeshoptimizingSurfSmooth.optionKrigBox = true
        myMeshoptimizingSurfSmooth.optionMoveAvg = false
        // set default parameters 
        myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("nuggetForInternalNodes"), -100)
        checkboxSkinNuggetByDistance.checked = true
        myMeshoptimizingSurfSmooth.setSmoothOptionBool(qsTr("computeSkinNuggetByDistanceToBone"), true)
        internalSkinNodeNuggetInput.enabled = false
        internalSkinNodeNuggetInput.text = qsTr("-100")
        myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("nuggetForSkin"), -100)
        internalBoneNodeNuggetInput.text = qsTr("0")
        myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("nuggetForBones"), 0)
        adjustWindowSizeToContent();
        Connections
        {
            target: myMeshoptimizingSurfSmooth
            onOrigModelLoadedChanged : 
            {
                smoothByKrigingAction.enabled = Qt.binding(function(){ return  myMeshoptimizingSurfSmooth.origModelLoaded && context.hasModel })
			}
        }
    }

	Action{
		id: smoothByKrigingAction
		text : qsTr("Smooth")
	    shortcut : StandardKey.Refresh
		onTriggered :
        {
            myMeshoptimizingSurfSmooth.setEntitiesToProcess(entityBrowser.itemSelected)
            myModelValidator.text = context.getValidHistoryName("Transformation smooth by kriging")
            myModelValidator.open()
        }
    }

    ModelHistoryNameDialog{
        id: myModelValidator
        onAccepted : myMeshoptimizingSurfSmooth.smoothSurface(text)
    }
}

