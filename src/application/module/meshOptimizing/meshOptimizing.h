/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "common/Context.h"
#include "meshoptimizer/meshOptimizerInterface.h"

#include <QtCore>
#include <QtWidgets>

namespace piper {
namespace meshoptimizing {


struct TableQualityItem {
    TableQualityItem(int n) {
        m_critafter.resize(n);
        std::fill(m_critafter.begin(), m_critafter.end(), -1);
        m_critbefore.resize(n);
        std::fill(m_critbefore.begin(), m_critbefore.end(), -1);
    }
    TableQualityItem(hbm::Id id, hbm::Id partid, QString partname, std::vector<int> critbefore, int n) :
        m_id(id), m_partid(partid), m_partname(partname), m_critbefore(critbefore) {
        m_critafter.resize(n);
        std::fill(m_critafter.begin(), m_critafter.end(), -1);
    }
    qint32 m_id;
    qint32 m_partid;
    QString m_partname;
    std::vector<qint32> m_critbefore;
    std::vector<qint32> m_critafter;

    void setAfter(std::vector<int> critafter) {
        m_critafter = critafter;
    }
    };

struct TableQualityItemMES : public TableQualityItem  {
    TableQualityItemMES() : TableQualityItem(2) {}
    TableQualityItemMES(hbm::Id id, hbm::Id partid, QString partname, std::vector<int> critbefore) :
        TableQualityItem(id, partid, partname, critbefore, 2) {}

};

struct TableQualityItemSurfSmooth : public TableQualityItem {
	TableQualityItemSurfSmooth() : TableQualityItem(0) {}
	TableQualityItemSurfSmooth(hbm::Id id, hbm::Id partid, QString partname, std::vector<int> critbefore) :
		TableQualityItem(id, partid, partname, critbefore, 0) {}
};

class TableQualityModel : public QAbstractListModel {
    Q_OBJECT
public:
    TableQualityModel(QObject *parent = 0);
    
    //void setIsComputed(bool truefalse);
    //bool isComputed() const;
    virtual void resetThreshold() = 0;
    virtual void construct(hbm::VId& vid, hbm::VId& vidpart, std::vector<std::string>& vname, std::vector<std::vector<int>>& vnum) = 0;
    void setThreshold(QString& metric, const QVariant &value);
    double getThreshold(QString& metric) const;
    virtual std::map<meshoptimizer::QualityMetrics, double>& getThreshold() { return m_threshold; }
    QList<TableQualityItem*> items;
    void clear();

	Qt::ItemFlags flags(const QModelIndex &index) const;
	// returns the size of the table
	int columnCount(const QModelIndex &parent) const;
	int rowCount(const QModelIndex &parent) const;

	virtual bool setData(const QModelIndex &index, const QVariant &value, int role) = 0;
	bool setData(int row, int column, const QVariant value)
	{
		int role = Qt::UserRole + 1 + column;
		return setData(index(row, 0), value, role);
	}

protected:
    std::map<meshoptimizer::QualityMetrics, double> m_threshold;
	int noOfColumns; // the number of columns in the table
    //bool m_iscomputed;

public slots:
virtual void update() {
    this->beginResetModel();
    this->endResetModel();
}
};

class TableQualityModelMES : public TableQualityModel
{
    Q_OBJECT
public:
    TableQualityModelMES(QObject *parent = 0);
    ~TableQualityModelMES() { qDeleteAll(items); }
    //implement abstract method of QAbstractTableModel
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;
	// change the noOfColumns in constructor in case of adding new roles
    enum Roles {
        PartId = Qt::UserRole + 1,
        PartName,
        InvBefore,
        MeanrationBefore,
        InvAfter,
        Meanrationafter
    };
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    void construct(hbm::VId& vid, hbm::VId& vidpart, std::vector<std::string>& vname, std::vector<std::vector<int>>& vnum);
    void resetThreshold();
    //const for default value of threshold
    const double defautMeanRatio = 300;
};

class TableQualityModelSurfSmooth : public TableQualityModel
{
	Q_OBJECT
public:
	TableQualityModelSurfSmooth(QObject *parent = 0);
	~TableQualityModelSurfSmooth() { qDeleteAll(items); }
	//implement abstract method of QAbstractTableModel
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QHash<int, QByteArray> roleNames() const;
	// change the noOfColumns in constructor in case of adding new roles
	enum Roles {
		PartId = Qt::UserRole + 1,
		PartName
	};
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	void construct(hbm::VId& vid, hbm::VId& vidpart, std::vector<std::string>& vname, std::vector<std::vector<int>>& vnum);
	void resetThreshold();
};




class tableQualityManager {
public:
    tableQualityManager();
    ~tableQualityManager();

    TableQualityModel* getTableQuality(meshoptimizer::METRIC_TYPE metric);
    void set(meshoptimizer::METRIC_TYPE metric, TableQualityModel* table);
    void reset();

private:
    TableQualityModel* m_mes;
	TableQualityModel* m_surfsmooth;
};

class OptimizerManager {
public:
    OptimizerManager();
    ~OptimizerManager();

    meshoptimizer::meshOptimizerInterface* getOptimizer(meshoptimizer::METRIC_TYPE metric);
    void reset();

private:
    std::map<meshoptimizer::METRIC_TYPE, meshoptimizer::meshOptimizerInterface*> m_mapoptimizer;
};

static const std::array<QString, 3> METRIC_TYPEName_str = { { "Mesquite", "Crease detection", "Surface smoothing" } };


class MeshOptimizing : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TableQualityModel* m_tablemodel READ getModel NOTIFY modelChanged)
    Q_PROPERTY(bool optionBone READ optionBone WRITE setOptionBone NOTIFY optionBoneChanged)
    Q_PROPERTY(bool optionElementInv READ optionElementInv WRITE setOptionElementInv NOTIFY optionElementInvChanged)
    Q_PROPERTY(bool optionElementMeanRatio READ optionElementMeanRatio WRITE setOptionElementMeanRatio NOTIFY optionElementMeanRatioChanged)
	Q_PROPERTY(bool optionTaubinFIR READ optionTaubinFIR WRITE setOptionTaubinFIR NOTIFY optionTaubinFIRChanged)
	Q_PROPERTY(bool optionKrigBox READ optionKrigBox WRITE setOptionKrigBox NOTIFY optionKrigBoxChanged)
    Q_PROPERTY(bool optionMoveAvg READ optionMoveAvg WRITE setOptionMoveAvg NOTIFY optionMoveAvgChanged)
	Q_PROPERTY(bool optionElementInvertedKriging READ optionElementInvertedKriging WRITE setOptionElementInvertedKriging NOTIFY optionElementInvertedKrigingChanged)
    Q_PROPERTY(bool optionVisualizationOn READ optionVisualizationOn WRITE setOptionVisualizationOn NOTIFY optionVisualizationOnChanged)
    Q_PROPERTY(bool optionGenerateBoxesAndSelect READ optionGenerateBoxesAndSelect WRITE setOptionGenerateBoxesAndSelect NOTIFY optionGenerateBoxesAndSelectChanged)
    Q_PROPERTY(bool optionMergeBoxes READ optionMergeBoxes WRITE setOptionMergeBoxes NOTIFY optionMergeBoxesChanged)
    Q_PROPERTY(QString metric READ metric WRITE setMetric NOTIFY metricChanged)
    Q_PROPERTY(bool qualityStatus READ qualityStatus NOTIFY qualityStatusChanged)
    Q_PROPERTY(bool optimStatus READ optimStatus NOTIFY optimStatusChanged)
	Q_PROPERTY(bool origModelLoaded READ origModelLoaded NOTIFY origModelLoadedChanged)
    Q_PROPERTY(bool optionQualUseBase READ optionQualUseBase WRITE setOptionQualUseBase NOTIFY optionQualUseBaseChanged)
    Q_PROPERTY(QString origModelName READ origModelName NOTIFY origModelNameChanged)

public:
    MeshOptimizing();
    ~MeshOptimizing();
    Q_INVOKABLE double getMaxMetricValue(QString const& metric) const;
    Q_INVOKABLE double getMinMetricValue(QString const& metric) const;
    Q_INVOKABLE double getThreshold(QString metric) const; 
    Q_INVOKABLE void setThreshold(QString metric, const QVariant &value);
    Q_INVOKABLE void setSmoothOptionInt(QString optionName, QVariant value);
    Q_INVOKABLE void setSmoothOptionDouble(QString optionName, QVariant value);
    Q_INVOKABLE void setSmoothOptionBool(QString optionName, QVariant value);

    /// <summary>
    /// his function is used to call doExportQualityFile and display the "busy task" spinner on screen
    /// </summary>
    /// <param name="file">The file we want to export quality values in. </param>
    /// <param name="indexMetricUsed">The index of the metric function used to compute quality</param>
    Q_INVOKABLE void exportQualityFile(const QUrl& file, int indexMetricUsed);

    // compute mesh quality and displays it using the color code chosen by the user
    Q_INVOKABLE void QualityComputeAndVisualisation(QVariantList userInputArray, int indexMetricFunctionUsed, double alphaValue);
    /// <summary>
    /// Selects the elements of either the full model or the entities, based on what is currently visualized, based on the quality array and specified thresholds
    /// </summary>
    /// <param name="userInputArray">A list of threshold ranges. Only elements with quality values within the thresholds will be selected.</param>
    Q_INVOKABLE void SelectByQuality(QVariantList  userInputArray);
    
    Q_INVOKABLE void optionMesqSelectLowQuality();
    
    Q_INVOKABLE void setEntitiesToProcess(QVariantList entityNames);


    void exportQualityfile(std::string const& file) const;

    void setMetric(meshoptimizer::METRIC_TYPE metric);
    void setMetric(QString metric);
    void setOptionBone(const QVariant &value);
    void setOptionElementInv(const QVariant &value);
    void setOptionElementMeanRatio(const QVariant &value);
	void setOptionTaubinFIR(const QVariant &value);
	void setOptionKrigBox(const QVariant &value);
    void setOptionMoveAvg(const QVariant &value);
    void setOptionElementInvertedKriging(const QVariant &value);
    void setOptionGenerateBoxesAndSelect(const QVariant &value);
    void setOptionMergeBoxes(const QVariant &value);
    void setOptionVisualizationOn(const QVariant &value);
    void setOptionQualUseBase(const QVariant &value);
    
    /// <summary>
    /// Sets up the display for visualization of the results of the optimization results.
    /// </summary>
    void setupVisualization();

    void setOperationSignals(bool model, bool qualityStatus, bool optimStatusChanged);

public slots:
    void reset();
    void optimMesh(QString historyName);
	void computeQuality();
    /// <summary>
    /// Loads the version of the mesh before deformation from a project file.
    /// It is expected that the loaded mesh has the same nodes and elements as the current one (only different node coordinates),
    /// otherwise the behaviour is undefined.
    /// </summary>
    /// <param name="modelFile">The path to the model file.</param>
    void loadBaselineMeshFromFile(const QUrl& modelFile);   

    /// <summary>
    /// Loads the version of the mesh before deformation from a history node.
    /// It is expected that the loaded mesh has the same nodes and elements as the current one (only different node coordinates),
    /// otherwise the behaviour is undefined.
    /// </summary>
    /// <param name="historyNodeName">The name of the context Model History "nod" that should be used as a baseline.</param>
    void loadBaselineMeshFromHistory(const QString historyNodeName);
    void smoothSurface(QString historyName);
    void resetQualityAndOptimStatus();

    void exportQuality(const QUrl& file);

    bool optionBone() const;
    bool optionElementInv() const;
    bool optionElementMeanRatio() const;
	bool optionTaubinFIR() const;
	bool optionKrigBox() const;
    bool optionMoveAvg() const;
    bool optionElementInvertedKriging() const;
    bool optionGenerateBoxesAndSelect() const;
    bool optionMergeBoxes() const;
    bool optionVisualizationOn() const;
    QString metric() const;
    bool qualityStatus() const;
    bool optimStatus() const;
	bool origModelLoaded() const;
    bool optionQualUseBase() const;
    QString origModelName() const;
    TableQualityModel* getModel();

signals:
    void modelChanged();
    void metricChanged();
    void optionBoneChanged();
    void optionElementInvChanged();
	void optionElementMeanRatioChanged();
    void optionElementInvertedKrigingChanged();
    void optionGenerateBoxesAndSelectChanged();
    bool optionMergeBoxesChanged();
    void optionVisualizationOnChanged();
    void optionTaubinFIRChanged();
    void optionKrigBoxChanged();
    void optionMoveAvgChanged();
    void qualityStatusChanged();
    void optimStatusChanged();
	void origModelLoadedChanged();
    void optionQualUseBaseChanged();
    void origModelNameChanged();

private:
       
    /// <summary>
    /// Turns off all "upToDate" flags of objects that are being visualized. Should be called whenever the output data changes.
    /// </summary>
    void invalidateVisualization();
    void doSetupVisualization();

    void doOptimMesh(hbm::HumanBodyModel* hbm);
    /// <summary>
    /// Computes quality of the mesh based on a selected metric.
    /// An original model has to be loaded using the loadOriginalMesh.
    /// </summary>
    /// <param name="hbm">The HBM mesh for which to compute the quality.</param>
    void doComputeQuality(hbm::HumanBodyModel* hbm);
    void doSmoothSurface(hbm::HumanBodyModel* hbm);
    
    void doLoadBaselineMeshFromFile(std::string const& modelFile);

    /// <summary>
    /// Checks if a quality array exists, then call writeQualityFile on a FEModelVTK object
    /// </summary>
    /// <param name="file">The file we want to export quality values in. </param>
    /// <param name="indexMetricUsed">The index of the metric function used to compute quality</param>
    void doExportQualityFile(std::string const& filePath, int indexMetricUsed);

    /// <summary>
    /// Checks if all entities, that were specified through setEntitesToProcess are valid for the specified HBM
    /// and clears those that are not valid from the entitiesToProcess list.
    /// </summary>
    /// <param name="hbm">The model that should contain the entities</param>
    /// <return><c>True</c> if at least one valid entity exists, <c>false</c> otherwise.</return>
    bool validateEntitiesToProcess(hbm::HumanBodyModel* hbm);


    TableQualityModel* m_tablemodel;
    tableQualityManager m_tablemanager;
    meshoptimizer::meshOptimizerInterface* myoptimizer;
    OptimizerManager m_optimizerManager;
    piper::hbm::HumanBodyModel hbm_orig; //the model before deformation - selected by the user, loaded using the "Load the model before deformation" button
    bool isOrigModelLoaded;
    bool qualityUseBaseline = false; // true to relative quality - based on compating the difference between current and specified original model

    //options
    enum class QUALITY_OPT {
        ElementInverted,
        ElementMeaRation,
        evaluatedBone,
        TaubinFIR,
        KrigBox,
        MoveAvg,
        ElementInvertedKriging
    };
    std::map<QUALITY_OPT, bool> m_option;
    std::map<std::string, int> m_smoothOptionInt;
    std::map<std::string, double> m_smoothOptionDouble;
    std::map<std::string, bool> m_smoothOptionBool;

    meshoptimizer::METRIC_TYPE m_metric;
    std::list<std::string> entitiesToProcessNames; 
    QString originalModelName = "none"; // contains either an QUrl-format string of a file or a name of a Model History
    bool baselineModelFromHistory = false; // if set to true, will read the baseline model from history instead from the hbm_orig. the history name must be set in the originalModelName

    // visualization options accesible from the GUI
    bool generateBoxesAndSelect = false;
    bool visualizationOn = false;
    bool resetCamera = true; // flag to turn off or on if a certain action should cause resetting of camera
    bool boxesUpToDate = false;
    bool boxSelectionUpToDate = false;
    bool mergeBoxes = false;

    // collection of boxes to be used by methods that use boxes for optimizing
    // meshOptimizing only concatenates collections created by other means, it is not responsible for deleteing the content of this collection
    boost::container::vector<vtkOBBNodePtr> curSelectionBoxes;
    // names of actors of boxes that were added to the display so that we know what to remove when we are removing boxes
    boost::container::vector<std::string> boxesAddedToContextDisplay;
};

}
}

Q_DECLARE_METATYPE(piper::meshoptimizing::TableQualityModel*)
Q_DECLARE_METATYPE(piper::meshoptimizing::TableQualityModelMES*)
Q_DECLARE_METATYPE(piper::meshoptimizing::TableQualityModelSurfSmooth*)
