// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import Piper 1.0
import piper.meshOptimizing 1.0
import QtQml.Models 2.2
import Qt.labs.settings 1.0

GroupBox 
{
    title: "Select baseline model"
    property MeshOptimizing myMeshoptimizing
    signal baseModelChanged();

    Settings {
        id: settings
        category: "baseModelSelection"
        property url recentBaseModelFolder
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : 10

        Button
        {
            Layout.fillWidth : true
            id: loadOriginalMesh
            action : loadOriginalMeshAction
        } // Button
        Label
        {
            text: "OR"
            font.bold: true
            anchors.horizontalCenter : parent.horizontalCenter
        }
        Button
        {
            Layout.fillWidth : true
            id: loadHistoryMesh
            action : loadHistoryMeshAction
        } // Button
        Label
        {
            text: "Currently chosen model: "
        }
        Label
        {
            id: baseModelNameLabel
            text : myMeshoptimizing.origModelName
            font.bold: true
        }
    }

    Connections
    {
        target: myMeshoptimizing
        onOrigModelNameChanged:
        {
            baseModelNameLabel.text = myMeshoptimizing.origModelName
            baseModelChanged()
        }
    }

    Action{
        id: loadOriginalMeshAction
        text : qsTr("Load the baseline model from file")
        onTriggered : openDialog.open()
    }
    FileDialog{
        id: openDialog
        title : qsTr("Load the model before deformation")
        nameFilters : ["Piper model files (*.pmd)"]
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentBaseModelFolder))
                folder = settings.recentBaseModelFolder;
        }
        onAccepted : {
            settings.recentBaseModelFolder = folder;
            myMeshoptimizing.loadBaselineMeshFromFile(openDialog.fileUrl)
        }
    }
    ListModel
    {
        id: historyModel
    }
    Action{
        id: loadHistoryMeshAction
        enabled : !context.isBusy && context.hasModel
        text : qsTr("Choose the baseline model from current Model History")
        onTriggered : 
        {
            historyModel.clear()
            var history = context.getHistoryList()
            for (var i = 0; i < history.length; i++)
                historyModel.append({ "ModelHistory": history[i] })
            loadHistoryDialog.open()
        }
    }
    Dialog
    {
        id: loadHistoryDialog
        title : qsTr("Choose baseline Model History")
        standardButtons: StandardButton.Ok | StandardButton.Cancel
        TableView
        {
            id: tableView
            anchors.right: parent.right
            anchors.left : parent.left
            selectionMode : SelectionMode.SingleSelection
            model: historyModel
            TableViewColumn
            {
                title: "Model History"
            }
            onModelChanged: {
                if (model.count > 0)
                    selection.select(0) 
            }
        }
        onAccepted: {
            tableView.selection.forEach(function(rowIndex) {
                myMeshoptimizing.loadBaselineMeshFromHistory(tableView.model.get(rowIndex).ModelHistory)
            })
        }
    }
}
