// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.1
import Piper 1.0
import piper.meshOptimizing 1.0

import VtkQuick 1.0


ModuleToolWindow 
{
    title: "Surface smoothing using Taubin algorithm"
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin
    property MeshOptimizing myMeshoptimizingSurfSmooth

    ColumnLayout 
    {
        anchors.fill: parent
        anchors.margins : 10
		CheckBox
		{
			Layout.fillWidth: true
			id: optionSmoothSelected
			text : qsTr("Smooth only selected")
			onClicked :myMeshoptimizingSurfSmooth.setSmoothOptionBool(qsTr("smoothSelectedOnly"), checked)			
		}
        GroupBox
        {
		    id: rectSurfSmoothOpt
		    title: "Surface smoothing options"
            Layout.fillWidth : true
            Layout.fillHeight : true

            ColumnLayout
            {
                id: smoothOptions
                anchors.fill : parent
				EntityTreeBrowser
				{
					anchors.fill: parent
					id: entityBrowser
					defaultEntity: "Skin"
					treeViewTitle: "Choose entities to process"
				}
                RowLayout
                {
                    Layout.fillWidth: true
    	            Text
                    {
    		            id: noOfIterations
    		            text : qsTr("Number of iterations: ")
    		            fontSizeMode : Text.Fit;
                        minimumPixelSize: 9;
    		            horizontalAlignment : Text.AlignLeft
    	            }
                    TextField
                    {
    		            id: noOfIterationsInput
                        inputMask : qsTr("999")
    		            font.pointSize : 9
    		            horizontalAlignment : Text.AlignRight
                        onEditingFinished: {
                            myMeshoptimizingSurfSmooth.setSmoothOptionInt(sTr("numberOfIterations"), parseInt(text, 10)) // the 10 specifies it is a decadic number
                        }
    	            }
                }
                LabeledSlider
                {
                    id: passBand
                    sliderMaxValue: 2.0
                    sliderMinValue: 0.0
                    sliderStep: 0.01
                    sliderText: "Pass band value: "
    		        onSliderValueModified : if (myMeshoptimizingSurfSmooth) myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("passBandValue"), sliderValue)
                } 
            }
        } //rectSurfSmoothOpt
        Button
        {
            id: smoothSurface
            Layout.fillWidth : true
            action : smoothMeshAction
            tooltip: qsTr("Apply Taubin smoothing algorithm")
        } // Button
    }
    Component.onCompleted: {
        entityBrowser.treeModel.updateEntityList();
        entityBrowser.treeView.selection.selectSingleEntity("Skin");
        myMeshoptimizingSurfSmooth.metric = qsTr("Surface smoothing")
        myMeshoptimizingSurfSmooth.optionTaubinFIR = true
        myMeshoptimizingSurfSmooth.optionKrigBox = false
        myMeshoptimizingSurfSmooth.optionMoveAvg = false
        // set the default parameters
        optionSmoothSelected.checked = true
        myMeshoptimizingSurfSmooth.setSmoothOptionBool(qsTr("smoothSelectedOnly"), true)
        passBand.sliderValue = 0.1
        myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("passBandValue"), 0.1)
        noOfIterationsInput.text = qsTr("20")
        myMeshoptimizingSurfSmooth.setSmoothOptionInt(qsTr("numberOfIterations"), 20)
        adjustWindowSizeToContent();
    }

	Action{
		id: smoothMeshAction
		text : qsTr("Smooth surface")
	    shortcut : StandardKey.Refresh
        enabled: context.hasModel
        onTriggered : 
        {
            myMeshoptimizingSurfSmooth.setEntitiesToProcess(entityBrowser.itemSelected)
            myModelValidator.text = context.getValidHistoryName("Skin smooth - Taubin FIR")
            myModelValidator.open()
        }
	}

    ModelHistoryNameDialog{
        id: myModelValidator
        onAccepted : myMeshoptimizingSurfSmooth.smoothSurface(text)
    }

}
























