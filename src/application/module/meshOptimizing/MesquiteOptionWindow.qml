// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.meshOptimizing 1.0
import QtQuick.Dialogs 1.2

import piper.Check 1.0
import VtkQuick 1.0

ModuleToolWindow
{
    title: "Mesh optimization using the mesquite library"
    property MeshOptimizing myMeshoptimizingMes
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins : 10
        
        GroupBox
        {
            id: rectMES
            title: "Quality options"
            Layout.fillWidth: true
            ColumnLayout
            {
                anchors.left : parent.left
                anchors.margins : 10
                anchors.right : parent.right
                CheckBox {
                    id: optionbones
                    text: qsTr("Compute Quality for Skeleton")
                    onClicked: myMeshoptimizingMes.optionBone=checked
                    Component.onCompleted: checked = myMeshoptimizingMes.optionBone
                    Connections {
                        target: myMeshoptimizingMes
                        onOptionBoneChanged: optionbones.checked = myMeshoptimizingMes.optionBone
                    }
                }
                CheckBox
                {
                    id: checkboxRelative
                    checked: myMeshoptimizingMes.optionQualUseBase
                    onClicked : myMeshoptimizingMes.optionQualUseBase = checked
                    text: "Use relative quality - difference between this and baseline model"
                }
                BaseModelSelectGroupBox
                {
                    enabled: checkboxRelative.checked
                    Layout.fillWidth: true
                    myMeshoptimizing: myMeshoptimizingMes
                    onBaseModelChanged:
                    {
                        adjustWindowSizeToContent();
                    }
                }
            }
        }//rectMES
        GroupBox
        {
            id: rectOptim
            title: "Optimization options"
            Layout.fillWidth: true
            Layout.fillHeight : true
            Component.onCompleted : rectOptim.enabled = myMeshoptimizingMes.qualityStatus
            Connections{
                target: myMeshoptimizingMes
                onQualityStatusChanged : {
                    rectOptim.enabled = myMeshoptimizingMes.qualityStatus
                }
            }
            ColumnLayout
            {
                anchors.fill: parent
                anchors.margins : 10
                Button {
                    id: applyMeshOptim
                    Layout.fillWidth: true
                    action: applyMeshOptimAction
                } // Button
                GroupBox
                {
                    Layout.fillWidth: true
                    Layout.fillHeight : true
                    title : "Select skin entities"
                    EntityTreeBrowser
                    {
                        anchors.fill: parent
                        anchors.margins : 10
                        id : entityBrowser
                        defaultEntity : "Skin"
                        treeViewTitle : "Select skin entities"
                    }
                }
                LabeledSlider
                {
                    id: meanRatioSlider
                    sliderStep: 0
                    sliderText : "Mean Ratio Threshold: "
                    enabled: myMeshoptimizingMes.qualityStatus
                    onSliderValueModified : 
                    {                     
                        if (myMeshoptimizingMes)
                        {
                            myMeshoptimizingMes.setThreshold(qsTr("meanratio"), sliderValue)
                        }
                    }
                } 
                Button
                {
                    Layout.fillWidth: true
                    action: selectLowQualityAction
                }
                CheckBox 
                {
                    id: optionmeanratio
                    Layout.fillWidth: true
                    text: qsTr("Improve mean ratio")
                    onClicked: 
                    {
                        myMeshoptimizingMes.optionElementMeanRatio = checked
                        if (checked)
                            applyMeshOptim.enabled = true
                        else if (!optioninverted.checked) // if both are turned off, disable optimization - no criteria to optimize
                            applyMeshOptim.enabled = false
                    }
                    Component.onCompleted: checked = myMeshoptimizingMes.optionElementMeanRatio
                    Connections 
                    {
                        target: myMeshoptimizingMes
                        onOptionElementMeanRatioChanged: optionmeanratio.checked = myMeshoptimizingMes.optionElementMeanRatio
                    }
                }
                CheckBox
                {
                    id: optioninverted
                    Layout.fillWidth: true
                    text: qsTr("Remove inverted elements")
                    onClicked: 
                    {
                        myMeshoptimizingMes.optionElementInv = checked
                        if (checked)
                            applyMeshOptim.enabled = true
                        else if (!optionmeanratio.checked) // if both are turned off, disable optimization - no criteria to optimize
                            applyMeshOptim.enabled = false

                    }
                    Component.onCompleted: checked = myMeshoptimizingMes.optionElementInv
                    Connections 
                    {
                        target: myMeshoptimizingMes
                        onOptionElementInvChanged: optioninverted.checked = myMeshoptimizingMes.optionElementInv
                    }
                }
            }
        }
        Button 
        {
            id: computeQuality
		    action : computeQualityAction
            Layout.fillWidth: true
        }
		Button 
		{
			id: applySaveQuality
			action: applySaveQualityAction                    
			Layout.fillWidth: true
			tooltip: qsTr("Warning, not the same library used as in 'Element quality' computation. Results may vary")
			Component.onCompleted: applySaveQualityAction.enabled = myMeshoptimizingMes.qualityStatus
			Connections 
			{
				target: myMeshoptimizing
				onQualityStatusChanged: applySaveQualityAction.enabled = myMeshoptimizingMes.qualityStatus
			}
		}
    }
    Connections 
    {
        target: myMeshoptimizingMes
        onQualityStatusChanged:
        {
            if (myMeshoptimizing.metric===qsTr("Mesquite") && myMeshoptimizingMes.qualityStatus) 
            {
                meanRatioSlider.sliderMaxValue = myMeshoptimizingMes.getMaxMetricValue(qsTr("meanratio"))
                meanRatioSlider.sliderMinValue = myMeshoptimizingMes.getMinMetricValue(qsTr("meanratio"))
                meanRatioSlider.sliderValue = myMeshoptimizingMes.getThreshold(qsTr("meanratio"))
            }
        }
    }
    Component.onCompleted: {
        entityBrowser.treeModel.updateEntityList();
        entityBrowser.treeView.selection.selectSingleEntity("Skin");
        myMeshoptimizingMes.metric = qsTr("Mesquite")
        myMeshoptimizingMes.setThreshold(qsTr("meanratio"), meanRatioSlider.sliderValue)
        widthMargin *= 2
        heightMargin *= 2
        adjustWindowSizeToContent();
    }

    Action 
    {
        id: computeQualityAction
        enabled: !checkboxRelative.checked || myMeshoptimizingMes.origModelLoaded
        text: qsTr("Compute Quality")
        onTriggered: myMeshoptimizingMes.computeQuality()
    }

    Action {
        id: applyMeshOptimAction
        text: qsTr("Apply Mesh Optimization")
        onTriggered :
        {
            myMeshoptimizingMes.setEntitiesToProcess(entityBrowser.itemSelected)
            if (optioninverted.checked && optionmeanratio.checked)
                myModelValidator.text = "optimized inv. ele. and mean ratio"
            else if (optionmeanratio.checked)
                myModelValidator.text = "optimized mean ratio"
            else if (optioninverted.checked)
                myModelValidator.text = "optimized inv. ele."
            myModelValidator.text = context.getValidHistoryName(myModelValidator.text)
            myModelValidator.open()
        }
    }

    ModelHistoryNameDialog{
        id: myModelValidator
        onAccepted : myMeshoptimizingMes.optimMesh(text)
    }

    Action {
        id: applySaveQualityAction
        text: qsTr("Save Quality to file")              
        onTriggered: saveQualityDialog.open()
    }

    FileDialog {
        id: saveQualityDialog
        title: qsTr("Save quality as...")
        selectExisting: false
        nameFilters: ["Quality files (*.csv)"]
        onAccepted: myMeshoptimizingMes.exportQuality(saveQualityDialog.fileUrl)
    }

    Action{
        id: selectLowQualityAction
        text : qsTr("Select elements below threshold")
        enabled : myMeshoptimizingMes.qualityStatus
        onTriggered : myMeshoptimizingMes.optionMesqSelectLowQuality()
    }
}
