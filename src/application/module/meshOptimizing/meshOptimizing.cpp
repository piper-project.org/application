/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

#include <utility>

// for visualization
#include <vtkLookupTable.h>
#include <vtkPointData.h>
#include <vtkColorTransferFunction.h>
#include <vtkCell.h>

#include "meshOptimizing.h"

using namespace piper::hbm;
using namespace piper::meshoptimizer;
using namespace boost::container;

namespace piper {
    namespace meshoptimizing {

#pragma region TableQualityModel

        TableQualityModel::TableQualityModel(QObject *parent): QAbstractListModel(parent) {}

        //void TableQualityModel::setIsComputed(bool truefalse) {
        //    m_iscomputed = truefalse;
        //}

        //bool TableQualityModel::isComputed() const {
        //    return m_iscomputed;
        //}

        void TableQualityModel::clear() {
            qDeleteAll(items);
            items.clear();
            //m_iscomputed = false;
        }
		
		void TableQualityModel::setThreshold(QString& metric, const QVariant &value) {
			std::string metriclower(metric.toStdString());
			std::transform(metriclower.begin(), metriclower.end(), metriclower.begin(), ::tolower);
			if (!metriclower.compare("meanratio")){
				m_threshold[meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio] = value.toDouble();
			}
			else if (!metriclower.compare("scaledjacobian")){
				m_threshold[meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN] = value.toDouble();
			}
			else if (!metriclower.compare("clustersizeperc")){
				m_threshold[meshoptimizer::QualityMetrics::MSHDMGDETECTOR_ClusterSizeBoundaryPercentage] = value.toDouble();
			}
			else if (!metriclower.compare("dianglethreshold")){
				m_threshold[meshoptimizer::QualityMetrics::MSHDMGDETECTOR_DihedralAngleThreshold] = qDegreesToRadians(value.toDouble());
			}
		}

		int TableQualityModel::columnCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return noOfColumns;
		}

		int TableQualityModel::rowCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return items.count();
		}
		
		double TableQualityModel::getThreshold(QString& metric) const {
			std::string metriclower(metric.toStdString());
			std::transform(metriclower.begin(), metriclower.end(), metriclower.begin(), ::tolower);
			if (!metriclower.compare("scaledjacobian")){
				return m_threshold.at(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN);
			}
			if (!metriclower.compare("meanratio")){
				return m_threshold.at(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio);
			}
			if (!metriclower.compare("clustersizeperc")){
				return m_threshold.at(meshoptimizer::QualityMetrics::MSHDMGDETECTOR_ClusterSizeBoundaryPercentage);
			}
			if (!metriclower.compare("dianglethreshold")){
				return qRadiansToDegrees(m_threshold.at(meshoptimizer::QualityMetrics::MSHDMGDETECTOR_DihedralAngleThreshold));
			}
			return 0;
		}


		Qt::ItemFlags TableQualityModel::flags(const QModelIndex &index) const {
			Qt::ItemFlags flags = QAbstractItemModel::flags(index);
			if (index.column() > 0)
				flags |= Qt::ItemIsEditable;
			return flags;
		}

        TableQualityModelMES::TableQualityModelMES(QObject *parent) : TableQualityModel(parent) {
            m_threshold[meshoptimizer::QualityMetrics::MESQUITE_Inverted] = 1;
            m_threshold[meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio] = defautMeanRatio;
			noOfColumns = 6;
        }

        QVariant TableQualityModelMES::data(const QModelIndex &index, int role) const {
            switch (role) {
            case PartId: return items[index.row()]->m_partid;
            case PartName: return items[index.row()]->m_partname;
            case InvBefore: return items[index.row()]->m_critbefore[1];
            case MeanrationBefore: return items[index.row()]->m_critbefore[0];
            case InvAfter: return items[index.row()]->m_critafter[1];
            case Meanrationafter: return items[index.row()]->m_critafter[0];
            }
			return QVariant();
        }

        QHash<int, QByteArray> TableQualityModelMES::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[PartId] = "Part Id";
            roles[PartName] = "Part Name";
            roles[InvBefore] = "Nb Elements Inverted before Optimization";
            roles[MeanrationBefore] = "MeanRatio before Optimization";
            roles[InvAfter] = "Nb Elements Inverted after Optimization";
            roles[Meanrationafter] = "MeanRatio after Optimization";

            return roles;
        }

        bool TableQualityModelMES::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
            case PartId: items[index.row()]->m_partid = value.toInt(); break;
            case PartName: items[index.row()]->m_partname = value.toString(); break;
            case InvBefore: items[index.row()]->m_critbefore[1] = value.toInt(); break;
            case MeanrationBefore: items[index.row()]->m_critbefore[0] = value.toDouble(); break;
            case InvAfter: items[index.row()]->m_critafter[1] = value.toInt(); break;
            case Meanrationafter: items[index.row()]->m_critafter[0] = value.toDouble(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void TableQualityModelMES::construct(hbm::VId& vid, hbm::VId& vidpart, std::vector<std::string>& vname, std::vector<std::vector<int>>& vnum) {
            clear();
            for (size_t n = 0; n < vid.size(); n++) {
                items.append(new TableQualityItemMES(vid[n], vidpart[n], QString::fromStdString(vname[n]), vnum[n]));
            }
        }

        void TableQualityModelMES::resetThreshold() {
            m_threshold.clear();
            m_threshold[meshoptimizer::QualityMetrics::MESQUITE_Inverted] = 1;
            m_threshold[meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio] = defautMeanRatio;
        }

		TableQualityModelSurfSmooth::TableQualityModelSurfSmooth(QObject *parent) : TableQualityModel(parent) {
			noOfColumns = 2;
		}

		QVariant TableQualityModelSurfSmooth::data(const QModelIndex &index, int role) const {
			switch (role) {
				case PartId: return items[index.row()]->m_partid;
				case PartName: return items[index.row()]->m_partname;
			}
			return QVariant();
		}

		QHash<int, QByteArray> TableQualityModelSurfSmooth::roleNames() const {
			QHash<int, QByteArray> roles;
			roles[PartId] = "Part Id";
			roles[PartName] = "Part Name";

			return roles;
		}

		bool TableQualityModelSurfSmooth::setData(const QModelIndex &index, const QVariant &value, int role) {
			switch (role) {
			case PartId: items[index.row()]->m_partid = value.toInt(); break;
			case PartName: items[index.row()]->m_partname = value.toString(); break;
			}
			emit dataChanged(index, index);
			return true;
		}

		void TableQualityModelSurfSmooth::construct(hbm::VId& vid, hbm::VId& vidpart, std::vector<std::string>& vname, std::vector<std::vector<int>>& vnum) {
			clear();
			for (size_t n = 0; n < vid.size(); n++) {
				items.append(new TableQualityItemSurfSmooth(vid[n], vidpart[n], QString::fromStdString(vname[n]), vnum[n]));
			}
		}

		void TableQualityModelSurfSmooth::resetThreshold() {
			m_threshold.clear();
			m_threshold[meshoptimizer::QualityMetrics::MSHDMGDETECTOR_ClusterSizeBoundaryPercentage] = 0.6;
			m_threshold[meshoptimizer::QualityMetrics::MSHDMGDETECTOR_DihedralAngleThreshold] = 0.09;
		}

#pragma endregion
        
		MeshOptimizing::MeshOptimizing() : m_tablemodel(nullptr), myoptimizer(nullptr), m_metric(METRIC_TYPE::MESQUITE), hbm_orig()
        {
            m_option[QUALITY_OPT::ElementInverted] = true;
            m_option[QUALITY_OPT::ElementMeaRation] = true;
            m_option[QUALITY_OPT::evaluatedBone] = true;
            reset();
            // when model changes, reset the whole optimizer
            connect(&Context::instance(), &Context::modelChanged, this, &MeshOptimizing::reset);
            // when model is updated, only invalidate quality of all metrics
            connect(&Context::instance(), &Context::modelUpdated, this, &MeshOptimizing::resetQualityAndOptimStatus);

            isOrigModelLoaded = false;
        }

        MeshOptimizing::~MeshOptimizing() 
        {
            curSelectionBoxes.clear();
        }


        void MeshOptimizing::setMetric(meshoptimizer::METRIC_TYPE metric) {
            m_metric = metric;
            myoptimizer = m_optimizerManager.getOptimizer(m_metric);
            m_tablemodel = m_tablemanager.getTableQuality(m_metric);
            connect(this, &MeshOptimizing::modelChanged, m_tablemodel, &TableQualityModel::update);
            resetCamera = true;
            invalidateVisualization();
            emit metricChanged();
            emit qualityStatusChanged();
            emit modelChanged();
            emit optimStatusChanged();
        }
        
        
        void MeshOptimizing::setMetric(QString metric){
            std::string metriclower(metric.toStdString());
            std::transform(metriclower.begin(), metriclower.end(), metriclower.begin(), ::tolower);
            METRIC_TYPE newmetric;
            if (!metriclower.compare("mesquite")){
                newmetric = METRIC_TYPE::MESQUITE;
            }
			else if (!metriclower.compare("surface smoothing")){
				newmetric = METRIC_TYPE::SURFSMOOTH;
			}
            else if (!metriclower.compare("crease detection")){
                newmetric = METRIC_TYPE::CREASEDET;
            }
			else {
				pCritical() << "Unknown optimization type" ;
				return;
			}
            if (m_metric != newmetric) {
                setMetric(newmetric);  
            }
        }

        void MeshOptimizing::setOperationSignals(bool model, bool qualityStatus, bool optimStatusChanged)
        {
            disconnect(&Context::instance(), &Context::operationFinished, 0 ,0);
            if (model)
                connect(&Context::instance(), &Context::operationFinished, this, &MeshOptimizing::modelChanged);
            if (qualityStatus)
                connect(&Context::instance(), &Context::operationFinished, this, &MeshOptimizing::qualityStatusChanged);
            if (optimStatusChanged)
                connect(&Context::instance(), &Context::operationFinished, this, &MeshOptimizing::optimStatusChanged);
        }
        
        TableQualityModel* MeshOptimizing::getModel()  {
            return m_tablemodel;
        }

        void MeshOptimizing::optimMesh(QString historyName) {
            if (validateEntitiesToProcess(&Context::instance().project().model()))
            {
                setOperationSignals(true, true, true);
                // create a new history node
                Context::instance().addNewHistory(Context::instance().getValidHistoryName(historyName));
                // TODO: check if doOptimMesh can throw an exception, if yes use Context::wrapClassMethodCall
                Context::instance().performAsynchronousOperation(std::bind(&MeshOptimizing::doOptimMesh, this, &Context::instance().project().model()),
                    false, true, false, false, false);
            }
            else
                pInfo() << INFO << "Skin entities not chosen, mesh not optimized.";

       }

        void MeshOptimizing::doOptimMesh(hbm::HumanBodyModel* hbm){
            pInfo() << START << QStringLiteral("Apply mesh optimization...");
            // determine node id of the skin
            hbm::VId vidSkin;
            for (auto it = entitiesToProcessNames.begin(); it != entitiesToProcessNames.end(); it++)
            {
                hbm::VId temp = hbm->metadata().entity(*it).getEntityNodesIds(hbm->fem());
                vidSkin.insert(vidSkin.begin(), temp.begin(), temp.end());
            }
            hbm::IdKey curid;
            std::string curkeyword;
            if (!myoptimizer->isOptimized()) {
                if (false) // todo - GUI interface for choosing between selected only or all
                {
                    myoptimizer->optimizeMeshQuality_selectedOnly(hbm->metadata().entities(), false);
                }
                else
                {
                    VId vid;
                    for (QList<TableQualityItem*>::iterator it = m_tablemodel->items.begin(); it != m_tablemodel->items.end(); ++it) {
                        vid.push_back((*it)->m_id);
                        // check option
                        std::vector<meshoptimizer::QualityMetrics> vmetric;
                        bool spy = false;
                        if ((m_option[QUALITY_OPT::ElementInverted]) && ((*it)->m_critbefore[1] > 0)) {
                            spy = true;
                            vmetric.push_back(meshoptimizer::QualityMetrics::MESQUITE_Inverted);
                        }
                        if ((m_option[QUALITY_OPT::ElementMeaRation]) && ((*it)->m_critbefore[0] > 0)) {
                            spy = true;
                            vmetric.push_back(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio);
                        }
                        if (spy) {
                            hbm->fem().getInfoId<hbm::GroupElements3D>((*it)->m_id, curkeyword, curid);
                            pInfo() << INFO << " Apply mesh optimization to " << QString::fromStdString(hbm->fem().get<hbm::GroupElements3D>((*it)->m_id)->getName()) <<
                                " - " << curid.id;
                            myoptimizer->optimizeMeshQuality((*it)->m_id, vidSkin, vmetric);
                        }
                    }
                    hbm::VId vidafter;
                    std::vector<std::vector<int>> vnumafter;
                    hbm::VId vidignore;
                    myoptimizer->computeMeshQuality();
                    myoptimizer->identifyLowQualityGroup(m_tablemodel->getThreshold(), vidafter, vnumafter, vidignore);

                    hbm::VId::const_iterator itv;
                    for (QList<TableQualityItem*>::iterator it = m_tablemodel->items.begin(); it != m_tablemodel->items.end(); ++it) {
                        itv = std::find(vidafter.begin(), vidafter.end(), (*it)->m_id);
                        if (itv != vidafter.end()) {
                            size_t nn = itv - vidafter.begin();
                            (*it)->setAfter(vnumafter[nn]);
                        }
                        else {
                            std::vector<int> tmpafter(2, 0);
                            (*it)->setAfter(tmpafter);
                        }
                    }
                }
            }
            pInfo() << DONE;
        }

        bool MeshOptimizing::validateEntitiesToProcess(hbm::HumanBodyModel* hbm)
        {
            for (auto it = entitiesToProcessNames.begin(); it != entitiesToProcessNames.end();)
            {
                if (!Context::instance().project().model().metadata().hasEntity(*it))
                    it = entitiesToProcessNames.erase(it);
                else
                    it++; // do not advance the iterator if we were erasing
            }
            return entitiesToProcessNames.size() > 0;
        }

        void MeshOptimizing::computeQuality() {
            if (Context::instance().hasModel())
            {
                if (myoptimizer->getMetric() == METRIC_TYPE::CREASEDET) // for metrics for which entityToProcess is relevant, check if it is set correctly
                {
                    if (!validateEntitiesToProcess(&Context::instance().project().model()))
                    {
                        pInfo() << INFO << "No valid entities selected, quality not computed.";
                        return;
                    }
                }
                // clear the table and update view
                m_tablemodel->clear();
                modelChanged(); // TODO not sure about this signal, should it be sent *before* doComputeQuality() ?
                setOperationSignals(true, true, true); // TODO check signals to emit

                // TODO: check if doComputeQuality can throw an exception, if yes use Context::wrapClassMethodCall
                Context::instance().performAsynchronousOperation(std::bind(&MeshOptimizing::doComputeQuality, this, &Context::instance().project().model()),
                    false, false, false, false, false);
            }
            else
                pInfo() << INFO << "No model loaded, quality not computed.";
        }

		void MeshOptimizing::doComputeQuality(hbm::HumanBodyModel* hbm) {
            pInfo() << START << "Compute quality..." ;
            if (!myoptimizer->isQualityComputed()) {
                myoptimizer->initialize(hbm->fem());
                myoptimizer->setQualityUseBaselineModel(qualityUseBaseline);
                myoptimizer->setEntitiesToProcess(entitiesToProcessNames);

                if (baselineModelFromHistory)
                {
                    if (hbm->history().isHistory(originalModelName.toStdString()))
                    {
                        std::string currHistory = hbm->history().getCurrent();
                        hbm->history().setCurrentModel(originalModelName.toStdString());
                        myoptimizer->processBaselineMesh(hbm->fem());
                        hbm->history().setCurrentModel(currHistory);
                    }
                    else
                        myoptimizer->processBaselineMesh(hbm->fem()); // if the model history does not exist, just use current - a reasonable default for methods that don't care about the baseline mesh
                }
                else
                    myoptimizer->processBaselineMesh(hbm_orig.fem());

                myoptimizer->updateThresholds(m_tablemodel->getThreshold());
                myoptimizer->computeMeshQuality();
                if (generateBoxesAndSelect)
                {
                    for (int i = 0; i < boxesAddedToContextDisplay.size(); i++)
                        Context::instance().display().RemoveActor(boxesAddedToContextDisplay[i]);
                    boxesAddedToContextDisplay.clear();
                    curSelectionBoxes.clear();
                    curSelectionBoxes = myoptimizer->generateSelectionBoxes(mergeBoxes);
                }
                invalidateVisualization();
                emit qualityStatusChanged();
            }
            if (myoptimizer->isOptimized())
                myoptimizer->setOptimized(false);
            hbm::FEModel const& fem = hbm->fem();

            hbm::VId vid_ignore;
            if (!m_option[QUALITY_OPT::evaluatedBone]) {
                // ignore bones entities: define ids of elements of bone entities to be ignored when part with poor quality are identified
                vid_ignore = hbm->findBoneElements();
            }

            std::vector<std::vector<int>> vnum;
            hbm::VId vid;
            myoptimizer->identifyLowQualityGroup(m_tablemodel->getThreshold(), vid, vnum, vid_ignore);

            hbm::IdKey curid;
            std::string curkeyword;
            hbm::VId vpartid;
            std::vector<std::string> vpartname;
            std::vector<std::string> vpartkeywords;
            if (vid.size() > 0) {
                pInfo() << INFO << "Identify parts according to threshold value...";
                for (hbm::VId::const_iterator it = vid.begin(); it != vid.end(); ++it) {
                    fem.getInfoId<hbm::GroupElements3D>(*it, curkeyword, curid);
                    vpartid.push_back(curid.id);
                    vpartname.push_back(fem.get<hbm::GroupElements3D>(*it)->getName());
                    vpartkeywords.push_back(curkeyword);
                }
            }
            m_tablemodel->construct(vid, vpartid, vpartname, vnum);
            pInfo() << DONE;
            if (visualizationOn)
                doSetupVisualization();
        }
		
        void MeshOptimizing::loadBaselineMeshFromHistory(const QString historyNodeName) {
            if (Context::instance().hasModel())
            {
                if (Context::instance().project().model().history().isHistory(historyNodeName.toStdString()))
                {
                    originalModelName = historyNodeName;
                    emit origModelNameChanged();
                    baselineModelFromHistory = true;
                    isOrigModelLoaded = true;
                    myoptimizer->setIsQualityComputed(false);
                    invalidateVisualization();
                    emit origModelLoadedChanged();
                }
                else
                    pInfo() << WARNING << "The specified history name does not exist.";
            }
            else
                pInfo() << WARNING << "No current model loaded, cannot load history model."; // should never happen because the GUI that invokes this method is disabled if no model is loaded
        }
        
        void MeshOptimizing::loadBaselineMeshFromFile(const QUrl& modelFile) {
            void(*loadBaseline)(MeshOptimizing*, void (MeshOptimizing::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall<MeshOptimizing, void (MeshOptimizing::*)(std::string const&), std::string const&>;

            Context::instance().performAsynchronousOperation(std::bind(loadBaseline, this, &MeshOptimizing::doLoadBaselineMeshFromFile, modelFile.toLocalFile().toStdString()),
                false, false, false, false, false);
		}

        void MeshOptimizing::doLoadBaselineMeshFromFile(std::string const& modelFile) {
            pInfo() << START << "Loading mesh in the initial position...";

            Project::validateXml(modelFile, "hbm.dtd");
			hbm_orig.read(modelFile);
			isOrigModelLoaded = true;
            baselineModelFromHistory = false;
            myoptimizer->setIsQualityComputed(false);
            invalidateVisualization();
            originalModelName = QString::fromStdString(modelFile);
            emit origModelNameChanged();
			emit origModelLoadedChanged();
            pInfo() << DONE;
		}

		void MeshOptimizing::smoothSurface(QString historyName)
        {
            if (Context::instance().hasModel())
            {
                if (validateEntitiesToProcess(&Context::instance().project().model()))
                {
                    // create a new history node
                    Context::instance().addNewHistory(Context::instance().getValidHistoryName(historyName));

                    setOperationSignals(false, true, false);
                    Context::instance().performAsynchronousOperation(std::bind(&MeshOptimizing::doSmoothSurface, this, &Context::instance().project().model()),
                        false, true, false, false, false);
                }
                else
                    pInfo() << INFO << "No valid entities to smooth were chosen, no smoothing performed.";
            }
            else
                pInfo() << INFO << "No model loaded, no smoothing performed.";
		}
		
		/// <summary>
		/// Smooth the surface of selected entities of the mesh.
		/// </summary>
		/// <param name="hbm">The hbm mesh.</param>
		void MeshOptimizing::doSmoothSurface(hbm::HumanBodyModel* hbm) {
            pInfo() << START << "Apply mesh surface smoothing...";
            myoptimizer->initialize(hbm->fem());
            myoptimizer->setEntitiesToProcess(entitiesToProcessNames);
            
            bool success = false;
            if (optionTaubinFIR()) {
                pInfo() << INFO << "Applying mesh surface smoothing by Taubin FIR filter.";
                success = myoptimizer->smoothMeshSurface(SURFSMOOTH_ALG::TAUBIN_FIR, hbm, m_smoothOptionInt, m_smoothOptionDouble, m_smoothOptionBool, curSelectionBoxes);
			}
			else // all transformation smoothing methods
            {
                m_smoothOptionBool[SMOOTH_OPT_KRIG_INTERP_DISPLACEMENT] = 
                    Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT);
                m_smoothOptionBool[SMOOTH_OPT_KRIG_USE_DRIFT] =
                    Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT);
                // load the reference points either from hbm_orig (loaded from file) or from history of current model
                vtkSmartPointer<vtkPoints> referencePoints;
                if (baselineModelFromHistory)
                {
                    if (hbm->history().isHistory(originalModelName.toStdString()))
                    {
                        referencePoints = vtkSmartPointer<vtkPoints>::New();
                        std::string currHistory = hbm->history().getCurrent();
                        hbm->history().setCurrentModel(originalModelName.toStdString());
                        referencePoints->DeepCopy(hbm->fem().getFEModelVTK()->getVTKMesh()->GetPoints());
                        hbm->history().setCurrentModel(currHistory);
                        success = true;
                    }
                    else
                        pInfo() << WARNING << "The specified history name no longer exists, smoothing not performed.";
                }
                else
                {
                    referencePoints = hbm_orig.fem().getFEModelVTK()->getVTKMesh()->GetPoints();
                    success = true;
                }

                if (success)
                {
                    if (optionMoveAvg())
                    {
                        pInfo() << INFO << "Applying mesh smoothing by Moving Average.";

                        success = myoptimizer->smoothMeshSurface(SURFSMOOTH_ALG::LOCAL_AVG, hbm,
                            m_smoothOptionInt, m_smoothOptionDouble, m_smoothOptionBool, curSelectionBoxes, referencePoints);
                    }
                    else // all by-box methods
                    {
                        m_smoothOptionInt[SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP] = Context::instance().project().moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP);
                        m_smoothOptionDouble[SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP] = Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_OVERLAP);
                        pInfo() << INFO << "Applying mesh smoothing by Kriging-in-a-box.";
                        // concatenate the boxes created by the optmizer and those created in the context display by picking
                        vector<vtkOBBNodePtr> concat;
                        map<int, vtkOBBNodePtr> *displayBoxes = Context::instance().display().highlights.GetSelectionBoxes();
                        for (auto it = displayBoxes->begin(); it != displayBoxes->end(); it++)
                            concat.push_back(it->second);
                        // this variable controls if boxes are in the visualization. if it is true, contextVtkDisplay has a copy of all the boxes from curSelectionBoxes,
                        // plus it has all the boxes create by picking, so we use only those from vtk display. if it is not up to date, we concatenate both lists
                        if (!boxesUpToDate)
                            concat.insert(concat.end(), curSelectionBoxes.begin(), curSelectionBoxes.end());

                        
                        if (optionKrigBox())
                            success = myoptimizer->smoothMeshSurface(SURFSMOOTH_ALG::KRIG_BOX, hbm,
                                m_smoothOptionInt, m_smoothOptionDouble, m_smoothOptionBool, concat, referencePoints);
                        else if (optionElementInvertedKriging())
                            success = myoptimizer->smoothMeshSurface(SURFSMOOTH_ALG::ELE_INV_KRIG, hbm,
                                m_smoothOptionInt, m_smoothOptionDouble, m_smoothOptionBool, curSelectionBoxes, referencePoints);
                    }
                }
			}
            if (!success)
                pInfo() << WARNING << "Smoothing not processed, check if all required parameters are set";
            pInfo() << DONE;
		}

        void MeshOptimizing::setupVisualization() {
            // some modes of visualization need special computations -> it can be time consuming -> use operationWatcher
            if (visualizationOn && Context::instance().hasModel())
            {
                Context::instance().performAsynchronousOperation(std::bind(&MeshOptimizing::doSetupVisualization, this),
                    false, false, false, false, false);
            }
        }

        void MeshOptimizing::doSetupVisualization() {

            pInfo() << START << "Setting-up visualization";
            hbm::HumanBodyModel* hbm = &Context::instance().project().model();
            // get the context model full mesh name
            std::string fullModelName = Context::instance().objectName().toStdString();
            if (fullModelName == "")
                fullModelName = "full_model";

            Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::FULL_MESH);
            Context::instance().display().DeactivateDisplayMode(DISPLAY_MODE::ENTITIES);
            switch (myoptimizer->getMetric())
            {
                case METRIC_TYPE::SURFSMOOTH:
                {
                    break;
                }
                case METRIC_TYPE::CREASEDET:
                {
                    if (!myoptimizer->isQualityComputed()) return;

                    Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS);
                    vtkSmartPointer<vtkUnstructuredGrid> contextModel = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh();
                    meshDamageDetector *mdd = (meshDamageDetector *)myoptimizer->getMeshQualityComputer();
                    vtkSmartPointer<vtkPolyData> polyModel = mdd->GetDeformedModelPolys();

                    // debug visualization - visualize each cluster with different color instead of damaged elements
#if 0
                    vector<int> damagedIDs = mdd->GetDamagedClustersIDs();
                    vector<vector<int>> clusters = mdd->GetClusters();
                    vtkSmartPointer<vtkIntArray> colors = vtkSmartPointer<vtkIntArray>::New();
                    colors->SetName(ELEM_COLOR_INDEX_ARRAY.c_str());
                    colors->SetNumberOfValues(contextModel->GetNumberOfCells());

                    vtkSmartPointer<vtkFloatArray> colorValues = vtkSmartPointer<vtkFloatArray>::New();
                    colorValues->SetNumberOfComponents(4);
                    colorValues->SetNumberOfTuples(clusters.size());
                    // coloring based on cluster ID
                    for (int i = 0; i < clusters.size(); i++)
                    {
                        float colorIntensity = 0.5 + 0.5*((float)i / clusters.size()); // from half-dark to brightest depending on ID
                        // switch color channels every 3 IDs to get more contrast
                        if (i % 8 == 0)
                            colorValues->SetTuple4(i, 0.2, 0.5, 0.8, 1);
                        else
                            colorValues->SetTuple4(i, (i % 2 == 1)*colorIntensity, // first channel is active for every odd id - the first bit is active
                            (((i % 8) & 2) == 2)*colorIntensity, // second channel for 2,3,6,7 - the middle bit is active
                            (i % 8 >= 4)*colorIntensity, 1); // third channel for the "second half" - the top bit is active

                        vector<int> currCluster = clusters[i];
                        for (int j = 0; j < currCluster.size(); j++)
                            colors->SetValue(currCluster[j], i); // cluster ID as ID of color
                    }
                    lut = st->CreateColorLUT(colorValues, colors);
                    polyModel->GetCellData()->SetScalars(colors);
#endif

                    if (generateBoxesAndSelect && !boxSelectionUpToDate) // if visualization is not up to date and the user wants to see the box selection, compute them
                    {
                        vtkSmartPointer<piper::hbm::vtkSelectionTools> st = vtkSmartPointer<vtkSelectionTools>::New();
                        vtkSmartPointer<vtkBitArray> sel_nodes = st->ObtainSelectionArrayPoints(contextModel, true);

                        st->AddInputData(contextModel); // set the data on which to perform the selection
                        st->SetSelectionTargetType(SELECTION_TARGET::NODES);
                        for (auto it = curSelectionBoxes.begin(); it != curSelectionBoxes.end(); it++)
                        {
                            // select all vertices inside the bounding boxes
                            st->UseSelectByOrientedBox((*it)->Corner, (*it)->Axes); // perform selection by OBB
                            st->Update();

                            // add the boxes to the display
                            vtkSmartPointer<VtkDisplayActor> boxactor = Context::instance().display().highlights.SelectionBox((*it)->Corner, (*it)->Axes);
                            Context::instance().display().AddActor(boxactor, boxactor->GetName(), false, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);
                            boxesAddedToContextDisplay.push_back(boxactor->GetName());
                        }
                        contextModel->GetPointData()->Modified();
                        // deselect bone nodes
                        hbm::VId vid_ignoreNodes = hbm->findBoneNodes();
                        for (auto it = vid_ignoreNodes.begin(); it != vid_ignoreNodes.end(); it++)
                            sel_nodes->SetValue(*it, IS_NOT_PRIMITIVE_SELECTED); // remove bones from selection
                            
                        boxSelectionUpToDate = true;
                        boxesUpToDate = true;
                    }
                    Context::instance().display().UpdateActor(fullModelName);

                    Context::instance().display().HighlightSelectedPoints(fullModelName, 0.5);
                }

                    break;
                case METRIC_TYPE::MESQUITE:
                {
                    vtkUnstructuredGrid *contextModel = hbm->fem().getFEModelVTK()->getVTKMesh();
                    if (!Context::instance().display().HasActor(fullModelName))
                        Context::instance().display().AddVtkPointSet(contextModel, fullModelName, false, DISPLAY_MODE::FULL_MESH, true, resetCamera);
                    Context::instance().display().SetActorVisible(fullModelName, true);

                    // TODO - non-binary coloring based on the quality values
                }
                    break;
            }

            Context::instance().display().Refresh(resetCamera);
            resetCamera = false;
            pInfo() << DONE;
        }

        void MeshOptimizing::invalidateVisualization()
        {
            boxesUpToDate = false;
            boxSelectionUpToDate = false;
        }

        void MeshOptimizing::reset() {
            //setOptionBone(true);
            setOptionElementInv(true);
            setOptionElementMeanRatio(true);
            m_tablemanager.reset();
            m_optimizerManager.reset();
            setMetric(m_metric);
            originalModelName = "none";
            isOrigModelLoaded = false;
            emit origModelNameChanged();
            emit origModelLoadedChanged();
        }

		void MeshOptimizing::resetQualityAndOptimStatus() {
            m_optimizerManager.getOptimizer(METRIC_TYPE::MESQUITE)->setIsQualityComputed(false);
            m_optimizerManager.getOptimizer(METRIC_TYPE::CREASEDET)->setIsQualityComputed(false);
            m_optimizerManager.getOptimizer(METRIC_TYPE::SURFSMOOTH)->setIsQualityComputed(false);
            m_optimizerManager.getOptimizer(METRIC_TYPE::MESQUITE)->setOptimized(false);
            m_optimizerManager.getOptimizer(METRIC_TYPE::CREASEDET)->setOptimized(false);
            m_optimizerManager.getOptimizer(METRIC_TYPE::SURFSMOOTH)->setOptimized(false);
		}

        double MeshOptimizing::getMaxMetricValue(QString const& metric) const{
            std::string metriclower(metric.toStdString());
            std::transform(metriclower.begin(), metriclower.end(), metriclower.begin(), ::tolower);
            if (!metriclower.compare("scaledjacobian"))
                return myoptimizer->getMaxValueMetric3D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN);
            else if (!metriclower.compare("meanratio"))
                return myoptimizer->getMaxValueMetric3D(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio);
            else return -1;
        }

        double MeshOptimizing::getMinMetricValue(QString const& metric) const{
            std::string metriclower(metric.toStdString());
            std::transform(metriclower.begin(), metriclower.end(), metriclower.begin(), ::tolower);
            if (!metriclower.compare("scaledjacobian"))
                return myoptimizer->getMinValueMetric3D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN);
            else if (!metriclower.compare("meanratio"))
                return myoptimizer->getMinValueMetric3D(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio);
            else return -1;
        }

        double MeshOptimizing::getThreshold(QString metric) const {
            return m_tablemodel->getThreshold(metric);
        }

        void MeshOptimizing::setThreshold(QString metric, const QVariant &value) {
            m_tablemodel->setThreshold(metric, value);
        }

        void MeshOptimizing::setSmoothOptionInt(QString optionName, QVariant value) {
            m_smoothOptionInt[optionName.toStdString()] = value.toInt();
        }

        void MeshOptimizing::setSmoothOptionDouble(QString optionName, QVariant value) {
            m_smoothOptionDouble[optionName.toStdString()] = value.toDouble();
        }

        void MeshOptimizing::setSmoothOptionBool(QString optionName, QVariant value) {
            m_smoothOptionBool[optionName.toStdString()] = value.toBool();
        }

        void MeshOptimizing::setOptionBone(const QVariant &value) {
            m_option[QUALITY_OPT::evaluatedBone] = value.toBool();
            reset();
        }

        void MeshOptimizing::setOptionElementInv(const QVariant &value) {
            m_option[QUALITY_OPT::ElementInverted] = value.toBool();
            emit optionElementInvChanged();
        }

        void MeshOptimizing::setOptionElementMeanRatio(const QVariant &value) {
            m_option[QUALITY_OPT::ElementMeaRation] = value.toBool();
            emit optionElementMeanRatioChanged();
        }

		void MeshOptimizing::setOptionTaubinFIR(const QVariant &value) {
			m_option[QUALITY_OPT::TaubinFIR] = value.toBool();
			emit optionTaubinFIRChanged();
		}

		void MeshOptimizing::setOptionKrigBox(const QVariant &value) {
			m_option[QUALITY_OPT::KrigBox] = value.toBool();
			emit optionKrigBoxChanged();
		}

        void MeshOptimizing::setOptionMoveAvg(const QVariant &value) {
            m_option[QUALITY_OPT::MoveAvg] = value.toBool();
            emit optionMoveAvgChanged();
        }

		void MeshOptimizing::setOptionElementInvertedKriging(const QVariant &value) {
			m_option[QUALITY_OPT::ElementInvertedKriging] = value.toBool();
			emit optionElementInvertedKrigingChanged();
		}

        void MeshOptimizing::setOptionGenerateBoxesAndSelect(const QVariant &value) {
            generateBoxesAndSelect = value.toBool();

            // if there is already something to visualize, re-setup it based on the new visualization option
            if (myoptimizer->isQualityComputed())
            {
                if (generateBoxesAndSelect)
                {
                    for (int i = 0; i < boxesAddedToContextDisplay.size(); i++)
                    {
                        Context::instance().display().SelectSelectionBox(boxesAddedToContextDisplay[i]);
                        Context::instance().display().RemoveSelectedBox();
                    }
                    boxesAddedToContextDisplay.clear();
                    curSelectionBoxes.clear();
                    curSelectionBoxes = myoptimizer->generateSelectionBoxes(mergeBoxes);
                    invalidateVisualization();
                }
                setupVisualization();
            }

            emit optionGenerateBoxesAndSelectChanged();
        }

        void MeshOptimizing::optionMesqSelectLowQuality() {
            if (myoptimizer->isQualityComputed() && Context::instance().hasModel())
            {
                // color each element that is below the threshold with one color, rest with other - low quality
                vtkUnstructuredGrid *contextModel = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh();

                vtkSmartPointer<vtkIdTypeArray> eid = vtkIdTypeArray::SafeDownCast(contextModel->GetCellData()->GetArray("eid"));
                vtkSmartPointer<vtkBitArray> sel_prim = hbm::vtkSelectionTools::ObtainSelectionArrayCells(contextModel, true);
                hbm::VId vid_ignore;
                if (!m_option[QUALITY_OPT::evaluatedBone]) {
                    // ignore bones entities: define ids of elements of bone entities to be ignored when part with poor quality are identified
                    vid_ignore = Context::instance().project().model().findBoneElements();
                }

                std::vector<std::vector<int>> vnum;
                hbm::VId vid;
                myoptimizer->identifyLowQualityGroup(m_tablemodel->getThreshold(), vid, vnum, vid_ignore);
                auto lowQualityElements = myoptimizer->getLowQualityElements();
                for (auto it = lowQualityElements->begin(); it != lowQualityElements->end(); it++) // for each group of low quality elements
                {
                    for (auto iter = it->second.begin(); iter != it->second.end(); iter++) // for each quality metric
                    {
                        for (auto eleIter = iter->second.begin(); eleIter != iter->second.end(); eleIter++) // mark each element
                        {
                            if (eid)
                                sel_prim->SetValue(eid->GetTuple1(*eleIter), IS_PRIMITIVE_SELECTED);
                            else
                                sel_prim->SetValue(*eleIter, IS_PRIMITIVE_SELECTED);
                        }
                    }
                }
                sel_prim->Modified();
                if (visualizationOn)
                {
                    // get the context model full mesh name
                    std::string fullModelName = Context::instance().objectName().toStdString();
                    if (fullModelName == "")
                        fullModelName = "full_model";
                    setupVisualization();
                    Context::instance().display().UpdateActor(fullModelName);
                    Context::instance().display().HighlightElementsByScalars(fullModelName, SELECTED_PRIMITIVES);
                    Context::instance().display().Render();
                }
            }
        }

        void MeshOptimizing::setOptionMergeBoxes(const QVariant &value) {
            mergeBoxes = value.toBool();
            boxesUpToDate = false;
            boxSelectionUpToDate = false;
            if (generateBoxesAndSelect)
                setOptionGenerateBoxesAndSelect(true); // force rebuilding of the boxes
        }

        void MeshOptimizing::setOptionVisualizationOn(const QVariant &value) {
            visualizationOn = value.toBool();
            // changing to the visualization panel causes a new framebuffer to be created -> new camera -> need to reset
            // would be nice if we instead kept the same framebuffer and just toggled it visible / invisible instead
            resetCamera = true; 
            setupVisualization();
            emit optionVisualizationOnChanged();
        }

        void MeshOptimizing::setEntitiesToProcess(QVariantList entityNames)
        {
            entitiesToProcessNames.clear();
            for (auto it = entityNames.begin(); it != entityNames.end(); it++)
                entitiesToProcessNames.push_back(it->toString().toStdString());
            myoptimizer->setIsQualityComputed(false);
            emit qualityStatusChanged();
        }

        void MeshOptimizing::setOptionQualUseBase(const QVariant &value) {
            qualityUseBaseline = value.toBool();
            myoptimizer->setIsQualityComputed(false);
            emit qualityStatusChanged();
            emit optionQualUseBaseChanged();
        }

        void MeshOptimizing::exportQualityfile(std::string const& file) const {
            myoptimizer->exportQuality(file);
        }
        

        void MeshOptimizing::exportQuality(const QUrl& file) {
            pInfo() << START << "Save Quality in file: " << file.path() ;
            setOperationSignals(true, true, true); // TODO check signals to emit

            // TODO: check if exportQualityfile can throw an exception, if yes use Context::wrapClassMethodCall
            Context::instance().performAsynchronousOperation(std::bind(&MeshOptimizing::exportQualityfile, this, file.toLocalFile().toStdString()),
                false, false, false, false, false);
        }


        void MeshOptimizing::exportQualityFile(const QUrl& file, int indexMetricUsed) {
            //Check if a model has been loaded in the application
            if (Context::instance().hasModel())
            {
                Context::instance().performAsynchronousOperation(std::bind(
                    &MeshOptimizing::doExportQualityFile, this, file.toLocalFile().toStdString(), indexMetricUsed),
                    false, false, false, false, false);
            }
            else{
                pInfo() << WARNING << QStringLiteral("No mesh loaded yet");
            }
        }

        //Allow you to lauch the saving in a file of mesh qualitiy
        void MeshOptimizing::doExportQualityFile(std::string const& filePath, int indexMetricUsed)
        {
            pInfo() << START << "Save Quality in file: " << filePath;
            hbm::FEModelVTK* vtkUnstructuredMesh = Context::instance().project().model().fem().getFEModelVTK();
            vtkUnstructuredMesh->writeEntitiesQualityIntoFile(filePath, indexMetricUsed);
            pInfo() << DONE;
        }

        void MeshOptimizing::SelectByQuality(QVariantList userInputArray)
        {
            // transform the qml array
            boost::container::vector<std::pair<double, double>> ranges;
            for (int i = 0; i < userInputArray.size(); i++)
            {
                QList <QVariant> currentLine = userInputArray[i].toList();
                ranges.push_back(std::pair<double, double>(currentLine[0].toDouble(), currentLine[1].toDouble()));
            }

            // Get the fem models in the scene and browse through them 
            FEModel& fem = Context::instance().project().model().fem();

            std::string name = objectName().toStdString();
            if (name == "")
                name = "full_model";

            if (Context::instance().display().IsActorVisible(name)) // if the full mesh is being rendered, color it, otherwise color the entities
            {
                hbm::vtkSelectionTools::SelectCellsByScalars(fem.getFEModelVTK()->getVTKMesh(), QUALITY_SCALAR_ARRAY, &ranges, true);
                Context::instance().display().UpdateActor(name);
                Context::instance().display().HighlightElementsByScalars(SELECTED_PRIMITIVES, false, name);
            }
            else
            {
                auto map = fem.getFEModelVTK()->getVTKEntityMesh();
                for (auto it = map->begin(); it != map->end(); it++)
                {
                    hbm::vtkSelectionTools::SelectCellsByScalars(it->second, QUALITY_SCALAR_ARRAY, &ranges, true);
                    Context::instance().display().UpdateActor(it->first);
                    Context::instance().display().HighlightElementsByScalars(SELECTED_PRIMITIVES, false, it->first);
                }
            }
            //Update the scene
            Context::instance().display().Render();
        }

        //This function is used to call doQualityComputeAndVisualisation and display the "busy task" spinner on screen
        void MeshOptimizing::QualityComputeAndVisualisation(QVariantList  userInputArray, int indexMetricFunctionUsed, double alphaValue)
        {
            int nbRanges = 0;

            double range1 [2];
            double range2 [2];
            double range3 [2];
            
            // first index is 0 = under 1st range, 1 = first range, 2 = second range, 3 = third range, 4 = over the range
            // second index is element type: 0 = triangles, 1 = quads, 2 = tetra, 3 = hexa
            int nbCellsInRange[5][4]{{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }};
            bool relevantElTypes[4]{false, false, false, false}; // true if the selected metric can be computed for the specific element type - count only elements of those types
            switch (static_cast<piper::METRIC_FUNCTION>(indexMetricFunctionUsed))
            {
            case METRIC_FUNCTION::ASPECT_FROBENIUS:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::MIN_ANGLE:
                relevantElTypes[0] = relevantElTypes[1] = true;
                break;
            case METRIC_FUNCTION::MAX_ANGLE:
                relevantElTypes[0] = relevantElTypes[1] = true;
                break;
            case METRIC_FUNCTION::CONDITION:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::SCALED_JACOBIAN:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::RELATIVE_SIZE_SQUARED:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::SHAPE:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::SHAPE_AND_SIZE:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::DISTORTION:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::AREA:
                relevantElTypes[0] = relevantElTypes[1] = true;
                break;
            case METRIC_FUNCTION::EDGE_RATIO:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            case METRIC_FUNCTION::ASPECT_RATIO:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = true;
                break;
            case METRIC_FUNCTION::RADIUS_RATIO:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = true;
                break;
            case METRIC_FUNCTION::VOLUME:
                relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            default:
                relevantElTypes[0] = relevantElTypes[1] = relevantElTypes[2] = relevantElTypes[3] = true;
                break;
            }
            
            pInfo() << START << "Computing " << QualityMetrics_str[indexMetricFunctionUsed] << "...";
            //Check if a model has been loaded in the application
            if (Context::instance().hasModel())
            {
                //Define ranges
                double rangeMin, rangeMax;

                //Define some colors
                QColor firstColorInterpol, lastColorInterpol;

                //This object will compute colors between the two given by the user
                vtkSmartPointer<vtkColorTransferFunction> ctf = vtkSmartPointer<vtkColorTransferFunction>::New();

                //set default color for every polygons not in the ranges of the ctf
                double defaultColor[3] = { 0.9, 0.9, 0.9 };

                ctf->SetAboveRangeColor(defaultColor[0], defaultColor[1], defaultColor[2]);
                ctf->SetBelowRangeColor(defaultColor[0], defaultColor[1], defaultColor[2]);
                ctf->UseAboveRangeColorOn();
                ctf->UseBelowRangeColorOn();

                //If the parameters array is not empty
                if (userInputArray.size())
                {
                    //Get the number of ranges seted in GUI by the user
                    nbRanges = userInputArray.size();
                   
                    double rangesBias = 0.00000001;

                    //Go trough the user input array given by QML
                    for (int y = 0; y < nbRanges; y++)
                    {
                        //Read the user input array line by line
                        QList <QVariant> currentLine = userInputArray[y].toList();

                        //Get the min and max of this range
                        rangeMin = currentLine[3].toDouble();
                        rangeMax = currentLine[4].toDouble();                            

                        //If just the first color of the line is defined
                        if (currentLine[1].toString() != " " && currentLine[2].toString() == " ")
                        {
                            //We only need one color, so set the same color as first and last interpollation color
                            firstColorInterpol = QColor(currentLine[1].toString());
                            lastColorInterpol = QColor(currentLine[1].toString());
                        }
                        //If color A and B are defined, then proceed to interpolation
                        else if (currentLine[1].toString() != " " && currentLine[2].toString() != " ")
                        {
                            //Get the two defined colors
                            firstColorInterpol = QColor(currentLine[1].toString());
                            lastColorInterpol = QColor(currentLine[2].toString());
                        }
                        //Set the first and last colors for color transfert function
                        //We use a Bias to respect the includes or excludes some values on the edge of the interval (as displayed in GUI)
                        //[[  range 1
                        if (y == 0)
                        {
                            ctf->AddRGBPoint(rangeMin, firstColorInterpol.redF(), firstColorInterpol.greenF(), firstColorInterpol.blueF());
                            ctf->AddRGBPoint(rangeMax - rangesBias, lastColorInterpol.redF(), lastColorInterpol.greenF(), lastColorInterpol.blueF());
                            range1[0] = rangeMin; range1[1] = rangeMax;
                        }
                        //[]  range 2
                        else if (y == 1)
                        {
                            ctf->AddRGBPoint(rangeMin, firstColorInterpol.redF(), firstColorInterpol.greenF(), firstColorInterpol.blueF());
                            ctf->AddRGBPoint(rangeMax, lastColorInterpol.redF(), lastColorInterpol.greenF(), lastColorInterpol.blueF());
                            range2[0] = rangeMin; range2[1] = rangeMax;
                        }
                        //]]  range 3
                        else if (y == 2)
                        {
                            ctf->AddRGBPoint(rangeMin + rangesBias, firstColorInterpol.redF(), firstColorInterpol.greenF(), firstColorInterpol.blueF());
                            ctf->AddRGBPoint(rangeMax, lastColorInterpol.redF(), lastColorInterpol.greenF(), lastColorInterpol.blueF());
                            range3[0] = rangeMin; range3[1] = rangeMax;
                        }
                    }
                    ctf->SetAlpha(alphaValue);

                    // Get the fem models in the scene and browse through them 
                    FEModel& fem = Context::instance().project().model().fem();
                    HumanBodyModel& hbm = Context::instance().project().model();

                    std::string name = objectName().toStdString();
                    if (name == "")
                        name = "full_model";

                    if (Context::instance().display().IsActorVisible(name)) // if the full mesh is being rendered, color it, otherwise color the entities
                    {
                        vtkSmartPointer<vtkDoubleArray> refQuality = NULL;
                        if (qualityUseBaseline)
                        {
                            if (baselineModelFromHistory)
                            {
                                if (hbm.history().isHistory(originalModelName.toStdString()))
                                {
                                    std::string currHistory = hbm.history().getCurrent();
                                    hbm.history().setCurrentModel(originalModelName.toStdString());
                                    FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, fem.getFEModelVTK()->getVTKMesh());
                                    refQuality = vtkSmartPointer<vtkDoubleArray>::New();
                                    refQuality->DeepCopy(vtkDoubleArray::SafeDownCast(fem.getFEModelVTK()->getVTKMesh()->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY)));
                                    hbm.history().setCurrentModel(currHistory);
                                    
                                }
                                else
                                {
                                    pInfo() << WARNING << "The specified history name no longer exists, quality not computed.";
                                    pInfo() << DONE;
                                    return;
                                }
                            }
                            else
                            {
                                FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, hbm_orig.fem().getFEModelVTK()->getVTKMesh());
                                refQuality = vtkDoubleArray::SafeDownCast(hbm_orig.fem().getFEModelVTK()->getVTKMesh()->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));
                            }
                        }

                        FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, fem.getFEModelVTK()->getVTKMesh());
                        vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(fem.getFEModelVTK()->getVTKMesh()->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));
                        vtkIdType nCells = quality->GetNumberOfTuples();
                        if (qualityUseBaseline)
                        {
                            for (vtkIdType i = 0; i < nCells; i++)
                                quality->SetValue(i, quality->GetValue(i) - refQuality->GetValue(i));
                        }
                        auto cellTypes = fem.getFEModelVTK()->getVTKMesh()->GetCellTypesArray();
                        for (vtkIdType i = 0; i < nCells; i++)
                        {
                            double qual = quality->GetValue(i);
                            int elTypeIndex = -1;
                            switch (static_cast<int>(cellTypes->GetValue(i)))
                            {
                            case VTK_TRIANGLE:
                                elTypeIndex = 0;
                                break;
                            case VTK_QUAD:
                                elTypeIndex = 1;
                                break;
                            case VTK_TETRA:
                                elTypeIndex = 2;
                                break;
                            case VTK_HEXAHEDRON:
                                elTypeIndex = 3;
                                break;
                            }
                            if (elTypeIndex >= 0)
                            {
                                if (qual < range1[0])
                                    nbCellsInRange[0][elTypeIndex]++;
                                else if (qual >= range1[0] && qual < range1[1])
                                    nbCellsInRange[1][elTypeIndex]++;
                                else if (qual >= range2[0] && qual < range2[1])
                                    nbCellsInRange[2][elTypeIndex]++;
                                else if (qual >= range3[0] && qual <= range3[1])
                                    nbCellsInRange[3][elTypeIndex]++;
                                else if (qual > range3[1])
                                    nbCellsInRange[4][elTypeIndex]++;
                            }
                        }
                        Context::instance().display().UpdateActor(name);
                        Context::instance().display().HighlightElementsByScalars(QUALITY_SCALAR_ARRAY, false, name, ctf);
                    }
                    else
                    {
                        auto map = fem.getFEModelVTK()->getVTKEntityMesh();
                        boost::container::map<std::string, vtkSmartPointer<vtkDoubleArray>> refQualityMap;
                        if (qualityUseBaseline)
                        {
                            if (baselineModelFromHistory)
                            {
                                if (hbm.history().isHistory(originalModelName.toStdString()))
                                {
                                    std::string currHistory = hbm.history().getCurrent();
                                    hbm.history().setCurrentModel(originalModelName.toStdString());
                                    for (auto it = map->begin(); it != map->end(); it++)
                                    {
                                        //Compute quality for current mesh
                                        FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, it->second);
                                        vtkSmartPointer<vtkDoubleArray> refQuality = vtkSmartPointer<vtkDoubleArray>::New();
                                        refQuality->DeepCopy(vtkDoubleArray::SafeDownCast(it->second->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY)));
                                        refQualityMap[it->first] = refQuality;
                                    }
                                    hbm.history().setCurrentModel(currHistory);
                                }
                                else
                                {
                                    pInfo() << WARNING << "The specified history name no longer exists, quality not computed.";
                                    pInfo() << DONE;
                                    return;
                                }
                            }
                            else
                            {
                                auto refMap = hbm_orig.fem().getFEModelVTK()->getVTKEntityMesh();
                                for (auto it = map->begin(); it != map->end(); it++)
                                {
                                    if (refMap->at(it->first))
                                    {
                                        //Compute quality for current mesh
                                        FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, refMap->at(it->first));
                                        vtkSmartPointer<vtkDoubleArray> refQuality = vtkSmartPointer<vtkDoubleArray>::New();
                                        refQuality->DeepCopy(vtkDoubleArray::SafeDownCast(refMap->at(it->first)->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY)));
                                        refQualityMap[it->first] = refQuality;
                                    }
                                    else
                                    {
                                        pInfo() << WARNING << "The entities in the baseline and current model do not match, quality not computed.";
                                        pInfo() << DONE;
                                        return;
                                    }
                                }
                            }
                        }

                        for (auto it = map->begin(); it != map->end(); it++)
                        {
                            //Compute quality for current mesh
                            FEModelVTK::computeMeshQuality(indexMetricFunctionUsed, it->second);
                            vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(it->second->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));
                            vtkIdType nCells = quality->GetNumberOfTuples();
                            if (qualityUseBaseline)
                            {
                                for (vtkIdType i = 0; i < nCells; i++)
                                    quality->SetValue(i, quality->GetValue(i) - refQualityMap[it->first]->GetValue(i));
                            }
                            Context::instance().display().UpdateActor(it->first);
                            Context::instance().display().HighlightElementsByScalars(QUALITY_SCALAR_ARRAY, false, it->first, ctf);
                            auto cells = it->second->GetCellTypesArray();
                            for (vtkIdType i = 0; i < nCells; i++)
                            {
                                double qual = quality->GetValue(i);
                                int elTypeIndex = -1;
                                switch (static_cast<int>(cells->GetValue(i)))
                                {
                                case VTK_TRIANGLE:
                                    elTypeIndex = 0;
                                    break;
                                case VTK_QUAD:
                                    elTypeIndex = 1;
                                    break;
                                case VTK_TETRA:
                                    elTypeIndex = 2;
                                    break;
                                case VTK_HEXAHEDRON:
                                    elTypeIndex = 3;
                                    break;
                                }
                                if (elTypeIndex >= 0)
                                {
                                    if (qual < range1[0])
                                        nbCellsInRange[0][elTypeIndex]++;
                                    else if (qual >= range1[0] && qual < range1[1])
                                        nbCellsInRange[1][elTypeIndex]++;
                                    else if (qual >= range2[0] && qual < range2[1])
                                        nbCellsInRange[2][elTypeIndex]++;
                                    else if (qual >= range3[0] && qual <= range3[1])
                                        nbCellsInRange[3][elTypeIndex]++;
                                    else if (qual > range3[1])
                                        nbCellsInRange[4][elTypeIndex]++;
                                }
                            }
                        }

                    }
                    //Update the scene
                    Context::instance().display().Render();

                    // make the log look like a table
                    pInfo() << INFO << "Number of elements within each range:";
                    Context::instance().setLogNBSP(true);
                    pInfo() << " Range | TRIANGLES | QUADRILATERALS | TETRAHEDRA | HEXAHEDRA |";
                    pInfo() << "-------------------------------------------------------------|";
                    std::string firstColumn[5] {" Under |", "   1.  |", "   2.  |", "   3.  |", " Over  |"};
                    for (int i = 0; i < 5; i++)
                    {
                        std::stringstream line;
                        if (relevantElTypes[0])
                            // the word TRIANGLE has 9 characters + 1 for space at the beggining (which in worst case can be used for another digit), etc. for the others
                            line << std::setfill(' ') << std::setw(10) << nbCellsInRange[i][0] << " |";
                        else line << "    N/A    |";
                        if (relevantElTypes[1])
                            line << std::setfill(' ') << std::setw(15) << nbCellsInRange[i][1] << " |";
                        else line << "      N/A       |";
                        if (relevantElTypes[2])
                            line << std::setfill(' ') << std::setw(11) << nbCellsInRange[i][2] << " |";
                        else  line << "    N/A     |";
                        if (relevantElTypes[3])
                            line << std::setfill(' ') << std::setw(10) << nbCellsInRange[i][3] << " |";
                        else line << "    N/A    |";
                        pInfo() << firstColumn[i] << line.str();
                    }             
                    Context::instance().setLogNBSP(false);
                }
            }
            else
            {
                pInfo() << INFO << QStringLiteral("No mesh loaded yet");
            }
            pInfo() << DONE;
        }


        bool MeshOptimizing::optionBone() const {
            return m_option.at(QUALITY_OPT::evaluatedBone);
        }

        bool MeshOptimizing::optionElementInv() const {
            return m_option.at(QUALITY_OPT::ElementInverted);
        }

        bool MeshOptimizing::optionElementMeanRatio() const {
            return m_option.at(QUALITY_OPT::ElementMeaRation);
		}

		bool MeshOptimizing::optionTaubinFIR() const {
			return m_option.at(QUALITY_OPT::TaubinFIR);
		}

		bool MeshOptimizing::optionKrigBox() const {
			return m_option.at(QUALITY_OPT::KrigBox);
		}

        bool MeshOptimizing::optionMoveAvg() const {
            return m_option.at(QUALITY_OPT::MoveAvg);
        }

		bool MeshOptimizing::optionElementInvertedKriging() const {
			return m_option.at(QUALITY_OPT::ElementInvertedKriging);
		}

        bool MeshOptimizing::optionGenerateBoxesAndSelect() const {
            return generateBoxesAndSelect;
        }

        bool MeshOptimizing::optionMergeBoxes() const {
            return mergeBoxes;
        }

        bool MeshOptimizing::optionVisualizationOn() const {
            return visualizationOn;
        }

        QString MeshOptimizing::metric() const {
            return METRIC_TYPEName_str[(unsigned int)m_metric];
        }

        bool MeshOptimizing::qualityStatus() const {
            return myoptimizer->isQualityComputed();
        }
        
        bool MeshOptimizing::optimStatus() const {
            return myoptimizer->isOptimized();
        }

		bool MeshOptimizing::origModelLoaded() const {
			return isOrigModelLoaded;
		}

        bool MeshOptimizing::optionQualUseBase() const {
            return qualityUseBaseline;
        }

        QString MeshOptimizing::origModelName() const {
            return originalModelName;
        }

        tableQualityManager::tableQualityManager() : 
            m_mes(new TableQualityModelMES()), m_surfsmooth(new TableQualityModelSurfSmooth()) {}

        tableQualityManager::~tableQualityManager() {
            if (m_mes != nullptr)
                delete m_mes;
			if (m_surfsmooth != nullptr)
				delete m_surfsmooth;
        }

        TableQualityModel* tableQualityManager::getTableQuality(METRIC_TYPE metric) {
            if (metric == METRIC_TYPE::MESQUITE) {
                if (m_mes == nullptr)
                    m_mes = new TableQualityModelMES();
                return m_mes;
            }
            else if (metric == METRIC_TYPE::SURFSMOOTH || metric == METRIC_TYPE::CREASEDET) {
				if (m_surfsmooth == nullptr)
					m_surfsmooth = new TableQualityModelSurfSmooth();
				return m_surfsmooth;
			}
            else return nullptr;
        }

        void tableQualityManager::set(meshoptimizer::METRIC_TYPE metric, TableQualityModel* table) {
            if (metric == METRIC_TYPE::MESQUITE) {
                m_mes = table;
            }
            else if (metric == METRIC_TYPE::SURFSMOOTH || metric == METRIC_TYPE::CREASEDET) {
				m_surfsmooth = table;
			}
        }

        void tableQualityManager::reset() {
            if (m_mes != nullptr) {
                m_mes->clear();
                m_mes->resetThreshold();
            }
			if (m_surfsmooth != nullptr) {
				m_surfsmooth->clear();
				m_surfsmooth->resetThreshold();
			}
        }


        OptimizerManager::OptimizerManager(){
            m_mapoptimizer[METRIC_TYPE::MESQUITE] = new meshoptimizer::meshOptimizerInterface();
			m_mapoptimizer[METRIC_TYPE::MESQUITE]->setMetric(METRIC_TYPE::MESQUITE);
            m_mapoptimizer[METRIC_TYPE::CREASEDET] = new meshoptimizer::meshOptimizerInterface();
            m_mapoptimizer[METRIC_TYPE::CREASEDET]->setMetric(METRIC_TYPE::CREASEDET);
			m_mapoptimizer[METRIC_TYPE::SURFSMOOTH] = new meshoptimizer::meshOptimizerInterface();
			m_mapoptimizer[METRIC_TYPE::SURFSMOOTH]->setMetric(METRIC_TYPE::SURFSMOOTH);
        }

        OptimizerManager::~OptimizerManager() {
            for (auto it = m_mapoptimizer.begin(); it != m_mapoptimizer.end(); ++it)
                delete it->second;
            m_mapoptimizer.clear();
        }

        meshoptimizer::meshOptimizerInterface* OptimizerManager::getOptimizer(METRIC_TYPE metric) {
            return m_mapoptimizer[metric];
        }

        void OptimizerManager::reset() {
            for (auto it = m_mapoptimizer.begin(); it != m_mapoptimizer.end(); ++it)
                it->second->reset();
        }

    }
}

