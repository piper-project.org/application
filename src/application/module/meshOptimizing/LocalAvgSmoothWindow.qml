// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.meshOptimizing 1.0

import VtkQuick 1.0


ModuleToolWindow 
{
    title: "Smooth transformation using local average - EXPERIMENTAL"
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin
    property MeshOptimizing myMeshoptimizingSurfSmooth

    ColumnLayout 
    {
        anchors.fill: parent
        anchors.margins : 10
        

        BaseModelSelectGroupBox
        {
            Layout.fillWidth: true
            myMeshoptimizing: myMeshoptimizingSurfSmooth
            onBaseModelChanged:
            {
                adjustWindowSizeToContent();
            }
        }
        GroupBox
        {
            id: chooseEntOpt
            title: "Choose entities to use as targets"
            Layout.fillWidth : true
            Layout.fillHeight : true
            EntityTreeBrowser
            {
                anchors.fill: parent
                anchors.margins : 10
                id: entityBrowser
                defaultEntity : "Skin"
                treeViewTitle : "Select target entities"
            }
        }
        GroupBox
        {
		    id: rectKrigingOpt
            Layout.fillWidth: true
            title: "Local average options"

            GridLayout
            {
                id: krigingOptions
                anchors.fill: parent
                anchors.margins : 10

                Text
                {
                    Layout.row: 0
                    Layout.column : 0
    		        id: iterationText
    		        text : qsTr("Maximum iterations: ")
    		        fontSizeMode : Text.Fit;
                    minimumPixelSize: 9;
    		        horizontalAlignment : Text.AlignLeft
    	        }
                TextField
                {
                    Layout.row: 0
                    Layout.column : 1
    		        id: iterationTextInput
                    validator : IntValidator{ bottom:1; }
    		        font.pointSize : 9
    		        horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            myMeshoptimizingSurfSmooth.setSmoothOptionInt(qsTr("maxIterations"), parseInt(text))
                    }
    	        }
                Text
                {
                    Layout.row: 1
                    Layout.column : 0
    		        id: neighborsText
    		        text : qsTr("Number of neighbours: ")
    		        fontSizeMode : Text.Fit;
                    minimumPixelSize: 9;
    		        horizontalAlignment : Text.AlignLeft
    	        }
                TextField
                {
                    Layout.row: 1
                    Layout.column : 1
    		        id: neighborsTextInput
                    validator : IntValidator{ bottom:1; }
    		        font.pointSize : 9
    		        horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            myMeshoptimizingSurfSmooth.setSmoothOptionInt(qsTr("noOfNeighbors"), parseInt(text))
                    }
    	        }
                CheckBox
                {
                    Layout.row: 2
                    Layout.columnSpan : 2
                    id: useAutoStop
                    text : qsTr("Automatic stop")
                    onCheckedChanged :
                    {
                        autoStopInput.enabled = checked
                        myMeshoptimizingSurfSmooth.setSmoothOptionBool(qsTr("useAutoStop"), checked)
                    }
                }   
                Text
                {
                    Layout.row: 3
                    Layout.column : 0
                    id: autoStopText
                    text : qsTr("Auto stop displacement threshold (stops if change is lower): ")
                    fontSizeMode : Text.Fit;
                    minimumPixelSize: 9;
                    horizontalAlignment : Text.AlignLeft
                }
                TextField
                {
                    Layout.row: 3
                    Layout.column : 1
                    id: autoStopInput
                    validator : DoubleValidator{ bottom:0; }
                    font.pointSize : 9
                    horizontalAlignment : Text.AlignRight
                    onTextChanged: {
                        if (acceptableInput)
                            myMeshoptimizingSurfSmooth.setSmoothOptionDouble(qsTr("autoStopThreshold"), parseFloat(text))
                    }
                }
            }
        } //rectSurfSmoothOpt
		Button
		{
            Layout.fillWidth : true
            id: smooth
            action : smoothAction
        }
    }
    Component.onCompleted: {
        entityBrowser.treeModel.updateEntityList();
        entityBrowser.treeView.selection.selectSingleEntity("Skin");
        myMeshoptimizingSurfSmooth.metric = qsTr("Surface smoothing")
        myMeshoptimizingSurfSmooth.optionTaubinFIR = false
        myMeshoptimizingSurfSmooth.optionKrigBox = false
        myMeshoptimizingSurfSmooth.optionMoveAvg = true
        // set default parameters 
        useAutoStop.checked = true
        autoStopInput.text = 0
        neighborsTextInput.text = 25
        iterationTextInput.text = 3
        adjustWindowSizeToContent();
        Connections
        {
            target: myMeshoptimizingSurfSmooth
            onOrigModelLoadedChanged : 
            {
                smoothAction.enabled = Qt.binding(function(){ return  myMeshoptimizingSurfSmooth.origModelLoaded && context.hasModel })
			}
        }
    }

	Action{
		id: smoothAction
		text : qsTr("Smooth")
	    shortcut : StandardKey.Refresh
		onTriggered :
        {
            myMeshoptimizingSurfSmooth.setEntitiesToProcess(entityBrowser.itemSelected)
            myModelValidator.text = context.getValidHistoryName("Transformation smooth by local average")
            myModelValidator.open()
        }
    }

    ModelHistoryNameDialog{
        id: myModelValidator
        onAccepted : myMeshoptimizingSurfSmooth.smoothSurface(text)
    }
}
























