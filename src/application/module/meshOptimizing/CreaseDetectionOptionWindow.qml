// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.meshOptimizing 1.0
import QtQml.Models 2.2

import VtkQuick 1.0


ModuleToolWindow 
{
    title: "Detect creases in surface mesh by comparing to baseline model"
    width : childrenRect.width + widthMargin
    height : childrenRect.height + heightMargin
    property MeshOptimizing myMeshoptimizingSurfSmooth

    ColumnLayout 
    {
        anchors.fill: parent
        anchors.margins : 10
        
        BaseModelSelectGroupBox
        {
            Layout.fillWidth: true
            myMeshoptimizing: myMeshoptimizingSurfSmooth
            onBaseModelChanged:
            {
                adjustWindowSizeToContent();
            }
        }
        GroupBox
        {
		    id: rectSurfDmgDet
            Layout.fillWidth: true
            Layout.fillHeight: true
            title: "Crease detection options"

            ColumnLayout
            {
                id: creaseDetOptions
                anchors.fill : parent
                anchors.margins : 10
                EntityTreeBrowser
                {
                    id: entityBrowser
                    defaultEntity: "Skin"
                    treeViewTitle: "Choose entities to process"
                }
                LabeledSlider
                {
                    id: dihedralAngleSlider
                    sliderMaxValue: 90
                    sliderMinValue: 0.0
                    sliderStep: 0.5
                    sliderText : "Dihedral angle threshold: "
                    onSliderValueModified : 
                    {
                        if (myMeshoptimizingSurfSmooth)
                        {
    				        myMeshoptimizingSurfSmooth.setThreshold(qsTr("diAngleThreshold"), sliderValue)
                        }	
					}
                } 
                LabeledSlider
                {
                    id: clusterSizeSlider
                    sliderText : "Cluster percentage: "
                    onSliderValueModified : 
                    {
                        if (myMeshoptimizingSurfSmooth)
                        {
                            myMeshoptimizingSurfSmooth.setThreshold(qsTr("clusterSizePerc"), sliderValue)
                        }
                    }
                } 
                CheckBox
                {
                    Layout.fillWidth: true
                    Layout.fillHeight : true
                    id: checkboxGenBoxesAndSelect
                    text : qsTr("Generate boxes around selected elements and select points within them")
                    //tooltip : qsTr("Will switch from skin to full mesh visualization.")
                    onClicked : myMeshoptimizingSurfSmooth.optionGenerateBoxesAndSelect = checkboxGenBoxesAndSelect.checked
                    Component.onCompleted : checked = false
                    Connections
                    {
                        target: myMeshoptimizingSurfSmooth
                        onOptionGenerateBoxesAndSelectChanged : checkboxGenBoxesAndSelect.checked = myMeshoptimizingSurfSmooth.optionGenerateBoxesAndSelect
                    }
                }
                CheckBox
                {
                    Layout.fillWidth: true
                    Layout.fillHeight : true
                    id: checkboxMergeBoxes
                    text : qsTr("Merge generated boxes that are overlapping each other")
                    //tooltip : qsTr("Will switch from skin to full mesh visualization.")
                    onClicked : myMeshoptimizingSurfSmooth.optionMergeBoxes = checkboxMergeBoxes.checked
                    Component.onCompleted : checked = false
                    Connections
                    {
                        target: myMeshoptimizingSurfSmooth
                        onOptionGenerateBoxesAndSelectChanged : checkboxMergeBoxes.checked = myMeshoptimizingSurfSmooth.optionMergeBoxes
                    }
                }
            }
         } //rectSurfDmg
        GroupBox
        {
            Layout.fillWidth: true
            id: rectVisOptions
            title:"Visualization options"

            ColumnLayout
            {
                anchors.margins: 10
                anchors.left : parent.left
                anchors.right : parent.right
                Layout.fillWidth: true
                Layout.fillHeight : true
                CheckBox
                {
                    id: checkboxVisualizeBoxes
                    Layout.fillWidth: true
                    Layout.fillHeight : true    
                    text : qsTr("See selection boxes")                 
                    onClicked :  contextVtkDisplay.optionVisualizeBoxes = checkboxVisualizeBoxes.checked
                    Component.onCompleted : checked =  contextVtkDisplay.optionVisualizeBoxes
                    Connections
                    {
                        target: contextVtkDisplay
                        onOptionVisualizeBoxesChanged : checkboxVisualizeBoxes.checked = contextVtkDisplay.optionVisualizeBoxes
                    }
                }
            }
        }
		Button 
        {
            Layout.fillWidth : true
		    id: computeQuality
		    action : computeQualityAction
        }// Button
    }
    Component.onCompleted: {
        myMeshoptimizingSurfSmooth.metric = qsTr("Crease detection");
        adjustWindowSizeToContent();
        entityBrowser.treeModel.updateEntityList();
        entityBrowser.treeView.selection.selectSingleEntity("Skin");
        Connections
        {
            target: myMeshoptimizingSurfSmooth
            onQualityStatusChanged :
            {
                dihedralAngleSlider.sliderValue = Qt.binding(function(){ return  myMeshoptimizingSurfSmooth.getThreshold(qsTr("diAngleThreshold")) })
                clusterSizeSlider.sliderValue = Qt.binding(function(){ return  myMeshoptimizingSurfSmooth.getThreshold(qsTr("clusterSizePerc")) })
            }
            onOrigModelLoadedChanged : 
            {
                computeQualityAction.enabled = Qt.binding(function(){ return  myMeshoptimizingSurfSmooth.origModelLoaded && context.hasModel})
			}
        }
    }

    Action 
    {
        id: computeQualityAction
        text: qsTr("Compute Quality")
        shortcut: StandardKey.Refresh
        onTriggered: 
        {
            myMeshoptimizingSurfSmooth.setEntitiesToProcess(entityBrowser.itemSelected)
            myMeshoptimizingSurfSmooth.computeQuality()
        }
    }
}
























