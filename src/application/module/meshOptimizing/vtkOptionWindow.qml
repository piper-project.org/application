// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.VtkDisplay 1.0
import piper.meshOptimizing 1.0

ModuleToolWindow 
{
    title: "Vtk Options"
    Layout.fillWidth: true


        Rectangle 
        {
            id: rectVTK
            border.color: "black"
            anchors.topMargin: 10
            anchors.bottomMargin: 80
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            Layout.fillWidth: true
            Layout.fillHeight: true
            //Component.onCompleted: visible = myMeshoptimizing.qualityStatus
            implicitWidth: 220
            implicitHeight: 100
        
            Connections 
            {
                target: myMeshoptimizing
                onQualityStatusChanged: 
                {
                    //console.log(" qualityStatus : " + myMeshoptimizing.qualityStatus)
                    if (myMeshoptimizing.metric===qsTr("VTK")) 
                    {
                        rectVTK.visible = myMeshoptimizing.qualityStatus
                        if (myMeshoptimizing.qualityStatus) 
                        {
                            vtkjacobian.maximumValue=myMeshoptimizing.getMaxMetricValue(qsTr("scaledjacobian"))
                            vtkjacobian.minimumValue=myMeshoptimizing.getMinMetricValue(qsTr("scaledjacobian"))
                            vtkjacobian.value=myMeshoptimizing.getThreshold(qsTr("scaledjacobian"))
                        }
                    }
                    else
                        rectVTK.visible = false
                }
            }
            Text 
            {
                id: thresholdmetric
                text:"Scaled Jacobian Threshold: "
                font.pointSize: 9
                horizontalAlignment: Text.AlignLeft
                anchors.top: rectVTK.top
                anchors.topMargin: 10
                anchors.left: rectVTK.left
                anchors.leftMargin: 10

            }

            TextInput 
            {
                id: thresholdjaco
                text:vtkjacobian.value.toPrecision(5)
                font.pointSize: 9
                //width: parent.width/3
                horizontalAlignment: Text.AlignLeft
                anchors.top: thresholdmetric.top
                anchors.topMargin: 20
                anchors.left: rectVTK.left
                anchors.leftMargin: 10
            }
            Slider 
            {
                id: vtkjacobian
                stepSize: 0
                maximumValue: 1.0
                minimumValue: 0.0
                //value: 0.15
                updateValueWhileDragging: true
                anchors.top: thresholdmetric.top
                anchors.topMargin: 40
                anchors.left: rectVTK.left
                anchors.leftMargin: 10
                onPressedChanged: 
                {
                    if (pressed==false) 
                    {
                        myMeshoptimizing.setThreshold(qsTr("scaledjacobian"),value)
                        myMeshoptimizing.computeQuality()
                    }
                }
            }
        
        }//rectVTK
    
    MeshOptimizing 
    {
        id: myMeshoptimizing
    }
}

       
