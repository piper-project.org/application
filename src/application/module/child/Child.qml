// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQml.Models 2.2

import piper.AnthropometryModule 1.0
import Piper 1.0
import VtkQuick 1.0

//import piper.BodyDimTargetGen 1.0
//import "qrc:///module//bodyDimTargetGen" 1.0
import "qrc:///module//anthropometry" 1.0


ModuleLayout
{
    id: root
    content: DefaultVtkViewer { }
    toolbar:ColumnLayout
    {
        ColumnLayout
        {
            id: moduleSpecificControls
			anchors.top: parent.top
			RowLayout
			{
				GroupBox
				{
					id: scalingTools
					title: qsTr("Scaling Tools")

					ExclusiveGroup
					{
						id: scalingToolsGroup
					}

					ColumnLayout
					{
						anchors.fill: parent
						ModuleToolWindowButton
						{
							id: anthroPersoChildTool
							text:"  \&PIPER-child scaling  "
							shortcutChar: "C"
							toolWindow: anthroPersoChild
							tooltip: qsTr("Scale child model by age or height") 
							checkable: true
							Layout.fillWidth: true						
							exclusiveGroup: scalingToolsGroup
						}
					}
				}
			}
		}
		ColumnLayout
		{
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 10
			DetachableVtkViewerCommonTools
			{
			}
        }

		Item
		{
			AnthropometryModule
			{
				id:myAnthropometryModule
			}
			Connections
			{
				target: context
				onVisDataLoaded: {
					contextVtkDisplay.Refresh();
				}
			}
			Component.onCompleted: {
				anthroPerso._myAnthropometryModule = myAnthropometryModule
				myAnthropometryModule.checkIfResultFolderExists();
			}
				
		}

		AnthroPersoChild {
			id: anthroPersoChild
		}
		AnthroPerso {
			id: anthroPerso
		}	
	}
}
