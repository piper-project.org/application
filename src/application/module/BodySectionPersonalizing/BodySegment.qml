// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0

Item {
    id: viapointProp
//    property int currentviapoint: -1
    property string currentsegment: ""

    Layout.fillWidth : true
    ColumnLayout {
        Layout.fillWidth : true
        width: 370
        anchors.margins : itemMargin

        Button {
            id: newViaPointButton
            Layout.fillWidth : true
            anchors.margins : itemMargin
            action: newViaPointAction
        }
        TableView {
            id: viapointTable
            Layout.fillWidth : true
            Layout.fillHeight : true
            Layout.preferredHeight: 110
            model: myBodySectionPersonalizing.viapoint
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            currentRow: -1

            TableViewColumn {
                id: col_name
                role: "NameRole"
                title: "Name"
                movable: false
                resizable: false
                width: 115
            }
            TableViewColumn {
                id: col_cpviapoint
                role:"cpViaPointRole"
                title: "C.P. on viapoint"
                resizable: true
                movable: false
                width: 110
                delegate: Item {
                    CheckBox {
                        id: checkCpViaPoint
                        enabled: viapointTable.currentRow === styleData.row
                        anchors.centerIn:parent
                        Component.onCompleted: {
                            checked= viapointTable.model.get(styleData.row).cpViaPointRole                            
                        }
                        onClicked: {
                            myBodySectionPersonalizing.setViapointCP(viapointTable.currentRow, checked)
                            if (viapointTable.currentRow > -1) {
                                viapointTable.selection.clear()
                                viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
                            }
                        }
                    }
                }
            }

            TableViewColumn {
                id: col_cplandmark
                role:"cpLandmarkRole"
                title: "C.P. on landmarks"
                resizable: false
                movable: false
                width: 110
                delegate: Item {
                    CheckBox {
                        id: checkCpLandmark
                        enabled: viapointTable.currentRow === styleData.row
                        anchors.centerIn:parent
                        Component.onCompleted: {
                            checked= viapointTable.model.get(styleData.row).cpLandmarkRole
                        }
                        onClicked: {
                            myBodySectionPersonalizing.setLandmarkCP(viapointTable.currentRow, checked)
                            if (viapointTable.currentRow > -1) {
                                viapointTable.selection.clear()
                                viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
                            }
                        }
                    }
                }
            }
            onCurrentRowChanged: {
                landamrksComboTable.internalTableModel = viapointTable.model.getLandmarksModel(viapointTable.currentRow)
                _myAnthropoModelDisplay.highlightSourceSegmentViaPoint(myBodySectionPersonalizing.currentComponent, viapointTable.currentRow, true, true)
            }
            Menu { id: contextMenuViaPoint
                MenuItem {
                    action: deleteViaPointAction
                }
            }
            rowDelegate: Item {
                Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height
                    color: styleData.selected ? 'lightblue' : 'white'
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        propagateComposedEvents: true
                        onReleased: {
                            if (typeof styleData.row === 'number') {
                                viapointTable.currentRow = styleData.row
                                if (mouse.button === Qt.RightButton) {
                                   contextMenuViaPoint.popup()
                                }//if
                            }//if
                            mouse.accepted = false
                        }//onReleased
                    }//MouseArea
                }//Rectangle
            }//rowDelegate
        }

        ComboTableComponent {
            id: landamrksComboTable
            Layout.fillWidth : true
            implicitTableHeight: 90
            enabled: viapointTable.currentRow!= -1

            internalComboList: myBodySectionPersonalizing.hbmlandmarks
            internalTableModel: myBodySectionPersonalizing.viapoint.getLandmarksModel(viapointTable.currentRow)
            onAddInTable: {
                myBodySectionPersonalizing.addLandmarks(viapointTable.currentRow, comboItem)
                landamrksComboTable.internalTableModel = viapointTable.model.getLandmarksModel(viapointTable.currentRow)
            }
            onRemoveCurrentItem: {
                myBodySectionPersonalizing.removeLandmarks(viapointTable.currentRow, tableItem)
                landamrksComboTable.internalTableModel = viapointTable.model.getLandmarksModel(viapointTable.currentRow)
            }
            onInternalTableModelChanged: {
                if (viapointTable.currentRow > -1) {
                    viapointTable.selection.clear()
                    viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
                }
            }

        }
    }

    onVisibleChanged: viapointTable.currentRow=-1

    Connections {
        target: myBodySectionPersonalizing
        onCurrentComponentChanged: {
            if (currentsegment !== myBodySectionPersonalizing.currentComponent) {
                currentsegment = myBodySectionPersonalizing.currentComponent
                viapointTable.currentRow = -1
            }
            if (viapointTable.currentRow > -1) {
                viapointTable.selection.clear()
                viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
            }
        }
    }

    Action {
        id: newViaPointAction
        text: qsTr("Add New Via-point")
        onTriggered: {
            myBodySectionPersonalizing.insertViapoint(viapointTable.currentRow+1)
            viapointTable.currentRow = viapointTable.currentRow + 1
            if (viapointTable.currentRow > -1) {
                viapointTable.selection.clear()
                viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
            }
        }
    }
    Action {
        id: deleteViaPointAction
        text: qsTr("delete current Via-point")
        onTriggered: {
            viapointTable.currentRow = viapointTable.currentRow - 1
            myBodySectionPersonalizing.removeViapoint(viapointTable.currentRow+1)
            if (viapointTable.currentRow > -1) {
                viapointTable.selection.clear()
                viapointTable.selection.select(viapointTable.currentRow, viapointTable.currentRow)
            }
        }
    }

}
