/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/HumanBodyModel.h"
#include "BodySectionPersonalizer/AnthropoModel.h"
#include "AnthropoModelDisplay.h"
#include "AnthropoTree.h"
#include "module/kriging/Kriging.h"

#include <QObject>
#include <QString>
#include <QUrl>
#include <QtCore>


namespace piper {

    namespace anthropoPersonalizing {

        enum class OPTION_SCALING {
            ISOTROPIC = 0,     ///< 
            PARAMETRIC,     ///< 
            FIXBONES,
            NONE,     ///< 
        };
       
		struct ModeItem {
			ModeItem(QString name) : m_name(name) {}
			QString m_name;
		};

		class ModelTable : public QAbstractListModel
		{
			Q_OBJECT
		public:
			ModelTable(QObject *parent = 0) : QAbstractListModel(parent) {}
			ModelTable(std::set<std::string> const& listnames, QObject *parent = 0);
			//implement abstract method of QAbstractTableModel
			QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            Q_INVOKABLE QString const get(int index) const { return items.at(index)->m_name; }
            Q_INVOKABLE QVariant const get(QString const& value) const;
			int rowCount(const QModelIndex &parent) const;
			int columnCount(const QModelIndex &parent) const;
			QHash<int, QByteArray> roleNames() const;
			enum Roles {
				NameRole = Qt::UserRole + 1,
			};
			bool setData(const QModelIndex &index, const QVariant &value, int role);
			Q_INVOKABLE bool setData(int row, int column, const QVariant value)
			{
				int role = Qt::UserRole + 1 + column;
				return setData(index(row, 0), value, role);
			}
			Qt::ItemFlags flags(const QModelIndex &index) const;


			void construct(const std::set<std::string>& listnames);
			void clear();
			//private:
			QList<ModeItem*> items;
		};

        struct ModelcomboItem {
            ModelcomboItem(QString const& name, anthropometricmodel::AnthropoDimensionType const& type, bool const& isrelative, QString const& ref);
            ModelcomboItem(QString const& name, anthropometricmodel::AnthropoDimensionType const& type, bool const& isrelative);
            QString m_name;
            QString m_type;
            QString m_ref;
            bool m_isrelative;
        };


        class ModelTableCombo : public QAbstractListModel
        {
            Q_OBJECT
        public:
            ModelTableCombo(QObject *parent = 0) : QAbstractListModel(parent), noOfColumns(2){}
            //ModelTableCombo(std::vector<std::string>& listnames, QObject *parent = 0);
            //implement abstract method of QAbstractTableModel
            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            Q_INVOKABLE QString const getName(int index) const { return items.at(index)->m_name; }
            Q_INVOKABLE QString const getType(int index) const { return items.at(index)->m_type; }
            Q_INVOKABLE QString const getRef(int index) const { return items.at(index)->m_ref; }
            Q_INVOKABLE bool const getIsRelative(int index) const { return items.at(index)->m_isrelative; }
            Q_INVOKABLE QVariant const get(QString const& value) const;
            Q_INVOKABLE int rowCount(const QModelIndex &parent) const;
            int columnCount(const QModelIndex &parent) const;
            QHash<int, QByteArray> roleNames() const;
            enum Roles {
                NameRole = Qt::UserRole + 1,
                TypeRole,
                RefRole,
                RelativeRole,
            };
            bool setData(const QModelIndex &index, const QVariant &value, int role);
            Q_INVOKABLE bool setData(int row, int column, const QVariant value)
            {
                int role = Qt::UserRole + 1 + column;
                return setData(index(row, 0), value, role);
            }
            Qt::ItemFlags flags(const QModelIndex &index) const;


            void construct(anthropometricmodel::AnthropoModel const& anthropoModel, std::vector<std::string> const& list_dim);

            void clear();
            //private:
            QList<ModelcomboItem*> items;
        protected:
            int noOfColumns; // the number of columns in the table
        };

        struct ModelAssBodySectionItem {
            ModelAssBodySectionItem(QString const& name, QVariant const& weight);
            QString m_name;
            double m_weight;
        };

        class ModelAssBodySection : public QAbstractListModel
        {
            Q_OBJECT
        public:
            ModelAssBodySection(QObject *parent = 0) : QAbstractListModel(parent), noOfColumns(2){}
            //ModelTableCombo(std::vector<std::string>& listnames, QObject *parent = 0);
            //implement abstract method of QAbstractTableModel
            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            Q_INVOKABLE QString const getName(int index) const { return items.at(index)->m_name; }
            Q_INVOKABLE QVariant const getWeight(int index) const { return items.at(index)->m_weight; }
            Q_INVOKABLE QVariant const get(QString const& value) const;
            Q_INVOKABLE int rowCount(const QModelIndex &parent) const;
            int columnCount(const QModelIndex &parent) const;
            QHash<int, QByteArray> roleNames() const;
            enum Roles {
                NameRole = Qt::UserRole + 1,
                WeightRole,
            };
            bool setData(const QModelIndex &index, const QVariant &value, int role);
            Q_INVOKABLE bool setData(int row, int column, const QVariant value)
            {
                int role = Qt::UserRole + 1 + column;
                return setData(index(row, 0), value, role);
            }
            Qt::ItemFlags flags(const QModelIndex &index) const;


            void construct(const std::map<anthropometricmodel::AbstractAnthropoComponentNode*, double>& assbodysection);
            void clear();
            //private:
            QList<ModelAssBodySectionItem*> items;
        protected:
            int noOfColumns; // the number of columns in the table
        };


		struct ModelBodySectionSourceTargetItem {
            ModelBodySectionSourceTargetItem(std::string const& dimName, anthropometricmodel::AnthropoDimensionType const& dimType, std::string const& compName, double value);
            ModelBodySectionSourceTargetItem(std::string const& dimName, anthropometricmodel::AnthropoDimensionType const& dimType, std::string const& compName, double value, double valuetarget);
			QString m_nameDimension;
			QString m_typeDimension;
			QString m_nameComp;
			double m_valueSource;
            double m_valueTarget;
		};

        class BodyDimensionSourceTargetTable : public QAbstractListModel
		{
			Q_OBJECT
		public:
			BodyDimensionSourceTargetTable(QObject *parent = 0) : QAbstractListModel(parent), noOfColumns(5){}
			//implement abstract method of QAbstractTableModel
			QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            Q_INVOKABLE QString getSectionName(const int &index) const { return items[index]->m_nameComp; }
            Q_INVOKABLE QString getDimName(const int &index) const { return items[index]->m_nameDimension; }
            Q_INVOKABLE double getTargetValue(const int &index) const { return items[index]->m_valueTarget; }
            int rowCount(const QModelIndex &parent) const;
			int columnCount(const QModelIndex &parent) const;
			QHash<int, QByteArray> roleNames() const;
			enum Roles {
				NameDimRole = Qt::UserRole + 1,
				TypeDimRole,
				NameSectionRole,
				ValueSourceRole,
                ValueTargetRole,

			};
			bool setData(const QModelIndex &index, const QVariant &value, int role);
            Q_INVOKABLE bool setData(int row, int column, const QVariant value);
			Qt::ItemFlags flags(const QModelIndex &index) const;


            void construct(anthropometricmodel::AnthropoModel& personalizer, std::string const& type);
			void clear();
			//private:
			QList<ModelBodySectionSourceTargetItem*> items;
		protected:
			int noOfColumns; // the number of columns in the table
		};

        class ListModel : public QObject {
			Q_OBJECT
				Q_PROPERTY(QStringList data  READ data NOTIFY dataChanged)
		public:
			//ListModel();
			Q_INVOKABLE void init(const std::vector<std::string>& values);
			Q_INVOKABLE int getindex(QString const& value);
			QStringList data() const { return m_data; }
            void append(QString const& value) { m_data.append(value); }
            void remove(QString const& value) { m_data.removeAll(value); }
			void clear() { m_data.clear(); }
		signals:
			void dataChanged();
		private:
			QStringList m_data;
		};

        
        struct ModelviaPointItem {
            ModelviaPointItem(QString const& name, QVariant const& cpViaPointRole, QVariant const& cpLandmarkRole, std::set<std::string> const& landmarks);
            QString m_name;
            bool m_cpViaPointRole;
            bool m_cpLandmarkRole;
            ModelTable* m_landmarks;
        };

        class ModelviaPoint : public QAbstractListModel {
            Q_OBJECT
        public:
            ModelviaPoint(QObject *parent = 0) : QAbstractListModel(parent), noOfColumns(2){}
            //implement abstract method of QAbstractTableModel
            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            Q_INVOKABLE QVariantMap get(int idx) const;
            Q_INVOKABLE int rowCount(const QModelIndex &parent) const;
            int columnCount(const QModelIndex &parent) const;
            QHash<int, QByteArray> roleNames() const;
            enum Roles {
                NameRole = Qt::UserRole + 1,
                cpViaPointRole,
                cpLandmarkRole,
            };
            bool setData(const QModelIndex &index, const QVariant &value, int role);
            Q_INVOKABLE bool setData(int row, int column, const QVariant value)
            {
                int role = Qt::UserRole + 1 + column;
                return setData(index(row, 0), value, role);
            }
            void construct(anthropometricmodel::AnthropoBodySegment * segment);
            Q_INVOKABLE ModelTable* getLandmarksModel(int idx) const;
            void clear();
            //private:
            QList<ModelviaPointItem*> items;
        protected:
            int noOfColumns; // the number of columns in the table
        };


        /** @brief BodySectionPersonalizing module.
		 *
		 * @author Erwan Jolivet @date 2016
         */
        class BodySectionPersonalizing : public QObject
        {
            Q_OBJECT

                Q_PROPERTY(bool isAnthropoModelLoaded READ isAnthropoModelDefined NOTIFY anthropoModelChanged)


				//PROPERTY RELATED TO HBM in CONTEXT
                // list of landmarks defined on hbm
				Q_PROPERTY(QStringList hbmlandmarks READ hbmlandmarks NOTIFY hbmlandmarksChanged)
                // list of entities defined on hbm
				Q_PROPERTY(QStringList hbmentities READ hbmentities NOTIFY hbmentitiesChanged)
                // list of set of control points defined on hbm
                Q_PROPERTY(QStringList hbmcontrolpoints READ hbmcontrolpoints NOTIFY hbmcontrolpointsChanged)

                Q_PROPERTY(piper::anthropometricmodel::AnthropoModelDisplay* anthroDisplay READ anthroDisplay)
                Q_PROPERTY(piper::kriging::Kriging* kriging READ kriging)

                Q_PROPERTY(AnthropoTreeProxy* anthropoTree READ anthropoTree NOTIFY anthropoModelChanged)

                //COMPONENT
                Q_PROPERTY(QStringList listComponents READ listComponents NOTIFY listComponentsChanged)
                Q_PROPERTY(QString currentComponent READ currentComponent WRITE setCurrentComponent NOTIFY currentComponentChanged)
                Q_PROPERTY(QString currentComponentType READ currentComponentType WRITE setCurrentComponentType NOTIFY currentComponentChanged)
                Q_PROPERTY(QString currentComponentParent READ currentComponentParent WRITE setCurrentComponentParent NOTIFY currentComponentChanged)
                Q_PROPERTY(double currentComponentPosition READ position WRITE setPosition NOTIFY currentComponentChanged)
                Q_PROPERTY(bool controlPointOnParent READ controlPointOnParent WRITE setControlPointOnParent NOTIFY currentComponentChanged)
                Q_PROPERTY(bool centerScaleOnParent READ centerScaleOnParent WRITE setCenterScaleOnParent NOTIFY currentComponentChanged)
                Q_PROPERTY(bool isDefinedFromParent READ isDefinedFromParent WRITE setIsDefinedFromParent NOTIFY currentComponentChanged)
                Q_PROPERTY(double useBone READ useBone WRITE setUseBone NOTIFY currentComponentChanged)
                Q_PROPERTY(double useSkin READ useSkin WRITE setUseSkin NOTIFY currentComponentChanged)

                // BODY SEGMENT
                Q_PROPERTY(QStringList listBodySegment READ listBodySegment NOTIFY listBodySegmentChanged)
                Q_PROPERTY(ModelviaPoint* viapoint READ viapoint NOTIFY currentComponentChanged)

                // BODY SECTION
				Q_PROPERTY(QStringList listBodysection READ listBodysection NOTIFY listBodysectionChanged)				
                Q_PROPERTY(ModelTable* entity READ entity NOTIFY currentComponentChanged)
                Q_PROPERTY(double anglerotationaxe READ anglerotationaxe WRITE setAnglerotationaxe NOTIFY currentComponentChanged)
                Q_PROPERTY(double angletilt READ angletilt WRITE setAngletilt NOTIFY currentComponentChanged)
                Q_PROPERTY(bool optionCenter READ optionCenter WRITE setOptionCenter NOTIFY currentComponentChanged)
                Q_PROPERTY(bool optionMinMaxWidth READ optionMinMaxWidth WRITE setOptionMinMaxWidth NOTIFY currentComponentChanged)
                Q_PROPERTY(bool optionMinMaxDepth READ optionMinMaxDepth WRITE setOptionMinMaxDepth NOTIFY currentComponentChanged)
                Q_PROPERTY(QString optionAngularPosition READ optionAngularPosition WRITE setOptionAngularPosition NOTIFY currentComponentChanged)

                // BODY DIMENSION
                Q_PROPERTY(ModelTableCombo* modelDimensionSegment READ modelDimensionSegment NOTIFY modelDimensionSegmentChanged)
                Q_PROPERTY(ModelTableCombo* modelDimensionSection READ modelDimensionSection NOTIFY modelDimensionSectionChanged)
                Q_PROPERTY(ModelAssBodySection* modelDimensionCompSection READ modelDimensionCompSection NOTIFY modelDimensionCompSectionChanged)
                Q_PROPERTY(ModelAssBodySection* modelDimensionCompSegment READ modelDimensionCompSegment NOTIFY modelDimensionCompSegmentChanged)
                Q_PROPERTY(QString currentBodyDim READ currentBodyDim WRITE setCurrentBodyDim NOTIFY currentBodyDimChanged)

				//source target
                Q_PROPERTY(BodyDimensionSourceTargetTable* bodySectionSourceTarget READ bodySectionSourceTarget NOTIFY bodySectionSourceTargetChanged)
                Q_PROPERTY(BodyDimensionSourceTargetTable* bodySegmentSourceTarget READ bodySegmentSourceTarget NOTIFY bodySegmentSourceTargetChanged)
                Q_PROPERTY(QString currentBodyDimTarget READ currentBodyDimTarget WRITE setCurrentBodyDimTarget NOTIFY currentBodyDimTargetChanged)

                /*!
                *  \brief double property for target height
                *
                */
                Q_PROPERTY(QString targetHeight READ targetHeight WRITE setTargetHeight NOTIFY targetHeightChanged)
                
        public:
            BodySectionPersonalizing();
            ~BodySectionPersonalizing();

            Q_INVOKABLE void InitModuleData();
            Q_INVOKABLE void importAnthropoModel(QUrl filename);
            Q_INVOKABLE void exportAnthropoModel(QUrl filename);
            Q_INVOKABLE bool isAnthropoModelDefined() const;
            Q_INVOKABLE void saveKrigingControlPoints(const QString &name);


			// get PROPERTY RELATED TO HBM in CONTEXT
			Q_INVOKABLE QStringList hbmlandmarks() const;
			Q_INVOKABLE QStringList hbmentities() const;
            Q_INVOKABLE QStringList hbmcontrolpoints() const;

            Q_INVOKABLE AnthropoTreeProxy* anthropoTree() const;
            Q_INVOKABLE QStringList listComponents() const;
            Q_INVOKABLE QString currentComponent() const;
            Q_INVOKABLE QString currentComponentType() const;
            Q_INVOKABLE QString currentComponentParent() const;
            Q_INVOKABLE void setCurrentComponent(QString const& componentname);
            Q_INVOKABLE void setCurrentComponentType(QString const& componenttype);
            Q_INVOKABLE void setCurrentComponentParent(QString const& componentparent);
            Q_INVOKABLE void setPosition(const double &value);
            Q_INVOKABLE void setControlPointOnParent(const bool &value);
            Q_INVOKABLE void setCenterScaleOnParent(const bool &value);
            Q_INVOKABLE void deleteComponent(QString const& sectionname);
            Q_INVOKABLE void deleteComponentAndChild(QString const& sectionname);
            Q_INVOKABLE bool renameComponent(QString const& oldname, QString const& newname);
            Q_INVOKABLE bool isDefinedFromParent() const;
            Q_INVOKABLE void setIsDefinedFromParent(const bool &value);
            Q_INVOKABLE bool newComponent(QString const& dimname, QString const& type);


            Q_INVOKABLE bool isCurrentSegment() const;
            Q_INVOKABLE bool isCurrentSection() const;


            //BODY SEGMENT
            Q_INVOKABLE QStringList listBodySegment();
            Q_INVOKABLE ModelviaPoint* viapoint() const;
            Q_INVOKABLE void setViapointCP(const QVariant &n, const bool &truefalse);
            Q_INVOKABLE void setLandmarkCP(const QVariant &n, const bool &truefalse);
            Q_INVOKABLE void addLandmarks(const QVariant &n, QString const& landname);
            Q_INVOKABLE void removeLandmarks(const QVariant &n, QString const& landname);
            Q_INVOKABLE void insertViapoint(const QVariant &n);
            Q_INVOKABLE void removeViapoint(const QVariant &n);

            //BODY SECTION
			Q_INVOKABLE QStringList listBodysection();
            Q_INVOKABLE void setOptionCenter(const QVariant &value);
            Q_INVOKABLE void setOptionMinMaxWidth(const QVariant &value);
			Q_INVOKABLE void setOptionMinMaxDepth(const QVariant &value);
			Q_INVOKABLE void setAnglerotationaxe(const double &value);
			Q_INVOKABLE void setAngletilt(const double &value);
            Q_INVOKABLE void setOptionAngularPosition(const QString &value);
            Q_INVOKABLE ModelTable* entity() const;
            Q_INVOKABLE void setEntity(QString const& nameBodysection, QString const& nameLand);
            Q_INVOKABLE void removeEntity(QString const& nameBodysection, QString const& nameLand);


            //Q_INVOKABLE ModelTableCombo* modelBodydim();
            Q_INVOKABLE ModelTableCombo* modelDimensionSegment();
            Q_INVOKABLE ModelTableCombo* modelDimensionSection();
            Q_INVOKABLE ModelAssBodySection* modelDimensionCompSegment();
            Q_INVOKABLE ModelAssBodySection* modelDimensionCompSection();
            Q_INVOKABLE void setCurrentBodyDim(QString const& dimname);
            Q_INVOKABLE void setCurrentBodyDimTarget(QString const& dimname);
            Q_INVOKABLE void setBodydimType(QString const& dimname, QString const& dimtype);
            Q_INVOKABLE bool setBodydimRef(QString const& dimname, QString const& dimref);
            Q_INVOKABLE void removeBodydimRef(QString const& dimname);
            Q_INVOKABLE bool renameBodydim(QString const& oldname, QString const& newname);
            Q_INVOKABLE void deleteBodydim(QString const& sectionname);
            Q_INVOKABLE bool newBodydim(QString const& sectionname, QString const& type);
            Q_INVOKABLE void setBodydimRelative(QString const& dimname, bool const& truefalse);


            Q_INVOKABLE void setAnthropoCompWeight(QString const& dimname, QString const& compname, const QVariant &weight = 1.0);
            Q_INVOKABLE bool addAnthropoComp(QString const& nameBodydim, QString const& compname, const QVariant &weight = 1.0);
            Q_INVOKABLE void removeAnthropoComp(QString const& nameBodydim, QString const& compname);
            Q_INVOKABLE QString currentBodyDim() const;
            Q_INVOKABLE QString currentBodyDimTarget() const;
            

            Q_INVOKABLE BodyDimensionSourceTargetTable* bodySectionSourceTarget();
            Q_INVOKABLE BodyDimensionSourceTargetTable* bodySegmentSourceTarget();
            Q_INVOKABLE static bool isSegmentDimensionType(QString const& type);


            // TARGETS
            Q_INVOKABLE void clearTargets();
            Q_INVOKABLE void loadTargets();
            
            /// import a target from file, stores it in project's target list and loads it into the bodysection's target list
            Q_INVOKABLE void loadTargetsFromFile(const QUrl& targetFile);
            Q_INVOKABLE void saveTargets() const;
            Q_INVOKABLE void setNewTargetValue(QString const& nameBodysection, const QVariant &value);
            Q_INVOKABLE void setTargetHeight(const QString &value);            
            Q_INVOKABLE QString getHbmHeight() const;
            Q_INVOKABLE QString getValidControlPointName() const;

            // KRIGING
            Q_INVOKABLE void setUseBone(const double &value);
            Q_INVOKABLE void setUseSkin(const double &value);
                       
            void setOperationSignals(bool moduleData, bool anthropomodel);
            void cleanupOperationSignals();
            static bool isSegmentDimensionType(anthropometricmodel::AnthropoDimensionType const& type);


        public slots:        

            // will update the control points of the underlying anthropo model (if they are not up to date). 
            void updateControlPoints();
            double useBone() const;
            double useSkin() const;
            bool optionCenter() const;
            bool optionMinMaxWidth() const;
            bool optionMinMaxDepth() const;
            QString optionAngularPosition() const;
			double position() const;
            bool controlPointOnParent() const;
            bool centerScaleOnParent() const;
            double anglerotationaxe() const;
            double angletilt() const;
            QString targetHeight() const;
            anthropometricmodel::AnthropoModelDisplay *anthroDisplay();    
            void updateCurrentSelection();
            kriging::Kriging *kriging();
            void updateHBMHeight();

        signals:
            void anthropoModelChanged();
            void anthropoModelCleared();
            void currentComponentChanged();
            void listComponentsChanged();
            void currentComponentDeleted();
           
			// signal PROPERTY RELATED TO HBM in CONTEXT
			void hbmlandmarksChanged();
			void hbmentitiesChanged();
            void hbmcontrolpointsChanged();
            // BODY SEGMENT
            void listBodySegmentChanged();
			// BODY SECTION
			void listBodysectionChanged();
            // BODY DIM
            void modelDimensionSegmentChanged();
            void modelDimensionSectionChanged();
            void currentBodyDimChanged();
            void currentBodyDimTargetChanged();
            void modelDimensionCompSectionChanged();
            void modelDimensionCompSegmentChanged();
            // TARGETS
            void bodySectionSourceTargetChanged();
            void bodySegmentSourceTargetChanged();
            void targetHeightChanged() const;

private:
            anthropometricmodel::AnthropoModel& m_anthropoModel;
            std::shared_ptr<anthropometricmodel::AnthropoModelDisplay> m_anthroDisplay;
            OPTION_SCALING m_option;
            piper::kriging::Kriging m_kriging;

            //member model PROPERTY RELATED TO HBM in CONTEXT
            ListModel* m_hbmlandmarks;
            ListModel* m_hbmentities;
            ListModel* m_hbmcontrolpoints;

            AnthropoTree* m_antrhopoTree;
            AnthropoTreeProxy* m_antrhopoTreeProxy;
            std::string m_currentComponent;
            ListModel* m_listBodySegment;
            ListModel* m_listBodysection;
            ModelTable* m_entity;
            ModelTableCombo* m_modelDimensionSegment;
            ModelTableCombo* m_modelDimensionSection;
            ModelviaPoint* m_viapoint;
            ModelAssBodySection* m_assBodycomponentSection;
            ModelAssBodySection* m_assBodycomponentSegment;
            //std::string m_currentBodydim;
            BodyDimensionSourceTargetTable* m_BodySectionSourceTarget;
            BodyDimensionSourceTargetTable* m_BodyLengthSourceTarget;
            bool m_showtarget;
            bool m_showtargetcontrolpoint;

            void doInitModuleData();
            void updateModuleGUI();
            void doSaveKrigingControlPoints(std::string const &name);



            void doImportAnthropoModel(std::string const& filename);
            void doExportAnthropoModel(std::string const& filename) const;
            void validateXml(std::string const& file);
            void doLoadTargetsFromFile(std::string const& targetFile);

            static QString angularPositiontoQstring(std::vector<double> const& values);
            static std::vector<double> angularPositiontoQstring(QString const& valuestr);
 
            static bool isSegmentDimensionType(std::string const& type);
        };


    }

} // namespace piper

Q_DECLARE_METATYPE(piper::anthropoPersonalizing::ModelTable*)
Q_DECLARE_METATYPE(piper::anthropoPersonalizing::ModelTableCombo*)
Q_DECLARE_METATYPE(piper::anthropoPersonalizing::BodyDimensionSourceTargetTable*)
//Q_DECLARE_METATYPE(piper::anthropoPersonalizing::BodyLengthSourceTargetTable*)
Q_DECLARE_METATYPE(piper::anthropoPersonalizing::ModelAssBodySection*)
Q_DECLARE_METATYPE(piper::anthropoPersonalizing::ListModel*)
Q_DECLARE_METATYPE(piper::anthropoPersonalizing::ModelviaPoint*)
