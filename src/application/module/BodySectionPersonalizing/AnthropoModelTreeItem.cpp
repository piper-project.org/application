/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoTreeItem.h"

#include "BodySectionPersonalizer/AnthropoModel.h"

using namespace piper::anthropometricmodel;

namespace piper {

    namespace anthropoPersonalizing {

#include <QStringList>

        AnthropoTreeItem::AnthropoTreeItem(const QList<QVariant> &data, anthropometricmodel::AnthropoModel const& anthropopmodel, AnthropoTreeItem *parent)
        {
            m_parentItem = parent;
            m_itemData = data;
            std::set<AbstractAnthropoComponentNode*> const& children = anthropopmodel.getAnthropoComponent(data.at(0).toString().toStdString())->getChildren();
            for (auto const& child : children) {
                QList<QVariant> curdata;
                curdata << QString::fromStdString(child->name());
                curdata << QString::fromStdString(AnthropoComponentType_str[(unsigned int)child->type()]);
                appendChild(new AnthropoTreeItem(curdata, anthropopmodel, this));
            }
        }

        AnthropoTreeItem::~AnthropoTreeItem()
        {
            qDeleteAll(m_childItems);
        }

        void AnthropoTreeItem::appendChild(AnthropoTreeItem *item)
        {
            m_childItems.append(item);
        }

        AnthropoTreeItem *AnthropoTreeItem::child(int row)
        {
            return m_childItems.value(row);
        }

        int AnthropoTreeItem::childCount() const
        {
            return m_childItems.count();
        }

        int AnthropoTreeItem::columnCount() const
        {
            return m_itemData.count();
        }

        QVariant AnthropoTreeItem::data(int column) const
        {
            return m_itemData.value(column);
        }

        AnthropoTreeItem *AnthropoTreeItem::parentItem()
        {
            return m_parentItem;
        }

        int AnthropoTreeItem::row() const
        {
            if (m_parentItem)
                return m_parentItem->m_childItems.indexOf(const_cast<AnthropoTreeItem*>(this));

            return 0;
        }

    }
}
