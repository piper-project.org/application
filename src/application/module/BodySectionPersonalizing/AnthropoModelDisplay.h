/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANTHROPOMODELDISPLAY_H
#define PIPER_ANTHROPOMODELDISPLAY_H

#include <QObject>

#include "BodySectionPersonalizer/AnthropoModel.h"
#include "common/MetaViewManager.h"
#include "common/KrigingDisplay.h"

const unsigned int ANTHROPO_COLOR_SOURCE_UNSELECTED = 0xFFFFFFFF;
const unsigned int ANTHROPO_COLOR_SOURCE_SELECTED_1 = 0xFFFF0000;
const unsigned int ANTHROPO_COLOR_SOURCE_SELECTED_2 = 0xFF33FF00;
const unsigned int ANTHROPO_COLOR_TARGET_UNSELECTED = 0xFF00CCFF;
const unsigned int ANTHROPO_COLOR_TARGET_SELECTED_1 = 0xFFFF6600;
const unsigned int ANTHROPO_COLOR_TARGET_SELECTED_2 = 0xFF33FF99;

const std::string COMPONENT = "component";
const std::string COMPONENT_TARGET = "componentTarget";
const std::string COMPONENT_FREECP = "freecontrolpoints";
const std::string COMPONENT_CP = "controlpoints";
const std::string COMPONENT_CP_TARGET = "controlpointsTarget";
const std::string COMPONENT_CP_HIGHLIGHT = "controlpointshighlight";
const std::string COMPONENT_HIGHLIGHT = "componenthighlight";
const std::string COMPONENT_LANDMARK = "componentlandmark";
const std::string COMPONENT_FRAME = "componentFrame";

namespace piper {
    namespace anthropometricmodel {


        /** This class used to display a anthropometric model
        *
        * \author Erwan Jolivet \date 2016
        */
        class AnthropoModelDisplay : public kriging::KrigingDisplay {
            Q_OBJECT

        public:
            AnthropoModelDisplay();
            ~AnthropoModelDisplay();


            Q_INVOKABLE void clearAnthropoModelDisplay(bool refreshDisplay);
            // set refreshDisplay to false if you are using more display*** calls in a row for better performance - refresh only on the last call
            Q_INVOKABLE void displaySourceSegment(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displaySourceSection(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displaySourceComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame=COMPONENT);
            Q_INVOKABLE void displaySourceControlPointsSegment(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displaySourceControlPointsSection(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displaySourceControlPointsComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_CP);

            Q_INVOKABLE void displayTargetSegment(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displayTargetSection(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displayTargetComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_TARGET);
            Q_INVOKABLE void displayTargetControlPointsSegment(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displayTargetControlPointsSection(bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void displayTargetControlPointsComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_CP_TARGET);

            Q_INVOKABLE void displayFreeLandmarksControlPoints(bool const& source, bool const& target, bool refreshDisplay);

            Q_INVOKABLE void highlightComponent(QString const& name, bool refreshDisplay);
            Q_INVOKABLE void highlightSourceSegment(QString const& name, bool refreshDisplay);
            Q_INVOKABLE void highlightSourceSegmentViaPoint(QString const& name, int const& n, bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void highlightSourceSegmentLandmark(QString const& name, bool refreshDisplay);
            Q_INVOKABLE void highlightSourceSection(QString const& name, bool refreshDisplay);
            //
            //           
            Q_INVOKABLE void updateHighlight(bool refreshDisplay=false);
            Q_INVOKABLE void highlightDimension(QString const& name, bool const& refreshDisplay);
            Q_INVOKABLE void highlightDimensionComponent(QString const& name, bool const& refreshDisplay);
            //
            Q_INVOKABLE void deleteComponent(QString const& name, bool const& refreshDisplay);

            void displaySourceComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT);
            void displaySourceControlPointsComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_CP);

            void displayTargetComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_TARGET);
            void displayTargetControlPointsComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame = COMPONENT_CP_TARGET);

            void highlightComponent(std::string const& name, bool const& refreshDisplay, bool const& subcompo = true);
            void highlightSourceSection(std::string const& name, bool const& refreshDisplay, bool const& subcompo = true);
            void highlightSourceSegment(std::string const& name, bool const& refreshDisplay, bool const& subcompo = true);
            void highlightDimension(std::string const& name, bool const& refreshDisplay);

        private:
            AnthropoModel& m_anthropomodel;
            //std::set<std::string> drawnLandmark;
            std::map<std::string, std::vector<std::string>> drawnHightlight;
            std::string highlightLandmark, highlightcomp, highlightDimensionComp;

            void AddGroupOfPoints(hbm::VCoord const& vcoord, std::string const& nameactor);
            void AddGroupOfPoints(Eigen::Matrix3Xd const& vcoord, std::string const& nameactor);
            void clearHighlight();
            void highlightSourceSegmentLandmark(std::string const& name, bool const& refreshDisplay);
            void highlightSourceSegmentViaPoint(std::string const& name, int const& n, bool const& visible, bool refreshDisplay);
            void highlightDimensionComponent(std::string const& name, bool const& refreshDisplay);

            VtkDisplayHighlighters drawer;
            QVariantList m_visibleLandmarks;
        };
    }
}

#endif // PIPER_ANTHROPOMODELDISPLAY_H
