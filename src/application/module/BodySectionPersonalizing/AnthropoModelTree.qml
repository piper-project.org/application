// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQml.Models 2.2

import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import piper.AnthropoModelDisplay 1.0

ModuleToolWindow
{
    title: "Simplified Scalable Model"
    //width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window
    property string currentComponent :""

    property BodySectionPersonalizing myBodySectionPersonalizing
    property AnthropoModelDisplay _myAnthropoModelDisplay
    property MessageDialog _warningDialog



    function getIndexFromName(name) {
        var indexes = mytree.model.getAllIndexes()
        for(var i = 0; i < indexes.length; i ++){
            var index = indexes[i]
            var curname = mytree.model.get(index).name
            if (curname === name)
                return index
        }
        return index
    }

    function expandAll(){
        var indexes = mytree.model.getAllIndexes()
        for(var i = 0; i < indexes.length; i ++){
            mytree.expand(indexes[i])
        }
    }


    Connections {
        target: myBodySectionPersonalizing
        onCurrentComponentChanged: {
            if (visible)
                window.currentComponent = myBodySectionPersonalizing.currentComponent
        }
    }
    onCurrentComponentChanged: {
        myBodySectionPersonalizing.setCurrentComponent(window.currentComponent)
        if (window.currentComponent!=="") {
            mytree.selection.setCurrentIndex(getIndexFromName(window.currentComponent), ItemSelectionModel.ClearAndSelect)
        }
        else {
            mytree.selection.clearCurrentIndex()
        }
    }

    onVisibleChanged: {
        if (!visible) {
            anthropoComponent.close()
            window.currentComponent = ""
        }
    }

    AnthropoComponent {
        id: anthropoComponent
    }

    Menu { id: contextMenuTreeView
        MenuItem {
            action: editComponent
        }
        MenuItem {
            action: deleteComponent
        }
        MenuItem {
            action: deleteComponentAndChild
        }
    }
    ColumnLayout {
        anchors.fill: parent
        anchors.margins : itemMargin
        Button {
            id: loadButton
            Layout.fillWidth : true
            action: importAnthropoModel
        }
        Button {
            id: exportButton
            Layout.fillWidth : true
            action: exportAction
        }
        Button {
            id: newCompButton
            Layout.fillWidth : true
            action: newComponent
        }

        TreeView {
            id: mytree
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: newCompButton.bottom
            Layout.fillWidth : true
            Layout.fillHeight: true
//            height: 320

//            anchors.margins : itemMargin
            TableViewColumn {
                title: "Name"
                role: "name"
                width: 200
            }
            TableViewColumn {
                title: "Type"
                role: "type"
                width: 100
            }
            model: myBodySectionPersonalizing.anthropoTree
            onModelChanged: {
                    expandAll()
                    if (myBodySectionPersonalizing.currentComponent!=="") {
                        window.currentComponent = myBodySectionPersonalizing.currentComponent
                        mytree.selection.setCurrentIndex(getIndexFromName(window.currentComponent), ItemSelectionModel.ClearAndSelect)
                    }
                    else {
                        mytree.selection.clearCurrentIndex()
                    }
            }
            selectionMode: SelectionMode.SingleSelection
            selection: ItemSelectionModel {
                model: mytree.model
                onCurrentChanged: {
                        if (currentIndex.valid) {
                            window.currentComponent = model.get(currentIndex).name
                        }
                        else {
                            window.currentComponent = ""
                        }
                }
            }
            onDoubleClicked: isExpanded(index) ? collapse(index) : expand(index)
            itemDelegate: Item {
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    color: {
                        mytree.model.get(styleData.index).defined ? "black" : "red"
                    }
                    text: styleData.value
                }
            }
            rowDelegate: Item {
                Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height
                    color: styleData.selected ? 'lightblue' : 'white'
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        propagateComposedEvents: true
                        onReleased: {
                            if (styleData.selected) {
                                if (mouse.button === Qt.RightButton) {
                                    contextMenuTreeView.popup()
                                }//if
                            }//if
                            mouse.accepted = false
                        }//onReleased
                    }//MouseArea
                }//Rectangle
            }//rowDelegate
        }

        Action {
            id: newComponent
            text: qsTr("New Component")
            onTriggered:
                nameComponentDialog.open()
            tooltip: "Define a new component (segment or section)."
        }

        Dialog {
            id: nameComponentDialog
            title: qsTr("Name of the new Component")
            standardButtons: StandardButton.Ok | StandardButton.Cancel
            property bool spy
            onAccepted: {
                if (newname.text.length > 0) {
                    spy = myBodySectionPersonalizing.newComponent(newname.text, newtypeCombo.textAt(newtypeCombo.currentIndex))
                    if (!spy)
                        _warningDialog.open()
                }
            }
            onVisibleChanged: {
                if (visible == true) {
                    newname.text=""
                }
            }
            RowLayout {
                width: parent.width
                TextField {
                    id: newname
                    Layout.fillWidth: true
                    focus: true
                    text: ""
                }
                ComboBox {
                    id: newtypeCombo
                    Layout.fillWidth: true
                    model: [ "SEGMENT", "SECTION" ]
                    Component.onCompleted: width=120
                }

            }
            Component.onCompleted: {
                width=500;
                newname.text=""
                newtypeCombo.currentIndex=0
            }
        }

    }
    Component.onCompleted: {
        adjustWindowSizeToContent(20, 20)
        height += 240;
        width += 200;
        anthropoComponent.myBodySectionPersonalizing = myBodySectionPersonalizing
        anthropoComponent._myAnthropoModelDisplay = _myAnthropoModelDisplay
        anthropoComponent._warningDialog = _warningDialog
    }


    Action {
        id: editComponent
        text: qsTr("Edit selected component")
        onTriggered:
            anthropoComponent.show()
    }
    Action {
        id: deleteComponent
        text: qsTr("Delete selected component")
        onTriggered: {
            myBodySectionPersonalizing.deleteComponent(window.currentComponent)
        }
    }
    Action {
        id: deleteComponentAndChild
        text: qsTr("Delete component and child")
        onTriggered:
            myBodySectionPersonalizing.deleteComponentAndChild(window.currentComponent)
    }




    Action {
        id: importAnthropoModel
        text: qsTr("Import Simplified Scalable Model")
        tooltip: qsTr("Import Simplified Scalable Model for a xml file.\n If one is alreday loaded, it will be deleted.")
        onTriggered: {
            openBodySectionDialog.open()
        }
    }
    FileDialog {
        id: openBodySectionDialog
        title: qsTr("Import Simplified Scalable Model")
        nameFilters: [ "xml files (*.xml)" ]
        onAccepted:{
            myBodySectionPersonalizing.importAnthropoModel(openBodySectionDialog.fileUrl)
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentAnthroModelImportFolder = folder
        }
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentAnthroModelImportFolder))
                folder = settings.recentAnthroModelImportFolder
        }
    }
    Action {
        id: exportAction
        text: qsTr("Export Simplified Scalable Model")
        tooltip: qsTr("Export Simplified Scalable Model in a xml file.")
        onTriggered: exportBodySectionDialog.open()
    }

    FileDialog {
        id: exportBodySectionDialog
        title: qsTr("Export Simplified Scalable Model")
        selectExisting: false
        nameFilters: [ "xml files (*.xml)" ]
        onAccepted:{
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentAnthroModelImportFolder = folder
            myBodySectionPersonalizing.exportAnthropoModel(exportBodySectionDialog.fileUrl)
        }
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentAnthroModelImportFolder))
                folder = settings.recentAnthroModelImportFolder
        }
    }

    Settings
    {
        id: settings
        category: "application"
        property url recentAnthroModelImportFolder
    }

}



