/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoModelDisplay.h"

#include "hbm/types.h"

#include "common/Context.h"

#include <vtkPointData.h>

namespace piper {
    namespace anthropometricmodel {


        AnthropoModelDisplay::AnthropoModelDisplay()
            : m_anthropomodel(Context::instance().project().anthropoModel())
        {
            m_display = &Context::instance().display();
            m_display->ActivateDisplayMode(DISPLAY_MODE::ENTITIES | DISPLAY_MODE::AXES | DISPLAY_MODE::LANDMARKS);
        }

        AnthropoModelDisplay::~AnthropoModelDisplay() {
        }
        
        void AnthropoModelDisplay::clearAnthropoModelDisplay(bool refreshDisplay) {
            drawnHightlight.clear();
            highlightLandmark.clear(); highlightcomp.clear(); highlightDimensionComp.clear();
            m_visibleLandmarks.clear();
            deleteModuleActors();
        }


        void AnthropoModelDisplay::displaySourceComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            displaySourceComponent(name.toStdString(), visible, refreshDisplay, trame);
        }

        void AnthropoModelDisplay::displaySourceComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            if (!visible) {
                m_display->RemoveActor(trame + name);
            }
            else {
                displaySourceComponent(name, false, false, trame);
                if (m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                    Eigen::Matrix3Xd const& segcoord = m_anthropomodel.getAnthropoComponent(name)->getComponentCoord(phbm);
                    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                    points->SetNumberOfPoints(segcoord.cols());
                    for (Eigen::Matrix3Xd::Index n = 0; n < segcoord.cols(); n++)
                        points->SetPoint(n, segcoord(0, n), segcoord(1, n), segcoord(2, n));
                    std::string nameactor = trame + name;
                    m_display->AddActor(drawer.polyLine(points), nameactor, false, DISPLAY_MODE::LANDMARKS, false); // maybe need a different display mode for contours
                }
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::displaySourceSection(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysections = m_anthropomodel.listAnthropoBodySection();
            if (!visible) {
                for (auto const& cur : listBodysections)
                    m_display->RemoveActor(COMPONENT+cur);
            }
            else {
                displaySourceSection(false, false);
                if (listBodysections.size() > 0) {
                    for (auto const& cur : listBodysections) {
                        displaySourceComponent(QString::fromStdString(cur), visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displaySourceSegment(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysegment = m_anthropomodel.listAnthropoBodySegment();
            if (!visible) {
                for (auto const& cur : listBodysegment)
                    m_display->RemoveActor(COMPONENT+cur);
            }
            else {
                displaySourceSegment(false, false);
                if (listBodysegment.size() > 0) {
                    for (auto const& cur : listBodysegment) {
                        displaySourceComponent(QString::fromStdString(cur), visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displaySourceControlPointsComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            displaySourceControlPointsComponent(name.toStdString(), visible, refreshDisplay, trame);
        }

        void AnthropoModelDisplay::displaySourceControlPointsComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            if (!visible) {
                m_display->RemoveActor(trame + name);
            }
            else {
                displaySourceControlPointsComponent(name, false, false, trame);
                if (m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                    Eigen::Matrix3Xd const& vcoord = m_anthropomodel.getAnthropoComponent(name)->getComponentControlPoints(phbm);
                   AddGroupOfPoints(vcoord, trame + name);
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayFreeLandmarksControlPoints(bool const& source, bool const& target, bool refreshDisplay) {
            std::map<std::string, piper::hbm::Coord> const& sourceland = m_anthropomodel.getFreeLandmarksSource();
            std::map<std::string, piper::anthropometricmodel::Landmarkconstraint> const& targetland = m_anthropomodel.getFreeLandmarksTargets();

            if (!source) {
                m_display->RemoveActor(COMPONENT_FREECP + "_source");
            }
            if (!target) {
                m_display->RemoveActor(COMPONENT_FREECP + "_target");
            }
            if (target || source) {
                displayFreeLandmarksControlPoints(false, false, false);
                if (source && sourceland.size() > 0) {
                    Eigen::Matrix3Xd vcoord;
                    vcoord.resize(3, sourceland.size());
                    size_t n = 0;
                    for (auto const& cur : sourceland) {
                        vcoord.col(n) = cur.second;
                        n++;
                    }
                    AddGroupOfPoints(vcoord, COMPONENT_FREECP + "_source");
                }
                if (target && targetland.size() > 0) {
                    Eigen::Matrix3Xd vcoord;
                    vcoord.resize(3, targetland.size());
                    size_t n = 0;
                    for (auto const& cur : targetland) {
                        vcoord.col(n) = cur.second.coord;
                        n++;
                    }
                    AddGroupOfPoints(vcoord, COMPONENT_FREECP + "_target");
                    m_display->SetObjectColor(COMPONENT_FREECP + "_target", ANTHROPO_COLOR_TARGET_UNSELECTED);
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }


        void AnthropoModelDisplay::displaySourceControlPointsSection(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const listBodysections = m_anthropomodel.listAnthropoBodySection();
            if (!visible) {
                for (auto const& cur : listBodysections) {
                    m_display->RemoveActor(COMPONENT_CP + cur);
                }
            }
            else {
                displaySourceControlPointsSection(false, false);
                if (listBodysections.size() > 0) {
                    for (auto const& cur : listBodysections) {
                        displaySourceControlPointsComponent(cur, visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displaySourceControlPointsSegment(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysegment = m_anthropomodel.listAnthropoBodySegment();
            if (!visible) {
                for (auto const& cur : listBodysegment) {
                    m_display->RemoveActor(COMPONENT_CP+ cur);
                }
            }
            else {
                displaySourceControlPointsSegment(false, false);
                if (listBodysegment.size() > 0) {
                    for (auto const& cur : listBodysegment) {
                        displaySourceControlPointsComponent(cur, visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayTargetSegment(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysegment = m_anthropomodel.listAnthropoBodySegment();
            if (!visible) {
                for (auto const& cur : listBodysegment)
                    m_display->RemoveActor(COMPONENT_TARGET+cur);
            }
            else {
                displayTargetSegment(false, false);
                if (listBodysegment.size() > 0) {
                    for (auto const& cur : listBodysegment) {
                        displayTargetComponent(QString::fromStdString(cur), visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayTargetSection(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysections = m_anthropomodel.listAnthropoBodySection();
            if (!visible) {
                for (auto const& cur : listBodysections)
                    m_display->RemoveActor(COMPONENT_TARGET+cur);
            }
            else {
                displayTargetSection(false, false);
                if (listBodysections.size() > 0) {
                    for (auto const& cur : listBodysections) {
                        displayTargetComponent(QString::fromStdString(cur), visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayTargetComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            displayTargetComponent(name.toStdString(), visible, refreshDisplay, trame);
        }

        void AnthropoModelDisplay::displayTargetComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            if (!visible) {
                m_display->RemoveActor(trame + name);
            }
            else {
                displayTargetComponent(name, false, false, trame);
                if (m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                    Eigen::Matrix3Xd const& segcoord = m_anthropomodel.getAnthropoComponent(name)->getComponentCoordTarget(phbm);
                    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                    points->SetNumberOfPoints(segcoord.cols());
                    for (Eigen::Matrix3Xd::Index n = 0; n < segcoord.cols(); n++)
                        points->SetPoint(n, segcoord(0, n), segcoord(1, n), segcoord(2, n));
                    std::string nameactor = trame + name;
                    m_display->AddActor(drawer.polyLine(points), nameactor, false, DISPLAY_MODE::LANDMARKS, false); // maybe need a different display mode for contours
                    m_display->SetObjectColor(nameactor, ANTHROPO_COLOR_TARGET_UNSELECTED);
                }
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::displayTargetControlPointsSegment(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const& listBodysegment = m_anthropomodel.listAnthropoBodySegment();
            if (!visible) {
                for (auto const& cur : listBodysegment) {
                    m_display->RemoveActor(COMPONENT_CP_TARGET + cur);
                }
            }
            else {
                displayTargetControlPointsSegment(false, false);
                if (listBodysegment.size() > 0) {
                    for (auto const& cur : listBodysegment) {
                        displayTargetControlPointsComponent(cur, visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayTargetControlPointsSection(bool const& visible, bool refreshDisplay) {
            std::set<std::string> const listBodysections = m_anthropomodel.listAnthropoBodySection();
            if (!visible) {
                for (auto const& cur : listBodysections) {
                    m_display->RemoveActor(COMPONENT_CP_TARGET + cur);
                }
            }
            else {
                displayTargetControlPointsSection(false, false);
                if (listBodysections.size() > 0) {
                    for (auto const& cur : listBodysections) {
                        displayTargetControlPointsComponent(cur, visible, false);
                    }
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::displayTargetControlPointsComponent(QString const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            displayTargetControlPointsComponent(name.toStdString(), visible, refreshDisplay, trame);
        }

        void AnthropoModelDisplay::displayTargetControlPointsComponent(std::string const& name, bool const& visible, bool refreshDisplay, std::string const& trame) {
            if (!visible) {
                m_display->RemoveActor(trame + name);
            }
            else {
                displayTargetControlPointsComponent(name, false, false, trame);
                hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                if (m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    Eigen::Matrix3Xd const& vcoord = m_anthropomodel.getAnthropoComponent(name)->getComponentControlPointsTarget(phbm);
                    AddGroupOfPoints(vcoord, trame + name);
                    m_display->SetObjectColor(trame + name, ANTHROPO_COLOR_TARGET_UNSELECTED);
                }
            }
            if (refreshDisplay) {
                updateHighlight();
                m_display->Refresh();
            }
        }

        void AnthropoModelDisplay::highlightComponent(QString const& name, bool refreshDisplay) {
            highlightComponent(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightComponent(std::string const& name, bool const& refreshDisplay, bool const& subcompo) {
            if (m_anthropomodel.hasAnthropoBodySegment(name))
                highlightSourceSegment(name, refreshDisplay);
            else if (m_anthropomodel.hasAnthropoBodySection(name))
                highlightSourceSection(name, refreshDisplay);
            else {
                if (refreshDisplay) {
                    clearHighlight();
                    highlightcomp.clear();
                    m_display->Refresh();
                }
                return;
            }
        }

        void AnthropoModelDisplay::highlightSourceSegment(QString const& name, bool refreshDisplay) {
            highlightSourceSegment(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightSourceSegment(std::string const& name, bool const& refreshDisplay, bool const& subcompo) {
            if (subcompo) {
                highlightcomp = name;
                clearHighlight();
            }
            if (name.size() > 0) {
                // highlight new segment
                m_display->SetActorVisible(COMPONENT + name, false);
                if (!m_display->HasActor(COMPONENT_HIGHLIGHT + name))
                    displaySourceComponent(name, true, false, COMPONENT_HIGHLIGHT);
                m_display->SetObjectColor(COMPONENT_HIGHLIGHT + name, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                drawnHightlight[name].push_back(COMPONENT_HIGHLIGHT + name);
                m_display->SetObjectColor(COMPONENT_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_1);
                m_display->SetObjectColor(COMPONENT_CP_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_1);
                if (subcompo && m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                    // display section frame
                    double const* tmp = m_anthropomodel.getAnthropoComponent(name)->getLocalFrameMatrix(phbm);
                    double tmp_transpose[16];
                    for (int i = 0; i < 4; ++i) {
                        for (int j = 0; j < 4; ++j) {
                            double value = tmp[i + j * 4];
                            tmp_transpose[j + i * 4] = value;
                        }
                    }
                    drawnHightlight[name].push_back(COMPONENT_FRAME);
                    m_display->AddAxesActor(drawer.frame(tmp_transpose, 50.0), COMPONENT_FRAME, false);
                    ////draw landmark
                    //std::vector<std::set<std::string>> const& landmarks = m_anthropomodel.getAnthropoBodySegment(name)->getLandmarks();
                    //QVariantList landvisibility;
                    //for (auto const& curset : landmarks) {
                    //    for (auto const& curland : curset) {
                    //        m_visibleLandmarks.append(QString::fromStdString(curland));
                    //        std::cout << "add visible landmark " << curland << std::endl;
                    //        landvisibility.append(true);
                    //    }
                    //}
                    //Context::instance().metaManager().landmarkDisplayList(m_visibleLandmarks, landvisibility, false);
                    // display control points
                    Eigen::Matrix3Xd const& points = m_anthropomodel.getAnthropoBodySegment(name)->getComponentControlPoints(phbm);
                    if (points.cols() > 0) {
                        std::string nameactor = COMPONENT_CP_HIGHLIGHT;
                        drawnHightlight[name].push_back(nameactor);
                        AddGroupOfPoints(points, nameactor);
                        m_display->SetObjectColor(nameactor, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                    }
                }
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::highlightSourceSegmentViaPoint(QString const& name, int const& n, bool const& visible, bool refreshDisplay) {
            highlightSourceSegmentViaPoint(name.toStdString(), n, visible, refreshDisplay);
        }

        void AnthropoModelDisplay::highlightSourceSegmentViaPoint(std::string const& name, int const& n, bool const& visible, bool refreshDisplay) {
            // remove old ones
            if (m_visibleLandmarks.size() > 0) {
                QVariantList landvisibility;
                for (auto const& curland : m_visibleLandmarks) {
                    landvisibility.append(false);
                }
                Context::instance().metaManager().landmarkDisplayList(m_visibleLandmarks, landvisibility, false);
                m_visibleLandmarks.clear();
            }
            //draw landmark
            QVariantList landvisibility;
            if (n > -1) {
                std::vector<std::set<std::string>> const& landmarks = m_anthropomodel.getAnthropoBodySegment(name)->getLandmarks();
                //for (auto const& curset : landmarks) {
                if (landmarks[n].size() > 0) {
                    for (auto const& curland : landmarks[n]) {
                        m_visibleLandmarks.append(QString::fromStdString(curland));
                        landvisibility.append(true);
                    }
                }
            }
            //}
            Context::instance().metaManager().landmarkDisplayList(m_visibleLandmarks, landvisibility, false);
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::highlightSourceSegmentLandmark(QString const& name, bool refreshDisplay) {
            highlightSourceSegmentLandmark(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightSourceSegmentLandmark(std::string const& name, bool const& refreshDisplay) {
            // un highlight previous one
            if (highlightLandmark.size() > 0) {
                m_display->SetObjectColor(highlightLandmark, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                highlightLandmark.clear();
            }
            if (name.size() > 0) {
                highlightLandmark = COMPONENT_LANDMARK + name;
                m_display->SetObjectColor(highlightLandmark, ANTHROPO_COLOR_SOURCE_SELECTED_2);
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::highlightSourceSection(QString const& name, bool refreshDisplay) {
            highlightSourceSection(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightSourceSection(std::string const& name, bool const& refreshDisplay, bool const& subcompo) {
            if (subcompo) {
                clearHighlight();
                highlightcomp = name;
            }
            if (name.size() > 0) {
                hbm::HumanBodyModel *const phbm = &Context::instance().project().model();
                // highlight section
                m_display->SetActorVisible(COMPONENT + name, false);
                if (!m_display->HasActor(COMPONENT_HIGHLIGHT + name))
                    displaySourceComponent(name, true, false, COMPONENT_HIGHLIGHT);
                m_display->SetObjectColor(COMPONENT_HIGHLIGHT + name, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                drawnHightlight[name].push_back(COMPONENT_HIGHLIGHT + name);
                m_display->SetObjectColor(COMPONENT_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_1);
                m_display->SetObjectColor(COMPONENT_CP_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_1);
                if (subcompo && m_anthropomodel.getAnthropoComponent(name)->isDefined()) {
                    //highlight ref segment
                    std::string segname = m_anthropomodel.getAnthropoComponent(name)->getParent()->name();
                    if (segname.size() > 0) {
                        highlightSourceSegment(segname, false, false);
                    }
                    // display section frame
                    double const* tmp = m_anthropomodel.getAnthropoComponent(name)->getLocalFrameMatrix(phbm);
                    double tmp_transpose[16];
                    for (int i = 0; i < 4; ++i) {
                        for (int j = 0; j < 4; ++j) {
                            double value = tmp[i + j * 4];
                            tmp_transpose[j + i * 4] = value;
                        }
                    }
                    drawnHightlight[name].push_back(COMPONENT_FRAME);
                    m_display->AddAxesActor(drawer.frame(tmp_transpose, 50.0), COMPONENT_FRAME, false);
                    // display control points
                    Eigen::Matrix3Xd const& points = m_anthropomodel.getAnthropoBodySection(name)->getComponentControlPoints(phbm);
                    if (points.cols() > 0) {
                        std::string nameactor = COMPONENT_CP_HIGHLIGHT;
                        drawnHightlight[name].push_back(nameactor);
                        AddGroupOfPoints(points, nameactor);
                        m_display->SetObjectColor(nameactor, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                    }
                }
            }
            if (refreshDisplay)
                m_display->Refresh();

        }

        void AnthropoModelDisplay::AddGroupOfPoints(Eigen::Matrix3Xd const& vcoord, std::string const& nameactor) {
            hbm::VCoord vectorcoord;
            for (Eigen::Matrix3Xd::Index n = 0; n < vcoord.cols(); n++)
                vectorcoord.push_back(vcoord.col(n));
            AddGroupOfPoints(vectorcoord, nameactor);
        }

        void AnthropoModelDisplay::AddGroupOfPoints(hbm::VCoord const& vcoord, std::string const& nameactor) {
            if (vcoord.size() > 0) {
                // create a vtk polydata for cotrol points
                vtkSmartPointer<vtkPolyData> controlPointMesh = vtkSmartPointer<vtkPolyData>::New();
                vtkSmartPointer<vtkPoints> controlPointPoints = vtkSmartPointer<vtkPoints>::New();
                controlPointPoints->Allocate(vcoord.size());
                controlPointMesh->SetPoints(controlPointPoints);
                for (auto const& cur : vcoord)
                    controlPointMesh->GetPoints()->InsertNextPoint(cur[0], cur[1], cur[2]);
                m_display->AddGroupOfPoints(controlPointMesh, nameactor, false, m_display->POINTHIGHLIGHT_RADIUS, "", DISPLAY_MODE::LANDMARKS, false, false);
            }

        }

        void AnthropoModelDisplay::updateHighlight(bool refreshDisplay) {
            clearHighlight();
            if (highlightcomp.size() > 0) {
                if (m_anthropomodel.hasAnthropoBodySection(highlightcomp))
                    highlightSourceSection(highlightcomp, true);
                else if (m_anthropomodel.hasAnthropoBodySegment(highlightcomp))
                    highlightSourceSegment(highlightcomp, true);
                else
                    highlightDimension(highlightcomp, true);
            }
            if (m_visibleLandmarks.size() > 0) {
                highlightSourceSegmentLandmark(highlightLandmark, true);
            }
            if (highlightDimensionComp.size() > 0)
                highlightDimensionComponent(highlightDimensionComp, true);
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::clearHighlight() {
            if (m_visibleLandmarks.size() > 0) {
                QVariantList landvisibility;
                for (auto const& curland : m_visibleLandmarks) {
                    landvisibility.append(false);
                }
                Context::instance().metaManager().landmarkDisplayList(m_visibleLandmarks, landvisibility, false);
                m_visibleLandmarks.clear();
            }
            if (drawnHightlight.size() > 0) {
                for (std::map<std::string, std::vector<std::string>>::iterator it = drawnHightlight.begin(); it != drawnHightlight.end(); ++it) {
                    for (auto const& curactor : it->second)
                        m_display->RemoveActor(curactor);
                    if (m_anthropomodel.hasAnthropoComponent(it->first)) {
                        m_display->SetObjectColor(COMPONENT_TARGET + it->first, ANTHROPO_COLOR_TARGET_UNSELECTED);
                        m_display->SetActorVisible(COMPONENT + it->first, true);
                    }
                }
                drawnHightlight.clear();
            }
        }

        void AnthropoModelDisplay::highlightDimension(QString const& name, bool const& refreshDisplay) {
            highlightDimension(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightDimension(std::string const& name, bool const& refreshDisplay) {
            clearHighlight();
            highlightcomp = name;
            if (name.size() > 0) {
                if (m_anthropomodel.getAnthropoBodyDimension(name)->isDefined()) {
                    std::string const& ref = m_anthropomodel.getAnthropoBodyDimension(name)->getReferenceAnthropoComponent();
                    if (m_anthropomodel.hasAnthropoBodySegment(ref)) {
                        highlightSourceSegment(ref, false, false);
                    }
                    else {
                        highlightSourceSection(ref, false, false);
                    }
                    // 
                    std::map<anthropometricmodel::AbstractAnthropoComponentNode*, double> const& listcomp = m_anthropomodel.getAnthropoBodyDimension(name)->getAnthropoComponent();
                    for (auto const& cur : listcomp) {
                        if (ref != cur.first->name()) {
                            if (m_anthropomodel.hasAnthropoBodySegment(cur.first->name())) {
                                highlightSourceSegment(cur.first->name(), false, false);
                            }
                            else {
                                highlightSourceSection(cur.first->name(), false, false);
                            }
                        }
                    }
                }
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::highlightDimensionComponent(QString const& name, bool const& refreshDisplay) {
            highlightDimensionComponent(name.toStdString(), refreshDisplay);
        }

        void AnthropoModelDisplay::highlightDimensionComponent(std::string const& name, bool const& refreshDisplay) {
            // un highlight previous one
            if (highlightDimensionComp.size() > 0) {
                m_display->SetObjectColor(highlightDimensionComp, ANTHROPO_COLOR_SOURCE_SELECTED_1);
                m_display->SetObjectColor(COMPONENT_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_1);
                highlightDimensionComp.clear();
            }
            if (name.size() > 0) {
                highlightDimensionComp = COMPONENT_HIGHLIGHT + name;
                m_display->SetObjectColor(highlightDimensionComp, ANTHROPO_COLOR_SOURCE_SELECTED_2);
                m_display->SetObjectColor(COMPONENT_TARGET + name, ANTHROPO_COLOR_TARGET_SELECTED_2);
            }
            if (refreshDisplay)
                m_display->Refresh();
        }

        void AnthropoModelDisplay::deleteComponent(QString const& name, bool const& refreshDisplay) {

            //delete all modules actors
            m_display->RemoveNonpersistentActors();
            clearHighlight();
            ////drawnLandmark.clear();
            //drawnHightlight.clear();
            //highlightLandmark.clear();
            //highlightcomp.clear();
            //highlightDimensionComp.clear();

        }

    }
}
