// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5
import QtQuick.Layouts 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import VtkQuick 1.0
import "../kriging"



ModuleLayout
{
    id: root
    content: DefaultVtkViewer {}
    toolbar:ColumnLayout 
	{
        anchors.fill: parent

        ExclusiveGroup
        {
            id: moduletoolsGroup
        }


        GroupBox
        {
            id: targettmodule
            title: qsTr("Targets")
            anchors.top: parent.top
            Layout.fillWidth: true
            ColumnLayout {
                anchors.fill: parent
                ModuleToolWindowButton {
                    id: targetDimensionButton
                    text:"\&Target Dimensions"
                    shortcutChar: 'T'
                    toolWindow: targetDimension
                    exclusiveGroup: moduletoolsGroup
                    tooltip: "Define Anthropoometric dimension target values."
                }
                Button {
                    id: savetargetButton
                    anchors.left: parent.left
                    anchors.right: parent.right
                    action: savetargetAction
                }
            }
        }
        GroupBox
        {
            id: moduletools
            title: qsTr("Module Tools")
            anchors.top: targettmodule.bottom
            Layout.fillWidth: true
            ColumnLayout {
                anchors.fill: parent
                Layout.fillWidth: true
                ModuleToolWindowButton {
                    id: modeltreeButton
                    text:"Scalable Model"
                    shortcutChar: 'M'
                    toolWindow: anthropoModelTree
                    exclusiveGroup: moduletoolsGroup
                    tooltip: "Tool to create, edit, import Scalable Model (.xml)."
                }
                ModuleToolWindowButton {
                    id: dimensionButton
                    text:"Link Dimensions to Sections"
                    shortcutChar: 'L'
                    toolWindow: bodyDimension
                    exclusiveGroup: moduletoolsGroup
                    tooltip: "Tool to create, edit dimensions on model."
                }
                ModuleToolWindowButton {
                    id: targetbodydimButton
                    text:"Adjust Target Dimensions"
                    shortcutChar: 'T'
                    toolWindow: persoSourceTargetDialog
                    exclusiveGroup: moduletoolsGroup
                    tooltip: "Tool to edit current dimension values on model and target values."
                }
                ModuleToolWindowButton {
                    id: krigingdeformationButton
                    text:"Perform Deformation"
                    shortcutChar: 'P'
                    toolWindow: krigingdeformation
                    exclusiveGroup: moduletoolsGroup
                    tooltip: "Tool to apply, save the transformation."
                }
                Button {
                    id: saveControlPointsButton
                    anchors.left: parent.left
                    anchors.right: parent.right
                    action: saveControlPointsAction
                }

            }
        }
        AnthropoModelDisplayGUI {
			anchors.top: moduletools.bottom
            id: myAnthropoModelDisplayGUI
            Component.onCompleted:
            {
                myAnthropoModelDisplayGUI.anthropoModelDisplay = myBodySectionPersonalizing.anthroDisplay
            }
            Connections {
                target: myBodySectionPersonalizing
                onAnthropoModelChanged: {
                    if (visible) {
                        myAnthropoModelDisplayGUI.refresh()
                    }
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onAnthropoModelCleared: {
                    if (visible)
                        myAnthropoModelDisplayGUI.clearAnthropoModelDisplay()
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onCurrentComponentChanged: {
                    if (visible)
                        myAnthropoModelDisplayGUI.anthropoModelDisplay.highlightComponent(myBodySectionPersonalizing.currentComponent,true)
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onCurrentComponentDeleted: {
                    if (visible)
                        myAnthropoModelDisplayGUI.anthropoModelDisplay.deleteComponent(myBodySectionPersonalizing.currentComponent,true)
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onCurrentBodyDimChanged: {
                    if (visible)
                        myAnthropoModelDisplayGUI.anthropoModelDisplay.highlightDimension(myBodySectionPersonalizing.currentBodyDim,true)
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onCurrentBodyDimTargetChanged: {
                    if (visible)
                        myAnthropoModelDisplayGUI.anthropoModelDisplay.highlightDimension(myBodySectionPersonalizing.currentBodyDim,true)
                }
            }
        }
		ColumnLayout
		{
			DetachableVtkViewerCommonTools
			{
				id: detachableVtkViewerCommonTools
			}
		}

        AnthropoModelTree {
            id: anthropoModelTree
        }
        BodyDimension {
            id: bodyDimension
        }
        TargetDimension {
            id: targetDimension
        }
        Krigingdeformation {
            id: krigingdeformation
            allowIntermediate: true
        }
        Action {
            id: savetargetAction
            text: qsTr("Save Target")
            onTriggered: myBodySectionPersonalizing.saveTargets()
            tooltip: "Save current target values for anthropometric dimensions in a new list of anthropometric targets (deleting previous one)."
        }
        

        ControlPointsNameDialog {
            id: myControlPointsDialog
            text: myBodySectionPersonalizing.getValidControlPointName()
            onAccepted: {
                krigingAutoLoaControlPointsName = text
                myBodySectionPersonalizing.saveKrigingControlPoints(text)
            }
            onRejected: krigingAutoLoaControlPointsName = "none"
            onVisibleChanged: {
                if (visible) {
                    text = myBodySectionPersonalizing.getValidControlPointName()
                }
            }
        }

        Action {
            id: saveControlPointsAction
            text: qsTr("Save Control Points")
            onTriggered: {
                myControlPointsDialog.open()
            }
        }

        PersoSourceTargetDialog {
            id: persoSourceTargetDialog
            showtarget: true
        }

        MessageDialog {
            id: warningDialog
            title: "WARNING:"
            icon: StandardIcon.Warning
            text: "This name already exist."
            standardButtons: StandardButton.Ok
        }
        Item {
            BodySectionPersonalizing {
                id: myBodySectionPersonalizing
            }
            Component.onCompleted: {
                krigingdeformation.krigingIntrfc = myBodySectionPersonalizing.kriging
                anthropoModelTree.myBodySectionPersonalizing = myBodySectionPersonalizing
                targetDimension.myBodySectionPersonalizing = myBodySectionPersonalizing
                bodyDimension.myBodySectionPersonalizing = myBodySectionPersonalizing
                bodyDimension._myAnthropoModelDisplay = myBodySectionPersonalizing.anthroDisplay
                persoSourceTargetDialog.myBodySectionPersonalizing = myBodySectionPersonalizing
                persoSourceTargetDialog._myAnthropoModelDisplay = myBodySectionPersonalizing.anthroDisplay
                anthropoModelTree._myAnthropoModelDisplay = myAnthropoModelDisplayGUI.anthropoModelDisplay
                bodyDimension._myAnthropoModelDisplay = myAnthropoModelDisplayGUI.anthropoModelDisplay

                bodyDimension._warningDialog = warningDialog
                anthropoModelTree._warningDialog = warningDialog


            }
        }
    }
}
