// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.1
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import "../anthropometry"

ModuleToolWindow
{
    title: "Target Body dimensions"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window


    property BodySectionPersonalizing myBodySectionPersonalizing

    onMyBodySectionPersonalizingChanged:
    {
        loadtargetAction.enabled = Qt.binding(function() { return myBodySectionPersonalizing.isAnthropoModelLoaded })
    }

    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : itemMargin
        RowLayout{
            Layout.fillWidth : true
            Button {
                id: loadtargetButton
                Layout.fillWidth : true
                action: loadtargetAction
                tooltip: "Use current list of anthropometric and landmarks targets. Required to have an Anthropometric modelloaded."
            }
            Button {
                id: importtargetButton
                Layout.fillWidth : true
                action: importTargetAction
                tooltip: "Import list of anthropometric and landmarks targets for a target file (*.ptt).\n All targets already defined are deleted. Required to have an Anthropometric modelloaded."
            }
        }
        ModuleToolWindowButton
        {
            text:"Create Anthropometric target"
            toolWindow: anthroPerso
            shortcutChar: "A"
            checkable: true
            //enabled: myBodySectionPersonalizing.isAnthropoModelLoaded
            tooltip: "Create a  list of anthropometric targets using anthropometry prediction tool."

        }
//        GridLayout {
//            Layout.fillWidth : true
//            columns: 2
//            Label {
//                id: currentHeightLabel
//                Layout.fillWidth : true
//                text: "Current (" + context.modelLengthUnit + ")"
//                horizontalAlignment: TextInput.AlignLeft
//            }
//            Text {
//                id: currentHeightText
//                Layout.fillWidth : true
//                text: myBodySectionPersonalizing.getHbmHeight()
//                horizontalAlignment: TextInput.AlignLeft
//                Connections {
//                    target: context
//                    onMetadataChanged: currentHeightText.text = myBodySectionPersonalizing.getHbmHeight()
//                }
//                Connections {
//                    target: context
//                    onModelChanged: currentHeightText.text = myBodySectionPersonalizing.getHbmHeight()
//                }
//            }

//            Label {
//                id: targetHeightLabel
//                Layout.fillWidth : true
//                text: "Target height (" + context.modelLengthUnit + ")"
//                horizontalAlignment: TextInput.AlignLeft
//            }
//            TextField {
//                id: targetHeightText
//                Layout.fillWidth : true
//                placeholderText:qsTr("Target height")
//                text: myBodySectionPersonalizing.targetHeight
//                onEditingFinished: {
//                    myBodySectionPersonalizing.targetHeight = text
//                }
//                Connections {
//                    target: myBodySectionPersonalizing
//                    onTargetHeightChanged: targetHeightText.text = myBodySectionPersonalizing.targetHeight
//                }
//            }
//        }
//        CheckBox {
//            id: heightscalingCheckBox
//            Layout.fillWidth : true
//            text: qsTr("do not scale skeleton to the target height\n(this may lead to deformation artefact, Use it only for small change in body shape.)")
//            onClicked: myBodySectionPersonalizing.optionScaleSkeleton=(checked !== true)
//            Component.onCompleted: {
//                checked = (myBodySectionPersonalizing.optionScaleSkeleton !== true)
//            }
//        }
    }
    AnthroPerso
    {
        id: anthroPerso
    }
    Action {
        id: loadtargetAction
        enabled: context.hasModel
        text: qsTr("Use current targets")
        tooltip: qsTr("Use targets that are currently loaded within the internal target list")
        onTriggered: myBodySectionPersonalizing.loadTargets()
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
        setCurrentWindowSizeFixed()
    }

    Action {
        id: importTargetAction
        enabled: context.hasModel
        text: qsTr("Load targets from file")
        tooltip: qsTr("Import targets from file")
        onTriggered: importTargetDialog.open()
    }
    FileDialog {
        id: importTargetDialog
        title: qsTr("Import target...")
        nameFilters: ["Target files (*.ptt)"]
        onAccepted:{
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
                settings.recentAnthroTargetImportFolder = folder
            myBodySectionPersonalizing.loadTargetsFromFile(importTargetDialog.fileUrl)
        }
        onVisibleChanged:
        {
            if(visible && context.doFolderExist(settings.recentAnthroTargetImportFolder))
                folder = settings.recentAnthroTargetImportFolder
        }
    }

    Settings
    {
        id: settings
        category: "application"
        property url recentAnthroTargetImportFolder
    }
}


