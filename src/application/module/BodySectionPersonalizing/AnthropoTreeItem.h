/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

namespace piper {
    namespace anthropometricmodel {
        class AnthropoModel;
    }
    namespace anthropoPersonalizing {
        class AnthropoTreeItem
        {
        public:
            explicit AnthropoTreeItem(const QList<QVariant> &data, anthropometricmodel::AnthropoModel const& anthropopmodel, AnthropoTreeItem *parentItem = 0);
            ~AnthropoTreeItem();

            void appendChild(AnthropoTreeItem *child);

            AnthropoTreeItem *child(int row);
            int childCount() const;
            int columnCount() const;
            QVariant data(int column) const;
            int row() const;
            AnthropoTreeItem *parentItem();

        private:
            QList<AnthropoTreeItem*> m_childItems;
            QList<QVariant> m_itemData;
            AnthropoTreeItem *m_parentItem;
        };

    }
}

