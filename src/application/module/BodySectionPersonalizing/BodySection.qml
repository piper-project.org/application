// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0

Item {
    id: sectionProperties
    //anchors.fill:parent
    Layout.fillWidth : true
    ColumnLayout {
        id: sectionProp
        Layout.fillWidth : true

        RowLayout {
            id: rowSkin
            Layout.fillWidth : true
            Label {
                id: skinregionText
                Layout.fillWidth : true
                text: "Skin region"
                horizontalAlignment: TextInput.AlignHCenter
            }
            ComboTableComponent {
                id: skinregionComboTable
                Layout.fillWidth : true
                implicitTableHeight: 60

                internalComboList: myBodySectionPersonalizing.hbmentities
                internalTableModel: myBodySectionPersonalizing.entity
                onAddInTable: {
                    myBodySectionPersonalizing.setEntity(myBodySectionPersonalizing.currentComponent, comboItem)
                }
                onRemoveCurrentItem: {
                    myBodySectionPersonalizing.removeEntity(myBodySectionPersonalizing.currentComponent, tableItem)
                }
            }
        }
        SpinSliderComponent {
            id: rotationComp
            title: "Rotation / normal section (-180° <-> 180°)"
            Layout.fillWidth : true
            incValue: 10
            maxValue: 180
            minValue: -180
            decimals: 1
            onValueFinalized: {
                myBodySectionPersonalizing.setAnglerotationaxe(value)
            }
//            Connections {
//                target: myBodySectionPersonalizing
//                onAnglerotationaxeChanged: sectionProp.value_axialrot = myBodySectionPersonalizing.anglerotationaxe
//            }
        }
        SpinSliderComponent {
            id: tiltComp
            title: "Tilt section (-90° <-> 90°)"
            incValue: 10
            maxValue: 90
            minValue: -90
            decimals: 1
            implicitWidth: rotationComp.implicitWidth
            onValueFinalized: {
                myBodySectionPersonalizing.setAngletilt(value)
            }
//            Connections {
//                target: myBodySectionPersonalizing
//                onAngletiltChanged: sectionProp.value_tilt = myBodySectionPersonalizing.angletilt
//            }
        }
        GroupBox {
            id: controlPointGroup
            title: "Control Points"
            Layout.fillWidth : true
            RowLayout {
                id: cpColumn
                anchors.fill: parent
                Label {
                    id: angularpositionTitle
                    Layout.fillWidth : true
                    text: "angular Positions (°)\n between -180° and 180° "
                    horizontalAlignment: TextInput.AlignHCenter
                }
                TextField {
                    id: angularpositionText
                    Layout.fillWidth : true
                    placeholderText:qsTr("values between -180° and 180° separated by ;")
                    onEditingFinished: {
                        myBodySectionPersonalizing.optionAngularPosition = text
                    }
                    Component.onCompleted: text = myBodySectionPersonalizing.optionAngularPosition
//                    Connections {
//                        target: myBodySectionPersonalizing
//                        onOptionAngularPositionChanged: angularpositionText.text = myBodySectionPersonalizing.optionAngularPosition
//                    }
                }
                ColumnLayout {
                    width: parent.width
                    CheckBox {
                        id: optionCenterCheck
                        Layout.fillWidth : true
                        text: qsTr("at Center")
                        onClicked: myBodySectionPersonalizing.optionCenter=checked
                        Component.onCompleted: checked = myBodySectionPersonalizing.optionCenter
//                        Connections {
//                            target: myBodySectionPersonalizing
//                            onOptionCenterChanged: optionCenterCheck.checked = myBodySectionPersonalizing.optionCenter
//                        }
                    }
                    CheckBox {
                        id: optionMinMaxWidthCheck

                        Layout.fillWidth : true
                        text: qsTr("Min. Max Width")
                        onClicked: myBodySectionPersonalizing.optionMinMaxWidth=checked
                        Component.onCompleted: checked = myBodySectionPersonalizing.optionMinMaxWidth
//                        Connections {
//                            target: myBodySectionPersonalizing
//                            onOptionMinMaxWidthChanged: optionMinMaxWidthCheck.checked = myBodySectionPersonalizing.optionMinMaxWidth
//                        }
                    }
                    CheckBox {
                        id: optionMinMaxDepthCheck

                        Layout.fillWidth : true
                        text: qsTr("Min. Max Depth")
                        onClicked: myBodySectionPersonalizing.optionMinMaxDepth=checked
                        Component.onCompleted: checked = myBodySectionPersonalizing.optionMinMaxDepth
//                        Connections {
//                            target: myBodySectionPersonalizing
//                            onOptionMinMaxDepthChanged: optionMinMaxDepthCheck.checked = myBodySectionPersonalizing.optionMinMaxDepth
//                        }
                    }
                }
            }
        }
    }


    Connections {
        target: myBodySectionPersonalizing
        onCurrentComponentChanged: {
            rotationComp.valueComponent = myBodySectionPersonalizing.anglerotationaxe
            tiltComp.valueComponent = myBodySectionPersonalizing.angletilt
            angularpositionText.text = myBodySectionPersonalizing.optionAngularPosition
            optionCenterCheck.checked = myBodySectionPersonalizing.optionCenter
            optionMinMaxWidthCheck.checked = myBodySectionPersonalizing.optionMinMaxWidth
            optionMinMaxDepthCheck.checked = myBodySectionPersonalizing.optionMinMaxDepth
        }
    }
}
