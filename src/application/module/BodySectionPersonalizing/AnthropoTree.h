/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANTHROPOTREE_H
#define PIPER_ANTHROPOTREE_H


#include <QAbstractItemModel>
#include <qsortfilterproxymodel.h>
#include <QModelIndex>
#include <QVariant>

namespace piper {
    namespace anthropometricmodel {
        class AnthropoModel;
    }
    namespace anthropoPersonalizing {

        class AnthropoTreeItem;

        class AnthropoTree : public QAbstractItemModel
        {
            Q_OBJECT

        public:
            explicit AnthropoTree( QObject *parent = 0);
            ~AnthropoTree();

            Q_INVOKABLE QVariantMap get(const QModelIndex &index) const;
            Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
            /// \return indexes of all elements in the model
            Q_INVOKABLE QModelIndexList getAllIndexes() const;
            Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
            /// \return indexes of all children of \a parent if \a recursive is false return only direct children indexes
            Q_INVOKABLE QModelIndexList getChildrenIndexes(const QModelIndex &parent = QModelIndex(), bool recursive = true) const;

            QVariant headerData(int section, Qt::Orientation orientation,
                int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
            QModelIndex index(int row, int column,
                const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
            QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
            int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
            int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

            enum DisplayRoles {
                NameRole = Qt::UserRole + 1,
                TypeRole,
                DefinedRole
            };

            QHash<int, QByteArray> roleNames() const {
                QHash<int, QByteArray> roles;
                roles[NameRole] = "name";
                roles[TypeRole] = "type";
                roles[DefinedRole] = "defined";
                return roles;
            }

            void clear();
            void update();

        private:
            void setupModelData(anthropometricmodel::AnthropoModel const& anthropopmodel, AnthropoTreeItem *parent);

            AnthropoTreeItem *rootItem;

        };

        class AnthropoTreeProxy : public QSortFilterProxyModel {
            Q_OBJECT
        public:
            Q_INVOKABLE QModelIndexList getAllIndexes() {
                QModelIndexList listindexes;
                //for (int i = 0; i<this->rowCount(); i++){
                //    QModelIndex index = this->index(i, 0);
                //    listindexes.append(index);
                //}
                //return listindexes;
                QModelIndexList model_index = static_cast<AnthropoTree*>(sourceModel())->getAllIndexes();
                foreach(QModelIndex index, model_index)
                    listindexes.push_back(mapFromSource(index));
                return listindexes;

            }
            Q_INVOKABLE QVariantMap get(const QModelIndex &index) const {
                return static_cast<AnthropoTree*>(sourceModel())->get(mapToSource(index));
            }

        };


    }
}

Q_DECLARE_METATYPE(piper::anthropoPersonalizing::AnthropoTreeProxy*)

#endif
