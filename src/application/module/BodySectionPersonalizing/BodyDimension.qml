// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import piper.AnthropoModelDisplay 1.0

ModuleToolWindow
{
    title: "Body Dimensions"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window


    property BodySectionPersonalizing myBodySectionPersonalizing
    property MessageDialog _warningDialog
    property AnthropoModelDisplay _myAnthropoModelDisplay

    property alias dimensionSegmentTabItem: tabsegment.item
    property alias dimensionSectionTabItem: tabsection.item

    onVisibleChanged: {
         myBodySectionPersonalizing.currentBodyDim = ""
    }


    Component {
        id: dimensionSegmentComp
        BodyDimensionTab {
            id: dimensionSegmentTab
            dimensionType: dimensionTypeSegment
            modelDimension: myBodySectionPersonalizing.modelDimensionSegment
            componentList: myBodySectionPersonalizing.listBodySegment
            componentList2: myBodySectionPersonalizing.listBodySegment
            dimensionComponent: myBodySectionPersonalizing.modelDimensionCompSegment
        }
    }
    Component {
        id: dimensionSectionComp
        BodyDimensionTab {
            id: dimensionSectionTab
            dimensionType: dimensionTypeSection
            modelDimension: myBodySectionPersonalizing.modelDimensionSection
            componentList: myBodySectionPersonalizing.listBodysection
            componentList2: myBodySectionPersonalizing.listBodysection
            dimensionComponent: myBodySectionPersonalizing.modelDimensionCompSection
        }
    }
    function initSegment() {
        if (dimensionSectionTabItem !== null)
        dimensionSegmentTabItem.dimensionType = dimensionTypeSegment
    }
    function initSection() {
        if (dimensionSegmentTabItem !== null)
        dimensionSectionTabItem.dimensionType = dimensionTypeSection
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins : itemMargin
        TabView {
            id: dimTabView
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 0
            anchors.margins : itemMargin
            frameVisible: false
            Tab {
                id: tabsegment
                title: "Segment"
                active: true
                onLoaded: {
                    initSegment()
                }
                onFocusChanged: {
                    if (!focus) {
                        myBodySectionPersonalizing.currentBodyDim=""
                    }
                }
                sourceComponent:dimensionSegmentComp
            }
            Tab {
                id: tabsection
                title: "Sections"
                anchors.fill: parent
                active: true
                onLoaded: {
                    initSection()
                }
                onFocusChanged: {
                    if (!focus) {
                        myBodySectionPersonalizing.currentBodyDim=""
                    }
                }
                sourceComponent:dimensionSectionComp

            }
        }
//        Button {
//            id: sourcebodydimButton
//            Layout.fillWidth : true
//            Layout.row: 1
//            anchors.top:dimTabView.botom
//            action: showtablesourceDim
//        }
    }
    DimensionTypeSegment {
        id: dimensionTypeSegment
    }
    DimensionTypeSection {
        id: dimensionTypeSection
    }
//    PersoSourceTargetDialog {
//        id: persoSourceNoTargetDialog
//        showtarget: false
//    }
//    Action {
//        id: showtablesourceDim
//        text: qsTr("Dimension value on current HBM")
//        onTriggered: {
//            myBodySectionPersonalizing.currentComponent=""
//            persoSourceNoTargetDialog.title = "Dimensions Source"
//            persoSourceNoTargetDialog.show()
//            persoSourceNoTargetDialog.width= 700

//        }
//    }
    Component.onCompleted: {
//        persoSourceNoTargetDialog.myBodySectionPersonalizing = myBodySectionPersonalizing
//        persoSourceNoTargetDialog._myAnthropoModelDisplay = _myAnthropoModelDisplay
        adjustWindowSizeToContent();
        height += 240;
        width += 350;
        setCurrentWindowSizeFixed();
    }
}


