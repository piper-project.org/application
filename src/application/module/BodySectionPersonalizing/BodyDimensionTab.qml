// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import piper.AnthropoModelDisplay 1.0


Rectangle {
    id: tab
    

    property variant modelDimension
    property variant dimensionType
    property variant componentList
    property variant componentList2
    property variant dimensionComponent
    
    


    onVisibleChanged:{
        bodydimTableView.currentRow=-1
        bodydimTableView.selection.clear()
    }

    color: "lightgray"
    ColumnLayout
    {
        anchors.fill: parent
        anchors.margins : itemMargin
        Button {
            id: newDimSegButton
            Layout.fillWidth : true
            action: newDimSeg
        }
        TableView {
            id: bodydimTableView
            Layout.fillWidth: true
            height: 240
            model: tab.modelDimension
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            
            TableViewColumn {
                id: col_name
                role: "NameRole"
                title: "Name"
                movable: false
                resizable: true
                width: 160
            }
            TableViewColumn {
                id: col_type
                role:"TypeRole"
                title: "Type"
                resizable: false
                movable: false
                width: 160
                delegate: Item {
                    ComboBox {
                        id: typeCombo
                        anchors.fill:parent
                        model: tab.dimensionType
                        enabled: bodydimTableView.currentRow === styleData.row
                        Component.onCompleted: {
                            currentIndex= find(bodydimTableView.model.getType(styleData.row))
                        }
                        onActivated: {
                            myBodySectionPersonalizing.setBodydimType(bodydimTableView.model.getName(styleData.row),  typeCombo.textAt(index))
                        }
                    }
                }
            }

            TableViewColumn {
                id: col_ref
                role:"RefRole"
                title: "Ref Anthropo Comp"
                resizable: true
                movable: false
                width: 140
                delegate: Item {
                    ComboBox {
                        id: typeComboRef
                        anchors.fill:parent
                        property bool spy
                        model: tab.componentList
                        enabled: bodydimTableView.currentRow === styleData.row
                        Component.onCompleted: {
                            currentIndex= find(bodydimTableView.model.getRef(styleData.row))
                        }
                        onModelChanged: {
                            if (styleData.row > -1) {
                                currentIndex= find(bodydimTableView.model.getRef(styleData.row))
                            }
                        }
                        onActivated: {
                            if (bodydimTableView.currentRow > -1 && index > 0) {
                                spy = myBodySectionPersonalizing.setBodydimRef(bodydimTableView.model.getName(styleData.row),  typeComboRef.textAt(index))
                                if (spy) {
                                    bodydimsectionTableView.setCurrent( typeComboRef.textAt(index))
                                    _myAnthropoModelDisplay.highlightDimensionComponent( typeComboRef.textAt(index), true)
                                }
                                else {
                                    currentIndex= find(bodydimTableView.model.getRef(styleData.row))
                                }
                            }
                            else if (bodydimTableView.currentRow > -1 && index == 0) {
                                myBodySectionPersonalizing.removeBodydimRef(bodydimTableView.model.getName(styleData.row),  typeComboRef.textAt(index))
                                _myAnthropoModelDisplay.highlightDimensionComponent( typeComboRef.textAt(index), true)
                            }
                        }
                    }
                }
            }

            TableViewColumn {
                id: col_relative
                role:"RelativeRole"
                title: "Relative Dim"
                resizable: true
                movable: false
                width: 140
                delegate: Item {
                    CheckBox {
                        id: checkRelative
                        enabled: bodydimTableView.currentRow === styleData.row
                        anchors.fill:parent
                        Component.onCompleted: {
                            checked= bodydimTableView.model.getIsRelative(styleData.row)
                        }
                        onClicked: {
                            myBodySectionPersonalizing.setBodydimRelative(bodydimTableView.model.getName(styleData.row), checked)
                        }
                    }
                }
            }




            onCurrentRowChanged: {
                if (currentRow > -1) {
                    myBodySectionPersonalizing.currentBodyDim = model.getName(currentRow)
                }
                else
                    myBodySectionPersonalizing.currentBodyDim = ""
            }
            Menu { id: contextMenuBodyDim
                MenuItem {
                    action: renameBodydimAction
                }
                MenuItem {
                    action: deleteBodydimAction
                }
            }
            rowDelegate: Item {
                Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        verticalCenter: parent.verticalCenter
                    }
                    height: parent.height
                    color: styleData.selected ? 'lightblue' : 'white'
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        propagateComposedEvents: true
                        onReleased: {
                            if (typeof styleData.row === 'number') {
                                bodydimTableView.currentRow = styleData.row
                                if (mouse.button === Qt.RightButton) {
                                    contextMenuBodyDim.popup()
                                }//if
                            }//if
                            mouse.accepted = false
                        }//onReleased
                    }//MouseArea
                }//Rectangle
            }//rowDelegate
        }
        GroupBox {
            id:bodydimsectionass
            enabled: bodydimTableView.currentRow>-1
            Layout.fillWidth : true

            title: "Current Body Dimension:"
            ColumnLayout {
                anchors.fill: parent
                spacing: 0
                Label {
                    id: bodydimsectionText
                    Layout.fillWidth : true
                    width: 17
                    text: "Associated Anthropo Component:"
                    horizontalAlignment: TextInput.AlignHCenter
                }
                ComboTableSpinComponent {
                    id: bodydimsectionTableView
                    Layout.fillWidth : true
                    implicitTableHeight: 90
                    internalComboList: tab.componentList2
                    internalTableModel: tab.dimensionComponent
                    onAddInTable: {
                        if (bodydimTableView.currentRow > -1) {
                            if (myBodySectionPersonalizing.addAnthropoComp(bodydimTableView.model.getName(bodydimTableView.currentRow), comboItem)) {
                                bodydimsectionTableView.setCurrent( comboItem)
                                _myAnthropoModelDisplay.highlightDimensionComponent( comboItem, true)
//                                dimensionComponent = myBodySectionPersonalizing.modelAnthropoComp(myBodySectionPersonalizing.currentBodyDim)
                            }
                        }
                    }
                    onSetCurrentItem: {
                        if (bodydimTableView.currentRow > -1) {
                            _myAnthropoModelDisplay.highlightDimensionComponent( tableItem, true)
                        }
                    }
                    onRemoveCurrentItem: {
                        if (bodydimTableView.currentRow > -1) {
                            myBodySectionPersonalizing.removeAnthropoComp(bodydimTableView.model.getName(bodydimTableView.currentRow), tableItem)
//                            dimensionComponent = myBodySectionPersonalizing.modelAnthropoComp(myBodySectionPersonalizing.currentBodyDim)
                        }
                    }
                    onSetWeight: {
                        if (bodydimTableView.currentRow > -1) {
                            myBodySectionPersonalizing.setAnthropoCompWeight(bodydimTableView.model.getName(bodydimTableView.currentRow), tab.dimensionComponent.getName(tableItem), weight)
//                            dimensionComponent = myBodySectionPersonalizing.modelAnthropoComp(myBodySectionPersonalizing.currentBodyDim)
                        }
                    }
                }
            }
        }

        Action {
            id: newDimSeg
            text: qsTr("Add New Dimension for segment")
            onTriggered: {
                nameDimensionDialog.open()
            }
        }
        Action {
            id: deleteBodydimAction
            text: qsTr("Delete")
            onTriggered: {
                myBodySectionPersonalizing.deleteBodydim(bodydimTableView.model.getName( bodydimTableView.currentRow))
                bodydimTableView.currentRow=-1
            }
        }
        Action {
            id: renameBodydimAction
            text: qsTr("Rename")
            onTriggered: {
                renameBodyDimDialog.open()
            }
        }
        Dialog {
            id: nameDimensionDialog
            title: qsTr("Name of the new Dimension")
            standardButtons: StandardButton.Ok | StandardButton.Cancel
            property bool spy
            onAccepted: {
                if (newname.text.length > 0) {
                    spy = myBodySectionPersonalizing.newBodydim(newname.text, newtypeCombo.textAt(newtypeCombo.currentIndex))
                    if (!spy)
                        _warningDialog.open()
                }
            }
            onVisibleChanged: {
                if (visible == true) {
                    newname.text=""
                }
            }
            RowLayout {
                width: parent.width
                TextField {
                    id: newname
                    Layout.fillWidth: true
                    focus: true
                    text: ""
                }
                ComboBox {
                    id: newtypeCombo
                    Layout.fillWidth: true
                    model: tab.dimensionType
                    Component.onCompleted: width=120
                }

            }
            Component.onCompleted: {
                width=500;
                newname.text=""
                newtypeCombo.currentIndex=0
            }
        }
        Dialog {
            id: renameBodyDimDialog
            title: qsTr("New name of the Body Dimension")
            standardButtons: StandardButton.Ok | StandardButton.Cancel
            property bool spy
            onAccepted: {
                spy = myBodySectionPersonalizing.renameBodydim(bodydimTableView.model.getName( bodydimTableView.currentRow), modelrenamedim.text)
                if (!(spy))
                    _warningDialog.open()
            }
            onVisibleChanged: {
                if (visible == true) {
                    if (bodydimTableView.currentRow>-1)
                        modelrenamedim.text=bodydimTableView.model.getName( bodydimTableView.currentRow)
                    else
                        modelrenamedim.text=""
                }
            }
            TextField {
                id: modelrenamedim
                Layout.fillWidth: true
                anchors.left: parent.left
                anchors.right: parent.right
                focus: true
            }
            Component.onCompleted: {
                width=500;
                if (bodydimTableView.currentRow>-1)
                    modelrenamedim.text=bodydimTableView.model.getName( bodydimTableView.currentRow)
                else
                    modelrenamedim.text=""
            }
        }
        MessageDialog {
            id: _warningDialog
            title: "WARNING:"
            icon: StandardIcon.Warning
            text: "This name already exist."
            standardButtons: StandardButton.Ok
        }
    }
}
