// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0




ColumnLayout {
    width:parent.width
    id: myComboSpinTable
    property variant internalComboList
    property variant internalTableModel
    property int implicitTableHeight

    signal addInTable(string  comboItem)
    signal setCurrentItem(string tableItem)
    signal removeCurrentItem(string tableItem)
    signal setWeight(string tableItem, real weight)

    function clear() {
        myTable.selection.clear()
        myTable.currentRow=-1
    }

    function setCurrent( name) {
        myTable.selection.clear()
        if (myTable.currentRow !== internalTableModel.get( name))
                myTable.currentRow = internalTableModel.get( name)
        if (myTable.currentRow > -1)
            myTable.selection.select(myTable.currentRow, myTable.currentRow)
    }

    height: parent.height

    ComboBox {
        id: myCombo
        Layout.fillWidth : true
        Layout.fillHeight : true
        model: parent.internalComboList
        onCurrentIndexChanged: {
            if (currentIndex >0) {
                myComboSpinTable.addInTable(myCombo.currentText)
            }
            myCombo.currentIndex=0
        }
    }

    TableView {
        id: myTable
        Layout.fillWidth : true
        Layout.fillHeight : true

        Layout.preferredHeight: implicitTableHeight
        model: parent.internalTableModel
        headerVisible: false
        selectionMode: SelectionMode.SingleSelection
        TableViewColumn {
            role: "NameRole"
            title: "Name"
            resizable: false
        }


        TableViewColumn {
            role:"WeightRole"
            title: "Weight"
            resizable: true
            movable: false
            delegate: Item {
                SpinBox {
                    id: weightspin
                    enabled: false
                    Layout.fillWidth : true
                    Layout.fillHeight : true
                    stepSize: 0.1
                    maximumValue: 1.0
                    minimumValue: 0.0
                    decimals: 2
                    onValueChanged: {
                        myComboSpinTable.setWeight(styleData.row, weightspin.value)
                    }
                    Component.onCompleted: {
                        value = myTable.model.getWeight(styleData.row)
                    }
                }
                Connections {
                    target: myTable
                    onCurrentRowChanged: {
                        if (myTable.currentRow > -1 && myTable.currentRow === styleData.row)
                            weightspin.enabled=true
                        else
                            weightspin.enabled=false
                    }
                }
            }
        }


        onCurrentRowChanged:  {
            if (currentRow > -1) {
                myComboSpinTable.setCurrentItem(model.getName(myTable.currentRow))
            }
        }
        Menu { id: contextMenu
            MenuItem {
                action: removeAction
            }
        }
        rowDelegate: Item {
            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }//anchors
                height: parent.height
                color: styleData.selected ? 'lightblue' : 'white'
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.RightButton
                    propagateComposedEvents: true
                    onReleased: {
                        if (typeof styleData.row === 'number') {
                            myTable.currentRow = styleData.row
                            if (mouse.button === Qt.RightButton) { // never true
                                contextMenu.popup()
                            }//if
                        }//if
                        mouse.accepted = false
                    }//onReleased
                }//MouseArea
            }//Rectangle
        }//rowDelegate
        onModelChanged: {
            //resizeColumnsToContents()
            myTable.currentRow = -1
        }
    }
    Action {
        id: removeAction
        text: qsTr("Remove")
        onTriggered: {
            myComboSpinTable.removeCurrentItem(myTable.model.getName(myTable.currentRow))
            myTable.currentRow = -1
        }
    }
}

