// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQml.Models 2.2

import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import piper.AnthropoModelDisplay 1.0

ModuleToolWindow
{
    title: "Anthropometric Component"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

    property BodySectionPersonalizing myBodySectionPersonalizing
    property AnthropoModelDisplay _myAnthropoModelDisplay
    property MessageDialog _warningDialog

    property string currentsegment: ""

    Component {
        id: bodysectionProperties
        BodySection  { }
    }
    Component {
        id: bodysegmentProperties
        BodySegment  {
            Component.onCompleted: {
                _myAnthropoModelDisplay = window._myAnthropoModelDisplay
            }
        }
    }
    GridLayout {
        anchors.fill: parent
        anchors.margins : itemMargin
        columns: 2
        Label {
            Layout.columnSpan: 1
            Layout.fillWidth : true
            text: "Name"
        }
        TextField {
            id: name
            Layout.columnSpan: 1
            Layout.fillWidth : true
            readOnly: false
            text: myBodySectionPersonalizing.currentComponent
            placeholderText: qsTr("component name")
            property bool spy
            onAccepted: {
                name.spy = myBodySectionPersonalizing.renameComponent(myBodySectionPersonalizing.currentComponent, name.text)
                if (spy) {
                    myBodySectionPersonalizing.currentComponent = name.text
                }
                else {
                    _warningDialog.open()
                    name.text = myBodySectionPersonalizing.currentComponent
                }
                name.focus=false
            }
            Connections {
                target: myBodySectionPersonalizing
                onCurrentComponentChanged: name.text = myBodySectionPersonalizing.currentComponent
            }
        }
        Label {
            Layout.columnSpan: 1
            Layout.fillWidth : true
            text: "Type"
        }
        ComboBox {
            id: comptype
            Layout.columnSpan: 1
            Layout.fillWidth : true
            model: [ "SECTION", "SEGMENT" ]
            enabled: false
            //            currentIndex: {
            //                if (myBodySectionPersonalizing!==null)
            //                    find(myBodySectionPersonalizing.currentComponentType)
            //                else
            //                    -1
            //            }
            onModelChanged:  {
                if (myBodySectionPersonalizing != null)
                    comptype.currentIndex = comptype.find(myBodySectionPersonalizing.currentComponentType)
            }
        }
        Label {
            Layout.columnSpan: 1
            Layout.fillWidth : true
            text: "Parent"
        }
        ComboBox {
            id: compparent
            Layout.columnSpan: 1
            Layout.fillWidth : true
            model: myBodySectionPersonalizing.listComponents
            enabled: true
            onModelChanged: {
                compparent.currentIndex=-1
            }

            onActivated:  {
                if (myBodySectionPersonalizing !== null && index > -1 && myBodySectionPersonalizing.currentComponentParent !== textAt(index)) {
                    myBodySectionPersonalizing.currentComponentParent = textAt(index)
                }
            }
        }

        Label {
            Layout.columnSpan: 1
            Layout.fillWidth : true
            text: "center scaling"
        }
        CheckBox {
            id: compCenterCheck
            Layout.fillWidth : true
            enabled: myBodySectionPersonalizing.currentComponentParent !== "none"
            text: qsTr("on parent")
            onClicked: {
                if (myBodySectionPersonalizing.centerScaleOnParent !== checked)
                    myBodySectionPersonalizing.centerScaleOnParent=checked
            }
            Component.onCompleted:  {
                if (myBodySectionPersonalizing !== null)
                    checked = myBodySectionPersonalizing.centerScaleOnParent
            }
        }

        GroupBox {
            id: compGroupParent
            checkable: true
            Layout.columnSpan: 2
            Layout.fillWidth : true
            flat: false
            title: "Position on parent"
            onCheckedChanged: {
                myBodySectionPersonalizing.isDefinedFromParent = checked
            }
            Component.onCompleted:  {
                if (myBodySectionPersonalizing !== null)
                    checked = myBodySectionPersonalizing.isDefinedFromParent
            }
            enabled: compparent.currentIndex > 0
            GridLayout {
                anchors.fill: parent
//                anchors.margins : itemMargin
                columns: 2
                enabled: compGroupParent.checked
                SpinSliderComponent {
                    id: positionComp
                    title: "position"
                    Layout.columnSpan: 2
                    Layout.fillWidth : true
                    incValue: 0.01
                    maxValue: 1
                    minValue: 0
                    decimals: 2
                    onValueFinalized: {
                        if (myBodySectionPersonalizing !== null) {
                            myBodySectionPersonalizing.currentComponentPosition = value
                        }
                    }
                }
                Label {
                    Layout.columnSpan: 1
                    text: "Define Contol Point"
                }
                CheckBox {
                    id: compCpParent
                    //                    text: qsTr("on parent")
                    onClicked: {
                        if (myBodySectionPersonalizing.controlPointOnParent !== checked)
                            myBodySectionPersonalizing.controlPointOnParent=checked
                    }
                    Component.onCompleted:  {
                        if (myBodySectionPersonalizing !== null && checked !== myBodySectionPersonalizing.controlPointOnParent)
                            checked = myBodySectionPersonalizing.controlPointOnParent
                    }
                }
            }
        }

        GroupBox {
            id: paramCPParent
            Layout.columnSpan: 2
            Layout.fillWidth : true
            ColumnLayout {
                anchors.fill: parent
                SpinSliderComponent {
                    id: usedBoneComp
                    title: "Used for Bone"
                    Layout.fillWidth : true
//                    Layout.columnSpan: 2
                    incValue: 0.1
                    maxValue: 1.0
                    minValue: 0.0
                    decimals: 1
                    updateValueWhileDragging: true
                    onValueFinalized: {
                        myBodySectionPersonalizing.setUseBone( value)
                    }
                }
                SpinSliderComponent {
                    id: usedSkinComp
                    title: "Used for skin"
                    Layout.fillWidth : true
//                    Layout.columsnSpan: 2
                    incValue: 0.1
                    maxValue: 1.0
                    minValue: 0.0
                    decimals: 1
                    updateValueWhileDragging: true
                    onValueFinalized: {
                        myBodySectionPersonalizing.setUseSkin( value)
                    }
                }
            }
        }

        GroupBox {
            title: "Component Properties"
            id: grpProp
            Layout.columnSpan: 2
            Layout.fillWidth : true
            Layout.fillHeight : true
            width: compGroupProp.width
//            anchors.margins: itemMargin
            Loader {
                id: compGroupProp
                sourceComponent: bodysegmentProperties
            }
        }

        Connections {
            target: myBodySectionPersonalizing
            onCurrentComponentChanged: {
                comptype.currentIndex = comptype.find(myBodySectionPersonalizing.currentComponentType)
                compparent.currentIndex = compparent.find(myBodySectionPersonalizing.currentComponentParent)
                compCenterCheck.checked = myBodySectionPersonalizing.centerScaleOnParent
                compCenterCheck.enabled = myBodySectionPersonalizing.currentComponentParent !== "none"
                compGroupParent.checked = myBodySectionPersonalizing.isDefinedFromParent
                positionComp.valueComponent = myBodySectionPersonalizing.currentComponentPosition
                compCpParent.checked = myBodySectionPersonalizing.controlPointOnParent
                usedBoneComp.valueComponent = myBodySectionPersonalizing.useBone
                usedSkinComp.valueComponent = myBodySectionPersonalizing.useSkin
                if (currentsegment !== myBodySectionPersonalizing.currentComponent) {
                    currentsegment = myBodySectionPersonalizing.currentComponent
                    if (myBodySectionPersonalizing.isCurrentSegment()) {
                        compGroupProp.sourceComponent = bodysegmentProperties
                    }
                    else {
                        compGroupProp.sourceComponent = bodysectionProperties
                    }
                }
            }
        }
    }


    Component.onCompleted: {
        adjustWindowSizeToContent(20, 20)
        height += 260
        width += 140
        setCurrentWindowSizeFixed()
    }




}



