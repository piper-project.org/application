// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQuick 2.5

import Piper 1.0
import piper.BodySectionPersonalizing 1.0
import piper.AnthropoModelDisplay 1.0

ModuleToolWindow
{
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window
    visible: false
    color: "white"
    title: "Dimensions Target"

    property AnthropoModelDisplay _myAnthropoModelDisplay
    property BodySectionPersonalizing myBodySectionPersonalizing
    property bool showtarget

    property alias dimensionSegmentTabItem: tabsegment.item
    property alias dimensionSectionTabItem: tabsection.item


    function initSegment() {
//        dimensionSegmentTabItem.model = myBodySectionPersonalizing.BodySegmentSourceTarget
        dimensionSegmentTabItem.currentRow=-1
        dimensionSegmentTabItem.selection.clear()
    }
    function initSection() {
//        dimensionSectionTabItem.model = myBodySectionPersonalizing.BodySectionSourceTarget
        dimensionSectionTabItem.currentRow=-1
        dimensionSectionTabItem.selection.clear()
    }

    onVisibleChanged: {
//        myBodySectionPersonalizing.currentBodyDimTarget=""
        tabsegment.sourceComponent=dimensionSegmentComp
        tabsection.sourceComponent=dimensionSectionComp
        dimensionSegmentTabItem.currentRow=-1
        dimensionSegmentTabItem.selection.clear()
        dimensionSectionTabItem.selection.clear()
        dimensionSectionTabItem.currentRow=-1
    }

    Connections {
        target: myBodySectionPersonalizing
        onAnthropoModelChanged: {
            dimensionSectionTabItem.model = myBodySectionPersonalizing.bodySectionSourceTarget
            dimensionSegmentTabItem.model = myBodySectionPersonalizing.bodySegmentSourceTarget

        }
    }

    Item {
        Component {
            id: editableDelegateSection
            Item {
                Text {
                    width: parent.width
                    anchors.margins: 4
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    elide: styleData.elideMode
                    text: styleData.value !== undefined ? styleData.value : ""
                    color: styleData.textColor
                    visible: !styleData.selected
                }
                Loader {
                    id: loaderEditorTargetSection
                    anchors.fill: parent
                    anchors.margins: 4
                    Connections {
                        target: loaderEditorTargetSection.item
                        onEditingFinished: {
                            if (styleData.role === "ValueTargetRole") {
                                if(styleData.row < 0){
                                    return;
                                }
//                                dimensionSectionTabItem.model.setTargetValue(styleData.row, styleData.column, loaderEditorTargetSection.item.text)
                                myBodySectionPersonalizing.setNewTargetValue(dimensionSectionTabItem.model.getDimName(styleData.row), loaderEditorTargetSection.item.text)
//                                dimensionSectionTabItem.model.currentRow = -1
                            }
                        }
                    }
                    sourceComponent: styleData.selected ? editorTargetSection : null
                    Component {
                        id: editorTargetSection
                        TextInput {
                            id: textinput
                            color: styleData.textColor
                            text: styleData.value
                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent
                                hoverEnabled: true
                                onClicked: textinput.forceActiveFocus()
                            }
                        }
                    }
                }
            }
        }
    }

    Item {
        Component {
            id: editableDelegate
            Item {
                Text {
                    width: parent.width
                    anchors.margins: 4
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    elide: styleData.elideMode
                    text: styleData.value !== undefined ? styleData.value : ""
                    color: styleData.textColor
                    visible: !styleData.selected
                }
                Loader {
                    id: loaderEditor
                    anchors.fill: parent
                    anchors.margins: 4
                    Connections {
                        target: loaderEditor.item
                        onEditingFinished: {
                            if (styleData.role === "ValueTargetRole") {
                                if(styleData.row < 0){
                                    return;
                                }
                                //dimensionSegmentTabItem.model.setTargetValue(styleData.row, styleData.column, loaderEditor.item.text)
                                myBodySectionPersonalizing.setNewTargetValue(dimensionSegmentTabItem.model.getDimName(styleData.row), loaderEditor.item.text)
//                                dimensionSegmentTabItem.model.currentRow = -1
                            }
                        }
                    }
                    sourceComponent: styleData.selected ? editor : null
                    Component {
                        id: editor
                        TextInput {
                            id: textinput
                            color: styleData.textColor
                            text: styleData.value
                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent
                                hoverEnabled: true
                                onClicked: textinput.forceActiveFocus()
                            }
                        }
                    }
                }
            }
        }
    }
    Component {
        id: dimensionSectionComp
        TableView {
            id: bodysectionTable
            Layout.fillWidth: true
            model: myBodySectionPersonalizing.bodySectionSourceTarget
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            TableViewColumn {
                role: "NameDimRole"
                title: "Body Dimension"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "TypeDimRole"
                title: "Type"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "NameSectionRole"
                title: "Body Section"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "ValueSourceRole"
                title: "Source Value"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "ValueTargetRole"
                title: "Target Value"
                resizable: true
                visible: window.showtarget
                delegate: editableDelegateSection
                movable: false
            }
            onCurrentRowChanged: {
                if (currentRow>-1) {
                    myBodySectionPersonalizing.currentBodyDimTarget = model.getDimName(currentRow)
                }
                else
                    myBodySectionPersonalizing.currentBodyDimTarget = ""
            }
            onModelChanged: {
                if (currentRow >= model.rowCount())
                    currentRow = -1
            }
            onVisibleChanged: currentRow = -1
        }
    }
    Component {
        id: dimensionSegmentComp
        TableView {
            id: bodylengthTable
            Layout.fillWidth: true
            model: myBodySectionPersonalizing.bodySegmentSourceTarget
            headerVisible: true
            selectionMode: SelectionMode.SingleSelection
            TableViewColumn {
                role: "NameDimRole"
                title: "Body Dimension"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "TypeDimRole"
                title: "Type"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "NameSectionRole"
                title: "Body Segment"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "ValueSourceRole"
                title: "Source Value"
                resizable: true
                movable: false
            }
            TableViewColumn {
                role: "ValueTargetRole"
                title: "Target Value"
                resizable: true
                visible: window.showtarget
                delegate: editableDelegate
                movable: false
            }
            onCurrentRowChanged: {
                if (currentRow>-1) {
                    myBodySectionPersonalizing.currentBodyDimTarget = model.getDimName(currentRow)
                }
                else
                    myBodySectionPersonalizing.currentBodyDimTarget = ""
            }
            onModelChanged: {
                if (currentRow >= model.rowCount())
                    currentRow = -1
            }
            onVisibleChanged: currentRow = -1
        }
    }
    ColumnLayout {
        anchors.fill: parent
        TabView {
            anchors.fill: parent
            anchors.top: parent.top
            anchors.bottom: closeButton.top
            Tab {
                id: tabsection
                title: "Body Sections"
                active: true
                onLoaded: {
                    initSection()
                }
                onFocusChanged: {
                    if (focus) {
                        myBodySectionPersonalizing.currentBodyDimTarget=""
                        item.currentRow=-1
                        item.selection.clear()
                    }
                }
                sourceComponent:dimensionSegmentComp
            }
            Tab {
                id: tabsegment
                title: "Body Segment"
                active: true
                onLoaded: {
                    initSegment()
                }
                onFocusChanged: {
                    if (focus) {
                        myBodySectionPersonalizing.currentBodyDimTarget=""
                        item.currentRow=-1
                        item.selection.clear()
                    }
                }
                sourceComponent:dimensionSectionComp
            }
            //            Connections {
            //                target: myBodySectionPersonalizing
            //                onCurrentBodyDimChanged: {
            //                    if (isSegmentDimensionType(myBodySectionPersonalizing.currentBodyDimTarget)) {
            //                        dimTabView.getTab(dimTabView.currentIndex).item.bodylengthTable.model = dimTabView.getTab(dimTabView.currentIndex).item.modelDimension.get(myBodySectionPersonalizing.currentComponent)
            //                    }
            //                }
            //            }
        }
        Button {
            id: closeButton
            Layout.fillWidth: true
            text: "Close"
            onClicked: {
                myBodySectionPersonalizing.currentBodyDimTarget = ""
                window.close()
            }
        }
    }

    Component.onCompleted: {
        console.log("CREATE")
        adjustWindowSizeToContent();
        height += 300;
        width += 780;
    }

}

