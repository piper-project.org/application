/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoTree.h"

#include "AnthropoTreeItem.h"
#include "BodySectionPersonalizer/AnthropoModel.h"
#include "common/Context.h"

using namespace piper::anthropometricmodel;

namespace piper {
    namespace anthropoPersonalizing {

#include <QStringList>

        AnthropoTree::AnthropoTree(QObject *parent)
            : QAbstractItemModel(parent)
            , rootItem(nullptr)
        {

        }

        AnthropoTree::~AnthropoTree()
        {
            clear();
            delete rootItem;
        }

        void AnthropoTree::clear() {
            if (rootItem != nullptr)
                delete rootItem;
            this->beginResetModel();
            this->endResetModel();
        }

        int AnthropoTree::columnCount(const QModelIndex &parent) const
        {
            if (parent.isValid())
                return static_cast<AnthropoTreeItem*>(parent.internalPointer())->columnCount();
            else
                return rootItem->columnCount();
        }

        QVariant AnthropoTree::data(const QModelIndex &index, int role) const
        {
            if (!index.isValid())
                return QVariant();

            AnthropoTreeItem *item = static_cast<AnthropoTreeItem*>(index.internalPointer());

            if (index.isValid() && role >= NameRole) {
                switch (role) {
                case NameRole:
                    return item->data(0);
                case TypeRole:
                    return item->data(1);
                case DefinedRole:
                    return item->data(2);
                }
            }
            
            return QVariant();
        }

        Qt::ItemFlags AnthropoTree::flags(const QModelIndex &index) const
        {
            if (!index.isValid())
                return 0;

            return QAbstractItemModel::flags(index);
        }

        QVariant AnthropoTree::headerData(int section, Qt::Orientation orientation,
            int role) const
        {
            if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
                return rootItem->data(section);

            return QVariant();
        }

        QVariantMap AnthropoTree::get(const QModelIndex &index) const {
            QVariantMap map;
            QHash<int, QByteArray> modelroles = roleNames();
            for (QHash<int, QByteArray>::iterator i = modelroles.begin(); i != modelroles.end(); ++i) {
                map[i.value()] = index.data(i.key());
            }
            return map;
        }

        QModelIndex AnthropoTree::index(int row, int column, const QModelIndex &parent)
            const
        {
            if (!hasIndex(row, column, parent))
                return QModelIndex();

            AnthropoTreeItem *parentItem;

            if (!parent.isValid())
                parentItem = rootItem;
            else
                parentItem = static_cast<AnthropoTreeItem*>(parent.internalPointer());

            AnthropoTreeItem *childItem = parentItem->child(row);
            if (childItem)
                return createIndex(row, column, childItem);
            else
                return QModelIndex();
        }

        QModelIndex AnthropoTree::parent(const QModelIndex &index) const
        {
            if (!index.isValid())
                return QModelIndex();

            AnthropoTreeItem *childItem = static_cast<AnthropoTreeItem*>(index.internalPointer());
            AnthropoTreeItem *parentItem = childItem->parentItem();

            if (parentItem == rootItem)
                return QModelIndex();

            return createIndex(parentItem->row(), 0, parentItem);
        }

        int AnthropoTree::rowCount(const QModelIndex &parent) const
        {
            AnthropoTreeItem *parentItem;
            if (parent.column() > 0)
                return 0;

            if (!parent.isValid())
                parentItem = rootItem;
            else
                parentItem = static_cast<AnthropoTreeItem*>(parent.internalPointer());

            return parentItem->childCount();
        }

        void AnthropoTree::update() {
            this->beginResetModel();
            if (rootItem != nullptr)
                delete rootItem;
            QList<QVariant> rootData;
            rootData << "Name" << "Type";
            rootItem = new AnthropoTreeItem(rootData, Context::instance().project().anthropoModel());
            setupModelData(Context::instance().project().anthropoModel(), rootItem);
            this->endResetModel();
        }

        void AnthropoTree::setupModelData(anthropometricmodel::AnthropoModel const& anthropopmodel, AnthropoTreeItem *parent)
        {
            QList<AnthropoTreeItem*> parents;
            parents << parent;

            std::set<std::string> rootcomponent = anthropopmodel.getRootComponentName();
            for (auto const& curroot : rootcomponent) {
                AbstractAnthropoComponentNode const* curNode = anthropopmodel.getAnthropoComponent(curroot);
                QList<QVariant> curdata;
                curdata << QString::fromStdString(curNode->name());
                curdata << QString::fromStdString(AnthropoComponentType_str.at(curNode->type()));
                curdata << curNode->isDefined();
                parent->appendChild(new AnthropoTreeItem(curdata, anthropopmodel, parent));
            }
        }

        QModelIndexList AnthropoTree::getAllIndexes() const
        {
            return getChildrenIndexes();
        }

        QModelIndexList AnthropoTree::getChildrenIndexes(const QModelIndex &parent, bool recursive) const
        {
            AnthropoTreeItem* current;
            QModelIndexList indexes;

            if (!parent.isValid())
                current = this->rootItem;
            else
                current = static_cast<AnthropoTreeItem*>(parent.internalPointer());

            int count = current->childCount();

            for (int i = 0; i < count; ++i){
                indexes.append(index(i, 0, parent));
                if (recursive)
                    indexes.append(getChildrenIndexes(index(i, 0, parent), true));
            }
            return indexes;
        }

    }
}
