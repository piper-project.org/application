/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "BodySectionPersonalizing.h"


#include <functional>

#include "common/Context.h"
#include "common/XMLValidationProcess.h"

#include "hbm/Helper.h"

#include "anatomyDB/query.h"

namespace piper {
    using namespace hbm;
    using namespace anthropometricmodel;

    namespace anthropoPersonalizing {

		ModelTable::ModelTable(std::set<std::string> const& listnames, QObject *parent) : QAbstractListModel(parent) {
			construct(listnames);
		}

		int ModelTable::columnCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return 1;
		}

		int ModelTable::rowCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return items.count();
		}

        QVariant const ModelTable::get(QString const& value) const {
            int count = 0;
            for (auto const& cur : items) {
                if (cur->m_name == value)
                    return count;
                else count++;
            }
            return -1;
        }

		QVariant ModelTable::data(const QModelIndex &index, int role) const {
			switch (role) {
			case NameRole: 
            default:
                return items[index.row()]->m_name;
			}
		}

		QHash<int, QByteArray> ModelTable::roleNames() const {
			QHash<int, QByteArray> roles;
			roles[NameRole] = "NameRole";

			return roles;
		}

		bool ModelTable::setData(const QModelIndex &index, const QVariant &value, int role) {
			switch (role) {
			case NameRole: items[index.row()]->m_name = value.toString(); break;
			}
			emit dataChanged(index, index);
			return true;
		}

		void ModelTable::construct(const std::set<std::string>& listnames) {
			items.clear();
            this->beginResetModel();
			for (auto it = listnames.begin(); it != listnames.end(); ++it)
				items.append(new ModeItem(QString::fromStdString(*it)));
			this->endResetModel();
		}

		Qt::ItemFlags ModelTable::flags(const QModelIndex &index) const {
			Qt::ItemFlags flags = QAbstractItemModel::flags(index);
			if (index.column() > 0)
				flags |= Qt::ItemIsEditable;
			return flags;
		}

		void ModelTable::clear() {
			items.clear();
			this->beginResetModel();
			this->endResetModel();
		}

 
        ModelcomboItem::ModelcomboItem(QString const& name, AnthropoDimensionType const& type, bool const& isrelative, QString const& ref)
            : m_name(name)
            , m_type(QString::fromStdString(AnthropoDimensionType_str[(unsigned int)type]))
            , m_ref(ref)
            , m_isrelative(isrelative)
        {}
 
        ModelcomboItem::ModelcomboItem(QString const& name, AnthropoDimensionType const& type, bool const& isrelative)
            : m_name(name)
            , m_type(QString::fromStdString(AnthropoDimensionType_str[(unsigned int)type]))
            , m_ref("Select")
            , m_isrelative(isrelative)
        {}

        int ModelTableCombo::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return noOfColumns;
        }

        int ModelTableCombo::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return items.count();
        }

        QVariant const ModelTableCombo::get(QString const& value) const {
            int count = 0;
            for (auto const& cur : items) {
                if (cur->m_name == value)
                    return count;
                else count++;
            }
            return -1;
        }
        QVariant ModelTableCombo::data(const QModelIndex &index, int role) const {
            switch (role) {
            case NameRole: return items[index.row()]->m_name;
            case TypeRole: 
            default:
                return items[index.row()]->m_type;
            }
        }

        QHash<int, QByteArray> ModelTableCombo::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[TypeRole] = "TypeRole";
            roles[RefRole] = "RefRole";
            roles[RelativeRole] = "RelativeRole";
            return roles;
        }

        bool ModelTableCombo::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
            case NameRole: items[index.row()]->m_name = value.toString(); break;
            case TypeRole: items[index.row()]->m_type = value.toString(); break;
            case RefRole: items[index.row()]->m_ref = value.toString(); break;
            case RelativeRole: items[index.row()]->m_isrelative = value.toBool(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void ModelTableCombo::construct(anthropometricmodel::AnthropoModel const& anthropoModel, std::vector<std::string> const& list_dim) {
            items.clear();
            for (auto const& cur : list_dim) {
                AnthropoDimensionType curtype = anthropoModel.getAnthropoBodyDimension(cur)->getType();
                std::string ref = anthropoModel.getAnthropoBodyDimension(cur)->getReferenceAnthropoComponent();
                bool relative = anthropoModel.getAnthropoBodyDimension(cur)->isScalingRelative();
                if (anthropoModel.getAnthropoBodyDimension(cur)->getReferenceAnthropoComponent().size() > 0)
                    items.append(new ModelcomboItem(QString::fromStdString(cur), curtype, relative, QString::fromStdString(ref)));
                else
                    items.append(new ModelcomboItem(QString::fromStdString(cur), curtype, relative));
            }
            this->beginResetModel();
            this->endResetModel();
        }

        Qt::ItemFlags ModelTableCombo::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
        }

        void ModelTableCombo::clear() {
            items.clear();
            this->beginResetModel();
            this->endResetModel();
        }




        ModelAssBodySectionItem::ModelAssBodySectionItem(QString const& name, QVariant const& weight)
            : m_name(name)
            , m_weight(weight.toDouble())
        {}

        int ModelAssBodySection::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return noOfColumns;
        }

        int ModelAssBodySection::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return items.count();
        }

        QVariant const ModelAssBodySection::get(QString const& value) const {
            int count = 0;
            for (auto const& cur : items) {
                if (cur->m_name == value)
                    return count;
                else count++;
            }
            return -1;
        }
        QVariant ModelAssBodySection::data(const QModelIndex &index, int role) const {
            switch (role) {
            case NameRole: return items[index.row()]->m_name;
            case WeightRole:
            default:
                return items[index.row()]->m_weight;
            }
        }

        QHash<int, QByteArray> ModelAssBodySection::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[WeightRole] = "WeightRole";
            return roles;
        }

        bool ModelAssBodySection::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
            case NameRole: items[index.row()]->m_name = value.toString(); break;
            case WeightRole: items[index.row()]->m_weight = value.toDouble(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void ModelAssBodySection::construct(const std::map<anthropometricmodel::AbstractAnthropoComponentNode*, double>& assbodysection) {
            items.clear();
            this->beginResetModel();
            for (auto const& cur : assbodysection) {
                items.append(new ModelAssBodySectionItem(QString::fromStdString(cur.first->name()), cur.second));
            }
            this->endResetModel();
        }

        Qt::ItemFlags ModelAssBodySection::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
        }

        void ModelAssBodySection::clear() {
            items.clear();
            this->beginResetModel();
            this->endResetModel();
        }     
                
        void ListModel::init(const std::vector<std::string>& values) {
			m_data.clear();
            //m_data.append("Add");
            if (values.size() > 0) {
                for (auto const& cur : values)
                    m_data.append(QString::fromStdString(cur));
            }
            m_data.sort();
            m_data.prepend("Add");
			//emit dataChanged();
		}

		int ListModel::getindex(QString const& value) {
			if (m_data.contains(value)) {
				return m_data.indexOf(value);
			}
			else return 0;
		}

        ModelBodySectionSourceTargetItem::ModelBodySectionSourceTargetItem(std::string const& dimName, AnthropoDimensionType const& dimType, std::string const& nameComp, double value)
			: m_nameDimension(QString::fromStdString(dimName))
            , m_typeDimension(QString::fromStdString(AnthropoDimensionType_str[(unsigned int)dimType]))
            , m_nameComp(QString::fromStdString(nameComp))
			, m_valueSource(value)
            , m_valueTarget(value)
		{}

        ModelBodySectionSourceTargetItem::ModelBodySectionSourceTargetItem(std::string const& dimName, AnthropoDimensionType const& dimType, std::string const& nameComp, double value, double valuetarget)
            : m_nameDimension(QString::fromStdString(dimName))
            , m_typeDimension(QString::fromStdString(AnthropoDimensionType_str[(unsigned int)dimType]))
            , m_nameComp(QString::fromStdString(nameComp))
            , m_valueSource(value)
            , m_valueTarget(valuetarget)
        {}
        
        int BodyDimensionSourceTargetTable::columnCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return noOfColumns;
		}

		int BodyDimensionSourceTargetTable::rowCount(const QModelIndex &parent) const {
			Q_UNUSED(parent);
			return items.count();
		}

		QVariant BodyDimensionSourceTargetTable::data(const QModelIndex &index, int role) const {
			switch (role) {
			case NameDimRole: return items[index.row()]->m_nameDimension;
			case TypeDimRole: return items[index.row()]->m_typeDimension;
            case NameSectionRole: return items[index.row()]->m_nameComp;
            case ValueSourceRole: return items[index.row()]->m_valueSource;
            case ValueTargetRole: 
            default:
                return items[index.row()]->m_valueTarget;
			}
		}

		QHash<int, QByteArray> BodyDimensionSourceTargetTable::roleNames() const {
			QHash<int, QByteArray> roles;
			roles[NameDimRole] = "NameDimRole";
			roles[TypeDimRole] = "TypeDimRole";
			roles[NameSectionRole] = "NameSectionRole";
            roles[ValueSourceRole] = "ValueSourceRole";
            roles[ValueTargetRole] = "ValueTargetRole";
			return roles;
		}

		bool BodyDimensionSourceTargetTable::setData(const QModelIndex &index, const QVariant &value, int role) {
			switch (role) {
			case NameDimRole: items[index.row()]->m_nameDimension = value.toString(); break;
			case TypeDimRole: items[index.row()]->m_typeDimension = value.toString(); break;
            case NameSectionRole: items[index.row()]->m_nameComp = value.toString(); break;
            case ValueSourceRole: items[index.row()]->m_valueSource = value.toDouble(); break;
            case ValueTargetRole: items[index.row()]->m_valueTarget = value.toDouble(); break;
			}
			emit dataChanged(index, index);
			return true;
		}

        bool BodyDimensionSourceTargetTable::setData(int row, int column, const QVariant value)
        {
            bool spy = false;
            int role = Qt::UserRole + 1 + column;
            if (setData(index(row, 0), value, role)) {
                spy = true;
                QString curDimension = items.at(row)->m_nameDimension;
                for (int n = 0; n < items.size(); n++) {
                    if (items.at(n)->m_nameDimension == curDimension && n != row)
                        spy = setData(index(n, 0), value, role);
                    if (!spy) return spy;
                }
            }
            return spy;
        }


        void BodyDimensionSourceTargetTable::construct(anthropometricmodel::AnthropoModel& personalizer, std::string const& type) {
            items.clear();
            this->beginResetModel();
            std::set<std::string> const listdim = personalizer.listAnthropoBodyDimension();
            for (auto const& dim : listdim) {
                if ((BodySectionPersonalizing::isSegmentDimensionType(personalizer.getAnthropoBodyDimension(dim)->getType()) && (type == "segment")) ||
                    (!BodySectionPersonalizing::isSegmentDimensionType(personalizer.getAnthropoBodyDimension(dim)->getType()) && (type == "section")))  {
                    std::string const& refsection = personalizer.getAnthropoBodyDimension(dim)->getReferenceAnthropoComponent();
                    AnthropoDimensionType const& type = personalizer.getAnthropoBodyDimension(dim)->getType();
                    double value = personalizer.getAnthropoBodyDimension(dim)->getValue(&Context::instance().project().model());
                    double const& targetvalue = personalizer.getAnthropoBodyDimension(dim)->getTargetValue(&Context::instance().project().model());
                    ModelBodySectionSourceTargetItem* tmp = new ModelBodySectionSourceTargetItem(dim, type, refsection, value, targetvalue);
                    items.append(tmp);
                }
            }
            this->endResetModel();
        }

        Qt::ItemFlags BodyDimensionSourceTargetTable::flags(const QModelIndex &index) const {
            Qt::ItemFlags flags = QAbstractItemModel::flags(index);
            if (index.column() > 0)
                flags |= Qt::ItemIsEditable;
            return flags;
		}

		void BodyDimensionSourceTargetTable::clear() {
			items.clear();
			this->beginResetModel();
			this->endResetModel();
		}

        BodySectionPersonalizing::BodySectionPersonalizing()
            : m_anthropoModel(Context::instance().project().anthropoModel())
            , m_hbmlandmarks(new ListModel())
            , m_hbmentities(new ListModel())
            , m_hbmcontrolpoints(new ListModel())
            , m_antrhopoTree(new AnthropoTree())
            , m_antrhopoTreeProxy(new AnthropoTreeProxy())
            , m_listBodySegment(new ListModel())
            , m_listBodysection(new ListModel())
            , m_entity(new ModelTable())
            , m_modelDimensionSegment(new ModelTableCombo())
            , m_modelDimensionSection(new ModelTableCombo())
            , m_BodySectionSourceTarget(new BodyDimensionSourceTargetTable())
            , m_BodyLengthSourceTarget(new BodyDimensionSourceTargetTable())
            , m_viapoint(new ModelviaPoint())
            , m_currentComponent("")
            , m_anthroDisplay(std::make_shared<AnthropoModelDisplay>())
            , m_assBodycomponentSection(new ModelAssBodySection())
            , m_assBodycomponentSegment(new ModelAssBodySection())
            , m_option(OPTION_SCALING::NONE)
        {
            m_antrhopoTree->update();
            m_antrhopoTreeProxy->setSourceModel(m_antrhopoTree);
            m_antrhopoTreeProxy->setSortRole(AnthropoTree::DisplayRoles::NameRole);
            m_antrhopoTreeProxy->setDynamicSortFilter(true);
            m_antrhopoTreeProxy->sort(0);
			//signals related to hbm/model status
            connect(&Context::instance(), &Context::visDataLoaded, this, &BodySectionPersonalizing::InitModuleData);
            connect(&Context::instance(), &Context::modelUpdated, this, &BodySectionPersonalizing::InitModuleData);

            //coonect model signals to properties changed signals
			connect(this->m_hbmlandmarks, &ListModel::dataChanged, this, &BodySectionPersonalizing::hbmlandmarksChanged);
			connect(this->m_hbmentities, &ListModel::dataChanged, this, &BodySectionPersonalizing::hbmentitiesChanged);
            connect(this->m_hbmcontrolpoints, &ListModel::dataChanged, this, &BodySectionPersonalizing::hbmcontrolpointsChanged);
            connect(this->m_BodySectionSourceTarget, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::bodySectionSourceTargetChanged);
            connect(this->m_BodyLengthSourceTarget, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::bodySegmentSourceTargetChanged);
            connect(this->m_modelDimensionSegment, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::modelDimensionSegmentChanged);
            connect(this->m_modelDimensionSection, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::modelDimensionSectionChanged);
            connect(this->m_assBodycomponentSection, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::modelDimensionCompSectionChanged);
            connect(this->m_assBodycomponentSegment, &BodyDimensionSourceTargetTable::dataChanged, this, &BodySectionPersonalizing::modelDimensionCompSegmentChanged);
           

            connect(this, &BodySectionPersonalizing::anthropoModelChanged, this, &BodySectionPersonalizing::updateCurrentSelection);

            m_anthropoModel.setModel(&Context::instance().project().model());
            m_anthropoModel.setKrigingInterface(m_kriging.getKrigingInterface());

            connect(&m_kriging, &piper::kriging::Kriging::willPerformKriging, this, &BodySectionPersonalizing::updateControlPoints,
                Qt::ConnectionType::DirectConnection); // emitted just before kriging
            connect(&Context::instance(), &Context::historyListChanged, this, &BodySectionPersonalizing::anthropoModelChanged); // emitted after kriging finished
            connect(&Context::instance(), &Context::historyListChanged, this, &BodySectionPersonalizing::updateHBMHeight);

            m_anthroDisplay->init(m_anthropoModel.getKrigingInterface()->getKrigingInterfaceGeodesic(),
                m_anthropoModel.getKrigingInterface()->getKrigingInterfaceEuclidBones(), &Context::instance().display());
            m_kriging.setDisplay(m_anthroDisplay);

            // any change to the model causes resetting of kriging targets, but only some reset the sources (adding/removing ctrl poitns etc.)
            // -> handle those in the AnthropoModel. This connection mainly turns off previews that are no longer valid and invalidates kriging's nugget and decimations
            connect(this, &BodySectionPersonalizing::anthropoModelChanged, &m_kriging, &kriging::Kriging::resetKrigingTargets);
        }

        void BodySectionPersonalizing::updateCurrentSelection() {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent))
                emit this->currentComponentChanged();
            else if (m_anthropoModel.hasAnthropoBodyDimension(m_currentComponent)) {
                emit this->currentBodyDimChanged();
            }
            m_BodyLengthSourceTarget->construct(m_anthropoModel, "segment");
            //emit this->bodySegmentSourceTarget();

            m_BodySectionSourceTarget->construct(m_anthropoModel, "section");
            //emit this->bodySectionSourceTarget();

            m_modelDimensionSegment->clear();
            std::set< std::string> const& list = m_anthropoModel.listAnthropoBodyDimension();
            std::vector< std::string> selected_dim;
            for (auto const& cur : list) {
                if (isSegmentDimensionType(m_anthropoModel.getAnthropoBodyDimension(cur)->getType())) {
                    selected_dim.push_back(cur);
                }
            }
            m_modelDimensionSegment->construct(m_anthropoModel, selected_dim);

            m_modelDimensionSection->clear();
            selected_dim.clear();
            for (auto const& cur : list) {
                if (!isSegmentDimensionType(m_anthropoModel.getAnthropoBodyDimension(cur)->getType())) {
                    selected_dim.push_back(cur);
                }
            }
            m_modelDimensionSection->construct(m_anthropoModel, selected_dim);
            // segment, section list
            std::vector<std::string> vlistseg, vlistsec;
            //vlistseg.push_back("Select");
            std::set<std::string> const vbodysegment = m_anthropoModel.listAnthropoBodySegment();
            vlistseg.insert(vlistseg.end(), vbodysegment.begin(), vbodysegment.end());
            m_listBodySegment->init(vlistseg);
            emit this->listBodySegmentChanged();
            m_listBodysection->clear();
            //vlistsec.push_back("Select");
            std::set<std::string> const& vbodysection = m_anthropoModel.listAnthropoBodySection();
            vlistsec.insert(vlistsec.end(), vbodysection.begin(), vbodysection.end());
            m_listBodysection->init(vlistsec);
            emit this->listBodysectionChanged();


        }

        BodySectionPersonalizing::~BodySectionPersonalizing() {
            //clear body segment
            if (m_hbmlandmarks != nullptr) {
                m_hbmlandmarks->clear();
                delete m_hbmlandmarks;
            }
            if (m_listBodySegment != nullptr) {
                m_listBodySegment->clear();
                delete m_listBodySegment;
            }
            if (m_hbmentities != nullptr) {
                m_hbmentities->clear();
                delete m_hbmentities;
            }
            if (m_listBodysection != nullptr) {
                m_listBodysection->clear();
                delete m_listBodysection;
            }
            if (m_modelDimensionSegment != nullptr) {
                m_modelDimensionSegment->clear();
                delete m_modelDimensionSegment;
            }
            if (m_modelDimensionSection != nullptr) {
                m_modelDimensionSection->clear();
                delete m_modelDimensionSection;
            }
            if (m_BodySectionSourceTarget != nullptr) {
                m_BodySectionSourceTarget->clear();
                delete m_BodySectionSourceTarget;
            }
            if (m_BodyLengthSourceTarget != nullptr) {
                m_BodyLengthSourceTarget->clear();
                delete m_BodyLengthSourceTarget;
            }
            if (m_viapoint != nullptr) {
                m_viapoint->clear();
                delete m_viapoint;
            }
            if (m_assBodycomponentSection != nullptr) {
                m_assBodycomponentSection->clear();
                delete m_assBodycomponentSection;
            }
            if (m_assBodycomponentSegment != nullptr) {
                m_assBodycomponentSegment->clear();
                delete m_assBodycomponentSegment;
            }

            //kriging
            if (m_hbmcontrolpoints != nullptr) {
                m_hbmcontrolpoints->clear();
                delete m_hbmcontrolpoints;
            }
            //if (m_antrhopoTreeProxy != nullptr)
            //    delete m_antrhopoTreeProxy;
            //if (m_antrhopoTree != nullptr) {
            //    delete m_antrhopoTree;
            //}
        }

        void BodySectionPersonalizing::updateControlPoints()
        {
            m_anthropoModel.setGlobalNugget(Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT));
            m_anthropoModel.updateControlPoints();
        }

		QStringList BodySectionPersonalizing::hbmlandmarks() const {
			return m_hbmlandmarks->data();
		}

		QStringList BodySectionPersonalizing::hbmentities() const {
			return m_hbmentities->data();
		}

        QStringList BodySectionPersonalizing::hbmcontrolpoints() const {
            return m_hbmcontrolpoints->data();
        }

        void BodySectionPersonalizing::InitModuleData() {
            if (Context::instance().hasModel()) {
                void(*initModuleDataWrapper)(piper::anthropoPersonalizing::BodySectionPersonalizing*, void (BodySectionPersonalizing::*)())
                    = &piper::wrapClassMethodCall < BodySectionPersonalizing, void (BodySectionPersonalizing::*)() > ;

                Context::instance().performAsynchronousOperation(std::bind(initModuleDataWrapper,
                    this, &piper::anthropoPersonalizing::BodySectionPersonalizing::doInitModuleData),
                    false, false, false, false, false);
            }
            else {
                m_hbmlandmarks->clear();
                m_hbmentities->clear();
                m_hbmcontrolpoints->clear();
                m_currentComponent = "";
                emit this->currentComponentChanged();
                // clean model
                m_viapoint->clear();

                //
                //
                updateModuleGUI();
                Context::instance().display().Refresh();
            }
        }
        
        void BodySectionPersonalizing::doInitModuleData() {
            pInfo() << INFO << "Module Initialisation.";
            //pInfo() << INFO << "Initialisation of body section module.";
            // init list hbm landmarks
            setOperationSignals(true, true);
            hbm::Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();
            std::vector<std::string> listlandmarks;
			for (auto const& cur : landmarks)
				listlandmarks.push_back(cur.first);
			m_hbmlandmarks->init(listlandmarks);
			// init skin entities from hbm
            std::vector<std::string> listentities;
            hbm::Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();
			for (auto const& cur : entities) {
                listentities.push_back(cur.first);
			}
            m_hbmentities->init(listentities);
            // init list of set of control ponts from hbm metadata
            hbm::Metadata::InteractionControlPointCont const& controlpoints = Context::instance().project().model().metadata().interactionControlPoints();
            std::vector<std::string> listcontrolpoints;
            listcontrolpoints.push_back(" ");
            for (auto const& cur : controlpoints)
                listcontrolpoints.push_back(cur.first);
            m_hbmcontrolpoints->init(listcontrolpoints);
            // init current
            m_currentComponent = "";
            // clean model
            try
            {
                m_anthropoModel.check();
            }
            catch (std::runtime_error e)
            {
                pWarning() << "Current Scalable Model is incompatible with the current HBM, the Scalable Model will be unloaded. Error message: " << e.what();
                return;
            }
            
            m_anthropoModel.clearTargetsValue();
            m_anthropoModel.resetModel();
       }

        void BodySectionPersonalizing::importAnthropoModel(QUrl filename){
            if (Context::instance().hasModel()) {
                setOperationSignals(true, true);
                void(*importAnthropoModelWrapper)(piper::anthropoPersonalizing::BodySectionPersonalizing*, void (BodySectionPersonalizing::*)(std::string const&), std::string const&)
                    = &piper::wrapClassMethodCall < BodySectionPersonalizing, void (BodySectionPersonalizing::*)(std::string const&), std::string const& > ;
                Context::instance().performAsynchronousOperation(std::bind(importAnthropoModelWrapper, this,
                    &BodySectionPersonalizing::doImportAnthropoModel, filename.toLocalFile().toStdString()),
                    false, false, false, false, false);
            }
            else {
                pCritical() << QStringLiteral("No model is present: can not import a body section file.");
            }
        }

        void BodySectionPersonalizing::exportAnthropoModel(QUrl filename) {
            setOperationSignals(false, false);
            void(*exportAnthropoModelWrapper)(piper::anthropoPersonalizing::BodySectionPersonalizing*, void (BodySectionPersonalizing::*)(std::string const&)const, std::string const&)
                = &piper::wrapClassMethodCall<BodySectionPersonalizing, void (BodySectionPersonalizing::* )(std::string const&) const, std::string const&>;
            Context::instance().performAsynchronousOperation(std::bind(exportAnthropoModelWrapper, this,
                &BodySectionPersonalizing::doExportAnthropoModel, filename.toLocalFile().toStdString()),
                false, false, false, false, false);
        }

        void BodySectionPersonalizing::doExportAnthropoModel(std::string const& filename) const {
            pInfo() << START << "export Body Section file";
            m_anthropoModel.write(filename);
            pInfo() << DONE;
        }

        void BodySectionPersonalizing::doImportAnthropoModel(std::string const& filename) {
            //init();
            pInfo() << START << "Read Anthropometric Model file";
            validateXml(filename);

            try {
                m_anthropoModel.read(filename);
                std::set<std::string> const& listing = m_anthropoModel.listAnthropoBodySection();
                std::set<std::string> const& listingsegment = m_anthropoModel.listAnthropoBodySegment();

                std::string msgseg;
                if (listingsegment.size() > 0) {
                    for (auto const& cur : listingsegment)
                        msgseg = msgseg + cur + " - ";
                }
                else msgseg = " No body segment defined.";
                pInfo() << INFO << "Body Segments : " << msgseg;

                std::string msg;
                if (listing.size() > 0) {
                    for (auto const& cur : listing)
                        msg = msg + cur + " - ";
                }
                else msg = " No body section defined.";
                pInfo() << INFO << "Body Sections : " << msg;

                //determine value of ANSUR dim
                std::set<std::string> const listingdim = m_anthropoModel.listAnthropoBodyDimension();
                std::string msgdim;
                if (listing.size() > 0) {
                    for (auto const& cur : listing)
                        msgdim = msgdim + cur + " - ";
                }
                else msgdim = " No body Dimension defined.";
                pInfo() << INFO << "Body Dimensions : " << msgdim;

                pInfo() << DONE;
            }
            catch (std::exception const& e) {
                m_anthropoModel.clearAnthropoModel();
                pCritical() << "Reading Anthropomodel: " << e.what();
            }
        }

        AnthropoTreeProxy* BodySectionPersonalizing::anthropoTree() const {
            m_antrhopoTree->update();
            return m_antrhopoTreeProxy;
        }

        ModelTable* BodySectionPersonalizing::entity() const {
            m_entity->clear();
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent))
                m_entity->construct(m_anthropoModel.getAnthropoBodySection(m_currentComponent)->getEntity());
            return m_entity;
        }

        ModelTableCombo* BodySectionPersonalizing::modelDimensionSegment() {
            return m_modelDimensionSegment;
        }

        ModelTableCombo* BodySectionPersonalizing::modelDimensionSection() {
            return m_modelDimensionSection;
        }

        ModelAssBodySection* BodySectionPersonalizing::modelDimensionCompSegment() {
            return m_assBodycomponentSegment;
        }

        ModelAssBodySection* BodySectionPersonalizing::modelDimensionCompSection() {
            return m_assBodycomponentSection;
        }

        ModelviaPoint* BodySectionPersonalizing::viapoint() const{
            m_viapoint->clear();
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent))
                m_viapoint->construct(m_anthropoModel.getAnthropoBodySegment(m_currentComponent));
            return m_viapoint;
        }
        
        QStringList BodySectionPersonalizing::listBodysection() {
            return m_listBodysection->data();
        }

        QStringList BodySectionPersonalizing::listBodySegment() {
            return m_listBodySegment->data();
        }

        QStringList BodySectionPersonalizing::listComponents() const {
            m_anthropoModel.listComponents();
            QStringList list;
            for (auto const& cur : m_anthropoModel.listComponents())
                list.append(QString::fromStdString(cur));      
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {// remove all children of current components
                std::list<std::string> listchild = m_anthropoModel.getAnthropoComponent(m_currentComponent)->getRecursiveChildrenList();
                QStringList list_child;
                list_child.append(QString::fromStdString(m_currentComponent));
                for (auto const& cur : listchild)
                    list_child.append(QString::fromStdString(cur));
                QSet<QString> setdiff = list.toSet().subtract(list_child.toSet());
                list = setdiff.toList();
            }
            list.removeDuplicates();
            list.sort();
            list.prepend("none");
            return list;
        }

        QString BodySectionPersonalizing::currentComponent() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent))
                return QString::fromStdString(m_currentComponent);
            else return "";
        }

        QString BodySectionPersonalizing::currentBodyDim() const {
            if (m_anthropoModel.hasAnthropoBodyDimension(m_currentComponent))
                return QString::fromStdString(m_currentComponent);
            else return "";
        }

        QString BodySectionPersonalizing::currentBodyDimTarget() const {
            if (m_anthropoModel.hasAnthropoBodyDimension(m_currentComponent))
                return QString::fromStdString(m_currentComponent);
            else return "";
        }

        bool BodySectionPersonalizing::isCurrentSegment() const {
            return m_anthropoModel.hasAnthropoBodySegment(m_currentComponent);
        }

        bool BodySectionPersonalizing::isCurrentSection() const {
            return m_anthropoModel.hasAnthropoBodySection(m_currentComponent);
        }

        QString BodySectionPersonalizing::currentComponentType() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent))
                return QString::fromStdString(AnthropoComponentType_str.at(m_anthropoModel.getAnthropoComponent(m_currentComponent)->type()));
            else
                return "";
        }

        QString BodySectionPersonalizing::currentComponentParent() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) &&
                m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                return QString::fromStdString(m_anthropoModel.getAnthropoComponent(m_currentComponent)->getParent()->name());
            }
            else 
                return "none";
        }

        void BodySectionPersonalizing::updateHBMHeight()
        {
            // updaye anthropo height if necessary
            if (m_option == OPTION_SCALING::ISOTROPIC && Context::instance().project().model().metadata().anthropometry().height.isDefined())
                Context::instance().project().model().metadata().anthropometry().height.setValue(m_anthropoModel.getTargetHeight());
        }

        void BodySectionPersonalizing::setCurrentComponent(QString const& componentname) {
            if (m_currentComponent != componentname.toStdString()) {
                if (componentname.toStdString().size() == 0 || m_anthropoModel.hasAnthropoComponent(componentname.toStdString())) {
                    m_currentComponent = componentname.toStdString();
                    emit listComponentsChanged();
                    emit currentComponentChanged();
                }
            }
        }

        void BodySectionPersonalizing::setCurrentComponentType(QString const& componenttype) {
            //if (m_currentComponent != componentname.toStdString()) {
            //    m_currentComponent = componentname.toStdString();
            //    emit currentComponentChanged();
            //}
        }

        void BodySectionPersonalizing::setCurrentComponentParent(QString const& componentparent) {
            std::string newparent = componentparent.toStdString();
            if (newparent != m_currentComponent && m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                AbstractAnthropoComponentNode* node = m_anthropoModel.getAnthropoComponent(m_currentComponent);
                bool spy = false;
                if (node->hasParent() && node->getParent()->name() != newparent) {
                    node->removeParent();
                    spy = true;
                }
                if (newparent != "none" && m_anthropoModel.hasAnthropoComponent(newparent)) {
                    node->setParent(m_anthropoModel.getAnthropoComponent(newparent));
                    spy = true;
                }
                if (spy) {
                    //emit currentComponentChanged();
                    emit anthropoModelChanged();
                }
            }
        }
        
        bool BodySectionPersonalizing::renameComponent(QString const& oldname, QString const& newname) {
            if (oldname != newname && m_anthropoModel.hasAnthropoComponent(oldname.toStdString()) && !m_anthropoModel.hasAnthropoComponent(newname.toStdString())) {
                m_anthropoModel.renameAnthropoComponent(m_anthropoModel.getAnthropoComponent(oldname.toStdString())->type(), oldname.toStdString(), newname.toStdString());
                m_currentComponent = newname.toStdString();
                std::set<std::string> const& rootcomponent = m_anthropoModel.getRootComponentName();
                emit this->anthropoModelChanged();
                return true;
            }
            else return false;
        }

        bool BodySectionPersonalizing::renameBodydim(QString const& oldname, QString const& newname) {
            if (oldname != newname && m_anthropoModel.hasAnthropoBodyDimension(oldname.toStdString()) && !m_anthropoModel.hasAnthropoBodyDimension(newname.toStdString())) {
                m_anthropoModel.renameAnthropoComponent(AnthropoComponentType::DIMENSION, oldname.toStdString(), newname.toStdString());
                m_currentComponent = newname.toStdString();
                if (m_currentComponent.size() > 0) {
                    m_assBodycomponentSection->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
                    m_assBodycomponentSegment->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
                }
                else {
                    m_assBodycomponentSection->clear();
                    m_assBodycomponentSegment->clear();
                }
                emit this->bodySectionSourceTargetChanged();
                emit this->bodySegmentSourceTargetChanged();
                emit this->anthropoModelCleared();
                emit this->anthropoModelChanged();
                return true;
            }
            else return false;
        }

        void BodySectionPersonalizing::deleteComponent(QString const& compname) {
            if (m_anthropoModel.hasAnthropoComponent(compname.toStdString())) {
                AnthropoComponentType typecomp = m_anthropoModel.getAnthropoComponent(compname.toStdString())->type();
                m_anthropoModel.deleteAnthropoComponent(typecomp, compname.toStdString());
                emit currentComponentDeleted();
                setCurrentComponent(QString(""));
                emit anthropoModelChanged();
            }
        }

        void BodySectionPersonalizing::deleteComponentAndChild(QString const& compname) {
            if (m_anthropoModel.hasAnthropoComponent(compname.toStdString())) {
                AnthropoComponentType typecomp = m_anthropoModel.getAnthropoComponent(compname.toStdString())->type();
                std::set<AbstractAnthropoComponentNode*> children = m_anthropoModel.getAnthropoComponent(compname.toStdString())->getChildren();
                std::vector<std::string> childnames;
                for (auto & child : children)
                    childnames.push_back(child->name());
                for (auto & child : childnames) {
                    deleteComponentAndChild( QString::fromStdString(child));
                }
                setCurrentComponent(compname);
                m_anthropoModel.deleteAnthropoComponent(typecomp, compname.toStdString()); 
                emit currentComponentDeleted();
                setCurrentComponent(QString(""));
                emit anthropoModelChanged();
            }
        }

        void BodySectionPersonalizing::deleteBodydim(QString const& sectionname) {
            m_currentComponent = "";
            m_anthropoModel.deleteAnthropoComponent(AnthropoComponentType::DIMENSION, sectionname.toStdString());
            if (m_currentComponent.size() > 0) {
                m_assBodycomponentSection->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
                m_assBodycomponentSegment->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
            }
            else {
                m_assBodycomponentSection->clear();
                m_assBodycomponentSegment->clear();
            }
            emit this->anthropoModelCleared();
            emit this->anthropoModelChanged();
        }

        bool BodySectionPersonalizing::newComponent(QString const& name, QString const& type) {
            if (m_anthropoModel.hasAnthropoComponent(name.toStdString()))
                return false;
            m_anthropoModel.newAnthropoComponent(AbstractAnthropoComponent::AnthropoComponentTypefromString(type.toStdString()), name.toStdString());
            m_currentComponent = name.toStdString();
            //if (m_currentComponent.size() > 0) {
            //    m_assBodycomponentSection->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
            //    m_assBodycomponentSegment->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
            //}
            //else {
            //    m_assBodycomponentSection->clear();
            //    m_assBodycomponentSegment->clear();
            //}
            emit this->listComponentsChanged();
            emit this->anthropoModelChanged();
            return true;
        }
        
        bool BodySectionPersonalizing::newBodydim(QString const& dimname, QString const& type) {
            if (!m_anthropoModel.hasAnthropoBodyDimension(dimname.toStdString())) {
                m_anthropoModel.newAnthropoComponent(AnthropoComponentType::DIMENSION, dimname.toStdString());
                m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->setType(type.toStdString());
                setCurrentComponent(dimname);
                emit this->anthropoModelChanged();
                return true;
            }
            else return false;
        }

		void BodySectionPersonalizing::setCurrentBodyDim(QString const& dimname) {
            if (m_currentComponent != dimname.toStdString()) {
                if (dimname.toStdString().size() == 0 || m_anthropoModel.hasAnthropoBodyDimension(dimname.toStdString())) {
                    m_currentComponent = dimname.toStdString();
                    //update ass model between dimension and model components
                    if (m_currentComponent.size() > 0) {
                        m_assBodycomponentSection->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
                        m_assBodycomponentSegment->construct(m_anthropoModel.getAnthropoBodyDimension(m_currentComponent)->getAnthropoComponent());
                    }
                    else {
                        m_assBodycomponentSection->clear();
                        m_assBodycomponentSegment->clear();
                    }
                    emit anthropoModelChanged();
                }
            }
        }

        void BodySectionPersonalizing::setCurrentBodyDimTarget(QString const& dimname) {
            if (m_currentComponent != dimname.toStdString()) {
                if (dimname.toStdString().size() == 0 || m_anthropoModel.hasAnthropoBodyDimension(dimname.toStdString())) {
                    m_currentComponent = dimname.toStdString();
                    emit currentBodyDimTargetChanged();
                }
            }
        }

        void BodySectionPersonalizing::setBodydimType(QString const& dimname, QString const& dimtype) {
            AnthropoBodyDimension* dimension = m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString());
            if (!dimension->setType(AnthropoBodyDimension::AnthropoBodyDimensionTypefromString(dimtype.toStdString()))) {
                pInfo() << WARNING << "The type of dimension " << dimname.toStdString() << " was not modified.It will overconstrained at least one component in the model.";
                return;
            }
            emit this->bodySectionSourceTargetChanged();
            emit this->bodySegmentSourceTargetChanged();
            emit this->anthropoModelChanged();
            return;
        }

        bool BodySectionPersonalizing::setBodydimRef(QString const& dimname, QString const& dimref) {
            if ((isSegmentDimensionType(m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->getType()) && m_anthropoModel.hasAnthropoBodySegment(dimref.toStdString())) ||
                (!isSegmentDimensionType(m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->getType()) && m_anthropoModel.hasAnthropoBodySection(dimref.toStdString()))) {
                if (!m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->setReferenceAnthropoComponent(
                    m_anthropoModel.getAnthropoComponent(dimref.toStdString()))) {
                    pInfo() << WARNING << "The component " << dimref.toStdString() << " was not added in current dimensions.It will be overconstrained if the dimension " << dimname.toStdString() << " is considered";
                    return false;

                }
                emit this->bodySectionSourceTargetChanged();
                emit this->bodySegmentSourceTargetChanged();
                emit this->anthropoModelChanged();
                return true;
            }
            return false;
        }

        void BodySectionPersonalizing::setBodydimRelative(QString const& dimname, bool const& truefalse) {
            if (m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->isScalingRelative() != truefalse) {
                m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->setScalingRelative(truefalse);
                emit this->bodySectionSourceTargetChanged();
                emit this->bodySegmentSourceTargetChanged();
                emit this->anthropoModelChanged();
            }
        }

        void BodySectionPersonalizing::removeBodydimRef(QString const& dimname) {
            m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->removeReferenceAnthropoComponent();
            emit this->bodySectionSourceTargetChanged();
            emit this->bodySegmentSourceTargetChanged();
            emit this->anthropoModelChanged();
        }

        void BodySectionPersonalizing::setAnthropoCompWeight(QString const& dimname, QString const& compname, const QVariant &weight) {
            std::map<AbstractAnthropoComponentNode*, double> const& bodydimmass = m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->getAnthropoComponent();
            AbstractAnthropoComponentNode* componentptr = m_anthropoModel.getAnthropoComponent(compname.toStdString());
            if (bodydimmass.at(componentptr) != weight.toDouble()) {
                m_anthropoModel.getAnthropoBodyDimension(dimname.toStdString())->setAnthropoComponent(componentptr, weight.toDouble());
                emit this->anthropoModelChanged();
            }
        }

		bool BodySectionPersonalizing::optionCenter() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                return m_anthropoModel.getAnthropoBodySection(m_currentComponent)->hasControlPointatCenter();
            }
			else return false;
		}

		void BodySectionPersonalizing::setOptionCenter(const QVariant &value) {
			if (value == optionCenter())
				return;
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setControlPointatCenter(value.toBool());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
		}

        bool BodySectionPersonalizing::optionMinMaxWidth() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                return m_anthropoModel.getAnthropoBodySection(m_currentComponent)->hasControlPointatMinMaxWidth();
            }
            else return false;
        }

		void BodySectionPersonalizing::setOptionMinMaxWidth(const QVariant &value) {
			if (value == optionMinMaxWidth())
				return;
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setControlPointatMinMaxWidth(value.toBool());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }
        
        bool BodySectionPersonalizing::optionMinMaxDepth() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                return m_anthropoModel.getAnthropoBodySection(m_currentComponent)->hasControlPointatMinMaxDepth();
            }
            else return false;
        }

        void BodySectionPersonalizing::setOptionMinMaxDepth(const QVariant &value) {
			if (value == optionMinMaxDepth())
				return;
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setControlPointatMinMaxDepth(value.toBool());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();

            }
        }

        QString BodySectionPersonalizing::optionAngularPosition() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                QString strvalues = angularPositiontoQstring(m_anthropoModel.getAnthropoBodySection(m_currentComponent)->getcontrolPointsPosition());
                return strvalues;
            }
            else return "";
        }

		void BodySectionPersonalizing::setOptionAngularPosition(const QString &value) {
			if (value == optionAngularPosition())
				return;
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                std::vector<double> newpos = angularPositiontoQstring(value);
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setcontrolPointsPosition(newpos);
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        bool BodySectionPersonalizing::isDefinedFromParent() const  {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) && m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->isDefinedFromParent();
            }
            else return false;
        }
        
        void BodySectionPersonalizing::setIsDefinedFromParent(const bool &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) && m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                if (m_anthropoModel.getAnthropoComponent(m_currentComponent)->isDefinedFromParent() != value) {
                    m_anthropoModel.getAnthropoComponent(m_currentComponent)->setIsDefinedFromParent(value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }

        double BodySectionPersonalizing::position() const  {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) && m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->getPosition();
			}
			else return 0.0;
		}

        bool BodySectionPersonalizing::controlPointOnParent() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) && m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->controlPointOnParent();
            }
            else return false;
        }

        void BodySectionPersonalizing::setControlPointOnParent(const bool &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                AbstractAnthropoComponentNode* node = m_anthropoModel.getAnthropoComponent(m_currentComponent);
                if (node->controlPointOnParent() != value  && node->hasParent()) {
                    node->setControlPointOnParent(value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }

        double BodySectionPersonalizing::useBone() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->getControlPointParameter(anthropometricmodel::AnthropoComponentParam::USE_BONE);
            }
            else return 0.0;
        }

        void BodySectionPersonalizing::setUseBone(const double &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                AbstractAnthropoComponentNode* node = m_anthropoModel.getAnthropoComponent(m_currentComponent);
                if (node->getControlPointParameter(AnthropoComponentParam::USE_BONE) != value) {
                    node->setControlPointParameter(AnthropoComponentParam::USE_BONE, value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }
        double BodySectionPersonalizing::useSkin() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->getControlPointParameter(anthropometricmodel::AnthropoComponentParam::USE_SKIN);
            }
            else return 0.0;
        }

        void BodySectionPersonalizing::setUseSkin(const double &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                AbstractAnthropoComponentNode* node = m_anthropoModel.getAnthropoComponent(m_currentComponent);
                if (node->getControlPointParameter(AnthropoComponentParam::USE_SKIN) != value) {
                    node->setControlPointParameter(AnthropoComponentParam::USE_SKIN, value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }
        bool BodySectionPersonalizing::centerScaleOnParent() const {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent))
                return m_anthropoModel.getAnthropoComponent(m_currentComponent)->centerOnParent();
            else return false;
        }

        void BodySectionPersonalizing::setCenterScaleOnParent(const bool &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent)) {
                AbstractAnthropoComponentNode* node = m_anthropoModel.getAnthropoComponent(m_currentComponent);
                if (node->centerOnParent() != value && node->hasParent()) {
                    node->setCenterOnParent(value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }

        void BodySectionPersonalizing::setPosition(const double &value) {
            if (m_anthropoModel.hasAnthropoComponent(m_currentComponent) && m_anthropoModel.getAnthropoComponent(m_currentComponent)->hasParent()) {
                if (value != m_anthropoModel.getAnthropoComponent(m_currentComponent)->getPosition() && m_anthropoModel.getAnthropoComponent(m_currentComponent)->isDefinedFromParent()) {
                    m_anthropoModel.getAnthropoComponent(m_currentComponent)->setPosition(value);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }
		
		double BodySectionPersonalizing::anglerotationaxe() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                return m_anthropoModel.getAnthropoBodySection(m_currentComponent)->getRotationAngleAxial();
            }
            else return 0.0;
        }

        void BodySectionPersonalizing::setAnglerotationaxe(const double &value) {
			if (value == anglerotationaxe())
				return;
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setRotationAngleAxial(value);
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        double BodySectionPersonalizing::angletilt() const  {
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                return m_anthropoModel.getAnthropoBodySection(m_currentComponent)->getTiltangle();
            }
            else return 0.0;
        }

        void BodySectionPersonalizing::setAngletilt(const double &value) {
			if (value == angletilt())
				return;    
            if (m_anthropoModel.hasAnthropoBodySection(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySection(m_currentComponent)->setTiltangle(value);
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        QString BodySectionPersonalizing::angularPositiontoQstring(std::vector<double> const& values) {
            std::stringstream ss;
            for (auto const& cur : values)
                ss << cur << ";";
            ss << std::endl;
            std::string out = ss.str();
            return QString::fromStdString(out);
        }

        std::vector<double> BodySectionPersonalizing::angularPositiontoQstring(QString const& valuestr) {
            std::vector<double> values;
            std::stringstream ss(valuestr.toStdString());
            double i;
            while (ss >> i) {
                values.push_back(i);
                if (ss.peek() == ';')
                    ss.ignore();
            }
            return values;
        }

        bool BodySectionPersonalizing::addAnthropoComp(QString const& nameBodydim, QString const& compname, const QVariant &weight) {
            AbstractAnthropoComponentNode* componentptr = m_anthropoModel.getAnthropoComponent(compname.toStdString());
            if (m_anthropoModel.getAnthropoBodyDimension(nameBodydim.toStdString())->setAnthropoComponent(componentptr, weight.toDouble())) {
                // 
                emit anthropoModelChanged();
                //if (m_anthropoModel.hasAnthropoBodySegment(compname.toStdString()))
                //    emit this->modelDimensionSegmentChanged();
                //else
                //    emit this->modelDimensionSectionChanged();
                return true;
            }
            else {
                pInfo() << WARNING << "The component " << compname.toStdString() << " was not added.It will be overconstrained if the dimension " << nameBodydim.toStdString() <<  " is considered";
                return false;
            }
        }

        void BodySectionPersonalizing::removeAnthropoComp(QString const& nameBodydim, QString const& compname) {
            m_anthropoModel.getAnthropoBodyDimension(nameBodydim.toStdString())->removeAnthropoComponent(compname.toStdString());
            emit this->anthropoModelChanged();
		}

        void BodySectionPersonalizing::setEntity(QString const& nameBodysection, QString const& nameentity) {
            m_anthropoModel.getAnthropoBodySection(nameBodysection.toStdString())->setEntity(nameentity.toStdString());
            emit this->anthropoModelChanged();
            //emit this->currentComponentChanged();
        }

        void BodySectionPersonalizing::removeEntity(QString const& nameBodysection, QString const& nameentity) {
            m_anthropoModel.getAnthropoBodySection(nameBodysection.toStdString())->removeEntity(nameentity.toStdString());
            emit this->anthropoModelChanged();
            //emit this->currentComponentChanged();
        }

		BodyDimensionSourceTargetTable* BodySectionPersonalizing::bodySectionSourceTarget() {
            return m_BodySectionSourceTarget;
        }

        BodyDimensionSourceTargetTable* BodySectionPersonalizing::bodySegmentSourceTarget() {
            return m_BodyLengthSourceTarget;
        }
        		
        void BodySectionPersonalizing::clearTargets() {
            m_anthropoModel.clearTargetsValue();
        }


        void BodySectionPersonalizing::loadTargets() {
            pInfo() << START << "Load anthropometricDimension targets";
            try {
                clearTargets();
                std::set<std::string> const listdim = m_anthropoModel.listAnthropoBodyDimension();
                Context::instance().project().target().anthropometricDimension;
                // anthropometricDimension target
                std::vector<std::string> missing_dim;
                std::vector<std::string> inconsistent_rel, inconsistent_absolute;
                for (hbm::AnthropometricDimensionTarget const& bodysectiondimtarget : Context::instance().project().target().anthropometricDimension) {
                    if (listdim.find(bodysectiondimtarget.name()) == listdim.end())
                        missing_dim.push_back(bodysectiondimtarget.name());
                    else {
                        if (!m_anthropoModel.getAnthropoBodyDimension(bodysectiondimtarget.name())->setTargetValue(bodysectiondimtarget)) {
                            if (m_anthropoModel.getAnthropoBodyDimension(bodysectiondimtarget.name())->isScalingRelative())
                                inconsistent_rel.push_back(bodysectiondimtarget.name());
                            else
                                inconsistent_absolute.push_back(bodysectiondimtarget.name());
                        }
                    }
                }
                if (missing_dim.size() > 0) {
                    std::string warnmsg;
                    warnmsg += "No body dimension defined on HBM for the target ";
                    for (auto const& curdim : missing_dim) {
                        warnmsg += curdim;
                        warnmsg += " ";
                    }
                    pInfo() << INFO << warnmsg;
                }
                if (inconsistent_rel.size() > 0) {
                    std::string warnmsg;
                    warnmsg += "Dimensions defined as relative with inconsistent target: ";
                    for (auto const& curdim : inconsistent_rel) {
                        warnmsg += curdim;
                        warnmsg += " ";
                    }
                    pInfo() << WARNING << warnmsg;
                }
                if (inconsistent_absolute.size() > 0) {
                    std::string warnmsg;
                    warnmsg += "Dimensions defined as absolute with inconsistent target: ";
                    for (auto const& curdim : inconsistent_absolute) {
                        warnmsg += curdim;
                        warnmsg += " ";
                    }
                    pInfo() << WARNING << warnmsg;
                }
                // ladnmark target
                if (Context::instance().project().target().landmark.size() > 0) {// add a option in GUI to say yes orno i want to use such lmandmark targets for anthropomodel target definition
                    std::list<std::string> missingland = m_anthropoModel.setLandmarksTarget(Context::instance().project().target().landmark);
                    if (missingland.size() > 0) {
                        std::string warnmsg;
                        warnmsg += "Following landmark target doesn't not have equivalent landmarks on current model: ";
                        for (auto const& curland : missingland) {
                            warnmsg += curland;
                            warnmsg += " ";
                        }
                        pInfo() << WARNING << warnmsg;
                    }
                }

                // load target height
                if (Context::instance().project().target().height.size() != 0) {
                    m_anthropoModel.setTargetHeight(Context::instance().project().target().height.begin()->value());
                    emit this->targetHeightChanged();
                }
                pInfo() << DONE;
                emit this->anthropoModelChanged();
            }
            catch (std::exception const& e) {
                pCritical() << "Loadings Targets: " << e.what();
            }

        }

        void BodySectionPersonalizing::loadTargetsFromFile(const QUrl& targetFile)
        {
            void(*loadTargetWrapper)(BodySectionPersonalizing*, void (BodySectionPersonalizing::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall<BodySectionPersonalizing, void (BodySectionPersonalizing::*)(std::string const&), std::string const&>;
            Context::instance().performAsynchronousOperation(
                std::bind(loadTargetWrapper, this, &BodySectionPersonalizing::doLoadTargetsFromFile, targetFile.toLocalFile().toStdString()),
                false, false, false, true, false);
        }

        void BodySectionPersonalizing::doLoadTargetsFromFile(std::string const& targetFile)
        {
            Context::instance().project().loadTarget(targetFile);
            clearTargets();
            loadTargets();
        }

        void BodySectionPersonalizing::saveTargets() const {
            //shortcut
            pInfo() << START << "Save anthropometricDimension targets";
            hbm::TargetList& target = Context::instance().project().target();
            std::set<std::string> listdim = m_anthropoModel.listAnthropoBodyDimension();
            target.anthropometricDimension.clear(); //workaround until we can manage several targetlists
            int count = 0;
            //Dimension targets
            for (auto const& cur : listdim) {
                if (m_anthropoModel.getAnthropoBodyDimension(cur)->isTargetValueDefined()) {
                    hbm::AnthropometricDimensionTarget newtarget = hbm::AnthropometricDimensionTarget();
                    newtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
                    newtarget.setName(cur);
                    newtarget.setValue(m_anthropoModel.getAnthropoBodyDimension(cur)->getTargetValue(&Context::instance().project().model()));
                    target.add(newtarget);
                    count++;
                }
            }
            // landmarks targets
            std::set<std::string> const& listcomp = m_anthropoModel.listAnthropoBodySegment();
            std::set<std::string> listland;
            target.landmark.clear(); //workaround until we can manage several targetlists
            for (auto comp : listcomp) {
                for (auto const& land : m_anthropoModel.getAnthropoComponent(comp)->getLandmarksTarget()) {
                    if (listland.find(land.first) == listland.end()) { //don't add twice
                        hbm::LandmarkTarget newtarget = hbm::LandmarkTarget();
                        newtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
                        newtarget.setName(land.first);
                        hbm::LandmarkTarget::ValueType targetcoord;
                        targetcoord[0] = land.second[0];
                        targetcoord[1] = land.second[1];
                        targetcoord[2] = land.second[2];
                        newtarget.setValue(targetcoord);
                        newtarget.landmark = land.first;
                        target.add(newtarget);
                        count++;
                        listland.insert(land.first);
                    }
                }
            }
            // save free landmarks targets
            std::map<std::string, Landmarkconstraint>const& freelandtargets = m_anthropoModel.getFreeLandmarksTargets();
            for (auto land : freelandtargets) {
                if (listland.find(land.first) == listland.end()) { //don't add twice
                    hbm::LandmarkTarget newtarget = hbm::LandmarkTarget();
                    newtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
                    newtarget.setName(land.first);
                    hbm::LandmarkTarget::ValueType targetcoord;
                    targetcoord[0] = land.second.coord(0);
                    targetcoord[1] = land.second.coord(1);
                    targetcoord[2] = land.second.coord(2);
                    newtarget.setValue(targetcoord);
                    newtarget.setKrigingUseBone(land.second.use_bone);
                    newtarget.setKrigingUseSkin(land.second.use_skin);
                    newtarget.landmark = land.first;
                    target.add(newtarget);
                    count++;
                    listland.insert(land.first);
                }
            }

            // create targets height
            if (m_option == OPTION_SCALING::ISOTROPIC && Context::instance().project().model().metadata().anthropometry().height.isDefined()) {
                if (Context::instance().project().target().height.size() == 0) {
                    hbm::HeightTarget newheighttarget;
                    newheighttarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
                    newheighttarget.setValue(m_anthropoModel.getTargetHeight());
                    target.add(newheighttarget);
                }
                else if (Context::instance().project().target().height.size() == 1) {
                    target.height.begin()->setUnit(Context::instance().project().model().metadata().lengthUnit());
                    target.height.begin()->setValue(m_anthropoModel.getTargetHeight());
                }
            }

            pInfo() << DONE << "saved " << count << " anthropometric dimensions and landmarks targets";

        }

        void BodySectionPersonalizing::validateXml(std::string const& file) {
            static XMLValidationProcess xmllint;
            xmllint.setInput(QString::fromStdString(file), QString::fromStdString("anthropometricModel.dtd"));
            if (!xmllint.canBeStarted())
                throw std::runtime_error("File validity check cannot be started");
            else if (!xmllint.isValid())
                throw std::runtime_error(file + " is not valid\n"
                + xmllint.readAllStandardOutput().constData());
        }

        void BodySectionPersonalizing::setNewTargetValue(QString const& nameBodydim, const QVariant &value) {
            if (m_anthropoModel.hasAnthropoBodyDimension(nameBodydim.toStdString())) {
                if (m_anthropoModel.getAnthropoBodyDimension(nameBodydim.toStdString())->getTargetValue(&Context::instance().project().model()) != value.toDouble()) {
                    m_anthropoModel.getAnthropoBodyDimension(nameBodydim.toStdString())->setScalingRelative(false);
                    m_anthropoModel.getAnthropoBodyDimension(nameBodydim.toStdString())->setTargetValue(value.toDouble());
                    emit this->anthropoModelChanged();
                }
            }
        }
        


        QString BodySectionPersonalizing::getHbmHeight() const {
            std::string valuestr = std::to_string(Context::instance().project().model().metadata().anthropometry().height.value());
            return QString::fromStdString(valuestr);
        }

        void BodySectionPersonalizing::setTargetHeight(const QString &value) {
            if (value.toDouble() == m_anthropoModel.getTargetHeight())
                return;
            m_anthropoModel.setTargetHeight(value.toDouble());
            emit this->targetHeightChanged();
            emit this->anthropoModelChanged();
        }

        QString BodySectionPersonalizing::targetHeight() const {
            return QString::fromStdString(std::to_string(m_anthropoModel.getTargetHeight()));
        }

        QString BodySectionPersonalizing::getValidControlPointName() const {
            Metadata::InteractionControlPointCont & ctrlPtCont = Context::instance().project().model().metadata().interactionControlPoints();
            std::string trame("ControlPoints");
            std::string validname(trame);
            std::vector<std::string> vnames;
            if (ctrlPtCont.size() > 0) {
                for (auto const& cur : ctrlPtCont)
                    vnames.push_back(cur.first);
            }

            piper::hbm::TargetList const& target = Context::instance().project().target();
            if (target.controlPoint.size() > 0) {
                for (auto const& cur : target.controlPoint)
                    vnames.push_back(cur.name());
            }
            getValidName(trame, vnames);
            return QString::fromStdString(trame);
        }

        void BodySectionPersonalizing::saveKrigingControlPoints(const QString &name) {
            void(*saveKrigingControlPointsWrapper)(piper::anthropoPersonalizing::BodySectionPersonalizing*, void (BodySectionPersonalizing::*)(std::string const&), std::string const&)
                = &piper::wrapClassMethodCall<BodySectionPersonalizing, void (BodySectionPersonalizing::*)(std::string const&), std::string const&>;
            Context::instance().performAsynchronousOperation(std::bind(saveKrigingControlPointsWrapper, this,
                &BodySectionPersonalizing::doSaveKrigingControlPoints, name.toStdString()),
                false, false, false, false, false);
        }

        void BodySectionPersonalizing::doSaveKrigingControlPoints(std::string const &name) {
            setOperationSignals(true, false);
            pInfo() << START << "Save Control points source and targets.";
            InteractionControlPoint source = m_anthropoModel.getControlPointsSource();
            InteractionControlPoint target = m_anthropoModel.getControlPointsTarget();
            //
            source.setName(name);
            source.setControlPointRole(InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE);
            HumanBodyModel& hbmref = Context::instance().project().model();
            if (!hbmref.metadata().addInteractionControlPoint(source)) {
                pCritical() << "Control Point metadata with name = " << source.name() << " already exists.";
            }

            // define target
            ControlPoint addtarget;
            VCoord CPs;
            addtarget.setName(name);
            target.getCoordControlPoint(Context::instance().project().model().fem(), CPs);
            addtarget.setValue(CPs);
            addtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
            Context::instance().project().target().add(addtarget);
            pInfo() << DONE << "Saved " << CPs.size() << " control points.";
        }

        void BodySectionPersonalizing::cleanupOperationSignals() {
            disconnect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::updateModuleGUI);
            disconnect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::anthropoModelCleared);
            disconnect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::cleanupOperationSignals);
        }

        void BodySectionPersonalizing::setOperationSignals( bool moduleData, bool anthropomodel) {
            if (moduleData)
                connect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::updateModuleGUI, Qt::ConnectionType::DirectConnection);
            if (anthropomodel)
                connect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::anthropoModelCleared, Qt::ConnectionType::DirectConnection);
            connect(&Context::instance(), &Context::operationFinished, this, &BodySectionPersonalizing::cleanupOperationSignals, Qt::ConnectionType::DirectConnection);
        }

        void BodySectionPersonalizing::updateModuleGUI() {
            // signal to update all models of the module
            emit this->hbmlandmarksChanged();
            emit this->hbmentitiesChanged();
            emit this->hbmcontrolpointsChanged();
            emit this->anthropoModelChanged();
            emit this->currentComponentChanged();

 
            emit this->targetHeightChanged();
        }


        
        bool BodySectionPersonalizing::isSegmentDimensionType(anthropometricmodel::AnthropoDimensionType const& type) {
            std::string type_str = anthropometricmodel::AnthropoDimensionType_str[(unsigned int)type];
            return isSegmentDimensionType(type_str);
        }

        bool BodySectionPersonalizing::isSegmentDimensionType(QString const& type) {
            return isSegmentDimensionType(type.toStdString());
        }

        bool BodySectionPersonalizing::isSegmentDimensionType(std::string const& type) {
            if (type.substr(0, 7) == "SECTION")
                return false;
            return true;
        }

        anthropometricmodel::AnthropoModelDisplay * BodySectionPersonalizing::anthroDisplay()
        {
            return m_anthroDisplay.get();
        }

        kriging::Kriging *BodySectionPersonalizing::kriging()
        {
            return &m_kriging;
        }
        
        void BodySectionPersonalizing::setViapointCP(const QVariant &n, const bool &truefalse) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                if (m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->getControlPointViaPoint((size_t)n.toInt()) != truefalse) {
                    m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->setControlPointViaPoint((size_t)n.toInt(), truefalse);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }

        void BodySectionPersonalizing::setLandmarkCP(const QVariant &n, const bool &truefalse) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                if (m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->getControlPointtLandmark((size_t)n.toInt()) != truefalse) {
                    m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->setControlPointtLandmark((size_t)n.toInt(), truefalse);
                    emit this->anthropoModelChanged();
                    //emit this->currentComponentChanged();
                }
            }
        }

        void BodySectionPersonalizing::addLandmarks(const QVariant &n, QString const& landname) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->addLandmark((size_t)n.toInt(), landname.toStdString());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        void BodySectionPersonalizing::removeLandmarks(const QVariant &n, QString const& landname) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->removeLandmark((size_t)n.toInt(), landname.toStdString());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        void BodySectionPersonalizing::insertViapoint(const QVariant &n) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->insertViapoint((size_t)n.toInt());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        void BodySectionPersonalizing::removeViapoint(const QVariant &n) {
            if (m_anthropoModel.hasAnthropoBodySegment(m_currentComponent)) {
                m_anthropoModel.getAnthropoBodySegment(m_currentComponent)->removeViapoint((size_t)n.toInt());
                emit this->anthropoModelChanged();
                //emit this->currentComponentChanged();
            }
        }

        bool BodySectionPersonalizing::isAnthropoModelDefined() const {
            return Context::instance().project().anthropoModel().isDefined();
        }


        ModelviaPointItem::ModelviaPointItem(QString const& name, QVariant const& cpViaPointRole, QVariant const& cpLandmarkRole, std::set<std::string> const& landmarks)
            : m_name(name)
            , m_cpViaPointRole(cpViaPointRole.toDouble())
            , m_cpLandmarkRole(cpLandmarkRole.toDouble())
            , m_landmarks(new ModelTable(landmarks))
        {}

        int ModelviaPoint::columnCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return 3;
        }

        int ModelviaPoint::rowCount(const QModelIndex &parent) const {
            Q_UNUSED(parent);
            return items.count();
        }

        QVariantMap ModelviaPoint::get(int idx) const {
            QVariantMap map;
            foreach(int k, roleNames().keys()) {
                map[roleNames().value(k)] = data(index(idx, 0), k);
            }
            return map;
        }

        QVariant ModelviaPoint::data(const QModelIndex &index, int role) const {
            switch (role) {
            case NameRole: return items[index.row()]->m_name;
            case cpViaPointRole: return items[index.row()]->m_cpViaPointRole;
            case cpLandmarkRole:
            default:
                return items[index.row()]->m_cpLandmarkRole;
            }
        }

        QHash<int, QByteArray> ModelviaPoint::roleNames() const {
            QHash<int, QByteArray> roles;
            roles[NameRole] = "NameRole";
            roles[cpViaPointRole] = "cpViaPointRole";
            roles[cpLandmarkRole] = "cpLandmarkRole";
            return roles;
        }

        bool ModelviaPoint::setData(const QModelIndex &index, const QVariant &value, int role) {
            switch (role) {
            case NameRole: items[index.row()]->m_name = value.toString(); break;
            case cpViaPointRole: items[index.row()]->m_cpViaPointRole = value.toBool(); break;
            case cpLandmarkRole: items[index.row()]->m_cpLandmarkRole = value.toBool(); break;
            }
            emit dataChanged(index, index);
            return true;
        }

        void ModelviaPoint::construct(anthropometricmodel::AnthropoBodySegment * segment) {
            items.clear();
            this->beginResetModel();
            std::vector<std::set<std::string>> const& landmark_viapoint = segment->getLandmarks();
            for (size_t n = 0; n < landmark_viapoint.size(); ++n) {
                QString name = QString::fromStdString("viapoint_" + std::to_string(n));
                items.append(new ModelviaPointItem(name, segment->getControlPointViaPoint(n), segment->getControlPointtLandmark(n), landmark_viapoint[n]));
            }
            this->endResetModel();
        }

        void ModelviaPoint::clear() {
            items.clear();
            this->beginResetModel();
            this->endResetModel();
        }

        ModelTable* ModelviaPoint::getLandmarksModel(int idx) const {
            if (idx > -1)
                return items[idx]->m_landmarks;
            else
                return nullptr;
        }

    }
}


 
