/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoTree.h"

#include "AnthropoTreeItem.h"
#include "BodySectionPersonalizer/AnthropoModel.h"
#include "common/Context.h"

using namespace piper::anthropometricmodel;

namespace piper {
    namespace anthropoPersonalizing {

#include <QStringList>

        AnthropoTree::AnthropoTree(QObject *parent)
            : QAbstractItemModel(parent)
        {
            QList<QVariant> rootData;
            rootData << "Component" << "Type";
            rootItem = new AnthropoTreeItem(rootData, Context::instance().project().anthropoModel());
            setupModelData(Context::instance().project().anthropoModel(), rootItem);
        }

        AnthropoTree::~AnthropoTree()
        {
            delete rootItem;
        }

        int AnthropoTree::columnCount(const QModelIndex &parent) const
        {
            if (parent.isValid())
                return static_cast<AnthropoTreeItem*>(parent.internalPointer())->columnCount();
            else
                return rootItem->columnCount();
        }

        QVariant AnthropoTree::data(const QModelIndex &index, int role) const
        {
            if (!index.isValid())
                return QVariant();

            if (role != Qt::DisplayRole)
                return QVariant();

            AnthropoTreeItem *item = static_cast<AnthropoTreeItem*>(index.internalPointer());

            return item->data(index.column());
        }

        Qt::ItemFlags AnthropoTree::flags(const QModelIndex &index) const
        {
            if (!index.isValid())
                return 0;

            return QAbstractItemModel::flags(index);
        }

        QVariant AnthropoTree::headerData(int section, Qt::Orientation orientation,
            int role) const
        {
            if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
                return rootItem->data(section);

            return QVariant();
        }

        QModelIndex AnthropoTree::index(int row, int column, const QModelIndex &parent)
            const
        {
            if (!hasIndex(row, column, parent))
                return QModelIndex();

            AnthropoTreeItem *parentItem;

            if (!parent.isValid())
                parentItem = rootItem;
            else
                parentItem = static_cast<AnthropoTreeItem*>(parent.internalPointer());

            AnthropoTreeItem *childItem = parentItem->child(row);
            if (childItem)
                return createIndex(row, column, childItem);
            else
                return QModelIndex();
        }

        QModelIndex AnthropoTree::parent(const QModelIndex &index) const
        {
            if (!index.isValid())
                return QModelIndex();

            AnthropoTreeItem *childItem = static_cast<AnthropoTreeItem*>(index.internalPointer());
            AnthropoTreeItem *parentItem = childItem->parentItem();

            if (parentItem == rootItem)
                return QModelIndex();

            return createIndex(parentItem->row(), 0, parentItem);
        }

        int AnthropoTree::rowCount(const QModelIndex &parent) const
        {
            AnthropoTreeItem *parentItem;
            if (parent.column() > 0)
                return 0;

            if (!parent.isValid())
                parentItem = rootItem;
            else
                parentItem = static_cast<AnthropoTreeItem*>(parent.internalPointer());

            return parentItem->childCount();
        }

        void AnthropoTree::setupModelData(anthropometricmodel::AnthropoModel const& anthropopmodel, AnthropoTreeItem *parent)
        {
            QList<AnthropoTreeItem*> parents;
            parents << parent;

            std::set<std::string> const& rootcomponent = anthropopmodel.getRootComponentName();
            for (auto const& curroot : rootcomponent) {
                AbstractAnthropoComponentNode const* curNode = anthropopmodel.getAnthropoComponent(curroot);
                QList<QVariant> curdata;
                curdata << QString::fromStdString(curNode->name());
                curdata << QString::fromStdString(AnthropoComponentType_str[(unsigned int)curNode->type()]);
                parent->appendChild(new AnthropoTreeItem(curdata, anthropopmodel, parent));
            }

            //int number = 0;

            //while (number < lines.count()) {
            //    int position = 0;
            //    while (position < lines[number].length()) {
            //        if (lines[number].at(position) != ' ')
            //            break;
            //        position++;
            //    }

            //    QString lineData = lines[number].mid(position).trimmed();

            //    if (!lineData.isEmpty()) {
            //        // Read the column data from the rest of the line.
            //        QStringList columnStrings = lineData.split("\t", QString::SkipEmptyParts);
            //        QList<QVariant> columnData;
            //        for (int column = 0; column < columnStrings.count(); ++column)
            //            columnData << columnStrings[column];

            //        if (position > indentations.last()) {
            //            // The last child of the current parent is now the new parent
            //            // unless the current parent has no children.

            //            if (parents.last()->childCount() > 0) {
            //                parents << parents.last()->child(parents.last()->childCount() - 1);
            //                indentations << position;
            //            }
            //        }
            //        else {
            //            while (position < indentations.last() && parents.count() > 0) {
            //                parents.pop_back();
            //                indentations.pop_back();
            //            }
            //        }

            //        // Append a new item to the current parent's list of children.
            //        parents.last()->appendChild(new AnthropoTreeItem(columnData, parents.last()));
            //    }

            //    ++number;
            //}
        }

    }
}
