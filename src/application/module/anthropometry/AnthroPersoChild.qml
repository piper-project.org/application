// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5

import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import QtQml.Models 2.2

import VtkQuick 1.0
import Piper 1.0

import piper.AnthroChildScaling 1.0

ModuleToolWindow
{
    title: "Child Scaling"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

    property alias controlpointset: listControlPoints.currentText;
    property bool sliderDependecyMutex: false

    AnthroChildScaling
    {
        id:myAnthroChildScaling
    }

    function refresh() {
        myAnthroChildScaling.generatePreview() // preview needs to be generated to see target points
        myAnthroChildScaling.displayPreview(previewButton.checked, false)
        myAnthroChildScaling.displayControlPointsSource(listControlPoints.currentText, displaySource.checked, false)
        myAnthroChildScaling.displayControlPointsTarget(listControlPoints.currentText, displayTarget.checked, true)
    }

    onVisibleChanged: {
        if (visible)
            refresh()
        else {
            myAnthroChildScaling.generatePreview() // preview needs to be generated to see target points
            myAnthroChildScaling.displayPreview(previewButton.checked, false)
            myAnthroChildScaling.displayControlPointsSource(listControlPoints.currentText, false, false)
            myAnthroChildScaling.displayControlPointsTarget(listControlPoints.currentText, false, true)
        }
    }

    Connections {
        target: context
        onVisDataLoaded: {
            myAnthroChildScaling.init()
        }
    }
    Connections {
        target: myAnthroChildScaling
        onPreviewModelChanged: refresh()
        onDeformed:
        {
            myModelValidator.text = context.getValidHistoryName("Model_Scaling")
            myModelValidator.open()
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins : itemMargin
        ColumnLayout 
		{
            //anchors.fill: parent
            anchors.margins : itemMargin
			
            SpinSliderComponent 
			{
                id: ageTarget
                title: "Target Age in months"
                Layout.fillWidth : true
                incValue: 1
				minValue: 18
                maxValue: 126
                decimals: 1
                Component.onCompleted: ageTarget.valueComponent = myAnthroChildScaling.getModelAge()
                updateValueWhileDragging: true
                Connections 
				{
                    target: context
                    onMetadataChanged: 
					{
                        ageTarget.valueComponent = myAnthroChildScaling.getModelAge()
                    }
                }
                onValueFinalized: 
                {
                    if (!sliderDependecyMutex)
                    {
                        sliderDependecyMutex = true
                        if (value >= 18 && value < 36)
                            heightTarget.valueComponent = (((value - 18) * 146) / 18) + 806
                        if (overrideAgeLimit.checked)
                        {
                            if (value >= 36 && value <= 216)
                                heightTarget.valueComponent = (((value - 36) * 840) / 180) + 952
                        }
                        else
                        {
                            if (value >= 36 && value <= 126)
                                heightTarget.valueComponent = (((value - 36) * 438) / 90) + 952
                        }
                        sliderDependecyMutex = false
                    }
                    myAnthroChildScaling.setTargetAge(value)
				}	
                onMaxValueChanged:
                {
                    if (maxValue < valueComponent)
                        valueComponent = maxValue
                }
            }

			
			SpinSliderComponent 
			{
                id: heightTarget
                title: "Target Height in mm"
                Layout.fillWidth : true
                incValue: 1
				minValue: 806
                maxValue: 1390
                decimals: 1
                //Component.onCompleted: valueComponent = ssmyAnthroChildScaling.getModelAge()
                updateValueWhileDragging: true
				onValueFinalized:
				{
                    if (!sliderDependecyMutex)
                    {
                        sliderDependecyMutex = true
                        if (value >= 806 && value < 952)
                            ageTarget.valueComponent = (((value - 806) * 18) / 146) + 18
                        if (overrideAgeLimit.checked)
                        {
                            if (value >= 952.1 && value <= 1792)
                                ageTarget.valueComponent = (((value - 952) * 180) / 840) + 36
                        }
                        else
                        {
                            if (value >= 952.1 && value <= 1390)
                                ageTarget.valueComponent = (((value - 952) * 90) / 438) + 36
                        }
                        sliderDependecyMutex = false
                    }
				}
            }
			
            RowLayout 
			{
                Label 
				{
                    Layout.fillWidth : true
                    text: "Set of Control Points"
                    horizontalAlignment: TextInput.AlignHCenter
                }
                ComboBox {
                    id: listControlPoints
                    model: myAnthroChildScaling.getListControlpointset()
                    currentIndex: find(controlpointset)
                    onCurrentIndexChanged: {
                        if (currentIndex > 0) {
                            myAnthroChildScaling.setListControlpointset(textAt(currentIndex))
                            krigingAutoLoaControlPointsName = textAt(currentIndex)
                        }
                        else {
                            krigingAutoLoaControlPointsName = "none"
                            myAnthroChildScaling.setListControlpointset("")
                        }
                    }
                    Connections {
                        target: myAnthroChildScaling
                        onInitialized: {
                            listControlPoints.model = myAnthroChildScaling.getListControlpointset()
                            listControlPoints.currentIndex = listControlPoints.find(controlpointset)
                            if (listControlPoints.currentIndex > 0)
                            {
                                krigingAutoLoaControlPointsName = controlpointset
                                myAnthroChildScaling.setListControlpointset(controlpointset)
                            }
                            else
                                krigingAutoLoaControlPointsName = "none"
                        }
                    }
                }
            }
            CheckBox {
                id:previewButton
                enabled: listControlPoints.currentIndex > 0
                anchors.margins : itemMargin
                Layout.fillWidth : true
                checked: false
                text: qsTr("Preview")
                onCheckedChanged : {
                   myAnthroChildScaling.displayPreview(previewButton.checked, true)
                }
            }
			CheckBox {
                id:materialScalingScript
                Layout.fillWidth : true
                checked: false
                text: qsTr("Material Scaling (Experimental)")
                onCheckedChanged : {
                   myAnthroChildScaling.displayPreview(previewButton.checked, true)
                }
            }
            Button
            {
                id: applyKrigingButton
                action: applyKrigingAction
				text: "Apply"
                anchors.margins : itemMargin
                Layout.fillWidth : true
				onClicked: 
				{
					if(materialScalingScript.checked)
					{	
						var args = [myAnthroChildScaling.getModelAge(),ageTarget.valueComponent]
						pythonScript.setPathUrl("file:///"+context.shareDirPath()+"/octave/ChildScaling/materialScaling.py")
						pythonScript.setArgs(args);
						scripting.runScript()
					}
					else
						applyKrigingAction						
				}	
            }
            Button {
                id: goKrigingButton
                anchors.margins : itemMargin
                Layout.fillWidth : true
                iconSource: "qrc:///icon/arrow-right.png"
                text: "Go to Kriging Module"
                onClicked: krigingAction.trigger();

            }
        }
		ColumnLayout
		{	
			CheckBox
			{
				id: overrideAgeLimit
                checked: false
                text: qsTr("Override Age limit")
				onCheckedChanged:
				{
					if(checked)
					{
						console.warn("If target age is over 10.5 years, the result of the scaling might be unrealistic.")
						ageTarget.maxValue = 216
						heightTarget.maxValue = 1792
					}
					else
					{
						ageTarget.maxValue = 126
						heightTarget.maxValue = 1390
					}
				}
			}
			GroupBox {
				title: "Display Control Points"
				enabled: listControlPoints.currentIndex > 0
				ColumnLayout {
					//anchors.fill: parent
					anchors.margins : itemMargin
					CheckBox {
						id: displaySource
						text: qsTr("Source Control Points")
						Layout.fillWidth: true
						checked: true
						onCheckedChanged: {
							myAnthroChildScaling.displayControlPointsSource(listControlPoints.currentText, checked, true)
						}
					}
					CheckBox {
						id: displayTarget
						text: qsTr("Target Control Points")
						Layout.fillWidth: true
						checked: true
						onCheckedChanged: {
							myAnthroChildScaling.generatePreview() // preview needs to be generated to see target points
							myAnthroChildScaling.displayControlPointsTarget(listControlPoints.currentText, checked, true)
						}
					}
				}
			}
		}
    }

    Action {
        id: applyKrigingAction
        enabled: listControlPoints.currentIndex > 0
        shortcut: StandardKey.Refresh
        onTriggered: {
            myAnthroChildScaling.krige()
        }
    }
    ModelHistoryNameDialog {
        id: myModelValidator
        onAccepted: myAnthroChildScaling.setResultinHistory(text)
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
    }
}
