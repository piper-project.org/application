/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "common/KrigingDisplay.h"

#include "common/AnthropometryPredictor.h"

namespace piper {
    namespace anthropoPersonalizing {
        class BodySectionPersonalizing;
    }

    class AnthropometryModule : public QObject
    {
        Q_OBJECT
        Q_PROPERTY(int testOctaveComputeResult READ testOctaveComputeResult NOTIFY testOctaveComputeResultChanged)

    public:
        AnthropometryModule();
        ~AnthropometryModule();
        int testOctaveComputeResult() const { return m_testOctaveComputeResult; }

        public slots:
        /// \returns return a list containing reference codes of an anthropometric database
        QList<QVariant> getDataBaseMemberList(QString dbName);
        //void checkSelection(QList<QString> liste);
        QList<QString> getSelectedDataBaseMemberInAList(QList<QVariant> selectedElementsArray);
        QVariantList getDataBaseMemberListOfList();
        void checkIfResultFolderExists();
        QList<QVariant> getPredictorListFromCSV();
        QString getAbsoluteScriptPath();
        QString getAbsoluteDatabasePath();
        QString getAbsolutePathFromFile(QUrl filePath);
        void getNumberOfSubject();
        QString returnFolderPathFromFilePath(QString filePath);
        QString returnBaseNameWithoutExtension(QString basenameWithExtension);
    signals:
        void testOctaveComputeResultChanged(int result);
        //void deformed();
        //void anthropoModelChanged();
        //void previewModelChanged();
        void targetsUpdated();

    private:
        void updateTarget();
        int m_testOctaveComputeResult;
        bool m_targetsLoaded = false;
        // sets signals to be emitted after the next Context::operationFinished
        void setOperationSignals(bool targetsUpdated);

        anthropoPersonalizing::BodySectionPersonalizing* m_bodypersonalizer;


        hbm::Nodes hbm_nodes;
        bool previewDone;

        void doInit();
    };


    class AnthroChildScaling : public QObject
    {
        Q_OBJECT


    public:
        AnthroChildScaling();
        ~AnthroChildScaling();

        Q_INVOKABLE double getModelAge() const;
        Q_INVOKABLE QStringList getListControlpointset() const;
        Q_INVOKABLE void setListControlpointset(QString const& setname);
        Q_INVOKABLE void generatePreview();
        Q_INVOKABLE void init();
        Q_INVOKABLE void krige();
        Q_INVOKABLE void setResultinHistory(QString const& historyName);
        Q_INVOKABLE void displayControlPointsSource(QString const& setname, bool const& truefalse, bool refreshDisplay);
        Q_INVOKABLE void displayControlPointsTarget(QString const& setname, bool const& truefalse, bool refreshDisplay);
        Q_INVOKABLE void displayPreview(bool const& truefalse, bool refreshDisplay);
        Q_INVOKABLE void setTargetAge(double targetage);

    private:
        KrigingPiperInterface m_kriging;
        kriging::KrigingDisplay m_krigingdisplay;
        hbm::VCoord coord_source, coord_target_skin, coord_target_inner;
        hbm::VId id_source_skin, id_source_inner; // stores the piper id of nodes
        std::string m_setname;
        std::map<std::string, vtkSmartPointer<vtkPolyData>> m_preview;
        hbm::Nodes preview_source, preview_target, hbm_nodes;
        bool previewDone;
        double m_targetage;

        void doInit();
        void doLoadTargetPoint(std::string const& filename, std::string const& name);
        void doKrige();
        void doSetResultinHistory(std::string const& historyName);

    signals:
        void deformed();
        void previewModelChanged();
        void initialized();

    };

}
