// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5

import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import QtQml.Models 2.2

import VtkQuick 1.0
import Piper 1.0
import Qt.labs.settings 1.0

import Piper 1.0
import piper.AnthropometryModule 1.0
import piper.predictors 1.0

ModuleToolWindow
{
    title: "Create anthropometric targets"
    id: root
	Component.onCompleted: adjustWindowSizeToContent()
	minimumWidth: windowWidth
    minimumHeight: windowHeight

    //Selected items in predictor list
    property var selectedPredictorItemsArray: []
    property var selectedPredictorItemsList: []

	property var selectedMeasureOfInterestItemsArray: []
    property var selectedMeasureOfInterestItemsList: []
	property variant snyderListOfListOfVarByGroups: [[]]

    property AnthropometryModule _myAnthropometryModule

	//Global variables
	property var genderByDataset
	property var selectedDatasetName
	property var ageNameByDataset
	property var raceNameByDataset
	property var windowHeight: 720
	property var windowWidth: 870
	property bool onPopulationPercentage_init: true
	property bool sampleInputType_initbool : true
	property bool sampleOutputCombo_initbool: true
	property var nbPopPercentageTxtFieldInRepeater : 0
	property bool onRaceCombo_init: true
	property bool addSnyderGroup1Variables: false
	property var nbRaceComboInRepeater: 0
	property var nbPredictorColumns: 2

	GridLayout
	{
		id: rootGrid
		columns: 2
		Rectangle
		{
			id: leftRectangle
			width:265
			height: windowHeight
			anchors.top: root.top
			Layout.column: 0
			GridLayout
			{
				id: leftGrid
				columns: 3
				ColumnLayout
				{	
					RowLayout
					{
						Image 
						{
							id: newOrExistingReg_img
							source:  "qrc:///icon/redCircle.png"
							sourceSize.width: 12
							sourceSize.height: 12
							fillMode: Image.Stretch
						}
						Label
						{
							text: "Generate target from:" 
							font.bold: true
						}
					}
					RowLayout
					{
						Rectangle
						{
							width:25
							height:20
						}
						ExclusiveGroup { id: leftPanelOption_RadioGroup }
						ExclusiveGroup { id: newOrExistingRegFile_RadioGroup }
						RadioButton 
						{
							id: generateNewRegFile_radio
							text: "New regression"
							exclusiveGroup: newOrExistingRegFile_RadioGroup
							onClicked:
							{
								datasetOptions.visible = true
								saveRegressionFile_groupBox.visible = true
								selectRegressionFile.visible = false
								existingRegressionFile_textField.text =""
								newOrExistingReg_img.source= "qrc:///icon/redCircle.png"
								checkIfAllInfoDefined()
							}
						}
						RadioButton 
						{
							id: openExistingRegFile_radio
							text: "Existing regression"
							exclusiveGroup: newOrExistingRegFile_RadioGroup 
							onClicked:
							{					
								selectRegressionFile.visible = true
								datasetOptions.visible =false
								saveRegressionFile_groupBox.visible = false
								newOrExistingReg_img.source= "qrc:///icon/redCircle.png"
								checkIfAllInfoDefined()
							}
						}
					}
					RowLayout
					{
						id: selectRegressionFile
						visible: false
						Rectangle
						{
							width:5
							height:20
						}
						Label
						{
							text: "Regression File :"
						}
						TextField
						{
							id: existingRegressionFile_textField 
							onTextChanged: 
							{
								checkIfAllInfoDefined()
							}
						}
						Button
						{
							tooltip: qsTr("Select existing regression file")
                            iconSource: "qrc:///icon/document-open.png"
							onClicked: 
							{
								selectDataset_radio.checked = false
								defineVariables_radio.checked = false
								setPopulationDescriptors_radio.checked = false   
								selectRegressFileDialog.open()
							}
						}
					}
					RowLayout
					{ 
						id: datasetOptions 
						visible: false
						ColumnLayout
						{
							RowLayout
							{
								Rectangle
								{
									width:25
									height:20
								}
								Image 
								{
									id: datasetOptions_img
									source:  "qrc:///icon/redCircle.png"
									sourceSize.width: 12
									sourceSize.height: 12
									fillMode: Image.Stretch
								}
								Label
								{
									text: "Dataset Options:" 
									font.bold: true
								}
							}
							RowLayout
							{
								Rectangle
								{
									width:50
									height:20
								}
                                GroupBoxNoTitle
								{
									ColumnLayout
									{	
                                        anchors.centerIn: parent
										RowLayout
										{
											Layout.row: 0
											Image 
											{
												id: selectDataset_img
												source:  "qrc:///icon/redCircle.png"
												sourceSize.width: 12
												sourceSize.height: 12
												fillMode: Image.Stretch
												Layout.column: 0
											}
											RadioButton 
											{
												id: selectDataset_radio
												text: "Select DataSet"
												Layout.column: 1
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents() 
											}
										}
										RowLayout
										{
											Layout.row: 1
											Image 
											{
												id: defineVariables_img
												source:  "qrc:///icon/redCircle.png"
												Layout.column: 0
												sourceSize.width: 12
												sourceSize.height: 12
												fillMode: Image.Stretch
											}
											RadioButton 
											{
												id: defineVariables_radio
												text: "Select Variables"
												Layout.column: 1
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
										RowLayout
										{
											Layout.row: 2
											Image 
											{
												id: setPopulationDescriptors_img
												source:  "qrc:///icon/redCircle.png"
												Layout.column: 0
												sourceSize.width: 12
												sourceSize.height: 12
												fillMode: Image.Stretch
											}
											RadioButton
											{
												id: setPopulationDescriptors_radio
												text: "Set Population Descriptors"
												Layout.column: 1
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
									}
								}
							}
						}
					}
					RowLayout
					{
						ColumnLayout
						{
							RowLayout
							{
								Image 
								{
									id: targetOptions_img
									source:  "qrc:///icon/redCircle.png"
									sourceSize.width: 12
									sourceSize.height: 12
									fillMode: Image.Stretch
								}
								Label
								{
									text: "Target Options:" 
									font.bold: true
								}
							}
							RowLayout
							{
								Rectangle
								{
									width:25
									height:20
								}														
                                GroupBox//NoTitle
								{
									ColumnLayout
									{	
                                        anchors.centerIn: parent
										RowLayout
										{
											Image 
											{
												id: defineTargetInfo_img
												source:  "qrc:///icon/redCircle.png"
												sourceSize.width: 12
												sourceSize.height: 12
												fillMode: Image.Stretch
											}
											RadioButton 
											{
												id: defineTargetInfo_radio
												text: "Define Target Information"
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
										RowLayout
										{
											id: setPredictorValues_row
											visible: false
											Image 
											{
												id: setPredictorValues_img
												source:  "qrc:///icon/redCircle.png"
												sourceSize.width: 12
												sourceSize.height: 12
												fillMode: Image.Stretch
											}
											RadioButton 
											{
												id: setPredictorvalues_radio
												text: "Set Predictor Values"
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
									}
								}
							}
						}
					}
					RowLayout
					{
						ColumnLayout
						{
							RowLayout
							{
								Image 
								{
									id: outputOptions_img
									source:  "qrc:///icon/redCircle.png"
									sourceSize.width: 12
									sourceSize.height: 12
									fillMode: Image.Stretch
								}
								Label
								{
									text: "Output Options:" 
									font.bold: true
								}
							}
							RowLayout
							{
								Rectangle
								{
									width:25
									height:20
								}
                                GroupBoxNoTitle
								{
									ColumnLayout
									{	
                                        anchors.centerIn: parent
										RowLayout
										{	
											Image 
											{
												id: setTargetFilePath_img
												source:  "qrc:///icon/redCircle.png"
												sourceSize.width: 12												
												sourceSize.height: 12
												fillMode: Image.Stretch


											}
											RadioButton 
											{
												id: setTargetFilePath_radio
												text: "Set Target File Path"
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
										RowLayout
										{	
											id: saveRegressionFile_groupBox
											ExclusiveGroup { id: saveReg_RadioGroup }
											visible: true
											
											Image 
											{
												id: setRegFilePath_img
												source:  "qrc:///icon/redCircle.png"
												sourceSize.width: 12												
												sourceSize.height: 12
												fillMode: Image.Stretch

											}
											RadioButton 
											{
												id: setRegFilePath_radio
												text: "Set Regression File Path"
												exclusiveGroup: leftPanelOption_RadioGroup
												onClicked: showHideComponents()
											}
										}
									}
								}
							}
						}
					}
				}
			}
			RowLayout
			{
				anchors.bottom: leftRectangle.bottom
				anchors.bottomMargin: 20
				anchors.horizontalCenter: leftRectangle.horizontalCenter
				Button
				{
					id: generateTarget_button
					text: "Generate Targets"
					Layout.fillWidth: true
					enabled: false
					Layout.column: 0
					onClicked: sendInputDataToAnthroScript() 
				}
				Button
				{
					id: resetGUI_button
					text: "Reset"
					Layout.fillWidth: true
					iconSource:  "qrc:///icon/view-refresh.png"
					Layout.column: 1
					enabled: true
					onClicked: resetGUI()
				}
			}
		}
		
		Rectangle
		{

			id: rightRectangle
			width:600

			height: windowHeight
			Layout.column: 1
			ScrollView 
			{
				height: rightRectangle.height
				width: rightRectangle.width

				GridLayout
				{
					id: rightGrid
					columns: 3
					Rectangle
					{
						width: 20; height: 1
					}
					ColumnLayout
					{
						id: selectDataset_panel
						visible: false
						Rectangle
						{
							width: 20; height: 1
						}
						RowLayout
						{
							Label
							{
								text: "Select a dataset you want to generate targets from"
								font.bold: true
							}
						}
						RowLayout
						{	
							ExclusiveGroup { id: selectDataset_RadioGroup }
							RadioButton 
							{
								id: selectAnsurDataset_radio
								text: "Adult Dataset (ANSUR)"
								exclusiveGroup: selectDataset_RadioGroup
								Layout.column:0
								onClicked:
								{
									selectedDatasetName= "ANSUR"
									fixedRepeat.model =""

									predictorList.treeView.setDataList(_myAnthropometryModule.getDataBaseMemberList(selectedDatasetName))

									predictorBox.visible= true;
									measureOfInterestBox.visible = false;

									predictorList.clearSelection()
									measureOfInterestList.clearSelection()

									selectedPredictorItemsList = []
									selectedMeasureOfInterestItemsList = []
								
									genderByDataset = "GENDER"
									selectedDatasetName = "ANSUR"
									ageNameByDataset = "AGE-ANSUR88"
									raceNameByDataset = "RACE_OF_SUBJECT-ANSUR88"
									checkIfAllInfoDefined()

                                    useLandmarkscheck.checked = false
								}
							}
							RadioButton 
							{
								id: selectSnyderDataset_radio 
								text: "Child Dataset (SNYDER)"
								exclusiveGroup: selectDataset_RadioGroup
								Layout.column:1
								onClicked:
								{
									selectedDatasetName= "SNYDER"
									fixedRepeat.model =""
									snyderListOfListOfVarByGroups = _myAnthropometryModule.getDataBaseMemberListOfList()


									predictorList.treeView.setDataList(_myAnthropometryModule.getDataBaseMemberList(selectedDatasetName))
									measureOfInterestList.treeView.setDataList(_myAnthropometryModule.getDataBaseMemberList(selectedDatasetName))

									predictorBox.visible= true;
									measureOfInterestBox.visible = true;

									predictorList.clearSelection()
									measureOfInterestList.clearSelection()

									selectedPredictorItemsList = []
									selectedMeasureOfInterestItemsList = []

									genderByDataset = "Gender"
									selectedDatasetName = "SNYDER"
									ageNameByDataset = "Age_in_Years"
									raceNameByDataset= "Race"
									checkIfAllInfoDefined()

                                    useLandmarkscheck.checked = false
								}
							}
							RadioButton 
							{
								id: selectCeesarDataset_radio 
								text: "Adult Dataset (CCTANTHRO)"
								exclusiveGroup: selectDataset_RadioGroup
								Layout.column:2
                                visible: true
								onClicked:
								{
									selectedDatasetName= "CCTANTHRO"
									fixedRepeat.model =""
									//context.logWarning(_myAnthropometryModule.getDataBaseMemberList(selectedDatasetName))
									predictorList.treeView.setDataList(_myAnthropometryModule.getDataBaseMemberList(selectedDatasetName))

									predictorBox.visible= true;
									measureOfInterestBox.visible = false;

									predictorList.clearSelection()
									measureOfInterestList.clearSelection()

									selectedPredictorItemsList = []
									selectedMeasureOfInterestItemsList = []

									checkIfAllInfoDefined()

									genderByDataset = "Gender"
                                    ageNameByDataset = "Age"
									raceNameByDataset= "Race"
								}

							}
						}
					              
                        CheckBox 
                        {
                            id: useLandmarkscheck
                            visible: false //selectCeesarDataset_radio.checked //NOT YET FULLY IMPLEMENTED; WAITING FOR SCRIPTS BY CHRISTOPHE -> make it invisible
                            enabled: context.hasModel
                            text: "Generate regressions for landmarks.\nOption available only for CCTANTHRO dataset and requires to have a model loaded in PIPER application."
//                            ToolTip: "Option available only CCTANTHRO datasets and required to load a model in PIPER application"
                            onClicked: {
                                if (selectCeesarDataset_radio.checked !== true)
                                    useLandmarkscheck.checked = false
                            }
                        }
					}
					RowLayout
					{
						id: selectVariables_panel
						visible: false
						Layout.minimumHeight: windowHeight - 10
						Layout.fillHeight: true
						Rectangle
						{
							width: 20; height: 1
						}
						ColumnLayout 
						{ 
							id: predictorBox
							visible: false
							Label
							{
								text: "Predictor List :"  
								font.bold: true
							}
							EntityTreeBrowser
							{
								id: predictorList
								selectMode: SelectionMode.MultiSelection
								treeView.alternatingRowColors: false
								treeView.onPressAndHold: checkIfAllInfoDefined()
							}
							Connections
							{
								target: predictorList.treeView.selection
								onSelectionChanged:  
								{
									selectedPredictorItemsList = []
																		
									if(selectedDatasetName==="SNYDER")
									{
										var lastItem = predictorList.itemSelected.length-1
										if(predictorList.itemSelected[lastItem]==="Group 1" || predictorList.itemSelected[lastItem]==="Group 2" || predictorList.itemSelected[lastItem]==="Group 3" || predictorList.itemSelected[lastItem]==="Group 4" || predictorList.itemSelected[lastItem]==="Group 5" || predictorList.itemSelected[lastItem]==="Group 6")
											{
												predictorList.itemSelected[lastItem]= ""
											}
									}
									selectedPredictorItemsList=_myAnthropometryModule.getSelectedDataBaseMemberInAList(predictorList.itemSelected)
									checkIfAllInfoDefined()
								}
							}	
							Connections
							{
								target: predictorList.clearButton
								onClicked:  
								{
									predictorList.clearSelection()
									checkIfAllInfoDefined()
								}
							}
						}
						Rectangle
						{
							width: 40; height: 1
						}
						ColumnLayout
						{
							id: measureOfInterestBox
							visible: false
							Label
							{
								text: "Measure Of Interest List :"  
								font.bold: true
							}
							EntityTreeBrowser
							{
								id: measureOfInterestList
								selectMode: SelectionMode.MultiSelection
								treeView.alternatingRowColors: false
								height: 580
								width: 200
							}
							Connections
							{
								target: measureOfInterestList.treeView.selection
								onSelectionChanged:  
								{
									selectedPredictorItemsList = []
									selectedMeasureOfInterestItemsList = []
									if(selectedDatasetName==="SNYDER")
									{
										var lastItem = measureOfInterestList.itemSelected.length-1
										if(measureOfInterestList.itemSelected[lastItem]==="Group 1" || measureOfInterestList.itemSelected[lastItem]==="Group 2" || measureOfInterestList.itemSelected[lastItem]==="Group 3" || measureOfInterestList.itemSelected[lastItem]==="Group 4" || measureOfInterestList.itemSelected[lastItem]==="Group 5" || measureOfInterestList.itemSelected[lastItem]==="Group 6")
											{
												measureOfInterestList.itemSelected[lastItem]= ""
											}
									}
									selectedPredictorItemsList = _myAnthropometryModule.getSelectedDataBaseMemberInAList(predictorList.itemSelected)
									selectedMeasureOfInterestItemsList = _myAnthropometryModule.getSelectedDataBaseMemberInAList(measureOfInterestList.itemSelected)
									checkIfAllInfoDefined()
								}
							}
							Connections
							{
								target: measureOfInterestList.clearButton
								onClicked:  
								{
									measureOfInterestList.clearSelection()
									checkIfAllInfoDefined()
								}
							}
						}
					}
					RowLayout
					{
						id: populationDescriptor_panel
						visible: false
						GroupBox
						{
							id: criteriaBox
							title: "Population descriptor"
							anchors.topMargin: 20
							Label 
							{
								id: nbBinsLabel
								anchors.leftMargin: 10      
								text: "Number of bins :"
							}
							ComboBox
							{
								id: nbBinsCombo
								model: [" ",1,2,3]
								anchors.left: nbBinsLabel.right
								anchors.leftMargin: 10
								onCurrentIndexChanged:
								{   
									nbPopPercentageTxtFieldInRepeater =0
									//nbRaceComboInRepeater =1
									onPopulationPercentage_init= true
									//onRaceCombo_init= true
									if(selectedDatasetName !== "")
										repeatBinsGrid.visible = true;
									else
										repeatBinsGrid.visible = false;
									checkIfAllInfoDefined()	
								}
							}
							Grid
							{
								id: repeatBinsGrid
								rows: 1  
								columns: 3
								anchors.top: nbBinsCombo.bottom
								anchors.topMargin: 10   
								visible: false
								Repeater
								{
									id: rectNbBinsRepeat
									model: nbBinsCombo.currentText
									visible: false
									Rectangle
									{
										id: popCriteriasParams
										width: 185
										height: 120
										Label
										{
											id: popPorcentageLabel
											anchors.top: popCriteriasParams.top
											anchors.topMargin: 14
											anchors.left: popCriteriasParams.left
											anchors.leftMargin: 5
											text: "% of population :"
										}
										TextField 
										{
											id: popPorcentageTxt
											anchors.top: popCriteriasParams.top
											anchors.topMargin: 8
											anchors.left: popPorcentageLabel.right
											anchors.leftMargin: 5
											placeholderText: qsTr("% of population")
											readOnly: false
											 validator: RegExpValidator {
													regExp: /(100(\.[0]{1,2})?|[0-9]{1,2}(\.[0-9]{1,2})?)/
												}
											width: 75
											Component.onCompleted: {
												text= Number((100 / parseFloat(nbBinsCombo.currentText)).toFixed(2));
												nbPopPercentageTxtFieldInRepeater= nbPopPercentageTxtFieldInRepeater+1
												if(nbPopPercentageTxtFieldInRepeater ===nbBinsCombo.currentIndex)
													onPopulationPercentage_init = false
											}
											onTextChanged: {
												if(onPopulationPercentage_init===false){
													if(checkPopEqualHundredPercent())
														checkIfAllInfoDefined()
													else
														console.warn("Be sure that the sum of population % equals 100 !")
												}
											}
										}
										Label
										{
											id: genderLabel
											anchors.top: popPorcentageLabel.bottom
											anchors.topMargin: 14
											anchors.left: popCriteriasParams.left
											anchors.leftMargin: 5
											text: "Gender :"
										}
										ComboBox 
										{
											id: genderCombo
											anchors.top: popPorcentageTxt.bottom
											anchors.topMargin: 8
											anchors.left: popPorcentageLabel.right
											anchors.leftMargin: 5
											currentIndex: 0
											model: [ " ", "Male", "Female" ]
											width: 75
											onCurrentIndexChanged: checkIfAllInfoDefined()
										}
										Label 
										{
											id: raceLabel 
											anchors.top: genderLabel.bottom
											anchors.topMargin: 14
											anchors.left: popCriteriasParams.left
											anchors.leftMargin: 5
											text: "Ethnicity :"
										}
										ComboBox 
										{
											id: raceCombo
											anchors.top: genderCombo.bottom
											anchors.topMargin: 8
											anchors.left: popPorcentageLabel.right
											anchors.leftMargin: 5
											currentIndex: 0
											Component.onCompleted:
											{	
												model: {
													if(selectedDatasetName === "ANSUR")
														model= [ " ", "Hispanic", "Black", "White" , "Asian" ]
													if(selectedDatasetName === "SNYDER")
														model= [ " ", "White", "Black", "Oriental" , "American Indian", "Other" ]
													if(selectedDatasetName === "CCTANTHRO")
														model= [ " "]
													nbRaceComboInRepeater = nbRaceComboInRepeater+1
													if(nbRaceComboInRepeater === nbBinsCombo.currentIndex)
													{
														onRaceCombo_init = false
													}

												}
											}
											width: 75
											onCurrentIndexChanged:	checkIfAllInfoDefined() 
										}
										Label 
										{
											id: ageLabel
											anchors.top: raceLabel.bottom
											anchors.topMargin: 14
											anchors.left: popCriteriasParams.left
											anchors.leftMargin: 5
											text: "Age range :"
										}
										SpinBox 
										{
											id: minAgeRangeVal
											anchors.top: raceCombo.bottom
											anchors.topMargin: 8
											anchors.left: raceCombo.left
											value: minimumValue
											stepSize: 1
											minimumValue:
											{
												if(selectedDatasetName === "ANSUR")
													minimumValue: 17
												if(selectedDatasetName === "SNYDER")
													minimumValue: 1
												if(selectedDatasetName === "CCTANTHRO")
													minimumValue: 55
											}
											maximumValue: maxAgeRangeVal.value-1
										}
										SpinBox 
										{
											id: maxAgeRangeVal
											anchors.top: raceCombo.bottom
											anchors.topMargin: 8
											anchors.right: raceCombo.right
											value: maximumValue
											stepSize: 1
											minimumValue: minAgeRangeVal.value+1
											maximumValue:
											{
												if(selectedDatasetName === "ANSUR")
													maximumValue: 51
												if(selectedDatasetName === "SNYDER")
													maximumValue: 19
												if(selectedDatasetName === "CCTANTHRO")
													maximumValue: 94
											}
										}
									}
								}
							}
						}
					}
					RowLayout
					{
						id: defineSampleInformation_panel
						visible: false
						onVisibleChanged: selectedPredictorItemsArray = predictorList.treeView.getSelectedItems()

						GroupBox
						{
							id: sampleInfoBox
							title: "Target Information :"
							GridLayout
							{
								rows: 8
								columns: 2
								RowLayout 
								{ 
									Layout.row: 0
									Label 
									{
										id: sampleTypeLabel
										Layout.column: 0
										text: "Sample Type :"
										Layout.fillWidth: true
									}
									ComboBox 
									{
										id: sampleTypeCombo
										Layout.column: 1
										currentIndex: -10
										width: 150
										model: [ " ", "MeanOnly", "SampledOutput", "MeanOnly,SampledOutput" ]
										onCurrentIndexChanged:
										{			
											if( currentIndex == 0)
											{
												rectNbInputSamples.visible = false
												nbSamplesFixedValueSlider.value = 0
												checkIfAllInfoDefined() 
											}	
											if( currentIndex == 1)
											{
												rectNbInputSamples.visible= false
												checkIfAllInfoDefined() 
											}
											if( currentIndex == 2)
											{
												rectNbInputSamples.visible =true
												checkIfAllInfoDefined() 
											}
											if( currentIndex == 3)
											{
												rectNbInputSamples.visible =true
												checkIfAllInfoDefined() 
											}
										
										}
									}
								}
								RowLayout
								{
									id: rectNbInputSamples
									visible: false
									Layout.row: 1
									Label
									{
										id: nbSamplesFixedValueLabel
										text: "Number of samples :"
										Layout.column: 0
										Layout.fillWidth: true
									}
									Rectangle
									{
										width: 30
										height: 10
										Layout.column: 1
										TextInput 
										{
											id: nbSamplesFixedValueText
											text: nbSamplesFixedValueSlider.value
											validator: IntValidator { bottom:0; top: 1000}  
											onTextChanged: 
											{
												if(nbSamplesFixedValueText.text =="")
													nbSamplesFixedValueSlider.value = 0
												else
													nbSamplesFixedValueSlider.value = nbSamplesFixedValueText.text
												checkIfAllInfoDefined()
											}             
										}
									}
									Slider 
									{   
										id: nbSamplesFixedValueSlider
										maximumValue: 1000
										stepSize: 1.0
										Layout.maximumWidth: 125
										Layout.column: 2
									}
								}
								RowLayout
								{
									Layout.row: 2
									Label 
									{
										id: sampleInputTypeLabel
										Layout.column: 0
										text: "Sample input type :"
										Layout.fillWidth: true
									}
									ComboBox
									{
										id: sampleInputTypeCombo
										Layout.column: 1
										width: 150
										currentIndex: -10
										model: [ " ", "Fixed Predictor", "Sampled Predictor" ]
										onCurrentIndexChanged:
										{
											if(currentIndex===1){
												setPredictorValues_row.visible = true
												sampledPredictorInput_row.visible= false
												checkIfAllInfoDefined()
											}
											else if(currentIndex===2)
											{
												sampledPredictorInput_row.visible= true
												setPredictorValues_row.visible = false
												checkIfAllInfoDefined()
											}	
											else if(currentIndex===0)
											{
												sampledPredictorInput_row.visible= false
												setPredictorValues_row.visible = false
												checkIfAllInfoDefined()
											}
										}
									}
								}
								RowLayout
								{	
									id: sampledPredictorInput_row
									visible: false
									Layout.row: 3
									GridLayout
									{
										rows: 2
										columns: 5
										RowLayout 
										{ 
											Layout.row: 0
											Label 
											{
												id: sampleInputLabel
												Layout.column: 0
												text: "Sample input type :"
												Layout.fillWidth: true
											}
											ComboBox
											{
												id: sampleInputCombo 
												Layout.column: 1
												width: 150
												currentIndex: 1
												model: [" ", "Normal" ]
												Component.onCompleted: sampleInputType_initbool = false
												onCurrentIndexChanged:
												{
													if(sampleInputType_initbool === false)	
														checkIfAllInfoDefined()
												}
											}
										}
										RowLayout 
										{ 
											Layout.row: 1

											Label
											{
												id: nbSamplesSampledLabel  
												Layout.column: 0
												Layout.fillWidth: true
												text: "Number of samples :"
											}
											Rectangle
											{
												width: 30
												height: 10
												Layout.column: 1
												TextInput 
												{
													id: nbSamplesSampledValueText 
													text: nbSamplesSampledValueSlider.value
													validator: IntValidator { bottom:0; top: 1000}  
													onTextChanged: 
													{   
														checkIfAllInfoDefined() // checkIfGlobalTargetOptionsDefined()
														if(nbSamplesSampledValueText.text =="")
															nbSamplesSampledValueSlider.value = 0
														else
															nbSamplesSampledValueSlider.value = nbSamplesSampledValueText.text
													}
												}
											}
											Slider 
											{
												id: nbSamplesSampledValueSlider 
												Layout.column: 2
												Layout.maximumWidth: 125
												maximumValue: 1000
												stepSize: 1.0
												value: 0
												updateValueWhileDragging : true
											}

										}
									}
								}
								RowLayout
								{
									Layout.row: 4
                        
									RowLayout
									{
										Label 
										{
											id: sampleOutputLabel
											Layout.column: 0
											text: "Sample Output type :"
											Layout.fillWidth: true
										}
										ComboBox
										{
											id: sampleOutputCombo
											Layout.column: 1
											currentIndex: 1
											model: [" ", "Normal"]
											width: 150
											Component.onCompleted: sampleOutputCombo_initbool = false
											onCurrentIndexChanged:
											{
												if(sampleOutputCombo_initbool === false)
													checkIfAllInfoDefined()
											}
										}
									}
								}
							}   
						}
					}
					RowLayout
					{
						id: setPredictorValues_panel
						visible: false
						onVisibleChanged: {
							showHidePredictorsLabels()
							checkIfAllInfoDefined()
						}
						GroupBox
						{
							title: "Set predictors value"
							ColumnLayout
							{
								RowLayout
								{
									Label
									{
										id: definePredictorInfoLabel
										text: "Lengths in Mm, weights in Hg, included in the range [10 : 2500]"
										font.italic: true
									}
								}
								RowLayout
								{
									Label
									{
										id: definePredictorInfoLabel2
										text: "Make sure you use '.' and not ',' for decimal values !"
										font.bold: true
									}
								}
								RowLayout
								{
									GridLayout
									{                    
										RowLayout
										{
											id: repeaterRectRow
											Layout.row:1
											anchors.topMargin: 10
											Grid 
											{
												id: repeaterGrid
												//columns: 4
												Layout.row: 1
												Repeater 
												{ 
													id: fixedRepeat
													Rectangle
													{ 
														id: rectRepeatFixed
														height: 25
														width: 260
														Label
														{
															id: labRepeat
															Layout.fillWidth: true
															anchors.left: rectRepeatFixed.left
															anchors.leftMargin: 10
															font.pixelSize: 10
														}
														TextField 
														{
															placeholderText: qsTr("Predictor input value")
															readOnly: false
															validator: DoubleValidator {decimals: 1;bottom: 0.0; top: 2500.0; notation:DoubleValidator.StandardNotation;}
															
															anchors.left: labRepeat.right
															anchors.leftMargin: 20
															anchors.right: rectRepeatFixed.right
															anchors.rightMargin: 5
															onTextChanged:
															{
																checkIfAllInfoDefined()
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					RowLayout
					{
						id: setTargetFilePath_panel
						visible: false
						Rectangle
						{
							width:5
							height:20
						}
						Label
						{
							text: " save target file in :"
							font.bold: true
						}
						TextField
						{
							id: setTargetFilePath_textField
							text: ""
							placeholderText: qsTr("Target file location")
							onTextChanged: 
							{
								checkIfAllInfoDefined() 
							}
						}
						Button
						{
							tooltip: qsTr("Set target file path")
							iconSource: "qrc:///icon/document-open.png"
							onClicked: selectSampleDir.open()
						}
					}
					RowLayout
					{
						id: setRegressionFilePath_panel
						visible: false
						Rectangle
						{
							width:5
							height:20
						}
						ColumnLayout
						{
							RowLayout
							{
								Label 
								{
									text: "Save Regression ?" 
									font.bold: true
								}
							}
							RowLayout
							{
								RadioButton 
								{
									id: dontSaveRegression_radio
									text: "No"
									exclusiveGroup: saveReg_RadioGroup
									checked: true
									onClicked:
									{
										regressionFilePath_row.visible= false
										checkIfAllInfoDefined()
									}
								}
								RadioButton 
								{
									id: doSaveRegression_radio
									text: "Yes"
									exclusiveGroup: saveReg_RadioGroup
									onClicked:
									{
										regressionFilePath_row.visible= true
										checkIfAllInfoDefined()
									}
								}
							}
							RowLayout
							{
								id: regressionFilePath_row
								visible: false
								Label
								{
									text: " save regression file in :"
									font.bold: true
								}
								TextField
								{
									id: setRegressionFilePath_textField
									text: ""
									placeholderText: qsTr("Regression file location")
									onTextChanged: 
									{
										checkIfAllInfoDefined()
									}
								}
								Button
								{
									tooltip: qsTr("Set regression file path")
									iconSource: "qrc:///icon/document-open.png"
									onClicked: setRegressFileDialog.open()
								}
							}
						}
					}
					RowLayout
					{
						Label
						{
							id: warningSelectDataset_label
							text: "Please, you need to select a dataset before continuing, or load a regression file"  
							font.bold: true
							color: "red"
							visible: false
						}
					}
					RowLayout
					{
						Label
						{
							id: warningSelectPred_label
							text: "No predictors selected! Please, you need to select predictors before continuing"  
							font.bold: true
							color: "red"
							visible: false
						}
					}
				}
			}
		}
	}

	function showHideComponents()
	{	
		if(generateNewRegFile_radio.checked)
		{
			datasetOptions.visible = true
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = false
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = false
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = false
			setRegressionFilePath_panel.visible = false
			warningSelectPred_label.visible = false
		}
		if(openExistingRegFile_radio.checked)
		{
			datasetOptions.visible = false
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = false
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = false
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = false
			setRegressionFilePath_panel.visible = false
			warningSelectPred_label.visible = false
		}
		if(selectDataset_radio.checked)
		{
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = true
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = false
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = false
			setRegressionFilePath_panel.visible = false
			warningSelectPred_label.visible = false

		}
		if(defineVariables_radio.checked)
		{
			if(selectAnsurDataset_radio.checked === false && selectSnyderDataset_radio.checked === false && selectCeesarDataset_radio.checked === false)
				warningSelectDataset_label.visible=true
			else
			{
				warningSelectDataset_label.visible = false
				selectDataset_panel.visible = false
				selectVariables_panel.visible = true
				populationDescriptor_panel.visible = false
				defineSampleInformation_panel.visible = false
				setPredictorValues_panel.visible = false
				setTargetFilePath_panel.visible = false
				setRegressionFilePath_panel.visible = false
				warningSelectPred_label.visible = false
			}
		}
		if(setPopulationDescriptors_radio.checked)
		{
			if(selectAnsurDataset_radio.checked === false && selectSnyderDataset_radio.checked === false  && selectCeesarDataset_radio.checked === false)
				warningSelectDataset_label.visible=true
			else
			{
				warningSelectDataset_label.visible = false
				selectDataset_panel.visible = false
				selectVariables_panel.visible = false
				populationDescriptor_panel.visible = true
				defineSampleInformation_panel.visible = false
				setPredictorValues_panel.visible = false
				setTargetFilePath_panel.visible = false
				setRegressionFilePath_panel.visible = false
				warningSelectPred_label.visible = false
			}
		}
		if(defineTargetInfo_radio.checked)
		{
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = false
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = true
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = false
			setRegressionFilePath_panel.visible = false
			warningSelectPred_label.visible = false
		}
		if(setPredictorvalues_radio.checked)
		{
			if(selectAnsurDataset_radio.checked === false && selectSnyderDataset_radio.checked === false && selectCeesarDataset_radio.checked === false &&   openExistingRegFile_radio.checked === false){
				warningSelectDataset_label.visible = true
			}
			else if((selectedDatasetName === "ANSUR" && predictorList.itemSelected.length===0) || (selectedDatasetName === "SNYDER" && (selectedPredictorItemsList.length ===0 || selectedMeasureOfInterestItemsList.length  ===0 )) || (selectedDatasetName === "CCTANTHRO" && predictorList.itemSelected.length===0))
			{
				warningSelectPred_label.visible= true
			}
			else
			{
				warningSelectDataset_label.visible = false
				selectDataset_panel.visible = false
				selectVariables_panel.visible = false
				populationDescriptor_panel.visible = false
				defineSampleInformation_panel.visible = false
				setPredictorValues_panel.visible = true
				setTargetFilePath_panel.visible = false
				setRegressionFilePath_panel.visible = false
				warningSelectPred_label.visible = false
			}
		}
		if(setTargetFilePath_radio.checked)
		{
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = false
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = false
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = true
			setRegressionFilePath_panel.visible = false
			warningSelectPred_label.visible = false
		}
		if(setRegFilePath_radio.checked)
		{
			warningSelectDataset_label.visible = false
			selectDataset_panel.visible = false
			selectVariables_panel.visible = false
			populationDescriptor_panel.visible = false
			defineSampleInformation_panel.visible = false
			setPredictorValues_panel.visible = false
			setTargetFilePath_panel.visible = false
			setRegressionFilePath_panel.visible = true
			warningSelectPred_label.visible = false
		}
	}

	FileDialog 
    {
        id: selectRegressFileDialog
        title: qsTr("Get regression file...")
        selectFolder: false
		nameFilters: ["mat (*.mat)"]
        onAccepted: 
		{
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
			    settings.recentRegressFolder = folder
			existingRegressionFile_textField.text = selectRegressFileDialog.fileUrl
		}
		onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentRegressFolder))
				folder = settings.recentRegressFolder
		}
    }

	FileDialog 
    {
        id: setRegressFileDialog
        title: qsTr("Set regression file...")
        selectFolder: false
		selectExisting: false
		nameFilters: ["mat (*.mat)"]
        onAccepted: 
        {
            if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
			    settings.recentRegressFolder = folder
			setRegressionFilePath_textField.text = setRegressFileDialog.fileUrl
			checkIfAllInfoDefined()
		}
		onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentRegressFolder))
				folder = settings.recentRegressFolder
		}
    }

	FileDialog 
    {
        id: selectSampleDir
        title: qsTr("Set target file (.ptt) :")
		selectExisting: false
		nameFilters: ["ptt (*.ptt)"]
        onAccepted:
		{
		    if (!context.isUNCPath(folder)) // save only non-UNC paths, storing UNC paths leads to crashes due to qt bug, check https://bugreports.qt.io/browse/QTBUG-63710 if it has been resolved
			    settings.recentTargetFolder = folder
            setTargetFilePath_textField.text = selectSampleDir.fileUrl
			checkIfAllInfoDefined()
		}
		onVisibleChanged:
		{
			if(visible && context.doFolderExist(settings.recentTargetFolder))
				folder = settings.recentTargetFolder
		}	
    }

	function showHidePredictorsLabels()
    {
		// When oppening an existing regression file
        if(openExistingRegFile_radio.checked===true)
        {
            if(existingRegressionFile_textField.text !== "" )
            {
                anthropopredictor.startGetInfo(selectRegressFileDialog.fileUrl)
//                _myAnthropometryModule.extractPredictorsFromRegressionFile(selectRegressFileDialog.fileUrl)
                selectedPredictorItemsList = _myAnthropometryModule.getPredictorListFromCSV()
            }
            else 
            {
                context.logWarning("Please select a regression file")
                return null
            }
        }
        if(selectedPredictorItemsList.length  !==0)
        {
			repeaterGrid.columns = nbPredictorColumns
            fixedRepeat.model = selectedPredictorItemsList.length 
            for(var i=0; i<fixedRepeat.model; ++i)
            {
                fixedRepeat.itemAt(i).children[0].text = selectedPredictorItemsList[i]
            }
        }
        else
        {
            fixedRepeat.model = 0
            repeaterRectRow.visible = false
			showHideComponents() 
        }
    }

	function checkIfDatasetSelected()
	{
		if(generateNewRegFile_radio.checked === true && (selectAnsurDataset_radio.checked === true || selectSnyderDataset_radio.checked === true  || selectCeesarDataset_radio.checked === true))
		{
			selectDataset_img.source = "qrc:///icon/greenCircle.png"
			return true
		}
		else
		{
			selectDataset_img.source = "qrc:///icon/redCircle.png"
			return false
		}
	}


	function checkIfVariablesSelected()
	{
		//console.log("Pred Length : "+predictorList.itemSelected.length)
		//console.log("MOI Length : "+measureOfInterestList.itemSelected.length)
									
		if((selectAnsurDataset_radio.checked === true && predictorList.itemSelected.length !== 0))
		{
			defineVariables_img.source= "qrc:///icon/greenCircle.png"
			return true
		}																								
		else if(selectSnyderDataset_radio.checked === true && selectedPredictorItemsList.length !== 0 && selectedMeasureOfInterestItemsList.length !== 0)
		{
			defineVariables_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else if((selectCeesarDataset_radio.checked === true && predictorList.itemSelected.length !== 0))
		{
			defineVariables_img.source= "qrc:///icon/greenCircle.png"
			return true
		}	
		else
		{
			defineVariables_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfExistingRegFilePathExists() 
	{
		if( openExistingRegFile_radio.checked && existingRegressionFile_textField.text !== "" && context.doFolderExist(_myAnthropometryModule.returnFolderPathFromFilePath(existingRegressionFile_textField.text)))
		{
			newOrExistingReg_img.source= "qrc:///icon/greenCircle.png"		
			return true
		}
		else
			newOrExistingReg_img.source= "qrc:///icon/redCircle.png"		
			return false		
	}


	function checkIfPopulationDefined()
    {
		if(onRaceCombo_init === false)
		{
			if(nbBinsCombo.currentIndex === 0)
			{	
				setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
				return false;
			}
			else if(nbBinsCombo.currentIndex === 1)
			{			
				setPopulationDescriptors_img.source = "qrc:///icon/greenCircle.png"
				return true
			}
			if(nbBinsCombo.currentIndex === 2)
			{
				if((rectNbBinsRepeat.itemAt(0).children[3].currentIndex !== 0 && rectNbBinsRepeat.itemAt(1).children[3].currentIndex === 0) || (rectNbBinsRepeat.itemAt(0).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[3].currentIndex !== 0))
				{
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				if((rectNbBinsRepeat.itemAt(0).children[3].currentIndex ===  rectNbBinsRepeat.itemAt(1).children[3].currentIndex) && ((rectNbBinsRepeat.itemAt(0).children[3].currentIndex>0 && rectNbBinsRepeat.itemAt(1).children[3].currentIndex >0) && (rectNbBinsRepeat.itemAt(0).children[5].currentIndex ===  rectNbBinsRepeat.itemAt(1).children[5].currentIndex)))
				{
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else if((rectNbBinsRepeat.itemAt(0).children[5].currentIndex !== 0 && rectNbBinsRepeat.itemAt(1).children[5].currentIndex === 0) || (rectNbBinsRepeat.itemAt(0).children[5].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[5].currentIndex !== 0)) 
				{
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else if(rectNbBinsRepeat.itemAt(0).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(0).children[5].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[5].currentIndex === 0)
				{	
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else if((rectNbBinsRepeat.itemAt(0).children[3].currentIndex ===  rectNbBinsRepeat.itemAt(1).children[3].currentIndex) || (rectNbBinsRepeat.itemAt(0).children[5].currentIndex ===  rectNbBinsRepeat.itemAt(1).children[5].currentIndex))
				{
					setPopulationDescriptors_img.source = "qrc:///icon/greenCircle.png"
					return true;
				}
				else
				{	
					if(checkPopEqualHundredPercent())
					{
						setPopulationDescriptors_img.source = "qrc:///icon/greenCircle.png"
						return true
					}
				}
			}
			else if(nbBinsCombo.currentIndex === 3 && checkPopEqualHundredPercent())
			{
				if(( rectNbBinsRepeat.itemAt(0).children[3].currentIndex !== 0  && (rectNbBinsRepeat.itemAt(1).children[3].currentIndex === 0 || rectNbBinsRepeat.itemAt(2).children[3].currentIndex === 0)) || (rectNbBinsRepeat.itemAt(1).children[3].currentIndex !== 0 && (rectNbBinsRepeat.itemAt(0).children[3].currentIndex === 0 || rectNbBinsRepeat.itemAt(2).children[3].currentIndex === 0)) || (rectNbBinsRepeat.itemAt(2).children[3].currentIndex !== 0 && (rectNbBinsRepeat.itemAt(0).children[3].currentIndex === 0 || rectNbBinsRepeat.itemAt(1).children[3].currentIndex === 0)) || (rectNbBinsRepeat.itemAt(0).children[3].currentIndex === rectNbBinsRepeat.itemAt(1).children[3].currentIndex && rectNbBinsRepeat.itemAt(0).children[5].currentIndex === rectNbBinsRepeat.itemAt(1).children[5].currentIndex ) || (rectNbBinsRepeat.itemAt(0).children[3].currentIndex === rectNbBinsRepeat.itemAt(2).children[3].currentIndex && rectNbBinsRepeat.itemAt(0).children[5].currentIndex === rectNbBinsRepeat.itemAt(2).children[5].currentIndex ) || (rectNbBinsRepeat.itemAt(1).children[3].currentIndex === rectNbBinsRepeat.itemAt(2).children[3].currentIndex && rectNbBinsRepeat.itemAt(1).children[5].currentIndex === rectNbBinsRepeat.itemAt(2).children[5].currentIndex ))
				{
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else if(( rectNbBinsRepeat.itemAt(0).children[5].currentIndex !== 0  && (rectNbBinsRepeat.itemAt(1).children[5].currentIndex === 0 || rectNbBinsRepeat.itemAt(2).children[5].currentIndex === 0)) || (rectNbBinsRepeat.itemAt(1).children[5].currentIndex !== 0 && (rectNbBinsRepeat.itemAt(0).children[5].currentIndex === 0 || rectNbBinsRepeat.itemAt(2).children[5].currentIndex === 0)) || (rectNbBinsRepeat.itemAt(2).children[5].currentIndex !== 0 && (rectNbBinsRepeat.itemAt(0).children[5].currentIndex === 0 || rectNbBinsRepeat.itemAt(1).children[5].currentIndex === 0)))
				{
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else if(rectNbBinsRepeat.itemAt(0).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(2).children[3].currentIndex === 0 && rectNbBinsRepeat.itemAt(0).children[5].currentIndex === 0 && rectNbBinsRepeat.itemAt(1).children[5].currentIndex === 0 && rectNbBinsRepeat.itemAt(2).children[5].currentIndex === 0) 
				{	
					setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
					return false;
				}
				else
				{		
					setPopulationDescriptors_img.source = "qrc:///icon/greenCircle.png"
					return true
				}
			}
		}
    }

	function checkPopEqualHundredPercent()
	{
		var tempPercentage =0.0
		if(nbBinsCombo.currentIndex > 0)
		{	
			for(var i=0; i<rectNbBinsRepeat.count; ++i)
			{
				if(rectNbBinsRepeat.itemAt(i) !== null)
					tempPercentage += parseFloat(rectNbBinsRepeat.itemAt(i).children[1].text)
			}
			if(Math.round(tempPercentage) === 100)
				return true
			else
			{
				setPopulationDescriptors_img.source = "qrc:///icon/redCircle.png"
				return false
			}
		}
	} 

	function checkIfDatasetOptionsDefined()
	{
		if(checkIfDatasetSelected())
		{
			if(checkIfVariablesSelected() || checkIfPopulationDefined())
			{
				if(checkIfPopulationDefined() && checkIfVariablesSelected())
				{
					datasetOptions_img.source= "qrc:///icon/greenCircle.png"
					newOrExistingReg_img.source= "qrc:///icon/greenCircle.png"
					return true
				}
				else
				{
					datasetOptions_img.source= "qrc:///icon/redCircle.png"
					newOrExistingReg_img.source= "qrc:///icon/redCircle.png"
					return false
				}
			}
			else
			{
				datasetOptions_img.source= "qrc:///icon/redCircle.png"
				newOrExistingReg_img.source= "qrc:///icon/redCircle.png"
				return false
			}
		}
		else
		{
			datasetOptions_img.source= "qrc:///icon/redCircle.png"
			newOrExistingReg_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfTargetOptionsDefined()
	{
		if((sampleTypeCombo.currentIndex >0 && sampleInputTypeCombo.currentIndex ===1 &&  sampleOutputCombo.currentIndex ===1) || (sampleTypeCombo.currentIndex >0 && sampleInputTypeCombo.currentIndex ===2 && sampleInputCombo.currentIndex === 1 && sampleOutputCombo.currentIndex ===1))
		{
			defineTargetInfo_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else
		{
			defineTargetInfo_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}
			
	function checkIfPredictorsDefined()
	{      
		if(predictorList.itemSelected === [] || fixedRepeat.count ===0)
		{
			setPredictorValues_img.source= "qrc:///icon/redCircle.png"
			return false
		}
		for(var i=0; i<fixedRepeat.count; ++i)
        {
			if(fixedRepeat.itemAt(i).children[1].text === "" || fixedRepeat.count ===0)
            {
				setPredictorValues_img.source= "qrc:///icon/redCircle.png"
				return false
            }
        }     
		if( fixedRepeat.count !==0)
		{
			setPredictorValues_img.source= "qrc:///icon/greenCircle.png"   
			return true
		}
	} 
	
	function checkIfGlobalTargetOptionsDefined()
	{
		if((checkIfTargetOptionsDefined() && sampleInputTypeCombo.currentIndex ===1 && checkIfPredictorsDefined()) || (checkIfTargetOptionsDefined() && sampleInputTypeCombo.currentIndex ===2))
		{
			targetOptions_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else
		{
			targetOptions_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfOutputOptionsDefined()
	{
		if((checkIfTargetOutputPathDefined() && checkIfRegressionOutputPathDefined()) || (checkIfRegressionOutputPathDefined() && checkIfTargetOutputPathDefined())) 
		{
			outputOptions_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else
		{
			outputOptions_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfTargetOutputPathDefined()
	{
		if(setTargetFilePath_textField.text!=="" )
		{
			if(context.doFolderExist(_myAnthropometryModule.returnFolderPathFromFilePath(setTargetFilePath_textField.text)))
			{
				setTargetFilePath_img.source= "qrc:///icon/greenCircle.png"
				return true
			}	
		}
		else
		{
			setTargetFilePath_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfRegressionOutputPathDefined()
	{
		if(setRegressionFilePath_textField.text!=="" && context.doFolderExist(_myAnthropometryModule.returnFolderPathFromFilePath(setRegressionFilePath_textField.text)) && doSaveRegression_radio.checked ===true)
		{
			setRegFilePath_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else if(dontSaveRegression_radio.checked === true)
		{
			setRegFilePath_img.source= "qrc:///icon/greenCircle.png"
			return true
		}
		else
		{
			setRegFilePath_img.source= "qrc:///icon/redCircle.png"
			return false
		}
	}

	function checkIfAllInfoDefined()
	{
		if((openExistingRegFile_radio.checked=== false && (checkIfDatasetOptionsDefined()  && checkIfGlobalTargetOptionsDefined() && checkIfOutputOptionsDefined()) || (checkIfOutputOptionsDefined() && checkIfGlobalTargetOptionsDefined() &&checkIfDatasetOptionsDefined()) || (checkIfGlobalTargetOptionsDefined() && checkIfOutputOptionsDefined() &&checkIfDatasetOptionsDefined())))
			generateTarget_button.enabled= true 
		else if((openExistingRegFile_radio.checked && checkIfExistingRegFilePathExists()  && checkIfGlobalTargetOptionsDefined() && checkIfOutputOptionsDefined()) || (openExistingRegFile_radio.checked && checkIfOutputOptionsDefined() && checkIfGlobalTargetOptionsDefined() &&checkIfDatasetOptionsDefined()) || (openExistingRegFile_radio.checked && checkIfGlobalTargetOptionsDefined() && checkIfOutputOptionsDefined() &&checkIfDatasetOptionsDefined()))
			generateTarget_button.enabled= true 
		else
			generateTarget_button.enabled= false
	}


	 property variant userInputArray: [[]]

    //Return an array to c++ with all the input setled in the GUI by the user
    //With the proper format to be easy to port to octave script
    function sendInputDataToAnthroScript() 
    {    
        //Reinitialize global variables
        var nbLines = 0;

        userInputArray = [[]];
		
		userInputArray[nbLines++] =  "Parameter,"+"CodeDir,"+ _myAnthropometryModule.getAbsoluteScriptPath()

		userInputArray[nbLines++] =  "Parameter,"+"DatabaseDir,"+_myAnthropometryModule.getAbsoluteDatabasePath()

		if(openExistingRegFile_radio.checked && existingRegressionFile_textField.text !== "" )
			 userInputArray[nbLines++] = "Parameter,RegressionDir,"+_myAnthropometryModule.getAbsolutePathFromFile(_myAnthropometryModule.returnFolderPathFromFilePath(existingRegressionFile_textField.text));
        else if(doSaveRegression_radio.checked ===true && setRegressionFilePath_textField.text!=="")
            userInputArray[nbLines++] = "Parameter,RegressionDir,"+_myAnthropometryModule.getAbsolutePathFromFile(_myAnthropometryModule.returnFolderPathFromFilePath(setRegressionFilePath_textField.text));
		else
			userInputArray[nbLines++] = "Parameter,RegressionDir,"+context.tempDirectoryPath("Anthro", true);

        if(setTargetFilePath_textField.text != "")
            userInputArray[nbLines++] = "Parameter,SampleDir,"+ _myAnthropometryModule.getAbsolutePathFromFile(_myAnthropometryModule.returnFolderPathFromFilePath(setTargetFilePath_textField.text))
		
        if(selectedDatasetName !== "" && generateNewRegFile_radio.checked == true)
            userInputArray[nbLines++] = "RegressionInformation,AnthropometricDataset,"+selectedDatasetName+",,,,,,,,,"

		if(generateNewRegFile_radio.checked == true)
		{
			//Get list of selected predictors

			selectedPredictorItemsList = _myAnthropometryModule.getSelectedDataBaseMemberInAList(predictorList.treeView.getSelectedItems())

			selectedMeasureOfInterestItemsList = _myAnthropometryModule.getSelectedDataBaseMemberInAList(measureOfInterestList.treeView.getSelectedItems())

			//Get list of all of the measures of interest //default
			var selectedMoiItemsList = []

			selectedMoiItemsList = _myAnthropometryModule.getDataBaseMemberList(selectedDatasetName)

			if(selectedPredictorItemsList.length !== 0 )
			{	
				if(selectedDatasetName === "ANSUR" || selectedDatasetName === "CCTANTHRO")
				{
					for(var i=0; i<selectedPredictorItemsList.length; ++i)
					{
						userInputArray[nbLines++] = "RegressionInformation,Predictor,"+selectedPredictorItemsList[i]+",,,,,,,,,"
					}
				}
				if(selectedDatasetName === "SNYDER")
				{
					/*
					checkIfAddSnyderGroup1Variables()
					if(addSnyderGroup1Variables === true)
					{
						for(var i=1; i<snyderListOfListOfVarByGroups[0].length; ++i)
						{
							userInputArray[nbLines++] = "RegressionInformation,Predictor,"+snyderListOfListOfVarByGroups[0][i]+",Internal,,,,,,,,"
						}
					}
					*/
					for(var i=0; i<selectedPredictorItemsList.length; ++i)
					{
						userInputArray[nbLines++] = "RegressionInformation,Predictor,"+selectedPredictorItemsList[i]+",,,,,,,,,"
					}
				}
			}
			else if(sampleInputTypeCombo.currentIndex === 1)
			{
				for(var i=0; i<fixedRepeat.count; ++i)
				{
					if(fixedRepeat.itemAt(i).children !== null)
					{
						userInputArray[nbLines++] = "RegressionInformation,Predictor,"+fixedRepeat.itemAt(i).children[0].text+",,,,,,,,,"
					}
				}  
			}
			
			//Get all of measure of interest if DB= ANSUR
			if(selectedMoiItemsList.length !== 0 && (selectedDatasetName === "ANSUR" || selectedDatasetName === "CCTANTHRO"))	
				for(var i=0; i<selectedMoiItemsList.length; ++i)
					userInputArray[nbLines++] = "RegressionInformation,MeasurementOfInterest,"+selectedMoiItemsList[i]+",,,,,,,,,"
		
			//Get list of measure of interest selected by user if DB = SNYDER
			if(selectedMoiItemsList.length !== 0 && selectedDatasetName === "SNYDER")
			{
				
				if(addSnyderGroup1Variables === true)
				{
					for(var i=1; i<snyderListOfListOfVarByGroups[0].length; ++i)
					{
						userInputArray[nbLines++] = "RegressionInformation,MeasurementOfInterest,"+snyderListOfListOfVarByGroups[0][i]+",Internal,,,,,,,,"
					}
				}
				
				for(var i=0; i<selectedMeasureOfInterestItemsList.length; ++i)
					userInputArray[nbLines++] = "RegressionInformation,MeasurementOfInterest,"+selectedMeasureOfInterestItemsList[i]+",,,,,,,,,"
			}
			//Population descriptor

			if(nbBinsCombo.currentIndex !== -10 && nbBinsCombo.currentIndex !== 0)
				userInputArray[nbLines++] = "RegressionInformation,PopulationDescriptor,NumberOfBins,"+nbBinsCombo.currentText+",,,,,,"
        
			//Dealing with the criterias in one line
			for(var i=0; i<nbBinsCombo.currentText; ++i)
			{
				var nbCriterias =0;
				var subString = ""
			
				//Write the gender criteria  to substring
				if(rectNbBinsRepeat.itemAt(i).children[3].currentIndex!== 0)
				{
					subString += ",criteria,"+genderByDataset+",value,"+rectNbBinsRepeat.itemAt(i).children[3].currentIndex
					nbCriterias++	
				} 
				//Write the ethnicity criteria  to substring
				if(rectNbBinsRepeat.itemAt(i).children[5].currentIndex !== 0 )
				{
					subString += ",criteria,"+raceNameByDataset+",value,"
					if(selectedDatasetName === "ANSUR")	
					{
						if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "White")
							subString += "1"

						else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Black")
							subString += "2"

						if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Hispanic")
							subString += "3"

						 else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Asian")
							subString += "4"
					}
					else if(selectedDatasetName === "SNYDER")
					{
						if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "White")
							subString += "1"

						else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Black")
							subString += "2"

						else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Oriental")
							subString += "3"

						else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "American Indian")
							subString += "4"

						else if(rectNbBinsRepeat.itemAt(i).children[5].currentText === "Other")
							subString += "5"
					}
					nbCriterias++
				}
				//Write the Age criteria  to substring
				subString += ",criteria,"+ageNameByDataset+",value,"

				var ageRangeStr= "["+rectNbBinsRepeat.itemAt(i).children[7].value+","+rectNbBinsRepeat.itemAt(i).children[8].value+"]"
				subString += '"'+ageRangeStr+'"'

				nbCriterias++

				userInputArray[nbLines++] = "RegressionInformation,PopulationDescriptor,Bin,"+(i+1)+",percentage,"+rectNbBinsRepeat.itemAt(i).children[1].text+",NCriteria,"+nbCriterias+""+subString+",,,"
			}
		}

        if(generateNewRegFile_radio.checked === true)
            userInputArray[nbLines++] = "RegressionInformation,RegressionMatFilename,RegressionAnthroModule.mat,,,,,,,,,"
        else if(openExistingRegFile_radio.checked === true)
            userInputArray[nbLines++] = "RegressionInformation,RegressionMatFilename,"+basename(existingRegressionFile_textField.text)+",,,,,,,,,"
		

        if(sampleTypeCombo.currentIndex !== 0 && setTargetFilePath_textField.text!=="")
        {
            if(sampleTypeCombo.currentIndex === 1) // Mean Only
            {
                userInputArray[nbLines++] = "SampleInformation,SampleOutputType,"+sampleTypeCombo.currentText+",,,,,,,,,"
				userInputArray[nbLines++] = "SampleInformation,SampleMeanOnlyXMLFilename,"+_myAnthropometryModule.returnBaseNameWithoutExtension(basename(setTargetFilePath_textField.text))+"_MeanOnly.ptt,,,,,,,,,"
            }
            if(sampleTypeCombo.currentIndex === 2) // Sampled Output 
            {           
                userInputArray[nbLines++] = "SampleInformation,SampleOutputType,"+sampleTypeCombo.currentText+",,,,,,,,,"                                                        
                userInputArray[nbLines++] = "SampleInformation,SampleSampledOutputXMLFilename,"+_myAnthropometryModule.returnBaseNameWithoutExtension(basename(setTargetFilePath_textField.text))+"_SampledOutput.ptt,,,,,,,,,"
                userInputArray[nbLines++] = "SampleInformation,NbOutputSamples,"+nbSamplesFixedValueSlider.value+",,,,,,,,,"
            }
            if(sampleTypeCombo.currentIndex === 3) // Mean Only and Sampled Output
            {                                 
                userInputArray[nbLines++] = "SampleInformation,SampleOutputType,"+'"'+sampleTypeCombo.currentText+'"'+",,,,,,,,,"                                 
                userInputArray[nbLines++] = "SampleInformation,SampleMeanOnlyXMLFilename,"+_myAnthropometryModule.returnBaseNameWithoutExtension(basename(setTargetFilePath_textField.text))+"_MeanOnly.ptt,,,,,,,,,"
                userInputArray[nbLines++] = "SampleInformation,SampleSampledOutputXMLFilename,"+_myAnthropometryModule.returnBaseNameWithoutExtension(basename(setTargetFilePath_textField.text))+"_SampledOutput.ptt,,,,,,,,,"
                userInputArray[nbLines++] = "SampleInformation,NbOutputSamples,"+nbSamplesFixedValueSlider.value+",,,,,,,,,"
            }
        }
        
        if(sampleInputTypeCombo.currentIndex !== -10 && sampleInputTypeCombo.currentIndex !== 0)
        {         
            //Fixed Predictors
            if(sampleInputTypeCombo.currentIndex === 1)
            {
                userInputArray[nbLines++] = "SampleInformation,SampleInputType,FixedPredictor,,,,,,,,,"
                    
                var substring ="SampleInformation,SampleInputValues"
                for(var i=0; i<fixedRepeat.count; ++i)
                {
                    if(fixedRepeat.itemAt(i).children !== null)
                    {
                        substring += ","+fixedRepeat.itemAt(i).children[0].text+","+fixedRepeat.itemAt(i).children[1].text
                    }
                }        
                userInputArray[nbLines++] = substring
            }
            //Sampled Predictors
            if(sampleInputTypeCombo.currentIndex === 2)
            {
                userInputArray[nbLines++] = "SampleInformation,SampleInputType,SampledPredictor,,,,,,,,,"

                if(sampleInputCombo.currentText === "Normal")
                    userInputArray[nbLines++] = "SampleInformation,SampleInputPDFType,"+sampleInputCombo.currentText+",,,,,,,,,"

                if(nbSamplesSampledValueSlider.value !== 0 && nbSamplesSampledValueText.text !== "")
                    userInputArray[nbLines++] = "SampleInformation,NbInputSamples,"+nbSamplesSampledValueText.text+",,,,,,,,,"
            }
			 
            if(openExistingRegFile_radio.checked === true &&  setRegressionFilePath_textField.text !== "" )                                                      
                userInputArray[nbLines++] = "SampleInformation,SampleInputReferencePopulationMatFilename,"+basename(setRegressionFilePath_textField.text)+",,,,,,,,,"
        }
		
        if(sampleOutputCombo.currentIndex === 1)
            userInputArray[nbLines++] = "SampleInformation,SampleOutputPDFType,"+sampleOutputCombo.currentText+",,,,,,,,," 

		addSnyderGroup1Variables = false

        //Send the full array to C++
        anthropopredictor.startPrediction(context.tempDirectoryPath() + "/anthropoPersoUserInput.csv", userInputArray,useLandmarkscheck.checked);
//        _myAnthropometryModule.CreateTargets(userInputArray);
    }

    function basename(str)
    {
        return (str.slice(str.lastIndexOf("/")+1))
    }

	/*
	///This function was planned to be used to put internal flag on the MOI's if the selection of predictors and MOI's
	///in snyder where not from group 1 and from 2 diferent groups 
	function checkIfAddSnyderGroup1Variables()
	{
		var groupNumber = 0
		var anyoneFromGroupOnePred = false
		var anyoneFromDifferentGroupsPred = false
		var anyoneFromGroupOneMoi = false
		var anyoneFromDifferentGroupsMoi = false

		for(var i=0;i<selectedPredictorItemsList.length; ++i)
		{
			for(var j=0; j<snyderListOfListOfVarByGroups.length; ++j)
			{				
				for(var k=0; k< snyderListOfListOfVarByGroups[j].length; ++k)
				{
					if(selectedPredictorItemsList[i] === snyderListOfListOfVarByGroups[j][k])
					{
						if(groupNumber ===0)
							groupNumber= j+1
						if(groupNumber!= j+1)
						{
							anyoneFromDifferentGroupsPred = true
						}
						groupNumber= j+1
						if(groupNumber ===1)
						{
							anyoneFromGroupOnePred = true
						}
					}	
				}
			}
		}
		groupNumber= 0
		for(var i=0;i<selectedMeasureOfInterestItemsList.length; ++i)
		{
			for(var j=0; j<snyderListOfListOfVarByGroups.length; ++j)
			{				
				for(var k=0; k< snyderListOfListOfVarByGroups[j].length; ++k)
				{
					if(selectedMeasureOfInterestItemsList[i]=== snyderListOfListOfVarByGroups[j][k])
					{
						if(groupNumber ===0)
							groupNumber= j+1
						if(groupNumber!= j+1)
						{
							anyoneFromDifferentGroupsMoi = true
						}
						groupNumber= j+1
						if(groupNumber ===1)
						{
							anyoneFromGroupOneMoi = true
						}
					}	
				}
			}
		}
		if(!anyoneFromGroupOnePred && anyoneFromDifferentGroupsPred && !anyoneFromGroupOneMoi && anyoneFromDifferentGroupsMoi)
		{
			addSnyderGroup1Variables = true
		}
		return 0
	}
	*/

	function resetGUI()
	{		
		//Hide directory lines, and set default value
		generateNewRegFile_radio.checked = false
		openExistingRegFile_radio.checked = false
		existingRegressionFile_textField.text =""

		selectDataset_radio.checked = false
		defineVariables_radio.checked = false
		setPopulationDescriptors_radio.checked = false
		defineTargetInfo_radio.checked = false
		setPredictorvalues_radio.checked = false
		setTargetFilePath_radio.checked = false
		setRegFilePath_radio.checked = false

		
		predictorList.itemSelected = []
		measureOfInterestList.itemSelected = []
		nbBinsCombo.currentIndex = 0
		sampleTypeCombo.currentIndex = 0
		setTargetFilePath_textField.text =""
		dontSaveRegression_radio.checked = true
		setRegressionFilePath_textField.text =""
		sampleInputTypeCombo.currentIndex = 0
		sampleOutputCombo.currentIndex = 0
		selectAnsurDataset_radio.checked = false
		selectSnyderDataset_radio.checked = false
		defineVariables_img.source= "qrc:///icon/redCircle.png"
		setPopulationDescriptors_img.source=  "qrc:///icon/redCircle.png"

		datasetOptions.visible = false
		selectRegressionFile.visible = false
		warningSelectDataset_label.visible = false
		selectDataset_panel.visible = false
		selectVariables_panel.visible = false
		populationDescriptor_panel.visible = false
		defineSampleInformation_panel.visible = false
		setPredictorValues_panel.visible = false
		setTargetFilePath_panel.visible = false
		setRegressionFilePath_panel.visible = false

		checkIfAllInfoDefined()
	}
	
	Settings 
	{
        id: settings
        category: "application"
		property url recentTargetFolder
		property url recentRegressFolder
    }   
}
