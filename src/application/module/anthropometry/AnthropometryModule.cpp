/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropometryModule.h"

#include "hbm/Metadata.h"
#include "kriging/CtrlPtLoader.h"
#include "../BodySectionPersonalizing/BodySectionPersonalizing.h"

#include <QtConcurrent/QtConcurrent>

#include <QMap>
#include <common/helper.h>
#include <common/Context.h>
#include <common/OctaveProcess.h>
#include <common/ChildScaling.h>
#include "common/XMLValidationProcess.h"

#include <vtkPointData.h>

#include <fstream>
#include <sstream>

using namespace std;
using namespace piper::hbm;

namespace piper {

    AnthropometryModule::AnthropometryModule()
        : m_testOctaveComputeResult(0)
        , previewDone(false)
        , m_bodypersonalizer(new anthropoPersonalizing::BodySectionPersonalizing())
    {
        connect(&Context::instance(), &Context::targetChanged, this, &AnthropometryModule::updateTarget);
    }

    AnthropometryModule::~AnthropometryModule() {
        delete m_bodypersonalizer;
    }



    void AnthropometryModule::setOperationSignals(bool targetsUpdated)
    {
        disconnect(&Context::instance(), &Context::operationFinished, 0, 0);
        if (targetsUpdated)
            connect(&Context::instance(), &Context::operationFinished, this, &AnthropometryModule::targetsUpdated);
    }

    QVariantList AnthropometryModule::getDataBaseMemberListOfList()
    {
        QFile file;
        //the output list
        QVariantList list;

        QStringList SnyderlistG1;
        SnyderlistG1.append("Group 1");
        QStringList SnyderlistG2;
        SnyderlistG2.append("Group 2");
        QStringList SnyderlistG3;
        SnyderlistG3.append("Group 3");
        QStringList SnyderlistG4;
        SnyderlistG4.append("Group 4");
        QStringList SnyderlistG5;
        SnyderlistG5.append("Group 5");
        QStringList SnyderlistG6;
        SnyderlistG6.append("Group 6");

        file.setFileName(piper::octaveScriptDirectoryPath() + "/anthropoPerso/dataBase/Dataset_Snyder_1977/Reference_snyder_withBMI_withGroup.csv");

        if (!file.open(QIODevice::ReadOnly)) {
            pInfo() << file.errorString();
        }
        //Go through the database file
        while (!file.atEnd())
        {
            //Read the current line, and split it by ','
            QList<QByteArray> splitedLine = file.readLine().split(',');

            QString firstOfLine = QString::fromStdString(splitedLine.at(0).toStdString()); // First element is variable Name
            QString lastOfLine = QString::fromStdString(splitedLine.at(splitedLine.size() - 1).toStdString()); // last element is variable type

            if (lastOfLine.contains("Numeric"))
            {

                QString groupNumber = QString::fromStdString(splitedLine.at(splitedLine.size() - 2).toStdString());

                if (groupNumber == "1")
                    SnyderlistG1.append(firstOfLine);
                if (groupNumber == "2")
                    SnyderlistG2.append(firstOfLine);
                if (groupNumber == "3")
                    SnyderlistG3.append(firstOfLine);
                if (groupNumber == "4")
                    SnyderlistG4.append(firstOfLine);
                if (groupNumber == "5")
                    SnyderlistG5.append(firstOfLine);
                if (groupNumber == "6")
                    SnyderlistG6.append(firstOfLine);
            }
        }
        list << SnyderlistG1 << SnyderlistG2 << SnyderlistG3 << SnyderlistG4 << SnyderlistG5 << SnyderlistG6;
        return list;
    }

    QVariantList AnthropometryModule::getDataBaseMemberList(QString dbName)
    {
        QFile file;
        //the output list
        QVariantList list;
        QList<QVariant> Ansurlist;

        QList<QVariant> SnyderlistG1;
        SnyderlistG1.append("Group 1");
        QList<QVariant> SnyderlistG2;
        SnyderlistG2.append("Group 2");
        QList<QVariant> SnyderlistG3;
        SnyderlistG3.append("Group 3");
        QList<QVariant> SnyderlistG4;
        SnyderlistG4.append("Group 4");
        QList<QVariant> SnyderlistG5;
        SnyderlistG5.append("Group 5");
        QList<QVariant> SnyderlistG6;
        SnyderlistG6.append("Group 6");
        bool snyderDb = false;

        //The user selected ANSUR Database
        if (dbName == "ANSUR")
        {
            //Open the corresponding database file
            file.setFileName(piper::octaveScriptDirectoryPath() + "/anthropoPerso/dataBase/Dataset_Ansur_1989/Reference_ansur_withBMI.csv");
        }
        else if (dbName == "SNYDER")
        {
            file.setFileName(piper::octaveScriptDirectoryPath() + "/anthropoPerso/dataBase/Dataset_Snyder_1977/Reference_snyder_withBMI_withGroup.csv");
            snyderDb = true;
        }
        else if (dbName == "CCTANTHRO")
        {
            file.setFileName(piper::octaveScriptDirectoryPath() + "/anthropoPerso/dataBase/Dataset_CCTanthro_2017/Reference_cctanthro_withBMI.csv");
        }
        if (!file.open(QIODevice::ReadOnly)) {
            pInfo() << file.errorString();
        }
        //Go through the database file
        while (!file.atEnd())
        {
            //Read the current line, and split it by ','
            QList<QByteArray> splitedLine = file.readLine().split(',');

            QString firstOfLine;
            QString lastOfLine;
            if (dbName == "CCTANTHRO")
            {
                QString firstOfLine2 = QString::fromStdString(splitedLine.at(0).toStdString()); // First element is variable Name
                // Convert to Latin unicode characters
                for (int i = 0; i < firstOfLine2.size(); i++)
                {
                    firstOfLine.append(firstOfLine2.at(i).toLatin1());
                }
                lastOfLine = QString::fromStdString(splitedLine.at(splitedLine.size() - 2).toStdString()); // last element is variable type
            }
            else
            {
                firstOfLine = QString::fromStdString(splitedLine.at(0).toStdString()); // First element is variable Name
                lastOfLine = QString::fromStdString(splitedLine.at(splitedLine.size() - 1).toStdString()); // last element is variable type
            }
            if (lastOfLine.contains("Numeric"))
            {
                if (snyderDb == false)
                    Ansurlist.append(firstOfLine);
                else
                {
                    QString groupNumber = QString::fromStdString(splitedLine.at(splitedLine.size() - 2).toStdString());
                    QVariant qv(firstOfLine);
                    if (groupNumber == "1")
                        SnyderlistG1.append(qv);
                    if (groupNumber == "2")
                        SnyderlistG2.append(firstOfLine);
                    if (groupNumber == "3")
                        SnyderlistG3.append(firstOfLine);
                    if (groupNumber == "4")
                        SnyderlistG4.append(firstOfLine);
                    if (groupNumber == "5")
                        SnyderlistG5.append(firstOfLine);
                    if (groupNumber == "6")
                        SnyderlistG6.append(firstOfLine);
                }
            }
        }
        if (snyderDb == true)
            list << SnyderlistG1 << SnyderlistG2 << SnyderlistG3 << SnyderlistG4 << SnyderlistG5 << SnyderlistG6;
        else
            list.append(Ansurlist);
        
        return list;
    }

    //Transforms a list of QVariants in a list of QStrings and get rid of the empty elements
    QList<QString> AnthropometryModule::getSelectedDataBaseMemberInAList(QList<QVariant> selectedElementsArray)
    {
        QList<QString> result;
        for (int j = 0; j < selectedElementsArray.size(); j++)
        {
            if (selectedElementsArray[j].toString() != "")
            {
                result.append(selectedElementsArray.at(j).toString());
            }
        }
        return result;
    }


    //Check if the result folder and his subfolders (/sample & /regression) exists in the anthropoPerso folder
    //If not it creates these folders
    void AnthropometryModule::checkIfResultFolderExists()
    {
        QString resultFolderPath = QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/result";
        QFileInfo folderPathFI(resultFolderPath);

        QString resultFolderAbsolutePath = folderPathFI.absoluteFilePath();
        QString sampleFolderPath = resultFolderAbsolutePath + "/sample";
        QString regressionFolderPath = resultFolderAbsolutePath + "/regression";

        if (QDir(resultFolderAbsolutePath).exists() == false)
            QDir().mkdir(resultFolderAbsolutePath);

        if (QDir(sampleFolderPath).exists() == false)
            QDir().mkdir(sampleFolderPath);

        if (QDir(regressionFolderPath).exists() == false)
            QDir().mkdir(regressionFolderPath);
    }

    QString AnthropometryModule::returnFolderPathFromFilePath(QString filePath)
    {
        QString result = "";
        QStringList tempList = filePath.split("/");
        for (int i = 0; i < tempList.size() - 1; i++)
        {
            result.append(tempList.at(i));
            result.append("/");
        }
        return result;
    }


    QList<QVariant> AnthropometryModule::getPredictorListFromCSV()
    {
        QList<QVariant> result;
        //Open the corresponding database file
        QFile file(QString::fromStdString(tempDirectoryPath() + "/extractedPredictors.csv"));
        if (!file.open(QIODevice::ReadOnly))
        {
            pInfo() << file.errorString();
        }
        //the output list
        QList<QVariant> list;
        //Go through the database file
        while (!file.atEnd())
        {
            //Read the current line
            QByteArray line = file.readLine();
            result.append(QString::fromStdString(line.toStdString()).simplified());
        }
        return result;
    }

    //This function goal is to go through an octave generated output text file in the temp folder, retrieve the desired informations and print them on screen.
    void AnthropometryModule::getNumberOfSubject()
    {
        //Parameters
        QString totalPop;
        QStringList result;
        QList<double> minResults;

        //Path to the output octave file
        QFile octaveConsoleOutput(QString::fromStdString(tempDirectoryPath() + "/consoleReturns/standardOctaveConsoleOutput.dat"));
        bool firstGroup = true;
        bool error = false;
        int minValueOfBins = 0;

        //If the octave file is accessible
        if (octaveConsoleOutput.open(QIODevice::ReadOnly))
        {
            QTextStream in(&octaveConsoleOutput);
            //Read it
            while (!in.atEnd())
            {
                QString line = in.readLine();   //Line by line
                if (line.contains("size of bins before upsampling"))
                {
                    result = line.split(":").at(2).split(QRegExp("\\s+"), QString::SkipEmptyParts);    //Get bin sizes before upsampling

                    if (firstGroup == true)
                    {
                        for (int e = 0; e < result.length(); e++)
                        {
                            //Write them in totalPop
                            totalPop.append(result.at(e));
                            if (e < result.length() - 1)
                                totalPop.append(", ");
                        }
                        firstGroup = false;
                    }
                    //For the other lines, get the minimal value of size before upsampling
                    else
                    {
                        for (int f = 0; f < result.length(); f++)
                        {
                            if (minValueOfBins >result.at(f).toInt() && minValueOfBins >0)
                                minValueOfBins = result.at(f).toInt();
                            else if (minValueOfBins == 0)
                                minValueOfBins = result.at(f).toInt();
                        }
                    }
                }
                if (line.contains("The population bins are empty."))
                    error = true;
            }
            //end of file
            octaveConsoleOutput.close();
        }
        //If no error was thrawn
        if (error == false)
        {
            //Get the minimal value of the minResults list
            if (totalPop>0)
                pInfo() << "Initial number of subjects in each bin of the master regression: " << totalPop << "\n";
            if (minValueOfBins >0)
                pInfo() << "The minimal subjects number of all bins from every slave regressions: " << minValueOfBins << "\n";
        }
        else
            pWarning() << "Script execution failed, because population bins are empty. Please check the criterias.";
    }

    QString AnthropometryModule::getAbsoluteScriptPath()
    {
        QFileInfo folderPathFI(QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/script");
        return folderPathFI.absoluteFilePath();
    }

    QString AnthropometryModule::getAbsoluteDatabasePath()
    {
        QFileInfo folderPathFI(QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/dataBase");
        return folderPathFI.absoluteFilePath();
    }

    //Function to remove the "file:///" from a qml file path string
    QString AnthropometryModule::getAbsolutePathFromFile(QUrl filePath)
    {
        return filePath.toLocalFile();
    }

    QString AnthropometryModule::returnBaseNameWithoutExtension(QString basenameWithExtension)
    {
        return basenameWithExtension.split(".", QString::SkipEmptyParts).at(0);
    }

    void AnthropometryModule::updateTarget()
    {
        m_bodypersonalizer->loadTargets();
    }

    AnthroChildScaling::AnthroChildScaling()
        : previewDone(false)
        , m_targetage(0.0),
        m_setname("")
    {
        m_krigingdisplay.init(&m_kriging, &m_kriging, &Context::instance().display());
        connect(&Context::instance(), &Context::modelChanged, this, &AnthroChildScaling::doInit);
    }

    AnthroChildScaling::~AnthroChildScaling()
    {
    }

    void AnthroChildScaling::init() {
        doInit();
        //void(*initWrapper)(piper::AnthroChildScaling*, void (piper::AnthroChildScaling::*)())
        //    = &piper::wrapClassMethodCall < piper::AnthroChildScaling, void (piper::AnthroChildScaling::*)() >;
        //Context::instance().performAsynchronousOperation(std::bind(initWrapper, this, &piper::AnthroChildScaling::doInit),
        //    false, false, false, false, false);
    }

    void AnthroChildScaling::doInit()
    {
        hbm_nodes.clear();
        m_kriging.clearSystemMatrix();
        coord_source.clear();
        coord_target_inner.clear();
        coord_target_skin.clear();
        id_source_skin.clear();
        id_source_inner.clear();
        m_setname = "";
        previewDone = false;
        m_krigingdisplay.setSkinPreviewValid(false);
        emit this->initialized();
        if (!previewDone)
            emit this->previewModelChanged();
    }

    double AnthroChildScaling::getModelAge() const {
        //if age metadata is defined, let us it to init target age value
        if (Context::instance().project().model().metadata().anthropometry().age.isDefined()) {
            double age = Context::instance().project().model().metadata().anthropometry().age.value();
            age = units::convert(age, Context::instance().project().model().metadata().anthropometry().age.unit(), units::Age::month);
            return age;
        }
        else {
            if (Context::instance().hasModel())
                pWarning() << "No age is defined for current human body model.";
            return 0.0;
        }
    }

    QStringList AnthroChildScaling::getListControlpointset() const {
        hbm::Metadata::InteractionControlPointCont & ctrlPtCont = Context::instance().project().model().metadata().interactionControlPoints();
        QStringList list;
        list.append("");
        if (ctrlPtCont.size() > 0) {
            for (auto const& cur : ctrlPtCont) {
                list.append(QString::fromStdString(cur.first));
            }
        }
        return list;
    }

    void AnthroChildScaling::setTargetAge(double targetage)
    {
        if (targetage != m_targetage) // if the target actually did not change, do nothing
        {
            m_targetage = targetage;
            if (previewDone)
            {
                previewDone = false;
                generatePreview();
            }
        }
    }

    void AnthroChildScaling::generatePreview() {
        if (!previewDone && coord_source.size() > 0)
        {
            HumanBodyModel& hbmodel = Context::instance().project().model();
            //check if age model is defined
            if (!Context::instance().project().model().metadata().anthropometry().age.isDefined())
                pWarning() << "Age of the current human body model is not defined!";
            double source_age = Context::instance().project().model().metadata().anthropometry().age.value();

            // creta target age
            std::list<hbm::AgeTarget>& agetarget = Context::instance().project().target().age;
            hbm::AgeTarget ageTarget;
            if (agetarget.size() == 0) {
                ageTarget.setUnit(units::Age::month);
                ageTarget.setValue(m_targetage);
                ageTarget.setName("AGE");
                ageTarget.convertToUnit(Context::instance().project().model().metadata().ageUnit());
                agetarget.push_back(ageTarget);
            }
            else {
                agetarget.begin()->setName("AGE");
                agetarget.begin()->setUnit(units::Age::month);
                agetarget.begin()->setValue(m_targetage);
                agetarget.begin()->convertToUnit(Context::instance().project().model().metadata().ageUnit());
                ageTarget = *agetarget.begin();
            }

            // export target in temp directory
            QString targetfilepath = QString::fromStdString(piper::tempDirectoryPath() + "/pipertarget.ptt");
            QFileInfo infofile(targetfilepath);
            QFile file(targetfilepath);
            if (infofile.exists())
                file.remove();
            Context::instance().project().target().write(targetfilepath.toStdString());

            //export current control point source (should be one defined on child for scaling and imported as txt file!!!!)
            QString controlpointfilepath = QString::fromStdString(piper::tempDirectoryPath() + "/controlpointsSource.txt");
            QFileInfo infofileCP(controlpointfilepath);
            QFile fileCP(controlpointfilepath);
            if (infofileCP.exists())
                fileCP.remove();
            ofstream myfile;
            myfile.open(controlpointfilepath.toStdString());

            Metadata::InteractionControlPointCont & ctrlPtCont = hbmodel.metadata().interactionControlPoints();
            // for each source control point subset, write the source to file for the script
            std::string key;
            IdKey idfe;
            std::vector<double> as_skin = ctrlPtCont[m_setname].getAssociationSkin();
            if (as_skin.size() != id_source_inner.size() + id_source_skin.size())
                as_skin.insert(as_skin.begin(), id_source_inner.size(), 0); // if association does not exist, set it to 0 for skin
            int skinIndex = 0, innerIndex = 0;
            for (size_t n = 0; n < as_skin.size(); n++)
            {
                if (as_skin[n] == 1)
                    hbmodel.fem().getInfoId<Node>(id_source_skin[skinIndex++], key, idfe); // get the FE id and write it
                else
                    hbmodel.fem().getInfoId<Node>(id_source_inner[innerIndex++], key, idfe); // get the FE id and write it
                myfile << idfe.id << " " << coord_source[n][0] << " " << coord_source[n][1] << " " << coord_source[n][2] << std::endl;
            }
            myfile.close();
            //path of the output file
            QString outputFilePath = QString::fromStdString(piper::tempDirectoryPath() + "/controlpointsTarget.txt");
            QFileInfo infofiletarget(outputFilePath);
            QFile filetarget(outputFilePath);
            // lanuch octave scripth
            ChildScaling myChildScaling;

            // read target in outputfile and make association
            if (!myChildScaling.compute(source_age, ageTarget.value(), targetfilepath, controlpointfilepath, outputFilePath) || !infofiletarget.exists()) {
                pCritical() << "Target control point file is not created, cannot be loaded!";
                return;
            }
            pInfo() << START << "Generating Preview";

            doLoadTargetPoint(outputFilePath.toStdString(), m_setname);

            try
            {
                double nug = hbmodel.metadata().interactionControlPoint(m_setname)->getGlobalWeight();
                if (std::isnan(nug))
                    nug = Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT);
                if (coord_target_skin.empty())
                {
                    m_kriging.setControlPoints(coord_source, coord_target_inner, nug,
                        Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT),
                        Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT));
                    m_kriging.compute();
                }
                else // setup skin kriging for the preview
                {
                    vtkSmartPointer<vtkPolyData> skin = hbmodel.fem().getFEModelVTK()->getSkin(false, hbmodel.metadata().entities(), hbmodel.fem());
                    m_kriging.setControlPoints(hbmodel.fem(), id_source_skin, coord_target_skin, nug,
                        Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT),
                        Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT),
                        skin, (SurfaceDistance)Context::instance().project().moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_GEODESIC_TYPE));

                    m_kriging.compute();
                }
            }
            catch (std::exception e)
            {
                pCritical() << ERRORMSG << "Kriging failed: " << e.what();
                pInfo() << DONE;
                return;
            }
            //m_krigingdisplay.updatePreview();
            pInfo() << SUCCESS << "Initializing Kriging System for preview.";

            //apply kriging
            previewDone = true;
            m_krigingdisplay.setSkinPreviewValid(false);
            emit this->previewModelChanged();
            pInfo() << DONE;
        }
        //
    }

    void AnthroChildScaling::doLoadTargetPoint(std::string const& filename, std::string const& name) {
        if (Context::instance().project().model().empty()){
            pInfo() << INFO << "No Model - make sure that a model has been opened and that the \"Check\" module runs successfully.";
            pCritical() << QStringLiteral("while loading target points : no  model.");
            return;
        }
        //pInfo() << START << "Load target points (" << name << "): " << filename;

        kriging::CtrlPtLoader myLoader;
        if (!myLoader.loadData(filename))
        {
            pCritical() << "Error while loading child scaling script results, " << filename << " could not be read.\nCheck that you are using a correct set of control points.";
            return;
        }
        HumanBodyModel& hbmref = Context::instance().project().model();
        Metadata::InteractionControlPointCont & ctrlPtCont = hbmref.metadata().interactionControlPoints();

        hbm::Coord tempCoord;
        coord_target_skin.clear();
        coord_target_inner.clear();
        // load the target created by the script 
        // this is all based on the assumption that the target is in the same order as the source was provided
        std::vector<double> &as_skin = ctrlPtCont[m_setname].getAssociationSkin();
        if (as_skin.size() != ctrlPtCont[m_setname].getNbControlPt())
            as_skin.insert(as_skin.begin(), ctrlPtCont[m_setname].getNbControlPt(), 0); // if association does not exist, set it to "associated with bone" for all points
        std::vector<double>::iterator scriptIter = myLoader.getData().begin();
        for (size_t i = 0; i < ctrlPtCont[m_setname].getNbControlPt(); i++) // read the amount of control points that was in the source
        {
            for (int j = 0; j < 3; j++, scriptIter++)
                tempCoord[j] = *scriptIter;

            if (as_skin[i] == 1)
                coord_target_skin.push_back(tempCoord);
            else
                coord_target_inner.push_back(tempCoord);
        }
        if (!coord_target_skin.empty()) // if the control points are in different subsets, set them as two distinctive targets
        {
            // define target
            std::list<hbm::ControlPoint>& ControlPointTarget = Context::instance().project().target().controlPoint;
            for (auto it = ControlPointTarget.begin(); it != ControlPointTarget.end();) {
                if (it->name() == name + "_skin" || it->name() == name + "_inner") {
                    it = ControlPointTarget.erase(it);
                }
                else
                    it++;
            }
            ControlPoint addtargetSkin;
            addtargetSkin.setName(name + "_skin");
            addtargetSkin.setValue(coord_target_skin);
            addtargetSkin.setUnit(Context::instance().project().model().metadata().lengthUnit());
            ControlPointTarget.push_back(addtargetSkin);

            ControlPoint addtargetInner;
            addtargetInner.setName(name + "_inner");
            addtargetInner.setValue(coord_target_inner);
            addtargetInner.setUnit(Context::instance().project().model().metadata().lengthUnit());
            ControlPointTarget.push_back(addtargetInner);
            pInfo() << SUCCESS << "loaded target points: " << name << " with " << coord_target_skin.size() << " points on the skin and "
                << coord_target_inner.size() << " other points.";
        }
        else
        {
            // define target
            std::list<hbm::ControlPoint>& ControlPointTarget = Context::instance().project().target().controlPoint;
            for (auto it = ControlPointTarget.begin(); it != ControlPointTarget.end(); it++) {
                if (it->name() == name) {
                    ControlPointTarget.erase(it);
                    break;
                }
            }
            ControlPoint addtarget;
            addtarget.setName(name);
            addtarget.setValue(coord_target_inner);
            addtarget.setUnit(Context::instance().project().model().metadata().lengthUnit());
            ControlPointTarget.push_back(addtarget);
            m_krigingdisplay.removeControlPointsTarget(name, false);
            pInfo() << SUCCESS << "loaded target points: " << name << " with " << coord_target_inner.size() << " points.";
        }

    }

    void AnthroChildScaling::setListControlpointset(QString const& setname) {
        if (m_setname != setname.toStdString() && setname.size() > 0) {
            previewDone = false;
            m_setname = setname.toStdString();
            id_source_skin.clear();
            id_source_inner.clear();
            coord_source.clear();
            coord_target_inner.clear();
            coord_target_skin.clear();
            piper::hbm::HumanBodyModel& hbm = Context::instance().project().model();
            InteractionControlPoint *ctrlPointSet = hbm.metadata().interactionControlPoint(m_setname);
            if (ctrlPointSet)
            {
                std::vector<Coord> &pt = ctrlPointSet->getControlPt3dCont();
                VId const& vid = ctrlPointSet->getGroupNodeId();

                std::vector<double> &as_skin = ctrlPointSet->getAssociationSkin();
                if (vid.size() > 0 && as_skin.size() != vid.size())
                    as_skin.insert(as_skin.begin(), vid.size(), 0); // if no association is defined, set it associated to bone for all points
                else if (vid.size() == 0 && as_skin.size() != pt.size())
                    as_skin.insert(as_skin.begin(), pt.size(), 0);

                if (vid.size() > 0) {
                    for (size_t i = 0; i < vid.size(); i++)
                    {
                        if (as_skin[i] == 1)
                            id_source_skin.push_back(vid[i]);
                        else
                            id_source_inner.push_back(vid[i]);
                        coord_source.push_back(hbm.fem().getNode(vid[i]).get());
                    }
                }
                else { //the set of control points has been imported in kriging module
                    coord_source.insert(coord_source.begin(), pt.begin(), pt.end());

                    for (size_t i = 0; i < pt.size(); i++)
                    {
                        if (as_skin[i] == 1)
                            id_source_skin.push_back((Id)i);
                        else
                            id_source_inner.push_back((Id)i);
                    }
                }
                m_krigingdisplay.reset();
                emit this->previewModelChanged();
            }
        }
    }


    void AnthroChildScaling::krige() {
        void(*krigeWrapper)(piper::AnthroChildScaling*, void (piper::AnthroChildScaling::*)())
            = &piper::wrapClassMethodCall < piper::AnthroChildScaling, void (piper::AnthroChildScaling::*)() >;
        Context::instance().performAsynchronousOperation(std::bind(krigeWrapper, this, &piper::AnthroChildScaling::doKrige),
            false, false, false, false, false);
    }


    void AnthroChildScaling::doKrige() {
        pInfo() << START << "Applying kriging to the model.";
        HumanBodyModel& hbmref = Context::instance().project().model();
        hbm_nodes = piper::hbm::Nodes(hbmref.fem().getNodes());
        // if there are no specific skin control points, the kriging system used for preview is the same - just run the deformation with the system that is already created
        // otherwise we do two step kriging - first by skin, then by the rest
        m_kriging.applyDeformation(hbmref.fem().getNodes(), &hbm_nodes);
        if (!coord_target_skin.empty())
        {
            pInfo() << INFO << "Kriging by skin points done, building dense control points system using the skin and internal control points.";
            // now do dense skining using the skin points + the inner points
            // !!! the skin is not updated yet at this point - we use it just to get the node IDs and take the coordinates from hbm::FEModel
            // this is also not super efficient since FEModel does not have any O(1) method of getting point coordinates based on it's ID (only O(NlogN))
            // but it is probably still faster to get each skin node this way (NxNlogN) than rebuilding the whole skin mesh (which is like O(2N^2) + many memory allocation operations)
            vtkSmartPointer<vtkPolyData> skin = hbmref.fem().getFEModelVTK()->getSkin(false, hbmref.metadata().entities(), hbmref.fem());
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(skin->GetPointData()->GetArray("nid"));
            // first insert the "inner" points
            VId concatCtrlPointsIDs;
            std::vector<Coord> concatCtrlPointCoords;
            std::vector<double> nuggets;
            concatCtrlPointsIDs.insert(concatCtrlPointsIDs.begin(), id_source_inner.begin(), id_source_inner.end());
            concatCtrlPointCoords.insert(concatCtrlPointCoords.begin(), coord_target_inner.begin(), coord_target_inner.end());
            // now add the skin points
            for (vtkIdType i = 0; i < skin->GetNumberOfPoints(); i++)
            {
                vtkIdType n = nid->GetValue(i);
                concatCtrlPointsIDs.push_back(n);
                std::shared_ptr<Node> curNode = hbm_nodes.getEntity(n); // hbm_nodes contains the results of the previous skin-based kriging - use that as target
                concatCtrlPointCoords.push_back(curNode->get());
            }
            double nug = hbmref.metadata().interactionControlPoint(m_setname)->getGlobalWeight();
            if (std::isnan(nug))
                nug = Context::instance().project().moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT);
            m_kriging.setControlPoints(hbmref.fem(), concatCtrlPointsIDs, concatCtrlPointCoords, nug,
                Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT),
                Context::instance().project().moduleParameter().getBool(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT)); // use euclidean kriging
            m_kriging.compute();
            pInfo() << INFO << "System built, applying dense control points.";
            // now run the deformation again using the dense control points
            m_kriging.applyDeformation(hbmref.fem().getNodes(), &hbm_nodes);
        }

        pInfo() << DONE;
        emit this->deformed();
    }

    void AnthroChildScaling::setResultinHistory(QString const& historyName) {
        void(*setResultinHistoryWrapper)(piper::AnthroChildScaling*, void (piper::AnthroChildScaling::*)(std::string const&), std::string const&)
            = &piper::wrapClassMethodCall < piper::AnthroChildScaling, void(piper::AnthroChildScaling::*)(std::string const&), std::string const& >;

        Context::instance().performAsynchronousOperation(std::bind(setResultinHistoryWrapper,
            this, &piper::AnthroChildScaling::doSetResultinHistory, historyName.toStdString()),
            false, true, false, false, false);
    }

    void AnthroChildScaling::doSetResultinHistory(std::string const& historyName) {
        pInfo() << START << "Save model in history ";
        FEModel& femodel = Context::instance().project().model().fem(); //shortcut
        Context::instance().addNewHistory(QString::fromStdString(historyName));
        // copy new nodes coordinates in the current model in history
        femodel.getNodes() = hbm_nodes;
        femodel.updateVTKRepresentation();
        // ugly workaround assuming that kirging is used only in scaling workflow!!!!! done for Beta version
        TargetList const& target = Context::instance().project().target();
        Metadata& metadata = Context::instance().project().model().metadata();
        //look for age target
        if (target.age.size() == 1) {
            if (metadata.anthropometry().age.isDefined())
                metadata.anthropometry().age.convertToUnit(metadata.ageUnit());
            metadata.anthropometry().age.setValue(target.age.begin()->value());
        }
        std::string curmodel = Context::instance().currentModel().toStdString();
        // reload source control point positions
        std::string temp = m_setname;
        m_setname = ""; // to force the reload
        setListControlpointset(QString::fromStdString(temp));
        pInfo() << DONE << "Model after Kriging deformation is saved in history as " << curmodel;
    }

    void AnthroChildScaling::displayControlPointsSource(QString const& setname, bool const& visible, bool refreshDisplay) {
        m_krigingdisplay.displayControlPointsSource(setname.toStdString(), visible, refreshDisplay);
    }

    void AnthroChildScaling::displayControlPointsTarget(QString const& setname, bool const& visible, bool refreshDisplay) {
        m_krigingdisplay.displayControlPointsTarget(setname.toStdString(), visible, refreshDisplay);
    }

    void AnthroChildScaling::displayPreview(bool const& visible, bool refreshDisplay)
    {
        if (visible)
            generatePreview();
        if (previewDone)
            m_krigingdisplay.visibleSkinPreview(visible, refreshDisplay, false);
    }
}
