// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
import QtQuick 2.5

import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import QtQml.Models 2.2

import VtkQuick 1.0
import Piper 1.0

import piper.AnthropometryModule 1.0
import piper.BodySectionPersonalizing 1.0


ModuleToolWindow
{
    title: "Scaling by skin sections"
    width : childrenRect.width + 20
    height : childrenRect.height + 20
    id: window

//    property AnthropometryModule _myAnthropometryModule

    BodySectionPersonalizing {
        id: myBodySectionPersonalizing
    }

    onVisibleChanged: {
        if (visible)
            myAnthropoModelDisplayGUI.refresh()
        else {
            myAnthropoModelDisplayGUI.clear()
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins : itemMargin
        ColumnLayout {
            //anchors.fill: parent
            anchors.margins : itemMargin
            //        GroupBox {
            //            id: anthropoModel
            //            anchors.margins: itemMargin
            //            Layout.fillWidth: true
            //            title: qsTr("Anthropometrc Model Tools")
            //            ColumnLayout {
            //                anchors.fill: parent
            //                //anchors.margins : itemMargin
            Text {
                id: loadmodel
                text: {if (myBodySectionPersonalizing.isAnthropoModelDefined() === false)
                        qsTr("Anthropmetric Model is NOT loaded.")
                    else
                        qsTr("Anthropmetric Model is loaded.")
                }
                Connections
                {
                    target: myBodySectionPersonalizing
                    onAnthropoModelChanged: {
                        if (myBodySectionPersonalizing.isAnthropoModelDefined() === false)
                            loadmodel.text=qsTr("Anthropmetric Model is NOT loaded..")
                        else
                            loadmodel.text=qsTr("Anthropometric Model is loaded.")
                    }
                }
            }
            Button {
                id: loadButton
                Layout.fillWidth: true
                action: loadAnthropoModelAction
                //                }
                //            }
            }
            Button
            {
                id: applyKrigingButton
                action: applyKrigingAction
                anchors.margins : itemMargin
                Layout.fillWidth : true
            }
            Button {
                id: goBodySectionButton
                anchors.margins : itemMargin
                Layout.fillWidth : true
                iconSource: "qrc:///icon/arrow-right.png"
                text: "Go to Body Section module"
                onClicked: bodysectionModuleAction.trigger();
            }
        }
        AnthropoModelDisplayGUI {
            id: myAnthropoModelDisplayGUI
            Connections {
                target: myBodySectionPersonalizing
                onAnthropoModelChanged: {
                    if (visible) {
                        myAnthropoModelDisplayGUI.refresh()
                    }
                }
            }
            Connections {
                target: myBodySectionPersonalizing
                onAnthropoModelCleared: {
                    if (visible)
                        myAnthropoModelDisplayGUI.clearAnthropoModelDisplay()
                }
            }
            Connections {
                target: context
                onModelUpdated: {
                    if (visible)
                        myAnthropoModelDisplayGUI.refreshPreview()
                }
            }
            Connections {
                target: context
                onVisDataLoaded: {
                    if (visible)
                        myAnthropoModelDisplayGUI.refreshPreview()
                }
            }
        }
    }

    Action {
        id: loadAnthropoModelAction
        text: qsTr("Import Anthropometric Model")
        tooltip: qsTr("Load Anthropometric model.")
        onTriggered: {
            loadAnthropoModelDialog.open()
        }
    }
    FileDialog {
        id: loadAnthropoModelDialog
        title: qsTr("Load Anthropometric model")
        nameFilters: [ "xml files (*.xml)" ]
        onAccepted: myBodySectionPersonalizing.importAnthropoModel(loadAnthropoModelDialog.fileUrl)
    }
    Action {
        id: applyKrigingAction
        enabled : myBodySectionPersonalizing.isAnthropoModelLoaded
        text: "Apply"
        shortcut: StandardKey.Refresh
        onTriggered: {
            myBodySectionPersonalizing.krige()
        }
    }
    Connections {
        target: myBodySectionPersonalizing
		/*
        onDeformed:
        {
            myModelValidator.text = context.getValidHistoryName("Model_Scaling")
            myModelValidator.open()
        }
		*/
    }

    ModelHistoryNameDialog {
        id: myModelValidator
        onAccepted: myBodySectionPersonalizing.setResultinHistory(text)
    }

    Component.onCompleted: {
        adjustWindowSizeToContent();
    }
}
