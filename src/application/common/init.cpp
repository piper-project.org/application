/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Python.h"

#include "init.h"

#include <QString>
#include <QDir>
#include <QCoreApplication>

#pragma warning(push, 0)
#include <sofa/helper/Utils.h>
#include <sofa/helper/system/FileRepository.h>
#include <SofaSimulationGraph/init.h>
#include <SofaSimulationGraph/DAGSimulation.h>
#include <SofaComponentCommon/initComponentCommon.h>
#include <SofaComponentBase/initComponentBase.h>
#include <SofaComponentGeneral/initComponentGeneral.h>
#include <SofaComponentAdvanced/initComponentAdvanced.h>
#include <SofaComponentMisc/initComponentMisc.h>
#include <sofa/helper/system/PluginManager.h>
#pragma warning(pop)

#include <anatomyDB/query.h>

#include "helper.h"
#include "logging.h"
#include "OctaveProcess.h"

namespace piper {

void init()
{
    // log initialization, by default messages go to the console
    LoggingParameter::instance().addMessageHandler(piper::consoleMessageHandler);

    // create this piper instance temporary path
    tempDirectoryPath("", true);
    pDebug() << "Temporary directory: " << tempDirectoryPath();

    // anatomyDB initialization
    anatomydb::init();

    // python initialization
    Py_Initialize();
    // add piper package path
    QString pythonInit =
            "import sys\n"
            "sys.path.append('" + QCoreApplication::applicationDirPath() + "/../lib/python2.7/site-packages')\n"
            "import locale\n"
            "locale.setlocale(locale.LC_ALL, 'C')\n";
    PyRun_SimpleString(pythonInit.toUtf8());
    pDebug() << "Python initialised";

    // Sofa ressources configuration
    pDebug() << "Sofa prefix: " << sofa::helper::Utils::getSofaPathPrefix();
#ifdef WIN32
        sofa::helper::system::PluginRepository.addFirstPath(sofa::helper::Utils::getSofaPathPrefix()+"/bin"); // plugins distributed with sofa
        sofa::helper::system::PluginRepository.addFirstPath(QCoreApplication::applicationDirPath().toStdString()); // SofaPiper plugin compiled with piper
#else
    sofa::helper::system::PluginRepository.addFirstPath(sofa::helper::Utils::getSofaPathPrefix()+"/lib"); // plugins distributed with sofa
    sofa::helper::system::PluginRepository.addFirstPath(QCoreApplication::applicationDirPath().toStdString()+"/../lib"); // SofaPiper plugin compiled with piper
#endif
    sofa::helper::system::DataRepository.addFirstPath(QCoreApplication::applicationDirPath().toStdString());
    sofa::helper::system::DataRepository.addFirstPath(sofa::helper::Utils::getSofaPathPrefix()+"/share/sofa");

    // Octave
    OctaveProcess op;
    if (!op.canBeStarted())
        pWarning() << "Octave cannot be started, some functionnalities are disabled - octave command: " << op.program();
    else {
        pDebug() << "Octave executable: " << op.program();
        pDebug() << "Octave version: " << op.version();
    }
}

void cleanup()
{
    pDebug() << "Cleaning up Piper...";
    // Sofa cleanup
    sofa::simulation::graph::cleanup();

    // Python
    Py_Finalize();

    // remove tmp directory
    QDir(QString::fromStdString(piper::tempDirectoryPath())).removeRecursively();
}

void sofaInit()
{
	sofa::simulation::graph::init();
	sofa::component::initComponentBase();
	sofa::component::initComponentCommon();
	sofa::component::initComponentGeneral();
	sofa::component::initComponentAdvanced();
	sofa::component::initComponentMisc();

	sofa::simulation::setSimulation(new sofa::simulation::graph::DAGSimulation());

	sofa::helper::system::PluginManager::getInstance().loadPlugin("SofaPython");
	sofa::helper::system::PluginManager::getInstance().init();
}

}
