/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "SpinePredictor.h"

#include "common/logging.h"
#include "common/OctaveProcess.h"
#include "common/Context.h"

namespace piper {


QStringList SpinePredictor::postureNames() const
{
    return QStringList() << "Standing erect" << "Standing slouch" << "Seating erect" << "Supine" << "Forward flexed" << "Seating driving";
}

void SpinePredictor::setupOctaveProcess(piper::OctaveProcess* op, QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity)
{
    op->setScriptPath("spinePredictor/Scripts_release/spinePredictor.m");
    op->setScriptArguments(QStringList() << inputCSFile << inputGravityFile << QString::number(sourcePosture+1) << QString::number(targetPosture+1) << QString::number(postureValue) << QString::number(latFlexTL) << QString::number(latFlexC) << outputDir << (alignOnGravity ? "1" : "0") );
}

void SpinePredictor::compute(piper::OctaveProcess* op, QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity)
{
    setupOctaveProcess(op, inputCSFile, inputGravityFile, sourcePosture, targetPosture, postureValue, latFlexTL, latFlexC, outputDir, alignOnGravity);
    pInfo() << START << "Spine prediction";
    try {
        op->executeScript();
    }
    catch (std::exception const& e) {
        pCritical() << "Spine predictor: " << e.what();
    }
    pInfo() << DONE;
}

void SpinePredictor::startCompute(QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity) {
    setupOctaveProcess(Context::instance().octaveProcessPtr(), inputCSFile, inputGravityFile, sourcePosture, targetPosture, postureValue, latFlexTL, latFlexC, outputDir, alignOnGravity);
    connect(Context::instance().octaveProcessPtr(), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onScriptFinished(int, QProcess::ExitStatus)), Qt::UniqueConnection);
    pInfo() << START << "Spine prediction";
    Context::instance().octaveProcess().startScript();
}

void SpinePredictor::onScriptFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    if (!Context::instance().octaveProcess().isLastExecutionSuccessful()) {
        pCritical() << "Spine predictor: " << Context::instance().octaveProcess().errorMessage();
    }
    else {
        pInfo() << DONE;
        computeFinished();
    }
}

}
