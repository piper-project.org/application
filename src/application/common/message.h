/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QTextStream>

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

namespace piper {

/** Predefined rich text messages to send to a QTextStream
 * \ingroup logging
 * @{
 */

/// print [DEBUG]
PIPERCOMMON_EXPORT QTextStream& DEBUGMSG(QTextStream& out);

/// print [SUCCESS]
PIPERCOMMON_EXPORT QTextStream& SUCCESS(QTextStream& out);

/// print [INFO]
PIPERCOMMON_EXPORT QTextStream& INFO(QTextStream& out);

/// print [START] and start a timer
PIPERCOMMON_EXPORT QTextStream& START(QTextStream& out);

/// print [DONE] and display the duration from the previous START
PIPERCOMMON_EXPORT QTextStream& DONE(QTextStream& out);

/// print [WARNING]
PIPERCOMMON_EXPORT QTextStream& WARNING(QTextStream& out);

/// print [ERROR]
PIPERCOMMON_EXPORT QTextStream& ERRORMSG(QTextStream& out);

//PIPERCOMMON_EXPORT QTextStream& FAILED(QTextStream& out); // TODO: msvc does not compile this, try to find a workaround
PIPERCOMMON_EXPORT QTextStream& FAILEDMSG(QTextStream& out);

/// html line ending
PIPERCOMMON_EXPORT QTextStream& endl(QTextStream& out);

///@}

/// Pretty print the duration expressed in milliseconds
PIPERCOMMON_EXPORT QString TIMING(unsigned int msec);

}
