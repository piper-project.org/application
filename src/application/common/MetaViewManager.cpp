/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "MetaViewManager.h"
#include "Context.h"
#include <QQmlContext>
#include <iostream>

#include "lib/contours/main_repositioning.h"

#include <vtkPointData.h>

#include "anatomyDB/query.h"
#include "anatomyDB/check.h"

using namespace piper::hbm;

namespace piper {

	MetaViewManager::MetaViewManager(){}

	MetaViewManager::~MetaViewManager()
	{
	}

#pragma region Tables

	QList<QString> MetaViewManager::getEntityList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::EntityCont const& entities = Context::instance().project().model().metadata().entities();
			for (Metadata::EntityCont::const_iterator it = entities.begin(); it != entities.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.name()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getAnatomyDBEntityList(){
		QList<QString> list;
		std::vector<std::string> entityList;
		anatomydb::getAnatomicalEntityList(entityList);
		for (unsigned int i = 0; i < entityList.size(); i++)
			list.append(QString::fromStdString(entityList.at(i)));
		return list;
	}

	QList<QString> MetaViewManager::getLandmarkList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();
			for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.name()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getLandmarkTypeList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();
			for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it) {
				switch (it->second.type()){
				case Landmark::Type::POINT:{
					dataList.append(QString::fromStdString("POINT"));
					break;
				}
				case Landmark::Type::BARYCENTER:{
					dataList.append(QString::fromStdString("BARYCENTER"));
					break;
				}
				case Landmark::Type::SPHERE:{
					dataList.append(QString::fromStdString("SPHERE"));
					break;
				}
				}
			}
		}
		return dataList;
	}

	QList<int> MetaViewManager::getFrameList(){
		QList<int> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			const Frames& coordframes = m_fem.getFrames();
			for (Frames::const_iterator it = coordframes.begin(); it != coordframes.end(); ++it)
			{
				const Id& id = coordframes.curId(it);
				dataList.append(id);
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getJointList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.name()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getJointEntity1List(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.getEntity1()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getJointEntity2List(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.getEntity2()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getJointEntity1FrameList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				dataList.append(QString::fromStdString(std::to_string(it->second.getEntity1FrameId())));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getJointEntity2FrameList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				dataList.append(QString::fromStdString(std::to_string(it->second.getEntity2FrameId())));
			}
		}
		return dataList;
	}

	QList<int> MetaViewManager::getContourList(int bodyRegionNo, int contourNo){
		QList<int> dataList;
		Metadata& m_meta = Context::instance().project().model().metadata();
		std::vector<piper::hbm::Contour*> bodyRegion = getBodyRegion(m_meta, bodyRegionNo);
		int i = 0;
		if (contourNo == 0){
			for (std::vector<piper::hbm::Contour*>::iterator it = bodyRegion.begin(); it != bodyRegion.end(); it++){
				dataList.append(bodyRegion.at(i)->contid);
				i++;
			}
		}
		else{
			for (std::vector<piper::hbm::NodePtr>::iterator itt = bodyRegion.at(contourNo)->contour_points.begin(); itt != bodyRegion.at(contourNo)->contour_points.end(); ++itt){
				int contNo = i;
				dataList.append(contNo);
				i++;
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getTargetList(){
		QList<QString> dataList;
		// Target
		if (!Context::instance().project().target().empty()) {
			TargetList const& targetList = Context::instance().project().target();
			for (hbm::AnthropometricDimensionTarget const& target : targetList.anthropometricDimension)
				dataList.append(QString::fromStdString(target.name()));
			for (hbm::FixedBoneTarget const& target : targetList.fixedBone)
				dataList.append(QString::fromStdString(target.name()));
			for (hbm::LandmarkTarget const& target : targetList.landmark)
				dataList.append(QString::fromStdString(target.name()));
			for (hbm::JointTarget const& target : targetList.joint)
				dataList.append(QString::fromStdString(target.name()));
			for (hbm::FrameToFrameTarget const& target : targetList.frameToFrame)
				dataList.append(QString::fromStdString(target.name()));
			for (hbm::ScalingParameterTarget const& target : targetList.scalingparameter)
				dataList.append(QString::fromStdString(target.name()));

		}
		return dataList;
	}

	QList<QString> MetaViewManager::getTargetTypeList(){
		QList<QString> dataList;
		// Target
		if (!Context::instance().project().target().empty()) {
			TargetList const& targetList = Context::instance().project().target();
			for (hbm::AnthropometricDimensionTarget const& target : targetList.anthropometricDimension)
				dataList.append(QString::fromStdString(target.type()));
			for (hbm::FixedBoneTarget const& target : targetList.fixedBone)
				dataList.append(QString::fromStdString(target.type()));
			for (hbm::LandmarkTarget const& target : targetList.landmark)
				dataList.append(QString::fromStdString(target.type()));
			for (hbm::JointTarget const& target : targetList.joint)
				dataList.append(QString::fromStdString(target.type()));
			for (hbm::FrameToFrameTarget const& target : targetList.frameToFrame)
				dataList.append(QString::fromStdString(target.type()));
			for (hbm::ScalingParameterTarget const& target : targetList.scalingparameter)
				dataList.append(QString::fromStdString(target.type()));
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getGenericMetadataList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::GenericMetadataCont const& genericMetadatas = Context::instance().project().model().metadata().genericmetadatas();
			for (Metadata::GenericMetadataCont::const_iterator it = genericMetadatas.begin(); it != genericMetadatas.end(); ++it) {
				dataList.append(QString::fromStdString(it->second.name()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getControlPointList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			auto const& objs = Context::instance().project().model().metadata().interactionControlPoints();
			for (auto it = objs.begin(); it != objs.end(); ++it)
			{
				dataList.append(QString::fromStdString(it->second.name()));
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getControlPointRoleList(){
		QList<QString> dataList;
		if (!Context::instance().project().model().empty()) {
			auto const& objs = Context::instance().project().model().metadata().interactionControlPoints();
			for (auto it = objs.begin(); it != objs.end(); ++it)
			{
				dataList.append(QString::fromStdString(it->second.getControlPointRoleStr()));
			}
		}
		return dataList;
	}

	QList<double> MetaViewManager::getControlPointNumberList(){
		QList<double> dataList;
		if (!Context::instance().project().model().empty()) {
			auto const& objs = Context::instance().project().model().metadata().interactionControlPoints();
			for (auto it = objs.begin(); it != objs.end(); ++it)
			{
				dataList.append(it->second.getNbControlPt());
			}
		}
		return dataList;
	}

	QList<QString> MetaViewManager::getAnatomyDBLandmarksList(){
		std::vector<std::string> landmarkList;
		QList<QString> dataList;
		anatomydb::getLandmarkList(landmarkList);
		for (int i = 0; i < landmarkList.size(); i++) {
			dataList.append(QString::fromStdString(landmarkList[i]));
		}
		return dataList;

	}

	QList<QString>  MetaViewManager::getAnatomyDBJointList(){
		std::vector<std::string> jointList;
		QList<QString> dataList;
		anatomydb::getJointList(jointList);
		for (int i = 0; i < jointList.size(); i++) {
			dataList.append(QString::fromStdString(jointList[i]));
		}
		return dataList;
	}

	QString MetaViewManager::getAnatomyDBDescription(QString entityName){
		std::string description;
		QString data;
		description = anatomydb::getEntityDescription(entityName.toStdString());
		if (description == "")
			description = "Description not available";
		return QString::fromStdString(description);

	}
#pragma endregion

#pragma region DisplayUICalls


	void MetaViewManager::entityDisplay(bool visible, QString name){
		Context::instance().display().SetActorVisible(name.toStdString(), visible);
		if (Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::ENTITIES))
			Context::instance().display().Render();
	}

	void MetaViewManager::landmarkDisplay(bool visible, QString name, bool visualizeAsPoints){
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata& m_meta = Context::instance().project().model().metadata();
			Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();

			Context::instance().display().SetActorVisible(CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME, visualizeAsPoints);
			Context::instance().display().SetActorVisible(CONTEXT_MODEL_LANDMARKS_BY_POS_NAME, !visualizeAsPoints);

			for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it)
			{
				if (m_meta.hasLandmark(it->second.name()) && (name == "all" || it->second.name() == name.toStdString())) // only do it for the specified name, or if all are specified
				{
					if (visualizeAsPoints)
					{
						if (visible)
							m_fem.getFEModelVTK()->activateLandmarkPoints(it->second.name());
						else
							m_fem.getFEModelVTK()->deactivateLandmarkPoints(it->second.name());
					}
					else
					{
						if (visible)
							m_fem.getFEModelVTK()->activateLandmarkPositions(it->second.name());
						else
							m_fem.getFEModelVTK()->deactivateMeshLandmarkPositions(it->second.name());
					}
					if (name != "all")
						break;
				}
			}
			Context::instance().display().Render();
		}
	}

	void MetaViewManager::landmarkDisplayList(QVariantList landmarkNames, QVariantList landmarkVisibility, bool visualizeAsPoints)
	{
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata& m_meta = Context::instance().project().model().metadata();
			Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();

			Context::instance().display().SetActorVisible(CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME, visualizeAsPoints);
			Context::instance().display().SetActorVisible(CONTEXT_MODEL_LANDMARKS_BY_POS_NAME, !visualizeAsPoints);

			auto itVisible = landmarkVisibility.begin();
			for (auto itName = landmarkNames.begin(); itName != landmarkNames.end(); ++itName, ++itVisible)
			{
				if (m_meta.hasLandmark(itName->toString().toStdString()))
				{
					if (visualizeAsPoints)
					{
						if (itVisible->toBool())
							m_fem.getFEModelVTK()->activateLandmarkPoints(itName->toString().toStdString());
						else
							m_fem.getFEModelVTK()->deactivateLandmarkPoints(itName->toString().toStdString());
					}
					else
					{
						if (itVisible->toBool())
							m_fem.getFEModelVTK()->activateLandmarkPositions(itName->toString().toStdString());
						else
							m_fem.getFEModelVTK()->deactivateMeshLandmarkPositions(itName->toString().toStdString());
					}
				}
			}
			Context::instance().display().Render();
		}
	}

	void MetaViewManager::frameDisplay(bool visible, int frameNo){
		std::stringstream name;
		name << "frame_" << frameNo;
		FEModel& m_fem = Context::instance().project().model().fem();
		if (m_fem.getNbFrames() > 0) {
			const Frames& coordframes = m_fem.getFrames();
			for (Frames::const_iterator it = coordframes.begin(); it != coordframes.end(); ++it)
			{
				const Id& id = coordframes.curId(it);
				const FramePtr pFrame = coordframes.curDef(it);
				if (id == frameNo && pFrame->isCompletelyDefined())
				{
					if (visible)
					{
						// Show Frame if already created
						if (Context::instance().display().HasFrame(name.str())) {
							Context::instance().display().SetFrameVisible(name.str(), true);
							Context::instance().display().Render();
						}
						else{  // Create Frame if it does not exist
							Eigen::Quaternion<double> framequat = m_fem.getFrameOrientation(id);
							Eigen::Isometry3d frame(framequat);
							frame.translation() = m_fem.getFrameOrigin(id);
							frame.matrix().transposeInPlace();
							Context::instance().display().AddAxesActor(draw.frame(&(frame.matrix()(0)), Context::instance().display().FRAME_LENGTH),
								name.str(), false);
							Context::instance().display().Refresh(false);
						}
					}
					else if (visible == 0){ // no need to check if it exists or not - if it doesnt, nothing will happen
						Context::instance().display().SetFrameVisible(name.str(), false);
						Context::instance().display().Render();
					}
					break;
				}
			}
		}
	}

	void MetaViewManager::frameDisplayList(QVariantList frameNumbers, QVariantList frameVisibility)
	{
		FEModel& m_fem = Context::instance().project().model().fem();
		if (m_fem.getNbFrames() > 0) {
			const Frames& coordframes = m_fem.getFrames();
			auto itVisible = frameVisibility.begin();
			for (auto it = frameNumbers.begin(); it != frameNumbers.end(); ++it, ++itVisible)
			{
				unsigned int id = it->toUInt();
				if (id < coordframes.getMaxId())
				{
					std::stringstream name;
					name << "frame_" << id;
					if (itVisible->toBool())
					{
						// Show Frame if already created
						if (Context::instance().display().HasFrame(name.str())) {
							Context::instance().display().SetFrameVisible(name.str(), true);
							Context::instance().display().Render();
						}
						else if (m_fem.get<FEFrame>(id)->isCompletelyDefined())
							// Create Frame if it does not exist                           				 
						{
							Eigen::Quaternion<double> framequat = m_fem.getFrameOrientation(id);
							Eigen::Isometry3d frame(framequat);
							frame.translation() = m_fem.getFrameOrigin(id);
							frame.matrix().transposeInPlace();
							Context::instance().display().AddAxesActor(draw.frame(&(frame.matrix()(0)), Context::instance().display().FRAME_LENGTH),
								name.str(), false);
							Context::instance().display().Refresh(false);
						}
					}
					else { // no need to check if it exists or not - if it doesnt, nothing will happen
						Context::instance().display().SetFrameVisible(name.str(), false);
					}
				}
			}
			Context::instance().display().Render();
		}
	}


	void MetaViewManager::jointDisplay(bool visible, QString name){
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			for (Metadata::JointCont::const_iterator it = joints.begin(); it != joints.end(); ++it) {
				if (it->second.name() == name.toStdString()){
					frameDisplay(visible, it->second.getEntity1FrameId());
					frameDisplay(visible, it->second.getEntity2FrameId());
				}
			}
		}
	}

	void MetaViewManager::jointDisplayList(QVariantList jointNames, QVariantList jointVisibility)
	{
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::JointCont const& joints = Context::instance().project().model().metadata().joints();
			auto itVisible = jointVisibility.begin();
			for (auto it = jointNames.begin(); it != jointNames.end(); ++it, ++itVisible) {
				auto joint = joints.find(it->toString().toStdString());
				if (joint != joints.end()){
					frameDisplay(itVisible->toBool(), joint->second.getEntity1FrameId());
					frameDisplay(itVisible->toBool(), joint->second.getEntity2FrameId());
				}
			}
		}
	}

	void MetaViewManager::genericMetadataDisplay(bool visible, QString name){
		Context::instance().display().SetActorVisible(name.toStdString(), visible);
		if (Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::GENERICMETADATA))
			Context::instance().display().Render();
		else // if entities are not active, activate them and refresh the display
		{
			Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::GENERICMETADATA);
			Context::instance().display().Refresh();
		}
	}


	void MetaViewManager::controlPointDisplayList(QVariantList names, QVariantList visibility)
	{
		if (!Context::instance().project().model().empty()) {
			auto objs = Context::instance().project().model().metadata().interactionControlPoints();
			auto itVisible = visibility.begin();
			for (auto itNames = names.begin(); itNames != names.end(); itNames++, itVisible++)
			{
				for (auto obj : objs){
					if (obj.second.name() == itNames->toString().toStdString())
					{
						for (Id id : obj.second.getGroupNodeId())
						{
							std::string actorName = "CP#" + itNames->toString().toStdString() + to_string(id);
							if (!itVisible->toBool()){
								Context::instance().display().SetActorVisible(actorName, false);
							}
							else{
								if (Context::instance().display().HasActor(actorName)){
									Context::instance().display().SetActorVisible(actorName, true);
								}
								else{
									FEModel& m_fem = Context::instance().project().model().fem();
									auto node = m_fem.get<Node>(id);
									vtkSmartPointer<VtkDisplayActor> pointActor = draw.point(node->getCoordX(), node->getCoordY(), node->getCoordZ(), 3, 1.0, 0.0, 0.0);
									Context::instance().display().AddActor(pointActor, actorName, false, DISPLAY_MODE::POINTS, true, false);

									if (Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::POINTS))
										Context::instance().display().Refresh();
									else
									{
										Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::POINTS);
										Context::instance().display().Refresh();
									}
								}
							}
						}
					}
				}
			}
			Context::instance().display().Render();
		}
	}


	void MetaViewManager::targetDisplay(bool visible, QString name, QString type){
		if (!Context::instance().project().model().empty()) {
			TargetList targetList = Context::instance().project().target();
			if (type.toStdString() == "Landmark"){
				for (hbm::LandmarkTarget target : targetList.landmark){
					if (target.name() == name.toStdString()){
						if (visible){
							Context::instance().display().SetActorVisible("T_" + name.toStdString(), false);
						}
						else{
							if (Context::instance().display().HasActor("T_" + name.toStdString())){
								Context::instance().display().SetActorVisible("T_" + name.toStdString(), true);
							}
							else{
								hbm::LandmarkTarget::ValueType value = target.value();
								vtkSmartPointer<VtkDisplayActor> pointActor = draw.point(value[0], value[1], value[2], 3);
								Context::instance().display().AddActor(pointActor, "T_" + name.toStdString(), false, DISPLAY_MODE::POINTS, true, false);
							}
						}
						Context::instance().display().Render();
					}
				}
			}
		}
	}

	void MetaViewManager::targetDisplayList(QVariantList targetNames, QVariantList targetVisibility, QVariantList targetTypes)
	{
		if (!Context::instance().project().model().empty()) {
			TargetList targetList = Context::instance().project().target();
			auto itType = targetTypes.begin();
			auto itVisible = targetVisibility.begin();
			for (auto itNames = targetNames.begin(); itNames != targetNames.end(); itNames++, itVisible++, itType++)
			{
				if (itType->toString().toStdString() == "Landmark"){
					for (hbm::LandmarkTarget target : targetList.landmark){
						if (target.name() == itType->toString().toStdString()){
							if (itVisible->toBool()){
								Context::instance().display().SetActorVisible("T_" + itType->toString().toStdString(), false);
							}
							else{
								if (Context::instance().display().HasActor("T_" + itType->toString().toStdString())){
									Context::instance().display().SetActorVisible("T_" + itType->toString().toStdString(), true);
								}
								else{
									hbm::LandmarkTarget::ValueType value = target.value();
									vtkSmartPointer<VtkDisplayActor> pointActor = draw.point(value[0], value[1], value[2], 3);
									Context::instance().display().AddActor(pointActor, "T_" + itType->toString().toStdString(), false, DISPLAY_MODE::POINTS, true, false);
									Context::instance().display().Refresh();
								}
							}
						}
					}
				}
			}
			Context::instance().display().Render();
		}
	}

	void MetaViewManager::contourBodyRegionDisplay(bool visible, int body_region){
		int i = 0;
		for (auto itt = contourBRSet.at(body_region).begin(); itt != contourBRSet.at(body_region).end(); ++itt)
		{
			if (visible){
				vtkActor::SafeDownCast(contourBRSet.at(body_region).at(i))->VisibilityOn();
			}
			else{
				vtkActor::SafeDownCast(contourBRSet.at(body_region).at(i))->VisibilityOff();
			}
			i++;
		}
		Context::instance().display().Render();
	}

	void MetaViewManager::contourDisplay(bool visible, int body_region, int contour_no){
		int i = 0;
		for (auto itt = contourBRSet.at(body_region).begin(); itt != contourBRSet.at(body_region).end(); ++itt)
		{
			if (i == contour_no){
				if (visible){
					vtkActor::SafeDownCast(contourBRSet.at(body_region).at(i))->VisibilityOn();
				}
				else{
					vtkActor::SafeDownCast(contourBRSet.at(body_region).at(i))->VisibilityOff();
				}
			}
			i++;
		}
		Context::instance().display().Render();
	}

	void MetaViewManager::contourPointDisplay(bool visible, int body_region, int contour_no, int point_no){
		Metadata& m_meta = Context::instance().project().model().metadata();
		std::vector<piper::hbm::Contour*> bodyRegion = getBodyRegion(m_meta, body_region);
		// create a unique name of the contour point
		std::stringstream name;
		name << "contour_point_" << bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordX() << "_" <<
			bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordY() << "_" <<
			bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordZ();

		if (visible){
			Context::instance().display().RemoveActor(name.str()); // remove it from the display if visibility == 0 - don't we want to just set it invisible using SetActorVisible(false)?
		}
		else{
			// create a point actor for it
			auto actor = draw.point(bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordX(),
				bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordY(),
				bodyRegion.at(contour_no)->contour_points.at(point_no)->getCoordZ(),
				Context::instance().display().POINTHIGHLIGHT_RADIUS);
			Context::instance().display().AddActor(actor, name.str(), false, DISPLAY_MODE::POINTS);
			Context::instance().display().SetObjectColor(name.str(), 1, 0, 0);
		}
		Context::instance().display().Refresh();
	}

#pragma endregion	
	void MetaViewManager::openContour(std::string const& contourFile){
		QUrl m_contourFile; ///< current contour file
		m_contourFile = QUrl::fromLocalFile(QString::fromStdString(contourFile));
		Metadata& m_meta = Context::instance().project().model().metadata();

		m_meta.m_hbm_contours = piper::contours::MainRepositioning::readContour(contourFile, &m_meta.cont_det.contour_map, &vec_spline_new);

		if (!Context::instance().project().model().empty()) {
			m_meta.m_hbm_contours.sort_all();

			//Draw All Contours		
			m_meta.m_hbm_contours.sort_all();
			for (int body_region = 0; body_region < maxBodyRegions; body_region++){
				int i = 0;
				std::vector<piper::hbm::Contour*> bodyRegion = getBodyRegion(m_meta, body_region);
				std::vector<vtkSmartPointer<vtkActor>> contourSet;
				for (std::vector<piper::hbm::Contour*>::iterator it = bodyRegion.begin(); it != bodyRegion.end(); ++it){
					int j = 0;
					int contouroriginstr = bodyRegion.at(i)->contid;
					if (bodyRegion.at(i)->contour_points.size()){

						// Create a vtkPoints object and store the points in it
						vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

						for (std::vector<piper::hbm::NodePtr>::iterator itt = bodyRegion.at(i)->contour_points.begin(); itt != bodyRegion.at(i)->contour_points.end(); ++itt){
							double pt[3] = { bodyRegion.at(i)->contour_points.at(j)->getCoordX(), bodyRegion.at(i)->contour_points.at(j)->getCoordY(), bodyRegion.at(i)->contour_points.at(j)->getCoordZ() };
							points->InsertNextPoint(pt);
							j++;
						}

						//Join Last Point to First to close the Contour
						double pt[3] = { bodyRegion.at(i)->contour_points.at(0)->getCoordX(), bodyRegion.at(i)->contour_points.at(0)->getCoordY(), bodyRegion.at(i)->contour_points.at(0)->getCoordZ() };
						points->InsertNextPoint(pt);
						std::stringstream name;
						name << "contour_BR_" << body_region << "_contourNo_" << i; // I don't know if this name is actually unique, please check /TJ
						auto actor = draw.polyLine(points);
						Context::instance().display().AddActor(actor, name.str(), false, DISPLAY_MODE::ENTITIES); // maybe some other display mode?
						contourSet.push_back(actor);
					}
					i++;
				}
				contourBRSet.push_back(contourSet);
			}
			Context::instance().display().Refresh();
		}
	}

	std::vector<piper::hbm::Contour*> MetaViewManager::getBodyRegion(piper::hbm::Metadata& m_meta, int body_region){

		std::vector<piper::hbm::Contour*> bodyRegion;
		if (!Context::instance().project().model().empty()) {
			switch (body_region){
			case 0:		bodyRegion = m_meta.m_hbm_contours.Right_Foot;	break;
			case 1:		bodyRegion = m_meta.m_hbm_contours.Left_Foot;	break;
			case 2:		bodyRegion = m_meta.m_hbm_contours.Right_Ankle_to_Calf;	break;
			case 3:		bodyRegion = m_meta.m_hbm_contours.Left_Ankle_to_Calf;	break;
			case 4:		bodyRegion = m_meta.m_hbm_contours.Right_Knee_lower;	break;
			case 5:		bodyRegion = m_meta.m_hbm_contours.Left_Knee_lower;	break;
			case 6:		bodyRegion = m_meta.m_hbm_contours.Right_Knee_upper;	break;
			case 7:		bodyRegion = m_meta.m_hbm_contours.Left_Knee_upper;	break;
			case 8:		bodyRegion = m_meta.m_hbm_contours.Right_Thigh;	break;
			case 9:		bodyRegion = m_meta.m_hbm_contours.Left_Thigh;	break;
			case 10:	bodyRegion = m_meta.m_hbm_contours.Hip;	break;
			case 11:	bodyRegion = m_meta.m_hbm_contours.Abdomen;	break;
			case 12:	bodyRegion = m_meta.m_hbm_contours.Thorax;	break;
			case 13:	bodyRegion = m_meta.m_hbm_contours.Neck;	break;
			case 14:	bodyRegion = m_meta.m_hbm_contours.Head;	break;
			case 15:	bodyRegion = m_meta.m_hbm_contours.Right_Palm;	break;
			case 16:	bodyRegion = m_meta.m_hbm_contours.Left_Palm;	break;
			case 17:	bodyRegion = m_meta.m_hbm_contours.Right_Forearm;	break;
			case 18:	bodyRegion = m_meta.m_hbm_contours.Left_Forearm;	break;
			case 19:	bodyRegion = m_meta.m_hbm_contours.Right_Elbow_lower;	break;
			case 20:	bodyRegion = m_meta.m_hbm_contours.Left_Elbow_lower;	break;
			case 21:	bodyRegion = m_meta.m_hbm_contours.Right_Elbow_upper;	break;
			case 22:	bodyRegion = m_meta.m_hbm_contours.Left_Elbow_upper;	break;
			case 23:	bodyRegion = m_meta.m_hbm_contours.Right_Biceps;	break;
			case 24:	bodyRegion = m_meta.m_hbm_contours.Left_Biceps;	break;
			case 25:	bodyRegion = m_meta.m_hbm_contours.Right_biceps_to_shoulder;	break;
			case 26:	bodyRegion = m_meta.m_hbm_contours.Left_biceps_to_shoulder;	break;
			case 27:	bodyRegion = m_meta.m_hbm_contours.Hip_right;	break;
			case 28:	bodyRegion = m_meta.m_hbm_contours.Hip_left;	break;
			case 29:	bodyRegion = m_meta.m_hbm_contours.Keypoints;	break;
			case 30:	bodyRegion = m_meta.m_hbm_contours.Right_Abdomen;	break;
			case 31:	bodyRegion = m_meta.m_hbm_contours.Left_Abdomen;	break;
			case 32:	bodyRegion = m_meta.m_hbm_contours.Right_Thorax;	break;
			case 33:	bodyRegion = m_meta.m_hbm_contours.Left_Thorax;	break;
			}
		}
		return bodyRegion;
	}

	void MetaViewManager::setFullModelOpacity(float value){
		Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::FULL_MESH, value);
		Context::instance().display().Render();
	}

	void MetaViewManager::setEntitiesOpacity(float value) {
		Context::instance().display().SetDisplayModeOpacity(DISPLAY_MODE::ENTITIES, value);
		Context::instance().display().Render();
	}

	void MetaViewManager::setDisplayModeActive(unsigned int displayMode, bool active)
	{
		if (active)
			Context::instance().display().ActivateDisplayMode(displayMode);
		else Context::instance().display().DeactivateDisplayMode(displayMode);
		Context::instance().display().Refresh();
	}

	void MetaViewManager::changeColor(QString color, QString name, float alpha)
	{
		Context::instance().display().SetObjectColor(name.toStdString(), HexadecimalToRGB(color.toStdString()).at(0), HexadecimalToRGB(color.toStdString()).at(1), HexadecimalToRGB(color.toStdString()).at(2), alpha);
		Context::instance().display().GetRenderWindow()->Render();
	}

	void MetaViewManager::changeRadius(QString name, float radius)
	{
		Context::instance().display().GetRenderWindow()->Render();
	}

	float MetaViewManager::HexadecimalToDecimal(string hex) {
		float hexLength = hex.length();
		float dec = 0;

		for (int i = 0; i < hexLength; ++i)
		{
			char b = hex[i];

			if (b >= 48 && b <= 57)
				b -= 48;
			else if (b >= 65 && b <= 70)
				b -= 55;

			dec += b * pow(16, ((hexLength - i) - 1));
		}

		return (float)dec / 255;
	}

	std::vector<float> MetaViewManager::HexadecimalToRGB(std::string hex) {
		if (hex[0] == '#')
			hex = hex.erase(0, 1);

		std::vector<float> rgb;

		rgb.push_back((float)HexadecimalToDecimal(hex.substr(0, 2)));
		rgb.push_back((float)HexadecimalToDecimal(hex.substr(2, 2)));
		rgb.push_back((float)HexadecimalToDecimal(hex.substr(4, 2)));

		return rgb;
	}

	bool MetaViewManager::getIsModelChanged(){
		return is_model_changed;
	}

	void MetaViewManager::setIsModelChanged(bool val){
		is_model_changed = val;
	}

#pragma region contourCLUICalls
	QVariantMap  MetaViewManager::ui_getUndefinedContourCLElements(){
		QVariantMap map;
		Metadata& m_meta = Context::instance().project().model().metadata();
		std::map<std::string, std::vector<std::string>> undefined = m_meta.ctrl_line.undefinedCLElements;
		typedef std::map<std::string, std::vector<std::string>>::iterator it_type;
		for (it_type iterator = undefined.begin(); iterator != undefined.end(); iterator++) {
			QList<QString> row;
			std::istringstream iss(iterator->second.at(0));
			std::string sub;
			do{
				iss >> sub;
			} while (iss);
			if (sub == "Landmark"){
				if (m_meta.hasLandmark(iterator->first)){
					undefined.erase(iterator);
					continue;
				}
			}
			else if (sub == "Entity"){
				if (m_meta.hasEntity(iterator->first)){
					undefined.erase(iterator);
					continue;
				}
			}
			for (unsigned int i = 0; i < iterator->second.size(); i++){
				row.append(QString::fromStdString(iterator->second.at(i)));
			}
			map.insert(QString::fromStdString(iterator->first), QVariant(row));
		}
		return map;
	}

	QVariantMap MetaViewManager::ui_getContourCLTree(){
		Metadata& m_meta = Context::instance().project().model().metadata();
		QVariantMap map;
		QList<QString> list, childBr;
		ContourCLbr* br;
		if (m_meta.ctrl_line.root)
			br = m_meta.ctrl_line.root;
		else
			return map;
		stack<ContourCLbr*> bodyRegions;
		map.insert("Root", QString::fromStdString(br->name));
		bodyRegions.push(br);
		br = bodyRegions.top();
		bodyRegions.pop();
		list.clear();
		for (unsigned int i = 0; i < br->childrenjts.size(); i++){
			list.append(QString::fromStdString(br->childrenjts.at(i)->name));
			childBr.clear();
			childBr.append(QString::fromStdString(br->childrenjts.at(i)->childbodyregion->name));
			bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
			map.insert(QString::fromStdString(br->childrenjts.at(i)->name), QVariant(childBr));
		}
		map.insert(QString::fromStdString(br->name), QVariant(list));

		while (!bodyRegions.empty()){
			br = bodyRegions.top();
			bodyRegions.pop();
			list.clear();
			for (unsigned int i = 0; i < br->childrenjts.size(); i++){
				list.append(QString::fromStdString(br->childrenjts.at(i)->name));
				childBr.clear();
				if (br->childrenjts.at(i)->childbodyregion){
					childBr.append(QString::fromStdString(br->childrenjts.at(i)->childbodyregion->name));
					bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
					map.insert(QString::fromStdString(br->childrenjts.at(i)->name), QVariant(childBr));
				}
			}
			map.insert(QString::fromStdString(br->name), QVariant(list));
		}

		return map;


	}

	QList<QString> MetaViewManager::ui_getAnatomyDBLandmarks(){
		QList<QString> list;
		std::vector<std::string> landmarkList;
		anatomydb::getLandmarkList(landmarkList);
		for (unsigned int i = 0; i < landmarkList.size(); i++)
			list.append(QString::fromStdString(landmarkList.at(i)));
		return list;
	}

	QString MetaViewManager::ui_getLandmarkDescriptionFromAnatomyDB(QString name){
		return QString::fromStdString(anatomydb::getEntityDescription(name.toStdString()));
	}

	QString MetaViewManager::ui_getBRDescription(QString name){
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		return QString::fromStdString(br->description);
	}

	QString MetaViewManager::ui_getJointDescription(QString jt){
		ContourCLj* joint = getJointObject(jt.toStdString());
		return QString::fromStdString(joint->joint_description);
	}

#pragma endregion

	ContourCLbr* MetaViewManager::getBodyRegionObject(std::string name){
		Metadata& m_meta = Context::instance().project().model().metadata();
		ContourCLbr* br = m_meta.ctrl_line.root;
		stack<ContourCLbr*> bodyRegions;
		bodyRegions.push(br);
		while (!bodyRegions.empty()){
			br = bodyRegions.top();
			bodyRegions.pop();
			if (br->name == name)
				return br;
			for (unsigned int i = 0; i < br->childrenjts.size(); i++)
				if (br->childrenjts.at(i)->childbodyregion)
					bodyRegions.push(br->childrenjts.at(i)->childbodyregion);

		}
		return NULL;
	}

	ContourCLj* MetaViewManager::getJointObject(std::string name){
		Metadata& m_meta = Context::instance().project().model().metadata();
		ContourCLbr* br = m_meta.ctrl_line.root;
		stack<ContourCLbr*> bodyRegions;
		bodyRegions.push(br);
		while (!bodyRegions.empty()){
			br = bodyRegions.top();
			bodyRegions.pop();
			for (unsigned int i = 0; i < br->childrenjts.size(); i++){
				if (br->childrenjts.at(i)->name == name)
					return br->childrenjts.at(i);
				if (br->childrenjts.at(i)->childbodyregion)
					bodyRegions.push(br->childrenjts.at(i)->childbodyregion);
			}
		}
		return NULL;
	}

	QList<QString> MetaViewManager::ui_getCtrlPtLandmarks(QString name){
		QList<QString> list;
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		for (unsigned int i = 0; i < br->cplandmarks.size(); i++)
			list.append(QString::fromStdString(br->cplandmarks.at(i)));
		return list;
	}

	void MetaViewManager::ui_addCtrlPtLandmark(QString bodyRegion, QString name, QString desc){
		ContourCLbr* br = getBodyRegionObject(bodyRegion.toStdString());
		br->cplandmarks.push_back(name.toStdString());
		br->cplandmarks_info.push_back(desc.toStdString());
	}

	void MetaViewManager::ui_removeCtrlPtLandmark(QString bodyRegion, QString name){
		ContourCLbr* br = getBodyRegionObject(bodyRegion.toStdString());
		string landmark = name.toStdString();
		string desc;
		for (unsigned int i = 0; i < br->cplandmarks.size(); i++)
			if (br->cplandmarks.at(i) == landmark)
				desc = br->cplandmarks_info.at(i);
		br->cplandmarks.erase(std::remove(br->cplandmarks.begin(), br->cplandmarks.end(), name.toStdString()), br->cplandmarks.end());
		br->cplandmarks_info.erase(std::remove(br->cplandmarks_info.begin(), br->cplandmarks_info.end(), desc), br->cplandmarks_info.end());
	}

	void MetaViewManager::ui_moveUpCtrlLandmark(QString bodyRegion, QString name){
		string landmark = name.toStdString();
		ContourCLbr* br = getBodyRegionObject(bodyRegion.toStdString());
		std::vector<std::string> cplandmarks = br->cplandmarks;
		for (unsigned int i = 1; i < br->cplandmarks.size(); i++){
			if (br->cplandmarks.at(i) == landmark){
				br->cplandmarks.at(i) = br->cplandmarks.at(i - 1);
				br->cplandmarks.at(i - 1) = landmark;
				std::string info_temp = br->cplandmarks_info.at(i - 1);
				br->cplandmarks_info.at(i - 1) = br->cplandmarks_info.at(i);
				br->cplandmarks_info.at(i) = info_temp;
				return;
			}
		}
	}

	void MetaViewManager::ui_moveDownCtrlLandmark(QString bodyRegion, QString name){
		string landmark = name.toStdString();
		ContourCLbr* br = getBodyRegionObject(bodyRegion.toStdString());
		std::vector<std::string> cplandmarks = br->cplandmarks;
		for (unsigned int i = 0; i < br->cplandmarks.size() - 1; i++){
			if (br->cplandmarks.at(i) == landmark){
				br->cplandmarks.at(i) = br->cplandmarks.at(i + 1);
				br->cplandmarks.at(i + 1) = landmark;
				std::string info_temp = br->cplandmarks_info.at(i + 1);
				br->cplandmarks_info.at(i + 1) = br->cplandmarks_info.at(i);
				br->cplandmarks_info.at(i) = info_temp;
				return;
			}
		}
	}

	QString MetaViewManager::ui_getSkingm(QString name){
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		if (br->skingenericmetadata != "")
			return QString::fromStdString(br->skingenericmetadata);
		else
			return QString::fromStdString("");
	}
	void MetaViewManager::ui_setSkingm(QString brname, QString entity){
		ContourCLbr* br = getBodyRegionObject(brname.toStdString());
		br->skingenericmetadata = entity.toStdString();
	}


	QList<QString> MetaViewManager::ui_getCplLandmarksDesc(QString name){
		QList<QString> list;
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		for (unsigned int i = 0; i < br->cplandmarks_info.size(); i++){
			if (br->cplandmarks_info.at(i).size() != 0)
				list.append(QString::fromStdString(br->cplandmarks_info.at(i)));
			else
				list.append(QString::fromStdString(""));
		}
		return list;
	}

	QList<QString> MetaViewManager::ui_getSplineInfo(QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(name.toStdString());
		if (joint->joint_splinep0.size() != 0)
			list.append(QString::fromStdString(joint->joint_splinep0));
		if (joint->joint_splinep1.size() != 0)
			list.append(QString::fromStdString(joint->joint_splinep1));
		if (joint->joint_splinet1.size() != 0)
			list.append(QString::fromStdString(joint->joint_splinet1));
		if (joint->joint_splinet2.size() != 0)
			list.append(QString::fromStdString(joint->joint_splinet2));
		return list;
	}

	void MetaViewManager::ui_addSplineInfo(QString jt, QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		if (joint->joint_splinep0 == ""){
			joint->joint_splinep0 = name.toStdString();
			return;
		}
		if (joint->joint_splinep1 == ""){
			joint->joint_splinep1 = name.toStdString();
			return;
		}
		if (joint->joint_splinet1 == ""){
			joint->joint_splinet1 = name.toStdString();
			return;
		}
		if (joint->joint_splinet2 == "")
			joint->joint_splinet2 = name.toStdString();
	}

	void MetaViewManager::ui_removeSplineInfo(QString jt, QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		if (joint->joint_splinep0 == name.toStdString()){
			joint->joint_splinep0 = string();
			joint->spline_vec_info.at(0) = "";
		}
		if (joint->joint_splinep1 == name.toStdString()){
			joint->joint_splinep1 = string();
			joint->spline_vec_info.at(1) = "";
		}
		if (joint->joint_splinet1 == name.toStdString()){
			joint->joint_splinet1 = string();
			joint->spline_vec_info.at(2) = "";
		}
		if (joint->joint_splinet2 == name.toStdString()){
			joint->joint_splinet2 = string();
			joint->spline_vec_info.at(3) = "";
		}
	}

	void MetaViewManager::ui_moveUpSplineLandmark(QString jt, QString name){
		string landmark = name.toStdString();
		string temp;
		ContourCLj* joint = getJointObject(jt.toStdString());
		if (landmark == joint->joint_splinep1){
			temp = joint->joint_splinep1;
			joint->joint_splinep1 = joint->joint_splinep0;
			joint->joint_splinep0 = temp;
			temp = joint->spline_vec_info.at(0);
			joint->spline_vec_info.at(0) = joint->spline_vec_info.at(1);
			joint->spline_vec_info.at(1) = temp;
			return;
		}
		if (landmark == joint->joint_splinet1){
			temp = joint->joint_splinet1;
			joint->joint_splinet1 = joint->joint_splinep1;
			joint->joint_splinep1 = temp;
			temp = joint->spline_vec_info.at(2);
			joint->spline_vec_info.at(2) = joint->spline_vec_info.at(1);
			joint->spline_vec_info.at(1) = temp;
			return;
		}
		if (landmark == joint->joint_splinet2){
			temp = joint->joint_splinet1;
			joint->joint_splinet1 = joint->joint_splinet2;
			joint->joint_splinet2 = temp;
			temp = joint->spline_vec_info.at(2);
			joint->spline_vec_info.at(2) = joint->spline_vec_info.at(3);
			joint->spline_vec_info.at(3) = temp;
			return;
		}
	}

	void MetaViewManager::ui_moveDownSplineLandmark(QString jt, QString name){
		string landmark = name.toStdString();
		string temp;
		ContourCLj* joint = getJointObject(jt.toStdString());
		if (landmark == joint->joint_splinep0){
			temp = joint->joint_splinep1;
			joint->joint_splinep1 = joint->joint_splinep0;
			joint->joint_splinep0 = temp;
			temp = joint->spline_vec_info.at(0);
			joint->spline_vec_info.at(0) = joint->spline_vec_info.at(1);
			joint->spline_vec_info.at(1) = temp;
			return;
		}
		if (landmark == joint->joint_splinep1){
			temp = joint->joint_splinet1;
			joint->joint_splinet1 = joint->joint_splinep1;
			joint->joint_splinep1 = temp;
			temp = joint->spline_vec_info.at(2);
			joint->spline_vec_info.at(2) = joint->spline_vec_info.at(1);
			joint->spline_vec_info.at(1) = temp;
			return;
		}
		if (landmark == joint->joint_splinet1){
			temp = joint->joint_splinet1;
			joint->joint_splinet1 = joint->joint_splinet2;
			joint->joint_splinet2 = temp;
			temp = joint->spline_vec_info.at(2);
			joint->spline_vec_info.at(2) = joint->spline_vec_info.at(3);
			joint->spline_vec_info.at(3) = temp;
			return;
		}
	}

	QList<QString> MetaViewManager::ui_getSplineDesc(QString jt){

		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		for (unsigned int i = 0; i < joint->spline_vec_info.size(); i++)
			if (joint->spline_vec_info.at(i).size() != 0)
				list.append(QString::fromStdString(joint->spline_vec_info.at(i)));
			else
				list.append(QString::fromStdString(""));
		return list;
	}

	QList<QString> MetaViewManager::ui_getFlexionAxesLandmarks(QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(name.toStdString());
		for (unsigned int i = 0; i < joint->flexionaxeslandmarks.size(); i++){
			list.append(QString::fromStdString(joint->flexionaxeslandmarks.at(i)));
		}
		return list;
	}

	void MetaViewManager::ui_addFlexionAxesLandmark(QString name, QString newValue){
		ContourCLj* joint = getJointObject(name.toStdString());
		joint->flexionaxeslandmarks.push_back(newValue.toStdString());
		joint->flexionaxeslandmarks_info.push_back("");
	}

	void MetaViewManager::ui_removeFlexionAxesLandmarks(QString j, QString name){
		ContourCLj* joint = getJointObject(j.toStdString());
		for (unsigned int i = 0; i < joint->flexionaxeslandmarks.size(); i++){
			if (joint->flexionaxeslandmarks.at(i) == name.toStdString())
				joint->flexionaxeslandmarks_info.erase(joint->flexionaxeslandmarks_info.begin() + i);
		}
		joint->flexionaxeslandmarks.erase(std::remove(joint->flexionaxeslandmarks.begin(), joint->flexionaxeslandmarks.end(), name.toStdString()), joint->flexionaxeslandmarks.end());
	}

	QList<QString> MetaViewManager::ui_getFlexionAxesLandmarksDesc(QString jt){
		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		for (unsigned int i = 0; i < joint->flexionaxeslandmarks_info.size(); i++)
			if (joint->flexionaxeslandmarks_info.at(i).size() != 0)
				list.append(QString::fromStdString(joint->flexionaxeslandmarks_info.at(i)));
			else
				list.append(QString::fromStdString(""));
		return list;
	}

	QList<QString> MetaViewManager::ui_getTwistAxesLandmarks(QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(name.toStdString());
		for (unsigned int i = 0; i < joint->twistaxeslandmarks.size(); i++){
			list.append(QString::fromStdString(joint->twistaxeslandmarks.at(i)));
		}
		return list;
	}

	void MetaViewManager::ui_addTwistAxesLandmark(QString name, QString newValue){
		ContourCLj* joint = getJointObject(name.toStdString());
		joint->twistaxeslandmarks.push_back(newValue.toStdString());
		joint->twistaxeslandmarks_info.push_back("");
	}

	void MetaViewManager::ui_removeTwistAxesLandmarks(QString j, QString name){
		ContourCLj* joint = getJointObject(j.toStdString());
		for (unsigned int i = 0; i < joint->twistaxeslandmarks.size(); i++){
			if (joint->twistaxeslandmarks.at(i) == name.toStdString())
				joint->twistaxeslandmarks_info.erase(joint->twistaxeslandmarks_info.begin() + i);
		}
		joint->twistaxeslandmarks.erase(std::remove(joint->twistaxeslandmarks.begin(), joint->twistaxeslandmarks.end(), name.toStdString()), joint->twistaxeslandmarks.end());
	}

	QList<QString> MetaViewManager::ui_getTwistAxesLandmarksDesc(QString jt){
		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		for (unsigned int i = 0; i < joint->twistaxeslandmarks_info.size(); i++)
			if (joint->twistaxeslandmarks_info.at(i).size() != 0)
				list.append(QString::fromStdString(joint->twistaxeslandmarks_info.at(i)));
			else
				list.append(QString::fromStdString(""));
		return list;
	}

	QList<QString> MetaViewManager::ui_getJointCircumference(QString name){
		QList<QString> list;
		ContourCLj* joint = getJointObject(name.toStdString());
		for (unsigned int i = 0; i < joint->joint_circum_vec.size(); i++)
			list.append(QString::fromStdString(joint->joint_circum_vec.at(i)));
		return list;
	}

	void MetaViewManager::ui_addJointCircumference(QString name, QString newValue){
		ContourCLj* joint = getJointObject(name.toStdString());
		joint->joint_circum_vec.push_back(newValue.toStdString());
		joint->joint_circum_vec_info.push_back("");
	}

	void MetaViewManager::ui_removeJointCircumference(QString j, QString name){
		ContourCLj* joint = getJointObject(j.toStdString());
		for (unsigned int i = 0; i < joint->joint_circum_vec.size(); i++){
			if (joint->joint_circum_vec.at(i) == name.toStdString())
				joint->joint_circum_vec_info.at(i) = "";
		}
		joint->joint_circum_vec.erase(std::remove(joint->joint_circum_vec.begin(), joint->joint_circum_vec.end(), name.toStdString()), joint->joint_circum_vec.end());
	}

	QList<QString> MetaViewManager::ui_getBRCircumference(QString name){
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		QList<QString> list;
		for (unsigned int i = 0; i < br->circumfvec.size(); i++)
			list.append(QString::fromStdString(br->circumfvec.at(i)));
		return list;
	}

	void MetaViewManager::ui_addBRCircumference(QString br, QString newValue){
		ContourCLbr* b = getBodyRegionObject(br.toStdString());
		b->circumfvec.push_back(newValue.toStdString());
	}

	void MetaViewManager::ui_removeBRCircumference(QString bodyRegion, QString name){
		ContourCLbr* br = getBodyRegionObject(bodyRegion.toStdString());
		for (unsigned int i = 0; i < br->circumfvec_info.size(); i++){
			if (br->circumfvec.at(i) == name.toStdString())
				br->circumfvec_info.at(i) = "";
		}
		br->circumfvec.erase(std::remove(br->circumfvec.begin(), br->circumfvec.end(), name.toStdString()), br->circumfvec.end());
	}

	QList<QString> MetaViewManager::ui_getBRCircumDesc(QString name){
		QList<QString> list;
		ContourCLbr* br = getBodyRegionObject(name.toStdString());
		for (unsigned int i = 0; i < br->circumfvec_info.size(); i++)
			list.append(QString::fromStdString(br->circumfvec_info.at(i)));
		return list;
	}

	QList<QString> MetaViewManager::ui_getJointCircumDesc(QString jt){
		QList<QString> list;
		ContourCLj* joint = getJointObject(jt.toStdString());
		return list;
	}

	void MetaViewManager::ui_highlightCLElement(QString name, QString type){
		if (type.toStdString() == "landmark"){

		}
		if (type.toStdString() == "entity"){

		}
	}

	QString MetaViewManager::getTypeOfLandmark(QString name){
		std::string landmark = name.toStdString();
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata::LandmarkCont const& landmarks = Context::instance().project().model().metadata().landmarks();
			for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it) {
				if (it->second.name() == landmark)
					switch (it->second.type()){
					case Landmark::Type::POINT:{
						return(QString::fromStdString("POINT"));
						break;
					}
					case Landmark::Type::BARYCENTER:{
						return(QString::fromStdString("BARYCENTER"));
						break;
					}
					default:
					case Landmark::Type::SPHERE:{
						return(QString::fromStdString("SPHERE"));
						break;
					}
				}
			}
		}
		return "";
	}

	QList<double> MetaViewManager::getCoordinatesOfSelectedNode(){
		QList<double> coordinates;
		auto actors = Context::instance().display().GetDisplayActors();
		vtkUnstructuredGrid *contextModel = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh();
		vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(contextModel->GetPointData()->GetArray("nid"));
		for (auto it = actors->begin(); it != actors->end(); ++it)// for each actor
		{
			if (it->second->HasSelectedPoints())
			{
				auto sel_primitives = hbm::vtkSelectionTools::ObtainSelectionArrayPoints(it->second->GetOriginalDataSet(), false);
				int i = sel_primitives->GetNumberOfTuples() - 1;
				if (sel_primitives->GetValue(sel_primitives->GetNumberOfTuples()) == IS_PRIMITIVE_SELECTED)
				{
					FEModel& m_fem = Context::instance().project().model().fem();
					auto node = m_fem.get<Node>(nid->GetValue(i));
					coordinates.append(node->getCoordX());
					coordinates.append(node->getCoordY());
					coordinates.append(node->getCoordZ());
				}
			}
		}

		return  coordinates;
	}


	void MetaViewManager::displayOriginAxis(bool visible){
		if (visible){
            double size = 100;
            double pos_transform[16]{
                1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1
            };
            // set the position and size relative to the bounding box of the loaded fe model
            if (Context::instance().hasModel())
            {
                double *bounds = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh()->GetBounds();
                // translate to xmin, ymin, zmin
                pos_transform[3] = bounds[0];
                pos_transform[7] = bounds[2];
                pos_transform[11] = bounds[4];
                
                // get the shortest length, set size of each axis to 1/5 of that
                size = std::min(std::min(bounds[1] - bounds[0], bounds[3] - bounds[2]), bounds[5] - bounds[4]) / 5.0;                
            }

            Context::instance().display().AddAxesActor(draw.frame(pos_transform, size), "Origin_Axis", false);
		}
		else{
			Context::instance().display().RemoveActor("Origin_Axis");
		}
		Context::instance().display().Refresh(false);
	}

}

