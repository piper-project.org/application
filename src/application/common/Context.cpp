/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Context.h"

#include <exception>

#include <QDebug>
#include <QTimer>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QUrl>
#include <QDesktopServices>
#include <QStandardPaths>

#include <vtkMath.h>
#include <vtkXMLUnstructuredGridWriter.h>

#include "helper.h"
#include "memoryUsage.h"
#include "version.h"
#include "anatomydb/query.h"

#include <units/units.h>

#include <hbm/Helper.h>
#include <hbm/FEModelVTK.h>
#include <functional>

namespace piper {

	using namespace hbm;

	// explicit wrapping for Project::importModel because QtConcurrent::run does not support more than 5 parameters (6 required with the generic piper::wrapClassMethodCall solution)
	void wrap_Project_importModel(Project *project, std::string const& modelrulesFile) {
		try {
			project->importModel( modelrulesFile);
		}
		catch (std::exception const& e) {
            pCritical() << e.what();
            project->model().clear();
		}
	}

	void wrap_Project_addEnvironment(Project *project, std::string const& name, std::string const& filename,
		std::string const& formatrulefile, units::Length lengthUnit) {
		try {
			project->addEnvironment(name, filename, formatrulefile, lengthUnit);
		}
		catch (std::exception const& e) {
			pCritical() << e.what();
		}
	}

	void wrap_HumanBodyModel_read(hbm::HumanBodyModel *model, std::string const& filename) {
		try {
			model->read(filename);
		}
		catch (std::exception const& e) {
			model->clear();
			pCritical() << e.what();
		}
	}

	void wrap_HumanBodyModel_write(hbm::HumanBodyModel *model, std::string const& filename) {
		try {
			//std::cout << filename << std::endl;
			model->write(filename);
		}
		catch (std::exception const& e) {
			pCritical() << e.what();
		}
	}

    bool Context::isTargetLoaded()
    {
        if (m_project.target().size() == 0)
            return false;
        else
            return true;
    }

	Context& Context::instance() {
		static Context context;
		if (qEnvironmentVariableIsSet("PIPER_BATCH_MODE"))
			throw std::runtime_error("Context cannot be accessed in batch mode");
		return context;
	}

	void Context::performAsynchronousOperation(std::function<void()> operation, bool modelChanged, bool modelUpdated, bool metadataUpdated, bool target, bool environment)
	{
		m_opWatcherLock.lock();
		if (m_operationWatcher.future().isRunning())
		{
			m_opWatcherLock.unlock();
			qDebug() << WARNING << "operationWatcher already has a task, the new task will be now handled sequentially."
				<< "If this task is being invoked by the task currently observed by the operationWatcher, it will run within that \"parent\" task's thread, but consider refactoring your code to avoid this anyway."
				<< "But if this task is being invoked from a completely different, parallel thread, it is very likely it will cause data corruption and crashes - refactor your code if that is the case.";
			operation();
			if (modelChanged) emit this->modelChanged();
			if (modelUpdated) emit this->modelUpdated();
			if (metadataUpdated) emit this->metadataChanged();
			if (target) emit this->targetChanged();
			if (environment) emit this->envChanged();
			qDebug() << WARNING << "Sequential task done.";
		}
		else
		{
			setOperationSignals(modelChanged, modelUpdated, target, environment);
			m_display.PrepareRenderBlockingAction(m_operationWatcher);
			m_operationWatcher.setFuture(QtConcurrent::run(operation));
			m_opWatcherLock.unlock();
		}
	}

	Context::Context()
		: m_isBusy(false)
		, m_busyCancelVisible(false)
		, m_busyTaskCancelRequested(false)
		, m_memoryUsage(0)
	{
        m_display.SetDisplayMode(DISPLAY_MODE::ENTITIES); // show entities by default

		connect(&m_operationWatcher, &QFutureWatcher<void>::started, this, &Context::setBusy);
		connect(this, &Context::modelChanged, this, &Context::reloadVisualization); // whenever model changes, reload visualization
		connect(this, &Context::modelChanged, this, &Context::metadataChanged); // whenever model changes, metadata change
		connect(this, &Context::modelUpdated, this, &Context::updateModelVTKRepresentation); // whenever model updates, update the vtk representation
		connect(this, &Context::modelChanged, this, &Context::historyListChanged); // whenever model changes or updates, update the history list in GUI
		connect(this, &Context::modelUpdated, this, &Context::historyListChanged);
		connect(this, &Context::envChanged, this, &Context::createEnvironment);

		// recalculate visualization properties (landmark radius and frame lengths) when model changes to the current units
		connect(this, &Context::modelChanged, [this]() {
			m_display.POINTHIGHLIGHT_RADIUS = m_display.POINTHIGHLIGHT_DEF_RADIUS;
			units::convert(units::Length::m, project().model().metadata().lengthUnit(), m_display.POINTHIGHLIGHT_RADIUS); });
		connect(this, &Context::metadataChanged, [this]() {
			m_display.POINTHIGHLIGHT_RADIUS = m_display.POINTHIGHLIGHT_DEF_RADIUS;
			units::convert(units::Length::m, project().model().metadata().lengthUnit(), m_display.POINTHIGHLIGHT_RADIUS); });
		connect(this, &Context::modelChanged, [this]() {
			m_display.FRAME_LENGTH = m_display.FRAME_DEF_LENGTH;
			units::convert(units::Length::m, project().model().metadata().lengthUnit(), m_display.FRAME_LENGTH); });
		connect(this, &Context::metadataChanged, [this]() {
			m_display.FRAME_LENGTH = m_display.FRAME_DEF_LENGTH;
			units::convert(units::Length::m, project().model().metadata().lengthUnit(), m_display.FRAME_LENGTH); });

		// connections for environment display
		connect(this, &Context::envScaled, &display(), &VtkDisplay::SetActorScale_qt);
		connect(this, &Context::envScaled, &display(), &VtkDisplay::Render);
		connect(this, &Context::envTranslated, &display(), &VtkDisplay::SetActorTranslation_qt);
		connect(this, &Context::envTranslated, &display(), &VtkDisplay::Render);
		connect(this, &Context::envRotated, &display(), &VtkDisplay::SetActorRotation_qt);
		connect(this, &Context::envRotated, &display(), &VtkDisplay::Render);
		connect(this, &Context::envVisibleChanged, &display(), static_cast<void (VtkDisplay::*)(QString, bool visible)>(&VtkDisplay::SetActorVisible));
		connect(this, &Context::envVisibleChanged, &display(), &VtkDisplay::Render);
		connect(this, &Context::envOpacityChanged,
			[this](float arg){ display().SetDisplayModeOpacity(DISPLAY_MODE::ENVIRONMENT, arg); });
		connect(this, &Context::envOpacityChanged, &display(), &VtkDisplay::Render);

		connect(&m_octaveProcess, SIGNAL(started()), this, SLOT(setBusy()));
		connect(&m_octaveProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(setNotBusy()));

		// update the busyTaskCancelRequested flag when the task is finally cancelled
		connect(this, &Context::busyTaskCancelled, [this]() {
			if (!m_busyTaskCancelRequested) return;
			m_busyTaskCancelRequested = false;
			busyTaskCancelRequestedChanged(false);
		});

		// regular update of memory usage
		QTimer *timer = new QTimer(this);
		connect(timer, SIGNAL(timeout()), this, SLOT(updateMemoryUsage()));
		timer->start(1000);
	}

	bool Context::isBusy() const
	{
		return m_isBusy;
	}

	bool Context::busyCancelVisible() const
	{
		return m_busyCancelVisible;
	}

	void Context::setBusyCancelVisible(bool busyCancelVisible)
	{
		if (busyCancelVisible == m_busyCancelVisible)
			return;
		m_busyCancelVisible = busyCancelVisible;
		busyCancelVisibleChanged(m_busyCancelVisible);
	}

	void Context::setOperationSignals(bool modelChanged, bool modelUpdated, bool target, bool environment)
	{
		// disconect all
		disconnect(&m_operationWatcher, &QFutureWatcher<void>::finished, 0, 0);
		// internal method
		connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::onOperationWatcherFinished);
		// select correct signals

		// The order matters a lot...maybe too much. Apparently once QFutureWatcher<void>::finished is triggered,
		// the operationwatcher resolves all methods (registerd through connect) in a list tied to it in the order they were connected,
		// but if something modifies that list, the subsequent methods won't be called!
		// A better way would be to put all the connected method on a stack/queue once the signal is emitted and then resolve all of them,
		// regardless of what each of them does. But I don't know if there is such implementation available somewhere.
		// As things are now, if the first signal leads to calling setOperationSignals, it will cancel
		// emission of other previously set signals (by calling the diconnect above).
		// The current order works for the current operations...but it's not exactly bulletproof system.
		// If modelChanged is set, modelUpdated and targetUpdated are obsolete anyway, so it should be resolved before them.
		// However, environment and modelChanged do not rely on each other in any way, so if some operation sets both of them,
		// it might just happen that one will cancel the other. So far, such operation is only loading new project and for that
		// it works in this order - first enviro then model - the other way does not work so don't change it!
		if (environment)
			connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::envChanged);
		if (modelChanged)
			connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::modelChanged);
		if (modelUpdated)
			connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::modelUpdated);
		if (target)
			connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::targetChanged);

		// keep this one at the end
		connect(&m_operationWatcher, &QFutureWatcher<void>::finished, this, &Context::operationFinished);

	}

	void Context::setBusy()
	{
		if (m_isBusy)
			return;
		m_isBusy = true;
		emit busyChanged(m_isBusy);
	}
	void Context::setNotBusy()
	{
		if (!m_isBusy)
			return;
		m_isBusy = false;
		setBusyCancelVisible(false);
		emit busyChanged(m_isBusy);
	}

	void Context::onCancelTaskButtonClicked()
	{
		if (m_busyTaskCancelRequested) return;
		m_busyTaskCancelRequested = true;
		busyTaskCancelRequestedChanged(true);
	}

	QString Context::modelLengthUnit()  {
		return QString::fromStdString(units::LengthUnit::unitToStr(project().model().metadata().lengthUnit()));
	}

	QString Context::tempDirectoryPath(QString module, bool doCreate)
	{
		return QString::fromStdString(piper::tempDirectoryPath(module.toStdString(), doCreate));
	}

    QString Context::shareDirPath() const
    {
        return piper::shareFolderPath();
    }

    QString Context::userHomeDirPath() const
    {
        QStringList home = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
        if (home.size()>0)
            return home[0];
        else
            return "";

    }

	void Context::clearProject()
	{
		m_project.clear();
		m_display.RemoveAllActors();
		loadedModelActors.clear();
		setProjectFile(QUrl());
		emit modelChanged();
		emit envChanged();
		emit targetChanged();
		emit operationFinished();
	}

	void Context::log(QString const& message) {
		pInfo() << message;
	}
	void Context::logSuccess(QString const& message) {
		pInfo() << piper::SUCCESS << message;
	}
	void Context::logInfo(QString const& message) {
		pInfo() << piper::INFO << message;
	}
    void Context::logInfo(std::string const& message) {
        logInfo(QString::fromStdString(message));
    }
	void Context::logDebug(QString const& message) {
		pDebug() << message;
	}
	void Context::logStart(QString const& message) {
		pInfo() << piper::START << message;
	}
	void Context::logDone(QString const& message) {
		pInfo() << piper::DONE << message;
	}
	void Context::logWarning(QString const& message) {
		pWarning() << message;
	}
    void Context::logWarning(std::string const& message) {
        logWarning(QString::fromStdString(message));
    }
	void Context::logError(QString const& message) {
		pCritical() << message;
	}
	void Context::logFailed(QString const& message) {
		pInfo() << piper::FAILEDMSG << message;
	}

	void Context::showUserGuideHTML() {
		//        if (QProcess::NotRunning == m_qtAssistantProcess.state()) {
		//            // let's start
		//            m_qtAssistantProcess.setProgram(QCoreApplication::applicationDirPath() + "/assistant");
		//            m_qtAssistantProcess.setArguments(QStringList() << "-collectionFile" << qtHelpFilePath());
		//            m_qtAssistantProcess.start();
		//            if (!m_qtAssistantProcess.waitForStarted()) {
		//                pWarning() << "The documentation browser cannot be started";
		//                return;
		//            }
		//        }
		//        else {
		//            // TODO find a way to ask the windowing system to raise the assistant window above other windows
		//        }
        QUrl docUrl(QCoreApplication::applicationDirPath() + "/../share/doc/html/index.html");
        docUrl = QUrl::fromLocalFile(docUrl.toString(QUrl::NormalizePathSegments));
        if (!QDesktopServices::openUrl(docUrl))
            pWarning() << "Documentation was not found in " << docUrl.toString() << ", check your installation";
	}

    void Context::showUserGuidePDF() {
        QUrl docUrl(QCoreApplication::applicationDirPath() + "/../share/doc/user-guide.pdf");
        docUrl = QUrl::fromLocalFile(docUrl.toString(QUrl::NormalizePathSegments));
        if (!QDesktopServices::openUrl(docUrl))
            pWarning() << "Documentation was not found in " << docUrl.toString() << ", check your installation";
    }

	void Context::newEmptyProject()
	{
		clearProject();
		pInfo() << INFO << "New empty project";
	}

	void Context::importModel(const QUrl& modelRulesFile)
	{
		performAsynchronousOperation(std::bind(&wrap_Project_importModel, &m_project,modelRulesFile.toLocalFile().toStdString()),
			true, false, false, false, false);
	}

    void Context::openProject(const QUrl& projectFile) {
        m_display.RemoveAllActors();
        loadedModelActors.clear();
		setProjectFile(projectFile);

		void(*readProjectWrapper)(piper::Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;

		performAsynchronousOperation(std::bind(readProjectWrapper, &m_project, &Project::read, projectFile.toLocalFile().toStdString()),
			true, false, false, true, true);
	}

	QString const Context::currentModel() {
		return QString::fromStdString(m_project.model().history().getCurrent());
	}

	void Context::setCurrentModel(QString const& historyname) {
		try{
			project().setCurrentModel(historyname.toStdString());
		}
		catch (std::exception e){
			pCritical() << e.what();
			return;
		}
		emit modelChanged();
	}

	void Context::reloadFEMesh() {
		void(*reloadFEMeshWrapper)(piper::Project*, void (Project::*)())
			= &piper::wrapClassMethodCall<Project, void (Project::*)()>;

		performAsynchronousOperation(std::bind(reloadFEMeshWrapper, &m_project, &Project::reloadFEMesh),
			true, false, false, false, false);
	}



	QStringList const Context::getHistory() {
		QStringList Qhistory;
		std::vector<std::string> const history = m_project.model().history().getHistoryList();
		for (std::string const& hist : history)
			Qhistory.push_back(QString::fromStdString(hist));
		return Qhistory;
	}

	QStringList Context::getHistoryList()
	{
		QStringList Qhistory;
		std::vector<std::string> const history = m_project.model().history().getHistoryList();
		for (std::string hist : history)
			Qhistory.push_back(QString::fromStdString(hist));
		return Qhistory;
	}

	QString Context::getValidHistoryName(QString const& historyName)
	{
		return QString::fromStdString(m_project.model().history().getValidHistoryName(historyName.toStdString()));
	}

    void Context::addNewHistory(std::string const& historyName) {
        m_project.model().history().addNewHistory(
            m_project.model().history().getValidHistoryName(historyName)
            );
        emit historyListChanged();
    }

	void Context::addNewHistory(QString const& historyName)
	{
        addNewHistory(historyName.toStdString());
	}

	void Context::renameCurrentModel(QString const& proposal) {
		try{
			project().model().history().renameCurrentModel(proposal.toStdString());
		}
		catch (std::exception e){
			pCritical() << e.what();
			return;
		}
		emit historyListChanged();
	}


	void Context::updateModelVTKRepresentation()
	{
		if (!project().model().empty())
		{
			project().model().fem().updateVTKRepresentation();
			if (m_display.GetRenderNormals()) // recompute normals if they are visualized
				ShowNormals(true);
			else // if normals are computed, it will call refresh anyway
				m_display.Render();
		}
	}

	void Context::BlankSelectedElements()
	{
		performAsynchronousOperation(std::bind(&VtkDisplay::BlankSelectedElements, &m_display),
			false, false, false, false, false);
	}

	void Context::Unblank(bool unblankAll)
	{
		performAsynchronousOperation(std::bind(&VtkDisplay::Unblank, &m_display, unblankAll),
			false, false, false, false, false);
	}

    void Context::ExportSelectedElements(const QUrl& exportFolder)
    {
        std::string folder = exportFolder.toLocalFile().toStdString();
        performAsynchronousOperation(std::bind(&FEModelVTK::writeSelectedElements, project().model().fem().getFEModelVTK(),
            folder, project().model().metadata().entities(), project().model().fem()),
            false, false, false, false, false);
    }

    void Context::DeselectBoneElements()
    {
        VId boneIDs1D{}, boneIDs2D{}, boneIDs3D{};
        FEModel &fem = project().model().fem();
        for (auto &entity : project().model().metadata().entities())
        {
            // concatenate all IDs of elements that belong to a bony entity into a single vector
            if (anatomydb::isBone(entity.first))
            {
                const VId& groupElement3D = entity.second.get_groupElement3D();
                for (Id groupID : groupElement3D) {
                    const VId& sid = fem.getGroupElements3D(groupID).get();
                    boneIDs3D.insert(boneIDs3D.end(), sid.begin(), sid.end());
                }
                const VId& groupElement2D = entity.second.get_groupElement2D();
                for (Id groupID : groupElement2D) {
                    const VId& sid = fem.getGroupElements2D(groupID).get();
                    boneIDs2D.insert(boneIDs2D.end(), sid.begin(), sid.end());

                }
                const VId& groupElement1D = entity.second.get_groupElement1D();
                for (Id groupID : groupElement1D) {
                    const VId& sid = fem.getGroupElements1D(groupID).get();
                    boneIDs1D.insert(boneIDs1D.end(), sid.begin(), sid.end());
                }
            }
        }
        fem.getFEModelVTK()->deselectElementsByEid(boneIDs1D, boneIDs2D, boneIDs3D);
        display().UpdateActor("full_model");
    }

	void Context::ShowNormals(bool show)
	{
		if (m_display.GetRenderWindow()->GetContextSupportsOpenGL32()) // this means setting normals would be unsuccessful
			performAsynchronousOperation(std::bind(&VtkDisplay::SetRenderNormals, &m_display, show),
			false, false, false, false, false);
		else
			logWarning(QString("Displaying normals requires a graphics hardware supporting OpenGL standard version 3.2. Such hardware was not detected, we apologize for the inconvenience."));
	}


	void Context::saveProject(const QUrl& projectFile)
	{
		QString _projectFile(projectFile.toLocalFile());
		if (QFileInfo(_projectFile).suffix() != "ppj")
			_projectFile += ".ppj";

		setProjectFile(QUrl::fromLocalFile(_projectFile));

		void(*writeProjectWrapper)(piper::Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;

		performAsynchronousOperation(std::bind(writeProjectWrapper, &m_project, &Project::write, _projectFile.toStdString()),
			false, false, false, false, false);
	}

	void Context::loadModel(const QUrl& modelFile)
	{
		void(*loadModelWrapper)(piper::Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;

		performAsynchronousOperation(std::bind(loadModelWrapper, &m_project, &Project::loadModel, modelFile.toLocalFile().toStdString()),
			true, false, false, false, false);
	}

	void Context::saveModel(const QUrl& modelFile)
	{
		QString _modelFile(modelFile.toLocalFile());
		if (QFileInfo(_modelFile).suffix() != "pmd")
			_modelFile += ".pmd";

		void(*saveModelWrapper)(piper::Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;

		performAsynchronousOperation(std::bind(saveModelWrapper, &m_project, &Project::saveModel, _modelFile.toStdString()),
			false, false, false, false, false);
	}

    void Context::loadMetadata(const QUrl& metadataFile, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool anatomicalJoint, bool controlpoint, bool HbmParameter)
	{
        void(*loadMetadataWrapper)(piper::Project*, void (Project::*)(std::string const&, bool, bool, bool, bool, bool, bool, bool, bool, bool), std::string const&, bool, bool, bool, bool, bool, bool, bool, bool, bool)
            = &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&, bool, bool, bool, bool, bool, bool, bool, bool, bool), std::string const&, bool, bool, bool, bool, bool, bool, bool, bool, bool>;

        performAsynchronousOperation(std::bind(loadMetadataWrapper, &m_project, &Project::loadMetadata, metadataFile.toLocalFile().toStdString(), anthropometry, entity, landmark, genericmetadata, contact, joint, anatomicalJoint, controlpoint, HbmParameter),
			true, false, true, false, false);
	}

	void Context::saveMetadata(const QUrl& metadataFile, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool controlpoint, bool HbmParameter)
	{
		QString _metadataFile(metadataFile.toLocalFile());
		if (QFileInfo(_metadataFile).suffix() != "pmr")
			_metadataFile += ".pmr";

		void(*saveMetadataWrapper)(piper::Project*, void (Project::*)(std::string const&, bool, bool, bool, bool, bool, bool, bool, bool), std::string const&, bool, bool, bool, bool, bool, bool, bool, bool)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&, bool, bool, bool, bool, bool, bool, bool, bool), std::string const&, bool, bool, bool, bool, bool, bool, bool, bool>;

		performAsynchronousOperation(std::bind(saveMetadataWrapper, &m_project, &Project::saveMetadata, _metadataFile.toStdString(), anthropometry, entity, landmark, genericmetadata, contact, joint, controlpoint, HbmParameter),
			false, false, false, false, false);
	}


	void Context::exportModel(const QUrl& modelDirectory)
	{
		void(*exportModelWrapper)(Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;
		performAsynchronousOperation(std::bind(
			exportModelWrapper,
			&m_project, &Project::exportModel, modelDirectory.toLocalFile().toStdString()),
			false, false, false, false, false);
	}

	void Context::loadTarget(const QUrl& targetFile)
	{
		void(*loadTargetWrapper)(Project*, void (Project::*)(std::string const&), std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&), std::string const&>;
		performAsynchronousOperation(
			std::bind(loadTargetWrapper, &m_project, &Project::loadTarget, targetFile.toLocalFile().toStdString()),
			false, false, false, true, false);
	}

	void Context::saveTarget(const QUrl& targetFile)
	{
		QString _targetFile = targetFile.toLocalFile();
		if (QFileInfo(_targetFile).suffix() != "ptt")
			_targetFile += ".ptt";

		void(*saveTargetWrapper)(Project*, void (Project::*)(std::string const&) const, std::string const&)
			= &piper::wrapClassMethodCall<Project, void (Project::*)(std::string const&) const, std::string const&>;
		performAsynchronousOperation(
			std::bind(saveTargetWrapper, &m_project, &Project::saveTarget, _targetFile.toStdString()),
			false, false, false, false, false);
	}

	void Context::addenvModel(const std::string &name, const std::string &envmodelFile, const std::string &formatRulesFile, units::Length lengthUnit)
	{
		if (m_display.HasActor(name))
			pCritical() << "Environment not added: an object with the same name (" << name << ") already exists. Remove the existing one or rename this one.";
		else
			performAsynchronousOperation(std::bind(&wrap_Project_addEnvironment, &m_project, name,
			envmodelFile, formatRulesFile, lengthUnit),
			false, false, false, false, true);
	}

	void Context::removeEnvModel(const std::string &name)
	{
		Context::instance().project().environment().deleteEnvironmentModel(name);
		m_display.RemoveActor(name);
		pInfo() << INFO << "Environment model deleted: " << name;
		emit envChanged();
	}

	void Context::clearTarget()
	{
		size_t s = m_project.target().size();
		m_project.target().clear();
		pInfo() << INFO << "Cleared " << s << " target(s)";
		emit targetChanged();
		emit operationFinished();
	}

	void Context::exportLandmarks(const QUrl &filename)
	{
		std::size_t nb = project().model().exportLandmarks(filename.toLocalFile().toStdString());
		pInfo() << INFO << nb << " landmarks exported to " << filename.toLocalFile();
	}

	void Context::exportEntityNodes(const QUrl &directory)
	{
		performAsynchronousOperation(
			std::bind(&HumanBodyModel::exportEntityNodes, &project().model(),
			directory.toLocalFile().toStdString()),
			false, false, false, false, false);
	}

	void Context::setProjectFile(QUrl const& projectFile)
	{
		m_projectFile = projectFile;
		emit projectFileChanged();
	}

	void Context::onOperationWatcherFinished()
	{
		setNotBusy();
	}

	void Context::updateMemoryUsage()
	{
		m_memoryUsage = (unsigned int)(getMemoryUsage() / 1048576.0); // convert from bytes to MB: 1024 * 1024 = 1048576.0
		emit memoryUsageChanged();
	}

	void Context::reloadDisplay()
	{
		reloadVisualization();
	}

	void Context::reloadLandmarkDisplay(){
		loadDataIntoDisplay(false, false, true, false, true);
	}

#pragma region VtkDisplayManagement

	void Context::loadDataIntoDisplay(bool loadFullMesh, bool loadEntities, bool loadLandmarks, bool loadGenericMetadatas, bool forceReload)
	{
		if (!m_project.model().empty())
		{
			loadFullMesh = loadFullMesh && (forceReload || !fullModelLoadedInDisplay);
			loadEntities = loadEntities &&
				(forceReload || !entitiesLoadedInDisplay || m_project.model().fem().getFEModelVTK()->getVTKEntityMesh()->size() != m_project.model().metadata().entities().size());
			loadLandmarks = loadLandmarks && (forceReload || !landmarksLoadedInDisplay);
			loadGenericMetadatas = loadGenericMetadatas &&
				(forceReload || !genericMetadatasLoadedInDisplay || m_project.model().fem().getFEModelVTK()->getVTKGenericMetadataMesh()->size() != m_project.model().metadata().genericmetadatas().size());

			performAsynchronousOperation(std::bind(&Context::doLoadDataIntoDisplay, this, loadFullMesh, loadEntities, loadLandmarks, loadGenericMetadatas),
				false, false, false, false, false);
		}
		else
		{
			// if the model is empty, mark these flags as if something was loaded - after all, all of the loaded data (none) is visualized as requested
			// without this, data won't be visualized if user loads something AFTER this function was called - the reloadVisualization needs to know what type of data is requested
			fullModelLoadedInDisplay = loadFullMesh;
			entitiesLoadedInDisplay = loadEntities;
			landmarksLoadedInDisplay = loadLandmarks;
			genericMetadatasLoadedInDisplay = loadGenericMetadatas;
			m_display.Refresh(); // if the project was not empty and was cleaned now, call refresh to get rid of the old actors
			emit visDataLoaded();
		}
	}

	void Context::doLoadDataIntoDisplay(bool loadFullMesh, bool loadEntities, bool loadLandmarks, bool loadGenericMetadatas)
	{
		if (loadFullMesh)
			createActorFullHBM();
		if (loadEntities)
			createEntityActorsFromHBM();
		if (loadLandmarks)
			createLandmarkActorsFromHBM();
		if (loadGenericMetadatas)
			createGenericMetadataActorsFromHBM();

		m_display.Refresh();
		emit visDataLoaded();
	}

	void Context::createActorFullHBM()
	{
		pInfo() << START << QStringLiteral("Loading FE model to visualize...");
		// Get the fem model and intilize the VTK Unstructured grid
		FEModel& fem = m_project.model().fem();
		FEModelVTK* vtkUnstructuredMesh = fem.getFEModelVTK();
		//Create a mapper and actor
		std::string name = objectName().toStdString();
		if (name == "")
			name = "full_model";

		m_display.AddVtkPointSet(vtkUnstructuredMesh->getVTKMesh(), name, true, DISPLAY_MODE::FULL_MESH);
		loadedModelActors.push_back(name);
		fullModelLoadedInDisplay = true;
		pInfo() << DONE;
	}

	void Context::createEntityActorsFromHBM()
	{
		pInfo() << START << QStringLiteral("Loading entities of the FE model to visualize...");

		// Get the fem model and intilize the VTK Unstructured grid
		HumanBodyModel* m_hbm = &m_project.model();
		FEModel& fem = m_hbm->fem();
		FEModelVTK* vtkUnstructuredEntityMesh = fem.getFEModelVTK();
		if (fem.getFEModelVTK()->getVTKEntityMesh()->size() != m_hbm->metadata().entities().size()) // only reload the entities if their numbers do not add up
			vtkUnstructuredEntityMesh->setMeshAsEntities(m_hbm->metadata().entities(), fem, false);
		auto map = vtkUnstructuredEntityMesh->getVTKEntityMesh();
		// for each entity, create its actor
        vtkMath::RandomSeed(7);
		for (auto it = map->begin(); it != map->end(); it++)
		{
			m_display.AddVtkPointSet(it->second, it->first, true, DISPLAY_MODE::ENTITIES);
			m_display.SetObjectColor(it->first, vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0));
			loadedModelActors.push_back(it->first);
		}
		entitiesLoadedInDisplay = true;
		pInfo() << DONE;
	}

	void Context::createLandmarkActorsFromHBM()
	{
		pInfo() << START << QStringLiteral("Loading landmarks of the FE model to visualize...");
		HumanBodyModel* m_hbm = &m_project.model();
		FEModel& fem = m_hbm->fem();
		fem.getFEModelVTK()->setModelLandmarks(m_hbm->metadata().landmarks(), fem, false);
		// create an actor for the landmarks - all points of the full mesh
		m_display.AddGroupOfPoints(fem.getFEModelVTK()->getVTKMesh(), CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME, true, m_display.POINTHIGHLIGHT_RADIUS,
			ACTIVE_LANDMARK_POINTS, DISPLAY_MODE::LANDMARKS, true, true, fem.getFEModelVTK());
		loadedModelActors.push_back(CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME);
		m_display.SetObjectColor(CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME, m_display.highlights._p_LandmarkColor);
		m_display.AddGroupOfPoints(fem.getFEModelVTK()->getLandmarkMesh(), CONTEXT_MODEL_LANDMARKS_BY_POS_NAME, true,
			m_display.POINTHIGHLIGHT_RADIUS, ACTIVE_LANDMARK_POINTS, DISPLAY_MODE::LANDMARKS);
		loadedModelActors.push_back(CONTEXT_MODEL_LANDMARKS_BY_POS_NAME);
		m_display.SetObjectColor(CONTEXT_MODEL_LANDMARKS_BY_POS_NAME, m_display.highlights._p_LandmarkColor);
		landmarksLoadedInDisplay = true;
		pInfo() << DONE;
	}

	void Context::createGenericMetadataActorsFromHBM()
	{
        pInfo() << START << "Loading named metadata of the FE model to visualize...";
        // Get the fem model and intilize the VTK Unstructured grid
        HumanBodyModel* m_hbm = &m_project.model();

        FEModel& fem = m_hbm->fem();
        FEModelVTK* vtkUnstructuredGenericMetadataMesh = fem.getFEModelVTK();
        if (fem.getFEModelVTK()->getVTKGenericMetadataMesh()->size() != m_hbm->metadata().genericmetadatas().size()) // only reload the genericMetadatas if their numbers do not add up
            vtkUnstructuredGenericMetadataMesh->setMeshAsGenericMetadatas(m_hbm->metadata().genericmetadatas(), fem, false);
        auto map = vtkUnstructuredGenericMetadataMesh->getVTKGenericMetadataMesh();
        // for each genericMetadata, create its actor
        vtkMath::RandomSeed(7);
        for (auto it = map->begin(); it != map->end(); it++)
        {
            m_display.AddVtkPointSet(it->second, it->first, true, DISPLAY_MODE::GENERICMETADATA); // if at least one adding fails, stop the process
            m_display.UpdateActor(it->first); // the actor must be updated before highlighting, otherwise the mapper won't have the data loaded
            std::string hightlightActorName = m_display.HighlightPoints(it->first, true, m_display.POINTHIGHLIGHT_RADIUS / m_display.POINTHIGHLIGH_SMALL_FACTOR, false);
            double r = vtkMath::Random(.4, 1.0), g = vtkMath::Random(.4, 1.0), b = vtkMath::Random(.4, 1.0);
            m_display.SetObjectColor(it->first, r, g, b);
            m_display.SetObjectColor(hightlightActorName, r, g, b);
            loadedModelActors.push_back(it->first);
        }
        m_display.ActivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS);
        genericMetadatasLoadedInDisplay = true;
        pInfo() << DONE;
	}

	void Context::createEnvironment()
	{
		EnvironmentModels& models = m_project.environment();
		if (models.getNumberenvironment() > 0)
		{
			pInfo() << START << QStringLiteral("Loading environment to visualize...");
			for (std::string name : models.getListNames())
			{
				if (!m_display.HasActor(name)) // reload only models that are not loaded yet
				{
					EnvironmentModelPtr m = models.getEnvironmentModel(name);
					if (m != NULL)
					{
						m_display.AddVtkPointSet(m->envmodel().getFEModelVTK()->getVTKMesh(),
							name, true, DISPLAY_MODE::ENVIRONMENT, false, true); // if at least one adding fails, stop the process
						m_display.SetObjectColor(name, _p_defaultEnviroColor);
                        Coord const& scale = m->envmodel().getScale();
                        m_display.SetActorScale(name, scale.x(), scale.y(), scale.z());
                        Coord const& rotation = m->envmodel().getRotation();
                        m_display.SetActorRotation(name, rotation.x(), rotation.y(), rotation.z());
                        Coord const& translation = m->envmodel().getTranslation();
                        m_display.SetActorTranslation(name, translation.x(), translation.y(), translation.z());
					}
				}
			}
			m_display.ActivateDisplayMode(DISPLAY_MODE::ENVIRONMENT);
			pInfo() << DONE;
		}
		else
			m_display.DeactivateDisplayMode(DISPLAY_MODE::ENVIRONMENT);
		m_display.Refresh(); // refresh even if there is no environment left - maybe envChanged was invoked because of removing
	}

	void Context::reloadVisualization()
	{
		if (fullModelLoadedInDisplay || entitiesLoadedInDisplay || landmarksLoadedInDisplay || genericMetadatasLoadedInDisplay)
		{
			// clear the display of the previously loaded data
			for (std::string name : loadedModelActors)
				m_display.RemoveActor(name, true);
			loadedModelActors.clear();
			bool oldFullModelFlag = fullModelLoadedInDisplay;
			bool oldEntityFlag = entitiesLoadedInDisplay;
			bool oldLandmarkFlag = landmarksLoadedInDisplay;
			bool oldGenericMetadataFlag = genericMetadatasLoadedInDisplay;

			fullModelLoadedInDisplay = false;
			entitiesLoadedInDisplay = false;
			landmarksLoadedInDisplay = false;
			genericMetadatasLoadedInDisplay = false;
			loadDataIntoDisplay(oldFullModelFlag, oldEntityFlag, oldLandmarkFlag, oldGenericMetadataFlag, false);
		}
	}

	bool Context::isSomethingSelected()
	{
		bool result = false;
		auto map = m_display.GetDisplayActors();

		//Create a copy of the humanbodymodel displayed in the scene
		piper::hbm::HumanBodyModel tempModelSelection = m_project.model();

		//For every entity composing this copy of the model
		for (auto it = map->begin(); it != map->end(); it++)
		{
			//If current entity have some cells selected, do things + return true.
			if (it->second->GetNoSelectedCells() > 0)
			{
				std::cout << "actors ID = " << it->second->GetName() << std::endl;
				std::cout << "nbSelectedCells = " << it->second->GetNoSelectedCells() << std::endl;
				result = true;
			}
			//If the current entity is fully selected, do things + return true
			else if (it->second->GetIsSelected())
			{
				std::cout << "actors ID = " << it->second->GetName() << std::endl;
				result = true;
			}
			//else remove this entity from the copy of the model.
			else
			{
				tempModelSelection.metadata().removeEntity(it->second->GetName());
			}
		}

		/*
		const piper::hbm::Metadata::EntityCont entityList = tempModelSelection.metadata().entities();
		for (auto it2 = entityList.begin(); it2 != entityList.end(); it2++)
		{
		std::cout << "actors IDFINAL = " << it2->second.name() << std::endl;
		}
		*/

		//If some scene elements have been selected
		if (result == true)
		{
			std::string dirPath = tempDirectoryPath().toStdString() + "/exportTest2";
			const std::vector<std::string>  vs;
			tempModelSelection.exportModel(dirPath, vs);
			//tempModelSelection.exportModelCustom(dirPath, vs, tempModelSelection.fem(),tempModelSelection.metadata());
		}
		else
			pInfo() << WARNING << QStringLiteral("No element displayed");
		return result;
	}

#pragma endregion

	QQuickWindow* Context::itemWindow(QQuickItem* item) const
	{
		if (!item)
			return nullptr;
		return item->window();
	}

    bool Context::doFolderExist(QUrl folderUrl)
    {
        QFileInfo folder(folderUrl.toLocalFile());
        if (folder.exists())
            return true;
        return false;
    }

    bool Context::isUNCPath(QUrl folderUrl)
    {
        QFileInfo folder(folderUrl.toLocalFile());
        if (folder.exists())
            return (folder.absolutePath().startsWith("\\")
                ||
                folder.absolutePath().startsWith("//"));// according to qt documentation the backslashes are converted into forward slashes...let's just check for both
        return false;
    }

}
