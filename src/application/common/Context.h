/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTEXT_H
#define PIPER_CONTEXT_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif


#include <mutex>
#pragma warning(push, 0)
#include <sofa/simulation/Simulation.h>
#pragma warning(pop)
#include "version.h"
#include "logging.h"
#include "Project.h"
#include "MetaViewManager.h"
#include "MetaEditor.h"
#include "OctaveProcess.h"
#include "PythonScript.h"
#include "VtkDisplay.h"

#include <QList>
#include <QUrl>
#include <QStringList>
#include <QTextStream>
#include <QCoreApplication>
#include <QFileInfo>


namespace piper {

	const std::string CONTEXT_MODEL_LANDMARKS_BY_POINTS_NAME = "full_mesh_landmark_points";
	const std::string CONTEXT_MODEL_LANDMARKS_BY_POS_NAME = "full_mesh_landmark_positions";

	/** Context of the application. It makes the glue between the GUI and the application core.
	*
	* \todo move the Context class away from the pipercommon lib when the logging system will not depend on it anymore
    * \author Thomas Lemaire \date 2014
    * \todo store the current module name
	*/
	class PIPERCOMMON_EXPORT Context : public QObject
	{
		Q_OBJECT
			Q_PROPERTY(bool isBusy READ isBusy NOTIFY busyChanged)
			Q_PROPERTY(bool busyCancelVisible READ busyCancelVisible WRITE setBusyCancelVisible NOTIFY busyCancelVisibleChanged)
			Q_PROPERTY(bool busyTaskCancelRequested READ busyTaskCancelRequested NOTIFY busyTaskCancelRequestedChanged)
			Q_PROPERTY(QUrl projectFile READ projectFile NOTIFY projectFileChanged)
			Q_PROPERTY(bool hasModel READ hasModel NOTIFY modelChanged)
			Q_PROPERTY(QString modelLengthUnit READ modelLengthUnit NOTIFY modelChanged)
			Q_PROPERTY(QStringList historylist READ getHistory NOTIFY historyListChanged)
			Q_PROPERTY(QString currentModel READ currentModel NOTIFY historyListChanged)
			Q_PROPERTY(unsigned int memoryUsage READ memoryUsage NOTIFY memoryUsageChanged)
			//    Q_PROPERTY(QString bodyDimDBFileName READ bodyDimDBFileName WRITE setBodyDimDBFileName) Where is it used ?

	public:

		/// @return the unique context
		static Context& instance();

		/// <summary>
		/// Performs the specified operation using the Context's operations watcher, i.e. runs it in worker thread that is asynchronous
		/// with the main thread. Also makes sure that rendering the Context's VtkDisplay is disabled for the duration of the operation to avoid
		/// the worker and the render thread accessing the data at the same time (which would cause crashes)
		/// </summary>
		/// <param name="operation">The operation to process. 
		/// To wrap your method call in the required std::function<void()>, use std::bind(pointer to your method, pointer to the owner of the method, arg1, arg2,...)</param>
		/// <param name="modelChanged">If set to <c>true</c>, modelChanged signal will be emitted after the operation finishes.</param>
		/// <param name="modelUpdated">If set to <c>true</c>, modelUpdated signal will be emitted after the operation finishes.</param>
		/// <param name="metadataUpdated">If set to <c>true</c>, metadataUpdated signal will be emitted after the operation finishes.</param>
		/// <param name="target">If set to <c>true</c>, targetChanged signal will be emitted after the operation finishes.</param>
		/// <param name="environment">If set to <c>true</c>, envChanged signal will be emitted after the operation finishes.</param>
		/// \todo use a flag style parameter instead of a list of bools, it would make the code more readable
		void performAsynchronousOperation(std::function<void()> operation, bool modelChanged, bool modelUpdated, bool metadataChanged, bool target, bool environment);

		bool isBusy() const;
		bool busyCancelVisible() const;
		void setBusyCancelVisible(bool busyCancelVisible);
		/// return true if the current task is requested to be cancelled, but not yet cancelled
		bool busyTaskCancelRequested() const { return m_busyTaskCancelRequested; }

		QUrl const& projectFile() const { return m_projectFile; }
		Project& project() { return m_project; }
		Project const& project() const { return m_project; }
		// Use this display to show data based on the loaded HBM
		VtkDisplay& display() { return m_display; }
		// Use this to manage which metadata of the loaded HBM are displayed in the display
		MetaViewManager& metaManager() { return m_metaManager; }
		// Use this to manage metadata editing tools
		MetaEditor& metaEditor() { return m_metaEditor; }

		void clearProject();

		bool hasModel() { return !project().model().empty(); }
		QString modelLengthUnit();

		unsigned int memoryUsage() const { return m_memoryUsage; }

		/// @todo remove this when it is avaible in a standard qml module
		Q_INVOKABLE QString applicationDirPath() const { return QCoreApplication::applicationDirPath(); }

		Q_INVOKABLE QString tempDirectoryPath(QString module = "", bool doCreate = false);

        Q_INVOKABLE QString shareDirPath() const;

        Q_INVOKABLE QString userHomeDirPath() const;

		Q_INVOKABLE QString versionDate() { return PIPER_VERSION_DATE; }
		Q_INVOKABLE QString versionGitHash() { return PIPER_VERSION_GIT_HASH; }

		Q_INVOKABLE piper::OctaveProcess* octaveProcessPtr() { return &m_octaveProcess; } // only OctaveProcess* fit the QMetaType system
		OctaveProcess & octaveProcess() { return m_octaveProcess; }

		//    QString bodyDimDBFileName();

		Q_INVOKABLE unsigned int verboseLevel() const { return static_cast<unsigned int>(LoggingParameter::instance().verboseLevel()); }
		Q_INVOKABLE void setVerboseLevel(unsigned int level) { LoggingParameter::instance().setVerboseLevel(static_cast<LoggingParameter::VerboseLevel>(level)); }

		QString const currentModel();
        Q_INVOKABLE void setDefaultModuleParameter() {m_project.moduleParameter().setDefault();}
		QStringList const getHistory();
		// accessing list as Q_PROPERTY is ineffective and apparently can cause some bugs - when you want to only read the list
		// without assigning it as a dataModel for some qml component, use this method instead of the historyList property
		Q_INVOKABLE QStringList getHistoryList();
		Q_INVOKABLE QString getValidHistoryName(QString const& historyName);
        void addNewHistory(std::string const& historyName);
		Q_INVOKABLE void addNewHistory(QString const& historyName);
		Q_INVOKABLE void renameCurrentModel(QString const& proposal);
		// updates the VTK representation of the current project's FE model
		Q_INVOKABLE void updateModelVTKRepresentation();

		/// <summary>
		/// Invokes the method "BlankSelectedElements" of the context's VtkDisplay as an asynchronous operation (removes all selected elements from the scene).
		/// </summary>
        Q_INVOKABLE void BlankSelectedElements();

        /// <summary>
        /// Invokes the method "Unblank" of the context's VtkDisplay as an asynchronous operation (restores actors to their unblanked state)
        /// </summary>
        /// <param name="UnblankAll">If set to <c>true</c>, restores the very original state of the actor.
        /// If set to <c>false</c>, restores only to the state before the last blank.</param>
        Q_INVOKABLE void Unblank(bool unblankAll);

        /// <summary>
        /// Exports selected elements of the current mesh to the specified folder. Each connected component is saved as a separate vtkUnstructuredGrid.
        /// </summary>
        Q_INVOKABLE void ExportSelectedElements(const QUrl& exportFolder);

        /// <summary>
        /// Deselects all elements that belong to bone entities 
        /// </summary>
        Q_INVOKABLE void DeselectBoneElements();

		/// <summary>
		/// Invokes the method "SetRenderNormals" of the context's VtkDisplay as an asynchronous operation (renders normals of all actors)
		/// </summary>
		/// <param name="show">If set to <c>true</c>, normals will be rendered, otherwise thew will be hidden.</param>
		Q_INVOKABLE void ShowNormals(bool show);

		/// <summary>
		/// Loads data from the current hbm into the context's VtkDisplay.
		/// </summary>
		/// <param name="loadFullMesh">If set to <c>true</c>, an actor representing the whole FE mesh will be created.
		/// The actor is added to the display in the FULL_MESH display mode.</param>
		/// <param name="loadEntities">If set to <c>true</c>, actors for individual entites of the FE model will be created.
		/// All these actors are added to the display in the ENTITIY display mode.</param>
		/// <param name="loadLandmarks">If set to <c>true</c>, two actors representing the FE model's landmarks will be created.
		/// <param name="loadGenericMetadatass">If set to <c>true</c>, actors for individual genericMetadatas of the FE model will be created.
		/// All these actors are added to the display in the GENERICMETADATA display mode.</param>
		/// One represents each landmark as the group of points that it is defined by, the other by the final position of the landmark (i.e. as a single point).
		/// Both are added in the LANDMARK display mode.</param>
		/// <param name="forceReload">If set to <c>true</c>, the selected data will be force to reload. Otherwise (default), it will be loaded only if it was not loaded yet.</param>
		Q_INVOKABLE void loadDataIntoDisplay(bool loadFullMesh, bool loadEntities, bool loadLandmarks, bool loadGenericMetadatas, bool forceReload = false);

		Q_INVOKABLE QQuickWindow* itemWindow(QQuickItem* item) const;

		void removeEnvModel(std::string const& name);

		void addenvModel(std::string const& name, std::string const& envmodelFile, std::string const& formatRulesFile, units::Length lengthUnit);


		public slots:

		void setBusy();
		void setNotBusy();

		/// slot called when the cancel button is clivked
		void onCancelTaskButtonClicked();

		void newEmptyProject();
		void openProject(const QUrl& projectFile);
		void saveProject(const QUrl& projectFile);

		void importModel(const QUrl& modelRulesFile);
		void loadModel(const QUrl& modelFile);
		void saveModel(const QUrl& modelFile);
		void exportModel(const QUrl& modelDirectory);
		void setCurrentModel(QString const& historyname);
		void reloadFEMesh();

		//    void setBodyDimDBFileName(QString filename){ setBodyDimDBFileName(filename.toStdString()); }

		void loadTarget(const QUrl& targetFile);
		void saveTarget(const QUrl& targetFile);
		void clearTarget();

		void exportLandmarks(const QUrl &filename);
		void exportEntityNodes(const QUrl &directory);

        void loadMetadata(const QUrl& metadataFile, bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool anatomicalJoint = true, bool controlpoint = true, bool HbmParameter = true);
		void saveMetadata(const QUrl& metadataFile, bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool controlpoint = true, bool HbmParameter = true);


		void log(QString const& message);
		void logSuccess(QString const& message);
		void logInfo(QString const& message);
        void logInfo(std::string const& message);
		void logDebug(QString const& message);
		void logStart(QString const& message);
		void logDone(QString const& message = "");
		void logWarning(QString const& message);
        void logWarning(std::string const& message);
		void logError(QString const& message);
		void logFailed(QString const& message);

		/// <summary>
		/// Tells us if the user have selected something with a picker in the scene
		/// </summary>
		/// <returns>Return true if something has been selected by the user </returns>
		bool isSomethingSelected();

		/// Start the Qt assitant process or promote the window if already started
		void showUserGuideHTML();

        /// Open the PDF documentation
        void showUserGuidePDF();

		void reloadDisplay();
		bool isTargetLoaded();
		void reloadLandmarkDisplay();
        bool doFolderExist(QUrl folderUrl);
        // checks if the absolute path starts with "\\", returns false if does not or if it does not exist
        bool isUNCPath(QUrl folderUrl);
    
	signals:
		void busyChanged(bool isBusy);
		void busyCancelVisibleChanged(bool busyCancelVisible);
		void busyTaskCancelRequestedChanged(bool busyTaskCancelRequested);
		/// This signal is emitted when the "Cancel" button is clicked by the user
		void busyTaskCancelButtonClicked();
		/// This signal must be emitted once the task has been actually cancelled
		void busyTaskCancelled();
		void projectFileChanged();

		/** This signal is emitted after operation that causes the internal structure of the model to change,
		* e.g. when a new model is opened, when parts of the model (metadata, nodes, elements) are added or removed etc.
		* Emitting this signal will, among others, cause a reload and reinitialization of all data related to visualization - this can be a lengthy action!
		* \sa Context::setOperationSignals
		*/
		void modelChanged();

		/** This signal is emitted when the nodes coordinates change, but no topological change occured.
		* \sa Context::modelChanged()
		* \sa Context::setOperationSignals
		*/
		void modelUpdated();
		/** This signal is emitted when the metadata change, with no impact on model (tipycally textual metadata)
		* \sa Context::modelChanged()
		* \sa Context::setOperationSignals
		*/
		void metadataChanged();
		void targetChanged();
		/** This signal is emitted when the environment objects change.
		* \sa Context::setOperationSignals
		*/
		void envChanged();

		/// <summary>
		/// Signal emitted when rotation of the model changes.
		/// </summary>
		/// <param name="name">The name of the environment model as specified by the user.</param>
		/// <param name="x">The current rotation along x-axis in degrees.</param>
		/// <param name="y">The current rotation along y-axis in degrees.</param>
		/// <param name="z">The current rotation along z-axis in degrees.</param>
		void envRotated(QString name, double x, double y, double z);

		/// <summary>
		/// Signal emitted when translation of the model changes.
		/// </summary>
		/// <param name="name">The name of the environment model as specified by the user.</param>
		/// <param name="x">The current absolute displacement along x-axis.</param>
		/// <param name="y">The current absolute displacement along y-axis.</param>
		/// <param name="z">The current absolute displacement along z-axis.</param>
        void envTranslated(QString name, double x, double y, double z);

		/// <summary>
		/// Signal emitted when scale of the model changes.
		/// </summary>
		/// <param name="name">The name of the environment model as specified by the user.</param>
		/// <param name="x">The current scale factor of x-axis (compared to the original model).</param>
		/// <param name="y">The current scale factor of y-axis (compared to the original model).</param>
		/// <param name="z">The current scale factor of z-axis (compared to the original model).</param>
        void envScaled(QString name, double x, double y, double z);

		/// <summary>
		/// Signal emitted when visibility of the model changes.
		/// </summary>
		/// <param name="name">The name of the environment model as specified by the user.</param>
		/// <param name="isVisible">If set to <c>true</c>, the user requests the model to be visible, otherwise it should be made invisible.</param>
		void envVisibleChanged(QString name, bool isVisible);

		/// <summary>
		/// Signal emitted when opacity of the environment models changes.
		/// </summary>
		/// <param name="currentOpacity">The current opacity requested by the user, 0 = fully transparent, 1 = fully opaque.</param>
		void envOpacityChanged(double currentOpacity);

		/** This signal is emitted when an operation is finished
		* \sa Context::setOperationSignals
		*/
		void operationFinished();
		void newLogMessage(QString message);
        // use to set whether log should replace all spaces by non-breaking spaces - turn on when you want to do ascii art, turn off afterwards
        void setLogNBSP(bool value);
		void importantMessageReceived();
		void memoryUsageChanged();
		void historyListChanged();

		// signals for visualization
		void visDataLoaded();

	private:

		/** Select the signals which are emitted when the operation finishes.
		* Also disconnects all other signals that were previously associated with the operation watcher.
		* the Context::operationFinished() signal is always emitted as the last signal
		* \sa Context::operationWatcher
		*/
		void setOperationSignals(bool modelChanged, bool modelUpdated, bool target, bool environment);

		//    FRIEND_TEST(common_test, read);
		friend void piper::logPanelMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
		// singleton design pattern
		Context();
		Context(Context const&) { }

		// does the actual loading of data - called using the operation watcher from loadDataIntoDisplay
		void doLoadDataIntoDisplay(bool loadFullMesh, bool loadEntities, bool loadLandmarks, bool loadGenericMetadatas);

		// VtkDisplay-related methods
		// converts the whole context HBM into a single mesh
		void createActorFullHBM();
		// converts the hbm in the context of the app into a set of entities, displays each in a different color
		void createEntityActorsFromHBM();
		// converts the landmarks of the hbm in the context of the app into a set of meshes
		void createLandmarkActorsFromHBM();
		// converts the hbm in the context of the app into a set of genericMetadatas, displays each in a different color
		void createGenericMetadataActorsFromHBM();
		// converts the environment model of the project into a single VTK mesh
		void createEnvironment();


		/// <summary>
		/// Checks which type of data was previously loaded with loadDataIntoDisplay and reloads them. Should be called after the data has changed.
		/// </summary>
		void reloadVisualization();


		//    Context& operator=(Context const&) { } //must return a value

		QFutureWatcher<void> m_operationWatcher; ///< object to monitor operations worker thread
		std::mutex m_opWatcherLock;

		bool m_isBusy; ///< Is the application busy ? to notify the GUI
		bool m_busyCancelVisible; ///< Can the busy task be cancelled
		bool m_busyTaskCancelRequested; ///< Has task cancelling been requested ? This is for internal use, with the python bindings

		// boolean: true if current operation are related to open/save project operations
		Project m_project;
		QUrl m_projectFile; ///< current project file
		unsigned int m_memoryUsage; ///< current memory usage (in MB)

		OctaveProcess m_octaveProcess;

		void setProjectFile(QUrl const& projectFile);

		VtkDisplay m_display; // for displaying context data
		MetaViewManager m_metaManager; // for managing what metadata are displayed in the m_display
		MetaEditor m_metaEditor; // for managing metaeditor tools
		bool entitiesLoadedInDisplay = false;
		bool fullModelLoadedInDisplay = false;
		bool landmarksLoadedInDisplay = false;
		bool genericMetadatasLoadedInDisplay = false;
		std::list<std::string> loadedModelActors; // list of names of actors related to the current HBM loaded in display

		unsigned int _p_defaultEnviroColor = 0xFFFFFFFF; // default color for visualization of environment models

		/// qt assistant process to browse the user guide
		QProcess m_qtAssistantProcess;


		private slots:
		void onOperationWatcherFinished();
		void updateMemoryUsage();

	};

} // namespace piper

#endif
