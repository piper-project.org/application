/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"

#include "common/helper.h"
#include "common/OctaveProcess.h"

#include <QFile>
#include <QTextStream>
#include <QDir>

using namespace piper;

TEST(OctaveProcess_test, canBeStarted) {
    OctaveProcess op;
    EXPECT_TRUE(op.canBeStarted());
}

TEST(OctaveProcess_test, helloWorld) {
    OctaveProcess op;
    EXPECT_THROW(op.setScriptPath("script_does_not_exists.m"), std::runtime_error);

    EXPECT_NO_THROW(op.setScriptPath("test/test_octave_script_01.m"));
    op.setScriptArguments(QStringList() << "dummy arg1" << "dummy arg2");
    EXPECT_NO_THROW(op.executeScript());

    std::cerr << op.readAllStandardOutput().toStdString() << std::endl;
    std::cerr << op.readAllStandardError().toStdString() << std::endl;
}

TEST(OctaveProcess_test, sum) {
    OctaveProcess op;
    QString outputFilePath = QString::fromStdString(piper::tempDirectoryPath("test_OctaveProcess", true)) + "/res.txt";
    EXPECT_NO_THROW(op.setScriptPath("test/test_octave_script_02.m"));
    op.setScriptArguments(QStringList() << outputFilePath << "2" << "3");
    EXPECT_NO_THROW(op.executeScript());

    std::cerr << op.readAllStandardOutput().toStdString() << std::endl;
    std::cerr << op.readAllStandardError().toStdString() << std::endl;

    QFile outputFile(outputFilePath);
    ASSERT_TRUE(outputFile.exists());
    ASSERT_TRUE(outputFile.open(QIODevice::ReadOnly | QIODevice::Text));
    int res;
    QTextStream(&outputFile) >> res;
    EXPECT_EQ(5, res);
}

TEST(OctaveProcess_test, xml) {
    OctaveProcess op;
    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_OctaveProcess", true));
    ASSERT_TRUE(QFile::copy("test_XMLOctave.xml", tmpDir + "/test_XMLOctave.xml"));
    QString outputFilePath = QString::fromStdString(piper::tempDirectoryPath("test_OctaveProcess", true)) + "/resXml.txt";

    EXPECT_NO_THROW(op.setScriptPath("test/test_octave_script_03.m"));

    QString jar1 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xercesImpl.jar");
    QString jar2 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xml-apis.jar");

    op.setScriptArguments(QStringList() << tmpDir + "/test_XMLOctave.xml" << outputFilePath << jar1 << jar2);

    EXPECT_NO_THROW(op.executeScript());

    std::cerr << op.readAllStandardOutput().toStdString() << std::endl;
    std::cerr << op.readAllStandardError().toStdString() << std::endl;

    QFile outputFile(outputFilePath);
    ASSERT_TRUE(outputFile.exists());
    ASSERT_TRUE(outputFile.open(QIODevice::ReadOnly | QIODevice::Text));
    double value1, value2;
    QString strvalue1, strvalue2, strvalue3, strvalue4;
    QTextStream(&outputFile) >> strvalue1 >> strvalue2 >> value1 >> strvalue3 >> strvalue4 >> value2;
    EXPECT_EQ("AnthropometricDimension", strvalue1);
    EXPECT_EQ("ANKLE_CIRCUMFERENCE", strvalue2);
    EXPECT_EQ(189.291, value1);
    EXPECT_EQ("AnthropometricDimension", strvalue3);
    EXPECT_EQ("ANKLE_HEIGHT_OUTSIDE", strvalue4);
    EXPECT_EQ(56.4257, value2);
}
