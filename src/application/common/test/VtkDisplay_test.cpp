/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include "common/logging.h"
#include "hbm/HumanBodyModel.h"

#include "common/VtkDisplay.h"
#include <vtkMath.h>
/*
#include <QCoreApplication>
#include <qquickitem.h>
#include <QQuickWindow>
#include <QApplication>
#include <QQuickView>*/

using namespace piper;

class VtkDisplay_test : public ::testing::Test {

protected:
    
    virtual void SetUp() {
        hbm.setSource("meshoptimizer_optim_quality_3.pmr");
    }

    void loadScene()
    {
        hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
        auto map = hbm.fem().getFEModelVTK()->getVTKEntityMesh();
        for (auto it = map->begin(); it != map->end(); it++)
        {
            display.AddVtkPointSet(it->second, it->first, true, DISPLAY_MODE::ENTITIES);
            display.SetObjectColor(it->first, vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), 0.8);
        }
    }

    void renderScene() 
    {

    }

    hbm::HumanBodyModel hbm;
    VtkDisplay display;
 /*   QQuickItem *parentRect;
    QVTKFrameBufferObjectItem *framebuffer;
    QQmlApplicationEngine engine;*/
};

TEST_F(VtkDisplay_test, loadingData) {
    loadScene();
    auto map = hbm.fem().getFEModelVTK()->getVTKEntityMesh();
    EXPECT_TRUE(display.HasActor("Entity_1"));
    EXPECT_TRUE(display.HasActor("LSHELL5"));
    EXPECT_TRUE(display.HasActor("Entity_3"));
    EXPECT_TRUE(display.HasActor("Skin"));


  /* 
  I will maybe make this work one day....TJ
  char **argv;
    int argc = 0;
    QApplication app(argc, argv);

    qmlRegisterType<QVTKFrameBufferObjectItem>("VtkQuick", 1, 0, "VtkRenderWindow");
    qmlRegisterType<piper::VtkDisplay>("piper.VtkDisplay", 1, 0, "VtkDisplay");
    qRegisterMetaType<piper::VtkDisplay*>("VtkDisplay*");
    QQuickView view;
    display.qmlEngine = (QQmlApplicationEngine *)view.engine();
    display.RegisterToQmlContextAs("test_display");
    display.qmlEngine->load(QUrl::fromLocalFile("D:\\PIPER\\piper\\src\\application\\common\\test\\VtkDisplay_test_window.qml"));
  //  QQmlComponent component(display.qmlEngine);
    //component.setData("import QtQuick 2.3 \n import QtQml 2.2 \n ApplicationWindow { \n id: window \n visible : true \n Rectangle{ id: rect \n width : 800 \n height : 800 \n VtkRenderWindow{ \n id : framebuffer \n anchors.fill : parent \n Component.onCompleted : { \n test_display.frameBuffer = framebuffer } } } }", QUrl());
    //QQuickItem *item = qobject_cast<QQuickItem *>(component.create());
    //view.setContent(QString("url"), &component, item);
   // view.setSource(QUrl::fromLocalFile("D:\\PIPER\\piper\\src\\application\\common\\test\\VtkDisplay_test_window.qml"));
    view.show();

    //const QString framebufferName(std::string("framebuffer").c_str());
    //display.setFrameBuffer(engine.findChild<QVTKFrameBufferObjectItem *>(framebufferName));
   // app.exec();
    display.HighlightPoints("Entity_1");
    EXPECT_TRUE(display.HasActor("Entity_1_pointHighlights_"));*/
}
