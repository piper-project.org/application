/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <Python.h>

#include <QCoreApplication>
#include <QDir>
#include <QTextDocument>

#include <gtest/gtest.h>

#include <common/logging.h>
#include <common/helper.h>

void gtestMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static QTextDocument document;
    switch (type) {
    case QtCriticalMsg:
    case QtFatalMsg:
        document.setHtml(msg);
        ADD_FAILURE_AT(context.file, context.line) << document.toPlainText().toStdString();
    }
}

void cleanup()
{
    pDebug() << "Cleaning up PIPER test_common...";

    // Python
    Py_Finalize();

    // remove tmp directory
    QDir(QString::fromStdString(piper::tempDirectoryPath())).removeRecursively();
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    piper::LoggingParameter::instance().addMessageHandler(piper::consoleMessageHandler);
//    piper::LoggingParameter::instance().addMessageHandler(gtestMessageHandler);

    // create this piper instance temporary path
    piper::tempDirectoryPath("", true);

    // python initialization
    Py_SetProgramName(argv[0]);
    Py_Initialize();
    // add piper package path
    QString pythonInit =
            "import sys\n"
            "sys.path.append('" + QCoreApplication::applicationDirPath() + "/../lib/python2.7/site-packages')\n"
            "import locale\n"
            "locale.setlocale(locale.LC_ALL, 'C')\n";
    PyRun_SimpleString(pythonInit.toUtf8());

    ::testing::InitGoogleTest(&argc, argv);

    std::atexit(cleanup);

    return RUN_ALL_TESTS();
}
