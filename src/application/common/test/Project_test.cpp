/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include "common/Project.h"
#include "common/logging.h"
#include "common/helper.h"

#include <boost/filesystem.hpp>

using namespace piper;

class Project_test : public ::testing::Test {

protected:

    virtual void SetUp() {
        project.read("model_01.ppj");
    }

    void checkProject() {
        EXPECT_TRUE(project.model().metadata().hasEntity("Entity_1"));
        EXPECT_EQ(36, project.model().fem().getNbNode());
        EXPECT_EQ(1, project.environment().getNumberenvironment());
        EXPECT_TRUE(project.environment().isExistingName("Env_test"));
        EXPECT_EQ(0, project.target().anthropometricDimension.size());
        EXPECT_EQ(1, project.target().frameToFrame.size());
        EXPECT_EQ(2, project.anthropoModel().listAnthropoBodySegment().size());
        EXPECT_EQ(2, project.anthropoModel().listAnthropoBodySection().size());
        EXPECT_EQ(2, project.anthropoModel().listAnthropoBodyDimension().size());
    }

    Project project;

};

TEST_F(Project_test, read) {
   checkProject();
}

TEST_F(Project_test, write) {
    // write current model to tmp
    boost::filesystem::path tmp = piper::tempDirectoryPath();
    tmp /= "piper_common_test_project.ppj";
    project.write(tmp.string());

    // read it
    project.clear();
    project.read(tmp.string());
    checkProject();
}

