/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QFileInfo>
#include <QFile>

#include "gtest/gtest.h"

#include "common/helper.h"
#include "common/OctaveProcess.h"
#include "common/AnthropoOctave.h"
#include "application/module/anthropometry/AnthropometryModule.h"
#include <common/Context.h>

using namespace piper;

TEST(AnthropoOctave_test, compute) {
    OctaveProcess op;
    AnthropoOctave ao;

    //Stream declaration to write in a csv file
    ofstream inputFile_ofStream;
    QFileInfo inputFile_QFile(QCoreApplication::applicationDirPath() + "/../share/test/anthropoPersoUserInputF_check_fixInp_Meon.csv");

    if (inputFile_QFile.exists())
    {
        inputFile_ofStream.open(QCoreApplication::applicationDirPath().toStdString() + "/../share/test/anthropoPersoUserInputF_check_fixInp_Meon.csv");
        AnthropometryModule am;
        for (int j = 0; j < 4; j++)
        {
            if (j == 0)
                inputFile_ofStream <<  "Parameter/codeDir/" + piper::octaveScriptDirectoryPath().toStdString() + "/anthropoPerso/script" + '\n';
            if (j == 1)
                inputFile_ofStream << "Parameter/DatabaseDir/" + piper::octaveScriptDirectoryPath().toStdString() + "/anthropoPerso/dataBase\n";
            if (j == 2)
                inputFile_ofStream << "Parameter/RegressionDir/" + piper::octaveScriptDirectoryPath().toStdString() + "/anthropoPerso/regression" + '\n';
            if (j == 3)
                inputFile_ofStream << "Parameter/SampleDir/" + piper::octaveScriptDirectoryPath().toStdString() + "/anthropoPerso/sample"+ '\n';
        }
        inputFile_ofStream.close();
        OctaveProcess & op = Context::instance().octaveProcess();
        op.setMatlabCompatibleMode(false);
        EXPECT_NO_THROW(op.setScriptPath("/anthropoPerso/script/ToolsWP3/launchPersoAnthropoFromWP3.m"));
        op.setScriptArguments(QStringList() << QString(inputFile_QFile.absoluteFilePath()) << piper::octaveScriptDirectoryPath() + "/anthropoPerso/script");
        EXPECT_NO_THROW(op.executeScript());

        EXPECT_TRUE(QFileInfo::exists(piper::octaveScriptDirectoryPath() + "/anthropoPerso/script/regression/regress.mat"));
        EXPECT_TRUE(QFileInfo::exists(piper::octaveScriptDirectoryPath() + "/anthropoPerso/script/sample/meon.ptt"));

    }
}
            

    
