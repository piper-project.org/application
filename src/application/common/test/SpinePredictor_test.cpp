/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QFileInfo>
#include <QFile>

#include "gtest/gtest.h"

#include "common/helper.h"
#include "common/OctaveProcess.h"
#include "common/SpinePredictor.h"

using namespace piper;

TEST(SpinePredictor_test, compute) {
    OctaveProcess op;
    SpinePredictor predictor;

    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_SpinePredictor", true));
    ASSERT_TRUE( QFile::copy("CS.dat", tmpDir+"/input_CS.dat") );
    ASSERT_TRUE( QFile::copy("dummy_gravity.dat", tmpDir+"/gravity.dat") );

    ASSERT_NO_THROW( predictor.compute(&op,
                                       tmpDir+"/input_CS.dat",
                                       tmpDir+"/gravity.dat",
                                       3, // seated erect
                                       5, // forward flexed
                                       0.5,
                                       15.,
                                       -6.,
                                       tmpDir+"/",
                                       false) );
    EXPECT_TRUE( QFileInfo::exists(tmpDir+"/CS.dat") );
    EXPECT_TRUE( QFileInfo::exists(tmpDir+"/splinePoints.dat") );
}
