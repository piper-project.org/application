/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include <boost/filesystem.hpp>

#include "common/ModuleParameter.h"
#include "common/helper.h"

using namespace piper;

TEST(ModuleParameter_test, init) {
    ModuleParameter parameter;
    EXPECT_EQ(0, parameter.getUInt("PhysPosiInter","affineDensity"));
    EXPECT_EQ(500, parameter.getUInt("PhysPosiDefo","affineDensity"));
    EXPECT_DOUBLE_EQ(5e-3, parameter.getDouble("PhysPosi", "voxelSize"));
    EXPECT_EQ(3, parameter.getUInt("PhysPosiInter","voxelCoarsening"));
    EXPECT_FALSE(parameter.getBool("PhysPosiDefo", "boneCollisionEnabled"));
}

TEST(ModuleParameter_test, parseXml) {
    ModuleParameter parameter;
    tinyxml2::XMLDocument xmlProject;
    xmlProject.LoadFile("model_01.ppj");
    ASSERT_FALSE(xmlProject.Error());
    parameter.parseXml(xmlProject.RootElement());
    EXPECT_EQ(10,parameter.getUInt("PhysPosiInter","affineDensity"));
    EXPECT_DOUBLE_EQ(1e-3, parameter.getDouble("PhysPosi", "voxelSize"));
}

TEST(ModuleParameter_test, serializeXml) {
    // temporary file
    boost::filesystem::path tmp = piper::tempDirectoryPath();
    tmp /= "piper_common_test_ModuleParameter.xml";
    {
        // save an xml file with parameters
        ModuleParameter parameter;
        parameter.set("PhysPosiDefo", "boneCollisionEnabled", true);
        parameter.set("PhysPosiInter", "affineDensity", 560.);

        tinyxml2::XMLDocument xmlDocument;
        tinyxml2::XMLElement* xmlProject = xmlDocument.NewElement("piper-project");
        xmlDocument.InsertEndChild(xmlProject);
        parameter.serializeXml(xmlProject);

        xmlDocument.SaveFile(tmp.string().c_str());
        ASSERT_FALSE(xmlDocument.Error());
    }
    {
        // read the xml file with parameters
        ModuleParameter parameter;
        tinyxml2::XMLDocument xmlDocument;
        xmlDocument.LoadFile(tmp.string().c_str());
        ASSERT_FALSE(xmlDocument.Error());

        parameter.parseXml(xmlDocument.RootElement());
        EXPECT_TRUE(parameter.getBool("PhysPosiDefo", "boneCollisionEnabled"));
        EXPECT_DOUBLE_EQ(560, parameter.getDouble("PhysPosiInter", "affineDensity"));

    }


}
