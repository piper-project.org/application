/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"

#include "common/PythonScript.h"

using namespace piper;

TEST(PythonScript_test, run_basic) {
    PythonScript script;
    script.run("PythonScript_01.py");
    EXPECT_TRUE(script.success());
}

TEST(PythonScript_test, run_hbm) {
    PythonScript script;
    script.run("PythonScript_02.py");
    EXPECT_TRUE(script.success());
}

TEST(PythonScript_test, run_scriptFileNotFound) {
    PythonScript script;
    script.run("file_not_found.py");
    EXPECT_FALSE(script.success());
}

TEST(PythonScript_test, run_scriptInvalidSyntax) {
    PythonScript script;
    script.run("PythonScript_03.py");
    EXPECT_FALSE(script.success());
}

TEST(PythonScript_test, run_scriptArguments) {
    PythonScript script;
    script.run("PythonScript_04.py", QStringList() << "1" << "2");
    EXPECT_TRUE(script.success());
}

TEST(PythonScript_test, description) {
    PythonScript script;
    script.setPath("PythonScript_01.py");
    EXPECT_EQ(" Trivial python test\n", script.description());
}
