/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QFileInfo>
#include <QFile>
#include <QDir>

#include "gtest/gtest.h"

#include "common/helper.h"
#include "common/OctaveProcess.h"
#include "common/KrigingTargetGenerator.h"

using namespace piper;

TEST(KrigingTarget_test, compute) {
    OctaveProcess op;
    EXPECT_TRUE(op.canBeStarted());

    KrigingTargetGenerator targetcreator;

    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_KrigingTarget", true));
    ASSERT_TRUE(QFile::copy("KrigingTarget_ControlPointData.txt", tmpDir + "/KrigingTarget_ControlPointData.txt"));
    //ASSERT_TRUE(QFile::copy("KrigingTarget_TestScript.m", tmpDir + "/KrigingTarget_TestScript.m"));


    QString nameSet = "KrigingTarget_ControlPointData";

    ASSERT_NO_THROW(targetcreator.compute(&op,
        "kriging/KrigingTarget_ExampleScript.m",
        nameSet,
        tmpDir + "/KrigingTarget_ControlPointData.txt",
        tmpDir + "/KrigingTarget_ControlPointData_output.txt"));
    EXPECT_TRUE(QFileInfo::exists(tmpDir + "/KrigingTarget_ControlPointData_output.txt"));
}
