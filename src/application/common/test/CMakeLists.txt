project(test_common)

set(CMAKE_AUTOMOC ON)

set(SOURCE_FILES
    Project_test.cpp
    ModuleParameter_test.cpp
    VtkDisplay_test.cpp
    OctaveProcess_test.cpp
    SpinePredictor_test.cpp
    PythonScript_test.cpp
#	AnthropoOctave_test.cpp
    KrigingTarget_test.cpp
    main.cpp
    AnthropometryPredictor_test.cpp
)


add_executable(${PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} Qt5::Core pipercommon gtest vtkPIPERFilters)

if(MSVC_IDE)
    add_test(NAME ${PROJECT_NAME} WORKING_DIRECTORY ${PIPER_DATA_PATH} COMMAND ${PROJECT_NAME})
else()
    add_test(NAME ${PROJECT_NAME} WORKING_DIRECTORY ${PIPER_DATA_PATH} COMMAND ${PROJECT_NAME} --gtest_output=${GTEST_OUTPUT})
    if(WIN32)
        set_tests_properties(${PROJECT_NAME} PROPERTIES ENVIRONMENT "PATH=${PIPER_QT_INSTALL_PREFIX}/bin\;${VTK_INSTALL_PREFIX}/bin\;${Boost_LIBRARY_DIRS}\;$ENV{PATH}")
    endif()
endif()

#setting MSVC project properties for Environment variable (to ease setting project debugging environment)
IF(WIN32)    
    set (PATH_WINDOWS $ENV{PATH})
    STRING(REPLACE ";" "\\;" PATH_WINDOWS "${PATH_WINDOWS}")
    set (PATH_WINDOWS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CONFIGURATION}/\;${PIPER_PACKAGE_DIR}/Python27/\;${PIPER_PACKAGE_DIR}/Python27/lib/\;${SOFA_ROOT}/bin/\;${XMLLINT_DIR}/\;${Mesquite_BINARY_DIR}/\;${Boost_LIBRARY_DIRS}\;${VTK_INSTALL_PREFIX}/bin\;${_qt5Core_install_prefix}/bin\;${OCTAVE_ROOT_DIR}/bin/;${PATH_WINDOWS}") #escape semicolon in the string, otherwise only the first path from the semicolon separated list would be taken into account
    set_tests_properties(${PROJECT_NAME} PROPERTIES ENVIRONMENT  "PATH=${PATH_WINDOWS};SOFA_ROOT=${SOFA_ROOT};PYTHONPATH=${PIPER_PACKAGE_DIR}/Python27/;PYTHONHOME=${PIPER_PACKAGE_DIR}/Python27/;PYTHONUSERBASE=${PIPER_PACKAGE_DIR}/Python27/")
    IF(MSVC_IDE)
        SET(USERFILE_ENVIRONMENT "PATH=${PATH_WINDOWS}/;
            QML_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
            QML2_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
            QT_QPA_PLATFORM_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/platforms
            QT_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/
            SOFA_ROOT=${SOFA_ROOT}
            PYTHONPATH=${PIPER_PACKAGE_DIR}/Python27/
            PYTHONHOME=${PIPER_PACKAGE_DIR}/Python27/
            PYTHONUSERBASE=${PIPER_PACKAGE_DIR}/Python27/")
        
        SET(USERFILE_WORKINGDIR "${PIPER_DATA_PATH}")

        # Configure the template file
        SET(USER_FILE ${PROJECT_NAME}.vcxproj.user)
        SET(OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/${USER_FILE})
        CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/piper.vcxproj.user.in ${USER_FILE} @ONLY)
    ENDIF(MSVC_IDE)
    
ENDIF(WIN32)


