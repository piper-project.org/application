/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <QFileInfo>
#include <QFile>
#include <QCoreApplication>

#include "gtest/gtest.h"

#include "common/helper.h"
#include "common/OctaveProcess.h"
#include "common/AnthropometryPredictor.h"

#include <fstream>
#include <sstream>

using namespace piper;

TEST(AnthropometryPredictor_test, computeSNYDER) {
    OctaveProcess op;
    AnthropometryPredictor predictor;
    
    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_AnthropometryPredictor", true));

    QString folderScript = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/script")).absoluteFilePath();
    QString folderDatabase = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/dataBase")).absoluteFilePath();
    QString folderRegression(tmpDir);
    QString folderSampleDir(tmpDir);

    ASSERT_TRUE(QFile::copy("anthropoPersoUserInput_SNYDER.csv", tmpDir + "/input_anthropoPersoUserInput_SNYDER.csv"));
    ASSERT_TRUE(QFile::copy("target_anthropo_SNYDER_check_MeanOnly.ptt", tmpDir + "/target_anthropo_SNYDER_check_MeanOnly.ptt"));
    
    //adapt path info incsvfile
    std::ifstream csvfile;
    csvfile.open("anthropoPersoUserInput_SNYDER.csv", std::ios::in);
    std::ofstream csvfile_test;
    csvfile_test.open(tmpDir.toStdString() + "/input_anthropoPersoUserInput_SNYDER.csv", std::ios::trunc);
    std::string line;
    while (std::getline(csvfile, line)) {
        // target file for estimation of dimension for hbm weight and stature
        std::istringstream csvStream(line);
        std::vector<std::string> csvColumn;
        std::string information;
        std::getline(csvStream, information, ',');
        std::getline(csvStream, information, ',');
        if (information == "CodeDir") {
            line = "Parameter,CodeDir," + folderScript.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "DatabaseDir") {
            line = "Parameter,DatabaseDir," + folderDatabase.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "RegressionDir") {
            line = "Parameter,RegressionDir," + folderRegression.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "SampleDir") {
            line = "Parameter,SampleDir," + folderSampleDir.toStdString();
            csvfile_test << line << "\n";
        }
        else
            csvfile_test << line << "\n";
    }
    csvfile.close();
    csvfile_test.close();


    ASSERT_NO_THROW(predictor.computePrediction(&op,
        tmpDir + "/input_anthropoPersoUserInput_SNYDER.csv"));

    EXPECT_TRUE(QFileInfo::exists(tmpDir + "/target_SNYDER.ptt"));
}

TEST(AnthropometryPredictor_test, computeANSUR) {
    OctaveProcess op;
    AnthropometryPredictor predictor;

    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_AnthropometryPredictor", true));

    QString folderScript = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/script")).absoluteFilePath();
    QString folderDatabase = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/dataBase")).absoluteFilePath();
    QString folderRegression(tmpDir);
    QString folderSampleDir(tmpDir);

    ASSERT_TRUE(QFile::copy("anthropoPersoUserInput_ANSUR.csv", tmpDir + "/input_anthropoPersoUserInput_ANSUR.csv"));
    ASSERT_TRUE(QFile::copy("target_anthropo_ANSUR_check_MeanOnly.ptt", tmpDir + "/target_anthropo_ANSUR_check_MeanOnly.ptt"));

    //adapt path info incsvfile
    std::ifstream csvfile;
    csvfile.open("anthropoPersoUserInput_ANSUR.csv", std::ios::in);
    std::ofstream csvfile_test;
    csvfile_test.open(tmpDir.toStdString() + "/input_anthropoPersoUserInput_ANSUR.csv", std::ios::trunc);
    std::string line;
    while (std::getline(csvfile, line)) {
        // target file for estimation of dimension for hbm weight and stature
        std::istringstream csvStream(line);
        std::vector<std::string> csvColumn;
        std::string information;
        std::getline(csvStream, information, ',');
        std::getline(csvStream, information, ',');
        if (information == "CodeDir") {
            line = "Parameter,CodeDir," + folderScript.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "DatabaseDir") {
            line = "Parameter,DatabaseDir," + folderDatabase.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "RegressionDir") {
            line = "Parameter,RegressionDir," + folderRegression.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "SampleDir") {
            line = "Parameter,SampleDir," + folderSampleDir.toStdString();
            csvfile_test << line << "\n";
        }
        else
            csvfile_test << line << "\n";
    }
    csvfile.close();
    csvfile_test.close();


    ASSERT_NO_THROW(predictor.computePrediction(&op,
        tmpDir + "/input_anthropoPersoUserInput_ANSUR.csv"));

    EXPECT_TRUE(QFileInfo::exists(tmpDir + "/target_ANSUR.ptt"));
}



TEST(AnthropometryPredictor_test, computeCCTAnthropo) {
    OctaveProcess op;
    AnthropometryPredictor predictor;

    QString tmpDir = QString::fromStdString(piper::tempDirectoryPath("test_AnthropometryPredictor", true));

    QString folderScript = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/script")).absoluteFilePath();
    QString folderDatabase = QFileInfo((QCoreApplication::applicationDirPath() + "/../share/octave/anthropoPerso/dataBase")).absoluteFilePath();
    QString folderRegression(tmpDir);
    QString folderSampleDir(tmpDir);

    ASSERT_TRUE(QFile::copy("anthropoPersoUserInput_CCTAnthropo.csv", tmpDir + "/input_anthropoPersoUserInput_CCTAnthropo.csv"));
    ASSERT_TRUE(QFile::copy("target_anthropo_CCTAnthropo_check_MeanOnly.ptt", tmpDir + "/target_anthropo_CCTAnthropo_check_MeanOnly.ptt"));

    //adapt path info incsvfile
    std::ifstream csvfile;
    csvfile.open("anthropoPersoUserInput_CCTAnthropo.csv", std::ios::in);
    std::ofstream csvfile_test;
    csvfile_test.open(tmpDir.toStdString() + "/input_anthropoPersoUserInput_CCTAnthropo.csv", std::ios::trunc);
    std::string line;
    while (std::getline(csvfile, line)) {
        // target file for estimation of dimension for hbm weight and stature
        std::istringstream csvStream(line);
        std::vector<std::string> csvColumn;
        std::string information;
        std::getline(csvStream, information, ',');
        std::getline(csvStream, information, ',');
        if (information == "CodeDir") {
            line = "Parameter,CodeDir," + folderScript.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "DatabaseDir") {
            line = "Parameter,DatabaseDir," + folderDatabase.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "RegressionDir") {
            line = "Parameter,RegressionDir," + folderRegression.toStdString();
            csvfile_test << line << "\n";
        }
        else if (information == "SampleDir") {
            line = "Parameter,SampleDir," + folderSampleDir.toStdString();
            csvfile_test << line << "\n";
        }
        else
            csvfile_test << line << "\n";
    }
    csvfile.close();
    csvfile_test.close();


    ASSERT_NO_THROW(predictor.computePrediction(&op,
        tmpDir + "/input_anthropoPersoUserInput_CCTAnthropo.csv"));

    EXPECT_TRUE(QFileInfo::exists(tmpDir + "/target_CCTAnthropo.ptt"));
}
