/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ModuleParameter.h"

#include <stdexcept>
#include <cassert>
#include <cmath>

namespace piper {

ModuleParameter::ModuleParameter()
{
    setDefault();
}

void ModuleParameter::setDefault()
{
    // Physics Positioning common parameter
    init("PhysPosi", "autoStopNbIterationMax", static_cast<unsigned int>(50));
    init("PhysPosi", "autoStopVelocity", static_cast<double>(1e-6));
    init("PhysPosi", "voxelSize", static_cast<double>(5e-3));
    init("PhysPosi", "scaleVoxelSize", false);
    init("PhysPosi", "scaleVoxelSizeRefHeight", static_cast<double>(1.75));
    init("PhysPosi", "boneCollisionOffset", static_cast<double>(0.));
    init("PhysPosi", "boneCollisionNbVertex", static_cast<unsigned int>(100));
    init("PhysPosi", "nbAffinePerCapsule", static_cast<unsigned int>(4));
    init("PhysPosi","tibiaPatellaConstraintEnabled", false);
    init("PhysPosi", "softJointStiffness", static_cast<double>(1e4));

    // Physics based interactive positioning module
    init("PhysPosiInter", "boneCollisionEnabled", true);
    init("PhysPosiInter", "capsuleEnabled", false);
    init("PhysPosiInter", "affineDensity", static_cast<unsigned int>(0));
    init("PhysPosiInter", "customAffine", false);

    init("PhysPosiInter", "voxelCoarsening", static_cast<unsigned int>(3));
    init("PhysPosiInter", "modelUpdateEnabled", true);
    init("PhysPosiInter", "targetDefaultStiffness", static_cast<double>(1e8));
    init("PhysPosiInter", "spineTargetDefaultStiffness", static_cast<double>(1e3));

    // Physics based positioning module deformation
    init("PhysPosiDefo", "boneCollisionEnabled", false);
    init("PhysPosiDefo", "capsuleEnabled", true);
    init("PhysPosiDefo", "affineDensity", static_cast<unsigned int>(500));
    init("PhysPosiDefo", "customAffine", true);
    init("PhysPosiDefo", "targetDefaultStiffness", static_cast<double>(1e4));

    // Kriging
    init(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT, static_cast<double>(0.));
    init(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP, static_cast<unsigned int>(8000));
    init(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_OVERLAP, static_cast<double>(0));
    init(PARAM_MODULE_KRIG, PARAM_KRIG_INTERP_DISPLACEMENT, true);
    init(PARAM_MODULE_KRIG, PARAM_KRIG_USE_DRIFT, false);
    init(PARAM_MODULE_KRIG, PARAM_KRIG_GEODESIC_PRECISION, static_cast<double>(1000000));
    init(PARAM_MODULE_KRIG, PARAM_KRIG_GEODESIC_TYPE, static_cast<unsigned int>(0));

    // Physics scaling
    init("PhysScale", "nbPointHandles", static_cast<unsigned int>(500));
    init("PhysScale", "targetDefaultStiffness", static_cast<double>(1e8));
    init("PhysScale", "skinYoungModulus", static_cast<double>(1e5));
}

bool ModuleParameter::getBool(std::string const& moduleName, std::string const& parameterName)
{
    QVariant value = m_parameter.value(checkKey(moduleName, parameterName));
    if (!value.canConvert(QMetaType::Bool))
        throw std::runtime_error("ModuleParameter: cannot get parameter as boolean module: "+moduleName+" parameter: "+parameterName);
    return value.toBool();
}

unsigned int ModuleParameter::getUInt(std::string const& moduleName, std::string const& parameterName)
{
    QVariant value = m_parameter.value(checkKey(moduleName, parameterName));
    if (!value.canConvert(QMetaType::UInt))
        throw std::runtime_error("ModuleParameter: cannot get parameter as unsigned int module: "+moduleName+" parameter: "+parameterName);
    return value.toUInt();
}

std::list<unsigned int> ModuleParameter::getListUInt(std::string const& moduleName, std::string const& parameterName)
{
    QVariant valueList = m_parameter.value(checkKey(moduleName, parameterName));
    if (!valueList.canConvert(QMetaType::QVariantList))
        throw std::runtime_error("ModuleParameter: cannot get parameter as std::list<unsigned int> module: "+moduleName+" parameter: "+parameterName);
    std::list<unsigned int> list;
    for (QVariant value : valueList.toList()) {
        if (!value.canConvert(QMetaType::UInt))
            throw std::runtime_error("ModuleParameter: cannot get parameter as std::list<unsigned int> module: "+moduleName+" parameter: "+parameterName);
        list.push_back(value.toUInt());
    }
    return list;
}


double ModuleParameter::getDouble(std::string const& moduleName, std::string const& parameterName)
{
    QString key = checkKey(moduleName, parameterName);
    QVariant value = m_parameter.value(key);
    if (!value.canConvert(QMetaType::Double))
        throw std::runtime_error("ModuleParameter: cannot get parameter as double - module: "+moduleName+" parameter: "+parameterName);
    double valueConv = value.toDouble();
    if (std::isnan(valueConv))
        throw std::runtime_error("ModuleParameter: cannot get parameter as double (NaN) - module: "+moduleName+" parameter: "+parameterName);
    return valueConv;
}

void ModuleParameter::parseXml(const tinyxml2::XMLElement* parentElement)
{
    if (parentElement->FirstChildElement("moduleParameter")==nullptr)
        return;
    const tinyxml2::XMLElement* xmlValue = parentElement->FirstChildElement("moduleParameter")->FirstChildElement("parameterValue");
    while(xmlValue!=nullptr) {
        std::string module = xmlValue->Attribute("module");
        std::string parameter = xmlValue->Attribute("parameter");

        QString k = key(module, parameter);
        if (!m_parameter.contains(k)) {
            // silently ignore the parameter if it does not exist, it is safe, the default value is used
            // but avoid creating ghost parameter
            xmlValue = xmlValue->NextSiblingElement("parameterValue");
            continue;
        }

        if (m_parameter[k].type() == QVariant::List) {
            QString str(QString::fromUtf8(xmlValue->GetText()));
            QVariantList valueList;
            str = str.simplified();
            if (str.size() > 0) {
                for(QString s: str.split(' '))
                    valueList << QVariant(s);
                set(module, parameter, valueList);
            }
        }
        else {
            QVariant value(QString::fromUtf8(xmlValue->GetText()));
            set(module, parameter, value);
        }

        xmlValue = xmlValue->NextSiblingElement("parameterValue");
    }
}

void ModuleParameter::serializeXml(tinyxml2::XMLElement* parentElement) const
{
    tinyxml2::XMLElement* xmlParameter= parentElement->GetDocument()->NewElement("moduleParameter");
    parentElement->InsertEndChild(xmlParameter);
    for (QString key : m_parameter.keys()) {
        tinyxml2::XMLElement* xmlValue = xmlParameter->GetDocument()->NewElement("parameterValue");
        QStringList moduleValue = key.split('_');
        xmlValue->SetAttribute("module", moduleValue.at(0).toLatin1().data());
        xmlValue->SetAttribute("parameter", moduleValue.at(1).toLatin1().data());
        QVariant val = m_parameter.value(key);
        if (val.canConvert(QMetaType::QString))
            xmlValue->SetText(val.toString().toUtf8().data());
        else if (val.canConvert(QMetaType::QStringList)) {
            xmlValue->SetText(val.toStringList().join(' ').toUtf8().data());
        }
        else {
            // should not get here
            assert(false && "ModuleParameter::serializeXml: invalid parameter value");
        }

        xmlParameter->InsertEndChild(xmlValue);
    }
}

QString ModuleParameter::key(std::string const& moduleName, std::string const& parameterName)
{
    return QString::fromStdString(moduleName)+"_"+QString::fromStdString(parameterName);
}

QString ModuleParameter::checkKey(std::string const& moduleName, std::string const& parameterName)
{
    QString k = key(moduleName, parameterName);
    if (!m_parameter.contains(k))
        throw std::runtime_error("ModuleParameter: unknown parameter module: "+moduleName+" parameter: "+parameterName);
    return k;
}

void ModuleParameter::init(std::string const& moduleName, std::string const& parameterName, QVariant value)
{
    m_parameter.insert(key(moduleName, parameterName), value);
}

}
