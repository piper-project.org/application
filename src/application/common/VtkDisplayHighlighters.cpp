/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "VtkDisplayHighlighters.h"

#include "Context.h"

#include "hbm/VtkSelectionTools.h"

#include <vtkLookupTable.h>

#include <vtkGlyph3DMapper.h>
#include <vtkPolyDataMapper.h>

#include <vtkPlaneSource.h>
#include <vtkCubeSource.h>
#include <vtkSphereSource.h>
#include <vtkProperty.h>
#include <vtkMath.h>
#include <vtkPolyLine.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkArrowSource.h>

#include <vtkObjectFactory.h>


namespace piper {

    vtkStandardNewMacro(VtkDisplayActor)

    VtkDisplayHighlighters::VtkDisplayHighlighters()
    {
    }

    VtkDisplayHighlighters::~VtkDisplayHighlighters()
    {
        selectionBoxes.clear();
    }

    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::plane(piper::hbm::Node nodeorigin, piper::hbm::Node nodefirstdir, piper::hbm::Node nodeseconddir)
	{
		// Create a plane

		vtkSmartPointer<vtkPlaneSource> planeSource = vtkSmartPointer<vtkPlaneSource>::New();
		planeSource->SetCenter(nodeorigin.getCoordX(), nodeorigin.getCoordY(), nodeorigin.getCoordZ());
		planeSource->SetPoint1(nodefirstdir.getCoordX(), nodefirstdir.getCoordY(), nodefirstdir.getCoordZ());
		planeSource->SetPoint2(nodeseconddir.getCoordX(), nodeseconddir.getCoordY(), nodeseconddir.getCoordZ());

		planeSource->SetXResolution(1000);
		planeSource->SetYResolution(1000);

		planeSource->Update();

		vtkPolyData* plane = planeSource->GetOutput();

		// Create a mapper and actor
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();

		mapper->SetInputData(plane);

        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		actor->SetMapper(mapper);
        actor->GetProperty()->SetOpacity(0.5); 
        actor->SetOriginalDataSet(plane);

        return actor;
	}

    vtkSmartPointer<vtkAxesActor> VtkDisplayHighlighters::frame(const double transformationMatrix[16], double const& size) {
        vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
        transform->SetMatrix(transformationMatrix);
        vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();

        // The axes are positioned with a user transform
        axes->SetUserTransform(transform);

        // axis label		
        axes->SetXAxisLabelText("");
        axes->SetYAxisLabelText("");
        axes->SetZAxisLabelText("");

        double v[3] = { size, size, size };
        axes->SetTotalLength(v);
        
        return axes;
    }

    vtkSmartPointer<vtkAxesActor> VtkDisplayHighlighters::frame(piper::hbm::Node nodeorigin, piper::hbm::Node nodefirstdir, piper::hbm::Node nodeseconddir){
		double v1[3] = { (nodefirstdir.getCoordX() - nodeorigin.getCoordX()), (nodefirstdir.getCoordY() - nodeorigin.getCoordY()), (nodefirstdir.getCoordZ() - nodeorigin.getCoordZ()) };
		double v2[3] = { 1,0,0 };
		double dot = vtkMath::Dot(v1, v2);
		double lenSq1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
		double lenSq2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
		double angleX = vtkMath::DegreesFromRadians(acos(dot / sqrt(lenSq1 * lenSq2)));

		double vy1[3] = { (nodeseconddir.getCoordX() - nodeorigin.getCoordX()), (nodeseconddir.getCoordY() - nodeorigin.getCoordY()), (nodeseconddir.getCoordZ() - nodeorigin.getCoordZ()) };
		double vy2[3] = { 0, 1, 0 };
		double doty = vtkMath::Dot(vy1, vy2);
		double ylenSq1 = vy1[0] * vy1[0] + vy1[1] * vy1[1] + vy1[2] * vy1[2];
		double ylenSq2 = vy2[0] * vy2[0] + vy2[1] * vy2[1] + vy2[2] * vy2[2];
		double angleY = vtkMath::DegreesFromRadians(acos(doty / sqrt(ylenSq1 * ylenSq2)));

		double thirddir[3];
		vtkMath::Cross(vy1, vy2, thirddir);

		double vz1[3] = { (thirddir[0] - 0), (thirddir[1] - 0), (thirddir[2] - 0) };
		double vz2[3] = { 0, 0, 1 };
		double dotz = vtkMath::Dot(vz1, vz2);
		double zlenSq1 = vz1[0] * vz1[0] + vz1[1] * vz1[1] + vz1[2] * vz1[2];
		double zlenSq2 = vz2[0] * vz2[0] + vz2[1] * vz2[1] + vz2[2] * vz2[2];
		double angleZ = vtkMath::DegreesFromRadians(acos(dotz / sqrt(zlenSq1 * zlenSq2)));

		vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
		transform->Translate(nodeorigin.getCoordX(), nodeorigin.getCoordY(), nodeorigin.getCoordZ());
		transform->RotateX(angleX);
		transform->RotateY(angleY);
		transform->RotateZ(angleZ);

		vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();

		// The axes are positioned with a user transform
		axes->SetUserTransform(transform);

		// axis label		
		axes->SetXAxisLabelText("");
		axes->SetYAxisLabelText("");
		axes->SetZAxisLabelText("");

		double v[3] = { 100.0, 100.0, 100.0 };
		axes->SetTotalLength(v);

		return axes;
	}

	vtkSmartPointer<vtkAxesActor> VtkDisplayHighlighters::normalFrame(hbm::Coord coord){
		double nodeorigin[3] = { 1, 0, 0 };
		double nodefirstdir[3] = { 0, 1, 0 };
		double nodeseconddir[3] = { 0, 0, 1 };

		double v1[3] = { (nodefirstdir[0] - nodeorigin[0]), (nodefirstdir[1] - nodeorigin[1]), (nodefirstdir[2] - nodeorigin[2]) };
		double v2[3] = { 1, 0, 0 };
		double dot = vtkMath::Dot(v1, v2);
		double lenSq1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
		double lenSq2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
		double angleX = vtkMath::DegreesFromRadians(acos(dot / sqrt(lenSq1 * lenSq2)));

		double vy1[3] = { (nodeseconddir[0] - nodeorigin[0]), (nodeseconddir[1] - nodeorigin[1]), (nodeseconddir[2] - nodeorigin[2]) };
		double vy2[3] = { 0, 1, 0 };
		double doty = vtkMath::Dot(vy1, vy2);
		double ylenSq1 = vy1[0] * vy1[0] + vy1[1] * vy1[1] + vy1[2] * vy1[2];
		double ylenSq2 = vy2[0] * vy2[0] + vy2[1] * vy2[1] + vy2[2] * vy2[2];
		double angleY = vtkMath::DegreesFromRadians(acos(doty / sqrt(ylenSq1 * ylenSq2)));

		double thirddir[3];
		vtkMath::Cross(vy1, vy2, thirddir);

		double vz1[3] = { (thirddir[0] - 0), (thirddir[1] - 0), (thirddir[2] - 0) };
		double vz2[3] = { 0, 0, 1 };
		double dotz = vtkMath::Dot(vz1, vz2);
		double zlenSq1 = vz1[0] * vz1[0] + vz1[1] * vz1[1] + vz1[2] * vz1[2];
		double zlenSq2 = vz2[0] * vz2[0] + vz2[1] * vz2[1] + vz2[2] * vz2[2];
		double angleZ = vtkMath::DegreesFromRadians(acos(dotz / sqrt(zlenSq1 * zlenSq2)));

		vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
		transform->Translate(coord[0], coord[1], coord[2]);

		vtkSmartPointer<vtkAxesActor> axes = vtkSmartPointer<vtkAxesActor>::New();

		// The axes are positioned with a user transform
		axes->SetUserTransform(transform);

		// axis label		
		axes->SetXAxisLabelText("");
		axes->SetYAxisLabelText("");
		axes->SetZAxisLabelText("");

		double v[3] = { 100.0, 100.0, 100.0 };
		axes->SetTotalLength(v);

		return axes;
	}

    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::cube( double center[3]){

		// Create a cube.
		vtkSmartPointer<vtkCubeSource> cubeSource =
			vtkSmartPointer<vtkCubeSource>::New();

		cubeSource->SetCenter(center);

		cubeSource->SetXLength(100);
		cubeSource->SetYLength(100);
		cubeSource->SetZLength(100);
		cubeSource->Update();
		cout << "Cells: " << cubeSource->GetOutput()->GetNumberOfCells() << "\n";
		cout << "Points: " << cubeSource->GetOutput()->GetNumberOfPoints() << "\n";
		for (int i = 0; i < 24; i++){
			cout << "Point " << i + 1 << " :: " << cubeSource->GetOutput()->GetPoint(i)[0] << ", " << cubeSource->GetOutput()->GetPoint(i)[1] << ", " << cubeSource->GetOutput()->GetPoint(i)[2] << "\n";
		}
		// Create a mapper and actor.
		vtkSmartPointer<vtkPolyDataMapper> mapper =
			vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputConnection(cubeSource->GetOutputPort());

		vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		actor->SetMapper(mapper);
		actor->GetProperty()->SetOpacity(0.5);
        actor->SetOriginalDataSet(cubeSource->GetOutput());
        return actor;
	}



    vtkSmartPointer<vtkGlyph3DMapper> VtkDisplayHighlighters::AllPoints(vtkSmartPointer<vtkDataSet> sourceData, double radius)
    {

        // initialize mapper for point highlighting
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        sphereSource->SetCenter(0, 0, 0);
        sphereSource->SetRadius(radius);
        vtkSmartPointer<vtkGlyph3DMapper> pointHighlight = vtkSmartPointer<vtkGlyph3DMapper>::New();
        pointHighlight->OrientOff();

        pointHighlight->SetSourceConnection(sphereSource->GetOutputPort());
        pointHighlight->SetInputData(sourceData);
        pointHighlight->SetScaleModeToNoDataScaling();

        return pointHighlight;
	}

    vtkSmartPointer<vtkGlyph3DMapper> VtkDisplayHighlighters::CellNormals(vtkSmartPointer<vtkPolyData> sourceData, double scale)
    {
        hbm::vtkSelectionTools::computeNormalizedNormalsInPlace(sourceData); // can't be sure that the normals are up to date even if they exist -> recompute them
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        points->Initialize();
        points->SetNumberOfPoints(sourceData->GetNumberOfCells());
        double cellPoint[3];

        vtkDataArray *normals = sourceData->GetCellData()->GetNormals();
        for (int i = 0; i < points->GetNumberOfPoints(); i++)
        {
            vtkCell *curCell = sourceData->GetCell(i);
            double centroid[3] = { 0, 0, 0 };
            for (int j = 0; j < curCell->GetNumberOfPoints(); j++) // for each point of the cell
            {
                sourceData->GetPoint(curCell->GetPointId(j), cellPoint);
                for (int k = 0; k < 3; k++) // add the coordinates to the centroid
                    centroid[k] += cellPoint[k];
            }
            for (int k = 0; k < 3; k++) // average the accumulated coordinates to get the center
                centroid[k] /= curCell->GetNumberOfPoints();
            points->SetPoint(i, centroid); // set the centroid as the point at which to display the normal
        }
        vtkSmartPointer<vtkGlyph3DMapper> normalHighlight = vtkSmartPointer<vtkGlyph3DMapper>::New();
        // set the normals as a point array of the data structure for the glyph mapper
        vtkSmartPointer<vtkPolyData> actorDataForNormals = vtkSmartPointer<vtkPolyData>::New();
        actorDataForNormals->SetPoints(points);
        actorDataForNormals->GetPointData()->SetNormals(normals);
        vtkSmartPointer<vtkArrowSource> arrow = vtkSmartPointer<vtkArrowSource>::New();
        arrow->SetTipResolution(3);
        arrow->SetTipLength(0.3);
        arrow->SetTipRadius(0.1);
        normalHighlight->SetSourceConnection(arrow->GetOutputPort());
        normalHighlight->SetInputData(actorDataForNormals);
        normalHighlight->SetOrientationArray(vtkDataSetAttributes::NORMALS);
        normalHighlight->SetOrientationModeToDirection();
        normalHighlight->SetScaleFactor(scale);
        normalHighlight->OrientOn();
        normalHighlight->Update();

        return normalHighlight;
    }

    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::point(double x, double y, double z, double radius, float r, float g, float b)
    {
        std::stringstream name;
        name << "point_" << x << "_" << y << "_" << z;
        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        sphereSource->SetCenter(x, y, z);
        sphereSource->SetRadius(radius);
        vtkSmartPointer<vtkPolyDataMapper> pointHighlight = vtkSmartPointer<vtkPolyDataMapper>::New();
        pointHighlight->SetInputConnection(sphereSource->GetOutputPort());
        actor->SetMapper(pointHighlight);
        actor->GetProperty()->SetDiffuseColor(r, g, b);
        actor->SetName(name.str());
        return actor;
    }

    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::polyLine(vtkSmartPointer<vtkPoints> points){
		vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New();
		polyLine->GetPointIds()->SetNumberOfIds(points->GetNumberOfPoints());
		for (vtkIdType k = 0; k < points->GetNumberOfPoints(); k++)
		{
			polyLine->GetPointIds()->SetId(k, k);
		}

		// Create a cell array to store the lines in and add the lines to it
		vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
		cells->InsertNextCell(polyLine);

		// Create a polydata to store everything in
		vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

		// Add the points to the dataset
		polyData->SetPoints(points);

		// Add the lines to the dataset
		polyData->SetLines(cells);

		// Setup actor and mapper
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputData(polyData);
        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		actor->SetMapper(mapper);
        
		return actor;
	}

    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::polygon(vtkSmartPointer<vtkPoints> pts){
		// Create a polygon
		vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
        vtkIdType nbPoints = pts->GetNumberOfPoints();
		polygon->GetPointIds()->SetNumberOfIds(nbPoints);
		for (vtkIdType i = 0; i < nbPoints; i++){
			polygon->GetPointIds()->SetId(i, i);
		}

		// Add the polygon to a list of polygons
		vtkSmartPointer<vtkCellArray> polygons = vtkSmartPointer<vtkCellArray>::New();
		polygons->InsertNextCell(polygon);

		// Create a PolyData
		vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
		polyData->SetPoints(pts);
		polyData->SetPolys(polygons);

		// Create a mapper and actor
		vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputData(polyData);

        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		actor->SetMapper(mapper);
		actor->GetProperty()->SetColor(1.0, 0, 0);

		return actor;
	}

    
    boost::container::map<int, vtkOBBNodePtr> *VtkDisplayHighlighters::GetSelectionBoxes()
    {
        return &selectionBoxes;
    }

    vtkSmartPointer<VtkDisplayActor>  VtkDisplayHighlighters::SelectionBox(vtkOBBNodePtr obb)
    {
        vtkSmartPointer<vtkPolyData> Box_ActorData = obb->GenerateAsPolydata();

        // create a unique name of the box
        int boxID = 0;
        for (auto it = selectionBoxes.begin(); it != selectionBoxes.end(); it++)
        {
            if (it->first >= boxID)
                boxID = it->first + 1; // make it so that the newly added box will have the highest index
        }
        std::stringstream boxName;
        boxName << SELECTION_BOX_ACTOR << boxID;
        // store the box so that it is later avaiable to be edited etc.
        selectionBoxes[boxID] = obb;

        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
        vtkSmartPointer<vtkPolyDataMapper>mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputDataObject(Box_ActorData);
        actor->SetOriginalDataSet(Box_ActorData);
        actor->SetMapper(mapper);
        actor->SetName(boxName.str());
        float r, g, b, a;
        colorFormatTransferUINTtoFLOAT(_p_colorOfSelection, &a, &r, &g, &b);
        actor->GetProperty()->SetDiffuseColor(r, g, b);
        actor->GetProperty()->SetOpacity(0.5);
        actor->SetVisibility(boxesVisible);
        return actor;
    }

    vtkSmartPointer<VtkDisplayActor>  VtkDisplayHighlighters::SelectionBox(double boxOrigin[3], double boxAxes[3][3])
    {
        vtkOBBNodePtr obb = std::make_shared<vtkOBBNodeWNeighbors>();
        for (int i = 0; i < 3; i++)
        {
            obb->Axes[i][0] = boxAxes[i][0];
            obb->Axes[i][1] = boxAxes[i][1];
            obb->Axes[i][2] = boxAxes[i][2];
            obb->Corner[i] = boxOrigin[i];
        }
        return SelectionBox(obb);
    }
    
    vtkSmartPointer<VtkDisplayActor> VtkDisplayHighlighters::SelectionRectangle(double center[3], double firstdir[3], double seconddir[3], double sizeFirstDir, double sizeSecondDir)
    {
        // Create a plane
        vtkMath::Normalize(firstdir);
        vtkMath::Normalize(seconddir);
        sizeFirstDir /= 2;
        sizeSecondDir /= 2;
        for (int i = 0; i < 3; i++)
        {
            firstdir[i] = firstdir[i] * sizeFirstDir;
            seconddir[i] = seconddir[i] * sizeSecondDir;
        }
        // create corners of the rectangle
        double pointA[3]{   center[0] - firstdir[0] - seconddir[0],
                            center[1] - firstdir[1] - seconddir[1],
                            center[2] - firstdir[2] - seconddir[2]};
        double pointB[3]{   center[0] + firstdir[0] - seconddir[0],
                            center[1] + firstdir[1] - seconddir[1],
                            center[2] + firstdir[2] - seconddir[2]};
        double pointC[3]{   center[0] + firstdir[0] + seconddir[0],
                            center[1] + firstdir[1] + seconddir[1],
                            center[2] + firstdir[2] + seconddir[2]};
        double pointD[3]{   center[0] - firstdir[0] + seconddir[0],
                            center[1] - firstdir[1] + seconddir[1],
                            center[2] - firstdir[2] + seconddir[2]};

        vtkSmartPointer<vtkPolyData> polyRect = vtkSmartPointer<vtkPolyData>::New();
        polyRect->Initialize();
        polyRect->Allocate();
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        points->InsertNextPoint(pointA);
        points->InsertNextPoint(pointB);
        points->InsertNextPoint(pointC);
        points->InsertNextPoint(pointD);
        polyRect->SetPoints(points);
        vtkSmartPointer<vtkIdList> list = vtkSmartPointer<vtkIdList>::New();
        list->InsertNextId(0);
        list->InsertNextId(1);
        list->InsertNextId(2);
        list->InsertNextId(3);
        polyRect->InsertNextCell(VTK_QUAD, list);
        // Create a mapper and actor
        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
        mapper->SetInputDataObject(polyRect);
        actor->SetOriginalDataSet(polyRect);
        actor->SetMapper(mapper);
        float r, g, b, a;
        colorFormatTransferUINTtoFLOAT(_p_colorOfSelection, &a, &r, &g, &b);
        actor->GetProperty()->SetDiffuseColor(r, g, b);
        actor->GetProperty()->SetOpacity(0.5);
        return actor;
    }


    void VtkDisplayHighlighters::SelectNextSelectionBox()
    {
        if (selectedBoxID == -1 && selectionBoxes.size() > 0)
            selectedBoxID = selectionBoxes.begin()->first;
        else if (selectionBoxes.size() == 1) // if there is only one, keep it selected
            selectedBoxID = selectionBoxes.begin()->first;
        else if (selectionBoxes.size() > 1)
        {
            auto itNext = ++(selectionBoxes.find(selectedBoxID));
            if (itNext == selectionBoxes.end())
                selectedBoxID = selectionBoxes.begin()->first;
            else
                selectedBoxID = itNext->first;
        }
    }

    void VtkDisplayHighlighters::SelectPrevSelectionBox()
    {
        if (selectedBoxID == -1 && selectionBoxes.size() > 0)
            selectedBoxID = selectionBoxes.begin()->first;
        else if (selectionBoxes.size() == 1) // if there is only one, keep it selected
            selectedBoxID = selectionBoxes.begin()->first;
        else if (selectionBoxes.size() > 1)
        {
            auto itCur = selectionBoxes.find(selectedBoxID);
            if (itCur == selectionBoxes.begin())
                selectedBoxID = (--(selectionBoxes.end()))->first;
            else
                selectedBoxID = (--(itCur))->first;
        }
    }

    void VtkDisplayHighlighters::RemoveSelectedBox()
    {
        if (selectedBoxID != -1)
        {
            auto box = selectionBoxes.find(selectedBoxID);
            if (box != selectionBoxes.end())
            {
                selectionBoxes.erase(box);
                selectedBoxID = -1;
            }
        }
    }

    vtkSmartPointer<vtkLookupTable> VtkDisplayHighlighters::CreateBinaryColorLUT(float r1, float g1, float b1, float a1,
        float r2, float g2, float b2, float a2, int value1, int value2)
    {
        vtkSmartPointer<vtkLookupTable> lut = vtkLookupTable::New();
        lut->SetNumberOfTableValues(2);
        lut->Build();

        lut->SetTableValue(value1, r1, g1, b1, a1);
        lut->SetTableValue(value2, r2, g2, b2, a2);
        return lut;
    }

    vtkSmartPointer<vtkLookupTable> VtkDisplayHighlighters::CreateColorLUT(vtkFloatArray *colors, vtkIntArray *values)
    {
        vtkSmartPointer<vtkLookupTable> lut = vtkSmartPointer<vtkLookupTable>::New();
        if (!colors || !values || colors->GetNumberOfComponents() != 4)
        {
            // wrong parameters
            std::cerr << "Wrong parameters in CreateColorLUT, returning empty LUT!" << std::endl;
            return lut;
        }
        int size = colors->GetNumberOfTuples();
        lut->SetNumberOfTableValues(size);
        lut->Build();

        for (int i = 0; i < size; i++)
            lut->SetTableValue(i, colors->GetTuple4(i));

        return lut;
    }

}
