/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropometryPredictor.h"

#include "common/logging.h"
#include "common/OctaveProcess.h"
#include "common/Context.h"
#include "common/helper.h"

#include "hbm/Metadata.h"

#include <sstream>
#include <fstream>


namespace piper {

    bool AnthropometryPredictor::setupOctavePredictionProcess(piper::OctaveProcess* op, QString inputCSVFile, QStringList userInputArray, bool genLandmarks)
    {
        if (genLandmarks) {
            PythonScript myscript;
            QString pythonscriptilfe = QCoreApplication::applicationDirPath() + QString::fromStdString("/../share/python/scripting/exportLandmarks.py");
            QStringList argList;
            argList.append(QString::fromStdString(tempDirectoryPath() + "/Anthro/HBM_landmarks.csv"));
            myscript.setPath(pythonscriptilfe);
            myscript.setArgs(argList);
            if (!myscript.run())
                return false;
        }
        prepareCSVfile(inputCSVFile, userInputArray, genLandmarks);
        setupOctavePredictionProcess(op, inputCSVFile);
        return true;
    }

    void AnthropometryPredictor::setupOctavePredictionProcess(piper::OctaveProcess* op, QString inputCSVFile) {
        pInfo() << START << "Generating current hbm dimensions from " << inputCSVFile.toStdString();
        op->setMatlabCompatibleMode(false);
        op->setScriptPath("/anthropoPerso/script/ToolsWP3/launchPersoAnthropoFromWP3.m");
        //Octave console output 
        op->setStandardOutputFile(QString::fromStdString(tempDirectoryPath("consoleReturns", true)) + "/standardOctaveConsoleOutput.dat");
        //First argument of the script: path to the csv input file, then: the path to the anthropometric scripts folder
        op->setScriptArguments(QStringList() << inputCSVFile/*"D:/mear/Desktop/exampleInputC.csv"*/ << piper::octaveScriptDirectoryPath() + "/anthropoPerso/script");
    }

    void AnthropometryPredictor::computePrediction(piper::OctaveProcess* op, QString inputCSVFile)
    {
        setupOctavePredictionProcess(op, inputCSVFile);
        pInfo() << START << "Anthropometry prediction";
        try {
            op->executeScript();
        }
        catch (std::exception const& e) {
            pCritical() << "Anthropometry predictor: " << e.what();
        }
        pInfo() << DONE;
    }

    void AnthropometryPredictor::computePrediction(piper::OctaveProcess* op, QString inputCSVFile, QStringList userInputArray, bool genLandmarks)
    {
        if (setupOctavePredictionProcess(op, inputCSVFile, userInputArray, genLandmarks))
        {
            try {
                op->executeScript();
            }
            catch (std::exception const& e) {
                pCritical() << "Anthropometry predictor: " << e.what();
            }
            pInfo() << DONE;
        }
        // else the error message is printed inside the handler of the script that can fail
    }

    void AnthropometryPredictor::startPrediction(QString inputCSVFile, QStringList userInputArray, bool genLandmarks) {
        if (setupOctavePredictionProcess(Context::instance().octaveProcessPtr(), inputCSVFile, userInputArray, genLandmarks))
        {
            QFileInfo inputFromUserCsv(inputCSVFile);
            if (inputFromUserCsv.exists()) {
                connect(Context::instance().octaveProcessPtr(), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onScriptFinished(int, QProcess::ExitStatus)), Qt::UniqueConnection);
                Context::instance().octaveProcess().startScript();
            }
        }
        // else the error message is printed inside the handler of the script that can fail
    }

    void AnthropometryPredictor::onScriptFinished(int exitCode, QProcess::ExitStatus exitStatus) {
        if (!Context::instance().octaveProcess().isLastExecutionSuccessful()) {
            pCritical() << "Anthropometry predictor: " << Context::instance().octaveProcess().errorMessage();
        }
        else {
            pInfo() << DONE;
            if (m_relative) {
                QFileInfo inputRelativeCsv = QString::fromStdString(piper::tempDirectoryPath() + "/" + relativeCSVFile);
                if (m_relative && inputRelativeCsv.exists()) {
                    setupOctavePredictionProcess(Context::instance().octaveProcessPtr(), inputRelativeCsv.absoluteFilePath());
                    m_relative = false;
                    connect(Context::instance().octaveProcessPtr(), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onScriptFinished(int, QProcess::ExitStatus)), Qt::UniqueConnection);
                    Context::instance().octaveProcess().startScript();
                }
            }
            else
            computeFinished();
        }
    }

    void AnthropometryPredictor::prepareCSVfile(QString inputCSVFile, QStringList userInputArray, bool genLandmarks) {
        pInfo() << START << "Generating temporary CSV input file " << (inputCSVFile.toStdString()) << " based on the specified input...";
        //nes of inputArray are paths, and we want to get rid of the "" at the begining of these paths    
        QStringList currentLine;
        //Stream declaration to write in a csv file
        ofstream myfile;
        //create the csv input file in the piper temp folder
        myfile.open(inputCSVFile.toStdString(), ios::trunc);

        //Go through the array containing user input
        for (int j = 0; j < userInputArray.size(); j++)
        {
            //split the line regarding commas
            currentLine = userInputArray.at(j).split(",");

            //If the last part of the current line contains a Path, we need to get rid of the "file:///" at the begining of the string paths
            if (currentLine.last().contains("file:/"))
            {
                //Create a QUrl with the path (located after last comma on current line)
                QUrl url(currentLine.last());
                //Remove unwanted old path from custom list
                currentLine.removeLast();
                //Add the new wanted path in the place of the old one
                currentLine += url.toLocalFile() + ",,,,,,,,,";
                //Replace the entire old current line with a line containing the new path 
                userInputArray[j] = currentLine.join(",");
            }
            //write current line into the csv input file
            myfile << userInputArray.at(j).toStdString() + '\n';

        }
        if (genLandmarks)
            myfile << "Parameter,useLandmarks,true\n";
        else
            myfile << "Parameter,genLandmarks,false\n";

        myfile.close();

        std::string dataset = getDataSetfromCSVfile(inputCSVFile);
        m_relative = false;
        // check requirements for relative target
        piper::hbm::Metadata const& metadata = Context::instance().project().model().metadata();
        if ((dataset == "SNYDER" && metadata.anthropometry().weight.isDefined() && metadata.anthropometry().height.isDefined() && metadata.anthropometry().age.isDefined()) ||
            (dataset != "SNYDER" && metadata.anthropometry().weight.isDefined() && metadata.anthropometry().height.isDefined())) {
            prepareCSVfileRelativeScaling(inputCSVFile, QString::fromStdString(dataset));
        }

        pInfo() << DONE;
    }

    void AnthropometryPredictor::prepareCSVfileRelativeScaling(QString inputCSVFile, QString dataset) {
        piper::hbm::Metadata const& metadata = Context::instance().project().model().metadata();
        std::string stature, weight, age;
        if (dataset == "ANSUR") {
            stature = "STATURE";
            weight = "WEIGHT";
            age = "AGE";
        }
        else if (dataset == "SNYDER") {
            stature = "Stature";
            weight = "Weight";
            age = "Age_in_Years";
        }
        else if (dataset == "CCTANTHRO") {
            stature = "Taille";
            weight = "Poids";
            age = "Age";
        }
        else return;

        m_relative = true;

        double weightvalue(0.0);
        double staturevalue(0.0);
        double agevalue(-1.0);
        weightvalue = units::convert(metadata.anthropometry().weight.value(), metadata.massUnit(), units::Mass::hg);
        staturevalue = units::convert(metadata.anthropometry().height.value(), metadata.lengthUnit(), units::Length::mm);
        if (dataset == "SNYDER") {
            agevalue = units::convert(metadata.anthropometry().age.value(), metadata.ageUnit(), units::Age::year);
        }
        // write another target file, to define relative scaling factors for anthropometric dimensions
        ofstream myfileHbm;
        //create the csv input file in the piper temp folder
        myfileHbm.open(piper::tempDirectoryPath() + "/" + relativeCSVFile, ios::trunc);
        //re read the user input file to create a new fake one
        ifstream csvfile;
        csvfile.open(inputCSVFile.toStdString(), ios::in);
        std::string line;
        bool spy = true;
        while (std::getline(csvfile, line)) {
            // target file for estimation of dimension for hbm weight and stature
            std::string HbmDimensionFile = "HbmDimension.ptt";
            std::istringstream csvStream(line);
            std::vector<std::string> csvColumn;
            std::string information;
            // get selon element
            std::getline(csvStream, information, ',');
            std::getline(csvStream, information, ',');
            if (information == "SampleDir") {
                line = "Parameter,SampleDir," + piper::tempDirectoryPath() + "/";
                myfileHbm << line << "\n";
            }
            else if (information == "Predictor") {
                if (spy) {
                    line = "RegressionInformation,Predictor," + stature + ",,,,,,,,,";
                    myfileHbm << line << "\n";
                    line = "RegressionInformation,Predictor," + weight + ",,,,,,,,,";
                    myfileHbm << line << "\n";
                    if (dataset == "SNYDER") {
                        line = "RegressionInformation,Predictor," + age + ",,,,,,,,,";
                        myfileHbm << line << "\n";
                    }
                    spy = false;
                }
            }
            else if (information == "RegressionMatFilename") {
                std::string regressionfile;
                getline(csvStream, regressionfile, ',');
                std::size_t pos = regressionfile.find(".mat");
                regressionfile = regressionfile.substr(0, pos) + "_hbm.mat";
                line = "RegressionInformation,RegressionMatFilename," + regressionfile + ",,,,,,,,,";
                myfileHbm << line << "\n";
            }
            else if (information == "SampleOutputType") {
                line = "SampleInformation,SampleOutputType,MeanOnly,,,,,,,,,";
                myfileHbm << line << "\n";
            }
            else if (information == "SampleInputType") {
                line = "SampleInformation,SampleInputType,FixedPredictor,,,,,,,,,";
                myfileHbm << line << "\n";
            }
            else if (information == "SampleOutputPDFType") {
                line = "SampleInformation,SampleOutputPDFType,Normal,,,,,,,,,";
                myfileHbm << line << "\n";
            }
            else if (information == "SampleMeanOnlyXMLFilename") {
                line = "SampleInformation,SampleMeanOnlyXMLFilename," + HbmDimensionFile + ",,,,,,,,,";
                myfileHbm << line << "\n";
            }
            else if (information == "SampleInputValues") {
                if (dataset == "SNYDER")
                    line = "SampleInformation,SampleInputValues," + stature + "," + std::to_string(staturevalue) + "," + weight + "," + std::to_string(weightvalue) + "," + age + "," + std::to_string(agevalue);
                else
                    line = "SampleInformation,SampleInputValues," + stature + "," + std::to_string(staturevalue) + "," + weight + "," + std::to_string(weightvalue);
                myfileHbm << line << "\n";
            }
            else
                myfileHbm << line << "\n";
        }
        csvfile.close();
        myfileHbm.close();
    }

    std::string AnthropometryPredictor::getDataSetfromCSVfile(QString inputCSVFile) {
        std::string dataset = "";
        //Stream declaration to write in a csv file
        ifstream csvfile;
        //create the csv input file in the piper temp folder
        csvfile.open(inputCSVFile.toStdString(), ios::in);
        std::string line;
        while (std::getline(csvfile, line)) {
            std::istringstream csvStream(line);
            std::vector<std::string> csvColumn;
            std::string information;
            // get selon element
            std::getline(csvStream, information, ',');
            std::getline(csvStream, information, ',');
            if (information == "AnthropometricDataset") {
                getline(csvStream, dataset, ',');
                break;
            }
        }
        csvfile.close();
        return dataset;
    }

    void AnthropometryPredictor::computeGetInfo(piper::OctaveProcess* op, QUrl pathToRegressionFile) {
        setupOctaveGetInfoProcess(op, pathToRegressionFile);
        try {
            op->executeScript();
        }
        catch (std::exception const& e) {
            pCritical() << "Extracting Predictors: " << e.what();
        }
        pInfo() << DONE;
    }

    void AnthropometryPredictor::setupOctaveGetInfoProcess(piper::OctaveProcess* op, QUrl pathToRegressionFile) {
        pInfo() << START << "Extracting Predictors from regression file " << pathToRegressionFile.toLocalFile().toStdString();
        QFileInfo inputRegressionFile(pathToRegressionFile.toLocalFile());
        op->setMatlabCompatibleMode(false);
        op->setScriptPath("/anthropoPerso/script/ToolsWP3/infoRegression.m");
        //Octave console output 
        op->setStandardOutputFile(QString::fromStdString(tempDirectoryPath("consoleReturns", true)) + "/standardOctaveConsoleOutput.dat", QIODevice::WriteOnly);
        //First argument of the script: path to the regression file, then: the path to the output csvfile that will be created
        op->setScriptArguments(QStringList() << inputRegressionFile.absoluteFilePath() << QString::fromStdString(tempDirectoryPath() + "/extractedPredictors.csv"));
    }


    void AnthropometryPredictor::startGetInfo(QUrl pathToRegressionFile) {
        QFileInfo inputRegressionFile(pathToRegressionFile.toLocalFile());
        if (inputRegressionFile.exists() && inputRegressionFile.isFile()) {
            m_relative = false;
            setupOctaveGetInfoProcess(Context::instance().octaveProcessPtr(), pathToRegressionFile);
            connect(Context::instance().octaveProcessPtr(), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onScriptFinished(int, QProcess::ExitStatus)), Qt::UniqueConnection);
            Context::instance().octaveProcess().startScript();
        }
    }


}
