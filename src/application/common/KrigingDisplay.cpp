/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "KrigingDisplay.h"

#include "common/Context.h"
#include "anatomyDB/query.h"

#include <vtkPointData.h>

using namespace piper::hbm;

namespace piper {
    namespace kriging {

        KrigingDisplay::KrigingDisplay()
            :m_krigingSkin(nullptr), m_krigingBones(nullptr)
            , m_display(nullptr)
        {}


        void KrigingDisplay::init(KrigingPiperInterface* krigingSkin, KrigingPiperInterface* krigingBone, piper::VtkDisplay* display)
        {
            m_krigingSkin = krigingSkin;
            m_krigingBones = krigingBone;
            m_display = display;
        }

        KrigingDisplay::~KrigingDisplay() {
            m_display->DeactivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
        }

        void KrigingDisplay::reset()
        {
            deleteModuleActors();
        }
        
        void KrigingDisplay::deleteModuleActors() {
            m_display->RemoveActor(previewSkinName);
            for (auto &cur : m_bonePreview)
                m_display->RemoveActor(cur.first);
            m_bonePreview.clear();

            for (auto &cur : drawnControlPointsSource)
                m_display->RemoveActor(cur);
            drawnControlPointsSource.clear();

            for (auto &cur : drawnControlPointsTarget)
                m_display->RemoveActor(cur);
            
            for (auto &cur : m_TargetAssoMesh)
                m_display->RemoveActor("Asso_" + cur.first);            

            drawnControlPointsTarget.clear();
            m_TargetAssoMesh.clear();
            m_skinPreviewValid = false;
            m_bonePreviewValid = false;
        }

        void KrigingDisplay::displayControlPointsSource(std::string const& setname, bool const& visible, bool refreshDisplay) {
            std::string name = "cpsource_" + setname;
            if (!m_display->HasActor(name))
            {
                piper::hbm::FEModel const& fem = Context::instance().project().model().fem();
                piper::hbm::Metadata metadata = Context::instance().project().model().metadata();
                //
                if (metadata.hasInteractionControlPoint(setname)) 
                {
                    // create a vtk polydata for cotrol points
                    vtkSmartPointer<vtkPoints> controlPointPoints = vtkSmartPointer<vtkPoints>::New();
                    InteractionControlPoint *s = metadata.interactionControlPoint(setname);
                    std::vector<Coord> curvcoord;
                    s->getCoordControlPoint(fem, curvcoord);
                    for (auto const& cur : curvcoord)
                        controlPointPoints->InsertNextPoint(cur[0], cur[1], cur[2]);
                    vtkSmartPointer<vtkPolyData> controlPointMesh = vtkSmartPointer<vtkPolyData>::New();
                    controlPointMesh->SetPoints(controlPointPoints);
                    drawnControlPointsSource.push_back(name);
                    m_ControlPointsMesh[name] = controlPointMesh;
                    m_display->AddGroupOfPoints(controlPointMesh, name, false, m_display->POINTHIGHLIGHT_RADIUS, "",
                        DISPLAY_MODE::ENTITIES | DISPLAY_MODE::FULL_MESH | DISPLAY_MODE::POINTS, false, false);
                    Context::instance().display().SetObjectColor(name, 224.0 / 255.0, 138.0 / 255.0, 44.0 / 255.0);
                }
            }
            m_display->SetActorVisible(name, visible);
            if (refreshDisplay)
                m_display->Refresh();
        }

        void KrigingDisplay::displayControlPointsTarget(std::string const& setname, bool const& visible, bool refreshDisplay)
        {
            std::string name = "cptarget_" + setname;
            if (!m_display->HasActor(name))
            {
                piper::hbm::TargetList const& target = Context::instance().project().target();

                // create a vtk polydata for cotrol points
                vtkSmartPointer<vtkPoints> controlPointPoints = vtkSmartPointer<vtkPoints>::New();

                for (auto const& cur : target.controlPoint)
                {
                    if (cur.name() == setname)
                    {
                        VCoord const& curvcoord = cur.value();
                        for (auto const& cur : curvcoord)
                            controlPointPoints->InsertNextPoint(cur[0], cur[1], cur[2]);
                    }
                }

                if (controlPointPoints->GetNumberOfPoints() == 0) // try to look among the control point sets if there is some with "target" association with this name
                {
                    piper::hbm::Metadata metadata = Context::instance().project().model().metadata();
                    piper::hbm::FEModel const& fem = Context::instance().project().model().fem();
                    if (metadata.hasInteractionControlPoint(setname))
                    {
                        InteractionControlPoint *s = metadata.interactionControlPoint(setname);
                        if (s->getControlPointRole() != InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE)
                        {
                            std::vector<Coord> curvcoord;
                            s->getCoordControlPoint(fem, curvcoord);
                            for (auto const& cur : curvcoord)
                                controlPointPoints->InsertNextPoint(cur[0], cur[1], cur[2]);
                        }
                    }
                }
                if (controlPointPoints->GetNumberOfPoints() > 0)
                {
                    drawnControlPointsTarget.push_back(name);
                    vtkSmartPointer<vtkPolyData> controlPointMesh = vtkSmartPointer<vtkPolyData>::New();
                    controlPointMesh->SetPoints(controlPointPoints);
                    m_TargetPointsMesh[name] = controlPointMesh;
                    m_display->AddGroupOfPoints(controlPointMesh, name, false, m_display->POINTHIGHLIGHT_RADIUS, "",
                        DISPLAY_MODE::ENTITIES | DISPLAY_MODE::FULL_MESH | DISPLAY_MODE::POINTS, false, false);
                    Context::instance().display().SetObjectColor(name, 30.0 / 255.0, 179.0 / 255.0, 238.0 / 255.0);
                }
            }
            else // if it already exists, it might have association - make it visible/invisible as well
            {
                m_display->SetActorVisible("Asso_" + setname, visible);
            }
            m_display->SetActorVisible(name, visible);
            if (refreshDisplay)
                m_display->Refresh();
        }

        void KrigingDisplay::displayAsso(std::string const& setname, bool refreshDisplay, std::string const& sourceasso)
        {
            if (m_TargetAssoMesh.find(setname) == m_TargetAssoMesh.end()) 
            {
                std::string name = "cptarget_" + setname;
                vtkPoints *sourcePoints = m_ControlPointsMesh["cpsource_" + sourceasso]->GetPoints();
                vtkPoints *targetPoints = m_TargetPointsMesh[name]->GetPoints();
                vtkIdType nPoints = sourcePoints->GetNumberOfPoints();
                if (nPoints == targetPoints->GetNumberOfPoints())
                {
                    vtkSmartPointer<vtkUnstructuredGrid> assoMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
                    vtkSmartPointer<vtkPoints> assoPoints = vtkSmartPointer<vtkPoints>::New();
                    assoPoints->SetNumberOfPoints(2 * nPoints);
                    assoMesh->SetPoints(assoPoints);
                    vtkIdType list[2];
                    for (vtkIdType n = 0; n < nPoints; n++) 
                    {
                        assoPoints->SetPoint(n, sourcePoints->GetPoint(n));
                        assoPoints->SetPoint(n + nPoints, targetPoints->GetPoint(n));
                        list[0] = n;
                        list[1] = n + nPoints;
                        assoMesh->InsertNextCell(VTK_LINE, 2, list);
                    }
                    m_TargetAssoMesh[setname] = assoMesh;
                    Context::instance().display().AddVtkPointSet(assoMesh, "Asso_" + setname, false, DISPLAY_MODE::ALL, false, false);
                    Context::instance().display().SetObjectColor("Asso_" + setname, 255.0 / 255.0, 255.0 / 255.0, 0.0 / 255.0);
                    if (refreshDisplay)
                        m_display->Refresh();
                }
            }
        }

        void KrigingDisplay::removeControlPointsTarget(std::string const& setname, bool refreshDisplay)
        {
            std::string name = "cptarget_" + setname;
            for (auto it = drawnControlPointsTarget.begin(); it != drawnControlPointsTarget.end(); it++)
            {
                if (*it == name)
                {
                    auto meshIt = m_TargetPointsMesh.find(name);
                    if (meshIt != m_TargetPointsMesh.end()) // should always be true, but just to be safe
                        m_TargetPointsMesh.erase(meshIt);
                    drawnControlPointsTarget.erase(it);
                    m_display->RemoveActor(name);
                    removeAsso(setname, false);
                    if (refreshDisplay)
                        m_display->Refresh();
                    return;
                }
            }
        }

        void KrigingDisplay::removeControlPointsSource(std::string const& setname, bool refreshDisplay)
        {
            std::string name = "cpsource_" + setname;
            for (auto it = drawnControlPointsSource.begin(); it != drawnControlPointsSource.end(); it++)
            {
                if (*it == name)
                {
                    auto meshIt = m_ControlPointsMesh.find(name);
                    if (meshIt != m_ControlPointsMesh.end()) // should always be true, but just to be safe
                        m_ControlPointsMesh.erase(meshIt);
                    drawnControlPointsSource.erase(it);
                    m_display->RemoveActor(name);
                    if (refreshDisplay)
                        m_display->Refresh();
                    return;
                }
            }            
        }

        void KrigingDisplay::removeAsso(std::string const& setname, bool refreshDisplay)
        {
            auto assoIt = m_TargetAssoMesh.find(setname);
            if (assoIt != m_TargetAssoMesh.end())
            {
                m_display->RemoveActor("Asso_" + setname);
                m_TargetAssoMesh.erase(assoIt);
                if (refreshDisplay)
                    m_display->Refresh();
            }
        }

        bool KrigingDisplay::GetCPSourceVisible(std::string const& setname)
        {
            return m_display->IsActorVisible("cpsource_" + setname);
        }

        bool KrigingDisplay::GetCPTargetVisible(std::string const& setname)
        {
            return m_display->IsActorVisible("cptarget_" + setname);
        }

        void KrigingDisplay::HighlightControlPointsSource(std::string const& setname) {
            // reset color
            if (drawnControlPointsSource.size() > 0) {
                piper::hbm::FEModel const& fem = Context::instance().project().model().fem();
                for (auto const& curset : drawnControlPointsSource)  {
                    Context::instance().display().SetObjectColor(curset, 224.0 / 255.0, 138.0 / 255.0, 44.0 / 255.0);
                    Context::instance().display().UpdateActor(curset);
                }

                if (setname.size() > 0) // if it's not an empty string
                {
                    std::string name = "cpsource_" + setname;
                    Context::instance().display().SetObjectColor(name, 1, 0, 0);
                    Context::instance().display().UpdateActor(name);
                }
            }
            m_display->Render();
        }

        void KrigingDisplay::HighlightControlPointsTarget(std::string const& setname) {
            // reset color
            if (drawnControlPointsTarget.size() > 0) 
            {
                for (auto const& curset : drawnControlPointsTarget)  {
                    Context::instance().display().SetObjectColor(curset, 30.0 / 255.0, 179.0 / 255.0, 238.0 / 255.0);
                    Context::instance().display().UpdateActor(curset);
                }

                if (setname.size() > 0) // if it's not an empty string
                {
                    std::string name = "cptarget_" + setname;
                    Context::instance().display().SetObjectColor(name, 0, 1, 0);
                    Context::instance().display().UpdateActor(name);
                }
            }            
            m_display->Render();
        }

        void KrigingDisplay::SetSkinPreview(vtkSmartPointer<vtkPolyData> skinMesh)
        {
            m_skinPreview = skinMesh;
            m_display->AddVtkPointSet(m_skinPreview, previewSkinName, false, DISPLAY_MODE::HIGHLIGHTERS, false, true);
            m_display->ActivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
            // change color and opacity
            m_display->SetObjectColor(previewSkinName, 1, 0, 0, 0.5);
            m_skinPreviewValid = true;
        }

        vtkSmartPointer<vtkPolyData> KrigingDisplay::GetSkinPreview()
        {
            return m_skinPreview;
        }

        void KrigingDisplay::SetBonePreview(boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> const& bonesPreview)
        {
            m_bonePreview.clear();
            for (auto &bone : bonesPreview)
            {
                std::string name("preview_" + bone.first);
                m_bonePreview[name] = bone.second;
                // add preview models as non persistent actors
                m_display->AddVtkPointSet(bone.second, name, false, DISPLAY_MODE::HIGHLIGHTERS, false, true);
                m_display->SetActorVisible(name, false);
                m_display->ActivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
                // change color and opacity
                m_display->SetObjectColor(name, 1, 1, 0, 0.5);
            }
            m_bonePreviewValid = true;
        }

        boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> *KrigingDisplay::GetBonePreview()
        {
            return &m_bonePreview;
        }

        void KrigingDisplay::initSkinPreview() 
        {
            //look for entity skin(s)
            vtkSmartPointer<vtkPolyData> skin = Context::instance().project().model().fem().getFEModelVTK()->getSkin(false,
                Context::instance().project().model().metadata().entities(), Context::instance().project().model().fem());
            m_skinPreview = vtkSmartPointer<vtkPolyData>::New();
            m_skinPreview->DeepCopy(skin); // make a copy of the skin so that we can freely deform it
            if (skin->GetNumberOfPoints() == 0) {
                pCritical() << "No skins entities are defined in the current model: it is not possible to generate the preview of the deformation.";
                return;
            }
            previewSkin_source.clear();
            // get copy of nodes that define entities skin(s)
            for (vtkIdType i = 0; i < skin->GetNumberOfPoints(); i++) // for each node in the vtk representation
            {
                NodePtr n = std::make_shared<Node>();
                double *c = skin->GetPoint(i);
                n->setCoord(c[0], c[1], c[2]);
                previewSkin_source.setEntity(n);
            }
            previewSkin_target = hbm::Nodes(previewSkin_source);
            // add preview models as non persistent actors
            m_display->AddVtkPointSet(m_skinPreview, previewSkinName, false, DISPLAY_MODE::HIGHLIGHTERS, false, true);
            // change color and opacity
            m_display->SetObjectColor(previewSkinName, 1, 0, 0, 0.5);
            m_display->SetActorVisible(previewSkinName, false);
            m_display->ActivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
            m_skinPreviewValid = true;
            m_display->Refresh();
        }

        void KrigingDisplay::initBonePreview()
        {
            m_bonePreviewValid = false;
            if (Context::instance().hasModel()) 
            {
                m_bonePreview.clear();
                VId boneNodeIds = Context::instance().project().model().findBoneNodes(true, false, true);
                // get copy of nodes that define entities skin(s)
                for (Id & cur : boneNodeIds)
                {
                    Node nO = Context::instance().project().model().fem().getNode(cur);
                    NodePtr n = std::make_shared<Node>(nO);
                    previewBone_source.setEntity(n);
                }
                previewBone_target = hbm::Nodes(previewBone_source);
                // extract VTK mesh entities to display them
                auto entities = Context::instance().project().model().fem().getFEModelVTK()->getVTKEntityMesh();
                // all entities share the same points array - make a deep copy of that, but otherwise use shallow copies for the rest
                vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                points->DeepCopy(Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh()->GetPoints());
                for (auto const& cur : *entities) 
                {
                    if (anatomydb::isBone(cur.first))
                    {                        
                        vtkSmartPointer<vtkUnstructuredGrid> curentity = cur.second;
                        curentity->SetPoints(points);
                        std::string name("preview_" + cur.first);
                        m_bonePreview[name] = curentity;
                        // add preview models as non persistent actors
                        m_display->AddVtkPointSet(curentity, name, false, DISPLAY_MODE::HIGHLIGHTERS, false, true);
                        m_display->SetActorVisible(name, false);
                        m_display->ActivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
                        // change color and opacity
                        m_display->SetObjectColor(name, 1, 1, 0, 0.5);
                    }
                }
                m_bonePreviewValid = true;
            }
        }

        void KrigingDisplay::setSkinPreviewValid(bool valid)
        {
            m_skinPreviewValid = valid;
            if (!valid)
                m_display->RemoveActor(previewSkinName);
        }

        bool KrigingDisplay::getSkinPreviewValid()
        {
            return m_skinPreviewValid;
        }

        void KrigingDisplay::setBonePreviewValid(bool valid)
        {
            m_bonePreviewValid = valid;
            if (!valid)
            {
                for (auto cur : m_bonePreview)
                    m_display->RemoveActor(cur.first);
            }
        }

        bool KrigingDisplay::getBonePreviewValid()
        {
            return m_bonePreviewValid;
        }

        void KrigingDisplay::updateSkinPreview(bool forcereset)
        {
            if ((!m_skinPreviewValid || forcereset) && Context::instance().hasModel() && m_krigingSkin != nullptr)
            {
                if (!m_display->HasActor(previewSkinName))
                    initSkinPreview();

                m_krigingSkin->applyDeformation(previewSkin_source, &previewSkin_target);
                vtkIdType i = 0;

                for (NodePtr node : previewSkin_target) // for each node in the vtk representation of skin
                    m_skinPreview->GetPoints()->SetPoint(i++, node->getCoordX(), node->getCoordY(), node->getCoordZ());

                m_skinPreview->Modified();

                m_display->SetActorVisible(previewSkinName, true);
                m_skinPreviewValid = true;
                m_display->Render();
            }
        }

        void KrigingDisplay::updateBonePreview(bool forcereset) 
        {
            if ((!m_bonePreviewValid || forcereset) && Context::instance().hasModel() && m_krigingBones != nullptr)
            {
                if (m_bonePreview.empty())
                    initBonePreview();
                if (!m_bonePreview.empty())
                {
                    m_krigingBones->applyDeformation(previewBone_source, &previewBone_target);

                    vtkSmartPointer<vtkPoints> points = m_bonePreview.begin()->second->GetPoints();
                    vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(
                        Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh()->GetPointData()->GetArray("nid"));
                    std::map<Id, vtkIdType> inverseNid;
                    for (vtkIdType i = 0; i < nid->GetNumberOfTuples(); i++)
                        inverseNid[nid->GetValue(i)] = i;

                    for (NodePtr node : previewBone_target)
                        points->SetPoint(inverseNid[node->getId()], node->getCoordX(), node->getCoordY(), node->getCoordZ());
                    for (auto &cur : m_bonePreview)
                        m_display->SetActorVisible(cur.first, true);

                    m_bonePreviewValid = true;
                    m_display->Render();
                }
            }
        }

        void KrigingDisplay::visibleSkinPreview(bool const& visible, bool refreshDisplay, bool forcereset)
        {
            if (visible && !m_skinPreviewValid)
                updateSkinPreview(forcereset);
            else
                m_display->SetActorVisible(previewSkinName, visible);
            if (refreshDisplay)
                m_display->Refresh();
        }

        void KrigingDisplay::visibleBonePreview(bool const& visible, bool refreshDisplay, bool forcereset)
        {
            if (visible && !m_bonePreviewValid)
                updateBonePreview(forcereset);
            else
            {
                for (auto &cur : m_bonePreview)
                    m_display->SetActorVisible(cur.first, visible);
            }
            if (refreshDisplay)
                m_display->Refresh();
        }
    }
}
