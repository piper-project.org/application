/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "MetaEditor.h"
#include "Context.h"
#include "VtkDisplay.h"

#include <utility>
#include "lib/hbm/Group.h"
#include "lib/hbm/Helper.h"

#include <vtkMath.h>

// for visualization
#include <vtkPointData.h>

using namespace piper::hbm;

namespace piper {
	MetaEditor::MetaEditor()
	{ }

	MetaEditor::~MetaEditor() { }

	void MetaEditor::FramesActive(bool activateFrames)
	{
		if (activateFrames)
			Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::AXES);
		else Context::instance().display().DeactivateDisplayMode(DISPLAY_MODE::AXES);
		Context::instance().display().Refresh(false);
	}

#pragma region DisplayUICalls

	void MetaEditor::modelDisplay(int type){
		switch (type){
		case 0:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::FULL_MESH);
			break;
		case 1:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::ENTITIES);
			break;
		case 2:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::LANDMARKS);
			break;
		case 3:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::ALL);
			break;
		case 5:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::GENERICMETADATA);
			break;
		default:
			Context::instance().display().SetDisplayMode(DISPLAY_MODE::NONE);
			break;
		}
		Context::instance().display().Refresh(false);
	}

#pragma endregion

	void MetaEditor::getSelectedElementIDsBusyIndicator(QString name, int create, int update){
		Context::instance().performAsynchronousOperation(std::bind(&MetaEditor::getSelectedElementIDs, this, name, create, update), false, false, false, false, false);
	}

	void MetaEditor::getSelectedElementIDs(QString name, int create, int update){
		if (!Context::instance().project().model().empty()) {
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata& m_meta = Context::instance().project().model().metadata();
			FEModelVTK* m_fvtk = m_fem.getFEModelVTK();

			VId vid1D, vid2D, vid3D;

			// ------------------------------------------------------------------------------
			// Check for selected entities
			// ------------------------------------------------------------------------------
			std::vector <std::string> selectedActorNames = Context::instance().display().GetSelectedActorNames();
			SId sId1D, sId2D, sId3D;
			if (selectedActorNames.size() > 0){
				for (auto it = selectedActorNames.begin(); it != selectedActorNames.end(); ++it){
					VId ge1D = m_meta.entity(*it).get_groupElement1D();
					VId ge2D = m_meta.entity(*it).get_groupElement2D();
					VId ge3D = m_meta.entity(*it).get_groupElement3D();

					for (auto it = ge1D.begin(); it != ge1D.end(); ++it)
						sId1D.insert(m_fem.getGroupElements1D(*it).get().begin(), m_fem.getGroupElements1D(*it).get().end());
					for (auto it = ge2D.begin(); it != ge2D.end(); ++it)
						sId2D.insert(m_fem.getGroupElements2D(*it).get().begin(), m_fem.getGroupElements2D(*it).get().end());
					for (auto it = ge3D.begin(); it != ge3D.end(); ++it)
						sId3D.insert(m_fem.getGroupElements3D(*it).get().begin(), m_fem.getGroupElements3D(*it).get().end());
				}

				for (auto it = sId1D.begin(); it != sId1D.end(); ++it)
					vid1D.push_back(*it);
				for (auto it = sId2D.begin(); it != sId2D.end(); ++it)
					vid2D.push_back(*it);
				for (auto it = sId3D.begin(); it != sId3D.end(); ++it)
					vid3D.push_back(*it);
			}

			// ------------------------------------------------------------------------------
			// Check for selected elements
			// ------------------------------------------------------------------------------
			auto actors = Context::instance().display().GetDisplayActors();
			vtkUnstructuredGrid *contextModel = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh();
			vtkSmartPointer<vtkIdTypeArray> eid = vtkIdTypeArray::SafeDownCast(contextModel->GetCellData()->GetArray("eid"));
			for (auto it = actors->begin(); it != actors->end(); ++it)// for each actor
			{
				if (m_fvtk->getEntity(it->first) && it->second->HasSelectedCells())
				{
					auto sel_primitives = hbm::vtkSelectionTools::ObtainSelectionArrayCells(it->second->GetOriginalDataSet(), false); // get the selection array for cells of the original dataset of the actor
					vtkSmartPointer<vtkIdTypeArray> origCells = vtkIdTypeArray::SafeDownCast(m_fvtk->getEntity(it->first)->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
					for (int i = 0; i < sel_primitives->GetNumberOfTuples(); i++) // for each cell
					{
						if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED) // if that cell is selected, add it to the element
						{
							int indexInMainMesh = origCells->GetValue(i);
							int piperID = eid->GetValue(indexInMainMesh);
							vtkCell *cell = it->second->GetOriginalDataSet()->GetCell(i);
							int type = cell->GetCellType();

							if (type == VTK_LINE) {//1D
								vid1D.push_back(piperID);
							}
							else if ((type == VTK_TRIANGLE) || (type == VTK_QUAD)) {//2D
								vid2D.push_back(piperID);
							}
							else { //3D
								vid3D.push_back(piperID);
							}
						}
					}
				}
			}

			if (create == 0){
				addEntity(name.toStdString(), vid1D, vid2D, vid3D, update);
			}
			else{
				addGenericMetadata(name.toStdString(), vid1D, vid2D, vid3D, update);
			}
		}
	}

	void MetaEditor::getSelectedNodeIDsBusyIndicator(QString name, QString type, int create, int update){
		Context::instance().performAsynchronousOperation(std::bind(&MetaEditor::getSelectedNodeIDs, this, name, type, create, update), false, false,
			create == 0, // if create == 0, a new landmark will be added -> metadaUpdated should be emitted
			false, false);
	}

	void MetaEditor::getSelectedNodeIDs(QString name, QString type, int create, int update){
		if (!Context::instance().project().model().empty()) {
			VId vId;
			auto actors = Context::instance().display().GetDisplayActors();
			vtkUnstructuredGrid *contextModel = Context::instance().project().model().fem().getFEModelVTK()->getVTKMesh();
			vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(contextModel->GetPointData()->GetArray("nid"));
			for (auto it = actors->begin(); it != actors->end(); ++it)// for each actor
			{
				if (it->second->HasSelectedPoints())
				{
					auto sel_primitives = hbm::vtkSelectionTools::ObtainSelectionArrayPoints(it->second->GetOriginalDataSet(), false);
					for (int i = 0; i < sel_primitives->GetNumberOfTuples(); i++)
					{
						if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
						{
							vId.push_back(nid->GetValue(i));
						}
					}
				}
			}
			if (create == 0){
				addLandmark(name.toStdString(), type.toStdString(), vId, update);
			}
			else if (create == 1){
				addControlPoint(name.toStdString(), type.toStdString(), vId, update);
			}
		}
	}

#pragma region AddEditMetadata

	void MetaEditor::addEntity(std::string name, VId vid1D, VId vid2D, VId vid3D, int update){
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();
		FEModelVTK* m_fvtk = m_fem.getFEModelVTK();

		if (vid1D.size() > 0 || vid2D.size() > 0 || vid3D.size() > 0){

			if (update == 0){
				Entity newObject(name);
				if (!m_meta.hasEntity(name)){
					m_meta.addEntity(newObject);
				}
				else{
					pInfo() << WARNING << QStringLiteral("Entity with this name already exists, enter an new name");
					return;
				}
			}
			Entity& object = m_meta.entity(name);

			if (vid1D.size() > 0){
				GroupElements1DPtr new_ge1D = m_fem.get_GroupElements1DPtr();
				new_ge1D->set(vid1D);
				m_fem.set(new_ge1D);
				VId vid;
				vid.push_back(new_ge1D->getId());
				object.get_groupElement1D() = vid;
			}
			if (vid2D.size() > 0){
				GroupElements2DPtr new_ge2D = m_fem.get_GroupElements2DPtr();
				new_ge2D->set(vid2D);
				m_fem.set(new_ge2D);
				VId vid;
				vid.push_back(new_ge2D->getId());
				object.get_groupElement2D() = vid;
			}
			if (vid3D.size() > 0){
				GroupElements3DPtr new_ge3D = m_fem.get_GroupElements3DPtr();
				new_ge3D->set(vid3D);
				m_fem.set(new_ge3D);
				VId vid;
				vid.push_back(new_ge3D->getId());
				object.get_groupElement3D() = vid;
			}

			if (update == 0){
				new_entities.push_back(name);
			}
			else{
				Context::instance().display().RemoveActor(name, true); // Removing previous entity actor					
			}

			// Creating new entity actor
			vtkSmartPointer<vtkUnstructuredGrid> mesh = m_fvtk->addEntityMesh(object, m_fem, false);
			Context::instance().display().AddVtkPointSet(mesh, name, true, DISPLAY_MODE::ENTITIES, true, true);
			Context::instance().display().SetObjectColor(name, vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), 0.8);
			if (!Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::ENTITIES))
			{
				Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::ENTITIES);
			}
			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();

			new_entities.push_back(name);
		}
		else{
			pInfo() << INFO << QStringLiteral("Empty Entity");
		}
	}

	void MetaEditor::addLandmark(std::string name, std::string type, VId vId, int update){
		if (vId.size() > 0){
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata& m_meta = Context::instance().project().model().metadata();
			FEModelVTK* m_fvtk = m_fem.getFEModelVTK();

			std::string typeStr = type;
			Landmark::Type m_type;
			std::transform(typeStr.begin(), typeStr.end(), typeStr.begin(), ::toupper);
			if (typeStr == "POINT"){
				if (vId.size() == 1){
					m_type = Landmark::Type::POINT;
				}
				else{
					pInfo() << WARNING << QStringLiteral("Point Landmark can consist of only one node.");
					return;
				}
			}
			else if (typeStr == "SPHERE"){
				if (vId.size() >= 5){
					m_type = Landmark::Type::SPHERE;
				}
				else{
					pInfo() << WARNING << QStringLiteral("Point Landmark can consist of atleast five nodes.");
					return;
				}
			}
			else if (typeStr == "BARYCENTER"){
				if (vId.size() >= 1){
					m_type = Landmark::Type::BARYCENTER;
				}
				else{
					pInfo() << WARNING << QStringLiteral("Point Landmark can consist of atleast one node.");
					return;
				}
			}
			else{
				if (vId.size() == 1){
					m_type = Landmark::Type::POINT;
				}
				else{
					pInfo() << WARNING << QStringLiteral("Point Landmark can consist of only one node.");
					return;
				}
			}
			if (update == 0){
				Landmark newObject(name, m_type);
				if (!m_meta.hasLandmark(name)){
					m_meta.addLandmark(newObject);
				}
				else{
					pInfo() << WARNING << QStringLiteral("Landmark with this name already exists, enter an new name");
					return;
				}
			}
			Landmark& object = m_meta.landmark(name);
			object.setType(m_type);
			object.setNodes(vId);

			if (!Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::LANDMARKS))
			{
				Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::LANDMARKS);
			}
			Context::instance().display().RemoveNonpersistentActors();
			Context::instance().display().DeselectAllActors();
			Context::instance().display().deselectAllPoints();


			if (update == 0){
				new_landmarks.push_back(name);
			}
		}
		Context::instance().reloadLandmarkDisplay();
		emit Context::instance().metadataChanged();
		Context::instance().display().Refresh(false);
	}

	void MetaEditor::addControlPoint(std::string name, std::string type, VId vId, int update){
		if (vId.size() > 0){
			FEModel& m_fem = Context::instance().project().model().fem();
			Metadata& m_meta = Context::instance().project().model().metadata();
			FEModelVTK* m_fvtk = m_fem.getFEModelVTK();

			std::string typeStr = type;
			InteractionControlPoint::ControlPointRole m_role;
			std::transform(typeStr.begin(), typeStr.end(), typeStr.begin(), ::toupper);
			if (typeStr == "NONE"){
				m_role = InteractionControlPoint::ControlPointRole::CONTROLPOINT_ROLENOTSET;
			}
			else if (typeStr == "SOURCE"){
				m_role = InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE;
			}
			else if (typeStr == "TARGET"){
				m_role = InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET;
			}

			if (update == 0){
				InteractionControlPoint newObject(name, vId);
				newObject.setControlPointRole(m_role);
				if (!m_meta.hasInteractionControlPoint(name)){
					m_meta.addInteractionControlPoint(newObject);
				}
				else{
					pInfo() << WARNING << QStringLiteral("Control Point with this name already exists, enter an new name");
					return;
				}
			}
			else{
				InteractionControlPoint *object = m_meta.interactionControlPoint(name);
				for (auto it = vId.begin(); it != vId.end(); ++it) {
					object->addNodeId(*it);
				}
			}
			if (update != 0){
				removeControlPointActors(name);
			}

			if (!Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::POINTS))
			{
				Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::POINTS);
			}
			Context::instance().display().RemoveNonpersistentActors();
			Context::instance().display().DeselectAllActors();
			Context::instance().display().deselectAllPoints();

			if (update == 0){
				new_controlpoints.push_back(name);
			}
		}
		emit Context::instance().metadataChanged();
		Context::instance().display().Refresh(false);
	}

	void MetaEditor::addJoint(QString qName, QString constrainedDofType, QString entity1, int entity1Frame, QString entity2, int entity2Frame, int update){
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();

		std::string name = qName.toStdString();

		if (update == 0){
			EntityJoint newObject(name);
			if (!m_meta.hasJoint(name)){
				m_meta.addJoint(newObject);
			}
			else{
				pInfo() << WARNING << QStringLiteral("Joint with this name already exists, enter an new name");
				return;
			}
		}
		EntityJoint& object = m_meta.joint(name);

		if (constrainedDofType.toStdString() == "HARD"){
			object.setConstrainedDofType(piper::hbm::BaseJointMetadata::ConstrainedDofType::HARD);
		}
		else{
			object.setConstrainedDofType(piper::hbm::BaseJointMetadata::ConstrainedDofType::SOFT);
		}
		object.setEntity1(entity1.toStdString());
		object.setEntity2(entity2.toStdString());
		object.setEntity1FrameId(entity1Frame);
		object.setEntity2FrameId(entity2Frame);

		m_meta.addJoint(object);
		new_joints.push_back(name);

		if (!Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::AXES))
		{
			Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::AXES);
		}
		Context::instance().display().Refresh(false);
		emit Context::instance().metadataChanged();
	}

	void MetaEditor::addGenericMetadata(std::string name, VId vid1D, VId vid2D, VId vid3D, int update){
		FEModel& m_fem = Context::instance().project().model().fem();
		Metadata& m_meta = Context::instance().project().model().metadata();
		FEModelVTK* m_fvtk = m_fem.getFEModelVTK();

		if (vid1D.size() > 0 || vid2D.size() > 0 || vid3D.size() > 0){
			if (update == 0){
				GenericMetadata newObject(name);
				if (!m_meta.hasGenericmetadata(name)){
					m_meta.addGenericmetadata(newObject);
				}
				else{
					pInfo() << WARNING << QStringLiteral("GenericMetadata with this name already exists, enter an new name");
					return;
				}
			}
			GenericMetadata& object = m_meta.genericmetadata(name);

			if (vid1D.size() > 0){
				GroupElements1DPtr new_ge1D = m_fem.get_GroupElements1DPtr();
				new_ge1D->set(vid1D);
				m_fem.set(new_ge1D);
				VId vid;
				vid.push_back(new_ge1D->getId());
				object.get_groupElement1D() = vid;
			}
			if (vid2D.size() > 0){
				GroupElements2DPtr new_ge2D = m_fem.get_GroupElements2DPtr();
				new_ge2D->set(vid2D);
				m_fem.set(new_ge2D);
				VId vid;
				vid.push_back(new_ge2D->getId());
				object.get_groupElement2D() = vid;
			}
			if (vid3D.size() > 0){
				GroupElements3DPtr new_ge3D = m_fem.get_GroupElements3DPtr();
				new_ge3D->set(vid3D);
				m_fem.set(new_ge3D);
				VId vid;
				vid.push_back(new_ge3D->getId());
				object.get_groupElement3D() = vid;
			}

			if (update == 0){
				new_genericmetadatas.push_back(name);
			}
			else{
				Context::instance().display().RemoveActor(name, true); // Removing previous genericMetdata actor					
			}

			// Creating new genericMetdata actor
			m_fvtk->addGenericMetadataMesh(object, m_fem, false);
            vtkSmartPointer<vtkUnstructuredGrid> mesh = m_fvtk->getGenericMetadata(object.name());
			Context::instance().display().AddVtkPointSet(mesh, name, true, DISPLAY_MODE::GENERICMETADATA, true, true);
			Context::instance().display().SetObjectColor(name, vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), vtkMath::Random(.4, 1.0), 0.8);
			if (!Context::instance().display().IsDisplayModeActive(DISPLAY_MODE::GENERICMETADATA))
				Context::instance().display().ActivateDisplayMode(DISPLAY_MODE::GENERICMETADATA);
			Context::instance().display().Refresh(false);

			emit Context::instance().metadataChanged();
			new_genericmetadatas.push_back(name);
		}
		else{
			pInfo() << INFO << QStringLiteral("Empty GenericMetadata");
		}
	}
#pragma endregion	

#pragma region RemoveMetadata

	std::vector<std::string> MetaEditor::getControlPointActorNames(std::string name){
		std::vector<std::string> controlPoints;
		auto actors = Context::instance().display().GetDisplayActors();
		for (auto it = actors->begin(); it != actors->end(); ++it)// for each actor
		{
			if (it->first.find("CP#" + name) != std::string::npos){
				controlPoints.push_back(it->first);
			}
		}
		return controlPoints;
	}

	void MetaEditor::removeControlPointActors(std::string name){
		std::vector<std::string> landmarks = getControlPointActorNames(name);
		for (auto const& value : landmarks) {
			Context::instance().display().RemoveActor(value, true);
		}
	}

	void MetaEditor::removeControlPoint(QString name){
		if (!Context::instance().project().model().empty()) {
			Metadata& m_meta = Context::instance().project().model().metadata();
			m_meta.removeInteractionControlPoint(name.toStdString());
			removeControlPointActors(name.toStdString());

			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();
		}
	}

	void MetaEditor::removeEntity(QString name){
		if (!Context::instance().project().model().empty()) {
			Metadata& m_meta = Context::instance().project().model().metadata();
			m_meta.removeEntity(name.toStdString());

			Context::instance().display().RemoveActor(name.toStdString(), true);
			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();
		}
	}

	void MetaEditor::removeLandmark(QString name){
		if (!Context::instance().project().model().empty()) {
			Metadata& m_meta = Context::instance().project().model().metadata();
			m_meta.removeLandmark(name.toStdString());

			Context::instance().reloadLandmarkDisplay();
			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();
		}
	}

	void MetaEditor::removeGenericMetadata(QString name){
		if (!Context::instance().project().model().empty()) {
			Metadata& m_meta = Context::instance().project().model().metadata();
			m_meta.removeGenericMetadata(name.toStdString());

			Context::instance().display().RemoveActor(name.toStdString(), true);
			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();
		}
	}

	void MetaEditor::removeJoint(QString name){
		if (!Context::instance().project().model().empty()) {
			Metadata& m_meta = Context::instance().project().model().metadata();
			m_meta.removeJoint(name.toStdString());

			Context::instance().display().Refresh(false);
			Context::instance().display().RemoveNonpersistentActors();

			emit Context::instance().metadataChanged();
		}
	}

#pragma endregion

	void MetaEditor::selectActor(bool visible, QString name){
		std::string strName = name.toStdString();

		m_actors = Context::instance().display().GetDisplayActors();

		auto it = (*m_actors).find(strName);
		if (it != (*m_actors).end())
		{
			Context::instance().display().SelectActor(((*m_actors)[strName]), APPEND_ACTOR, false);
		}
		Context::instance().display().SelectElementsByEntities();
		Context::instance().display().DeselectAllActors();
	}
}

