/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HELPER_H
#define PIPER_HELPER_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <string>

#include <QString>
#include <QStringList>

namespace piper {

/// @return a safe temporary dir to be used in the piper application, append \a module as the last directory
PIPERCOMMON_EXPORT std::string tempDirectoryPath(std::string const& module = "", bool doCreateNotExisting=false);

/// @return the path to octave scripts
PIPERCOMMON_EXPORT QString octaveScriptDirectoryPath();

/// @return the path to Qt Help piper file
PIPERCOMMON_EXPORT QString qtHelpFilePath();

/// @return the path to piper share folder
PIPERCOMMON_EXPORT QString shareFolderPath();

/** wrapper around the PyRun_SimpleFile CPython function.
 * set current application directory to the path containing the script
 * this is what the user expects, in the script path to external files are given relative to the script location
 * the previous working dir is restored after script execution
 * \return error code
 */
PIPERCOMMON_EXPORT int runPythonScript(QString scriptPath, QStringList args = QStringList());

}

#endif // PIPER_HELPER_H
