/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "batch.h"

#include "logging.h"

#include "BodySectionPersonalizer/AnthropoModel.h"
#include "kriging/IntermediateTargetsInterface.h"
#include "kriging/CtrlPtLoader.h"

namespace piper {


    void importSimplifiedScalableModel(piper::Project & project, std::string const& filepath) {
        project.anthropoModel().clearAnthropoModel();
        project.anthropoModel().read(filepath);
        project.anthropoModel().setModel(&project.model());
    }

    void applyAnthropoScalingTarget(piper::Project & project, piper::hbm::TargetList const& target, bool const& useKrigingWIntermediates,
        bool const& useGeodesicForSkin, bool const& useBoneIntermediates, bool const& useSkinIntermediates, bool const& useDomains)
    {
        // Load targets
        piper::anthropometricmodel::AnthropoModel& modelanthropo = project.anthropoModel();
        piper::hbm::HumanBodyModel& hbmref = project.model();
        if (hbmref.empty()){
            pCritical() << QStringLiteral("No model loaded, aborting.");
            return;
        }
        if (!modelanthropo.isDefined()) {
            pCritical() << "No Simplified Scalable model is defined in project. Targets can not be applied";
        }

        modelanthropo.clearTargetsValue();
        try {
            std::set<std::string> const listdim = modelanthropo.listAnthropoBodyDimension();
            // anthropometricDimension target
            std::vector<std::string> missing_dim;
            std::vector<std::string> inconsistent_rel, inconsistent_absolute;
            for (hbm::AnthropometricDimensionTarget const& bodysectiondimtarget : target.anthropometricDimension) {
                if (listdim.find(bodysectiondimtarget.name()) == listdim.end())
                    missing_dim.push_back(bodysectiondimtarget.name());
                else {
                    if (!modelanthropo.getAnthropoBodyDimension(bodysectiondimtarget.name())->setTargetValue(bodysectiondimtarget)) {
                        if (modelanthropo.getAnthropoBodyDimension(bodysectiondimtarget.name())->isScalingRelative())
                            inconsistent_rel.push_back(bodysectiondimtarget.name());
                        else
                            inconsistent_absolute.push_back(bodysectiondimtarget.name());
                    }
                }
            }
            if (missing_dim.size() > 0) {
                std::string warnmsg;
                warnmsg += "No body dimension defined on HBM for the target ";
                for (auto const& curdim : missing_dim) {
                    warnmsg += curdim;
                    warnmsg += " ";
                }
                pInfo() << INFO << warnmsg;
            }
            if (inconsistent_rel.size() > 0) {
                std::string warnmsg;
                warnmsg += "Dimensions defined as relative with inconsistent target: ";
                for (auto const& curdim : inconsistent_rel) {
                    warnmsg += curdim;
                    warnmsg += " ";
                }
                pInfo() << WARNING << warnmsg;
            }
            if (inconsistent_absolute.size() > 0) {
                std::string warnmsg;
                warnmsg += "Dimensions defined as absolute with inconsistent target: ";
                for (auto const& curdim : inconsistent_absolute) {
                    warnmsg += curdim;
                    warnmsg += " ";
                }
                pInfo() << WARNING << warnmsg;
            }
            // landmark target
            if (target.landmark.size() > 0) {// add a option in GUI to say yes orno i want to use such lmandmark targets for anthropomodel target definition
                std::list<std::string> missingland = modelanthropo.setLandmarksTarget(target.landmark);
                if (missingland.size() > 0) {
                    std::string warnmsg;
                    warnmsg += "Following landmark target doesn't not have equivalent landmarks on current model: ";
                    for (auto const& curland : missingland) {
                        warnmsg += curland;
                        warnmsg += " ";
                    }
                    pInfo() << WARNING << warnmsg;
                }
            }

        }
        catch (std::exception const& e) {
            pCritical() << "Loadings Targets: " << e.what();
        }

        project.anthropoModel().setGlobalNugget(project.moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT));
        project.anthropoModel().updateControlPoints();        

        //Apply transformation
        if (useKrigingWIntermediates)
        {
            project.anthropoModel().getKrigingInterface()->setUseGeodesicSkin(useGeodesicForSkin);
            project.anthropoModel().getKrigingInterface()->setUseIntermediateDomains(useDomains);
            if (!useBoneIntermediates && !useSkinIntermediates)
                project.anthropoModel().getKrigingInterface()->setUseIntermediateSkin(true);
            else
            {
                project.anthropoModel().getKrigingInterface()->setUseIntermediateBones(useBoneIntermediates);
                project.anthropoModel().getKrigingInterface()->setUseIntermediateSkin(useSkinIntermediates);
            }

            project.anthropoModel().getKrigingInterface()->scaleHBMDenseWIntermediates(&project.model().fem().getNodes(),
                project.moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP), 0);
        }
        else
        {
            // copy fem nodes
            piper::hbm::Nodes my_nodes = piper::hbm::Nodes(hbmref.fem().getNodes());
            project.anthropoModel().getKrigingInterface()->scaleSparseAllCPs(hbmref.fem().getNodes(), &my_nodes);
            hbmref.fem().getNodes() = my_nodes;
        }

        pInfo() << INFO << "Model updated";

    }

    void loadTargetCPs(piper::Project & project, std::string const& filepath, std::string const& name)
    {
        kriging::CtrlPtLoader myLoader;
        if (myLoader.loadData(filepath))
        {
            //store data in a ctrlptset
            std::vector<hbm::Coord> vcoord;
            myLoader.getCoordinates(vcoord);
            // define target
            hbm::ControlPoint addtarget;
            addtarget.setName(name);
            addtarget.setSubsetName(name);
            addtarget.setValue(vcoord);
            addtarget.setUnit(project.model().metadata().lengthUnit());
            project.target().add(addtarget);
        }
        else
            pCritical() << "Reading file " << filepath << " failed.";
    }

    void loadSourceCPs(piper::Project & project, std::string const& filepath, std::string const& name)
    {
        kriging::CtrlPtLoader myLoader;
        if (myLoader.loadData(filepath))
        {
            hbm::HumanBodyModel& hbmref = project.model();
            hbm::Metadata::InteractionControlPointCont & ctrlPtCont = hbmref.metadata().interactionControlPoints();

            //store data in a ctrlptset
            hbm::InteractionControlPoint ctrlPt;
            ctrlPt.setName(name);
            ctrlPt.setControlPointRole(hbm::InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE);
            myLoader.getCoordinates(ctrlPt.getControlPt3dCont());

            if (!hbmref.metadata().addInteractionControlPoint(ctrlPt))
                pCritical() << "Adding control points " << name << " failed, insuficcient memory.";
        }
        else
            pCritical() << "Reading file " << filepath << " failed.";
    }

    void applyCPScalingTarget(piper::Project & project, piper::IntermediateTargetsInterface &krig_interface, bool useIntermediates)
    {
        hbm::HumanBodyModel& hbmref = project.model();
        if (hbmref.empty()){
            pCritical() << QStringLiteral("No model loaded, aborting.");
            return;
        }
        pInfo() << START << "Applying kriging to the model.";
        piper::hbm::Nodes nodes;

        if (useIntermediates)
            krig_interface.scaleHBMDenseWIntermediates(&nodes,
                project.moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_SPLIT_THRESHOLD_CP),
                project.moduleParameter().getUInt(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT));
        else
        {
            nodes = piper::hbm::Nodes(hbmref.fem().getNodes());
            krig_interface.scaleSparseAllCPs(hbmref.fem().getNodes(), &nodes);
        }

        project.model().fem().getNodes() = nodes;
        project.model().fem().updateVTKRepresentation();
        pInfo() << DONE;
    }

} // namespace piper
