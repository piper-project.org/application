/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_KRIGINGTARGET_H
#define PIPER_KRIGINGTARGET_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <QObject>
#include <QString>
#include <QStringList>

namespace piper {

    class OctaveProcess;

    /** This class used to apply Octave script to a set of control point defined in a file.
    *
    * \author Erwan Jolivet \date 2016
    */
    class PIPERCOMMON_EXPORT KrigingTargetGenerator : public QObject {
        Q_OBJECT

    public:

        /** Call the octave script.
        *
        * 1. produce the input files
        * 2. call the script with its required parameters
        * 3. Load target point txt file and associated target control points with 
        *
        * \param op to be used to run the scripts
        * \param ScriptPath input scripth path
        * \param ControlPointSet input name of control point source set defined in metadata
        * \param inputCPFile input file with coordinates of control point source
        * \param outputCPFile outpuput file with coordinates of control point target
        *
        * \see OctaveProcess
        * \todo better interface without duplicated parameters gravityVector / alignOnGravity
        */
        Q_INVOKABLE void compute(piper::OctaveProcess* op, QString ScriptPath, QString ControlPointSet, QString inputCPFile, QString outputCPFile);

        Q_INVOKABLE void compute(QString ScriptPath, QString ControlPointSet, QString inputCPFile, QString outputCPFile);
    };

}

#endif // PIPER_SPINEPREDICTOR_H
