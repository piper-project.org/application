/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_METADATAQMLPROXY_H
#define PIPER_METADATAQMLPROXY_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <QObject>
#include <QVector>
#include <QVariantList>

#include <hbm/Metadata.h>

namespace piper {

/** This class makes the glue between the GUI and the libhbm metadata.
 * It enables to retrieve and update some metadata from the gui.
 * @author Thomas Lemaire @date 2017
 */
class PIPERCOMMON_EXPORT MetadataQmlProxy : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList gravity READ gravity WRITE setGravity NOTIFY metadataChanged)
public:
    MetadataQmlProxy();

    QVariantList gravity() const;
    void setGravity(QVariantList const& g);

signals:
    void metadataChanged();

private:
    hbm::Metadata* m_metadata;

};

}

#endif // PIPER_METADATAQMLPROXY_H
