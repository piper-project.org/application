/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_KRIGINGDISPLAY_H
#define PIPER_KRIGINGDISPLAY_H

#include "kriging/KrigingPiperInterface.h"
#include "common/VtkDisplay.h"


#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#define SPHERE_SIZE_CP 10

namespace piper {
    namespace kriging {

        /** This class used to display a kriging control points and association
        *
        * \author Erwan Jolivet \date 2016
        */
        class PIPERCOMMON_EXPORT KrigingDisplay : public QObject {
            Q_OBJECT

        public:
            KrigingDisplay();
            ~KrigingDisplay();
            
            /// <summary>
            /// Initializes the display.
            /// </summary>
            /// <param name="krigingSkin">The kriging interface that is used for transforming the skin (can be the same as krigingBone).</param>
            /// <param name="krigingBone">The kriging interface that is used for transforming the bones (can be the same as krigingSkin).</param>
            /// <param name="display">The vtk display in which the rendering is done.</param>
            void init(KrigingPiperInterface* krigingSkin, KrigingPiperInterface* krigingBone, piper::VtkDisplay* display);
            // deletes all actors, reset all to default
            void reset();
            // set refreshDisplay to false if you are using more display*** calls in a row for better performance - refresh only on the last call
            /// display a set of control points defined in metadata
            Q_INVOKABLE void displayControlPointsSource(std::string const& setname, bool const& visible, bool refreshDisplay);
            Q_INVOKABLE void HighlightControlPointsSource(std::string const& setname);
            /// creates actors for displaying a set of control points defined in target
            Q_INVOKABLE void displayControlPointsTarget(std::string const& setname, bool const& visible, bool refreshDisplay);
            /// creates actors for displaying association between source and target control points as yellow lines
            void displayAsso(std::string const& setname, bool refreshDisplay, std::string const& sourceasso);
            /// renders all target points by the default color and only the specified "setname" ones by the highlight color
            Q_INVOKABLE void HighlightControlPointsTarget(std::string const& setname);
            /// udpate the preview (only skin entitites are deformed), according to current state of the kriging system
            Q_INVOKABLE void updateSkinPreview(bool forcereset);
            Q_INVOKABLE void updateBonePreview(bool forcereset);

            Q_INVOKABLE void setSkinPreviewValid(bool valid);
            bool getSkinPreviewValid();
            Q_INVOKABLE void setBonePreviewValid(bool valid);
            bool getBonePreviewValid();
            Q_INVOKABLE void visibleSkinPreview(bool const& visible, bool refreshDisplay, bool forcereset = false);
            Q_INVOKABLE void visibleBonePreview(bool const& visible, bool refreshDisplay, bool forcereset = false);

            /// <summary>
            /// Returns the visibility of source control points with the provided name.
            /// </summary>
            /// <param name="setname">Name of the control point set.</param>
            /// <returns><c>False</c> in case the set is currently hidden in the display OR if it does not exist, <c>true</c> otherwise.</returns>
            bool GetCPSourceVisible(std::string const& setname);

            /// <summary>
            /// Returns the visibility of target control points with the provided name.
            /// </summary>
            /// <param name="setname">Name of the control point set.</param>
            /// <returns><c>False</c> in case the set is currently hidden in the display OR if it does not exist, <c>true</c> otherwise.</returns>
            bool GetCPTargetVisible(std::string const& setname);

            void removeControlPointsTarget(std::string const& setname, bool refreshDisplay);
            void removeControlPointsSource(std::string const& setname, bool refreshDisplay);
            void removeAsso(std::string const& setname, bool refreshDisplay);
            
            /// <summary>
            /// Assigns the provided mesh as the skin preview for this display - this bypasses creation of the preview
            /// by using the KrigingPiperInterface provided on initialization.
            /// </summary>
            /// <param name="skinMesh">The skin mesh.</param>
            void SetSkinPreview(vtkSmartPointer<vtkPolyData> skinMesh);

            vtkSmartPointer<vtkPolyData> GetSkinPreview();
            
            /// <summary>
            /// Assigns the provided meshes as the preview for bones for this display - this bypasses creation of the preview
            /// by using the KrigingPiperInterface provided on initialization.
            /// </summary>
            /// <param name="bonesPreview">The bones preview, mapped by the name of the bone for each one.</param>
            void SetBonePreview(boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> const& bonesPreview);

            boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> *GetBonePreview();

            const std::string previewSkinName = "preview_skin";

        protected:
            KrigingPiperInterface* m_krigingSkin;
            KrigingPiperInterface* m_krigingBones;
            VtkDisplay* m_display;
            vtkSmartPointer<vtkPolyData> m_skinPreview;
            boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> m_bonePreview;
            std::vector<std::string> drawnControlPointsSource, drawnControlPointsTarget;
            hbm::Nodes previewSkin_source, previewSkin_target, previewBone_source, previewBone_target;

            boost::container::map<std::string, vtkSmartPointer<vtkPolyData>> m_ControlPointsMesh;
            boost::container::map<std::string, vtkSmartPointer<vtkPolyData>> m_TargetPointsMesh;
            boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> m_TargetAssoMesh;

            void deleteModuleActors();
            void initSkinPreview();
            void initBonePreview();
            bool m_skinPreviewValid = false;
            bool m_bonePreviewValid = false;
        };
    }
}

#endif // PIPER_KRIGINGDISPLAY_H
