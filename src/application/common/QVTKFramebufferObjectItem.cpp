/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "QVTKFramebufferObjectItem.h"

#include <QQuickFramebufferObject>
#include <QQuickWindow>
#include <QOpenGLFramebufferObject>
#include <QVTKInteractorAdapter.h>

#include <vtkRenderWindowInteractor.h>
#include <vtkObjectFactory.h>

#include <vtkSmartPointer.h>
#include <vtkRendererCollection.h>
#include <vtkCamera.h>
#include <vtkProperty.h>

#include <qglfunctions.h>

#include "common/logging.h"
#include "common/message.h"
#include "common/VtkMouseInteract.h"


class QVTKFramebufferObjectRenderer : public QQuickFramebufferObject::Renderer
{
    friend class vtkInternalOpenGLRenderWindow;

public:
    QVTKFramebufferObjectRenderer(vtkSmartPointer<vtkInternalOpenGLRenderWindow> rw) :
        m_framebufferObject(0)
    {
        m_vtkRenderWindow = rw;

        m_vtkRenderWindow->QtParentRenderer = this;
    }

    ~QVTKFramebufferObjectRenderer()
    {
        m_vtkRenderWindow->QtParentRenderer = 0;
        glFrontFace(GL_CCW); // restore default settings
    }

    virtual void synchronize(QQuickFramebufferObject * item)
    {
        // the first synchronize call - right before the the framebufferObject
        // is created for the first time
        if (!m_framebufferObject)
        {
            QVTKFrameBufferObjectItem *vtkItem = static_cast<QVTKFrameBufferObjectItem*>(item);
            vtkItem->init();
        }
    }

    // Called from the render thread when the GUI thread is NOT blocked
    virtual void render()
    {

        //m_vtkRenderWindow->PushState();

        // The initialization and storing/restoring gl state is now being done using the start/end slots
        // m_vtkRenderWindow->OpenGLInitState();

        m_vtkRenderWindow->InternalRender(); // vtkXOpenGLRenderWindow renders the scene to the FBO

        //  m_vtkRenderWindow->PopState();
    }

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size)
    {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::Depth);
        m_framebufferObject = new QOpenGLFramebufferObject(size, format);

        m_vtkRenderWindow->SetFramebufferObject(m_framebufferObject);

        return m_framebufferObject;
    }

    vtkSmartPointer<vtkInternalOpenGLRenderWindow> m_vtkRenderWindow;
    QOpenGLFramebufferObject *m_framebufferObject;
};

#pragma region vtkInternalOpenGLRenderWindow
vtkStandardNewMacro(vtkInternalOpenGLRenderWindow);

vtkInternalOpenGLRenderWindow::vtkInternalOpenGLRenderWindow() :
QtParentRenderer(0)
{
    vtkOpenGLRenderWindow::OpenGLInitContext();
    scenePicker = vtkSmartPointer<vtkHardwareSelector>::New();
    if (!this->GetContextSupportsOpenGL32())
    {
        pInfo() << piper::WARNING <<
            "OpenGL 3.2-compatible hardware not detected, using \"legacy\" visualization code. Rendering and interaction with the models might feel slow, especially during selection operations. Landmark picking by mouse will not work.";
        pInfo() << piper::WARNING <<
            "OpenGL 3.2-compatible hardware is required to correctly render transluscent objects. Transluscent objects might incorrectly appear to be \"behind\" or \"in front\" of other objects.";
    }
}

void vtkInternalOpenGLRenderWindow::OpenGLInitState()
{
    this->MakeCurrent();
    vtkOpenGLRenderWindow::OpenGLInitState();
    // Before any of the gl* functions in QOpenGLFunctions are called for a
    // given OpenGL context, an initialization must be run within that context
    initializeOpenGLFunctions();
    // use depth peeling for rendering transluscent objects
    if (hasTransparentObjects && this->GetContextSupportsOpenGL32())
    {
        // if the necessary prerequisities are available, enable depth-peeling
        // http://www.vtk.org/Wiki/VTK/Depth_Peeling#Required_OpenGL_extensions
        // vtk 7.0.0 does not seem to have simple way to ask directly for the needed extensions. less then 3.2 would be sufficient
        this->SetAlphaBitPlanes(1);
        this->SetMultiSamples(0);
        this->defaultRenderer->SetUseDepthPeeling(1);
        this->defaultRenderer->SetMaximumNumberOfPeels(10);
        this->defaultRenderer->SetOcclusionRatio(0.01);
    }

    glFrontFace(GL_CW); // to compensate for the switched Y axis
}

void vtkInternalOpenGLRenderWindow::InternalRender()
{
    if (!blockRendering)
    {
        if (!blockScenePicker && !scenePickerUpToDate && pickingRenderer->GetActors()->GetNumberOfItems() > 0) // if we want scene picking and the picker is not up to date, do a scenePicker render pass
        {
            pickerRenderPass = true;
            // switch active renderer
            defaultRenderer->DrawOff();
            defaultRenderer->EraseOff();
            scenePicker->ClearBuffers();
            pickingRenderer->DrawOn();
            pickingRenderer->EraseOn();
            // if edge rendering is on, we have to disable it as it creates artifacts in nodes picking when exactly the edge is clicked
            // note - this assumes that if it is turned on, it is turned on for all actors. If the would API enables selective on/off of edge visibility,
            // we would have to create a mask for each actor and turn it on/off selectively
            bool renderEdgesOn = false;
            vtkActorCollection *actors = pickingRenderer->GetActors();
            if (actors->GetLastItem()->GetProperty()->GetEdgeVisibility())
            {
                renderEdgesOn = true;
                actors->InitTraversal();
                vtkActor *actor;
                while ((actor = actors->GetNextActor()) != NULL)
                    actor->GetProperty()->SetEdgeVisibility(false);
            }
            // disable translucency - has no use for picking, only slows it down
            actors->InitTraversal();
            vtkActor *actor;
            while ((actor = actors->GetNextActor()) != NULL)
                actor->ForceOpaqueOn();

            scenePicker->SetArea(0, 0, this->GetSize()[0], this->GetSize()[1]);
            scenePicker->CaptureBuffers();
            scenePickerUpToDate = true;
            pickingRenderer->DrawOff();
            pickingRenderer->EraseOff();
            pickerRenderPass = false;
            if (renderEdgesOn) // set back edge rendering if it was disabled
            {
                actors->InitTraversal();
                vtkActor *actor;
                while ((actor = actors->GetNextActor()) != NULL)
                    actor->GetProperty()->SetEdgeVisibility(true);
            }
            // re-enable translucency
            actors->InitTraversal();
            while ((actor = actors->GetNextActor()) != NULL)
                actor->ForceOpaqueOff();
            // switch renderers back
            defaultRenderer->DrawOn();
            defaultRenderer->EraseOn();
        }
        vtkOpenGLRenderWindow::Render();
    }
}

void vtkInternalOpenGLRenderWindow::SetPickingRenderer(vtkSmartPointer<vtkRenderer> renderer)
{
    pickingRenderer = renderer;
    scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS);
    scenePicker->SetRenderer(renderer);
    vtkRenderWindow::AddRenderer(renderer);

    // make both renderers share the same camera of the default renderer
    if (defaultRenderer)
        pickingRenderer->SetActiveCamera(defaultRenderer->GetActiveCamera());
    pickingRenderer->DrawOff();
    pickingRenderer->EraseOff();
}

void vtkInternalOpenGLRenderWindow::SetDefaultRenderer(vtkSmartPointer<vtkRenderer> renderer)
{
    defaultRenderer = renderer;
    vtkRenderWindow::AddRenderer(renderer);
    // make both renderers share the same camera of the default renderer
    if (pickingRenderer)
        pickingRenderer->SetActiveCamera(defaultRenderer->GetActiveCamera());
}

void vtkInternalOpenGLRenderWindow::AddRenderer(vtkRenderer *renderer)
{
    SetDefaultRenderer(renderer);
}

//
// vtkInternalOpenGLRenderWindow Definitions
//

void vtkInternalOpenGLRenderWindow::Render()
{
    // if render was called during scenePicker render pass, we are already in the middle of updating the Qt parent renderer
    // -> we want to just directly call the vtkRenderWindow Render function
    if (pickerRenderPass) 
        vtkOpenGLRenderWindow::Render();
    else if (this->QtParentRenderer)
    {
        this->QtParentRenderer->update();
    }
}

void vtkInternalOpenGLRenderWindow::SetFramebufferObject(QOpenGLFramebufferObject *fbo)
{
    // QOpenGLFramebufferObject documentation states that "The color render
    // buffer or texture will have the specified internal format, and will
    // be bound to the GL_COLOR_ATTACHMENT0 attachment in the framebuffer
    // object"
    this->BackLeftBuffer = this->FrontLeftBuffer = this->BackBuffer = this->FrontBuffer =
        static_cast<unsigned int>(GL_COLOR_ATTACHMENT0);

    // Save GL objects by static casting to standard C types. GL* types
    // are not allowed in VTK header files.
    QSize fboSize = fbo->size();
    this->Size[0] = fboSize.width();
    this->Size[1] = fboSize.height();
    this->NumberOfFrameBuffers = 1;
    this->FrameBufferObject = static_cast<unsigned int>(fbo->handle());
    this->DepthRenderBufferObject = 0; // static_cast<unsigned int>(depthRenderBufferObject);
    this->TextureObjects[0] = static_cast<unsigned int>(fbo->texture());
    this->OffScreenRendering = 1;
    this->OffScreenUseFrameBuffer = 1;
    this->Modified();
}


#pragma endregion // render window

#pragma region QVTKFrameBufferObjectItem

void QVTKFrameBufferObjectItem::Start()
{
    m_win->OpenGLInitState();
}

void QVTKFrameBufferObjectItem::End()
{
}


void QVTKFrameBufferObjectItem::MakeCurrent()
{
    this->window()->openglContext()->makeCurrent(this->window());
}

void QVTKFrameBufferObjectItem::IsCurrent(vtkObject*, unsigned long, void*, void* call_data)
{
    bool* ptr = reinterpret_cast<bool*>(call_data);
    *ptr = this->window()->openglContext();
}

void QVTKFrameBufferObjectItem::IsDirect(vtkObject*, unsigned long, void*, void* call_data)
{
    int* ptr = reinterpret_cast<int*>(call_data);
    *ptr = QGLFormat::fromSurfaceFormat(this->window()->openglContext()->format()).directRendering();
}

void QVTKFrameBufferObjectItem::SupportsOpenGL(vtkObject*, unsigned long, void*, void* call_data)
{
    int* ptr = reinterpret_cast<int*>(call_data);
    *ptr = QGLFormat::hasOpenGL();
}


QVTKFrameBufferObjectItem::QVTKFrameBufferObjectItem(QQuickItem *parent) : QQuickFramebufferObject(parent)
{
    setAcceptedMouseButtons(Qt::AllButtons);

    m_irenAdapter = new QVTKInteractorAdapter(this);
    m_win = vtkSmartPointer<vtkInternalOpenGLRenderWindow>::New();
    
    // make a connection between the vtk signals and qt slots so that an initialized and madeCurrent opengl context is given to the vtk
    // we probably need only the Start(), MakeCurrent() and End() one, but just to be sure...
    mConnect = vtkSmartPointer<vtkEventQtSlotConnect>::New();
    mConnect->Connect(m_win, vtkCommand::WindowMakeCurrentEvent, this, SLOT(MakeCurrent()));
    mConnect->Connect(m_win, vtkCommand::WindowIsCurrentEvent, this, SLOT(IsCurrent(vtkObject*, unsigned long, void*, void*)));
    mConnect->Connect(m_win, vtkCommand::StartEvent, this, SLOT(Start()));
    mConnect->Connect(m_win, vtkCommand::EndEvent, this, SLOT(End()));
    mConnect->Connect(m_win, vtkCommand::WindowIsDirectEvent, this, SLOT(IsDirect(vtkObject*, unsigned long, void*, void*)));
    mConnect->Connect(m_win, vtkCommand::WindowSupportsOpenGLEvent, this, SLOT(SupportsOpenGL(vtkObject*, unsigned long, void*, void*)));
}

QVTKFrameBufferObjectItem::~QVTKFrameBufferObjectItem()
{
    mConnect->Disconnect(); // disconnect all slots
    if (m_irenAdapter)
        delete m_irenAdapter;
}

QQuickFramebufferObject::Renderer *QVTKFrameBufferObjectItem::createRenderer() const
{
   return new QVTKFramebufferObjectRenderer(m_win);
}

vtkSmartPointer<vtkInternalOpenGLRenderWindow> QVTKFrameBufferObjectItem::GetRenderWindow() const
{
   return m_win;
}

void QVTKFrameBufferObjectItem::init()
{
}

// theoretically not needed now - the Y is being flipped in render and devicePixelRatio will almost always be = 1 on a PC anyway...but lets keep it to be sure
QMouseEvent QVTKFrameBufferObjectItem::openGLToNative(QMouseEvent const& event)
{
    QPointF localPos(event.localPos());
    localPos.setX(localPos.x() * window()->devicePixelRatio());
	localPos.setY(localPos.y() * window()->devicePixelRatio());
    QMouseEvent nativeEvent(event.type(), localPos, event.button(), event.buttons(), event.modifiers());
    return nativeEvent;
}

void QVTKFrameBufferObjectItem::mouseMoveEvent(QMouseEvent * event)
{
    m_win->GetInteractor()->SetSize(this->width(), this->height());
    QMouseEvent nativeEvent = openGLToNative(*event);
    m_irenAdapter->ProcessEvent(&nativeEvent, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::mousePressEvent(QMouseEvent * event)
{
    m_win->GetInteractor()->SetSize(this->width(), this->height());
    QMouseEvent nativeEvent = openGLToNative(*event);
    m_irenAdapter->ProcessEvent(&nativeEvent, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::mouseReleaseEvent(QMouseEvent * event)
{
    m_win->GetInteractor()->SetSize(this->width(), this->height());
    QMouseEvent nativeEvent = openGLToNative(*event);
    m_irenAdapter->ProcessEvent(&nativeEvent, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::mouseDoubleClickEvent(QMouseEvent * event)
{
    // try to convert the interactor style to the default PIPER mouse interactor
    piper::vtkMousePickStylePIPER *style = piper::vtkMousePickStylePIPER::SafeDownCast(this->m_win->GetInteractor()->GetInteractorStyle());
    // in case of success, notify the interactor that the following event is a double click. in case of other interactor...well, its up to the author of that interactor
    if (style != NULL)
        style->SetIsDoubleClick();
    m_win->GetInteractor()->SetSize(this->width(), this->height());
    QMouseEvent nativeEvent = openGLToNative(*event);
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::wheelEvent(QWheelEvent *event)
{
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}


void QVTKFrameBufferObjectItem::keyPressEvent(QKeyEvent* event)
{
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::keyReleaseEvent(QKeyEvent* event)
{
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}
void QVTKFrameBufferObjectItem::focusInEvent(QFocusEvent * event)
{
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}

void QVTKFrameBufferObjectItem::focusOutEvent(QFocusEvent * event)
{
    m_irenAdapter->ProcessEvent(event, this->m_win->GetInteractor());
}

#pragma endregion //QVTKFrameBufferObjectItem
