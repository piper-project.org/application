/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ChildScaling.h"

#include "common/logging.h"
#include "common/OctaveProcess.h"
#include "common/Context.h"
#include "common/helper.h"


namespace piper {


    bool ChildScaling::compute(piper::OctaveProcess* op, double age, double targetAge, QString targetfile, QString inputCPFile, QString outputCPFile)
    {
        ////addXMLPacakage
        //op->setScriptPath("xerces-2_11_0/addXMLPackage.m");
        //QString jar1 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xercesImpl.jar");
        //QString jar2 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xml-apis.jar");
        //op->setScriptArguments(QStringList() << jar1 << jar2);
        //pInfo() << START << "addXMLPackage";
        //try {
        //    op->executeScript();
        //}
        //catch (std::exception const& e) {
        //    pCritical() << "addXMLPackage: " << e.what();
        //}
        //pInfo() << DONE;


        QString jar1 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xercesImpl.jar");
        QString jar2 = piper::octaveScriptDirectoryPath() + QString::fromStdString("/xerces-2_11_0/xml-apis.jar");

        op->setScriptPath("ChildScaling/ChildScaling.m");
        op->setScriptArguments(QStringList() << QString::number(age) << QString::number(targetAge) << targetfile << inputCPFile << outputCPFile << jar1 << jar2);
        pInfo() << START << "Generate Target Points for Child Scaling";
        try {
            op->executeScript();
        }
        catch (std::exception const& e) {
            pCritical() << "Generate Target Points for Child Scaling failed: " << e.what();
            return false;
        }
        pInfo() << DONE;
        return true;
    }

    bool ChildScaling::compute(double age, double targetAge, QString targetfile, QString inputCPFile, QString outputCPFile) {
        return compute(Context::instance().octaveProcessPtr(), age, targetAge, targetfile, inputCPFile, outputCPFile);
    }

}
