/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <Python.h>

#include "helper.h"

#include <boost/filesystem.hpp>

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>

#include "logging.h"

namespace piper {

std::string tempDirectoryPath(std::string const& module, bool doCreateNotExisting)
{
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper-"+QString::number(QCoreApplication::applicationPid()).toStdString();
    if (!module.empty())
        tmp /= module;
    if (doCreateNotExisting && !boost::filesystem::exists(tmp))
        if (!boost::filesystem::create_directories(tmp))
            pCritical() << "Temporary path could not be created: " << tmp.string();
    return tmp.string();
}

QString octaveScriptDirectoryPath()
{
    return QCoreApplication::applicationDirPath()+"/../share/octave";
}

QString qtHelpFilePath()
{
    return QCoreApplication::applicationDirPath()+"/../share/doc/piper.qhc";
}

QString shareFolderPath()
{
    return QCoreApplication::applicationDirPath()+"/../share";
}

int runPythonScript(QString scriptPath, QStringList args)
{
    QString intermediate_script_file = shareFolderPath() + "/python/traceback_to_piper.py";

    QString currentPath = QDir::currentPath();
    QString scriptWorkingDir = QFileInfo(scriptPath).absoluteDir().absolutePath();
    if (!QDir::setCurrent(scriptWorkingDir))
        pWarning() << "Working directory could not be changed to :" << scriptWorkingDir;

    char**argv = new char*[args.size()+2];
    argv[0] = new char[intermediate_script_file.size()+1];
    strcpy( argv[0], intermediate_script_file.toStdString().c_str() );
    argv[1] = new char[scriptPath.size()+1];
    strcpy( argv[1], scriptPath.toStdString().c_str() );
    for(int i=0; i<args.size() ; ++i ) {
        argv[i+2] = new char[args[i].size()+1];
        strcpy( argv[i+2], args[i].toStdString().c_str() );
    }
    PySys_SetArgvEx(args.size()+2, argv, 0);

    PyObject* pyFileObject = PyFile_FromString(intermediate_script_file.toUtf8().data(), const_cast<char*>("r"));
    int errorCode = PyRun_SimpleFileExFlags(PyFile_AsFile(pyFileObject), intermediate_script_file.toUtf8().constData(), true, nullptr);
    QDir::setCurrent(currentPath);
    for (size_t i = 0; i<args.size() + 2; ++i)
    {
        delete[] argv[i];
    }
    delete[] argv;
    return errorCode;
}

}
