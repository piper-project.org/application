/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef VTKDISPLAY_PIPER_H
#define VTKDISPLAY_PIPER_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif


#include <string>
#include "VtkMouseInteract.h"

#include "QVTKFramebufferObjectItem.h"

#include <vtkCamera.h>
#include <vtkScalarsToColors.h>
#include <vtkPlane.h>

#include <QFutureWatcher>

namespace piper {


    const std::string ELEM_COLOR_INDEX_ARRAY = "elemColorIndex";

	// an enum for various display modes - keep it bitwise so that more modes can be combined
    enum class PIPERCOMMON_EXPORT DISPLAY_MODE { NONE = 0, FULL_MESH = 1, ENTITIES = 2, LANDMARKS = 4,
        GENERICMETADATA = 8, GEBOD = 16, POINTS = 32, AXES = 64, ENVIRONMENT = 128, HIGHLIGHTERS = 256, SELECTION_HIGHLIGHTERS = 512, ALL = 0x7FFFFFFF };

    PIPERCOMMON_EXPORT inline DISPLAY_MODE operator|(DISPLAY_MODE a, DISPLAY_MODE b)
	{
		return static_cast<DISPLAY_MODE>(static_cast<int>(a) | static_cast<int>(b));
	}

    PIPERCOMMON_EXPORT inline unsigned int operator|(unsigned int a, DISPLAY_MODE b)
    {
        return static_cast<unsigned int>(static_cast<int>(a) | static_cast<int>(b));
    }

    PIPERCOMMON_EXPORT inline unsigned int operator^(unsigned int a, DISPLAY_MODE b)
    {
        return static_cast<unsigned int>(static_cast<int>(a) ^ static_cast<int>(b));
    }
    
    PIPERCOMMON_EXPORT inline DISPLAY_MODE operator&(DISPLAY_MODE a, DISPLAY_MODE b)
	{
		return static_cast<DISPLAY_MODE>(static_cast<int>(a)& static_cast<int>(b));
	}

    PIPERCOMMON_EXPORT inline DISPLAY_MODE operator&(unsigned int a, DISPLAY_MODE b)
    {
        return static_cast<DISPLAY_MODE>(static_cast<int>(a)& static_cast<int>(b));
    }

    PIPERCOMMON_EXPORT inline bool operator==(unsigned int a, DISPLAY_MODE b)
	{
		return static_cast<int>(a) == static_cast<int>(b);
	}

    PIPERCOMMON_EXPORT inline bool operator!=(unsigned int a, DISPLAY_MODE b)
    {
        return static_cast<int>(a) != static_cast<int>(b);
    }
    
  
	/**
	!!! READ HOW TO INCORPORATE VTKDISPLAY IN YOUR MODULE !!!
	A component of type 'VtkRenderWindow' has to be defined in a qml file in order for VtkDisplay to work under the QT context.
	Then it has to be bound to the frameBuffer property of vtkDisplay. If the display is defined directly in the qml, this binding can be defined there, 
	otherwise if the display is created from c++, the binding has to be created by calling

	displayName.frameBuffer = Qt.binding(function(){ return idOfTheRenderWindow })

	or similar somewhere in the qml, ideally in the onCompleted event of the VtkRenderWindow called "idOfTheRenderWindow".
	The displayName has to be made available on the qml side by using the vtkDisplay constructor that takes one string as a parameter 
	or explicitly calling afterwards the RegisterToQmlContextAs method.
	When the VtkRenderWindow is completed, it should also call displayName.initialize().
	*/

	/** @brief 	A class for visualization of multiple named vtkDataSet models.
	*
	* @author Thomas Lemaire @date 2014, Tomas Janak @date 2016, Sukhraj Singh @date 2016
	*/
    class PIPERCOMMON_EXPORT VtkDisplay : public QObject
	{
		Q_OBJECT
        Q_PROPERTY(QVTKFrameBufferObjectItem *frameBuffer READ getFrameBuffer WRITE setFrameBuffer NOTIFY frameBufferChanged)
        Q_PROPERTY(bool optionVisualizeBoxes READ optionVisualizeBoxes WRITE setOptionVisualizeBoxes NOTIFY optionVisualizeBoxesChanged)
        Q_PROPERTY(bool useParallelProjection READ GetUseParallelProjection WRITE SetUseParallelProjection NOTIFY useParallelProjectionChanged)
        Q_PROPERTY(bool renderEdges READ GetRenderEdges WRITE SetRenderEdges NOTIFY renderEdgesChanged)
        Q_PROPERTY(bool renderNormals READ GetRenderNormals WRITE SetRenderNormals NOTIFY renderNormalsChanged)
        Q_PROPERTY(int blankLevel READ GetBlankLevel NOTIFY blankLevelChanged)

	public:
		VtkDisplay();	
		/// <summary>
		/// Initializes a new instance of the <see cref="VtkDisplay"/> class.
		/// Use this constructor if you are initializing the display from within c++ and your qml file does not have a VtkDisplay explicitly created in it -
		/// this display will be added as a context property to the root Qt context so that you can access it in your qml file using this name.
		/// </summary>
		/// <param name="displayNameInQML">The display name to be used in qml files.</param>
		~VtkDisplay();

        /// <summary>
        /// Creates an actor for the model and add it to the current render window.
        /// </summary>
        /// <param name="model">The model.
        /// vtkPointSet includes vtkPolyData, vtkUnstructuredGrid and other common geometry representation classes.</param>
        /// <param name="name">Name of the model that you want to be visible in the GUI. Uniqueness is not checked here.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="activeIn">Sets in which display mode should this point set be rendered. You can use bitwise operations to select more modes.
        /// The mesh will be rendered only when the display is set to the selected mode(s), otherwise it won't.</param>
        /// <param name="isPickable">If set to <c>true</c>, it will be possible to select this data, otherwise it won't be.</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor. Useful when
        /// creating new scene, but you might want to turn it off when adding some small parts (e.g. highlighting primitives).</param>
        void AddVtkPointSet(vtkPointSet *model, std::string name, bool persistent, DISPLAY_MODE activeIn = DISPLAY_MODE::ALL, bool isPickable = true, bool resetCamera = true);
	
        /// <summary>
        /// Creates an actor for the model and add it to the current render window.
        /// </summary>
        /// <param name="model">The model.
        /// vtkPointSet includes vtkPolyData, vtkUnstructuredGrid and other common geometry representation classes.</param>
        /// <param name="lut">The look up table for colors. The colors must be already set as a scalar array named "elemColorIndex" (ELEM_COLOR_INDEX_ARRAY)
        /// in the mesh. The values of the scalar array are indices into the lut, which contains different colors to use for coloring the elements.</param>
        /// <param name="name">Name of the model that you want to be visible in the GUI. Uniqueness is not checked here.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="activeIn">Sets in which display mode should this point set be rendered. You can use bitwise operations to select more modes.
        /// The mesh will be rendered only when the display is set to the selected mode(s), otherwise it won't.</param>
        /// <param name="isPickable">If set to <c>true</c>, it will be possible to select this data, otherwise it won't be.</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor. Useful when
        /// creating new scene, but you might want to turn it off when adding some small parts (e.g. highlighting primitives).</param>
        void AddVtkPointSet(vtkPointSet *model, vtkLookupTable *lut, std::string name, bool persistent, DISPLAY_MODE activeIn = DISPLAY_MODE::ALL, bool isPickable = true, bool resetCamera = true);
        
        /// <summary>
        /// Creates an actor for the model and add it to the current display.
        /// </summary>
        /// <param name="model">The model.
        /// vtkPointSet includes vtkPolyData, vtkUnstructuredGrid and other common geometry representation classes.</param>
        /// <param name="name">Name of the model that you want to be visible in the GUI. Uniqueness is not checked here.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="activeIn">Sets in which display mode should this point set be rendered. You can use bitwise operations to select more modes.
        /// The mesh will be rendered only when the display is set to the selected mode(s), otherwise it won't.</param>
        /// <param name="isPickable">If set to <c>true</c>, it will be possible to select this data, otherwise it won't be.</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor. Useful when
        /// creating new scene, but you might want to turn it off when adding some small parts (e.g. highlighting primitives).</param>
        void AddVtkPointSet(vtkPointSet *model, std::string name, bool persistent, unsigned int activeIn, bool isPickable = true, bool resetCamera = true);

        /// <summary>
        /// Creates an actor for only the points of the specified dataset and add it to the current display.
        /// The points will be visualized using glyphs for each point - a sphere with a given radius.
        /// </summary>
        /// <param name="model">The model.
        /// vtkPointSet includes vtkPolyData, vtkUnstructuredGrid and other common geometry representation classes.</param>
        /// <param name="name">Name of the model that you want to be visible in the GUI.
        /// Uniqueness is not checked here, but the name has to be unique, otherwise it will overwrite other actors.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="radius">The radius of the sphere by which to represent the points.</param>
        /// <param name="maskArrayName">The name of the scalar pointdata array to use as a mask. It must be a vtkBitArray with a value for each point,
        /// all points with value 0 will not be highlighted. Default empty - all points are highlighted.</param>
        /// <param name="activeIn">Sets in which display mode should this point set be rendered. You can use bitwise operations to select more modes.
        /// The mesh will be rendered only when the display is set to the selected mode(s), otherwise it won't.</param>
        /// <param name="isPickable">If set to <c>true</c>, it will be possible to select this data, otherwise it won't be.
        /// Note that glyphed points are only possible to pick using the FREE_POINTS picking style - it will not be possible to pick this actor using node picker or entity picker!</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor. Useful when
        /// creating new scene, but you might want to turn it off when adding some small parts (e.g. highlighting primitives).</param>
        /// <param name="parentContainingLandmarkInfo">If you specify a FEModelVTK here, the group of points will be treated as points defining landmarks of the specified FEModel.
        /// This is used mainly for selection - groups of points with non-NULL parentIncludingLandmarkInfo will use the parent to find out other points 
        /// that define the same landmark as the selected point in order to highlight them. 
        /// !!! If you specify this, the "model" parameter will be ignored, instead the return value of getVTKMesh() of this FEModelVTK will be used
        /// - otherwise it does nto make sense !!! 
        /// If you are adding landmarks directly (one points = position of one landmark), do not specify this.</param>
        void AddGroupOfPoints(vtkDataSet *model, std::string name, bool persistent, double radius, std::string maskArrayName = "", DISPLAY_MODE activeIn = DISPLAY_MODE::POINTS, bool isPickable = true, bool resetCamera = true, hbm::FEModelVTK *parentContainingLandmarkInfo = NULL);

        /// <summary>
        /// Adds the specified actor to the current display.
        /// </summary>
        /// <param name="actor">The actor to add.</param>
        /// <param name="name">Name of the model that you want to be visible in the GUI. Uniqueness is not checked here.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="activeIn">Sets in which display mode should this actor be rendered. You can use bitwise operations to select more modes.
        /// The mesh will be rendered only when the display is set to the selected mode(s), otherwise it won't.</param>
        /// <param name="isPickable">If set to <c>true</c>, it will be possible to select this data, otherwise it won't be.</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor. Useful when
        /// creating new scene, but you might want to turn it off when adding some small parts (e.g. highlighting primitives).</param>
        void AddActor(vtkSmartPointer<VtkDisplayActor> actor, std::string name, bool persistent, unsigned int activeIn, bool isPickable = true, bool resetCamera = false);

        void AddActor(vtkSmartPointer<VtkDisplayActor> actor, std::string name, bool persistent, DISPLAY_MODE activeIn, bool isPickable = true, bool resetCamera = false)
        {
            AddActor(actor, name, persistent, (unsigned int)activeIn, isPickable, resetCamera);
        }
        
        /// <summary>
        /// Adds an axes actor to the display. This actor is held in a data structure separate from the other regular axes.
        /// It cannot be be picked or otherwise selected and majority of VtkDisplay's methods that do not have "Axes" in it's name will not affect it.
        /// The DISPLAY_MODE::AXES must be active to render the axes actors.
        /// </summary>
        /// <param name="actor">The axes actor.</param>
        /// <param name="name">The name of the actor.</param>
        /// <param name="persistent">If set to <c>true</c>, this object will persist in the display until explicitly removed through RemoveActor.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="resetCamera">If set to <c>true</c>, the camera's position will be reset after adding this actor.</param>
        void AddAxesActor(vtkSmartPointer<vtkAxesActor> actor, std::string name, bool persistent, bool resetCamera = false);
	
		/// <summary>
		/// Sets the diffuse color of the object and opacity.
		/// </summary>
		/// <param name="name">The name of the object.</param>
		/// <param name="r">The r channel of the color in the range of 0 to 1.</param>
		/// <param name="g">The g channel of the color in the range of 0 to 1.</param>
		/// <param name="b">The b channel of the color in the range of 0 to 1.</param>
		/// <param name="a">The opacity channel of the color in the range of 0 (fully transparent) to 1 (opaque). Default opaque.</param>
		void SetObjectColor(std::string name, float r, float g, float b, float a = 1);
	
		/// <summary>
		/// Sets the diffuse color of the object and opacity.
		/// </summary>
		/// <param name="name">The name of the object.</param>
		/// <param name="color">The color in the UINT 4x8-bit ARGB format.</param>
		void SetObjectColor(std::string name, unsigned int color)
		{
			float r, g, b, a;
			colorFormatTransferUINTtoFLOAT(color, &a, &r, &g, &b);
			SetObjectColor(name, r, g, b, a);
		}

		/// <summary>
		/// Sets the radius of th object.
		/// </summary>
		/// <param name="name">The name of the object.</param>
		/// <param name="radius">The radius of the object.</param>
		void SetObjectRadius(std::string name, float radius);

        /// <summary>
        /// Sets the diffuse color of the point highlighters (spheres rendered around points of the dataset)and opacity for the specified actor.
        /// Note - this changes color only for the "pointHighlighters", not the "selectedPointsHighlighters".
        /// </summary>
        /// <param name="name">The name of the actor. If it does not exists or it does not have point highlighters, nothing will happen.</param>
        /// <param name="r">The r channel of the color in the range of 0 to 1.</param>
        /// <param name="g">The g channel of the color in the range of 0 to 1.</param>
        /// <param name="b">The b channel of the color in the range of 0 to 1.</param>
        /// <param name="a">The opacity channel of the color in the range of 0 (fully transparent) to 1 (opaque). Default opaque.</param>
        void SetActorPointHighlightsColor(std::string name, float r, float g, float b, float a = 1);

        /// <summary>
        /// Sets the diffuse color of the point highlighters (spheres rendered around points of the dataset)and opacity for the specified actor.
        /// Note - this changes color only for the "pointHighlighters", not the "selectedPointsHighlighters".
        /// </summary>
        /// <param name="name">The name of the actor. If it does not exists or it does not have point highlighters, nothing will happen.</param>
        /// <param name="color">The color in the UINT 4x8-bit format.</param>
        void SetActorPointHighlightsColor(std::string name, unsigned int color)
        {
            float r, g, b, a;
            colorFormatTransferUINTtoFLOAT(color, &a, &r, &g, &b);
            SetActorPointHighlightsColor(name, r, g, b, a);
        }
        
        /// <summary>
        /// Gets the opacity of the specified object.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <returns>The value of the opacity - 1 totally opaque, 0 transparent.</returns>
        double GetObjectOpacity(std::string name);

        /// <summary>
        /// Sets the opacity of the specified object, keeping the current color.
        /// </summary>
        /// <param name="name">The name of the object.</param>
        /// <param name="value">The value of the opacity - 1 totally opaque, 0 transparent.</param>
        void SetObjectOpacity(std::string name, float value);
        
        /// <summary>
        /// Sets the same opacity to all objects that are registered under the specified display mode.
        /// Is performed for all actors in the display mode, regardless of whether the mode is active or the actor is visible.
        /// </summary>
        /// <param name="mode">The display mode for which to change the opacity.</param>
        /// <param name="value">The value between 0 and 1 - 0 = transparent, 1 = solid.</param>
        void SetDisplayModeOpacity(unsigned int mode, float value);

        /// <summary>
        /// Sets the same opacity to all objects that are registered under the specified display mode.
        /// Is performed for all actors in the display mode, regardless of whether the mode is active or the actor is visible.
        /// </summary>
        /// <param name="mode">The display mode for which to change the opacity.</param>
        /// <param name="value">The value between 0 and 1 - 0 = transparent, 1 = solid.</param>
        void SetDisplayModeOpacity(DISPLAY_MODE mode, float value) { SetDisplayModeOpacity((unsigned int)mode, value); }

        /// <summary>
        /// Set the width of a line,contour,frames 
        /// Note that the default VTK width is 1.0
        /// </summary>
        /// <param name="name">The name of the actor. If not found in this VtkDisplay, nothing will happen.</param>
        /// <param name="width"> Width in float,expressed in screen units. The default is 1.0
        /// </param>
        void SetObjectWidth(std::string name, float width);
                
        /// <summary>
        /// Marks the specified actor as selected. Then re-renders the display so that the actor is properly highlighted.
        /// </summary>
        /// <param name="actor">The actor to be selected. In case this actor is not in the scene or is already selected, nothing happens. 
        /// If the actor is NULL, nothing happens.</param>
        /// <param name="append">Defines how are the other selected actors treated when adding this actor.
        /// If set to APPEND_ACTOR (default), only this actor will be selected, all previously selected actors will be deselected.
        /// If set to APPEND_ALL, all previously selected actors will remain selected.
        /// If set to DESELECT_ACTOR, only this actor will be deselected.
        /// If set to DESELECT_ALL, all actors will be deselected.</param>
        /// <param name="forceSelect">If set to true, the actor will always end up selected, regardless of whether it currently is selected or not. 
        /// If set to false, it will be selected only if it is not selected and deselected if it is currently selected (the selection will toggle).</param>
        /// </param>
        void SelectActor(vtkSmartPointer<VtkDisplayActor> actor, PICK_APPENDING_STYLE append = PICK_APPENDING_STYLE::APPEND_ACTOR, bool forceSelect = false);

        /// <summary>
        /// Selects the actor that is beneath the specified pixel. Then re-renders the display so that the actor is properly highlighted.
        /// If there is none under the specified pixel and append style is not APPEND_ALL, all actors will be deselected.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of a point on a cell that belongs to the actor.</param>
        /// <param name="actor">The actor to be deselected. In case this actor is not in the scene or is not selected, nothing happens.
        /// If no actor is specified, deselects all actors.</param>
        void SelectActor(int pickPosition[2], PICK_APPENDING_STYLE append);

        
        /// <summary>
        /// Selects all active actors, i.e. only those that are being rendered.
        /// </summary>
        void SelectAllActors();
        
        /// <summary>
        /// Marks the specified actor as not selected. Then re-renders the display so that the actor is properly highlighted.
        /// </summary>
        /// <param name="actor">The actor to be deselected. In case this actor is not in the scene or is not selected, nothing happens.</param>
        void DeselectActor(vtkSmartPointer<VtkDisplayActor> actor) { SelectActor(actor, PICK_APPENDING_STYLE::DESELECT_ACTOR); }
                
        /// <summary>
        /// Deselects all active actors, i.e. only those that are being rendered.
        /// </summary>
        void DeselectAllActors();

        /// <summary>
        /// Selects the cell that has been rendered on a given screen space coordinates.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of a point on the cell.</param>
        /// <param name="append">Controls the way previously selected cells are handled - see documentation for PICK_APPENDING_STYLE for details.</param>
        /// <param name="forceValue">If set to <c>true</c>, the value specified in the next argument will be set to a cell if it is picked no matter if it is currently selected or not.</param>
        /// <param name="value">If set to <c>true</c>, cell will be marked as selected, otherwise as not selected. This is ignored if <c>forceValue</c> is set to <c>false</c>.</param>
        void SelectCell(int pickPosition[2], PICK_APPENDING_STYLE append, bool forceValue = false, bool value = true);
        
        /// <summary>
        /// Selects all that is within a specified box.
        /// </summary>
        /// <param name="boxOrigin">The box origin.</param>
        /// <param name="boxAxes">The box axes (vectors originating in the origin and defining the three box axes).</param>
        /// <param name="append">Controls the way previously selected primitives are handled. If set to DESELECT_ACTOR, all inside the box will be deselected
        /// instead of selected, but things outside this box will be left alone. If set to APPEND_ACTOR, everything will be deselected 
        /// and all box cleared, only this box will remain. If set to APPEND_ALL, all boxes will be left intact.</param>
        /// <param name="target">What should be selected - currently supports only elements and nodes.</param>
        /// <param name="createBoxVisualization">If set to <c>true</c>, new actor for the box' geometry will be created and added to the scene, 
        /// display will be refreshed afterwards.</param>
        void SelectByBox(double boxOrigin[3], double boxAxes[3][3], PICK_APPENDING_STYLE append,
            piper::hbm::SELECTION_TARGET target, bool createBoxVisualization);

        /// <summary>
        /// Removes all selection boxes from the scene.
        /// </summary>
        Q_INVOKABLE void ClearSelectionBoxes();

        /// <summary>
        /// Creates a bounding box around all selected cells of all visible actors in the scene,
        /// then subdivides it in order to create a box around groups of selected cells that are significantly far away from other groups,
        /// i.e. creates the boxes around clusters of cells.
        /// </summary>
        Q_INVOKABLE void BuildBoxesBySelCells();

        /// <summary>
        /// Creates a bounding box around all selected points of all visible actors in the scene,
        /// then subdivides it in order to create a box around groups of selected points that are significantly far away from other groups,
        /// i.e. creates the boxes around clusters of points.
        /// </summary>
        Q_INVOKABLE void BuildBoxesBySelPoints();

        /// <summary>
        /// Returns the list of the selected actors
        /// </summary>
        Q_INVOKABLE std ::vector < std::string > GetSelectedActorNames();

		/// <summary>
		/// Returns the list of the selected actors
		/// </summary>
		Q_INVOKABLE QString getSelectedActorNameAsQString();
        
        /// <summary>
        /// Turns visualization of selection boxes on or off. Also changes the starting visibility of any boxes added in the future to the specified value.
        /// </summary>
        /// <param name="visualize">If set to <c>true</c>, boxes will be visualized, otherwise they will be hidden.</param>
        Q_INVOKABLE void SetBoxesVisible(bool visualize);

        /// <summary>
        /// Selects the next selection box managed by this display's VtkDisplayHighlighters. The selected box will be rendered in the secondary picking color.
        /// </summary>
        /// <returns>The ID of the selected box.</returns>
        Q_INVOKABLE int SelectNextSelectionBox();
        
        /// <summary>
        /// Selects the previous selection box managed by this display's VtkDisplayHighlighters. The selected box will be rendered in the secondary picking color.
        /// </summary>
        /// <returns>The ID of the selected box.</returns>
        Q_INVOKABLE int SelectPrevSelectionBox();

        /// <summary>
        /// Removes the selected box. Does not deselect what was inside. Which box is selected is handled by the VtkDisplayHighlighters belonging to this display.
        /// </summary>
        Q_INVOKABLE void RemoveSelectedBox();

        /// <summary>
        /// Deselects all that is inside the selected selection box. Which box is selected is handled by the VtkDisplayHighlighters belonging to this display.
        /// The type of selection has to be set using the SetPickingType / SelectionTargetChanged.
        /// </summary>
        /// <param name="deselect">If set to <c>true</c>, everything inside the box will be deselected instead of selected.</param>
        /// <returns>Returns <c>true</c> if the selection was succesful, <c>false</c> in case the selected box is no longer valid.</returns>
        Q_INVOKABLE bool SelectBySelectedBox(bool deselect);
        
        /// <summary>
        /// For each active actor in the scene, selects all elements that have at least one node selected.
        /// </summary>
        /// <param name="deselect">If set to <c>true</c>, the nodes will be deselected instead.</param>
        Q_INVOKABLE void SelectNodesByElements(bool deselect);

        /// <summary>
        /// For each active actor in the scene, selects all nodes that belong to at least one selected element.
        /// </summary>
        Q_INVOKABLE void SelectElementsByNodes();

		/// <summary>
		/// For each active actor in the scene, selects all entities that belong to at least one selected element.
		/// </summary>
		Q_INVOKABLE void SelectElementsByEntities();

		/// <summary>
		/// Creates a geometry for the specified plane and visualizes it.
		/// The plane is added in the SELECTION_HIGHLIGHTERS group, as non-persistent.
		/// The size of the plane is determined by the size of the bounding box of active actors.
		/// </summary>
		/// <param name="planeRefPoint">Reference point through which the plane passes.</param>
		/// <param name="planeNormal">Normal of the plane.</param>
		/// <param name="actorName">Name to use for the actor. If an actor with this name already exists, it will be rewritten!</param>
		Q_INVOKABLE void VisualizePlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
			double planeNormalX, double planeNormalY, double planeNormalZ, QString actorName);

		/// <summary>
		/// Selects elements of all active pickable actors that are on one side of a provided plane.
		/// </summary>
		/// <param name="planeRefPoint">Reference point through which the plane passes.</param>
		/// <param name="planeNormal">Normal of the plane.</param>
		/// <param name="positiveDirection">If set to <c>true</c>, selects elements that are in the side of the plane in direction of the normal,
		/// otherwise the ones on the other side are selected.</param>
		Q_INVOKABLE void SelectElementsByPlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
			double planeNormalX, double planeNormalY, double planeNormalZ, bool positiveDirection);
        
		/// <summary>
		/// Selects nodes of all active pickable actors that are on one side of a provided plane.
		/// </summary>
		/// <param name="planeRefPoint">Reference point through which the plane passes.</param>
		/// <param name="planeNormal">Normal of the plane.</param>
		/// <param name="positiveDirection">If set to <c>true</c>, selects nodes that are in the side of the plane in direction of the normal,
		/// otherwise the ones on the other side are selected.</param>
		Q_INVOKABLE void SelectNodesByPlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
			double planeNormalX, double planeNormalY, double planeNormalZ, bool positiveDirection);
		
        /// <summary>
        /// For each active actor in the scene, selects all nodes that are not selected and deselect all that are.
        /// </summary>
        Q_INVOKABLE void InvertMeshNodesSelection();

        /// <summary>
        /// For each active actor in the scene, selects all elements that are not selected and deselect all that are.
        /// </summary>
        Q_INVOKABLE void InvertCellsSelection();
                       
        /// <summary>
        /// Selects the selection box with the specified actor name that is managed by this display's highlights. Nothing happens if it there is no box with this ID.
        /// </summary>
        /// <param name="boxID">TThe name of the box' actor.</param>
        void SelectSelectionBox(std::string boxActorName);

        /// <summary>
        /// Deselects all cells in all actors.
        /// </summary>
        void DeselectAllCells();
        
        /// <summary>
        /// Increases or decreases the "hit pixel count" for a cell to which the picked pixel belongs.
        /// The counter is stored in the dataset's celldata array HIT_PIXEL_COUNT. If the pixel count changes to/from 0, the cell is deselected / selected.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of a point on the cell.</param>
        /// <param name="hitOrLeft">If set to <c>true</c>, it marks that the pixel was picked, if set to <c>false</c> it marks that the mouse / selection rectangle has left the pixel.</param>
        /// <param name="append">Controls the way previously selected points are handled. Only two values have meaning here - APPEND_ALL and DESELECT_ALL.
        /// If APPEND_ALL is specified, the hit cell will be selected once it's hit pixel count is above zero, if DESELECT_ALL, cell will be deselected once hit pixel count is above zero.
        /// If something else is specified, it will be treated as DESELECT_ALL.</param>
        void SelectCellPixel(int pickPosition[2], bool hitOrLeft, PICK_APPENDING_STYLE append);
        
        /// <summary>
        /// Removes the cell pixel hit arrays for each actor (see SelectCellPixel).
        /// </summary>
        void RemoveCellPixelHitArrays();
        
        /// <summary>
        /// Computes the coordinates of a point that is beneath the picked screen coordinates.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of the point.</param>
        /// <param name="returnCoordinates">Upon returning, this will contain the coordinates the picked point. This 
        /// is effectively an intersection of the displayed mesh and the ray casted from the screen through the specified pickPosition.</param>
        bool GetPickedPointOnMesh(int pickPosition[2], double returnCoordinates[3]);

        /// <summary>
        /// Selects a node of some mesh that has been rendered near a given screen space coordinates.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of the point.</param>
        /// <param name="append">Controls the way previously selected points are handled - see documentation for PICK_APPENDING_STYLE for details.</param>
        void SelectMeshNode(int pickPosition[2], PICK_APPENDING_STYLE append);
        
        /// <summary>
        /// Gets the names of actors that have some nodes selected.
        /// </summary>
        /// <returns>A list of actors that have at least one node selected.</returns>
        std::vector<std::string> GetActorsWithSelectedNodes();
        
        /// <summary>
        /// Selects a point that is represented as a glyph (using vtkGlyph3DMapper) based on a dataset that is an input to this mapper.
        /// The results of the selectino are stored in the SELECTED_PRIMITIVES point array of the input dataset.
        /// If the selected actor is a ladnmark-by-points actor (it has a non-NULL FEModelVTK assigned), the FEModelVTK will be used to find other points
        /// that belong to the same landmark as the selected point and highlight them in a different color.
        /// </summary>
        /// <param name="pickPosition">The screen space coordinates of the point.</param>
        /// <param name="append">Controls the way previously selected points are handled - see documentation for PICK_APPENDING_STYLE for details.</param>
        /// <param name="pickOnlyThisName">If this string is non-empty (empty by default), the picking will be processed only if the picked actor has the specified name.</param>
        /// <returns>The ID of the picked point, -1 if nothing was picked. -1 is also returned if pickOnlyThisName is specified but the picked actor has a different name.</returns>
        int SelectGlyphedPoint(int pickPosition[2], PICK_APPENDING_STYLE append, std::string pickOnlyThisName = "");
        
        /// <summary>
        /// For all actors that are currently visible, removes selected elements from the parent dataset and creates a new actor
        /// based on this data that share the name with original actor. The original actor is removed from the actor list and is 
        /// replace with the blanked one. Reference to the original actor is kept by the new actor so that it can be reversed.
        /// </summary>
        void BlankSelectedElements();

        /// <summary>
        /// For all actors that were created by blanking, restores their parent.
        /// </summary>
        /// <param name="UnblankAll">If set to <c>true</c>, restores the very original state of the actor.
        /// If set to <c>false</c>, restores only to the state before the last blank.</param>
        void Unblank(bool unblankAll);

		/// <summary>
        /// Sets everything that is neccessary for a given model for its cells to be colored according to a specified scalar array.
		/// </summary>
        /// <param name="scalarArrayName">Name of the scalar array to use for coloring the . 
        /// If the actor's mapper's input does not have <c>scalarArrayName</c> array, nothing will happen.</param>
        /// <param name="usePointData">If set to <c>true</c>, the <c>scalarArrayName</c> array must be a pointData array,
        /// otherwise it must be a cellData array. Basically this defines whether the scalars are defined per element or per point.</param>
		/// <param name="actorName">The name of the actor. If a model with this name is not found in this VtkDisplay, nothing will happen.
        /// If no name is specified, coloring will be turned on for every model in the scene - i.e. only for active actors, not all registered actors.</param>
		/// <param name="lut">A binary LUT with the color mapping you want to use.
		/// If you do not provide one, a default one will be created, using the colorOfSelection to render selected cells and the original color of the model to 
        /// render the rest. </param>
        /// <param name="updateBlanked">If set to <c>true</c>, the scalar arrays of actors that were created by blanking
        /// will be first updated according to values in the original dataset.</param>
        void HighlightElementsByScalars(std::string scalarArrayName, bool usePointData, std::string actorName = "",
            vtkSmartPointer<vtkScalarsToColors> lut = 0, bool updateBlanked = true);
       
        /// <summary>
        /// Creates a small sphere around each point of the actor's mapper's input data and set it for rendering.
        /// </summary>
        /// <param name="name">The name of the model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        /// <param name="persistent">If set to <c>true</c>, this points will remain highlighted in the display until explicitly turned off by HighlightPointsOff.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="radius">The size of the sphere for each point. Note that if it is changed from the default value, it will be changed for ALL highlighted points
        /// (i.e. all point highlighters must have the same size).</param>
        /// <param name="refreshScreen">If set to <c>true</c>, the display will be refreshed after adding the actor.</param>
        /// <param name="maskArrayName">The name of the scalar pointdata array to use as a mask. It must be a vtkBitArray with a value for each point,
        /// all points with value 0 will not be highlighted. Default empty - all points are highlighted. The name of the mask array, if present,
        /// will be used to name the actor with the highlighters to differentiate between differently masked point sets.</param>
        /// <param name="dummySourceData">If set to <c>false</c>, the input actor's dataset will be used a input for the newly created actor that represents the point highlighter.
        /// If set to <c>true</c>, a dummy dataset that has the same points as the input actor's points will be created and set as a source for the highlighter actor. This can be useful
        /// if you need to assign some scalar array as active to the highlighters, but don't want to affect the active scalars of the original dataset.
        /// The maskArryaName from the original dataset will be also pointed to by the dummy data set.</param>
        std::string HighlightPoints(std::string name, bool persistent, double radius, bool refreshScreen = true, std::string maskArrayName = "", bool dummySourceData = false);

        /// <summary>
        /// Turns off highlighting of points for a given actor off (invisible).
        /// </summary>
        /// <param name="name">The name of the parent model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        void HighlightPointsOff(std::string name);

        /// <summary>
        /// Creates a small sphere around each selected point of the actor's mapper's input data, according to the SELECTED_PRIMITIVES array, and set it for rendering.
        /// </summary>
        /// <param name="name">The name of the model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        /// <param name="radius">The size of the sphere for each point. Note that if it is changed from the default value, it will be changed for ALL highlighted points
        /// (i.e. all point highlighters must have the same size).</param>
        /// <param name="blockRefreshing">If set to <c>true</c>, the display will not be refreshed after adding the point highlighters.
        /// This significantly improves performance if you are turning highlighting on for more than one actor,
        /// but do not forget to call Refresh explicitly once you are done, otherwise the point highlighters will not be added to the scene.</param>
        void HighlightSelectedPoints(std::string name, double radius, bool blockRefreshing = false);
        
        /// <summary>
        /// Turns off highlighting of points for a given actor off (invisible).
        /// </summary>
        /// <param name="name">The name of the parent model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        void HighlightSelectedPointsOff(std::string name);

        /// <summary>
        /// Creates a small sphere around a given point and set it for rendering.
        /// Creates a unique actor for the point - not recommended if you want to highlight a large number of points. In such cases, consider using HighlighSelectedPoints.
        /// </summary>
        /// <param name="name">The name of the parent model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        /// <param name="persistent">If set to <c>true</c>, this point will remain highlighted in the display until explicitly turned off by HighlightPointOff.
        /// If set to <c>false</c>, this object will be removed during the RemoveNonpersistentActors - which is usually bind to some signal.</param>
        /// <param name="baseColor">Color that should be considered as the default one for the new actor.</param>
        /// <param name="pointID">The ID of the point in the parent model that should be highlighted.</param>
        void HighlightSinglePoint(std::string name, bool persistent, unsigned int baseColor, int pointID);

        /// <summary>
        /// Removes the highlighter of a given point.
        /// </summary>
        /// <param name="name">The name of the parent model. If a model with this name is not found in this VtkDisplay, nothing will happen.</param>
        /// <param name="pointID">The ID of the point in the parent model that should be not highlighted. 
        /// If -1 (the default) is specified, highlighting of all points of the named actor will be turned off.</param>
        void HighlightPointOff(std::string name, int pointID = -1);
        
        /// <summary>
        /// Removes an actor from the VtkDisplay completely - this does not just hide it, it removes it!.
        /// Do not forget to call Refresh after you are done with removing to make the changes visible.
        /// </summary>
        /// <param name="name">The name of the actor. If actor with the name is non-existent, nothing will happen.</param>
        /// <param name="removeDependentActors">If set to <c>true</c>, dependent actors of this actor,
        /// such as point highlighters, normal highlighters etc., will also be removed.</param>
        void RemoveActor(std::string const& name, bool removeDependentActors = true);        
		Q_INVOKABLE void RemoveActor(QString name, bool removeDependentActors) { RemoveActor(name.toStdString(), removeDependentActors); }

        /// <summary>
        /// Removes all actors from the display - not just hide, it removes!.
        /// Only cleans the internal storage, does not touch the renderer - call Refresh to make the changes visible
        /// </summary>
        void RemoveAllActors();

        /// <summary>
        /// Sets a flag
        /// </summary>
        /// <param name="isInMotion">Flag describing whether the camera is currently being moved.
        /// Should be set at the beggining of any camera moving events to enhance performance, otherwise some irrelevant 
        /// calls (updating of selection data structures) might be called during the camera motion, 
        /// where it would make sense to call them only after the camera is standing still.
        /// Should be set to false after any camera update to notify the display that the motion has ended and it is needed to update the aforementioned structures.</param>
        void SetIsCameraInMotion(bool isInMotion);


		/// <summary>
		/// Sets what type of data that should be visualized. Only the specified mode will be active.
        /// If you want to only add this mode to the already active ones, use ActivateDisplayMode instead.
		/// </summary>
		/// <param name="mode">The type. You can use bitwise operations to set more modes at once.</param>
		void SetDisplayMode(DISPLAY_MODE mode);
        
        /// <summary>
        /// Activates the specified display mode - adds it to the already activated modes. If it is already activated, nothing changes.
        /// </summary>
        /// <param name="mode">The mode to activate.</param>
        void ActivateDisplayMode(DISPLAY_MODE mode) { ActivateDisplayMode((unsigned int)mode); }

        /// <summary>
        /// Activates the specified display mode - adds it to the already activated modes. If it is already activated, nothing changes.
        /// If you want only this mode to be active, you SetDisplayMode instead.
        /// </summary>
        /// <param name="mode">The mode to activate.</param>
        void ActivateDisplayMode(unsigned int mode);

        /// <summary>
        /// Deactivates the specified display mode. If it is not active, nothing will change.
        /// </summary>
        /// <param name="mode">The mode to deactivate.</param>
        void DeactivateDisplayMode(DISPLAY_MODE mode) { DeactivateDisplayMode((unsigned int)mode); }
        
        /// <summary>
        /// Determines whether the specified display mode is active.
        /// </summary>
        /// <param name="mode">The display mode.</param>
        /// <returns><c>True</c> if it is currently active, <c>false</c> otherwise.</returns>
        bool IsDisplayModeActive(DISPLAY_MODE mode) { return IsDisplayModeActive((unsigned int)mode); }

        /// <summary>
        /// Determines whether the specified display mode is active.
        /// </summary>
        /// <param name="mode">The display mode, or their combination (use bitwise operators to combine modes).</param>
        /// <returns><c>True</c> if it is currently active, <c>false</c> otherwise.</returns>
        Q_INVOKABLE bool IsDisplayModeActive(unsigned int mode);

        /// <summary>
        /// Deactivates the specified display mode. If it is not active, nothing will change.
        /// </summary>
        /// <param name="mode">The mode to deactivate.</param>
        void DeactivateDisplayMode(unsigned int mode);

        /// <summary>
        /// Sets what type of data that should be pickable/selectable. Only the specified mode will be active.
        /// If this mode is not active for visualization (see SetDisplayMode), the specified actors will obviously not be pickable.
        /// If you want to only add this mode to the already active ones, use ActivatePickableDisplayMode instead.
        /// </summary>
        /// <param name="mode">The type. You can use bitwise operations to set more modes at once.</param>
        void SetPickableDisplayMode(DISPLAY_MODE mode);

        /// <summary>
        /// Allow actors of the specified display mode to be pickable - adds it to the already activated pickable modes. If it is already activated, nothing changes.
        /// </summary>
        /// <param name="mode">The mode to activate.</param>
        void ActivatePickableDisplayMode(DISPLAY_MODE mode) { ActivatePickableDisplayMode((unsigned int)mode); }

        /// <summary>
        /// Allow actors of the specified display mode to be pickable - adds it to the already activated pickable modes. If it is already activated, nothing changes.
        /// </summary>
        /// <param name="mode">The mode to activate.</param>
        void ActivatePickableDisplayMode(unsigned int mode);

        /// <summary>
        /// Disables actors of the specified display mode to be pickable. If it is already disabled, nothing changes.
        /// </summary>
        /// <param name="mode">The mode to deactivate.</param>
        void DeactivatePickableDisplayMode(DISPLAY_MODE mode) { DeactivatePickableDisplayMode((unsigned int)mode); }

        /// <summary>
        /// Disables actors of the specified display mode to be pickable. If it is already disabled, nothing changes.
        /// </summary>
        /// <param name="mode">The mode to deactivate.</param>
        void DeactivatePickableDisplayMode(unsigned int mode);
        
        /// <summary>
        /// Makes the specified actor (un)available for picking.
        /// Note - his display mode must be pickable as well in order to make it pickeable - use ActivatePickableDisplayMode etc.
        /// </summary>
        /// <param name="name">The name of the actor.</param>
        /// <param name="pickable">If set to <c>true</c>, the actor will be pickable if it exists, if <c>false</c>, it will be made unavailable for picking.</param>
        void SetActorPickable(std::string name, bool pickable);
        
        /// <summary>
        /// Makes the highlighters of points (spheres rendered around the points) of the specified actor (un)available for picking.
        /// Note - his display mode SELECTION_HIGHLIGHTERS must be pickable as well in order to make it pickeable - use ActivatePickableDisplayMode etc.
        /// </summary>
        /// <param name="name">The name of the actor.</param>
        /// <param name="pickable">If set to <c>true</c>, the actor will be pickable if it exists, if <c>false</c>, it will be made unavailable for picking.</param>
        void SetActorPointHighlightersPickable(std::string name, bool pickable);
        
        /// <summary>
        /// Sets the specified actor visible or invisible. If it does not exist, nothing will happen. Does not refresh the display - do it yourself.
        /// Note - to change visibility of axes actors, use SetAxesVisible.
        /// </summary>
        /// <param name="name">The name of the actor.</param>
        /// <param name="visible">If set to <c>true</c>, the actor will be visible.
        /// If set to <c>false</c>, the actor will be invisible even if it is in an active DISPLAY_MODE.</param>
        void SetActorVisible(std::string const& name, bool visible = true);
        void SetActorVisible(QString name, bool visible = true);

        /// <summary>
        /// Determines whether there is an actor with this name. 
        /// Note - this does not work on frames (will return false even if you give a correct frame name), use HasFrame to find out if there is a frame with this name.
        /// </summary>
        /// <param name="name">The name of the actor. This has to be the same name that was used with AddActor or AddVtkPointSet in order for this to return true.</param>
        /// <returns>True if there is an actor with this name, false otherwise.</returns>
        bool HasActor(std::string name);
        
        /// <summary>
        /// Determines whether the object with the specified name is visible. This does not check only the "visibility" property but also 
        /// the current display mode etc. Simply put, if this returns true, you should be able to see the object on the screen (if it's within the camera).
        /// Note - this does not work on frames (will return false even if you give a correct frame name).
        /// </summary>
        /// <param name="name">The name of the actor.</param>
        /// <returns>True if the object is currently being rendered, false otherwise. Also returns false if the object does not exist.</returns>
        bool IsActorVisible(std::string name);
                
        /// <summary>
        /// Sets the specified axes visible or invisible. Does not refresh the display - do it yourself.
        /// </summary>
        /// <param name="name">The name of the axes.</param>
        /// <param name="visible">Toggles visibility of the axes.</param>
        void SetFrameVisible(std::string name, bool visible = true);
        
        /// <summary>
        /// Determines whether there is a frame (axesActor) actor with this name. 
        /// </summary>
        /// <param name="name">The name of the frame. This has to be the same name that was used with AddAxesActor in order for this to return true.</param>
        /// <returns>True if there is an axes actor with this name, false otherwise.</returns>
        bool HasFrame(std::string name);
        
        /// <summary>
        /// Determines whether the specified frame is selected.
        /// </summary>
        /// <param name="name">The name of the frame. This has to be the same name that was used with AddAxesActor in order for this to be succesful.</param>
        /// <returns>True if the frame is selected, false if it is not OR if there is no frame with such name.</returns>
        bool IsFrameSelected(std::string name);
        
        /// <summary>
        /// Tells the VtkDisplay that selection target of the interactor has changed. VtkDisplay then calls necessarry routines to set up the selection tools.
        /// MUST be called whenever the target changes
        /// </summary>
        /// <param name="currentSelectionTarget">The current selection target.</param>
        void SelectionTargetChanged(piper::hbm::SELECTION_TARGET currentSelectionTarget);
        
        /// <summary>
        /// Forces the VTK pipeline connected to this actor to update (specifically, the actor's mapper will be updated).
        /// Sometimes you will have to call this to make changes made in the original dataset project onto the rendered actors,
        /// but don't do this unless you need to - especially if the actor is based on vtkUnstructuredGrid dataset, this can slower the performance.
        /// Basically, if something does not render the way it should, try calling this. If it does not help, remove it from your code again.
        /// </summary>
        /// <param name="name">The name of the actor to update. If it does not exist, nothing happens.</param>
        void UpdateActor(std::string name);

        /// <summary>
        /// Scales the specified actor according to the provided scaling factors.
        /// This will not concatenate with previously provided values, but rather overwrite them, 
        /// i.e. you should provide scaling factors relative to the original size.
        /// </summary>
        /// <param name="name">The name of the actor. If it does not exist, nothing happens.</param>
        /// <param name="x">The scale factor of x-axis.</param>
        /// <param name="y">The scale factor of y-axis.</param>
        /// <param name="z">The scale factor of z-axis.</param>
        void SetActorScale(std::string name, double x, double y, double z);
        void SetActorScale_qt(QString name, double x, double y, double z) // need a different declaration to be able to use as a qt event callback method
        {
            SetActorScale(name.toStdString(), x, y, z);
        }

        /// <summary>
        /// Translates the specified actor by the specified distances from their initial position.
        /// This will not concatenate with previously provided values, but rather overwrite them, 
        /// i.e. you should provide distances relative to the original position.
        /// </summary>
        /// <param name="name">The name of the actor. If it does not exist, nothing happens.</param>
        /// <param name="x">The translation distance along x-axis.</param>
        /// <param name="y">The translation distance along y-axis.</param>
        /// <param name="z">The translation distance along z-axis.</param>
        void SetActorTranslation(std::string name, double x, double y, double z);
        void SetActorTranslation_qt(QString name, double x, double y, double z) // need a different declaration to be able to use as a qt event callback method
        {
            SetActorTranslation(name.toStdString(), x, y, z);
        }

        /// <summary>
        /// Rotates the specified actor by the provided angles along its axes.
        /// This will not concatenate with previously provided values, but rather overwrite them,
        /// i.e. you should provide angles relative to the original orientation.
        /// </summary>
        /// <param name="name">The name of the actor. If it does not exist, nothing happens.</param>
        /// <param name="x">The angle of rotation along x-axis.</param>
        /// <param name="y">The angle of rotation along y-axis.</param>
        /// <param name="z">The angle of rotation along z-axis.</param>
        void SetActorRotation(std::string name, double x, double y, double z);
        void SetActorRotation_qt(QString name, double x, double y, double z) // need a different declaration to be able to use as a qt event callback method
        {
            SetActorRotation(name.toStdString(), x, y, z);
        }

        /// <summary>
        /// Turns on or off clipping by the global plane. Modify the plane by using the SetGlobalClipPlanePosition and SetGlobalClipPlaneNormal methods.
        /// </summary>
        /// <param name="on">If set to <c>true</c>, clipping will be turned on, if set to <c>false</c> it will be turned off.</param>
        /// <seealso cref="SetGlobalClipPlanePosition" />
        /// <seealso cref="SetGlobalClipPlaneNormal" />
        Q_INVOKABLE void UseGlobalClipping(bool on);

        /// <summary>
        /// Sets the position of the global clipping plane.
        /// </summary>
        /// <param name="distanceFrom0">The distance from zero along the normal of the plane.</param>
        /// <seealso cref="UseGlobalClipping" />
        /// <seealso cref="SetGlobalClipPlaneNormal" />
        Q_INVOKABLE void SetGlobalClipPlanePosition(double distanceFrom0);

        /// <summary>
        /// Sets the normal of the global clipping plane.
        /// </summary>
        /// <param name="nx">The x component of the normal.</param>
        /// <param name="ny">The y component of the normal.</param>
        /// <param name="nz">The z component of the normal.</param>
        /// <seealso cref="UseGlobalClipping" />
        /// <seealso cref="SetGlobalClipPlanePosition" />
        Q_INVOKABLE void SetGlobalClipPlaneNormal(double nx, double ny, double nz);

public:
		/// <summary>
		/// Returns vtkRenderWindow
		/// </summary>
        vtkSmartPointer<vtkInternalOpenGLRenderWindow> GetRenderWindow();

		/// <summary>
		/// Returns all actors registered in the display.
		/// </summary>
        boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *GetDisplayActors();

		/// <summary>
		/// Clears the selection array for the mesh represented by the all actors.
		/// In case the mesh is mapped using vtkDataSurfaceFilter, which is done if the original input mesh is vtkUnstructuredGrid, it resets the selection for both
		/// the original unstructured grid and for the rendered surface mesh.
		/// </summary>
		void deselectAllPoints();

        // a generator of highlighting primitives for this display
        VtkDisplayHighlighters highlights;
        // name of the scalar arrays that vtkDataSetSurfaceFilter creates to map the original input mesh (which has 3D elements) to the output surface mesh
        // that is used for rendering and contains only 2D elements. these arrays contain for each output cell an ID of the original cell that created it (and the same for points)
        std::string OriginalCellIdsArrayName = ORIGINAL_CELL_IDS;
        std::string OriginalPointIdsArrayName = ORIGINAL_POINT_IDS;

        unsigned int _p_defaultColor = 0xFFDDDDDD;

		boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> updatedActors;
		void movePoint(int pickPosition[2], double coord[3]);

        double POINTHIGHLIGHT_DEF_RADIUS = 0.01; // in metres, can be set from the GUI (TODO - setting it from the GUI)
        double POINTHIGHLIGHT_RADIUS = POINTHIGHLIGHT_DEF_RADIUS; // POINTHIGHLIGHT_DEF_RADIUS in units of the current model, is converted when model changes
        double POINTHIGHLIGH_SMALL_FACTOR = 10; // factor by which POINTHIGHLIGHT_RADIUS will be divided for cases where small spheres are more suitable, e.g. for selected points
        double FRAME_DEF_LENGTH = 0.04; // in metres, can be set from the GUI (TODO - setting it from the GUI)
        double FRAME_LENGTH = FRAME_DEF_LENGTH; // FRAME_DEF_LENGTH in units of the current model, is converted when model changes

	public slots:

        /// <summary>
        /// Re-setups the scene and then renders the content. This should be called after you have added or removed some object to the scene,
        /// otherwise the addition/removal will not be registered by the renderers. 
        /// If you did not add or removed anything, only changed properties of objects that are already in the scene (e.g. color), calling <c>Render</c> should be sufficient (and more efficient).
        /// </summary>
        /// <param name="resetCam">You may specify <c>true</c> to ensure that the camera will be reset after the refresh. If you keep the parameter as <c>false</c>,
        /// the camera will still be reset in case it was already set to be resetted by some other call (e.g. AddVtkPointSet). I.e. explicitly setting this
        /// to <c>false</c> has no effect, only <c>true</c> has some meaning.</param>
        void Refresh(bool resetCam = false);
        
        /// <summary>
        /// Renders the content of the scene. This does not re-setup the scene, i.e. if you added or removed something from the scene,
        /// you need to call <c>Refresh</c> instead.
        /// </summary>
        void Render();
                
        /// <summary>
        /// Resets the camera of the display - whole scene becomes visible, focus point gets reset to (0,0,0).
        /// </summary>
        Q_INVOKABLE void ResetCamera();

		/// <summary>
		/// Set the camera to look in the given axis direction.
		/// <param name="axis">Integer representing the aixs. <c>0</c>: x, <c>1</c>: y, <c>2</c>: z, <c>3</c>: -x, <c>4</c>: -y, <c>5</c>: -toz.
		/// </summary>
		Q_INVOKABLE void SetCamera(int axis);
    
		/// <summary>
		/// Creates a new rw if it is not existent or loads it from the parent qml file and creates a new renderer and QTVTKInteractor
		/// </summary>
		void initialize();

		QVTKFrameBufferObjectItem *getFrameBuffer() { return frameBuffer; }
        
        /// <summary>
        /// Sets the frame buffer and then re-initializes the display based on this new framebuffer.
        /// </summary>
        /// <param name="fb">The framebuffer to render to.</param>
        void setFrameBuffer(QVTKFrameBufferObjectItem *fb);


        /// <summary>
        /// Turns off coloring of a given model by any scalar array (the model will be rendered using default colors).
        /// </summary>
        /// <param name="name">The name of the model. If the name is not specified, 
        /// coloring will be turned off for every model in the scene - i.e. only for active actors, not all registered actors.</param>
        void HighlightElementsByScalarsOff(std::string name = "");
        
        /// <summary>
        /// Removes all actors that were not marked as persistent from the display.
        /// </summary>
        void RemoveNonpersistentActors();
        
        /// <summary>
        /// Sets the type of the picking.
        /// </summary>
        /// <param name="pickType">
        /// -1 = nothing (picking turned off)
        /// 0 = nodes picking
        /// 1 = faces picking
        /// 2 = elements picking
        /// 3 = entities picking
        /// 4 = free points (landmarks) picking
        /// 5 = rubberband element picking
        /// 6 = create points on intersection with picked element
        ///</param>
        /// <returns>A text with a hint for user on how to use the given picking type.</returns>
        QString SetPickingType(int pickType = 0);
        
        /// <summary>
        /// Initializes picking of camera focus points - provides the vtkMousePicker the pointer to the focus point 
        /// to fill it with the new coordinates on next click.
        /// </summary>
        Q_INVOKABLE void PickCameraFocus();
        
        /// <summary>
        /// Slot for vtkMouseInteractorStyle signal - updates the camera focus after it has been changed by the mouse interactor style.
        /// </summary>
        /// <param name="call_data">The new focal point.</param>
        void UpdateCameraFocus(vtkObject* caller, unsigned long vtk_event, void* client_data, void* call_data);

        bool optionVisualizeBoxes() const;

        void setOptionVisualizeBoxes(const QVariant &value);

        bool GetUseParallelProjection();
        
        bool GetRenderEdges();
        bool GetRenderNormals();        
        /// <summary>
        /// Gets the blank level.
        /// </summary>
        /// <returns>Order number indicating the latest blanking action.</returns>
        int GetBlankLevel();

        /// <summary>
        /// Toggles perspective and parallel projection of camera.
        /// </summary>
        /// <param name="value">If set to <c>true</c>, parallel projection will be used.</param>
        void SetUseParallelProjection(const QVariant &value);

        /// <summary>
        /// Toggles rendering of element edges of all actors.
        /// </summary>
        /// <param name="value">If set to <c>true</c>, edges will be rendered.</param>
        void SetRenderEdges(const QVariant &value);
        
        /// <summary>
        /// Toggles rendering of face normals of all actors.
        /// </summary>
        /// <param name="value">If set to <c>true</c>, normals will be rendered.</param>
        void SetRenderNormals(const QVariant &value);
                
        /// <summary>
        /// Blocks all rendering by the current RenderWindow and registers the UnblockRendering to be called
        /// after the specified QFutureWatcher's "finished" signal is fired, thus enabling the rendering again.
        /// </summary>
        /// <param name="futureWatcher">The future watcher that will be used for the upcoming "action". Rendering using the current
        /// render window will be blocked until this future watcher's "finished" signal is emitted.</param>
        void PrepareRenderBlockingAction(QFutureWatcher<void>& futureWatcher);        

        /// <summary>
        /// Unblocks the rendering of the current render window.
        /// If a Refresh was requested while rendering was blocked, this will also call Refresh().
        /// </summary>
        void UnblockRendering();

	signals:
        void frameBufferChanged();
        void optionVisualizeBoxesChanged();
        void useParallelProjectionChanged();
        void renderEdgesChanged();
        void renderNormalsChanged();
        void actorSelectDeselect();
        void displayModeChanged();
        void pickTargetChanged();
        void blankLevelChanged();
	
	protected:
		QVTKFrameBufferObjectItem *frameBuffer;
		unsigned int currentDisplayMode; // a visualization mode - see DISPLAY_MODE for details
        unsigned int pickableMode; // only actors of these modes will be pickable - see DISPLAY_MODE for details
		boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> actors; // map of all actors registered in the display, mapped by name specified during insertion
        bool resetCamera = true; // a flag taking care of whether camera should be reset during the next scene setup. after it is, the flag is turned back off
        // Name appended to the specified axes name for each of its parts.
        static const std::string axisPartsNameSuffix[];
        bool useParallelProjection = true; // type of projection currently used by the default renderer, false for perspective, true for parallel
        bool renderEdges = false; // if set to true, edges of cells will be rendered as visible
        bool renderNormals = false; // if set to true, normals of cells will be rendered
        bool refreshRequested = false; // to postpone refresh if rendering is currently blocked (due to computations being done in not-main thread)
        int blankLevel = 0; // counter of how many blank operations were made - to enable unblanking one by one
        vtkSmartPointer<vtkEventQtSlotConnect> mConnect;
        vtkSmartPointer<vtkPlane> globalClipPlane; // when global clipping is enabled, this clipping plane will be applied to all actors
        
        vtkSmartPointer<hbm::vtkSelectionTools> st;
        vtkSmartPointer<vtkInternalOpenGLRenderWindow> rw; // the window used for current rendering
        vtkSmartPointer<vtkCamera> activeCamera = NULL; // the camera that is being used (and shared) by the default and picking renderer

        hbm::SELECTION_TARGET activeSelectionTarget = hbm::SELECTION_TARGET::NONE;

        /// <summary>
        /// Initializes the scene based on the current DISPLAY_MODE, i.e. sets which actors are active and which are not.
        /// </summary>
        void setupScene();
	
        /// <summary>
        /// Clears the selection array for the mesh represented by the specified actor.
        /// In case the mesh is mapped using vtkDataSurfaceFilter, which is done if the original input mesh is vtkUnstructuredGrid, it resets the selection for both
        /// the original unstructured grid and for the rendered surface mesh.
        /// </summary>
        /// <param name="actor">The actor that represents the dataset.</param>
        void deselectCells(vtkSmartPointer<VtkDisplayActor> actor);
        
        /// <summary>
        /// Selects or deselects specified point based on its current value and the pick appending append rule.
        /// Result is written into the SELECTED_PRIMITIVES array.
        /// </summary>
        /// <param name="actor">The actor to which the point belongs - more precisely, the point has to belong to the OriginalData of that actor.</param>
        /// <param name="pointID">The ID of the point.</param>
        /// <param name="append">The append rule.</param>
        /// <param name="forceValue">If set to true, the point will always end up having the value specified by the next parameter,
        ///  if false, the selection value will be evaluate based on the current value -
        /// it will be toggled - if it is selected, it will be deselected, if it is not selected, it will be selected.</param>
        /// <param name="value">The value to assign to the point. Only used if forceValue is true.</param>
        /// <returns>IS_PRIMITIVE_SELECTED if the point is now selected, IS_NOT_PRIMITIVE_SELECTED otherwise.</returns>
        int selectPointEvaluate(vtkSmartPointer<VtkDisplayActor> actor, int pointID, PICK_APPENDING_STYLE append, bool forceValue = false, int value = IS_PRIMITIVE_SELECTED);

        /// <summary>
        /// Clears the selection array for the mesh represented by the specified actor.
        /// In case the mesh is mapped using vtkDataSurfaceFilter, which is done if the original input mesh is vtkUnstructuredGrid, it resets the selection for both
        /// the original unstructured grid and for the rendered surface mesh.
        /// </summary>
        /// <param name="actor">The actor that represents the dataset.</param>
        void deselectPoints(vtkSmartPointer<VtkDisplayActor> actor);

		/// <summary>
		/// Performs selection on all active actors by the private selection tool st.
		/// Set what selection method of st should be used prior to calling this method.
		/// This does not call Refresh at any point to avoid calling it multiple times when more actors are updated,
		/// so if you are calling this with target == nodes, call refresh afterwards to add point highlights to render.
		/// </summary>
		/// <param name="append">Controls the way previously selected primitives are handled. If set to DESELECT_ACTOR, primitives will be deselected
		/// instead of selected, but those outside the selection area will be left intact. If set to APPEND_ACTOR, everything will be deselected 
		/// prior to performing the selection. If set to APPEND_ALL, what was already selected before will remain selected, the result of this selection will just be added to it.</param>
		/// <param name="target">What should be selected - currently supports only elements and nodes.</param>
		void selectBySelectionTools(PICK_APPENDING_STYLE append, hbm::SELECTION_TARGET target);
        
        /// <summary>
        /// Toggles off or on highlighting of a specified actor.
        /// </summary>
        /// <param name="on">If set to <c>true</c>, all selected actor will be highlighted. By setting it to <c>false</c>, highlighting will be turned off.
        /// Note that this has no effect on whether the actor is actually selected or not - that is handled by the selectedActors map.</param>
        /// <param name="actor">The actor to highlight. If it is null (the default), the highlighting for all actors in the scene (active actors) will be turned on/off.</param>
        void selectedActorHighlighting(bool on, vtkSmartPointer<VtkDisplayActor> actor = 0);
        
        /// <summary>
        /// Sets everything that is neccessary for a given model for its cells to be colored according to a specified scalar array.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="scalarArrayName">Name of the scalar array to use for coloring the . 
        /// If the actor's mapper's input does not have "scalarArrayName" array for cells, nothing will happen.</param>
        /// <param name="usePointData">If set to <c>true</c>, the <c>scalarArrayName</c> array must be a pointData array,
        /// otherwise it must be a cellData array. Basically this defines whether the scalars are defined per element or per point.</param>
        /// <param name="lut">A binary LUT with the color mapping you want to use.
        /// If you do not provide one, a default one will be created, using the colorOfSelection to render selected cells and the original color of the model to
        /// render the rest.</param>
        void highlightElementsByScalars(vtkSmartPointer<VtkDisplayActor> actor, std::string scalarArrayName, bool usePointData, vtkSmartPointer<vtkScalarsToColors> lut = 0);
        
        /// <summary>
        /// Goes reccursively up the blanking hierarchy and updates the children cell scalar array based on the values in the original parent using the pedigree cell IDs.
        /// </summary>
        /// <param name="actor">The actor to update.</param>
        /// <param name="scalarArrayName">Name of the scalar array to update.</param>
        void updateCellScalarsFromParent(vtkSmartPointer<VtkDisplayActor> actor, std::string scalarArrayName);

        /// <summary>
        /// Creates a normal-actor for the specified actor and adds it as the "normalHighlights" actor of the specified actor.
        /// The normal actor is given a name: actor->GetName() + "_normals".
        /// </summary>
        /// <param name="actor">The actor for which to create normals.</param>
        void renderActorsNormal(vtkSmartPointer<VtkDisplayActor> actor);

		/// <summary>
		/// Computes the bounding box of all visible actors.
		/// </summary>
		/// <param name="boundingBox">Pointer to fill with the bounding box data. Upon exiting, it will
		/// contain {Xmin,Xmax,Ymin,Ymax,Zmin,Zmax} of the box.</param>
		/// <returns>True if the bounding box was created, false if there are no active actors -> no bounding box.</returns>
		bool computeVisiblesBoundingBox(double boundingBox[6]);
	};


} // namespace piper

#endif //VTKDISPLAY_PIPER_H
