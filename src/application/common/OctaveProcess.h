/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_OCTAVEPROCESS_H
#define PIPER_OCTAVEPROCESS_H

#include <QString>
#include <QProcess>
#include <QFile>

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

namespace piper {

/** The OctaveProcess class configure a QProcess ready to launch the Octave application
 * and run .m script.
 *
 * \todo add a debug mode to get the octave process standard output
 * \author Matthieu MEAR \date July 2016
 */
class PIPERCOMMON_EXPORT OctaveProcess : public QProcess
{
public:
#define MAX_SCRIPT_RUNTIME 600000 // maximum amount of time piper will wait for an octave process to finish (in miliseconds)

        OctaveProcess();

        bool matlabCompatibleMode() const {return m_matlabCompatibleMode;}
        /// set the --traditional octave option, more information with "octave --help"
        void setMatlabCompatibleMode(bool matlabCompatibleMode=true) {m_matlabCompatibleMode = matlabCompatibleMode;}

        bool persist() const {return m_persist;}
        /// set the --persist octave option, more information with "octave --help"
        void setPersist(bool persist=true) {m_persist = persist;}

        QString const& scriptPath() const {return m_scriptPath;}
        /// set the path \a scriptPath to the script to be executed
        void setScriptPath(QString scriptPath);

        QStringList const& scriptArguments() const {return m_scriptArguments;}
        /// set the \a arguments to pass to the script
        void setScriptArguments(QStringList scriptArguments);

        /// \returns return true if the OctaveProcess can be started.
        bool canBeStarted();

        /// @returns octave --version first line
        QString version();

        /** Execute the script.
         * This function is blocking and will block until the script has finished. It is usefull in test or batch mode. To run a script from the GUI application prefer startScript().
         * \return True in case the script is finished, false in case the process took more than MAX_SCRIPT_RUNTIME ms - the process is still running in such a case!
         * \throw a std::runtime_error on failure including standard output and standard error in its message
         * \see startScript()
         */
        bool executeScript();

        /** Start script execution and return. This function is non blocking, when the execution has finished the QProcess::finished() signal is sent.
         */
        void startScript();

        /// \return true if last script execution finished successfully
        bool isLastExecutionSuccessful() const;

        /// Build a standard error message to retrieve when the script finishes with error
        QString errorMessage();

    private:
        bool m_matlabCompatibleMode;
        bool m_persist;
        QString m_scriptPath;
        QStringList m_scriptArguments;

        /// internal method to setup octave process arguments
        void prepareOctaveProcess();

    };
}

Q_DECLARE_METATYPE(piper::OctaveProcess*)

#endif // PIPER_OCTAVEPROCESS_H
