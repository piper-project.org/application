/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef METAEDITOR_H_
#define METAEDITOR_H_

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include "VtkDisplayHighlighters.h"
#include "MetaViewManager.h"
#include "hbm/FEModel.h"

namespace piper {
	/** @brief 	A class for Metadata Editing Module.
	*
	* @author Sachiv Paruchuri @date 2016
	*/
	class PIPERCOMMON_EXPORT MetaEditor : public QObject
	{
		Q_OBJECT

	public:
		MetaEditor();
		~MetaEditor();

		public slots:
		/// <summary>
		/// Turns on or off rendering of frames.
		/// </summary>
		/// <param name="activateFrames">If set to <c>true</c>, all frames will be rendered in the display.</param>
		void FramesActive(bool activateFrames);

		//DISPLAY --

		/// <summary>
		/// Switch between Entity/Full Model Display
		/// </summary>
		/// <param name="type">The type of model wich has to be shown. 0: FULL_MESH, 1: ENTITIES</param>
		void modelDisplay(int type);


		/// <summary>
		/// Busy Indicator of getSelectedElementIDs function
		/// <param name="name">Name of the object to be created/edited.</param>
		/// <param name="create">Integer representing the type of object to be created/edited. <c>0</c>: Entity, <c>1</c>: GenericMetadata. </param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new object, <c>1</c>:  edit existing object. </param>
		/// </summary>
		void getSelectedElementIDsBusyIndicator(QString name, int create = 0, int update = 0);

		/// <summary>
		/// Get VId of selected elements.
		/// <param name="name">Name of the object to be created/edited.</param>
		/// <param name="create">Integer representing the type of object to be created/edited. <c>0</c>: Entity, <c>1</c>: GenericMetadata. </param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new object, <c>1</c>:  edit existing object. </param>
		/// </summary>
		void getSelectedElementIDs(QString name, int create = 0, int update = 0);

		/// <summary>
		///  Busy Indicator of getSelectedNodeIDs function
		/// <param name="name">Name of the object to be created/edited.</param>
		/// <param name="create">Integer representing the type of object to be created/edited. <c>0</c>: Landmark, <c>1</c>: ControlPoint, <c>2</c>: GenericMetadata. </param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new object, <c>1</c>:  edit existing object. </param>
		/// </summary>
		void getSelectedNodeIDsBusyIndicator(QString name, QString type, int create = 0, int update = 0);

		/// <summary>
		/// Get VId of selected nodes.
		/// <param name="name">Name of the object to be created/edited.</param>
		/// <param name="create">Integer representing the type of object to be created/edited. <c>0</c>: Landmark, <c>1</c>: ControlPoint, <c>2</c>: GenericMetadata. </param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new object, <c>1</c>:  edit existing object. </param>
		/// </summary>
		void getSelectedNodeIDs(QString name, QString type, int create = 0, int update = 0);


		/// <summary>
		/// Add/Edit an Entity and push it to FEM.
		/// <param name="name">Name of the Entity to be created/edited.</param>
		/// <param name="vid1D">VId of piper IDs of Element1D objects.</param>
		/// <param name="vid2D">VId of piper IDs of Element2D objects.</param>
		/// <param name="vid3D">VId of piper IDs of Element3D objects.</param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new Entity, <c>1</c>:  edit existing Entity. </param>
		/// </summary>
		void addEntity(std::string name, piper::hbm::VId vid1D, piper::hbm::VId vid2D, piper::hbm::VId vid3D, int update = 0);

		/// <summary>
		/// Add/Edit a Landmark and push it to FEM.
		/// <param name="name">Name of the Landmark to be created/edited.</param>
		/// <param name="type">Type of Landmark.</param>
		/// <param name="vid1D">VId of piper IDs of Nodes.</param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new Landmark, <c>1</c>:  edit existing Landmark. </param>
		/// </summary>
		void addLandmark(std::string name, std::string type, piper::hbm::VId vId, int update = 0);

		/// <summary>
		/// Add/Edit a Control Point and push it to FEM.
		/// <param name="name">Name of the Control Point to be created/edited.</param>
		/// <param name="type">Type of Control Point.</param>
		/// <param name="vid1D">VId of piper IDs of Nodes.</param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new Control Point, <c>1</c>:  edit existing Control Point. </param>
		/// </summary>
		void addControlPoint(std::string name, std::string type, piper::hbm::VId vId, int update = 0);

		/// <summary>
		/// Add/Edit a Joint and push it to FEM.
		/// <param name="name">Name of the Joint to be created/edited.</param>
		/// <param name="constrainedDofType">Type of constrainedDofType.</param>
		/// <param name="entity1">Name of Entity 1.</param>
		/// <param name="entity1Frame">ID of Entity Frame 1.</param>
		/// <param name="entity2">Name of Entity 2.</param>
		/// <param name="entity2Frame">ID of Entity Frame 2.</param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new Joint, <c>1</c>:  edit existing Joint.</param>
		/// </summary>
		void addJoint(QString name, QString constrainedDofType, QString entity1, int entity1Frame, QString entity2, int entity2Frame, int update = 0);

		/// <summary>
		/// Add/Edit an GenericMetadata and push it to FEM.
		/// <param name="name">Name of the GenericMetadata to be created/edited.</param>
		/// <param name="vid1D">VId of piper IDs of Element1D objects.</param>
		/// <param name="vid2D">VId of piper IDs of Element2D objects.</param>
		/// <param name="vid3D">VId of piper IDs of Element3D objects.</param>
		/// <param name="update">Integer representing the method to be executed. <c>0</c>:create new GenericMetadata, <c>1</c>:  edit existing GenericMetadata. </param>
		/// </summary>
		void addGenericMetadata(std::string name, piper::hbm::VId vid1D, piper::hbm::VId vid2D, piper::hbm::VId vid3D, int update = 0);


		/// <summary>
		/// Remove existing Entity.
		/// <param name="name">Name of the Entity to be removed.</param>
		/// </summary>
		void removeEntity(QString name);

		/// <summary>
		/// Remove existing Landmark.
		/// <param name="name">Name of the Landmark to be removed.</param>
		/// </summary>
		void removeLandmark(QString name);

		/// <summary>
		/// Remove existing GenericMetadata.
		/// <param name="name">Name of the GenericMetadata to be removed.</param>
		/// </summary>
		void removeGenericMetadata(QString name);

		/// <summary>
		/// Remove existing Joint.
		/// <param name="name">Name of the Joint to be removed.</param>
		/// </summary>
		void removeJoint(QString name);

		/// <summary>
		/// Remove existing Control Point.
		/// <param name="name">Name of the Control Point to be removed.</param>
		/// </summary>
		void removeControlPoint(QString name);

		/// <summary>
		/// Select a VTK actor.
		/// <param name="visible">Boolean representing the visibility of actor.</param>
		/// <param name="name">Name of the Actor to be selected.</param>
		/// </summary>
		void selectActor(bool visible, QString name);

		/// <summary>
		/// Get the names of actors representing the Control Point.
		/// <param name="name">Name of the Control Point.</param>
		/// <returns>Vector of names of actors representing the Control Point.< / returns>
		/// </summary>
		std::vector<std::string> getControlPointActorNames(std::string name);

	private:
		/// <summary>
		/// Remove Control Point actors.
		/// <param name="name">Name of the Control Point.</param>
		/// </summary>
		void removeControlPointActors(std::string name);

	protected:
		boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *m_actors;
		VtkDisplayHighlighters draw;
		MetaViewManager metaViewManager;

		std::vector<std::string> new_entities;
		std::vector<std::string> new_landmarks;
		std::vector<std::string> new_genericmetadatas;
		std::vector<std::string> new_joints;
		std::vector<std::string> new_controlpoints;
	};
}

#endif
