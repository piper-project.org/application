/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "logging.h"

#include <cstdlib>
#include <mutex>

#include <QTextStream>
#include <QTextDocument>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDateTime>

#include <common/Context.h>

Q_LOGGING_CATEGORY(piperApp, "piper.app")

namespace piper {

QDebug operator<<(QDebug debug, std::string const& str)
{
    debug << QString::fromStdString(str);
    return debug;
}

/*
 * class LoggingParameter
 */

// message handler to dispatch messages to registered message handlers
void piperMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static std::mutex loggingMutex;

    loggingMutex.lock();
    for(QtMessageHandler handler: LoggingParameter::instance().m_messageHandlers)
        handler(type, context, msg);
    loggingMutex.unlock();

    if (type==QtFatalMsg) {
        // A fatal message aborts the application
        QCoreApplication::exit(-1);
        // in batch mode there is no event loop
        std::abort();
    }
}

// category filters
void defaultCategoryFilter(QLoggingCategory * category)
{
    // filter out messages not coming from piper.app, keep critical messages
    if (strcmp(category->categoryName(),"piper.app") != 0) {
        category->setEnabled(QtDebugMsg, false);
        category->setEnabled(QtInfoMsg, false);
        category->setEnabled(QtWarningMsg, false);
    }
    else
        // enable piper.app debug messages
        category->setEnabled(QtDebugMsg, false);
}

void verboseCategoryFilter(QLoggingCategory * category)
{
    // filter out messages not coming from piper.app
    if (strcmp(category->categoryName(),"piper.app") != 0) {
        category->setEnabled(QtDebugMsg, false);
        category->setEnabled(QtInfoMsg, false);
        category->setEnabled(QtWarningMsg, false);
    }
    else
        // enable piper.app debug messages
        category->setEnabled(QtDebugMsg, true);
}

void veryVerboseCategoryFilter(QLoggingCategory * category)
{
    if (strcmp(category->categoryName(),"piper.app") == 0 || strcmp(category->categoryName(),"qml") == 0 || strcmp(category->categoryName(),"default") == 0)
        // enable piper.app and qml debug messages
        category->setEnabled(QtDebugMsg, true);
    else
        category->setEnabled(QtDebugMsg, false);
}

LoggingParameter::LoggingParameter()
{
    qInstallMessageHandler(piper::piperMessageHandler);
    setVerboseLevel(VerboseLevel::DEFAULT);
    QString logDir = QDir::homePath()+"/.piper";
    if (!QFileInfo::exists(logDir))
        QDir().mkpath(logDir);
    setLogFilePath(logDir+"/"+ QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss")+".log");
}

LoggingParameter& LoggingParameter::instance() {
    static LoggingParameter logParam;
    return logParam;
}

void LoggingParameter::setVerboseLevel(VerboseLevel level)
{
    m_verboseLevel = level;
    switch (level) {
    case VerboseLevel::DEFAULT:
        QLoggingCategory::installFilter(defaultCategoryFilter);
        break;
    case VerboseLevel::VERBOSE:
        QLoggingCategory::installFilter(verboseCategoryFilter);
        break;
    case VerboseLevel::VERY_VERBOSE:
        QLoggingCategory::installFilter(veryVerboseCategoryFilter);
        break;
    }
}

void LoggingParameter::addMessageHandler(QtMessageHandler handler)
{
    m_messageHandlers.append(handler);
}

void LoggingParameter::removeMessageHandler(QtMessageHandler handler)
{
    m_messageHandlers.removeOne(handler);
}

void LoggingParameter::clearMessageHandler()
{
    m_messageHandlers.clear();
}

/*
 * message handlers
 */

void appendCategory(QTextStream& stream, const QMessageLogContext &context)
{
    // TODO align field to a constant number of chars using &nbsp;
    stream << QString::fromStdString("<font face=\"monospace\" color=\"dimgrey\">["+std::string(context.category)+"]&nbsp;</font>");
}

void logPanelMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString message;
    QTextStream stream(&message, QIODevice::Append);
    if (LoggingParameter::VerboseLevel::VERY_VERBOSE == LoggingParameter::instance().verboseLevel())
        appendCategory(stream, context);
    switch (type) {
    case QtDebugMsg:
        stream << DEBUGMSG << msg << endl;
        break;
    case QtInfoMsg:
        stream << msg << endl;
        break;
    case QtWarningMsg:
        stream << WARNING << msg << endl;
        Context::instance().importantMessageReceived();
        break;
    case QtCriticalMsg:
    case QtFatalMsg:
        stream << ERRORMSG << msg << endl;
        Context::instance().importantMessageReceived();
        break;
    }
    static const int chunkSize = 2048;

    if (message.size() < chunkSize)
        Context::instance().newLogMessage(message);
    else {
        int current_index = 0;
        while (current_index < message.size()) {
            int next_index = message.indexOf(",", current_index+chunkSize);
            if (-1 == next_index)
                next_index = current_index+chunkSize;
            else
                next_index += 1; // infoAppend appends on a new line, to keep the ',' on the same line
            Context::instance().newLogMessage(message.mid(current_index, next_index-current_index));
            current_index=next_index;
        }
    }
}

void consoleMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    static QTextStream qStdout(stdout);
    static QTextStream qStderr(stderr);

    // this QTextDocument is used to convert from rich text content to plain text content suitable for console or log file output
    static QTextDocument document;
    static QString formatedMessage;
    static QTextStream messageFormater(&formatedMessage, QIODevice::Append);
    formatedMessage.clear();
    if (LoggingParameter::VerboseLevel::VERY_VERBOSE == LoggingParameter::instance().verboseLevel())
        appendCategory(messageFormater, context);
    switch (type) {
    case QtDebugMsg:
        messageFormater << DEBUGMSG << msg;
        break;
    case QtInfoMsg:
        messageFormater << msg;
        break;
    case QtWarningMsg:
        messageFormater << WARNING << msg;
        break;
    case QtCriticalMsg:
    case QtFatalMsg:
        messageFormater << ERRORMSG << msg;
        document.setHtml(formatedMessage);
        qStderr << document.toPlainText() << ::endl;
        qStderr.flush();
        return;
    }
    document.setHtml(formatedMessage);
    qStdout << document.toPlainText() << ::endl;
    qStdout.flush();
}

void logFileMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static bool errorMessageDisplayed = false;
    QFile logFile(LoggingParameter::instance().logFilePath());
    if (!logFile.open(QIODevice::Text | QIODevice::Append | QIODevice::Unbuffered)) {
        if (!errorMessageDisplayed) {
            QTextStream(stderr) << "Cannot write to log file " << LoggingParameter::instance().logFilePath() << ::endl;
            errorMessageDisplayed = true;
        }
        return;
    }

    // this QTextDocument is used to convert from rich text content to plain text content suitable for console or log file output
    static QTextDocument document;
    static QString formatedMessage;
    static QTextStream messageFormater(&formatedMessage, QIODevice::Append);
    formatedMessage.clear();
    if (LoggingParameter::VerboseLevel::VERY_VERBOSE == LoggingParameter::instance().verboseLevel())
        appendCategory(messageFormater, context);
    switch (type) {
    case QtDebugMsg:
        messageFormater << DEBUGMSG << msg;
        break;
    case QtInfoMsg:
        messageFormater << msg;
        break;
    case QtWarningMsg:
        messageFormater << WARNING << msg;
        break;
    case QtCriticalMsg:
    case QtFatalMsg:
        messageFormater << ERRORMSG << msg;
    }
    document.setHtml(formatedMessage);
    logFile.write(document.toPlainText().toUtf8());
    logFile.write(QString('\n').toUtf8());
    logFile.close();
}


}
