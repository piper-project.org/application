/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef METAVIEWMANAGER_H_
#define METAVIEWMANAGER_H_

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include "VtkDisplayHighlighters.h"

namespace piper {

	class PIPERCOMMON_EXPORT MetaViewManager : public QObject
	{
		Q_OBJECT

	public:
		MetaViewManager();
		~MetaViewManager();

		piper::hbm::ContourCLbr* getBodyRegionObject(std::string name);
		piper::hbm::ContourCLj* getJointObject(std::string name);


		public slots:
		void openContour(const QUrl& modelFile) { openContour(modelFile.toLocalFile().toStdString()); }

		//TABLES ---

		/// <summary>
		/// Get Names of Entities
		/// </summary>
		/// <returns>QList of QSting containing Entity Names</returns>
		QList<QString> getEntityList();

		/// <summary>
		/// Get Names of Landmarks
		/// </summary>
		/// <returns>QList of QSting containing Landmark Names</returns>
		QList<QString> getLandmarkList();

		/// <summary>
		/// Get Types of Landmarks
		/// </summary>
		/// <returns>QList of QSting containing Landmark Types</returns>
		QList<QString> getLandmarkTypeList();

		/// <summary>
		/// Get Names of Entities from AnatomyDB
		/// </summary>
		/// <returns>QList of QSting containing Entity Names</returns>
		QList<QString> getAnatomyDBEntityList();

		/// <summary>
		/// Get Names of Landmarks from AnatomyDB
		/// </summary>
		/// <returns>QList of QSting containing Landmark Names</returns>
		QList<QString> getAnatomyDBLandmarksList();

		/// <summary>
		/// Get Names of Joints
		/// </summary>
		/// <returns>QList of QSting containing Joint Names</returns>
		QList<QString> getAnatomyDBJointList();

		/// <summary>
		/// Get Names of Frames
		/// </summary>
		/// <returns>QList of QSting containing Frame Names</returns>
		QList<int> getFrameList();

		/// <summary>
		/// Get Names of Joints
		/// </summary>
		/// <returns>QList of QSting containing Joint Names</returns>
		QList<QString> getJointList();

		/// <summary>
		/// Get Names of Joint Entity 1
		/// </summary>
		/// <returns>QList of QSting containing Joint Entity 1</returns>
		QList<QString> getJointEntity1List();

		/// <summary>
		/// Get Names of Joint Entity 2
		/// </summary>
		/// <returns>QList of QSting containing Joint Entity 2</returns>
		QList<QString> getJointEntity2List();

		/// <summary>
		/// Get Names of Joint Entity 1 Frames
		/// </summary>
		/// <returns>QList of QSting containing Joint Entity 1 Frames</returns>
		QList<QString> getJointEntity1FrameList();

		/// <summary>
		/// Get Names of Joint Entity 2 Frames
		/// </summary>
		/// <returns>QList of QSting containing Joint Entity 2 Frames</returns>
		QList<QString> getJointEntity2FrameList();

		/// <summary>
		/// Get IDs of Contours
		/// </summary>
		/// <returns>QList of int containing Contour IDs</returns>
		QList<int> getContourList(int bodyRegionNo = 0, int contourNo = 0);

		/// <summary>
		/// Get Names of GenericMetadatas
		/// </summary>
		/// <returns>QList of QSting containing GenericMetadata Names</returns>
		QList<QString> getGenericMetadataList();

		/// <summary>
		/// Get Names of Control Points
		/// </summary>
		/// <returns>QList of QSting containing Control Point Names</returns>
		QList<QString> getControlPointList();

		/// <summary>
		/// Get Roles of Control Points
		/// </summary>
		/// <returns>QList of QSting containing Control Point Roles</returns>
		QList<QString> getControlPointRoleList();

		/// <summary>
		/// Get Numbers of Control Points
		/// </summary>
		/// <returns>QList of QSting containing Control Point Numbers</returns>
		QList<double> getControlPointNumberList();

		/// <summary>
		/// Get Names of Targets
		/// </summary>
		/// <returns>QList of QSting containing Target Names</returns>
		QList<QString> getTargetList();

		/// <summary>
		/// Get Types of Targets
		/// </summary>
		/// <returns>QList of QSting containing Target Types</returns>
		QList<QString> getTargetTypeList();


		/// <summary>
		/// Get desctiption of an Entity from AnatomyDB
		/// </summary>
		/// <param name="entityName">Name of tne Entity for which the description is returned.</param>
		/// <returns>QString of description of an Entity</returns>
		QString getAnatomyDBDescription(QString entityName);


		/// <summary>
		/// Get the type of the Landmark 
		/// </summary>
		/// <param name="name">QSting value indicating the name of Landmark</param>
		/// <returns>QSting of the Landmark Type</returns>
		QString getTypeOfLandmark(QString name);

		/// <summary>
		/// Get the coordinates of selected node
		/// </summary>
		/// <returns>QList of double of sleected Node coordinates</returns>
		QList<double> getCoordinatesOfSelectedNode();

		//DISPLAY ---

		/// <summary>
		/// Show/hide Specific Entity
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Entity, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="name">QSting value indicating the name of Entity</param>
		void entityDisplay(bool visible, QString name);

		/// <summary>
		/// Show/hide Specific genericMetadata
		/// </summary>
		/// <param name="visible">Integer value indicating the visibility of genericMetadata, 0: Hide, 1: Show</param>
		/// <param name="name">QSting value indicating the name of genericMetadata</param>
		void genericMetadataDisplay(bool visible, QString name);

		/// <summary>
		/// Show/hide Specific Landmark
		/// </summary>
		/// <param name="visible">Bool value indicating the visibility of Landmark.</param>
		/// <param name="name">QSting value indicating the name of Landmark</param>
		/// <param name="visualizeAsPoints">If set to <c>true</c>, landmarks will be visualized as sets of points that define the landmark.
		/// If set to <c>false</c>, they will be visualized as a single sphere around the point defined by Landmark::position().</param>
		void landmarkDisplay(bool visible, QString name, bool visualizeAsPoints);

		/// <summary>
		/// Updates visibility of all landmarks in the provided lists
		/// </summary>
		/// <param name="landmarkNames">Names of landmarks to be updated.</param>
		/// <param name="landmarkVisibility">For each landmark in <c>landmarkNames</c>, a bool indicating whether it should be visible or not.
		/// The order (and size) must match <c>landmarkNames</c>.</param>
		/// <param name="visualizeAsPoints">If set to <c>true</c>, landmarks will be visualized as sets of points that define the landmark.
		/// If set to <c>false</c>, they will be visualized as a single sphere around the point defined by Landmark::position().</param>
		void landmarkDisplayList(QVariantList landmarkNames, QVariantList landmarkVisibility, bool visualizeAsPoints);

		/// <summary>
		/// Show/hide Specific Frame
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Frame, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="name">QSting value indicating the name of Frame</param>
		void frameDisplay(bool visible, int frameNo);

		/// <summary>
		/// Updates visibility of all frames in the provided lists
		/// </summary>
		/// <param name="frameNumbers">ID numbers of frames to be updated.</param>
		/// <param name="frameVisibility">For each frame in <c>frameNumbers</c>, a bool indicating whether it should be visible or not.
		/// The order (and size) must match <c>frameNumbers</c>.</param>
		void frameDisplayList(QVariantList frameNumbers, QVariantList frameVisibility);

		/// <summary>
		/// Show/hide Specific Joint
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Joint, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="name">QSting value indicating the name of Joint</param>
		void jointDisplay(bool visible, QString name);

		/// <summary>
		/// Updates visibility of all joints in the provided lists
		/// </summary>
		/// <param name="jointNames">Names of joints to be updated.</param>
		/// <param name="jointVisibility">For each joint in <c>jointNames</c>, a bool indicating whether it should be visible or not.
		/// The order (and size) must match <c>jointNames</c>.</param>
		void jointDisplayList(QVariantList jointNames, QVariantList jointVisibility);

		/// <summary>
		/// Updates visibility of all Control Points in the provided lists
		/// </summary>
		/// <param name="names">Names of Control Points to be updated.</param>
		/// <param name="visibility">For each Control Point in <c>names</c>, a bool indicating whether it should be visible or not.
		/// The order (and size) must match <c>names</c>.</param>
		void controlPointDisplayList(QVariantList names, QVariantList visibility);

		/// <summary>
		/// Show/hide Specific Contour Body Region
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Contour Body Region, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="name">QSting value indicating the name of Contour Body Region</param>
		void contourBodyRegionDisplay(bool visible, int bodyRegion);

		/// <summary>
		/// Show/hide Specific Contour
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Contour, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="bodyRegion">Int value indicating the ID of Contour Body Region</param>
		/// <param name="contourNo">Int value indicating the Contour number</param>
		void contourDisplay(bool visible, int bodyRegion, int contourNo);

		/// <summary>
		/// Show/hide Specific Contour Point
		/// </summary>
		/// <param name="visible">Boolean value indicating the visibility of Contour point, <c>false</c> Hide, <c>true</c> Show</param>
		/// <param name="bodyRegion">Int value indicating the ID of Contour Body Region</param>
		/// <param name="contourNo">Int value indicating the Contour number</param>
		/// <param name="point_no">Int value indicating the Contour Point number</param>
		void contourPointDisplay(bool visible, int bodyRegion, int contourNo, int point_no);

		/// <summary>
		/// Show/hide Specific Target
		/// </summary>
		/// <param name="visible">Integer value indicating the visibility of Target, <c>0</c>: Hide, <c>1</c>: Show</param>
		/// <param name="name">QSting value indicating the name of Target</param>
		/// <param name="type">QSting value indicating the type of Target</param>
		void targetDisplay(bool visible, QString name, QString type);

		/// <summary>
		/// Updates visibility of all targets in the provided lists
		/// </summary>
		/// <param name="targetNames">Names of targets to be updated.</param>
		/// <param name="targetVisibility">For each joint in <c>targetNames</c>, a bool indicating whether it should be visible or not.
		/// The order (and size) must match <c>targetNames</c>.</param>
		/// <param name="targetTypes">For each joint in <c>targetNames</c>, its type.
		/// The order (and size) must match <c>targetNames</c>.</param>
		void targetDisplayList(QVariantList targetNames, QVariantList targetVisibility, QVariantList targetTypes);

		/// <summary>
		/// Sets the same opacity to all actors that are registered under the display mode FULL_MESH
		/// </summary>
		/// <param name="value">Float value indicating the opacity of actors to be set.</param>
		void setFullModelOpacity(float value);

		/// <summary>
		/// Sets the same opacity to all actors that are registered under the display mode ENTITIES
		/// </summary>
		/// <param name="value">Float value indicating the opacity of all actors that are registered under the display mode ENTITIES to be set.</param>		
		void setEntitiesOpacity(float value);

		/// <summary>
		/// Sets a given display mode as active or inactive
		/// </summary>
		/// <param name="displayMode">Unsigned int value indicating the Display Mode.
		/// DISPLAY_MODE: { FULL_MESH = 1, ENTITIES = 2, LANDMARKS = 4, POINTS = 8, AXES = 16, SELECTION_HIGHLIGHTERS = 512, ALL = 0x7FFFFFFF }</param>
		/// <param name="active">Boolean value indicating the state of display mode, <c>false</c> Inactive, <c>true</c> Active</param>
		void setDisplayModeActive(unsigned int displayMode, bool active = true);

		//DISPLAY CONFIGS ---
		void changeColor(QString color, QString name, float alpha = 0.8);
		void changeRadius(QString name, float radius);

		bool getIsModelChanged();
		void setIsModelChanged(bool val);

		/// <summary>
		/// Show/Hide the origin axis
		/// </summary>
		/// <param name="active">Boolean value indicating the visibility of origin axis, <c>false</c> Inactive, <c>true</c> Active</param>
		void displayOriginAxis(bool visible);

		QVariantMap  ui_getUndefinedContourCLElements();

		QVariantMap ui_getContourCLTree();

		QList<QString> ui_getAnatomyDBLandmarks();
		QString ui_getLandmarkDescriptionFromAnatomyDB(QString);

		QString ui_getBRDescription(QString);
		QString ui_getJointDescription(QString);

		QList<QString> ui_getCtrlPtLandmarks(QString);
		void ui_addCtrlPtLandmark(QString, QString, QString);
		void ui_removeCtrlPtLandmark(QString, QString);
		void ui_moveUpCtrlLandmark(QString, QString);
		void ui_moveDownCtrlLandmark(QString, QString);
		QList<QString> ui_getCplLandmarksDesc(QString);

		QString ui_getSkingm(QString);
		void ui_setSkingm(QString, QString);

		QList<QString> ui_getSplineInfo(QString);
		void ui_addSplineInfo(QString, QString);
		void ui_removeSplineInfo(QString, QString);
		void ui_moveUpSplineLandmark(QString, QString);
		void ui_moveDownSplineLandmark(QString, QString);
		QList<QString> ui_getSplineDesc(QString);

		QList<QString> ui_getFlexionAxesLandmarks(QString);
		void ui_addFlexionAxesLandmark(QString, QString);
		void ui_removeFlexionAxesLandmarks(QString, QString);
		QList<QString> ui_getFlexionAxesLandmarksDesc(QString);

		QList<QString> ui_getTwistAxesLandmarks(QString);
		void ui_addTwistAxesLandmark(QString, QString);
		void ui_removeTwistAxesLandmarks(QString, QString);
		QList<QString> ui_getTwistAxesLandmarksDesc(QString);

		QList<QString> ui_getBRCircumference(QString);
		void ui_addBRCircumference(QString, QString);
		void ui_removeBRCircumference(QString, QString);
		QList<QString> ui_getJointCircumference(QString);
		void ui_addJointCircumference(QString, QString);
		void ui_removeJointCircumference(QString, QString);
		QList<QString> ui_getBRCircumDesc(QString);
		QList<QString> ui_getJointCircumDesc(QString);

		void ui_highlightCLElement(QString name, QString type);

	private:
		/// <summary>
		/// Open Contour from an external .txt/.xml file and create respective actors
		/// </summary>
		/// <param name="modelFile">Path of input file</param>
		void openContour(std::string const& modelFile);

		/// <summary>
		/// Switch case to access a specific Contour Body Region vector from a given integer
		/// </summary>
		/// <param name="modelFile">Path of input file</param>
		std::vector<piper::hbm::Contour*> getBodyRegion(piper::hbm::Metadata& m_meta, int body_region);
		std::string getBodyRegionName(int body_region);

		/// <summary>
		/// Show/Hide specific Landmark 
		/// </summary>
		/// <param name="name">String indicating the name of the Landmark.</param>
		/// <param name="visible">Boolean value indicating the visibility of Landmark, <c>false</c> Hide, <c>true</c> Show</param>
		void toggleLandmarkDisplay(std::string name, bool visible);


		//HEX to RGB colorConverter

		/// <summary>
		/// Convert Hexadecimal to Decimal
		/// </summary>
		/// <param name="hex">String indicating hexadecimal to be converted.</param>
		/// <returns>Float value of Decimal value.</returns>
		float HexadecimalToDecimal(std::string hex);

		/// <summary>
		/// Convert Hexadecimal to RGB
		/// </summary>
		/// <param name="hex">String indicating hexadecimal to be converted.</param>
		/// <returns>Vector of Float value of RGB.</returns>
		std::vector<float> HexadecimalToRGB(std::string hex);

	protected:
		int maxBodyRegions = 34;

		VtkDisplayHighlighters draw;

		std::vector<std::vector<vtkSmartPointer<vtkActor>>> contourBRSet;

		std::vector<piper::hbm::Node*> vec_node;
		std::vector<piper::hbm::Node*> vec_node_new;
		std::vector<piper::hbm::NodePtr> vec_spline_new;
		std::map<int, piper::hbm::Plane*> contour_plane;
		std::map<int, piper::hbm::Plane*> contour_plane_new;
		std::map<int, piper::hbm::Contour*> contour_map;
		std::vector<piper::hbm::Contour*> vec_circle_cont;
		std::vector<piper::hbm::Contour*> vec_spline_cont;
		std::vector<piper::hbm::Contour*> vec_poly_cont;

		bool is_model_changed = false;

	};


} // namespace piper

#endif
