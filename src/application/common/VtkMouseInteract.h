/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_INTERACTORS_H_
#define PIPER_INTERACTORS_H_

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include "VtkDisplayHighlighters.h"

#include <vtkCellPicker.h>

#include <vtkInteractorStyleRubberBandPick.h>


// For vtkBoxWidget2:
#include <vtkBoxWidget2.h>
#include <vtkBoxRepresentation.h>
#include <vtkCommand.h>
#include <vtkTransform.h>
#include <vtkActorCollection.h>
#include <QVTKInteractor.h>
#include <vtkActor2D.h>

namespace piper {

    const int VTKEVENT_SURF_POINT_PICKED = 1; // code of a custom vtk event invoked after surface point has been picked

    class VtkDisplay;

    /// <summary>
    /// Serves the screen interactors to tell the VtkDisplay in what way to append or not append selected primitives (cells, points) to what is currently already selected.
    /// </summary>
    enum PICK_APPENDING_STYLE {
        APPEND_ALL, // nothing that was previously selected will be erased, the newly picked primitive will only be marked for selection
        APPEND_ACTOR, // selected primitives in the actor to which the newly picked one belongs will be preserved as selected, but primitives in all other actors will be reset
        DESELECT_ACTOR, // all primitives in the actor to which the newly picked one belongs will be deselected (except the new one of course), but primitives belonging to other actors will be left intact
        DESELECT_ALL // all primitives in all actors will be deselected, the newly picked primitive will be the only one marked as selected in the end
    };

    enum INTERACT_ACTIONS {
        ROTATE, ZOOM, PICK, PAN, NONE
    };

    /// <summary>
    /// Interactor style for basic mouse picking of elements, points and whole meshes.
    /// </summary>
    /// <seealso cref="vtkInteractorStyleTrackballCamera" />
    class PIPERCOMMON_EXPORT vtkMousePickStylePIPER : public vtkInteractorStyleTrackballCamera
	{
	public:
        
        /// <summary>
        /// Create a new instance of this picker. Be sure to set the parent display using SetParentDisplay()
        /// </summary>
        /// <returns>New instance of vtkMousePickStylePIPER</returns>
        static vtkMousePickStylePIPER* New();
        vtkTypeMacro(vtkMousePickStylePIPER, vtkInteractorStyleTrackballCamera)

        vtkMousePickStylePIPER();
        ~vtkMousePickStylePIPER();

		virtual void OnLeftButtonDown();
		virtual void OnLeftButtonUp();
		virtual void OnRightButtonDown();
		virtual void OnRightButtonUp();
		virtual void OnMiddleButtonDown();
		virtual void OnMiddleButtonUp();
        virtual void OnMouseWheelForward();
        virtual void OnMouseWheelBackward();
        virtual void OnMouseMove();

        /// <summary>
        /// Notifies the interactor that the next event shall be processed as a double click. Needs to be called before invoking the actual event
        /// - a neccessary "hack" as the vtk apparently does not have a built in support for recognizing double clicks (and implementing it correctly,
        /// i.e. with respecting operation system settings of what should be considered double click etc. is too complicated).
        /// The double click flag will automatically be reset after processing the next event.
        /// </summary>
        void SetIsDoubleClick();
        
        /// <summary>
        /// Sets the parent VtkDisplay.
        /// </summary>
        /// <param name="parent">The parent VtkDisplay that uses this interactor. Selection results are passed to it when the interactor evaluates them.</param>
        void SetParentDisplay(VtkDisplay *parent);
        
        /// <summary>
        /// Initializes picking of "free points", i.e. any point on the surface of a mesh. When user clicks the pick button (default LMB),
        /// the coordinates of the pick will be sent along with the (vtkCommand::UserEvent + VTKEVENT_SURF_POINT_PICKED) signal.
        /// </summary>
        void InitSurfacePointPick();

        /// <summary>
        /// Sets the type of the picking.
        /// </summary>
        /// <param name="target">The target to pick (points, elements etc.).</param>
        /// <param name="activateRubberBand">If set to <c>true</c>, picking will be done using the rubberband, otherwise a simple one-click picking will be used.</param>
        /// <param name="activateBox">If set to <c>true</c> picking will be done using a box - user draws a rectangle, a box is created from near plane to far
        /// plane of the camera and everything inside it is selected.</param>
        /// <returns>A text with a hint for user on how to use the given picking type.</returns>
        std::string SetPickingType(hbm::SELECTION_TARGET target, bool activateRubberBand, bool activateBox, bool activeDrag = false);
		void PickEntity();        
        /// <summary>
        /// Gets the coordinates of the points on the closest surface right beneath the cursor and writes it to the freePointTarget buffer.
        /// Call InitSurfacePointPick before this to set up the buffer, otherwise the pick will be ignored.
        /// </summary>
        void PickSurfacePoint();
        void CreatePointWC();
		void PickElement();        
        /// <summary>
        /// For picking nodes of a mesh.
        /// </summary>
        void PickNode();        
        /// <summary>
        /// For picking any free points / landmarks - assumes that points are represented by a vtkGlyph3DMapper.
        /// </summary>
        void PickPoint();        
		void setStyleData(vtkPolyData* pStyleData);

		// actions that can be bound to buttons        
		void Picking();
		void InitPanning();
		void EndPanning();
		void InitRotation();
		void EndRotation();
        void ZoomIn();
        void ZoomOut();

		//metaEditor
		std::vector<vtkSmartPointer<vtkCellPicker>> getSelectedElements();
		std::vector<vtkSmartPointer<vtkActor>> getSelectedActors();
		vtkSmartPointer<vtkActor> selectedActor;

	private:
        
        /// <summary>
        /// Performs the action bound to the button with the specified name.
        /// </summary>
        /// <param name="buttonName">Name of the button - "LMB" for left mouse button, "RMB" for right, "MMB" for middle.</param>
        /// <param name="buttonDown">If set to <c>true</c>, it will call the action bound for pressing down the button. If set to <c>false</c>,
        /// the action for button up (release) will be called.</param>
        /// <param name="shiftKey">Indicate if shift was held while pressing the button that initiated the action.</param>
        /// <param name="ctrlKey">Indicate if control was held while pressing the button that initiated the action.</param>
        /// <param name="doubleClick">Indicate whether the input was single or double click.</param>
        void performBoundAction(std::string buttonName, bool buttonDown);
        
        /// <summary>
        /// Gets the pick appending style based on what keys are held.
        /// TODO - make it possible to change the mappings of the keys. Currently the hard-coded mappings are:
        /// APPEND_ALL if shift is held, APPEND_ACTOR if alt is held, DESELECT_ALL if ctrl is held and DESELECT_ACTOR as default (nothing held).
        /// </summary>
        /// <returns>The pick appending style - it controls what to do with primitives that were selected previously.</returns>
        /// <seealso cref="PICK_APPENDING_STYLE" />
        PICK_APPENDING_STYLE getPickAppendingStyle();
        
        /// <summary>
        /// Initializes drawing of the rubber band - a yellow rectangle based on the mouse position
        /// </summary>
        void initDrawRubberBand();
        
        /// <summary>
        /// Redraws the rubber band rectangle based on the current mouse position - must be stored in the RBP_currPosition.
        /// </summary>
        void redrawRubberBandRectangle();

		void movedSelectedActor();
      
        void initRubberBandPicking(int startPosition[2]);
        void endRubberBandPicking();
        // called during mouse move when rubberband picking is going on
        void updateRubberBandPick(int pickPos[2]);

        void initBoxPicking(int startPosition[2]);
        void endBoxPicking();
        // called during mouse move when box picking is going on
        void updateBoxPick(int pickPos[2]);

		void initActorDragging(int startPosition[2]);
		void endActorDragging();
		void updateActorDrag(int startPosition[2]);

        void doSelectByBox();


		vtkActor    *LastPickedActor;
		vtkProperty *LastPickedProperty;
		hbm::SELECTION_TARGET targetType;

        VtkDisplay *parent; // the parent display which to notify about changes in selection results


		vtkSmartPointer<vtkPolyData> Data;

		std::map<std::string, INTERACT_ACTIONS> actionBinding; // a map that binds mouse/keyboard buttons to actions

        bool doubleClick = false;

		std::vector<vtkSmartPointer<vtkCellPicker>> selectedElements;
		std::vector<vtkSmartPointer<vtkActor>> selectedPoints;
		vtkSmartPointer<vtkActorCollection> selectedActors = vtkSmartPointer<vtkActorCollection>::New();
		vtkSmartPointer<vtkPolyData> Points;

        VtkDisplayHighlighters draw;

        // for rubberbandpicking
        int RBP_startPosition[2];
        int RBP_currPosition[2];
        vtkSmartPointer<vtkActor2D> RBP_Actor;
        bool RBP_active = false; // is true only when the mouse is down and the rubberband picking is being done
        bool useRBP = false; // set to true to activate RBP
        bool useBox = false; // set to true to activate box

		// for actor dragging
		bool useDrag = false; // set to true to activate mouse drag
		int AD_startPosition[3];
		int AD_currPosition[3];
		vtkSmartPointer<VtkDisplayActor>AD_Actor = vtkSmartPointer<VtkDisplayActor>::New();

        double freePointTarget[3]; // for free point picking - a buffer to which the next picked point is written

		struct point{
			double cords[3];
		};

		struct element {
			int Type;
			std::vector <point> Points;
		};

		std::vector<element> activeElements;

		vtkSmartPointer<VtkDisplayActor> actorSelected;
	};

	class vtkBoxCallback : public vtkCommand
	{
	public:
		static vtkBoxCallback *New()
		{
			return new vtkBoxCallback;
		}

		vtkSmartPointer<vtkActor> m_actor;

		void SetActor(vtkSmartPointer<vtkActor> actor)
		{
			m_actor = actor;
		}

		virtual void Execute(vtkObject *caller, unsigned long, void*)
		{
			vtkSmartPointer<vtkBoxWidget2> boxWidget =
				vtkBoxWidget2::SafeDownCast(caller);

			vtkSmartPointer<vtkTransform> t =
				vtkSmartPointer<vtkTransform>::New();

			vtkBoxRepresentation::SafeDownCast(boxWidget->GetRepresentation())->GetTransform(t);
			this->m_actor->SetUserTransform(t);
		}

		vtkBoxCallback(){}
	};

}

#endif //PIPER_INTERACTORS_H_
