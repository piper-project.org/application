/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HIHGLIGHTERS_H
#define PIPER_HIHGLIGHTERS_H


#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif


#include <QObject>
#include <QString>
#include <QQmlApplicationEngine>

#include <string>
#include <boost/container/list.hpp>

#include <vtkOpenGLActor.h>
#include "lib/hbm/FEModelVTK.h"
#include "lib/hbm/VtkSelectionTools.h"
#include <vtkAxesActor.h>
#include <vtkColor.h>
#include <lib/hbm/Node.h>

#include <memory>


class vtkLookupTable;
class vtkGlyph3DMapper;
class vtkFloatArray;

namespace piper {

    const std::string SELECTION_BOX_ACTOR = "piper_cur_selection_box";
    

    /// <summary>
    /// Transforms a color in the 4x8-bit format AARRGGBB to four float values in the range 0 (= 0) - 1 (=255).
    /// </summary>
    /// <param name="color">The color in UINT format.</param>
    /// <param name="a">The alpha channel of color as float.</param>
    /// <param name="r">The r channel of color as float.</param>
    /// <param name="g">The g channel of color as float.</param>
    /// <param name="b">The b channel of color as float.</param>
    PIPERCOMMON_EXPORT inline void colorFormatTransferUINTtoFLOAT(unsigned int color, float *a, float *r, float *g, float *b)
    {
        *a = ((color & 0xFF000000) >> 24) / 255.0;
        *r = ((color & 0x00FF0000) >> 16) / 255.0;
        *g = ((color & 0x0000FF00) >> 8) / 255.0;
        *b = (color & 0x000000FF) / 255.0;
    }

    /// <summary>
    /// Transforms a color from four float values in the range 0 (= 0) - 1 (=255) to the 4x8-bit format AARRGGBB.
    /// </summary>
    /// <param name="color">The resulting color in UINT format.</param>
    /// <param name="a">The alpha channel of color as float.</param>
    /// <param name="r">The r channel of color as float.</param>
    /// <param name="g">The g channel of color as float.</param>
    /// <param name="b">The b channel of color as float.</param>
    PIPERCOMMON_EXPORT inline void colorFormatTransferFLOATtoUINT(unsigned int *color, float a, float r, float g, float b)
    {
        *color = ((unsigned int)(a * 255) << 24) | ((unsigned int)(r * 255) << 16) | ((unsigned int)(g * 255) << 8) | (unsigned int)(b * 255);
    }

    /// <summary>
    /// A wrapper for vtkActor with several additional information about the object it represents.
    /// This structure is used by VtkDisplay to describe any object within it.
    /// </summary>
    class PIPERCOMMON_EXPORT VtkDisplayActor : public vtkOpenGLActor
    {
    public:
        static VtkDisplayActor *New();
        vtkTypeMacro(VtkDisplayActor, vtkOpenGLActor);

        void SetName(std::string name) { this->Name = name; }
        std::string GetName() { return this->Name; }
        
        /// <summary>
        /// If this is set to <c>true</c>, the actor will be treated as if it has no cells, even if the parent data set has some.
        /// This is relevant mainly for picking - actors that are group of points won't be processed during cell picking.
        /// </summary>
        /// <returns><c>True</c> if this actor represents glyphed group of points.</returns>
        bool GetIsGroupOfPoints() { return this->IsGroupOfPoints; }
        void SetIsGroupOfPoints(bool isGOP) { this->IsGroupOfPoints = isGOP; }

        void SetIsSelected(bool isSelected) { this->IsSelected = isSelected; }
        bool GetIsSelected() { return this->IsSelected; }

        void SetIsPersistent(bool persistent) { this->IsPersistent = persistent; }
        bool GetIsPersistent() { return this->IsPersistent; }

        int GetNoSelectedCells() { return noOfSelectedCells; }
        void CellsSelected(int noOfCells) { this->noOfSelectedCells += noOfCells; }
        void CellsDeselected(int noOfCells) { this->noOfSelectedCells -= noOfCells; }
        bool HasSelectedCells() { return this->noOfSelectedCells > 0; }
        void AllCellsDeselected() { this->noOfSelectedCells = 0; }
        void AllCellsSelected() { this->noOfSelectedCells = originalDataSet->GetNumberOfCells(); }

        int GetNoSelectedPoints() { return noOfSelectedPoints; }
        void PointsSelected(int noOfPoints) { this->noOfSelectedPoints += noOfPoints; }
        void PointsDeselected(int noOfPoints) { this->noOfSelectedPoints -= noOfPoints; }
        bool HasSelectedPoints() { return this->noOfSelectedPoints > 0; }
        void AllPointsDeselected() { this->noOfSelectedPoints = 0; }
        void AllPointsSelected() { this->noOfSelectedPoints = originalDataSet->GetNumberOfPoints(); }


        void SetDisplayMode(unsigned int displayMode) { this->DisplayMode = displayMode; }
        unsigned int GetDisplayMode() { return this->DisplayMode; }

        void SetBaseColor(vtkColor4d baseColor) { this->BaseColor = baseColor; }
        vtkColor4d *GetBaseColor() { return &this->BaseColor; }
        
        /// <summary>
        /// Gets the list of other actors that together with this one create one object. 
        /// </summary>
        /// <returns>Usually this will be empty, but e.g. for axes, this will contain the
        /// different parts of the frame - shafts and tips.</returns>
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> *GetOtherParts() { return &this->otherParts; }
        
        /// <summary>
        /// Add a specified actor to the list of parts that together with this actor create one object.
        /// Call this if this actor is created to represent some part of a frame (an axis shaft or tip).
        /// </summary>
        void AddPart(vtkSmartPointer<VtkDisplayActor> otherPart) { this->otherParts.push_back(otherPart); }
        
        /// <summary>
        /// If this actor represents a part of an axis, this will be pointing to the original axes actor that was used to create it
        /// </summary>
        /// <returns>Axes actor that of which one part was used to create this actor. NULL if this actor does not belong to a frame.</returns>
        vtkSmartPointer<vtkAxesActor> GetParentAxesActor() { return this->parentAxesActor; }
        
        /// <summary>
        /// Sets the parent axes actor - call this if this actor is a part of a frame and the parent is the vtkAxesActor representation of that frame.
        /// </summary>
        /// <param name="parent">The parent axes actor.</param>
        void SetParentAxesActor(vtkSmartPointer<vtkAxesActor> parent) { this->parentAxesActor = parent; }
        
        /// <summary>
        /// Sets the original data set, which is not necessarily the input of this actor - it is what the input to this actor is based on.
        /// Most commonly, this will be a vtkUnstructuredGrid containing a FE model BEFORE it was processed by vtkDataSetSurfaceFilter and used for this actor
        /// </summary>
        /// <param name="originalDataSet">The original data set.</param>
        void SetOriginalDataSet(vtkSmartPointer<vtkPointSet> originalDataSet) { this->originalDataSet = originalDataSet; }        
        /// <summary>
        /// Gets the original data set, which is not necessarily the input of this actor - it is what the input to this actor is based on.
        /// Most commonly, this will be a vtkUnstructuredGrid containing a FE model BEFORE it was processed by vtkDataSetSurfaceFilter and used for this actor
        /// </summary>
        /// <returns>The original dataset.</returns>
        vtkSmartPointer<vtkPointSet> GetOriginalDataSet() { return this->originalDataSet; }
        
        vtkSmartPointer<VtkDisplayActor> GetPointHighlights() { return this->pointHighlights; }
        void SetPointHighlights(vtkSmartPointer<VtkDisplayActor> actor) { this->pointHighlights = actor; }

        vtkSmartPointer<VtkDisplayActor> GetSelectedPointHighlights() { return this->selectedPointHighlights; }
        void SetSelectedPointHighlights(vtkSmartPointer<VtkDisplayActor> actor) { this->selectedPointHighlights = actor; }

        vtkSmartPointer<VtkDisplayActor> GetNormalHighlights() { return this->normalHighlights; }
        void SetNormalHighlights(vtkSmartPointer<VtkDisplayActor> actor) { this->normalHighlights = actor; }
        
        /// <summary>
        /// Parent actor - used for blanking of elements. This actor shares the same name and original dataset,
        /// basically it is a derived copy of the parent actor.
        /// </summary>
        /// <returns>The actor that was used as a base for this actor.</returns>
        vtkSmartPointer<VtkDisplayActor> GetBlankParent() { return this->blankParent; }
        
        /// <summary>
        /// Sets the blank parent actor.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="blankLevel">The blank level - number indicating which operation created this actor.</param>
        void SetBlankParent(vtkSmartPointer<VtkDisplayActor> actor, int blankLevel) { this->blankParent = actor; this->currentBlankLevel = blankLevel; }
        
        /// <summary>
        /// Gets the blank level - a number indicating by which blanking operation the current blankParent was created.
        /// </summary>
        /// <returns>The blank level, 0 if no blanking was done on this actor.</returns>
        int GetBlankLevel() { return this->currentBlankLevel; }
        
        /// <summary>
        /// If it is not NULL, this model has been used to create this actor.
        /// </summary>
        /// <returns>The FE model that was used to create this actor.</returns>
        hbm::FEModelVTK *GetFEModelWithLandmarkInfo() { return this->modelContainingLandmarkInfo; }
        
        /// <summary>
        /// Sets the FE model that (among other things) contains the landmark information. Use this only if you want this actor to be treated as
        /// a set of groups of points that define landmarks, otherwise leave it blank even if the FEModelVTK's mesh is used to create this actor!
        /// </summary>
        /// <returns>The FE model that was used to create this actor.</returns>
        void SetFEModelWithLandmarkInfo(hbm::FEModelVTK *model) { this->modelContainingLandmarkInfo = model; }

    protected:

        VtkDisplayActor() 
        {
            pointHighlights = NULL; selectedPointHighlights = NULL;
            originalDataSet == NULL; parentAxesActor = NULL;
            modelContainingLandmarkInfo = NULL; blankParent = NULL;
            IsGroupOfPoints = false;
        }
        ~VtkDisplayActor(){}

        std::string Name;
        bool IsSelected, IsPersistent, IsGroupOfPoints;
        unsigned int noOfSelectedCells = 0;
        unsigned int noOfSelectedPoints = 0;
        unsigned int DisplayMode;
        // holds its original color - used with selection to roll back from the "selection highlight color" to the original color after the actor is de-selected
        vtkColor4d BaseColor;
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> otherParts; // list containing other actors that belong to this one if it is an object made of more parts - used e.g. for axes
        vtkSmartPointer<vtkAxesActor> parentAxesActor; // if this actor represents a part of an axis, this will be pointing to the original axes actor that was used to create it
        // this is a link to the very original data set before any vtk processing is done to it. 
        // most commonly, this will be a vtkUnstructuredGrid containing a FE model BEFORE it was processed by vtkDataSetSurfaceFilter and used for this actor
        vtkSmartPointer<vtkPointSet> originalDataSet;
        hbm::FEModelVTK *modelContainingLandmarkInfo;

        vtkSmartPointer<VtkDisplayActor> pointHighlights;
        vtkSmartPointer<VtkDisplayActor> selectedPointHighlights;
        vtkSmartPointer<VtkDisplayActor> normalHighlights;

        vtkSmartPointer<VtkDisplayActor> blankParent;
        int currentBlankLevel = 0;
    };

    /// <summary>
    /// A class providing supporting graphic primitives for the VtkDisplay to highlight selection results and render miscellaneous objects (frames, planes etc.).
    /// </summary>
    class PIPERCOMMON_EXPORT VtkDisplayHighlighters
	{
		public:

            VtkDisplayHighlighters();
            ~VtkDisplayHighlighters();

            vtkSmartPointer<VtkDisplayActor> plane(piper::hbm::Node nodeorigin, piper::hbm::Node nodefirstdir, piper::hbm::Node nodeseconddir);
            vtkSmartPointer<vtkAxesActor> frame(piper::hbm::Node nodeorigin, piper::hbm::Node nodefirstdir, piper::hbm::Node nodeseconddir);
			vtkSmartPointer<vtkAxesActor> normalFrame(piper::hbm::Coord coord);
            vtkSmartPointer<vtkAxesActor> frame(const double transformationMatrix[16], double const& size);
            vtkSmartPointer<VtkDisplayActor> cube(double center[3]);
            
            
            /// <summary>
            /// Creates a new mapper for sphere glyphs to represent all points of the parent data set.
            /// Note - you can use this to visualize only a set of points not tied to any other dataset - simply create a "dummy" vtkPolyData with
            /// no cells, only points, give it the set of points you want to visualize and then call AllPoints on it!
            /// </summary>
            /// <param name="sourceData">The data containing the points on which to render the glyphs. Use the Mask array to get only a subset of points (by default, masking is off).</param>
            /// <param name="radius">The radius of the sphere representing a point.</param>
            /// <returns>The mapper that represents all points of the parent dataset.</returns>
            vtkSmartPointer<vtkGlyph3DMapper> AllPoints(vtkSmartPointer<vtkDataSet> sourceData, double radius);
            
            /// <summary>
            /// Creates a new mapper for arrow glyphs to represent cell normals of the specified data set.
            /// The input of the mapper will be a newly created vtkPolyData containing only points, one for each centroid of each cell
            /// of the sourceData. Normals have their beginning in those centroids.
            /// </summary>
            /// <param name="sourceData">The source data.</param>
            /// <param name="scale">The scale factor to apply to the arrows representing the normal. With scale == 1, the arrow will be 1 [current unit] long.</param>
            /// <returns>Mapper representing all normals of the parent dataset.</returns>
            vtkSmartPointer<vtkGlyph3DMapper> CellNormals(vtkSmartPointer<vtkPolyData> sourceData, double scale);
            
            /// <summary>
            /// Creates a sphere at a specified location as a highlighter for a point. A unique name in the form of "point_x_y_z" is generated
            /// and stored in the returned actor (accesible by GetName()), where x,y,z are the provided coordinates.
            /// WARNING - adding a large number of actors to a vtk scene causes SIGNIFICANT slowdown. If you need to visualize a large number of points,
            /// consider using the AllPoints highlighter, which creates only one actor for a whole set of points.
            /// </summary>
            /// <param name="x">The x coordinate of the center of the sphere.</param>
            /// <param name="y">The y coordinate of the center of the sphere.</param>
            /// <param name="z">The z coordinate of the center of the sphere.</param>
            /// <param name="radius">The radius of the sphere.</param>
            /// <param name="r">The red component of color.</param>
            /// <param name="g">The green component of color.</param>
            /// <param name="b">The blue component of color.</param>
            /// <returns>A VtkDisplay actor of the sphere with the selected color, radius and named based on the coordinates.</returns>
            vtkSmartPointer<VtkDisplayActor> point(double x, double y, double z, double radius, float r = 1, float g = 1, float b = 1);

            vtkSmartPointer<VtkDisplayActor> polyLine(vtkSmartPointer<vtkPoints> points);
            vtkSmartPointer<VtkDisplayActor> polygon(vtkSmartPointer<vtkPoints> picker);


            /// <summary>
            /// Creates a color look-up table for only two colors - useful for color-coding boolean scalar data.
            /// Colors are floats in the 0 (dark) - 1 (light) range.
            /// </summary>
            /// <param name="r1">Red channel for the first value.</param>
            /// <param name="g1">Green channel for the first value.</param>
            /// <param name="b1">Blue channel for the first value.</param>
            /// <param name="a1">Alpha channel for the first value (1=opaque, 0=transparent).</param>
            /// <param name="r2">Red channel for the second value.</param>
            /// <param name="g2">Green channel for the second value.</param>
            /// <param name="b2">Blue channel for the second value.</param>
            /// <param name="a2">Alpha channel for the second value (1=opaque, 0=transparent).</param>
            /// <param name="value1">The "key" value associated with the first color. Default 0 ("false").</param>
            /// <param name="value1">The "key" value associated with the second color. Default 1 ("true").</param>
            /// <returns>The created look-up table.</returns>
            vtkSmartPointer<vtkLookupTable> CreateBinaryColorLUT(float r1, float g1, float b1, float a1,
                float r2, float g2, float b2, float a2, int value1 = 0, int value2 = 1);

            /// <summary>
            /// Creates a color look-up table for only two colors - useful for color-coding boolean scalar data.
            /// Colors are floats in the 0 (dark) - 1 (light) range.
            /// </summary>
            /// <param name="r1">Red channel for the first value.</param>
            /// <param name="g1">Green channel for the first value.</param>
            /// <param name="b1">Blue channel for the first value.</param>
            /// <param name="a1">Alpha channel for the first value (1=opaque, 0=transparent).</param>
            /// <param name="color2">Color in the unsigned 4x8-bit format - AARRGGBB.</param>
            /// <param name="value1">The "key" value associated with the first color. Default 0 ("false").</param>
            /// <param name="value1">The "key" value associated with the second color. Default 1 ("true").</param>
            /// <returns>The created look-up table.</returns>
            vtkSmartPointer<vtkLookupTable> CreateBinaryColorLUT(float r1, float g1, float b1, float a1,
                unsigned int color2, int value1 = 0, int value2 = 1)
            {
                float r2, g2, b2, a2;
                colorFormatTransferUINTtoFLOAT(color2, &a2, &r2, &g2, &b2);
                return CreateBinaryColorLUT(r1, g1, b1, a1, r2, g2, b2, a2, value1, value2);
            }

            /// <summary>
            /// Creates a color look-up table for color coding scalar data.
            /// Colors are floats in the 0 (dark) - 1 (light) range.
            /// The size of the lut is deduced from the size of the color parameter (colors->GetNumberOfTuples()).
            /// </summary>
            /// <param name="colors">The colors, each item must be a quadruple of r,g,b,a for the particular color
            /// (use setNumberOfComponents(4) and then fill the array with 4-tuples.).</param>
            /// <param name="values">The "key" values to map the colors.</param>
            /// <returns>The created look-up table.</returns>
            vtkSmartPointer<vtkLookupTable> CreateColorLUT(vtkFloatArray *colors, vtkIntArray *values);


            /// <summary>
            /// Retrieves the list of selection boxes managed by this instance of highlighters.
            /// </summary>
            /// <returns>Boxes are mapped by their index which is used to create the actor name as SELECTION_BOX_ACTORin, 
            /// where SELECTION_BOX_ACTOR is a macro defined in VtkDisplayHighlighters.h and in is the index - there are no spaces or other characters in between.</returns>
            boost::container::map<int, vtkOBBNodePtr> *GetSelectionBoxes();

            /// <summary>
            /// Adds the specified selection box to the collection of boxes in the scene. The box will be managed by this instance of highlighters.
            /// Note that this does not perform any actual selection, only adds the highlighter!
            /// </summary>
            /// <param name="obb">The box origin as an isntance vtkOBBNode.</param>
            /// <returns>A VtkDisplay actor of the selection box.</returns>
            vtkSmartPointer<VtkDisplayActor> SelectionBox(vtkOBBNodePtr obb);

            /// <summary>
            /// Creates an instance of vtkOBBNode and then added based on the specified origin and axes
            /// and adds is to the collection of boxes in the scene. The box will be managed by this instance of highlighters.
			/// The color will be default selection color and opacity = 0.5.
            /// Note that this does not perform any actual selection, only adds the highlighter!
            /// </summary>
            /// <param name="boxOrigin">The box origin. </param>
            /// <param name="boxAxes">The box axes (vectors originating in the origin and defining the three box axes).</param>
            /// <returns>A VtkDisplay actor of the selection box.</returns>
            vtkSmartPointer<VtkDisplayActor> SelectionBox(double boxOrigin[3], double boxAxes[3][3]);
            
            /// <summary>
            /// Creates a rectangle representing selection plane, limited by a specified size.
            /// The color will be default selection color and opacity = 0.5.
            /// Note that this does not perform any actual selection, only adds the highlighter!
            /// </summary>
            /// <param name="center">Center of the plane.</param>
            /// <param name="firstdir">One direction of the rectangle.</param>
            /// <param name="seconddir">Other direction of the rectangle. Should be perpendicular to get an actual rectangle.</param>
            /// <param name="sizeFirstDir">Length of edge in the first direction.</param>
            /// <param name="sizeSecondDir">Length of edge in the second direction.</param>
            /// <returns>A VtkDisplay actor of the plane.</returns>
            vtkSmartPointer<VtkDisplayActor> SelectionRectangle(double center[3], double firstdir[3], double seconddir[3], double sizeFirstDir, double sizeSecondDir);

            /// <summary>
            /// Selects the next selection box. The ID of the selected box is stored in the selectedBoxID. -1 if there are no boxes
            /// </summary>
            void SelectNextSelectionBox();

            /// <summary>
            /// Selects the next selection box. The ID of the selected box is stored in the selectedBoxID. -1 if there are no boxes
            /// </summary>
            void SelectPrevSelectionBox();
            
            /// <summary>
            /// Removes the selected box. Resets selectedBoxID to -1.
            /// </summary>
            void RemoveSelectedBox();

            // visualization parameters
            unsigned int _p_colorOfSelection = 0xFF00DD00;// color by which to mark selected features in the ARGB 32-bit format, default green - 00DD00
            // color by which to color highlighters (non-selected), e.g. spheres highlighting mesh points etc. in the ARGB 32-bit format
            unsigned int _p_defaultHighlighterColor = 0xFF0000DD;
            unsigned int _p_secondHighlighterColor = 0xFF00DDDD;
            unsigned int _p_LandmarkColor = 0xFFB2B200;
            bool boxesVisible = true; // all new boxes will be added visible or invisible based on this value
            int selectedBoxID = -1; // currently selected box that will be modified by the methods that work with "Selected box"
    protected:
        // mapped by index, index is used to generate name of actor 
        // => simple vector will not do (removing element would cause re-indexing and that would make it unable to retrieve correct name)
        boost::container::map<int, vtkOBBNodePtr> selectionBoxes;
	};
}


#endif//PIPER_HIHGLIGHTERS_H
