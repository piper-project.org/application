/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "VtkMouseInteract.h"
#include "Context.h"
#include "message.h"
#include <boost/container/list.hpp>

#include <vtkPolyDataMapper2D.h>
#include <vtkProperty2D.h>
#include <vtkDataSetMapper.h>
#include <vtkObjectFactory.h>
#include <vtkProperty.h>
#include <vtkMath.h>
#include <vtkRenderer.h>

//for the legacy rubberband picking - delete if it gets removed completely
#include <vtkExtractGeometry.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkPlanes.h>
#include <vtkAreaPicker.h>

#include <QtConcurrent/QtConcurrent>

#include <vtkSphereSource.h>
#include <vtkAlgorithmOutput.h>

using namespace piper::hbm;

namespace piper {

    const std::string ACTION_LMB_CLICK = "LMB";
    const std::string ACTION_RMB_CLICK = "RMB";
    const std::string ACTION_MMB_CLICK = "MMB";
    // double click codes must be single click + "D"
    const std::string ACTION_LMB_DOUBLECLICK = "LMBD";
    const std::string ACTION_RMB_DOUBLECLICK = "RMBD";
    const std::string ACTION_MMB_DOUBLECLICK = "MMBD";
    const std::string ACTION_MW_SCROLL = "MW";

	vtkMousePickStylePIPER::vtkMousePickStylePIPER()
	{
        parent = NULL;
		LastPickedActor = NULL;
		LastPickedProperty = vtkProperty::New();

		// bind actions to the default buttons - TODO - load from a configuration file?
		actionBinding[ACTION_RMB_CLICK] = INTERACT_ACTIONS::ROTATE;
		actionBinding[ACTION_MMB_CLICK] = INTERACT_ACTIONS::PAN;
		actionBinding[ACTION_LMB_CLICK] = INTERACT_ACTIONS::PICK;
        actionBinding[ACTION_MW_SCROLL] = INTERACT_ACTIONS::ZOOM; // mouse wheel
        actionBinding[ACTION_LMB_DOUBLECLICK] = INTERACT_ACTIONS::NONE; // left mouse double click
        actionBinding[ACTION_MMB_DOUBLECLICK] = INTERACT_ACTIONS::NONE; // middle mouse double click
        actionBinding[ACTION_RMB_DOUBLECLICK] = INTERACT_ACTIONS::NONE; // right mouse double click
	}

	vtkMousePickStylePIPER::~vtkMousePickStylePIPER()
	{
		LastPickedProperty->Delete();
	}

	void vtkMousePickStylePIPER::OnLeftButtonDown()
	{
        performBoundAction(ACTION_LMB_CLICK, true);
	}

	void vtkMousePickStylePIPER::OnLeftButtonUp()
	{
		performBoundAction(ACTION_LMB_CLICK, false);
	}

	void vtkMousePickStylePIPER::OnRightButtonDown()
	{
		performBoundAction(ACTION_RMB_CLICK, true);
	}

	void vtkMousePickStylePIPER::OnRightButtonUp()
	{
        performBoundAction(ACTION_RMB_CLICK, false);
	}

	void vtkMousePickStylePIPER::OnMiddleButtonDown()
	{
        performBoundAction(ACTION_MMB_CLICK, true);
	}

	void vtkMousePickStylePIPER::OnMiddleButtonUp()
	{
        performBoundAction(ACTION_MMB_CLICK, false);
	}

    void vtkMousePickStylePIPER::OnMouseWheelForward()
    {
        performBoundAction(ACTION_MW_SCROLL, false); // buttonUp false == wheel forward
    }

    void vtkMousePickStylePIPER::OnMouseWheelBackward()
    {
        performBoundAction(ACTION_MW_SCROLL, true);// buttonUp true == wheel backward
    }


    void vtkMousePickStylePIPER::OnMouseMove()
    {
        if (this->State == VTKIS_PAN)
        {
            int last[2];
            Interactor->GetLastEventPosition(last); // setting new current position will overwrite the last, so save the real last position and then restore it
            Interactor->SetEventPositionFlipY(Interactor->GetEventPosition());
            Interactor->SetLastEventPosition(last);
        }
        Superclass::OnMouseMove();
        // if the rubberbandpickign is active, update the picks
        if (RBP_active) // both box and rubberband picking use the same flag - they cant both be active at the same time anyway
        {
            // switch Y axis - the picker uses the VTK coordinate system which has reverted Y axis
            Interactor->SetEventPositionFlipY(Interactor->GetEventPosition());
            if (useRBP)
                updateRubberBandPick(Interactor->GetEventPosition());
            else if (useBox)
                updateBoxPick(Interactor->GetEventPosition());
			else if (useDrag)
				updateActorDrag(Interactor->GetEventPosition());
        }
    }


    void vtkMousePickStylePIPER::SetIsDoubleClick()
    {
        doubleClick = true;
    }

	/// <summary>
	/// Performs the action bound to the button with the specified name.
	/// </summary>
	/// <param name="buttonName">Name of the button - "LMB" for left mouse button, "RMB" for right, "MMB" for middle.</param>
	/// <param name="buttonDown">If set to <c>true</c>, it will call the action bound for pressing down the button. If set to <c>false</c>,
	/// the action for button up (release) will be called.</param>
    void vtkMousePickStylePIPER::performBoundAction(std::string buttonName, bool buttonDown)
	{
        if (doubleClick) // if it is a doubleclick, append D -> different actions can be mapped to LMBD then to LMB etc.
            buttonName.append("D");

		if (actionBinding[buttonName] == ROTATE)
		{
            if (buttonDown) InitRotation();
            else EndRotation();
		}
		else if (actionBinding[buttonName] == PAN)
        {
            Interactor->SetEventPositionFlipY(Interactor->GetEventPosition());
            Interactor->SetLastEventPosition(Interactor->GetEventPosition());
            if (buttonDown) InitPanning();
            else EndPanning();            
		}
		else if (actionBinding[buttonName] == PICK)
		{
            // switch Y axis - the picker uses the VTK coordinate system which has reverted Y axis
            Interactor->SetEventPositionFlipY(Interactor->GetEventPosition());
            if (buttonDown)
            {
                if (useRBP) 
                    initRubberBandPicking(Interactor->GetEventPosition());
                else if (useBox)
                    initBoxPicking(Interactor->GetEventPosition());
				else if (useDrag)
					initActorDragging(Interactor->GetEventPosition());
                else Picking();
            }
            else if (useRBP) // on mouse button up and when using RBP, end RBP
                endRubberBandPicking();
            else if (useBox)
                endBoxPicking();
			else if (useDrag)
				endActorDragging();
		}
        else if (actionBinding[buttonName] == ZOOM)
        {
            if (buttonDown) ZoomOut();
            else ZoomIn();
        }

        // reset doubleClick
        doubleClick = false;
	}

    PICK_APPENDING_STYLE vtkMousePickStylePIPER::getPickAppendingStyle()
    {
        // choose the appropriate appending style based on what keys are held
        if (this->Interactor->GetShiftKey())
            return PICK_APPENDING_STYLE::APPEND_ALL;
        else if (this->Interactor->GetControlKey())
            return PICK_APPENDING_STYLE::DESELECT_ACTOR;
        else if (this->Interactor->GetAltKey())
            return PICK_APPENDING_STYLE::DESELECT_ALL;
        return PICK_APPENDING_STYLE::APPEND_ACTOR;
    }

	/// <summary>
	/// Starts the picking routine.
	/// </summary>
	void vtkMousePickStylePIPER::Picking()
	{
        if (parent) // if no VtkDisplay is associated, there is no point in picking
        {
            switch (targetType)
            {
            case SELECTION_TARGET::NODES:
                PickNode();
                break;

            case SELECTION_TARGET::FACES:
                pInfo() << WARNING << "Selection of individual faces not implemented yet. Selecting whole elements.";
                PickElement();
                break;

            case SELECTION_TARGET::ELEMENTS:
                PickElement();
                break;

            case SELECTION_TARGET::ENTITIES:
                PickEntity();
                break;

            case SELECTION_TARGET::FREE_POINTS:
                PickPoint();
                break;
            case SELECTION_TARGET::CREATE_POINTS:
                PickSurfacePoint();
                break;
            case SELECTION_TARGET::NONE:
            default:
                break;
            }
        }
	}

	void vtkMousePickStylePIPER::InitPanning()
    {
        this->parent->SetIsCameraInMotion(true);
		// panning is bound to the middle mouse button down in the parent class - call it
		vtkInteractorStyleTrackballCamera::OnMiddleButtonDown();
	}

	void vtkMousePickStylePIPER::EndPanning()
    {
        this->parent->SetIsCameraInMotion(false);
		// panning is bound to the middle mouse button down in the parent class - call it
		vtkInteractorStyleTrackballCamera::OnMiddleButtonUp();
	}

	void vtkMousePickStylePIPER::InitRotation()
	{
        this->parent->SetIsCameraInMotion(true);
		// rotation is bound to the left mouse button down in the parent class - call it
		vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
	}

	void vtkMousePickStylePIPER::EndRotation()
    {
        this->parent->SetIsCameraInMotion(false);
		// rotation is bound to the left mouse button up in the parent class - call it
        vtkInteractorStyleTrackballCamera::OnLeftButtonUp(); 
	}

    void vtkMousePickStylePIPER::ZoomIn()
    {
        this->parent->SetIsCameraInMotion(false);
        // zooming is bound to the wheel of the mouse in the parent class - call it
        vtkInteractorStyleTrackballCamera::OnMouseWheelForward();
    }

    void vtkMousePickStylePIPER::ZoomOut()
    {
        this->parent->SetIsCameraInMotion(false);
        // zooming is bound to the wheel of the mouse in the parent class - call it
        vtkInteractorStyleTrackballCamera::OnMouseWheelBackward();
    }

    void vtkMousePickStylePIPER::initRubberBandPicking(int startPosition[2])
    {
        RBP_startPosition[0] = startPosition[0];
        RBP_startPosition[1] = startPosition[1];
        RBP_currPosition[0] = RBP_startPosition[0];
        RBP_currPosition[1] = RBP_startPosition[1];
        if (!Interactor->GetShiftKey() && !Interactor->GetControlKey()) // if shift is or ctrl is not held, deselect everything
        {
            parent->DeselectAllCells();
            parent->Render();
        }
        RBP_active = true;
        initDrawRubberBand();
    }

    void vtkMousePickStylePIPER::initDrawRubberBand()
    {
        vtkSmartPointer<vtkPolyDataMapper2D> mapper = vtkSmartPointer<vtkPolyDataMapper2D>::New();
        vtkSmartPointer<vtkPolyData> data = vtkSmartPointer<vtkPolyData>::New();
        data->Allocate(4);
        // init the points to be the same point - the clicked one
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        points->SetNumberOfPoints(4);
        points->SetPoint(0, RBP_startPosition[0], RBP_startPosition[1], 0);
        points->SetPoint(1, RBP_startPosition[0], RBP_startPosition[1], 0);
        points->SetPoint(2, RBP_startPosition[0], RBP_startPosition[1], 0);
        points->SetPoint(3, RBP_startPosition[0], RBP_startPosition[1], 0);
        data->SetPoints(points);
        // setup the lines of the rectangle
        vtkIdType lineIds[2];
        lineIds[0] = 0;
        lineIds[1] = 1;
        data->InsertNextCell(VTK_LINE, 2, lineIds);
        lineIds[0] = 1;
        lineIds[1] = 2;
        data->InsertNextCell(VTK_LINE, 2, lineIds);
        lineIds[0] = 2;
        lineIds[1] = 3;
        data->InsertNextCell(VTK_LINE, 2, lineIds);
        lineIds[0] = 3;
        lineIds[1] = 0;
        data->InsertNextCell(VTK_LINE, 2, lineIds);
        mapper->SetInputDataObject(0, data);
        RBP_Actor = vtkSmartPointer<vtkActor2D>::New();
        RBP_Actor->SetMapper(mapper);
        RBP_Actor->GetProperty()->SetLineWidth(3);
        RBP_Actor->GetProperty()->SetColor(1, 1, 0);
        // we will add it directly - not through VtkDisplay API, because it only lasts for the selection anyway and we would need to enhance the API for 2D actors
        parent->GetRenderWindow()->GetDefaultRenderer()->AddActor2D(RBP_Actor);
    }

    void vtkMousePickStylePIPER::updateRubberBandPick(int pickPos[2])
    {
        boost::container::list<std::tuple<int, int>> newlySelectedPixels;
        boost::container::list<std::tuple<int, int>> newlyDeselectedPixels;
        for (int axis = 0, otherAxis = 1; axis < 2; axis++, otherAxis--)
        {
            newlyDeselectedPixels.clear();
            newlySelectedPixels.clear();
            int direction = RBP_currPosition[axis] - RBP_startPosition[axis]; // if the rectangle spans in the positive direction of the axis
            // if the new direction is different from the current one, all that is currently selected will surely get deselected, so reset the selection and start anew
            if ((((pickPos[axis] - RBP_startPosition[axis] < 0) && direction > 0) || ((pickPos[axis] - RBP_startPosition[axis] > 0) && direction < 0)))
            {
                endRubberBandPicking();
                initRubberBandPicking(RBP_startPosition);
                updateRubberBandPick(pickPos);
                return;
            }

            // check which columns were added
            int change = pickPos[axis] - RBP_currPosition[axis];
            if (change != 0 && // if there is some change
                // this second condition is a bit of a hack...if they are equal, only one line of pixels would be selected - but that same line would be selected again when processing Y-axis 
                // and the same pixel would contribute twice during a single querry, but later when it is deselected, it is deselected only once, leaving the cell with
                // one extra hit count. Proper way would be to mark which pixels were selected by X axis so that they are not selected again by Y axis, but they select
                // the same line only in this border case - when start and current position are the same. so this way works and is the most efficient
                (axis == 1 || RBP_currPosition[otherAxis] != RBP_startPosition[otherAxis])) // check this only for the X axis - continue only if it is Y axis or the second condition is met
            {
                int currOtherAxisMax = RBP_currPosition[otherAxis] > RBP_startPosition[otherAxis] ? RBP_currPosition[otherAxis] : RBP_startPosition[otherAxis];
                int currOtherAxisMin = RBP_currPosition[otherAxis] < RBP_startPosition[otherAxis] ? RBP_currPosition[otherAxis] : RBP_startPosition[otherAxis];
                if (change > 0) // if the change is positive, it means additional columns are added in the positive direction
                {
                    // if we are adding to selection, we can ignore the current border and just add what is behind it
                    // but if we are deselecting, we have to also de-select the pixel line on the current border
                    int offsetCurrent = direction > 0 ? 1 : 0;
                    int offsetPicked = direction < 0 ? 0 : 1;
                    for (int i = RBP_currPosition[axis] + offsetCurrent; i < pickPos[axis] + offsetPicked; i++) // for each added column
                    {
                        for (int j = currOtherAxisMin; j <= currOtherAxisMax; j++) // for each row
                        {
                            if (direction >= 0) // direction of the rectangle is positive, change is also positive -> we are adding
                                newlySelectedPixels.push_back(std::tuple<int, int>(i, j));
                            else // if the direction is negative, we are actually deselecting by appending columns in the positive direction
                                newlyDeselectedPixels.push_back(std::tuple<int, int>(i, j));
                        }
                    }
                }
                else if (change < 0) // if it is negative, it means columns are added in the negative direction
                {
                    // if we are adding to selection, we can ignore the current border and just add what is behind it
                    // but if we are deselecting, we have to also de-select the pixel line on the current border
                    int offsetCurrent = direction > 0 ? 0 : 1;
                    int offsetPicked = direction > 0 ? 0 : 1;
                    for (int i = RBP_currPosition[axis] - offsetCurrent; i > pickPos[axis] - offsetPicked; i--) // for each added column
                    {
                        for (int j = currOtherAxisMin; j <= currOtherAxisMax; j++) // for each row
                        {
                            if (direction > 0) // direction of the rectangle is positive, change is negative -> we are removing
                                newlyDeselectedPixels.push_back(std::tuple<int, int>(i, j));
                            else
                                newlySelectedPixels.push_back(std::tuple<int, int>(i, j));
                        }
                    }
                }
            }

            RBP_currPosition[axis] = pickPos[axis]; // update the current X position
            // now run SelectCells for each selected pixels, then for all deselected
            int pixel[2];
            for (auto it = newlySelectedPixels.begin(); it != newlySelectedPixels.end(); it++)
            {
                pixel[axis] = std::get<0>(*it); // the pixel coordinate order is switched for the two axes - switch it here
                pixel[otherAxis] = std::get<1>(*it);
                parent->SelectCellPixel(pixel, true, Interactor->GetControlKey() ? DESELECT_ALL : APPEND_ALL);// if control is held, invert the selection - deselect what is inside the box
            }

            for (auto it = newlyDeselectedPixels.begin(); it != newlyDeselectedPixels.end(); it++)
            {
                pixel[axis] = std::get<0>(*it);
                pixel[otherAxis] = std::get<1>(*it);
                parent->SelectCellPixel(pixel, false, Interactor->GetControlKey() ? DESELECT_ALL : APPEND_ALL);// if control is held, invert the selection - deselect what is inside the box
            }
        }    

        redrawRubberBandRectangle();
    }

    void vtkMousePickStylePIPER::redrawRubberBandRectangle()
    {
        // update and re-draw the picking rectangle
        vtkSmartPointer<vtkPolyData> rectangle = vtkPolyData::SafeDownCast(RBP_Actor->GetMapper()->GetInputDataObject(0, 0));
        rectangle->GetPoints()->SetPoint(0, RBP_startPosition[0], RBP_startPosition[1], 0);
        rectangle->GetPoints()->SetPoint(1, RBP_startPosition[0], RBP_currPosition[1], 0);
        rectangle->GetPoints()->SetPoint(2, RBP_currPosition[0], RBP_currPosition[1], 0);
        rectangle->GetPoints()->SetPoint(3, RBP_currPosition[0], RBP_startPosition[1], 0);
        rectangle->Modified();

        parent->Render();
    }

    void vtkMousePickStylePIPER::endRubberBandPicking()
    {
        parent->GetRenderWindow()->GetDefaultRenderer()->RemoveActor(RBP_Actor);
        RBP_active = false;
        parent->RemoveCellPixelHitArrays();
        parent->Render();
    }

    void vtkMousePickStylePIPER::initBoxPicking(int startPosition[2])
    {
        RBP_startPosition[0] = startPosition[0];
        RBP_startPosition[1] = startPosition[1];
        RBP_currPosition[0] = startPosition[0];
        RBP_currPosition[1] = startPosition[1];

        RBP_active = true;

        initDrawRubberBand();
    }

	void vtkMousePickStylePIPER::endActorDragging()
	{
		actorSelected = vtkSmartPointer<VtkDisplayActor>::New();
		RBP_active = false;
		parent->RemoveCellPixelHitArrays();
		parent->Render();
	}

	void vtkMousePickStylePIPER::initActorDragging(int startPosition[2])
	{
		parent->DeselectAllActors();
		parent->deselectAllPoints();		
		actorSelected = VtkDisplayActor::SafeDownCast(parent->GetRenderWindow()->scenePicker->GetPixelInformation((unsigned int *)startPosition).Prop);
		std::string actorName = actorSelected->GetName();
		if (actorName.find("L#") != std::string::npos) {
			parent->updatedActors[actorSelected->GetName()] = actorSelected;
			vtkSmartPointer<vtkAlgorithm> algorithm = actorSelected->GetMapper()->GetInputConnection(0, 0)->GetProducer();
			vtkSmartPointer<vtkSphereSource> srcReference = vtkSphereSource::SafeDownCast(algorithm);

			double intersection[3];
			if (this->Interactor->GetControlKey()){	//Move freely			
				if (this->parent->GetPickedPointOnMesh(startPosition, intersection))
				{
					srcReference->SetCenter(intersection[0], intersection[1], intersection[2]);
				}
			}
			else{ //Select only mesh nodes
				parent->movePoint(startPosition, intersection);
				srcReference->SetCenter(intersection[0], intersection[1], intersection[2]);
			}
			actorSelected->GetProperty()->SetColor(1.0, 0.647, 0.0);
			RBP_active = true;
			parent->Refresh();
		}
	}

	void vtkMousePickStylePIPER::updateActorDrag(int startPosition[2])
	{
		std::string actorName = actorSelected->GetName();
		if (actorName.find("#") != std::string::npos) {
			vtkSmartPointer<vtkAlgorithm> algorithm = actorSelected->GetMapper()->GetInputConnection(0, 0)->GetProducer();
			vtkSmartPointer<vtkSphereSource> srcReference = vtkSphereSource::SafeDownCast(algorithm);

			double intersection[3];
			if (this->Interactor->GetAltKey()){	//Move freely			
				if (this->parent->GetPickedPointOnMesh(startPosition, intersection))
				{
					srcReference->SetCenter(intersection[0], intersection[1], intersection[2]);					
				}
			}
			else{ //Select only mesh nodes
				parent->movePoint(startPosition, intersection);
				srcReference->SetCenter(intersection[0], intersection[1], intersection[2]);
			}
			actorSelected->GetProperty()->SetColor(1.0, 0.647, 0.0);
			RBP_active = true;
			parent->Refresh();
		}
	}

    void vtkMousePickStylePIPER::updateBoxPick(int pickPos[2])
    {   
        RBP_currPosition[0] = pickPos[0];
        RBP_currPosition[1] = pickPos[1];

        redrawRubberBandRectangle();
    }	

    void vtkMousePickStylePIPER::endBoxPicking()
    {
        RBP_active = false;
       // no need to remove RBP actor explicity, it will be removed in the refresh in the end of selection anyway

        Context::instance().performAsynchronousOperation(std::bind(&vtkMousePickStylePIPER::doSelectByBox, this),
            false, false, false, false, false);
    }

    void vtkMousePickStylePIPER::doSelectByBox()
    {
        pInfo() << START << "Selecting by box...";
        double edgeA1[3];
        double edgeA2[3];
        double edgeB1[3];
        double edgeB2[3];
        double edgeC1[3];
        double edgeC2[3];

        // get the endpoints of three edges of the box as rays casted from the picked screen coordinates towards the far clipping plane
        if (vtkSelectionTools::ComputePointInScreenRay(RBP_startPosition[0], RBP_startPosition[1], true, parent->GetRenderWindow()->GetDefaultRenderer(), edgeA1, edgeA2) &&
            vtkSelectionTools::ComputePointInScreenRay(RBP_currPosition[0], RBP_startPosition[1], true, parent->GetRenderWindow()->GetDefaultRenderer(), edgeB1, edgeB2) &&
            vtkSelectionTools::ComputePointInScreenRay(RBP_startPosition[0], RBP_currPosition[1], true, parent->GetRenderWindow()->GetDefaultRenderer(), edgeC1, edgeC2))
        {
            // create the origin + 3 axes representation of the box
            double origin[3];
            double axes[3][3];
            for (int i = 0; i < 3; i++)
            {
                origin[i] = edgeA1[i];
                axes[0][i] = edgeB1[i] - origin[i];
                axes[1][i] = edgeC1[i] - origin[i];
                axes[2][i] = edgeA2[i] - origin[i];
            }

            // clip the computed rays by the visible pickable data (non-pickable data are not interesting for the user anyway)
            double *boundsOfData = parent->GetRenderWindow()->GetPickingRenderer()->ComputeVisiblePropBounds();
            double boundingBoxVerts[8][3];
            for (int i = 0; i < 8; i++)
            {
                boundingBoxVerts[i][0] = boundsOfData[i % 2];
                boundingBoxVerts[i][1] = boundsOfData[i % 4 < 2 ? 2 : 3];
                boundingBoxVerts[i][2] = boundsOfData[i < 4 ? 4 : 5];
            }

            // calculate the shortest and longest distance from the bound rectangle to the front face of the box
            double dirVector[3];
            double maxDistance = VTK_DOUBLE_MIN;
            double minDistance = VTK_DOUBLE_MAX;
            // plane normal of the fron face is equal to the third axes, but we must normalize it
            double normal[3];
            double norm = vtkMath::Norm(axes[2]);
            for (int i = 0; i < 3; i++)
                normal[i] = axes[2][i] / norm;
            for (int i = 0; i < 8; i++) // for each vertex of the data bounding box
            {
                // vector between the tested vertex and arbitrary point in the front face (origin will do)
                dirVector[0] = boundingBoxVerts[i][0] - origin[0];
                dirVector[1] = boundingBoxVerts[i][1] - origin[1];
                dirVector[2] = boundingBoxVerts[i][2] - origin[2];
                double dist = vtkMath::Dot(dirVector, normal); // distance between the plane of the drawn box' front face and the bounding box vertex
                if (dist > maxDistance)
                    maxDistance = dist;
                if (dist < minDistance)
                    minDistance = dist;
            }
            for (int i = 0; i < 3; i++)
            {
                // move the "deep" endpoint of the box to a new location based on the max distance - endpoint = (originalOrigin[i] + normal[i] * maxDistance), axes = endpoint - Neworigin[i]
                axes[2][i] = origin[i] + normal[i] * maxDistance;
                origin[i] += normal[i] * minDistance;  // move the origin closer to the dataset, based on the minDistance
                axes[2][i] = axes[2][i] - origin[i];
            }

            parent->SelectByBox(origin, axes, getPickAppendingStyle(), targetType, true);
        }
        pInfo() << DONE;
    }

	void vtkMousePickStylePIPER::PickEntity()
	{
        this->parent->SelectActor(this->Interactor->GetEventPosition(), getPickAppendingStyle());
        /*
        OLD CODE without color picking - might be useful if we want to include "legacy code"
        // Pick from this location.
		vtkSmartPointer<vtkCellPicker>  picker = vtkSmartPointer<vtkCellPicker>::New();

        if (1 == picker->Pick(clickPos[0], clickPos[1], 0, this->GetDefaultRenderer()))
        {
            parent->SelectActor(VtkDisplayActor::SafeDownCast(picker->GetActor()), getPickAppendingStyle());
        }
        else // deselect all on clicking outside
            parent->DeselectActor();
        */
    }

    void vtkMousePickStylePIPER::PickElement()
    {
            this->parent->SelectCell(this->Interactor->GetEventPosition(), getPickAppendingStyle());
            parent->Render();
        /* OLD CODE using vtkCellPicker instead of vtkHardwareSelector + highlighting of primitives by drawing additional polygons instead of using scalar arrays
        - might be useful if we want to include "legacy code"
        vtkSmartPointer<vtkCellPicker> picker = vtkSmartPointer<vtkCellPicker>::New();
        // Pick from this location.
        picker->Pick(clickPos[0], clickPos[1], 0, this->GetDefaultRenderer());
        //std::cout << "Cell ID: " << picker->GetCellId() << " | Point ID:" << picker->GetPointId() << " | Cell Type: " << picker->GetDataSet()->GetCell(picker->GetCellId())->GetCellType() << "\n";

        if (picker->GetCellId() != -1){

        //Clear selections if Shift key is not held down
        if (!this->Interactor->GetShiftKey())
        {
        if (selectedActors->GetNumberOfItems() >= 1){
        // Clear selections
        for (vtkIdType i = selectedActors->GetNumberOfItems() - 1; i >= 0; i--)
        {
        vtkSmartPointer<vtkActor> actor = selectedActors->GetLastActor();
        this->GetDefaultRenderer()->RemoveActor(actor);
        selectedActors->RemoveItem(i);
        }
        selectedElements.clear();
        }
        vtkSmartPointer<vtkPoints> pts = picker->GetDataSet()->GetCell(picker->GetCellId())->GetPoints();
        draw.polygon(this->GetDefaultRenderer()->GetRenderWindow(), pts);
        }
        else{
        vtkSmartPointer<vtkPoints> pts = picker->GetDataSet()->GetCell(picker->GetCellId())->GetPoints();
        selectedElements.push_back(picker);
        vtkSmartPointer<vtkActor> actor;
        actor = draw.polygon(this->GetDefaultRenderer()->GetRenderWindow(), pts);
        selectedActors->AddItem(actor);
        element ele;
        for (int i = 0; i < pts->GetNumberOfPoints(); i++){
        double x[3];
        pts->GetPoint(i, x);
        point pt;
        pt.cords[0] = x[0];
        pt.cords[1] = x[1];
        pt.cords[2] = x[2];
        ele.Points.push_back(pt);
        }
        activeElements.push_back(ele);

        int i = 0;
        for (auto it = activeElements.begin(); it != activeElements.end(); ++it){
        std::vector <point> Points = activeElements.at(i).Points;
        int j = 0;
        vtkSmartPointer<vtkPoints> pts = vtkSmartPointer<vtkPoints>::New();;
        for (auto itt = Points.begin(); itt != Points.end(); ++itt){
        pts->InsertNextPoint(Points.at(j).cords);
        j++;
        }
        vtkSmartPointer<vtkActor> actor;
        actor = draw.polygon(this->GetDefaultRenderer()->GetRenderWindow(), pts);
        i++;
        }
        this->GetDefaultRenderer()->Render();
        }
        }*/
    }

    void vtkMousePickStylePIPER::PickNode()
    {
        this->parent->SelectMeshNode(this->Interactor->GetEventPosition(), getPickAppendingStyle());
    }

    void vtkMousePickStylePIPER::PickPoint()
    {
        this->parent->SelectGlyphedPoint(this->Interactor->GetEventPosition(), getPickAppendingStyle());
    }

    void vtkMousePickStylePIPER::PickSurfacePoint()
    {
        if (this->parent->GetPickedPointOnMesh(this->Interactor->GetEventPosition(), freePointTarget))
            this->InvokeEvent(vtkCommand::UserEvent + VTKEVENT_SURF_POINT_PICKED, freePointTarget);
    }

	void vtkMousePickStylePIPER::CreatePointWC()
	{
		//Clear selections if Shift key is not held down
		if (!this->Interactor->GetShiftKey())
		{
			if (selectedActors->GetNumberOfItems() >= 1){
				// Clear selections			
				for (vtkIdType i = selectedActors->GetNumberOfItems() - 1; i >= 0; i--)
				{
					vtkSmartPointer<vtkActor> actor = selectedActors->GetLastActor();
					this->GetDefaultRenderer()->RemoveActor(actor);
					selectedActors->RemoveItem(i);
				}
				selectedElements.clear();
				selectedPoints.clear();
			}
		}

		int* clickPos = this->Interactor->GetEventPosition();

		vtkSmartPointer<vtkCellPicker> picker = vtkSmartPointer<vtkCellPicker>::New();

		// Pick from this location.
		picker->Pick(clickPos[0], clickPos[1], 0, this->GetDefaultRenderer());
		double* worldPosition = picker->GetPickPosition();

		//Create a sphere
        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		actor = draw.point(worldPosition[0], worldPosition[1], worldPosition[2], this->parent->POINTHIGHLIGHT_RADIUS);
		actor->GetProperty()->SetColor(0, 1.0, 1.0);
        this->parent->AddActor(actor, actor->GetName(), false, DISPLAY_MODE::POINTS, false); // add the point actor properly to the display
        this->parent->Refresh();
		//Add to selected Actors
		selectedElements.push_back(picker);
		selectedActors->AddItem(actor);	
		selectedPoints.push_back(actor);
	}

    void vtkMousePickStylePIPER::SetParentDisplay(VtkDisplay *parent)
    {
        this->parent = parent;
    }

    void vtkMousePickStylePIPER::InitSurfacePointPick()
    {
        targetType = SELECTION_TARGET::CREATE_POINTS;
        if (this->parent)
            this->parent->SelectionTargetChanged(SELECTION_TARGET::CREATE_POINTS);
    }

	std::string vtkMousePickStylePIPER::SetPickingType(SELECTION_TARGET target, bool activateRubberBand, bool activateBox, bool activeDrag)
	{
		targetType = target;
        this->useRBP = activateRubberBand;
        this->useBox = activateBox;
		this->useDrag = activeDrag;
        if (this->parent)
            this->parent->SelectionTargetChanged(target);
        std::stringstream hint;
        if (target != SELECTION_TARGET::NONE)
        {
            std::string pickingButtonName;
            std::string clickOrDouble;
            for (auto it = actionBinding.begin(); it != actionBinding.end(); it++)
            {
                if (it->second == PICK)
                {
                    if (it->first == ACTION_LMB_CLICK)
                    {
                        pickingButtonName = "left mouse button";
                        clickOrDouble = "Press ";
                    }
                    if (it->first == ACTION_LMB_DOUBLECLICK)
                    {
                        pickingButtonName = "left mouse button";
                        clickOrDouble = "Double click ";
                    }
                    if (it->first == ACTION_RMB_CLICK)
                    {
                        pickingButtonName = "right mouse button";
                        clickOrDouble = "Press ";
                    }
                    if (it->first == ACTION_RMB_DOUBLECLICK)
                    {
                        pickingButtonName = "left mouse button";
                        clickOrDouble = "Double click ";
                    }
                    if (it->first == ACTION_MMB_CLICK)
                    {
                        pickingButtonName = "middle mouse button";
                        clickOrDouble = "Press ";
                    }
                    if (it->first == ACTION_MMB_DOUBLECLICK)
                    {
                        pickingButtonName = "middle mouse button";
                        clickOrDouble = "Double click ";
                    }
                    if (it->first == ACTION_LMB_CLICK)
                    {
                        pickingButtonName = "left mouse button";
                        clickOrDouble = "Press ";
                    }
                    break;
                }
            }
            std::string selectTarget;
            switch (target)
            {
                case SELECTION_TARGET::ENTITIES:
                    selectTarget = "entities";
                break;
                case SELECTION_TARGET::ELEMENTS:
                    selectTarget = "elements";
                    break;
                case SELECTION_TARGET::NODES:
                    selectTarget = "nodes";
                    break;
                case SELECTION_TARGET::FREE_POINTS:
                    selectTarget = "points";
                    break;
                case SELECTION_TARGET::FACES:
                    selectTarget = "faces";
                break;
            }
            if (useRBP)
            {
                hint << "Hold " << pickingButtonName << " and drag to select " << selectTarget << " inside the rectangle." << std::endl
                    << "If you hold no keys, all that is currently selected will be deselected." << std::endl 
                    << "If you hold SHIFT, your pick will be appended to the current selection." << std::endl 
                    << "If you hold CTRL, all inside the rectangle will be DE - selected instead.";
            }
            else if (useBox)
            {
                hint << "Hold " << pickingButtonName << " and drag to select " << selectTarget << " inside the box." << std::endl
                    << "The box will extend from your drawn rectangle through the entire scene using parallel projection." << std::endl
                    << "Your camera will be automatically switched to parallel projection for better control." << std::endl
                    << "If you hold no keys, all that is currently selected will be deselected." << std::endl 
                    << "If you hold SHIFT, your pick will be appended to the current selection." << std::endl 
                    << "If you hold CTRL, all inside the box will be DE-selected instead. Everything outside the box will be unchanged.";
            }
			else if (useDrag)
			{
				hint << "Hold " << pickingButtonName << " and drag to move the selected point landmark." << std::endl
					<< "The landmark will move along the mouse." << std::endl
					<< "If you hold no keys, the point will move to the closest node near the mouse." << std::endl
					<< "If you hold CTRL, the point will move freely on the mesh surface.";
			}
            else
            {
                if (target == SELECTION_TARGET::ELEMENTS || target == SELECTION_TARGET::FACES || target == SELECTION_TARGET::NODES)
                    hint << clickOrDouble << pickingButtonName << " to select " << selectTarget << " under your cursor. If it is already selected, it will be deselected." << std::endl
                    << "If you hold no keys, your pick will be appended to the current selection on its entity, but all other entities will be entirely deselected." << std::endl
                    << "If you hold SHIFT, your pick will be appended to the current selection without deselecting other entities." << std::endl
                    << "If you hold CTRL, all but the clicked primitive will be DE-selected on the clicked entity. Other entities will be kept intact.";
                else
                    hint << clickOrDouble << pickingButtonName << " to select " << selectTarget << " under your cursor. If it is already selected, it will be deselected. " << std::endl 
                    << "Click on an empty space to deselect all." << std::endl
                    << "If you hold no keys, only the clicked entity will be selected, others will be deselected." << std::endl
                    << "If you hold SHIFT, your pick will be appended to the current selection without deselecting other entities." << std::endl;
            }
        }
        else hint << "No picker is active";
        return hint.str();
	}


	void vtkMousePickStylePIPER::setStyleData(vtkPolyData* vtkStyleData)
	{
		Data = vtkStyleData;
	}

	std::vector<vtkSmartPointer<vtkCellPicker>> vtkMousePickStylePIPER::getSelectedElements(){
		return selectedElements;
	}

	std::vector<vtkSmartPointer<vtkActor>> vtkMousePickStylePIPER::getSelectedActors(){
		return selectedPoints;
	}

	vtkStandardNewMacro(vtkMousePickStylePIPER);

}
