/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "OctaveProcess.h"

#include <QCoreApplication>
#include <QProcessEnvironment>
#include <QFileInfo>
#include <QDebug>

#include <iostream>

#include "common/logging.h"
#include "common/helper.h"

namespace piper
{

    //Default constructor
    OctaveProcess::OctaveProcess()
        : m_matlabCompatibleMode(false)
        , m_persist(false)
    {
        setProcessChannelMode(QProcess::MergedChannels);

        //If piper have been regulary installed on a computer 
        if (QProcessEnvironment::systemEnvironment().contains("OCTAVE_ROOT")) {
            // If the user/dev specified an octave installation, use it
            setProgram(QProcessEnvironment::systemEnvironment().value("OCTAVE_ROOT") + "/bin/octave-cli");
        }
        else {
            // Look for Octave in the application folder
            QString octavePath = QCoreApplication::applicationDirPath() + "/octave/bin/octave-cli";
            if (QFileInfo(octavePath).exists())
                setProgram(octavePath);
            else
                // if not found try to use the system wide installed octave
                setProgram("octave-cli");
        }
    }

    void OctaveProcess::setScriptPath(QString script)
    {
        QFileInfo scriptFileInfo(piper::octaveScriptDirectoryPath() + "/" + script);
        if (!scriptFileInfo.exists())
            throw std::runtime_error("Octave script not found: "+script.toStdString());
        m_scriptPath = script;
    }

    void OctaveProcess::setScriptArguments(QStringList scriptArguments)
    {
        m_scriptArguments = scriptArguments;
    }

    void OctaveProcess::prepareOctaveProcess()
    {
        QFileInfo scriptFileInfo(piper::octaveScriptDirectoryPath() + "/" + m_scriptPath);
        setWorkingDirectory(scriptFileInfo.absolutePath());

        QStringList args;
        args << "--silent";
        if (m_matlabCompatibleMode)
            args << "--traditional";
        if (m_persist)
            args << "--persist";
        args << scriptFileInfo.fileName();
        args << m_scriptArguments;
        setArguments(args);
    }

    bool OctaveProcess::executeScript()
    {
        startScript();
        if (waitForStarted()) {
            if (waitForFinished(MAX_SCRIPT_RUNTIME))
            {
                if (exitStatus()!=QProcess::NormalExit || exitCode()!=0) 
                {
                    QString message = "Octave script " + m_scriptPath + " finished abnormally\n"
                            + readAllStandardOutput() + "\n"
                            + readAllStandardError();
                    throw std::runtime_error(message.toStdString());
                }
                return true;
            }
        }
        else
            throw std::runtime_error("Octave process cannot be started");
        return false;
    }

    void OctaveProcess::startScript() {
        prepareOctaveProcess();
        pDebug() << "Starting octave process with arguments: " << arguments();
        if (QProcess::Running != state())
            start();
    }

    bool OctaveProcess::isLastExecutionSuccessful() const {
        return QProcess::NormalExit == exitStatus() && 0 == exitCode();
    }

    QString OctaveProcess::errorMessage() {
        QString message;
        QTextStream stream(&message, QIODevice::Append);
        stream <<  "Octave script " << scriptPath() << " finished abnormally" << endl
               << readAllStandardOutput() << endl
               << readAllStandardError();
        return message;
    }

    //Start the octave QProcess
    bool OctaveProcess::canBeStarted()
    {
        setArguments(QStringList() << "--version");
        if (QProcess::Running != state())
            start();
        if (waitForStarted()) {
            waitForFinished(MAX_SCRIPT_RUNTIME);
            return true;
        }
        return false;
    }

    QString OctaveProcess::version()
    {
        QString version;
        setArguments(QStringList() << "--version");
        start();
        if (waitForStarted()) {
            waitForFinished(MAX_SCRIPT_RUNTIME);
            version = QString(readAllStandardOutput()).split("\n").at(0);
        }
        else {
            throw std::runtime_error("Octave process cannot be started");
        }
        return version;
    }
}
