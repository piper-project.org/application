/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Context.h"
#include "VtkErrorObserver.h"

namespace piper {
    
    VtkErrorObserver::~VtkErrorObserver()
    {
        if (turnErrorDisplayBackOn)
            vtkObject::GlobalWarningDisplayOn();
    }

    bool VtkErrorObserver::GetError() const
    {
        return this->Error;
    }

    bool VtkErrorObserver::GetWarning() const
    {
        return this->Warning;
    }

    void VtkErrorObserver::Clear()
    {
        if (turnErrorDisplayBackOn)
            vtkObject::GlobalWarningDisplayOn();
        this->Error = false;
        this->Warning = false;
        this->ErrorMessage = "";
        this->WarningMessage = "";
    }

    std::string VtkErrorObserver::GetErrorMessage()
    {
        return ErrorMessage;
    }
    std::string VtkErrorObserver::GetWarningMessage()
    {
        return WarningMessage;
    }

    void VtkErrorObserver::processMessage(std::string &message)
    {
        if (LoggingParameter::instance().verboseLevel() == LoggingParameter::VerboseLevel::DEFAULT)
        {
            std::istringstream iss(message);
            std::stringstream processed;
            processed.str("");
            std::string token;
            // all this is based on the current format of error messages in vtkErrorMacro in vtk 7.0.1, should it change in the future this needs to be modified
            std::getline(iss, token, ':'); // message starts with "Error:" or "Warning:"
            processed << token << ":";
            std::getline(iss, token, '\n'); // then - " In " __FILE__ ", line " << __LINE__ << "\n" " - we will discard that
            std::getline(iss, token, ':'); // also discard: self->GetClassName() << " (" << self << "):
            while (std::getline(iss, token)) processed << token; // finally the actual message
            message = processed.str();
        }
    }

    void VtkErrorObserver::Execute(vtkObject *caller, unsigned long ev, void *calldata)
    {
        switch (ev)
        {
        case vtkCommand::ErrorEvent:
            ErrorMessage = static_cast<char *>(calldata);
            processMessage(ErrorMessage);
            pCritical() << ErrorMessage;
            this->Error = true;
            turnErrorDisplayBackOn = caller->GetGlobalWarningDisplay();
            caller->GlobalWarningDisplayOff(); // prevents other errors further down the pipeline from popping up
            break;
        case vtkCommand::WarningEvent:
            WarningMessage = static_cast<char *>(calldata);
            processMessage(WarningMessage);
            pWarning() << WarningMessage;
            this->Warning = true;
            break;
        }
    }
}
