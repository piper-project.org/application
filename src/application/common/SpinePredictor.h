/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_SPINEPREDICTOR_H
#define PIPER_SPINEPREDICTOR_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <QObject>
#include <QString>
#include <QStringList>
#include <QProcess>

namespace piper {

class OctaveProcess;

/** This class integrates the WP2 spine predictor into the Piper application.
 *
 * \author Thomas Lemaire \date 2016
 */
class PIPERCOMMON_EXPORT SpinePredictor : public QObject {
    Q_OBJECT

public:

    /// @returns posture names as understood by the octave script
    Q_INVOKABLE QStringList postureNames() const;

    /** Call the octave script.
     *
     * 1. produce the input files
     * 2. call the script with its required parameters
     * 3. read the targets and add them the current project
     *
     * \param op to be used to run the scripts
     * \param inputCSFile input vertebra coordinate system file
     * \param inputGravityFile input gravity vector file
     * \param sourcePosture index of source posture, starting at 0
     * \param targetPosture index of target posture, starting at 0
     * \param postureValue value in [0,1] to specify a posture in sourcePosture->targetPosture
     * \param latFlexTL lateral flexion of thoraco-lumbar segment
     * \param latFlexC lateral flexion of cervical segment
     * \param alignOnGravity if true, the gravity is the output posture vectical
     *
     * \see OctaveProcess
     * \todo better interface without duplicated parameters gravityVector / alignOnGravity
     */
    void compute(piper::OctaveProcess* op, QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity);

    /** Non blocking version of compute()
     * \sa compute()
     */
    Q_INVOKABLE void startCompute(QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity);

signals:
    /// This signal is emitted when a startCompute() finishes successfully
    void computeFinished();

private:
    void setupOctaveProcess(piper::OctaveProcess* op, QString inputCSFile, QString inputGravityFile, int sourcePosture, int targetPosture, double postureValue, double latFlexTL, double latFlexC, QString outputDir, bool alignOnGravity);

private slots:
    void onScriptFinished(int exitCode, QProcess::ExitStatus exitStatus);


};

}

#endif // PIPER_SPINEPREDICTOR_H
