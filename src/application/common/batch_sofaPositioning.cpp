/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "batch.h"

#pragma warning(push, 0)
#include <sofa/helper/system/FileRepository.h>
#include <sofa/simulation/Simulation.h>
#include <sofa/simulation/Node.h>
#include <sofa/core/objectmodel/GUIEvent.h>
#include <SofaPython/PythonScriptControllerHelper.h>
#pragma warning(pop)
#include <SofaPiper/tools.h>

#include "logging.h"

namespace piper {

// TODO error management
// TODO log management
// TODO frameToFrame targets
void physPosiDeform(Project & project, hbm::TargetList const& target)
{
    sofaPiper::setProject(project);

    std::string scenePath;

    scenePath = "scenePrepareDeforming.py";
    sofa::helper::system::DataRepository.findFile(scenePath);
    sofa::simulation::Node::SPtr sofaRootNode = sofa::simulation::getSimulation()->load(scenePath.c_str());
    sofa::simulation::getSimulation()->init(sofaRootNode.get());
    // no need to animate, all prepare is done at scene initialization
    sofa::simulation::getSimulation()->unload(sofaRootNode);

    scenePath = "scenePositioning.py";
    sofa::helper::system::DataRepository.findFile(scenePath);
    sofaRootNode = sofa::simulation::getSimulation()->load(scenePath.c_str());
    sofa::simulation::getSimulation()->init(sofaRootNode.get());

    bool success;
    unsigned int nbTargets=0;
    // TODO if target cannot be added, ignore it and print a warning instead of a FATAL error
    for (hbm::FixedBoneTarget const& fixedBone : target.fixedBone) {
        pDebug() << "Add target - FixedBone: " << fixedBone.name();
        sofa::helper::PythonScriptController_call(&success, sofaRootNode, "fixedBonesInteractor", "setBoneFixed", fixedBone.bone, true);
        if (!success)
            pWarning() << "Target ignored: " << fixedBone.name();
        else
            ++nbTargets;
    }

    for (hbm::JointTarget const& joint : target.joint) {
        pDebug() << "Add target - Joint: "<< joint.name();
        sofa::helper::PythonScriptController_call(&success, sofaRootNode, "jointControllerInteractor", "addJointController", joint.joint);
        if (!success) {
            pWarning() << "Target ignored: " << joint.name();
            continue;
        }
        for (auto const& index_value : joint.value()) {
            sofa::helper::PythonScriptController_call(&success, sofaRootNode, "jointControllerInteractor", "setTarget", joint.joint, index_value.first, index_value.second);
            if (!success)
                pWarning() << "Target value ignored: " << joint.name() << " - " << index_value.first << ":" << index_value.second;
        }
        ++nbTargets;
    }

//    for (hbm::FrameToFrameTarget const frameToFrame : target.frameToFrame) {
//        pDebug() << "Add target - FrameToFrame: "<< frameToFrame.name();
//        sofa::helper::PythonScriptController_call(&success, "frameControllerInteractor", "addController", frameToFrame.name(), frameToFrame.frameSource, frameToFrame.frameTarget, frameToFrame.mask());
//        if (!success) {
//            pWarning() << "Target ignored: " << frameToFrame.name();
//            continue;
//        }
//        for (auto const& index_value : frameToFrame.value()) {
//            sofa::helper::PythonScriptController_call(&success, "frameControllerInteractor", "setTarget", frameToFrame.name(), index_value.first, index_value.second);
//            if (!success)
//                pWarning() << "Target value ignored: " << frameToFrame.name() << " - " << index_value.first << ":" << index_value.second;
//        }
//        ++nbTargets;
//    }

    pInfo() << INFO << "Added targets: " << nbTargets << "/" << target.size();

    pInfo() << START << "Deformation...";
    sofa::helper::PythonScriptController_call(nullptr, sofaRootNode, "simulationControlInteractor", "setAllNodesActivated", false);
sofa::helper::PythonScriptController_call(nullptr, sofaRootNode, "simulationControlInteractor", "setAutoStop", true);
    sofaRootNode->animate_.setValue(true);
    while (sofaRootNode->animate_.getValue()) {
        sofa::simulation::getSimulation()->animate(sofaRootNode.get());
    }
    unsigned int nbIteration;
    sofa::helper::PythonScriptController_call(&nbIteration, sofaRootNode, "simulationControlInteractor", "getNbIteration");
    double velocity;
    sofa::helper::PythonScriptController_call(&velocity, sofaRootNode, "simulationControlInteractor", "getMaxCartesianVelocity");
    pInfo() << DONE << " - iterations: " << nbIteration << " - max dof velocity: " << velocity;

    // update model
    sofa::helper::PythonScriptController_call(nullptr, sofaRootNode, "simulationControlInteractor", "setAllNodesActivated", true);
    sofa::simulation::getSimulation()->animate(sofaRootNode.get());
    sofa::core::objectmodel::GUIEvent exportFullModel("","exportFullModel","");
    sofaRootNode->propagateEvent(sofa::core::ExecParams::defaultInstance(), &exportFullModel);
    sofa::core::objectmodel::GUIEvent exportEntities("","exportEntities","");
    sofaRootNode->propagateEvent(sofa::core::ExecParams::defaultInstance(), &exportEntities);

    pInfo() << INFO << "Model updated";

    sofa::simulation::getSimulation()->unload(sofaRootNode);
}

} // namespace piper
