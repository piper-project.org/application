/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Project.h"

#include <stdexcept>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <tinyxml/tinyxml2.h>

#include <QDebug>
#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>

#include <hbm/Helper.h>

#include "version.h"
#include "logging.h"
#include "XMLValidationProcess.h"
//#include "Context.h"
//#include "message.h"

namespace piper {

Project::Project(std::string const& projectFile)
{
    read(projectFile);
}

void Project::clear()
{
    m_bodyDimDBFileName.clear();
    m_model.clear();
    m_env.clear();
    m_target.clear();
    m_anthropomodel.clearAnthropoModel();
    m_moduleParameter.setDefault();
}

void Project::read(std::string const& projectFile)
{
    pInfo() << START << "Open project: " << projectFile;
    clear();
    // validity check
    validateXml(projectFile, "piper.dtd");

    boost::filesystem::path projectFileDirectory = boost::filesystem::absolute(boost::filesystem::path(projectFile)).parent_path();

    tinyxml2::XMLDocument project;
    project.LoadFile(projectFile.c_str());
    if (project.Error()) {
        clear();
        std::stringstream str;
        str << "Failed to load project file: " << projectFile << std::endl << project.ErrorStr() << std::endl;
        throw std::runtime_error(str.str().c_str());
    }

    tinyxml2::XMLElement* element;

    // piper app version  which was used to write this file
    std::string version = project.RootElement()->Attribute("version");
    std::vector<std::string> tokens;
    boost::split(tokens, version, boost::is_any_of("."));

    // to be used internally, in parseXml() methods, if necessary
    unsigned int fileVersionMajor, fileVersionMinor;
    std::istringstream(tokens[0]) >> fileVersionMajor;
    std::istringstream(tokens[1]) >> fileVersionMinor;

    // hbm
    element = project.RootElement()->FirstChildElement("hbm");
    if (element != nullptr) {
        if (element->GetText() != nullptr) {
            boost::filesystem::path modelPath(element->GetText());
            _loadModel((projectFileDirectory/modelPath).string());
        }
    }

    // todo: take care of format_rules delivered with piper app
    m_env.read(projectFile);
    m_env.scaleEnvModels(model().metadata().lengthUnit());

    //BodyDimDB
    element = project.RootElement()->FirstChildElement("BodyDimDB");
    m_bodyDimDBFileName.clear();
    if (element != nullptr) {
        if (element->GetText() != nullptr) {
            boost::filesystem::path bodyDimDBFilePath(element->GetText());
            m_bodyDimDBFileName = (projectFileDirectory/bodyDimDBFilePath).string();
        }
    }

    // target
    m_target.parseXml(project.RootElement());
    m_target.convertToUnit(model().metadata().lengthUnit());
    m_target.convertToUnit(model().metadata().massUnit());
    m_target.convertToUnit(model().metadata().ageUnit());

    // BodySectionDimension
    m_anthropomodel.parseXml(project.RootElement(), m_anthropomodel);

    // modules parameters
    m_moduleParameter.parseXml(project.RootElement());
    pInfo() << DONE;
}

void Project::write(std::string const& projectFile)
{
    pInfo() << START << "Save project: " << projectFile;
    boost::filesystem::path filePath(projectFile);
    std::string projectFileDirectory = boost::filesystem::absolute(filePath).parent_path().string();

    // build tinyxml document
    tinyxml2::XMLDocument project;
    tinyxml2::XMLElement* element;

    project.InsertEndChild(project.NewDeclaration());
    project.InsertEndChild(project.NewUnknown("DOCTYPE piper SYSTEM \"piper.dtd\""));

    tinyxml2::XMLElement* xmlPiper = project.NewElement("piper-project");
    std::ostringstream version;
    version << PIPER_VERSION_MAJOR << "." << PIPER_VERSION_MINOR << "." << PIPER_VERSION_PATCH;
    xmlPiper->SetAttribute("version", version.str().c_str());
    project.InsertEndChild(xmlPiper);

    // model file
    std::string modelFile = filePath.stem().string() + ".pmd";
    element = project.NewElement("hbm");
    element->SetText(modelFile.c_str());
    xmlPiper->InsertEndChild(element);

    // environment models
    // todo: take care of format_rules delivered with piper app
    m_env.serializeXml(xmlPiper, projectFileDirectory);

    // targetList
    m_target.serializeXml(project.RootElement());

    // body dimension database
    element = project.NewElement("BodyDimDB");
    element->SetText( piper::hbm::relativePath(m_bodyDimDBFileName, projectFileDirectory).generic_string().c_str());
    xmlPiper->InsertEndChild(element);

    // BodySectionDimension
    m_anthropomodel.serialize(project.RootElement());

    // modules parameters
    m_moduleParameter.serializeXml(project.RootElement());

    // write tinyxml document
    project.SaveFile(projectFile.c_str());
    if (project.Error()) {
        std::stringstream str;
        str << "Failed to save project file: " << projectFile << std::endl << project.ErrorStr() << std::endl;
        throw std::runtime_error(str.str().c_str());
    }

    // write hbm
    boost::filesystem::path modelFilePath = boost::filesystem::path(projectFileDirectory) / boost::filesystem::path(modelFile);
    model().write(modelFilePath.string());
    pInfo() << DONE;
}

void Project::_loadModel(std::string const& modelFile)
{
    // validity check
    validateXml(modelFile, "hbm.dtd");
    model().read(modelFile);
}

void Project::loadModel(std::string const& modelFile)
{
    pInfo() << START << "Load model: " << modelFile;
    _loadModel(modelFile);
    pInfo() << DONE;
}

void Project::saveModel(std::string const& modelFile)
{
    pInfo() << START << "Save model: " << modelFile << " (+.vtu)";
    model().write(modelFile);
    pInfo() << DONE;
}

void Project::loadMetadata(std::string const& metadataFile, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool anatomicalJoint, bool controlpoint, bool HbmParameter) {
    pInfo() << START << "Load metadata: " << metadataFile;
    // validity check
    validateXml(metadataFile, "ModelRules.dtd");
    model().metadata().importMetadata(metadataFile, model().fem(), anthropometry, entity, landmark, genericmetadata, contact, joint, anatomicalJoint, controlpoint, HbmParameter);
    pInfo() << DONE;
}

void Project::saveMetadata(std::string const& metadataFile, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool controlpoint, bool HbmParameter)
{
    pInfo() << START << "Save metadata: " << metadataFile << ".";
    model().metadata().exportMetadata(metadataFile, model().fem(), anthropometry, entity, landmark, genericmetadata, contact, joint, controlpoint, HbmParameter);
    pInfo() << DONE;
}

void Project::collectControlPoints(piper::hbm::InteractionControlPoint& source, piper::hbm::InteractionControlPoint& target,
    std::map<std::string, std::string> association)
{
    piper::hbm::FEModel const& fem = model().fem();
    piper::hbm::Metadata const& metadata = model().metadata();


    std::vector<double> &as_bones = source.getAssociationBones();
    std::vector<double> &as_skin = source.getAssociationSkin();
    std::vector<double> &nuggets = source.getWeight();
    hbm::VCoord &pointsS = source.getControlPt3dCont();
    hbm::VCoord &pointsT = target.getControlPt3dCont();

    std::vector<std::string> targets;
    for (auto const& cur : association) // concatenate all matched pairs of control points into one set of control points
    {
        hbm::InteractionControlPoint const* set = metadata.interactionControlPoint(cur.second);
        if (set)
        {
            std::vector<hbm::Coord> targetValue;
            for (auto const& curset : m_target.controlPoint)
            {
                if (curset.name() == cur.first) // find the matched target
                {
                    targetValue = curset.value();
                    break;
                }
            }
            if (targetValue.empty()) // look for the target among control points in metadata
            {
                hbm::InteractionControlPoint const* targetSet = metadata.interactionControlPoint(cur.first);
                if (targetSet && targetSet->getControlPointRole() != hbm::InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE)
                    targetSet->getCoordControlPoint(fem, targetValue);
            }
            if (targetValue.size() != set->getNbControlPt())
                pWarning() << "The number of target points, " << targetValue.size() << ", for target " << cur.first
                << " does not match the number of source points (" << set->getNbControlPt() << "). Target ignored.";
            else
            {
                // concat coordinates
                hbm::VCoord s;
                set->getCoordControlPoint(fem, s);
                pointsS.insert(pointsS.end(), s.begin(), s.end());
                pointsT.insert(pointsT.end(), targetValue.begin(), targetValue.end());

                // concat associations - only for source, in the end copy to target
                std::vector<double> asBones = set->getAssociationBones();
                if (asBones.size() != s.size())
                {
                    if (asBones.size() > 0)
                        pWarning() << "Per-point bones association for control point set " << cur.first <<
                        " will be ignored, amount of values (" << asBones.size() << ") does not match number of control points (" << s.size() << ").";
                    double asBonesGlobal = set->getAssociationBonesGlobal();
                    as_bones.insert(as_bones.end(), s.size(), std::isnan(asBonesGlobal) ? KRIGING_DEF_AS_BONESGLOBAL : asBonesGlobal); // if it is not set, consider it a bone
                }
                else
                    as_bones.insert(as_bones.end(), asBones.begin(), asBones.end());

                std::vector<double> asSkin = set->getAssociationSkin();
                if (asSkin.size() != s.size())
                {
                    if (asSkin.size() > 0)
                        pWarning() << "Per-point skin association for control point set " << cur.first <<
                        " will be ignored, amount of values (" << asSkin.size() << ") does not match number of control points (" << s.size() << ").";
                    double asSkinGlobal = set->getAssociationSkinGlobal();
                    as_skin.insert(as_skin.end(), s.size(), std::isnan(asSkinGlobal) ? KRIGING_DEF_AS_SKINGLOBAL : asSkinGlobal);
                }
                else
                    as_skin.insert(as_skin.end(), asSkin.begin(), asSkin.end());
                // concat nuggets
                std::vector<double> const& n = set->getWeight();
                if (n.size() != s.size())
                {
                    if (n.size() > 0)
                        pWarning() << "Per-point nuggets for control point set " << cur.first <<
                        " will be ignored, amount of values (" << n.size() << ") does not match number of control points (" << s.size() << ").";
                    if (std::isnan(set->getGlobalWeight()))
                        nuggets.insert(nuggets.end(), s.size(),
                        moduleParameter().getDouble(PARAM_MODULE_KRIG, PARAM_KRIG_NUGGET_DEFAULT));
                    else
                        nuggets.insert(nuggets.end(), s.size(), set->getGlobalWeight());
                }
                else
                    nuggets.insert(nuggets.end(), n.begin(), n.end());
            }
        }
    }

    target.setAssociation(as_bones, as_skin);
    target.setWeights(nuggets);
}

void Project::renameCurrentModel(std::string const& modelName) {
	pInfo() << START << "Rename current model into : " << modelName << ".";
    model().history().renameCurrentModel(modelName);
	pInfo() << DONE;
}

void Project::setCurrentModel(std::string const& historyname) {
	pInfo() << START << "Set current model : " << historyname << ".";
	model().history().setCurrentModel(historyname);
	pInfo() << DONE;
}


void Project::importModel(std::string const& modelrulesFile)
{   
    pInfo() << START
        << "New model | model description: " << modelrulesFile;

    // validity check of the model rules file
    validateXml(modelrulesFile, "ModelRules.dtd");

    m_model.initSource(modelrulesFile, QCoreApplication::applicationDirPath().toStdString());
    //check validity of the format rules file
    if (!m_model.metadata().sourceFormat().empty())
        validateXml(m_model.metadata().sourceFormat(), "FormatRules.dtd");

    // apply the source th create the model
    m_model.applySource();

    pInfo() << DONE;
}


void Project::exportModel(std::string const& directory)
{
    pInfo() << START << "Export model in: " << directory;
    //string message with piper version to add as comment in FE files
    std::vector<std::string> msgversion;
    msgversion.push_back(" This file was generated by:");
    msgversion.push_back("Piper application version " + std::to_string(PIPER_VERSION_MAJOR) + "." + std::to_string(PIPER_VERSION_MINOR) + std::to_string(PIPER_VERSION_PATCH));
    m_model.exportModel(directory, msgversion);
    pInfo() << DONE;
}

void Project::reloadFEMesh() {
	pInfo() << START << "Reload FE mesh of the baseline model.";
	//string message with piper version to add as comment in FE files
	m_model.reloadFEMesh();
	pInfo() << DONE;

}

void Project::loadTarget(std::string const& targetFile)
{
    pInfo() << START << "Import target: " << targetFile ;
    try
    {
        validateXml(targetFile, "target.dtd");
    }
    catch (std::runtime_error e)
    {
        if (e.what() != "File validity check cannot be started") // do not throw error on wrong target, only warning
            pWarning() << "XML validation failed, not all targets are defined correctly! Message: " << e.what();
        else
            throw e;
    }
    std::string errorLog = m_target.read(targetFile);
    if (errorLog != "")
        pWarning() << errorLog;
    m_target.convertToUnit(m_model.metadata().lengthUnit());
    m_target.convertToUnit(m_model.metadata().massUnit());
    m_target.convertToUnit(m_model.metadata().ageUnit());
    pInfo() << DONE << m_target.size() << " targets imported";
}

void Project::saveTarget(std::string const& targetFile) const
{
    pInfo() << START << "Export target: " << targetFile;
    m_target.write(targetFile);
    pInfo() << DONE;
}

void Project::addEnvironment(std::string const& name, std::string const& envModelFile, std::string const& formatRulesFile, units::Length lengthUnit) {
    pInfo() << START << "Adding environment model: " << envModelFile;
    validateXml(formatRulesFile, "FormatRules.dtd");
    if (!environment().addModel(name, envModelFile, formatRulesFile, lengthUnit, model().metadata().lengthUnit()))
        pWarning() << "Model with this name already exists! Original model was kept.";
    pInfo() << DONE;
}


void Project::validateXml(std::string const& file, std::string const& dtd) {
    static XMLValidationProcess xmllint;
    xmllint.setInput(QString::fromStdString(file), QString::fromStdString(dtd));
    if (!xmllint.canBeStarted())
        throw std::runtime_error("File validity check cannot be started");
    else if (!xmllint.isValid())
        throw std::runtime_error(file + " is not valid\n"
                                 + xmllint.readAllStandardOutput().constData());
}


}
