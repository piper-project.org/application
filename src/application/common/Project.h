/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_PROJECT_H
#define PIPER_PROJECT_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <string>

#include <QObject>

#include "hbm/HumanBodyModel.h"
#include "hbm/EnvironmentModels.h"
#include "hbm/target.h"
#include "ModuleParameter.h"
#include "BodySectionPersonalizer/AnthropoModel.h"

namespace piper {
    
/** This class represents a Piper project, it contains :
 *  - hbm::HumanBodyModel
 *  - hbm::TargetList
 *  - hbm::EnvironmentModels
 *  - BodySectionPersonalizer
 *
 * @todo manage several target lists
 * @author Thomas Lemaire @date 2016
 */
class PIPERCOMMON_EXPORT Project : public QObject
{
    Q_OBJECT

public:
    Project() {}
    Project(std::string const& projectFile);

    /// @return the current hbm::HumanBodyModel
    hbm::HumanBodyModel& model() { return m_model; }
    hbm::HumanBodyModel const& model() const { return m_model; }
    hbm::EnvironmentModels& environment() { return m_env; }
    /// @return the current TargetList
    hbm::TargetList& target() { return m_target; }
    hbm::TargetList const& target() const { return m_target; }
    ModuleParameter& moduleParameter() { return m_moduleParameter; }
    anthropometricmodel::AnthropoModel& anthropoModel() { return m_anthropomodel; }

    void clear();

    void read(std::string const& projectFile);
    void write(std::string const& projectFile);

    /// Replace the current target list with the one stored in \a targetFile
    void loadTarget(std::string const& targetFile);
    void saveTarget(std::string const& targetFile) const;

    /// load the FE model from \a modelFile (piper model in .pmd), the current FE model is replaced
    void loadModel(std::string const& modelFile);
    /// save the FE model to \a modelFile (piper model in .pmd)
    void saveModel(std::string const& modelFile);
	/// rename the current model
	void renameCurrentModel(std::string const& modelName);
	/// set current model in histroy
	void setCurrentModel(std::string const& historyname);

    void importModel(std::string const& modelrulesFile);
    void exportModel(std::string const& directory);
	void reloadFEMesh();

    std::string const& bodyDimDBFileName() const { return m_bodyDimDBFileName; }
    void setBodyDimDBFileName(std::string filename) { m_bodyDimDBFileName = filename; }

    void addEnvironment(std::string const& name, std::string const& envModelFile, std::string const& formatRulesFile, units::Length lengthUnit);

    Q_INVOKABLE bool isGravityDefined() const { return model().metadata().isGravityDefined(); }
    Q_INVOKABLE unsigned int targetSize() const { return static_cast<unsigned int>(target().size()); }

    /// save the model metadata to \a metadataFile (piper metadata in .pmr)
    void saveMetadata(std::string const& metadataFile, bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool controlpoint = true, bool HbmParameter = true);
    /// load the model metadata from \a metadataFile (piper model in .pmr), the current model metadata are deleted and replaced by new ones
    void loadMetadata(std::string const& metadataFile, bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool anatomicalJoint = true, bool controlpoint = true, bool HbmParameter = true);
    
    /// <summary>
    /// Gathers all pairs of source and target control points for scaling in one collection based on the specified name association.
    /// The source control points are read from the current model's metadata, the targets are first searched for in the project's target list
    /// and if it does not have CPs with such name, it searches through the metada for CPs that are not specified as of role ControlPointRole::CONTROLPOINT_SOURCE. 
    /// All CP parameters, i.e. nugget, bone and skin association will be "unrolled" into a per-point representation. That is if for example a given
    /// control point set has no per-points nugget defined, but has a global nugget, the per-point nugget array (InteractionControlPoint::getWeight())
    /// will be created and filled with the global nugget for each point.If neither global nor per-point values are assigned, the default values
    /// will be used for the unrolling (i.e. nugget obtained from moduleParameter and as_skin/as_bones from KRIGING_DEF_AS_SKINGLOBAL / KRIGING_DEF_AS_BONESGLOBAL).
    /// The parameter vectors are shared by reference between the source and target.
    /// </summary>
    /// <param name="source">The container to store all source points. The points are concatenated in the order the associations
    /// are stored in the association map. This method does not clear the containers - if they already have some points, new ones will be appended.</param>
    /// <param name="target">The container to store all target points. The points are concatenated in the order the associations
    /// are stored in the association map. This method does not clear the containers - if they already have some points, new ones will be appended.</param>
    void collectControlPoints(piper::hbm::InteractionControlPoint& source, piper::hbm::InteractionControlPoint& target, 
        std::map<std::string, std::string> association);

    /** xml validation of \a file against \a dtd
    * \throw std::runtime_error on failure
    */
    static void validateXml(std::string const& file, std::string const& dtd);

private:

    hbm::HumanBodyModel m_model;
    hbm::EnvironmentModels m_env;
    hbm::TargetList m_target;
    std::string m_bodyDimDBFileName;
    ModuleParameter m_moduleParameter;

    anthropometricmodel::AnthropoModel m_anthropomodel;

    
    /// no logging verion for internal usage
    void _loadModel(std::string const& modelFile);

};

}

#endif // PIPER_PROJECT_H
