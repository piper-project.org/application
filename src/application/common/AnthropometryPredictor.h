/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANTHROPOPREDICTOR_H
#define PIPER_ANTHROPOPREDICTOR_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <QObject>
#include <QString>
#include <QProcess>
#include <QUrl>

namespace piper {

class OctaveProcess;

/** This class integrates interface for the WP2 anthropometry predictor into the Piper application.
 *
 * \author E. Joivet \date 2017
 */
class PIPERCOMMON_EXPORT AnthropometryPredictor : public QObject {
    Q_OBJECT

public:

    /** Call the octave script for prediction.
    *
    * 1. produce the input files
    * 2. call the script with its required parameters
    *
    * \param op to be used to run the scripts
    * \param inputCSVFile input for satistics tools
    *
    * \see OctaveProcess
    */
    void computePrediction(piper::OctaveProcess* op, QString inputCSVFile);

    /** Call the octave script for prediction.
     *
     * 1. produce the input files
     * 2. call the script with its required parameters
     *
     * \param op to be used to run the scripts
     * \param inputCSVFile input for satistics tools
     * \param userInputArray input prediction parameters
     *
     * \see OctaveProcess
     */
    void computePrediction(piper::OctaveProcess* op, QString inputCSVFile, QStringList userInputArray, bool genLandmarks = false);

    /** Non blocking version of compute()
     * \sa compute()
     */
    Q_INVOKABLE void startPrediction(QString inputCSVFile, QStringList userInputArray, bool genLandmarks);

    /** Call the octave script to get info from reressions.
    *
    * 1. produce the input files
    * 2. call the script with its required parameters
    *
    * \param op to be used to run the scripts
    * \param inputRegressionFile input 
    *
    * \see OctaveProcess
    */
    void computeGetInfo(piper::OctaveProcess* op, QUrl pathToRegressionFile);

    /** Non blocking version of compute()
    * \sa compute()
    */
    Q_INVOKABLE void startGetInfo(QUrl pathToRegressionFile);
signals:
    /// This signal is emitted when a startCompute() finishes successfully
    void computeFinished();

private:
    // uses share/python/scripting/exportLandmarks.py - returns false if this script failed
    bool setupOctavePredictionProcess(piper::OctaveProcess* op, QString inputCSVFile, QStringList userInputArray, bool genLandmarks);
    void setupOctavePredictionProcess(piper::OctaveProcess* op, QString inputCSVFile);
    void setupOctaveGetInfoProcess(piper::OctaveProcess* op, QUrl pathToRegressionFile);
    void prepareCSVfile(QString inputCSVFile, QStringList userInputArray, bool genLandmarks);
    std::string getDataSetfromCSVfile(QString inputCSVFile);
    void prepareCSVfileRelativeScaling(QString inputCSVFile, QString dataset);
    bool m_relative;
    std::string const relativeCSVFile = "anthropoPersoUserHbmInput.csv";

private slots:
    void onScriptFinished(int exitCode, QProcess::ExitStatus exitStatus);


};

}

#endif // PIPER_ANTHROPOPREDICTOR_H
