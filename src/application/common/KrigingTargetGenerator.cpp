/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "KrigingTargetGenerator.h"

#include "common/logging.h"
#include "common/OctaveProcess.h"
#include "common/Context.h"

namespace piper {



    void KrigingTargetGenerator::compute(piper::OctaveProcess* op, QString ScriptPath, QString ControlPointSet, QString inputCPFile, QString outputCPFile)
    {
        op->setScriptPath(ScriptPath);
        op->setScriptArguments(QStringList() << ControlPointSet << inputCPFile << outputCPFile);
        pInfo() << START << "Compute Target Control Points";
        try {
            op->executeScript();
        }
        catch (std::exception const& e) {
            pCritical() << "Kriging Target Control Points: " << e.what();
        }
        pInfo() << DONE;
    }

    void KrigingTargetGenerator::compute(QString ScriptPath, QString ControlPointSet, QString inputCPFile, QString outputCPFile) {
        compute(Context::instance().octaveProcessPtr(), ScriptPath, ControlPointSet, inputCPFile, outputCPFile);
    }

}
