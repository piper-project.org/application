/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "message.h"

#include <QTime>

#include "Context.h"

namespace piper {

// Used to measure duration between a START and a DONE
static QTime timing;

QTextStream& DEBUGMSG(QTextStream& out)
{
        return out << "<font face=\"monospace\" color=\"purple\">[DEBUG]&nbsp;&nbsp;</font> ";
}

QTextStream& SUCCESS(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"green\">[SUCCESS]</font> ";
}

QTextStream& INFO(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"blue\">[INFO]&nbsp;&nbsp;&nbsp;</font> ";
}

QTextStream& START(QTextStream& out)
{
    timing.start();
    return out << "<font face=\"monospace\" color=\"green\">[START]&nbsp;&nbsp;</font> ";
}

QTextStream& DONE(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"green\">[DONE]&nbsp;&nbsp;&nbsp;</font> " << TIMING(timing.elapsed()) << " ";
}

QTextStream& WARNING(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"orangered\">[WARNING]</font> ";
}

QTextStream& ERRORMSG(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"red\">[ERROR]&nbsp;&nbsp;</font> ";
}

// QTextStream& FAILED(QTextStream& out)
QTextStream& FAILEDMSG(QTextStream& out)
{
    return out << "<font face=\"monospace\" color=\"red\">[FAILED]&nbsp;</font> ";
}

QTextStream& endl(QTextStream& out)
{
    return out << "<br/>";
}

QString TIMING(unsigned int msec)
{
    QTime time(0,0);
    time = time.addMSecs(msec);
    QString format="mm:ss";
    if (time.hour()>0)
        format = "hh:"+format;
    return "timing: " + time.toString(format);
}

}
