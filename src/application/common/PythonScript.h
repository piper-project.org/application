/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_PYTHONSCRIPT_H
#define PIPER_PYTHONSCRIPT_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <QObject>
#include <QString>
#include <QUrl>
#include <QStringList>

namespace piper {

/** This class offers an simple interface to run an external python script.
 *
 * \todo make the runAsynchronous() spawn its own thread, not using the mechanism in Context, to have its own "scriptFinished" signal
 * \author Thomas Lemaire \date 2016
 */
class PIPERCOMMON_EXPORT PythonScript : public QObject {
    Q_OBJECT

public:
    PythonScript();

    QString path() {return m_scriptPath;}
    void setPath(QString scriptPath) {m_scriptPath = scriptPath;}
    Q_INVOKABLE QUrl pathUrl() const;
    Q_INVOKABLE void setPathUrl(QUrl scriptPath);

    Q_INVOKABLE QStringList args() const {return m_args;}
    Q_INVOKABLE void setArgs(QStringList args = QStringList()) {m_args = args;}
    Q_INVOKABLE QString description() const;

    /** Run the script asynchroneously.
    */
    Q_INVOKABLE void runAsynchronous();

    /// run the current script file with its current arguments
    /// returns true if the execution of the script was succesful, false otherwise    
    bool run(QString scriptPath, QStringList args = QStringList());
    Q_INVOKABLE bool run();
    bool success() const {return m_success;}

signals:
    void descriptionChanged();

private:
    bool m_success;
    QString m_scriptPath;
    QStringList m_args;

    void updateDescription();
};

}

#endif // PIPER_PYTHONSCRIPT_H
