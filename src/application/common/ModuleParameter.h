/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_MODULEPARAMETER_H
#define PIPER_MODULEPARAMETER_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <list>

#include <QObject>
#include <QString>
#include <QQmlPropertyMap>

#include <tinyxml/tinyxml2.h>

namespace piper {

    const std::string PARAM_MODULE_KRIG = "Kriging";
    const std::string PARAM_KRIG_NUGGET_DEFAULT = "defaultNugget"; // nugget to apply when no nugget is chosen

    // kriging in a box parameters - see KrigingPiperInterface::applyDeformationInABox
    const std::string PARAM_KRIG_SPLIT_THRESHOLD_CP = "splitThresholdCP";
    const std::string PARAM_KRIG_SPLIT_OVERLAP = "splitBoxOverlap";
    const std::string PARAM_KRIG_INTERP_DISPLACEMENT = "interpolateDisplacement";
    const std::string PARAM_KRIG_USE_DRIFT = "useDrift";
    const std::string PARAM_KRIG_GEODESIC_TYPE = "geoType";
    const std::string PARAM_KRIG_GEODESIC_PRECISION = "geoPrecision";

    // default values for the as_skin/as_bones global parameters
    static const double KRIGING_DEF_AS_BONESGLOBAL = 1;
    static const double KRIGING_DEF_AS_SKINGLOBAL = 0;

/** Parameters for the piper modules.
 *
 * \todo: serialization to project xml file
 * \todo: add help text for each parameter to be displayed in the GUI
 *
 * @author Thomas Lemaire @date 2015
 */
class PIPERCOMMON_EXPORT ModuleParameter
{
public:

    typedef QQmlPropertyMap ModuleParameterType;

    /** Constructor, set default values.
     */
    ModuleParameter();

    /// set parameters default values
    void setDefault();

    ModuleParameterType const& parameter() const { return m_parameter; }
    ModuleParameterType & parameter() { return m_parameter; }

    bool getBool(std::string const& moduleName, std::string const& parameterName);
    unsigned int getUInt(std::string const& moduleName, std::string const& parameterName);
    std::list<unsigned int> getListUInt(std::string const& moduleName, std::string const& parameterName);
    double getDouble(std::string const& moduleName, std::string const& parameterName);


    template<typename T>
    void set(std::string const& moduleName, std::string const& parameterName, T value)
    {
        QVariant valueVariant(value);
        QString key = checkKey(moduleName, parameterName);
        if (!valueVariant.convert(m_parameter[key].type()))
            throw std::runtime_error("ModuleParameter: invalid parameter type module: "+moduleName+" parameter: "+parameterName);
        m_parameter.insert(key, valueVariant);
    }

    /** Parse moduleParameters from \a xmlDocument
     */
    void parseXml(const tinyxml2::XMLElement* parentElement);
    /** Serialize targetList to \a xmlElement
     */
    void serializeXml(tinyxml2::XMLElement* parentElement) const;

private:
    ModuleParameterType m_parameter; ///< modules parameters stored as m_parameter["MyModule_myParameter"]
    QString key(std::string const& moduleName, std::string const& parameterName);
    QString checkKey(std::string const& moduleName, std::string const& parameterName);
    void init(std::string const& moduleName, std::string const& parameterName, QVariant value);


};

} // namespace piper

#endif // PIPER_MODULEPARAMETER_H
