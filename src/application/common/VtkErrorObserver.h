/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_VTKERROROBSERVER_H
#define PIPER_VTKERROROBSERVER_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <vtkCommand.h>

namespace piper {
    
    /// For catching warnings and errors happening inside VTK modules and write them out in the PIPER (Context) console
    /// typical usage: 
    /// vtkSmartPointer<VtkErrorObserver> errorObserver = vtkSmartPointer<VtkErrorObserver>::New();
    /// myVtkFilter->AddObserver(vtkCommand::ErrorEvent, m_errorObserver);
    /// myVtkFilter->AddObserver(vtkCommand::WarningEvent, m_errorObserver);
    /// ... do stuff with myVtkFilter...
    /// if (errorObserver>GetError()) handle error state;
    /// else if (errorObserver->GetWarning()) handle warning state (most likely do nothing);
    /// else everything went well
    class PIPERCOMMON_EXPORT VtkErrorObserver : public vtkCommand
    {
    public:
        VtkErrorObserver() :
            Error(false),
            Warning(false),
            ErrorMessage(""),
            WarningMessage("") {}

        static VtkErrorObserver *New()
        {
            return new VtkErrorObserver;
        }
        ~VtkErrorObserver();
        bool GetError() const;
        bool GetWarning() const;
        // call this to erase all error and warning messages (after you've reported them) in order to reuse the same instance of errorObserver
        void Clear();
        virtual void Execute(vtkObject *caller, unsigned long ev, void *calldata);
        std::string GetErrorMessage();
        std::string GetWarningMessage();
    private:
        /// In case we are not in a verbose mode, strips the message off of debug info like file names and object addresses, otherwise leaves it intact.
        void processMessage(std::string & message);
        bool        Error;
        bool        Warning;
        // when error happens, the VTK global error display is turned off. this flag tells whether we should turn it back on once we're done processing the error
        bool turnErrorDisplayBackOn = false;
        std::string ErrorMessage;
        std::string WarningMessage;
    };

} // namespace piper

#endif
