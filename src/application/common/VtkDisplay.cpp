/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "VtkDisplay.h"
#include "logging.h"

#include <vtkIdTypeArray.h>

#include <vtkSphereSource.h>
#include <vtkAppendPolyData.h>

#include <vtkLookupTable.h>
#include <vtkPointData.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkMergePoints.h>
#include <vtkGlyph3DMapper.h>

#include <vtkCell.h>
#include <vtkRenderer.h>

#include <vtkDataSetSurfaceFilter.h>

#include <vtkAlgorithmOutput.h>

/*
VtkDisplay requires calling an initialization method. They create a vtkRenderWindow if the rw variable is null, create a default renderer and interactor 
and interconnects them properly with the rw.

string-map "actors" serves for storing all actors in the scene, they are identified by a name; it is users responsibility to keep those names unique.

*/

using namespace piper::hbm;


namespace piper {


    // the ordering is based on the implementation of vtkAxesActor in VTK 7.0.0, in theory this does not work universally
    const std::string VtkDisplay::axisPartsNameSuffix[] = { "_XShaft", "_YShaft", "_ZShaft", "_XTip", "_YTip", "_ZTip" };

	VtkDisplay::VtkDisplay() : frameBuffer(nullptr)
	{
		st = vtkSmartPointer<vtkSelectionTools>::New();
		SetDisplayMode(DISPLAY_MODE::ENTITIES);
        SetPickableDisplayMode(DISPLAY_MODE::ALL);
        // read the array names directly from the vtkDataSetSurfaceClass in case somebody for some reason decides to change it in future releases
        vtkSmartPointer<vtkDataSetSurfaceFilter> temp = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
        this->OriginalCellIdsArrayName = temp->GetOriginalCellIdsName();
        this->OriginalPointIdsArrayName = temp->GetOriginalPointIdsName();
        mConnect = vtkSmartPointer<vtkEventQtSlotConnect>::New();
        globalClipPlane = vtkSmartPointer<vtkPlane>::New();
	}

	VtkDisplay::~VtkDisplay()
	{

        mConnect->Disconnect();
	}
    
    void VtkDisplay::setupScene()
	{
        initialize(); // just in case someone forgets to call initialize themselves
        if (renderNormals)
            ActivateDisplayMode(DISPLAY_MODE::HIGHLIGHTERS);
        rw->hasTransparentObjects = false;
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> highlightersToRemove;
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> defaultRendererActors;
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> pickingRendererActors;
		// activate actors based on current display mode
		for (auto it = actors.begin(); it != actors.end(); it++)
		{
			// add it to the scene if it fits at least one of the display modes, otherwise remove it from the scene
            if (((it->second->GetDisplayMode() & currentDisplayMode) != 0))
            {
                defaultRendererActors.push_back(it->second);
                if (it->second->GetPickable() && (it->second->GetDisplayMode() & pickableMode) != 0) // if it is pickable and at least one of it's modes is set as a pickable mode
                    pickingRendererActors.push_back(it->second);
                if (it->second->GetProperty()->GetOpacity() < 1)
                    rw->hasTransparentObjects = true;
            }
            else // if it is not added, make sure that the dependent actors are not added as well - remove it from the list of actors to add
            {
                if (it->second->GetPointHighlights() && (it->second->GetPointHighlights()->GetDisplayMode() & currentDisplayMode) != 0)
                    highlightersToRemove.push_back(it->second->GetPointHighlights());
                if (it->second->GetSelectedPointHighlights() && (it->second->GetSelectedPointHighlights()->GetDisplayMode() & currentDisplayMode) != 0)
                    highlightersToRemove.push_back(it->second->GetSelectedPointHighlights());
                if (it->second->GetNormalHighlights() && (it->second->GetNormalHighlights()->GetDisplayMode() & currentDisplayMode) != 0)
                    highlightersToRemove.push_back(it->second->GetNormalHighlights());
            }
		}
        // remove the highlighters of the unused actors before sending the lists to render window
        for (auto it = highlightersToRemove.begin(); it != highlightersToRemove.end(); it++)
        {
            vtkSmartPointer<vtkProp> highlighter = *it;
            for (auto iter = defaultRendererActors.begin(); iter != defaultRendererActors.end(); iter++)
            {
                if ((*iter) == highlighter) // if this is the same prop, remove it from the list
                {
                    defaultRendererActors.erase(iter);
                    for (auto iterp = pickingRendererActors.begin(); iterp != pickingRendererActors.end(); iterp++) //it can be in picking only if it is also in default
                    {
                        if ((*iterp) == highlighter) // if this is the same prop, remove it from the list
                        {
                            pickingRendererActors.erase(iterp);
                            break;
                        }
                    }
                    break;
                }
            }
        }
        // finally, reset the scene
        vtkSmartPointer<vtkRenderer> defRenderer = rw->GetDefaultRenderer();
        vtkSmartPointer<vtkRenderer> pickRenderer = rw->GetPickingRenderer();

        defRenderer->RemoveAllViewProps(); // remove all actors
        pickRenderer->RemoveAllViewProps();
        for (auto it = defaultRendererActors.begin(); it != defaultRendererActors.end(); it++)
            defRenderer->AddActor(*it);
        for (auto it = pickingRendererActors.begin(); it != pickingRendererActors.end(); it++)
            pickRenderer->AddActor(*it);

        if (resetCamera)
        {
            defRenderer->ResetCamera();
            resetCamera = false;
        }
	}

	void VtkDisplay::Refresh(bool resetCam)
	{
        if (rw)
        {
            if (rw->IsRenderingBlocked())
                refreshRequested = true;
            else
            {
                this->resetCamera = resetCam || this->resetCamera; // if resetCamera is true because it was set somewhere else, let it be true
                setupScene(); // make sure that the correct actors are loaded
                rw->scenePickerUpToDate = false;
                rw->Render();
                refreshRequested = false;
            }
        }
	}

    void VtkDisplay::Render()
    {
        if (rw)
            rw->Render();
    }

    void VtkDisplay::ResetCamera()
    {
        if (rw && !rw->IsRenderingBlocked()) // Q_INVOKABLE action -> check everything is ok before doing anything
        {
            rw->GetDefaultRenderer()->ResetCamera();
            rw->Render();
			rw->GetDefaultRenderer()->GetActiveCamera()->SetViewUp(0,0,1);
			rw->Render();
        }
    }

	void VtkDisplay::SetCamera(int axis)
	{
		if (rw && !rw->IsRenderingBlocked()) // Q_INVOKABLE action -> check everything is ok before doing anything
		{
            auto cam = rw->GetDefaultRenderer()->GetActiveCamera();
            cam->SetFocalPoint(0, 0, 0);
			switch (axis){
				case 1:{
					cam->SetPosition(0, -1, 0);
					cam->SetViewUp(0, 0, 1);
					break;
				}
				case 2:{
					cam->SetPosition(0, 0, -1);
					cam->SetViewUp(1, 0, 0);
					break;
				}
                case 3:{
                    cam->SetPosition(1, 0, 0);
                    cam->SetViewUp(0, 0, 1);
                    break;
                }
                case 4:{
                    cam->SetPosition(0, 1, 0);
                    cam->SetViewUp(0, 0, 1);
                    break;
                }
                case 5:{
                    cam->SetPosition(0, 0, 1);
                    cam->SetViewUp(1, 0, 0);
                    break;
                }
                case 0:
				default:{
					cam->SetPosition(-1, 0, 0);
					cam->SetViewUp(0, 0, 1);
					break;
				}
			}
			rw->GetDefaultRenderer()->ResetCamera();
			rw->Render();
		}
	}

    void VtkDisplay::SetUseParallelProjection(const QVariant &value)
    {
        this->useParallelProjection = value.toBool();
        if (rw && !rw->IsRenderingBlocked()) // Q_INVOKABLE action -> check everything is ok before doing anything
        {
            rw->GetDefaultRenderer()->GetActiveCamera()->SetParallelProjection(useParallelProjection);
            rw->Render();
        }
        emit useParallelProjectionChanged();
    }
        
    void VtkDisplay::SetRenderEdges(const QVariant &value)
    {
        renderEdges = value.toBool();
        for (auto actor : actors)
            actor.second->GetProperty()->SetEdgeVisibility(renderEdges);
        Render();
        emit renderEdgesChanged();
    }

    void VtkDisplay::SetRenderNormals(const QVariant &value)
    {
        renderNormals = value.toBool();

        if (renderNormals)
        {
            if (rw->GetContextSupportsOpenGL32())
            {
                // AddActor would break the iteration through it, so put all actors to a list and then add them all at once
                boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> toAdd;
                for (auto actor : actors)
                {
                    if (((unsigned int)(DISPLAY_MODE::FULL_MESH) & actor.second->GetDisplayMode()) != 0
                        || ((unsigned int)(DISPLAY_MODE::ENTITIES) & actor.second->GetDisplayMode()) != 0)
                    {
                        renderActorsNormal(actor.second);
                        toAdd[actor.second->GetNormalHighlights()->GetName()] = actor.second->GetNormalHighlights();
                    }
                }
                for (auto actor : toAdd)
                    AddActor(actor.second, actor.first, actor.second->GetIsPersistent(), DISPLAY_MODE::HIGHLIGHTERS, false, false);
            }
            else
                renderNormals = false;
        }
        else
        {
            for (auto actor : actors)
            {
                if (actor.second->GetNormalHighlights())
                    RemoveActor(actor.second->GetNormalHighlights()->GetName());
            }
        }
        Refresh();
        emit renderNormalsChanged();
    }

    void VtkDisplay::renderActorsNormal(vtkSmartPointer<VtkDisplayActor> actor)
    {
        actor->GetMapper()->Update();
        vtkSmartPointer<vtkPolyData> renderData = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
        vtkDataArray *active = renderData->GetCellData()->GetScalars();
        vtkSmartPointer<vtkGlyph3DMapper> mapper = this->highlights.CellNormals(renderData, this->FRAME_LENGTH / 3.0);
        if (active == NULL)
            actor->GetMapper()->SetScalarVisibility(0); // do not color based on the normals
        else // if some scalars were active before creating the normals, set it back as active
            renderData->GetCellData()->SetActiveScalars(active->GetName());

        vtkSmartPointer<VtkDisplayActor> normalActor = vtkSmartPointer<VtkDisplayActor>::New();
        normalActor->SetMapper(mapper);
        normalActor->SetName(actor->GetName() + "_normals");
        mapper->ScalarVisibilityOff();
        actor->SetNormalHighlights(normalActor);
        normalActor->SetVisibility(actor->GetVisibility());
    }

    bool VtkDisplay::GetUseParallelProjection()
    {
        return this->useParallelProjection;
    }

    bool VtkDisplay::GetRenderEdges()
    {
        return this->renderEdges;
    }

    bool VtkDisplay::GetRenderNormals()
    {
        return this->renderNormals;
    }

    int VtkDisplay::GetBlankLevel()
    {
        return blankLevel;
    }


    void VtkDisplay::RemoveActor(std::string const& name, bool removeDependentActors)
    {
        auto itActor = actors.find(name);
        if (itActor != actors.end()) // if there is such object
        {
            vtkSmartPointer<VtkDisplayActor> toRemove = itActor->second;
            // first remove dependent actors
            if (removeDependentActors)
            {
                if (toRemove->GetPointHighlights() != NULL)
                    RemoveActor(toRemove->GetPointHighlights()->GetName());
                if (toRemove->GetSelectedPointHighlights() != NULL)
                    RemoveActor(toRemove->GetSelectedPointHighlights()->GetName());
                if (toRemove->GetNormalHighlights() != NULL)
                    RemoveActor(toRemove->GetNormalHighlights()->GetName());
            }
            // then finally the actor itself
            actors.erase(actors.find(name));
        }
        else if (actors.find(name + axisPartsNameSuffix[0]) != actors.end()) // if there is an axes with this name
        {
            vtkSmartPointer<VtkDisplayActor> axesActor = actors[name + axisPartsNameSuffix[0]];
            // remove all other parts
            for (auto it = axesActor->GetOtherParts()->begin(); it != axesActor->GetOtherParts()->end(); it++)
                RemoveActor(it->Get()->GetName()); // call remove actor with the actual name
            RemoveActor(axesActor->GetName());
        }
    }

    void VtkDisplay::RemoveAllActors()
    {
        actors.clear();
        ClearSelectionBoxes();
    }

    void VtkDisplay::SetIsCameraInMotion(bool isInMotion)
    {
        rw->scenePickerUpToDate = isInMotion; // fake-mark the scene picker as up to date in order to block updating it
    }

	void VtkDisplay::SetDisplayMode(DISPLAY_MODE type)
	{
		currentDisplayMode = (unsigned int)type;
        emit displayModeChanged();
	}

    void VtkDisplay::ActivateDisplayMode(unsigned int mode)
    {
        currentDisplayMode = currentDisplayMode | mode;
        emit displayModeChanged();
    }

    void VtkDisplay::DeactivateDisplayMode(unsigned int mode)
    {
        currentDisplayMode = currentDisplayMode & (0xFFFFFFFF ^ mode); // mask current mode by inverse of the mode that is supposed to be deactivated 
        emit displayModeChanged();
    }

    bool VtkDisplay::IsDisplayModeActive(unsigned int mode)
    {
        return (currentDisplayMode & mode) != 0;
    }


    void VtkDisplay::SetPickableDisplayMode(DISPLAY_MODE type)
    {
        pickableMode = (unsigned int)type;
    }

    void VtkDisplay::ActivatePickableDisplayMode(unsigned int mode)
    {
        pickableMode = pickableMode | mode;
    }

    void VtkDisplay::DeactivatePickableDisplayMode(unsigned int mode)
    {
        pickableMode = pickableMode & (0xFFFFFFFF ^ mode); // mask current mode by inverse of the mode that is supposed to be deactivated 
    }

    void VtkDisplay::SetActorPickable(std::string name, bool pickable)
    {
        auto actorIt = actors.find(name);
        if (actorIt != actors.end())
        {
            actorIt->second->SetPickable(pickable);
        }
    }

    void VtkDisplay::SetActorPointHighlightersPickable(std::string name, bool pickable)
    {
        auto actorIt = actors.find(name);
        if (actorIt != actors.end())
        {
            if (actorIt->second->GetPointHighlights()) 
                actorIt->second->GetPointHighlights()->SetPickable(pickable);
            if (actorIt->second->GetSelectedPointHighlights())
                actorIt->second->GetSelectedPointHighlights()->SetPickable(pickable);
        }
    }

    void VtkDisplay::SetActorVisible(std::string const& name, bool visible)
    {
        auto actorIt = actors.find(name);
        if (actorIt != actors.end())
        {
            actorIt->second->SetVisibility(visible);
            // change the visibility also for the highlighters
            if (actorIt->second->GetPointHighlights())
                actorIt->second->GetPointHighlights()->SetVisibility(visible);
            if (actorIt->second->GetSelectedPointHighlights())
                actorIt->second->GetSelectedPointHighlights()->SetVisibility(visible);
            if (actorIt->second->GetNormalHighlights())
                actorIt->second->GetNormalHighlights()->SetVisibility(visible);
            if (rw && actorIt->second->GetPickable())
                rw->scenePickerUpToDate = false;
        }
    }
    void VtkDisplay::SetActorVisible(QString name, bool visible) {
        SetActorVisible(name.toStdString(), visible);
    }

    bool VtkDisplay::HasActor(std::string name)
    {
        return actors.find(name) != actors.end();
    }

    bool VtkDisplay::IsActorVisible(std::string name)
    {
        auto actorIt = actors.find(name);
        if (actorIt != actors.end() && actorIt->second->GetVisibility() &&
            (currentDisplayMode & actorIt->second->GetDisplayMode()) != 0)
            return true;
        return false;
    }

    void VtkDisplay::SetFrameVisible(std::string name, bool visible)
    {
        auto actorIt = actors.find(name + axisPartsNameSuffix[0]);
        if (actorIt != actors.end())
        {
            actorIt->second->SetVisibility(visible);
            // do the same for all the other parts of the axes
            for (auto it = actorIt->second->GetOtherParts()->begin(); it != actorIt->second->GetOtherParts()->end(); it++)
                it->Get()->SetVisibility(visible);
        }
    }

    bool VtkDisplay::HasFrame(std::string name)
    {
        return actors.find(name + axisPartsNameSuffix[0]) != actors.end();
    }

    bool VtkDisplay::IsFrameSelected(std::string name)
    {
        auto actorIter = actors.find(name + axisPartsNameSuffix[0]);
        if (actorIter != actors.end())
            return actorIter->second->GetIsSelected();
        return false;
    }

    void VtkDisplay::SelectionTargetChanged(SELECTION_TARGET currentSelectionTarget)
    {
        activeSelectionTarget = currentSelectionTarget;
        switch (currentSelectionTarget)
        {
        case SELECTION_TARGET::ELEMENTS:
        case SELECTION_TARGET::FACES:
            selectedActorHighlighting(false);// turn off coloring by entity selection
            rw->scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS);
            rw->blockScenePicker = false;
            // turn on coloring by cell data for each active actor
            HighlightElementsByScalars(SELECTED_PRIMITIVES, false, "", NULL, false); // no parameters == all objects, color by selection, default lookuptable, but dont update based on the parent of blanked actors
            break;
        case SELECTION_TARGET::NODES:
            rw->scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS); // point picking is based on cell picking as well
            rw->blockScenePicker = false;
            break;
        case SELECTION_TARGET::ENTITIES:
            HighlightElementsByScalarsOff();
            rw->blockScenePicker = false;
            rw->scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS);
            selectedActorHighlighting(true);
            break;
        case SELECTION_TARGET::FREE_POINTS:
            rw->scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_POINTS); // assumes that all free_points are represented by vtkGlyph3DMapper!
            rw->blockScenePicker = false;
            break;
        case SELECTION_TARGET::CREATE_POINTS:
            rw->scenePicker->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS); // point picking is based on cell picking as well
            rw->blockScenePicker = false;
            break;
        case SELECTION_TARGET::NONE:
        default:
            rw->blockScenePicker = true; // to save resource, do not do the scenePicker rendering pass if there is no need for it
            break;
        }
        Refresh();
    }

    void VtkDisplay::UpdateActor(std::string name)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end())
        {
            actorIter->second->GetMapper()->Update();
            // update number of selected primitives
            vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(actorIter->second->GetOriginalDataSet(), false);
            if (sel_prim != nullptr)
            {
                int counter = 0;
                vtkIdType numTup = sel_prim->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        counter++;
                }
                actorIter->second->AllPointsDeselected();
                actorIter->second->PointsSelected(counter);
            }
            sel_prim = vtkSelectionTools::ObtainSelectionArrayCells(actorIter->second->GetOriginalDataSet(), false);
            if (sel_prim != nullptr)
            {
                int counter = 0;
                vtkIdType numTup = sel_prim->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        counter++;
                }
                actorIter->second->AllCellsDeselected();
                actorIter->second->CellsSelected(counter);
            }
        }
    }

    void VtkDisplay::SetActorScale(std::string name, double x, double y, double z)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end())
            actorIter->second->SetScale(x, y, z);
    }

    void VtkDisplay::SetActorTranslation(std::string name, double x, double y, double z)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end())
            actorIter->second->SetPosition(x, y, z);
    }


    void VtkDisplay::SetActorRotation(std::string name, double x, double y, double z)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end())
            actorIter->second->SetOrientation(x, y, z);
    }
    
    void VtkDisplay::UseGlobalClipping(bool on)
    {
        // note - actors that are added AFTER the clipping has been turned on will not be clipped
        // this is kind of a bug+feature, since you get to see newly added things without wondering why are they not showing when they are in the clipped zone
        // but maybe should be handled more consistently somehow (like keep a variable clipPlaneOn and check it when AddVtkPointSet / AddAxesActor, if true set the clip plane)
        for (auto actor : actors)
        {
            // first remove the previous plane to avoid duplicates in case UseGlobalClipping(true) is called multiple times (shouldn't, but just in case)
            // this also removes it if on == false
            if (actor.second->GetMapper()->GetClippingPlanes() != NULL) 
                actor.second->GetMapper()->RemoveClippingPlane(globalClipPlane);
            if (on)
                actor.second->GetMapper()->AddClippingPlane(globalClipPlane);
        }
    }

    void VtkDisplay::SetGlobalClipPlanePosition(double distanceFrom0)
    {
        double normal[3];
        globalClipPlane->GetNormal(normal);
        vtkMath::Normalize(normal);
        vtkMath::MultiplyScalar(normal, distanceFrom0);
        globalClipPlane->SetOrigin(normal);
    }

    void VtkDisplay::SetGlobalClipPlaneNormal(double nx, double ny, double nz)
    {
        globalClipPlane->SetNormal(nx, ny, nz);
    }

	void VtkDisplay::initialize()
	{
		if (rw == nullptr)
		{
			// if it is rendered by the qt, the source qml document should have a VtkRenderWindow
			// (which is actually a qt-registered name for QVTKFrameBufferObjectItem) component
			if (frameBuffer)
			{
                rw = frameBuffer->GetRenderWindow();
                if (rw) // if rw was succesfully created
                {
                    vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
                    vtkSmartPointer<vtkRenderer> pickingRenderer = vtkSmartPointer<vtkRenderer>::New();

                    // Qt and VTK use different Y-axis coordinates - make the tranformation
                    vtkSmartPointer<vtkTransform> scale = vtkSmartPointer<vtkTransform>::New();
                    scale->Scale(1, -1, 1);

                    // if some camera was already used before, use it's settings to maintain the position and lookat even when switching modules
                    if (activeCamera != NULL)
                        renderer->SetActiveCamera(activeCamera);
                    else
                    {
                        renderer->GetActiveCamera()->SetUserTransform(scale); // note - both renderers use the same camera, so this will switch the picking render's transform as well
                        renderer->GetActiveCamera()->SetParallelProjection(useParallelProjection);
                        activeCamera = renderer->GetActiveCamera();
                    }

                    rw->SetDefaultRenderer(renderer);
                    rw->SetPickingRenderer(pickingRenderer);

                    vtkSmartPointer<vtkMousePickStylePIPER> style = vtkSmartPointer<vtkMousePickStylePIPER>::New(); // picking demo
                    style->SetPickingType(SELECTION_TARGET::NONE, false, false); // turn off selection by default
                    style->SetParentDisplay(this);
                    style->SetDefaultRenderer(renderer);

                    // create a default interactor
                    vtkSmartPointer<QVTKInteractor> iren = vtkSmartPointer<QVTKInteractor>::New();
                    iren->Initialize();

                    iren->SetInteractorStyle(style);
                    rw->SetInteractor(iren);

                    emit useParallelProjectionChanged();
                }
			}
			else
			{
                pCritical() << "Rendering target for VtkDisplay not found in the QML definition. Check that the QML files (GUI) are valid.";
                return;
			}
		}
	}


    void VtkDisplay::setFrameBuffer(QVTKFrameBufferObjectItem *fb)
    {
        frameBuffer = fb;
        // invalidate initialization
        rw = nullptr;
        initialize();
    }
    

    void VtkDisplay::AddVtkPointSet(vtkPointSet *model, std::string name, bool persistent, DISPLAY_MODE activeIn, bool isPickable, bool resetCamera)
    {
        AddVtkPointSet(model, name, persistent, (unsigned int)activeIn, isPickable, resetCamera);
    }


    void VtkDisplay::AddVtkPointSet(vtkPointSet *model, std::string name, bool persistent, unsigned int activeIn, bool isPickable, bool resetCamera)
	{
		//Create a mapper and actor
        vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
		if (model->GetDataObjectType() == VTK_UNSTRUCTURED_GRID)
		{		
            // extract surface from the unstr grid - this is exactly the same that a vtkDataSetMapper would do anyway
            // except for the setPassThroughCell and PointIds, which we need for picking and there is no public API in vtkDataSetMapper to turn that on
            vtkSmartPointer<vtkDataSetSurfaceFilter> extractor = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
            extractor->SetPassThroughCellIds(true);
            extractor->SetPassThroughPointIds(true);
            extractor->UseStripsOff();
            extractor->SetInputDataObject(model);
            vtkSmartPointer<vtkPolyDataMapper>mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            mapper->SetInputConnection(extractor->GetOutputPort());
            actor->SetMapper(mapper);
		}
		else if (model->GetDataObjectType() == VTK_POLY_DATA)
		{
            vtkSmartPointer<vtkPolyDataMapper>mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
			mapper->SetInputDataObject(vtkPolyData::SafeDownCast(model));
			actor->SetMapper(mapper);
		}

        actor->SetOriginalDataSet(model);
        // set default colors
        float a, r, g, b;
        colorFormatTransferUINTtoFLOAT(_p_defaultColor, &a, &r, &g, &b);
        actor->SetBaseColor(vtkColor4d(r, g, b, a));
        AddActor(actor, name, persistent, activeIn, isPickable, resetCamera);
	}

    void VtkDisplay::AddGroupOfPoints(vtkDataSet *model, std::string name, bool persistent, double radius, 
        std::string maskArrayName, DISPLAY_MODE activeIn,
        bool isPickable, bool resetCamera, hbm::FEModelVTK *parentContainingLandmarkInfo)
    {
        if (parentContainingLandmarkInfo)
            model = parentContainingLandmarkInfo->getVTKMesh();
        if (rw->GetContextSupportsOpenGL32())
        {
            vtkSmartPointer<vtkGlyph3DMapper> mapper = highlights.AllPoints(model, radius);
            mapper->ScalarVisibilityOff();
            vtkSmartPointer<VtkDisplayActor> actor = vtkSmartPointer<VtkDisplayActor>::New();
            actor->SetMapper(mapper);
            if (maskArrayName != "")
            {
                mapper->MaskingOn();
                mapper->SetMaskArray(maskArrayName.data());
            }
            actor->SetFEModelWithLandmarkInfo(parentContainingLandmarkInfo);
            actor->SetIsGroupOfPoints(true);
            AddActor(actor, name, persistent, activeIn, isPickable, resetCamera);
        }
        else // legacy code - but picking on this will not work (user has been warned during the render window creation)!
        {
            vtkSmartPointer<vtkBitArray> mask = vtkBitArray::SafeDownCast(model->GetPointData()->GetArray(maskArrayName.data()));
            double currPoint[3];
            vtkSmartPointer<vtkPolyData> spheres = vtkSmartPointer<vtkPolyData>::New();
            spheres->Allocate();
            vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
            sphere->SetRadius(radius);
            sphere->SetPhiResolution(6);
            sphere->SetThetaResolution(6);
            vtkSmartPointer<vtkAppendPolyData> appender = vtkSmartPointer<vtkAppendPolyData>::New();
            appender->AddInputData(spheres);
            vtkIdType numPts = model->GetNumberOfPoints();
            for (vtkIdType i = 0; i < numPts; i++)
            {
                if (!mask || mask->GetValue(i) == IS_PRIMITIVE_SELECTED) // if it does not have the mask, or does and the primitive is not masked, add it
                {
                    model->GetPoint(i, currPoint);
                    sphere->SetCenter(currPoint);
                    sphere->Update();
                    vtkSmartPointer<vtkPolyData> temp = vtkSmartPointer<vtkPolyData>::New();
                    temp->DeepCopy(sphere->GetOutput());
                    appender->AddInputData(temp);
                }
            }
            appender->Update();

            AddVtkPointSet(appender->GetOutput(), name, persistent, activeIn, isPickable, resetCamera);
            actors[name]->SetFEModelWithLandmarkInfo(parentContainingLandmarkInfo);
            actors[name]->SetIsGroupOfPoints(true);
        }
    }


    void VtkDisplay::AddActor(vtkSmartPointer<VtkDisplayActor> actor, std::string name, bool persistent, unsigned int activeIn, bool isPickable, bool resetCamera)
    {
        actor->SetName(name);
        // register the mode in which the actor is active
        actor->SetDisplayMode(activeIn);
        actor->SetIsSelected(false); // set it as not selected
        actor->GetBaseColor()->SetAlpha(actor->GetProperty()->GetOpacity()); // store the opacity
        actor->GetProperty()->GetDiffuseColor(actor->GetBaseColor()->GetData()); // store the original color
        if (actor->GetOriginalDataSet() == NULL)
            actor->SetOriginalDataSet(vtkPointSet::SafeDownCast(actor->GetMapper()->GetInput()));
        actor->SetPickable(isPickable);
        actor->SetIsPersistent(persistent);
        actor->GetProperty()->SetEdgeVisibility(renderEdges);
        // register the actor
        actors[name] = actor;
        if (resetCamera) // if reset camera was already requested and this reset camera is false, do not overwrite the request to reset!
            this->resetCamera = resetCamera;
        if (rw && isPickable) // new item in the scene, re-render scene-picker, but only if it is not a non-pickable item
            rw->scenePickerUpToDate = false;
        // if normals are currently being rendered, create the normal actor for this actor
        if (renderNormals && (
            ((unsigned int)(DISPLAY_MODE::FULL_MESH) & actor->GetDisplayMode()) != 0
            || ((unsigned int)(DISPLAY_MODE::ENTITIES) & actor->GetDisplayMode()) != 0))
        {
            actor->GetMapper()->Update();
            renderActorsNormal(actor);
            AddActor(actor->GetNormalHighlights(), actor->GetNormalHighlights()->GetName(), persistent, DISPLAY_MODE::HIGHLIGHTERS, false, resetCamera);
        }
    }

    void VtkDisplay::AddAxesActor(vtkSmartPointer<vtkAxesActor> actor, std::string name, bool persistent, bool resetCamera)
    {
        // extract the individual actors from the axes actor and transforom them into VtkDisplayActor
        vtkSmartPointer<vtkPropCollection> axesActors = vtkSmartPointer<vtkPropCollection>::New();
        actor->GetActors(axesActors);
        axesActors->InitTraversal();
        
        for (int i = 0; i < 6; i++)
        {
            vtkActor *part = vtkActor::SafeDownCast(axesActors->GetNextProp());
            AddVtkPointSet(vtkPointSet::SafeDownCast(part->GetMapper()->GetInput()), name + axisPartsNameSuffix[i], persistent, DISPLAY_MODE::AXES, true, resetCamera);
            // copy the pointer to the property and set the color explicitly to overwrite the default colors
            vtkSmartPointer<VtkDisplayActor> theNewActor = actors[name + axisPartsNameSuffix[i]];
            theNewActor->SetProperty(part->GetProperty());
            SetObjectColor(name + axisPartsNameSuffix[i], part->GetProperty()->GetDiffuseColor()[0],
                part->GetProperty()->GetDiffuseColor()[1], part->GetProperty()->GetDiffuseColor()[2], part->GetProperty()->GetOpacity());
            theNewActor->SetParentAxesActor(actor);
            theNewActor->SetScale(part->GetScale());
            theNewActor->SetPosition(part->GetPosition());
            theNewActor->SetOrientation(part->GetOrientation());
            theNewActor->SetUserTransform(part->GetUserTransform());
        }
        // give each part a reference to all other parts
        for (int i = 0; i < 6; i++)
        {
            for (int j = i + 1; j < 6; j++)
            {
                actors[name + axisPartsNameSuffix[i]]->AddPart(actors[name + axisPartsNameSuffix[j]]);
                actors[name + axisPartsNameSuffix[j]]->AddPart(actors[name + axisPartsNameSuffix[i]]);
            }
        }

        this->resetCamera = resetCamera;
    }


    void VtkDisplay::AddVtkPointSet(vtkPointSet *model, vtkLookupTable *lut, std::string name, bool persistent, DISPLAY_MODE activeIn, bool isPickable, bool resetCamera)
	{
		AddVtkPointSet(model, name, (unsigned int)activeIn, isPickable, resetCamera);
		actors[name]->GetMapper()->SetScalarRange(0, lut->GetNumberOfTableValues() - 1);
		actors[name]->GetMapper()->SetLookupTable(lut);
		actors[name]->GetMapper()->SetScalarModeToUseCellData();
	}

	void VtkDisplay::SetObjectColor(std::string name, float r, float g, float b, float alpha)
	{
		if (actors.find(name) != actors.end()) // if there is such object
		{
			actors[name]->GetProperty()->SetDiffuseColor(r, g, b);
            actors[name]->GetBaseColor()->Set(r, g, b, alpha); // store the original color - for selection purposes
            actors[name]->GetProperty()->SetOpacity(alpha);
            actors[name]->GetBaseColor()->SetAlpha(alpha); // store the opacity
            actors[name]->GetProperty()->Modified();
            actors[name]->GetMapper()->Modified(); // seems to be required for vtkGlyph-based actors in order to correctly update the color
		}
	}

	void VtkDisplay::SetObjectRadius(std::string name, float radius)
	{
		if (actors.find(name) != actors.end()) // if there is such object
		{
			vtkSmartPointer<vtkAlgorithm> algorithm = actors[name]->GetMapper()->GetInputConnection(0, 0)->GetProducer();
			vtkSmartPointer<vtkSphereSource> srcReference = vtkSphereSource::SafeDownCast(algorithm);
			srcReference->SetRadius(radius);
		}
	}

    void VtkDisplay::SetActorPointHighlightsColor(std::string name, float r, float g, float b, float alpha)
    {
        auto actorsIter = actors.find(name);
        if (actorsIter != actors.end()) // if there is such object
        {
            if (actorsIter->second->GetPointHighlights())
            {
                vtkSmartPointer<VtkDisplayActor> actor = actorsIter->second->GetPointHighlights();
                actor->GetProperty()->SetDiffuseColor(r, g, b);
                actor->GetBaseColor()->Set(r, g, b, alpha); // store the original color - for selection purposes
                actor->GetProperty()->SetOpacity(alpha);
                actor->GetBaseColor()->SetAlpha(alpha); // store the opacity
            }
        }
    }

    double VtkDisplay::GetObjectOpacity(std::string name) {
        if (actors.find(name) != actors.end()) // if there is such object
        {
            return actors[name]->GetProperty()->GetOpacity();
        }
        else return -1;
    }

    void VtkDisplay::SetObjectOpacity(std::string name, float alpha)
    {
        if (actors.find(name) != actors.end()) // if there is such object
        {
            actors[name]->GetProperty()->SetOpacity(alpha);
            actors[name]->GetBaseColor()->SetAlpha(alpha); // store the opacity
            if (rw && alpha < 1 && (currentDisplayMode & actors[name]->GetDisplayMode()) != 0) // if this object is currently being rendered, turn on transparency handling
                rw->hasTransparentObjects = true;
        }
    }

    void VtkDisplay::SetDisplayModeOpacity(unsigned int mode, float value)
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & mode) != 0)
            {
                it->second->GetProperty()->SetOpacity(value);
                it->second->GetBaseColor()->SetAlpha(value); // store the opacity
            }
        }
        if (rw && value < 1 && (currentDisplayMode & mode) != 0) // if this display mode is currently active, turn on transparency handling
            rw->hasTransparentObjects = true;
    }

	void VtkDisplay::SetObjectWidth(std::string name, float width)
	{
		if (actors.find(name) != actors.end()) // if there is such object
		{
			actors[name]->GetProperty()->SetLineWidth(width);
		}
	}


    void VtkDisplay::SelectActor(vtkSmartPointer<VtkDisplayActor> actor, PICK_APPENDING_STYLE append, bool forceSelect)
    {
        if (append == DESELECT_ALL)
        {
            DeselectAllActors();
            return;
        }
        if (actor != 0)
        {
            bool wasSelected = actor->GetIsSelected();
            if (append == APPEND_ACTOR) // if we want to append only one actor, first deselect all other
                DeselectAllActors();
            // if we are selecting and the actor is not selected - if it is, it will be deselected
            if (forceSelect || ((append == APPEND_ACTOR || append == APPEND_ALL) && !wasSelected))
            {
                actor->SetIsSelected(true); // mark the selection
                selectedActorHighlighting(true, actor);
            }
            else // deselecting - restore the original color
            {
                actor->SetIsSelected(false); // mark the deselection
                selectedActorHighlighting(false, actor);
            }
            rw->Render();
        }

    }

    void VtkDisplay::SelectActor(int pickPosition[2], PICK_APPENDING_STYLE append)
    {
        vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop);
        if (pickedActor)
        {
            SelectActor(pickedActor, append);
            emit actorSelectDeselect();
        }
        else if (append != APPEND_ALL)
        {
            DeselectAllActors(); // if the user clicked outside and the mode is not append_all, deselect all actors
            emit actorSelectDeselect();
        }
    }

    std::vector < std::string > VtkDisplay::GetSelectedActorNames()
    {
        std::vector < std::string > actor_names;

        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if (it->second->GetIsSelected())
                actor_names.push_back(it->second->GetName());

        }

        return actor_names;
    }

	QString VtkDisplay::getSelectedActorNameAsQString(){
		//std::vector < std::string > actor_names;
		QString str;
		for (auto it = actors.begin(); it != actors.end(); it++)
		{
			if (it->second->GetIsSelected())
				str = QString::fromStdString(it->second->GetName());
		}
		return str;

	}

    void VtkDisplay::SelectAllActors()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & currentDisplayMode) != 0)// only process active actors
                SelectActor(it->second, APPEND_ALL, true);
        }
    }

    void VtkDisplay::DeselectAllActors()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & currentDisplayMode) != 0) // only process active actors
                SelectActor(it->second, DESELECT_ACTOR);
        }
    }


    void VtkDisplay::SelectCellPixel(int pickPosition[2], bool hitOrLeft, PICK_APPENDING_STYLE append)
    {
        vtkIdType pickedCellID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
        if (pickedCellID != -1) // is -1 in case of nothing being picked
        {
            // find the actor to which the cell belongs and turn on coloring by selection for it
            vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
            if (pickedActor->GetIsGroupOfPoints())
                return; // do not process point-only actors
            vtkPointSet *data = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered

            // if the dataset has "originalCellIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
            // in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
            vtkIdTypeArray *originalCellsMap = vtkIdTypeArray::SafeDownCast(data->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));

            if (originalCellsMap != NULL)
            {
                pickedCellID = originalCellsMap->GetValue(pickedCellID);
                // if it is a mapped one, use the original dataset to track the number of selected pixels - this way it will work for selecting multiple cells that have
                // the same parent cell, whereas if it was tracked on the rendered cells, selecting a new cell would deselect other cells that have the same parent...
                data = pickedActor->GetOriginalDataSet();
            }
            
            auto cellPixelHits = st->ObtainCellPixelHitArray(data, false);
            int value = cellPixelHits->GetValue(pickedCellID);
            if (hitOrLeft)
            {
                cellPixelHits->SetValue(pickedCellID, value + 1);
                if (value == 0) // if the original value is zero, the cell is now selected - select it
                    SelectCell(pickPosition, APPEND_ALL, true,  // with area picking, only append all kind of makes sense
                    append == APPEND_ALL ? true : false); // if we are appending, select the cell if it is hit, if we are deselecting, deselect it if it is hit
            }
            else if (value > 0) // if it is zero, do not decrease it
            {
                cellPixelHits->SetValue(pickedCellID, value - 1);
                if (value == 1) // if it is one, now it will be zero - need to de-select
                    SelectCell(pickPosition, APPEND_ALL, true, 
                    append == APPEND_ALL ? false : true);// if we are appending, deselect the cell if it is left, if we are deselecting, select it
            }
            data->GetCellData()->SetActiveScalars(SELECTED_PRIMITIVES); // updating the cellPixelHits will cause switch of active scalars. then if SelectCell is not called, it will stay as active
        }
    }

    void VtkDisplay::RemoveCellPixelHitArrays()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & currentDisplayMode) != 0)
            {
                it->second->GetOriginalDataSet()->GetCellData()->RemoveArray(HIT_PIXEL_COUNT); // if it is a "mapped" dataset, the pick array will be here instead of on the mappers input
                it->second->GetMapper()->GetInput()->GetCellData()->RemoveArray(HIT_PIXEL_COUNT);
            }
        }
    }


    void VtkDisplay::SelectCell(int pickPosition[2], PICK_APPENDING_STYLE append, bool forceValue, bool value)
    {
        vtkIdType pickedCellID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
        if (pickedCellID != -1) // is -1 in case of nothing being picked
        {
            // find the actor to which the cell belongs and turn on coloring by selection for it
            vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
            if (pickedActor->GetIsGroupOfPoints())
                return; // do not process point-only actors
            vtkPointSet *renderedData = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered

            // if the dataset has "originalCellIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
            // in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
            vtkIdTypeArray *originalCellsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));
            
            if (originalCellsMap != NULL)
                pickedCellID = originalCellsMap->GetValue(pickedCellID);

            vtkSmartPointer<vtkBitArray> sel_primitives;
            // if it does not have the selection array yet, we need to make sure we update the mapper after creating it
            if (!pickedActor->GetOriginalDataSet()->GetCellData()->HasArray(SELECTED_PRIMITIVES))
            {
                sel_primitives = st->ObtainSelectionArrayCells(pickedActor->GetOriginalDataSet(), false); // do the reset later if needed so that we have the original value
                // update the mapper - without the update, it somehow happens that the first select does not get registered in the SELECTED_PRIM array of the rendered mesh
                // not sure why and how, it should not be happening...but this fixes it until I figure out what to do
                pickedActor->GetOriginalDataSet()->Modified();
                pickedActor->GetMapper()->Update();
                originalCellsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetCellData()->GetArray(OriginalCellIdsArrayName.data())); // re-read the originalCellsMap
            }
            else // no update needed
                sel_primitives = st->ObtainSelectionArrayCells(pickedActor->GetOriginalDataSet(), false);  // do the reset later if needed

            // if the cell is selected, deselect it, if it isn't, select it
            int origValue = sel_primitives->GetValue(pickedCellID);
            // reset if needed
            bool reset = pickedActor->HasSelectedCells() && (append == DESELECT_ACTOR || append == DESELECT_ALL);
            if (reset)
                st->ObtainSelectionArrayCells(pickedActor->GetOriginalDataSet(), true);
            int newValue;
            if (forceValue)
                newValue = value ? IS_PRIMITIVE_SELECTED : IS_NOT_PRIMITIVE_SELECTED;
            else
                newValue = (origValue == IS_PRIMITIVE_SELECTED) ? IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED;

            sel_primitives->SetValue(pickedCellID, newValue);

            if (newValue && !origValue) // if it was not selected and now it is, increase selection counter
                pickedActor->CellsSelected(1);
            else if (!reset && (!newValue && origValue)) pickedActor->CellsDeselected(1); // if it was not reset, and the value chagned from selected to deselected, decrease the counter of selected cells

            // if we need to reset all other actors, do it
            if (append == APPEND_ACTOR || append == DESELECT_ALL)
            {
                for (auto it = actors.begin(); it != actors.end(); it++)
                {
                    if (it->second != pickedActor && it->second->HasSelectedCells())
                        deselectCells(it->second);
                }
            }

            // if the rendered dataset is mapped from some other orignial data, mark the selected cells also in the rendered dataset
            // NOTE that we could simply mark the original data as modified and call update, but this SHOULD be faster, as only the scalar data is updated,
            // instead of re-creating the whole geometry and other things
            if (originalCellsMap != NULL)
            {
                auto sel_primitivesRendered = st->ObtainSelectionArrayCells(renderedData, append == DESELECT_ACTOR || append == DESELECT_ALL);
                // each cell in the original dataset might have broken into several cells in the rendered one (e.g. tetrahedra -> 4 triangles)
                // we need to go through the entire map array and find each such cell
                vtkIdType numTup = originalCellsMap->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (originalCellsMap->GetValue(i) == pickedCellID) // pickedCellID here already contains the ID of the original parent cell
                        sel_primitivesRendered->SetValue(i, newValue);
                }
                sel_primitivesRendered->Modified();
            }
            
            highlightElementsByScalars(pickedActor, SELECTED_PRIMITIVES, false);
        }
    }

	void VtkDisplay::selectBySelectionTools(PICK_APPENDING_STYLE append, SELECTION_TARGET target)
	{
		st->SetInverseSelection(append == DESELECT_ACTOR);
		st->SetSelectionTargetType(target);

		bool selectCells = (target & SELECTION_TARGET::ELEMENTS) != 0 || (target & SELECTION_TARGET::FACES) != 0;
		bool selectPoints = (target & SELECTION_TARGET::NODES) != 0;
		if (append != APPEND_ALL && append != DESELECT_ACTOR) // deselect if needed
		{
			for (auto it = actors.begin(); it != actors.end(); it++)
			{
				if (((it->second->GetDisplayMode() & currentDisplayMode) != 0) && it->second->GetPickable() && it->second->GetVisibility())
				{
					if (selectCells && !it->second->GetIsGroupOfPoints())
						deselectCells(it->second);
					if (selectPoints)
						deselectPoints(it->second);
				}
			}

		}
		boost::container::vector<vtkSmartPointer<vtkBitArray>> processedPointScalarArrays;
		for (auto it = actors.begin(); it != actors.end(); it++)
		{
			vtkSmartPointer<VtkDisplayActor>  actor = it->second;
			if (((actor->GetDisplayMode() & currentDisplayMode) != 0) && actor->GetPickable() && actor->GetVisibility()) // if it is active and pickable, perform the selection on it
			{
				if (selectPoints) // if only points are to be selected, we can avoid doing the selection if the points array is shared with some other dataset that has already been processed
				{
					bool pointsAlreadyProcessed = false;
					vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(actor->GetOriginalDataSet(), false);
					for (auto it = processedPointScalarArrays.begin(); it != processedPointScalarArrays.end(); it++)
					{
						if (sel_prim == *it) // this array has already been processed
						{
							pointsAlreadyProcessed = true;
							break;
						}
					}
					if (pointsAlreadyProcessed)
						st->SetSelectionTargetType((0xFFFFFFFF ^ SELECTION_TARGET::NODES) & target); // turn off node selection
					else
						processedPointScalarArrays.push_back(sel_prim);
				}
				st->SetInputData(actor->GetOriginalDataSet());
				st->Update();
				// update the information about number of selected cells and points
				if (selectCells && !actor->GetIsGroupOfPoints())
				{
					vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayCells(actor->GetOriginalDataSet(), false);
					int counter = 0;
                    vtkIdType numTup = sel_prim->GetNumberOfTuples();
					for (vtkIdType i = 0; i < numTup; i++)
					{
						if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
							counter++;
					}
					actor->AllCellsDeselected();
					actor->CellsSelected(counter);
				}
				if (selectPoints)
				{
					vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(actor->GetOriginalDataSet(), false);
                    int counter = 0;
                    vtkIdType numTup = sel_prim->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
					{
						if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
							counter++;
					}
					actor->AllPointsDeselected();
					actor->PointsSelected(counter);
				}
				actor->GetOriginalDataSet()->Modified();
				actor->GetMapper()->Update();
				if (selectCells && !actor->GetIsGroupOfPoints())
					highlightElementsByScalars(actor, SELECTED_PRIMITIVES, false);
				if (selectPoints)
				{
					HighlightSelectedPoints(actor->GetName(), POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR, true);
					st->SetSelectionTargetType(target); // this might have been changed due to the shared-points handling above, so reset it
				}
			}
		}

		st->SetInverseSelection(false); // reset st to default
	}

    void VtkDisplay::SelectByBox(double boxOrigin[3], double boxAxes[3][3],
        PICK_APPENDING_STYLE append, SELECTION_TARGET target, bool createBoxVisualization)
    {
        st->Reset();
        st->UseSelectByOrientedBox(boxOrigin, boxAxes);
        
		selectBySelectionTools(append, target);

        if (append != APPEND_ALL && append != DESELECT_ACTOR)
            ClearSelectionBoxes();

		if (createBoxVisualization)
		{
			// add the highlighter for the box to the scene
			vtkSmartPointer<VtkDisplayActor> boxactor = this->highlights.SelectionBox(boxOrigin, boxAxes);
			AddActor(boxactor, boxactor->GetName(), false, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);

			Refresh();
		}
		else if ((target & SELECTION_TARGET::NODES) != 0)
			Refresh();
		else
			Render();
    }

	void VtkDisplay::SelectElementsByPlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
		double planeNormalX, double planeNormalY, double planeNormalZ, bool positiveDirection)
	{
		double planeNormal[3]{planeNormalX, planeNormalY, planeNormalZ};
		double planeRefPoint[3]{planeRefPointX, planeRefPointY, planeRefPointZ};
		if (!positiveDirection)
			vtkMath::MultiplyScalar(planeNormal, -1);
		st->Reset();
		st->UseSelectByPlane(planeRefPoint, planeNormal);
		selectBySelectionTools(PICK_APPENDING_STYLE::APPEND_ALL, SELECTION_TARGET::ELEMENTS);
		Render();
	}

	void VtkDisplay::SelectNodesByPlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
		double planeNormalX, double planeNormalY, double planeNormalZ, bool positiveDirection)
	{
		double planeNormal[3]{planeNormalX, planeNormalY, planeNormalZ};
		double planeRefPoint[3]{planeRefPointX, planeRefPointY, planeRefPointZ};
		if (!positiveDirection)
			vtkMath::MultiplyScalar(planeNormal, -1);
		st->Reset();
		st->UseSelectByPlane(planeRefPoint, planeNormal);
		selectBySelectionTools(PICK_APPENDING_STYLE::APPEND_ALL, SELECTION_TARGET::NODES);
		Refresh();
	}

	void VtkDisplay::VisualizePlane(double planeRefPointX, double planeRefPointY, double planeRefPointZ,
		double planeNormalX, double planeNormalY, double planeNormalZ, QString actorName)
	{
		double planeNormal[3]{planeNormalX, planeNormalY, planeNormalZ};
		double planeRefPoint[3]{planeRefPointX, planeRefPointY, planeRefPointZ};
		double boundingBox[6];
        RemoveActor(actorName, true);// remove the actor with this name if there is one so that it is not involved in computing the size
		if (computeVisiblesBoundingBox(boundingBox))
		{
            vtkMath::Normalize(planeNormal);
            double dir1[3]{1, 0, 0};
            // project X axis onto the plane to get a rectangle that is more or less aligned to some axis
            double distByNormal = vtkMath::Dot(dir1, planeNormal);
            if (distByNormal > 0.995 || distByNormal < -0.995) // the axis is (almost) perpendicular to the plane normal - choose a different one
            {
                dir1[0] = 0;
                dir1[1] = 1;
                distByNormal = vtkMath::Dot(dir1, planeNormal);
            }
            for (int i = 0; i < 3; i++)
                dir1[i] -= distByNormal * planeNormal[i];
            double dir2[3];
            // and now one that is perpendicular to both to have a rectangle
            vtkMath::Cross(dir1, planeNormal, dir2);
            // let's make a square with edge length equal to the longest edge of bounding box
            double maxLength = 0;
            for (int i = 0; i < 3; i++)
            {
                double length = boundingBox[i * 2 + 1] - boundingBox[i * 2];
                if (length > maxLength)
                    maxLength = length;
            }
			vtkSmartPointer<VtkDisplayActor> actor = highlights.SelectionRectangle(planeRefPoint, dir1, dir2, maxLength, maxLength);
			AddActor(actor, actorName.toStdString(), false, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);
			ActivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS);
			Refresh();
		}
	}

	bool VtkDisplay::computeVisiblesBoundingBox(double boundingBox[6])
	{
		bool ret = false;
		// first get initial bounding box equal to the first visible actor's box
		auto actor = actors.begin();
		for (; actor != actors.end(); actor++)
		{
			if (((actor->second->GetDisplayMode() & currentDisplayMode) != 0) && actor->second->GetVisibility())
			{
				actor->second->GetBounds(boundingBox);
				actor++;
				ret = true;
				break;
			}
		}
		// now expand it by checking other actors
		for (; actor != actors.end(); actor++)
		{
			if (((actor->second->GetDisplayMode() & currentDisplayMode) != 0) && actor->second->GetVisibility())
			{
				double *curBounds = actor->second->GetBounds();
				for (int i = 0; i < 3; i++)
				{
					if (boundingBox[i * 2] > curBounds[i * 2])
						boundingBox[i * 2] = curBounds[i * 2];
					if (boundingBox[i * 2 + 1] < curBounds[i * 2 + 1])
						boundingBox[i * 2 + 1] = curBounds[i * 2 + 1];
				}
			}
		}
		return ret;
	}

    void VtkDisplay::ClearSelectionBoxes()
    {
        auto boxMap = highlights.GetSelectionBoxes();
        std::stringstream name;
        for (auto it = boxMap->begin(); it != boxMap->end(); it++)
        {
            name.clear();
            name.str("");
            name << SELECTION_BOX_ACTOR << it->first;
            RemoveActor(name.str());
        }
        highlights.selectedBoxID = -1;
        boxMap->clear();
    }

    void VtkDisplay::BuildBoxesBySelCells()
    {
        vtkSmartPointer<vtkPoints> allSelectedPoints = vtkSmartPointer<vtkPoints>::New();
        allSelectedPoints->Initialize();
        vtkSmartPointer<vtkMergePoints> cleaner = vtkSmartPointer<vtkMergePoints>::New();
        cleaner->SetDivisions(10, 10, 10);
        cleaner->InitPointInsertion(allSelectedPoints, allSelectedPoints->GetBounds());
        vtkIdType id; // is ignored
        for (auto actor : actors)
        {
            if (((actor.second->GetDisplayMode() & currentDisplayMode) != 0) && actor.second->GetVisibility() && actor.second->HasSelectedCells())
            {
                vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayCells(actor.second->GetOriginalDataSet(), false);
                vtkIdType numTup = sel_prim->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    {
                        vtkCell *curCell = actor.second->GetOriginalDataSet()->GetCell(i);
                        vtkIdType numPts = curCell->GetNumberOfPoints();
                        for (vtkIdType j = 0; j < numPts; j++)
                            cleaner->InsertUniquePoint(actor.second->GetOriginalDataSet()->GetPoint(curCell->GetPointId(j)), id);
                    }
                }
            }
        }

        // cluster the cell-points
        boost::container::vector<vtkSmartPointer<vtkPoints>> clusters = vtkSelectionTools::CreateSpatialClusters(cleaner->GetPoints());
        double size[3];
        for (vtkSmartPointer<vtkPoints> cluster : clusters)
        {
            vtkOBBNodePtr node = std::make_shared<vtkOBBNodeWNeighbors>();
            vtkOBBTree::ComputeOBB(cluster, node->Corner, node->Axes[0], node->Axes[1], node->Axes[2], size);
            // add the highlighter for the box to the scene
            vtkSmartPointer<VtkDisplayActor> boxactor = this->highlights.SelectionBox(node);
            AddActor(boxactor, boxactor->GetName(), false, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);
        }
        Refresh();
    }

    void VtkDisplay::BuildBoxesBySelPoints()
    {
        vtkSmartPointer<vtkPoints> allSelectedPoints = vtkSmartPointer<vtkPoints>::New();
        allSelectedPoints->Initialize();
        boost::container::vector<vtkSmartPointer<vtkBitArray>> processedPointScalarArrays; // point arrays can be shared by multiple actors - don't process them twice
        for (auto actor : actors)
        {
            if (((actor.second->GetDisplayMode() & currentDisplayMode) != 0) && actor.second->GetVisibility() && actor.second->HasSelectedPoints()) 
            {
                bool pointsNotYetProcessed = true;
                vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(actor.second->GetOriginalDataSet(), false);
                for (auto it = processedPointScalarArrays.begin(); it != processedPointScalarArrays.end(); it++)
                {
                    if (sel_prim == *it) // this array has already been processed
                    {
                        pointsNotYetProcessed = false;
                        break;
                    }
                }
                if (pointsNotYetProcessed)
                {
                    processedPointScalarArrays.push_back(sel_prim);
                    vtkIdType numTup = sel_prim->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                    {
                        if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED) // insert coordinates of the selected point
                            allSelectedPoints->InsertNextPoint(actor.second->GetOriginalDataSet()->GetPoint(i));
                    }
                }
            }
        }

        // cluster the points
        boost::container::vector<vtkSmartPointer<vtkPoints>> clusters = vtkSelectionTools::CreateSpatialClusters(allSelectedPoints);
        double size[3];
        for (vtkSmartPointer<vtkPoints> cluster : clusters)
        {
            vtkOBBNodePtr node = std::make_shared<vtkOBBNodeWNeighbors>();
            vtkOBBTree::ComputeOBB(cluster, node->Corner, node->Axes[0], node->Axes[1], node->Axes[2], size);
            // add the highlighter for the box to the scene
            vtkSmartPointer<VtkDisplayActor> boxactor = this->highlights.SelectionBox(node);
            AddActor(boxactor, boxactor->GetName(), false, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);
        }
        Refresh();
    }

    void VtkDisplay::SetBoxesVisible(bool visualize)
    {
        auto boxMap = highlights.GetSelectionBoxes();
        std::stringstream name;
        for (auto it = boxMap->begin(); it != boxMap->end(); it++)
        {
            name.clear();
            name.str("");
            name << SELECTION_BOX_ACTOR << it->first;
            SetActorVisible(name.str(), visualize);
        }
        highlights.boxesVisible = visualize;
    }

    int VtkDisplay::SelectNextSelectionBox()
    {
        std::stringstream boxName;
        boxName << SELECTION_BOX_ACTOR << highlights.selectedBoxID;
        // change the color of the box that is being deselected back to primary selection color
        SetObjectColor(boxName.str(), highlights._p_colorOfSelection);
        SetObjectOpacity(boxName.str(), 0.5);
        highlights.SelectNextSelectionBox();

        // change the color of the selected box
        boxName.clear();
        boxName.str("");
        boxName << SELECTION_BOX_ACTOR << highlights.selectedBoxID;
        SetObjectColor(boxName.str(), highlights._p_secondHighlighterColor);
        SetObjectOpacity(boxName.str(), 0.5);
        return highlights.selectedBoxID;
    }

    int VtkDisplay::SelectPrevSelectionBox()
    {
        std::stringstream boxName;
        boxName << SELECTION_BOX_ACTOR << highlights.selectedBoxID;
        // change the color of the box that is being deselected back to primary selection color
        SetObjectColor(boxName.str(), highlights._p_colorOfSelection);
        SetObjectOpacity(boxName.str(), 0.5);
        highlights.SelectPrevSelectionBox();

        // change the color of the selected box
        boxName.clear();
        boxName.str("");
        boxName << SELECTION_BOX_ACTOR << highlights.selectedBoxID;
        SetObjectColor(boxName.str(), highlights._p_secondHighlighterColor);
        SetObjectOpacity(boxName.str(), 0.5);
        return highlights.selectedBoxID;
    }

    void VtkDisplay::SelectSelectionBox(std::string boxActorName)
    {
        try
        {
            int id = stoi(boxActorName.substr(SELECTION_BOX_ACTOR.length()));
            if (highlights.GetSelectionBoxes()->find(id) != highlights.GetSelectionBoxes()->end())
                highlights.selectedBoxID = id;
        }
        catch (std::exception){} //if exception happens do nothing
    }

    void VtkDisplay::RemoveSelectedBox()
    {
        std::stringstream boxName;
        boxName << SELECTION_BOX_ACTOR << highlights.selectedBoxID;
        RemoveActor(boxName.str());
        highlights.RemoveSelectedBox();
    }

    bool VtkDisplay::SelectBySelectedBox(bool deselect)
    {
        auto boxIt = highlights.GetSelectionBoxes()->find(highlights.selectedBoxID);
        if (boxIt != highlights.GetSelectionBoxes()->end() && boxIt->second)
        {
            if (activeSelectionTarget != SELECTION_TARGET::NONE)
            {
                SelectByBox(boxIt->second->Corner, boxIt->second->Axes, deselect ? DESELECT_ACTOR : APPEND_ALL, activeSelectionTarget, false);
                if ((activeSelectionTarget & SELECTION_TARGET::NODES) != 0 && !deselect) // if we are selecting nodes, new actors might have been added - we need to refresh
                    Refresh();
            }
            return true;
        }
        else return false;
    }

    void VtkDisplay::SelectNodesByElements(bool deselect)
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if (it->second->HasSelectedCells() && ((it->second->GetDisplayMode() & currentDisplayMode) != 0))
            {
                vtkSelectionTools::SelectPointsByCells(it->second->GetOriginalDataSet(), deselect);

                auto sel_primitives = st->ObtainSelectionArrayPoints(it->second->GetOriginalDataSet(), false);
                // count the number of selected nodes and update it in the actor
                int counter = 0;
                vtkIdType numTup = sel_primitives->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        counter++;
                }
                it->second->AllPointsDeselected();
                it->second->PointsSelected(counter);

                vtkPointSet *renderedData = vtkPointSet::SafeDownCast(it->second->GetMapper()->GetInput()); // get the dataset that was rendered
                // if the dataset has "originalPointsIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
                // in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
                vtkIdTypeArray *originalPointsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetPointData()->GetArray(OriginalPointIdsArrayName.data()));
                if (originalPointsMap)
                {
                    auto sel_primitivesRendered = st->ObtainSelectionArrayPoints(renderedData, false);
                    numTup = originalPointsMap->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                        sel_primitivesRendered->SetValue(i, sel_primitives->GetValue(originalPointsMap->GetValue(i)));
                    sel_primitivesRendered->Modified();
                }

                HighlightSelectedPoints(it->first, POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR, true);
            }
        }
        Refresh();
    }

    void VtkDisplay::SelectElementsByNodes()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if (it->second->HasSelectedPoints() && ((it->second->GetDisplayMode() & currentDisplayMode) != 0) && !it->second->GetIsGroupOfPoints())
            {
                vtkSelectionTools::SelectCellsByPoints(it->second->GetOriginalDataSet());

                vtkSmartPointer<vtkBitArray> sel_primitives = vtkSelectionTools::ObtainSelectionArrayCells(it->second->GetOriginalDataSet(), false);

                // count the number of selected cells and update it in the actor
                int counter = 0;
                vtkIdType numTup = sel_primitives->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        counter++;
                }
                it->second->AllCellsDeselected();
                it->second->CellsSelected(counter);

                vtkSmartPointer<vtkIdTypeArray> originalCellsMap = 
                    vtkIdTypeArray::SafeDownCast(it->second->GetMapper()->GetInput()->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));
                if (originalCellsMap != NULL) // update also the selection array of the rendered dataset
                {
                    it->second->GetOriginalDataSet()->GetCellData()->SetActiveScalars(SELECTED_PRIMITIVES);
                    vtkSmartPointer<vtkBitArray> sel_primitivesRendered = 
                        vtkSelectionTools::ObtainSelectionArrayCells(vtkPointSet::SafeDownCast(it->second->GetMapper()->GetInput()), false);
                    
                    numTup = originalCellsMap->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                        sel_primitivesRendered->SetValue(i, sel_primitives->GetValue(originalCellsMap->GetValue(i)));
                    sel_primitivesRendered->Modified();
                }
                highlightElementsByScalars(it->second, SELECTED_PRIMITIVES, false);
            }
        }
        rw->Render();
    }

	void VtkDisplay::SelectElementsByEntities()
	{
		std::vector <std::string> selectedActorNames = GetSelectedActorNames();
		if (selectedActorNames.size() > 0){
			for (auto it = selectedActorNames.begin(); it != selectedActorNames.end(); ++it)
            {
				// find the actor to which the cell belongs and turn on coloring by selection for it
				vtkSmartPointer<VtkDisplayActor> pickedActor = actors[*it];
                if (pickedActor->GetIsGroupOfPoints())
                    return; // do not process point-only actors
				vtkPointSet *renderedData = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered

				// if the dataset has "originalCellIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
				// in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
				vtkIdTypeArray *originalCellsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));

				vtkSmartPointer<vtkBitArray> sel_primitives;
				// if it does not have the selection array yet, we need to make sure we update the mapper after creating it
				if (!pickedActor->GetOriginalDataSet()->GetCellData()->HasArray(SELECTED_PRIMITIVES))
				{
					sel_primitives = st->ObtainSelectionArrayCells(pickedActor->GetOriginalDataSet(), false); // do the reset later if needed so that we have the original value
					// update the mapper - without the update, it somehow happens that the first select does not get registered in the SELECTED_PRIM array of the rendered mesh
					// not sure why and how, it should not be happening...but this fixes it until I figure out what to do
					pickedActor->GetOriginalDataSet()->Modified();
					pickedActor->GetMapper()->Update();
					originalCellsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetCellData()->GetArray(OriginalCellIdsArrayName.data())); // re-read the originalCellsMap
				}
				else // no update needed
					sel_primitives = st->ObtainSelectionArrayCells(pickedActor->GetOriginalDataSet(), false);  // do the reset later if needed

				int newValue = IS_PRIMITIVE_SELECTED;
				
				// if the rendered dataset is mapped from some other orignial data, mark the selected cells also in the rendered dataset
				// NOTE that we could simply mark the original data as modified and call update, but this SHOULD be faster, as only the scalar data is updated,
				// instead of re-creating the whole geometry and other things
				if (originalCellsMap != NULL)
				{
					auto sel_primitivesRendered = st->ObtainSelectionArrayCells(renderedData, DESELECT_ACTOR);
					// each cell in the original dataset might have broken into several cells in the rendered one (e.g. tetrahedra -> 4 triangles)
					// we need to go through the entire map array and find each such cell
                    vtkIdType numTup = originalCellsMap->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
						sel_primitivesRendered->SetValue(i, newValue);
					sel_primitivesRendered->Modified();
				}

				highlightElementsByScalars(pickedActor, SELECTED_PRIMITIVES, false);
			}
		}
		rw->Render();
	}

    void VtkDisplay::InvertMeshNodesSelection()
    {
        boost::container::vector<vtkSmartPointer<vtkBitArray>> processedPointScalarArrays;
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & currentDisplayMode) != 0 && it->second->GetPickable())
            {
                auto sel_primitives = st->ObtainSelectionArrayPoints(it->second->GetOriginalDataSet(), false);

                // check if this array wasn't already processed - points arrays are often shared among more actors
                bool pointsAlreadyProcessed = false;
                for (auto iter = processedPointScalarArrays.begin(); iter != processedPointScalarArrays.end(); iter++)
                {
                    if (sel_primitives == *iter) // this array has already been processed
                    {
                        pointsAlreadyProcessed = true;
                        processedPointScalarArrays.push_back(*iter);
                        break;
                    }
                }
                int count = 0;
                if (pointsAlreadyProcessed)
                {
                    // only count the number of selected points, but don't do the inversion
                    vtkIdType numTup = sel_primitives->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                    {
                        if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
                            count++;
                    }
                }
                else
                {
                    vtkIdType numTup = sel_primitives->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                    {
                        if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
                            sel_primitives->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                        else
                        {
                            sel_primitives->SetValue(i, IS_PRIMITIVE_SELECTED);
                            count++;
                        }
                    }
                }
                it->second->AllPointsDeselected();
                it->second->PointsSelected(count);
                sel_primitives->Modified();

                // if the dataset has "originalPointsIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
                // in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
                vtkSmartPointer<vtkIdTypeArray> originalPointsMap =
                    vtkIdTypeArray::SafeDownCast(it->second->GetMapper()->GetInput()->GetPointData()->GetArray(OriginalCellIdsArrayName.data()));
                if (originalPointsMap)
                {
                    auto sel_primitivesRendered = st->ObtainSelectionArrayPoints(vtkPointSet::SafeDownCast(it->second->GetMapper()->GetInput()), false);
                    vtkIdType numTup = originalPointsMap->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                        sel_primitivesRendered->SetValue(i, sel_primitives->GetValue(originalPointsMap->GetValue(i)) == IS_PRIMITIVE_SELECTED ?
                            IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED);
                    sel_primitivesRendered->Modified();
                }

                HighlightSelectedPoints(it->first, POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR, true);
            }
        }
        Refresh();
    }

    void VtkDisplay::InvertCellsSelection()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if ((it->second->GetDisplayMode() & currentDisplayMode) != 0 && it->second->GetPickable())
            {
                vtkSmartPointer<vtkBitArray> sel_primitives = vtkSelectionTools::ObtainSelectionArrayCells(it->second->GetOriginalDataSet(), false);

                int count = 0;
                vtkIdType numTup = sel_primitives->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                {
                    if (sel_primitives->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        sel_primitives->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                    else
                    {
                        sel_primitives->SetValue(i, IS_PRIMITIVE_SELECTED);
                        count++;
                    }
                }
                it->second->AllCellsDeselected();
                it->second->CellsSelected(count);
                sel_primitives->Modified();

                vtkSmartPointer<vtkIdTypeArray> originalCellsMap =
                    vtkIdTypeArray::SafeDownCast(it->second->GetMapper()->GetInput()->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));
                if (originalCellsMap != NULL) // update also the selection array of the rendered dataset
                {
                    it->second->GetOriginalDataSet()->GetCellData()->SetActiveScalars(SELECTED_PRIMITIVES);
                    vtkSmartPointer<vtkBitArray> sel_primitivesRendered =
                        vtkSelectionTools::ObtainSelectionArrayCells(vtkPointSet::SafeDownCast(it->second->GetMapper()->GetInput()), false);

                    numTup = originalCellsMap->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < numTup; i++)
                        sel_primitivesRendered->SetValue(i, sel_primitives->GetValue(originalCellsMap->GetValue(i)) == IS_PRIMITIVE_SELECTED ?
                            IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED);
                    sel_primitivesRendered->Modified();
                }
                highlightElementsByScalars(it->second, SELECTED_PRIMITIVES, false);
            }
        }
        rw->Render();
    }

    void VtkDisplay::DeselectAllCells()
    {
        for (auto it = actors.begin(); it != actors.end(); it++)
            deselectCells(it->second);
    }


    void VtkDisplay::deselectCells(vtkSmartPointer<VtkDisplayActor> actor)
    {
        vtkPointSet *data = vtkPointSet::SafeDownCast(actor->GetMapper()->GetInput());

        vtkIdTypeArray *originalCellsMap = vtkIdTypeArray::SafeDownCast(data->GetCellData()->GetArray(OriginalCellIdsArrayName.data()));
        if (originalCellsMap != NULL) // if it is a mapped dataset, reset the original mesh as well
            st->ObtainSelectionArrayCells(actor->GetOriginalDataSet(), true);

        st->ObtainSelectionArrayCells(data, true)->Modified(); // call modified so that mapper knows he has to re-read the scalars during rendering
        actor->AllCellsDeselected();
    }

    bool VtkDisplay::GetPickedPointOnMesh(int pickPosition[2], double returnCoordinates[3])
    {
        vtkIdType pickedCellID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
        if (pickedCellID != -1) // is -1 in case of nothing being picked
        {
            // find the actor to which the cell belongs and turn on coloring by selection for it
            vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
            vtkPointSet *renderedData = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered

            // find the ray intersection with the cell that was hit, then find which of those cells points is closest
            double ray1World[3]; double ray2World[3];
            if (vtkSelectionTools::ComputePointInScreenRay(pickPosition[0], pickPosition[1], rw->GetDefaultRenderer()->GetActiveCamera()->GetParallelProjection(),
                rw->GetDefaultRenderer(), ray1World, ray2World))
            {
                // take into account possible transformation applied on the actor - apply inverse transformation on the ray to transform it to the original position of the actor
                vtkSmartPointer<vtkTransform> t = vtkTransform::New();
                t->SetMatrix(pickedActor->GetMatrix());
                t->Inverse();
                double *transPoint = t->TransformDoublePoint(ray1World);
                ray1World[0] = transPoint[0]; ray1World[1] = transPoint[1]; ray1World[2] = transPoint[2];
                transPoint = t->TransformDoublePoint(ray2World);
                ray2World[0] = transPoint[0]; ray2World[1] = transPoint[1]; ray2World[2] = transPoint[2];

                return vtkSelectionTools::ComputeCellRayIntersection(renderedData, pickedCellID, ray1World, ray2World, returnCoordinates);
            }
        }
        return false;
    }

    void VtkDisplay::SelectMeshNode(int pickPosition[2], PICK_APPENDING_STYLE append)
    {
        double intersection[3];
        vtkIdType pickedCellID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
        if (GetPickedPointOnMesh(pickPosition, intersection))
        {
            vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
            vtkPointSet *renderedData = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered
            vtkIdType pickedPointID = vtkSelectionTools::FindClosestCellPointToPoint(renderedData, pickedCellID, intersection);
            if (pickedPointID == -1) return; // in case of some float precision error causes no hit, should not happen often

            // if the dataset has "originalPointsIds" array, it means it was mapped through a vtkDataSetSurfaceFilter (which is done for vtkUnstructuredGrids
            // in order to map 3D elements to 2D surfaces) and that means the IDs of the rendered mesh are not the same as the IDs in the orignial mesh -> need to be mapped
            vtkIdTypeArray *originalPointsMap = vtkIdTypeArray::SafeDownCast(renderedData->GetPointData()->GetArray(OriginalPointIdsArrayName.data()));
            vtkIdType origPointID = pickedPointID;
            if (originalPointsMap != NULL)
                origPointID = originalPointsMap->GetValue(pickedPointID);

            int value = selectPointEvaluate(pickedActor, origPointID, append);

            auto sel_primitivesRendered = vtkSelectionTools::ObtainSelectionArrayPoints(renderedData, false); // if it was supposed to be reset, it already was in deselectPoints
            sel_primitivesRendered->SetValue(pickedPointID, value);
            sel_primitivesRendered->Modified();

            HighlightSelectedPoints(pickedActor->GetName(), this->POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR);
        }
    }

    std::vector<std::string> VtkDisplay::GetActorsWithSelectedNodes()
    {
        std::vector<std::string> ret;
        for (auto actor : actors)
        {
            if (actor.second->HasSelectedPoints())
                ret.push_back(actor.first);
        }
        return ret;
    }


    int VtkDisplay::SelectGlyphedPoint(int pickPosition[2], PICK_APPENDING_STYLE append, std::string pickOnlyThisName)
    {
        vtkIdType pickedPointID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
        if (pickedPointID != -1) // is -1 in case of nothing being picked
        {
            vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
            if (pickOnlyThisName == "" || pickedActor->GetName() == pickOnlyThisName)
            {
                vtkSmartPointer<vtkGlyph3DMapper> mapper = vtkGlyph3DMapper::SafeDownCast(pickedActor->GetMapper());
                // if this dataset is not glyphed, do not continue
                if (mapper)
                {
                    vtkPointSet *renderedData = vtkPointSet::SafeDownCast(mapper->GetInput()); // get the dataset that was rendered
                    selectPointEvaluate(pickedActor, pickedPointID, append);
                    if (pickedActor->GetFEModelWithLandmarkInfo()) // if this is not NULL, this dataset should be treated as landmarks defined by groups of points
                    {
                        auto allPointsLandmarksNames = pickedActor->GetFEModelWithLandmarkInfo()->getLandmarkAssociatedWithPoint(pickedPointID);
                        vtkSmartPointer<vtkBitArray> sel_points = vtkSelectionTools::ObtainSelectionArrayPoints(pickedActor->GetFEModelWithLandmarkInfo()->getVTKMesh(), false);

                        vtkSmartPointer<vtkBitArray> highlightedPoints = vtkSelectionTools::ObtainArrayHighlightedGroupOfPoints(
                            pickedActor->GetFEModelWithLandmarkInfo()->getVTKMesh(), (append == DESELECT_ACTOR || append == DESELECT_ALL) ? true : false);

                        int value = sel_points->GetValue(pickedPointID);
                        bool wasPointDeselected = !value;
                        for (auto it = allPointsLandmarksNames.begin(); it < allPointsLandmarksNames.end(); it++) // for all landmarks pickedPoint belongs to
                        {
                            VId *landmarkPoints = pickedActor->GetFEModelWithLandmarkInfo()->getLandmarkPoints(*it);
                            if (wasPointDeselected) // if we are supposed to deselect, first we have to find out if other points in the landmark are not selected. 
                                // if some are, there is no need to turn off highlighting of the whole landmark
                            {
                                for (int i = 0; i < landmarkPoints->size(); i++) // for each of the points
                                {
                                    if ((landmarkPoints->at(i) != pickedPointID) && (sel_points->GetValue(landmarkPoints->at(i)) == IS_PRIMITIVE_SELECTED))
                                    {
                                        value = IS_PRIMITIVE_SELECTED;
                                        break;
                                    }
                                }
                            }
                            for (int i = 0; i < landmarkPoints->size(); i++) // for each of the points
                                highlightedPoints->SetValue(landmarkPoints->at(i), value); // turn highglights on or off based on whether the landmark is selected or not
                        }
                        HighlightPoints(pickedActor->GetName(), true, POINTHIGHLIGHT_RADIUS, false, SELECTED_POINT_GROUPS, true); // do not use the small_factor - this radius is what landmarks are using
                        SetActorPointHighlightsColor(pickedActor->GetName(), highlights._p_secondHighlighterColor); // ..and assign a new color to it
                    }
                    HighlightSelectedPoints(pickedActor->GetName(), POINTHIGHLIGHT_RADIUS * 1.1); // highlight the points by rendering slightly bigger spheres around them
                    rw->Render();
                }
            }
            else return -1;
        }
        return pickedPointID;
    }

    void VtkDisplay::BlankSelectedElements()
    {
        blankLevel++;
        st->Reset(); // reset to have selection tool set to NONE - do not perform any selection, only blanking
        st->SetBlankSelected(true, true);
        st->SetSelectionTargetType(SELECTION_TARGET::ELEMENTS);
        boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> setsToReplace;
        for (auto actor : actors)
        {
            if ((actor.second->GetDisplayMode() & currentDisplayMode) != 0 && actor.second->GetVisibility() && actor.second->HasSelectedCells())
            {
                vtkSmartPointer<vtkPointSet> origDataSet = actor.second->GetOriginalDataSet();
                st->SetInputData(origDataSet);
                st->Update();

                vtkSmartPointer<vtkUnstructuredGrid> blankedDataSet = vtkSmartPointer<vtkUnstructuredGrid>::New();
                vtkSmartPointer<vtkUnstructuredGrid> output = st->GetUnstructuredGridOutput();
                blankedDataSet->DeepCopy(output);
                setsToReplace[actor.second->GetName()] = blankedDataSet;
            }
        }
        for (auto dataset : setsToReplace)
        {
            vtkSmartPointer<VtkDisplayActor> parent = actors[dataset.first];
            vtkSmartPointer<vtkProperty> blankedProperty = vtkSmartPointer<vtkProperty>::New();
            blankedProperty->DeepCopy(parent->GetProperty());
            RemoveActor(dataset.first, false);
            AddVtkPointSet(dataset.second, dataset.first, parent->GetIsPersistent(), parent->GetDisplayMode(), parent->GetPickable(), false); // replace the parent actor
            vtkSmartPointer<VtkDisplayActor> blankedActor = actors[dataset.first];
            blankedActor->SetProperty(blankedProperty); // copy the visual settings from the parent
            blankedActor->SetBlankParent(parent, blankLevel);
            // create the various highlighters if they were present for the parent as well
            if (parent->GetPointHighlights())
                HighlightPoints(blankedActor->GetName(), blankedActor->GetIsPersistent(), false);
            if (parent->GetSelectedPointHighlights())
                HighlightSelectedPoints(blankedActor->GetName(), POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR, true);
            // normals will be computed during add actor, no need to do it explicitly
        }
        st->Reset();
        emit blankLevelChanged();
        Refresh();
    }

    void VtkDisplay::Unblank(bool unblankAll)
    {
        boost::container::list<vtkSmartPointer<VtkDisplayActor>> actorsReplace;
        for (auto actor : actors)
        {
            vtkSmartPointer<VtkDisplayActor> parent = actor.second->GetBlankParent();
            if (parent != NULL)
            {
                if (unblankAll)
                {
                    while (parent->GetBlankParent() != NULL)
                        parent = parent->GetBlankParent();
                    actorsReplace.push_back(parent);
                }
                else if (actor.second->GetBlankLevel() == blankLevel) // if the actor is on the current blank level, remove it, otherwise keep it
                    actorsReplace.push_back(parent);
            }
        }
        for (auto parent: actorsReplace)
        {
            RemoveActor(parent->GetName(), true); // remove the original one with all the highlights as well
            AddActor(parent, parent->GetName(), parent->GetIsPersistent(), parent->GetDisplayMode(), parent->GetPickable(), false);
            // restore dependent actors if there are any
            if (parent->GetPointHighlights())
                AddActor(parent->GetPointHighlights(), parent->GetPointHighlights()->GetName(), 
                    parent->GetPointHighlights()->GetIsPersistent(), parent->GetPointHighlights()->GetDisplayMode(), parent->GetPointHighlights()->GetPickable(), false);
            if (parent->GetSelectedPointHighlights())
                AddActor(parent->GetSelectedPointHighlights(), parent->GetSelectedPointHighlights()->GetName(),
                    parent->GetSelectedPointHighlights()->GetIsPersistent(), parent->GetSelectedPointHighlights()->GetDisplayMode(), parent->GetSelectedPointHighlights()->GetPickable(), false);

            parent->GetMapper()->Update();
            if (activeSelectionTarget == SELECTION_TARGET::ELEMENTS || activeSelectionTarget == SELECTION_TARGET::FACES) // reactivate highlighting of selected cells if it is on
                highlightElementsByScalars(parent, SELECTED_PRIMITIVES, false);
            // normals will be re-computed during the addActor anyway, no need to restore those
        }
        if (unblankAll)
            blankLevel = 0;
        else
            blankLevel--;
        emit blankLevelChanged();
        Refresh();
    }

    int VtkDisplay::selectPointEvaluate(vtkSmartPointer<VtkDisplayActor> actor, int pointID, PICK_APPENDING_STYLE append, bool forceValue, int value)
    {
        vtkSmartPointer<vtkBitArray> sel_primitives;
        // if it does not have the selection array yet, we need to make sure we update the mapper after creating it
        if (!actor->GetOriginalDataSet()->GetPointData()->HasArray(SELECTED_PRIMITIVES))
        {
            sel_primitives = st->ObtainSelectionArrayPoints(actor->GetOriginalDataSet(), false); // do the reset later if needed so that we have the original value
            // update the mapper - without the update, it somehow happens that the first select does not get registered in the SELECTED_PRIM array of the rendered mesh
            // not sure why and how, it should not be happening...but this fixes it until I figure out what to do
            actor->GetOriginalDataSet()->Modified();
            actor->GetMapper()->Update();
        }
        else // no update needed
            sel_primitives = st->ObtainSelectionArrayPoints(actor->GetOriginalDataSet(), false);  // do the reset later if needed so that we have the original value

        int prevValue = sel_primitives->GetValue(pointID);
        if (!forceValue) // if value is not forced, determine it by toggling the previous value
            value = prevValue == IS_PRIMITIVE_SELECTED ? IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED;

        bool reset = actor->HasSelectedPoints() && (append == DESELECT_ACTOR || append == DESELECT_ALL);
        if (reset)
            deselectPoints(actor);

        if (prevValue == IS_NOT_PRIMITIVE_SELECTED && value == IS_PRIMITIVE_SELECTED)
            actor->PointsSelected(1);
        else if (!reset && (prevValue != value)) actor->PointsDeselected(1);

        // if we need to reset all other actors, do it
        if (append == APPEND_ACTOR || append == DESELECT_ALL)
        {
            for (auto it = actors.begin(); it != actors.end(); it++)
            {
                if (it->second != actor && it->second->HasSelectedPoints())
                    deselectPoints(it->second);
            }
        }
        sel_primitives->SetValue(pointID, value); // mark the selected point in the selection array
        return value;
    }

    void VtkDisplay::deselectPoints(vtkSmartPointer<VtkDisplayActor> actor)
    {
        vtkPointSet *data = vtkPointSet::SafeDownCast(actor->GetMapper()->GetInput());

        // since multiple actors parent meshes can share the same poin array, use the actors selection array
        // to get the points that are only on that particular actor and deselect only those
        vtkSmartPointer<vtkBitArray> sel_primRendered = st->ObtainSelectionArrayPoints(data, false);
        vtkSmartPointer<vtkBitArray> sel_prim = st->ObtainSelectionArrayPoints(actor->GetOriginalDataSet(), false);
        vtkIdTypeArray *originalPointsMap = vtkIdTypeArray::SafeDownCast(data->GetPointData()->GetArray(OriginalPointIdsArrayName.data()));

        vtkIdType numTup = sel_primRendered->GetNumberOfTuples();
        for (vtkIdType i = 0; i < numTup; i++)
        {
            if (sel_primRendered->GetValue(i) == IS_PRIMITIVE_SELECTED)
            {
                sel_primRendered->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                if (originalPointsMap != NULL)
                    sel_prim->SetValue(originalPointsMap->GetValue(i), IS_NOT_PRIMITIVE_SELECTED);
                else
                    sel_prim->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
            }
        }

        sel_primRendered->Modified();
        sel_prim->Modified();
        actor->AllPointsDeselected();
    }

	void VtkDisplay::deselectAllPoints()
	{		
		for (auto it = actors.begin(); it != actors.end(); it++)
		{
			vtkPointSet *data = vtkPointSet::SafeDownCast(it->second->GetMapper()->GetInput());

			st->ObtainSelectionArrayPoints(it->second->GetOriginalDataSet(), true);
			st->ObtainSelectionArrayPoints(data, true)->Modified(); // call modified so that mapper knows he has to re-read the scalars during rendering

			it->second->AllPointsDeselected();
		}
	}

    void VtkDisplay::selectedActorHighlighting(bool on, vtkSmartPointer<VtkDisplayActor> actor)
    {
        auto actorsIterator = actors.begin();
        bool singleActor = false;
        if (actor != 0)
        {
            actorsIterator = actors.find(actor->GetName()); // if the name is specified, start from it
            singleActor = true; // mark that the user wants to apply the changes to only one actor
        }
        float a, r, g, b; // prepare the selection color
        colorFormatTransferUINTtoFLOAT(highlights._p_colorOfSelection, &a, &r, &g, &b);
        for (; actorsIterator != actors.end(); actorsIterator++) // for each actor...if one was specified and was wrong, list will point to the end of actors -> loop will immediately terminate
        {
            if ((actorsIterator->second->GetDisplayMode() & currentDisplayMode) != 0) // if the actor is active
            {
                actor = actorsIterator->second;
                if (on && actor->GetIsSelected())
                {
                    actor->GetMapper()->SetScalarVisibility(0); // turn of scalar visualization
                    // change the color. in case we want something more sophisticated, we will need a copy of the whole property and store it
                    actor->GetProperty()->GetDiffuseColor(actor->GetBaseColor()->GetData()); // store the current color
                    actor->GetProperty()->SetDiffuseColor(r, g, b); // write the selection color
                    // do the same for all parts of the actor if it is made of more parts
                    for (auto it = actor->GetOtherParts()->begin(); it != actor->GetOtherParts()->end(); it++)
                    {
                        it->Get()->GetMapper()->SetScalarVisibility(0); // turn of scalar visualization
                        // change the color. in case we want something more sophisticated, we will need a copy of the whole property and store it
                        it->Get()->GetProperty()->GetDiffuseColor(it->Get()->GetBaseColor()->GetData()); // store the current color
                        it->Get()->GetProperty()->SetDiffuseColor(r, g, b); // write the selection color
                    }
                }
                else
                {
                    actor->GetProperty()->SetDiffuseColor(actor->GetBaseColor()->GetData());
                    actor->GetProperty()->SetOpacity(actor->GetBaseColor()->GetAlpha());
                    // do the same for all parts of the actor if it is made of more parts
                    for (auto it = actor->GetOtherParts()->begin(); it != actor->GetOtherParts()->end(); it++)
                    {
                        it->Get()->GetProperty()->SetDiffuseColor(it->Get()->GetBaseColor()->GetData());
                        it->Get()->GetProperty()->SetOpacity(it->Get()->GetBaseColor()->GetAlpha());
                    }
                }
            }
            if (singleActor)
                break;
        }
    }
    
    void VtkDisplay::HighlightElementsByScalars(std::string scalarArrayName, bool usePointData, 
        std::string actorName, vtkSmartPointer<vtkScalarsToColors> lut, bool updateBlanked)
	{
        auto actorsIterator = actors.begin();
        bool singleActor = false;
        if (actorName != "")
        {
            actorsIterator = actors.find(actorName); // if the name is specified, start from it
            singleActor = true; // mark that the user wants to apply the changes to only one actor
        }
        for (; actorsIterator != actors.end(); actorsIterator++) // for each actor...if name was specified and was wrong, list will point to the end of actors -> loop will immediately terminate
		{
            if ((actorsIterator->second->GetDisplayMode() & currentDisplayMode) != 0) // if the actor is active
            {
                if (updateBlanked && actorsIterator->second->GetBlankParent() != NULL)
                {
                    updateCellScalarsFromParent(actorsIterator->second, scalarArrayName);
                    actorsIterator->second->GetMapper()->Update();
                }
                highlightElementsByScalars(actorsIterator->second, scalarArrayName, usePointData, lut);         

            }
            
            if (singleActor) // if some actor name was explicitly specified, process only it, i.e. return now
                return;
		}
	}


    void VtkDisplay::highlightElementsByScalars(vtkSmartPointer<VtkDisplayActor> actor, std::string scalarArrayName, bool usePointData, vtkSmartPointer<vtkScalarsToColors> lut)
    {
        if (actor != NULL)
        {
            vtkMapper *actorsMapper = actor->GetMapper();
            if (usePointData)
            {
                if (!actorsMapper->GetInput()->GetPointData()->HasArray(scalarArrayName.data()))
                    return;
                actorsMapper->SetScalarModeToUsePointData();
                actorsMapper->SetScalarVisibility(1);
                actorsMapper->Update();
                actorsMapper->GetInput()->GetPointData()->SetActiveScalars(scalarArrayName.data());
            }
            else
            {
                if (!actorsMapper->GetInput()->GetCellData()->HasArray(scalarArrayName.data()) || actor->GetIsGroupOfPoints())
                    return;
                actorsMapper->SetScalarModeToUseCellData();
                actorsMapper->SetScalarVisibility(1);
                actorsMapper->Update(); // mapper update must be called before SetActiveScalars...apparently
                actorsMapper->GetInput()->GetCellData()->SetActiveScalars(scalarArrayName.data());
                actorsMapper->GetInput()->GetCellData()->Update();
            }
            if (lut == NULL) // if lut has not been specified create a new default one
            {
                // ...set a lut with not-selected (0) color as the current color of the object and selected (1) as the color of selection
                lut = highlights.CreateBinaryColorLUT(
                    actor->GetProperty()->GetDiffuseColor()[0],
                    actor->GetProperty()->GetDiffuseColor()[1],
                    actor->GetProperty()->GetDiffuseColor()[2],
                    1, highlights._p_colorOfSelection); // set alpha of the colors as one and use the current alpha as the opacity of the whole model
                lut->SetAlpha(actor->GetProperty()->GetOpacity());
            }
            actor->GetProperty()->SetOpacity(lut->GetAlpha());
            actorsMapper->SetLookupTable(lut); // use the specified one
            if (lut->GetAlpha() < 1)
                rw->hasTransparentObjects = true;
        }
    }

    void VtkDisplay::updateCellScalarsFromParent(vtkSmartPointer<VtkDisplayActor> actor, std::string scalarArrayName)
    {
        vtkSmartPointer<VtkDisplayActor> parent = actor->GetBlankParent();
        vtkSmartPointer<vtkIdTypeArray> origCellsRendered = vtkIdTypeArray::SafeDownCast(
            actor->GetMapper()->GetInput()->GetCellData()->GetArray(OriginalCellIdsArrayName.c_str()));
        vtkSmartPointer<vtkIdTypeArray> origCells = vtkIdTypeArray::SafeDownCast(actor->GetOriginalDataSet()->GetCellData()->GetArray(OriginalCellIdsArrayName.c_str()));
        if (parent != NULL && origCells != NULL && origCellsRendered != NULL)
        {
            updateCellScalarsFromParent(parent, scalarArrayName); // recursively update from the parent's parent
            vtkSmartPointer<vtkDataArray> parentScalars = parent->GetOriginalDataSet()->GetCellData()->GetArray(scalarArrayName.c_str());
            if (parentScalars != NULL)
            {
                vtkSmartPointer<vtkDataArray> scalars = actor->GetOriginalDataSet()->GetCellData()->GetArray(scalarArrayName.c_str());
                vtkSmartPointer<vtkDataArray> scalarsRendered = actor->GetMapper()->GetInput()->GetCellData()->GetArray(scalarArrayName.c_str());
                if (scalars == NULL) // if the array does not exist, create it as a copy of the parent array
                {
                    scalars = vtkSmartPointer<vtkDataArray>::NewInstance(parentScalars);
                    scalars->SetNumberOfTuples(origCells->GetNumberOfTuples());
                    scalars->SetName(scalarArrayName.c_str());
                    actor->GetOriginalDataSet()->GetCellData()->AddArray(scalars);
                }
                if (scalarsRendered == NULL)
                {
                    scalarsRendered = vtkSmartPointer<vtkDataArray>::NewInstance(parentScalars);
                    scalarsRendered->SetNumberOfTuples(origCellsRendered->GetNumberOfTuples());
                    scalarsRendered->SetName(scalarArrayName.c_str());
                    actor->GetMapper()->GetInput()->GetCellData()->AddArray(scalarsRendered);
                }
                vtkIdType numTup = origCells->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                    scalars->SetTuple(i, origCells->GetValue(i), parentScalars);
                numTup = origCellsRendered->GetNumberOfTuples();
                for (vtkIdType i = 0; i < numTup; i++)
                    scalarsRendered->SetTuple(i, origCellsRendered->GetValue(i), scalars);
                scalars->Modified();
                scalarsRendered->Modified();
            }
        }
    }

    void VtkDisplay::HighlightElementsByScalarsOff(std::string name)
    {
        auto actorsIterator = actors.begin();
        bool singleActor = false;
        if (name != "")
        {
            actorsIterator = actors.find(name); // if the name is specified, start from it
            singleActor = true; // mark that the user wants to apply the changes to only one actor
        }
        for (; actorsIterator != actors.end(); actorsIterator++) // for each actor...if name was specified and was wrong, list will point to the end of actors -> loop will immediately terminate
        {
            if ((actorsIterator->second->GetDisplayMode() & currentDisplayMode) != 0) // if the actor is active
            {
                actorsIterator->second->GetMapper()->SetScalarVisibility(0);
                actorsIterator->second->GetMapper()->SetScalarModeToDefault();
                actorsIterator->second->GetProperty()->SetOpacity(actorsIterator->second->GetBaseColor()->GetAlpha()); // restore opacity
            }

            if (singleActor) // if some actor name was explicitly specified, process only it, i.e. return now
                return;
        }
    }

    void VtkDisplay::RemoveNonpersistentActors()
    {
        std::vector<std::string> todelete;
        for (auto it = actors.begin(); it != actors.end(); it++)
        {
            if (!(it->second->GetIsPersistent()))
            {
                todelete.push_back(it->first);
                //std::string name = it->first;
                //it++; // go to the next
                //RemoveActor(name);
                //it--; // get back one so that the it++ in the loop control gets us to the next
            }
        }
        if (todelete.size() > 0) {
            for (auto const& cur : todelete)
                RemoveActor(cur);
        }
    }

    std::string VtkDisplay::HighlightPoints(std::string name, bool persistent, double radius, bool refreshScreen, std::string maskArrayName, bool dummySourceData)
    {
        if (actors.find(name) != actors.end()) // if there is such object
        {
            vtkSmartPointer<VtkDisplayActor> actor = actors[name];
            std::stringstream nameOfSpheres;
            nameOfSpheres << name << "_pointHighlights_" << maskArrayName;
            if (rw->GetContextSupportsOpenGL32())
            {
                if (actor->GetPointHighlights() == NULL) // if it does not have an actor for point highlights yet, create it
                {
                    vtkSmartPointer<vtkPointSet> data = vtkPointSet::SafeDownCast(actor->GetMapper()->GetInput());
                    if (dummySourceData) // if dummyData are requested, create it, pointing to the same points array as the original data's points
                    {
                        vtkSmartPointer<vtkPolyData> dummyData = vtkSmartPointer<vtkPolyData>::New();
                        dummyData->SetPoints(data->GetPoints());
                        dummyData->GetPointData()->AddArray(data->GetPointData()->GetArray(maskArrayName.data()));
                        data = dummyData;
                    }
                    AddGroupOfPoints(data, nameOfSpheres.str(), persistent, radius, maskArrayName,
                        DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false, false);
                    SetObjectColor(nameOfSpheres.str(), highlights._p_defaultHighlighterColor);
                    actor->SetPointHighlights(actors[nameOfSpheres.str()]);
                    actors[nameOfSpheres.str()]->SetUserMatrix(actor->GetMatrix()); // make sure the highlighters use the same transformation as the parent model
                }
                else if (maskArrayName != "") // if the actor was newly created, this was done in the AddGroupOfPoints already, so do it only if we are not creating it
                {
                    vtkGlyph3DMapper::SafeDownCast(actor->GetPointHighlights()->GetMapper())->MaskingOn();
                    vtkGlyph3DMapper::SafeDownCast(actor->GetPointHighlights()->GetMapper())->SetMaskArray(maskArrayName.data());
                }
                else
                    vtkGlyph3DMapper::SafeDownCast(actor->GetPointHighlights()->GetMapper())->MaskingOff();
                actor->GetPointHighlights()->SetVisibility(true);
                actor->GetPointHighlights()->GetMapper()->Modified();
                actor->GetPointHighlights()->GetMapper()->Update();
            }            
            else// if opengl 3.2 is not supported, use the old, slow code
            {
                actor->GetMapper()->Update();
                vtkDataSet * data = actor->GetMapper()->GetInput();

                AddGroupOfPoints(data, nameOfSpheres.str(), persistent, radius, maskArrayName, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, true, false); // add as non-pickable
                SetObjectColor(nameOfSpheres.str(), highlights._p_defaultHighlighterColor);
                actor->SetPointHighlights(actors[nameOfSpheres.str()]);
                actors[nameOfSpheres.str()]->SetUserMatrix(actor->GetMatrix()); // make sure the highlighters use the same transformation as the parent model
            }

            if (refreshScreen)
            {
                resetCamera = false; // we don't want to reset the camera after adding the highlighters
                Refresh(); // we have added a new actor - need to re-setup the scene and re-render
            }
            return nameOfSpheres.str();
        }
        return "";
    }

    void VtkDisplay::HighlightPointsOff(std::string name)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end() && actorIter->second->GetPointHighlights()) // if there is such object
            actorIter->second->GetPointHighlights()->SetVisibility(false);
    }

	void VtkDisplay::HighlightSelectedPoints(std::string name, double radius, bool blockRefresh)
	{
        auto backup = actors[name]->GetPointHighlights(); // HighlightPoints will write to the PointHighlights property - back it up
        actors[name]->SetPointHighlights(actors[name]->GetSelectedPointHighlights());
        HighlightPoints(name, true, radius, false, SELECTED_PRIMITIVES);
        SetObjectColor(actors[name]->GetPointHighlights()->GetName(), highlights._p_colorOfSelection);
        actors[name]->SetSelectedPointHighlights(actors[name]->GetPointHighlights());
        actors[name]->SetPointHighlights(backup);

        resetCamera = false; // we don't want to reset the camera after adding the highlighters
        if (!blockRefresh)
            Refresh(); // we have added a new actor - need to re-setup the scene and re-render
	}

    void VtkDisplay::HighlightSelectedPointsOff(std::string name)
    {
        auto actorIter = actors.find(name);
        if (actorIter != actors.end() && actorIter->second->GetSelectedPointHighlights()) // if there is such object
        {
            actorIter->second->GetSelectedPointHighlights()->SetVisibility(false);
            rw->Render();
        }
    }

    void VtkDisplay::HighlightSinglePoint(std::string name, bool persistent, unsigned int baseColor, int pointID)
    {
        if (actors.find(name) != actors.end()) // if there is such object
        {
            vtkDataSet *data = actors[name]->GetMapper()->GetInput();
            int endCycle = pointID + 1;
            if (pointID == -1) // highlight all points
            {
                endCycle = data->GetNumberOfPoints();
                pointID = 0;
            }
            float r, g, b, a;
            colorFormatTransferUINTtoFLOAT(baseColor, &a, &r, &g, &b);
            for (int i = pointID; i < endCycle; i++)
            {
                std::stringstream nameOfSpheres;
                nameOfSpheres << name << "_point_" << i;
                double currPoint[3];
                data->GetPoint(i, currPoint);
                vtkSmartPointer<VtkDisplayActor> actor = highlights.point(currPoint[0], currPoint[1], currPoint[2], POINTHIGHLIGHT_RADIUS / POINTHIGHLIGH_SMALL_FACTOR);
                actor->GetProperty()->SetDiffuseColor(r, g, b); // set the default color
                AddActor(actor, nameOfSpheres.str(), persistent, DISPLAY_MODE::SELECTION_HIGHLIGHTERS, false);
            }

            resetCamera = false; // we don't want to reset the camera after adding the highlighters
            Refresh(); // we have added a new actor - need to re-setup the scene and re-render
        }
    }

    void VtkDisplay::HighlightPointOff(std::string name, int pointID)
    {
        if (actors.find(name) != actors.end()) // if there is such object
        {
            if (pointID != -1)
            {
                std::stringstream nameOfSpheres;
                nameOfSpheres << name << "_point_" << pointID;
                if (actors.find(nameOfSpheres.str()) != actors.end())
                    RemoveActor(nameOfSpheres.str());
            }
            else // remove all
            {
                std::stringstream nameOfSpheres;
                nameOfSpheres << name << "_point_";
                for (auto iter = actors.begin(); iter != actors.end(); iter++) // find all the actors that match the prefix and remove them
                {
                    auto res = std::mismatch(nameOfSpheres.str().begin(), nameOfSpheres.str().end(), iter->first.begin()); // check if the nameOfSpheres is a prefix of name of the actor
                    if (res.first == nameOfSpheres.str().end())
                        RemoveActor(iter->first);
                }
            }
        }
    }


    void VtkDisplay::UnblockRendering()
    {
        if (rw)
        {
            rw->BlockRendering(false);
            if (refreshRequested)
                Refresh();
            else
                Render();
        }
    }

    void VtkDisplay::PrepareRenderBlockingAction(QFutureWatcher<void>& futureWatcher)
    {
        if (rw)
            rw->BlockRendering(true);
        futureWatcher.connect(&futureWatcher, &QFutureWatcher<void>::finished, this, &VtkDisplay::UnblockRendering);
    }

    vtkSmartPointer<vtkInternalOpenGLRenderWindow> VtkDisplay::GetRenderWindow(){
		return rw;
	}

	boost::container::map<std::string, vtkSmartPointer<VtkDisplayActor>> *VtkDisplay::GetDisplayActors(){
		return &actors;
	}    

    void VtkDisplay::PickCameraFocus()
    {
        vtkMousePickStylePIPER::SafeDownCast(rw->GetInteractor()->GetInteractorStyle())->InitSurfacePointPick();

        mConnect->Connect(rw->GetInteractor()->GetInteractorStyle(), vtkCommand::UserEvent + VTKEVENT_SURF_POINT_PICKED,
            this, SLOT(UpdateCameraFocus(vtkObject*, unsigned long, void*, void*)));
    }


    void VtkDisplay::UpdateCameraFocus(vtkObject*, unsigned long, void*, void* call_data)
    {
        activeCamera->SetFocalPoint(static_cast<double *>(call_data));
        rw->scenePickerUpToDate = false; // invalidate picking renderer 
        Render();
    }

    QString VtkDisplay::SetPickingType(int type)
    {
        vtkSmartPointer<vtkMousePickStylePIPER> style = vtkMousePickStylePIPER::SafeDownCast(rw->GetInteractor()->GetInteractorStyle());
        ActivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS);
        emit pickTargetChanged();
        switch (type){
        case 0:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::NODES, false, false));
        case 1:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::FACES, false, false));
        case 2:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::ELEMENTS, false, false));
        case 3:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::ENTITIES, false, false));
        case 4:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::FREE_POINTS, false, false));
        case 5:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::ELEMENTS, true, false)); // RUBBERBAND PICKING OF ELEMENTS
        case 6:
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::CREATE_POINTS, false, false));
        case 7:
            if (!useParallelProjection)
                SetUseParallelProjection(true);
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::NODES, false, true));
        case 8:
            if (!useParallelProjection)
                SetUseParallelProjection(true);
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::ELEMENTS, false, true));
		case 9:
			return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::NODES, false, false, true));
        case -1:
        default:
            // disconnect the signal for setting camera focus
            mConnect->Disconnect(rw->GetInteractor()->GetInteractorStyle(), vtkCommand::UserEvent + VTKEVENT_SURF_POINT_PICKED,
                this, SLOT(UpdateCameraFocus(vtkObject*, unsigned long, void*, void*)));
            DeactivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS); // turn off highlights
            return QString::fromStdString(style->SetPickingType(SELECTION_TARGET::NONE, false, false));
        }
    }

    bool VtkDisplay::optionVisualizeBoxes() const {
        return highlights.boxesVisible;
    }

    void VtkDisplay::setOptionVisualizeBoxes(const QVariant &value) {
        bool val = value.toBool();
        SetBoxesVisible(val);
        if (val && !IsDisplayModeActive(DISPLAY_MODE::SELECTION_HIGHLIGHTERS))
        {
            ActivateDisplayMode(DISPLAY_MODE::SELECTION_HIGHLIGHTERS);
            Refresh();
        }
        else
            Render();
        emit optionVisualizeBoxesChanged();
    }


	void VtkDisplay::movePoint(int pickPosition[2], double coord[3])
	{

		double intersection[3];
		vtkIdType pickedCellID = rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).AttributeID;
		if (GetPickedPointOnMesh(pickPosition, intersection))
		{
			vtkSmartPointer<VtkDisplayActor> pickedActor = VtkDisplayActor::SafeDownCast(rw->scenePicker->GetPixelInformation((unsigned int *)pickPosition).Prop); // the parent prop
			vtkPointSet *renderedData = vtkPointSet::SafeDownCast(pickedActor->GetMapper()->GetInput()); // get the dataset that was rendered

			// finally, find the closest point of the cell to the intersection
			double minDistance = VTK_DOUBLE_MAX;
			vtkCell *cell = renderedData->GetCell(pickedCellID);
			int closestpoint = 0;
            vtkIdType numPts = cell->GetNumberOfPoints();
			for (vtkIdType i = 0; i < numPts; i++)
			{
				double distance = vtkMath::Distance2BetweenPoints(intersection, renderedData->GetPoint(cell->GetPointId(i)));
				if (distance < minDistance)
				{
					minDistance = distance;
					closestpoint = i;
				}
			}
			renderedData->GetPoint(cell->GetPointId(closestpoint), coord);
		}
	}

}

