/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_BATCH_SOFAPOSITIONING_H
#define PIPER_BATCH_SOFAPOSITIONING_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include "common/Project.h"

namespace piper {

/// \name Modules specific functions
/// \{

/** This function applies the physics based positioning deformation to \a model using \a target
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void physPosiDeform(piper::Project & model, piper::hbm::TargetList const& target);

/** This function defines scaling parameter value defined in \a target to \a model.
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void applyTargetScalingParameter(piper::Project & model, piper::hbm::TargetList const& target);


/** This function defines reads the contourCL.xml file and populates the contourCL datastructs
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void readContourCL(piper::hbm::FEModel& c_fem, piper::hbm::Metadata& c_meta);

/** This function import Simplified Scalable Model from \a filepath in the \a project 
* \ingroup piper_app
*/
PIPERCOMMON_EXPORT void importSimplifiedScalableModel(piper::Project & project, std::string const& filepath);

/** This function applies anthropometric dimension target and landmark targets in \a target to current model by Kriging deformation.
 * Mimics the scaling using the scaling constraints module.
 * \a useKrigingWIntermediates: set true if intermediates target skin and bones are used for transformation.
 * \a useGeodesicForSkin: set true to use geodesic distance to scale the skin intermediate target. not relevant if useKrigingWIntermediates is false.
 * \a useBoneIntermediates: set true to use bones as intermediate target for the transformation. not relevant if useKrigingWIntermediates is false.
 * \a useSkinIntermediates: set true to use skin as intermediate target for the transformation. not relevant if useKrigingWIntermediates is false.
 *  !! if both this and useBoneIntermediates is set to false, this will be reset to true - there must be at least one intermediate target.
 * \a useDomains: set true if intermediates target skin and bones are used for transformation. not relevant if useKrigingWIntermediates is false.
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void applyAnthropoScalingTarget(piper::Project & project, piper::hbm::TargetList const& target, bool const& useKrigingWIntermediates = true,
    bool const& useGeodesicForSkin = true, bool const& useBoneIntermediates = true, bool const& useSkinIntermediates = true, bool const& useDomains = true);

/** Reads the provided file with control points and stores them in the target list of the project under the specified name.
 * The format of the file is one CP per line, each line either "x y z" or "id x y z", but note that the id is not used even if present - 
 * the CPs are stored in the order they are read.
 * \a filepath: Full path to the text file with control points
 * \a name: Arbitrary name to serve as an ID for the points. Must be unique - if multiple sets are added under the same name,
 * subsequent querries will always return only the first one added. Use project.target().removeControlPoint(name) to remove CPs you don't want.
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void loadTargetCPs(piper::Project & project, std::string const& filepath, std::string const& name);


/** Reads the provided file with control points and stores them in the current model's metadata under the specified name.
 * The format of the file is one CP per line, each line either "x y z" or "id x y z", but note that the id is not used even if present -
 * the CPs are stored in the order they are read.
 * \a filepath: Full path to the text file with control points
 * \a name: Arbitrary name to serve as an ID for the points. Must be unique - if multiple sets are added under the same name,
 * subsequent querries will always return only the first one added. Use project.model().metadata().removeInteractionControlPoint(name) to remove CPs you don't want.
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void loadSourceCPs(piper::Project & project, std::string const& filepath, std::string const& name);

/** Will transform the current model by kriging - mimics the "Apply" buttons of "Kriging Deformation" window.
 * \a krig_interface: Kriging deformer that will deform the model. Parameters of the provided kriging interface object must be fully set-up before calling this function, namely control points must be set.
 * \a useIntermediates: If set to true, intermediate targets will be used to create skin and bone intermediate targets
 * and then nodes of these targets will be used to create the final result (as in clicking the "Apply - with intermediate targets" button).
 * If set to false, all CPs will be used directly (as in clicking the "Apply - no intermediate targets" button).
 * \ingroup piper_app
 */
PIPERCOMMON_EXPORT void applyCPScalingTarget(piper::Project & project, piper::IntermediateTargetsInterface &krig_interface, bool useIntermediates);

/// \}
}

#endif // PIPER_BATCH_SOFAPOSITIONING_H
