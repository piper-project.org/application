/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <Python.h>

#include "PythonScript.h"

#include "Context.h"
#include "helper.h"

namespace piper {

PythonScript::PythonScript()
    : m_success(false)
{ }

QUrl PythonScript::pathUrl() const
{
    return QUrl::fromLocalFile(m_scriptPath);
}

void PythonScript::setPathUrl(QUrl scriptPath)
{
    setPath(scriptPath.toLocalFile());
    m_args.clear();
}

QString PythonScript::description() const
{
    QFile file(m_scriptPath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return "";
    QString description;
    QString line;
    if (!file.atEnd())
        line = file.readLine().trimmed();
    while (line.size() > 0 && line.startsWith('#')) {
        description.append(line.midRef(1));
        description.append("\n");
        if (file.atEnd()) break;
        line = file.readLine().trimmed();
    }
    return description;
}

bool PythonScript::run(QString scriptPath, QStringList args)
{
    setPath(scriptPath); m_args = args;
    return run();
}

void PythonScript::runAsynchronous()
{
    void(*runScriptWrapper)(piper::PythonScript*, bool (PythonScript::*)())
        = &piper::wrapClassMethodCall<piper::PythonScript, bool (PythonScript::*)()>;

    Context::instance().performAsynchronousOperation(
        std::bind(runScriptWrapper, this, static_cast<bool (PythonScript::*)()>(&PythonScript::run)),
        false, false, false, false, false);
}

bool PythonScript::run()
{
    if (!QFileInfo::exists(m_scriptPath)) {
        m_success = false;
        pCritical() << ERRORMSG << "PythonScript: file not found: " << m_scriptPath.toStdString();
        return m_success;
    }
    
    pInfo() << START << "running script at " << m_scriptPath << " - arguments (" << m_args.size() <<  "): " << m_args;
    m_success = (0 == piper::runPythonScript(m_scriptPath, m_args));

    if (!m_success)
        pCritical() << ERRORMSG << "PythonScript: error in script: " << m_scriptPath.toStdString();
    pInfo() << DONE;
    return m_success;
}

}
