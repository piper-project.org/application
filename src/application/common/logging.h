/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_LOGGING_H
#define PIPER_LOGGING_H

#ifdef WIN32
#	ifdef pipercommon_EXPORTS
#		define PIPERCOMMON_EXPORT __declspec( dllexport )
#	else
#		define PIPERCOMMON_EXPORT __declspec( dllimport )
#	endif
#else
#	define PIPERCOMMON_EXPORT
#endif

#include <string>
#include <Eigen/Core>

#include <QString>
#include <QList>
#include <QtDebug>
#include <QLoggingCategory>


#include "common/message.h"

/** \brief The Piper logging system is based on the Qt debugging technique and the QDebug class.
 *
 * The log level can be controlled by commandline switch, to get help start <tt>piper -h</tt>
 * The logging stream support the Qt rich text (which is a subset of html) to display nice log with colors.
 * The 4 macro to use are pDebug(), pInfo(), pWarning() and pCritical() according to the message to send. All macro automatically prefix the message with the according line header ([DEBUG], [WARNING], [ERROR]), except pInfo() which you can customize.
 *
 * Here are a few examples :
\code{.cpp}
#include "common/logging.h"

// Sofa ressources configuration
pDebug() << "Sofa prefix: " << sofa::helper::Utils::getSofaPathPrefix();

pInfo() << INFO << "Project checked";

if (!success) {
    pWarning() << "Target ignored: " << joint.name();
    continue;
}

pInfo() << START << "Open project: " << projectFile;
pInfo() << DONE;
\endcode
 *
 * Messages can be sent to the log from from the QML GUI throught the piper::Context class :
\code{.qml}
onFinished: { context.logInfo("Loaded "+nbTarget+" targets"); }

context.logStart("Positioning");
context.logDone(" - iterations: " + nbIterations)
\endcode
 *
 * And also from a \ref pageBatchMode python script :
\code{.py}
import piper.app
piper.app.logInfo("Starting example 01")
\endcode
 *
 * \see Qt debugging techniques: http://doc.qt.io/qt-5/debug.html
 * \see Supported HTML Subset: http://doc.qt.io/qt-5/richtext-html-subset.html
 * \todo log to file
 * \todo gui to control the log
 * \todo get log from 3rd party libraries such as sofa
 * \defgroup logging Logging facility for the Piper application
 */

PIPERCOMMON_EXPORT Q_DECLARE_LOGGING_CATEGORY(piperApp)

namespace piper {

/**\ingroup logging
 * @{
 */
#define pDebug(...)    qCDebug(piperApp,__VA_ARGS__).nospace().noquote()
#define pInfo(...)     qCInfo(piperApp,__VA_ARGS__).nospace().noquote()
#define pWarning(...)  qCWarning(piperApp,__VA_ARGS__).nospace().noquote()
#define pCritical(...) qCCritical(piperApp,__VA_ARGS__).nospace().noquote()
/// @}

/** Make QDebug knows about std::string we are using extensively
 *\ingroup logging
 */
PIPERCOMMON_EXPORT QDebug operator<<(QDebug debug, std::string const& str);

///** Make QDebug knows about Eigen::Matrix
// *\ingroup logging
// */
//template<typename _Scalar, int _Rows, int _Cols> // int _Options, int _MaxRows, int _MaxCols
//QDebug operator<<(QDebug debug, Eigen::Matrix<_Scalar, _Rows, _Cols > const& mat)
//{
//    std::ostringstream ss;
//    ss << mat;
//    debug << QString::fromStdString(ss.str());
//    return debug;
//}

/** This class is used to control the logging system behavior.
 * \ingroup logging
 */
class PIPERCOMMON_EXPORT LoggingParameter {
public:

    enum class VerboseLevel {
        DEFAULT=0, ///< all messages are displayed, except debug messages
        VERBOSE, ///< display also debug messages.
        VERY_VERBOSE ///< display also messages from 3rd party libraries
    };

    /// @return the unique logging parameters instance
    static LoggingParameter& instance();

    /// set the verbosity level to \a level
    void setVerboseLevel(VerboseLevel level);
    VerboseLevel verboseLevel() const {return m_verboseLevel;}

    /// filepath to use with the logFileMessageHandler
    void setLogFilePath(QString const& filePath) {m_filePath=filePath;}
    QString const& logFilePath() const {return m_filePath;}

    void addMessageHandler(QtMessageHandler handler);
    void removeMessageHandler(QtMessageHandler handler);
    void clearMessageHandler();

private:
    VerboseLevel m_verboseLevel;
    QString m_filePath;

    friend void piperMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    // singleton design pattern
    LoggingParameter();
    LoggingParameter(LoggingParameter const&) { }

    QList<QtMessageHandler> m_messageHandlers;
};

/* Message handler to send messages to the application log panel
 * \ingroup logging
 */
PIPERCOMMON_EXPORT void logPanelMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

/* Message handler to send messages to the console
 * \ingroup logging
 */
PIPERCOMMON_EXPORT void consoleMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

PIPERCOMMON_EXPORT void logFileMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

///Call function with args, catch any exception and send the error message to the log.
template<typename FunctionPtrT, typename... Args>
void wrapFunctionCall(FunctionPtrT function, Args... args) {
    try {
        (*function)(args...);
    }
    catch (std::exception const& e) {
        pCritical() << e.what();
    }
}
///Call method of instance with args, catch any exception and send the error message to the log.
template<class ClassT, typename MethodPtrT, typename... Args>
void wrapClassMethodCall(ClassT *instance, MethodPtrT method, Args... args) {
    try {
        (instance->*method)(args...);
    }
    catch (std::exception const& e) {
        pCritical() << e.what();
    }
}

}

#endif // PIPER_LOGGING_H
