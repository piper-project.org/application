// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
%module app

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "exception.i"

%include "std_string.i"
%include "std_list.i"
%include "std_vector.i"
%include "std_map.i"

%{
#include <units/units.h>
#include <hbm/HumanBodyModel.h>
#include <hbm/EnvironmentModels.h>
#include <hbm/target.h>
#include <hbm/ControlPoint.h>

#include "message.h"
#include "logging.h"
#include "helper.h"
#include "Context.h"
#include "ModuleParameter.h"
#include "Project.h"

#include "batch.h"
%}

%exception {
  try {
    $action
  } catch (const std::runtime_error& e) {
    pCritical() << e.what(); //TODO add information on exception source
    SWIG_exception(SWIG_RuntimeError,const_cast<char*>(e.what()));
  }
}

%template(Listui) std::list<unsigned int>;
%template(vecDouble) std::vector<double>;
%template(mapStringString) std::map<std::string, std::string>;

namespace piper {

namespace units {
enum class Length {m, dm, cm, mm};
}

%nodefaultctor Context;
%nodefaultdtor Context;
class Context {
public:
    static piper::Context& instance();
    piper::Project& project();
    bool busyTaskCancelRequested() const;
    void addNewHistory(std::string const& historyName);

};

class ModuleParameter {
public:

    bool getBool(std::string const& moduleName, std::string const& parameterName);
    unsigned int getUInt(std::string const& moduleName, std::string const& settingName);
    std::list<unsigned int> getListUInt(std::string const& moduleName, std::string const& parameterName);
    double getDouble(std::string const& moduleName, std::string const& settingName);

    template<typename T>
    void set(std::string const& moduleName, std::string const& parameterName, T value);

    %template(setBool) set<bool>;
    %template(setInt) set<int>;
    %template(setFloat) set<float>;

};


class Project {
public:

    Project();
    Project(std::string const& projectFile);

    void read(std::string const& projectFile);
    void write(std::string const& projectFile);

    void loadTarget(std::string const& targetFile);

    void importModel(std::string const& modelrulesFile);
    void exportModel(std::string const& directory);

	void collectControlPoints(piper::hbm::InteractionControlPoint& source, piper::hbm::InteractionControlPoint& target, 
        std::map<std::string, std::string> association);

    piper::hbm::HumanBodyModel& model();
    piper::hbm::EnvironmentModels& environment();
    piper::hbm::TargetList& target();
    piper::ModuleParameter& moduleParameter();
};

std::string tempDirectoryPath(std::string const& module="");

}

%include "batch.h"

%inline
%{
// specific wrapping for logging functions as QDebug stream cannot be wrapped as-is

    void logDebug(std::string const& message) {
        pDebug() << QString::fromStdString(message);
    }
    void logInfo(std::string const& message) {
        pInfo() << piper::INFO << QString::fromStdString(message);
    }
    void logWarning(std::string const& message) {
        pWarning() << QString::fromStdString(message);
    }
    void logError(std::string const& message) {
        pCritical() << QString::fromStdString(message);
    }

    void logSuccess(std::string const& message) {
        pInfo() << piper::SUCCESS << QString::fromStdString(message);
    }
    void logStart(std::string const& message) {
        pInfo() << piper::START << QString::fromStdString(message);
    }
    void logDone(std::string const& message="") {
        pInfo() << piper::DONE << QString::fromStdString(message);
    }

    void modelUpdated() {
        piper::Context::instance().modelUpdated();
    }

    void metadataChanged() {
        piper::Context::instance().metadataChanged();
    }

    void targetChanged() {
        piper::Context::instance().targetChanged();
    }

    void historyListChanged() {
        piper::Context::instance().historyListChanged();
    }

%}


