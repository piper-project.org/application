/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "QTargetLoader.h"

#include <iostream>

#include "common/Context.h"

namespace piper {

void QTargetLoader::process(std::string const& /*targetListName*/)
{
    process(Context::instance().project().target());
}

void QTargetLoader::process(hbm::TargetList const& targetList)
{
    Context::instance().performAsynchronousOperation(std::bind(&QTargetLoader::_process, this, targetList), false, false, false, false, false);
}

void QTargetLoader::_process(hbm::TargetList const& targetList)
{
    started();
    // fixed bones
    for(hbm::FixedBoneTarget const& target : targetList.fixedBone)
        fixedBone(QString::fromStdString(target.bone));
    // landmarks
    for(hbm::LandmarkTarget const& target : targetList.landmark) {
        QVariantList mask;
        for (bool m : target.mask())
            mask.append((int)m);
        QVariantList value;
        for (std::size_t i = 0; i< target.mask().size(); ++i) {
            auto const& dof_val = target.value().find((unsigned int)i);
            if (dof_val != target.value().end())
                value.append(dof_val->second);
        }
        newLandmarkTarget(
                    QString::fromStdString(target.name()),
                    QString::fromStdString(target.landmark),
                    mask,
                    value);
    }
    // joint
    for(hbm::JointTarget const& target : targetList.joint) {
        QVariantList value;
        for (std::size_t i = 0; i< target.mask().size(); ++i) {
            auto const& dof_val = target.value().find((unsigned int)i);
            if (dof_val != target.value().end())
                value.append(dof_val->second);
        }
        newJointTarget(QString::fromStdString(target.name()), QString::fromStdString(target.joint), value);
    }
    // frame
    for(hbm::FrameToFrameTarget const& target : targetList.frameToFrame) {
        QVariantList mask;
        for (bool m : target.mask())
            mask.append((int)m);
        QVariantList value;
        for (std::size_t i = 0; i< target.mask().size(); ++i) {
            auto const& dof_val = target.value().find((unsigned int)i);
            if (dof_val != target.value().end())
                value.append(dof_val->second);
        }
        newFrameToFrameTarget(
                    QString::fromStdString(target.name()),
                    QString::fromStdString(target.frameSource),
                    QString::fromStdString(target.frameTarget),
                    target.isRelative,
                    mask,
                    value);
    }
    finished();
}

}
