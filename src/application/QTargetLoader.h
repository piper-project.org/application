/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_QTARGETLOADER_H
#define PIPER_QTARGETLOADER_H

#include <string>
#include <hbm/target.h>

#include <QObject>
#include <QString>
#include <QVariantList>

namespace piper {

/** Target loader that sends Qt signals to create targets from a target list
 *
 * @sa piper::hbm::TargetList
 * @author Thomas Lemaire @date 2015
 */
class QTargetLoader : public QObject
{
    Q_OBJECT
public:

    /// \todo get the correct targetList by name
    void process(std::string const& targetListName);

    Q_INVOKABLE void process(QString targetListName) { process(targetListName.toStdString()); }

    void process(hbm::TargetList const& targetList);

signals:
    void started();
    void fixedBone(QString boneName);
    void newLandmarkTarget(QString targetName, QString landmarkName, QVariantList mask, QVariantList value);
    void newJointTarget(QString targetName, QString jointName, QVariantList value);
    void newFrameToFrameTarget(QString targetName, QString frameSource, QString frameTarget, bool isRelative, QVariantList mask, QVariantList value);
    void finished();

private:
    void _process(hbm::TargetList const& targetList);

};

}

#endif // PIPER_QTARGETLOADER_H
