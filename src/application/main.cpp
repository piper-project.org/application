/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
//#if defined(_MSC_VER) && defined(_DEBUG)
//#undef _DEBUG
//#include <Python.h>
//#define _DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#else
#include <Python.h>
//#endif

#include <cstdio>

#include "version.h"

#pragma warning(push, 0)
#include <sofa/helper/Utils.h>
#include <sofa/helper/system/PluginManager.h>
#include <sofa/helper/logging/MessageDispatcher.h>
#include <sofa/gui/BaseGUI.h>
#pragma warning(pop)
#include <SofaPiper/tools.h>

#include <anatomyDB/anatomictreemodel.h>

#include <common/init.h>
#include <common/batch.h>
#include <common/helper.h>
#include <common/SpinePredictor.h>
#include <common/AnthropometryPredictor.h>
#include <common/KrigingDisplay.h>
#include <common/MetadataQmlProxy.h>

#include <module/check/Check.h>
#include <module/anthropometry/AnthropometryModule.h>
#include <module/kriging/KrigingModule.h>
#include <module/kriging/RegisterSurfaces.h>
#include <module/meshOptimizing/meshOptimizing.h>
#include <module/BodySectionPersonalizing/BodySectionPersonalizing.h>
#include <module/BodySectionPersonalizing/AnthropoModelDisplay.h>
#include <module/BodySectionPersonalizing/AnthropoTree.h>
#include <module/contourDeformation/ContourDeformation.h>
#include <module/contourPersonalization/ContourPersonalization.h>
#include <module/bodyDimTargetGen/BodyDimTargetGen.h>
#include <module/scalingParameter/ScalingParameter.h>
#include "EnvManager.h"
#include "QTargetLoader.h"


#include <QtDebug>
#include <QLocale>
#include <QCommandLineParser>
#include <QtQml>
#include <QQmlApplicationEngine>
#include <QQmlContext>


using namespace piper;

int main(int argc, char *argv[])
{
    // TODO: this command disable the multithreaded render loop, currently we need this on Linux/OSX because our implementation of the sofa interface is not thread-safe
    qputenv("QSG_RENDER_LOOP", "basic");
    QGuiApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName(QStringLiteral("Piper"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("www.piper-project.org/"));
    QCoreApplication::setApplicationName(QStringLiteral("Piper"));
    QCoreApplication::setApplicationVersion(QString("%1.%2.%3").arg(PIPER_VERSION_MAJOR).arg(PIPER_VERSION_MINOR).arg(PIPER_VERSION_PATCH));

    // for string/floating point number convertions done with Qt
    QLocale::setDefault(QLocale::C);

    QApplication app(argc, argv);
    QObject::connect(&app, &QCoreApplication::aboutToQuit, piper::cleanup);

    // command line arguments
    QCommandLineParser argumentParser;
    argumentParser.setApplicationDescription("Piper application");
    argumentParser.addHelpOption();
    argumentParser.addVersionOption();
    QCommandLineOption batchOption(QStringList() << "batch" << "b",
                                   QCoreApplication::translate("main", "Start in batch mode."));
    argumentParser.addOption(batchOption);
    argumentParser.addPositionalArgument("file", "Optionnal project file (.ppj) | In batch mode mandatory python script (.py).","[path]");
    QCommandLineOption verboseOption(QStringList() << "verbose" << "V",
                                     "Make the application more verbose");
    argumentParser.addOption(verboseOption);
    QCommandLineOption veryVerboseOption(QStringList() << "very_verbose" << "VV",
                                         "Make the application even more verbose");
    argumentParser.addOption(veryVerboseOption);
    QCommandLineOption logToFileOption(QStringList() << "log_to_file" << "l",
                                     "Write application log to file");
    argumentParser.addOption(logToFileOption);
    argumentParser.process(app);

    if (argumentParser.isSet(veryVerboseOption)) {
        LoggingParameter::instance().setVerboseLevel(LoggingParameter::VerboseLevel::VERY_VERBOSE);
        // TODO redirect sofa messages to the piper log
    }
    else if (argumentParser.isSet(verboseOption)) {
        LoggingParameter::instance().setVerboseLevel(LoggingParameter::VerboseLevel::VERBOSE);
        sofa::helper::logging::MessageDispatcher::clearHandlers();
    }
    else {
        sofa::helper::logging::MessageDispatcher::clearHandlers();
    }
//    if (argumentParser.isSet(logToFileOption))
        LoggingParameter::instance().addMessageHandler(piper::logFileMessageHandler);

    Py_SetProgramName(argv[0]);
    piper::init();

    if (argumentParser.isSet(batchOption)) {
        /*
         * batch mode
         */
        if (argumentParser.positionalArguments().size()!=1) {
            argumentParser.showHelp(-1);
        }

        qputenv("PIPER_BATCH_MODE", "1");

        // Sofa initialization
        piper::sofaInit();

        pInfo() << INFO << "Piper " << QCoreApplication::applicationVersion() << " - compiled on: " << PIPER_VERSION_DATE << " - git hash: " << PIPER_VERSION_GIT_HASH;

        int errorCode = piper::runPythonScript(argumentParser.positionalArguments().first());

        piper::cleanup();

        std::exit(errorCode);
    }
    else {
        /*
         * interactive mode
         */

        // sofa initialization
        sofaPiper::setProject(Context::instance().project());

        // load Sofa GUI plugin
#ifdef WIN32
        app.addLibraryPath(QString::fromStdString(sofa::helper::Utils::getSofaPathPrefix()+"/bin"));
#else
        app.addLibraryPath(QString::fromStdString(sofa::helper::Utils::getSofaPathPrefix()+"/lib"));
#endif
        QString pluginPath("SofaQtQuickGUI");
    #ifdef DEBUG
        pluginPath.append("_d");
    #endif
        QPluginLoader pluginLoader(pluginPath);
        // first call to instance() initializes the plugin
        if(0 == pluginLoader.instance())
            qFatal("plugin %s has not been found: %s", pluginPath.toStdString().c_str(), pluginLoader.errorString().toStdString().c_str());
        sofa::helper::system::PluginManager::s_gui_postfix = "qtquickgui";

        QQmlApplicationEngine engine;
	    engine.addImportPath("qrc:/");
        engine.addImportPath("qrc:/qml");
        // register modules
        qmlRegisterType<piper::QTargetLoader>("piper.QTargetLoader",1,0,"QTargetLoader");
        qmlRegisterType<piper::Check>("piper.Check",1,0,"Check");
        qmlRegisterType<piper::AnthropometryModule>("piper.AnthropometryModule", 1, 0, "AnthropometryModule");
        qmlRegisterType<piper::AnthroChildScaling>("piper.AnthroChildScaling", 1, 0, "AnthroChildScaling");
        qmlRegisterType<piper::AnthropometryPredictor>("piper.predictors", 1,0, "AnthropometryPredictor");


        qmlRegisterType<piper::scalingparameter::ScalingParameter>("piper.ScalingParameter",1,0,"ScalingParameter");
		qRegisterMetaType<piper::scalingparameter::TableParameterModel*>("TableParameterModel*");

        qmlRegisterType<piper::kriging::KrigingModule>("piper.Kriging", 1, 0, "KrigingModule");
        qRegisterMetaType<piper::kriging::KrigingModule*>("ModelTableSource*");
        qRegisterMetaType<piper::kriging::KrigingModule*>("ModelTableTarget*");
        qmlRegisterType<piper::kriging::Kriging>("piper.Kriging", 1, 0, "Kriging");
        qmlRegisterType<piper::kriging::RegisterSurfaces>("piper.Kriging", 1, 0, "RegisterSurfacesIntrfc");

        qRegisterMetaType<piper::anthropoPersonalizing::AnthropoTreeProxy*>("AnthropoTreeProxy*");
        qmlRegisterType<piper::anthropoPersonalizing::BodySectionPersonalizing>("piper.BodySectionPersonalizing", 1, 0, "BodySectionPersonalizing");
        qRegisterMetaType<piper::anthropoPersonalizing::ModelTable*>("ModelTable*");
        qmlRegisterType<piper::anthropometricmodel::AnthropoModelDisplay>("piper.AnthropoModelDisplay", 1, 0, "AnthropoModelDisplay");
        qRegisterMetaType<piper::anthropoPersonalizing::ModelTableCombo*>("ModelTableCombo*");
        qRegisterMetaType<piper::anthropoPersonalizing::ModelAssBodySection*>("ModelAssBodySection*");
        qRegisterMetaType<piper::anthropoPersonalizing::BodyDimensionSourceTargetTable*>("BodyDimensionSourceTargetTable*");
        qmlRegisterType<piper::anthropoPersonalizing::ListModel>("piper.ListModel", 1, 0, "ListModel");
        qRegisterMetaType<piper::anthropoPersonalizing::ModelviaPoint*>("ModelviaPoint*");

		qmlRegisterType<QVTKFrameBufferObjectItem>("VtkQuick", 1, 0, "VtkRenderWindow");
		qmlRegisterType<piper::MetaViewManager>("piper.MetaViewManager", 1, 0, "MetaViewManager");
		qmlRegisterType<piper::MetaEditor>("piper.MetaEditor", 1, 0, "MetaEditor");

        qmlRegisterType<piper::BodyDimTargetGen::BodyDimTargetGen>("piper.BodyDimTargetGen", 1, 0, "BodyDimTargetGen");
		qmlRegisterType<piper::contourdeformation::ContourDeformation>("piper.ContourDeformation", 1, 0, "ContourDeformation");
		qmlRegisterType<piper::contours::ContourPersonalization>("piper.ContourPersonalization", 1, 0, "ContourPersonalization");
        qmlRegisterType<piper::envmanager::EnvManager>("piper.EnvManager",1,0,"EnvManager");
		qRegisterMetaType<piper::envmanager::TableEnvModel*>("TableEnvModel*");
        qmlRegisterType<piper::meshoptimizing::MeshOptimizing>("piper.meshOptimizing", 1, 0, "MeshOptimizing");
        qRegisterMetaType<piper::meshoptimizing::TableQualityModel*>("TableQualityModel*");
        qRegisterMetaType<piper::meshoptimizing::TableQualityModelMES*>("TableQualityModelMES*");
		qRegisterMetaType<piper::meshoptimizing::TableQualityModelSurfSmooth*>("TableQualityModelSurfSmooth*");
		qRegisterMetaType<piper::VtkDisplay*>("VtkDisplay*");
		qRegisterMetaType<piper::MetaViewManager*>("MetaViewManager*");
		qRegisterMetaType<piper::MetaEditor*>("MetaEditor*");
//        qRegisterMetaType<piper::OctaveProcess*>();
        qmlRegisterType<piper::SpinePredictor>("piper.predictors", 1,0, "SpinePredictor");
        qmlRegisterType<piper::PythonScript>("piper.scripting", 1,0, "PythonScript");

        qmlRegisterType<AnatomicTreeModel>("AnatomicTreeModel", 1, 0, "AnatomicTreeModel");
        qmlRegisterType<piper::MetadataQmlProxy>("piper.proxy", 1, 0, "MetadataQmlProxy");

        // register singletons into qml context
        engine.rootContext()->setContextProperty("context", &Context::instance());
        engine.rootContext()->setContextProperty("project", &Context::instance().project());
        engine.rootContext()->setContextProperty("moduleParameter", &Context::instance().project().moduleParameter().parameter());
        engine.rootContext()->setContextProperty("contextVtkDisplay", &Context::instance().display());
        engine.rootContext()->setContextProperty("contextMetaManager", &Context::instance().metaManager());
		engine.rootContext()->setContextProperty("myMetaEditor", &Context::instance().metaEditor());
        engine.rootContext()->setContextProperty("applicationDirPath", QGuiApplication::applicationDirPath());

        engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

        // once the gui is loaded, messages can be sent to the log panel
        LoggingParameter::instance().removeMessageHandler(piper::consoleMessageHandler);
        LoggingParameter::instance().addMessageHandler(piper::logPanelMessageHandler);
        pInfo() << INFO << "Piper " << QCoreApplication::applicationVersion() << " - compiled on: " << PIPER_VERSION_DATE << " - git hash: " << PIPER_VERSION_GIT_HASH;
        if (argumentParser.isSet(logToFileOption))
            pInfo() << INFO << "Logging to file " << LoggingParameter::instance().logFilePath();

        if (!argumentParser.positionalArguments().empty())
            Context::instance().openProject(QUrl::fromLocalFile(argumentParser.positionalArguments().first()));

        return app.exec();

    }
}
