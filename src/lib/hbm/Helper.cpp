/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Helper.h"

#include <sstream>
#include <sys/stat.h>
#include <string>

using namespace std;



namespace piper { 
	namespace hbm {


        
     
        
        std::string absolutePath(std::string const& inputpath, std::string const& originpath) {
			boost::filesystem::path path(inputpath);
			if (path.is_absolute())
				return inputpath;
			else {
				boost::filesystem::path absolutePath(originpath);
				absolutePath /= inputpath;
				return  absolutePath.string();
			}
		}

		// boost::filesystem doesn't have this functionnality !
		boost::filesystem::path relativePath(const boost::filesystem::path &path, const boost::filesystem::path &relative_to)
		{
			namespace fs = boost::filesystem;

            if (path.empty())
                return path;

			// create absolute paths
			fs::path p = fs::absolute(path);
			fs::path r = fs::absolute(relative_to);

			// if root paths are different, return absolute path
			if (p.root_path() != r.root_path())
				return p;

			// initialize relative path
			fs::path result;

			// find out where the two paths diverge
			fs::path::const_iterator it_path = p.begin();
			fs::path::const_iterator it_relative_to = r.begin();
			while (*it_path == *it_relative_to && it_path != p.end() && it_relative_to != r.end()) {
				++it_path;
				++it_relative_to;
			}

			// add "../" for each remaining token in relative_to
			while (it_relative_to != r.end()) {
				result /= "..";
				++it_relative_to;
			}

			// add remaining path
			while (it_path != p.end()) {
				result /= *it_path;
				++it_path;
			}

			return result;
		}
	
        void get_all_in_directory(const fs::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret)
        {
            if (!fs::exists(root) || !fs::is_directory(root)) return;

            fs::recursive_directory_iterator it(root);
            fs::recursive_directory_iterator endit;

            while (it != endit)
            {
                if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path().filename());
                ++it;

            }

        }
        
        string space2underscore(string const& text)
		{
			string textreturn( text);
			replace(textreturn.begin(), textreturn.end(), ' ', '_');
			return textreturn;
		}


		std::vector<std::string> split(const std::string &s, char delim) {
			std::vector<std::string> elems;
			std::stringstream ss(s);
			std::string item;
			while (std::getline(ss, item, delim)) {
				elems.push_back(item);
			}
			return elems;
		}

		bool dirExists(const char *path)
		{
			struct stat info;

			if(stat( path, &info ) != 0)
				return 0;
			else if(info.st_mode & S_IFDIR)
				return 1;
			else
				return 0;
		}

		bool fileExists(const char *file)
		{
			struct stat info;
			// file attribute
			int ret = stat(file, &info);
			if(ret != 0)
				return 0;
			else
				return 1;
		}


// inspired by http://www.geometrictools.com/GTEngine/Include/GteApprSphere3.h
unsigned int fitSphere(VCoord const& points, Coord& center, double& radius, unsigned int maxIterations)
{
    size_t numPoints = points.size();

    // Compute the average of the data points.
    Coord average = Coord::Zero();
    for (Coord const& point : points)
        average += point;
    double invNumPoints = 1.0/numPoints;
    average *= invNumPoints;
    // The initial guess for the center.
    center = average;
    unsigned int iteration;
    for (iteration = 0; iteration < maxIterations; ++iteration) {
        // Update the iterates.
        Coord current = center;

        // Compute average L, dL/da, dL/db, dL/dc.
        double lenAverage = 0.0;
        Coord derLenAverage = Coord::Zero();
        for (Coord point : points) {
            Coord diff = point - center;
            double length = diff.norm();
            if (length > 0.0){
                lenAverage += length;
                derLenAverage -= diff / length;
            }
        }
        lenAverage *= invNumPoints;
        derLenAverage *= invNumPoints;

        center = average + lenAverage * derLenAverage;
        radius = lenAverage;

        if ((center-current).isApprox(Coord::Zero()))
            break;
    }
    return ++iteration;
}

void getFiles(const std::string& p, const std::string& ext, std::vector<std::string>& ret)
{
    boost::filesystem::path root(p);
    //root = root.parent_path();
    if (!fs::exists(root) || !fs::is_directory(root)) return;

    fs::directory_iterator end_itr;

    // cycle through the directory
    for (fs::directory_iterator itr(p); itr != end_itr; ++itr)
    {
        // If it's not a directory, list it. If you want to list directories too, just remove this check.
        if (is_regular_file(itr->path()) && itr->path().extension() == ext) 
            ret.push_back(itr->path().string());
    }
}

void getValidName(std::string& name, std::vector<std::string> const& listnames) {
    // find history name that starts with trame
    int appendNumber = 0;
    while (true) // for overflow protection
    {
        for (auto const& cur : listnames) {
            if (name == cur)
                appendNumber = appendNumber == 0 ? 1 : appendNumber; // if no number was appended yet, use 1, otherwise keep the highest
            else {
                std::size_t pos = cur.find_last_of("_");
                if (pos != std::string::npos && name == cur.substr(0, pos)) {// possibliy an auto increment name
                    std::string check = cur.substr(pos + 1);
                    if (check.find_last_not_of("0123456789") == std::string::npos)// check that it has only numbers
                    {
                        if (check.size() > 9) // to prevent overflow, add another "_" until a valid appendnumber can be set
                        {
                            name += "_";
                            continue;
                        }
                        int number = stoi(check); // no need to check for exceptions - we know check has only numbers. only exception can be 
                        if (number >= appendNumber)
                            appendNumber = number + 1; // set what should be appended -> 1 higher
                    }
                }
            }
        }
        break;
    }
    if (appendNumber > 0) {
        name += "_" + std::to_string(appendNumber);
    }

}

}
}
