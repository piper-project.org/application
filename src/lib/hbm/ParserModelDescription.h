/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PARSERMODELDESCRIPTION__H
#define PARSERMODELDESCRIPTION__H


#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "ParserHelper.h"

#include <string>

namespace piper {
	namespace hbm {
        class FEModel;
        class Metadata;
        namespace parser {


            class TermDescription : public Prototype<TermDescription> {
            public:
                TermDescription() {}
                virtual void addTerm(std::shared_ptr<TermDescription> t);
                virtual bool parseXml(tinyxml2::XMLElement* element);
                virtual void fillmetadata(FEModel* femodel, Metadata& metadata) const;
            protected:
                std::string name;
                Factory<TermDescription> facTermDescription;
                std::vector<std::shared_ptr<TermDescription>> children;
            };

            typedef std::shared_ptr<TermDescription> TermDescriptionPtr;

			class ParserModelDescription: public TermDescription {
			public:
				ParserModelDescription() {};
				ParserModelDescription(const std::string& file);
                void setFile(const std::string& file, bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool anatomicalJoint = true,  bool controlpoint = true, bool HbmParameter = true);

            private:
                void initFactory(bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool anatomicalJoint = true, bool controlpoint = true, bool HbmParameter = true);
            };

            class AssociationAnthropometry : public TermDescription {
            public:
                AssociationAnthropometry() {}
                bool parseXml(tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
            private:
                double m_value;
                std::string m_unit_type, m_unit;
            };


            class AssociationGenericMetadata : public TermDescription {
            public:
                AssociationGenericMetadata() : m_auto(false) {}
                bool parseXml(tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
                void setAuto() { m_auto = true; }
                bool isAuto() const { return m_auto; }
            protected:
                bool m_auto; //for auto association with a dictionnaire
                std::vector<std::string> key;
                std::vector<std::vector<IdKey>> vidkey;
            };

            
            class AssociationEntity : public TermDescription {
			public:
                AssociationEntity() : m_auto(false), m_innernormal(false) {}
				bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
				void setAuto() {m_auto=true;}
				bool isAuto() const {return m_auto;}
			protected:
                bool m_innernormal;
				bool m_auto; //for auto association with a dictionnaire
                std::vector<std::string> key;
                std::vector<std::vector<IdKey>> vidkey;
            };


			class AssociationLandmarks: public TermDescription {
			public:
				AssociationLandmarks(): m_auto(false) {}
				bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
				void setAuto() {m_auto=true;}
				bool isAuto() const {return m_auto;}
			protected:
				bool m_auto; //for auto association with a dictionnaire
				Landmark::Type m_typelandmark;
                std::vector<std::string> key;
                std::vector<std::vector<IdKey>> vidkey;
                VCoord vcoord;

            };

			class AssociationControlPoint: public TermDescription {
			public:
				AssociationControlPoint(){
                    weight = NAN;// set weight as NAN - a flag that will be interpreted as "no weight specified, using the default weight"
                    globalAs_bones = NAN;
                    globalAs_skin = NAN;
                }                

                bool parseXml(tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
			protected:

				std::string _ctrlPtRole;
				std::string _name;
                std::vector<std::string> key;
                std::vector<std::vector<IdKey>> vidkey;
                VCoord vcoord;
                double weight, globalAs_bones, globalAs_skin; // global values (per CP-set)
                std::vector<double> weightsCont, as_bone, as_skin; // per-point values
			};

            /// \todo test it in FEModel_test
            class AssociationJoint: public TermDescription {
			public:
				AssociationJoint() {}
				bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
			protected:
				std::string entityA;
				std::string entityB;
                std::string keyFrameA, keyFrameB;
                IdKey idFrameA, idFrameB;
				std::string typeFrameA;
				std::string typeFrameB;
				std::vector<bool> dof;
                std::string constrainedDofType;
			};

            class AssociationAnatomicalJoint: public TermDescription {
            public:
                AssociationAnatomicalJoint() {}
                bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
            protected:
                std::vector<bool> dof;
                std::string constrainedDofType;
            };

			class Associationparameter: public TermDescription {
			public:
				bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
			protected:
				std::string source;
                std::vector<std::string> key;
                std::vector<std::vector<IdKey>> vidkey;
            };

			class AssociationContact: public TermDescription {	
			public:
				AssociationContact();
				bool parseXml( tinyxml2::XMLElement* element);
                void fillmetadata(FEModel* femodel, Metadata& metadata) const;
			protected:
				bool keepthickness;
				double thickness;
				std::string entity1, entity2;
				std::string type;
                std::string key1, key2;
                IdKey id1, id2;
            };
		}
	}
}

#endif
