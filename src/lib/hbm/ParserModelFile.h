/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PARSERMODELFILE__H
#define PARSERMODELFILE__H


#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif


#include "ParserContext.h"
#include "Metadata.h"

#include <string>
#include <vector>

namespace piper {
	namespace hbm {
		class FEModel;
		namespace parser {
			
			

			/**
			* @brief The ParserModelFile class encapsulates parsing/updating of the FE model files.
			*
			*
			* @author Erwan Jolivet @date 2015
			* \ingroup libhbm
			*/
			class HBM_EXPORT ParserModelFile {
			public:
                ParserModelFile() {};
                /*!
				*  \brief Construct ParserModelFile with formatrules file (xml file) only
				*
				*  \param formatrulesFile: xml file with general format information and rules to parse keyword FE format
				*
				*/
				ParserModelFile(std::string const& formatrulesFile);
				/*!
				*  \brief Construct ParserModelFile with formatrules file (xml file) and model description file (xml)
				*
				*  \param formatrulesFile: xml file with general format information and rules to parse keyword FE format
				*  \param modelrulesFile:  xml file with association to generate hbm metadata
				*
				*/
				ParserModelFile(std::string const& formatrulesFile, std::string const& modelrulesFile);

				/*!
				*  \brief do the parsing of FE model files
				*
				*  \param femodel: pointer to FEModel instance
                *  \param sourceFile: main model file
                *  \param includeFiles: additional model files not specified in the source file by include rule
                *  \param option: PARSER_OPION::NODES: parse only nodes, PARSER_OPION::MESH_ONLY: parse only nodes and elements, PARSER_OPION::WHOLE: parse whole model
                *
				*/
                void doParsing(FEModel* femodel, std::string const& sourceFile, std::vector<std::string> const&includeFiles, PARSER_OPTION option = PARSER_OPTION::WHOLE);


				/*!
				*  \brief do the update of FE model files
				*
				*  \param femodel: pointer to FEModel instance
				*  \param filename:  file path to the original FE model file
                *  \param includeFiles: additional model files not specified in the source file by include rule
				*  \param targetdirectory: directory where updated model files are written
				*  \param msg:  message to write as comment at the beginning of updated file
				*
				*/
                void doUpdating(FEModel* femodel, const std::string& filename, std::vector<std::string> const& includeFiles,
                    const std::string& targetdirectory, std::vector<std::string> const& msg);

				void fillMetadata( FEModel* femodel, Metadata& metadata);

                void setMetadataRulePath(std::string const& filename);
                void setFormatRulePath(std::string const& filename);

                void setFormatName(std::string const& name);



			private:


				ParserContext m_context;

				void parsefile(  const std::string& filename);
				void updatefile( const std::string& source, const std::string& target, std::vector<std::string> const& msg);

			};


		}
	}
}


#endif
