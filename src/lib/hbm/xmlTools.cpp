/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "xmlTools.h"
#include <iomanip>
#include <limits>
#include <sstream>

namespace piper {
namespace hbm {

void parseCoord(Coord& point3d, const tinyxml2::XMLElement* element)
{
    if (element != nullptr && element->GetText() != nullptr) {
        Coord coord;
        std::stringstream elemCoords(element->GetText());
        if (elemCoords >> coord[0] && elemCoords >> coord[1] && elemCoords >> coord[2])
            point3d = coord;
        else {
            std::ostringstream ss;
            ss << "parseCoord: Error when parsing element " << element->Name() << " - invalid data: " << element->GetText();
            throw std::runtime_error(ss.str());
        }
    }
    else {
        std::ostringstream ss;
        ss << "parseCoord: Error when parsing element " << element->Name() << " - no data";
        throw std::runtime_error(ss.str());
    }
}

void parseVecCoord(std::vector<Coord>& point3dCont, const tinyxml2::XMLElement* element)
{
    if (element != nullptr && element->GetText() != nullptr) {
        double coords[3];
        std::istringstream elemCoords(element->GetText());
        while(elemCoords >> coords[0] && elemCoords >> coords[1] && elemCoords >> coords[2]){
            point3dCont.push_back(Coord(coords[0],coords[1],coords[2]));
        }
    }
}

void serializeCoord(tinyxml2::XMLElement* element, Coord const& point3d)
{
    std::ostringstream ss;
    ss<<std::setprecision(std::numeric_limits<double>::digits10 + 1); //TODO : template double / float ?
    ss << point3d;
    element->SetText(ss.str().c_str());
}

void serializeVecCoord(tinyxml2::XMLElement* element, std::vector<Coord> const& point3dCont)
{
    std::ostringstream ids;
    int count=1;
    ids<<std::setprecision(std::numeric_limits<double>::digits10 + 1); //TODO : template double / float ?
    for (Coord const& coord : point3dCont) {
        ids << coord(0) << " " << coord(1) << " " << coord(2) << "  \n";
    //if(count%NB_MAX_ELEMENT_PER_LINE==0)
    //        ids<<"\n";
    }
    element->SetText(ids.str().c_str());
}

void serializeVecCoord(tinyxml2::XMLElement* element, std::vector<Coord> const& point3dCont, VId const& vid) {
    std::ostringstream ids;
    int count = 1;
    ids << std::setprecision(std::numeric_limits<double>::digits10 + 1); //TODO : template double / float ?
    for (Coord const& coord : point3dCont) {

        ids << vid[count - 1] << " " << coord(0) << " " << coord(1) << " " << coord(2) << "\n";
        count++;
        //if (count%NB_MAX_ELEMENT_PER_LINE == 0)
        //    ids << "\n";
    }
    element->SetText(ids.str().c_str());
}

}
}
