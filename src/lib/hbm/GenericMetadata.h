/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_GENMETADATA_H
#define PIPER_HBM_GENMETADATA_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "BaseMetadata.h"


namespace piper {
namespace hbm {

class FEModel;

 
 /**
 * @brief Generic Metadata.
 *
 * @author Erwan JOLIVET @date 2016
 */
 class HBM_EXPORT GenericMetadata : public BaseMetadataGroup {
public:

    GenericMetadata();
    GenericMetadata(std::string const& name);
    GenericMetadata(tinyxml2::XMLElement* element);

	/// <param name="femodel">The femodel.</param>
	/// <param name="skip1D">If true, nodes belonging to 1D elements will not be added to the output list.</param>
	/// <param name="skip2D">If true, nodes belonging to 2D elements will not be added to the output list.</param>
	/// <param name="skip3D">If true, nodes belonging to 3D elements will not be added to the output list.</param>
	/// <PARAM NAME="skip nodes">If true, nodes will not be added to the output list.</parma>
	void getGMNodesIds(const FEModel& femodel, VId & vecNodeId, bool skip1D = false, bool skip2D = false, bool skip3D = false, bool skipnodes = false) const;

    /// \returns vector of all nodes id of this generic metadata
    VId getNodeIdVec(const FEModel& fem) const;

    // substring at the end of generic metadata used to denote list of nodes that are the boundary between HBM's domains (for decomposition for kriging)
    static std::string const domainDecompositionBoundary;

protected:

	virtual bool has_groupNode() const {return true;}
	virtual bool has_groupElement1D() const {return true;}
	virtual bool has_groupElement2D() const {return true;}
	virtual bool has_groupElement3D() const {return true;}
	virtual bool has_groupGroup() const {return true;}
};

}
}

#endif // PIPER_HBM_GENMETADATA_H
