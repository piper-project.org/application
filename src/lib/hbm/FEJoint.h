/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_JOINT_H
#define PIPER_HBM_JOINT_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <array>

#include "MeshComponent.h"
#include "IdGenerator.h"
#include "MeshComponentIdName.h"
#include "MeshComponentContainer.h"


namespace piper {
namespace hbm {

HBM_EXPORT enum class JointType {NOTYPE=0, SPHERICAL, REVOLUTE, SIXDOF, SIZE }; // SIZE must be the last element
static const std::array<std::string,4> JointType_str={{"NOTYPE","SPHERICAL", "REVOLUTE","6DOF"}};


/**
* @brief A Joint defined in a FE model.
*
* Container for a joint defined by a vector of nodes that defines its DOF.
*
* - JOINT_SPHERICAL: one node to define its center
* - JOINT_REVOLUTE: two nodes to define its axis
*
* @author Erwan Jolivet @date 2015
*/
class HBM_EXPORT FEJoint : public MeshComponentIdName< SId, FEJoint> {
public :

	FEJoint( );
	explicit FEJoint(tinyxml2::XMLElement* element) {parseXml( element);}
	FEJoint( const std::string& key, const unsigned int& id): MeshComponentIdName< SId, FEJoint>( key, id), m_type(JointType::NOTYPE) {};
	FEJoint( const std::string& key, const std::string& idstr): MeshComponentIdName< SId, FEJoint>( key, idstr), m_type(JointType::NOTYPE) {};
	FEJoint( const std::string& key, const unsigned int& id, const std::string& idstr): MeshComponentIdName< SId, FEJoint>( key, id, idstr), m_type(JointType::NOTYPE) {};
	FEJoint( const std::string& key, const IdVendor& idv): MeshComponentIdName< SId, FEJoint>( key, idv), m_type(JointType::NOTYPE) {};
	//explicit FEJoint( JointType type): m_type( type) {};
	//explicit FEJoint( const Id id);

    /// set joint type
    void setType( const std::string& type);

    JointType getJointType( ) const;

    /// add a new node id for joint definition
    //void addNodeDef( const Id nid );
	void setJointFrameId( const Id& id);
	Id getJointFrameId() const { return *(get().begin());};
	std::array<bool,6> getDoF() const {return  m_dof;}
	void setDoF( std::array<bool,6> dof) {m_dof=dof;};

	void serializeXml(tinyxml2::XMLElement* element) const; 


protected:
    JointType m_type;
    /// TX, TY, TZ, RX, RY, RZ
	 std::array<bool,6> m_dof;

	 void parseXml(tinyxml2::XMLElement* element);

};


class FEJoints : public MeshComponentContainer< FEJoint> {

public:
    FEJoints(): MeshComponentContainer< FEJoint>( "FEJOINTS")  {};
};
}
}

#endif

