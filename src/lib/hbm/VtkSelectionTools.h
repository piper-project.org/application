/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_VTK_SELECTIONTOOLS_H
#define PIPER_VTK_SELECTIONTOOLS_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <vtkExtractSelection.h>
#include <vtkSmartPointer.h>
#include <vtkPointSetAlgorithm.h>
#include <vtkPolygon.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkBitArray.h>
#include <vtkOBBTree.h>

#include <memory>

#include <boost/container/vector.hpp>

#define SELECTED_PRIMITIVES "selected_primitives"// macro for naming the scalar array (vtkBitArray) with boolean flags denoting which nodes/elements are selected (1) and which are not (0).
#define ORIGINAL_CELL_IDS "vtkOriginalCellIds" // name of arrays for mapping indices between parent-child type of meshes (child = subset of parent mesh)
#define ORIGINAL_POINT_IDS "vtkOriginalPointIds"
#define HIT_PIXEL_COUNT "hit_pixel_count" // macro for vtkIntArray cellData that counts how many pixels of the given cells are currently selected - used for area picking
#define SELECTED_POINT_GROUPS "selected_groups_of_points" // macro for vtkBitArray pointData that marks points belonging to a group of points that has at least one selected point

class vtkRenderer;
class vtkIntArray;

namespace piper {

    /// <summary>
    /// Just extends the vtkOBBNode by information about neighboring OBBs
    /// </summary>
    /// <seealso cref="vtkOBBNode" />
    class vtkOBBNodeWNeighbors : public vtkOBBNode
    {
    public:
        vtkOBBNodeWNeighbors() : vtkOBBNode()
        {
        }

        ~vtkOBBNodeWNeighbors() {}
        
        /// <summary>
        /// Computes the coordinates of the four points that make the neighboring face of the box.
        /// </summary>
        /// <param name="neighborIndex">Index of the neighbor (0-5), see faceNeighbors.</param>
        /// <param name="face">Provide a [4][3] array to fill with the x,y,z coordinates of the four points. There is no special order of the four points.</param>
        void GetBoundaryFace(int neighborIndex, double face[4][3])
        {
            // let's compute all the points
            double points[8][3];
            for (int i = 0; i < 3; i++)
            {
                // 0-3 -> corner + second and third axes in counter clockwise orientation, 4-7 the opposite face
                points[0][i] = Corner[i];
                points[1][i] = Corner[i] + Axes[2][i]; 
                points[2][i] = Corner[i] + Axes[1][i] + Axes[2][i];
                points[3][i] = Corner[i] + Axes[1][i];
                for (int j = 4; j < 8; j++)
                    points[j][i] = points[j - 4][i] + Axes[0][i];
            }
            switch (neighborIndex)
            {
            case 0: // neighbor along first axis -> 5, 4, 7, 6
                for (int i = 0; i < 3; i++)
                {
                    face[0][i] = points[5][i];
                    face[1][i] = points[4][i];
                    face[2][i] = points[7][i];
                    face[3][i] = points[6][i];
                }
                break;
            case 1: // against first axis -> 0, 1, 2, 3
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 4; j++)
                        face[j][i] = points[j][i];
                }
                break;
            case 2: // along second axis -> 3, 2, 6, 7
                for (int i = 0; i < 3; i++)
                {
                    face[0][i] = points[3][i];
                    face[1][i] = points[2][i];
                    face[2][i] = points[6][i];
                    face[3][i] = points[7][i];
                }
                break;
            case 3: // against second axis -> 1, 0, 4, 5
                for (int i = 0; i < 3; i++)
                {
                    face[0][i] = points[1][i];
                    face[1][i] = points[0][i];
                    face[2][i] = points[4][i];
                    face[3][i] = points[5][i];
                }
                break;
            case 4: // along third axis -> 1, 5, 6, 2
                for (int i = 0; i < 3; i++)
                {
                    face[0][i] = points[1][i];
                    face[1][i] = points[5][i];
                    face[2][i] = points[6][i];
                    face[3][i] = points[2][i];
                }
                break;
            case 5: // against third axis -> 4, 0, 3, 7
                for (int i = 0; i < 3; i++)
                {
                    face[0][i] = points[4][i];
                    face[1][i] = points[0][i];
                    face[2][i] = points[3][i];
                    face[3][i] = points[7][i];
                }
                break;
            }
        }

        /// <summary>
        /// Creates a vtkPolyData representation of this box.
        /// </summary>
        /// <returns>A cuboid with 8 points at the locations of the endpoints of the box.</returns>
        vtkSmartPointer<vtkPolyData> GenerateAsPolydata()
        {
            vtkSmartPointer<vtkPolyData> box = vtkSmartPointer<vtkPolyData>::New();
            box->Allocate();
            vtkSmartPointer<vtkPoints> boxPoints = vtkSmartPointer<vtkPoints>::New();
            boxPoints->Initialize();
            // let's compute all the points
            double points[8][3];
            for (int i = 0; i < 3; i++)
            {
                // 0-3 -> corner + second and third axes in counter clockwise orientation, 4-7 the opposite face
                points[0][i] = Corner[i];
                points[1][i] = Corner[i] + Axes[2][i];
                points[2][i] = Corner[i] + Axes[1][i] + Axes[2][i];
                points[3][i] = Corner[i] + Axes[1][i];
                for (int j = 4; j < 8; j++)
                    points[j][i] = points[j - 4][i] + Axes[0][i];
            }
            for (int i = 0; i < 8; i++)
                boxPoints->InsertNextPoint(points[i]);
            box->SetPoints(boxPoints);

            vtkSmartPointer<vtkCellArray> faces = vtkSmartPointer<vtkCellArray>::New();
            vtkSmartPointer<vtkIdList> face = vtkSmartPointer<vtkIdList>::New();
            face->SetNumberOfIds(4);
            vtkIdType *raw = face->WritePointer(0, 4);
            // individual faces are:
            //5, 4, 7, 6
            //0, 1, 2, 3
            //3, 2, 6, 7
            //1, 0, 4, 5
            //1, 5, 6, 2
            //4, 0, 3, 7
            raw[0] = 5; raw[1] = 4; raw[2] = 7; raw[3] = 6;
            box->InsertNextCell(VTK_QUAD, face);
            raw[0] = 0; raw[1] = 1; raw[2] = 2; raw[3] = 3;
            box->InsertNextCell(VTK_QUAD, face);      
            raw[0] = 3; raw[1] = 2; raw[2] = 6; raw[3] = 7;
            box->InsertNextCell(VTK_QUAD, face);
            raw[0] = 1; raw[1] = 0; raw[2] = 4; raw[3] = 5;
            box->InsertNextCell(VTK_QUAD, face);
            raw[0] = 1; raw[1] = 5; raw[2] = 6; raw[3] = 2;
            box->InsertNextCell(VTK_QUAD, face);
            raw[0] = 4; raw[1] = 0; raw[2] = 3; raw[3] = 7;
            box->InsertNextCell(VTK_QUAD, face);

            return box;
        }

        // for each face, pointer to the neighbor
        // order by the axes - 0 & 1 are along and against axes[0], 2 & 3 along and against axes[1], 4 & 5 along and against axes[2]
        // BUT be aware that algorithms from VTK that work with box might change the order of the axes but not reorder the neighbors!
        std::vector<std::shared_ptr<vtkOBBNodeWNeighbors>> faceNeighbors[6];

        vtkIdType ID; // identifier of this box. this is not used or maintained in any way inside this class - use it as you see fit
    };

    typedef std::shared_ptr<vtkOBBNodeWNeighbors> vtkOBBNodePtr;

    const int IS_PRIMITIVE_SELECTED = 1;
    const int IS_NOT_PRIMITIVE_SELECTED = 0;
    const double DEF_CLUSTER_SPLIT_DISTANCE = 0.02;
    const unsigned int DEF_MIN_CLUSTER_SIZE = 10;

    namespace hbm {
        /// <summary>
        /// Defines what tool to use for the selection
        /// </summary>
        enum class HBM_EXPORT SELECTION_TOOL
        {
            NONE, // use to not perform any selectino - useful if you want to only blank
            OBB, // will select everything inside a specified OBB
            PLANE // will select everything on the positive side of a specified plane
        };

        /// Defines what type of primitives to take into account for selection.
        /// Use bitwise "or" to combine selection modes or "and" to mask them.
        /// E.g. (SELECTION_TARGET::NODES | SELECTION_TARGET::FACES) will activate selection of both faces and vertices.
        enum class HBM_EXPORT SELECTION_TARGET
        {
            NONE = 0, // turn off selection
            NODES = 1, // nodes will be marked as selected
            FACES = 2, // faces will be marked as selected - for 2D elements is equal to selecting ELEMENTS, for 3D elements, this can select only a part of the element
            ELEMENTS = 4, // whole elements will be marked - for 2D this is equal to selecting FACES
            ENTITIES = 8, // processes an ELEMENTS selection and then marks each whole entity that includes at least one of the selected elements
            FREE_POINTS = 16, // points that are not tied to any other entity - they are an object on their own and can be anywhere. useful for landmarks
            CREATE_POINTS = 64 // used to create points on the selected coordinates
        };

        HBM_EXPORT inline unsigned int operator|(SELECTION_TARGET a, SELECTION_TARGET b)
        {
            return static_cast<unsigned int>(static_cast<unsigned int>(a) | static_cast<unsigned int>(b));
        }

        HBM_EXPORT inline unsigned int operator&(SELECTION_TARGET a, SELECTION_TARGET b)
        {
            return static_cast<unsigned int>(static_cast<unsigned int>(a)& static_cast<unsigned int>(b));
        }

        HBM_EXPORT inline unsigned int operator&(unsigned int a, SELECTION_TARGET b)
        {
            return static_cast<unsigned int>(static_cast<unsigned int>(a)& static_cast<unsigned int>(b));
        }

        HBM_EXPORT inline bool operator==(unsigned int a, SELECTION_TARGET b)
        {
            return static_cast<int>(a) == static_cast<int>(b);
        }

        HBM_EXPORT inline bool operator!=(unsigned int a, SELECTION_TARGET b)
        {
            return static_cast<int>(a) != static_cast<int>(b);
        }

        HBM_EXPORT inline unsigned int operator^(unsigned int a, SELECTION_TARGET b)
        {
            return static_cast<unsigned int>(static_cast<int>(a) ^ static_cast<int>(b));
        }

        /*
        * A compilation of tools for selecting a subset of primitives from a vtk mesh.
        * A primitive in this context can be a point, cell, landmark, frame etc. etc. - any part of the mesh that can be visualized.
        *
        * @author Tomas Janak
        * @date 2016
        */
        class HBM_EXPORT vtkSelectionTools : public vtkPointSetAlgorithm
        {
        public:

            static vtkSelectionTools *New();
            vtkTypeMacro(vtkSelectionTools, vtkPointSetAlgorithm);

            /// <summary>
            /// Sets what kind of primitives should be selected.
            /// </summary>
            /// <param name="type">The type. You can use bitwise operations to set more types at once.</param>
            void SetSelectionTargetType(SELECTION_TARGET type) { this->targetType = (unsigned int)type; }
            
            /// <summary>
            /// Sets whether the selector should select (default) or deselect.
            /// </summary>
            /// <param name="inverse">If set to <c>true</c>, primitives inside the selection will be deselected instead of selected.</param>
            void SetInverseSelection(bool inverse) { this->p_inverseSelection = inverse; }

            /// <summary>
            /// Sets what kind of primitives should be selected.
            /// </summary>
            /// <param name="type">The type. You can use bitwise operations to set more types at once.</param>
            void SetSelectionTargetType(unsigned int type) { this->targetType = type; }
            
            /// <summary>
            /// If set, an output will be created that contains only those cells or points of the input data set that were not selected.
            /// Because there can be only one output mesh, only the first input mesh will be considered. If you need to do a section of 
            /// multiple meshes, do the selection for each of them separately (changing the input / using a different instance of vtkSelectionTools).
            /// </summary>
            /// <param name="blankSelected">If set to <c>true</c>, an output mesh that contains only cells/points that are NOT selected will be created.</param>
            /// <param name="keepPoints">If set to <c>true</c>, the output mesh will share the points array with the original mesh, i.e. the points
            /// array will use the same pointer. This mode is faster and uses less memory, but might be undesirable for some cases.</param>
            void SetBlankSelected(bool blankSelected, bool keepPoints) { this->p_blankSelected = blankSelected; this->p_keepPoints = keepPoints; }

            /// <summary>
            /// Clears all the selection results.
            /// </summary>
            /// <param name="alsoResetInData">If set to <c>true</c>, all the data connected to 
            /// all the previous selection querries will be have their "selected" flags reset,
            /// i.e. all nodes and elements will be marked as not selected. False by default.</param>
            void ResetResults(bool alsoResetData = false);
            
            /// <summary>
            /// Resets this selection tools to default values - SELECTION_TOOL::NONE, SELECTION_TARGET::NONE,
            /// inverse selection off, blanking off.
            /// </summary>
            void Reset();
            
            /// <summary>
            /// Sets the selection mode of the selection tool to selects everything of the input data that is inside of a given box. 
            /// Results will be written to the SELECTED_PRIMITIVES scalar arrays. DOES NOT PUT ANYTHING TO THE OUTPUT PORT!
            /// Note that this method alone does not run the computation, you have to make sure that the filter is updated to get the results (e.g. by calling Update()).
            /// </summary>
            /// <param name="origin">An arbitrary point of the box to use as the reference point.</param>
            /// <param name="axes">Axis vectors, each effectively representing an endpoint (origin+axes[i]=endpoint) of one of the edges connected to origin.</param>
            void UseSelectByOrientedBox(double origin[3], double axes[3][3]);


            /// <summary>
            /// Sets the selection mode of the selection tool to selects the portion of the mesh that is above a specified plane.
            /// The selected cells will be marked in the input's SELECTED_PRIMITIVES array.
            /// If the selection target is set to elements/faces, only elements that are FULLY above the plane will be selected.
            /// If the selection target is set to nodes, only elements that have all points above the plane will be in the output.
            /// Simply - this method will NOT cut any cells - if they are not fully above the plane, they will not be selected.
            /// If selection target is not elements, faces or nodes, nothing will be selected.
            /// Note that this method alone does not run the computation, you have to make sure that the filter is updated to get the results (e.g. by calling Update()).
            /// </summary>
            /// <param name="pointInPlane">An arbitrary point in the plane.</param>
            /// <param name="planeNormal">The plane normal.</param>
            void UseSelectByPlane(double pointInPlane[3], double planeNormal[3]);
            
            /// <summary>
            /// For each cell in the dataset that is selected (based on the SELECTED_PRIMITIVES CellData array),
            /// selects all of it's points (marks them in SELECTED_PRIMITIVES PointData array).
            /// </summary>
            /// <param name="data">The dataset. Will modify the SELECTED_PRIMITIVES PointData array, but not reset it,
            /// i.e. if something was selected there before, it will remain selected.</param>
            /// <param name="inverse">If set to <c>true</c>, the points will be deselected instead.</param>
            static void SelectPointsByCells(vtkSmartPointer<vtkPointSet> data, bool inverse);

            /// <summary>
            /// For each point in the dataset that is selected (based on the SELECTED_PRIMITIVES PointData array),
            /// selects all cells that are made by this point (marks them in SELECTED_PRIMITIVES CellData array).
            /// </summary>
            /// <param name="data">The dataset. Will modify the SELECTED_PRIMITIVES CellData array, but not reset it,
            /// i.e. if something was selected there before, it will remain selected.</param>
            static void SelectCellsByPoints(vtkSmartPointer<vtkPointSet> data);
            
            /// <summary>
            /// Selects cells of a specified data set based on values of another scalar array.
            /// </summary>
            /// <param name="data">The data containing the scalar array. The SELECTED_PRIMITIVES CellData array of this dataset will be modified.</param>
            /// <param name="scalarArrayName">Name of the scalar array based on which to select. The values of this array will be treated as doubles.
            /// If it does not exist, nothing will happen.</param>
            /// <param name="selectionRanges">Each entry is a range, first number is a minimum value, second is maximum.
            /// Only cells that have scalar values within the ranges in this vector will be selected.</param>
            /// <param name="reset">If set to <c>true</c>, the SELECTED_PRIMITIVES CellData array of the <c>data</c> will be reset prior to the selection,
            /// otherwise it will be kept intact, i.e. what was selected before will remain selected.</param>
            static void SelectCellsByScalars(vtkSmartPointer<vtkPointSet> data, std::string scalarArrayName,
                boost::container::vector <std::pair<double, double>> *selectionRanges, bool reset);


            /// <summary>
            /// Builds unstructured grid(s) from selected elements in the main provided dataset. Each such grid will have
            /// the ORIGINAL_CELL_IDS and ORIGINAL_POINT_IDS cellData/pointData arrays with indices into the original data.
            /// </summary>
            /// <param name="data">The data with the selected elements. Will not be modifid in any way. The SELECTED_PRIMITIVES will be used to determine what is selected.</param>
            static std::vector<vtkSmartPointer<vtkUnstructuredGrid>> ExtractSelectedParts(vtkSmartPointer<vtkPointSet> data);
                        
            /// <summary>
            /// Separates the specified points into clusters by recursively splitting oriented bounding boxes in the middle.
            /// The two split children are considered different clusters if the distance between two closest points from each of them
            /// is larger than <c>splitDistanceRatio * length of the bounding box edge along which it was split</c>.
            /// </summary>
            /// <param name="points">The points to assign to clusters.</param>
            /// <param name="splitDistanceRatio">A constant defining minimal distance (relative to the size of the bounding box of the points)
            /// that is considered significant enough to do the split. 
            /// Values should be between 0-1 - if it is more than 1 = no split will ever be done, 
            /// if it is less than 0 = boxes will always be split until only 1 point remains in the cluster.</param>
            /// <param name="points">Minimal amount of points in a cluster - below this number the cluster won't be further subdivided.</param>
            /// <returns>Array of clusters - each cluster is an array of points in that cluster.</returns>
            static boost::container::vector<vtkSmartPointer<vtkPoints>> CreateSpatialClusters(vtkSmartPointer<vtkPoints> points, 
                double splitDistanceRatio = DEF_CLUSTER_SPLIT_DISTANCE, int minClusterSize = DEF_MIN_CLUSTER_SIZE);

            /// <summary>
            /// For a given list of meshes, creates an OBB for each of them and optionally merges those OBBs.
            /// </summary>
            /// <param name="meshes">List of meshes for which to compute the OBBs.</param>
            /// <param name="mergeOverlapping">If true, overlapping OBBs will be (recursively) merged.</param>
            /// <returns>List of pointers to OBB nodes representing the OBBs.</returns>
            boost::container::vector<vtkOBBNodePtr> GenerateOBBs(boost::container::vector<vtkSmartPointer<vtkUnstructuredGrid>> meshes,
                bool mergeOverlapping);

            /// <summary>
            /// Merges all overlapping oriented bounding boxes in a given list.
            /// </summary>
            /// <param name="OBBNodes">The obb nodes to merge. Note that this list will be modified, if you want to keep the
            /// boxes before merging as well, make a copy before calling this method.</param>
            static void MergeOverlappingBoxes(boost::container::vector<vtkOBBNodePtr> &OBBNodes);

            /// <summary>
            /// Splits the specified OBB recursively by the longest axis until none of the newly created OBBs
            /// have more than the specified amount of points inside of it.
            /// </summary>
            /// <param name="OBB">The initial OBB to split.</param>
            /// <param name="data">The data containing the points this OBB is build upon.</param>
            /// <param name="pointIndicesAndLimits">Indices into the data's points array of points that are inside this OBB together
            /// with a number of maximum points from this set of points to allow inside a box. There can be multiple such sets, hence
            /// the usage of vector, but in general, most often there will be only one entry in the vector.</param>
            /// <param name="overlap">A percentage (0-1) by which the boxes should overlap after splitting. E.g. setting it to 0.25
            /// will make the split in the middle, but then add 25% of length beyond the splitplane to each of the two newly created boxes. Default 0 (i.e. exact split).</param>
            /// <returns>A list of new OBBs together with the list of indices of points that are inside that OBB for each of the sets defined in pointIndicesAndLimits.</returns>
            boost::container::vector<std::pair<vtkOBBNodePtr, boost::container::vector<vtkSmartPointer<vtkIntArray>>>> SplitOBBRecursivelyByPoints
                (vtkOBBNodePtr OBB, vtkSmartPointer<vtkPoints> data, boost::container::vector<std::pair<vtkSmartPointer<vtkIntArray>, int>> pointIndicesAndLimits, double overlap = 0);
            
            /// <summary>
            /// Splits the oriented bounding box (OBB) into two halves along the specified axis.
            /// The axes of the new boxes are reordered in such a way to have the longest axis at index 0, second longest at 1 and shortest at 3.
            /// </summary>
            /// <param name="splitAxisIndex">Index of the split axis.</param>
            /// <param name="parent">The OBB to split.</param>
            /// <param name="firstChild">Upon return, this will contain the child OBB on the positive side of the split axis,
            /// i.e. its corner will be in the middle of the original box.</param>
            /// <param name="secondChild">Upon return, this will contain the child OBB on the negative side of the spli axis,
            /// i.e. its corner will be equal to the parent box' corner.</param>
            /// <param name="overlap">A percentage (0-1) by which the boxes should overlap after splitting. E.g. setting it to 0.25
            /// will make the split in the middle, but then add 25% of length beyond the splitplane to each of the two newly created boxes. 0 = exact split.</param>
            /// <returns>The index into the Axes array of the created children of the axis used as splitAxis after reordering.</returns>
            static int SplitOBB(int splitAxisIndex, vtkOBBNodePtr parent, vtkOBBNodePtr firstChild, vtkOBBNodePtr secondChild, double overlap);


            /// <summary>
            /// For a specified dataset, obtains the selection array (SELECTED_PRIMITIVES) of cells.
            /// If the dataset does not have such array, it will be created, initialized (all cells not selected),
            /// assigned to the data set and the pointer will be returned.
            /// </summary>
            /// <param name="data">The dataset for which to get the array.</param>
            /// <param name="reset">If true, the array will be reset for all primitives to be not selected.
            /// Note that if the array does not exist, it will always be reset on initialization to not-selected for all primitives.</param>
            /// <returns>A pointer to the array. Unless the data is NULL, this array will always be
            /// initialized and have the proper size and name and will be linked to the dataset.</returns>
            static vtkSmartPointer<vtkBitArray> ObtainSelectionArrayCells(vtkPointSet *data, bool reset);

            /// <summary>
            /// For a specified dataset, obtains the selection array (SELECTED_PRIMITIVES) of points.
            /// If the dataset does not have such array, it will be created, initialized (all points not selected),
            /// assigned to the data set and the pointer will be returned.
            /// </summary>
            /// <param name="data">The dataset for which to get the array.</param>
            /// <param name="reset">If true, the array will be reset for all primitives to be not selected.
            /// Note that if the array does not exist, it will always be reset on initialization to not-selected for all primitives.</param>
            /// <returns>A pointer to the array. Unless the data is NULL, this array will always be
            /// initialized and have the proper size and name and will be linked to the dataset.</returns>
            static vtkSmartPointer<vtkBitArray> ObtainSelectionArrayPoints(vtkPointSet *data, bool reset);

            /// <summary>
            /// Obtains the array counting how many pixels on a given cells are currently selected - useful for area picking.
            /// </summary>
            /// <param name="data">The dataset for which to get the array.</param>
            /// <param name="reset">If true, the array will be reset for all primitives to be not selected.
            /// Note that if the array does not exist, it will always be reset on initialization to not-selected for all primitives.</param>
            /// <returns>A pointer to the array. Unless the data is NULL, this array will always be
            /// initialized and have the proper size and name and will be linked to the dataset.</returns>
            static vtkSmartPointer<vtkIntArray> ObtainCellPixelHitArray(vtkPointSet *data, bool reset);
            
            /// <summary>
            /// Obtains the array that marks as selected all points that belong to the same group of points as at least one selected point (marked in the standard SelectionArray).
            /// </summary>
            /// <param name="data">The dataset for which to get the array.</param>
            /// <param name="reset">If true, the array will be reset for all primitives to be not selected.
            /// Note that if the array does not exist, it will always be reset on initialization to not-selected for all primitives.</param>
            /// <returns>A pointer to the array. Unless the data is NULL, this array will always be
            /// initialized and have the proper size and name and will be linked to the dataset.</returns>
            static vtkSmartPointer<vtkBitArray> ObtainArrayHighlightedGroupOfPoints(vtkPointSet *data, bool reset);


            /// <summary>
            /// Culls all points that are "above" a given plane (i.e. normal points "inside" the culled points).
            /// Points that are exactly on the boundary are treated are not culled.
            /// </summary>
            /// <param name="normal">The normal of the plane.</param>
            /// <param name="pointInPlane">Any point in the plane.</param>
            /// <param name="data">The points on which to perform the culling.</param>
            /// <param name="clipMask">An array of indices that should be checked for culling. Useful for reentrant culling by more cliplanes
            /// - you can set clipMask = to the result of the previous cullPointsByClipPlane call. If you are clipping by only one plane,
            /// leave clipMask at the default value = 0, this way all points in "data" will be checked for culling.</param>
            /// <returns>Vector of point IDs that are NOT culled.</returns>
            vtkSmartPointer<vtkIntArray> CullPointsByClipPlane(double normal[3], double pointInPlane[3], vtkPoints *data, vtkSmartPointer<vtkIntArray> clipMask = 0);

            /// <summary>
            /// NOT AN EXACT CLIPPING.
            /// Only culls all cells that have ALL vertices "above" a given plane (i.e. normal points in the direction of the points that create the cell).
            /// Points that are exactly on the boundary are treated are not culled.
            /// </summary>
            /// <param name="normal">The normal of the clipping plane.</param>
            /// <param name="pointInPlane">Any point in the plane.</param>
            /// <param name="data">The data.</param>
            /// <param name="clipMask">An array of indices that should be checked for culling. Useful for reentrant culling by more cliplanes
            /// - you can set clipMask = to the result of the previous cullCellsByClipPlane call. If you are clipping by only one plane,
            /// leave clipMask at the default value = 0, this way all points in "data" will be checked for culling.</param>
            /// <returns>Vector of cell IDs that are NOT culled.</returns>
            vtkSmartPointer<vtkIntArray> CullCellsByClipPlane(double normal[3], double pointInPlane[3], vtkPointSet *data, vtkSmartPointer<vtkIntArray> clipMask = 0);

            /// <summary>
            /// Projects a specified screen-space point into the scene based on the current renderer and computes the endpoints of the resulting ray.
            /// Copy-pasted from vtkPicker.
            /// </summary>
            /// <param name="selectionX">The X-coordinate of the selected point on the screen.</param>
            /// <param name="selectionY">The Y-coordinate of the selected point on the screen.</param>
            /// <param name="useParallelProjection">Specify whether parallel (<c>true</c>) or perspective (<c>false</c>) projection should be used.</param>
            /// <param name="renderer">The renderer.</param>
            /// <param name="ray1World">After the computation is done this will contain the world coordinates of the ray endpoint that is on the near clipping plane.</param>
            /// <param name="ray2World">After the computation is done this will contain the world coordinates of the ray endpoint that is on the far clipping plane.</param>
            /// <returns><c>False</c> in case of some failure - renderer being NULL or numerical failure.</returns>
            static bool ComputePointInScreenRay(double selectionX, double selectionY, bool useParallelProjection, vtkRenderer *renderer, double ray1World[3], double ray2World[3]);
            
            /// <summary>
            /// Computes intersection of a ray and a plane defined by the specified cell.
            /// </summary>
            /// <param name="data">The dataset containing the cell in questino.</param>
            /// <param name="cellID">The cell identifier.</param>
            /// <param name="ray1World">Coordinates of one endpoint of the ray.</param>
            /// <param name="ray2World">Coordinates of the second endpoint.</param>
            /// <param name="intersection">Upon returning, this will contain the intersection.</param>
            /// <returns><c>True</c> if the intersection was computed - it isn't if the specified cell is not planar.</returns>
            static bool ComputeCellRayIntersection(vtkPointSet *data, vtkIdType cellID, double ray1World[3], double ray2World[3], double intersection[3]);

            /// <summary>
            /// For a given cell in a specified dataset, finds the closest of its vertices to a given point.
            /// </summary>
            /// <param name="data">The dataset containing the cell in questino.</param>
            /// <param name="cellID">The cell identifier.</param>
            /// <param name="searchPoint">Coordinates of the point to which find the closest one.</param>
            /// <returns>Index of the dataset point that is closest to the point.</returns>
            static vtkIdType FindClosestCellPointToPoint(vtkPointSet *data, vtkIdType cellID, double searchPoint[3]);
            
            /// <summary>
            /// Splits the specified mesh into two based on a provided list of vertices.
            /// It is assumed that the vertices form an enclosed ring around the polydata, i.e. form one connected polyline made of cell edges.
            /// The edges and vertices on the splitting ring are duplicated and the newly created hole is closed 
            /// by creating one new vertex for each of the two parts approximately in the middle of the hole and connecting it with each of the ring vertices.
            /// The algorithm assumes that the input mesh is manifold and has only one component - it traverses all cells and vertices connected to the ring and adds them to one
            /// or the other output, but any additional unconnected cells will be left out.
            /// </summary>
            /// <param name="data">The data to split.</param>
            /// <param name="ring">The indices of vertices to use for splitting. Can be in a random order, but most form a closed polyline made of mesh's edges.</param>
            /// <param name="extendRing">If set to <c>true</c>, elements that are around the ring and all their points will be added to both partitions,
            /// effecitively creating a two cells wide overlap between the partitions.</param>
            /// <returns>Two polydata, whose union is the input data. The cell and pointData vtkIdTypeArrays ORIGINAL_CELL_IDS and ORIGINAL_POINT_IDS
            /// are added to each of them for mapping to the input mesh. 
            /// WARNING - the newly created vertex and cells that fill the hole have these values mapped to -1, be sure to check for this when traversing these arrays.
            /// Returns pair of nullptr if the decomposition was not found - happens in case the ring is not correctly defined or if the mesh is not manifold.</returns>
            static std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>> SplitMeshByVertexRing(
                vtkSmartPointer<vtkPolyData> data, vtkSmartPointer<vtkIdList> ring, bool extendRing);


            /// <summary>
            /// Computes the normals (and normalizes them) for a given vtkPolyData mesh and add them as cell data array "Normals" (accessible by getNormals())
            /// Strangely enough, there is apparently no such functionality inside vtk (vtkPolyDataNormals creates a new polydata instance
            /// and does a lot of other useless things we do not care about).
            /// If anybody feels like you can use it and want to move this function to some more easily accesible location, feel free to do so.
            /// </summary>
            /// <param name="mesh">The mesh for which to compute normals without altering anything about it (except for adding the normals)</param>
            static void computeNormalizedNormalsInPlace(vtkSmartPointer<vtkPolyData> mesh)
            {
                vtkSmartPointer<vtkDoubleArray> normals = vtkSmartPointer<vtkDoubleArray>::New();
                normals->SetNumberOfComponents(3); // x,y,z for each normal
                normals->SetNumberOfTuples(mesh->GetNumberOfCells()); // normal for each element
                double normal[3];
                for (vtkIdType i = 0; i < mesh->GetNumberOfCells(); i++)
                {
                    vtkCell *curCell = mesh->GetCell(i);
                    vtkPolygon::ComputeNormal(mesh->GetPoints(), curCell->GetNumberOfPoints(),
                        curCell->GetPointIds()->GetPointer(0), normal); // computes normalized normal
                    normals->SetTuple3(i, normal[0], normal[1], normal[2]);
                }
                normals->SetName("Normals");
                mesh->GetCellData()->SetNormals(normals);
            }

        protected:

            vtkSelectionTools();
            ~vtkSelectionTools();


            // Description:
            // This is called by the superclass.
            // This is the method you should override.
            /*	int RequestDataObject(vtkInformation* request,
                    vtkInformationVector** inputVector,
                    vtkInformationVector* outputVector);*/

            // Description:
            // This is called by the superclass.
            // This is the method you should override.
            int RequestData(vtkInformation*,
                vtkInformationVector**,
                vtkInformationVector*);

            /// <summary>
            /// Performs the selection inside of a given box. See UseSelectByOrientedBox for details.
            /// </summary>
            /// <param name="origin">An arbitrary point of the box to use as the reference point.</param>
            /// <param name="axes">Axis vectors, each effectively representing an endpoint (origin+axes[i]=endpoint) of one of the edges connected to origin.</param>
            /// <seealso cref="UseSelectByOrientedBox" />
            void selectByOrientedBox();


            /// <summary>
            /// Performs the selection by plane. See UseSelectBySection for details.
            /// </summary>
            /// <seealso cref="UseSelectByPlane" />
            void selectByPlane();

            /// <summary>
            /// Fills the SELECTED_PRIMITIVES point data array according to the indices array.
            /// All points with IDs listed in the indices array will be marked as selected.
            /// If the SELECTED_PRIMITIVES point data array is not created, this method creates it.
            /// </summary>
            /// <param name="data">The dataset which has the points to be selected.</param>
            /// <param name="indices">The indices of the selected points that should be marked.</param>
            void markPointsAsSelected(vtkPointSet *data, vtkIntArray *indices);

            /// <summary>
            /// Fills the SELECTED_PRIMITIVES cell data array according to the indices array.
            /// All cells with IDs listed in the indices array will be marked as selected.
            /// If the SELECTED_PRIMITIVES cell data array is not created, this method creates it.
            /// </summary>
            /// <param name="data">The dataset which has the cells to be selected.</param>
            /// <param name="indices">The indices of the selected cells that should be marked.</param>
            void markCellsAsSelected(vtkPointSet *data, vtkIntArray *indices);
            
            /// <summary>
            /// The output dataset will be created by blanking selected cells or points(based on the target type) of the input dataset.
            /// </summary>
            /// <param name="output">The output containing the blanked mesh.</param>
            void blankSelected(vtkSmartPointer<vtkUnstructuredGrid> output);

            /// <summary>
            /// The output dataset will be created by blanking selected cells or points(based on the target type) of the input dataset.
            /// The output's point array will point to the array of the input, hence no points pedigree ID will be created, only for cells.
            /// All pointData arrays will be shared as well.
            /// </summary>
            /// <param name="output">The output containing the blanked mesh.</param>
            void blankSelectedKeepPoints(vtkSmartPointer<vtkUnstructuredGrid> output);

            /// <summary>
            /// Check each edge of a given cell for intersection with a given OBB.
            /// </summary>
            /// <param name="data">The dataset containing the cell.</param>
            /// <param name="cellID">The cell identifier.</param>
            /// <param name="OBB">The oriented bounding box against which to check for intersection. The OBB MUST be 
            /// made of six VTK_QUAD cells, otherwise expect a crash.</param>
            /// <returns>True in case of intersection, false in case of no intersection</returns>
            bool intersectsCellEdgesWithOBB(vtkPointSet *data, int cellID, vtkPolyData *OBB);
            
            /// <summary>
            /// Sorts the provided list of vertices that form a closed polyline according to their neighborhood.
            /// </summary>
            /// <param name="data">The data containing the mesh to which the ring belongs.</param>
            /// <param name="ring">Indices of vertices forming a closed polyline.</param>
            /// <param name="isRing">A map of the size of all points in the data, -1 for points that are on the ring, the index in the ring list if it is.</param>
            /// <param name="sortedRing">Sorted indices of the ring so that vertex at index <c>i</c> has edge-neighbors <c>i - 1</c> and <c>i + 1</c>.</param>
            /// <returns><c>False</c> in case of failure - ring must be closed, manifold.</returns>
            static bool SortVertexRing(vtkSmartPointer<vtkPolyData> data, vtkSmartPointer<vtkIdList> ring,
                std::vector<vtkIdType> &isRing, std::vector<vtkIdType> &sortedRing);

            vtkSmartPointer<vtkExtractSelection> output; // a concatenation of results of all the result querries since the last ResetResults() call

            // input parameters
            SELECTION_TOOL currentTool;
            unsigned int targetType; // type of the primitives that the user wants to select; see SELECTION_TARGET enum for details
            bool p_inverseSelection; // if set to true, things will be deselected instead of selected
            bool p_blankSelected; // if set to true, the output will be created by blanking selected cells of the input
            bool p_keepPoints; // if set to true, the output dataset will share the points array with the input dataset
            double planeNormal[3];
            double pointInPlane[3];
            vtkOBBNodePtr OBB;
        };
    }
}

#endif // PIPER_VTK_SELECTIONTOOLS_H
