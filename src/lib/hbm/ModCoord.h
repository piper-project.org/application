/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MODCOORD_H
#define MODCOORD_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <iostream>
#include <unordered_map>
#include <vector>
#include "Node.h"

namespace piper {
	namespace hbm {


		//!  ModCoord Class. 
		/*!
		ModCoord : backup of points
		Use to store the backup coordinates so that user can switch to earlier state
		User can discard the modified state and can go back to the original state
		depending upon the boolean modified the member variables of this class can store the coordinates of the modified state or original state
		*/

		class HBM_EXPORT ModCoord  {
		public:
			
			//! A constructor.
			ModCoord();

			//! A constructor.
			/*!
			A contour constructor for backup coordinates.
			*/
			ModCoord(double a, double b, double c, int id, bool mod = false);
			double modx;	/*!< x coordinate of the state */ 
			double mody;	/*!< y coordinate of the state */  
			double modz;	/*!< z coordinate of the stae */ 
			int mpiperid;	/*!< ID of the Node whose coordinates are stored to keep track of the Node */ 
			bool modified;	/*!< depending upon this boolean the object of this class can store original or modified state coordinates */
			void backupCoord(std::set<Id>);
			
			
	};
	



//		backup_cord();
//		swap_cord();
//		check_cord();
	}

}
#endif

