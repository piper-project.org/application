// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
%module hbm

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "std_string.i"
%include "std_list.i"
%include "std_vector.i"
// %include "std_array.i" // not yet available in swig
%include "std_set.i"
%include "std_map.i"
%include "std_pair.i"
%include "std_vector.i"
%include "eigen.i"

%nodefaultctor piper::hbm::BaseMetadataGroup;


%{
#include "types.h"
#include "IdRegistrator.h"
#include "HumanBodyModel.h"
#include "Metadata.h"
#include "FEModel.h"
#include "Element.h"
#include "FEModelVTK.h"
#include "BaseMetadata.h"
#include "Entity.h"
#include "EntityJoint.h"
#include "EntityContact.h"
#include "Landmark.h"
#include "AnthropoMetadata.h"
#include "EnvironmentModels.h"
#include "units/units.h"
#include "target.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <vector>
%}

%inline %{
namespace piper {
	namespace units {
		template <typename UnitType>
		std::string unitToStr(UnitType unit) {
			return units::UnitTraits<UnitType>::unitToStr(unit);
		}
	}
}
%}

namespace piper {
	namespace units {
	enum class Length {m, dm, cm, mm};

	enum class Mass { kg, hg, g, dg, cg, mg };
	
    enum class Age { year, month };
    }

	namespace hbm {

		typedef unsigned int Id;
		typedef std::set<Id> SId;
		typedef std::vector<Id> VId;
		typedef VId ElemDef;
		typedef Eigen::Vector3d Coord;

		enum  ElementType {ELT_HEXA, ELT_TETRA, ELT_PENTA, ELT_PYRA, ELT_QUAD, ELT_QUAD_THICK, ELT_TRI, ELT_TRI_THICK, ELT_BAR, ELT_UNDEFINED};

		template<typename T>
		class TargetUnit {
			public:
				T const&  unit() const;
				void setUnit(T const& unit);
		};

	} // namespace hbm
} // namespace piper

%template(EntityCont) std::map<std::string, piper::hbm::Entity>;
%template(JointCont)  std::map<std::string, piper::hbm::EntityJoint>;
%template(AnatomicalJointCont)  std::map<std::string, piper::hbm::AnatomicalJoint>;
%template(ContactCont) std::map<std::string, piper::hbm::EntityContact>;
%template(SId) std::set<piper::hbm::Id>;
%template(VId) std::vector<piper::hbm::Id>;
%template(MapIdUInt) std::map<piper::hbm::Id, unsigned int>;
%template(ElemDef) std::vector<piper::hbm::VId>;
%template(Vecd) std::vector<double>;
%template(Vecb) std::vector<bool>;
%template(Vecstr) std::vector<std::string>;
%template(InteractionControlPointCont) std::map<std::string, piper::hbm::InteractionControlPoint>;
%template(LandmarkCont) std::map<std::string, piper::hbm::Landmark>;
%template(GenericMetadataCont) std::map<std::string, piper::hbm::GenericMetadata>;
%template(EntityElementsMap) std::map<std::string, piper::hbm::VId>;
%template(IdPair) std::pair<std::string, piper::hbm::IdKey>;
%template(IdPairMap) std::map<piper::hbm::Id, std::pair<std::string, piper::hbm::IdKey>>;


// target
%template(RigidTransformationTargetValueCont) std::map<unsigned int, double>;
%template(FixedBoneTargetCont) std::list<piper::hbm::FixedBoneTarget>;
%template(LandmarkTargetCont) std::list<piper::hbm::LandmarkTarget>;
%template(JointTargetCont) std::list<piper::hbm::JointTarget>;
%template(FrameToFrameTargetCont) std::list<piper::hbm::FrameToFrameTarget>;
%template(AgeTargetCont) std::list<piper::hbm::AgeTarget>;
%template(ControlPointCont) std::list<piper::hbm::ControlPoint>;
%template(WeightTargetCont) std::list<piper::hbm::WeightTarget>;
%template(HeightTargetCont) std::list<piper::hbm::HeightTarget>;


%nodefaultctor TargetUnit;
%nodefaultdtor TargetUnit;
typedef piper::units::TargetUnit<piper::units::Length> TargetLengthUnit;
%template(TargetLengthUnit) piper::hbm::TargetUnit<piper::units::Length>;
%template(unitToStr) piper::units::unitToStr<piper::units::Length>;
%template(unitToStrAge) piper::units::unitToStr<piper::units::Age>;
%template(unitToStrMass) piper::units::unitToStr<piper::units::Mass>;
%nodefaultctor TargetAgeUnit;
%nodefaultdtor TargetAgeUnit;
typedef piper::units::TargetUnit<piper::units::Length> TargetAgeUnit;
%template(TargetAgeUnit) piper::hbm::TargetUnit<piper::units::Age>;
%nodefaultctor TargetWeightUnit;
%nodefaultdtor TargetWeightUnit;
typedef piper::units::TargetUnit<piper::units::Length> TargetWeightUnit;
%template(TargetWeightUnit) piper::hbm::TargetUnit<piper::units::Mass>;


namespace piper {

	namespace hbm {

		%eigen_typemaps(piper::hbm::Coord)

		const piper::hbm::Id ID_UNDEFINED;

		%nodefaultctor HistoryManager;
		%nodefaultdtor HistoryManager;

		class HistoryManager {
		public:
			std::vector<std::string> const getHistoryList() const;
			bool isHistory(std::string const& stringID) const;
			std::string const& getCurrent() const;
			void addNewHistory(std::string const& stringID);
			void setCurrentModel(std::string const& stringID);
            std::string getValidHistoryName(std::string const& stringID);
		};

		%nodefaultctor HumanBodyModel;
		%nodefaultdtor HumanBodyModel;

		class HumanBodyModel {
		public:

			piper::hbm::Metadata& metadata();
			piper::hbm::FEModel& fem();
			HistoryManager& history();
			bool empty() const;

		};

		%nodefaultctor BaseMetadata;
		%nodefaultdtor BaseMetadata;

		class BaseMetadata {
		public:
			std::string const& name() const;    
		};
  
		%nodefaultctor BaseMetadataGroup;
		%nodefaultdtor BaseMetadataGroup;

		class BaseMetadataGroup : public BaseMetadata {
  
		};
  
		class Entity : public BaseMetadataGroup {
		public:

			std::vector<double> getOneVertex(piper::hbm::FEModel const* fem) const;
			piper::hbm::VId const& getEnvelopNodes(piper::hbm::FEModel const& femodel) const;
			piper::hbm::VId getEntityNodesIds(const piper::hbm::FEModel& femodel, bool skip1D = false, bool skip2D = false, bool skip3D = false) const;
			std::map<piper::hbm::Id, unsigned int> const& getEnvelopNodesToIndexMap(piper::hbm::FEModel const& fem) const;
			std::vector<piper::hbm::ElemDef> const& getEnvelopElements(piper::hbm::FEModel const& femodel) const;

			piper::hbm::VId get_groupElement1D();
			piper::hbm::VId get_groupElement2D();
			piper::hbm::VId get_groupElement3D();
			piper::hbm::VId get_groupGroup();
		};

        class BaseJointMetadata: public BaseMetadata {
        public:
            enum class ConstrainedDofType {HARD, SOFT};
            ConstrainedDofType getConstrainedDofType() const;
            // std::array<bool,6> const& getDof() const; // not yet available in swig
        };
        // workaround for the EntityJoint::getDof() method
        %extend BaseJointMetadata {
            std::vector<bool> getDofVec() const {
                std::vector<bool> dofVec;
                dofVec.insert(dofVec.begin(), self->getDof().begin(), self->getDof().end());
                return dofVec;
            }
        }

        class EntityJoint : public BaseJointMetadata {
		public:
			std::string const& getEntity1() const;
			std::string const& getEntity2() const;
			piper::hbm::Id const& getEntity1FrameId() const;
			piper::hbm::Id const& getEntity2FrameId() const;

		};

        class AnatomicalJoint : public BaseJointMetadata { };

		class EntityContact {
			public:
				std::string const& name() const;

				std::string const& entity1() const;
				std::string const& entity2() const;

				piper::hbm::Id group1() const;
				piper::hbm::Id group2() const;

				bool keepThickness() const;
				double thickness() const;

			};

			%extend EntityContact {
			std::string const& typeStr() const {
				return piper::hbm::EntityContactType_str[(unsigned int)self->type()];
			}
		}


class FEFrame {
			public:
				Id const& getOriginId() const;
		};


		%nodefaultctor Node;
		%nodefaultdtor Node;

		class Node {
		public:
			piper::hbm::Coord const& get() const;
			void set( const Coord& def);
			void setCoord(double x, double y, double z);

		};
		
		%nodefaultctor IdKey;
		class IdKey {
			public:
				std::string idstr;
				int id;
		};

		%nodefaultctor FEModel;
		%nodefaultdtor FEModel;
		class FEModel {
		public:

			std::size_t getNbNode() const;
			std::size_t getNbElement1D() const;
			std::size_t getNbElement2D() const;
			std::size_t getNbElement3D() const;

			piper::hbm::Node const& getNode(const Id& id) const;
			piper::hbm::Element2D const& getElement2D(const Id& id) const;
			piper::hbm::Element3D const& getElement3D(const Id& id) const;
			piper::hbm::FEModelVTK *getFEModelVTK();

			template< typename T>
			std::map<piper::hbm::Id, std::pair<std::string, piper::hbm::IdKey>> computePiperIdToIdKeyMap() const;
	
			%template(computePiperIdToIdKeyMapNode) computePiperIdToIdKeyMap<piper::hbm::Node>;

		};


		%extend FEModel {
			std::vector<double> getFrameOrientationVec( const Id& id) const {
				Eigen::Quaternion<double> q = self->getFrameOrientation(id);
				std::vector<double> vec;
				vec.push_back(q.x());vec.push_back(q.y());vec.push_back(q.z());vec.push_back(q.w());
				return vec;
			}

			std::vector<double> getFrameOriginVec( const Id& id) const {
				piper::hbm::Coord c = self->getFrameOrigin(id);
				std::vector<double> vec;
				vec.push_back(c[0]);vec.push_back(c[1]);vec.push_back(c[2]);
				return vec;
			}
		}


		%nodefaultctor FEModelVTK;
		%nodefaultdtor FEModelVTK;
		class FEModelVTK {
		public:
			piper::hbm::VId getSelectedElements(int chosenDimension) const;
			std::map<std::string, piper::hbm::VId> getSelectedElementsByEntities(int chosenEleDimension) const;
			piper::hbm::VId getSelectedNodes() const;
		};


		%nodefaultctor Element;
		%nodefaultdtor Element;
		template <typename T>
		class Element {
		public:
			const piper::hbm::ElementType& getType() const;
			const piper::hbm::VId& get() const;
		};

		%template(ElementElement2D) piper::hbm::Element<piper::hbm::Element2D>;

		%nodefaultctor Element2D;
		%nodefaultdtor Element2D;
		class Element2D : public piper::hbm::Element<piper::hbm::Element2D> { };

		%template(ElementElement3D) piper::hbm::Element<piper::hbm::Element3D>;

		%nodefaultctor Element3D;
		%nodefaultdtor Element3D;
		class Element3D : public piper::hbm::Element<piper::hbm::Element3D> { };

		class InteractionControlPoint {
		public: 

			int getControlPointRole();
			void setGlobalWeight(double weight);
			void setWeights(std::vector<double> weight);

			void setGlobalAssociationBones(double association);
            void setGlobalAssociationSkin(double association);
			void setAssociation(std::vector<double> as_bones, std::vector<double> as_skin);
		};

		class Landmark : public BaseMetadataGroup {
		public: 

		 piper::hbm::Coord position(FEModel const& fem) const;

		piper::hbm::VId get_groupNode();

		};

		class GenericMetadata : public BaseMetadataGroup {
		public:
			piper::hbm::VId getNodeIdVec(const piper::hbm::FEModel& femodel) const;
		};

        template <typename UnitType>
        class AnthropoMetadata : public BaseMetadata {
        public:

            AnthropoMetadata();
            bool isDefined() const;
            void setValue(double const& value);
            double value() const;
        };

        %template(AnthropoMetadataLengthTmpl) AnthropoMetadata<piper::units::Length>;

        class AnthropoMetadataHeight : public AnthropoMetadata<piper::units::Length> {
        public:
            AnthropoMetadataHeight();
        };

        %template(AnthropoMetadataMassTmpl) AnthropoMetadata<piper::units::Mass>;

        class AnthropoMetadataWeight : public AnthropoMetadata<piper::units::Mass> {
        public:
            AnthropoMetadataWeight();
        };

        %template(AnthropoMetadataAgeTmpl) AnthropoMetadata<piper::units::Age>;

        class AnthropoMetadataAge : public AnthropoMetadata<piper::units::Age> {
        public:
            AnthropoMetadataAge();
        };

        class Anthropometry {
        public:
            AnthropoMetadataAge age;
            AnthropoMetadataWeight weight;
            AnthropoMetadataHeight height;
        };

		%nodefaultctor Metadata;
		%nodefaultdtor Metadata;
		class Metadata {
		public:

		typedef std::map<std::string, piper::hbm::Entity> EntityCont;
		typedef std::map<std::string, piper::hbm::EntityJoint> JointCont;
        typedef std::map<std::string, piper::hbm::AnatomicalJoint> AnatomicalJointCont;
		typedef std::map<std::string, piper::hbm::EntityContact> ContactCont;
		typedef std::map<std::string, piper::hbm::Landmark> LandmarkCont;
		typedef std::map<std::string, piper::hbm::InteractionControlPoint> InteractionControlPointCont;
		typedef std::map<std::string, piper::hbm::GenericMetadata> GenericMetadataCont;

		piper::units::Length lengthUnit() const;
		piper::units::Mass massUnit() const;
		piper::units::Age ageUnit() const;

		bool isGravityDefined() const;
		piper::hbm::Coord const& gravity() const;

		EntityCont const& entities() const;
		piper::hbm::Entity const& entity(const std::string& entityname) const;

		JointCont const& joints() const;
        AnatomicalJointCont const& anatomicalJoints() const;

		ContactCont const& contacts() const;

		InteractionControlPointCont & interactionControlPoints();

		piper::hbm::InteractionControlPoint const* interactionControlPoint(const std::string& name) const;

		LandmarkCont const& landmarks() const;

		GenericMetadataCont const& genericmetadatas() const;

        Anthropometry& anthropometry();

		};

		class EnvironmentModels {
		public:
			std::vector<std::string> getListNames() const;
		};

		class AbstractTarget {
		public:

			void setName(std::string const& name);
			std::string const& name() const;

		};

		class FixedBoneTarget : public AbstractTarget {
		public:
			FixedBoneTarget();
			FixedBoneTarget(std::string bone);
			std::string bone;
		};

		class AbstractRigidTransformationTarget : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Length> {
		public:
			typedef std::map<unsigned int, double> ValueType;
			ValueType const& value() const;
			void setValue(ValueType const& value);
		};

		class JointTarget : public AbstractRigidTransformationTarget {
		public:
			JointTarget();
			JointTarget(std::string joint, ValueType const& value);
			std::string joint;
		};

		class FrameToFrameTarget : public AbstractRigidTransformationTarget {
		public:
			bool isRelative;
			std::string frameSource, frameTarget;
		};

		class LandmarkTarget : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Length> {
		public:
			typedef std::map<unsigned int, double> ValueType;

			std::string landmark;

			ValueType const& value() const;
			void setValue(ValueType const& value);
		};

        class AgeTarget : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Age> {
		public:
            AgeTarget();
			double value() const;
            void setValue(double value);
		};

  		class HeightTarget : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Length> {
		public:
            HeightTarget();
			double value() const;
            void setValue(double value);
		};
        
        class WeightTarget : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Mass> {
		public:
            WeightTarget();
			double value() const;
            void setValue(double value);
		};

		class ControlPoint : public AbstractTarget, public piper::hbm::TargetUnit<piper::units::Length> {
		public:

            ControlPoint();
			std::vector<piper::hbm::Coord> const& value() const;
            void setValue(std::vector<piper::hbm::Coord> const& value);
			void setSubsetName(std::string subname);
			std::string const& subsetName() const ;
		};

        class ScalingParameterTarget : public AbstractTarget {
		public:
            ScalingParameterTarget();
			std::vector<double>const& value() const;
            void setValue(std::vector<double> const& value);
		};

		class TargetList {
		public:
			TargetList();
			void clear();
			std::size_t size();
			void add(FixedBoneTarget const& t);
			void add(LandmarkTarget const& t);
			void add(JointTarget const& t);
			void add(FrameToFrameTarget const& t);
            void add(AgeTarget const& t);
            void add(WeightTarget const& t);
            void add(HeightTarget const& t);
            void add(ScalingParameterTarget const& t);

			void removeFixedBone(std::string name);
			void removeLandmark(std::string name);
			void removeJoint(std::string name);
			void removeFrameToFrame(std::string name);
			void removeScalingParameter(std::string name);
			void removeAnthropometricDimension(std::string name);
			void removeControlPoint(std::string name);
			void removeAge(std::string name);
			void removeWeight(std::string name);
			void removeHeight(std::string name);

			void read(std::string const& filename);
			void write(std::string const& filename) const;

			std::list<FixedBoneTarget> fixedBone;
			std::list<LandmarkTarget> landmark;
			std::list<JointTarget> joint;
			std::list<FrameToFrameTarget> frameToFrame;
			std::list<AgeTarget> age;
			std::list<WeightTarget> weight;
			std::list<HeightTarget> height;
            std::list<ScalingParameterTarget> scalingparameter;
		};

	}
}
