/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef FACTORY__H
#define FACTORY__H

#include <map>
#include <string>
#include <functional>

// with auto registration to avoid dummy instance in registration with clone approach: http://www.codeproject.com/Articles/3734/Different-ways-of-implementing-factories
// http://stackoverflow.com/questions/10676498/factory-pattern-allocating-memory-at-compile-time-and-how-to-print-compile-time

namespace piper { 
	namespace hbm {

        template <class Object,class Key=std::string> class Factory
		{
			typedef Object* ObjectPtr;
		public:
			static Factory<Object, Key> * Instance();
			static ObjectPtr CreateInstance(Key key);
			void RegisterFactoryFunction(Key key, std::function<ObjectPtr(void)> classFactoryFunction);

		private:
			static std::map<Key, std::function<ObjectPtr(void)>> factoryFunctionRegistry;
		};




        template<class BaseObject, class Object, class Key=std::string>
		class Registrar {
		public:
			Registrar(Key key)
			{
				// register the class factory function 
				Factory<BaseObject, Key>::Instance()->RegisterFactoryFunction(key,
					[](void) -> BaseObject * { return new Object();});
			}
		};
	}
}
#include "Factory.inl"

#endif // FACTORY__H
