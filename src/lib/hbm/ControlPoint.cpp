/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ControlPoint.h"
#include "xmlTools.h"
#include <sstream>
#include <string>
#include <algorithm>
#include "FEModel.h"
#include "BaseMetadata.h"

namespace piper {
    namespace hbm {

    GenericControlPoint::GenericControlPoint() {}

    GenericControlPoint::GenericControlPoint(std::string const& name) : m_name(name) {}

    void GenericControlPoint::parseXml(tinyxml2::XMLElement* element)
    {
        doParseXml(element);

        parseVecCoord(this->point3dCont, element->FirstChildElement("point3d"));
        parseValueCont(this->groupNodeId, element->FirstChildElement("nodeId"));
        tinyxml2::XMLElement* name = element->FirstChildElement("name");
        if (name)
            m_name = name->GetText();
        else
            m_name = "";
    }

    void GenericControlPoint::serializeXml(tinyxml2::XMLElement* element) const
    {
        doSerializeXml(element);
        if (!m_name.empty())
        {
            tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
            xmlName->SetText(m_name.c_str());
            element->InsertEndChild(xmlName);
        }
        if(groupNodeId.size()){
            tinyxml2::XMLElement* xmlNodeIds;
            xmlNodeIds = element->GetDocument()->NewElement("nodeId");
            serializeValueCont(xmlNodeIds, groupNodeId);
            element->InsertEndChild(xmlNodeIds);
        }

        if(point3dCont.size()){
            tinyxml2::XMLElement* xmlPoint3d = element->GetDocument()->NewElement("point3d");
            serializeVecCoord(xmlPoint3d, point3dCont);
            element->InsertEndChild(xmlPoint3d);
        }
        //if(this->groupNodeId.size()){
        //    tinyxml2::XMLElement* xmlNodeIds;
        //    xmlNodeIds = element->GetDocument()->NewElement("nodeId");
        //    serializeValueCont(xmlNodeIds, this->groupNodeId);
        //    element->InsertEndChild(xmlNodeIds);
        //}
    }

void GenericControlPoint::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
    element->SetAttribute("name", m_name.c_str());
    doSerializePmr(element, fem);

    VCoord coord3D, coord3Dfix;
    VId idcoord;
    std::map<std::string, std::vector<IdKey>> fe_ids;
    if (groupNodeId.size()){
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<Node>();
        for (Id const& id : groupNodeId) {
            std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(id);
            if (it != mpIds.end()) {
                // node create at import
                if (it->second.first == (m_name + "_ControlPoint")) {
                    idcoord.push_back(it->second.second.id);
                    coord3D.push_back(fem.getNode(id).get());
                }
                else // regular node
                    fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
            }
            else { // control point imported
                coord3Dfix.push_back(fem.getNode(id).get());
            }
        }
    }
    if (point3dCont.size() > 0) {
        coord3Dfix.insert(coord3Dfix.end(), point3dCont.begin(), point3dCont.end());
    }
    if (coord3D.size() > 0) {
        tinyxml2::XMLElement* xmlCoord = element->GetDocument()->NewElement("coord");
        serializeVecCoord(xmlCoord, coord3D, idcoord);
        element->InsertEndChild(xmlCoord);
    }
    if (fe_ids.size() > 0)
        BaseMetadata::serializePmr_FEids(element, fe_ids);
    if (coord3Dfix.size() > 0) {
        tinyxml2::XMLElement* xmlCoordfix = element->GetDocument()->NewElement("coordfix");
        serializeVecCoord(xmlCoordfix, coord3Dfix);
        element->InsertEndChild(xmlCoordfix);
    }

}

void GenericControlPoint::clear(FEModel& fem) {
    std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<Node>();
    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it;
    for (Id const& idn : groupNodeId) {
        it = mpIds.find(idn);
        if ((it != mpIds.end()) && (mpIds.at(idn).first == m_name + "_ControlPoint"))
            fem.delNode(idn);
    }
    groupNodeId.clear();
    point3dCont.clear();

}

    std::string const& GenericControlPoint::name() const {
        return m_name;
    }

    void GenericControlPoint::setName(std::string const& name)  { 
        m_name = name;
    }

    std::vector<Coord> const& GenericControlPoint::getControlPt3dCont() const
    {
        return point3dCont;
    }

    size_t GenericControlPoint::getNbControlPt() const
    {
        return groupNodeId.size() + point3dCont.size();
    }

     VId const& GenericControlPoint::getGroupNodeId() const {
         return groupNodeId;
     }

     void GenericControlPoint::addNodeId(Id& id) {
         groupNodeId.push_back(id);
     }


    void GenericControlPoint::move3DPointsToFEModel(piper::hbm::FEModel& femodel){
        //get femodel max node id (vendor format id)
        //femodel.setMeshDef_NID();
        //Id maxFEModelId=femodel.getMaxNodeId();
        //femodel.setMeshDef_INDEX();
        Id maxPiperId = femodel.getMaxNodeId();
        std::vector<NodePtr> ctrlPtNodeVec;
	    int count = 0;
	    for (auto it = point3dCont.begin(); it != point3dCont.end(); ++it){
		    NodePtr nd = std::make_shared<Node>();
		    nd->setCoord(*it);
		    femodel.set(nd, "NodeCreateforControlPoint", IdKey(count));
            groupNodeId.push_back(nd->getId());
		    count++;
		    //Node* nd = new Node("NodeCreateforControlPoint",++maxFEModelId);
		    //nd->setCoord( *it);
      //      ctrlPtNodeVec.push_back( nd);
      //      groupNodeId.insert(maxFEModelId);
        }
        //add point3d to femodel
        //femodel.set(ctrlPtNodeVec);
        point3dCont.clear(); //remove point3d from controlpoint

        //go back to fem NID mode
        //femodel.setMeshDef_NID();
    }

    void GenericControlPoint::moveFEModelNodeSIdTo3DPoints(piper::hbm::FEModel& femodel){
        for(auto it=groupNodeId.begin(); it!=groupNodeId.end();++it){
            //should require sthg like that :
            // if(node.getIdFormat(*it).k.compare("NodeCreateforControlPoint")==0)
            point3dCont.push_back(femodel.getNode(*it).get());
            femodel.delNode(*it);
        }
        groupNodeId.clear();
    }

    void GenericControlPoint::duplicateCtrlPtFromFEModel(piper::hbm::FEModel& femodel){
        for(auto it=groupNodeId.begin(); it!=groupNodeId.end();++it){
            point3dCont.push_back(femodel.getNode(*it).get());
        }
        groupNodeId.clear();
        move3DPointsToFEModel(femodel);
    }

    void GenericControlPoint::clearCtrlPtFromFEModel(piper::hbm::FEModel& femodel){
        for(auto it=groupNodeId.begin(); it!=groupNodeId.end();++it){
            //should require sthg like that :
            // if(node.getIdFormat(*it).k.compare("NodeCreateforControlPoint")==0)
            femodel.delNode(*it);
        }
        groupNodeId.clear();
    }

    void GenericControlPoint::getCoordControlPoint(hbm::FEModel const& femModel, std::vector<Coord>& vcoord) const {
        vcoord.clear();
        for (auto const& curid : groupNodeId)
            vcoord.push_back(femModel.getNode(curid).get());
        for (auto const& curcoord : point3dCont)
            vcoord.push_back(curcoord);

    }


    InteractionControlPoint::InteractionControlPoint() : globalWeight(NAN), globalAs_bones(NAN), globalAs_skin(NAN) {}

    InteractionControlPoint::InteractionControlPoint(std::string const& name) 
        : GenericControlPoint(name), globalWeight(NAN), globalAs_bones(NAN), globalAs_skin(NAN) {}

    InteractionControlPoint::InteractionControlPoint(tinyxml2::XMLElement* element) : globalWeight(NAN), globalAs_bones(NAN), globalAs_skin(NAN)
    {
        parseXml(element);
    }

    InteractionControlPoint::InteractionControlPoint(std::string const& name, VId const& ctrlPtSId) 
        : GenericControlPoint(name), globalWeight(NAN), globalAs_bones(NAN), globalAs_skin(NAN)
    {
        groupNodeId=ctrlPtSId;
    }


    void InteractionControlPoint::clear(FEModel& fem)
    {
        GenericControlPoint::clear(fem);
        as_skin.clear();
        as_bones.clear();
        weightCont.clear();
    }

    void InteractionControlPoint::doParseXml(tinyxml2::XMLElement* element)
    {
        if (element)
        {
            if (element->Attribute("role"))
                setControlPointRole(element->Attribute("role"));
            globalWeight = NAN;
            globalAs_bones = NAN;
            globalAs_skin = NAN;
            parseValueCont(as_bones, element->FirstChildElement("as_bones"));
            parseValueCont(as_skin, element->FirstChildElement("as_skin"));
            parseValueCont(weightCont, element->FirstChildElement("weight")); // individual weights are set as element
            const char* w = element->Attribute("weight"); // global weight is set as attribute
            if (w)
                globalWeight = std::stod(w);
            const char* b = element->Attribute("as_bones");
            if (b)
                globalAs_bones = std::stod(b);
            const char* s = element->Attribute("as_skin");
            if (s)
                globalAs_skin = std::stod(s);
        }
    }

    void InteractionControlPoint::doSerializeXml(tinyxml2::XMLElement* element) const
    {
        std::string role;
        if (getControlPointRoleString(role)){
            element->SetAttribute("role", role.c_str());
        }
        if (weightCont.size() > 0){
            tinyxml2::XMLElement* xmlWeight;
            xmlWeight = element->GetDocument()->NewElement("weight");
            serializeValueCont(xmlWeight, weightCont);
            element->InsertEndChild(xmlWeight);
        }
        if (as_bones.size() > 0){
            tinyxml2::XMLElement* xmlBones;
            xmlBones = element->GetDocument()->NewElement("as_bones");
            serializeValueCont(xmlBones, as_bones);
            element->InsertEndChild(xmlBones);
        }
        if (as_skin.size() > 0){
            tinyxml2::XMLElement* xmlSkin;
            xmlSkin = element->GetDocument()->NewElement("as_skin");
            serializeValueCont(xmlSkin, as_skin);
            element->InsertEndChild(xmlSkin);
        }

        if (!std::isnan(globalWeight))
            element->SetAttribute("weight",globalWeight);
        if (!std::isnan(globalAs_bones))
            element->SetAttribute("as_bones", globalAs_bones);
        if (!std::isnan(globalAs_skin))
            element->SetAttribute("as_skin", globalAs_skin);
    }

    void InteractionControlPoint::doSerializePmr(tinyxml2::XMLElement* element, FEModel& fem) const
    {
        std::string role;
        if (getControlPointRoleString(role))
            element->SetAttribute("role", role.c_str());
        if (weightCont.size() > 0){
            tinyxml2::XMLElement* xmlWeight;
            xmlWeight = element->GetDocument()->NewElement("weight");
            serializeValueCont(xmlWeight, weightCont);
            element->InsertEndChild(xmlWeight);
       }
       if (as_bones.size() > 0){
           tinyxml2::XMLElement* xmlBones;
           xmlBones = element->GetDocument()->NewElement("as_bones");
           serializeValueCont(xmlBones, as_bones);
           element->InsertEndChild(xmlBones);
       }
       if (as_skin.size() > 0){
           tinyxml2::XMLElement* xmlSkin;
           xmlSkin = element->GetDocument()->NewElement("as_skin");
           serializeValueCont(xmlSkin, as_skin);
           element->InsertEndChild(xmlSkin);
       }
       if (!std::isnan(globalWeight))
           element->SetAttribute("weight", globalWeight);
       if (!std::isnan(globalAs_bones))
           element->SetAttribute("as_bones", globalAs_bones);
       if (!std::isnan(globalAs_skin))
           element->SetAttribute("as_skin", globalAs_skin);
    }

    void InteractionControlPoint::setGlobalWeight(double weight)
    {
        globalWeight = weight;
    }

    double InteractionControlPoint::getGlobalWeight()
    {
        return globalWeight;
    }

    double InteractionControlPoint::getGlobalWeight() const
    {
        return globalWeight;
    }


    void InteractionControlPoint::setAssociation(std::vector<double> as_bones, std::vector<double> as_skin)
    {
        this->as_bones.clear();
        this->as_bones.insert(this->as_bones.begin(), as_bones.begin(), as_bones.end());
        this->as_skin.clear();
        this->as_skin.insert(this->as_skin.begin(), as_skin.begin(), as_skin.end());
    }

    void InteractionControlPoint::setWeights(std::vector<double> weight)
    {
        this->weightCont = weight;
    }

    std::vector<double> &InteractionControlPoint::getAssociationSkin()
    {
        return as_skin;
    }

    std::vector<double> &InteractionControlPoint::getAssociationBones()
    {
        return as_bones;
    }

    std::vector<double> const&InteractionControlPoint::getAssociationSkin() const
    {
        return as_skin;
    }

    std::vector<double> const&InteractionControlPoint::getAssociationBones() const
    {
        return as_bones;
    }

    void InteractionControlPoint::setGlobalAssociationBones(double association)
    {
        globalAs_bones = association;
    }

    void InteractionControlPoint::setGlobalAssociationSkin(double association)
    {
        globalAs_skin = association;
    }

    double InteractionControlPoint::getAssociationSkinGlobal() const
    {
        return globalAs_skin;
    }

    double InteractionControlPoint::getAssociationBonesGlobal() const
    {
        return globalAs_bones;
    }

    std::vector<double> const& InteractionControlPoint::getWeight() const{
        return weightCont;
    }

    std::vector<Coord> & InteractionControlPoint::getControlPt3dCont(){
           return point3dCont;
    }
    std::vector<double> & InteractionControlPoint::getWeight() {
        return weightCont;
    }    

    InteractionControlPoint::ControlPointRole InteractionControlPoint::getControlPointRole() const{
        return ctrlPtRole;
    }

    std::string InteractionControlPoint::getControlPointRoleStr() const{
        std::string role;
        getControlPointRoleString(role);
        return role;
    }

    bool InteractionControlPoint::getControlPointRoleString(std::string& iControlPointRole) const{
        switch (ctrlPtRole){
        case ControlPointRole::CONTROLPOINT_SOURCE: {
            iControlPointRole = "source";
            return true; }
        case ControlPointRole::CONTROLPOINT_TARGET: {
            iControlPointRole = "target";
            return true; }
        case ControlPointRole::CONTROLPOINT_ROLENOTSET:
        default:
            iControlPointRole = "";
            return false;
        }
    }

    void InteractionControlPoint::setControlPointRole(ControlPointRole iControlPointRole){
        ctrlPtRole = iControlPointRole;
    }

    void InteractionControlPoint::setControlPointRole(const char* role){
        if (!role){
            ctrlPtRole = ControlPointRole::CONTROLPOINT_ROLENOTSET;
            return;
        }
        std::string roleString(role);
        std::transform(roleString.begin(), roleString.end(), roleString.begin(), ::tolower);
        if (!roleString.compare("source")){
            ctrlPtRole = ControlPointRole::CONTROLPOINT_SOURCE;
            return;
        }
        if (!roleString.compare("target")){
            ctrlPtRole = ControlPointRole::CONTROLPOINT_TARGET;
            return;
        }
        ctrlPtRole = ControlPointRole::CONTROLPOINT_ROLENOTSET;
    }

    }
}

