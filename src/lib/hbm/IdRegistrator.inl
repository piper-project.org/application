/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <iterator>
#include "IdRegistrator.h"

namespace piper {
	namespace hbm {

		template <typename T, typename TT>
		void IdRegistrator<T,TT>::clear() {
			for (typename IdRegistrator<T, TT>::map_kw::iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
				it->second.clear();
			}
			m_registered.clear();
            inverseComputed = false;
		}


		template <typename T, typename TT>
        bool IdRegistrator<T, TT>::getIdfromIdFormat(Id& idpiper, const std::string& key, const IdKey& id) {
            typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
            if (it == m_registered.end()) return false;
            else {
                typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.find(id);
                if (itv == it->second.end())
                    return false;
                else {
                    idpiper = itv->second;
                    return true;
                }
            }
        }

		template <typename T, typename TT>
        bool IdRegistrator<T, TT>::getIdfromIdFormat(Id& idpiper, const std::string& key, const IdKey& id) const {
            typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
            if (it == m_registered.end()) return false;
            else {
                typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.find(id);
                if (itv == it->second.end())
                    return false;
                else {
                    idpiper = itv->second;
                    return true;
                }
            }
        }


		template <typename T, typename TT>
        void IdRegistrator<T, TT>::getId_FE(const Id& idpiper, std::string& key, IdKey& id) const {
			for (typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
				for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv) {
					if (itv->second == idpiper) {
						key = it->first;
						id = itv->first;
						return;
					}
				}
			}
		}


        template <typename T, typename TT>
        void IdRegistrator<T, TT>::getId_FE(const VId& idpiper, std::vector<std::string>& key, std::vector<IdKey>& id) const {
            key.clear();
            key.resize(idpiper.size());
            id.clear();
            id.resize(idpiper.size());
            for (typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
                for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv) {
                    auto itsearch = std::find(idpiper.begin(), idpiper.end(), itv->second);
                    if (itsearch != idpiper.end()) {
                        size_t idx = itsearch - idpiper.begin();
                        key[idx] = it->first;
                        id[idx] = itv->second;
                    }
                }
            }
        }


        template <typename T, typename TT>
        int IdRegistrator<T, TT>::getMaxIdfromIdFormat(const std::string& key)
        {
            typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
            if (it != m_registered.end() && it->second.size() > 0) 
            {
                return (int)std::max_element(it->second.begin(), it->second.end())->second;
            }
            return -1;
        }
        
        template <typename T, typename TT>
		VId IdRegistrator<T,TT>::getIdfromIdFormat(const std::string& key) {
			std::vector<Id> vid;
			typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
			if (it != m_registered.end()) {
				for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv)
					vid.push_back(itv->second);
			}
			return vid;
		}

		template <typename T, typename TT>
		VId IdRegistrator<T,TT>::getIdfromIdFormat(const std::string& key) const{
			std::vector<Id> vid;
			typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
			if (it != m_registered.end()) {
				for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv)
					vid.push_back(itv->second);
			}
			return vid;
		}

		template <typename T, typename TT>
		void IdRegistrator<T,TT>::unregisteredId(const std::vector<Id>& vid) 
        {
            if (vid.size() > 0)
            {
                // create a map of IDs to delete
                unsigned int size = *(std::max_element(vid.begin(), vid.end())) + 1;
                bool *toDelete = new bool[size];
                std::fill(toDelete, toDelete + size, false);
                for (Id cur : vid)
                    toDelete[cur] = true;
                for (typename IdRegistrator<T, TT>::map_kw::iterator itt = m_registered.begin(); itt != m_registered.end(); ++itt) {
                    for (typename IdRegistrator<T, TT>::map_idv::iterator itv = itt->second.begin(); itv != itt->second.end();) {
                        if (itv->second < size && toDelete[itv->second]) // first condition is so that the map can be smaller, based only on the max ID of the elements to delete
                            itv = itt->second.erase(itv);
                        else
                            itv++;
                    }
                }
                delete[] toDelete;
                inverseComputed = false;

            }
		}

        template <typename T, typename TT>
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& IdRegistrator<T, TT>::computePiperIdToIdKeyMap() const {
            if (!inverseComputed) {
                m_piperIdToKWMap.clear();
                for (typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
                    for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv) {
                        m_piperIdToKWMap.insert(std::make_pair(itv->second, std::pair<std::string, IdKey>(it->first, itv->first)));
                    }
                }
                inverseComputed = true;
            }
            return m_piperIdToKWMap;
        }

		template <typename T, typename TT>
		void IdRegistrator<T,TT>::unregisteredId(const Id& vid) {
			std::vector<Id> vvid;
			vvid.push_back(vid);
			IdRegistrator<T,TT>::unregisteredId(vvid);
		}

		template <typename T, typename TT>
		void IdRegistrator<T,TT>::serializeIdXml(tinyxml2::XMLElement* element) {
			VId vidpiper, vIdKey;
			std::vector<std::string> vkey, vidstrvendor;
			int maxSize = 100000; 
			int count = 0;

			std::vector<std::string> k_list;
			k_list.reserve(m_registered.size());
			for (auto it = m_registered.begin(); it != m_registered.end(); ++it) {
				k_list.push_back(it->first);
			}
			std::sort(k_list.begin(), k_list.end());
            std::stringstream stream_idpiper, stream_id, stream_idstr, stream_key;
            Id cur_idpiper = -2;
            Id cur_idkey = -2;
            std::string cur_idstr = "";
            std::string cur_key = "";
            unsigned int count_idpiper, count_id, count_idstr, count_key;
            count_idpiper = 0;
            count_id = 0;
            count_idstr = 0;
            count_key = 0;
            bool init = false;

            if (k_list.size() > 0) {
                for (std::vector<std::string>::const_iterator it = k_list.begin(); it != k_list.end(); ++it) {
                    std::vector<IdKey> idf_list;
                    idf_list.reserve(m_registered[*it].size());
                    for (auto itt = m_registered[*it].begin(); itt != m_registered[*it].end(); ++itt) {
                        idf_list.push_back(itt->first);
                    }
                    std::sort(idf_list.begin(), idf_list.end());
                    //std::map<IdKey, TT*> orderedidv(it->second.begin(), it->second.end());
                    for (std::vector<IdKey>::const_iterator itv = idf_list.begin(); itv != idf_list.end(); ++itv) {
                        Id new_idpiper = m_registered[*it][*itv];
                        std::string new_idstr = itv->idstr;
                        if (itv->idstr == "")
                            new_idstr = "none";
                        else
                            new_idstr = itv->idstr;
                        Id new_idkey = itv->id;
                        std::string new_key = *it;
                        //
                        if (!init) {
                            cur_idpiper = new_idpiper ;
                            cur_idkey = new_idkey;
                            cur_idstr = new_idstr;
                            cur_key = new_key;
                            init = true;
                        }
                        else {
                            if (cur_idpiper + count_idpiper + 1 != new_idpiper) {
                                stream_idpiper << cur_idpiper << " " << count_idpiper << " ";
                                count_idpiper = 0;
                                cur_idpiper = new_idpiper;
                            }
                            else
                                count_idpiper++;
                            if (cur_idkey + count_id + 1 != new_idkey) {
                                stream_id << cur_idkey << " " << count_id << " ";
                                count_id = 0;
                                cur_idkey = new_idkey;
                            }
                            else
                                count_id++;
                            if (cur_idstr != new_idstr) {
                                stream_idstr << cur_idstr << " " << count_idstr << " ";
                                count_idstr = 0;
                                cur_idstr = new_idstr;
                            }
                            else
                                count_idstr++;
                            if (cur_key != new_key) {
                                stream_key << cur_key << " " << count_key << " ";
                                count_key = 0;
                                cur_key = new_key;
                            }
                            else
                                count_key++;
                        }
                    }
                }
                stream_idpiper << cur_idpiper << " " << count_idpiper << " ";
                stream_id << cur_idkey << " " << count_id << " ";
                stream_idstr << cur_idstr << " " << count_idstr << " ";
                stream_key << cur_key << " " << count_key << " ";
                //	vidpiper.push_back(m_registered[*it][*itv]);
                //	vIdKey.push_back(itv->id);
                //                if (itv->idstr=="")
                //                    vidstrvendor.push_back("none");
                //                else
                //                    vidstrvendor.push_back(itv->idstr);
                //	vkey.push_back(*it);
                //	count++;
                //	if (count == maxSize) {
                //		tinyxml2::XMLElement* xmlIdpiper;
                //		xmlIdpiper = element->GetDocument()->NewElement("idPiper");
                //		serializeValueCont(xmlIdpiper, vidpiper);
                //		element->InsertEndChild(xmlIdpiper);
                //		tinyxml2::XMLElement* xmlIdformat;
                //		xmlIdformat = element->GetDocument()->NewElement("idFormat");
                //		serializeValueCont(xmlIdformat, vIdKey);
                //		element->InsertEndChild(xmlIdformat);
                //		tinyxml2::XMLElement* xmlName;
                //		xmlName = element->GetDocument()->NewElement("NameFormat");
                //		serializeValueCont(xmlName, vidstrvendor);
                //		element->InsertEndChild(xmlName);
                //		tinyxml2::XMLElement* xmlKey;
                //		xmlKey = element->GetDocument()->NewElement("key");
                //		serializeValueCont(xmlKey, vkey);
                //		element->InsertEndChild(xmlKey);
                //		//clear
                //		count = 0;
                //		vidpiper.clear();
                //		vIdKey.clear();
                //		vidstrvendor.clear();
                //		vkey.clear();
                //	}
                //}
                //}
                element->SetAttribute("compress", "true");
                //if (count != 0) {
                tinyxml2::XMLElement* xmlIdpiper;
                xmlIdpiper = element->GetDocument()->NewElement("idPiper");
                xmlIdpiper->SetText(stream_idpiper.str().c_str());
                element->InsertEndChild(xmlIdpiper);

                tinyxml2::XMLElement* xmlIdformat;
                xmlIdformat = element->GetDocument()->NewElement("idFormat");
                xmlIdformat->SetText(stream_id.str().c_str());
                element->InsertEndChild(xmlIdformat);

                tinyxml2::XMLElement* xmlName;
                xmlName = element->GetDocument()->NewElement("NameFormat");
                xmlName->SetText(stream_idstr.str().c_str());
                element->InsertEndChild(xmlName);

                tinyxml2::XMLElement* xmlKey;
                xmlKey = element->GetDocument()->NewElement("key");
                xmlKey->SetText(stream_key.str().c_str());
                element->InsertEndChild(xmlKey);
                //}
            }
		}

		template <typename T, typename TT>
		std::string IdRegistrator<T,TT>::infoIds() const{
			std::string str;
			for (typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
				for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv) {
					std::string strcur;
					std::ostringstream oss;
					oss << itv->second << " - ";
					strcur = oss.str();
					strcur += it->first;
					strcur += "-";
					strcur += itv->first.infoId();
					strcur += "\n";
					str += strcur;
				}
			}
			return str;
		}


		template <typename T, typename TT>
		std::string IdRegistrator<T,TT>::infoId(Id const& id) const {
			std::string str;
			for (typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.begin(); it != m_registered.end(); ++it) {
				for (typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.begin(); itv != it->second.end(); ++itv) {
					if (itv->second == id) {
						std::ostringstream oss;
						oss << itv->second << " - ";
						str = oss.str();
						str += it->first;
						str += "-";
						str += itv->first.infoId();
						str += "\n";
						return str;
					}
				}
			}
			return str;
		}


		template <typename T, typename TT>
		void IdRegistrator<T,TT>::registerId(std::shared_ptr<TT> idcomp, const std::string& key) {
			IdKey idk = IdKey(getMaxIdfromIdFormat(key) + 1);
			return registerId( idcomp, key, idk);
		}


		template <typename T, typename TT>
        void IdRegistrator<T, TT>::registerId(std::shared_ptr<TT> piper, const std::string& key, const IdKey& id) {
            m_registered[key][id] = piper->getId();
            inverseComputed = false;
            //m_piperIdToKWMap[piper->getId()] = std::pair<std::string, IdKey>(key, id);
		}



		//template <typename T, typename TT>
		//TT* IdRegistrator<T, TT>::get(const std::string& key, const IdKey& id) {
  //          typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
  //          if (it == m_registered.end()) return nullptr;
  //          else {
  //              typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.find(id);
  //              if (itv == it->second.end())
  //                  return nullptr;
  //              else {
  //                  return itv->second;
  //              }
  //          }
		//}


		//template <typename T, typename TT>
  //      unsigned int IdRegistrator<T, TT>::get(const std::string& key, const IdKey& id) const {
  //          typename IdRegistrator<T, TT>::map_kw::const_iterator it = m_registered.find(key);
  //          if (it == m_registered.end()) return nullptr;
  //          else {
  //              typename IdRegistrator<T, TT>::map_idv::const_iterator itv = it->second.find(id);
  //              if (itv == it->second.end())
  //                  return nullptr;
  //              else {
  //                  return itv->second;
  //              }
  //          }
		//}


	}
}
