/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef GROUP_H
#define GROUP_H


#include "MeshComponentIdName.h"
#include "MeshComponentContainer.h"
#include "xmlTools.h"

namespace piper {
	namespace hbm {

		
		class Node;
		class Element1D;
		class Element2D;
		class Element3D;


        class Group : public MeshComponentIdName<VId, Group> {
		public:
            Group() : MeshComponentIdName<VId, Group>(), m_partflag(false) {}
            explicit Group(tinyxml2::XMLElement* element) : MeshComponentIdName<VId, Group>() { parseXml(element); }
			~Group(){};
			// ------------- Utilities -------------
			void addInGroup( const Id& id) {
				this->m_def.push_back(id);
			}
			void addInGroup(VId& vid) {
				this->m_def.insert(this->m_def.end(), vid.begin(), vid.end());
			}
			void serializeXml(tinyxml2::XMLElement* element) const {
				tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
				xmlName->SetText(this->m_name.c_str());
				element->InsertEndChild(xmlName);
				tinyxml2::XMLElement* xmlGroupId;
				xmlGroupId = element->GetDocument()->NewElement("Id");
				xmlGroupId->SetText(this->m_id);
				element->InsertEndChild(xmlGroupId);
                tinyxml2::XMLElement* xmlPartFlag;
                xmlPartFlag = element->GetDocument()->NewElement("PartFlag");
                if (m_partflag)
                    xmlPartFlag->SetText("true");
                else
                    xmlPartFlag->SetText("false");
                element->InsertEndChild(xmlPartFlag);
                tinyxml2::XMLElement* xmlIds;
				int count=0;
				VId cursid;
				for (VId::const_iterator it = this->m_def.begin(); it != this->m_def.end(); ++it) {
					cursid.push_back( *it);
					count++;
					if (count==NB_MAX_ELEMENT_PER_LINE) {
						xmlIds = element->GetDocument()->NewElement("Content");
						serializeValueCont(xmlIds, cursid);
						element->InsertEndChild(xmlIds);
						cursid.clear();
						count=0;	
					}
				}
				if (count!=0) {
					xmlIds = element->GetDocument()->NewElement("Content");
					serializeValueCont(xmlIds, cursid);
					element->InsertEndChild(xmlIds);
					cursid.clear();
				}
			}
            void setPart(bool const& flag) { m_partflag = flag; }
            bool isPart() const { return m_partflag; }
		private:
			void parseXml(tinyxml2::XMLElement* element) {
				//parseValueCont(m_def, element->FirstChildElement("Content"));				
				std::stringstream ids(element->FirstChildElement("Id")->GetText());
				ids >> this->m_id;
				this->m_name = element->FirstChildElement("name")->GetText();
                if (element->FirstChildElement("PartFlag")) {
                    std::string partflag = element->FirstChildElement("PartFlag")->GetText();
                    if (partflag == "true")
                        m_partflag = true;
                    else
                        m_partflag = false;
                }
				tinyxml2::XMLElement* elt = element->FirstChildElement("Content");
				while ( elt != nullptr) {
					VId cursid;
					parseValueCont(cursid, elt);
					this->m_def.insert(this->m_def.end(), cursid.begin(), cursid.end());
					elt = elt->NextSiblingElement("Content");
				}
			}

            bool m_partflag;

		protected:
            explicit Group(const Id& nid) : MeshComponentIdName<VId, Group>(nid) {}

		};

        class GroupNode : public Group {
		public:
			GroupNode();
			explicit GroupNode(tinyxml2::XMLElement* element);
			~GroupNode(){};

		protected:
			explicit GroupNode(const Id& nid);
		};

        class GroupElements1D : public Group {
		public:
			GroupElements1D();
			explicit GroupElements1D(tinyxml2::XMLElement* element);
			~GroupElements1D(){};

		protected:
			explicit GroupElements1D(const Id& nid);
		};


        class GroupElements2D : public Group {
		public:
			GroupElements2D();
			explicit GroupElements2D(tinyxml2::XMLElement* element);
			~GroupElements2D(){};

		protected:
			explicit GroupElements2D(const Id& nid);
        };


		
        class GroupElements3D : public Group {
		public:
			GroupElements3D();
			explicit GroupElements3D(tinyxml2::XMLElement* element);
			~GroupElements3D(){};

		protected:
			explicit GroupElements3D(const Id& nid);
		};


        class GroupGroups : public Group {
		public:
			GroupGroups();
			explicit GroupGroups(tinyxml2::XMLElement* element);
			~GroupGroups(){};

		protected:
			explicit GroupGroups(const Id& nid);
		};



		class ContGroupNodes: public MeshComponentContainer<GroupNode> {
		public:
			ContGroupNodes(): MeshComponentContainer< GroupNode>( "GroupesNodes")  {}
		};


		class ContGroupElements1D : public MeshComponentContainer< GroupElements1D> {

		public:
            ContGroupElements1D(): MeshComponentContainer< GroupElements1D>( "GroupElements1D")  {}
		};

		class ContGroupElements2D : public MeshComponentContainer< GroupElements2D> {

		public:
            ContGroupElements2D(): MeshComponentContainer< GroupElements2D>( "GroupElements2D")  {}
		};

		class ContGroupElements3D : public MeshComponentContainer< GroupElements3D> {

		public:
            ContGroupElements3D(): MeshComponentContainer< GroupElements3D>( "GroupElements3D")  {}
		};


		class ContGroupGroups : public MeshComponentContainer< GroupGroups> {

		public:
            ContGroupGroups(): MeshComponentContainer< GroupGroups>( "GroupGroups")  {}
		};
	}
}
#endif //GROUP_H
