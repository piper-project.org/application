/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "contourCLdof.h"

 
using namespace std;
namespace piper {
	namespace hbm {

		ContourCLdof::ContourCLdof() {
		}

		ContourCLdof::~ContourCLdof()
		{
		}

		vector<string> ContourCLdof::getMotionaxes(){
			return motion_axes;
		}

		vector<string> ContourCLdof::getReferenceaxes(){
			return reference_axes;
		}

		vector<string> ContourCLdof::getTargetaxes(){
			return target_axes;
		}

		string ContourCLdof::getdofname(){
			return dofname;
		}

		void ContourCLdof::setMotionaxes(std::string axes1, std::string axes2){
			motion_axes.clear();
			motion_axes.push_back(axes1);
			motion_axes.push_back(axes1);
		}

		void ContourCLdof::setReferenceaxes(string axes1, string axes2){
			reference_axes.clear();
			reference_axes.push_back(axes1);
			reference_axes.push_back(axes2);
		}

		void ContourCLdof::setTargetaxes(string axes1, string axes2){
			target_axes.clear();
			target_axes.push_back(axes1);
			target_axes.push_back(axes2);
		}

		void ContourCLdof::setName(string name){
			dofname = name;
		}

	}
}
