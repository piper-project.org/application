/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "IdGenerator.h"


namespace piper {
	namespace hbm {
		class Node;
		template <> int IdGenerator<Node>::lastId = -1;
		template <>	unsigned int IdGenerator<Node>::counter = 0;
		class Element1D;
		template <> int IdGenerator<Element1D>::lastId = -1;
		template <>	unsigned int IdGenerator<Element1D>::counter = 0;
		class Element2D;
		template <> int IdGenerator<Element2D>::lastId = -1;
		template <>	unsigned int IdGenerator<Element2D>::counter = 0;
		class Element3D;
		template <> int IdGenerator<Element3D>::lastId = -1;
		template <>	unsigned int IdGenerator<Element3D>::counter = 0;
		class Group;
		template <> int IdGenerator<Group>::lastId = -1;
		template <>	unsigned int IdGenerator<Group>::counter = 0;
		class FEFrame;
		template <> int IdGenerator<FEFrame>::lastId = -1;
		template <>	unsigned int IdGenerator<FEFrame>::counter = 0;
		class FEModelParameter;
		template <> int IdGenerator<FEModelParameter>::lastId = -1;
		template <>	unsigned int IdGenerator<FEModelParameter>::counter = 0;



	}
}

