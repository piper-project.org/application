/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <ctime>
#include "ParserModelFile.h"

#include "FEModel.h"
#include "Helper.h"
#include <stack>

#include <boost/filesystem.hpp>

// to disable warning about using localtime - localtime_s is not yet standardized and we don't actually care about localtime not being thread-safe -
// if slightly different times are written in the header of the exported files it's no problem
#pragma warning(push)
#pragma warning(disable:4996)

using namespace std;
namespace piper {
	namespace hbm {
		namespace parser {

            ParserModelFile::ParserModelFile(string const& formatrulesFile)
                : m_context(formatrulesFile) {}


			ParserModelFile::ParserModelFile(string const& formatrulesFile, string const& modelrulesFile)
                : m_context(formatrulesFile, modelrulesFile) {}


            void ParserModelFile::doParsing(FEModel* femodel, std::string const& sourceFile, std::vector<std::string> const& includeFiles, PARSER_OPTION option) {

                m_context.setFEModel(femodel);
                m_context.parsing();
				if (option == PARSER_OPTION::NODES_ONLY)
					m_context.parseOnlyNodeComponent(); //parse only node
				else
				    m_context.parseOnlyMeshComponent(); //parse only mesh and node to start

				stack<string> incfile;
                this->parsefile(sourceFile); // among other things loads include files in the source file
                for (std::string const& s : includeFiles) // add the additional include files defined in .pmr
                    m_context.addIncFiles(s);
                for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
                    incfile.push(*itv);
                while (incfile.size() > 0) {
                    std::string curfile = incfile.top();
                    incfile.pop();
                    this->parsefile(curfile);
                    // append additional include files the newly parsed file might have added
                    for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
                        incfile.push(*itv);
                }

                if (option==PARSER_OPTION::WHOLE) {
                    // second: parse other model features (groups, frames...)
                    m_context.parseOnlyNonMeshComponent();
                    // apply second evaluation on keyword rules
                    this->parsefile(sourceFile); // among other things loads include files in the source file
                    for (std::string const& s : includeFiles) // add the additional include files defined in .pmr
                        m_context.addIncFiles(s);
                    for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
                        incfile.push(*itv);
                    while (incfile.size() > 0) {
                        std::string curfile = incfile.top();
                        incfile.pop();
                        this->parsefile(curfile);
                        // append additional include files the newly parsed file might have added
                        for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
                            incfile.push(*itv);
                    }

				}
				// clean empty, some format produces empty with parsing rule (more easier to manage)
				femodel->cleanEmptyGroups();
				//
			}

			void ParserModelFile::fillMetadata( FEModel* femodel, Metadata& metadata) {
                metadata.setNameFormat(m_context.getParserFormatRule().m_format.format);
                m_context.getModelDescription().fillmetadata(femodel, metadata);
			}

            void ParserModelFile::doUpdating(FEModel* femodel, const string& filename, std::vector<std::string> const& includeFiles,
                const string& targetdirectory, vector<string> const& msg) {
                m_context.setFEModel(femodel);
                m_context.updating();

    			namespace fs=boost::filesystem;
				// first: copy model file in target directory
				fs::path sourcefilePath(filename);
				string sourcefile = sourcefilePath.filename().string();

				fs::path targetfilePath(targetdirectory);
				if (!fs::exists( targetfilePath))
					fs::create_directory( targetfilePath);
				fs::path fulltargetfilePath = targetfilePath / sourcefile;

				//prevent to export in the same diretory of the original FE files
                if (sourcefilePath.parent_path() == fulltargetfilePath.parent_path()) {
                    std::stringstream str;
                    str << "Can not export in the directory of the imported model" << endl;
                    throw std::runtime_error(str.str().c_str());
                    return;
                }
				if(fs::exists(  fulltargetfilePath))
					remove( fulltargetfilePath);

				stack<string> incfile;
				//
                this->updatefile(sourcefilePath.string(), fulltargetfilePath.string(), msg);
                for (std::string const& s : includeFiles) // add the additional include files defined in .pmr
                    m_context.addIncFiles(s);
                for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
					incfile.push( *itv);
				while (incfile.size() > 0) {
					std::string curfile=incfile.top();
					incfile.pop();
					// copy current include file
					fs::path fullsourceincfilePath( curfile);
					fs::path fulltargetincfilePath = targetfilePath / fullsourceincfilePath.filename();

					if(fs::exists(  fulltargetincfilePath))
						remove( fulltargetincfilePath);
					 this->updatefile( curfile, fulltargetincfilePath.string(), msg);
                     for (vector<string>::const_iterator itv = m_context.getIncfiles().begin(); itv != m_context.getIncfiles().end(); ++itv)
						incfile.push( *itv);
				}
			}


            void ParserModelFile::parsefile(const std::string& filename) {
                //set file ressource
                m_context.incFileReset();
                m_context.getFileRessourceParse().open(filename);
                m_context.getFileRessourceParse().setSearchInfo(m_context.getParserFormatRule().m_format.commcar);

                ParserFormatRule::keywordcont const listKw = m_context.getParserFormatRule().getListKeywords(); //list of key to be parsed

                while (m_context.getFileRessourceParse().getnextkeyword(listKw)) {
                    m_context.setParsedKw(m_context.getFileRessourceParse().getCurrentLineKeyword());
                    //cout << m_context.getParsedKw() << endl;
                    try 
                    {
                        m_context.getParserFormatRule().applyrules(&m_context);
                    }
                    catch (std::exception e)
                    {
                        std::stringstream s;
                        s << "Error parsing file " << filename << ", keyword '" << m_context.getParsedKw() <<
                            "' at line " << m_context.getFileRessourceParse().getCounterLine() << ": " << e.what() << ".";
                        m_context.freeResources();
                        throw std::runtime_error(s.str().c_str());
                    }
				}
                m_context.freeResources();
			}

			void ParserModelFile::updatefile( const string& source, const string& target, vector<string> const& msg) {
                //set file ressource
                m_context.incFileReset();
                m_context.getFileRessourceParse().open(source);
                m_context.getFileRessourceParse().setSearchInfo(m_context.getParserFormatRule().m_format.commcar);
                //set file ressource output
                m_context.getFileRessourceUpdate().open(target);

                ParserFormatRule::keywordcont const& listKw = m_context.getParserFormatRule().getListKeywords(); //list of key to be parsed

                // add msg added n the header of the output file
                if (msg.size() > 0) {                    
                    for (vector<string>::const_iterator it = msg.begin(); it != msg.end(); ++it) {
						std::string headerline = "";
						headerline.append(1, (char)(m_context.getParserFormatRule().m_format.commcar[0]));
						headerline.append(" " + *it);
                        m_context.getFileRessourceUpdate().write(headerline);
                    }
                    // add local time
                    time_t rawtime;
                    time(&rawtime);
                    tm *timeinfo = localtime(&rawtime);
                    std::stringstream headertime;
                    char timeAsText[100];
                    std::strftime(timeAsText, sizeof timeAsText, "%c", timeinfo);
                    headertime << (char)(m_context.getParserFormatRule().m_format.commcar[0]) << " " << timeAsText;
                    m_context.getFileRessourceUpdate().write(headertime.str());
                }

                m_context.updatecontext.reset();
                while (m_context.getFileRessourceParse().copykeyword(m_context.getFileRessourceUpdate(), listKw)) {
                    m_context.setParsedKw(m_context.getFileRessourceParse().getCurrentLineKeyword());
                    //cout << m_context.getParsedKw() << endl;
                    m_context.getParserFormatRule().applyrules(&m_context);
                    for (auto it = m_context.updatecontext.updatelinekw.begin(); it != m_context.updatecontext.updatelinekw.end(); ++it)
                        m_context.getFileRessourceUpdate().write(*it);
                    m_context.updatecontext.reset();
                }
                m_context.freeResources();
            }

            void ParserModelFile::setMetadataRulePath(std::string const& filename){
                m_context.setModelDescriptionFile(filename);
            }

            void ParserModelFile::setFormatName(std::string const& name) {
                m_context.setFormatName(name);
            }


		}//parser
	}//hbm
}//piper

#pragma warning(pop)
