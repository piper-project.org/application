/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "contourCL.h"

 
using namespace std;
namespace piper {
	namespace hbm {

		ContourCL::ContourCL() {
            root = nullptr;
		}

		ContourCL::~ContourCL()
		{
		}

		

		ContourCLj* ContourCL::findparentJoint(std::string name){
			ContourCLbr* temp = root;
			std::queue<ContourCLbr*> local;
			local.push(root);
			ContourCLj* joint;
			joint = nullptr;
			while (local.size() != 0){
				ContourCLbr* temp1 = local.front();
				local.pop();
				for (int i = 0; i < temp1->childrenjts.size(); i++){
					joint = temp1->childrenjts.at(i);
					if (joint->name == name){
						std::cout << std::endl << "found the name: " << joint->name;
                        //std::cout << std::endl << joint->flexionaxeslandmarks.at(0);
                        //std::cout << std::endl << joint->flexionaxeslandmarks.at(1);
						return joint;
					}
					if (temp1->childrenjts.at(i)->childbodyregion->name == name){
						joint = temp1->childrenjts.at(i);
						std::cout << std::endl << "found the name: " << joint->name;
                        //std::cout << std::endl << joint->flexionaxeslandmarks.at(0);
                        //std::cout << std::endl << joint->flexionaxeslandmarks.at(1);
						return joint;
					}
					local.push(temp1->childrenjts.at(i)->childbodyregion);
				}
			}
			return joint;
		}

		ContourCLj* ContourCL::getJoint(std::string name){
			ContourCLbr* temp = root;
			std::queue<ContourCLbr*> local;
			local.push(root);
			ContourCLj* joint;
			joint = nullptr;
			while (local.size() != 0){
				ContourCLbr* temp1 = local.front();
				local.pop();
				for (int i = 0; i < temp1->childrenjts.size(); i++){
					joint = temp1->childrenjts.at(i);
					if (joint->name == name){
						//std::cout << std::endl << "found the name: " << joint->name;
						std::reverse(joint->jointcont.begin(), joint->jointcont.end());
						//std::cout << std::endl << joint->flexionaxeslandmarks.at(0);
						//std::cout << std::endl << joint->flexionaxeslandmarks.at(1);
						return joint;
					}
					if (temp1->childrenjts.at(i)->childbodyregion->name == name){
						joint = temp1->childrenjts.at(i);
						//std::cout << std::endl << "found the name: " << joint->name;
						std::reverse(joint->jointcont.begin(), joint->jointcont.end());
						//std::cout << std::endl << joint->flexionaxeslandmarks.at(0);
						//std::cout << std::endl << joint->flexionaxeslandmarks.at(1);
						return joint;
					}
					local.push(temp1->childrenjts.at(i)->childbodyregion);
				}
			}
			return joint;
		}

		ContourCLbr* ContourCL::getBodyRegion(string name){
			ContourCLbr* br = root;
			stack<ContourCLbr*> bodyRegions;
			bodyRegions.push(br);
			while (!bodyRegions.empty()){
				br = bodyRegions.top();
				bodyRegions.pop();
				if (br->name == name)
				{ 
					std::reverse(br->brcont.begin(), br->brcont.end());
					return br;
				}
				for (unsigned int i = 0; i < br->childrenjts.size(); i++)
					if (br->childrenjts.at(i)->childbodyregion)
						bodyRegions.push(br->childrenjts.at(i)->childbodyregion);

			}
			auto tempbRegion = bodyRegions;
			while (tempbRegion.size())
				tempbRegion.pop();
			while (bodyRegions.size())
			{
				auto tp = bodyRegions.top();
				bodyRegions.pop();
				tempbRegion.push(tp);
			}
			bodyRegions = tempbRegion;
			//std::reverse(bodyRegions.begin(), bodyRegions.end());
			//std::cout << "\t ****Returning NULL";
			return NULL;
		}

		void ContourCL::printSkele(){

/*			if (breg == nullptr){
				breg = root;
			}
			
			cout << endl << endl  << breg->name << endl;
			
			for (int i = 0; i < breg->childrenjts.size(); i++){
				ContourCLj* jt = new ContourCLj;
				std::cout << "size" << breg->childrenjts.size() << endl;
				jt = breg->childrenjts.at(i);
				cout << jt->name << endl;
				ContourCLbr* bri = new ContourCLbr();
				bri = jt->childbodyregion;
				printSkele(bri);
			}
	*/
		
			ContourCLbr* temp = root;

			std::queue<ContourCLbr*> local;
			local.push(root);
			while (local.size() != 0){
				ContourCLbr* temp1 = local.front();
				std::cout << endl << temp1->name;
				if (!temp1->description.empty()){
					std::cout << endl << temp1->description;
				}
				for (int i = 0; i < temp1->cplandmarks.size(); i++){
					std::cout << temp1->cplandmarks.at(i) << endl;
					std::cout << endl << temp1->cplandmarks_info.size();
					if (temp1->cplandmarks_info.size() != 0){
						if (!temp1->cplandmarks_info.at(i).empty()){
							std::cout << endl << "hey hey" << temp1->cplandmarks_info.at(i);
						}
					}
				}
				for (int i = 0; i < temp1->circumfvec.size(); i++){
					std::cout << temp1->circumfvec.at(i) << endl;
					std::cout << endl << temp1->circumfvec_info.size();
					if (temp1->circumfvec_info.size() != 0){
						if (!temp1->circumfvec_info.at(i).empty()){
							std::cout << endl << "hey hey" << temp1->circumfvec_info.at(i);
						}
					}
				}
				local.pop();

				for (int i = 0; i < temp1->childrenjts.size(); i++){
					std::cout << temp1->childrenjts.size() << endl;
					std::cout << endl << temp1->childrenjts.at(i)->name << endl;
					if (!temp1->childrenjts.at(i)->joint_description.empty()){
						std::cout << endl << temp1->childrenjts.at(i)->joint_description;
					}
					if (!temp1->childrenjts.at(i)->special_landmark1.empty()){
						std::cout << temp1->childrenjts.at(i)->special_landmark1 << endl;
					}
					if (!temp1->childrenjts.at(i)->special_landmark2.empty()){
						std::cout << temp1->childrenjts.at(i)->special_landmark2 << endl;
					}
					if (!temp1->childrenjts.at(i)->entityjointname.empty()){
						std::cout << temp1->childrenjts.at(i)->entityjointname << endl;
					}
					if (!temp1->childrenjts.at(i)->joint_splinep0.empty()){
						std::cout << temp1->childrenjts.at(i)->joint_splinep0 << endl;
					}

					if (!temp1->childrenjts.at(i)->joint_splinep1.empty()){
						std::cout << temp1->childrenjts.at(i)->joint_splinep1 << endl;
					}
					if (!temp1->childrenjts.at(i)->joint_splinet1.empty()){
						std::cout << temp1->childrenjts.at(i)->joint_splinet1 << endl;
					}
					if (!temp1->childrenjts.at(i)->joint_splinet2.empty()){
						std::cout << temp1->childrenjts.at(i)->joint_splinet2 << endl;
					}
					if (!temp1->childrenjts.at(i)->circum.empty()){
						std::cout << temp1->childrenjts.at(i)->circum << endl;
					}


//					std::cout << endl << temp1->childrenjts.at(i)->childbodyregion->name << endl;
					local.push(temp1->childrenjts.at(i)->childbodyregion);
//					std::cout << temp1->childrenjts.size() << "i" << i <<endl;
				}
			}

		
		}


	}
}
