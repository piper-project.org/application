/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef HELPER_H
#define HELPER_H

#include <array>
#include <map>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

#include "types.h"

#pragma warning(disable:4503) // decorated name length exceeded generated by boost templates
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

/** \ingroup libhbm
 * @{
 */
#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

// from http://www.cplusplus.com/forum/beginner/154004/
namespace util
{
#if __cplusplus >= 201402L // C++14

    using std::make_unique;

#else // C++11

    template < typename T, typename... CONSTRUCTOR_ARGS >
    std::unique_ptr<T> make_unique(CONSTRUCTOR_ARGS&&... constructor_args)
    {
        return std::unique_ptr<T>(new T(std::forward<CONSTRUCTOR_ARGS>(constructor_args)...));
    }

#endif // __cplusplus == 201402L
}

namespace piper {
	namespace hbm {

        /** Brief returns a string of the absolute path of the file pointed by the parameter path. This is done by adding the prefix path pointed by the parameter originpath.
        * In case the parameter path is already absolute, it is returned as is.
        */
		HBM_EXPORT std::string absolutePath(std::string const& path, std::string const& originpath);


        /** Brief returns a string of the relative path of the file pointed by the parameter path. The returned path is relative to the path pointed by the parameter relative_to.
        */
        HBM_EXPORT boost::filesystem::path relativePath(const boost::filesystem::path &path, const boost::filesystem::path &relative_to);


        /** Brief returns a string of the relative path of the file pointed by the parameter path. The returned path is relative to the path pointed by the parameter relative_to.
        */
        void get_all_in_directory(const boost::filesystem::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret);

		std::string space2underscore(std::string const& text);


		std::vector<std::string> split(const std::string &s, char delim);
		bool dirExists(const char *path);
		bool fileExists(const char *file);
		inline bool isComment(const std::string &line, const std::string& comment) {
			return (line[0] == comment[0]);
		};

		inline bool isComment(const std::string &line, const std::vector<std::string>& comment) {
			return (std::find(comment.begin(), comment.end(), line.substr(0, 1)) != comment.end());
		};

		inline bool getline_skipcomments(std::ifstream &file, std::string &line, std::string& comment, unsigned int& count)
		{
			while (std::getline(file, line)) {
				count++;
				if (!isComment(line, comment)) {
					line.erase(line.find_last_not_of(" \r\t") + 1);
					return true;
				}
			}
			return false;
		};
		inline bool getline_skipcomments(std::ifstream &file, std::string &line, std::vector<std::string>& comment, unsigned int& count) {
			while (!std::getline(file, line).eof()) {
				count++;
				if (!isComment(line, comment)) {
					line.erase(line.find_last_not_of(" \r\t") + 1);
					return true;
				}
			}
			return false;
		};

		void copyline_comments(std::ifstream &file, std::ofstream &outfile, std::string &line, std::string& comment);
		void copyline_onlycomments(std::ifstream &file, std::ofstream &outfile, std::string &line, std::string& comment);

		// **********************************************************
		// fixed width cout
		// **********************************************************
		template <typename T, int W>
		struct FixedWidthVal {
			FixedWidthVal(T v_) : v(v_) {}
			T v;
		};
		template <typename T, int W>
		std::ostream& operator<< (std::ostream& ostr, const FixedWidthVal<T,W> &fwv) {
			return ostr << std::setw(W) << fwv.v;
		}

		// **********************************************************
		// unique value in vecor
		// **********************************************************
		template< typename T>
		size_t uniqueValue(typename std::vector<T> &v) {
			typename std::vector<T>::iterator it;
			it = std::unique (v.begin(), v.end());
			v.resize( std::distance(v.begin(),it) );
			return v.size();
		}

		// **********************************************************
		// linspace
		// **********************************************************
		template< typename T>
		std::vector<T> linspace(T min, T max, int n) {
			std::vector<T> result;
            if (n<=1)
                return result;
			// vector iterator
			int iterator = 0;
			for (int i = 0; i <= n; i++) {
				double temp = min + i*(max-min)/(n - 1);
				result.insert(result.begin() + iterator, (T)temp);
				iterator++;
			}
			result.insert(result.begin() + iterator, max);
			return result;
        }

        /** Compute the least square sphere \param center \param radius which best fits the \param points . It uses a maximim number of \param maxIterations for the fixed-point algorithm.
         * \see Theory from http://www.geometrictools.com/Documentation/LeastSquaresFitting.pdf
         * @return the effective number of iteration of the fixed-point algorithm.
         */
        unsigned int fitSphere(VCoord const& points, Coord& center, double& radius, unsigned int maxIterations=1000);


        /** /et of file in a directory with a specific extension
        *
        */
        namespace fs = ::boost::filesystem;
        void getFiles(const std::string& p, const std::string& ext, std::vector<std::string>& ret);

        HBM_EXPORT void getValidName(std::string& name, std::vector<std::string> const& listnames);

    }
}
/** @} */

#endif
