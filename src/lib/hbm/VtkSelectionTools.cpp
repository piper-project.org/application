/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "VtkSelectionTools.h"

#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkMath.h>
#include <vtkExtractSelectedIds.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkCamera.h>
#include <vtkPlane.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRenderer.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkDataSetSurfaceFilter.h>

#include <queue>
#include <map>
#include <list>

#define PIPER_RINGS_DEBUGVIS 0 // visualization of how boxes are split and what CPs are inside, DO NOT push code with this enabled to master, only for debugging
#if PIPER_RINGS_DEBUGVIS
#include <vtkOpenGLActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkGlyph3DMapper.h>
#include <vtkInteractorStyleTrackballCamera.h>
#endif

using namespace boost::container;

namespace piper {

    namespace hbm {

        vtkStandardNewMacro(vtkSelectionTools);

        vtkSelectionTools::vtkSelectionTools()
        {
            Reset();
            output = vtkSmartPointer<vtkExtractSelection>::New();
        }

        vtkSelectionTools::~vtkSelectionTools()
        {
        }

        /// <summary>
        /// Clears all the selection results.
        /// </summary>
        /// <param name="alsoResetInData">If set to <c>true</c>, all the data connected to 
        /// all the previous selection querries will be have their "selected" flags reset,
        /// i.e. all nodes and elements will be marked as not selected. False by default.</param>
        void vtkSelectionTools::ResetResults(bool alsoResetData)
        {
            // TODO - TEST IF IT ACTUALLY WORKS
            if (alsoResetData)
            {
                for (int curr_con = 0; GetNumberOfInputConnections(0); curr_con++) // port 0 is for data, port 1 for selectionNodes
                {
                    vtkDataObject *input = GetInputDataObject(0, curr_con);
                    if (input->IsA("vtkPointSet")) // if it is not a point set, do not do anything...but it should always be a pointset
                    {
                        vtkPointSet *data = vtkPointSet::SafeDownCast(input);
                        if (data)
                        {
                            vtkSmartPointer<vtkBitArray> scalarArray = vtkBitArray::SafeDownCast(data->GetPointData()->GetArray(SELECTED_PRIMITIVES));
                            if (scalarArray)
                            {
                                for (int i = 0; i < scalarArray->GetNumberOfTuples(); i++)
                                    scalarArray->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                            }
                            scalarArray = vtkBitArray::SafeDownCast(data->GetCellData()->GetArray(SELECTED_PRIMITIVES));
                            if (scalarArray)
                            {
                                for (int i = 0; i < scalarArray->GetNumberOfTuples(); i++)
                                    scalarArray->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                            }
                        }
                    }
                }
            }
            output->Delete();
            output = vtkSmartPointer<vtkExtractSelection>::New();
        }

        void vtkSelectionTools::Reset()
        {
            p_blankSelected = false;
            p_inverseSelection = false;
            p_keepPoints = false;
            targetType = (unsigned int) SELECTION_TARGET::NONE;
            OBB = std::make_shared<vtkOBBNodeWNeighbors>();
            currentTool = SELECTION_TOOL::NONE;
        }

        int vtkSelectionTools::RequestData(vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector)
        {
            // get the info objects
            vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
            vtkInformation *outInfo = outputVector->GetInformationObject(0);

            // get the input and output
            vtkDataSet *input = vtkDataSet::SafeDownCast(
                inInfo->Get(vtkDataObject::DATA_OBJECT()));
            vtkUnstructuredGrid *output = vtkUnstructuredGrid::SafeDownCast(
                outInfo->Get(vtkDataObject::DATA_OBJECT()));

            // based on the selected tool, run the appropriate algorithm
            switch (currentTool)
            {
                case SELECTION_TOOL::OBB:
                {
                    selectByOrientedBox();
                    break;
                }
                case SELECTION_TOOL::PLANE:
                {
                    selectByPlane();
                    break;
                }
            }
            if (p_blankSelected)
            {
                if (p_keepPoints)
                    blankSelectedKeepPoints(output);
                else
                    blankSelected(output);
            }
            return 1;
        }


        void vtkSelectionTools::UseSelectByOrientedBox(double origin[3], double axes[3][3])
        {
            // copy input values
            for (int i = 0; i < 3; i++) // for each axis
            {
                OBB->Corner[i] = origin[i];
                for (int j = 0; j < 3; j++) // for x, y, z component
                    OBB->Axes[i][j] = axes[i][j];
            }
            currentTool = SELECTION_TOOL::OBB;
            this->Modified();
        }

        void vtkSelectionTools::UseSelectByPlane(double pointInPlane[3], double planeNormal[3])
        {
            // copy input values
            for (int i = 0; i < 3; i++) // for each axis
            {
                this->pointInPlane[i] = pointInPlane[i];
                this->planeNormal[i] = planeNormal[i];
            }
            currentTool = SELECTION_TOOL::PLANE;
            this->Modified();
        }
                
        vector<vtkSmartPointer<vtkPoints>> vtkSelectionTools::CreateSpatialClusters(vtkSmartPointer<vtkPoints> points, 
            double splitDistanceRatio, int minClusterSize)
        {
            vector<vtkSmartPointer<vtkPoints>> ret;
            if (points->GetNumberOfPoints() == 0)
                return ret;
            if (points->GetNumberOfPoints() > minClusterSize) // to have some corner case break condition
            {
                double size[3];
                vtkOBBNodePtr parent = std::make_shared<vtkOBBNodeWNeighbors>();
                vtkOBBTree::ComputeOBB(points, parent->Corner, parent->Axes[0], parent->Axes[1], parent->Axes[2], size);
                // attempt split along any axes
                double secondChildEnpoint[3];
                vtkIdType nPoints = points->GetNumberOfPoints();
                double currPoint[3];
                double currPointToPlanePoint[3];
                // get point IDs in both halves of the split box
                vtkSmartPointer<vtkPoints> firstChildPoints = vtkSmartPointer<vtkPoints>::New();
                firstChildPoints->Initialize();
                vtkSmartPointer<vtkPoints> secondChildPoints = vtkSmartPointer<vtkPoints>::New();
                secondChildPoints->Initialize();
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                        secondChildEnpoint[j] = parent->Corner[j] + parent->Axes[i][j] * 0.5; // this is a point in the second half's splitting plane - 0.5 = exactly in half

                    firstChildPoints->Reset();
                    secondChildPoints->Reset();
                    // look for the points that are closest to the splitting plane from each side
                    double closestNegativeDist = VTK_DOUBLE_MAX, closestPositiveDist = VTK_DOUBLE_MAX;
                    vtkIdType closestNegID = 0, closestPosID = 0;
                    double normal[3] = { parent->Axes[i][0], parent->Axes[i][1], parent->Axes[i][2] };
                    vtkMath::Normalize(normal);
                    for (vtkIdType j = 0; j < nPoints; j++)
                    {
                        // check the direction it has in regard to the plane
                        points->GetPoint(j, currPoint);
                        vtkMath::Subtract(currPoint, secondChildEnpoint, currPointToPlanePoint); // currPointToPlanePoint = currPoint - pointInPlane
                        // if the angle between the normal and point-to-plane vector is higher than 90 deg (cos < 0), it is on negative side of the plane
                        double dot = vtkMath::Dot(currPointToPlanePoint, normal);
                        if (dot < 0)
                        {
                            dot = -dot; // absolute value of the distance
                            if (dot < closestNegativeDist)
                            {
                                closestNegativeDist = dot;
                                closestNegID = j;
                            }
                            firstChildPoints->InsertNextPoint(points->GetPoint(j));
                        }
                        else // it is on positive side of the plane
                        {
                            if (dot < closestPositiveDist)
                            {
                                closestPositiveDist = dot;
                                closestPosID = j;
                            }
                            secondChildPoints->InsertNextPoint(points->GetPoint(j));
                        }
                    }
                    if (firstChildPoints->GetNumberOfPoints() == 0 || secondChildPoints->GetNumberOfPoints() == 0) // shouldn't really happen, but if it does, it would make infinite recursion
                        continue;
                    // if the extreme points on both sides of the splitting plane are significantly distant from each other, register the split
                    if ((closestNegativeDist + closestPositiveDist) * (closestNegativeDist + closestPositiveDist) // test square of the distance
                            > vtkMath::Dot(parent->Axes[i], parent->Axes[i]) * splitDistanceRatio) // against 0.2 of square length of the splitting axes
                    {
                        vector<vtkSmartPointer<vtkPoints>> clusterFirstChild = CreateSpatialClusters(firstChildPoints, splitDistanceRatio);
                        vector<vtkSmartPointer<vtkPoints>> clusterSecondChild = CreateSpatialClusters(secondChildPoints, splitDistanceRatio);
                        if (clusterFirstChild.size() > 0)
                            ret.insert(ret.end(), clusterFirstChild.begin(), clusterFirstChild.end());
                        if (clusterSecondChild.size() > 0)
                            ret.insert(ret.end(), clusterSecondChild.begin(), clusterSecondChild.end());
                        return ret; // split was already made, no point in testing the other axes
                    }
                }
            }
            ret.push_back(points); // if no split was made, return the original points
            return ret;
            /*
             tried using k-means, but it does not work very well in my tests
            // Get the points into the format needed for KMeans
            vtkSmartPointer<vtkTable> inputData =
                vtkSmartPointer<vtkTable>::New();

            // put coordinates to separate bins for each axis
            for (int c = 0; c < 3; ++c)
            {
                std::stringstream colName;
                colName << "coord " << c;
                vtkSmartPointer<vtkDoubleArray> doubleArray = vtkSmartPointer<vtkDoubleArray>::New();
                doubleArray->SetNumberOfComponents(1);
                doubleArray->SetName(colName.str().c_str());
                doubleArray->SetNumberOfTuples(points->GetNumberOfPoints());

                for (int r = 0; r < points->GetNumberOfPoints(); ++r)
                {
                    double p[3];
                    points->GetPoint(r, p);

                    doubleArray->SetValue(r, p[c]);
                }

                inputData->AddColumn(doubleArray);
            }


            vtkSmartPointer<vtkKMeansStatistics> kMeansStatistics = vtkSmartPointer<vtkKMeansStatistics>::New();

            kMeansStatistics->SetInputData(vtkStatisticsAlgorithm::INPUT_DATA, inputData);
            kMeansStatistics->SetColumnStatus(inputData->GetColumnName(0), 1);
            kMeansStatistics->SetColumnStatus(inputData->GetColumnName(1), 1);
            kMeansStatistics->SetColumnStatus(inputData->GetColumnName(2), 1);
            std::cout << kMeansStatistics->GetMaxNumIterations() << std::endl;
            kMeansStatistics->SetMaxNumIterations(200);
            kMeansStatistics->RequestSelectedColumns();
            kMeansStatistics->SetLearnOption(true);
            kMeansStatistics->SetAssessOption(true);
            kMeansStatistics->Update();

            vtkTable* outputTable = vtkTable::SafeDownCast(
                vtkMultiBlockDataSet::SafeDownCast(kMeansStatistics->GetOutputDataObject(vtkStatisticsAlgorithm::OUTPUT_MODEL))->GetBlock(0));

            unsigned int totalNumberOfClusters = outputTable->GetNumberOfRows();
            std::cout << "total number of clusters: " << totalNumberOfClusters << std::endl;
            boost::container::vector<vtkSmartPointer<vtkPoints>> ret;

            for (unsigned int i = 0; i < totalNumberOfClusters; i++)
                ret.push_back(vtkSmartPointer<vtkPoints>::New());
            std::cout << "cluster arrays prepared" << std::endl;
            for (vtkIdType r = 0; r < kMeansStatistics->GetOutput()->GetNumberOfRows(); r++)
            {
                vtkVariant v = kMeansStatistics->GetOutput()->GetValue(r, kMeansStatistics->GetOutput()->GetNumberOfColumns() - 1);
               // std::cout << "Point " << r << " is in cluster " << v.ToInt() << std::endl;
                if (v.ToInt() >= totalNumberOfClusters)
                    std::cout << v.ToInt() << std::endl;
                else
                    ret[v.ToInt()]->InsertNextPoint(points->GetPoint(r));
            }
            std::cout << "clustering finished" << std::endl;
            return ret;*/
        }

        vector<vtkOBBNodePtr> vtkSelectionTools::GenerateOBBs(vector<vtkSmartPointer<vtkUnstructuredGrid>> meshes,
            bool mergeOverlapping)
        {
            double size[3];
            vector<vtkOBBNodePtr> OBBNodes;
            for (auto &mesh : meshes)
            {
                vtkOBBNodePtr node = std::make_shared<vtkOBBNodeWNeighbors>();
                vtkOBBTree::ComputeOBB(mesh->GetPoints(), node->Corner, node->Axes[0], node->Axes[1], node->Axes[2], size);

                OBBNodes.push_back(node); // pointer to the thirst item = pointer to the beggining of the array
            }
            if (mergeOverlapping)
                MergeOverlappingBoxes(OBBNodes);
            return OBBNodes;
        }

        void vtkSelectionTools::MergeOverlappingBoxes(vector<vtkOBBNodePtr> &OBBNodes)
        {
            vector<vtkSmartPointer<vtkPolyData>> OBBs;
            // first create a polydata representation for the boxes
            for (int i = 0; i < OBBNodes.size(); i++) // for each OBB
                OBBs.push_back(OBBNodes[i]->GenerateAsPolydata());

            // merge overlapping bounding boxes					
            vtkSmartPointer<vtkOBBTree> tree = vtkSmartPointer<vtkOBBTree>::New();
            double size[3]; // not used
            auto nOBBs = OBBNodes.size();
            for (size_t i = 0; i < nOBBs; i++) // for each OBB
            {
                vtkOBBNode *node1 = OBBNodes[i].get();

                for (size_t k = i + 1; k < nOBBs; k++) // test against each other OBB for intersection
                {
                    vtkOBBNode *node2 = OBBNodes[k].get();
                    if (!tree->DisjointOBBNodes(node1, node2, 0)) // if they are not disjoint (returns 0 if they are overlapping)
                    {
                        // insert all points of the second OBB to the first polydata 
                        OBBs[i]->GetPoints()->InsertPoints(OBBs[i]->GetPoints()->GetNumberOfPoints(),
                            OBBs[k]->GetPoints()->GetNumberOfPoints(), 0, OBBs[k]->GetPoints());
                        // recompute the OBB and erase the second one
                        vtkOBBTree::ComputeOBB(OBBs[i]->GetPoints(), node1->Corner, node1->Axes[0], node1->Axes[1], node1->Axes[2], size);
                        OBBNodes.erase(OBBNodes.begin() + k);
                        OBBs.erase(OBBs.begin() + k);
                        // reset the loops - a bit inefficient but should be ok
                        i = -1;
                        break;
                    }
                }
            }
        }

        boost::container::vector<std::pair<vtkOBBNodePtr, vector<vtkSmartPointer<vtkIntArray>>>> vtkSelectionTools::SplitOBBRecursivelyByPoints
            (vtkOBBNodePtr OBB, vtkSmartPointer<vtkPoints> data, vector<std::pair<vtkSmartPointer<vtkIntArray>, int>> pointIndicesAndLimits, double overlap)
        {
            vector<std::pair<vtkOBBNodePtr, vector<vtkSmartPointer<vtkIntArray>>>> ret;
            vector<vtkSmartPointer<vtkIntArray>> limitIntArrays;
            for (auto & limitRule : pointIndicesAndLimits)
            {
                if (limitRule.first->GetNumberOfTuples() < limitRule.second) // if there is no need to subdivide the box based on this criteria, store it
                    limitIntArrays.push_back(limitRule.first);
            }
            if (limitIntArrays.size() == pointIndicesAndLimits.size()) // if all criteria has been satisfied, return
            {
                ret.push_back(std::pair<vtkOBBNodePtr, vector<vtkSmartPointer<vtkIntArray>>>(OBB, limitIntArrays));
                return ret;
            }

            // split the longest side - it is the one on index zero, create the two bounding boxes
            vtkOBBNodePtr firstChild = std::make_shared<vtkOBBNodeWNeighbors>();
            vtkOBBNodePtr secondChild = std::make_shared<vtkOBBNodeWNeighbors>();

            int splitAxisAfterReorder = SplitOBB(0, OBB, firstChild, secondChild, overlap);

            vector<std::pair<vtkSmartPointer<vtkIntArray>, int >> firstChildPointsIndc;
            vector<std::pair<vtkSmartPointer<vtkIntArray>, int >> secondChildPointsIndc;
            // the inverse normal for the splitting plane - for culling on the other direction
            double normalReversed[3];
            double secondChildEnpoint[3];
            for (int i = 0; i < 3; i++)
            {
                normalReversed[i] = -OBB->Axes[0][i];
                secondChildEnpoint[i] = secondChild->Corner[i] + secondChild->Axes[splitAxisAfterReorder][i]; // this is a point in the second half's splitting plane
            }
            // for each limit array and both halves, do the culling
            for (auto & limitRule : pointIndicesAndLimits)
            {
                // the first half is the one with the new corner - cull everything that is behind this new corner
                firstChildPointsIndc.push_back(std::pair<vtkSmartPointer<vtkIntArray>, int>
                    (CullPointsByClipPlane(normalReversed, firstChild->Corner, data, limitRule.first), limitRule.second));
                // the second half is the one with original corner - cull everything after the endpoint along the main axis
                secondChildPointsIndc.push_back(std::pair<vtkSmartPointer<vtkIntArray>, int>
                    (CullPointsByClipPlane(OBB->Axes[0], secondChildEnpoint, data, limitRule.first), limitRule.second));
            }

            // recursively subdivide both halves and add the results to the return vector
            auto firstHalfResult = SplitOBBRecursivelyByPoints(firstChild, data, firstChildPointsIndc, overlap);
            ret.insert(ret.end(), firstHalfResult.begin(), firstHalfResult.end());
            auto secondHalfResult = SplitOBBRecursivelyByPoints(secondChild, data, secondChildPointsIndc, overlap);
            ret.insert(ret.end(), secondHalfResult.begin(), secondHalfResult.end());
            return ret;
        }

        int vtkSelectionTools::SplitOBB(int splitAxisIndex, vtkOBBNodePtr parent, vtkOBBNodePtr firstChild, vtkOBBNodePtr secondChild, double overlap)
        {
            for (int i = 0; i < 3; i++)
            {
                // create bounding boxes - they are the same boxes only with different corners
                firstChild->Axes[0][i] = parent->Axes[0][i] * (splitAxisIndex == 0 ? (0.5 + overlap) : 1);
                firstChild->Axes[1][i] = parent->Axes[1][i] * (splitAxisIndex == 1 ? (0.5 + overlap) : 1);
                firstChild->Axes[2][i] = parent->Axes[2][i] * (splitAxisIndex == 2 ? (0.5 + overlap) : 1);
                firstChild->Corner[i] = parent->Corner[i] + (parent->Axes[splitAxisIndex][i] * (0.5 - overlap)); // split the box in the middle of longest side - overlap

                // the boxes are the same, only origin in a different corner
                secondChild->Axes[0][i] = firstChild->Axes[0][i];
                secondChild->Axes[1][i] = firstChild->Axes[1][i];
                secondChild->Axes[2][i] = firstChild->Axes[2][i];
                secondChild->Corner[i] = parent->Corner[i];
            }
            // reorganize neighborhood
            double temp[3], oppCorner[3];
            double splitAxisSize = vtkMath::Norm(firstChild->Axes[splitAxisIndex]);
            for (int i = 0; i < 3; i++)
            {
                for (int againstOrAlong = 0; againstOrAlong < 2; againstOrAlong++)
                {
                    firstChild->faceNeighbors[i * 2 + againstOrAlong].clear();
                    secondChild->faceNeighbors[i * 2 + againstOrAlong].clear();

                    // replace the parent box in it's neighbors by the two children
                    for (vtkOBBNodePtr n : parent->faceNeighbors[i * 2 + againstOrAlong]) // for each neighbor in the current direction (i*2+againstOrAlong)
                    {
                        bool done = false;
                        for (int k = 0; k < 6 && !done; k++) // find the direction of the neighbor in which this node is the neighbor
                        {
                            for (auto iter = n->faceNeighbors[k].begin(); iter != n->faceNeighbors[k].end(); iter++)
                            {
                                if (*iter == parent) // we found the current node -> process
                                {
                                    n->faceNeighbors[k].erase(iter);
                                    if (i == splitAxisIndex) // if this is a neighbor along/against the split axis, only one of the children will be adjacent to it
                                    {
                                        if (againstOrAlong == 0) // if the neighbor is along the axis, it will be adjacent to the first child
                                        {
                                            n->faceNeighbors[k].push_back(firstChild);
                                            firstChild->faceNeighbors[i * 2 + againstOrAlong].push_back(n);
                                        }
                                        else
                                        {
                                            n->faceNeighbors[k].push_back(secondChild);
                                            secondChild->faceNeighbors[i * 2 + againstOrAlong].push_back(n);
                                        }
                                    }
                                    else // if this is not the split axis, we have to compare the span of the boxes - both children can be neighbors, but also only one can be
                                    {
                                        // check the distance between the corners of the two maybe-neighbors
                                        // if it they are closer than the new child is to its endpoint, they are neighbors, otherwise, they are not
                                        vtkMath::Subtract(n->Corner, firstChild->Corner, temp);
                                        // must project the distance on the split axis - that is the distance we care about
                                        double cornerDistance = vtkMath::Dot(temp, firstChild->Axes[splitAxisIndex]) / splitAxisSize;
                                        // compute the opposite corner of the neighbor
                                        vtkMath::Add(n->Corner, n->Axes[0], oppCorner);
                                        vtkMath::Add(oppCorner, n->Axes[1], oppCorner);
                                        vtkMath::Add(oppCorner, n->Axes[2], oppCorner);
                                        vtkMath::Subtract(oppCorner, firstChild->Corner, temp);
                                        double oppCornerDistance = vtkMath::Dot(temp, firstChild->Axes[splitAxisIndex]) / splitAxisSize;

                                        // either the corner of the neighbor must be between the corner of the new child and its end
                                        // or can be "behind" the new child, but then must be long enough to reach ahead of the corner of the child
                                        // => the opposite corner must be ahead of the corner of the child (all this projected in the split axis)
                                        if ((cornerDistance >= 0 && cornerDistance < splitAxisSize)
                                            || (cornerDistance < 0 && oppCornerDistance > 0))
                                        {
                                            n->faceNeighbors[k].push_back(firstChild);
                                            firstChild->faceNeighbors[i * 2 + againstOrAlong].push_back(n);
                                        }
                                        vtkMath::Subtract(n->Corner, secondChild->Corner, temp);
                                        cornerDistance = vtkMath::Dot(temp, secondChild->Axes[splitAxisIndex]) / splitAxisSize;
                                        vtkMath::Subtract(oppCorner, secondChild->Corner, temp);
                                        oppCornerDistance = vtkMath::Dot(temp, secondChild->Axes[splitAxisIndex]) / splitAxisSize;
                                        if ((cornerDistance >= 0 && cornerDistance < splitAxisSize)
                                            || (cornerDistance < 0 && oppCornerDistance > 0))
                                        {
                                            n->faceNeighbors[k].push_back(secondChild);
                                            secondChild->faceNeighbors[i * 2 + againstOrAlong].push_back(n);
                                        }
                                    }
                                    done = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            // each sibling becomes neighbor to each other
            firstChild->faceNeighbors[splitAxisIndex * 2 + 1].clear();
            firstChild->faceNeighbors[splitAxisIndex * 2 + 1].push_back(secondChild);
            secondChild->faceNeighbors[splitAxisIndex * 2].clear();
            secondChild->faceNeighbors[splitAxisIndex * 2].push_back(firstChild);

            int splitAxesAfterReorder = splitAxisIndex;
            // reorder axes lengths so that 0 is longest, 2 is shortest for both boxes
            for (int i = 1; i < 3; i++)
            {
                if (vtkMath::Dot(firstChild->Axes[i - 1], firstChild->Axes[i - 1]) < vtkMath::Dot(firstChild->Axes[i], firstChild->Axes[i]))
                {
                    for (int j = 0; j < 3; j++)
                    {
                        double temp = firstChild->Axes[i - 1][j];
                        firstChild->Axes[i - 1][j] = firstChild->Axes[i][j];
                        secondChild->Axes[i - 1][j] = secondChild->Axes[i][j]; // the second child has the same axes - switch it too
                        firstChild->Axes[i][j] = temp;
                        secondChild->Axes[i][j] = temp;
                    }
                    // swap neighbors
                    firstChild->faceNeighbors[(i - 1) * 2].swap(firstChild->faceNeighbors[i * 2]);
                    secondChild->faceNeighbors[(i - 1) * 2].swap(secondChild->faceNeighbors[i * 2]);
                    firstChild->faceNeighbors[(i - 1) * 2 + 1].swap(firstChild->faceNeighbors[i * 2 + 1]);
                    secondChild->faceNeighbors[(i - 1) * 2 + 1].swap(secondChild->faceNeighbors[i * 2 + 1]);

                    if (i == splitAxesAfterReorder)
                        splitAxesAfterReorder = i - 1;
                    else if (i - 1 == splitAxesAfterReorder)
                        splitAxesAfterReorder = i;
                }
            }
            return splitAxesAfterReorder;
        }

        vtkSmartPointer<vtkBitArray> vtkSelectionTools::ObtainSelectionArrayCells(vtkPointSet *data, bool reset)
        {
            if (!data) return nullptr;
            vtkSmartPointer<vtkBitArray> sel_prim = vtkBitArray::SafeDownCast(data->GetCellData()->GetArray(SELECTED_PRIMITIVES));
            if (!sel_prim || sel_prim->GetNumberOfTuples() != data->GetNumberOfCells()) // if it does not have anything selected, initialize the selection array
            {
                sel_prim = vtkSmartPointer<vtkBitArray>::New();
                sel_prim->SetNumberOfValues(data->GetNumberOfCells());
                sel_prim->SetName(SELECTED_PRIMITIVES);
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);
                data->GetCellData()->AddArray(sel_prim);
            }
            else if (reset)
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);
            return sel_prim;
        }

        vtkSmartPointer<vtkBitArray> vtkSelectionTools::ObtainSelectionArrayPoints(vtkPointSet *data, bool reset)
        {
            if (!data) return nullptr;
            vtkSmartPointer<vtkBitArray> sel_prim = vtkBitArray::SafeDownCast(data->GetPointData()->GetArray(SELECTED_PRIMITIVES));
            if (!sel_prim || sel_prim->GetNumberOfTuples() != data->GetNumberOfPoints()) // if it does not have anything selected, or is wrong size initialize the selection array
            {
                sel_prim = vtkSmartPointer<vtkBitArray>::New();
                sel_prim->SetNumberOfValues(data->GetNumberOfPoints());
                sel_prim->SetName(SELECTED_PRIMITIVES);
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);
                data->GetPointData()->AddArray(sel_prim);
            }
            else if (reset)
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);
            return sel_prim;
        }

        vtkSmartPointer<vtkIntArray> vtkSelectionTools::ObtainCellPixelHitArray(vtkPointSet *data, bool reset)
        {
            if (!data) return nullptr;
            vtkSmartPointer<vtkIntArray> sel_prim = vtkIntArray::SafeDownCast(data->GetCellData()->GetArray(HIT_PIXEL_COUNT));
            if (!sel_prim || sel_prim->GetNumberOfTuples() != data->GetNumberOfCells()) // if it does not have anything selected, initialize the selection array
            {
                sel_prim = vtkSmartPointer<vtkIntArray>::New();
                sel_prim->SetNumberOfValues(data->GetNumberOfCells());
                sel_prim->SetName(HIT_PIXEL_COUNT);
                sel_prim->FillComponent(0, 0);
                data->GetCellData()->AddArray(sel_prim);
            }
            else if (reset)
                sel_prim->FillComponent(0, 0);
            return sel_prim;
        }

        vtkSmartPointer<vtkBitArray> vtkSelectionTools::ObtainArrayHighlightedGroupOfPoints(vtkPointSet *data, bool reset)
        {
            if (!data) return nullptr;
            vtkSmartPointer<vtkBitArray> sel_prim = vtkBitArray::SafeDownCast(data->GetPointData()->GetArray(SELECTED_POINT_GROUPS));
            if (!sel_prim || sel_prim->GetNumberOfTuples() != data->GetNumberOfPoints()) // if it does not have anything selected, initialize the selection array
            {
                sel_prim = vtkSmartPointer<vtkBitArray>::New();
                sel_prim->SetNumberOfValues(data->GetNumberOfPoints());
                sel_prim->SetName(SELECTED_POINT_GROUPS);
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);
                data->GetPointData()->AddArray(sel_prim);
            }
            else if (reset)
                sel_prim->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED);

            return sel_prim;
        }

        void vtkSelectionTools::selectByOrientedBox()
        {
            // create a representation of the box as six planes for convenience
            double planePoints[6][3];
            double planeNormals[6][3];
            for (int i = 0; i < 3; i++) // for each axis
            {
                for (int j = 0; j < 3; j++) // for x, y, z component
                {
                    // first three planes can use the origin, the other three can use the endpoints of the axes, one for each remaining plane
                    // order the planes so that opposite ones are next to each other - should make culling more efficient 
                    // (first cull by one plane, then by the opposite one, by then most primitives should be culled)
                    planePoints[i * 2][j] = OBB->Corner[j];
                    planePoints[i * 2 + 1][j] = OBB->Corner[j] + OBB->Axes[i][j];
                    // for the three planes using the axes endpoints, normals are equal to the axes
                    planeNormals[i * 2 + 1][j] = OBB->Axes[i][j];
                    // for the planes using the origin, normals are opposite to the axes. the order is arbitrary
                    planeNormals[i * 2][j] = -OBB->Axes[i][j];
                }
            }

            // now select for every input dataset
            for (int currConn = 0; currConn < GetNumberOfInputConnections(0); currConn++)
            {
                vtkPointSet *data = vtkPointSet::SafeDownCast(this->GetInputDataObject(0, currConn));
                if (data)
                {
                    if ((targetType & SELECTION_TARGET::NODES) != 0) // if we wanted to select nodes, we just need to mark them, they are already found
                    {
                        vtkPoints *dataPoints = data->GetPoints();
                        vtkSmartPointer<vtkIntArray>ret = 0;
                        // select all points by checking them against all planes of the box, using the clipMask to reduce the number of tested points in each iteration
                        for (int i = 0; i < 6; i++)
                            ret = CullPointsByClipPlane(planeNormals[i], planePoints[i], dataPoints, ret);
                        markPointsAsSelected(data, ret);
                    }
                    if ((targetType & SELECTION_TARGET::ELEMENTS) != 0 || (targetType & SELECTION_TARGET::FACES) != 0)
                    {
                        // first cull only cells that have all vertices outside any of the plane
                        // that leaves us only with cells that are inside or in a close vicinity of the box
                        vtkSmartPointer<vtkIntArray>ret = 0;
                        for (int i = 0; i < 6; i++)
                            ret = CullCellsByClipPlane(planeNormals[i], planePoints[i], data, ret);
                        // now check the points of the remaining cells, if at least one is inside all six planes, the cell is within the box
                        vtkSmartPointer<vtkIntArray> isDefinetelyInside = vtkSmartPointer<vtkIntArray>::New();
                        vtkSmartPointer<vtkIntArray> needToCheckEdges = vtkSmartPointer<vtkIntArray>::New();
                        isDefinetelyInside->Initialize();
                        isDefinetelyInside->SetNumberOfComponents(1);
                        needToCheckEdges->Initialize();
                        needToCheckEdges->SetNumberOfComponents(1);
                        double currPoint[3];
                        double currPointToPlanePoint[3];
                        vtkIdType nCells = ret->GetNumberOfTuples();
                        for (vtkIdType i = 0; i < nCells; i++) // for each remaining cell
                        {
                            vtkIdList *currCellPoints = data->GetCell(ret->GetValue(i))->GetPointIds();
                            vtkIdType nCurrCellPoints = currCellPoints->GetNumberOfIds();
                            vtkIdType j = 0;
                            for (; j < nCurrCellPoints; j++) // for each point of that cell
                            {
                                int k = 0;
                                for (; k < 6; k++) // for each plane
                                {
                                    data->GetPoint(currCellPoints->GetId(j), currPoint);
                                    vtkMath::Subtract(currPoint, planePoints[k], currPointToPlanePoint); // currPointToPlanePoint = currPoint - pointInPlane
                                    // if the angle between the normal and point-to-plane vector is lower than 90 deg (cos > 0), the point is outside
                                    if (vtkMath::Dot(currPointToPlanePoint, planeNormals[k]) > 0)
                                        break; // this point is outside, need to check another one
                                }
                                if (k == 6) // k == 6 unless the previous test failed (ended by the break), so this means at least this point is inside the box -> the cell will be selected
                                {
                                    isDefinetelyInside->InsertNextValue(ret->GetValue(i));
                                    break;
                                }
                            }
                            if (j == nCurrCellPoints) // all points have been checked without breaking -> none of them is inside the box, have to run the next test for this cell
                                needToCheckEdges->InsertNextValue(ret->GetValue(i));
                        }
                        // lastly, for the remaining cells, check if their edges cross at least one wall of the box, if they dont, cull them
                        // create polygonal representation of the walls of the box
                        vtkSmartPointer<vtkPolyData> OBBpoly = OBB->GenerateAsPolydata();
                        vtkIdType nNeedToCheck = needToCheckEdges->GetNumberOfTuples();
                        for (vtkIdType i = 0; i < nNeedToCheck; i++)
                        {
                            int val = needToCheckEdges->GetValue(i);
                            if (intersectsCellEdgesWithOBB(data, val, OBBpoly))
                                isDefinetelyInside->InsertNextValue(val);
                        }
                        // finally, mark the selected cells
                        markCellsAsSelected(data, isDefinetelyInside);
                    }
                }
                // else it is not a pointSet we dont process it, maybe print a warning? should never happen though
            }
        }

        void vtkSelectionTools::selectByPlane()
        {
            // select for only the first data set
            vtkPointSet *data = vtkPointSet::SafeDownCast(this->GetInputDataObject(0, 0));
            if (data)
            {
                if (targetType == SELECTION_TARGET::ELEMENTS || targetType == SELECTION_TARGET::FACES)
                {
                    vtkSmartPointer<vtkIntArray> ret = CullCellsByClipPlane(planeNormal, pointInPlane, data); // find the cells on the negative side of the plane
                    markCellsAsSelected(data, ret); // marke them in the selection array
                }
                if (targetType == SELECTION_TARGET::NODES)
                {
                    vtkSmartPointer<vtkIntArray> ret = CullPointsByClipPlane(planeNormal, pointInPlane, data->GetPoints());
                    markPointsAsSelected(data, ret);
                }
            }
        }

        vtkSmartPointer<vtkIntArray> vtkSelectionTools::CullPointsByClipPlane(double normal[3], double pointInPlane[3], vtkPoints *data, vtkSmartPointer<vtkIntArray> clipMask)
        {
            vtkSmartPointer<vtkIntArray> selected = vtkSmartPointer<vtkIntArray>::New();
            selected->Initialize();
            selected->SetNumberOfComponents(1);
            double currPoint[3];
            double currPointToPlanePoint[3];
            bool isMaskOff = clipMask == 0;
            vtkIdType size = isMaskOff ? data->GetNumberOfPoints() : clipMask->GetNumberOfTuples();
            for (vtkIdType i = 0; i < size; i++) // for each point
            {
                // check the direction it has in regard to the plane
                int currPointID = isMaskOff ? i : clipMask->GetValue(i);// I assume the compiler will optimize this to not check it in every iteration, otherwise two different loops might be better
                data->GetPoint(currPointID, currPoint);
                vtkMath::Subtract(currPoint, pointInPlane, currPointToPlanePoint); // currPointToPlanePoint = currPoint - pointInPlane
                // if the angle between the normal and point-to-plane vector is higher than 90 deg (cos < 0), select the point, otherwise it will be culled
                if (vtkMath::Dot(currPointToPlanePoint, normal) <= 0)
                    selected->InsertNextValue(currPointID);
            }
            return selected;
        }

        vtkSmartPointer<vtkIntArray> vtkSelectionTools::CullCellsByClipPlane(double normal[3], double pointInPlane[3], vtkPointSet *data, vtkSmartPointer<vtkIntArray> clipMask)
        {
            vtkSmartPointer<vtkIntArray> selected = vtkSmartPointer<vtkIntArray>::New();
            selected->Initialize();
            selected->SetNumberOfComponents(1);
            double currPoint[3];
            double currPointToPlanePoint[3];
            bool isMaskOff = clipMask == 0;
            vtkIdType size = isMaskOff ? data->GetNumberOfCells() : (clipMask->GetNumberOfTuples());
            for (vtkIdType i = 0; i < size; i++) // for each cell
            {
                int currCellID = isMaskOff ? i : clipMask->GetValue(i);
                vtkIdList *currCellPoints = data->GetCell(currCellID)->GetPointIds();
                vtkIdType nCellPoints = currCellPoints->GetNumberOfIds();
                for (vtkIdType j = 0; j < nCellPoints; j++) // for each point of the cell
                {
                    data->GetPoint(currCellPoints->GetId(j), currPoint);
                    // check the direction it has in regard to the plane
                    vtkMath::Subtract(currPoint, pointInPlane, currPointToPlanePoint); // currPointToPlanePoint = currPoint - pointInPlane
                    // if the angle between the normal and point-to-plane vector is higher than 90 deg (cos < 0),
                    // at least this point is on the non-culling side of the plane, so do not cull this cell
                    if (vtkMath::Dot(currPointToPlanePoint, normal) <= 0)
                    {
                        selected->InsertNextValue(currCellID); // store the cell as not-culled
                        break; // there is no need to continue, we know we wont be culling this one
                    }
                }
            }
            return selected;
        }

        void vtkSelectionTools::SelectPointsByCells(vtkSmartPointer<vtkPointSet> data, bool inverse)
        {
            vtkSmartPointer<vtkBitArray> sel_cells = ObtainSelectionArrayCells(data, false);
            vtkSmartPointer<vtkBitArray> sel_points = ObtainSelectionArrayPoints(data, false);
            int selVal = inverse ? IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED;
            vtkIdType nCells = sel_cells->GetNumberOfTuples();
            for (vtkIdType i = 0; i < nCells; i++) // for each cell
            {
                if (sel_cells->GetValue(i) == IS_PRIMITIVE_SELECTED) // if it is selected
                {
                    vtkCell *curCell = data->GetCell(i);
                    vtkIdList *points = curCell->GetPointIds();
                    vtkIdType nPoints = points->GetNumberOfIds();
                    for (vtkIdType j = 0; j < nPoints; j++) // for each point, select the point
                        sel_points->SetValue(points->GetId(j), selVal);
                }
            }
            sel_points->Modified();
            data->Modified();
        }

        void vtkSelectionTools::SelectCellsByPoints(vtkSmartPointer<vtkPointSet> data)
        {
            vtkSmartPointer<vtkBitArray> sel_cells = ObtainSelectionArrayCells(data, false);
            vtkSmartPointer<vtkBitArray> sel_points = ObtainSelectionArrayPoints(data, false);
            vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
            vtkIdType nPoints = sel_points->GetNumberOfTuples();
            for (vtkIdType i = 0; i < nPoints; i++) // for each point
            {
                if (sel_points->GetValue(i) == IS_PRIMITIVE_SELECTED)
                {                    
                    data->GetPointCells(i, cellIds); // GetPointsCells resets the cellIds list, no need to do it ourselves
                    vtkIdType nCells = cellIds->GetNumberOfIds();
                    for (vtkIdType j = 0; j < nCells; j++)
                        sel_cells->SetValue(cellIds->GetId(j), IS_PRIMITIVE_SELECTED);
                }
            }
            sel_cells->Modified();
            data->Modified();
        }

        void vtkSelectionTools::SelectCellsByScalars(vtkSmartPointer<vtkPointSet> data, std::string scalarArrayName,
            boost::container::vector <std::pair<double, double>> *selectionRanges, bool reset)
        {
            vtkSmartPointer<vtkDataArray> scalars = data->GetCellData()->GetArray(scalarArrayName.data());
            if (scalars)
            {
                vtkSmartPointer<vtkBitArray> sel_prim = ObtainSelectionArrayCells(data, reset);
                vtkIdType nPrims = sel_prim->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nPrims; i++)
                {
                    double val = scalars->GetTuple1(i);
                    for (auto &selRange : *selectionRanges)
                    {
                        if (val >= selRange.first && val <= selRange.second)
                            sel_prim->SetValue(i, IS_PRIMITIVE_SELECTED);
                    }
                }
                sel_prim->Modified();
            }
        }

        std::vector<vtkSmartPointer<vtkUnstructuredGrid>> vtkSelectionTools::ExtractSelectedParts(vtkSmartPointer<vtkPointSet> data)
        {
            auto sel_cells = vtkSelectionTools::ObtainSelectionArrayCells(data, false);
            vtkIdType nPoints = data->GetNumberOfPoints();
            vtkIdType nCells = data->GetNumberOfCells();
            // for each node of the orig mesh, keep track if it has been assigned to some component already (-1 if not), and which one (ID into comps)
            vtkIdType *compIdPoints = new vtkIdType[nPoints];
            std::fill_n(compIdPoints, nPoints, -1);
            std::vector<std::vector<vtkIdType>> compPoints{};
            std::vector<std::vector<vtkIdType>> compCells{};
            for (vtkIdType i = 0; i < nCells; i++)
            {
                if (sel_cells->GetValue(i) == IS_PRIMITIVE_SELECTED)
                {
                    // find the correct component for this cell - if some of its vertices are already part of some component, append it to that component
                    vtkCell *curCell = data->GetCell(i);
                    vtkIdList *cellPoints = curCell->GetPointIds();
                    vtkIdType nCellPoints = cellPoints->GetNumberOfIds();
                    // 3 situations can occur:
                    // no node is part of any component -> create a new component
                    // some of the nodes are part of one component -> append to that component
                    // some nodes are part of one and others of another component -> merge components
                    vtkIdType curCompID = -1;
                    for (vtkIdType j = 0; j < nCellPoints; j++)
                    {
                        vtkIdType nID = cellPoints->GetId(j);
                        vtkIdType nodesCompID = compIdPoints[nID];
                        if (nodesCompID != -1 && nodesCompID != curCompID)
                        {
                            if (curCompID == -1)
                                curCompID = nodesCompID; // we found a node that already belongs to this component, we will be appending to it...
                            else // ...unless there is later some other node that is part of another component, we have to do a merge
                            {
                                vtkIdType preserveID{ curCompID };
                                vtkIdType assimilateID{ nodesCompID };
                                if (compCells[curCompID].size() < compCells[nodesCompID].size())
                                {
                                    preserveID = nodesCompID;
                                    assimilateID = curCompID;
                                    curCompID = nodesCompID; // we are switching the "current component"
                                }

                                // re-assign the associated component for each point of the assimilated component
                                for (vtkIdType id : compPoints[assimilateID])
                                    compIdPoints[id] = preserveID;
                                // merge the points and cells arrays
                                compPoints[preserveID].insert(compPoints[preserveID].end(), 
                                    compPoints[assimilateID].begin(), compPoints[assimilateID].end());
                                compCells[preserveID].insert(compCells[preserveID].end(),
                                    compCells[assimilateID].begin(), compCells[assimilateID].end());
                                // empty the assimilated comps to signal that they have been removed
                                compPoints[assimilateID].clear();
                                compCells[assimilateID].clear();
                            }
                        }
                    }
                    if (curCompID == -1) // no node belongs to any component -> create a new one
                    {
                        size_t newCompID = compPoints.size();
                        compPoints.emplace_back(nCellPoints);
                        compCells.emplace_back(1);
                        compCells[newCompID][0] = i; // insert the current cell ID
                        for (vtkIdType j = 0; j < nCellPoints; j++)
                        {
                            vtkIdType nID = cellPoints->GetId(j);
                            compPoints[newCompID][j] = nID;
                            compIdPoints[nID] = newCompID;
                        }
                    }
                    else
                    {
                        // the component already exists, just add this cell to it and points that have not yet been added
                        for (vtkIdType j = 0; j < nCellPoints; j++)
                        {
                            vtkIdType nID = cellPoints->GetId(j);
                            // it is either -1 or the current comp. if it was other comp, it was already changed in the merger above
                            // i.e. only add points that have not yet been added
                            if (compIdPoints[nID] == -1)
                            {
                                compIdPoints[nID] = curCompID;
                                compPoints[curCompID].push_back(nID);
                            }
                        }
                        compCells[curCompID].push_back(i);
                    }
                }
            }
            delete[] compIdPoints;
             
            // now transform the collections of node and cell ids into individual unstructured grids
            // for each point, has what is its ID in the component it is part of, need this to define cells using new pointIDs
            vtkIdType *inCompID = new vtkIdType[nPoints];
            vtkIdType nComps = static_cast<vtkIdType>(compPoints.size());
            vtkIdType ptIds[32]; // buffer for cell definition - big enough to handle any 2D or 3D elements
            std::vector<vtkSmartPointer<vtkUnstructuredGrid>> comps{};
            comps.reserve(nComps); // there will be at most nComps components, possibly less due to merging
            for (vtkIdType compID = 0; compID < nComps; compID++)
            {
                if (!compPoints[compID].empty()) // components that have been assimilated are empty - don't process those
                {
                    std::vector<vtkIdType> & curPoints = compPoints[compID];
                    vtkIdType nPoints = static_cast<vtkIdType>(curPoints.size());
                    // create new points array with the points of the current component + create map to our original mesh
                    vtkSmartPointer<vtkIdTypeArray> origPoints = vtkSmartPointer<vtkIdTypeArray>::New();
                    origPoints->SetNumberOfComponents(1);
                    origPoints->SetName(ORIGINAL_POINT_IDS);
                    origPoints->SetNumberOfValues(nPoints);
                    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                    points->SetNumberOfPoints(nPoints);
                    for (vtkIdType pointID = 0; pointID < nPoints; pointID++)
                    {
                        vtkIdType nID = curPoints[pointID];
                        points->SetPoint(pointID, data->GetPoint(nID));
                        origPoints->SetValue(pointID, nID);
                        inCompID[nID] = pointID;
                    }
                    std::vector<vtkIdType> & curCells = compCells[compID];
                    vtkIdType nCells = static_cast<vtkIdType>(curCells.size());
                    vtkSmartPointer<vtkUnstructuredGrid> newComp = vtkSmartPointer<vtkUnstructuredGrid>::New();
                    newComp->Allocate(nCells);
                    newComp->SetPoints(points);
                    newComp->GetPointData()->AddArray(origPoints);
                    vtkSmartPointer<vtkIdTypeArray> origCells = vtkSmartPointer<vtkIdTypeArray>::New();
                    origCells->SetNumberOfComponents(1);
                    origCells->SetName(ORIGINAL_CELL_IDS);
                    origCells->SetNumberOfValues(nCells);
                    for (vtkIdType cellID = 0; cellID < nCells; cellID++)
                    {
                        vtkIdType eID = curCells[cellID];
                        vtkCell *curCell = data->GetCell(eID);
                        vtkIdList *curCellPoints = curCell->GetPointIds();
                        vtkIdType nCellPoints = curCellPoints->GetNumberOfIds();
                        for (vtkIdType j = 0; j < nCellPoints; j++)
                            ptIds[j] = inCompID[curCellPoints->GetId(j)]; // inCompID has the new ID inside the component for each node of the original mesh 
                        // insert the new cell and store its original ID
                        newComp->InsertNextCell(curCell->GetCellType(), nCellPoints, ptIds);
                        origCells->SetValue(cellID, eID);
                    }
                    newComp->GetCellData()->AddArray(origCells);
                    comps.push_back(newComp);
                }
            }
            delete[] inCompID;
            return comps;
        }

        bool vtkSelectionTools::ComputePointInScreenRay(double selectionX, double selectionY, bool useParallelProjection,
            vtkRenderer *renderer, double ray1World[3], double ray2World[3])
        {
            int i;
            vtkCamera *camera;
            double cameraPos[4], cameraFP[4];
            double worldCoords[4];
            double *clipRange;
            double ray[3], rayLength;
            double tF, tB;
            double cameraDOP[3];
            double PickPosition[3];

            if (renderer == nullptr)
                return false;

            // Get camera focal point and position. Convert to display (screen)
            // coordinates. We need a depth value for z-buffer.
            camera = renderer->GetActiveCamera();
            camera->GetPosition(cameraPos);
            cameraPos[3] = 1.0;
            camera->GetFocalPoint(cameraFP);
            cameraFP[3] = 1.0;

            renderer->SetWorldPoint(cameraFP[0], cameraFP[1], cameraFP[2], cameraFP[3]);
            renderer->WorldToDisplay();
            double selectionZ = renderer->GetDisplayPoint()[2];


            // Convert the selection point into world coordinates.
            renderer->SetDisplayPoint(selectionX, selectionY, selectionZ);
            renderer->DisplayToWorld();
            renderer->GetWorldPoint(worldCoords);

            if (worldCoords[3] == 0.0)
                return false;

            for (i = 0; i < 3; i++)
            {
                PickPosition[i] = worldCoords[i] / worldCoords[3];
            }

            //  Compute the ray endpoints.  The ray is along the line running from
            //  the camera position to the selection point, starting where this line
            //  intersects the front clipping plane, and terminating where this
            //  line intersects the back clipping plane.
            for (i = 0; i < 3; i++)
            {
                ray[i] = PickPosition[i] - cameraPos[i];
                cameraDOP[i] = cameraFP[i] - cameraPos[i];
            }

            vtkMath::Normalize(cameraDOP);

            if ((rayLength = vtkMath::Dot(cameraDOP, ray)) == 0.0)
                return false;

            clipRange = camera->GetClippingRange();

            if (useParallelProjection)
            {
                tF = clipRange[0] + renderer->GetNearClippingPlaneTolerance() - rayLength;
                tB = clipRange[1] - renderer->GetNearClippingPlaneTolerance() - rayLength;
                for (i = 0; i < 3; i++)
                {
                    ray1World[i] = PickPosition[i] + tF*cameraDOP[i];
                    ray2World[i] = PickPosition[i] + tB*cameraDOP[i];
                }
            }
            else
            {
                tF = clipRange[0] / rayLength;
                tB = clipRange[1] / rayLength;
                for (i = 0; i < 3; i++)
                {
                    ray1World[i] = cameraPos[i] + tF*ray[i];
                    ray2World[i] = cameraPos[i] + tB*ray[i];
                }
            }
            return true;
        }

        bool vtkSelectionTools::ComputeCellRayIntersection(vtkPointSet *data, vtkIdType cellID, double ray1World[3], double ray2World[3], double intersection[3])
        {
            vtkCell *cell = data->GetCell(cellID);
            // process the cell only if it is planar...this condition in its current form might not be exhaustive...
            if (cell->GetNumberOfPoints() < 3 || (cell->GetCellType() != VTKCellType::VTK_TRIANGLE
                && cell->GetCellType() != VTKCellType::VTK_QUAD && cell->GetCellType() != VTKCellType::VTK_POLYGON))
                return false;
            //compute intersection of the ray and the plane in which the cell lies
            double pointInPlane[3];
            double normal[3];
            double cellEdge1[3];
            double cellEdge2[3];
            data->GetPoint(cell->GetPointId(0), pointInPlane); // choose an arbitrary point of the cell as the point in plane
            vtkMath::Subtract(pointInPlane, data->GetPoint(cell->GetPointId(1)), cellEdge1);
            vtkMath::Subtract(pointInPlane, data->GetPoint(cell->GetPointId(2)), cellEdge2);
            vtkMath::Cross(cellEdge1, cellEdge2, normal);
            // compute the intersection
            double t = 0; // we don't care about it, just to shut up compiler warnings about uninitialized variable
            vtkPlane::IntersectWithLine(ray1World, ray2World, normal, pointInPlane, t, intersection);
            return true;
        }

        vtkIdType vtkSelectionTools::FindClosestCellPointToPoint(vtkPointSet *data, vtkIdType cellID, double searchPoint[3])
        {
            // finally, find the closest point of the cell to the intersection
            double minDistance = VTK_DOUBLE_MAX;
            vtkCell *cell = data->GetCell(cellID);
            vtkIdType closestPointID = 0;
            vtkIdType nPoints = cell->GetNumberOfPoints();
            for (vtkIdType i = 0; i < nPoints; i++)
            {
                double distance = vtkMath::Distance2BetweenPoints(searchPoint, data->GetPoint(cell->GetPointId(i)));
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestPointID = cell->GetPointId(i);
                }
            }
            return closestPointID;
        }

        bool vtkSelectionTools::SortVertexRing(vtkSmartPointer<vtkPolyData> data, vtkSmartPointer<vtkIdList> ring,
            std::vector<vtkIdType> &isRing, std::vector<vtkIdType> &sortedRing)
        {
            vtkIdType ringSize = ring->GetNumberOfIds();
            vtkIdType *neighborsA = new vtkIdType[ringSize];
            std::fill(neighborsA, neighborsA + ringSize, -1);
            vtkIdType *neighborsB = new vtkIdType[ringSize];
            std::fill(neighborsB, neighborsB + ringSize, -1);
            vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
            // build info about which ring vertices are neighbors to which
            for (vtkIdType i = 0; i < ringSize; i++)
            {
                // find the neighbors on the ring if they haven't been found yet    
                if (neighborsA[i] == -1 || neighborsB[i] == -1)
                {
                    vtkIdType id = ring->GetId(i);
                    double *point = data->GetPoint(id);
                    data->GetPointCells(id, cellIds);
                    vtkIdType nCells = cellIds->GetNumberOfIds();
                    for (vtkIdType c = 0; c < nCells; c++)
                    {
                        vtkIdType cellIndex = cellIds->GetId(c);
                        vtkIdList *nei = data->GetCell(cellIndex)->GetPointIds();
                        vtkIdType nNeis = nei->GetNumberOfIds();
                        for (vtkIdType k = 0; k < nNeis; k++)
                        {
                            vtkIdType neiID = nei->GetId(k);
                            if (neiID != id && isRing[neiID] != -1 && 
                                // check that the neighbor either does not have neighbors yet assigned, or one of the neighbors is the current vertex
                                // this has to be done in order to prevent backtracking of the ring in cases when one ring vertex is connected by some edge to more than two ring
                                // vertices, i.e. cases like when all three vertices of a triangle are part of the ring (but only two edges can be part of the path)
                                (neighborsA[isRing[neiID]] == -1 || neighborsA[isRing[neiID]] == id || neighborsB[isRing[neiID]] == -1 || neighborsB[isRing[neiID]] == id))
                            {
                                if (neighborsA[i] == -1)
                                {
                                    neighborsA[i] = neiID;
                                    if (neighborsA[isRing[neiID]] == -1)
                                        neighborsA[isRing[neiID]] = id; // set the current point as the neighbor for the found one too
                                    else if (neighborsA[isRing[neiID]] != id)
                                        neighborsB[isRing[neiID]] = id;
                                }
                                else if (neighborsA[i] != neiID) // if this is not the one that was already set as neighbor A, set it as neighbor B and end
                                {
                                    neighborsB[i] = neiID;
                                    if (neighborsA[isRing[neiID]] == -1)
                                        neighborsA[isRing[neiID]] = id; // set the current point as the neighbor for the found one too
                                    else if (neighborsA[isRing[neiID]] != id)
                                        neighborsB[isRing[neiID]] = id;
                                    break;
                                }
                            }
                        }
                        if (neighborsB[i] != -1)
                            break;
                    }
                }
                if (neighborsA[i] == -1 || neighborsB[i] == -1)
                    return false;
            }

            // sort the ring based on the neighborhood
            sortedRing[0] = neighborsA[0];
            sortedRing[1] = ring->GetId(0); // we can start from any of the vertices
            sortedRing[2] = neighborsB[0];  // and go in any direction
            for (vtkIdType i = 3; i < ringSize; i++)
            {
                vtkIdType prevOrigIndex = isRing[sortedRing[i - 1]];
                // the next vertex in the ring is the neighbor of the previous vertex that was not used yet
                sortedRing[i] = (sortedRing[i - 2] == neighborsA[prevOrigIndex] ? neighborsB[prevOrigIndex] : neighborsA[prevOrigIndex]);
            }

#if PIPER_RINGS_DEBUGVIS
            vtkSmartPointer<vtkRenderer> renderer =
                vtkSmartPointer<vtkRenderer>::New();
            vtkSmartPointer<vtkRenderWindow> renderWindow =
                vtkSmartPointer<vtkRenderWindow>::New();
            renderWindow->AddRenderer(renderer);

            renderWindow->SetAlphaBitPlanes(1);
            renderWindow->SetMultiSamples(0);
            renderer->SetUseDepthPeeling(1);
            renderer->SetMaximumNumberOfPeels(10);
            renderer->SetOcclusionRatio(0.01);

            // An interactor
            vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
                vtkSmartPointer<vtkRenderWindowInteractor>::New();
            renderWindowInteractor->SetRenderWindow(renderWindow);
            vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
            vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            vtkSmartPointer<vtkPolyData> ringMesh = vtkSmartPointer<vtkPolyData>::New();
            ringMesh->Allocate();
            vtkSmartPointer<vtkPoints> meshPoints = vtkSmartPointer<vtkPoints>::New();
            ringMesh->SetPoints(meshPoints);
       /* // visualization with cells adjacent to the ring
            std::map<vtkIdType, vtkIdType> mappedPoints;
            std::map<vtkIdType, bool> mappedCells;
            vtkSmartPointer<vtkIdList> p = vtkSmartPointer<vtkIdList>::New();
            for (vtkIdType i = 0; i < ringSize; i++)
            {
                data->GetPointCells(ring->GetId(i), cellIds);
                for (vtkIdType j = 0; j < cellIds->GetNumberOfIds(); j++)
                {
                    if (mappedCells.find(cellIds->GetId(j)) == mappedCells.end())
                    {
                        p->Reset();
                        mappedCells[cellIds->GetId(j)] = true;
                        vtkCell *curCell = data->GetCell(cellIds->GetId(j));
                        vtkIdList *curCellPoints = curCell->GetPointIds();
                        for (vtkIdType i = 0; i < curCellPoints->GetNumberOfIds(); i++)
                        {
                            vtkIdType curpoint = curCellPoints->GetId(i);
                            if (mappedPoints.find(curpoint) == mappedPoints.end())
                                mappedPoints[curpoint] = meshPoints->InsertNextPoint(data->GetPoint(curpoint));
                            p->InsertNextId(mappedPoints[curpoint]);
                        }
                        ringMesh->InsertNextCell(curCell->GetCellType(), p);
                    }
                }
            }*/
            // visualization of the ring only as a polyline
            meshPoints->SetNumberOfPoints(sortedRing.size());
            vtkIdType pts[2];
            pts[0] = sortedRing.size() - 1;
            pts[1] = 0;
            for (size_t i = 0; i < sortedRing.size(); i++)
            {
                std::cout << sortedRing[i] << " ";
                meshPoints->SetPoint(i, data->GetPoint(sortedRing[i]));
                ringMesh->InsertNextCell(VTK_LINE, 2, pts);
                pts[0] = i;
                pts[1] = i + 1;
            }
            std::cout << std::endl;
            actor1mapper->SetInputData(ringMesh);
            actor1->SetMapper(actor1mapper);
            actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
            actor1->GetProperty()->SetOpacity(0.5);
            actor1->GetProperty()->EdgeVisibilityOn();
            renderer->AddActor(actor1);

            renderWindow->Render();

            vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
                vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

            renderWindowInteractor->SetInteractorStyle(style);

            // Begin mouse interaction
            renderWindowInteractor->Start();
#endif

            delete[] neighborsA;
            delete[] neighborsB;
            return true;
        }

        std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>> vtkSelectionTools::SplitMeshByVertexRing(
            vtkSmartPointer<vtkPolyData> data, vtkSmartPointer<vtkIdList> ring, bool extendRing)
        {
            vtkIdType ringSize = ring->GetNumberOfIds();
            if (data == nullptr || ring == nullptr || ringSize < 3 || data->GetNumberOfPoints() < ringSize)
                return std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>>(nullptr, nullptr);

            data->BuildLinks();
            // initialize error to keep track of which cell has been assigned to which output (0 for first, 1 for second, -1 for not assigned yet). the same for points
            int *cellAssoc = new int[data->GetNumberOfCells()];
            std::fill(cellAssoc, cellAssoc + data->GetNumberOfCells(), -1);
            int *pointAssoc = new int[data->GetNumberOfPoints()];
            std::fill(pointAssoc, pointAssoc + data->GetNumberOfPoints(), -1);

            double centroid[3] = { 0, 0, 0 }; // used to fill the hole
            // to speed up search for neighbors, create a map of which data's vertices are on the ring. -1 if it's not ring, the index in the "ring" list if it is
            std::vector<vtkIdType> isRing;
            isRing.resize(data->GetNumberOfPoints());
            std::fill(isRing.begin(), isRing.end(), -1);
            for (vtkIdType i = 0; i < ringSize; i++)
            {
                vtkIdType id = ring->GetId(i);
                isRing[id] = i;
                vtkMath::Add(centroid, data->GetPoint(id), centroid);
                pointAssoc[id] = 2; // for points, 2 means add to both
            }
            
            std::vector<vtkIdType> sortedRing;
            sortedRing.resize(ringSize);
            if (!SortVertexRing(data, ring, isRing, sortedRing))
                return std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>>(nullptr, nullptr);

            // first mark the two cells adjacent to the first edge randomly
            vtkSmartPointer<vtkIdList> edgeNeighbors = vtkSmartPointer<vtkIdList>::New();
            data->GetCellEdgeNeighbors(-1, sortedRing[0], sortedRing[ringSize - 1], edgeNeighbors); // cell ID = -1 => finds all cells shared by this edge
            if (edgeNeighbors->GetNumberOfIds() != 2) // must be manifold, otherwise the algorithm won't work
                return std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>>(nullptr, nullptr);

            vtkIdType startCellID = edgeNeighbors->GetId(0);
            vtkIdType endID = edgeNeighbors->GetId(1);
            cellAssoc[startCellID] = 0;
            cellAssoc[endID] = 1;

            // now go around the ring and mark all cells adjacent to the ring vertices based on the initial two cells
            std::list<vtkIdType> overlapCells; // keep the indices of those cells - they will be used to create the ring extenstion if it is required
            overlapCells.push_back(startCellID);
            overlapCells.push_back(endID);
            vtkIdType idPrev = sortedRing[ringSize - 1];
            for (vtkIdType i = 0; i < ringSize; i++)
            {
                vtkIdType id = sortedRing[i];
                vtkIdType idNext = sortedRing[(i + 1) % ringSize];

                // traverse from one of the marked cells, marking all cells by the same association, until the next neighbor is reached, then switch the assoc
                vtkIdType curCellID = startCellID;
                vtkIdType curEndID = endID; // this will be updated in the loop below - keep the value that is valid for this iteration
                int assoc = cellAssoc[curCellID];
                vtkIdList *curCellPoints = data->GetCell(curCellID)->GetPointIds();
                vtkIdType prevEdgePoint = idPrev;
                edgeNeighbors->Reset();
                vtkIdType curCellNPoints = curCellPoints->GetNumberOfIds();
                for (vtkIdType l = 0; l < curCellNPoints; l++)
                {
                    vtkIdType canID = curCellPoints->GetId(l);
                    if (canID != id && canID != prevEdgePoint)
                    {
                        data->GetCellEdgeNeighbors(curCellID, id, canID, edgeNeighbors);
                        if (edgeNeighbors->GetNumberOfIds() > 1) // must be manifold, otherwise the algorithm won't work
                            return std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>>(nullptr, nullptr);
                        else if (edgeNeighbors->GetNumberOfIds() == 1) // in case of quads or higher, there can be a pair of nodes that do not share an edge
                        {
                            if (edgeNeighbors->GetId(0) == curEndID)
                                break;
                            if (canID == idNext) // if we are crossing the ring in the next cell, switch association
                            {
                                assoc = 1 - assoc;
                                // also this edge will be the starting point for the next iteration - mark it
                                startCellID = curCellID;
                                endID = edgeNeighbors->GetId(0);
                            }
                            else
                                pointAssoc[canID] = assoc; // if it is not a point on the ring, mark it as belonging to current association
                            curCellID = edgeNeighbors->GetId(0); // move to next cell
                            
                            cellAssoc[curCellID] = assoc;
                            overlapCells.push_back(curCellID);

                            curCellPoints = data->GetCell(curCellID)->GetPointIds();
                            l = -1; // reset the loop - cur cell has changed
                            prevEdgePoint = canID;
                        }
                        else // just mark the point association
                            pointAssoc[canID] = assoc;
                    }
                }
                idPrev = id;
            }

            // now we have marked all cells around the ring. traverse mesh and mark all the other cells and points. We can use the last startCellID and endID as seeds for both halves
            std::queue<vtkIdType> unprocessedPoints;
            vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
            for (int part = 0; part < 2; part++)
            {
                int assoc = cellAssoc[startCellID];// this is the association we will be propagating
                vtkIdList *curCellPoints = data->GetCell(startCellID)->GetPointIds();
                for (vtkIdType pc = 0; pc < curCellPoints->GetNumberOfIds(); pc++)
                {
                    if (isRing[curCellPoints->GetId(pc)] == -1) // start from non-ring points of the cell - we know that the whole neighborhood of this point is in a single part
                        unprocessedPoints.push(curCellPoints->GetId(pc));
                }
                while (!unprocessedPoints.empty())
                {
                    vtkIdList *curCellPoints = data->GetCell(startCellID)->GetPointIds();
                    data->GetPointCells(unprocessedPoints.front(), cellIds);
                    unprocessedPoints.pop();
                    vtkIdType nCells = cellIds->GetNumberOfIds();
                    for (vtkIdType i = 0; i < nCells; i++) // look at all cells adjacent to the current point
                    {
                        vtkIdType curCellID = cellIds->GetId(i);
                        if (cellAssoc[curCellID] == -1) // ...and if it is not yet associated, associate it + all it's vertices
                        {
                            cellAssoc[curCellID] = assoc;
                            vtkIdList *p = data->GetCell(curCellID)->GetPointIds();
                            vtkIdType nCellPoints = p->GetNumberOfIds();
                            for (vtkIdType pc = 0; pc < nCellPoints; pc++)
                            {
                                vtkIdType canID = p->GetId(pc);
                                if (pointAssoc[canID] == -1) // push unprocessed vertices
                                {
                                    pointAssoc[canID] = assoc;
                                    unprocessedPoints.push(canID);
                                }
                            }
                        }
                    }
                }
                startCellID = endID; // now do the same for the other part
            }

            // in case extendRing was specified, we will need two separate rings for each part
            vtkSmartPointer<vtkIdList> extRings[2];
            std::vector<vtkIdType> sortedRings[2];
            double centroids[2][3] = { { 0, 0, 0 }, { 0, 0, 0 } };
            std::vector<vtkIdType> isRings[2];
            if (extendRing)
            {
                for (int ringID = 0; ringID < 2; ringID++)
                {
                    extRings[ringID] = vtkSmartPointer<vtkIdList>::New();
                    isRings[ringID].resize(data->GetNumberOfPoints());
                    std::fill(isRings[ringID].begin(), isRings[ringID].end(), -1);
                }
                for (vtkIdType id : overlapCells)
                {
                    if (cellAssoc[id] > 1) // in case the same cell ends up multiple times in the list
                        continue;
                    vtkIdList *p = data->GetCell(id)->GetPointIds();
                    vtkIdType nCellPoints = p->GetNumberOfIds();
                    for (vtkIdType i = 0; i < nCellPoints; i++)
                    {
                        vtkIdType pointID = p->GetId(i); 
                        vtkIdType belongsTo = 1 - cellAssoc[id]; // this ring will close the opposite part, hence 1 - cellAssoc        
                        if (isRing[pointID] == -1 // gather only points that are not on the original ring, i.e. the points that create the extension
                            && isRings[belongsTo][pointID] == -1)  // do not add the same point twice
                        {
                            isRings[belongsTo][pointID] = extRings[belongsTo]->InsertNextId(pointID);
                            pointAssoc[pointID] = 2 + cellAssoc[id]; // put 2 or 3 to be able to keep revert back to the original association in case extending fails
                        }
                    }
                    cellAssoc[id] = cellAssoc[id] + 2; // put 2 or 3 to be able to keep revert back to the original association in case extending fails
                }
                for (int ringID = 0; ringID < 2; ringID++) 
                {
                    sortedRings[ringID].resize(extRings[ringID]->GetNumberOfIds());
                    for (vtkIdType i = 0; i < extRings[ringID]->GetNumberOfIds(); i++)
                        vtkMath::Add(centroids[ringID], data->GetPoint(extRings[ringID]->GetId(i)), centroids[ringID]);

                    if (!SortVertexRing(data, extRings[ringID], isRings[ringID], sortedRings[ringID]))
                    {
                        // if it fails, fall back to not creating an overlap
                        extendRing = false;
                        break;
                    }
                }
                if (!extendRing) // if extending failed, revert the cell and point associations
                {
                    for (vtkIdType id : overlapCells)
                    {
                        if (cellAssoc[id] > 1)
                            cellAssoc[id] -= 2;
                    }
                    for (vtkIdType i = 0; i < extRings[0]->GetNumberOfIds(); i++)
                        pointAssoc[extRings[0]->GetId(i)] -= 2;
                    for (vtkIdType i = 0; i < extRings[1]->GetNumberOfIds(); i++)
                        pointAssoc[extRings[1]->GetId(i)] -= 2;
                }

            }
            if (!extendRing)
            {
                for (int i = 0; i < 2; i++)
                {
                    extRings[i] = ring;
                    centroids[i][0] = centroid[0];
                    centroids[i][1] = centroid[1];
                    centroids[i][2] = centroid[2];
                }
            }

            // all cells and points associations should be correctly set now - split the mesh
            vtkSmartPointer<vtkPolyData> parts[2] = { vtkSmartPointer<vtkPolyData>::New(), vtkSmartPointer<vtkPolyData>::New() };
            parts[0]->Allocate();
            vtkSmartPointer<vtkPoints> points1 = vtkSmartPointer<vtkPoints>::New();
            parts[1]->Allocate();
            vtkSmartPointer<vtkPoints> points2 = vtkSmartPointer<vtkPoints>::New();
            vtkSmartPointer<vtkIdTypeArray> pointMap1 = vtkSmartPointer<vtkIdTypeArray>::New();
            pointMap1->SetName(ORIGINAL_POINT_IDS);
            vtkSmartPointer<vtkIdTypeArray> pointMap2 = vtkSmartPointer<vtkIdTypeArray>::New();
            pointMap2->SetName(ORIGINAL_POINT_IDS);
            vtkSmartPointer<vtkIdTypeArray> cellMaps[2] = { vtkSmartPointer<vtkIdTypeArray>::New(), vtkSmartPointer<vtkIdTypeArray>::New() };
            cellMaps[0]->SetName(ORIGINAL_CELL_IDS);
            cellMaps[1]->SetName(ORIGINAL_CELL_IDS);

            // first write the points and their mapping to the original mesh
            vtkIdType pC1 = 0, pC2 = 0;
            std::map<vtkIdType, vtkIdType> inverseMaps[2];
            vtkIdType nPoints = data->GetNumberOfPoints();
            for (vtkIdType i = 0; i < nPoints; i++)
            {
                if (pointAssoc[i] != 1) // is 0 or 2 - belongs to part 1
                {
                    inverseMaps[0][i] = points1->InsertNextPoint(data->GetPoint(i));
                    pointMap1->InsertNextValue(i);
                }
                if (pointAssoc[i] != 0) // belongs to part 2
                {
                    inverseMaps[1][i] = points2->InsertNextPoint(data->GetPoint(i));
                    pointMap2->InsertNextValue(i);
                }
            }

            // insert the hole point as the last one
            vtkMath::MultiplyScalar(centroids[0], 1.0 / extRings[0]->GetNumberOfIds());
            vtkMath::MultiplyScalar(centroids[1], 1.0 / extRings[1]->GetNumberOfIds());
            points1->InsertNextPoint(centroids[0]);
            points2->InsertNextPoint(centroids[1]);
            pointMap1->InsertNextValue(-1);
            pointMap2->InsertNextValue(-1);

            parts[0]->SetPoints(points1);
            parts[1]->SetPoints(points2);

            // now write cells using the created point map
            vtkSmartPointer<vtkIdList> mappedCellPoints[2];
            vtkIdType nCells = data->GetNumberOfCells();
            for (vtkIdType i = 0; i < nCells; i++)
            {
                vtkCell *c = data->GetCell(i);
                vtkIdList* cellPoints = c->GetPointIds();
                if (cellAssoc[i] != 1)
                {
                    mappedCellPoints[0] = vtkSmartPointer<vtkIdList>::New();
                    mappedCellPoints[0]->SetNumberOfIds(cellPoints->GetNumberOfIds());
                }
                if (cellAssoc[i] != 0)
                {
                    mappedCellPoints[1] = vtkSmartPointer<vtkIdList>::New();
                    mappedCellPoints[1]->SetNumberOfIds(cellPoints->GetNumberOfIds());
                }
                for (vtkIdType j = 0; j < cellPoints->GetNumberOfIds(); j++)
                {
                    if (cellAssoc[i] != 1)
                        mappedCellPoints[0]->SetId(j, inverseMaps[0][cellPoints->GetId(j)]);
                    if (cellAssoc[i] != 0)
                        mappedCellPoints[1]->SetId(j, inverseMaps[1][cellPoints->GetId(j)]);
                }
                if (cellAssoc[i] != 1)
                {
                    parts[0]->InsertNextCell(c->GetCellType(), mappedCellPoints[0]);
                    cellMaps[0]->InsertNextValue(i);
                }
                if (cellAssoc[i] != 0)
                {
                    parts[1]->InsertNextCell(c->GetCellType(), mappedCellPoints[1]);
                    cellMaps[1]->InsertNextValue(i);
                }
            }

            // insert cells that fill the hole
            for (int partID = 0; partID < 2; partID++)
            {
                std::vector<vtkIdType> &curSortedRing = extendRing ? sortedRings[partID] : sortedRing;
                // we need to figure out the right orientation for the cells (clockwise or counterclockwise) and whether our ring is sorted as CW or CCW
                data->GetCellEdgeNeighbors(-1, curSortedRing[0], curSortedRing[1], cellIds);
                // the cell that is on the edge of current ring and also in the part that is being closed must be shared by both parts
                // if it isn't, it means it is outside the boundary we are creating right now - behind the overlap (if there is any)
                vtkIdType cellInCurPart = cellIds->GetId(0);
                if (cellAssoc[cellIds->GetId(1)] > 1) // only if there is overlap switch it
                    cellInCurPart = cellIds->GetId(1);
                vtkIdList *p = data->GetCell(cellInCurPart)->GetPointIds();
                // assume that the orientation in the cellInCurPart cell goes against the ring (descending order of sortedRings) - then the appended hole-filling cells must goes along the ring
                bool appendAlongRing = true;
                vtkIdType nCellPoints = p->GetNumberOfIds();
                for (vtkIdType i = 1; i < nCellPoints; i++)
                {
                    // if in this cell the order goes 0 -> 1, the order of the appended cell must be 1 -> 0 => switch the assumption
                    if (p->GetId(i - 1) == curSortedRing[0] && p->GetId(i) == curSortedRing[1])
                    {
                        appendAlongRing = false;
                        break;
                    }
                }
                vtkIdType pts[3];
                pts[2] = parts[partID]->GetNumberOfPoints() - 1; // the hole point will be always there
                vtkIdType nExtRingPoints = extRings[partID]->GetNumberOfIds();
                for (vtkIdType i = 0; i < nExtRingPoints; i++)
                {
                    vtkIdType id = curSortedRing[i];
                    vtkIdType idNext = curSortedRing[(i + 1) % nExtRingPoints];
                    if (appendAlongRing)
                    {
                        pts[0] = inverseMaps[partID][id];
                        pts[1] = inverseMaps[partID][idNext];
                    }
                    else
                    {
                        pts[0] = inverseMaps[partID][idNext];
                        pts[1] = inverseMaps[partID][id];

                    }
                    parts[partID]->InsertNextCell(VTK_TRIANGLE, 3, pts);
                }
            }
            parts[0]->GetPointData()->AddArray(pointMap1);
            parts[1]->GetPointData()->AddArray(pointMap2);
            parts[0]->GetCellData()->AddArray(cellMaps[0]);
            parts[1]->GetCellData()->AddArray(cellMaps[1]);

            delete[] cellAssoc;
            delete[] pointAssoc;
            return std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>>(parts[0], parts[1]);
        }

        void vtkSelectionTools::markPointsAsSelected(vtkPointSet *data, vtkIntArray *indices)
        {
            vtkSmartPointer<vtkBitArray> selected_primitives = ObtainSelectionArrayPoints(data, false);
            vtkIdType nIndices = indices->GetNumberOfTuples();
            vtkIdType nPoints = data->GetNumberOfPoints();
            int selVal = p_inverseSelection ? IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED;
            for (vtkIdType i = 0; i < nIndices; i++)
            {
                int index = indices->GetValue(i);
                if (index >= 0 && index < nPoints) // only select it if it actually is actually an index of a point...maybe unnecessary to check for this?
                    selected_primitives->SetValue(index, selVal);
            }
        }

        void vtkSelectionTools::markCellsAsSelected(vtkPointSet *data, vtkIntArray *indices)
        {
            vtkSmartPointer<vtkBitArray> selected_primitives = ObtainSelectionArrayCells(data, false);
            vtkIdType nIndices = indices->GetNumberOfTuples();
            vtkIdType nCells = data->GetNumberOfCells();
            int selVal = p_inverseSelection ? IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED;
            for (vtkIdType i = 0; i < nIndices; i++)
            {
                int index = indices->GetValue(i);
                if (index >= 0 && index < nCells) // only select it if it actually is actually an index of a cell...maybe unnecessary to check for this?
                    selected_primitives->SetValue(index, selVal);
            }
        }

        void vtkSelectionTools::blankSelectedKeepPoints(vtkSmartPointer<vtkUnstructuredGrid> output)
        {
            // select for only the first data set
            vtkPointSet *data = vtkPointSet::SafeDownCast(this->GetInputDataObject(0, 0));
            vtkSmartPointer<vtkDataSetSurfaceFilter> dummy = vtkSmartPointer<vtkDataSetSurfaceFilter>::New(); // just for the name of original cells is array
                // copy cells that are selected and create pedigree cell ID array
            vtkSmartPointer<vtkIdTypeArray> origCellsMap = vtkSmartPointer<vtkIdTypeArray>::New();
            origCellsMap->SetName(dummy->GetOriginalCellIdsName());
            // use the original points array for the blanked actor, so when the originalDataSet gets updated, so does the blanked
            output->Initialize();
            output->Allocate();
            output->SetPoints(data->GetPoints()); // share the points
            for (int i = 0; i < data->GetPointData()->GetNumberOfArrays(); i++)
                output->GetPointData()->AddArray(data->GetPointData()->GetArray(i)); // and share all pointData arrays as well
            if (targetType == SELECTION_TARGET::ELEMENTS || targetType == SELECTION_TARGET::FACES)
            {
                vtkSmartPointer<vtkBitArray> sel_primitives = ObtainSelectionArrayCells(data, false);
                sel_primitives->Modified(); // to make sure it is updated
                vtkIdType nPrims = sel_primitives->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nPrims; i++)
                {
                    if (sel_primitives->GetValue(i) == IS_NOT_PRIMITIVE_SELECTED)
                    {
                        vtkCell *curCell = data->GetCell(i);
                        output->InsertNextCell(curCell->GetCellType(), curCell->GetPointIds());
                        origCellsMap->InsertNextValue(i);
                    }
                }
            }
            else if (targetType == SELECTION_TARGET::NODES)
            {
                vtkSmartPointer<vtkBitArray> sel_primitives = ObtainSelectionArrayPoints(data, false);
                sel_primitives->Modified(); // to make sure it is updated
                vtkIdType nCells = data->GetNumberOfCells();
                for (vtkIdType i = 0; i < nCells; i++)
                {
                    vtkCell *curCell = data->GetCell(i);
                    bool keep = true;
                    vtkIdType nCellPoints = curCell->GetNumberOfPoints();
                    for (vtkIdType j = 0; j < nCellPoints; j++) // go through the cell, if at least one point is selected, it has to be removed -> remove the cell as well
                    {
                        if (sel_primitives->GetValue(curCell->GetPointId(j)) == IS_PRIMITIVE_SELECTED)
                        {
                            keep = false;
                            break;
                        }
                    }
                    if (keep)
                    {
                        output->InsertNextCell(curCell->GetCellType(), curCell->GetPointIds());
                        origCellsMap->InsertNextValue(i);
                    }
                }
            }
            output->GetCellData()->AddArray(origCellsMap);
        }

        void vtkSelectionTools::blankSelected(vtkSmartPointer<vtkUnstructuredGrid> output)
        {
            // select for only the first data set
            vtkPointSet *data = vtkPointSet::SafeDownCast(this->GetInputDataObject(0, 0));
            // threshold filter for removing the cells / points that are not selected in order to create the new output unstructured grid
            vtkSmartPointer<vtkExtractSelectedIds> threshold = vtkSmartPointer<vtkExtractSelectedIds>::New();
            threshold->PreserveTopologyOff(); // to generate the pedigree IDs arrays
            vtkSmartPointer<vtkSelection> sel = vtkSmartPointer<vtkSelection>::New();
            vtkSmartPointer<vtkSelectionNode> selectionNode =
                vtkSmartPointer<vtkSelectionNode>::New();
            selectionNode->SetContentType(vtkSelectionNode::INDICES);
            selectionNode->GetProperties()->Set(vtkSelectionNode::INVERSE(), p_inverseSelection ? 0 : 1);
            sel->AddNode(selectionNode);
            vtkSmartPointer<vtkIdTypeArray> ids = vtkSmartPointer<vtkIdTypeArray>::New();
            ids->SetNumberOfComponents(1);
            if (targetType == SELECTION_TARGET::ELEMENTS || targetType == SELECTION_TARGET::FACES)
            {
                vtkSmartPointer<vtkBitArray> sel_prim = ObtainSelectionArrayCells(data, false);
                // Set values
                vtkIdType nPrims = sel_prim->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nPrims; i++)
                {
                    if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        ids->InsertNextValue(i);
                }
                selectionNode->SetSelectionList(ids);
                selectionNode->SetFieldType(vtkSelectionNode::CELL);
                // create the sectioned mesh
                threshold->SetInputDataObject(0, data);
                threshold->SetInputDataObject(1, sel);
                threshold->Update();
                // write it to the output
                output->DeepCopy(threshold->GetOutput());
            }
            else if (targetType == SELECTION_TARGET::NODES)
            {
                vtkSmartPointer<vtkBitArray> sel_prim = ObtainSelectionArrayPoints(data, false);
                vtkIdType nPrims = sel_prim->GetNumberOfTuples();
                // Set values
                for (vtkIdType i = 0; i < nPrims; i++)
                {
                    if (sel_prim->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        ids->InsertNextValue(i);
                }
                selectionNode->SetSelectionList(ids);
                selectionNode->SetFieldType(vtkSelectionNode::POINT);
                // create the sectioned mesh
                threshold->SetInputDataObject(0, data);
                threshold->SetInputDataObject(1, sel);
                threshold->Update();
                // write it to the output
                output->DeepCopy(threshold->GetOutput());
            }
        }

        bool vtkSelectionTools::intersectsCellEdgesWithOBB(vtkPointSet *data, int cellID, vtkPolyData *OBB)
        {
            vtkCell *cell = data->GetCell(cellID);
            vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New(); // a polygon to represent the sides of the box
            polygon->GetPoints()->ShallowCopy(OBB->GetPoints()); // use the points from the OBB
            polygon->GetPointIds()->SetNumberOfIds(4);
            double t; // the intersection parameter - we dont really care about it
            double x[3]; // the intersection point - also dont care
            double pcoords[3]; // more useless sub-results
            int subId; // dont know, dont care
            for (int i = 0; i < OBB->GetNumberOfPolys(); i++) // number of polys should always be 6 but juuust in case
            {
                vtkCell *currBoxSide = OBB->GetCell(i);
                // fill in the indices of the current box side to the polygon instance
                polygon->GetPointIds()->SetId(0, currBoxSide->GetPointId(0));
                polygon->GetPointIds()->SetId(1, currBoxSide->GetPointId(1));
                polygon->GetPointIds()->SetId(2, currBoxSide->GetPointId(2));
                polygon->GetPointIds()->SetId(3, currBoxSide->GetPointId(3));
                for (int j = 0; j < cell->GetNumberOfEdges(); j++) // for each edge, check for intersection with the current box side
                {
                    // 0 in case of no intersection, something else otherwise
                    if (polygon->IntersectWithLine(data->GetPoint(cell->GetEdge(j)->GetPointId(0)), data->GetPoint(cell->GetEdge(j)->GetPointId(0)),
                        0.001, t, x, pcoords, subId)) // tolerance = 0.001						
                        return true;	// if there is an intersection, this cell should be selected, we can end

                }
            }
            return false;
        }
    }
}
