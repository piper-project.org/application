/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_ENVMODELS_H
#define PIPER_HBM_ENVMODELS_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <map>
#include <vector>

#include "EnvironmentModel.h"



namespace piper {
namespace hbm {

/**
* @brief A EnvironmentModels: Container for EnvironmentModel.
*
* Container for EnvironmentModel
*
* @author Erwan Jolivet @date 2015
*/
class HBM_EXPORT EnvironmentModels {
	typedef std::map<std::string, EnvironmentModelPtr> MapEnvironmentModel;
public:

	/*!
	*  \brief Constructor
	*
	*  Constructor of EnvironmentModels
	*
	*/
    EnvironmentModels() {}
	/*!
	*  \brief Destructor
	*
	*  Destructor of EnvironmentModels
	*
	*/
	~EnvironmentModels();

	void clear();
	/*!
	*  \brief
	*
	*  delete environement model identified by its name
	*
	*  \param name: name of the environement model to delete
	*
	*/
	void deleteEnvironmentModel(std::string const& name);
	/*!
	*  \brief
	*
	*  return true if environement model identified by name extists
	*
	*  \param name: name of the environement model
	*
	*/
	bool isExistingName(std::string const& name) const;
	/*!
	*  \brief
	*
	*  return the number of environement models in the container
	*
	*/
    std::size_t getNumberenvironment() const { return m_models.size(); }
	/*!
	*  \brief list of environement model name
	*
	*  return vector of names 
	*
	*/
	std::vector<std::string> getListNames() const;
	/*!
	*  \brief add of environement mode
	*
	* return true is model is added
	*
	*  \param name: name of the environment
	*  \param file: FE main file of the environment model
	*  \param formatrulesFile: format rule file to parse the environement model	
	*  \param lengthUnit: unit length of the environment model
	*
	*/
    EnvironmentModelPtr addModel(std::string const& name, std::string const& file, std::string const& formatrulesFile, units::Length lengthUnit);
	/*!
	*  \brief add of environement model and scale node coordinates to a target length unit
	*
	* return true is model is added
	*
	*  \param name: name of the environment
	*  \param file: FE main file of the environment model
	*  \param formatrulesFile: format rule file to parse the environement model
	*  \param lengthUnit: unit length of the environment model
	*  \param targetlenghtunit: unit length to convert environment model
	*/
    bool addModel(std::string const& name, std::string const& file, std::string const& formatrulesFile, units::Length lengthUnit, units::Length targetlenghtunit);

	/*!
	*  \brief get of environment model identified by its name
	*
	*  return reference to an environment model, NULL if there is no model with such name
	*
	*/
	EnvironmentModelPtr getEnvironmentModel( std::string const& name) const;
	/*!
	*  \brief write environement model
	*
	*  write environement model in XML element
	*
	*/
	void write(const std::string& file) const;

	/*!
	*  \brief serialize environement model
	*
	*  write environement model in XML element
	*
	*/
	void serializeXml(tinyxml2::XMLElement* element, std::string const& modelpath) const;
	/*!
	*  \brief read environement model information in xml file
	*
	*  read environement model information
	*
	*/
    void read(const std::string& file);
	/*!
	*  \brief read and import environment models from xml file
	*
	*  read and import environment models from xml file
	*
	*/
    void readandImport(const std::string& file, units::Length targetlenghtunit);
    /*!
    *  \brief scale all envirronments models according to a target unit
    *
    *  read and import environment models from xml file
    *
    */
    void scaleEnvModels(units::Length targetlenghtunit);
private:
	MapEnvironmentModel m_models;

};

}
}

#endif
