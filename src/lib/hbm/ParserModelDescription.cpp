/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParserModelDescription.h"

#include "Metadata.h"
#include "FEModel.h"
#include "ParserContext.h"


using namespace std;
namespace piper {
	namespace hbm {
		namespace parser {

            //template< >
            //map<string, TermDescription*> Factory<TermDescription>::m_map = map<string, TermDescription*>();

            bool TermDescription::parseXml(tinyxml2::XMLElement* element) {
                if (element != nullptr) {
                    element = element->FirstChildElement();
                    while (element != nullptr) {
                        //cout << element->Name() << endl;
                        TermDescriptionPtr newterm = this->facTermDescription.Create(element->Name());
                        if (newterm != nullptr)  { // term is defined in the factory
                            bool spyadd = newterm->parseXml(element);
                            if (spyadd)
                                addTerm(newterm);
                        }
                        element = element->NextSiblingElement();
                    }
                }
                return true;
            }

            void TermDescription::addTerm(TermDescriptionPtr t) {
                children.push_back(t);
            }

            void TermDescription::fillmetadata(FEModel* femodel, Metadata& metadata) const{
                 for (TermDescriptionPtr const& it : children) {
                    it->fillmetadata(femodel, metadata);
                }
            }

            void ParserModelDescription::setFile(const std::string& file, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool anatomicalJoint, bool controlpoint, bool HbmParameter) {
                initFactory(anthropometry, entity, landmark, genericmetadata, contact, joint, anatomicalJoint, controlpoint, HbmParameter);
                tinyxml2::XMLDocument model;
                model.LoadFile(file.c_str());
                if (model.Error()) {
                    std::stringstream str;
                    str << "[FAILED] Load description file: " << file << endl << model.ErrorStr() << endl;
                    throw std::runtime_error(str.str().c_str());
                    return;
                }
                tinyxml2::XMLElement* element;
                //
                element = model.FirstChildElement("model_description");
                //
                TermDescription::parseXml(element);

            }
            
            ParserModelDescription::ParserModelDescription(const string& file) {
                setFile(file);
			}

            void ParserModelDescription::initFactory(bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool anatomicalJoint, bool controlpoint, bool HbmParameter) {
                if (anthropometry)
                    Factory<TermDescription>::Register("anthropometry", std::function<std::shared_ptr<AssociationAnthropometry>()>(std::make_shared<AssociationAnthropometry>));
                if (genericmetadata)
                    Factory<TermDescription>::Register("genericmetadata",
                    std::function<std::shared_ptr<AssociationGenericMetadata>()>(std::make_shared<AssociationGenericMetadata>));
                if (entity)
                    Factory<TermDescription>::Register("entity",
                    std::function<std::shared_ptr<AssociationEntity>()>(std::make_shared<AssociationEntity>));
                if (landmark)
                    Factory<TermDescription>::Register("landmarks", 
                    std::function<std::shared_ptr<AssociationLandmarks>()>(std::make_shared<AssociationLandmarks>));
                if (joint)
                    Factory<TermDescription>::Register("joint", 
                    std::function<std::shared_ptr<AssociationJoint>()>(std::make_shared<AssociationJoint>));
                if (anatomicalJoint)
                    Factory<TermDescription>::Register("anatomicalJoint",
                    std::function<std::shared_ptr<AssociationAnatomicalJoint>()>(std::make_shared<AssociationAnatomicalJoint>));
                if (controlpoint)
                    Factory<TermDescription>::Register("controlPoint", 
                    std::function<std::shared_ptr<AssociationControlPoint>()>(std::make_shared<AssociationControlPoint>));
                if (HbmParameter)
                    Factory<TermDescription>::Register("hbm_parameter", 
                    std::function<std::shared_ptr<Associationparameter>()>(std::make_shared<Associationparameter>));
                if (contact)
                    Factory<TermDescription>::Register("contact", 
                    std::function<std::shared_ptr<AssociationContact>()>(std::make_shared<AssociationContact>));
            }

            bool AssociationAnthropometry::parseXml(tinyxml2::XMLElement* element) {
                name = element->Attribute("name");
                std::string testname(name);
                std::transform(name.begin(), name.end(), testname.begin(), ::tolower);
                if (element->FirstChildElement("units")->Attribute(units::LengthUnit::type().c_str())) {
                    m_unit_type = units::LengthUnit::type();
                    m_unit = element->FirstChildElement("units")->Attribute(units::LengthUnit::type().c_str());
                }
                else if (element->FirstChildElement("units")->Attribute(units::MassUnit::type().c_str())) {
                    m_unit_type = units::MassUnit::type();
                    m_unit = element->FirstChildElement("units")->Attribute(units::MassUnit::type().c_str());
                }
                else if (element->FirstChildElement("units")->Attribute(units::AgeUnit::type().c_str())) {
                    m_unit_type = units::AgeUnit::type().c_str();
                    m_unit = element->FirstChildElement("units")->Attribute(units::AgeUnit::type().c_str());
                }
                std::stringstream strvalue(element->FirstChildElement("value")->GetText());
                strvalue >> m_value;
                return true;

            }
            
            void AssociationAnthropometry::fillmetadata(FEModel* femodel, Metadata& metadata) const {
                if (name == "height") {
                    metadata.anthropometry().height.setValue(m_value);
                    metadata.anthropometry().height.unit() = units::LengthUnit::strToUnit(m_unit);
                    metadata.anthropometry().height.convertToUnit(metadata.lengthUnit());
                }
                else if (name == "weight") {
                    metadata.anthropometry().weight.setValue(m_value);
                    metadata.anthropometry().weight.unit() = units::MassUnit::strToUnit(m_unit);
                    metadata.anthropometry().weight.convertToUnit(metadata.massUnit());
                }
                else if (name == "age") {
                    metadata.anthropometry().age.setValue(m_value);
                    metadata.anthropometry().age.unit() = units::AgeUnit::strToUnit(m_unit);
                    metadata.anthropometry().age.convertToUnit(metadata.ageUnit());
                }

            }


            bool AssociationGenericMetadata::parseXml(tinyxml2::XMLElement* element) {
                //type = TYPE::ENTITY;
                name = element->Attribute("name");
                std::string testname(name);
                std::transform(name.begin(), name.end(), testname.begin(), ::tolower);
                element = element->FirstChildElement("keyword");
                keywordParseXml(element, key, vidkey);
                if (testname == "auto")
                    setAuto();
                return true;
            }
            
            void AssociationGenericMetadata::fillmetadata(FEModel* femodel, Metadata& metadata) const {
                if (key.size()>0) {
                    GenericMetadata newgen;
                    auto itt = vidkey.begin();
                    // based on keyword, get type of mesh component
                    for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                        bool spyNode = false;
                        bool spyE1D = false;
                        bool spyE2D = false;
                        bool spyE3D = false;
                        VId vid;
                        for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                            //check if it is a node or element insead of a group
                            if (femodel->get<Node>(*it, *itid) != nullptr) {
                                spyNode = true;
                                vid.push_back(femodel->get<Node>(*it, *itid)->getId());
                            }
                            else if (femodel->get<Element1D>(*it, *itid) != nullptr) {
                                spyE1D = true;
                                vid.push_back(femodel->get<Element1D>(*it, *itid)->getId());
                            }
                            else  if (femodel->get<Element2D>(*it, *itid) != nullptr) {
                                spyE2D = true;
                                vid.push_back(femodel->get<Element2D>(*it, *itid)->getId());
                            }
                            else if (femodel->get<Element3D>(*it, *itid) != nullptr) {
                                spyE3D = true;
                                vid.push_back(femodel->get<Element3D>(*it, *itid)->getId());
                            }
                            else {
                                spyNode = false;
                                spyE1D = false;
                                spyE2D = false;
                                spyE3D = false;
                            }
                        }
                        if (spyNode || spyE1D || spyE2D || spyE3D) {
                            if (isAuto())  {
                                stringstream str;
                                str << "For Generic Metadata '" << name <<"': cannot use 'Auto' mode when it is defined by node or elements IDs instead of groups." << endl;
                                throw std::runtime_error(str.str().c_str());
                            }
                            Id newGroupId;
                            newgen.setName(name);
                            // greade a group
                            if (spyNode) {
                                GroupNodePtr newgroup = std::make_shared<GroupNode>();
                                newgroup->set(vid);
                                femodel->set(newgroup);
                                newGroupId = newgroup->getId();
                                newgen.get_groupNode().push_back(newGroupId);
                            }
                            else if (spyE1D) {
                                GroupElements1DPtr newgroup = std::make_shared< GroupElements1D>();
                                newgroup->set(vid);
                                femodel->set(newgroup);
                                newGroupId = newgroup->getId();
                                newgen.get_groupElement1D().push_back(newGroupId);
                            }
                            else if (spyE2D) {
                                GroupElements2DPtr newgroup = std::make_shared<GroupElements2D>();
                                newgroup->set(vid);
                                newGroupId = newgroup->getId();
                                femodel->set(newgroup);
                                newgen.get_groupElement2D().push_back(newGroupId);
                            }
                            else if (spyE3D) {
                                GroupElements3DPtr newgroup = std::make_shared<GroupElements3D>();
                                newgroup->set(vid);
                                femodel->set(newgroup);
                                newGroupId = newgroup->getId();
                                newgen.get_groupElement3D().push_back(newGroupId);
                            }
                        }
                        else if (!spyNode && !spyE1D && !spyE2D && !spyE3D) {
                            for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                                GroupNodePtr gn = femodel->get<GroupNode>(*it, *itid);
                                GroupGroupsPtr gg = femodel->get<GroupGroups>(*it, *itid);
                                GroupElements1DPtr g1d = femodel->get<GroupElements1D>(*it, *itid);
                                GroupElements2DPtr g2d = femodel->get<GroupElements2D>(*it, *itid);
                                GroupElements3DPtr g3d = femodel->get<GroupElements3D>(*it, *itid);
                                if (gn == nullptr &&
                                    gg == nullptr &&
                                    g1d == nullptr &&
                                    g2d == nullptr &&
                                    g3d == nullptr) {
                                    stringstream str;
                                    str << "For Generic Metadata '" << name << "': group with ID '" << itid->infoId() << "' is not defined in the model." << endl;
                                    throw std::runtime_error(str.str().c_str());
                                }
                                if (g1d != nullptr) {
                                    if (!isAuto()) {
                                        newgen.setName(name);
                                        newgen.get_groupElement1D().push_back(g1d->getId());
                                    }
                                    else {
                                        newgen.setName(g1d->getName());
                                        newgen.get_groupElement1D().push_back(g1d->getId());
                                    }
                                }
                                else if (g2d != nullptr) {
                                    if (!isAuto()) {
                                        newgen.setName(name);
                                        newgen.get_groupElement2D().push_back(g2d->getId());
                                    }
                                    else {
                                        newgen.setName(g2d->getName());
                                        newgen.get_groupElement2D().push_back(g2d->getId());
                                    }
                                }
                                else if (g3d != nullptr) {
                                    if (!isAuto()) {
                                        newgen.setName(name);
                                        newgen.get_groupElement3D().push_back(g3d->getId());
                                    }
                                    else {
                                        newgen.setName(g3d->getName());
                                        newgen.get_groupElement3D().push_back(g3d->getId());
                                    }
                                }
                                else if (gn != nullptr) {
                                    if (!isAuto()) {
                                        newgen.setName(name);
                                        newgen.get_groupNode().push_back(gn->getId());
                                    }
                                    else {
                                        newgen.setName(gn->getName());
                                        newgen.get_groupNode().push_back(gn->getId());
                                    }
                                }
                                else if (gg != nullptr) {
                                    if (!isAuto()) {
                                        newgen.setName(name);
                                        newgen.get_groupGroup().push_back(gg->getId());
                                        VId& vgid = gg->get();
                                        for (VId::const_iterator itg = vgid.begin(); itg != vgid.end(); ++itg) {
                                            if (femodel->get<GroupNode>(*itg) != nullptr)
                                                newgen.get_groupNode().push_back(*itg);
                                            else if (femodel->get<GroupElements1D>(*itg) != nullptr)
                                                newgen.get_groupElement1D().push_back(*itg);
                                            else if (femodel->get<GroupElements2D>(*itg) != nullptr)
                                                newgen.get_groupElement2D().push_back(*itg);
                                            else if (femodel->get<GroupElements3D>(*itg) != nullptr)
                                                newgen.get_groupElement3D().push_back(*itg);
                                        }
                                    }
                                    else {

                                        newgen.setName(gg->getName());
                                        newgen.get_groupGroup().push_back(gg->getId());
                                        VId& vgid = gg->get();
                                        for (VId::const_iterator itg = vgid.begin(); itg != vgid.end(); ++itg) {
                                            if (femodel->get<GroupNode>(*itg) != nullptr)
                                                newgen.get_groupNode().push_back(*itg);
                                            else if (femodel->get<GroupElements1D>(*itg) != nullptr)
                                                newgen.get_groupElement1D().push_back(*itg);
                                            else if (femodel->get<GroupElements2D>(*itg) != nullptr)
                                                newgen.get_groupElement2D().push_back(*itg);
                                            else if (femodel->get<GroupElements3D>(*itg) != nullptr)
                                                newgen.get_groupElement3D().push_back(*itg);
                                        }
                                    }
                                }
                                if (isAuto()) {
                                    metadata.addGenericmetadata(newgen);
                                    newgen.get_groupNode().clear();
                                    newgen.get_groupElement1D().clear();
                                    newgen.get_groupElement2D().clear();
                                    newgen.get_groupElement3D().clear();
                                    newgen.get_groupGroup().clear();
                                }
                            }
                        }
                        itt++;
                    }
                    if (!isAuto())
                        metadata.addGenericmetadata(newgen);
                }
            }



            bool AssociationEntity::parseXml(tinyxml2::XMLElement* element) {
				//type = TYPE::ENTITY;
				name = element->Attribute("name");
                element->QueryBoolAttribute("innernormals", &m_innernormal);
                std::string testname(name);
                std::transform(name.begin(), name.end(), testname.begin(), ::tolower);
				element = element->FirstChildElement("keyword");
				keywordParseXml( element, key, vidkey);
                if (testname == "auto")
					setAuto();
				return true;
			}

			void AssociationEntity::fillmetadata( FEModel* femodel, Metadata& metadata) const {
                if (key.size()>0) {
					Entity newentity;
                    newentity.setFlagNormalesInner(m_innernormal);
                    auto itt = vidkey.begin();
					// based on keyword, get type of mesh component
                    for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                        for (auto itid = itt->begin(); itid != itt->end(); ++itid) {

                            GroupGroupsPtr gg = femodel->get<GroupGroups>(*it, *itid);
                            GroupElements1DPtr g1d = femodel->get<GroupElements1D>(*it, *itid);
                            GroupElements2DPtr g2d = femodel->get<GroupElements2D>(*it, *itid);
                            GroupElements3DPtr g3d = femodel->get<GroupElements3D>(*it, *itid);
                            if (gg == nullptr &&
                                g1d == nullptr &&
                                g2d == nullptr &&
                                g3d == nullptr) {
                                stringstream str;
                                str << "For entity '" << name << "': group with ID '" << itid->infoId() << "' is not defined in the model." << endl;
                                throw std::runtime_error(str.str().c_str());
                            }
                            if (g1d != nullptr) {
                                if (!isAuto()) {
                                    newentity.setName(name);
                                    newentity.get_groupElement1D().push_back(g1d->getId());
                                }
                                else {
                                    newentity.setName(g1d->getName());
                                    newentity.get_groupElement1D().push_back(g1d->getId());
                                }
                            }
                            else if (g2d != nullptr) {
                                if (!isAuto()) {
                                    newentity.setName(name);
                                    newentity.get_groupElement2D().push_back(g2d->getId());
                                }
                                else {
                                    newentity.setName(g2d->getName());
                                    newentity.get_groupElement2D().push_back(g2d->getId());
                                }
                            }
                            else if (g3d != nullptr) {
                                if (!isAuto()) {
                                    newentity.setName(name);
                                    newentity.get_groupElement3D().push_back(g3d->getId());
                                }
                                else {
                                    newentity.setName(g3d->getName());
                                    newentity.get_groupElement3D().push_back(g3d->getId());
                                }
                            }
                            else if (gg != nullptr) {
                                if (!isAuto()) {
                                    newentity.setName(name);
                                    newentity.get_groupGroup().push_back(gg->getId());
                                    VId& vgid = gg->get();
                                    for (VId::const_iterator itg = vgid.begin(); itg != vgid.end(); ++itg) {
                                        if (femodel->get<GroupElements1D>(*itg) != nullptr)
                                            newentity.get_groupElement1D().push_back(*itg);
                                        else if (femodel->get<GroupElements2D>(*itg) != nullptr)
                                            newentity.get_groupElement2D().push_back(*itg);
                                        else if (femodel->get<GroupElements3D>(*itg) != nullptr)
                                            newentity.get_groupElement3D().push_back(*itg);
                                    }
                                }
                                else {

                                    newentity.setName(gg->getName());
                                    newentity.get_groupGroup().push_back(gg->getId());
                                    VId& vgid = gg->get();
                                    for (VId::const_iterator itg = vgid.begin(); itg != vgid.end(); ++itg) {
                                        if (femodel->get<GroupElements1D>(*itg) != nullptr)
                                            newentity.get_groupElement1D().push_back(*itg);
                                        else if (femodel->get<GroupElements2D>(*itg) != nullptr)
                                            newentity.get_groupElement2D().push_back(*itg);
                                        else if (femodel->get<GroupElements3D>(*itg) != nullptr)
                                            newentity.get_groupElement3D().push_back(*itg);
                                    }
                                }
                            }
                            if (isAuto()) {
                                metadata.addEntity(newentity);
                                newentity.get_groupElement1D().clear();
                                newentity.get_groupElement2D().clear();
                                newentity.get_groupElement3D().clear();
                                newentity.get_groupGroup().clear();
                            }
                        }
                        itt++;
					}
                    if (!isAuto())
						metadata.addEntity( newentity);
				}
			}


			bool AssociationLandmarks::parseXml( tinyxml2::XMLElement* element) {
				//type = TYPE::LANDMARK;
				name = element->Attribute("name");
                std::string testname(name);
                std::transform(name.begin(), name.end(), testname.begin(), ::tolower);
                string typelandmark = element->Attribute("type");
				if (typelandmark=="point") 
					m_typelandmark = Landmark::Type::POINT;
				else if (typelandmark=="barycenter") 
					m_typelandmark = Landmark::Type::BARYCENTER;
				else if (typelandmark=="sphere") 
					m_typelandmark = Landmark::Type::SPHERE;
                else {
					stringstream str;
                    str << "For landmark '" << name << "': unknown landmark type." << endl;
					throw std::runtime_error(str.str().c_str());
					return true;
				}
                if (element->FirstChildElement("keyword") != nullptr) {
                    keywordParseXml(element->FirstChildElement("keyword"), key, vidkey);
                }
                if (element->FirstChildElement("coord") != nullptr) {
                    VCoordParseXml(element->FirstChildElement("coord"), vcoord);
                }
                if (testname == "auto")
					setAuto();
				return true;
			}

			void AssociationLandmarks::fillmetadata( FEModel* femodel, Metadata& metadata) const {
                    if (key.size() == 0) { // metadata landmark are defined using coord
                        Landmark newlandmark(name, m_typelandmark);
                        std::shared_ptr<GroupNode> gn = std::make_shared<GroupNode>();
                        for (Coord const& coord : vcoord) {
                            // create node + group of node
                            std::shared_ptr<Node> nd = std::make_shared<Node>();
                            nd->setCoord(coord);
                            femodel->set<Node>(nd);
                            gn->addInGroup(nd->getId());
                        }
                        femodel->set<GroupNode>(gn);
                        newlandmark.get_groupNode().push_back(gn->getId());
                        metadata.addLandmark(newlandmark);
                    }
                    else if ((!isAuto()) && (key.size() == 1)) {
                        Landmark newlandmark(name, m_typelandmark);
                        auto itt = vidkey.begin();
                        for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                            for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                                GroupNodePtr cur = femodel->get<GroupNode>(*it, *itid);
                                NodePtr curn = femodel->get<Node>(*it, *itid);
                                if (cur == nullptr && curn == nullptr) {
                                    stringstream str;
                                    str << "For landmark '" << name << "': group or nodes '" << *it << "', ID " << itid->infoId() << ", are not defined in the model." << endl;
                                    throw std::runtime_error(str.str().c_str());
                                }
                                if (cur != nullptr)            						
                                    newlandmark.get_groupNode().push_back(cur->getId());
                                else if (curn != nullptr)
                                    newlandmark.node.push_back(curn->getId());                                                                      
                            }
                            itt++;
                        }
                        // check if landmark is of type point and is defined with only one node
                        if (m_typelandmark == Landmark::Type::POINT || m_typelandmark == Landmark::Type::SPHERE) {
                            size_t nb = newlandmark.node.size();
                            for (Id& id : newlandmark.get_groupNode())
                                nb += femodel->getGroupNode(id).size();
                            if (m_typelandmark == Landmark::Type::POINT && nb != 1) {
                                stringstream str;
                                str << "For landmark '" << name << "', type 'point': more than one node define the landmark." << endl;
                                throw std::runtime_error(str.str().c_str());
                            }
                            else if (m_typelandmark == Landmark::Type::SPHERE && nb < 4) {
                                stringstream str;
                                str << "For landmark '" << name << "', type 'sphere': Less than four nodes define the landmark." << endl;
                                throw std::runtime_error(str.str().c_str());
                            }
                        }
	             		metadata.addLandmark(newlandmark); 
					}
					else {
                        auto itt = vidkey.begin();
                        for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                            for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                                GroupNodePtr cur = femodel->get<GroupNode>(*it, *itid);
                                if (cur == nullptr) {
                                    stringstream str;
                                    str << "For landmarks '" << name << "': Group '" << *it << "', ID " << itid->infoId() << ", is not defined in the model." << endl;
                                    throw std::runtime_error(str.str().c_str());
                                }
                                std::string _name = cur->getName();
                                Landmark newlandmark(_name, m_typelandmark);
                                newlandmark.get_groupNode().push_back(cur->getId());
                                // check if landmark is of type point and is defined with only one node
                                if (m_typelandmark == Landmark::Type::POINT || m_typelandmark == Landmark::Type::SPHERE) {
                                    size_t nb = newlandmark.node.size();
                                    for (Id& id : newlandmark.get_groupNode())
                                        nb += femodel->getGroupNode(id).size();
                                    if (m_typelandmark == Landmark::Type::POINT && nb != 1) {
                                        stringstream str;
                                        str << "For landmark '" << name << "', type 'point': more than one node define the landmark." << endl;
                                        throw std::runtime_error(str.str().c_str());
                                    }
                                    else if (m_typelandmark == Landmark::Type::SPHERE && nb < 4) {
                                        stringstream str;
                                        str << "For landmark '" << name << "', type 'sphere': less than four nodes define the landmark." << endl;
                                        throw std::runtime_error(str.str().c_str());
                                    }
                                }
                                metadata.addLandmark(newlandmark);
                            }
                            itt++;
                        }
                    }
			}

			bool AssociationControlPoint::parseXml( tinyxml2::XMLElement* element) {
				//type = TYPE::CONTROLPOINT;
                _name = element->Attribute("name");
				_ctrlPtRole = element->Attribute("role");
                if (element->FirstChildElement("keyword"))
                {
                    keywordParseXml(element->FirstChildElement("keyword"), key, vidkey);
                }
                else if (element->FirstChildElement("coord"))
                {
                    key.push_back(_name + "_ControlPoint");
                    std::vector<IdKey> newvidkey;
                    VCoordParseXml(element->FirstChildElement("coord"), newvidkey, vcoord);
                    vidkey.push_back(newvidkey);
                }
                else if (element->FirstChildElement("coordfix")) { //coordfix
                    VCoordParseXml(element->FirstChildElement("coordfix"), vcoord);
                }
                parseValueCont(weightsCont, element->FirstChildElement("weight"));
                parseValueCont(as_bone, element->FirstChildElement("as_bones"));
                parseValueCont(as_skin, element->FirstChildElement("as_skin"));
                const char* w = element->Attribute("weight"); // global weight is set as attribute
                if (w)
                    weight = std::stod(w);
                const char* b = element->Attribute("as_bones");
                if (b)
                    globalAs_bones = std::stod(b);
                const char* s = element->Attribute("as_skin");
                if (s)
                    globalAs_skin = std::stod(s);
				return true;
			}

			void AssociationControlPoint::fillmetadata( FEModel* femodel, Metadata& metadata) const {
                InteractionControlPoint set;
                set.setControlPointRole(_ctrlPtRole.c_str());
                set.setName(_name);
                if (vcoord.size() == 0) {
                    auto itt = vidkey.begin();
                    for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                        for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                            GroupNodePtr cur = femodel->get<GroupNode>(*it, *itid);
                            NodePtr curn = femodel->get<Node>(*it, *itid);
                            if (cur == nullptr && curn == nullptr) {
                                stringstream str;
                                str << "For Control Point '" << name << "': group or nodes '" << *it << "', ID " << itid->infoId() << ", are not defined in the model." << endl;
                                throw std::runtime_error(str.str().c_str());
                            }
                            if (cur != nullptr) {
                                VId currentGroupNode = femodel->getGroupNode(cur->getId()).get();
                                for (VId::const_iterator it2 = currentGroupNode.begin(); it2 != currentGroupNode.end(); ++it2)
                                    set.addNodeId(femodel->getNode(*it2).getId());
                            }
                            else if (curn != nullptr)
                                set.addNodeId(curn->getId());
                        }
                        itt++;
                    }
                }
                else {
                    if (key.size() > 0) // coords by ID
                    {
                        for (int i = 0; i < vcoord.size(); i++) {
                            std::shared_ptr<Node> nd = std::make_shared<Node>();
                            nd->setCoord(vcoord[i]);
                            femodel->set<Node>(nd, key[0], vidkey[0][i]);
                            set.addNodeId(nd->getId());
                        }
                    }
                    else // coordfix
                        set.getControlPt3dCont() = vcoord;
                }
                set.setWeights(weightsCont);
                set.setAssociation(as_bone, as_skin);
                // all points have been set, set the weight
                set.setGlobalWeight(weight);
                set.setGlobalAssociationBones(globalAs_bones);
                set.setGlobalAssociationSkin(globalAs_skin);
                //ctrlPt.duplicateCtrlPtFromFEModel(*femodel);
                //ctrlPt.setControlPointRole(_ctrlPtRole.c_str());
                //ctrlPt.duplicateCtrlPtFromFEModel(*femodel);
                metadata.addInteractionControlPoint(set);
			}

			bool AssociationJoint::parseXml( tinyxml2::XMLElement* element) {
				//type = TYPE::JOINT;
				name = element->Attribute("name");
				entityA = element->FirstChildElement("entity_master")->Attribute("name");
				entityB = element->FirstChildElement("entity_slave")->Attribute("name");
				vector<vector<IdKey>> tmpidkey;
                vector<string> tmpkey;
				if (element->FirstChildElement("entity_master")->FirstChildElement("setFrame")->Attribute("type","global")) {
					typeFrameA = "global";
				}
				else
				{
					typeFrameA = "local";
				}
                keywordParseXml(element->FirstChildElement("entity_master")->FirstChildElement("setFrame")->FirstChildElement("keyword"), tmpkey, tmpidkey);
                keyFrameA = tmpkey[0];
                idFrameA = tmpidkey[0][0];

				if (element->FirstChildElement("entity_slave")->FirstChildElement("setFrame")->Attribute("type","global")) {
					typeFrameB = "global";
				}
				else
				{
					typeFrameB = "local";
				}
                keywordParseXml(element->FirstChildElement("entity_slave")->FirstChildElement("setFrame")->FirstChildElement("keyword"), tmpkey, tmpidkey);
                keyFrameB = tmpkey[0];
                idFrameB = tmpidkey[0][0];

                parseValueCont(dof, element->FirstChildElement("setDof"));
				if (dof.size()!=6) {
					stringstream str;
                    str << "For joint: " << name << endl;
					str << " dof size is is not equal to 6 "<<endl;
					throw std::runtime_error(str.str().c_str());
				}
                if (element->FirstChildElement("setConstrainedDofType"))
                    constrainedDofType = element->FirstChildElement("setConstrainedDofType")->Attribute("value");
				return true;
			}

			void AssociationJoint::fillmetadata( FEModel* femodel, Metadata& metadata) const {
				EntityJoint newJoint( name);
				if (!metadata.hasEntity( entityA)) {
					stringstream str;
                    str << "For joint: '" << name << "': entity '" << entityA <<"' is not defined in the model."<<endl;
					throw std::runtime_error(str.str().c_str());
				}
				if (!metadata.hasEntity( entityB)) {
					stringstream str;
                    str << "For joint '" << name << "': entity '" << entityB <<"' is not defined in the model."<<endl;
					throw std::runtime_error(str.str().c_str());
				}
				newJoint.setEntity1( entityA);
				newJoint.setEntity2( entityB);
				if (typeFrameA=="local") {
                    FramePtr cur = femodel->get<FEFrame>(keyFrameA, idFrameA);
                    if (cur == nullptr) {
						stringstream str;
                        str << "For joint: '" << name << "': frame '" << keyFrameA << "', ID " << idFrameA.infoId() << ", is not defined in the model." << endl;
						throw std::runtime_error(str.str().c_str());
					}
                    newJoint.setEntity1FrameId(cur->getId());
				}
				else {
                    NodePtr cur = femodel->get<Node>(keyFrameA, idFrameA);
                    if (cur  == nullptr) {
						stringstream str;
                        str << "For joint '" << name << "': node '" << keyFrameA << "', ID " << idFrameA.infoId() << ", is not defined in the model." << endl;
						throw std::runtime_error(str.str().c_str());
					}
					// add global frame in the femodel
                    FramePtr frglobal = std::make_shared<FEFrame>();
					//Frame* frglobal = new Frame( "CreatedFrame", name);
                    frglobal->setOrigin(cur->getId());
					femodel->set(frglobal, "CreatedFrame", IdKey(name));
					newJoint.setEntity1FrameId( frglobal->getId());
				}
				//
				if (typeFrameB=="local") {
                    FramePtr cur = femodel->get<FEFrame>(keyFrameB, IdKey(idFrameB));
                    if (cur == nullptr) {
						stringstream str;
                        str << "For joint '" << name << "': frame '" << keyFrameB << "', ID " << idFrameB.infoId() << ", is not defined in the model." << endl;
						throw std::runtime_error(str.str().c_str());
					}
                    newJoint.setEntity2FrameId(cur->getId());
				}
				else {
                    NodePtr cur = femodel->get<Node>(keyFrameB, idFrameB);
                    if (cur == nullptr) {
						stringstream str;
                        str << "For joint '" << name << "': node '" << keyFrameB << "', ID " << idFrameB.infoId() << ", is not defined in the model." << endl;
						throw std::runtime_error(str.str().c_str());
					}
					if (idFrameB!=idFrameA) {
						// add global frame in the femodel
                        FramePtr frglobal = std::make_shared<FEFrame>();
						//Frame* frglobal = new Frame( "CreatedFrame", name+"_slave");
                        frglobal->setOrigin(cur->getId());
						femodel->set(frglobal, "CreatedFrame", IdKey(name + "_slave"));
						newJoint.setEntity2FrameId( frglobal->getId());
					}
					else {
						newJoint.setEntity2FrameId(femodel->get<FEFrame>( "CreatedFrame",IdKey(name))->getId());
					}
				}

				std::array<bool,6> arraydof;
				int n=0;
				for (std::vector<bool>::const_iterator it=dof.begin(); it!=dof.end(); ++it) {
					arraydof[n] = *it;
					n++;
				}
				newJoint.setDof( arraydof);
                if (!constrainedDofType.empty())
                    newJoint.setConstrainedDofType( EntityJoint::strToConstrainedDofType.at(constrainedDofType));
				//
				metadata.addJoint( newJoint);
			}




            bool AssociationAnatomicalJoint::parseXml( tinyxml2::XMLElement* element) {
                name = element->Attribute("name");
                parseValueCont(dof, element->FirstChildElement("setDof"));
                if (dof.size()!=6) {
                    stringstream str;
                    str << "For joint '" << name << "': dof size is not equal to 6." << endl;
                    throw std::runtime_error(str.str().c_str());
                }
                if (element->FirstChildElement("setConstrainedDofType"))
                    constrainedDofType = element->FirstChildElement("setConstrainedDofType")->Attribute("value");
                return true;
            }

            void AssociationAnatomicalJoint::fillmetadata( FEModel* femodel, Metadata& metadata) const {
                AnatomicalJoint newJoint( name);

                std::array<bool,6> arraydof;
                int n=0;
                for (std::vector<bool>::const_iterator it=dof.begin(); it!=dof.end(); ++it) {
                    arraydof[n] = *it;
                    n++;
                }
                newJoint.setDof( arraydof);
                if (!constrainedDofType.empty())
                    newJoint.setConstrainedDofType( EntityJoint::strToConstrainedDofType.at(constrainedDofType));
                //
                metadata.addAnatomicalJoint( newJoint);
            }

			bool Associationparameter::parseXml( tinyxml2::XMLElement* element) {
				// fill context to keep this information 
				name = element->Attribute("name");
				source = element->Attribute("source");
				if (element->FirstChildElement("keyword")) {
					element = element->FirstChildElement("keyword");
                    keywordParseXml(element, key, vidkey);
				}
				//else { //an empty element mean we keep all occurence for this parameter
				//	vector<IdFormat> vidf;
				//	parametercontext = vidf;
				//}
				return true;
			}

			void Associationparameter::fillmetadata( FEModel* femodel, Metadata& metadata) const {
				std::map<std::string, SId> parameterRequired;
				SId sid;
                if (!key.empty()) {
                    // based on keyword, get type of mesh component
                    auto itt = vidkey.begin();
                    for (vector<string>::const_iterator it = key.begin(); it != key.end(); ++it) {
                        if (itt->size() > 0) {
                            for (auto itid = itt->begin(); itid != itt->end(); ++itid) {
                                FEModelParameterPtr cur = femodel->get<FEModelParameter>(*it, *itid);
                                if (cur != nullptr)
                                    sid.insert(cur->getId());
                                else {
                                    stringstream str;
                                    str << "For parameter '" << name << "': parameter '" << source 
                                        << "' is not defined in keyword '" << *it << "', ID " << itid->infoId() << "." << endl;
                                    throw std::runtime_error(str.str().c_str());
                                }
                            }
                        }
                        else {
                            VId vtmp = femodel->m_registorModelparameter.getIdfromIdFormat(*it);
                            sid.insert(vtmp.begin(), vtmp.end());
                        }
                        itt++;
                    }
                }
                HbmParameter newParam(name);
                newParam.setSource(source);
                std::copy(sid.begin(), sid.end(), std::back_inserter(newParam.FEparameters));
                metadata.addHbmparameter(newParam);
            }

			AssociationContact::AssociationContact(): keepthickness(false), thickness(0.0) {}

			bool AssociationContact::parseXml( tinyxml2::XMLElement* element) {
				name = element->Attribute("name");
				type  = element->Attribute("type");
				if  (element->FirstChildElement("thickness") != nullptr) {
					keepthickness = element->FirstChildElement("thickness")->BoolAttribute("keep");
					if (element->FirstChildElement("thickness")->GetText() != nullptr) {
						std::stringstream thick(element->FirstChildElement("thickness")->GetText());
						thick >> thickness;
					}
				}
				entity1 = element->FirstChildElement("entity_contact_1")->Attribute("name");
                vector<vector<IdKey>> tmpidkey;
                vector<string> tmpkey;
                if (nullptr != element->FirstChildElement("entity_contact_1")->FirstChildElement("setGroup")) {
                    keywordParseXml(element->FirstChildElement("entity_contact_1")->FirstChildElement("setGroup")->FirstChildElement("keyword"), tmpkey, tmpidkey);
                    key1 = tmpkey[0];
                    id1 = tmpidkey[0][0];
                    tmpidkey.clear();
                    tmpkey.clear();
                }
				entity2 = element->FirstChildElement("entity_contact_2")->Attribute("name");
                if (nullptr != element->FirstChildElement("entity_contact_2")->FirstChildElement("setGroup")) {
                    keywordParseXml(element->FirstChildElement("entity_contact_2")->FirstChildElement("setGroup")->FirstChildElement("keyword"), tmpkey, tmpidkey);
                    key2 = tmpkey[0];
                    id2 = tmpidkey[0][0];
                    tmpidkey.clear();
                    tmpkey.clear();
                }
                return true;
			}

			void AssociationContact::fillmetadata( FEModel* femodel, Metadata& metadata) const {
				EntityContact newContact;
				newContact.setName( name);
				newContact.setType( type);
				if (keepthickness)
					newContact.setkeepThickness();
				if (!keepthickness)
					newContact.setThickness( thickness);
                GroupNodePtr cur = femodel->get<GroupNode>(key1, id1);
                if (cur != nullptr)
                    newContact.setEntity1(entity1, cur->getId());
                else
                    newContact.setEntity1(entity1);
                cur = femodel->get<GroupNode>(key2, id2);
                if (cur != nullptr)
                    newContact.setEntity2(entity2, cur->getId());
                else
                    newContact.setEntity2(entity2);
				metadata.addContact(newContact);
			}



		}
	}
}
