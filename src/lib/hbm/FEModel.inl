/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

namespace piper {
    namespace hbm {

        template<class contT, class T>
        void FEModel::setComponent(contT* container, std::shared_ptr<T> component) {
            container->setEntity(component);
        }

        template<typename T, typename TT>
        void FEModel::parseIdXml(tinyxml2::XMLElement* element, IdRegistrator<T, TT>& registrator, FEModel* fem) {
            //is it compress or not
            bool compress = false;
            element->QueryBoolAttribute("compress", &compress);
            VId vidfe, vid;
            std::vector<std::string> vkey, vidstrvendor;

            if (!compress) {
                tinyxml2::XMLElement* etlparse = element->FirstChildElement("idPiper");
                while (etlparse != nullptr) {
                    parseValueCont(vid, etlparse);
                    etlparse = etlparse->NextSiblingElement("idPiper");
                }
                etlparse = element->FirstChildElement("idFormat");
                while (etlparse != nullptr) {
                    parseValueCont(vidfe, etlparse);
                    etlparse = etlparse->NextSiblingElement("idFormat");
                }
                etlparse = element->FirstChildElement("key");
                while (etlparse != nullptr) {
                    parseValueCont(vkey, etlparse);
                    etlparse = etlparse->NextSiblingElement("key");
                }
                etlparse = element->FirstChildElement("NameFormat");
                while (etlparse != nullptr) {
                    parseValueCont(vidstrvendor, etlparse);
                    for (auto it = vidstrvendor.begin(); it != vidstrvendor.end(); ++it) {
                        if (*it == "none")
                            *it = "";
                    }
                    etlparse = etlparse->NextSiblingElement("NameFormat");
                }
            }
            else {
                int repeat;
                // idPiper
                {
                    tinyxml2::XMLElement* etlparse = element->FirstChildElement("idPiper");
                    std::stringstream elemValue(etlparse->GetText());
                    Id curid;
                    while (elemValue >> curid >> repeat) {
                        vid.push_back(curid);
                        if (repeat > 0)
                            for (int n = 1; n <= repeat; n++)
                                vid.push_back(curid+n);
                    }
                }
                // idFormat
                {
                    tinyxml2::XMLElement* etlparse = element->FirstChildElement("idFormat");
                    std::stringstream elemValue(etlparse->GetText());
                    Id curidformat;
                    while (elemValue >> curidformat >> repeat) {
                        vidfe.push_back(curidformat);
                        if (repeat > 0)
                            for (int n = 1; n <= repeat; n++)
                                vidfe.push_back(curidformat+n);
                    }
                }
                // key
                {
                    tinyxml2::XMLElement* etlparse = element->FirstChildElement("key");
                    std::stringstream elemValue(etlparse->GetText());
                    std::string curkey;
                    while (elemValue >> curkey >> repeat) {
                        vkey.push_back(curkey);
                        if (repeat > 0)
                            for (int n = 0; n < repeat; n++)
                                vkey.push_back(curkey);
                    }
                }
                // NameFormat
                {
                    tinyxml2::XMLElement* etlparse = element->FirstChildElement("NameFormat");
                    std::stringstream elemValue(etlparse->GetText());
                    std::string curnameforamt;
                    while (elemValue >> curnameforamt >> repeat) {
                        if (curnameforamt == "none")
                            curnameforamt = "";
                        vidstrvendor.push_back(curnameforamt);
                        if (repeat > 0)
                            for (int n = 0; n < repeat; n++)
                                vidstrvendor.push_back(curnameforamt);
                    }
                }
            }
            //
            VId::const_iterator itvidfe = vidfe.begin();
            std::vector<std::string>::const_iterator itvidstrvendor = vidstrvendor.begin();
            std::vector<std::string>::const_iterator itvkey = vkey.begin();

            for (auto it = vid.begin(); it != vid.end(); ++it) {
                if (*itvidfe == -1 )
                    registrator.registerId(fem->get<TT>(*it), *itvkey, IdKey(*itvidstrvendor));
                else
                    registrator.registerId(fem->get<TT>(*it), *itvkey, IdKey(*itvidfe));
                ++itvkey;
                ++itvidfe;
                ++itvidstrvendor;
            }

        }

    }
}

