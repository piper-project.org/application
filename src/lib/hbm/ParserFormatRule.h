/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PARSERFORMATRULE__H
#define PARSERFORMATRULE__H

#include "ParserHelper.h"

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

namespace piper {
    namespace hbm {
        namespace parser {
            /// enum of piper component type
            enum class MESHCOMP{
                // keep this enum int-iteratable (so we can do "for each type, do ..."), i.e. do not assign arbitrary (without refactoring the code)
                UNDEFINED = 0,
                // do not change order - there are several cycles depending on it. if you need to add something, make sure you don't break existing code
                include = 1, componentNode = 2, componentElement1D = 3, componentElement2D = 4, componentElement3D = 5,
                componentGNode = 6, componentGElement1D = 7, componentGElement2D = 8, componentGElement3D = 9, componentGGroup = 10, componentFrame = 11,
                // if possible, leave this one as last - for cycle iterating through this enum have end condition as (i <= componentModelParameter)
                // if it is not possible, update the cycles
                componentModelParameter = 12 
            };

            enum KW_TYPE { FULL_LINE, REG_EXP };

            /// <summary>
            /// A keyword
            /// </summary>
            class Kw
            {
            public:
                std::string name; // distinct identifier of the keyword
                KW_TYPE type; // how it should be parsed

                Kw(std::string name, KW_TYPE type) : name(name), type(type) {}

                // comparison operator so that Kw can be used as key for std::map - map by name
                bool operator< (const Kw& otherKw) const
                {
                    return this->name < otherKw.name;
                }
            };

            class ParserContext;
            /*!
            *  \brief struct to store format information shared by all rules
            *
            *  \param component: string that defines the type of component
            *  \param setkw:  set of keywords
            *
            */
            struct formatinfo {
                formatinfo() : format("undefined"), separator("none") {}
                std::string format;
                std::vector<int> commcar; // comment character
                std::set<Kw> inckw; // include keywords
                std::string separator;
            };

            class ParserFormatRule;

            class HBM_EXPORT TermParserFormatRule : public Prototype<TermParserFormatRule> {
            public:
                TermParserFormatRule() {}
                virtual ~TermParserFormatRule();
                virtual void addTerm(std::shared_ptr<TermParserFormatRule> t);

                // returns false if parsing failed, true if it was succesful
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                virtual void eval(ParserContext* p);
                virtual void evalUpdate(ParserContext* p);
                //virtual void clear();
                std::vector<std::shared_ptr<TermParserFormatRule>> children;
            protected:
                static Factory<TermParserFormatRule> facTermParserFormatRule;
            private:
                virtual void evalUpdateSpecial(ParserContext* p){};

            };

            typedef std::shared_ptr<TermParserFormatRule> TermParserFormatRulePtr;



            class rule;
            class HBM_EXPORT ParserFormatRule : public TermParserFormatRule {
            public:
                enum ActionType { PARSE, UPDATE };
                enum OPTION{ EVAL, EVAL_NODES, EVALSECOND, UPDATE_FILES };

                ParserFormatRule() {};
                ParserFormatRule(const std::string& file);

                typedef std::set<Kw> keywordcont;
                
                /*!
                *  \brief Adds a new set of keywords for a mesh component, a rule must be defined for this keywords.
                *  These keywords will be parsed by matching an entire line in the source file to them.
                *
                *  \param component: string that defines the type of component
                *  \param setkw:  set of keywords
                *  \param type: specifies how are those keywords parsed when looking for them in the source files
                *
                */
                void addrequiredKeywords(std::string const& component, std::vector<std::string> const& setkw, KW_TYPE type);


                void setKeywordrule(std::string const& kwName, std::shared_ptr<rule> setrule);
                
                /// <summary>
                /// Gets all keywords defined for a specified components.
                /// </summary>
                /// <param name="meshcomp">The component name.</param>
                /// <returns>List of all keywords - concatenated both fullLine and regexp keywords.</returns>
                std::set<std::string> getKeywordsComponents(MESHCOMP const& meshcomp) const;

                void setActionType(ActionType action);


                void setOption(OPTION option) { m_option = option; }

                keywordcont const getListKeywords() const;

                void applyrules(ParserContext* p) const;

                /// container of format information
                formatinfo m_format;

            private:
                typedef std::map<MESHCOMP, keywordcont> MeshCompCont;
                typedef std::map<std::string, std::shared_ptr<rule>> KeywordParserCont;
                typedef std::map<std::string, MESHCOMP> Map_Comp_String; // maps xml node names designating keywords to the MESHCOMP enum

                static Map_Comp_String m_mapFormatRule;
                static Map_Comp_String create_mapFormatRule();

                /// container of keyword registered for each piper component				
                MeshCompCont m_meshcomp;
                ///container of keyword parsing rule for each registered keyword
                KeywordParserCont keywordrules;
                // apply rule for parsing or updating 
                ActionType m_actionType;
                // option: identify which keywords will be parser or update
                OPTION m_option;


                // get MeshComp from string
                MESHCOMP getMeshCompfromString(const std::string& str);
                // chack that all rules are define for all required keywords
                void checkKwList() const;
                //register TermParserFormatRule
                void initFactory();
                //
                keywordcont const defineKwList() const;
            };


            class formatInformation : public TermParserFormatRule {
            public:
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class comment : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class includeFile : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class separator : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class meshComponent : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };



            class elementOrdering : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p){}
            };

            class rule : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            };

            class Condition : public TermParserFormatRule {
            public:
                bool getConditionValue(ParserContext* p);
            private:
            };


            class body : public TermParserFormatRule {
            public:
            };

            class TermWithCondition : public TermParserFormatRule {
            public:
                TermWithCondition();
                ~TermWithCondition();
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);

            protected:
                std::unique_ptr<Condition> condition;
                TermParserFormatRulePtr body;
            };



            class notfind : public Condition {
            public:
                notfind();
                void eval(ParserContext* p);
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            private:
                std::string m_val;
                std::string pos;
            };

            class find : public Condition {
            public:
                find();
                void eval(ParserContext* p);
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            private:
                std::string m_val;
                std::string pos;
            };

            class equal : public Condition {
            public:
                void eval(ParserContext* p);
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            private:
                std::string m_val;
            };





            class doWhile : public TermWithCondition {
            public:
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            };


            class doIf : public TermWithCondition {
            public:
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            };


            class forEach : public TermParserFormatRule {
            public:
                forEach() : body(nullptr)  {}
                ~forEach();
                void evalUpdate(ParserContext* p);
                void eval(ParserContext* p);
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            protected:
                TermParserFormatRulePtr body;
                std::string m_varfor;
                std::string m_variter;
            };

            class nextLine : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            protected:
                void evalUpdateSpecial(ParserContext* p);
            };


            class curLine : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            };

            class parse : public TermParserFormatRule {
            public:
            };

            //class parsedVar {
            //public:
            //    parsedVar() : var(nullptr), varType(Type::UNDEF) {}
            //    ~parsedVar() {
            //        if (var != nullptr)
            //            delete m_var;
            //    }

            //    Type varType;
            //    var* m_var;
            //};

            class parseRule : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            protected:
                void evalUpdateSpecial(ParserContext* p);
            private:
                bool parsevar;
                static Type getTypefromStr(const std::string& typestr);
                //varCont m_var;
                paramParseCont m_paramparse;
                catchParamCont m_catchparam;
                void generateCatchParam();
            };

            struct OrdercatchParamCont {
                inline bool operator () (const catchParam &_left, const int &start) {
                    return _left.start < start;
                }
                inline bool operator () (const int &start, const catchParam &_right) {
                    return start < _right.start;
                }
            };

            inline bool ComparatorContainer(const catchParam& pair1, const catchParam& pair2)
            {
                return pair1.start < pair2.start;
            }

            class variableAssign : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            };

            class variable : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            private:
                std::string m_var;
            };


            class generateId : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            protected:
                std::string m_start;
                std::string m_end;
                std::string m_var;
            };

            class append : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            protected:
                std::string m_value;
                std::string m_varassign;
                std::string m_var;
            };

            class clearVar : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            protected:
                std::vector<std::string> m_var;
            };

            //class assign : public TermParserFormatRule {
            //public:
            //    bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            //    TermParserFormatRulePtr Clone() const { return new assign(*this); }
            //    void eval(ParserContext* p);
            //protected:
            //    std::string m_varassign;
            //    std::string m_var;
            //    std::string m_value;
            //};


            class object : public TermParserFormatRule {
            public:
                object() {}
                object(const std::string type) :m_type(type) {}
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            protected:
                std::string m_type;
            };

            class objectModelFile : public object {
            public:
                objectModelFile() : object("Model") {}
            };

            class objectNode : public object {
            public:
                objectNode() : object("Node") {}
            };


            class objectElement3D : public object {
            public:
                objectElement3D() : object("Element3D") {}
            };


            class objectElement2D : public object {
            public:
                objectElement2D() : object("Element2D") {}
            };

            class objectElement1D : public object {
            public:
                objectElement1D() : object("Element1D") {}
            };

            class objectGroup : public object {
            public:
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class objectFrame : public object {
            public:
                objectFrame() : object("Frame") {}
            };

            class objectJoint : public object {
            public:
                objectJoint() : object("Joint") {}
            };


            class objectParameter : public object {
            public:
                objectParameter() : object("Parameter") {}
            };



            class Method : public TermParserFormatRule {
            public:
                virtual bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                virtual void eval(ParserContext* p);
                virtual void evalUpdate(ParserContext* p);
            protected:
                std::vector<std::string> m_input;
                std::vector<methodContext::MethodInputType> m_inputType;
                std::string m_methodname;

                void evalUpdateSpecial(ParserContext* p);
            };

            class setFile : public Method {
            public:
                void eval(ParserContext* p);
                void evalUpdate(ParserContext* p);
            protected:
                void evalUpdateSpecial(ParserContext* p);

            };


            class setId : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            };


            class setCenter : public Method {
            public:
            };

            class setAxis : public Method {
            public:
            };

            class setName : public Method {
            public:
            };

            class setPart : public Method {
            public:
            };

            class setType : public Method {
            public:
            };

            class setCoord : public Method {
            public:
            };


            class setElemDef : public Method {
            public:
            };

            class addInGroup : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            };


            class sourceId : public TermParserFormatRule {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
                void eval(ParserContext* p);
            private:
                std::set<std::string> m_source;
            };

            class setOrigin : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class setFirstAxis : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class setPlane : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };

            class setFirstDirection : public Method {
            public:
            };


            class setSecondDirection : public Method {
            public:
            };

            class addNode : public Method {
            public:
            };

            class setFrame : public Method {
            public:
            };

            class setDof : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            private:
                std::vector<bool> dof;
            };

            class setValue : public Method {
            public:
                bool parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p);
            };



        }
    }
}

#endif
