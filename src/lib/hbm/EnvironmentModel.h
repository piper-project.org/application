/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_ENVMODEL_H
#define PIPER_HBM_ENVMODEL_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <memory>

#include <units/units.h>

#include "FEModel.h"

namespace piper {
namespace hbm {
	namespace parser {
		class ParserModelFile;
	}

/**
* @brief A EnvironmentModel.
*
* Container for a FEModel with only nodes ad elements that defines an environement FE model
*
* @author Erwan Jolivet @date 2015
*/
class HBM_EXPORT EnvironmentModel {
public:

	/*!
	*  \brief Constructor
	*
	*  Constructor of EnvironmentModel
	*
	*/
    EnvironmentModel()
        : m_lengthUnit(units::LengthUnit::defaultUnit())
    {}
	/*!
	*  \brief Constructor: For solid element(s), a onverlay of 2D elements over the set of slid elements will be automatically generated and solid elements are deleted (with nodes)
	*
	*  Constructor of EnvironmentModel
	*  \param name: name of the environment
	*  \param file: FE main file of the environemen model
	*  \param formatrulesFile: format rule file to parse the environement model
    *  \param lengthUnit: unit for length
	*
	*/
    EnvironmentModel(std::string const& name, std::string const& file, std::string const& formatrulesFile, units::Length lengthUnit);

	/*!
	*  \brief 
	*
	*  return the FEmodel of the environement
	*
	*/
    FEModel const& envmodel() const { return m_envmodel; }
    FEModel& envmodel() { return m_envmodel; }
	/*!
	*  \brief
	*
	*  return the name of the environement
	*
	*/
    std::string const& name() const { return m_name; }
	/*!
	*  \brief
	*
	*  return absolute path of the main FE file of the environement
	*
	*/
	std::string const file() const { return m_file; }
	/*!
	*  \brief
	*
	*  return the length unit of the environement
	*
	*/
    units::Length const& lengthUnit() const { return m_lengthUnit; }
	/*!
	*  \brief
	*
	*  change coordinates of node of the environement model according to the target lenght unit
	*
	*  \param targetunit: target lenght unit
	*
	*/
    void scaleLength(units::Length targetunit);
	/*!
	*  \brief
	*
	*  return absolute path of the format rule file
	*
	*
	*/
	std::string sourceFormat() const;

	void clear();


private:
	FEModel m_envmodel;
	std::string m_name;
	std::string m_file;
    units::Length m_lengthUnit;
	std::string m_sourceFormat;

};

typedef std::shared_ptr<EnvironmentModel> EnvironmentModelPtr;


}
}

#endif
