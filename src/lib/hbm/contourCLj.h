/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_CONTOURCLJ_H
#define PIPER_CONTOURS_CONTOURCLJ_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif
#include "Contour.h"
#include "FEFrame.h"

namespace piper {
	namespace hbm {

		/**
		* @brief A class which involves the data structure of Joint.
		* It is intended for defining the joints and edges(bones). Object of HbmSkeleton should be prepopulated from the landmark information provided externally. 
		* it currently contains the datastructure for HingeJoint type which has 1 DOF.
		*
		* @author Aditya Chhabra @date 2016 
		*/
		class ContourCLbr;
		class Bone;

		class HBM_EXPORT ContourCLj {
		public:

			ContourCLj();
			~ContourCLj();
            
      
            std::string name;
            std::vector<Contour*> jointcont;
            ContourCLbr* childbodyregion;
            ContourCLbr* parentbodyregion;
            std::vector<std::string> childbrlandmarks;
            std::vector<std::string> parentbrlandmarks;
            std::vector<Eigen::Vector3d> ctrlpointschild;
            std::vector<Eigen::Vector3d> ctrlpointsparent;
            std::string skingm;
            std::string fleshgm;
			std::string entityjointname;
			std::string circum;
			std::string j_type;
			std::vector<std::string> joint_circum_vec;
			std::vector<std::string> joint_circum_vec_info;
			Id c_entity1FrameId, c_entity2FrameId;
			std::string special_landmark1;
			std::string special_landmark2;
			bool sp = false;
			std::string joint_splinep0;
			std::string joint_splinep1;
			std::string joint_splinet1;
			std::string joint_splinet2;
			std::vector<std::string> flexionaxeslandmarks;
			std::vector<std::string> twistaxeslandmarks;
			std::vector<std::string> twistaxeslandmarks_info;
            std::vector<Eigen::Vector3d> flexionaxes;
            std::array<bool, 6> c_dof;
            std::array<double, 3> c_max_angle;
            std::array<double, 3> c_min_angle;
			std::vector<double> absoluteangle;
            FEFrame jointframeparent;
            FEFrame jointframechild;
            // Spline jointspline
            double jointangle;
            double jointcenter;
            std::string circumlandmark;
            Eigen::Vector3d circumele;
			std::string joint_description;
			std::vector<std::string> special_landmarks_vec;
			std::vector<std::string> special_landmarks_vec_info;
			std::vector<std::string> spline_vec_info;
			std::vector<std::string> spline_vector;
			std::vector<std::string> flexionaxeslandmarks_info;
			std::vector<double> spline_tangent;

			std::string display_name;

			int degreesOfFreedom=0;
			std::vector<std::pair<std::string, std::string>> required_metadata;

			std::vector<std::string> dof1_target;
			std::vector<std::string> dof1_ref;
			std::vector<std::string> dof1_normal;
			std::vector<std::string> dof1_target_info;
			std::vector<std::string> dof1_ref_info;
			std::vector<std::string> dof1_normal_info;
			double dof1_limit_max = 0;
			double dof1_limit_min = 0;

			std::vector<std::string> dof2_target;
			std::vector<std::string> dof2_ref;
			std::vector<std::string> dof2_normal;
			std::vector<std::string> dof2_target_info;
			std::vector<std::string> dof2_ref_info;
			std::vector<std::string> dof2_normal_info;
			double dof2_limit_max = 0;
			double dof2_limit_min = 0;

			std::vector<std::string> dof3_target;
			std::vector<std::string> dof3_ref;
			std::vector<std::string> dof3_normal;
			std::vector<std::string> dof3_target_info;
			std::vector<std::string> dof3_ref_info;
			std::vector<std::string> dof3_normal_info;
			double dof3_limit_max = 0;
			double dof3_limit_min = 0;


        public:
            void setName(std::string jtname);
            void setJointContours(std::vector<Contour*>);
            void setChildBodyRegion(ContourCLbr*);
            void setParentBodyRegion(ContourCLbr*);
            void setCtrlPointsChild(std::vector<std::string>);
            void setCtrlPointsParent(std::vector<std::string>);
            void setChilBrLandmarks(ContourCLbr*);
            void setParentBrLandmarks(ContourCLbr*);
            void setSkinGM(std::string name);
            void setFleshGM(std::string name);
            void setFlexionAxesLandmarks(std::vector<std::string>);
            void setFlexionAxes(std::vector<std::string>);
            void computeJointFrame(ContourCLbr*, std::vector<Eigen::Vector3d>);
            void computeJointSpline(std::vector<Eigen::Vector3d>, std::vector<Eigen::Vector3d>);
            void setCircumLandmark(std::string);
            void computeCircumEle(std::string);
            
            
			std::vector<Contour*> getJointContours();
			std::vector<std::string> getFlexionAxisLanadmarks();
			std::vector<std::string> getJointSplineInfo();
			std::vector<std::string> getJointSplineVector();

                
            


		};



	}
}
#endif

