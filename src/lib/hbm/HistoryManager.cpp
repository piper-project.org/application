/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "HistoryManager.h"

#include "Helper.h"
#include "xmlTools.h"

#include <sstream>
#include <iostream>

namespace piper {
    namespace hbm {

        HistoryManager::HistoryManager() {
            init();
        }

        HistoryManager::~HistoryManager() {
        }

        HistoryManager& HistoryManager::operator=(const HistoryManager& other)
        {
            if (this != &other)
            {
                // copy m_orderhistory and m_maphistory
                clear();
                std::vector<std::string> otherOrderhistory = other.getHistoryList();
                for (auto it = otherOrderhistory.begin(); it != otherOrderhistory.end(); it++)
                {
                    m_orderhistory.push_back(*it);
                }
                // copy current active ID and set it as active
                m_id = other.getCurrent();
            }
            return *this;
        }

        void HistoryManager::init() {
            m_orderhistory.push_back("init");
            m_id = "init";
            //for (auto & cur : m_data) {
            //    //cur->addNewHistory("init");
            //    cur->setActive("init");
            //}
        }

        void HistoryManager::clear() {
            m_orderhistory.clear();
            init();
        }

        std::vector<std::string> const HistoryManager::getHistoryList() const {
            return m_orderhistory;
        }

        std::string HistoryManager::getValidHistoryName(std::string trame) {
            getValidName(trame, getHistoryList());
            return trame;
        }




        void HistoryManager::addNewHistory(std::string const& stringID) {
            // reset top history to the current data
         //   resetHistoryTo(m_id); - do not reset the history to allow toggle between two different states before choosing which one is the desired one
            if (!isHistory(stringID)) {
                m_orderhistory.push_back(stringID);
                for (auto & cur : m_data) {
                    cur->addNewHistory(stringID);
                }
                setCurrentModel(stringID);
            }
            else {
                std::stringstream str;
                str << "Can not add new model : " << stringID << " already exists in history." << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
        }

        void HistoryManager::setCurrentModel(std::string const& stringID) {
            if (std::find(m_orderhistory.begin(), m_orderhistory.end(), stringID) == m_orderhistory.end())
                throw std::runtime_error("Invalid history id: "+stringID);
            for (auto & cur : m_data) {
                cur->setActive(stringID);
            }
            m_id = stringID;
        }

        void HistoryManager::renameCurrentModel(std::string const& stringID) {
            if (!isHistory(stringID)) {
                std::string stringID2;
                if (m_id.substr(0, 1) == "*" && stringID.substr(0, 1) != "*")
                    std::string stringID2 = "*" + stringID;
                else
                    stringID2 = stringID;
                std::vector<std::string>::iterator it = std::find(m_orderhistory.begin(), m_orderhistory.end(), m_id);
                *it = stringID2;
                m_id = stringID2;

                for (auto & cur : m_data)
                    cur->renameActiveHistory(stringID);
            }
            else {
                std::stringstream str;
                str << "Can not rename current model : " << stringID << " already exists in history." << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
        }

        bool HistoryManager::isHistory(std::string const& stringID) const {
            return (std::find(m_orderhistory.begin(), m_orderhistory.end(), stringID) != m_orderhistory.end());
        }

        bool HistoryManager::resetHistoryTo(std::string const& stringID) {
            if (isHistory(stringID)) {
                std::vector<std::string> toerase;
                std::vector<std::string>::reverse_iterator it = m_orderhistory.rbegin();
                while (*it != stringID) {
                    toerase.push_back(*it);
                    m_orderhistory.pop_back();
                    it = m_orderhistory.rbegin();
                }

                for (auto & cur : m_data)
                    cur->deleteHistory(toerase);
                return true;
            }
            else return false;
        }

        std::string const& HistoryManager::getCurrent() const {
            return m_id;
        }

        void HistoryManager::setData(HistoryBase * data) {
            m_data.push_back(data);
        }

        void HistoryManager::read(std::string const& file) {
            tinyxml2::XMLDocument model;
            model.LoadFile(file.c_str());
            if (model.Error()) {
                clear();
                std::stringstream str;
                str << "Failed to load file: " << file << std::endl << model.ErrorStr() << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
            tinyxml2::XMLElement* element;
            // model name in history
            element = model.FirstChildElement("piper")->FirstChildElement("model_name");
            renameCurrentModel(element->GetText());
        }

        void HistoryManager::write(std::string const& file) {
            tinyxml2::XMLDocument model;
            model.LoadFile(file.c_str());
            tinyxml2::XMLElement* xmlPiper = model.FirstChildElement("piper");

            // write name of the current model in history
             tinyxml2::XMLElement*  element = model.NewElement("model_name");
            std::string modelname = m_id.c_str();
            if (modelname.substr(0, 1) == "*")
                modelname = modelname.substr(1, std::string::npos);
            element->SetText(modelname.c_str());
            tinyxml2::XMLElement* preced = xmlPiper->FirstChildElement("units");
            xmlPiper->InsertAfterChild(preced, element);

            model.SaveFile(file.c_str());

        }

    }
}
