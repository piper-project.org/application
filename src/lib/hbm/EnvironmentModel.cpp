/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "EnvironmentModel.h"

#include "ParserModelFile.h"

#define ENV_LOADING_PROFILING 0

namespace piper { 
	namespace hbm {

		EnvironmentModel::EnvironmentModel( std::string const& name, std::string const& file, 
            std::string const& formatrulesFile, units::Length lengthUnit) 
        {
#if ENV_LOADING_PROFILING
            time_t start = clock();
#endif
			parser::ParserModelFile* p = new parser::ParserModelFile( formatrulesFile);
            m_envmodel.parse(p, file, std::vector<std::string>(), PARSER_OPTION::MESH_ONLY);
#if ENV_LOADING_PROFILING
            time_t end = clock();
            std::cout << "parsed in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            start = clock();
#endif
            // gather elemendef of 3D elements and delete it
            Elements3D const& e3d = m_envmodel.getElements3D();
            std::vector<ElemDef> env2d, elem3d;
            for (Elements3D::const_iterator it = e3d.begin(); it != e3d.end(); it++) {
                elem3d.push_back(it->get()->get());
            }
            m_envmodel.deleteAllComponents<Element3D>();
            m_envmodel.deleteAllComponents<GroupElements3D>();
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "gathered and deleted 3D in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            //extract 2D elements
            start = clock();
#endif
            env2d = Element3D::extractElement2DfromElement3Ddefinition(elem3d);
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "extracted 2D from 3D in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            //  add 2D elements if not already defineds
            start = clock();
#endif
            Elements2D const& e2d = m_envmodel.getElements2D();
            std::vector<ElemDef> elem2d, env2d_ok;
            for (Elements2D::const_iterator it = e2d.begin(); it != e2d.end(); it++) {
                elem2d.push_back(it->get()->get());
            }
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "loaded 2D ele in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            start = clock();
#endif
            std::sort(elem2d.begin(), elem2d.end(), OrderElemDef());
            std::sort(env2d.begin(), env2d.end(), OrderElemDef());
            std::set_difference(env2d.begin(), env2d.end(), elem2d.begin(), elem2d.end(), back_inserter(env2d_ok), OrderElemDef());
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "made diff in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            start = clock();
#endif
            //add 2D elements
            for (ElemDef &cur : env2d_ok) {
                Element2DPtr e2d = std::make_shared<Element2D>();
                e2d.get()->set(cur);
                m_envmodel.set<Element2D>(e2d);
            }
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "set 2D ele in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            start = clock();
#endif
            //delee free nodes
            VId freenodevid = m_envmodel.freenodes();
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "gathered free nodes in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
            start = clock();
#endif
            m_envmodel.deleteComponents<Node>(freenodevid);
#if ENV_LOADING_PROFILING
            end = clock();
            std::cout << "deleted free nodes in " << ((double)(end - start) / CLOCKS_PER_SEC) << "s" << std::endl;
#endif
            //update vtk representation - happens in the end of deleteComponents<Nodes>
           // m_envmodel.getFEModelVTK()->setMesh(m_envmodel);
			m_file=file;
			m_name = name;		
            m_lengthUnit = lengthUnit;
			m_sourceFormat = formatrulesFile;
            delete p;
		}

		void EnvironmentModel::clear() {
			m_envmodel.clear();
			m_name.clear();
			m_file.clear();
            m_lengthUnit = units::LengthUnit::defaultUnit();
		}

        void EnvironmentModel::scaleLength(units::Length targetunit) {
            if (m_lengthUnit == targetunit)
                return;
            VId::const_iterator itn;
            VId const& vid = m_envmodel.getNodes().listId();
            for (itn = vid.begin(); itn != vid.end(); ++itn)
                units::convert(m_lengthUnit, targetunit, m_envmodel.get<Node>(*itn)->get());
            m_lengthUnit = targetunit;
		}

		//std::string const EnvironmentModel::file(std::string const& originpath) const {
		//	return absolutePath(m_file, originpath);
		//}

		//std::string EnvironmentModel::sourceFormat(std::string const& originpath) const {
		//	return absolutePath(m_sourceFormat, originpath);
		//}

		std::string EnvironmentModel::sourceFormat() const {
			return m_sourceFormat;
		}


	}
}
