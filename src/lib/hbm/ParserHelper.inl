/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <iomanip>
#include <array>

#include <boost/algorithm/string.hpp>

#include "FEModel.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace piper {
	namespace hbm {
		namespace parser {

            template< typename T>
            void Factory<T>::Register(const std::string& key, std::function<std::shared_ptr<T>()> obj)
            {
                if (m_map.find(key) == m_map.end()) {
                    m_map[key] = obj;
                }
            }

            template< typename T>
            std::shared_ptr<T> Factory<T>::Create(const std::string& key) const {
                auto it = m_map.find(key);
                if (it != m_map.end())  {
                    return it->second();
                }
                return nullptr;
            }

            template< typename T>
            void Factory<T>::clear() {
                m_map.clear();
            }

  
            template<typename T>
            void VarBase<T>::reserve(size_t const& n) {
                m_var.reserve(n);
            }

            template<typename T>
            void VarBase<T>::get(T& var, const int& n) const {
                var = m_var.at(n);
            }

            template<typename T>
            void VarBase<T>::get(std::vector<T>& var) const {
                var = m_var;
            }

            template<typename T>
            size_t VarBase<T>::size() const  {
                return m_var.size();
            }

            template<typename T>
            void VarBase<T>::setValue(T const& value) {
                m_var.push_back(value);
            }

            template<typename T>
            void VarBase<T>::setValue(std::vector<T> const& value) {
                m_var = value;
            }

            template<typename T>
            void VarBase<T>::convertIdKey(std::vector<IdKey>& idv) const {
                for (typename std::vector<T>::const_iterator it = m_var.begin(); it != m_var.end(); ++it) {
                    //if (m_type != Type::F)
                    idv.push_back(IdKey((*it)));
                }
            }

            template<typename T>
            varInterface* VarBase<T>::clone() const { return new VarBase<T>(*this); }

            template<typename T>
            void var::get(T& var, const int& n) const {
                myinterface->get(var, n);
            }

            template<typename T>
            void var::get(std::vector<T>& var) const {
                myinterface->get(var);
            }

            template<typename T>
            void var::pushValue(T const& value) {
                myinterface->setValue<T>(value);
            }
            
            template<typename T>
            vectorVar::vectorVar(Type const& type, T const& value) : m_type(type) {
				m_var.push_back(new var(m_type, value));
            }

            template<typename T>
            vectorVar::vectorVar(Type const& type, std::vector<T> const& value) : m_type(type) {
                m_var.push_back(new var(m_type, value));
            }

            template<typename T>
            void vectorVar::setValue(T const& value) {
                if (m_var.size()>0)
                    m_var.back()->pushValue(value);
                else {
                    var* newvar = new var(m_type, value);
                    m_var.push_back(newvar);
                }
            }

            template<typename T>
            void vectorVar::setValue(Type type, std::vector<T> const& value) {
                var* newvar = new var(type, value);
                m_var.push_back(newvar);
            }

            template<typename T>
            void localVar::getLastValue(T& output, const std::string& varname) const {
                if (map_var.find(varname) != map_var.end())
                    map_var.at(varname)->getLast()->get(output, 0);
                else {
                    std::stringstream str;
                    str << "using undefined variable: " << varname << endl;
                    throw std::runtime_error(str.str().c_str());
                }
            }

            template<typename T>
            void localVar::getLastValue(std::vector<T>& output, const std::string& varname) const {
                if (map_var.find(varname) != map_var.end())
                    map_var.at(varname)->getLast()->get(output);
                else {
                    std::stringstream str;
                    str << "using undefined variable: " << varname << endl;
                    throw std::runtime_error(str.str().c_str());
                }
            }

            template<typename T>
            void localVar::setVar(const std::string& varname, T const& value) {
                map_var[varname]->setValue(value);
            }

            template<typename T>
            void localVar::setVar(const std::string& varname, Type type, std::vector<T> const& value) {
                if (map_var.find(varname) == map_var.end()) {
                    vectorVar* newvectorvar = new vectorVar(type, value);
                    map_var[varname] = newvectorvar;
                }
                else
                    map_var[varname]->setValue(type, value);
            }


		}
	}
}
