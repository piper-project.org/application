/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_TARGET_H
#define PIPER_HBM_TARGET_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <list>
#include <array>
#include <memory>

#include <tinyxml/tinyxml2.h>

#include <Eigen/Geometry>

#include "units/units.h"
#include "hbm/types.h"

namespace piper {
namespace hbm {

/** Base class for classes which represent targets.
 *
 * A target classe should define
 * - a static std::string type(); to return a string representation of their type.
 * It is used to write/read the target to/from an xml file.
 * - a value() method to return the value of the target.
 */
class HBM_EXPORT AbstractTarget {
public:

    AbstractTarget() {}

    void setName(std::string const& name) {m_name=name;}
    std::string const& name() const {return m_name;}

    /// parse this target from \a element
    virtual void parseXml(const tinyxml2::XMLElement* element);

    /// add xml representation of this target to \a element
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

    /// The norm of the current error
//    virtual double errorNorm(HumanBodyModel) const = 0;

protected:
    /// a target has a name
    std::string m_name;

};

/** A generic class to represent target units.
 */
template< typename T>
class HBM_EXPORT TargetUnit {
public:

    typedef typename units::UnitTraits<T> UnitType;

    TargetUnit() : m_unit(UnitType::defaultUnit())
    {}

    /// parse target unit from \a element
    virtual void parseXml(const tinyxml2::XMLElement* element) {
        if (element->FirstChildElement("units") != nullptr)
        {
            const char *unit = element->FirstChildElement("units")->Attribute(UnitType::type().c_str());
            if (unit == nullptr)
            {
                std::stringstream er;
                er << "Unit does not match target type: target " << element->FirstChildElement("name")->GetText()
                    << " of type " << element->Attribute("type") << " must be in " << UnitType::type() << " units, but unit \""
                    << element->FirstChildElement("units")->FirstAttribute()->Value() << "\" was provided instead.";
                throw std::runtime_error(er.str());
            }
            m_unit = UnitType::strToUnit(unit);
        }
    }

    /// add xml representation of this target unit to \a element
    virtual void serializeXml(tinyxml2::XMLElement* element) const {
        tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("units");
        xmlName->SetAttribute(UnitType::type().c_str(), UnitType::unitToStr(m_unit).c_str());
        element->InsertEndChild(xmlName);
    }

    /// convert the target to \a lengthUnit
    void convertToUnit(T const& targetUnit) {
        if (m_unit == targetUnit)
            return;
        doConvertToUnit(targetUnit);
        m_unit = targetUnit;
    }

    /// get unit
    T const& unit() const { return m_unit; }
    /// set unit
    void setUnit(T const& unit) { m_unit = unit; }

protected:
    /// do the actual convertion job
    virtual void doConvertToUnit(T targetLengthUnit) = 0;
    T m_unit; ///< unit of the target


};

/** A class to represent a landmark position target.
 */
class HBM_EXPORT FixedBoneTarget : public AbstractTarget {
public:
    FixedBoneTarget() {}
    FixedBoneTarget(std::string bone);
    std::string bone;

    /// @return a string representation of the target type
    static std::string type() {return "FixedBone";}

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;
};

/** A class to represent a landmark position target.
 */
class HBM_EXPORT LandmarkTarget : public AbstractTarget, public TargetUnit<units::Length> {
public:

    LandmarkTarget();
    /** the key of the map is the coordinate index, it must be in [0,2]
     * {0:"x"},{1:"y"},{2:"z"}
     */
    typedef std::map<unsigned int, double> ValueType;

    /// true: this dof is controlled by the target, false: it is not
    typedef std::array<bool, 3> MaskType;

    std::string landmark;

    /// @return a string representation of the target type
    static std::string type() {return "Landmark";}

///iitd
//    Coord const& value() const {return m_value;}
//    void setValue(Coord const& value) {m_value=value;}
//	Coord getCoord() { return m_value; }
///iitd

	ValueType const& value() const {return m_value;}
    void setValue(ValueType const& value);

    MaskType mask() const;

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

    double const& getKrigingUseBone() const;
    void setKrigingUseBone(double const& value);

    double const& getKrigingUseSkin() const;
    void setKrigingUseSkin(double const& value);


protected:
    /// the coordinate
    ValueType m_value;
    virtual void doConvertToUnit(units::Length targetLengthUnit);
    double m_kriging_use_bone, m_kriging_use_skin;
};

/** A generic class to represent a transformation target.
 */
class HBM_EXPORT AbstractRigidTransformationTarget : public AbstractTarget, public TargetUnit<units::Length> {
public:

    /** the key of the map is the coordinate index, it must be in [0,5]
     * {0:"x"},{1:"y"},{2:"z"},{3:"rx"},{4:"ry"},{5:"rz"}
     * the value is the target value, [rx, ry,rz] is a rotation vector (direction is the rotation axis, the norm is the angle in radian)
     */
    typedef std::map<unsigned int, double> ValueType;

    /// true: this dof is controlled by the target, false: it is not
    typedef std::array<bool, 6> MaskType;

    ValueType const& value() const {return m_value;}
    void setValue(ValueType const& value);

    MaskType mask() const;

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    /// target values for each degrees of freedom of the joint.
    ValueType m_value;
    virtual void doConvertToUnit(units::Length targetLengthUnit);
};

/** A class to represent the position of a joint.
 */
class HBM_EXPORT JointTarget : public AbstractRigidTransformationTarget {
public:
    JointTarget() {}
    JointTarget(std::string joint, ValueType const& value);

    std::string joint;

    /// @return a string representation of the target type
    static std::string type() {return "Joint";}

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

};

/** A class to represent the transformation between two frames.
 */
class HBM_EXPORT FrameToFrameTarget : public AbstractRigidTransformationTarget {
public:

    std::string frameSource, frameTarget;
    /// if true, the transformation from frameSource to frameTarget is relative, expressed in frameSource
    bool isRelative;

    /// @return a string representation of the target type
    static std::string type() {return "FrameToFrame";}

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

};

/** A class to represent a ScalingParameter target.
*/
class HBM_EXPORT ScalingParameterTarget : public AbstractTarget {
public:

	/// @return a string representation of the target type
	static std::string type() { return "ScalingParameter"; }

    std::vector<double> const& value() const { return m_value; }
    void setValue(std::vector<double> const& value) { m_value = value; }
    void setValue(double const& value) { m_value.push_back(value); }

    void parseXml(const tinyxml2::XMLElement* element);
	virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
	/// the coordinate
	std::vector<double> m_value;
};

/** A class to represent an anthropometric dimensions target.
*/
class HBM_EXPORT AnthropometricDimensionTarget : public AbstractTarget, public TargetUnit<units::Length> {
public:
    AnthropometricDimensionTarget();

    /// @return a string representation of the target type
    static std::string type() { return "AnthropometricDimension"; }

    /// set the absolute target value for the dimension
    double value() const { return m_value; }
    void setValue(double value);
    bool hasAbsoluteValue() const { return m_value != -1; }

    /// set the scale factor for the dimension
    double scale() const { return m_scale; }
    void setScale(double scale);
    bool hasScaleValue() const { return m_scale != -1; }

    std::string const& posture() const { return m_posture; }
    void setPosture(std::string const& posture);

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    double m_value;
    double m_scale;
    std::string m_posture;
    virtual void doConvertToUnit(units::Length targetLengthUnit);
};

/** A class to represent the age target.
*/
class HBM_EXPORT AgeTarget : public AbstractTarget, public TargetUnit<units::Age> {
public:
    AgeTarget() : AbstractTarget(), TargetUnit() {}

    /// @return a string representation of the target type
    static std::string type() { return "Age"; }

    double value() const { return m_value; }
    void setValue(double value);

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    double m_value;
    virtual void doConvertToUnit(units::Age targetUnit);
};

/** A class to represent the weight target.
*/
class HBM_EXPORT WeightTarget : public AbstractTarget, public TargetUnit<units::Mass> {
public:
    WeightTarget() : AbstractTarget(), TargetUnit() {}

    /// @return a string representation of the target type
    static std::string type() { return "Weight"; }

    double value() const { return m_value; }
    void setValue(double value);

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    double m_value;
    virtual void doConvertToUnit(units::Mass targetUnit);
};

/** A class to represent the height target.
*/
class HBM_EXPORT HeightTarget : public AbstractTarget, public TargetUnit<units::Length> {
public:
    HeightTarget() : AbstractTarget(), TargetUnit() {}

    /// @return a string representation of the target type
    static std::string type() { return "Height"; }

    double value() const { return m_value; }
    void setValue(double value);

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    double m_value;
    virtual void doConvertToUnit(units::Length targetUnit);
};


/** A class to represent a list of 3D coord control_point target.
*/
class HBM_EXPORT ControlPoint : public AbstractTarget, public TargetUnit<units::Length> {
public:

    /// @return a string representation of the target type
    static std::string type() { return "ControlPoint"; }

    std::vector<Coord> const& value() const { return m_value; }
    void setValue(std::vector<Coord> const& value) { m_value = value; }

    // name of the subset of control points to which this point belongs
    void setSubsetName(std::string subname) { m_subsetName = subname; }
    std::string const& subsetName() const { return m_subsetName; }

    void parseXml(const tinyxml2::XMLElement* element);
    virtual void serializeXml(tinyxml2::XMLElement* element) const;

protected:
    std::vector<Coord> m_value;
    // usually will be paired with source InteractionControlPoint that has name == to m_name, 
    // and subset of the InteractionControlPoint to which this point belong will have name == m_subsetName
    std::string m_subsetName; 
    virtual void doConvertToUnit(units::Length targetLengthUnit);
};

/** A list of targets.
 */
class HBM_EXPORT TargetList {
public:
    TargetList() {}

    /// @return true if all target lists are empty
    bool empty() const;

    /// clear all target lists
    void clear();

    /// @return the number of targets in this target list
    size_t size() const;

    void add(FixedBoneTarget const& t) {fixedBone.push_back(t);}
    void add(LandmarkTarget const& t) {landmark.push_back(t);}
    void add(JointTarget const& t) {joint.push_back(t);}
    void add(FrameToFrameTarget const& t) {frameToFrame.push_back(t);}
    void add(ScalingParameterTarget const& t) {scalingparameter.push_back(t);}
    void add(AnthropometricDimensionTarget const& t) { anthropometricDimension.push_back(t); }
    void add(ControlPoint const& t) { controlPoint.push_back(t); }
    void add(AgeTarget const& t) { age.push_back(t); }
    void add(WeightTarget const& t) { weight.push_back(t); }
    void add(HeightTarget const& t) { height.push_back(t); }


    /// <summary>
    /// Removes a target from this list.
    /// </summary>
    /// <param name="name">Name of the target. If it is not present in the list, nothing will happen.
    /// Removes the first target with that name, then terminates - if there are duplicities, only the first one is removed.</param>
    void removeFixedBone(std::string name);
    void removeLandmark(std::string name);
    void removeJoint(std::string name);
    void removeFrameToFrame(std::string name);
    void removeScalingParameter(std::string name);
    void removeAnthropometricDimension(std::string name);
    void removeControlPoint(std::string name);
    void removeAge(std::string name);
    void removeWeight(std::string name);
    void removeHeight(std::string name);

    /// the list of anthropometric dimension targets
    std::list<AnthropometricDimensionTarget> anthropometricDimension;
    /// the list of fixed bones
    std::list<FixedBoneTarget> fixedBone;
    /// the list of landmark targets
    std::list<LandmarkTarget> landmark;
    /// the list of joint targets
    std::list<JointTarget> joint;
    /// the list of frame transformation targets
    std::list<FrameToFrameTarget> frameToFrame;
    /// the list of scalingParameter targets
	std::list<ScalingParameterTarget> scalingparameter;
    /// the list of controlpoint targets
    std::list<ControlPoint> controlPoint;
    /// age target, empty or one element
    std::list<AgeTarget> age;
    /// weight target, empty or one element
    std::list<WeightTarget> weight;
    /// height target, empty or one element
    std::list<HeightTarget> height;

    /// <summary>
    /// Parses targetList from \a xmlElement
    /// </summary>
    /// <param name="parentElement">The parent element.</param>
    /// <returns>Semicolon-delimited error messages for each unrecognized target type or empty string if no errors occured.</returns>
    std::string parseXml(const tinyxml2::XMLElement* parentElement);


    /// <summary>
    /// Read targetList from file \a filename. Targets with unrecognized types are skipped.
    /// </summary>
    /// <param name="filename">The file to read.</param>
    /// <returns>Semicolon-delimited error messages for each unrecognized target type or empty string if no errors occured.</returns>
    std::string read(std::string const& filename);
    /// Serialize targetList to \a xmlElement
    void serializeXml(tinyxml2::XMLElement* parentElement) const;
    /// Write targetList to file \a filename
    void write(std::string const& filename) const;


    void convertToUnit(units::Length targetLengthUnit);
    void convertToUnit(units::Mass targetMassUnit);
    void convertToUnit(units::Age targetAgeUnit);

private:

    /// create a new target of the given \a type and add it to the corresponding list
    AbstractTarget *addTarget(std::string type);
    
    /// <summary>
    /// Adds the object pointed to by the provided pointer among the list of targets.
    /// </summary>
    /// <param name="t">The pointer to the target. The pointer must be a pointer to a subclass matching the <c>type</c> parameter.</param>
    /// <param name="type">The type of the target. If it is not supported, the target is not added anywhere.</param>
    void addTarget(std::shared_ptr<AbstractTarget> t, std::string type);
    
    /// <summary>
    /// Creates a new target.
    /// </summary>
    /// <param name="type">The type of the target.</param>
    /// <returns>Pointer to the newly created target or nullptr if the type is unsupported.</returns>
    std::shared_ptr<AbstractTarget> createTarget(std::string type);
    
    template<class TargetType>
    void targetSerializeXml(TargetType const& target, tinyxml2::XMLElement* xmlTargetList) const
    {
        tinyxml2::XMLElement* xmlTarget = xmlTargetList->GetDocument()->NewElement("target");
        xmlTarget->SetAttribute("type", target.type().c_str());
        target.serializeXml(xmlTarget);
        xmlTargetList->InsertEndChild(xmlTarget);
    }
};

} // namespace hbm
} // namespace piper

#endif // PIPER_HBM_TARGET_H
