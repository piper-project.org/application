/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParserFormatRule.h"

#include "ParserContext.h"

#include "xmlTools.h"
#include "Element.h"
#include "Helper.h"

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;
namespace piper {
	namespace hbm {
		namespace parser {

            Factory<TermParserFormatRule> TermParserFormatRule::facTermParserFormatRule = Factory<TermParserFormatRule>();

            TermParserFormatRule::~TermParserFormatRule() {
                //for (auto it = children.begin(); it != children.end(); ++it) {
                //    if (*it != nullptr)
                //        delete *it;
                //}
                //children.clear();
                /*facTermParserFormatRule.clear();*/
            }

            bool TermParserFormatRule::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
                if (element != nullptr) {
                    element = element->FirstChildElement();
                    while (element != nullptr) {
                        //std::cout << element->Name() << std::endl;
                        TermParserFormatRulePtr newterm = facTermParserFormatRule.Create(element->Name());
                        if (newterm == nullptr) {
                            std::stringstream str;
                            str << "Definition of parsing rules: unknown command '" << element->Name() << "' on line " << element->GetLineNum() << endl;
                            throw std::runtime_error(str.str().c_str());
                            return false;
                        }
                        if (newterm->parseXml(element, p))
                            addTerm(newterm);
                        element = element->NextSiblingElement();
                    }
                }
                return true;
            }

            void TermParserFormatRule::addTerm(TermParserFormatRulePtr t) {
                children.push_back(t);
            }

            void TermParserFormatRule::eval(ParserContext* p) {
                vector<TermParserFormatRulePtr>::iterator it;
                for (it = children.begin(); it != children.end(); ++it) {
                    (*it)->eval(p);
                }
            }

            void TermParserFormatRule::evalUpdate(ParserContext* p) {
                vector<TermParserFormatRulePtr>::iterator it;
                for (it = children.begin(); it != children.end(); ++it) {
                    (*it)->evalUpdate(p);
                }
            }

            void ParserFormatRule::initFactory() {

                Factory<TermParserFormatRule>::Register("formatInformation", std::function<std::shared_ptr<formatInformation>()>(std::make_shared<formatInformation>));
                Factory<TermParserFormatRule>::Register("comment", std::function<std::shared_ptr<comment>()>(std::make_shared<comment>));
                Factory<TermParserFormatRule>::Register("includeFile", std::function<std::shared_ptr<includeFile>()>(std::make_shared<includeFile>));
                Factory<TermParserFormatRule>::Register("separator", std::function<std::shared_ptr<separator>()>(std::make_shared<separator>));
                Factory<TermParserFormatRule>::Register("meshComponent", std::function<std::shared_ptr<meshComponent>()>(std::make_shared<meshComponent>));
                Factory<TermParserFormatRule>::Register("elementOrdering", std::function<std::shared_ptr<elementOrdering>()>(std::make_shared<elementOrdering>));
                Factory<TermParserFormatRule>::Register("rule", std::function<std::shared_ptr<rule>()>(std::make_shared<rule>));

                // cond
                Factory<TermParserFormatRule>::Register("doIf", std::function<std::shared_ptr<doIf>()>(std::make_shared<doIf>));
                Factory<TermParserFormatRule>::Register("doWhile", std::function<std::shared_ptr<doWhile>()>(std::make_shared<doWhile>));
                Factory<TermParserFormatRule>::Register("condition", std::function<std::shared_ptr<Condition>()>(std::make_shared<Condition>));
                Factory<TermParserFormatRule>::Register("notfind", std::function<std::shared_ptr<notfind>()>(std::make_shared<notfind>));
                Factory<TermParserFormatRule>::Register("find", std::function<std::shared_ptr<find>()>(std::make_shared<find>));
                Factory<TermParserFormatRule>::Register("equal", std::function<std::shared_ptr<equal>()>(std::make_shared<equal>));
                Factory<TermParserFormatRule>::Register("body", std::function<std::shared_ptr<body>()>(std::make_shared<body>));
                Factory<TermParserFormatRule>::Register("forEach", std::function<std::shared_ptr<forEach>()>(std::make_shared<forEach>));

                //parse action
                Factory<TermParserFormatRule>::Register("nextLine", std::function<std::shared_ptr<nextLine>()>(std::make_shared<nextLine>));
                Factory<TermParserFormatRule>::Register("parseRule", std::function<std::shared_ptr<parseRule>()>(std::make_shared<parseRule>));
                Factory<TermParserFormatRule>::Register("curLine", std::function<std::shared_ptr<curLine>()>(std::make_shared<curLine>));
                Factory<TermParserFormatRule>::Register("parse", std::function<std::shared_ptr<parse>()>(std::make_shared<parse>));
                Factory<TermParserFormatRule>::Register("variableAssign", std::function<std::shared_ptr<variableAssign>()>(std::make_shared<variableAssign>));
                Factory<TermParserFormatRule>::Register("variable", std::function<std::shared_ptr<variable>()>(std::make_shared<variable>));

                //utility
                Factory<TermParserFormatRule>::Register("generateId", std::function<std::shared_ptr<generateId>()>(std::make_shared<generateId>));
                Factory<TermParserFormatRule>::Register("append", std::function<std::shared_ptr<append>()>(std::make_shared<append>));
                //Factory<TermParserFormatRule>::Register("assign", new assign);
                Factory<TermParserFormatRule>::Register("clearVar", std::function<std::shared_ptr<clearVar>()>(std::make_shared<clearVar>));

                //object 
                Factory<TermParserFormatRule>::Register("objectModelFile", std::function<std::shared_ptr<objectModelFile>()>(std::make_shared<objectModelFile>));
                Factory<TermParserFormatRule>::Register("objectNode", std::function<std::shared_ptr<objectNode>()>(std::make_shared<objectNode>));
                Factory<TermParserFormatRule>::Register("objectElement3D", std::function<std::shared_ptr<objectElement3D>()>(std::make_shared<objectElement3D>));
                Factory<TermParserFormatRule>::Register("objectElement2D", std::function<std::shared_ptr<objectElement2D>()>(std::make_shared<objectElement2D>));
                Factory<TermParserFormatRule>::Register("objectElement1D", std::function<std::shared_ptr<objectElement1D>()>(std::make_shared<objectElement1D>));
                Factory<TermParserFormatRule>::Register("objectGroup", std::function<std::shared_ptr<objectGroup>()>(std::make_shared<objectGroup>));
                Factory<TermParserFormatRule>::Register("objectFrame", std::function<std::shared_ptr<objectFrame>()>(std::make_shared<objectFrame>));
                Factory<TermParserFormatRule>::Register("objectJoint", std::function<std::shared_ptr<objectJoint>()>(std::make_shared<objectJoint>));
                Factory<TermParserFormatRule>::Register("objectParameter", std::function<std::shared_ptr<objectParameter>()>(std::make_shared<objectParameter>));


                // object method
                Factory<TermParserFormatRule>::Register("setFile", std::function<std::shared_ptr<setFile>()>(std::make_shared<setFile>));
                Factory<TermParserFormatRule>::Register("setId", std::function<std::shared_ptr<setId>()>(std::make_shared<setId>));
                Factory<TermParserFormatRule>::Register("setType", std::function<std::shared_ptr<setType>()>(std::make_shared<setType>));
                Factory<TermParserFormatRule>::Register("setName", std::function<std::shared_ptr<setName>()>(std::make_shared<setName>));
                Factory<TermParserFormatRule>::Register("setPart", std::function<std::shared_ptr<setPart>()>(std::make_shared<setPart>));
                Factory<TermParserFormatRule>::Register("setCoord", std::function<std::shared_ptr<setCoord>()>(std::make_shared<setCoord>));
                Factory<TermParserFormatRule>::Register("setElemDef", std::function<std::shared_ptr<setElemDef>()>(std::make_shared<setElemDef>));
                Factory<TermParserFormatRule>::Register("addInGroup", std::function<std::shared_ptr<addInGroup>()>(std::make_shared<addInGroup>));
                Factory<TermParserFormatRule>::Register("sourceId", std::function<std::shared_ptr<sourceId>()>(std::make_shared<sourceId>));
                Factory<TermParserFormatRule>::Register("setOrigin", std::function<std::shared_ptr<setOrigin>()>(std::make_shared<setOrigin>));
                Factory<TermParserFormatRule>::Register("setFirstAxis", std::function<std::shared_ptr<setFirstAxis>()>(std::make_shared<setFirstAxis>));
                Factory<TermParserFormatRule>::Register("setPlane", std::function<std::shared_ptr<setPlane>()>(std::make_shared<setPlane>));
                Factory<TermParserFormatRule>::Register("setFirstDirection", std::function<std::shared_ptr<setFirstDirection>()>(std::make_shared<setFirstDirection>));
                Factory<TermParserFormatRule>::Register("setSecondDirection", std::function<std::shared_ptr<setSecondDirection>()>(std::make_shared<setSecondDirection>));
                Factory<TermParserFormatRule>::Register("setCenter", std::function<std::shared_ptr<setCenter>()>(std::make_shared<setCenter>));
                Factory<TermParserFormatRule>::Register("setAxis", std::function<std::shared_ptr<setAxis>()>(std::make_shared<setAxis>));
                Factory<TermParserFormatRule>::Register("setFrame", std::function<std::shared_ptr<setFrame>()>(std::make_shared<setFrame>));
                Factory<TermParserFormatRule>::Register("setDof", std::function<std::shared_ptr<setDof>()>(std::make_shared<setDof>));
                Factory<TermParserFormatRule>::Register("setValue", std::function<std::shared_ptr<setValue>()>(std::make_shared<setValue>));
            }
            
            ParserFormatRule::ParserFormatRule(const string& file) {
                initFactory();
				tinyxml2::XMLDocument model;
				model.LoadFile(file.c_str());
				if (model.Error()) {
                    stringstream str;
                    str << "[FAILED] Load format rule file: " << file << ". Error: " << model.ErrorStr() << endl;
                    throw std::runtime_error(str.str().c_str());
                    return;
				}
				tinyxml2::XMLElement* element;
				//
				element = model.FirstChildElement("format_description");
                try
                {
                    TermParserFormatRule::parseXml(element, this);
                    //
                    checkKwList();
                }
                catch (std::runtime_error e)
                {
                    stringstream str;
                    str << "[FAILED] Load format rule file: " << file << ". Error: " << e.what() << endl;
                    throw std::runtime_error(str.str().c_str());
                    return;
                }
			}

            void ParserFormatRule::addrequiredKeywords(std::string const& component, std::vector<std::string> const& setkw, KW_TYPE type) {
                MESHCOMP mc = getMeshCompfromString(component);
                auto mcIt = m_meshcomp.find(mc);
                if (mcIt == m_meshcomp.end())
                    m_meshcomp[mc] = keywordcont();
                for (string const &s : setkw)
                    m_meshcomp[mc].insert(Kw(s, type));
            }


            ParserFormatRule::Map_Comp_String ParserFormatRule::m_mapFormatRule = ParserFormatRule::create_mapFormatRule();

            MESHCOMP ParserFormatRule::getMeshCompfromString(const string& str) {
                return m_mapFormatRule[str];
            }

            ParserFormatRule::Map_Comp_String ParserFormatRule::create_mapFormatRule() {
                ParserFormatRule::Map_Comp_String m;
                m["includeFile"] = MESHCOMP::include;
                m["componentNode"] = MESHCOMP::componentNode;
                m["componentElement1D"] = MESHCOMP::componentElement1D;
                m["componentElement2D"] = MESHCOMP::componentElement2D;
                m["componentElement3D"] = MESHCOMP::componentElement3D;
                m["componentGNode"] = MESHCOMP::componentGNode;
                m["componentGElement1D"] = MESHCOMP::componentGElement1D;
                m["componentGElement2D"] = MESHCOMP::componentGElement2D;
                m["componentGElement3D"] = MESHCOMP::componentGElement3D;
                m["componentGGroup"] = MESHCOMP::componentGGroup;
                m["componentFrame"] = MESHCOMP::componentFrame;
                m["componentModelParameter"] = MESHCOMP::componentModelParameter;
                return m;
            }
            
            void ParserFormatRule::checkKwList() const {
                // check if al required rules are described
                keywordcont keywordlist;
                for (int meshcomp_iter = 1; meshcomp_iter <= (int)MESHCOMP::componentModelParameter; meshcomp_iter++) // start from 1, not 0 - 0 is MESHCOMP::UNDEFINED, we don't want that
                {
                    auto kws = m_meshcomp.find(static_cast<MESHCOMP>(meshcomp_iter)); // all keywords for a given MESHCOMP
                    if (kws != m_meshcomp.end())
                    {
                        for (Kw const& kw : kws->second) // for each keyword, check if there is a rule for it 
                        {
                            if (keywordrules.find(kw.name) == keywordrules.end()) {
                                stringstream str;
                                str << "Definition of parsing rules: missing rules for Keyword '" << kw.name << "'." << endl;
                                throw std::runtime_error(str.str().c_str());
                                return;
                            }
                        }
                    }
                }
            }

            void ParserFormatRule::setKeywordrule(string const& kwName, shared_ptr<rule> setrule)
            {
                Kw dummy(kwName, KW_TYPE::FULL_LINE);
                for (auto & kws : m_meshcomp)
                {
                    keywordcont::iterator keyword = kws.second.find(dummy);
                    if (keyword != kws.second.end())
                    {
                        keywordrules[kwName] = setrule;
                        return;
                    }
                }
                dummy.type = KW_TYPE::REG_EXP;
                for (auto & kws : m_meshcomp)
                {
                    keywordcont::iterator keyword = kws.second.find(dummy);
                    if (keyword != kws.second.end())
                    {
                        keywordrules[kwName] = setrule;
                        return;
                    }
                }
                stringstream str;
                str << "Keyword '" << kwName << "' is not associated with a mesh component." << std::endl;
                throw runtime_error(str.str().c_str());
            }

            set<string> ParserFormatRule::getKeywordsComponents(MESHCOMP const& meshcomp) const {
                set<string> ret;
                auto kws = m_meshcomp.find(meshcomp);
                if (kws != m_meshcomp.end())
                {
                    for (Kw const& k : kws->second)
                        ret.insert(k.name);
                }
                return ret;
            }


            void ParserFormatRule::setActionType(ActionType action) {
                m_actionType = action;
                if (m_actionType == ActionType::UPDATE)
                    m_option = OPTION::UPDATE_FILES;
            }

            ParserFormatRule::keywordcont const ParserFormatRule::getListKeywords() const {
                ParserFormatRule::keywordcont const listKw = defineKwList();
                return listKw;
            }
            
            ParserFormatRule::keywordcont const ParserFormatRule::defineKwList() const{
                // get keyword to parser regarding option
                keywordcont m_keywordlist;
				if (m_option == OPTION::EVAL_NODES) {
					m_keywordlist.insert(m_format.inckw.begin(), m_format.inckw.end());
					m_keywordlist.insert(m_meshcomp.at(MESHCOMP::componentNode).begin(), m_meshcomp.at(MESHCOMP::componentNode).end());
				}
				else if (m_option == OPTION::EVAL) {
                    for (int meshcomp_iter = (int)MESHCOMP::include; meshcomp_iter <= (int)MESHCOMP::componentElement3D; meshcomp_iter++)
                    {
                        auto comp = m_meshcomp.find(static_cast<MESHCOMP>(meshcomp_iter));
                        if (comp != m_meshcomp.end())
                            m_keywordlist.insert(comp->second.begin(), comp->second.end());
                    }
                }
                else if (m_option == OPTION::UPDATE_FILES) {
                    auto comp = m_meshcomp.find(MESHCOMP::include);
                    if (comp != m_meshcomp.end())
                        m_keywordlist.insert(comp->second.begin(), comp->second.end());

                    comp = m_meshcomp.find(MESHCOMP::componentNode);
                    if (comp != m_meshcomp.end())
                        m_keywordlist.insert(comp->second.begin(), comp->second.end());

                    comp = m_meshcomp.find(MESHCOMP::componentModelParameter);
                    if (comp != m_meshcomp.end())
                        m_keywordlist.insert(comp->second.begin(), comp->second.end());
                }
                else if (m_option == OPTION::EVALSECOND) 
                { 
                    auto comp = m_meshcomp.find(MESHCOMP::include);
                    if (comp != m_meshcomp.end())
                        m_keywordlist.insert(comp->second.begin(), comp->second.end());

                    for (int meshcomp_iter = (int)MESHCOMP::componentGNode; meshcomp_iter <= (int)MESHCOMP::componentModelParameter; meshcomp_iter++)
                    {
                        auto comp = m_meshcomp.find(static_cast<MESHCOMP>(meshcomp_iter));
                        if (comp != m_meshcomp.end())
                            m_keywordlist.insert(comp->second.begin(), comp->second.end());
                    }

                    // remove kw already parse in mesh_only
                    keywordcont tmp;
                    for (int meshcomp_iter = (int)MESHCOMP::componentNode; meshcomp_iter <= (int)MESHCOMP::componentElement3D; meshcomp_iter++)
                    {
                        set_difference(m_keywordlist.begin(), m_keywordlist.end(),
                            m_meshcomp.at(static_cast<MESHCOMP>(meshcomp_iter)).begin(), 
                            m_meshcomp.at(static_cast<MESHCOMP>(meshcomp_iter)).end(), inserter(tmp, tmp.begin()));
                        m_keywordlist = tmp;
                        tmp.clear();
                    }
                }
                return m_keywordlist;
            }

            void ParserFormatRule::applyrules(ParserContext* p) const {
                if (m_actionType == ActionType::UPDATE)
                    keywordrules.at(p->getParsedKw())->evalUpdate(p);
                else
                    keywordrules.at(p->getParsedKw())->eval(p);
            }
            
            bool formatInformation::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
                p->m_format.format = element->Attribute("format");
                TermParserFormatRule::parseXml(element, p);
				return false;
			}

            bool comment::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
                vector<string> tmp;
                parseValueCont(tmp, element);
                for (auto it = tmp.begin(); it != tmp.end(); it++)
                    p->m_format.commcar.push_back(*it->c_str());
                return false;
			}


            bool includeFile::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p)
            {
                vector<string> tmp;
                parseValueCont(tmp, element);
                const tinyxml2::XMLAttribute * reg = element->FindAttribute("regExp");
                if (reg && reg->IntValue() > 0)
                {
                    for (string const& s : tmp)
                        p->m_format.inckw.insert(Kw(s, KW_TYPE::REG_EXP));
                    p->addrequiredKeywords(element->Name(), tmp, KW_TYPE::REG_EXP);
                }
                else
                {
                    for (string const& s : tmp)
                        p->m_format.inckw.insert(Kw(s, KW_TYPE::FULL_LINE));
                    p->addrequiredKeywords(element->Name(), tmp, KW_TYPE::FULL_LINE);
                }
                return false;
			}


            bool separator::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
                p->m_format.separator = element->GetText();
                if (p->m_format.separator == "ws") {
                    p->m_format.separator = ' ';
				}
                return false;
			}

            bool meshComponent::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				element=element->FirstChildElement();
                vector<string> setkw;
				while(element!=nullptr) 
                {
					string cur=element->Name();
                    if (cur == "kwRegExp")
                    {
                        for (const tinyxml2::XMLElement* kw = element->FirstChildElement(); kw; kw = kw->NextSiblingElement())
                        {
                            parseValueCont(setkw, kw);
                            p->addrequiredKeywords(kw->Name(), setkw, KW_TYPE::REG_EXP);
                            setkw.clear();
                        }
                    }
                    else if (cur == "elementOrdering")
                    {
                        TermParserFormatRulePtr newterm = facTermParserFormatRule.Create(element->Name());
                        newterm->parseXml(element, p);
					}
                    else if (cur == "kwFullLine")
                    {                
                        for (const tinyxml2::XMLElement* kw = element->FirstChildElement(); kw; kw = kw->NextSiblingElement())
                        {
                            parseValueCont(setkw, kw);
                            p->addrequiredKeywords(kw->Name(), setkw, KW_TYPE::FULL_LINE);
                            setkw.clear();
                        }
                    }
                    else // for backwards compatibility; all other nodes are full-line keywords
                    {
                        parseValueCont(setkw, element);
                        p->addrequiredKeywords(element->Name(), setkw, KW_TYPE::FULL_LINE);
                        setkw.clear();
					}
					element = element->NextSiblingElement();
				}
				return false;
			}

            bool elementOrdering::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				element=element->FirstChildElement();
				while(element!=nullptr) {
					vector<Id> order;
					parseValueCont( order, element);
					string cur =element->Value();
					if (cur=="penta")
						Element3D::setOrderForType( ElementType::ELT_PENTA, order);
					else if (cur=="hexa")
						Element3D::setOrderForType( ElementType::ELT_HEXA, order);
					element = element->NextSiblingElement();
				}
				return false;
			}

            bool rule::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				if (element!=nullptr) {
                    std::string kwName = element->Attribute("keyword");
                    TermParserFormatRule::parseXml(element, p);
                    p->setKeywordrule(kwName, std::make_shared<rule>(*this));
				}
				return false;
			}

			void rule::eval( ParserContext* p) {
				p->contextReset();
				p->vsourceId.insert( p->getParsedKw());
                TermParserFormatRule::eval(p);
			}

            void rule::evalUpdate(ParserContext* p) {
                p->contextReset();
                p->vsourceId.insert(p->getParsedKw());
                TermParserFormatRule::evalUpdate(p);
            }
            
            bool Condition::getConditionValue(ParserContext* p) {
				eval( p);
				return p->resultcondition;
			}

			TermWithCondition::TermWithCondition(): condition(nullptr), body(nullptr)  {
            }

			TermWithCondition::~TermWithCondition() {
				//if (condition!=nullptr) {
				//	delete condition;
				//}
				//if (body!=nullptr) {
				//	delete body;
				//}
			}

            bool TermWithCondition::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				if (element!=nullptr) {
                    tinyxml2::XMLElement const* child = element->FirstChildElement("condition");
                    if (child == nullptr) {
                        std::stringstream str;
                        str << "Definition of parsing rules: missing condition in a conditional action on line " << element->GetLineNum() << "." << endl;
                        throw std::runtime_error(str.str().c_str());
						return false;
					}
					condition = util::make_unique<Condition>();
                    condition->parseXml(child, p);
                    child = child->NextSiblingElement("body");
                    if (child == nullptr) {
                        std::stringstream str;
                        str << "Definition of parsing rules: missing condition body in a conditional action on line " << element->GetLineNum() << "." << endl;
                        throw std::runtime_error(str.str().c_str());
                        return false;
					}
                    body = facTermParserFormatRule.Create(child->Name());
                    body->parseXml(child, p);
				}
				return true;
			}



            notfind::notfind() : Condition(), pos("start") {}

            bool notfind::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_val = element->Attribute("value");
				if (element->Attribute("pos"))
					pos= element->Attribute("pos");
				TermParserFormatRule::parseXml( element, p);
				return true;
			}

			void notfind::eval( ParserContext* p) {
				TermParserFormatRule::eval( p);
				if (pos=="start") {
                    p->strconditiontest.erase(remove_if(p->strconditiontest.begin(), p->strconditiontest.end(), ::isspace), p->strconditiontest.end());
                    if (p->strconditiontest.substr(0, m_val.size()) != m_val)
						p->resultcondition = true;
					else
						p->resultcondition = false;
				}
				else if (pos=="any") {
                    if (p->strconditiontest.find(m_val) == string::npos)
						p->resultcondition = true;
					else
						p->resultcondition = false;
				}
			}

            bool find::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_val = element->Attribute("value");
				if (element->Attribute("pos"))
					pos= element->Attribute("pos");
				TermParserFormatRule::parseXml( element, p);
				return true;
			}

            find::find() : Condition(), pos("start") {}

			void find::eval( ParserContext* p) {
				TermParserFormatRule::eval( p);
                if (pos == "start") {
                    p->strconditiontest.erase(std::remove_if(p->strconditiontest.begin(), p->strconditiontest.end(), ::isspace), p->strconditiontest.end());
                    if (p->strconditiontest.substr(0, m_val.size()) != m_val)
						p->resultcondition = false;
					else
						p->resultcondition = true;
				}
				else if (pos=="any") {
                    if (p->strconditiontest.find(m_val) == string::npos)
						p->resultcondition = false;
					else
						p->resultcondition = true;
				}
			}

            bool equal::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_val = element->Attribute("value");
				TermParserFormatRule::parseXml( element, p);
				return true;
			}

			void equal::eval( ParserContext* p) {
				TermParserFormatRule::eval( p);		
                p->strconditiontest.erase(remove_if(p->strconditiontest.begin(), p->strconditiontest.end(), ::isspace), p->strconditiontest.end());
                if (p->strconditiontest == m_val)
					p->resultcondition = true;
				else
					p->resultcondition = false;
			}


			void doWhile::eval( ParserContext* p) {
				while(condition->getConditionValue( p)) 
					body->eval( p);
			}

            
            void doIf::eval(ParserContext* p) {
				if(condition->getConditionValue( p)) 
					body->eval( p);
			}


            void doWhile::evalUpdate(ParserContext* p) {
                while (condition->getConditionValue(p))
                    body->evalUpdate(p);
            }


            void doIf::evalUpdate(ParserContext* p) {
                if (condition->getConditionValue(p))
                    body->evalUpdate(p);
            }

            bool forEach::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				if (element!=nullptr) {
					m_varfor = element->FirstChildElement("variable")->Attribute("name");
					m_variter = element->FirstChildElement("variableAssign")->Attribute("name");
					element=element->FirstChildElement("body");
					if(element==nullptr) {
						cerr << "[FAILED] Definition of parsing rules: " << endl;
						cerr<< "missing body in a conditional action" << endl;
						return false;
					}
                    body = facTermParserFormatRule.Create(element->Name());
					body->parseXml( element, p);
				}
				return true;
			}


			forEach::~forEach() {
				//if (body!=nullptr) {
				//	delete body;
				//}
			}


			void forEach::eval( ParserContext* p) {
                std::vector<std::string> itervar;
                p->localvar.getLastValue(itervar, m_varfor);
                p->localvar.initVar(m_variter, Type::C);
                for (size_t n = 0; n<itervar.size(); n++) {
                    p->localvar.setVar(m_variter, itervar[n]);
					body->eval( p);
					p->localvar.clearVar( m_variter);
				}
			}				

            void forEach::evalUpdate(ParserContext* p) {
                std::vector<std::string> itervar;
                p->localvar.getLastValue(itervar, m_varfor);
                for (size_t n = 0; n<itervar.size(); n++) {
                    p->localvar.setVar(m_variter, itervar[n]);
                    body->evalUpdate(p);
                    p->localvar.clearVar(m_variter);
                }
            }

            bool nextLine::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				return true;
			}

			void nextLine::eval( ParserContext* p) {
                p->getFileRessourceParse().nextLine();
			}

            void nextLine::evalUpdate(ParserContext* p) {
                evalUpdateSpecial(p);
            }

            void nextLine::evalUpdateSpecial(ParserContext* p) {
                if ((p->updatecontext.updatelinekw.size() == 0) ||
                    (p->updatecontext.updatelinekw.rbegin()->compare(p->getFileRessourceParse().getCurLine())) ||
                    (*(p->updatecontext.isupdateline.rbegin()) == false)) {
                    p->updatecontext.updatelinekw.push_back(p->getFileRessourceParse().getCurLine());
                    p->updatecontext.isupdateline.push_back(false);
                }
                while (p->getFileRessourceParse().nextLineNoSkipComment()) {
                        std::string curline = p->getFileRessourceParse().getCurLine();
                        curline.erase(curline.find_last_not_of(" \r\t") + 1);
                        p->updatecontext.updatelinekw.push_back(curline);
                        p->updatecontext.isupdateline.push_back(false);
                }
            }

            bool curLine::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				return true;
			}

			void curLine::eval( ParserContext* p) {
                p->strconditiontest = p->getFileRessourceParse().getCurLine();
			}

            void parseRule::eval(ParserContext* p) {
                TermParserFormatRule::eval(p);
                if (!parsevar)
                    p->localvar.catchValues(p->getFileRessourceParse().getCurLine(), m_paramparse, m_catchparam);
                else
                    p->localvar.catchValues(p->strconditiontest, m_paramparse, m_catchparam);
                //// check result of parsing in terms of size
                //for (auto it = m_paramparse.begin(); it != m_paramparse.end(); ++it) {
                //    //get size of variable
                //    if (p->localvar.getVar(it->first)->getLast()->size() != m_paramparse[it->first].m_repeat) {
                //        std::stringstream str;
                //        str << "Variable: " << it->first << ": exptected size (" << m_paramparse[it->first].m_repeat << ") is not respected - current size(" << p->localvar.getVar(it->first)->getLast()->size() << ")" << std::endl;
                //        throw std::runtime_error(str.str().c_str());
                //        return;
                //    }
                //}
            }

            void parseRule::evalUpdate(ParserContext* p) {
                evalUpdateSpecial(p);
            }

            void parseRule::evalUpdateSpecial(ParserContext* p) {
                if ((p->updatecontext.updatelinekw.size() > 0) && (*(p->updatecontext.updatelinekw.rbegin()) != p->getFileRessourceParse().getCurLine())) {
                    p->updatecontext.updatelinekw.push_back(p->getFileRessourceParse().getCurLine());
                    p->updatecontext.isupdateline.push_back(true);
                    p->updatecontext.count++;
                }
                else if (p->updatecontext.updatelinekw.size() == 0) {
                    p->updatecontext.updatelinekw.push_back(p->getFileRessourceParse().getCurLine());
                    p->updatecontext.isupdateline.push_back(true);
                    p->updatecontext.count++;
                }
                for (auto it = m_paramparse.begin(); it != m_paramparse.end(); ++it)
                    p->updatecontext.map_count_var[p->updatecontext.count].insert(it->first);
                eval(p);
            }

            bool parseRule::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
                TermParserFormatRule::parseXml(element, p);
                if (element->FirstChildElement("variable"))
                    parsevar = true;
                else parsevar = false;
                element = element->FirstChildElement("parse");
                while (element != nullptr) {
                    std::string varnam = element->FirstChildElement("variableAssign")->Attribute("name");
                    Type type = getTypefromStr(element->Attribute("format"));
                    int length = element->IntAttribute("length");
                    int offset = element->IntAttribute("offset");
                    unsigned int repeat = element->UnsignedAttribute("repeat");
                    if (repeat < 1)
                    {
                        std::stringstream str;
                        str << "'Repeat' attribute of 'parse' element must be at least 1, got " << repeat 
                            << " on line " << element->GetLineNum() << "." << std::endl;
                        throw std::runtime_error(str.str().c_str());
                        return false;
                    }
                    std::string sep("none");
                    if (element->Attribute("separator")) {
                        sep = element->Attribute("separator");
                        if (sep == "ws")
                            sep = ' ';
                    }
                    if ((sep == "none") && (p->m_format.separator != "none"))
                        sep = p->m_format.separator;
                    else if (sep != "none" && length != 0)
                    {
                        std::stringstream str;
                        str << "A non-zero 'length' AND a separator was defined for 'parse' element on line " << element->GetLineNum() <<
                            ". 'Length' must be set to 0 (or not specified at all) if separator is used." << std::endl;
                        throw std::runtime_error(str.str().c_str());
                        return false;
                    }

                    unsigned int offrepeat;
                    if (element->Attribute("offsetrepeat"))
                        offrepeat = element->UnsignedAttribute("offsetrepeat");
                    else {
                        if (length != 0)
                            offrepeat = 0;
                        else
                            offrepeat = 1;
                    }
                    std::string defaultstr("");
                    bool hasDefault = false;
                    if (element->Attribute("default")) {
                        defaultstr = element->Attribute("default");
                        hasDefault = true;
                    }
                    m_paramparse[varnam] = ParseParam(type, offset,
                        length, sep, repeat, offrepeat, defaultstr, hasDefault);
                    //
                    element = element->NextSiblingElement("parse");
                }
                // now compute catchparameter for this line parse rules
                generateCatchParam();
                return true;
            }

            void parseRule::generateCatchParam() {
                for (auto it = m_paramparse.begin(); it != m_paramparse.end(); ++it) {
                    unsigned int n = 0;
                    std::stringstream os;
                    while (n < it->second.m_repeat) {
                        int start;
                        if (it->second.m_length != 0)
                            start = it->second.m_offset + n * (it->second.m_length + it->second.m_offrepeat);
                        else
                            start = it->second.m_offset + n * it->second.m_offrepeat;
                        catchParam cur = catchParam(it->first, start, it->second.m_length);
                        catchParamCont::iterator itt = std::lower_bound(m_catchparam.begin(), m_catchparam.end(),
                            start, OrdercatchParamCont());
                        if (itt != m_catchparam.end() && itt->start == start)
                        {
                            std::stringstream str;
                            str << "Conflict between parse parameter for variables " << itt->varname << " and " << it->first << std::endl;
                            throw std::runtime_error(str.str().c_str());
                            return;
                        }
                        else m_catchparam.insert(itt, cur);
                        n++;
                    }
                }
            }

            Type parseRule::getTypefromStr(const string& typestr) {
                if (typestr == "UIpos")
                    return Type::UIpos;
                if (typestr == "UI")
                    return Type::UI;
                if (typestr == "F")
                    return Type::F;
                if (typestr == "C")
                    return Type::C;
                return Type::UNDEF;
            }


            bool variableAssign::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				return true;
			}

			void variableAssign::eval( ParserContext* p) {}


            bool variable::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_var = element->Attribute("name");
				return true;
			}

			void variable::eval( ParserContext* p) {
                p->localvar.getLastValue(p->strconditiontest, m_var);
			}


            bool generateId::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_start=element->FirstChildElement("start")->FirstChildElement("variable")->Attribute("name");
				m_end=element->FirstChildElement("end")->FirstChildElement("variable")->Attribute("name");
				m_var=element->FirstChildElement("variableAssign")->Attribute("name");
				return true;
			}

			void generateId::eval( ParserContext* p) {
                VId genid;
                Id idstart, idend;
                p->localvar.getLastValue(idstart, m_start);
                p->localvar.getLastValue(idend, m_end);
                for (Id n = idstart; n <= idend; n++) {
                    genid.push_back(n);
                }
                p->localvar.setVar(m_var, Type::UIpos, genid);
			}

            bool append::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_value=element->Attribute("value");
				m_var=element->FirstChildElement("variable")->Attribute("name");
				if (element->FirstChildElement("variableAssign"))
					m_varassign=element->FirstChildElement("variableAssign")->Attribute("name");
				else
					m_varassign=m_var;
				return true;
			}

			void append::eval( ParserContext* p) {
                string tmp;
                p->localvar.getLastValue(tmp, m_var);
                tmp += m_value;
                p->localvar.initVar(m_varassign, Type::C);
                p->localvar.setVar(m_varassign, tmp);
			}

            bool clearVar::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				element=element->FirstChildElement("variable");
				while(element!=nullptr) {
					string curvar = element->Attribute("name");
					m_var.push_back( curvar);
					element=element->NextSiblingElement("variable");
				}
				return true;
			}

			void clearVar::eval( ParserContext* p) {
				for(vector<string>::const_iterator it=m_var.begin(); it!=m_var.end(); ++it) {
                    p->localvar.clearVar(*it);
				}
			}


			//bool assign::parseXml( tinyxml2::XMLElement* element, ParserFormatRule* p) {
			//	if (element->FirstChildElement("variable"))
			//		m_var=element->FirstChildElement("variable")->Attribute("name");
			//	else
			//		m_var="none";
			//	if (element->FirstChildElement("value"))
			//		m_value=element->FirstChildElement("value")->GetText();
			//	else
			//		m_value="none";
			//	m_varassign=element->FirstChildElement("variableAssign")->Attribute("name");
			//	return true;
			//}

			//void assign::eval( ParserContext* p) {
			//	if (m_var!="none")
   //                 p->localvar.setVar(m_varassign, Type::C, p->localvar.getLastValue(m_var));
			//	else if (m_value!="none") {
   //                 p->localvar.setVar(m_varassign, Type::C, m_value);
			//	}
			//}


			void object::eval( ParserContext* p) {
				p->curobjtype = m_type;
				TermParserFormatRule::eval( p);
			}

            void object::evalUpdate(ParserContext* p) {
                p->curobjtype = m_type;
                TermParserFormatRule::evalUpdate(p);
            }
            
            bool object::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				TermParserFormatRule::parseXml( element, p); 
				return true;
			}

            bool objectGroup::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				string contentype = element->Attribute("type");
				if (contentype=="Node")
					m_type = "GroupNode";
				else if (contentype=="Element3D")
					m_type = "GroupElements3D";
				else if (contentype=="Element2D")
					m_type = "GroupElements2D";
				else if (contentype=="Element1D")
					m_type = "GroupElements1D";
				else if (contentype=="Group")
					m_type = "GroupGroups";
                else 
                    m_type = "ObjectGroup";
                object::parseXml(element, p);
				return true;
			}


            bool Method::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				if (element->FirstChildElement("variable")) {
					m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
					m_inputType.push_back( methodContext::MethodInputType::Parsed);
				}
				else {
					m_input.push_back( element->GetText());
					m_inputType.push_back( methodContext::MethodInputType::User);
				}
				return true;
			}



            void Method::eval(ParserContext* p) {
                p->methodcontext.clear();
                p->methodcontext.method_name = m_methodname;
                p->methodcontext.method_input = m_input;
                p->methodcontext.input_type = m_inputType;
                p->applyObjectMethod();
                p->vsourceId.clear();
            }

            void Method::evalUpdate(ParserContext* p) {
                evalUpdateSpecial(p);
            }

            void Method::evalUpdateSpecial(ParserContext* p) {
                p->methodcontext.clear();
                if ((p->curobjtype == "Node") || (p->curobjtype == "Parameter")) {
                    p->methodcontext.clear();
                    p->methodcontext.method_name = m_methodname;
                    p->methodcontext.method_input = m_input;
                    p->methodcontext.input_type = m_inputType;
                    p->applyUpdateObjectMethod();
                    p->methodcontext.clear();
                }
                p->vsourceId.clear();
            }
            
            void setFile::eval(ParserContext* p) {
                std::string newinclude;
                p->localvar.getLastValue(newinclude, m_input[0]);
                p->addIncFiles(newinclude);
    //            vector<VarBase*>const& loc = p->localvar.getVar(m_input[0]);
				//for (vector<VarBase*>::const_iterator itv=loc.begin(); itv!=loc.end(); ++itv)
    //                p->addIncFiles((*itv)->get<string>(0));
			}

            void setFile::evalUpdate(ParserContext* p) {
                eval(p);
                evalUpdateSpecial(p);
            }

            void setFile::evalUpdateSpecial(ParserContext* p) {
                for (auto it = p->updatecontext.updatelinekw.begin(); it != p->updatecontext.updatelinekw.end(); ++it)
                    p->getFileRessourceUpdate().write(*it);
                p->updatecontext.reset();
            }
            
            void setId::eval(ParserContext* p) {
                TermParserFormatRule::eval(p);
                Method::eval(p);
            }
            
            bool setId::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
				m_inputType.push_back( methodContext::MethodInputType::Parsed);
                if (element->FirstChildElement("sourceId")) {
                    TermParserFormatRulePtr newterm = facTermParserFormatRule.Create("sourceId");
                    newterm->parseXml(element->FirstChildElement("sourceId"), p);
                    addTerm(newterm);
                }
				return true;
			}

				
            bool addInGroup::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
				m_inputType.push_back( methodContext::MethodInputType::Parsed);
				if (element->FirstChildElement("sourceId")) {
                    TermParserFormatRulePtr newterm = facTermParserFormatRule.Create("sourceId");
					newterm->parseXml( element->FirstChildElement("sourceId"), p);
					addTerm( newterm);
				}
				return true;
			}

			void addInGroup::eval( ParserContext* p) {
				TermParserFormatRule::eval( p);
				Method::eval(p);
			}

            bool sourceId::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				parseValueCont( m_source, element);
				return true;
			}

			void sourceId::eval( ParserContext* p) {
				p->vsourceId = m_source;
			}

            bool setOrigin::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
				m_inputType.push_back( methodContext::MethodInputType::Parsed);
				m_input.push_back( element->Attribute("type"));
				m_inputType.push_back( methodContext::MethodInputType::User);
				return true;
			}

            bool setFirstAxis::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
				m_inputType.push_back( methodContext::MethodInputType::Parsed);
				m_input.push_back( element->Attribute("type"));
				m_inputType.push_back( methodContext::MethodInputType::User);
				return true;
			}

            bool setPlane::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->FirstChildElement("variable")->Attribute("name"));
				m_inputType.push_back( methodContext::MethodInputType::Parsed);
				m_input.push_back( element->Attribute("type"));
				m_inputType.push_back( methodContext::MethodInputType::User);
				return true;
			}

            bool setDof::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				m_input.push_back( element->GetText());
				m_inputType.push_back( methodContext::MethodInputType::User);
				return true;
			}
			
            bool setValue::parseXml(const tinyxml2::XMLElement* element, ParserFormatRule* p) {
				m_methodname = element->Name();
				if (element->FirstChildElement("value")->FirstChildElement("variable")) {
					m_input.push_back( element->FirstChildElement("value")->FirstChildElement("variable")->Attribute("name"));
					m_inputType.push_back( methodContext::MethodInputType::Parsed);
				}
				else {
					m_input.push_back( element->FirstChildElement("value")->GetText());
					m_inputType.push_back( methodContext::MethodInputType::User);
				}			
				m_input.push_back( element->FirstChildElement("type")->GetText());
				m_inputType.push_back( methodContext::MethodInputType::User);
				//
				return true;
			}



		}
	}
}
