/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "EntityJoint.h"

#include "xmlTools.h"
#include "FEModel.h"

#include <sstream>

namespace piper {
namespace hbm {

const std::map<std::string, EntityJoint::ConstrainedDofType> BaseJointMetadata::strToConstrainedDofType =
{ { "hard", EntityJoint::ConstrainedDofType::HARD },
  { "soft", EntityJoint::ConstrainedDofType::SOFT } };

const std::map<EntityJoint::ConstrainedDofType, std::string> BaseJointMetadata::constrainedDofTypeToStr =
{ { EntityJoint::ConstrainedDofType::HARD, "hard" },
  { EntityJoint::ConstrainedDofType::SOFT, "soft" } };

BaseJointMetadata::BaseJointMetadata()
    : BaseMetadata()
    , m_constrainedDofType(ConstrainedDofType::HARD)
{ }

BaseJointMetadata::BaseJointMetadata(const std::string &name)
    : BaseMetadata(name)
    , m_constrainedDofType(ConstrainedDofType::HARD)
{ }

void BaseJointMetadata::parseXml(tinyxml2::XMLElement* element)
{
    BaseMetadata::parseXml(element);

    if (element->FirstChildElement("Dof")) {
        tinyxml2::XMLElement* xmlDof = element->FirstChildElement("Dof");
        if (xmlDof->Attribute("constrainedDofType"))
            m_constrainedDofType=strToConstrainedDofType.at(xmlDof->Attribute("constrainedDofType"));
        std::vector<bool> vdof;
        parseValueCont(vdof, xmlDof);
        int n=0;
        for (bool const& b : vdof) {
            m_dof[n] = b;
            n++;
        }
    }
}

void BaseJointMetadata::serializeXml(tinyxml2::XMLElement* element) const
{
    BaseMetadata::serializeXml(element);

    // dof
    tinyxml2::XMLElement* xmlDof;
    xmlDof = element->GetDocument()->NewElement("Dof");
    xmlDof->SetAttribute("constrainedDofType", constrainedDofTypeToStr.at(m_constrainedDofType).c_str());
    serializeValueCont(xmlDof, m_dof);
    element->InsertEndChild(xmlDof);
}

void BaseJointMetadata::serializePmr(tinyxml2::XMLElement* element) const {
    //Dof
    tinyxml2::XMLElement* xmlDof = element->GetDocument()->NewElement("setDof");
    serializeValueCont(xmlDof, m_dof);
    element->InsertEndChild(xmlDof);
    //Dof type 
    tinyxml2::XMLElement* xmlDofType = element->GetDocument()->NewElement("setConstrainedDofType");
    xmlDofType->SetAttribute("value", constrainedDofTypeToStr.at(m_constrainedDofType).c_str());
    element->InsertEndChild(xmlDofType);

}


EntityJoint::EntityJoint()
    : BaseJointMetadata()
{ }

EntityJoint::EntityJoint(std::string const& name)
    : BaseJointMetadata(name)
{}

EntityJoint::EntityJoint(tinyxml2::XMLElement* element)
{
    parseXml(element);
}

void EntityJoint::setEntity1( const std::string& name) {
    m_entity1Name=name;
}

void EntityJoint::setEntity2( const std::string& name) {
    m_entity2Name=name;
}

void EntityJoint::parseXml(tinyxml2::XMLElement* element)
{
    BaseJointMetadata::parseXml(element);

    tinyxml2::XMLElement* jointEntity = element->FirstChildElement("jointEntity");
    m_entity1Name = jointEntity->FirstChildElement("name")->GetText();
    jointEntity->FirstChildElement("FrameId")->QueryUnsignedText(&m_entity1FrameId);

    jointEntity = jointEntity->NextSiblingElement("jointEntity");
    m_entity2Name = jointEntity->FirstChildElement("name")->GetText();
    jointEntity->FirstChildElement("FrameId")->QueryUnsignedText(&m_entity2FrameId);

}

void EntityJoint::serializeXml(tinyxml2::XMLElement* element) const
{
    // name
    BaseJointMetadata::serializeXml(element);

    {
        // jointEntity 1
        tinyxml2::XMLElement* xmlJointEntity = element->GetDocument()->NewElement("jointEntity");
        tinyxml2::XMLElement* xmlJointEntityName = element->GetDocument()->NewElement("name");
        xmlJointEntityName->SetText(m_entity1Name.c_str());
        xmlJointEntity->InsertEndChild(xmlJointEntityName);
        tinyxml2::XMLElement* xmlJointEntityFrameId = element->GetDocument()->NewElement("FrameId");
        xmlJointEntityFrameId->SetText(m_entity1FrameId);
        xmlJointEntity->InsertEndChild(xmlJointEntityFrameId);
        element->InsertEndChild(xmlJointEntity);
    }

    {
        // jointEntity 2
        tinyxml2::XMLElement* xmlJointEntity = element->GetDocument()->NewElement("jointEntity");
        tinyxml2::XMLElement* xmlJointEntityName = element->GetDocument()->NewElement("name");
        xmlJointEntityName->SetText(m_entity2Name.c_str());
        xmlJointEntity->InsertEndChild(xmlJointEntityName);
        tinyxml2::XMLElement* xmlJointEntityFrameId = element->GetDocument()->NewElement("FrameId");
        xmlJointEntityFrameId->SetText(m_entity2FrameId);
        xmlJointEntity->InsertEndChild(xmlJointEntityFrameId);
        element->InsertEndChild(xmlJointEntity);
    }

}


void EntityJoint::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
    BaseMetadata::serializePmr(element);

    // entity master
    std::map<std::string, std::vector<IdKey>> fe_ids;
    tinyxml2::XMLElement* xmlent1 = element->GetDocument()->NewElement("entity_master");
    xmlent1->SetAttribute("name", m_entity1Name.c_str());
    tinyxml2::XMLElement* xmlframe1 = element->GetDocument()->NewElement("setFrame");
    std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<FEFrame>();
    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(m_entity1FrameId);
    if (it != mpIds.end() && it->second.first != "CreatedFrame")
        fe_ids[it->second.first].push_back(it->second.second);
    else {
        xmlframe1->SetAttribute("type", "global");
        Id idorg = fem.get<FEFrame>(m_entity1FrameId)->getOriginId();
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsNode = fem.computePiperIdToIdKeyMap<Node>();
        fe_ids[mpIdsNode.at(idorg).first].push_back(mpIdsNode.at(idorg).second);
    }
    serializePmr_FEids(xmlframe1, fe_ids);
    fe_ids.clear();
    xmlent1->InsertEndChild(xmlframe1);
    element->InsertEndChild(xmlent1);

    // entity slave
    tinyxml2::XMLElement* xmlent2 = element->GetDocument()->NewElement("entity_slave");
    xmlent2->SetAttribute("name", m_entity2Name.c_str());
    tinyxml2::XMLElement* xmlframe2 = element->GetDocument()->NewElement("setFrame");
    it = mpIds.find(m_entity2FrameId);
    if (it != mpIds.end() && it->second.first != "CreatedFrame")
        fe_ids[it->second.first].push_back(it->second.second);
    else {
        xmlframe2->SetAttribute("type", "global");
        Id idorg = fem.get<FEFrame>(m_entity2FrameId)->getOriginId();
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsNode = fem.computePiperIdToIdKeyMap<Node>();
        fe_ids[mpIdsNode.at(idorg).first].push_back(mpIdsNode.at(idorg).second);
    }
    serializePmr_FEids(xmlframe2, fe_ids);
    fe_ids.clear();
    xmlent2->InsertEndChild(xmlframe2);
    element->InsertEndChild(xmlent2);

    BaseJointMetadata::serializePmr(element);
}


AnatomicalJoint::AnatomicalJoint()
    : BaseJointMetadata()
{ }

AnatomicalJoint::AnatomicalJoint(std::string const& name)
    : BaseJointMetadata(name)
{ }

AnatomicalJoint::AnatomicalJoint(tinyxml2::XMLElement* element)
    : BaseJointMetadata()
{
    parseXml(element);
}


}
}
