/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "MeshDefState.h"


#include "FEModel.h"
#include "Node.h"
#include "vtkPointData.h"
#include "vtkIntArray.h"

#include <iostream>
#include <vector>

using namespace std;
namespace piper { 
	namespace hbm {


		VId MeshDefNID::getListNid(const FEModel* mesh) {
            return mesh->getNodes().listId();
		}

		//void MeshDefNID::printNodes(const FEModel* mesh) const {
		//	cout << "Nodes Defined by NID" << endl;
		//	mesh->m_nodes.print();

		//}

        void MeshDefNID::setNode(FEModel* mesh, vector<shared_ptr<Node>>& vnode) {
            for (vector<shared_ptr<Node>>::iterator it = vnode.begin(); it != vnode.end(); ++it) {
                mesh->getNodes().setEntity((*it));
			}
            mesh->m_map_restorenid.clear(); // invalidate the restorenid map to mark that it has to be rebuilt next time it is needed
		}

		void MeshDefNID::delNode(FEModel* mesh, const vector<Id>& vnid) {
            mesh->getNodes().delEntities(vnid);
            mesh->m_map_restorenid.clear(); // invalidate the restorenid map to mark that it has to be rebuilt next time it is needed
		}

		void MeshDefNID::updateMeshDef(FEModel* mesh) {

		}

        void MeshDefNID::setActive(FEModel* mesh, std::string const& stringID) {
            mesh->m_nodes.setActive(stringID);
        }


		VId MeshDefINDEX::getListNid(const FEModel* mesh) {
			VId vnid;
			for(auto it = mesh->m_map_restorenid.begin(); it != mesh->m_map_restorenid.end(); ++it) {
				vnid.push_back(it->second);
			}
			return vnid;
		}

		//void MeshDefINDEX::printNodes(const FEModel* mesh) const {
		//	cout << "Nodes Defined by INDEX" << endl;
		//	std::ostream_iterator<FixedWidthVal<coord , 8> > osi_nid(std::cout, " ");
		//	for (Nodes::const_iterator it = mesh->m_nodes.begin(); it != mesh->m_nodes.end(); ++it) {
		//		osi_nid =  mesh->m_map_restorenid.at( mesh->m_nodes.curId( it));
		//		mesh->m_nodes.curDef( it)->print();
		//	}
		//}

        void MeshDefINDEX::setNode(FEModel* mesh, vector<shared_ptr<Node>>& vnode) {
			vector< Id> vnid;
            int count = mesh->getNodes().size()>0 ? 1 : 0;
            Id maxnid = mesh->getNodes().getMaxId();
            Id oldId=0;
            for (vector<shared_ptr<Node>>::iterator it = vnode.begin(); it != vnode.end(); ++it) {
                oldId=(*it)->getId();
				(*it)->setId( maxnid+count);
				//Node n( maxnid+count);
				//n.setCoord( it->get());
                mesh->getNodes().setEntity((*it));
				mesh->m_map_restorenid.insert( pair< Id, Id>( maxnid+count, oldId ));
				count++;
			}
		}

		void MeshDefINDEX::delNode(FEModel* mesh, const vector<Id>& vnid) {
            mesh->getNodes().delEntities(vnid);

            // create a map of IDs to delete
            unsigned int size = *(std::max_element(vnid.begin(), vnid.end())) + 1;
            bool *toDelete = new bool[size];
            std::fill(toDelete, toDelete + size, false);
            for (Id cur : vnid)
                toDelete[cur] = true;
            for (auto it = mesh->m_map_restorenid.begin(); it != mesh->m_map_restorenid.end();) 
            {
                if (it->first < size && toDelete[it->first]) // in which way is the map mapped?? is it->first the piperID or the FE node ID? this assumes first is piper ID
				    it = mesh->m_map_restorenid.erase(it);
			}
            delete[] toDelete;
			updateMeshDef( mesh);
		}

		void MeshDefINDEX::updateMeshDef(FEModel* mesh) {
			mesh->setMeshDef_NID();
			mesh->setMeshDef_INDEX();
		}

		void MeshDefINDEX::setActive(FEModel* mesh, std::string const& stringID) {
			std::shared_ptr<Nodes> oldactive = mesh->m_nodes.getActive();
			mesh->m_nodes.setActive(stringID);
            std::shared_ptr<Nodes> activeNodes = mesh->m_nodes.getActive();
			for (auto it = mesh->m_map_restorenid.begin(); it != mesh->m_map_restorenid.end(); it++) {
				activeNodes->getEntity(it->second)->setId(it->first);
				oldactive->getEntity(it->first)->setId(it->second);
			}
		}
	}
}
