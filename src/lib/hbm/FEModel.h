/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESH__H
#define MESH__H


#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <iostream>
#include <array>
#include <set>

#include <units/units.h>

#include "MeshDefState.h"
#include "Element.h"
#include "Group.h"
#include "FEFrame.h"
#include "FEModelParameter.h"
#include "IdRegistrator.h"
#include "target.h"
#include "FEModelVTK.h"
#include "History.h"


namespace piper {
	namespace hbm {
		namespace parser {
			class ParserModelFile;
			class Associationparameter;
		}

		typedef std::shared_ptr<Element3D> Element3DPtr;
		typedef std::shared_ptr<Element2D> Element2DPtr;
		typedef std::shared_ptr<Element1D> Element1DPtr;
        typedef std::shared_ptr<FEFrame> FramePtr;
		typedef std::shared_ptr<Group> GroupPtr;
		typedef std::shared_ptr<GroupNode> GroupNodePtr;
		typedef std::shared_ptr<GroupElements1D> GroupElements1DPtr;
		typedef std::shared_ptr<GroupElements2D> GroupElements2DPtr;
		typedef std::shared_ptr<GroupElements3D> GroupElements3DPtr;
		typedef std::shared_ptr<GroupGroups> GroupGroupsPtr;
		typedef std::shared_ptr<FEModelParameter> FEModelParameterPtr;

		/// Option for parsing FE HBM model files
		enum PARSER_OPTION {
			NODES_ONLY, ///< parse only nodes 
			MESH_ONLY, ///< parse only nodes, elements and parameters
			WHOLE ///< parse all components 
		};

		/**
		* @brief The FEmodel class stores a Finite Element
		* It is composed of nodes, elements and groups.
		*
		* @author Erwan Jolivet @date 2015
		* \ingroup libhbm
		*/
		class HBM_EXPORT FEModel : public HistoryBase {

			friend class MeshDefNID;
			friend class MeshDefINDEX;
			friend class parser::Associationparameter; //\todo erwan: bad design....
			friend class HbmParameter;
		public:
			// ------------- Constructor --------------
			FEModel() : m_meshdefnid(MeshDefNID()), m_meshdefindex(MeshDefINDEX()), m_meshdefstate(&m_meshdefnid),
				m_isFEmodel(false), vtk_representation()
			{
				for (int i = 0; i < 3; i++)
				{
					t_scale[i] = 1;
					t_translation[i] = 0;
					t_rotation[i] = 0;
				}
			}
			// ------------- Copy-constructor --------------

			// ------------- Assignment operator --------------
			//	FEModel& operator=(const FEModel& other);

			// ------------- Destructor --------------
			~FEModel();

			/// <summary>
			/// Resets this FEModel as a copy of another specified model. No pointers are shared - all data is hard-copied by value (including the VTK representation).
			/// WARNING - this method has not been thoroughly tested and probably does not work entirely correctly
			/// </summary>
			/// <param name="source">The source model which this model should become a copy of.</param>
			void DeepCopy(FEModel &source);

			/** @brief get a pointer to a model component of type T identified its piper id
			*
			*/
			template< typename T>
			std::shared_ptr<T> get(const Id& id) { assert(sizeof(T) == 0); }
			/** @brief get a pointer to a model component of type T identified by a string key, and an IdKey
			*
			*/
			template< typename T>
			std::shared_ptr<T> get(const std::string& key, const IdKey& id);
			/** @brief get a const pointer to a model component of type T identified its id
			*
			*/
			template< typename T>
			const std::shared_ptr<T> get(const Id& id) const { assert(sizeof(T) == 0); }
			/** @brief get a const pointer to a model component of type T identified by a string key and an IdKey
			*
			*/
			template< typename T>
			const std::shared_ptr<T> get(const std::string& key, const IdKey& id) const;
			/** @brief defines a FE Id information for a component of type T identifed by its id
			*
			*  \param[out] key FE code keyword which defines this \a idpiper
			*  \param[out] id FE code id which defines this \a idpiper
			*/
			template< typename T>
			void getInfoId(const Id& idpiper, std::string& key, IdKey& id) const { assert(sizeof(T) == 0); }
			template< typename T>
			void getInfoId(const VId& idpiper, std::vector<std::string>& key, std::vector<IdKey>& id) const { assert(sizeof(T) == 0); }
			template< typename T>
			std::map<hbm::Id, std::pair<std::string, IdKey>> const& computePiperIdToIdKeyMap() const { assert(sizeof(T) == 0); }
			/** @brief defines a string with all id information for a model component of type T
			*
			*  \return string with id information
			*/
			template< typename T>
			std::string infoId(const Id& id) const { assert(sizeof(T) == 0); }
			/** @brief set a model component of type T without registration
			*
			*/
			template< typename T>
			void set(std::shared_ptr<T> component) { assert(sizeof(T) == 0); }
			/** @brief set a model component of type T with registration using a key
			*
			*/
			template< typename T>
			void set(std::shared_ptr<T> component, const std::string& key) { assert(sizeof(T) == 0); }
			/** @brief set a model component of type T with registration using a key and and IdKey
			*
			*/
			template< typename T>
			void set(std::shared_ptr<T> component, const std::string& key, const IdKey& id) { assert(sizeof(T) == 0); }
			//
			/** @brief delete a model component of type T and unregistres it if it is necessary
			*
			*/
			template< typename T>
			void deleteComponent(const Id& id) { assert(sizeof(T) == 0); }

			/** @brief delete all model components of type T specified in the provided vector and unregistres them if it is necessary
			*
			*/
			template< typename T>
			void deleteComponents(const std::vector<Id>& id) { assert(sizeof(T) == 0); }

			/** @brief delete all model components of type T and unregistres them if it is necessary
			*
			*/
			template< typename T>
			void deleteAllComponents() { assert(sizeof(T) == 0); }

			/** @brief returning the current meshdef type used by FEModel
			*
			*/
			MeshDefType getMeshDefType() const;
			/** @brief : returns true is FEmodel is generated after parseing of FE files
			*
			*/
			bool isFEmodel() const { return m_isFEmodel; }
			// ------------- Utilities: Nodes -------------
			std::vector< Id> getListNid() const;

			/// <summary>
			/// Deletes all the specified nodes, then resets the VTK representation.
			/// </summary>
			/// <param name="vnid">The vector of node IDs to delete.</param>
			void delNode(const std::vector<Id>& vnid);

			/// <summary>
			/// Deletes the node. 
			/// The VTK representation has to be reset after this -> use delNode(const std::vector<Id>& vnid) if you need to delete more nodes at once to reset it only once.
			/// </summary>
			/// <param name="nid">The node ID.</param>
			void delNode(const Id nid);

			// ------------- Utilities: Node -------------
			/** @brief : returns the number of nodes in the model
			*
			*/
			std::size_t getNbNode() const { return m_nodes.getActive()->size(); }
			/** @brief : returns const reference to nodes container of the current model history
			*
			*/
			const Nodes& getNodes() const { return *m_nodes.getActive(); }
			/** @brief : returns reference to nodes container of the current model history
			*
			*/
			Nodes& getNodes() { return *m_nodes.getActive(); }
			/** @brief : returns const reference to nodes container of the \a historyName model history
			*
			*/
			const Nodes& getNodes(std::string const& historyName) const;
			/** @brief : returns reference to nodes container of the \a historyName model history
			*
			*/
			Nodes& getNodes(std::string const& historyName);

			/** @brief : returns true if a node with \a id exist in the model
			*
			*/
			bool hasNode(const Id& id) const;
			/** @brief : returns const to node with \a id of the current model history
			*
			*/
			Node const& getNode(const Id& id) const;
			/** @brief : returns to node with \a id of the current model history
			*
			*/
			Node& getNode(const Id& id);

			/// <summary>
			/// Updates the nodes of the vtk representation of this FEModel to match the nodes of the FEModel.
			/// Call this when some action that does not use the vtk representation changed the nodes of this FEModel
			/// to synchronize both representations.
			/// To update this FEModel BY the vtk representation, i.e. the other way around, use FEModelVTK::updateFEmodelNodes method.
			/// </summary>
			void updateVTKRepresentation();
			/** @brief : returns the max node id (piper or vendor) currently used in FEModel.
			*
			* the max node id is relative to the id definition mode set by setMeshDef_TYPE
			*/
			Id getMaxNodeId();
			// ------------- Utilities: Elements3D -------------
			std::size_t getNbElement3D() const { return m_elem3D.size(); }
			const Elements3D& getElements3D() const { return m_elem3D; }
			Element3D const& getElement3D(const Id& id) const;
			// ------------- Utilities: Elements2D -------------
			std::size_t getNbElement2D() const { return m_elem2D.size(); }
			const Elements2D& getElements2D() const { return m_elem2D; }
			Element2D const& getElement2D(const Id& id) const;
			// ------------- Utilities: Elements1D -------------
			std::size_t getNbElement1D() const { return m_elem1D.size(); }
			const Elements1D& getElements1D() const { return m_elem1D; }
			Element1D const& getElement1D(const Id& id) const;
			// ------------- Utilities: Groups -------------
			void cleanEmptyGroups();
			// ------------- Utilities: GroupElements3D -------------
			std::size_t getNbGroupElements3D() const { return m_gelem3d.size(); }
			const ContGroupElements3D& getGroupElements3D() const { return m_gelem3d; }
			GroupElements3D const& getGroupElements3D(const Id& id) const;
			// ------------- Utilities: GroupElements2D -------------
			std::size_t getNbGroupElements2D() const { return m_gelem2d.size(); }
			const ContGroupElements2D& getGroupElements2D() const { return m_gelem2d; }
			GroupElements2D const& getGroupElements2D(const Id& id) const;
			GroupElements2D& getGroupElements2D(const Id& id);
			// ------------- Utilities: GroupElements1D -------------
			std::size_t getNbGroupElements1D() const { return m_gelem1d.size(); }
			const ContGroupElements1D& getGroupElements1D() const { return m_gelem1d; }
			GroupElements1D const& getGroupElements1D(const Id& id) const;
			// ------------- Utilities: GroupGroups -------------
			std::size_t getNbGroupGroups() const { return m_ggroup.size(); }
			const ContGroupGroups& getGroupGroups() const { return m_ggroup; }
			GroupGroups const& getGroupGroups(const Id& id) const;
			// ------------- Utilities: GroupNode -------------
			std::size_t getNbGroupNode() const { return m_gnodes.size(); }
			const ContGroupNodes& getGroupNodes() const { return m_gnodes; }
			GroupNode const& getGroupNode(const Id& id) const;
			// ------------- Utilities: Frames -------------
			std::size_t getNbFrames() const { return m_frames.size(); }
			const Frames& getFrames() const { return m_frames; }
            FEFrame const& getFrame(const Id& id) const;
			void normalizedFrame(const units::Length &unit);
			void restoreFrame();
			// ------------- Utilities: Parameters -------------
            std::size_t getNbParameters() const { return m_parameters.getActive()->size(); }
            const FEModelParameters& getParameters() const { return *m_parameters.getActive(); }
			FEModelParameter const& getFEModelParameter(const Id& id) const;
            void setParametersValue(const Id& id, std::string const& name, std::vector<double>const& value);
            void appendParametersValue(const Id& id, std::string const& name, std::vector<double>const& value);
            void appendParametersValue(const Id& id, std::string const& name, std::vector<unsigned int>const& value);
			/*!
			* \brief Get orientation of the frame identified by its id
			* \param id, id of the frame
			* \return a quaternion
			*/
			Eigen::Quaternion<double> getFrameOrientation(const Id& id) const;
			/*!
			*  \brief Get origin of the frame
			* \param id, id of the frame
			* \return coordinates of the origin of the frame identified by its id
			*/
			const Coord& getFrameOrigin(const Id& id) const;
			// ------------- Utilities: Writer/Reader -------------
			/*!
			*  \brief write mesh component of model (nodes and elements) in a VTK fomat
			* \param filemesh: full path of the VTK file
			*/
			void writeModel(const std::string& filemesh);
			/*!
			*  \brief write non mesh component of model (frame, group,..) that are not compatible with VTK fomat in metadata file
			* \param filemetadata: full path of the metadata file
			*/
			void writeModelMetadata(const std::string& filemetadata);
			/*!
			*  \brief read mesh component of model (nodes and elements) in a VTK fomat
			* \param file: full path of the VTK file
			*/
			void readModel(const std::string& file);
			/*!
			*  \brief read non mesh component of model (frame, group,..) that are not compatible with VTK fomat in metadata file
			* \param filemetadata: full path of the metadata file
			*/
			void readModelMetadata(const std::string& filemetadata);
			// ------------- Interface -------------
			virtual void convMeshDef(std::unordered_map<Id, Id>& map_id);
			/// clear the model: nodes, elements,...
			void clear();

			/*!
			*  \brief parse hbm files using information in metdata
			*
			*  \param formatrulesFile:
			*  \param modelrulesFile:
			*
			*/
            void reloadFEmesh(Metadata& metadata);

			/*!
			*  \brief parse hbm files using information in metdata
			*
			*  \param metadata:
			*
			*/
			void readGeomMesh(Metadata& metadata);

			/*!
			*  \brief parse hbm files using information in metdata
			*
			*  \param metadata:
			*
			*/
            void parseMesh(Metadata& metadata, PARSER_OPTION option = PARSER_OPTION::WHOLE);

			///*!
			//*  \brief parse hbm files using information in metdata
			//*
			//*  \param formatrulesFile:
			//*  \param modelrulesFile:
			//*  \param sourceModelFile:
			//*  \param option: PARSER_OPION::NODES: parse only nodes, PARSER_OPION::NODES: parse only nodes and elements, PARSER_OPION::WHOLE: parse while model
			//*
			//*/
			//void parseMesh(std::string const& formatrulesFile, std::string const& modelrulesFile, PARSER_OPTION option = PARSER_OPTION::WHOLE);
			/*!
			*  \brief parse hbm files
			*
			*  \param p:  model file parser
            *  \param sourceFile: main model file
            *  \param includeFiles: additional model files not specified in the source file by include rule
			*  \param option: PARSER_OPION::NODES: parse only nodes, PARSER_OPION::NODES: parse only nodes and elements, PARSER_OPION::WHOLE: parse while model
            *  \param optional sourceModelFile
            *
			*/
            void parse(parser::ParserModelFile* p, std::string const& sourceFile, std::vector<std::string> const&includeFiles, PARSER_OPTION option = PARSER_OPTION::WHOLE);

			/*!
			*  \brief update hbm files using information in metdata
			*
			*  \param metadata of hbm
			*  \param targetdirectory:  patho to target directory to write updated model files
			*  \param msg:  string to add at the top of the file as comment
			*
			*/
			void update(const Metadata& metadata, const std::string& targetdirectory, std::vector<std::string> const& msg);

			/*!
			*  \brief update hbm files using information in metdata
			*
			*  \param metadata of hbm
			*  \param targetdirectory:  patho to target directory to write updated model files
			*
			*/
			void update(const Metadata& metadata, const std::string& targetdirectory);

			/*!
			*  \brief fill medata using information in parser rule (waiting medata module)
			*
			*  \param p:  model file parser
			*  \param metadata:  metadata of hbm
			*
			*/
			void fillMetadata(parser::ParserModelFile* p, Metadata& metadata);

			/*!
			*  \brief update hbm files (only nodes coordinates are updated)
			*
			*  \param p:  model file parser
			*  \param filename:  string of hbm model file location
            *  \param includeFiles: additional model files not specified in the source file by include rule
			*  \param targetdirectory:  patho to target directory to write updated model files
			*  \param msg:  string to add at the top of the file as comment
			*
			*/
            void update(parser::ParserModelFile* p, const std::string& filename, std::vector<std::string> const& includeFiles, const std::string& targetdirectory, std::vector<std::string> const& msg);
			/*!
			*  \brief update hbm files (only nodes coordinates are updated)
			*
			*  \param p:  model file parser
			*  \param filename:  string of hbm model file location
            *  \param includeFiles: additional model files not specified in the source file by include rule
			*  \param targetdirectory:  patho to target directory to write updated model files
			*
			*/
            void update(parser::ParserModelFile* p, const std::string& filename, std::vector<std::string> const& includeFiles, const std::string& targetdirectory);

			bool setMeshDef_NID();
			bool setMeshDef_INDEX();

			/// \brief Returns the vtk representation of the geometry of this FEModel
			/// \return The vtk representation
			FEModelVTK *getFEModelVTK() { return &vtk_representation; }


			/// \brief set active the model history identified by a string identifier.
			/// 
			/// \param stringID: The string that identifies the node history to set active
			virtual void setActive(std::string const& stringID);

			/// \brief set active a new node history with string identifier.
			/// it deletes all histories after the current one and put a new one at the top of history.
			/// It copies the active model history in the new one.
			/// 
			/// \param stringID: The string that identifies the new node history
			virtual void addNewHistory(std::string const& stringID);

			/// \brief erase history 
			/// 
			/// \param vnames: vector of history names to be erased
			virtual void deleteHistory(std::vector<std::string> const& vnames);

			/// \brief rename active history
			/// 
			/// \param stringID: new string to identify active history 
			virtual void renameActiveHistory(std::string const& stringID);

			/// \brief Returns vector of node id that are not used in element definition
			/// \return vector of node id
			VId freenodes() const;

			/// <summary>
			/// Sets the scale transformation for this model. This does not change the coordinate values, only stores the transformation.
			/// </summary>
			/// <param name="x">Scaling factor for x-axis.</param>
			/// <param name="y">Scaling factor for y-axis.</param>
			/// <param name="z">Scaling factor for z-axis.</param>
			void setScale(double x, double y, double z);

			/// <summary>
			/// Sets the translation for this model. This does not change the coordinate values, only stores the transformation.
			/// </summary>
			/// <param name="x">Translation distance along x-axis.</param>
			/// <param name="y">Translation distance along y-axis.</param>
			/// <param name="z">Translation distance along z-axis.</param>
			void setTranslation(double x, double y, double z);

			/// <summary>
			/// Sets the rotation for this model. This does not change the coordinate values, only stores the transformation.
			/// </summary>
			/// <param name="x">Rotation around x-axis, in degrees.</param>
			/// <param name="y">Rotation around y-axis, in degrees.</param>
			/// <param name="z">Rotation around z-axis, in degrees.</param>
			void setRotation(double x, double y, double z);

			/// <summary>
			/// The scale transformation of this model.
			/// </summary>
			/// <returns>3 scale factors - for X, Y and Z axis on indices 0, 1, 2 respectively of the returned array.</returns>
			const Coord &getScale() const;

			/// <summary>
			/// The translation transformation of this model.
			/// </summary>
			/// <returns>3 translation distances - for X, Y and Z axis on indices 0, 1, 2 respectively of the returned array.</returns>
			const Coord &getTranslation() const;

			/// <summary>
			/// The rotation transformation of this model.
			/// </summary>
			/// <returns>3 rotation angles - around X, Y and Z axis on indices 0, 1, 2 respectively of the returned array. In radians.</returns>
			const Coord &getRotation() const;

			GroupNodePtr get_GroupNodePtr();

			GroupElements1DPtr get_GroupElements1DPtr();

			GroupElements2DPtr get_GroupElements2DPtr();

			GroupElements3DPtr get_GroupElements3DPtr();

		private:

			void setMeshDef_type(const MeshDefType meshdeftype);
			template<class contT, class T>
			static void setComponent(contT* container, std::shared_ptr<T> component);

			// ------------- private member --------------
			MeshDefNID m_meshdefnid;
			MeshDefINDEX m_meshdefindex;
			MeshDefState* m_meshdefstate;

			bool m_isFEmodel; // flag: true = femodel generated from FE file parsing

            // maps for switching between 0-based indexing of nodes and FE Node IDs
			std::unordered_map< Id, Id> m_map_restorenid; // key == Index, val == node ID
            std::unordered_map< Id, Id> m_map_restoreindex; // val == node ID, key == Index

			/// container for group of node components
			ContGroupNodes m_gnodes;
			/// container for group of 3D element components
			ContGroupElements3D m_gelem3d;
			/// container for group of 2D element components
			ContGroupElements2D m_gelem2d;
			/// container for group of 1D element components
			ContGroupElements1D m_gelem1d;
			/// container for group of group components
			ContGroupGroups		m_ggroup;

			/// container for node components in a vector for node hitory
			History<Nodes> m_nodes;
			/// container for 3D element components
			Elements3D m_elem3D;
			/// container for 2D element components
			Elements2D m_elem2D;
			/// container for 1D element components
			Elements1D m_elem1D;
		        /// container for Frame components
			Frames m_frames;
			/// container for Model parameter components
            History<FEModelParameters> m_parameters;


			// transformations for this model. Index 0 = X-axis, 1 = Y, 2 = Z
			Coord t_scale;
			Coord t_translation;
			Coord t_rotation; // in degrees

			/// pointer to ModelWriter and ModelReader
			//ModelWriter* modelwriter;
			//ModelReader* modelreader;

			IdRegistrator<Node, Node > m_registorNode;
			IdRegistrator<Element1D, Element1D> m_registorElement1D;
			IdRegistrator<Element2D, Element2D> m_registorElement2D;
			IdRegistrator<Element3D, Element3D> m_registorElement3D;
			IdRegistrator<Group, GroupNode> m_registorGroupNode;
			IdRegistrator<Group, GroupElements1D> m_registorGroupE1D;
			IdRegistrator<Group, GroupElements2D> m_registorGroupE2D;
			IdRegistrator<Group, GroupElements3D> m_registorGroupE3D;
			IdRegistrator<Group, GroupGroups> m_registorGroupGroup;
            IdRegistrator<FEFrame, FEFrame> m_registorFrame;
			IdRegistrator<FEModelParameter, FEModelParameter> m_registorModelparameter;
			FEModelVTK vtk_representation; //the geometry of this FEModel repres
			template<typename T, typename TT>
			static void parseIdXml(tinyxml2::XMLElement* element, IdRegistrator<T, TT>& registrator, FEModel* femodel);


			void readOBJ(std::string const& filename);
		};

		template< > HBM_EXPORT void FEModel::set<Node>(NodePtr node);
		template< > HBM_EXPORT void FEModel::set<Element3D>(Element3DPtr element);
		template< >	HBM_EXPORT void FEModel::set<Element2D>(Element2DPtr element);
		template< >	HBM_EXPORT void FEModel::set<Element1D>(Element1DPtr element);
        template< >	HBM_EXPORT void FEModel::set<FEFrame>(FramePtr element);
		template< >	HBM_EXPORT void FEModel::set<GroupNode>(GroupNodePtr gnode);
		template< >	HBM_EXPORT void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem);
		template< >	HBM_EXPORT void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem);
		template< >	HBM_EXPORT void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem);
		template< >	HBM_EXPORT void FEModel::set<GroupGroups>(GroupGroupsPtr gelem);
		template< >	HBM_EXPORT void FEModel::set<FEModelParameter>(FEModelParameterPtr param);

		template< > HBM_EXPORT void FEModel::set<Node>(NodePtr node, const std::string& key, const IdKey& id);
		template< > HBM_EXPORT void FEModel::set<Element3D>(Element3DPtr element, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<Element2D>(Element2DPtr element, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<Element1D>(Element1DPtr element, const std::string& key, const IdKey& id);
        template< >	HBM_EXPORT void FEModel::set<FEFrame>(FramePtr frame, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<GroupNode>(GroupNodePtr gnode, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<GroupGroups>(GroupGroupsPtr gelem, const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT void FEModel::set<FEModelParameter>(FEModelParameterPtr param, const std::string& key, const IdKey& id);

		template< > HBM_EXPORT void FEModel::set<Node>(NodePtr node, const std::string& key);
		template< > HBM_EXPORT void FEModel::set<Element3D>(Element3DPtr element, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<Element2D>(Element2DPtr element, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<Element1D>(Element1DPtr element, const std::string& key);
        template< >	HBM_EXPORT void FEModel::set<FEFrame>(FramePtr frame, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<GroupNode>(GroupNodePtr gnode, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<GroupGroups>(GroupGroupsPtr gelem, const std::string& key);
		template< >	HBM_EXPORT void FEModel::set<FEModelParameter>(FEModelParameterPtr param, const std::string& key);


		template< >	HBM_EXPORT NodePtr FEModel::get<Node>(const Id& id);
		template< >	HBM_EXPORT Element1DPtr FEModel::get<Element1D>(const Id& id);
		template< >	HBM_EXPORT Element2DPtr FEModel::get<Element2D>(const Id& id);
		template< >	HBM_EXPORT Element3DPtr FEModel::get<Element3D>(const Id& id);
        template< >	HBM_EXPORT FramePtr FEModel::get<FEFrame>(const Id& id);
		template< >	HBM_EXPORT GroupNodePtr FEModel::get<GroupNode>(const Id& id);
		template< >	HBM_EXPORT GroupElements1DPtr FEModel::get<GroupElements1D>(const Id& id);
		template< >	HBM_EXPORT GroupElements2DPtr FEModel::get<GroupElements2D>(const Id& id);
		template< >	HBM_EXPORT GroupElements3DPtr FEModel::get<GroupElements3D>(const Id& id);
		template< >	HBM_EXPORT GroupGroupsPtr FEModel::get<GroupGroups>(const Id& id);
		template< >	HBM_EXPORT FEModelParameterPtr FEModel::get<FEModelParameter>(const Id& id);


		template< >	HBM_EXPORT const NodePtr FEModel::get<Node>(const Id& id) const;
		template< >	HBM_EXPORT const Element1DPtr FEModel::get<Element1D>(const Id& id) const;
		template< >	HBM_EXPORT const Element2DPtr FEModel::get<Element2D>(const Id& id) const;
		template< >	HBM_EXPORT const Element3DPtr FEModel::get<Element3D>(const Id& id) const;
        template< >	HBM_EXPORT const FramePtr FEModel::get<FEFrame>(const Id& id) const;
		template< >	HBM_EXPORT const GroupNodePtr FEModel::get<GroupNode>(const Id& id) const;
		template< >	HBM_EXPORT const GroupElements1DPtr FEModel::get<GroupElements1D>(const Id& id) const;
		template< >	HBM_EXPORT const GroupElements2DPtr FEModel::get<GroupElements2D>(const Id& id) const;
		template< >	HBM_EXPORT const GroupElements3DPtr FEModel::get<GroupElements3D>(const Id& id) const;
		template< >	HBM_EXPORT const GroupGroupsPtr FEModel::get<GroupGroups>(const Id& id) const;
		template< >	HBM_EXPORT const FEModelParameterPtr FEModel::get<FEModelParameter>(const Id& id) const;

		template< >	HBM_EXPORT NodePtr FEModel::get<Node>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT Element1DPtr FEModel::get<Element1D>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT Element2DPtr FEModel::get<Element2D>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT Element3DPtr FEModel::get<Element3D>(const std::string& key, const IdKey& id);
        template< >	HBM_EXPORT FramePtr FEModel::get<FEFrame>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupPtr FEModel::get<Group>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupNodePtr FEModel::get<GroupNode>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupElements1DPtr FEModel::get<GroupElements1D>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupElements2DPtr FEModel::get<GroupElements2D>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupElements3DPtr FEModel::get<GroupElements3D>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT GroupGroupsPtr FEModel::get<GroupGroups>(const std::string& key, const IdKey& id);
		template< >	HBM_EXPORT FEModelParameterPtr FEModel::get<FEModelParameter>(const std::string& key, const IdKey& id);

		template< >	HBM_EXPORT const NodePtr FEModel::get<Node>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const Element1DPtr FEModel::get<Element1D>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const Element2DPtr FEModel::get<Element2D>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const Element3DPtr FEModel::get<Element3D>(const std::string& key, const IdKey& id) const;
        template< >	HBM_EXPORT const FramePtr FEModel::get<FEFrame>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const GroupNodePtr FEModel::get<GroupNode>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const GroupElements1DPtr FEModel::get<GroupElements1D>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const GroupElements2DPtr FEModel::get<GroupElements2D>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const GroupElements3DPtr FEModel::get<GroupElements3D>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const GroupGroupsPtr FEModel::get<GroupGroups>(const std::string& key, const IdKey& id) const;
		template< >	HBM_EXPORT const FEModelParameterPtr FEModel::get<FEModelParameter>(const std::string& key, const IdKey& id) const;

		template< >	HBM_EXPORT std::string FEModel::infoId<Node>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<Element1D>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<Element2D>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<Element3D>(const Id& id) const;
        template< >	HBM_EXPORT std::string FEModel::infoId<FEFrame>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<GroupNode>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<GroupElements1D>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<GroupElements2D>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<GroupElements3D>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<GroupGroups>(const Id& id) const;
		template< >	HBM_EXPORT std::string FEModel::infoId<FEModelParameter>(const Id& id) const;


		template< >	HBM_EXPORT void FEModel::getInfoId<Node>(const VId& idpiper, std::vector<std::string>& key, std::vector<IdKey>& id) const;

		template< >	HBM_EXPORT void FEModel::getInfoId<Node>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<Element1D>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<Element2D>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<Element3D>(const Id& idpiper, std::string& key, IdKey& id) const;
        template< >	HBM_EXPORT void FEModel::getInfoId<FEFrame>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<GroupNode>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<GroupElements1D>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<GroupElements2D>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<GroupElements3D>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<GroupGroups>(const Id& idpiper, std::string& key, IdKey& id) const;
		template< >	HBM_EXPORT void FEModel::getInfoId<FEModelParameter>(const Id& idpiper, std::string& key, IdKey& id) const;

		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Node>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element1D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element2D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element3D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupNode>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements1D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements2D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements3D>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupGroups>() const;
		template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<FEModelParameter>() const;
        template< >	HBM_EXPORT std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<FEFrame>() const;

		template< >	HBM_EXPORT void FEModel::deleteComponent<Node>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<Element1D>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<Element2D>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<Element3D>(const Id& id);
        template< >	HBM_EXPORT void FEModel::deleteComponent<FEFrame>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<GroupNode>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<GroupElements1D>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<GroupElements2D>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<GroupElements3D>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<GroupGroups>(const Id& id);
		template< >	HBM_EXPORT void FEModel::deleteComponent<FEModelParameter>(const Id& id);


		template< >	HBM_EXPORT void FEModel::deleteComponents<Node>(const std::vector<Id>& id);

		template< >	HBM_EXPORT void FEModel::deleteAllComponents<Element3D>();
		template< >	HBM_EXPORT void FEModel::deleteAllComponents<Element2D>();
		template< >	HBM_EXPORT void FEModel::deleteAllComponents<GroupElements3D>();

	}
}

#include "FEModel.inl"

#endif
