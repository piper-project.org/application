/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParserContext.h"
#include "Metadata.h"

#include <stdlib.h>

using namespace std;
namespace piper {
	namespace hbm {
		namespace parser {
            
            template< typename T>
            map<string, std::function<std::shared_ptr<T>()>> Factory<T>::m_map = map<string, std::function<std::shared_ptr<T>()>>(); // intended as a map of constructors of smart pointers

            template< >
            map<string, std::function<std::shared_ptr<TermDescription>()>> Factory<TermDescription>::m_map = 
                map<string, std::function<std::shared_ptr<TermDescription>()>>();

            template< >
            map<string, std::function<std::shared_ptr<TermParserFormatRule>()>> Factory<TermParserFormatRule>::m_map =
                map<string, std::function<std::shared_ptr<TermParserFormatRule>()>>();



            ParseParam::ParseParam()
                : m_type(Type::UNDEF)
                , m_offset(0)
                , m_length(0)
                , m_sep("none")
                , m_repeat(0)
                , m_offrepeat(0)
                , m_default("")
                , hasDefault(true) {}

            ParseParam::ParseParam(Type type,
                int offset,
                int length,
                std::string const& sep,
                unsigned int repeat,
                unsigned int offrepeat,
                std::string const& defaultstr,
                bool hasDefault)
                : m_type(type)
                , m_offset(offset)
                , m_length(length)
                , m_sep(sep)
                , m_repeat(repeat)
                , m_offrepeat(offrepeat)
                , m_default(defaultstr)
                , hasDefault(hasDefault) {}

            var::var(Type const& type, unsigned int const& value) {
                myinterface = new VarBase<unsigned int>();
                myinterface->setValue<unsigned int>(value);
            }

            var::var(Type const& type, double const& value) {
                myinterface = new VarBase<double>();
                myinterface->setValue<double>(value);
            }

            var::var(Type const& type, std::string const& value) {
                myinterface = new VarBase<std::string>();
                myinterface->setValue<std::string>(value);
            }

            var::var(Type const& type, std::vector<unsigned int> const& value) {
                myinterface = new VarBase<unsigned int>();
                myinterface->setValue<unsigned int>(value);
            }

            var::var(Type const& type, std::vector<double> const& value) {
                myinterface = new VarBase<double>();
                myinterface->setValue<double>(value);
            }

            var::var(Type const& type, std::vector<std::string> const& value) {
                myinterface = new VarBase<std::string>();
                myinterface->setValue<std::string>(value);
            }

            var::var(Type const& type) {
                switch (type) {
                case Type::UIpos: 
                case Type::UI:{
                    myinterface = new VarBase<unsigned int>();
                    break;
                }
                case Type::F: {
                    myinterface = new VarBase<double>();
                    break;
                }
                case Type::C: {
                    myinterface = new VarBase<std::string>();
                    break;
                }
                }
            }

            var::~var() {
                delete myinterface;
            }

            //var::var(const var &other) {
            //    myinterface = other.myinterface->clone();
            //}

            //var& var::operator= (const var &other) {
            //    if (&other == this)
            //        return *this;
            //    myinterface = other.myinterface->clone();
            //    return *this;
            //}

            size_t var::size() const {
                return myinterface->size();
            }



            void var::convertIdKey(std::vector<IdKey>& idv) const {
                myinterface->convertIdKey(idv);
            }

            void var::reserve(size_t const& n) {
                myinterface->reserve(n);
            }

            vectorVar::vectorVar(ParseParam const& parseparam) : m_param(parseparam), m_type(m_param.m_type) {
                var* newvar = new var(parseparam.m_type);
                newvar->reserve(parseparam.m_repeat);
                m_var.push_back(newvar);
            }

            vectorVar::vectorVar(Type& type) : m_type(type){
                var* newvar = new var(type);
                m_var.push_back(newvar);
            }

            vectorVar::vectorVar() {};

            vectorVar::~vectorVar() {
                for (auto cur : m_var)
                    delete cur;
                m_var.clear();
            }

            void vectorVar::initVar(ParseParam const& parseparam) {
                if (m_var.size() == 0) {
                    m_param = parseparam;
                    m_type = m_param.m_type;
                }
                var* newvar  = new var(parseparam.m_type);
                newvar->reserve(parseparam.m_repeat);
                m_var.push_back(newvar);
            }

            void vectorVar::initVar(Type& type) {
                if (m_var.size() == 0) {
                    m_type = type;
                }
                var* newvar=new var(type);
                m_var.push_back(newvar);
            }

            void vectorVar::pushValue(std::string const& varname, std::string const& str) {
                switch (m_param.m_type) {
                case Type::UIpos: {
                    unsigned int val = stoul(str);
                    if (val > 0)
                        m_var.back()->pushValue(val);
                    break;
                }
                case Type::UI: {
                    m_var.back()->pushValue((unsigned int)stoul(str));
                    break;
                }
                case Type::F: {
                    double value;
                    std::stringstream os;
                    os << str;
                    os >> value;
                    m_var.back()->pushValue(value);
                    break;
                }
                case Type::C: {
                    std::string strstr(str);
                    m_var.back()->pushValue(strstr);
                    break;
                }
                }

            }
            void vectorVar::clearVar() {
                for (auto cur : m_var)
                    delete cur;
                m_var.clear();
            }

            size_t vectorVar::size() const {
                return m_var.size();
            }

            var const* vectorVar::getLast() const {
                return m_var.back();
            }

            var* vectorVar::get(size_t const& n) const {
                return m_var[n];
            }

            ParseParam const& vectorVar::getParseParam() const {
                return m_param;
            }

            vectorVar::iterator vectorVar::begin() { return m_var.begin(); }
            vectorVar::const_iterator vectorVar::begin() const { return m_var.begin(); }
            vectorVar::iterator vectorVar::end() { return m_var.end(); }
            vectorVar::const_iterator vectorVar::end() const { return m_var.end(); }


            localVar::localVar() {}


            localVar::~localVar() {
                clear();
            }


            void localVar::clearVar(const string& varname) {
                varCont::iterator it = map_var.find(varname);
                if (it != map_var.end()) {
                    it->second->clearVar();
                }
            }

            void localVar::clear() {
                for (auto cur : map_var)
                    delete cur.second;
                map_var.clear();
            }


            void localVar::initVar(const std::string& varname, ParseParam const& parseparam) {
                if (map_var.find(varname) == map_var.end())
                    map_var[varname] = new vectorVar(parseparam);
                else
                    map_var[varname]->initVar(parseparam);
            }

            void localVar::initVar(const std::string& varname, Type type) {
                if (map_var.find(varname) == map_var.end())
                    map_var[varname] = new vectorVar(type);
                else
                    map_var[varname]->initVar(type);
            }

            void localVar::pushValue(std::string const& varname, std::string const& str) {
                map_var[varname]->pushValue(varname, str);
            }


            vectorVar const* localVar::getVar(std::string const& varname) const {
                auto f = map_var.find(varname);
                if (f == map_var.end())
                    return nullptr;
                return f->second;
            }


            unsigned int stringToUnsignedInt(const char *str) {
                int c = 0, sign = 0, x = 0;
                const char *p = str;

                for (c = *(p++); (c < 48 || c > 57); c = *(p++));
                //{
                    //int a = 1;
                //    if (c == 45) {
                //        sign = 1;
                //        c = *(p++);
                //        break;
                    //}
                //}; // eat whitespaces and check sign
                for (; c > 47 && c < 58; c = *(p++)) x = (x << 1) + (x << 3) + c - 48;

                //return sign ? -x : x;
                return x;
            }

            void localVar::catchValues(std::string& ss, paramParseCont const& paramparse, catchParamCont const& catchparam) {
                // declare new variable in localvar
                for (auto it = paramparse.begin(); it != paramparse.end(); ++it)
                    this->initVar(it->first,it->second);
                bool checkdefault;
                for (auto it = catchparam.begin(); it != catchparam.end(); ++it) {
                    checkdefault = false;
                    int start = it->start;
                    int length = it->length;
                    if (length > 0) { // fixed format
                        int lengthchar = std::min((int)ss.length() - start, length);
                        if (lengthchar > 0) {
                            // copy lengthchar characters after "start" to the target string
                            std::string param = ss.substr(start, lengthchar);
                            this->pushValue(it->varname, param);
                        }
                        else
                            checkdefault = true;
                    }
                    else { //separator Format
                        std::vector<std::string> tokens;
                        std::string sepcur = paramparse.at(it->varname).m_sep;
                        if (sepcur == " ") {
                            replace(ss.begin(), ss.end(), '\t', ' ');
                            boost::split(tokens, ss, boost::is_any_of(sepcur), boost::token_compress_on);
                        }
                        else
                            boost::split(tokens, ss, boost::is_any_of(sepcur));
                        //catch  just after the offset sperator
                        if (start < tokens.size())
                            if (tokens[start].size()>0)
                                this->pushValue(it->varname, tokens[start]);
                            else
                                checkdefault = true;
                        else
                            checkdefault = true;
                    }
                    if (checkdefault && paramparse.at(it->varname).hasDefault)
                        // check if a default value is defined
                        this->pushValue(it->varname, paramparse.at(it->varname).m_default);
                }
            }

            void updateValueInt(string& str, stringstream& ss, unsigned int& coord, const ParseParam& pp) {
                std::vector<unsigned int> vcoord;
                vcoord.push_back(coord);
                if (pp.m_length>0) // fixed format
                    updateValueinLineFixedFormat(str, ss, vcoord, pp);
                else
                    updateValueinLineSeparatorFormat(str, ss, vcoord, pp);
            }

            void updateValueInt(string& str, stringstream& ss, vector<unsigned int>& coord, const ParseParam& pp) {
                if (pp.m_length>0) // fixed format
                    updateValueinLineFixedFormat(str, ss, coord, pp);
                else
                    updateValueinLineSeparatorFormat(str, ss, coord, pp);
            }

            void updateValueinLineFixedFormat(string& str, stringstream& ss, vector<unsigned int>& coord, const ParseParam& pp) {
                if (pp.m_offset>0) {
                    ss << str.substr(0, pp.m_offset);
                    str.erase(0, pp.m_offset);
                }
                unsigned int n = 0;
                // e.g. if we have 6-char format -> cannot write 1000000, only lower numbers (999999 is fine). For bigger numbers, always use scientific notation
                auto writableFixNumberLimit = std::pow(10, pp.m_length);
                while (n<pp.m_repeat) {
                    // if the number is too large that it would not fit in the fixed format
                    if (coord[n] >= writableFixNumberLimit)
                    {
                        // scientific precision is always one digit, decimal point, n digits set by setprecision(n),
                        // e, minus or plus sign of the exponent and up to 3 exponent digits -> set precision to max allowed length:
                        // we have to reserve 1 character for decimal point, 1 for the one digit before decimal point,
                        // 1 for e-sign, 1 for the exponent sign and 3 for exponent values -> 7 characters that will always be used
                        ss << setw(pp.m_length) << std::scientific << std::setprecision(pp.m_length - 7) << coord[n];
                    }
                    else // otherwise use fixed notation - this way we can have higher precision since we need not reserve 5 character for exponent
                    {
                        // TODO - counting the number of digits can be more efficent for unsigned int than this: https://stackoverflow.com/a/25934909
                        // but need to make sure the code will be platform independent (i.e. do not just copy paste the code from the link)
                        ss << setw(pp.m_length) << std::fixed << std::setprecision(pp.m_length - log10(floor(coord[n]))) << coord[n];
                    }
                    str.erase(0, pp.m_length);
                    //ss << str.substr(0, pp.m_length);
                    if (pp.m_offrepeat > 0) {
                        ss << str.substr(0, pp.m_length);
                        str.erase(0, pp.m_offrepeat);
                    }
                    n++;
                }
                if (str.size() > 0)
                    ss << str;
            }

            void updateValueinLineSeparatorFormat(string& str, stringstream& ss, vector<unsigned int>& coord, const ParseParam& pp) {
                //find separator in line
                std::vector<std::string> tokens;
                if (pp.m_sep == " ")
                    boost::split(tokens, str, boost::is_any_of(pp.m_sep), boost::token_compress_on);
                else
                    boost::split(tokens, str, boost::is_any_of(pp.m_sep));
                // offset
                for (unsigned int n = 0; n<pp.m_offset; n++) {
                    ss << tokens[0] << pp.m_sep;
                    tokens.erase(tokens.begin());
                }
                unsigned int n = 0;
                ss << std::setprecision(numeric_limits<double>::digits); // set virtually perfect precision
                while (n<pp.m_repeat) {
                    ss << coord[n] << pp.m_sep;
                    tokens.erase(tokens.begin());
                    if (pp.m_offrepeat > 1) {
                        for (unsigned int n = 0; n<pp.m_offrepeat; n++) {
                            ss << tokens[0] << pp.m_sep;
                            tokens.erase(tokens.begin());
                        }
                    }
                    n++;
                }
                // end of line
                while (tokens.size()>0) {
                    if (tokens.size() == 1)
                        ss << tokens[0];
                    else
                        ss << tokens[0] << pp.m_sep;
                    tokens.erase(tokens.begin());
                }
            }


            void updateValueCoord(string& str, stringstream& ss, double& coord, const ParseParam& pp) {
                std::vector<double> vcoord;
                vcoord.push_back(coord);
                if (pp.m_length>0) // fixed format
                    updateValueinLineFixedFormat(str, ss, vcoord, pp);
                else
                    updateValueinLineSeparatorFormat(str, ss, vcoord, pp);
            }

			void updateValueCoord( string& str, stringstream& ss, vector<double>& coord, const ParseParam& pp) {
				if (pp.m_length>0) // fixed format
					updateValueinLineFixedFormat( str, ss, coord, pp);
				else
					updateValueinLineSeparatorFormat( str, ss, coord, pp);
			}

            // this will pring the coordinates to as many digits as are allowed, but even if it is not necessarry, 
            // i.e. it will fill the "empty" digits with trailing zeros. we could get rid of them by printing into a 
            // temporary buffer and remove them from there to make the output prettier, but the cost would be additional processing time 
			void updateValueinLineFixedFormat( string& str, stringstream& ss, vector<double>& coord, const ParseParam& pp) {
				if (pp.m_offset>0) {
					ss << str.substr(0, pp.m_offset);
					str.erase(0, pp.m_offset);
				}
				unsigned int n=0;
                // e.g. if we have 6-char format, reserve 1 for minus, 1 for decimal point-> max 4 characters-> 10^4 = 10000 
                // -> -10000. is seven characters -> cannot write 10000, only lower numbers (9999 is fine). For bigger numbers, always use scientific notation
                auto writableFixNumberLimit = std::pow(10, (pp.m_length - 2));
				while (n<pp.m_repeat) {
                    int reserveMinusSign = (coord[n] < 0 ? 1 : 0);
                    double absCoord = std::fabs(coord[n]);
                    // if writing e-xxx takes less characters then there are insignificant digits, use the scientific notation
                    // or if the number is too large that it would not fit in the fixed no matter what
                    // one minor drawback is that exact 0 will also be written with scientific notation, but that is OK
                    if ((absCoord < 0.00001) || (absCoord >= writableFixNumberLimit))
                    {
                        // scientific precision is always one digit, decimal point, n digits set by setprecision(n),
                        // e, minus or plus sign of the exponent and up to 3 exponent digits -> set precision to max allowed length:
                        // we have to reserve 1 character for decimal point, 1 for the one digit before decimal point,
                        // 1 for e-sign, 1 for the exponent sign and 3 for exponent values -> 7 characters that will always be used
                        ss << setw(pp.m_length) << std::scientific << std::setprecision(pp.m_length - 7 - reserveMinusSign) << (coord[n] + 0.0); // +0.0 ensures that negative 0 is written as positive 0 - it needs to be since negative 0 will not reserveMinusSign...
                    }
                    else // otherwise use fixed notation - this way we can have higher precision since we need not reserve 5 character for exponent
                    {
                        // count the number of digits before decimal point - perhaps can be done in a more efficient way,
                        // but keep it in the range of double, i.e. DO NOT just cast to 32-bit ints and use some code that works for that, find something that works for larger range
                        double reserveLog = log10(floor(absCoord));
                        // log is negative for values below 1 -> we reserve 2 chars (0.)
                        // otherwise for 0 - 9, floor(log) = 0->we reserve 2 chars(0.), for 10 - 99.99, floor(log) = 1->we reserve 3 chars(xx.) etc.
                        ss << setw(pp.m_length) << std::fixed << std::setprecision(pp.m_length - (reserveLog < 0 ? 2 : floor(reserveLog) + 2) - reserveMinusSign) << (coord[n] + 0.0); // +0.0 ensures that negative 0 is written as positive 0 - it needs to be since negative 0 will not reserveMinusSign...
                    }
					str.erase(0, pp.m_length);
					//ss << str.substr(0, pp.m_length);
					if (pp.m_offrepeat > 0) {
						ss << str.substr(0, pp.m_length);
						str.erase(0, pp.m_offrepeat);
					}
					n++;
				}
				if (str.size() > 0)
					ss << str;
			}

			void updateValueinLineSeparatorFormat( string& str, stringstream& ss, vector<double>& coord, const ParseParam& pp) {
				//find separator in line
				std::vector<std::string> tokens;
				if (pp.m_sep==" ")
					boost::split(tokens, str, boost::is_any_of(pp.m_sep), boost::token_compress_on);
				else
					boost::split(tokens, str, boost::is_any_of(pp.m_sep));
				// offset
				for (unsigned int n=0; n<pp.m_offset; n++) {
					ss << tokens[0] << pp.m_sep;
					tokens.erase( tokens.begin());
				}
				unsigned int n=0;
                ss << std::setprecision(numeric_limits<double>::digits); // set virtually perfect precision
				while (n<pp.m_repeat) {
                    ss << coord[n] << pp.m_sep;
					tokens.erase( tokens.begin());
					if (pp.m_offrepeat > 1) {
						for (unsigned int n=0; n<pp.m_offrepeat; n++) {
							ss << tokens[0] << pp.m_sep;
							tokens.erase( tokens.begin());
						}
					}
					n++;
				}
				// end of line
				while (tokens.size()>0) {
					if (tokens.size()==1)
						ss << tokens[0];
					else
						ss << tokens[0] << pp.m_sep;
					tokens.erase( tokens.begin());
				}
			}


            void keywordParseXml(tinyxml2::XMLElement* element, std::vector<std::string>& vkey, std::vector<std::vector<IdKey>>& vidkey) {
				while (element!=nullptr) {
                    vkey.push_back(element->Attribute("kw"));
					if (element->FirstChildElement("id")) {
						VId vid;
						parseValueCont( vid, element->FirstChildElement("id"));
                        std::vector<IdKey> tmp;
						for (VId::const_iterator it=vid.begin(); it!=vid.end(); ++it) {
                            tmp.push_back( IdKey(*it));
						}
                        vidkey.push_back(tmp);
					}
					else if (element->FirstChildElement("name")) {
						vector<string> vname;
						parseValueCont( vname, element->FirstChildElement("name"));
                        std::vector<IdKey> tmp;
                        for (vector<string>::const_iterator it = vname.begin(); it != vname.end(); ++it) {
                            tmp.push_back(IdKey(*it));
                        }
                        vidkey.push_back(tmp);
					}
					else {
                        std::vector<IdKey> tmp;
                        vidkey.push_back(tmp);
                    }
					element = element->NextSiblingElement("keyword");
				}
			}

            void VCoordParseXml(tinyxml2::XMLElement* element, std::vector<IdKey>& vid, VCoord& vcoord) {
                if (element != nullptr && element->GetText() != nullptr) {
                    double coords[3];
                    double id;
                    std::istringstream elemCoords(element->GetText());
                    while (elemCoords >> id && elemCoords >> coords[0] && elemCoords >> coords[1] && elemCoords >> coords[2]){
                        vcoord.push_back(Coord(coords[0], coords[1], coords[2]));
                        vid.push_back(IdKey(id));
                    }
                }
            }

            void VCoordParseXml(tinyxml2::XMLElement* element, VCoord& vcoord) {
                if (element != nullptr && element->GetText() != nullptr) {
                    double coords[3];
                    std::istringstream elemCoords(element->GetText());
                    while (elemCoords >> coords[0] && elemCoords >> coords[1] && elemCoords >> coords[2])
                        vcoord.push_back(Coord(coords[0], coords[1], coords[2]));
                }
            }

            void methodContext::clear() {
                method_name.clear();
                method_input.clear();
                input_type.clear();
            }

            vectorVar const* methodContext::getInput(localVar& var, size_t const& n) const{
                if (input_type[n] == MethodInputType::Parsed)
                {
                    vectorVar const* locvar = var.getVar(method_input[n]);
                    if (locvar == nullptr)
                    {
                        std::stringstream s;
                        s << "Requested variable '" << method_input[0] << "' is declared, but has not been assigned any value. Check format rules if 'variableAssign' is set up correctly for this variable.";
                        throw std::runtime_error(s.str().c_str());
                    }
                    return locvar;
                }
                else {
                    var.initVar(method_input[n], Type::C);
                    var.setVar(method_input[n], method_input[n]);
                    return var.getVar(method_input[n]);
                }
            }
		}
	}
}
