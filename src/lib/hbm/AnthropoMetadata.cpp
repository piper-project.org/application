/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoMetadata.h"


using namespace std;

namespace piper {
    namespace hbm {

        AnthropoMetadataHeight::AnthropoMetadataHeight() : AnthropoMetadata<units::Length>("height") {}

        AnthropoMetadataHeight:: AnthropoMetadataHeight(tinyxml2::XMLElement* element) {
            AnthropoMetadata<units::Length>::parseXml(element);
        }

        AnthropoMetadataWeight::AnthropoMetadataWeight() : AnthropoMetadata<units::Mass>("weight") {}

        AnthropoMetadataWeight::AnthropoMetadataWeight(tinyxml2::XMLElement* element) {
            AnthropoMetadata<units::Mass>::parseXml(element);
        }

        AnthropoMetadataAge::AnthropoMetadataAge() : AnthropoMetadata<units::Age>("age") {}

        AnthropoMetadataAge::AnthropoMetadataAge(tinyxml2::XMLElement* element) {
            AnthropoMetadata<units::Age>::parseXml(element);
        }

        void Anthropometry::clear() {
            age.setValue(0.0);
            height.setValue(0.0);
            weight.setValue(0.0);
        }


        void Anthropometry::setAnthropometry(tinyxml2::XMLElement* element) {
            if (element->FirstChildElement("name") != nullptr && element->FirstChildElement("name")->GetText() != nullptr) {
                if (element->FirstChildElement("name")->GetText() == height.name()) {
                    AnthropoMetadataHeight tmp(element);
                    height.setValue(tmp.value());
                }
                else if (element->FirstChildElement("name")->GetText() == age.name()) {
                    AnthropoMetadataAge tmp(element);
                    age.setValue(tmp.value());
                }
                else if (element->FirstChildElement("name")->GetText() == weight.name()) {
                    AnthropoMetadataWeight tmp(element);
                    weight.setValue(tmp.value());
                }
            }
        }
        void Anthropometry::serializeXml(tinyxml2::XMLElement* element) {
            if (height.isDefined())
                height.serializeXml(element);
            if (weight.isDefined())
                weight.serializeXml(element);
            if (age.isDefined())
                age.serializeXml(element);
        }
        void Anthropometry::serializePmr(tinyxml2::XMLElement* element) {
            if (height.isDefined())
                height.serializePmr(element);
            if (weight.isDefined())
                weight.serializePmr(element);
            if (age.isDefined())
                age.serializePmr(element);

        }
        void Anthropometry::convertToUnit(units::Length targetLengthUnit) {
            height.convertToUnit(targetLengthUnit);
        }
        void Anthropometry::convertToUnit(units::Mass targetMassUnit) {
            weight.convertToUnit(targetMassUnit);
        }
        void Anthropometry::convertToUnit(units::Age targetAgeUnit) {
            age.convertToUnit(targetAgeUnit);
        }

        void Anthropometry::convertToUnit(units::Length targetLengthUnit, units::Mass targetMassUnit, units::Age targetAgeUnit) {
            convertToUnit(targetLengthUnit);
            convertToUnit(targetMassUnit);
            convertToUnit(targetAgeUnit);
        }

    }
}
