/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_CONTOURCL_H
#define PIPER_HBM_CONTOURCL_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif
#include "contourCLj.h"
#include "contourCLbr.h"
#include <queue>
#include <stack>
namespace piper {
	namespace hbm {

		/**
		* @brief A class which defines the full HumanBody on the basis of bodyregions.
		* It is prepopulated by the landmarks when we load the contour module.
		* It represents the hierarchical structure of Skeleton or Structure of Skeleton in tree format
		*
		/// linear representation of skeleton hierarchy 
        Pelvis
		 Torso
		  Neck
		   Head
		  ShoulderL
		   ElbowL
		    WristL
		  ShoulderR
		    ElbowR
		     WristR
         HipL
		  KneeL
		   AnkleL
         HipR
		  KneeR
		   AnkleR            
		*
		* @author Aditya Chhabra @date 2016 
		*/


		
        class HBM_EXPORT ContourCL {
            public:

                ContourCL();
                ~ContourCL();
				void printSkele();
				
            

                ContourCLbr* root;  //pointer to body region
 //               piper::contours::ContoursHbmInterface conthbm;
				ContourCLj* findparentJoint(std::string);

				ContourCLj* getJoint(std::string);

				ContourCLbr* getBodyRegion(std::string name);

				std::map<std::string, std::vector<std::string>> undefinedCLElements;

				
		};



	}
}
#endif

