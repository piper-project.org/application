/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "GenericMetadata.h"

#include <vector>


#include "xmlTools.h"
#include "FEModel.h"

namespace piper {
namespace hbm {


    std::string const GenericMetadata::domainDecompositionBoundary = "_decomposition";

GenericMetadata::GenericMetadata() : BaseMetadataGroup()
{}

GenericMetadata::GenericMetadata(std::string const& name) : BaseMetadataGroup(name)
{}

GenericMetadata::GenericMetadata(tinyxml2::XMLElement* element)
{
    BaseMetadataGroup::parseXml(element);
}

void GenericMetadata::getGMNodesIds(const FEModel& femodel, VId & vecNodeId, bool skip1D, bool skip2D, bool skip3D, bool skipnodes) const
{
	vecNodeId.clear();
	if (!skip1D)
	{
		for (auto it = groupElement3D.begin(); it != groupElement3D.end(); ++it) {
			const VId& sid = femodel.getGroupElements3D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
				vecNodeId.insert(vecNodeId.end(), femodel.getElement3D(*it).get().begin(), femodel.getElement3D(*it).get().end());
			}
		}
	}
	if (!skip2D)
	{
		for (auto it = groupElement2D.begin(); it != groupElement2D.end(); ++it) {
			const VId& sid = femodel.getGroupElements2D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
				vecNodeId.insert(vecNodeId.end(), femodel.getElement2D(*it).get().begin(), femodel.getElement2D(*it).get().end());
			}
		}
	}
	if (!skip3D)
	{
		for (auto it = groupElement1D.begin(); it != groupElement1D.end(); ++it) {
			const VId& sid = femodel.getGroupElements1D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
				vecNodeId.insert(vecNodeId.end(), femodel.getElement1D(*it).get().begin(), femodel.getElement1D(*it).get().end());
			}
		}
	}
	if (!skipnodes)
	{
		for (auto it = groupNode.begin(); it != groupNode.end(); ++it) {
			const VId& sid = femodel.getGroupNode(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
				vecNodeId.push_back(*it);
			}
		}
	}

	std::sort(vecNodeId.begin(), vecNodeId.end());
	VId::iterator it;
	it = std::unique(vecNodeId.begin(), vecNodeId.end());
	vecNodeId.resize(std::distance(vecNodeId.begin(), it));
}

VId GenericMetadata::getNodeIdVec(const FEModel& fem) const
{
    VId vid;
    getGMNodesIds(fem, vid);
    return vid;
}

}
}


