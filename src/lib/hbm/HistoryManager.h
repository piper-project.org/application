/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HISTORYMANAGER_H
#define PIPER_HISTORYMANAGER_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "History.h"
#pragma warning(disable:4251)

namespace piper {
    namespace hbm {

        class HBM_EXPORT HistoryManager {

        public:
            HistoryManager();
            ~HistoryManager();

            HistoryManager & operator=(const HistoryManager& other);

            void clear();

            void read(std::string const& file);
            void write(std::string const& file);

            /// \brief set active a new node history with string identifier.
            /// it deletes all histories after the current one and put a new one at the top of history.
            /// It copies the active model history in the new one.
            /// 
            /// \param stringID: The string that identifies the new node history
            void addNewHistory(std::string const& stringID);

            /// \brief Returns the all history string identifiers
            /// 
            /// \return vector of string identifier 
            std::vector<std::string> const getHistoryList() const;

            /// \brief from a proposed name,generate a history name with duplication in history
            /// 
            /// \param trame: The trame to construct available  history string identifier
            std::string getValidHistoryName(std::string trame);

            /// \brief set active the model history identified by a string identifier.
            /// 
            /// \param stringID: The string that identifies the node history to set active
            void setCurrentModel(std::string const& stringID);

            /// \brief rename the current model in history.
            /// 
            /// \param stringID: The string that defines the new name. It must be defined using getAvailableNameHistory to get a valid name
            void renameCurrentModel(std::string const& stringID);

            /// \brief Returns the string identifier to the current active history
            /// 
            /// \param stringID: The string that identifies the node history
            /// \return returns true if history identified by stringID exists 
            bool isHistory(std::string const& stringID) const;


            /// \brief Returns the current model identifier
            ///  
            /// \return string identifier 
            std::string const& getCurrent() const;

            void setData(HistoryBase * data);

        private:
            std::vector<std::string> m_orderhistory;
            std::string m_id;
            std::vector < HistoryBase*> m_data;

            void init();

            /// \brief set active the node history identified by a string identifier. it delete all history after this one
            /// 
            /// \param stringID: The string that identifies the node history to set active 
            /// \return returns true if the resquested history is set active 
            bool resetHistoryTo(std::string const& stringID);

        };

    }
}
#endif
