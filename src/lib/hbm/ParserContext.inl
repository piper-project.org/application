/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

namespace piper {
	namespace hbm {
		namespace parser {

			template<typename T>
			bool ParserContext::apply_getId_inSource(VId& vid) {
				std::vector<Id> vidpiper;
				std::shared_ptr<T> curobj = nullptr;
				vectorVar const* loc = localvar.getVar(methodcontext.method_input[0]);
                if (loc == nullptr)
                    return false;
				bool spy = false;
				for (vectorVar::const_iterator itv = loc->begin(); itv != loc->end(); ++itv) {
					std::vector<IdKey> idv;
					(*itv)->convertIdKey(idv);
					std::shared_ptr<T> cur = searchId<T>(femodel, vsourceId, idv[0]);
					if (cur != nullptr) {
						vid.push_back(cur->getId());
						spy = true;
					}
				}
				return spy;
			}

			template<typename T>
			bool ParserContext::apply_getId(VId& vid) {
				std::vector<Id> vidpiper;
				vectorVar const* loc = localvar.getVar(methodcontext.method_input[0]);
                if (loc == nullptr)
                    return false;
				bool spy = false;
				for (vectorVar::const_iterator itv = loc->begin(); itv != loc->end(); ++itv) {
					std::vector<IdKey> idv;
					(*itv)->convertIdKey(idv);
					std::shared_ptr<T> cur = femodel->get<T>(curkwparsed, idv[0]);
					if (cur != nullptr) {
						vid.push_back(cur->getId());
						spy = true;
					}
				}
				return spy;
			}

            //template<typename T>
            //void ParserContext::apply_setId(const std::set<std::string>& kwsource) {
            //    std::vector<Id> vidpiper;
            //    std::shared_ptr<T> curobj = nullptr;
            //    vectorVar const* loc = localvar.getVar(methodcontext.method_input[0]);
            //    bool spy = false;
            //    for (vectorVar::const_iterator itv = loc->begin(); itv != loc->end(); ++itv) {
            //        std::vector<IdKey> idv;
            //        (*itv)->convertIdKey(idv);
            //        std::shared_ptr<T> cur = searchId<T>(femodel, vsourceId, idv[0]);
            //        if (cur != nullptr) {
            //            vid.push_back(cur->getId());
            //            spy = true;
            //        }
            //    }
            //    return spy;
            //}

			template<typename T>
			void ParserContext::apply_setId() {
				curobjidpiper.clear();
				vectorVar const* locvar = methodcontext.getInput(localvar, 0);
				std::string srcekw;
				for (vectorVar::const_iterator itv = locvar->begin(); itv != locvar->end(); ++itv) {
					std::vector<IdKey> idv;
					(*itv)->convertIdKey(idv);
					if (idv.size() == 0)
					{
						std::stringstream s;
						s << "Unexpected empty expression";
						throw std::runtime_error(s.str().c_str());
					}
					std::shared_ptr<T> curobj = femodel->get<T>(curkwparsed, idv[0]);
					if (curobj == nullptr) {
						curobj = std::make_shared<T>();
						femodel->set<T>(curobj, curkwparsed, idv[0]);
					}
					curobjidpiper.push_back(curobj->getId());
				}
			}

			template< typename PoliticMethod>
			void ParserContext::applyMethod() {
				//
				// iterator on id
				std::vector<Id>::const_iterator itid = curobjidpiper.begin();
				bool spy = false;
				size_t nb = methodcontext.getInput(localvar, 0)->size();
				if (curobjidpiper.size() == nb)
					spy = true;
                else if (curobjidpiper.size() > nb) {
                    spy = true;
                    nb = curobjidpiper.size();
                }
				int nid = 0;
                std::vector<parser::Type> vartype;
                for (size_t nn = 0; nn < methodcontext.getNumberInputs(); nn++) {
                    vartype.push_back(methodcontext.getInput(localvar, nn)->getParseParam().m_type);
                }
				for (size_t n = 0; n < nb; n++) {
					std::vector<var*> locs;
                    for (size_t nn = 0; nn < methodcontext.getNumberInputs(); nn++) {
                        if (methodcontext.getInput(localvar, nn)->size() > 1){
                            locs.push_back(methodcontext.getInput(localvar, nn)->get(n));
                        }
                        else {
                            locs.push_back(methodcontext.getInput(localvar, nn)->get(0));
                        }
					}
                    PoliticMethod::Apply(curobjidpiper[nid], locs, vartype, femodel);
					if (spy)
						nid++;
				}
			}

			template< typename PoliticMethod>
			void ParserContext::applyMethod(const std::set<std::string>& kwsource) {
				//
				// iterator on id
				std::vector<Id>::const_iterator itid = curobjidpiper.begin();
				bool spy = false;
				size_t nb = methodcontext.getInput(localvar, 0)->size();
				if (curobjidpiper.size() == nb)
					spy = true;
                else if (curobjidpiper.size() > nb) {
                    spy = true;
                    nb = curobjidpiper.size();
                }
                int nid = 0;
                std::vector<parser::Type> vartype;
                for (size_t nn = 0; nn < methodcontext.getNumberInputs(); nn++) {
                    vartype.push_back(methodcontext.getInput(localvar, nn)->getParseParam().m_type);
                }
				for (size_t n = 0; n < nb; n++) {
                    std::vector<var*> locs;
                    std::vector<parser::Type> vartype;
                    for (size_t nn = 0; nn < methodcontext.getNumberInputs(); nn++) {
                        if (methodcontext.getInput(localvar, nn)->size() > 1){
                            locs.push_back(methodcontext.getInput(localvar, nn)->get(n));
                        }
                        else {
                            locs.push_back(methodcontext.getInput(localvar, nn)->get(0));
                        }
					}
                    PoliticMethod::Apply(curobjidpiper[nid], locs, vartype, kwsource, femodel);
					if (spy)
						nid++;
				}
			}


			template<typename T>
            void ParserContext::apply_setName<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string name;
				vb[0]->get(name, 0);
				femodel->get<T>(id)->setName(name);
			}

			template<typename T>
            void ParserContext::apply_setType<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string type;
				vb[0]->get(type, 0);
				femodel->get<T>(id)->setType(type);
			}

			template<typename T>
            void ParserContext::apply_setPartFlag<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string partflag;
				vb[0]->get(partflag, 0);
				if (partflag == "true")
					femodel->get<T>(id)->setPart(true);
			}

			template<typename T>
            void ParserContext::apply_setCoord<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				// get id of current object to manipulate
				std::vector<double> coord;
				vb[0]->get(coord);
				femodel->get<T>(id)->setCoord(coord[0], coord[1], coord[2]);
			}



			template<typename T>
            void ParserContext::apply_setElemDef<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel) {
				std::vector<Id> elemVendor;
				vb[0]->get(elemVendor);
				//determine type of element for number of unique nid
				uniqueValue<Id>(elemVendor);
				std::vector<Id> elemdef; //nid piper for eleme def
				for (std::vector<Id>::const_iterator iti = elemVendor.begin(); iti != elemVendor.end(); ++iti) {
					NodePtr cur = searchId<Node>(femodel, kwsource, IdKey(*iti));
					if (cur != nullptr)
						elemdef.push_back(cur->getId());
					else {
						cur = std::make_shared<Node>();
						femodel->set(cur, *kwsource.begin(), IdKey(*iti));
						elemdef.push_back(cur->getId());
					}
				}
				std::shared_ptr<T> cur = femodel->get<T>(id);
				cur->setElemDef(elemdef);
				cur->orderElement();
			}


			template<typename T>
            void ParserContext::apply_setOrigin<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel) {
				std::string type;
				vb[1]->get(type, 0);
				// get idf for origin
				if (type == "nodeid") {
					Id idfe;
					vb[0]->get(idfe, 0);
					NodePtr cur = searchId<Node>(femodel, kwsource, IdKey(idfe));
					if (cur != nullptr)
						femodel->get<T>(id)->setOrigin(cur->getId());
				}
				else if (type == "coord") {
					std::vector<double> coord;
					vb[0]->get(coord);
					//add node for origin
					std::ostringstream oss;
					oss << "NodeCreateforOrg_frame_" << id;
					NodePtr ndorg = std::make_shared<Node>();
					ndorg->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndorg, oss.str());
					femodel->get<T>(id)->setOrigin(ndorg->getId());
				}
				else if (type == "vector") {
					//add node for origin
					std::ostringstream oss;
					oss << "NodeCreateforFrame_" << id;
					std::vector<double> coord;
					coord.push_back(0.0);
					coord.push_back(0.0);
					coord.push_back(0.0);
					NodePtr ndorg = std::make_shared<Node>();
					ndorg->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndorg, oss.str());
					femodel->get<T>(id)->setOrigin(ndorg->getId());
				}
			}

			template<typename T>
            void ParserContext::apply_setFirstAxis<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel) {
				std::string type;
				vb[1]->get(type, 0);
				// get idf for FirstAxis
				if (type == "nodeid") {
					Id idfe;
					vb[0]->get(idfe, 0);
					NodePtr cur = searchId<Node>(femodel, kwsource, IdKey(idfe));
					if (cur != nullptr)
						femodel->get<T>(id)->setFirstDirection(cur->getId());
				}
				else if (type == "coord") {
					std::vector<double> coord;
					vb[0]->get(coord);
					//add node for FirstAxis
					std::ostringstream oss;
					oss << "NodeCreateforFirstA_frame_" << id;
					NodePtr ndfa = std::make_shared<Node>();
					ndfa->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndfa, oss.str());
					femodel->get<T>(id)->setFirstDirection(ndfa->getId());
				}
				else if (type == "vector") {
					std::vector<double> vect;
					vb[0]->get(vect);
					if (!femodel->get<T>(id)->hasOrigin()) {
						//choose a node as origin
						femodel->get<T>(id)->setOrigin(femodel->getNodes().getMaxId());
					}
					Id idorgpiper = femodel->get<T>(id)->getOriginId();
					double orgx = femodel->getNode(idorgpiper).getCoordX();
					double orgy = femodel->getNode(idorgpiper).getCoordY();
					double orgz = femodel->getNode(idorgpiper).getCoordZ();
					//add node for FirstAxis
					std::ostringstream oss;
					oss << "NodeCreateforFirstA_frame_" << id;
					std::vector<double> coord;
					coord.push_back(orgx + vect[0]);
					coord.push_back(orgy + vect[1]);
					coord.push_back(orgz + vect[2]);
					NodePtr ndfa = std::make_shared<Node>();
					ndfa->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndfa, oss.str());
					//femodel->set( ndfa);
					femodel->get<T>(id)->setFirstDirection(ndfa->getId());
				}
			}

			template<typename T>
            void ParserContext::apply_setPlane<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel) {
				std::string type;
				vb[1]->get(type, 0);
				if (type == "nodeid") {
					Id idfe;
					vb[0]->get(idfe, 0);
					NodePtr cur = searchId<Node>(femodel, kwsource, IdKey(idfe));
					if (cur != nullptr)
						femodel->get<T>(id)->setSecondDirection(cur->getId());
				}
				else if (type == "coord") {
					std::vector<double> coord;
					vb[0]->get(coord);
					// create id for plane node to be created
					//add node for plane
					std::ostringstream oss;
					oss << "NodeCreateforSecondA_frame_" << id;
					NodePtr ndpl = std::make_shared<Node>();
					ndpl->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndpl, oss.str());
					femodel->get<T>(id)->setSecondDirection(ndpl->getId());
				}
				else if (type == "vector") {
					std::vector<double> vect;
					vb[0]->get(vect);
					if (!femodel->get<T>(id)->hasOrigin()) {
						//choose a node as origin
						femodel->get<T>(id)->setOrigin(femodel->getNodes().getMaxId());
					}
					Id idorgpiper = femodel->get<T>(id)->getOriginId();
					double orgx = femodel->getNode(idorgpiper).getCoordX();
					double orgy = femodel->getNode(idorgpiper).getCoordY();
					double orgz = femodel->getNode(idorgpiper).getCoordZ();
					//add node for plane
					std::ostringstream oss;
					oss << "NodeCreateforSecondA_frame_" << id;
					std::vector<double> coord;
					coord.push_back(orgx + vect[0]);
					coord.push_back(orgy + vect[1]);
					coord.push_back(orgz + vect[2]);
					NodePtr ndpl = std::make_shared<Node>();
					ndpl->setCoord(coord[0], coord[1], coord[2]);
					femodel->set(ndpl, oss.str());
					femodel->get<T>(id)->setFirstDirection(ndpl->getId());
				}
			}

			template<typename T>
            void ParserContext::apply_setFirstDirection<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string strdirection;
				vb[0]->get(strdirection, 0);
                strdirection.erase(std::remove_if(strdirection.begin(), strdirection.end(), ::isspace), strdirection.end());
                try {
                    femodel->get<T>(id)->setFirstDirection(strToAxisName.at(strdirection));
                } catch (std::out_of_range) {
                    throw std::runtime_error("hbm::ParserContext: Invalid direction name: "+strdirection);
                }
			}


			template<typename T>
            void ParserContext::apply_setSecondDirection<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string strdirection;
				vb[0]->get(strdirection, 0);
                strdirection.erase(std::remove_if(strdirection.begin(), strdirection.end(), ::isspace), strdirection.end());
                try {
                    femodel->get<T>(id)->setSecondDirection(strToAxisName.at(strdirection));
                } catch (std::out_of_range) {
                    throw std::runtime_error("hbm::ParserContext: Invalid direction name: "+strdirection);
                }
			}


			template<typename T, typename Tadd>
            void ParserContext::apply_addInGroup<T, Tadd>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel) {
				std::vector<IdKey> addvendorid;
				vb[0]->convertIdKey(addvendorid);
				VId vidpiper;
				for (std::vector<IdKey>::const_iterator iti = addvendorid.begin(); iti != addvendorid.end(); ++iti) {
					std::shared_ptr<Tadd> cur = searchId<Tadd>(femodel, kwsource, *iti);
					if (cur != nullptr)
						vidpiper.push_back(cur->getId());
				}
				femodel->get<T>(id)->addInGroup(vidpiper);

			}

			//template<typename T>
			//void ParserContext::apply_setFrame<T>::Apply(const Id id, const vectorVar& vb, const std::set<std::string>& kwsource, FEModel* femodel) {
			//	//std::vector<IdKey> addvendorid;
			//	//vb[0]->convertIdKey( addvendorid);
			//	//Id idframe;
			//	//if (IdGenerator<FEFrame>::searchId(idframe, kwsource, addvendorid[0])) {
			//	//	Eigen::Quaternion<double> quat=femodel->getFrameOrientation( idframe);
			//	//	Eigen::Matrix3d orient =  quat.toRotationMatrix();
			//	//	Coord org=femodel->getFrameOrigin( idframe);
			//	//	std::vector<double> coord;
			//	//	coord.push_back(org[0]+orient(0,0));
			//	//	coord.push_back(org[1]+orient(0,1));
			//	//	coord.push_back(org[2]+orient(0,2));					
			//	//	//Id idaxe=Node::getIdfromIdFormat( detail::IdFormat("NodeCreateforJoint",femodel->getNodes().getMaxId()+1));
			//	//	Node* nd=new Node( "NodeCreateforJoint");
			//	//	nd->setCoord( coord[0], coord[1], coord[2]);
			//	//	femodel->set( nd);
			//	//	//femodel->get<T>( id)->addNodeDef( idaxe);
			//	//}
			//}

			template<typename T>
            void ParserContext::apply_setValue<T>::Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel) {
				std::string paramname;
				vb[1]->get(paramname, 0);
                if (vartype[0] == parser::Type::F) {
                    std::vector<double> value;
                    vb[0]->get(value);
                    femodel->appendParametersValue(id, paramname, value);
                }
                else if (vartype[0] == parser::Type::UI || vartype[0] == parser::Type::UIpos) {
                    std::vector<unsigned int> value;
                    vb[0]->get(value);
                    femodel->appendParametersValue(id, paramname, value);
                }                
			}




		}
	}
}
