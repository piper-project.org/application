/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <sstream>
#include <fstream>
#include <string>
#include "Contour.h"

namespace piper {
	namespace hbm {

		
		Contour::Contour(){}

		Contour::Contour(int containerid, int seq, int region, int pid , int lcs , std::vector<NodePtr> *poly     )
		{

			contid = containerid;
			sequence_no = seq;
			body_region = region;
			planeid = pid;
			lcsid = lcs;
			contour_points = *poly;
            contour_type = POLYLINE_CONTOUR;
		}	

		Contour::Contour(int containerid, int seq, int region, int pid , int lcs , std::vector<NodePtr> *spline , std::vector<double>* u_vec , int no  )
		{

			contid = containerid;
			sequence_no = seq;
			body_region = region;
			planeid = pid;
			lcsid = lcs;
			spline_points = *spline;
			spline_u = *u_vec;
			no_of_desired_points = no;
            contour_type = CUBIC_INTERP_SPLINE_CONTOUR;
		}	
		Contour::Contour(int containerid, int seq, int region, NodePtr cent , int pid , double r1 , double r2 , int no  )
		{

			contid = containerid;
			sequence_no = seq;
			body_region = region;
			centre = cent;
			planeid = pid;
			radius1 =  r1;
			radius2 = r2;
			no_of_desired_points = no;
            contour_type = CIRCLE_CONTOUR;
		}	

		



		Node Contour::centroid_calculation(){

			double x = 0;
			double y = 0;
			double z = 0;
			centroid.setCoord(x, y, z);

			for (int i = 0; i<contour_points.size(); i++)
			{
				x = centroid.getCoordX() + contour_points.at(i)->getCoordX();
				y = centroid.getCoordY() + contour_points.at(i)->getCoordY();
				z = centroid.getCoordZ() + contour_points.at(i)->getCoordZ();
				centroid.setCoord(x, y, z);
			}
			x = centroid.getCoordX() / contour_points.size();
			y = centroid.getCoordY() / contour_points.size();
			z = centroid.getCoordZ() / contour_points.size();
			centroid.setCoord(x, y, z);
			modified_centroid = centroid;
			return centroid;
		}


		void Contour::setContourCentroid(){

			double x = 0;
			double y = 0;
			double z = 0;
			centroid.setCoord(x, y, z);

			for (int i = 0; i<contour_points.size(); i++)
			{
				x = centroid.getCoordX() + contour_points.at(i)->getCoordX();
				y = centroid.getCoordY() + contour_points.at(i)->getCoordY();
				z = centroid.getCoordZ() + contour_points.at(i)->getCoordZ();
				centroid.setCoord(x, y, z);
			}
			x = centroid.getCoordX() / contour_points.size();
			y = centroid.getCoordY() / contour_points.size();
			z = centroid.getCoordZ() / contour_points.size();
			centroid.setCoord(x, y, z);
			
		}


		Coord Contour::getCentroidCoord(){

			Coord cent;
			cent[0] = centroid.getCoordX();
			cent[1] = centroid.getCoordY();
			cent[2] = centroid.getCoordZ();
			return cent;
		}


		std::vector<double> Contour::ABCDforPlane(NodePtr p1, NodePtr p2, NodePtr p3)
		{
			double a, b, c, d;
			double x1 = p1->getCoordX(); double x2 = p2->getCoordX(); double x3 = p3->getCoordX();
			double y1 = p1->getCoordY(); double y2 = p2->getCoordY(); double y3 = p3->getCoordY();
			double z1 = p1->getCoordZ(); double z2 = p2->getCoordZ(); double z3 = p3->getCoordZ();

			a = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
			b = -((x2 * z3 - x3 * z2) - (x1 * z3 - x3 * z1) + (x1 * z2 - x2 * z1));
			c = (x2 * y3 - x3 * y2) - (x1 * y3 - x3 * y1) + (x1 * y2 - x2 * y1);
			d = -(x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1));

			std::vector<double> zk;
			zk.push_back(a);
			zk.push_back(b);
			zk.push_back(c);
			zk.push_back(d);

			return zk;
		}


		void Contour::get_points_for_plane(){
			size_t size = contour_points.size();
			size_t b, c;
			b = size / 3;
			c = size * 2 / 3;
			plane_points.push_back(contour_points.at(0));
			plane_points.push_back(contour_points.at(b));
			plane_points.push_back(contour_points.at(c));

		}

		void Contour::setABCDcontPlane(){
				std::vector<double> abcd;
				std::vector<NodePtr> plane_temp;
				get_points_for_plane();
				plane_temp = plane_points;	/*!< member function of Contours class returns a vector of pointer to three nodes */
				abcd = ABCDforPlane(plane_temp.at(0), plane_temp.at(1), plane_temp.at(2));
				abcd_contplane = abcd;
		}

		void Contour::setUvalue(double tval){
			uvalue = tval;
		}

		std::vector<NodePtr> Contour::enlarge_contour(std::vector<NodePtr> con_pts, double del)
		{
			double x, y, z;
			for (int i = 0; i<con_pts.size(); i++)
			{
				x = (con_pts.at(i)->getCoordX() - centroid.getCoordX())*del + centroid.getCoordX();
				y = (con_pts.at(i)->getCoordY() - centroid.getCoordY())*del + centroid.getCoordY();
				z = (con_pts.at(i)->getCoordZ() - centroid.getCoordZ())*del + centroid.getCoordZ();
				con_pts.at(i)->setCoord(x, y, z);
			}
			return con_pts;
		}









		Plane::Plane(const int pid, NodePtr pp1, NodePtr pp2, NodePtr pp3) : p1(pp1), p2(pp2), p3(pp3)
		{
			double x1 = pp1->getCoordX(); double x2 = pp2->getCoordX(); double x3 = pp3->getCoordX();
			double y1 = pp1->getCoordY(); double y2 = pp2->getCoordY(); double y3 = pp3->getCoordY();
			double z1 = pp1->getCoordZ(); double z2 = pp2->getCoordZ(); double z3 = pp3->getCoordZ();
			a = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
			b = -((x2 * z3 - x3 * z2) - (x1 * z3 - x3 * z1) + (x1 * z2 - x2 * z1));
			c = (x2 * y3 - x3 * y2) - (x1 * y3 - x3 * y1) + (x1 * y2 - x2 * y1);
			d = -(x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1));
			//set(pp);
			plane_id = pid;

		}
		/*
		void Plane::read_plane(std::string filename)
		{
		const int MAXLEN = 512;
		char line[MAXLEN];
		std::ifstream k_file(filename);
		std::ofstream data("Debug.txt");
		std::ofstream yahoo("yerrrr.txt");
		std::ofstream idexist("exists.txt");
		enum parser_state_t { INIT, READPOINT, READPLANE, READCOORD, READCIRCLE, READPOLY, READCUBIC };
		enum parser_state_t parser_state = INIT;
		int i=0,j=0,k=0;
		std::map<int,NodePtr> hash_point;
		std::map<int,Plane*> contour_plane;
		std::vector<NodePtr> p_vec;
		std::streampos pos;
		bool poly = false;
		while(!k_file.eof())
		{
		if(poly)
		{
		k_file.seekg(pos);
		p_vec.clear();
		}
		k_file.getline(line, MAXLEN, '\n');

		if (line[0] == '*') {				// state change (new section)
		poly = false;
		if (!strcmp(line+1, "POINT"))
		{
		parser_state = READPOINT;
		}
		else if (!strcmp(line+1, "PLANE"))
		{
		parser_state = READPLANE;
		}
		else
		{
		std::ofstream initt("init.txt");
		}

		}
		else if (line[0] == '$')
		{

		}
		else {							// read data

		switch (parser_state)
		{
		case READPOINT:
		{
		int point_id;
		double x, y, z;
		std::istringstream iss_line(line);
		iss_line >> point_id >> x >> y >> z;
		Id newid = Node::getIdfromIdFormat( IdFormat( "Contour_Nodes", point_id));
		std::ofstream node_piperid("piperid.txt");
		node_piperid << newid  << "endl" << point_id ;
		node_piperid.close();
		NodePtr newnode = new Node(newid,x,y,z);
		std::ofstream aditya("node.txt");
		aditya << "X:" << newnode->getCoordX() << "  Y:" << newnode->getCoordY() << "  Z:" << newnode->getCoordZ()  << x << " " << y << " " << z  ;
		aditya.close();

		hash_point.insert(std::pair<int,NodePtr>(point_id,newnode));
		data << line <<  std::endl;
		break;
		}

		case READPLANE:
		{
		int pid, p1, p2, p3;
		std::istringstream iss_line1(line);
		iss_line1 >> pid >> p1 >> p2 >> p3;
		NodePtr a = hash_point[p1];
		NodePtr b = hash_point[p2];
		NodePtr c = hash_point[p3];
		std::ofstream planeess("planes.txt");
		planeess << a->getCoordX() << " " << a->getCoordY() << " " << a->getCoordZ() ;
		planeess.close();
		Id newid = Plane::getIdfromIdFormat( IdFormat( "Contours_Planes", pid));
		yahoo << pid << " " << newid << IdGenerator<Plane>::count() << std::endl;
		Plane* newplane = new Plane(newid , a, b, c);
		/*				try {
		Plane* newplane = new Plane(newid , a, b, c);
		double y2 = b->getCoordY();
		double z3 = c->getCoordZ();
		double y3 = c->getCoordY();
		double z2 = b->getCoordZ();
		double y1 = a->getCoordY();
		double z1 = a->getCoordZ();

		double A = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
		if(newplane->getPontA() == A)
		throw "error";
		}catch(const char *e){
		std::ofstream error_p("plane_const.txt");
		error_p << "your constructor is wrong";
		} comment_close

		contour_plane[newid] = newplane;
		//m_planes.setEntity(newplane);
		//plane->set_max_id(pid);
		data << line << std::endl;
		std::ofstream size_p("plane_size.txt");
		size_p << "your plane size is" << contour_plane.size();
		size_p.close();

		break;
		}
		}

		}
		}
		std::ofstream last("last.txt");
		last << "I reached till here";
		last.close();

		yahoo.close();
		idexist.close();
		//distribute_contours_to_body_region();
		//m_hbm_contours.sort_all();
		data.close();


		//pInfo() << QStringLiteral("Checking model...") << endl;


		}


		*/







}

}
