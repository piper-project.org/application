/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef IDREGISTATOR__H
#define IDREGISTATOR__H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "xmlTools.h"

#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
#include <boost/functional/hash.hpp>
#include <boost/unordered_map.hpp>
#include <memory>

namespace piper {
    namespace hbm {
        /**
        * @brief The IdKey defines an id by  pair <int,string> from FE format
        *
        * @author Erwan Jolivet @date 2015
        * \ingroup libhbm
        */
        class HBM_EXPORT IdKey {

        public:
            IdKey();
            IdKey(const int& idinput);
            IdKey(const unsigned int& idinput);
            IdKey(const double& idinput);
            IdKey(const std::string& idstr);

            bool operator <(const IdKey& other) const;
            bool operator ==(const IdKey& other) const;
            bool operator !=(const IdKey& other) const;
            std::string infoId() const;

            std::string idstr;
            int id;

        };

        namespace detail {

            struct KwHasher
            {
                std::size_t operator()(const std::string& kw) const
                {
                    using boost::hash_value;
                    using boost::hash_combine;

                    // Start with a hash value of 0    .
                    std::size_t seed = 0;

                    // Modify 'seed' by XORing and bit-shifting in
                    // one member of 'Key' after the other:
                    hash_combine(seed, hash_value(kw));

                    // Return the result.
                    return seed;
                }
            };

            struct IdKeyHasher
            {
                std::size_t operator()(const IdKey& idv) const
                {
                    using boost::hash_value;
                    using boost::hash_combine;

                    // Start with a hash value of 0    .
                    std::size_t seed = 0;

                    // Modify 'seed' by XORing and bit-shifting in
                    // one member of 'Key' after the other:
                    hash_combine(seed, hash_value(idv.id));
                    hash_combine(seed, hash_value(idv.idstr));

                    // Return the result.
                    return seed;
                }
            };

        }

        /**
        * @brief The IdRegistrator register a piper Id with an IdFormat
        *
        * @author Erwan Jolivet @date 2015
        * \ingroup libhbm
        */
        namespace parser {
            class Associationparameter;
        }


        template <typename T, typename TT>
        class HBM_EXPORT IdRegistrator {

            friend class FEModel;
            typedef boost::unordered_map<IdKey, unsigned int, detail::IdKeyHasher> map_idv;
            typedef boost::unordered_map<std::string, map_idv, detail::KwHasher> map_kw;


        public:
            IdRegistrator() {}
            ~IdRegistrator() {}


            void clear();

            /// serialize register id
            void serializeIdXml(tinyxml2::XMLElement* element);

            /// register a id component associated with Key (can be a module, a FE keyword...)
            void registerId(std::shared_ptr<TT> piper, const std::string& key);
            /// register a id component associated with Key (can be a module, a FE keyword...), a integer Id and a string name
            void registerId(std::shared_ptr<TT> piper, const std::string& key, const IdKey& id);

            //unsigned int get(const std::string& key, const IdKey& id);

            //unsigned int get(const std::string& key, const IdKey& id) const;


            /// <summary>
            /// Gets the highest ID of the currently registered ones.
            /// </summary>
            /// <param name="key">The key.</param>
            /// <returns>The highest of current IDs or -1 if there is nothing registered / the key is invalid.</returns>
            int getMaxIdfromIdFormat(const std::string& key);

            VId getIdfromIdFormat(const std::string& key);
            bool getIdfromIdFormat(Id& idpiper, const std::string& key, const IdKey& id);

            VId getIdfromIdFormat(const std::string& key) const;
            bool getIdfromIdFormat(Id& idpiper, const std::string& key, const IdKey& id) const;

            void getId_FE(const VId& idpiper, std::vector<std::string>& key, std::vector<IdKey>& id) const;
            void getId_FE(const Id& idpiper, std::string& key, IdKey& id) const;

            std::map<hbm::Id, std::pair<std::string, IdKey>> const& computePiperIdToIdKeyMap() const;

            void unregisteredId(const std::vector<Id>& vid);
            void unregisteredId(const Id& vid);

        private:
            map_kw m_registered;
            mutable bool inverseComputed; //true if the inverse map m_piperIdToKWMap is computed 
            mutable std::map<unsigned int, std::pair<std::string, IdKey>> m_piperIdToKWMap;
            std::string infoIds() const;
            std::string infoId(Id const& id) const;



        };

    }
}
#endif
