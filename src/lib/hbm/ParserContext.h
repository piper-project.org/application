/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PARSERCONTEXT__H
#define PARSERCONTEXT__H


#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "ParserFormatRule.h"
#include "ParserModelDescription.h"

#include <set>
#include <sstream>
#include <algorithm>
#include <string>

namespace piper {
    namespace hbm {
        namespace parser {
            class Association;

            class fileRessource{
            public:
                fileRessource() : m_counter(0) {}
                ~fileRessource();

                virtual void open(std::string const& filename)=0;
                bool isOpen() const;
                void close();

                std::string const& getFilename();
                
                /// <summary>
                /// Line counter in the input file.
                /// </summary>
                /// <returns>The line number of the last line that was read.</returns>
                unsigned int getCounterLine() const;

           protected:
                fstream m_file;
                std::string m_filename;
                unsigned int m_counter;
                std::vector<int> m_commentcar;
            };

            class fileRessourceUpdate : public fileRessource {
            public:
                void open(std::string const& filename);
                void write(std::string const& line);

            };

            class fileRessourceParse : public  fileRessource {
            public:
                void open(std::string const& filename);
                void setSearchInfo(std::vector<int> const& commentcar);
                bool getnextkeyword(ParserFormatRule::keywordcont const& listKw);
                
                /// <summary>
                /// This is the keyword that was recognized on the current line.
                /// </summary>
                /// <returns>The string under which it can be found in the rules definition. Use <seealso cref="getCurLine"/>
                /// to get the keyword as the actual text that is on the line. For FULL_LINE keywords this will be the same,
                /// but for keywords defined as regular expressions it will be different.</returns>
                std::string const& getCurrentLineKeyword() const;
                // copy line in target file until a requested keyword is found
                bool copykeyword(fileRessourceUpdate& target, ParserFormatRule::keywordcont const& listKw);
                void nextLine();
                bool nextLineNoSkipComment();


                /// <summary>
                /// The last parsed line. If the last line was a keyword,
                /// this will contain it without any modification (<seealso cref="getCurrentLineKeyword"/>).
                /// </summary>
                /// <returns>The last parsed line.</returns>
                std::string const& getCurLine() const;
                std::string& getCurLine();

            private:
                std::string m_kw;
                std::string m_line;

                std::istream& safeGetlineAndKeywords(bool& iscomment);
                std::istream& safeGetlineAndKeywordsNoSkip(bool& iscomment);
                std::istream& safeGetline();
                bool isRequestedkeyword(ParserFormatRule::keywordcont const& listKw);
            };


           class ParserContext {
            public:
                ParserContext() {};
                explicit ParserContext(std::string const& formatrulesFile);
                explicit ParserContext(std::string const& formatrulesFile, std::string const& modelrulesFile);
                ~ParserContext();

                //void initFormatrule(std::string const& formatrulesFile);
                //void initModelDescription(std::string const& modelrulesFile);

                void setFEModel(FEModel* femodelset);

                void parsing();
                void updating();

				void parseOnlyNodeComponent();

                void parseOnlyMeshComponent();

                void parseOnlyNonMeshComponent();


                void freeResources();

                fileRessourceParse& getFileRessourceParse();
                fileRessourceUpdate& getFileRessourceUpdate();

                ParserFormatRule const& getParserFormatRule() const;
                ParserModelDescription const& getModelDescription() const;

                void setParsedKw(std::string const& kw) { curkwparsed = kw; }
                const std::string& getParsedKw() const { return curkwparsed; }

                void setModelDescriptionFile(std::string const& filename);
                void setFormatName(std::string const& name);

                std::vector<std::string> const& getIncfiles() const;
                void addIncFiles(std::string const& file);
                void incFileReset();

                void applyObjectMethod();

                void applyUpdateObjectMethod();

                /// pointer to femodel
                FEModel* femodel;

            private:
                ParserFormatRule m_formatrules;
                ParserModelDescription m_modeldescription;

                fileRessourceParse m_file;
                fileRessourceUpdate m_fileoutput;
                // current kw parsed
                std::string curkwparsed;

                //
                std::vector<std::string> incfiles;

                // update context
                struct updateContext {
                    int count;
                    std::vector<bool> isupdateline;
                    std::vector<std::string> updatelinekw;
                    std::map<int, std::set<std::string>> map_count_var;
                    void reset() { count = -1; isupdateline.clear(); updatelinekw.clear(); map_count_var.clear();}
                };

                
				template<typename T>
				bool apply_getId_inSource(VId& vid);

				template<typename T>
				bool apply_getId(VId& vid);

				template<typename T>
				void apply_setId();

				template<typename PoliticMethod>
				void applyMethod();

				template<typename PoliticMethod>
				void applyMethod(const std::set<std::string>& kwsource);

			template<typename T>
			struct apply_setName {
                static void  Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			template<typename T>
			struct apply_setType {
                static void  Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			template<typename T>
			struct apply_setPartFlag {
                static void  Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			template<typename T>
			struct apply_setCoord {
                static void  Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			template<typename T>
			struct apply_setElemDef {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel);
			};

			template<typename T, typename Tadd>
			struct  apply_addInGroup {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel);
			};


			template<typename T>
			struct  apply_setOrigin {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel);
			};


			template<typename T>
			struct  apply_setFirstAxis {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel);
			};

			template<typename T>
			struct  apply_setPlane {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, const std::set<std::string>& kwsource, FEModel* femodel);
			};

			template<typename T>
			struct  apply_setFirstDirection {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			template<typename T>
			struct  apply_setSecondDirection {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

			//template<typename T>
			//struct  apply_setFrame {
			//	static void Apply(const Id id, const vectorVar& vb, const std::set<std::string>& kwsource, FEModel* femodel);
			//};


			template<typename T>
			struct  apply_setValue {
                static void Apply(const Id id, const std::vector<var*>& vb, std::vector<parser::Type> const& vartype, FEModel* femodel);
			};

            public:

                /// container for association
                std::vector< Association*> association;
                /// current parser status: result of condition evaluation
                bool resultcondition;
                /// current parser status: current string to be parsed
                std::string strconditiontest;
                /// current parser status: container of local variables
                localVar localvar;
                /// current parser status: curent object ype
                std::string curobjtype;
                /// update context
                updateContext updatecontext;
                std::vector<std::string> updateline;
                /// method context
                methodContext methodcontext;
                // sourceId context
                std::set<std::string> vsourceId;
                /// current parser status: current object Id(s)
                VId curobjidpiper;
                /// reset context of the parser when start to parse a new keyword
                void contextReset();




            };

        }
    }
}
#include "ParserContext.inl"
#endif
