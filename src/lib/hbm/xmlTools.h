/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_XMLTOOLS_H
#define PIPER_HBM_XMLTOOLS_H

#include <set>
#include <vector>
#include <sstream>

#include <tinyxml/tinyxml2.h>

#include "types.h"
#define NB_MAX_ELEMENT_PER_LINE 500

namespace piper {

/** \ingroup libhbm
 * Some xml parsing/serializing re-usable functions
 * @{
 */
namespace hbm {


void parseCoord(Coord& point3d, const tinyxml2::XMLElement* element);
void parseVecCoord(std::vector<Coord>& point3dCont, const tinyxml2::XMLElement* element);

template<class ContainerType>
void parseValueCont(ContainerType& valueCont, const tinyxml2::XMLElement* element)
{
    if (element != nullptr && element->GetText() != nullptr) {
        typename ContainerType::value_type value;
        std::stringstream elemValue(element->GetText());
        while(elemValue >> value) {
            valueCont.insert(valueCont.end(), value);
        }
    }
}

void serializeCoord(tinyxml2::XMLElement* element, Coord const& point3d);
void serializeVecCoord(tinyxml2::XMLElement* element, std::vector<Coord> const& point3dCont);
void serializeVecCoord(tinyxml2::XMLElement* element, std::vector<Coord> const& point3dCont, VId const& vid);

template<class ContainerType>
void serializeValueCont(tinyxml2::XMLElement* element, ContainerType const& valueCont)
{
    std::stringstream values;
    int count=1;
    for (typename ContainerType::const_iterator it=valueCont.begin(); it!=valueCont.end(); ++it, ++count){
        values << *it << " ";
    if(count%NB_MAX_ELEMENT_PER_LINE==0)
            values<<"\n";
    }
    element->SetText(values.str().c_str());
}

}
}

#endif // PIPER_HBM_XMLTOOLS_H
