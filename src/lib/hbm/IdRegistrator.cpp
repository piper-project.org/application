/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "IdRegistrator.inl"
#include "Node.h"
#include "Element.h"
#include "Group.h"
#include "FEFrame.h"
#include "FEModelParameter.h"

#include <sstream>

template class piper::hbm::IdRegistrator<piper::hbm::Node, piper::hbm::Node>;
template class piper::hbm::IdRegistrator<piper::hbm::Element1D, piper::hbm::Element1D>;
template class piper::hbm::IdRegistrator<piper::hbm::Element2D, piper::hbm::Element2D>;
template class piper::hbm::IdRegistrator<piper::hbm::Element3D, piper::hbm::Element3D>;
template class piper::hbm::IdRegistrator<piper::hbm::Group, piper::hbm::GroupNode>;
template class piper::hbm::IdRegistrator<piper::hbm::Group, piper::hbm::GroupElements1D>;
template class piper::hbm::IdRegistrator<piper::hbm::Group, piper::hbm::GroupElements2D>;
template class piper::hbm::IdRegistrator<piper::hbm::Group, piper::hbm::GroupElements3D>;
template class piper::hbm::IdRegistrator<piper::hbm::Group, piper::hbm::GroupGroups>;
template class piper::hbm::IdRegistrator<piper::hbm::FEFrame, piper::hbm::FEFrame>;
template class piper::hbm::IdRegistrator<piper::hbm::FEModelParameter, piper::hbm::FEModelParameter>;


namespace piper {
    namespace hbm {

        IdKey::IdKey() : idstr(""), id(-1) {};
        IdKey::IdKey(const int& idinput) : idstr(""), id(idinput) {}
        IdKey::IdKey(const unsigned int& idinput) : idstr(""), id(idinput) {}
        IdKey::IdKey(const double& idinput) : idstr("") {
            if (idinput >= 0 && round(idinput) == idinput)
                id = (int)round(idinput);
            else {
                std::stringstream str;
                str << "Id must be an unsigned integer" << std::endl;
                throw std::runtime_error(str.str().c_str());
                return;
            }
        }
        IdKey::IdKey(const std::string& idstr) : idstr(idstr), id(-1) {}

        bool IdKey::operator <(const IdKey& other) const {
            if (idstr.compare(other.idstr) == 0)
                return id < other.id;
            return idstr.compare(other.idstr) < 0;
        }

        bool IdKey::operator ==(const IdKey& other) const {
            return ((idstr == other.idstr) && (id == other.id));
        }

        bool IdKey::operator !=(const IdKey& other) const {
            return (!(*this == other));
        }

        std::string IdKey::infoId() const {
            std::string str;
            if (id != -1) {
                std::ostringstream oss;
                oss << id;
                str = oss.str();
            }
            if (idstr != "") {
                if (id != -1)
                    str += "-";
                str += idstr;
            }
            return str;
        }

    }
}
