/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_BASEMETADATA_H
#define PIPER_HBM_BASEMETADATA_H

#pragma warning(disable:4251) // this should eventually be removed and dealt with properly through pImpl or other means

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <tinyxml/tinyxml2.h>

#include "types.h"
#include "IdRegistrator.h"

#include <map>

namespace piper {
    namespace hbm {
        class FEModel;
    /**
     * @brief Base class for Metadata.
     *
     * @author Erwan JOLIVET @date 2016
     */
    class HBM_EXPORT BaseMetadata {
    public:
        BaseMetadata();
        BaseMetadata(std::string const& name);

        ~BaseMetadata();

        void setName(const std::string& name);
        std::string const& name() const;

        virtual void serializeXml(tinyxml2::XMLElement* element) const;
        virtual void serializePmr(tinyxml2::XMLElement* element) const;
        static void serializePmr_FEids(tinyxml2::XMLElement* element, std::map<std::string, std::vector<IdKey>> const& mapids);

    protected:
        std::string m_name;
	    virtual void parseXml(tinyxml2::XMLElement* element);
    };


    /**
     * @brief base Metadata defined as group of FE components.
     *
     * @author Erwan JOLIVET @date 2016
     */
     class HBM_EXPORT BaseMetadataGroup : public BaseMetadata {
     public:
        BaseMetadataGroup();
        BaseMetadataGroup(std::string const& name);
        ~BaseMetadataGroup();

        virtual void serializeXml(tinyxml2::XMLElement* element) const;
        virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;

        const VId& get_groupNode() const { return groupNode; }
        const VId& get_groupElement1D() const { return groupElement1D; }
        const VId& get_groupElement2D() const { return groupElement2D; }
        const VId& get_groupElement3D() const { return groupElement3D; }
        const VId& get_groupGroup() const { return groupGroup; }

        VId& get_groupNode() { return groupNode; }
        VId& get_groupElement1D() { return groupElement1D; }
        VId& get_groupElement2D() { return groupElement2D; }
        VId& get_groupElement3D() { return groupElement3D; }
        VId& get_groupGroup() { return groupGroup; }

     protected:

         VId groupNode;
         VId groupElement1D;
         VId groupElement2D;
         VId groupElement3D;

         virtual bool has_groupNode() const=0;
         virtual bool has_groupElement1D() const=0;
         virtual bool has_groupElement2D() const=0;
         virtual bool has_groupElement3D() const=0;
         virtual bool has_groupGroup() const=0;

         virtual void parseXml(tinyxml2::XMLElement* element);

     private:
         VId groupGroup;


     };

    }
}

#endif // PIPER_HBM_BASEMETADATA_H
