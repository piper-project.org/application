/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_CONTROLPOINT_H
#define PIPER_HBM_CONTROLPOINT_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <tinyxml/tinyxml2.h>
#include "Node.h"

namespace piper {
    namespace hbm {
	class FEModel;

/**
 * @brief A ControlPoint.
 *
 * \todo do we need an Id ?
 * @author Thomas Dupeux @date 2015
 */
    class HBM_EXPORT GenericControlPoint {
        public:
            typedef std::set<Coord> Point3DCont;

           GenericControlPoint();
           GenericControlPoint(std::string const& name);

           void parseXml(tinyxml2::XMLElement* element);
           void serializeXml(tinyxml2::XMLElement* element) const;
           void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;

           virtual void clear(FEModel& fem);

           std::string const& name() const;
           void setName(std::string const& name);
           std::vector<Coord> const& getControlPt3dCont() const;
           VId const& getGroupNodeId() const;
           size_t getNbControlPt() const;
           void addNodeId(Id& id);
           void getCoordControlPoint(hbm::FEModel const& femModel, std::vector<Coord>& vcoord) const;


           void move3DPointsToFEModel(hbm::FEModel& femModel);
           void moveFEModelNodeSIdTo3DPoints(hbm::FEModel& femModel);
           void duplicateCtrlPtFromFEModel(hbm::FEModel& femModel);
           void clearCtrlPtFromFEModel(hbm::FEModel& femModel);

        private:
            virtual void doParseXml(tinyxml2::XMLElement* element)=0;
            virtual void doSerializeXml(tinyxml2::XMLElement* element) const =0;
    virtual void doSerializePmr(tinyxml2::XMLElement* element, FEModel& fem) const = 0;

        protected:
            std::string m_name;
            VId groupNodeId;
            VCoord point3dCont;
    };

    class HBM_EXPORT InteractionControlPoint : public GenericControlPoint{
    public:
        enum class ControlPointRole { CONTROLPOINT_ROLENOTSET = -1, CONTROLPOINT_SOURCE, CONTROLPOINT_TARGET };
            InteractionControlPoint();
            InteractionControlPoint(tinyxml2::XMLElement* element);
            InteractionControlPoint(std::string const& name);
            InteractionControlPoint(std::string const& name, const VId& ctrlPtNodeSId);

            std::vector<double> const& getWeight() const;
            std::vector<double> & getWeight();
            virtual void clear(FEModel& fem) override;

            std::vector<Coord> & getControlPt3dCont();

            void setControlPointRole(const char* role);
            ControlPointRole getControlPointRole() const;
            void setControlPointRole(ControlPointRole iControlPointRole);
            std::string getControlPointRoleStr() const;

            /// <summary>
            /// Sets the specified weight to be used by all control points in this set.
            /// </summary>
            /// <param name="weight">The weight to assign. 
            /// When this is set, it should be used to override the individual weights stored in weightcont. Set to NAN to not use it.</param>
            void setGlobalWeight(double weight);
            
            /// <summary>
            /// Gets the weight that should be used with all control points in this subset of control points.
            /// </summary>
            /// <returns>The weight to use or NAN if no global weight should be used (use weights obtained from getWeight instead of the global weight).</returns>
            double getGlobalWeight();
            double getGlobalWeight() const;
    
            /// <summary>
            /// Sets the bone/skin association for each control point. The order must match the order of control points in the control points containers,
            /// i.e. in the order the points would be returned from getCoordControlPoint().
            /// ALL previously assigned association values will be overwritten!
            /// </summary>
            /// <param name="as_bone">0-1 for each CP, describes how much should these control points affect bones.</param>
            /// <param name="as_skin">0-1 for each CP, describes how much should these control points affect skin.</param>
            void setAssociation(std::vector<double> as_bones, std::vector<double> as_skin);

            /// <summary>
            /// Sets the individual weights (nuggets) for each control point. The order must match the order of control points in the control points containers,
            /// i.e. in the order the points would be returned from getCoordControlPoint().
            /// ALL previously assigned weights will be overwritten!
            /// </summary>
            /// <param name="weight">Nugget values for individual control points in this set. Should be non-positive.</param>
            void setWeights(std::vector<double> weight);
            
            /// <summary>
            /// For each point, describes if it is associated with skin/bones.
            /// </summary>
            /// <returns>A value between 0 and 1 for each control point, 0 = no association at all, 1 = exactly on skin/bone.</returns>
            std::vector<double> &getAssociationSkin();
            std::vector<double> &getAssociationBones();
            std::vector<double> const&getAssociationSkin() const;
            std::vector<double> const&getAssociationBones() const;
            

            /// <summary>
            /// Association for the whole set.
            /// This should be treated with lower priority than per-point associtaions ( getAssociationSkin() ), 
            /// i.e. if .per-point association is defined, ignore this.
            /// </summary>
            /// <param name="association">One value applicable to whole set. NAN if it is not defined</param>
            void setGlobalAssociationBones(double association);
            void setGlobalAssociationSkin(double association);
            double getAssociationSkinGlobal() const;
            double getAssociationBonesGlobal() const;

        protected :
            virtual void doParseXml(tinyxml2::XMLElement* element);
            virtual void doSerializeXml(tinyxml2::XMLElement* element) const;
            virtual void doSerializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;
            bool getControlPointRoleString(std::string& iControlPointRole) const;

            std::vector<double> weightCont;
            double globalWeight, globalAs_bones, globalAs_skin;
            std::vector<double> as_bones, as_skin;
            ControlPointRole ctrlPtRole;
    };    
    }
}

#endif // PIPER_HBM_CONTROLPOINT_H
