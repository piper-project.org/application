/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_CONTOURCLdof_H
#define PIPER_HBM_CONTOURCLdof_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "Contour.h"

namespace piper {
	namespace hbm {

		
        class HBM_EXPORT ContourCLdof {
		public:
			ContourCLdof();
			~ContourCLdof();

			private:


				std::vector<std::string> motion_axes;
				std::vector<std::string> reference_axes;
				std::vector<std::string> target_axes;
				std::string dofname;

			public:

				std::vector<std::string> getMotionaxes();
				std::vector<std::string> getReferenceaxes();
				std::vector<std::string> getTargetaxes();
				std::string getdofname();
				void setMotionaxes(std::string axes1, std::string axes2);
				void setReferenceaxes(std::string axes1, std::string axes2);
				void setTargetaxes(std::string axes1, std::string axes2);
				void setName(std::string dofname);
           
				
		};



	}
}
#endif

