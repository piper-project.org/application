/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PARSERHELPER__H
#define PARSERHELPER__H


#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif


#include "Helper.h"
#include "FEModel.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <set>
#include <iostream>
#include <iomanip>
#include <sstream>


namespace piper {
	namespace hbm {
		class Metadata;
		namespace parser {



            ///fast string to int conversion
            //http://stackoverflow.com/questions/16826422/c-most-efficient-way-to-convert-string-to-int-faster-than-atoi
            unsigned int stringToUnsignedInt(const char *str);

            class ParserContext;
            template <class T> class HBM_EXPORT Prototype
            {
            public:
                virtual ~Prototype(){}
            };


            //small factory for Term
            template<typename T>
            class Factory {
            public:
                static std::map<std::string, std::function<std::shared_ptr<T>()>> m_map;
                static void clear();
            public:
                //associate key<=> prototype
                static void Register(const std::string& key, std::function<std::shared_ptr<T>()> obj);
                //create instance
                std::shared_ptr<T> Create(const std::string& key) const;
            };

            enum Type{ UNDEF, UIpos, UI, F, C };

            /// struct to store parse parameter for a variable
            class ParseParam {
            private:
                std::vector<bool> isParsed;
            public:
                ParseParam();
                ParseParam(Type type,
                    int offset,
                    int length,
                    std::string const& sep,
                    unsigned int repeat,
                    unsigned int offrepeat,
                    std::string const& defaultstr,
                    bool hasDefault);

                Type m_type;
                unsigned int m_offset;
                int m_length;
                std::string m_sep;
                unsigned int m_repeat;
                unsigned int m_offrepeat;
                std::string m_default;
                bool hasDefault;
            };

            struct catchParam {
                catchParam(std::string name, unsigned int startval, unsigned int lengthval)
                    : varname(name)
                    , start(startval)
                    , length(lengthval) {}
                catchParam(std::string name, unsigned int startval)
                    : varname(name)
                    , start(startval)
                    , length(0) {}
                std::string varname;
                int start;
                int length;
            };

            typedef std::unordered_map<std::string, ParseParam> paramParseCont;
            typedef std::vector<catchParam> catchParamCont;


            class varInterface{
            public:
                virtual ~varInterface() {}
                template<class T> void get(T& var, const int& n)const;
                template<class T> void get(std::vector<T>&)const;
                virtual size_t size() const = 0;
                template<class T, class U> void setValue(U const& value);
                template<class T, class U> void setValue(std::vector<U> const& value);
                virtual void convertIdKey(std::vector<IdKey>& idv) const{};
                virtual void reserve(size_t const& n) {};
                virtual varInterface* clone() const = 0;
            };

            template<typename T>
            class VarBase : public varInterface {
            public:
                VarBase() {}
                ~VarBase() {}
                VarBase(const VarBase &other) = default;
                VarBase& operator= (const VarBase &other) = default;
                void reserve(size_t const& n);
                void get(T& var, const int& n) const;
                void get(std::vector<T>& var) const;
                size_t size() const;
                void setValue(T const& value);
                void setValue(std::vector<T> const& value);
                void convertIdKey(std::vector<IdKey>& idv) const;
                varInterface* clone() const;
            private:
                std::vector<T> m_var;

            };

            template<class T> void varInterface::get(T& var, const int& n) const {
                dynamic_cast<const VarBase<T>&>(*this).get(var, n);
            }
            template<class T> void varInterface::get(std::vector<T>& var) const {
                dynamic_cast<const VarBase<T>&>(*this).get(var);
            }
            template<class T, class U> void varInterface::setValue(const U& value) {
                dynamic_cast<VarBase<T>&>(*this).setValue(value);
            }
            template<class T, class U> void varInterface::setValue(const std::vector<U>& value) {
                dynamic_cast<VarBase<T>&>(*this).setValue(value);
            }

            class var {
            public:
                var() : myinterface(nullptr) {}
                explicit var(Type const& type, unsigned int const& value);
                explicit var(Type const& type, double const& value);
                explicit var(Type const& type, std::string const& value);
                explicit var(Type const& type, std::vector<unsigned int> const& value);
                explicit var(Type const& type, std::vector<double> const& value);
                explicit var(Type const& type, std::vector<std::string> const& value);
                explicit var(Type const& type);
                ~var();
                // prevent copy
                var(const var &other) = delete;
                var& operator= (const var &other) = delete;
                //
                template<typename T> 
                void get(T& var, const int& n) const;
                template<typename T>
                void get(std::vector<T>& var) const;
                size_t size() const;
                template<typename T>
                void pushValue(T const& value);
                void convertIdKey(std::vector<IdKey>& idv) const;
                void reserve(size_t const& n);
            private:
                varInterface* myinterface;
            };

            class vectorVar {
            public:
                vectorVar();
                template<typename T> explicit vectorVar(Type const& type, T const& value);
                template<typename T> explicit vectorVar(Type const& type, std::vector<T> const& value);
                vectorVar(ParseParam const& parseparam);
                vectorVar(Type& type);
                ~vectorVar();

                //  prevent copy
                vectorVar(const vectorVar &other) = delete;
                vectorVar& operator= (const vectorVar &other) = delete;

                void initVar(ParseParam const& parseparam);
                void initVar(Type& type);
                void pushValue(std::string const& varname, std::string const& str);
                void clearVar();
                size_t size() const;
                var const* getLast() const;
                var* get(size_t const& n) const;
                ParseParam const& getParseParam() const;
                template<typename T>
                void setValue(T const& value);
                template<typename T>
                void setValue(Type type, std::vector<T> const& value);

                typedef std::vector<var*>::iterator iterator;
                typedef std::vector<var*>::const_iterator const_iterator;

                iterator begin();
                const_iterator begin() const;
                iterator end();
                const_iterator end() const;


            private:
                Type m_type;
                ParseParam m_param;
                std::vector<var*> m_var;

            };

            class localVar {
                //typedef boost::unordered_map<std::string, vectorVar, boost::hash<std::string>> varCont;
                typedef std::unordered_map<std::string, vectorVar*> varCont;

            public:
                localVar();
                ~localVar();
                // prevent copy
                localVar(const localVar &other) = delete;
                localVar& operator= (const localVar &other) = delete;

                void clear();
                void clearVar(const std::string& varname);
                template<typename T>
                void getLastValue(T& output, const std::string& varname) const;
                template<typename T>
                void getLastValue(std::vector<T>& output, const std::string& varname) const;
                template<typename T>
                void setVar(const std::string& varname, T const& value);
                template<typename T>
                void setVar(const std::string& varname, Type type, std::vector<T> const& value);
                void initVar(const std::string& varname, ParseParam const& parseparam);
                void initVar(const std::string& varname, Type type);
                void pushValue(std::string const& varname, std::string const& str);
                void catchValues(std::string& ss, paramParseCont const& paramparse, catchParamCont const& catchparam);
                
                /// <summary>
                /// Gets the variable with the specified name.
                /// </summary>
                /// <param name="varname">The name of the variable.</param>
                /// <returns>Pointer to the vector with values of the variable. If variable with the specified name is not found, returns nullptr.</returns>
                vectorVar const* getVar(std::string const& varname) const;
            private:
                varCont map_var;
            };
            

			template< typename T>
			std::shared_ptr<T> searchId(FEModel* fem, const std::set<std::string>& key, const IdKey& id) {
				for (std::set<std::string>::const_iterator it = key.begin(); it != key.end(); ++it) {
					std::shared_ptr<T> cur = fem->get<T>(*it, id);
					if (cur != nullptr)
						return cur;
				}
				return nullptr;
			}

			/// struct to store current method information
			struct methodContext {
				enum MethodInputType { Parsed, User };
				std::string method_name;
				std::vector<std::string>  method_input;
				std::vector<MethodInputType> input_type;
                size_t getNumberInputs() const { return method_input.size(); }
				void clear();
                vectorVar const* getInput(localVar& var, size_t const&) const;
			};

            void updateValueInt(std::string& str, std::stringstream& ss, std::vector<unsigned int>& coord, const ParseParam& pp);

            void updateValueInt(std::string& str, std::stringstream& ss, unsigned int& coord, const ParseParam& pp);

            void updateValueCoord(std::string& str, std::stringstream& ss, std::vector<double>& coord, const ParseParam& pp);

            void updateValueCoord(std::string& str, std::stringstream& ss, double& coord, const ParseParam& pp);

            void updateValueinLineFixedFormat(std::string& str, std::stringstream& ss, std::vector<unsigned int>& coord, const ParseParam& pp);

            void updateValueinLineSeparatorFormat(std::string& str, std::stringstream& ss, std::vector<unsigned int>& coord, const ParseParam& pp);
            
            void updateValueinLineFixedFormat(std::string& str, std::stringstream& ss, std::vector<double>& coord, const ParseParam& pp);

			void updateValueinLineSeparatorFormat(std::string& str, std::stringstream& ss, std::vector<double>& coord, const ParseParam& pp);

            void keywordParseXml(tinyxml2::XMLElement* element, std::vector<std::string>& vkey, std::vector<std::vector<IdKey>>& vidkey);

            void VCoordParseXml(tinyxml2::XMLElement* element, std::vector<IdKey>& vid, VCoord& vcoord);

            void VCoordParseXml(tinyxml2::XMLElement* element, VCoord& vcoord);


		}//Parser
	}//hbm
}//piper
#include "ParserHelper.inl"
#endif
