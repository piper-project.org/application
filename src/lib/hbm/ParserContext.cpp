/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParserContext.h"

#include "ParserFormatRule.h"
#include "ParserModelDescription.h"
#include "Helper.h"

#include <fstream>
#include <iostream>
#include <regex>

using namespace std;
namespace piper {
    namespace hbm {
        namespace parser {

            fileRessource::~fileRessource() {
                if (isOpen())
                    m_file.close();
            }

            bool fileRessource::isOpen() const {
                return m_file.is_open();
            }

            void fileRessource::close() {
                if (m_file.is_open()){
                    m_file.close();
                    m_filename.clear();
                }
                m_counter = 0;
            }

            std::string const& fileRessource::getFilename() {
                return m_filename;
            }

            unsigned int fileRessource::getCounterLine() const {
                return m_counter;
            }

            std::string const& fileRessourceParse::getCurLine() const {
                return m_line;
            }

            std::string& fileRessourceParse::getCurLine() {
                return m_line;
            }

            void fileRessourceParse::open(std::string const&  filename) {
                boost::filesystem::path modelFilePath(filename);
                m_file.open(modelFilePath.string(), ios::in);
                if (!m_file.is_open())	{
                    stringstream str;
                    str << "Parse model file: " << endl;
                    str << "Can not open model file: " << modelFilePath.string() << endl;
                    throw std::runtime_error(str.str().c_str());
                }
                m_filename = modelFilePath.string();
                m_file >> std::noskipws;
            }

            void fileRessourceParse::setSearchInfo(std::vector<int> const& commentcar) {
                m_commentcar = commentcar;
            }

            bool fileRessourceParse::getnextkeyword(ParserFormatRule::keywordcont const& listKw) {
                if (!isRequestedkeyword(listKw)) {
                    bool iscomment;
                    while (!safeGetlineAndKeywords(iscomment).eof()) {
                        if (!iscomment) {
                            if (isRequestedkeyword(listKw))
                                return true;
                        }
                    }
                }
                else return true;
                return false;
            }

            bool fileRessourceParse::isRequestedkeyword(ParserFormatRule::keywordcont const& listKw) {
                Kw curKw(m_kw, KW_TYPE::FULL_LINE);
                ParserFormatRule::keywordcont::const_iterator itk = listKw.find(curKw);
                if (itk == listKw.end()) // it is not full line keyword, check if it matches some of the regular expression ones
                {
                    for (Kw const& k : listKw)
                    {
                        if (k.type == KW_TYPE::REG_EXP)
                        {
                            regex reg(k.name);
                            if (regex_match(m_kw, reg))
                            {
                                m_kw = k.name; // rules for this keyword are stored as the regex - store it in m_kw so that later we know what keyword we read.
                                return true;
                            }
                        }
                    }
                }
                else
                    return true;
                return false;
            }

            std::istream& fileRessourceParse::safeGetlineAndKeywordsNoSkip(bool& iscomment) {
                m_line.clear();
                m_kw.clear();
                // The characters in the stream are read one-by-one using a std::streambuf.
                // That is faster than reading them one-by-one using the std::istream.
                // Code that uses streambuf this way must be guarded by a sentry object.
                // The sentry object performs various tasks,
                // such as thread synchronization and updating the stream state.
                std::istream::sentry se(m_file, true);
                std::streambuf* sb = m_file.rdbuf();
                m_counter++;
                int c = sb->sbumpc();
                bool firstwordend = false;
                bool firstwordendok = false;
                iscomment = false;
                // first caractere is a comment
                if (std::find(m_commentcar.begin(), m_commentcar.end(), c) != m_commentcar.end()) {
                    iscomment = true;
                    firstwordend = true;
                    m_line += (char)c;
                    c = sb->sbumpc();
                }
                for (;;) {
                    if (!firstwordend) {
                        if (c == ' ' || c == '\t')
                            firstwordend = true;
                    }
                    switch (c) {
                    case '\n':
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case '\r':
                        if (sb->sgetc() == '\n')
                            sb->sbumpc();
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case EOF:
                        // Also handle the case when the last line has no line ending
                        if (m_line.empty())
                            m_file.setstate(std::ios::eofbit);
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    default:
                        if (firstwordend && !firstwordendok) {
                            m_kw = m_line;
                            firstwordendok = true;
                        }
                        m_line += (char)c;
                    }
                    c = sb->sbumpc();
                }
            }

            std::istream& fileRessourceParse::safeGetlineAndKeywords(bool& iscomment) {
                m_line.clear();
                m_kw.clear();
                // The characters in the stream are read one-by-one using a std::streambuf.
                // That is faster than reading them one-by-one using the std::istream.
                // Code that uses streambuf this way must be guarded by a sentry object.
                // The sentry object performs various tasks,
                // such as thread synchronization and updating the stream state.
                std::istream::sentry se(m_file, true);
                std::streambuf* sb = m_file.rdbuf();
                m_counter++;
                int c = sb->sbumpc();
                bool firstwordend = false;
                bool firstwordendok = false;
                iscomment = false;
                // first caractere is a comment
                if (std::find(m_commentcar.begin(), m_commentcar.end(), c) != m_commentcar.end()) {
                    iscomment = true;
                    firstwordend = true;
                    c = sb->sbumpc();
                }
                for (;;) {
                    if (!firstwordend) {
                        if ((c == ' ' || c == '\t'))
                            firstwordend = true;
                    }
                    switch (c) {
                    case '\n':
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case '\r':
                        if (sb->sgetc() == '\n')
                            sb->sbumpc();
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case EOF:
                        // Also handle the case when the last line has no line ending
                        if (m_line.empty())
                            m_file.setstate(std::ios::eofbit);
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    default:
                        if (firstwordend && !firstwordendok) {
                            m_kw = m_line;
                            firstwordendok = true;
                        }
                        if (!iscomment)
                            m_line += (char)c;
                    }
                    c = sb->sbumpc();
                }
            }

            std::istream& fileRessourceParse::safeGetline() {
                m_line.clear();
                m_kw.clear();
                // The characters in the stream are read one-by-one using a std::streambuf.
                // That is faster than reading them one-by-one using the std::istream.
                // Code that uses streambuf this way must be guarded by a sentry object.
                // The sentry object performs various tasks,
                // such as thread synchronization and updating the stream state.
                std::istream::sentry se(m_file, true);
                std::streambuf* sb = m_file.rdbuf();
                m_counter++;
                int c = sb->sbumpc();
                bool firstwordend = false;
                bool firstwordendok = false;
                // first caractere is a comment
                if (std::find(m_commentcar.begin(), m_commentcar.end(), c) != m_commentcar.end()) {
                    m_line += (char)c; 
                    firstwordend = true;
                    c = sb->sbumpc();
                }
                for (;;) {
                    if (!firstwordend) {
                        if (c == ' ' || c == '\t')
                            firstwordend = true;
                    }
                    switch (c) {
                    case '\n':
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case '\r':
                        if (sb->sgetc() == '\n')
                            sb->sbumpc();
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    case EOF:
                        // Also handle the case when the last line has no line ending
                        if (m_line.empty())
                            m_file.setstate(std::ios::eofbit);
                        if (!firstwordendok)
                            m_kw = m_line;
                        return m_file;
                    default:
                        if (firstwordend && !firstwordendok) {
                            m_kw = m_line;
                            firstwordendok = true;
                        }
                        m_line += (char)c;
                    }
                    c = sb->sbumpc();
                }
            }


            void fileRessourceUpdate::open(std::string const&  filename) {
                boost::filesystem::path modelFilePath(filename);
                m_file.open(modelFilePath.string(), ios::out | ios::trunc);
                if (!m_file.is_open())	{
                    stringstream str;
                    str << "Parse model file: " << endl;
                    str << "Can not open model file: " << modelFilePath.string() << endl;
                    throw std::runtime_error(str.str().c_str());
                }
                m_filename = modelFilePath.string();
                m_file >> std::noskipws;
            }


            void fileRessourceUpdate::write(std::string const& line) {
                // do not use std::endl - that, apart from writing "\n", also flushes out the stream which can slow down writing significantly 
                // especially when writing to for example a network drive
                m_file << line << "\n";
            }

            std::string const& fileRessourceParse::getCurrentLineKeyword() const {
                return m_kw;
            }

            bool fileRessourceParse::copykeyword(fileRessourceUpdate& target, ParserFormatRule::keywordcont const& listKw) {
                if (isRequestedkeyword(listKw))
                    return true;
                else
                    if (m_counter!=0)
                        target.write(m_line);
                bool iscomment = false;
                while (!safeGetline().eof()) {
                    if (!isRequestedkeyword(listKw))
                        target.write(m_line);
                    else
                        return true;
                }
                target.write(m_line);
                return false;
            }

            void fileRessourceParse::nextLine() {
                bool iscommentline = true;
                while (iscommentline) {
                    if (safeGetlineAndKeywords(iscommentline).eof())
                        return;
                }
                m_line.erase(m_line.find_last_not_of(" \r\t") + 1);
            }

            bool fileRessourceParse::nextLineNoSkipComment() {
                bool iscommentline = true;
                if (!safeGetlineAndKeywordsNoSkip(iscommentline).eof())
                    return iscommentline;
                else
                    return false;
            }


            ParserContext::ParserContext(std::string const& formatrulesFile, std::string const& modelrulesFile)
                : femodel(nullptr)
                , m_formatrules(formatrulesFile)
                , m_modeldescription(modelrulesFile) {}

            ParserContext::ParserContext(std::string const& formatrulesFile)
                : femodel(nullptr)
                , m_formatrules(formatrulesFile) {}

            //void ParserContext::initFormatrule(string const& formatrulesFile) {
            //    m_formatrules = ParserFormatRule(formatrulesFile);
            //}

            //void ParserContext::initModelDescription(std::string const& modelrulesFile) {
            //    m_modeldescription = ParserModelDescription(modelrulesFile);
            //}

            ParserContext::~ParserContext() {
            }

            void ParserContext::setFEModel(FEModel* femodelset) {
                femodel = femodelset;
            }

            void ParserContext::parsing() {
                m_formatrules.setActionType(ParserFormatRule::ActionType::PARSE);
            }


            void ParserContext::updating() {
                m_formatrules.setActionType(ParserFormatRule::ActionType::UPDATE);
            }

            void ParserContext::parseOnlyMeshComponent() {
                m_formatrules.setOption(ParserFormatRule::OPTION::EVAL);
            }

			void ParserContext::parseOnlyNodeComponent() {
				m_formatrules.setOption(ParserFormatRule::OPTION::EVAL_NODES);
			}


            void ParserContext::parseOnlyNonMeshComponent() {
                m_formatrules.setOption(ParserFormatRule::OPTION::EVALSECOND);
            }

            void ParserContext::freeResources() {
                m_file.close();
                m_fileoutput.close();
            }

            fileRessourceParse& ParserContext::getFileRessourceParse() {
                return m_file;
            }

            fileRessourceUpdate& ParserContext::getFileRessourceUpdate() {
                return m_fileoutput;
            }

            ParserFormatRule const& ParserContext::getParserFormatRule() const {
                return m_formatrules;
            }

            ParserModelDescription const& ParserContext::getModelDescription() const {
                return m_modeldescription;
            }

            std::vector<std::string> const& ParserContext::getIncfiles() const {
                return incfiles;
            }

            void ParserContext::addIncFiles(std::string const& file) {
                boost::filesystem::path modelFilePath(getFileRessourceParse().getFilename());
                boost::filesystem::path modelIncPath(file);
                string tmp;
                if (!modelIncPath.has_parent_path() && modelFilePath.has_parent_path())
                    tmp = modelFilePath.parent_path().string() + "/" + file;
                else
                    tmp = file;
                incfiles.push_back(tmp);
            }

            void ParserContext::incFileReset() {
                incfiles.clear();
            }


            void ParserContext::applyObjectMethod() {
                if (curobjtype == "ObjectGroup") {
                    if (methodcontext.method_name == "setId") {
                        // the group exist already
                        VId id;
						if (apply_getId_inSource<GroupNode>(id))
                            curobjidpiper = id;
						else if (apply_getId_inSource<GroupElements1D>(id))
                            curobjidpiper = id;
						else if (apply_getId_inSource<GroupElements2D>(id))
                            curobjidpiper = id;
						else if (apply_getId_inSource<GroupElements3D>(id))
                            curobjidpiper = id;
						else if (apply_getId_inSource<GroupGroups>(id))
                            curobjidpiper = id;
                    }
                    else if (methodcontext.method_name == "setName") {
                        if (femodel->get<GroupNode>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setName<GroupNode>>();
                        else if (femodel->get<GroupElements1D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setName<GroupElements1D>>();
                        else if (femodel->get<GroupElements2D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setName<GroupElements2D>>();
                        else if (femodel->get<GroupElements3D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setName<GroupElements3D>>();
                        else if (femodel->get<GroupGroups>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setName<GroupGroups>>();
                    }
                    else if (methodcontext.method_name == "setPart") {
                        if (femodel->get<GroupElements1D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setPartFlag<GroupElements1D>>();
                        else if (femodel->get<GroupElements2D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setPartFlag<GroupElements2D>>();
                        else if (femodel->get<GroupElements3D>(curobjidpiper[0]) != nullptr)
                            applyMethod< apply_setPartFlag<GroupElements3D>>();
                    }
                }
                else if (curobjtype == "Parameter") {
                    if (methodcontext.method_name == "setId") {
                            apply_setId<FEModelParameter>();
                    }
                    else if (methodcontext.method_name == "setValue") {
                        applyMethod< apply_setValue< FEModelParameter>>();
                    }
                }

                else if (curobjtype == "Node") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<Node>();
                    }
                    else if (methodcontext.method_name == "setCoord") {
                        applyMethod< apply_setCoord< Node>>();
                    }
                }

                else if (curobjtype == "GroupNode") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<GroupNode>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<GroupNode>>();
                    }
                    else if (methodcontext.method_name == "addInGroup") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
                        applyMethod< apply_addInGroup<GroupNode, Node>>(kwsource);
                    }
                }


                else if (curobjtype == "Element3D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<Element3D>();
                    }
                    else if (methodcontext.method_name == "setElemDef") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setElemDef<Element3D>>(kwsource);
                    }

                }

                else if (curobjtype == "Element2D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<Element2D>();
                    }
                    else if (methodcontext.method_name == "setElemDef") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setElemDef<Element2D>>(kwsource);
                    }
                }

                else if (curobjtype == "Element1D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<Element1D>();
                    }
                    else if (methodcontext.method_name == "setElemDef") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setElemDef<Element1D>>(kwsource);
                    }
                }

                else if (curobjtype == "GroupElements1D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<GroupElements1D>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<GroupElements1D>>();
                    }
                    else if (methodcontext.method_name == "addInGroup") {
                        std::set<std::string> kwsource;
                        if (vsourceId.size() == 0)
                            kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentElement1D);
                        else
                            kwsource = vsourceId;
						applyMethod< apply_addInGroup<GroupElements1D, Element1D>>(kwsource);
                    }
                }

                else if (curobjtype == "GroupElements2D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<GroupElements2D>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<GroupElements2D>>();
                    }
                    else if (methodcontext.method_name == "addInGroup") {
                        std::set<std::string> kwsource;
                        if (vsourceId.size() == 0)
                            kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentElement2D);
                        else
                            kwsource = vsourceId;
						applyMethod< apply_addInGroup<GroupElements2D, Element2D>>(kwsource);
                    }
                }

                else if (curobjtype == "GroupElements3D") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<GroupElements3D>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<GroupElements3D>>();
                    }
                    else if (methodcontext.method_name == "addInGroup") {
                        std::set<std::string> kwsource;
                        if (vsourceId.size() == 0)
                            kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentElement3D);
                        else
                            kwsource = vsourceId;
						applyMethod< apply_addInGroup<GroupElements3D, Element3D>>(kwsource);
                    }
                }

                else if (curobjtype == "GroupGroups") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<GroupGroups>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<GroupGroups>>();
                    }
                    else if (methodcontext.method_name == "addInGroup") {
                        std::set<std::string> kwsource;
                        if (vsourceId.size() == 0) {
                            kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentGElement1D);
                            std::set<std::string> kwsourcetmp = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentGElement2D);
                            kwsource.insert(kwsourcetmp.begin(), kwsourcetmp.end());
                            kwsourcetmp = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentGElement3D);
                            kwsource.insert(kwsourcetmp.begin(), kwsourcetmp.end());
                            kwsourcetmp = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentGNode);
                            kwsource.insert(kwsourcetmp.begin(), kwsourcetmp.end());
                        }
                        else
                            kwsource = vsourceId;
						applyMethod< apply_addInGroup<GroupGroups, Group>>(kwsource);
                    }
                }

                else if (curobjtype == "Frame") {
                    if (methodcontext.method_name == "setId") {
                        apply_setId<FEFrame>();
                    }
                    else if (methodcontext.method_name == "setName") {
                        applyMethod< apply_setName<FEFrame>>();
                    }
                    else if (methodcontext.method_name == "setOrigin") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setOrigin<FEFrame>>(kwsource);
                    }
                    else if (methodcontext.method_name == "setFirstAxis") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setFirstAxis<FEFrame>>(kwsource);
                    }
                    else if (methodcontext.method_name == "setPlane") {
                        std::set<std::string> kwsource = getParserFormatRule().getKeywordsComponents(MESHCOMP::componentNode);
						applyMethod< apply_setPlane<FEFrame>>(kwsource);
                    }
                    else if (methodcontext.method_name == "setFirstDirection") {
                        applyMethod< apply_setFirstDirection<FEFrame>>();
                    }
                    else if (methodcontext.method_name == "setSecondDirection") {
                        applyMethod< apply_setSecondDirection<FEFrame>>();
                    }
                }

            }

            void ParserContext::applyUpdateObjectMethod() {
                if (curobjtype == "Parameter") {
                    if (methodcontext.method_name == "setId") {
                        VId id;
						apply_getId<FEModelParameter>(id);
                        curobjidpiper.clear();
                        curobjidpiper = id;

                        //curobjidpiper = apply_getId<FEModelParameter>(methodcontext, localvar, femodel, curkw);
                    }
                    else if (methodcontext.method_name == "setValue") {
                        if (curobjidpiper.size()>0) {
                            VId::const_iterator itvid = curobjidpiper.begin();
                            vector<string>::iterator itstr = updatecontext.updatelinekw.begin();
                            vector<bool>::const_iterator itbool;
                            //
                            ParseParam const& pp = methodcontext.getInput(localvar,0)->getParseParam();
                            string paramname;
                            methodcontext.getInput(localvar, 1)->getLast()->get(paramname, 0);
                            std::transform(paramname.begin(), paramname.end(), paramname.begin(), ::tolower);
                            int count = 0;
                            int countVal = 0;
                            for (itbool = updatecontext.isupdateline.begin(); itbool != updatecontext.isupdateline.end(); ++itbool) {
                                if (*itbool) {
                                    if (updatecontext.map_count_var[count].find(methodcontext.method_input[0]) !=
                                        updatecontext.map_count_var[count].end()) {
                                        vector<double> paramvalue = femodel->get<FEModelParameter>(*itvid)->get(paramname);
                                        if (paramvalue.size() < pp.m_repeat)
                                        {
                                            stringstream fileCorrupted;
                                            fileCorrupted << "Error processing file " << m_file.getFilename() <<
                                                ": keyword ID " << *itstr << " (" << paramname << ") " << " has incorrect (lower) number of parameters.";
                                            freeResources();
                                            throw std::runtime_error(fileCorrupted.str().c_str());
                                        }

                                        stringstream ss;
                                        if (pp.m_type == parser::Type::UI) {
                                            std::vector<unsigned int> paramvalueInt;
                                            paramvalueInt.reserve(paramvalue.size());
                                            for (double val : paramvalue)
                                                paramvalueInt.push_back(static_cast<unsigned int>(val));
                                            if (paramvalueInt.size() != pp.m_repeat)
                                                updateValueInt(*itstr, ss, paramvalueInt[countVal], pp);
                                            else
                                                updateValueInt(*itstr, ss, paramvalueInt, pp);
                                        }
                                        else {
                                            if (paramvalue.size() != pp.m_repeat)
                                                updateValueCoord(*itstr, ss, paramvalue[countVal], pp);
                                            else
                                                updateValueCoord(*itstr, ss, paramvalue, pp);
                                        }


                                        *itstr = std::string(ss.str());
                                        if (curobjidpiper.size()>1)
                                            ++itvid;
                                        countVal++;
                                    }
                                    count++;
                                }
                                ++itstr;
                            }
                        }
                    }
                }
                else if (curobjtype == "Node") {
                    if (methodcontext.method_name == "setId") {
                        VId id;
						apply_getId<Node>(id);
                        curobjidpiper.clear();
                        curobjidpiper = id;
                    }
                    else if (methodcontext.method_name == "setCoord") {

                        VId::const_iterator itvid = curobjidpiper.begin();
                        vector<string>::iterator itstr = updatecontext.updatelinekw.begin();
                        vector<bool>::const_iterator itbool;
                        //
                        ParseParam const& pp = methodcontext.getInput(localvar,0)->getParseParam();
                        //
                        for (itbool = updatecontext.isupdateline.begin(); itbool != updatecontext.isupdateline.end(); ++itbool, ++itstr) {
                            if (*itbool) {
                                stringstream ss;
                                Coord coord = femodel->getNode(*itvid).get();
                                vector<double> newcoord;
                                newcoord.push_back(coord[0]);
                                newcoord.push_back(coord[1]);
                                newcoord.push_back(coord[2]);
                                //
                                updateValueCoord(*itstr, ss, newcoord, pp);
                                *itstr = std::string(ss.str());
                                //
                                ++itvid;
                            }
                        }
                        //copyline( *(outstream), updatecontext.updatelinekw);
                        //updatecontext.reset();
                    }
                }
            }
            
            void ParserContext::contextReset() {
                updateline.clear();
                localvar.clear();
                strconditiontest.clear();
                vsourceId.clear();
                updatecontext.reset();
                resultcondition = false;
            }

            void ParserContext::setModelDescriptionFile(std::string const& filename) {
                this->contextReset();
                m_modeldescription.setFile(filename);
            }

            void ParserContext::setFormatName(std::string const& name) {
                m_formatrules.m_format.format = name;
            }




        }//parser
    }//hbm
}//piper
