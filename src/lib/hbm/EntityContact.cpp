/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "EntityContact.h"

#include "FEModel.h"
#include <iostream>
#include <cctype>
#include "xmlTools.h"

namespace piper {
namespace hbm {

EntityContact::EntityContact()
    : BaseMetadata()
    , m_keepThickness(false)
    , m_type(Type::NOTYPE)
    , m_group1(hbm::ID_UNDEFINED)
    , m_group2(hbm::ID_UNDEFINED)
{}

EntityContact::EntityContact(std::string const& name)
    : BaseMetadata(name)
    , m_keepThickness(false)
    , m_type(Type::NOTYPE)
    , m_group1(hbm::ID_UNDEFINED)
    , m_group2(hbm::ID_UNDEFINED)
{}

EntityContact::EntityContact(tinyxml2::XMLElement* element)
    : BaseMetadata()
    , m_keepThickness(false)
    , m_type(Type::NOTYPE)
    , m_group1(hbm::ID_UNDEFINED)
    , m_group2(hbm::ID_UNDEFINED)
{
parseXml(element);
}

void EntityContact::setEntity2(std::string const& name, const Id& id) {
	m_entity2=name;
	m_group2=id;
}

void EntityContact::setEntity1(std::string const& name, const Id& id) {
	m_entity1=name;
	m_group1=id;
}

void EntityContact::setType(const std::string& type) {
	std::string typelower( type);
	std::transform(typelower.begin(), typelower.end(), typelower.begin(), ::tolower);
	if(!typelower.compare("notype")){
		m_type=Type::NOTYPE;
	}
	else if(!typelower.compare("attached")){
		m_type=Type::ATTACHED;
	}
	else if(!typelower.compare("sliding")){
		m_type=Type::SLIDING;
	}
}

void EntityContact::serializeXml(tinyxml2::XMLElement* element) const
{
    BaseMetadata::serializeXml(element);
	tinyxml2::XMLElement* xmlType;
	xmlType = element->GetDocument()->NewElement("type");
	std::string typstr  =  EntityContactType_str[(unsigned int) type()] ;
	xmlType->SetText( typstr.c_str());
	element->InsertEndChild(xmlType);
	tinyxml2::XMLElement* xmlEntity1 = element->GetDocument()->NewElement("entity1");
	xmlEntity1->SetText(m_entity1.c_str());
	element->InsertEndChild(xmlEntity1);
	tinyxml2::XMLElement* xmlEntity2 = element->GetDocument()->NewElement("entity2");
	xmlEntity2->SetText(m_entity2.c_str());
	element->InsertEndChild(xmlEntity2);
    if (hbm::ID_UNDEFINED != m_group1) {
        tinyxml2::XMLElement* xmlId1;
        xmlId1 = element->GetDocument()->NewElement("groupId1");
        xmlId1->SetText( m_group1);
        element->InsertEndChild(xmlId1);
    }
    if (hbm::ID_UNDEFINED != m_group2) {
        tinyxml2::XMLElement* xmlId2;
        xmlId2 = element->GetDocument()->NewElement("groupId2");
        xmlId2->SetText( m_group2);
        element->InsertEndChild(xmlId2);
    }
	tinyxml2::XMLElement* xmlkt;
	xmlkt = element->GetDocument()->NewElement("keepThickness");
	xmlkt->SetText( m_keepThickness);
	element->InsertEndChild(xmlkt);
	if (!m_keepThickness) {
		tinyxml2::XMLElement* xmlthick;
		xmlthick = element->GetDocument()->NewElement("thickness");
		xmlthick->SetText( m_thickness);
		element->InsertEndChild(xmlthick);
	}
}

void EntityContact::parseXml(tinyxml2::XMLElement* element)
{
    BaseMetadata::parseXml(element);

    setType( element->FirstChildElement("type")->GetText());
	m_entity1 = element->FirstChildElement("entity1")->GetText();
	m_entity2 = element->FirstChildElement("entity2")->GetText();
    if (element->FirstChildElement("groupId1"))
        element->FirstChildElement("groupId1")->QueryUnsignedText(&m_group1);
    if (element->FirstChildElement("groupId2"))
        element->FirstChildElement("groupId2")->QueryUnsignedText(&m_group2);

	if (element->FirstChildElement("thickness"))
		element->FirstChildElement("thickness")->QueryDoubleText(&m_thickness);
    if (element->FirstChildElement("keepThickness"))
        element->FirstChildElement("keepThickness")->QueryBoolText(&m_keepThickness);
}

void EntityContact::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
    BaseMetadata::serializePmr(element);
    std::string type = EntityContactType_str[(unsigned int)this->type()];
    std::transform(type.begin(), type.end(), type.begin(), ::tolower);
    element->SetAttribute("type", type.c_str());
    tinyxml2::XMLElement* xmlkt;
    xmlkt = element->GetDocument()->NewElement("thickness");
    if (m_keepThickness)
        xmlkt->SetAttribute("keep", "true");
    else {
        xmlkt->SetAttribute("keep", "false");
        xmlkt->SetText(m_thickness);
    }
    element->InsertEndChild(xmlkt);
    // entity 1
    std::map<std::string, std::vector<IdKey>> fe_ids;
    tinyxml2::XMLElement* xmlent1 = element->GetDocument()->NewElement("entity_contact_1");
    xmlent1->SetAttribute("name", m_entity1.c_str());
    if (m_group1 != hbm::ID_UNDEFINED) {
        tinyxml2::XMLElement* xmlgrp = element->GetDocument()->NewElement("setGroup");
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupNode>();
        std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(m_group1);
        if (it != mpIds.end())
            fe_ids[mpIds.at(m_group1).first].push_back(mpIds.at(m_group1).second);
        else {
            std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Node>();
            //get group content
            VId vidcontent = fem.get<GroupNode>(m_group1)->get();
            for (Id const& idcontent : vidcontent) {
                fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
            }
        }
        serializePmr_FEids(xmlgrp, fe_ids);
        fe_ids.clear();
        xmlent1->InsertEndChild(xmlgrp);
    }
    element->InsertEndChild(xmlent1);
    // entity 2
    tinyxml2::XMLElement* xmlent2 = element->GetDocument()->NewElement("entity_contact_2");
    xmlent2->SetAttribute("name", m_entity2.c_str());
    if (m_group2 != hbm::ID_UNDEFINED) {
        tinyxml2::XMLElement* xmlgrp = element->GetDocument()->NewElement("setGroup");
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupNode>();
        std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(m_group2);
        if (it != mpIds.end())
            fe_ids[mpIds.at(m_group2).first].push_back(mpIds.at(m_group2).second);
        else {
            std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Node>();
            //get group content
            VId vidcontent = fem.get<GroupNode>(m_group2)->get();
            for (Id const& idcontent : vidcontent) {
                fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
            }
        }
        serializePmr_FEids(xmlgrp, fe_ids);
        fe_ids.clear();
        xmlent2->InsertEndChild(xmlgrp);
    }
    element->InsertEndChild(xmlent2);

}


}
}
