/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Metadata.h"

#include "ParserModelDescription.h"
#include "ParserFormatRule.h"
#include "Contour.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <set>


#include <tinyxml/tinyxml2.h>

#include "version.h"
#define PI 3.1415926535897932384626433832795 // TODO: use standard math.h m_PI !
#include "Helper.h"

#include "xmlTools.h"

using namespace std;

namespace piper {
namespace hbm {

Metadata::Metadata() {
    clear();
}

void Metadata::clear()
{
    m_modelPath.clear();
    m_source.clear();
    m_additionalInclude.clear();
    m_mesh.clear();
    m_rules.clear();
    m_lengthUnit = units::UnitTraits<units::Length>::defaultUnit();
    m_massUnit = units::UnitTraits<units::Mass>::defaultUnit();
    m_ageUnit = units::UnitTraits<units::Age>::defaultUnit();
    m_sourceFormat.clear();
    m_nameFormat.clear();
    m_gravity = Coord::Zero();
    m_landmarks.clear();
    m_controlPoints.clear();
    m_entities.clear();
    m_joints.clear();
    m_anatomicalJoints.clear();
    m_contacts.clear();
	m_hbmparameters.clear();
    m_genmetadatas.clear();
    m_anthropometry.clear();
}

void Metadata::setSourceModelFile(std::string const& sourceModelFile) {
    m_source = sourceModelFile;
}

void Metadata::setSource(std::string const& modelrulesFile, std::string const& formatrulesFileParent)
{
    clear();
    tinyxml2::XMLDocument model;
    model.LoadFile(modelrulesFile.c_str());
    if (model.Error()) {
        clear();
        std::stringstream str;
        str << "Failed to load model file: " << modelrulesFile << std::endl << model.ErrorStr() << std::endl;
        throw std::runtime_error(str.str().c_str());
    }

    tinyxml2::XMLElement* element;
    tinyxml2::XMLElement* xmlPiper = model.FirstChildElement("model_description");

    m_rules = modelrulesFile;
    // units
    element = xmlPiper->FirstChildElement("units");
    if (element->Attribute(units::LengthUnit::type().c_str()))
        setLengthUnit(units::UnitTraits<units::Length>::strToUnit(element->Attribute(units::LengthUnit::type().c_str())));
    if (element->Attribute(units::MassUnit::type().c_str()))
        setMassUnit(units::UnitTraits<units::Mass>::strToUnit(element->Attribute(units::MassUnit::type().c_str())));
    if (element->Attribute(units::AgeUnit::type().c_str()))
        setAgeUnit(units::UnitTraits<units::Age>::strToUnit(element->Attribute(units::AgeUnit::type().c_str())));


    // source
    element = xmlPiper->FirstChildElement("source");
    if (element != nullptr && element->GetText() != NULL)
        m_source = element->GetText();

    // additional include files not defined through includeFile rule
    element = xmlPiper->FirstChildElement("include_files");
    if (element != nullptr && element->GetText() != NULL)
    {
        m_additionalInclude.clear();
        parseValueCont<std::vector<std::string>>(m_additionalInclude, element);
    }

	//format rules
    element = xmlPiper->FirstChildElement("format_rules");
    if (element != nullptr){
        m_nameFormat = element->Attribute("format");
        if (element != nullptr && element->GetText() != NULL)
            m_sourceFormat = element->GetText();
    }
    if (m_nameFormat == "obj")
        m_sourceFormat = "";
    else {
        //if it is contain a path relative or not
        boost::filesystem::path sourceFormatPath(m_sourceFormat);
        if (!formatrulesFileParent.empty()) {
            if (sourceFormatPath.empty()) {
                // look for an pfr file that define this format in formatrulesFileParent
                vector<boost::filesystem::path> vpfr;
                get_all_in_directory(boost::filesystem::path(formatrulesFileParent), ".pfr", vpfr);
                for (auto const& cur : vpfr) {
                    boost::filesystem::path pfrpath = formatrulesFileParent / cur;
                    parser::ParserFormatRule formatrule(pfrpath.string());
                    if (formatrule.m_format.format == m_nameFormat) {
                        m_sourceFormat = pfrpath.string();
                    }
                }
            }
            else {
                if (sourceFormatPath.parent_path().empty()) {
                    //look for file in m_rules directory
                    boost::filesystem::path checkrelative = m_rules / sourceFormatPath;
                    //look for file in formatrulesFileParent directory
                    boost::filesystem::path checkparent = formatrulesFileParent / sourceFormatPath;
                    if (boost::filesystem::exists(checkrelative)) {// if in m_rules directory, take it
                        m_sourceFormat = checkrelative.string();
                    }
                    else if (boost::filesystem::exists(checkparent)) {// if in formatrulesFileParent directory, take it
                        m_sourceFormat = checkparent.string();
                    }
                }
            }
        }
    }
    updateModelPath(m_rules);

}



std::string const& Metadata::modelPath() const
{
    return m_modelPath;
}

std::string Metadata::sourcePath() const
{
    return absolutePath(m_source);
}

std::vector<std::string> Metadata::sourceIncludeFiles() const
{
    std::vector<std::string> ret;
    for (std::string const&s : m_additionalInclude)
        ret.push_back(absolutePath(s));
    return ret;
}


std::string Metadata::rulePath() const
{
    return absolutePath(m_rules);
}

std::string Metadata::sourceFormat() const
{
    return absolutePath(m_sourceFormat);
}

std::string Metadata::meshPath() const
{
    return absolutePath(m_mesh);
}

void Metadata::setLengthUnit(units::Length unit) {
    m_lengthUnit = unit;
    m_anthropometry.getActive()->convertToUnit(m_lengthUnit);
}

void Metadata::setMassUnit(units::Mass unit) {
    m_massUnit = unit;
    m_anthropometry.getActive()->convertToUnit(m_massUnit);
}

void Metadata::setAgeUnit(units::Age unit) {
    m_ageUnit = unit;
    m_anthropometry.getActive()->convertToUnit(m_ageUnit);
}

bool Metadata::addEntity(Entity const& entity)
{
    std::pair<EntityCont::iterator,bool> ret = m_entities.insert(std::make_pair(entity.name(), entity));
    return ret.second;
}

bool Metadata::hasEntity( const std::string& entityname) const {
	return (m_entities.find( entityname) != m_entities.end());
}

Entity const& Metadata::entity( const std::string& entityname) const {
		return m_entities.at(entityname);
}

Entity& Metadata::entity(const std::string& entityname){
	return m_entities.at(entityname);
}


EntityContact const& Metadata::contact( const std::string& contactname) const {
	return m_contacts.at(contactname);
}

bool Metadata::hasContact( const std::string& contactname) const {
	return (m_contacts.find( contactname) != m_contacts.end());
}

bool Metadata::addContact(EntityContact const& contact) {
	std::pair<ContactCont::iterator,bool> ret = m_contacts.insert(std::make_pair(contact.name(), contact));
	return ret.second;
}

EntityJoint const& Metadata::joint( const std::string& jointname) const {
	return m_joints.at(jointname);
}

EntityJoint& Metadata::joint(const std::string& jointname){
	return m_joints.at(jointname);
}

bool Metadata::hasJoint( const std::string& jointname) const {
	return (m_joints.find( jointname) != m_joints.end());
}

bool Metadata::addJoint(EntityJoint const& joint) {
	std::pair<JointCont::iterator,bool> ret = m_joints.insert(std::make_pair(joint.name(), joint));
	return ret.second;
}

AnatomicalJoint const& Metadata::anatomicalJoint( const std::string& jointname) const {
    return m_anatomicalJoints.at(jointname);
}

bool Metadata::hasAnatomicalJoint( const std::string& jointname) const {
    return (m_anatomicalJoints.find( jointname) != m_anatomicalJoints.end());
}

bool Metadata::addAnatomicalJoint(AnatomicalJoint const& joint) {
    std::pair<AnatomicalJointCont::iterator,bool> ret = m_anatomicalJoints.insert(std::make_pair(joint.name(), joint));
    return ret.second;
}

Metadata::GenericMetadataCont const& Metadata::genericmetadatas() const
{
    return m_genmetadatas;
}

GenericMetadata& Metadata::genericmetadata(std::string const& name)
{
    // TODO check existence
    return m_genmetadatas.at(name);
}


Metadata::LandmarkCont const& Metadata::landmarks() const
{
    return m_landmarks;
}

Landmark& Metadata::landmark(std::string const& name)
{
    // TODO check existence
    return m_landmarks.at(name);
}

Landmark const& Metadata::landmark(std::string const& name) const
{
    // TODO check existence
    return m_landmarks.at(name);
}

Metadata::InteractionControlPointCont & Metadata::interactionControlPoints()
{
    return m_controlPoints;
}

InteractionControlPoint *Metadata::interactionControlPoint(std::string const& name)
{
    if (m_controlPoints.find(name) != m_controlPoints.end())
        return &m_controlPoints.at(name);
    return NULL;
}

InteractionControlPoint const* Metadata::interactionControlPoint(std::string const& name) const
{
    if (m_controlPoints.find(name) != m_controlPoints.end())
        return &m_controlPoints.at(name);
    return NULL;
}

bool Metadata::hasInteractionControlPoint(std::string const& name) const{
    return (m_controlPoints.find(name) != m_controlPoints.end());
}

bool Metadata::addInteractionControlPoint(InteractionControlPoint& ctrlPt){
    std::pair<InteractionControlPointCont::iterator, bool> ret = m_controlPoints.insert(std::make_pair(ctrlPt.name(), ctrlPt));
    return ret.second;
}

bool Metadata::hasGenericmetadata(const std::string& name) const {
    return (m_genmetadatas.find(name) != m_genmetadatas.end());
}

bool Metadata::addGenericmetadata(GenericMetadata const& genmetadata)
{
    std::pair<GenericMetadataCont::iterator, bool> ret = m_genmetadatas.insert(std::make_pair(genmetadata.name(), genmetadata));
    return ret.second;
}

bool Metadata::hasLandmark( const std::string& landmarkname) const {
	return (m_landmarks.find( landmarkname) != m_landmarks.end());
}

bool Metadata::addLandmark(Landmark const& landmark)
{
    std::pair<LandmarkCont::iterator,bool> ret = m_landmarks.insert(std::make_pair(landmark.name(), landmark));
    return ret.second;
}

void Metadata::updateModelPath(std::string const& file)
{
    boost::filesystem::path filePath(file);
    m_modelPath = boost::filesystem::absolute(filePath.parent_path()).string();
}

std::string Metadata::absolutePath(std::string const& p) const
{
    if (p.empty())
        return p;
    boost::filesystem::path path(p);
    if (path.is_absolute())
        return p;
    else {
        boost::filesystem::path absolutePath(m_modelPath);
        absolutePath /= path;
        return  absolutePath.string();
    }
}

void Metadata::setNameFormat( const std::string& name) {
	m_nameFormat = name;
}

std::string Metadata::getNameFormat() const {
	return m_nameFormat;
}

template <typename MetadataType>
void parseCont(std::map<std::string, MetadataType> & cont, std::string const& tagName,
               tinyxml2::XMLElement* xmlPiper)
{
    typedef std::map<std::string, MetadataType> MetadataContType;
    tinyxml2::XMLElement* element;
    cont.clear();
    element = xmlPiper->FirstChildElement(tagName.c_str());
    while (element != nullptr) {
        MetadataType md(element);
        std::pair<typename MetadataContType::iterator, bool> ret = cont.insert(std::make_pair(md.name(), md));
        if (!ret.second)
            std::cerr << "[WARNING] Duplicated " << tagName << " ignored: " << md.name() << std::endl;
        element = element->NextSiblingElement(tagName.c_str());
    }
}

void Metadata::read(std::string file)
{
    clear();
    tinyxml2::XMLDocument model;
    model.LoadFile(file.c_str());
	if (model.Error()) {
		clear();
		std::stringstream str;
        str << "Failed to load model file: " << file << std::endl << model.ErrorStr() << std::endl;
		throw std::runtime_error(str.str().c_str());
	}

    tinyxml2::XMLElement* element;
    tinyxml2::XMLElement* xmlPiper = model.FirstChildElement("piper");

    // piper app version  which was used to write this file
    std::string version = xmlPiper->Attribute("version");
    std::vector<std::string> tokens;
    boost::split(tokens, version, boost::is_any_of("."));

    // to be used internally, in parseXml() methods, if necessary
    unsigned int fileVersionMajor, fileVersionMinor;
    std::istringstream(tokens[0]) >> fileVersionMajor;
    std::istringstream(tokens[1]) >> fileVersionMinor;

    updateModelPath(file);

    // source
    element = xmlPiper->FirstChildElement("source");
    if (element != nullptr && element->GetText()!=NULL)
        m_source = element->GetText();

    // additional include files not defined through includeFile rule
    element = xmlPiper->FirstChildElement("include_files");
    if (element != nullptr && element->GetText() != NULL)
    {
        m_additionalInclude.clear();
        parseValueCont<std::vector<std::string>>(m_additionalInclude, element);
    }

    // mesh
    element = xmlPiper->FirstChildElement("mesh");
    if (element != nullptr && element->GetText() != NULL)
        m_mesh = element->GetText();

	//model rules
    element = xmlPiper->FirstChildElement("model_rules");
    if (element != nullptr && element->GetText() != NULL)
        m_rules = element->GetText();

	//format rules
    element = xmlPiper->FirstChildElement("format_rules");
    if (element != nullptr){
        m_nameFormat = element->Attribute("format");
        if (element != nullptr && element->GetText() != NULL)
            m_sourceFormat = element->GetText();
    }


	// units
    element = xmlPiper->FirstChildElement("units");
    if (element->Attribute(units::LengthUnit::type().c_str()))
        setLengthUnit(units::UnitTraits<units::Length>::strToUnit(element->Attribute(units::LengthUnit::type().c_str())));
    if (element->Attribute(units::MassUnit::type().c_str()))
        setMassUnit(units::UnitTraits<units::Mass>::strToUnit(element->Attribute(units::MassUnit::type().c_str())));
    if (element->Attribute(units::AgeUnit::type().c_str()))
        setAgeUnit(units::UnitTraits<units::Age>::strToUnit(element->Attribute(units::AgeUnit::type().c_str())));

    element = xmlPiper->FirstChildElement("gravity");
    if (nullptr != element) {
        parseCoord(m_gravity, element);
    }


    // GenericMetadata
    parseCont(m_genmetadatas, "genericmetadata", xmlPiper);
    // Landmarks
    parseCont(m_landmarks, "landmark", xmlPiper);

    // ControlPoints
    parseCont(m_controlPoints, "controlPoint", xmlPiper);

    //m_controlPoints.clear();
    //element = model.FirstChildElement("piper")->FirstChildElement("controlPoint");
    //while(element!=nullptr) {
    //    InteractionControlPoint c;
    //    c.parseXml(element);
    //    std::pair<InteractionControlPointCont::iterator, bool> ret = m_controlPoints.insert(std::make_pair(c.name(), c));
    //    if (!ret.second)
    //        std::cerr << "[WARNING] Duplicated controlPoints ignored: " << c.name() << std::endl;
    //    element = element->NextSiblingElement("controlPoint");

    //}

    // Entities
    parseCont(m_entities, "entity", xmlPiper);
    // joints
    parseCont(m_joints, "joint", xmlPiper);
    // anatomical joints
    parseCont(m_anatomicalJoints, "anatomicalJoint", xmlPiper);
	// contacts
    parseCont(m_contacts, "contact", xmlPiper);
    // hbmparameters
    parseCont(m_hbmparameters, "hbmParameter", xmlPiper);

    // Anthropometry
    element = xmlPiper->FirstChildElement("anthropometry");
    // set unit before get value
    m_anthropometry.getActive()->age.unit() = m_ageUnit;
    m_anthropometry.getActive()->height.unit() = m_lengthUnit;
    m_anthropometry.getActive()->weight.unit() = m_massUnit;
    while (element != nullptr) {
        m_anthropometry.getActive()->setAnthropometry(element);
        element = element->NextSiblingElement("anthropometry");
    }
    //m_anthropometry.convertToUnit(m_lengthUnit, m_massUnit, m_ageUnit);


}

template <typename MetadataType>
void serializeCont(std::map<std::string, MetadataType> const& cont, std::string const& tagName,
                   tinyxml2::XMLElement* xmlPiper)
{
    typedef std::map<std::string, MetadataType> MetadataContType;
    tinyxml2::XMLElement* element;
    for (typename MetadataContType::const_iterator it = cont.begin(); it!=cont.end(); ++it) {
        element = xmlPiper->GetDocument()->NewElement(tagName.c_str());
        it->second.serializeXml(element);
        xmlPiper->InsertEndChild(element);
    }
}

template <typename MetadataType>
void serializeContPmr(std::map<std::string, MetadataType> const& cont, std::string const& tagName,
    tinyxml2::XMLElement* xmlPiper, FEModel& fem)
{
    typedef std::map<std::string, MetadataType> MetadataContType;
    tinyxml2::XMLElement* element;
    for (typename MetadataContType::const_iterator it = cont.begin(); it != cont.end(); ++it) {
        element = xmlPiper->GetDocument()->NewElement(tagName.c_str());
        it->second.serializePmr(element, fem);
        xmlPiper->InsertEndChild(element);
    }
}

void Metadata::importMetadata(std::string const& file, FEModel& fem, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool anatomicalJoint, bool controlpoint, bool HbmParameter) {
    parser::ParserModelDescription metadatarule;
    metadatarule.setFile(file, anthropometry, entity, landmark, genericmetadata, contact, joint, anatomicalJoint, controlpoint, HbmParameter);
    if (anthropometry) m_anthropometry.clear();
    if (entity) m_entities.clear();
    if (landmark) {
        for (auto & land : m_landmarks)
            land.second.clear(fem);
        m_landmarks.clear();
    }
    if (genericmetadata) m_genmetadatas.clear();
    if (contact) m_contacts.clear();
    if (joint) m_joints.clear();
    if (controlpoint) {
        for (auto & cp : m_controlPoints)
            cp.second.clear(fem);
        m_controlPoints.clear();
    }
    //if (anatomicaljoint) m_anatomicalJoints.clear();
    if (HbmParameter) m_hbmparameters.clear();

    metadatarule.fillmetadata(&fem, *this);

}

void Metadata::exportMetadata(std::string const& file, FEModel& fem, bool anthropometry, bool entity, bool landmark, bool genericmetadata, bool contact, bool joint, bool controlpoint, bool HbmParameter) {
    // compute new source and mesh, update modelPath
    boost::filesystem::path filePath(file);
    boost::filesystem::path sourcePath(this->sourcePath());
    boost::filesystem::path rulePath(this->rulePath());
    boost::filesystem::path formatrulePath(this->sourceFormat());
    std::string file_source = relativePath(sourcePath, filePath.parent_path()).string();
    std::string file_sourceFormat = relativePath(formatrulePath, filePath.parent_path()).string();

    // build tinyxml document
    tinyxml2::XMLDocument metadata;
    tinyxml2::XMLElement* element;

    metadata.InsertEndChild(metadata.NewDeclaration());
    metadata.InsertEndChild(metadata.NewUnknown("DOCTYPE piper SYSTEM \"ModelRules.dtd\""));

    tinyxml2::XMLElement* xmlMetadata = metadata.NewElement("model_description");
    metadata.InsertEndChild(xmlMetadata);

    // units
    element = metadata.NewElement("units");
    element->SetAttribute(units::LengthUnit::type().c_str(), units::LengthUnit::unitToStr(m_lengthUnit).c_str());
    element->SetAttribute(units::MassUnit::type().c_str(), units::MassUnit::unitToStr(m_massUnit).c_str());
    element->SetAttribute(units::AgeUnit::type().c_str(), units::AgeUnit::unitToStr(m_ageUnit).c_str());
    xmlMetadata->InsertEndChild(element);

    // source
    element = metadata.NewElement("source");
    //element->SetAttribute("format", FE_FORMAT_str[(size_t)sourceFormat()].c_str());
    element->SetText(file_source.c_str());
    xmlMetadata->InsertEndChild(element);

    //formatrules
    element = metadata.NewElement("format_rules");
    element->SetText(file_sourceFormat.c_str());
    element->SetAttribute("format", m_nameFormat.c_str());
    xmlMetadata->InsertEndChild(element);

    if (isGravityDefined()) {
        element = metadata.NewElement("gravity");
        serializeCoord(element, m_gravity);
        xmlMetadata->InsertEndChild(element);
    }























    if (anthropometry)
        m_anthropometry.getActive()->serializePmr(xmlMetadata);
   
    if (entity)
        serializeContPmr(m_entities, "entity", xmlMetadata, fem);

    if (landmark)
        serializeContPmr(m_landmarks, "landmarks", xmlMetadata, fem);

    if (genericmetadata)
        serializeContPmr(m_genmetadatas, "genericmetadata", xmlMetadata, fem);

    if (contact)
        serializeContPmr(m_contacts, "contact", xmlMetadata, fem);

    if (joint)
        serializeContPmr(m_joints, "joint", xmlMetadata, fem);

    if (HbmParameter)
        serializeContPmr(m_hbmparameters, "hbm_parameter", xmlMetadata, fem);

    if (controlpoint)
        serializeContPmr(m_controlPoints, "controlPoint", xmlMetadata, fem);

    // write tinyxml document
    metadata.SaveFile(file.c_str());
    if (metadata.Error()) {
        std::cerr << "[FAILED] Save metadata file (*.pmr): " << file << std::endl << metadata.ErrorStr() << std::endl;
    }
}


void Metadata::write(const std::string &file)
{
    // compute new source and mesh, update modelPath
    boost::filesystem::path filePath(file);
    boost::filesystem::path sourcePath(this->sourcePath());
    boost::filesystem::path rulePath(this->rulePath());
    boost::filesystem::path formatrulePath(this->sourceFormat());
    m_mesh = filePath.stem().string() + ".vtu";
    m_source = relativePath(sourcePath, filePath.parent_path()).string();
    m_rules = relativePath(rulePath, filePath.parent_path()).string();
    m_sourceFormat = relativePath(formatrulePath, filePath.parent_path()).string();

	updateModelPath(file);
	
	// build tinyxml document
    tinyxml2::XMLDocument model;
    tinyxml2::XMLElement* element;

    model.InsertEndChild(model.NewDeclaration());
    model.InsertEndChild(model.NewUnknown("DOCTYPE piper SYSTEM \"hbm.dtd\""));

    tinyxml2::XMLElement* xmlPiper = model.NewElement("piper");
    std::ostringstream version;
    version << PIPER_VERSION_MAJOR << "." << PIPER_VERSION_MINOR << "." << PIPER_VERSION_PATCH;
    xmlPiper->SetAttribute("version", version.str().c_str());
    model.InsertEndChild(xmlPiper);

    // source
    element = model.NewElement("source");
    //element->SetAttribute("format", FE_FORMAT_str[(size_t)sourceFormat()].c_str());
    element->SetText(m_source.c_str());
    xmlPiper->InsertEndChild(element);

    // additional include files
    element = model.NewElement("include_files");
    //element->SetAttribute("format", FE_FORMAT_str[(size_t)sourceFormat()].c_str());
    std::stringstream addI;
    for (std::string const& s : m_additionalInclude)
        addI << s << " ";
    element->SetText(addI.str().c_str());
    xmlPiper->InsertEndChild(element);

    //mesh
    element = model.NewElement("mesh");
    element->SetText(m_mesh.c_str());
    xmlPiper->InsertEndChild(element);

	//rules
	element = model.NewElement("model_rules");
	element->SetText(m_rules.c_str());
    xmlPiper->InsertEndChild(element);

	//formatrules
	element = model.NewElement("format_rules");
	element->SetText(m_sourceFormat.c_str());
	element->SetAttribute("format",m_nameFormat.c_str());
    xmlPiper->InsertEndChild(element);

    if (isGravityDefined()) {
        element = model.NewElement("gravity");
        serializeCoord(element, m_gravity);
        xmlPiper->InsertEndChild(element);
    }

    // units
    element = model.NewElement("units");
    element->SetAttribute(units::LengthUnit::type().c_str(), units::LengthUnit::unitToStr(m_lengthUnit).c_str());
    element->SetAttribute(units::MassUnit::type().c_str(), units::MassUnit::unitToStr(m_massUnit).c_str());
    element->SetAttribute(units::AgeUnit::type().c_str(), units::AgeUnit::unitToStr(m_ageUnit).c_str());
    xmlPiper->InsertEndChild(element);

    //Anthropometry
    m_anthropometry.getActive()->serializeXml(xmlPiper);

    // entities
    serializeCont(m_entities, "entity", xmlPiper);
	//joints
    serializeCont(m_joints, "joint", xmlPiper);
    // anatomical joints
    serializeCont(m_anatomicalJoints, "anatomicalJoint", xmlPiper);
	//contacts
    serializeCont(m_contacts, "contact", xmlPiper);
	// landmarks
    serializeCont(m_landmarks, "landmark", xmlPiper);
    // controlpoints
    serializeCont(m_controlPoints, "controlPoint", xmlPiper);
    //for (InteractionControlPointCont::const_iterator it = m_controlPoints.begin(); it != m_controlPoints.end(); ++it) {
    //    element = model.NewElement("controlPoint");
    //    it->second.serializeXml(element);
    //    xmlPiper->InsertEndChild(element);
    //}
	// hbmparameter
    serializeCont(m_hbmparameters, "hbmParameter", xmlPiper);
    // generic metadata
    serializeCont(m_genmetadatas, "genericmetadata", xmlPiper);

	// write tinyxml document
    model.SaveFile(file.c_str());
    if (model.Error()) {
        std::cerr << "[FAILED] Save model file: " << file << std::endl << model.ErrorStr() << std::endl;
    }
}


HbmParameter const& Metadata::hbmParameter(const std::string& parametername) const {
	return m_hbmparameters.at(parametername);
}

HbmParameter& Metadata::hbmParameter(const std::string& parametername) {
	return m_hbmparameters.at(parametername);
}

bool Metadata::hasHbmParameter(const std::string& parametername) const {
	return (m_hbmparameters.find(parametername) != m_hbmparameters.end());
}

bool Metadata::addHbmparameter(HbmParameter const& parameter) {
	std::pair<HbmParameterCont::iterator, bool> ret = m_hbmparameters.insert(std::make_pair(parameter.name(), parameter));	
    return ret.second;
	}
	
void Metadata::removeEntity(std::string entityName)
{
    m_entities.erase(entityName);
}

void Metadata::removeLandmark(std::string name)
{
	m_landmarks.erase(name);
}

void Metadata::removeGenericMetadata(std::string name)
{
	m_genmetadatas.erase(name);
}

void Metadata::removeJoint(std::string name)
{
	m_joints.erase(name);
}

void Metadata::removeInteractionControlPoint(std::string name)
{
	m_controlPoints.erase(name);
}

void Metadata::addLandmark(std::string name, std::vector<Id> elt_adr){

	std::string file = "LandmarksTest.xml";
	int groupNodeId = 1234;

	//WRITING ==============================================

	// build tinyxml document
	tinyxml2::XMLDocument modelr;

	modelr.InsertEndChild(modelr.NewDeclaration());
	tinyxml2::XMLElement* xmlPiper = modelr.NewElement("piper");
	std::ostringstream version;
	version << "0.1";
	xmlPiper->SetAttribute("version", version.str().c_str());
	modelr.InsertEndChild(xmlPiper);

	tinyxml2::XMLElement* landmarkR = modelr.NewElement("landmark");
	landmarkR->SetAttribute("type", "barycenter");
	xmlPiper->InsertEndChild(landmarkR);

	tinyxml2::XMLElement* nameXML = modelr.NewElement("name");
	nameXML->SetText(name.c_str());
	landmarkR->InsertEndChild(nameXML);

	if (elt_adr.size() > 0){
		tinyxml2::XMLElement* nodeXML = modelr.NewElement("node");
		std::stringstream values;
		int i = 0;
		for (auto it = elt_adr.begin(); it != elt_adr.end(); it++){
			values << elt_adr.at(i) << " ";
			i++;
		}
		nodeXML->SetText(values.str().c_str());
		landmarkR->InsertEndChild(nodeXML);
	}

	// write tinyxml document
	modelr.SaveFile(file.c_str());
	if (modelr.Error()) {
        std::cerr << "[FAILED] Save model file: " << file << std::endl << modelr.ErrorStr() << std::endl;
	}
	//WRITING ==============================================

	//READING ==============================================

	tinyxml2::XMLDocument model;
	model.LoadFile(file.c_str());
	if (model.Error()) {
		clear();
		std::stringstream str;
        str << "[FAILED] Load model file: " << file << std::endl << model.ErrorStr() << std::endl;
		throw std::runtime_error(str.str().c_str());
	}
	tinyxml2::XMLElement* element;
	m_landmarks.clear();
	element = model.FirstChildElement("piper")->FirstChildElement("landmark");
	while (element != nullptr) {
		Landmark l(element);
		std::pair<LandmarkCont::iterator, bool> ret = m_landmarks.insert(std::make_pair(l.name(), l));
		if (!ret.second)
			std::cerr << "[WARNING] Duplicated landmark ignored: " << l.name() << std::endl;
		element = element->NextSiblingElement("landmark");
	}
}

}
}
