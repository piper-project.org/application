/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "FEModel.h"


//#include "ModelWriter.h"
//#include "ModelReader.h"
#include "Metadata.h"
#include "ParserModelFile.h"
#include "types.h"
#include "IdRegistrator.h"

#include <stack>
#include <algorithm>


using namespace std;
namespace piper {
	namespace hbm {


		FEModel::~FEModel() {
			//clear();
		}

		void FEModel::DeepCopy(FEModel &source)
		{
			// ------------- private member --------------
			m_meshdefstate = &m_meshdefnid;

			m_isFEmodel = source.m_isFEmodel; // flag: true = femodel generated from FE file parsing

			m_map_restorenid.clear();
			m_map_restorenid.insert(source.m_map_restorenid.begin(), source.m_map_restorenid.end());

			/// all these have defined operator= that copies the data from the containers, so should be ok?
			m_gnodes = source.m_gnodes;
			m_gelem3d = source.m_gelem3d;
			m_gelem2d = source.m_gelem2d;
			m_gelem1d = source.m_gelem1d;
			m_ggroup = source.m_ggroup;
			m_nodes = source.m_nodes;
			m_elem3D = source.m_elem3D;
			m_elem2D = source.m_elem2D;
			m_elem1D = source.m_elem1D;
			m_frames = source.m_frames;
			m_parameters = source.m_parameters;

			// TJ - no idea what these "registrators" are and how they should work, this assignement is probably incorrect, please revise
			m_registorNode = source.m_registorNode;
			m_registorElement1D = source.m_registorElement1D;
			m_registorElement2D = source.m_registorElement2D;
			m_registorElement3D = source.m_registorElement3D;
			m_registorGroupNode = source.m_registorGroupNode;
			m_registorGroupE1D = source.m_registorGroupE1D;
			m_registorGroupE2D = source.m_registorGroupE2D;
			m_registorGroupE3D = source.m_registorGroupE3D;
			m_registorGroupGroup = source.m_registorGroupGroup;
			m_registorFrame = source.m_registorFrame;
			m_registorModelparameter = source.m_registorModelparameter;

			// re-create the vtk_representation
			vtk_representation = FEModelVTK(*this);
		}

		vector< Id> FEModel::getListNid() const {
			return m_meshdefstate->getListNid(this);
		}

		void FEModel::delNode(Id nid) {
			vector<Id> vnid;
			vnid.push_back(nid);
			delNode(vnid);
		}

		void FEModel::delNode(const vector<Id>& vnid) {
			m_meshdefstate->delNode(this, vnid);
			// update registrator for node
			for (Id const& id : vnid)
				m_registorNode.unregisteredId(id);
			vtk_representation.setMesh(*this); // reset VTK representation
		}

		bool FEModel::setMeshDef_NID() {
			if (!m_meshdefstate->getMeshDefState() == MeshDefType::NID) {
				convMeshDef(m_map_restorenid);
				m_meshdefstate = &m_meshdefnid;
				return true;
			}
			return false;
		}

		bool FEModel::setMeshDef_INDEX() {
			if (!m_meshdefstate->getMeshDefState() == MeshDefType::INDEX) 
            {
                auto& curNodes = *m_nodes.getActive();
                // in case some node was added or deleted since last call to this method, re-build the index-nid maps
                if (m_map_restorenid.size() != curNodes.size())
                {
                    m_map_restorenid.clear();
                    m_map_restoreindex.clear();
                    Id count = 0;
                    for (auto & node : curNodes)
                    {
                        Id nid = node->getId();
                        m_map_restoreindex[nid] = count;
                        m_map_restorenid[count] = nid;
                        count++;
                    }
                }
                convMeshDef(m_map_restoreindex);
				m_meshdefstate = &m_meshdefindex;
				return true;
			}
			return false;
		}

		void FEModel::setMeshDef_type(const MeshDefType meshdeftype) {
			if (!m_meshdefstate->getMeshDefState() == meshdeftype) {
				if (m_meshdefindex.getMeshDefState() == meshdeftype)
					setMeshDef_INDEX();
				else
					setMeshDef_NID();
			}
		}

		MeshDefType FEModel::getMeshDefType() const {
			return m_meshdefstate->getMeshDefState();
		}



		void FEModel::convMeshDef(unordered_map<Id, Id>& map_id) {
            // this ID conversion is quite slow for large models and this method is the bottleneck
            // -> the parallelization here pays off and should be save since each node-defined entity should have its own
            // vector of nodeIDs that define it -> no resource sharing
            #pragma omp parallel sections
            {
                #pragma omp section
                { m_gnodes.convId(map_id); }
                #pragma omp section
                { m_nodes.getActive()->convId(map_id); }
                #pragma omp section
                { m_elem1D.convId(map_id); }
                #pragma omp section
                {m_elem2D.convId(map_id); }
                #pragma omp section
                { m_elem3D.convId(map_id); }
                #pragma omp section
                { m_frames.convId(map_id); }
            }
		}

		void FEModel::clear() {
			m_nodes.clear();
			m_elem1D.clear();
			m_elem2D.clear();
			m_elem3D.clear();
			// 
			m_meshdefstate = &m_meshdefnid;
			m_map_restorenid.clear();
			m_gnodes.clear();
			m_gelem3d.clear();
			m_gelem2d.clear();
			m_gelem1d.clear();
			m_ggroup.clear();
			m_frames.clear();
			m_parameters.clear();
			// 
			m_registorNode.clear();
			m_registorElement1D.clear();
			m_registorElement2D.clear();
			m_registorElement3D.clear();
			m_registorFrame.clear();
			m_registorModelparameter.clear();
			m_registorGroupNode.clear();
			m_registorGroupE1D.clear();
			m_registorGroupE2D.clear();
			m_registorGroupE3D.clear();
			m_registorGroupGroup.clear();

			vtk_representation.clear();
		}

        void FEModel::reloadFEmesh(Metadata & metadata) {
            this->parseMesh(metadata, PARSER_OPTION::NODES_ONLY);

			//FEModel reloaded;
			//// update node coord in root history
			//Nodes const& nodereloaded = reloaded.getNodes();
			//string key;
			//IdKey id;
			//for (auto it = nodereloaded.begin(); it != nodereloaded.end(); ++it) {
			//    reloaded.getInfoId<Node>((*it)->getId(), key, id);
			//    this->get<Node>(key, id)->setCoord((*it)->get());
			//}
			//reloaded.clear();
		}

		void FEModel::readGeomMesh(Metadata& metadata) {
			vector<string> listfiles;
            parser::ParserModelFile* p = new parser::ParserModelFile();
            p->setMetadataRulePath(metadata.rulePath());

			getFiles(metadata.sourcePath(), ".obj", listfiles);
			if (listfiles.size() == 0) {
				stringstream str;
				str << "Failed to import geometric mesh file: " << endl;
				str << "No obj files are present in the directory : " << metadata.sourcePath() << endl;
				throw runtime_error(str.str().c_str());
			}
			else {
				for (auto const& cur : listfiles)
					this->readOBJ(cur);
				parser::ParserModelFile* p = new parser::ParserModelFile();
				p->setMetadataRulePath(metadata.rulePath());
				p->setFormatName("obj");
				this->fillMetadata(p, metadata);
                delete p;
			}
			vtk_representation = FEModelVTK(*this);
		}

		void FEModel::readOBJ(string const& filename) {
			ifstream infile;
			infile.open(filename);
			if (!infile.is_open()) {
				stringstream ss;
				ss << "Can not open the file: " << filename << endl;
				throw runtime_error(ss.str());
			}
			else {
				boost::filesystem::path p(filename);
				//use of the name file as a keyword for piper ids
				string key = p.stem().string();
				// create a group of E2D for this obj file,
				GroupElements2DPtr newgrp = make_shared<GroupElements2D>();
				//counter
				unsigned int countn = 1;
				unsigned int counte = 0;
				string line;
				while (getline(infile, line)) {
					if (line.empty()) continue;
					istringstream streamline(line);
					string token;
					//get first token
					streamline >> token;
					if (token == "v") {/* this is a new node */
						double x, y, z;
						streamline >> x >> y >> z;
						NodePtr nd = make_shared<Node>();
						nd->setCoord(x, y, z);
						this->set(nd, key, countn);
						countn++;
					}

					else if (token == "f") { /* new triangle */
						ElemDef elemdef;
						while (!streamline.eof()) {
							vector<int> v, vt, vn;
							string face;
							int value[3];
							streamline >> face;
							if (face.empty()) continue;
							for (int j = 0; j < 3; ++j) {
								value[j] = 0; //init
								string::size_type pos = face.find('/');
								string tmp = face.substr(0, pos);
								if (pos == string::npos)
									face = "";
								else
									face = face.substr(pos + 1);
								if (!tmp.empty())
									value[j] = atoi(tmp.c_str());
							}
							elemdef.push_back(value[0]);
						}
						// convert elemdef with piper node id
						for (auto & cur : elemdef)
							cur = this->get<Node>(key, cur)->getId();
						Element2DPtr e2d = make_shared<Element2D>();
						e2d->setElemDef(elemdef);
						this->set(e2d, key, counte);
						counte++;
						newgrp->addInGroup(e2d->getId());
					}
				}
				//add newgrp
				this->set(newgrp, "obj", IdKey(key));
			}
		}

        void FEModel::parseMesh(Metadata& metadata, PARSER_OPTION option) {
			// parse mesh file
			parser::ParserModelFile* p = new parser::ParserModelFile(metadata.sourceFormat(), metadata.rulePath());
            std::vector<std::string> includeFiles = metadata.sourceIncludeFiles();
            this->parse(p, metadata.sourcePath(), includeFiles, option);
			// fill metadata
			fillMetadata(p, metadata);
			delete p;
		}

		//void FEModel::parseMesh(string const& formatrulesFile, string const& modelrulesFile, PARSER_OPTION option) {
		//	// parse mesh file
		//	parser::ParserModelFile* p = new parser::ParserModelFile(formatrulesFile, modelrulesFile);
		//	this->parse(p, option);
		//	delete p;
		//}

        void FEModel::parse(parser::ParserModelFile* p, std::string const& sourceFile, std::vector<std::string> const& includeFiles, PARSER_OPTION option) 
        {
            p->doParsing(this, sourceFile, includeFiles, option);

			// init vtk representation
			if (option != PARSER_OPTION::NODES_ONLY) {//we have parsed sufficient info to generate a VTKrepresentation
				vtk_representation = FEModelVTK(*this);
			}
		}

		void FEModel::update(const Metadata& metadata, const string& targetdirectory) {
			vector<string> msg;
			update(metadata, targetdirectory, msg);
		}

		void FEModel::update(const Metadata& metadata, const string& targetdirectory, vector<string> const& msg) {
			// read rule file to get info for updating model files
			parser::ParserModelFile* p = new parser::ParserModelFile(metadata.sourceFormat(), metadata.rulePath());
			update(p, metadata.sourcePath(), metadata.sourceIncludeFiles(), targetdirectory, msg);
			delete p;
		}

        void FEModel::update(parser::ParserModelFile* p, const string& filename, std::vector<std::string> const& includeFiles, const string& targetdirectory) {
			vector<string> msg;
			update(p, filename, includeFiles, targetdirectory, msg);
		}

        void FEModel::update(parser::ParserModelFile* p, const string& filename, std::vector<std::string> const& includeFiles,
            const string& targetdirectory, vector<string> const& msg) {
			p->doUpdating(this, filename, includeFiles, targetdirectory, msg);
		}


		void FEModel::fillMetadata(parser::ParserModelFile* p, Metadata& metadata) {
			p->fillMetadata(this, metadata);
		}

		void FEModel::normalizedFrame(units::Length const& unit) {
			// look for free node used for frames 
			VId freenodevid = freenodes();
			double norm = units::convert(1e-3, units::Length::m, unit); // normalize to a small value (1e-3m)
			// find frame defined by freenodes
			for (Frames::const_iterator it = m_frames.begin(); it != m_frames.end(); ++it) {
				if (!(*it)->hasOnlyOrigin()) {
					Id id = (*it)->getFirstDirectionId();
					//Id curid=it->first;
					//IdFormat idf=Frame::getIdFormat( curid);
					//cout << "Frame  : " << curid << " - " << idf.m_id().m_id() << endl;
					if (find(freenodevid.begin(), freenodevid.end(), id) != freenodevid.end()) {
						Coord org = m_nodes.getActive()->getEntity((*it)->getOriginId())->get();
						Coord coord = m_nodes.getActive()->getEntity(id)->get();
						Coord newcoord = m_frames.curDef(it)->normFirstDirection(org, coord, norm);
						m_nodes.getActive()->getEntity(id)->setCoord(newcoord);
						//debug cout
						//cout << " free node : " << Node::getIdFormat( id).m_id().m_id() << " in 1 direction" <<endl;
						//coord[0] << "," << coord[1] << "," <<coord[2] << " - " << newcoord[0] << "," << newcoord[1] << "," <<newcoord[2] <<endl;
					}
					id = (*it)->getPlaneId();
					if (find(freenodevid.begin(), freenodevid.end(), id) != freenodevid.end()) {
						Coord org = m_nodes.getActive()->getEntity((*it)->getOriginId())->get();
						Coord coord = m_nodes.getActive()->getEntity(id)->get();
						Coord newcoord = m_frames.curDef(it)->normSecondDirection(org, coord, norm);
						m_nodes.getActive()->getEntity(id)->setCoord(newcoord);
						//debug cout
						//cout << " free node : " << Node::getIdFormat( id).m_id().m_id() << " in 2 direction" <<endl;
						//coord[0] << "," << coord[1] << "," <<coord[2] << " - " << newcoord[0] << "," << newcoord[1] << "," <<newcoord[2] <<endl;
					}
				}
			}
		}

		void FEModel::restoreFrame() {
			for (Frames::const_iterator it = m_frames.begin(); it != m_frames.end(); ++it) {
				if ((*it)->isNormalizedFirstDirection()) {
					Coord org = m_nodes.getActive()->getEntity((*it)->getOriginId())->get();
					Coord coord = m_nodes.getActive()->getEntity((*it)->getFirstDirectionId())->get();
					double normd = (*it)->getNormFirstDirection();
					Eigen::Vector3d v1 = coord - org;
					v1.normalize();
					Coord newcoord = org + v1*normd;
					m_nodes.getActive()->getEntity((*it)->getFirstDirectionId())->setCoord(newcoord);
				}
				if ((*it)->isNormalizedSecondDirection()) {
					Coord org = m_nodes.getActive()->getEntity((*it)->getOriginId())->get();
					Coord coord = m_nodes.getActive()->getEntity((*it)->getPlaneId())->get();
					double normd = (*it)->getNormSecondDirection();
					Eigen::Vector3d v1 = coord - org;
					v1.normalize();
					Coord newcoord = org + v1*normd;
					m_nodes.getActive()->getEntity((*it)->getPlaneId())->setCoord(newcoord);
				}
			}
		}


		template<>
		void FEModel::set<Node>(NodePtr node) {
			vector<NodePtr> vnode;
			vnode.push_back(node);
			m_meshdefstate->setNode(this, vnode);
		};

		template< >
		void FEModel::set<Element1D>(Element1DPtr element) {
			setComponent(&m_elem1D, element);
		}

		template< >
		void FEModel::set<Element2D>(Element2DPtr element) {
			setComponent(&m_elem2D, element);
		}

		template< >
		void FEModel::set<Element3D>(Element3DPtr element) {
			setComponent(&m_elem3D, element);
		}

		template< >
		void FEModel::set<FEFrame>( FramePtr element) {
			setComponent(&m_frames, element);
		}


		template< >
		void FEModel::set<GroupNode>(GroupNodePtr gnode) {
			setComponent(&m_gnodes, gnode);
		}

		template< >
		void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem) {
			setComponent(&m_gelem1d, gelem);
		}

		template< >
		void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem) {
			setComponent(&m_gelem2d, gelem);
		}

		template< >
		void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem) {
			setComponent(&m_gelem3d, gelem);
		}

		template< >
		void FEModel::set<GroupGroups>(GroupGroupsPtr ggroup) {
			setComponent(&m_ggroup, ggroup);
		}

		template< >
		void FEModel::set<FEModelParameter>(FEModelParameterPtr param) {
            setComponent(m_parameters.getActive().get(), param);
		}


		template<>
		void FEModel::set<Node>(NodePtr node, const string& key, const IdKey& id) {
			m_registorNode.registerId(node, key, id);
			set(node);
		}

		template<>
		void FEModel::set<Element3D>(Element3DPtr elem3D, const string& key, const IdKey& id) {
			m_registorElement3D.registerId(elem3D, key, id);
			set(elem3D);
		}

		template<>
		void FEModel::set<Element2D>(Element2DPtr elem2D, const string& key, const IdKey& id) {
			m_registorElement2D.registerId(elem2D, key, id);
			set(elem2D);
		}

		template<>
		void FEModel::set<Element1D>(Element1DPtr elem1D, const string& key, const IdKey& id) {
			m_registorElement1D.registerId(elem1D, key, id);
			set(elem1D);
		}

		template<>
        void FEModel::set<FEFrame>(FramePtr frame, const string& key, const IdKey& id) {
			m_registorFrame.registerId(frame, key, id);
			set(frame);
		}

		template<>
		void FEModel::set<GroupNode>(GroupNodePtr gnode, const string& key, const IdKey& id) {
			m_registorGroupNode.registerId(gnode, key, id);
			set(gnode);
		}

		template<>
		void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem, const string& key, const IdKey& id) {
			m_registorGroupE1D.registerId(gelem, key, id);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem, const string& key, const IdKey& id) {
			m_registorGroupE2D.registerId(gelem, key, id);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem, const string& key, const IdKey& id) {
			m_registorGroupE3D.registerId(gelem, key, id);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupGroups>(GroupGroupsPtr gelem, const string& key, const IdKey& id) {
			m_registorGroupGroup.registerId(gelem, key, id);
			set(gelem);
		}

		template<>
		void FEModel::set<FEModelParameter>(FEModelParameterPtr param, const string& key, const IdKey& id) {
			m_registorModelparameter.registerId(param, key, id);
			set(param);
		}

		template<>
		void FEModel::set<Node>(NodePtr node, const string& key) {
			m_registorNode.registerId(node, key);
			set(node);
		};

		template<>
		void FEModel::set<Element3D>(Element3DPtr elem3D, const string& key) {
			m_registorElement3D.registerId(elem3D, key);
			set(elem3D);
		};

		template<>
		void FEModel::set<Element2D>(Element2DPtr elem2D, const string& key) {
			m_registorElement2D.registerId(elem2D, key);
			set(elem2D);
		};

		template<>
		void FEModel::set<Element1D>(Element1DPtr elem1D, const string& key) {
			m_registorElement1D.registerId(elem1D, key);
			set(elem1D);
		};

		template<>
		void FEModel::set<FEFrame>(FramePtr frame, const string& key) {
			m_registorFrame.registerId(frame, key);
			set(frame);
		}

		template<>
		void FEModel::set<GroupNode>(GroupNodePtr gnode, const string& key) {
			m_registorGroupNode.registerId(gnode, key);
			set(gnode);
		}

		template<>
		void FEModel::set<GroupElements1D>(GroupElements1DPtr gelem, const string& key) {
			m_registorGroupE1D.registerId(gelem, key);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupElements2D>(GroupElements2DPtr gelem, const string& key) {
			m_registorGroupE2D.registerId(gelem, key);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupElements3D>(GroupElements3DPtr gelem, const string& key) {
			m_registorGroupE3D.registerId(gelem, key);
			set(gelem);
		}

		template<>
		void FEModel::set<GroupGroups>(GroupGroupsPtr gelem, const string& key) {
			m_registorGroupGroup.registerId(gelem, key);
			set(gelem);
		}

		template<>
		void FEModel::set<FEModelParameter>(FEModelParameterPtr param, const string& key) {
			m_registorModelparameter.registerId(param, key);
			set(param);
		}

		template<>
		string FEModel::infoId<Node>(const Id& id) const{
			return m_registorNode.infoId(id);
		};

		template< >
		string FEModel::infoId<Element1D>(const Id& id) const{
			return m_registorElement1D.infoId(id);
		}

		template< >
		string FEModel::infoId<Element2D>(const Id& id) const{
			return m_registorElement2D.infoId(id);
		}

		template< >
		string FEModel::infoId<Element3D>(const Id& id) const{
			return m_registorElement3D.infoId(id);
		}

		template< >
		string FEModel::infoId<FEFrame>(const Id& id) const{
			return m_registorFrame.infoId(id);
		}


		template< >
		string FEModel::infoId<GroupNode>(const Id& id) const{
			return m_registorGroupNode.infoId(id);
		}

		template< >
		string FEModel::infoId<GroupElements1D>(const Id& id) const{
			return m_registorGroupE1D.infoId(id);
		}

		template< >
		string FEModel::infoId<GroupElements2D>(const Id& id) const{
			return m_registorGroupE2D.infoId(id);
		}

		template< >
		string FEModel::infoId<GroupElements3D>(const Id& id) const{
			return m_registorGroupE3D.infoId(id);
		}

		template< >
		string FEModel::infoId<GroupGroups>(const Id& id) const{
			return m_registorGroupGroup.infoId(id);
		}

		template< >
		string FEModel::infoId<FEModelParameter>(const Id& id) const{
			return m_registorModelparameter.infoId(id);
		}

		template<>
		void FEModel::getInfoId<Node>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorNode.getId_FE(idpiper, key, id);
		}
		template<>
		void FEModel::getInfoId<Node>(const VId& idpiper, std::vector<std::string>& key, std::vector<IdKey>& id) const{
			m_registorNode.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<Element1D>(const Id& idpiper, string& key, IdKey& id) const{
			return m_registorElement1D.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<Element2D>(const Id& idpiper, string& key, IdKey& id) const{
			return m_registorElement2D.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<Element3D>(const Id& idpiper, string& key, IdKey& id) const{
			return m_registorElement3D.getId_FE(idpiper, key, id);
		}
		template< >
                void FEModel::getInfoId<FEFrame>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorFrame.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<GroupNode>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorGroupNode.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<GroupElements1D>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorGroupE1D.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<GroupElements2D>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorGroupE2D.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<GroupElements3D>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorGroupE3D.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<GroupGroups>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorGroupGroup.getId_FE(idpiper, key, id);
		}
		template< >
		void FEModel::getInfoId<FEModelParameter>(const Id& idpiper, string& key, IdKey& id) const{
			m_registorModelparameter.getId_FE(idpiper, key, id);
		}
		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Node>() const {
			return m_registorNode.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element1D>() const {
			return m_registorElement1D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element2D>() const {
			return m_registorElement2D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<Element3D>() const {
			return m_registorElement3D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupNode>() const {
			return m_registorGroupNode.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements1D>() const {
			return m_registorGroupE1D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements2D>() const {
			return m_registorGroupE2D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupElements3D>() const {
			return m_registorGroupE3D.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<GroupGroups>() const {
			return m_registorGroupGroup.computePiperIdToIdKeyMap();
		}

		template< >
		std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<FEModelParameter>() const {
			return m_registorModelparameter.computePiperIdToIdKeyMap();
		}


		template< >
                std::map<hbm::Id, std::pair<std::string, IdKey>> const& FEModel::computePiperIdToIdKeyMap<FEFrame>() const {
			return m_registorFrame.computePiperIdToIdKeyMap();
		}

		template<>
		void FEModel::deleteComponent<Node>(const Id& id) {
			delNode(id);
			m_registorNode.unregisteredId(id);
		};


		template<>
		void FEModel::deleteComponents<Node>(const std::vector<Id>& vid) {
			delNode(vid);
			m_registorNode.unregisteredId(vid);
		}

		template< >
		void FEModel::deleteComponent<Element1D>(const Id& id) {
			m_elem1D.delEntity(id);
			m_registorElement1D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<Element2D>(const Id& id) {
			m_elem2D.delEntity(id);
			m_registorElement2D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<Element3D>(const Id& id) {
			m_elem3D.delEntity(id);
			m_registorElement3D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<FEFrame>(const Id& id) {
			m_frames.delEntity(id);
			m_registorFrame.unregisteredId(id);
		}


		template< >
		void FEModel::deleteComponent<GroupNode>(const Id& id) {
			m_gnodes.delEntity(id);
			m_registorGroupNode.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<GroupElements1D>(const Id& id) {
			m_gelem1d.delEntity(id);
			m_registorGroupE1D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<GroupElements2D>(const Id& id) {
			m_gelem2d.delEntity(id);
			m_registorGroupE2D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<GroupElements3D>(const Id& id) {
			m_gelem3d.delEntity(id);
			m_registorGroupE3D.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<GroupGroups>(const Id& id) {
			m_ggroup.delEntity(id);
			m_registorGroupGroup.unregisteredId(id);
		}

		template< >
		void FEModel::deleteComponent<FEModelParameter>(const Id& id) {
            m_parameters.getActive()->delEntity(id);
			m_registorModelparameter.unregisteredId(id);
		}

		template< >
		void FEModel::deleteAllComponents<Element2D>() {
			m_elem2D.clear();
			m_registorElement2D.clear();
		}

		template< >
		void FEModel::deleteAllComponents<Element3D>() {
			m_elem3D.clear();
			m_registorElement3D.clear();
		}

		template< >
		void FEModel::deleteAllComponents<GroupElements3D>() {
			m_gelem3d.clear();
			m_registorGroupE3D.clear();
		}

		FEFrame const& FEModel::getFrame(const Id& id) const {
			return *m_frames.getEntity(id);
		}

		GroupNode const& FEModel::getGroupNode(const Id& id) const {
			return *m_gnodes.getEntity(id);
		}

		GroupElements1D const& FEModel::getGroupElements1D(const Id& id) const {
			return *m_gelem1d.getEntity(id);
		}

		GroupElements2D const& FEModel::getGroupElements2D(const Id& id) const {
			return *m_gelem2d.getEntity(id);
		}

		GroupElements2D& FEModel::getGroupElements2D(const Id& id) {
			return *m_gelem2d.getEntity(id);
		}

		GroupElements3D const& FEModel::getGroupElements3D(const Id& id) const {
			return *m_gelem3d.getEntity(id);
		}

		GroupGroups const& FEModel::getGroupGroups(const Id& id) const {
			return *m_ggroup.getEntity(id);
		}

        void FEModel::appendParametersValue(const Id& id, string const& name, vector<double>const& value) {
            m_parameters.getActive()->appendValue(id, name, value);
		}

        void FEModel::appendParametersValue(const Id& id, std::string const& name, std::vector<unsigned int>const& value) {
            std::vector<double> doubleValue(value.begin(), value.end());
            m_parameters.getActive()->appendValue(id, name, doubleValue);
        }


        void FEModel::setParametersValue(const Id& id, string const& name, vector<double>const& value) {
            m_parameters.getActive()->setValue(id, name, value);
        }

		FEModelParameter const& FEModel::getFEModelParameter(const Id& id) const {
            return *m_parameters.getActive()->getEntity(id);
		}

		Node const& FEModel::getNode(const Id& id) const {
			return *m_nodes.getActive()->getEntity(id);
		}
		Node& FEModel::getNode(const Id& id){
			return *m_nodes.getActive()->getEntity(id);
		}

		bool FEModel::hasNode(const Id& id) const{
			return (m_nodes.getActive()->getEntity(id) != NULL);
		}

		void FEModel::updateVTKRepresentation()
		{
			vtk_representation.setPoints(*this); // propagate the changes to the vtk_rep
		}


		Element1D const& FEModel::getElement1D(const Id& id) const {
			return *m_elem1D.getEntity(id);
		}

		Element2D const& FEModel::getElement2D(const Id& id) const {
			return *m_elem2D.getEntity(id);
		}


		Element3D const& FEModel::getElement3D(const Id& id) const {
			return *m_elem3D.getEntity(id);
		}


		template<>
		NodePtr FEModel::get<Node>(const Id& id) {
			return m_nodes.getActive()->getEntity(id);
		};

		template<>
		const NodePtr FEModel::get<Node>(const Id& id) const{
			return m_nodes.getActive()->getEntity(id);
		}

		template< >
		Element1DPtr FEModel::get<Element1D>(const Id& id) {
			return m_elem1D.getEntity(id);
		}

		template< >
		const Element1DPtr FEModel::get<Element1D>(const Id& id) const {
			return m_elem1D.getEntity(id);
		}

		template< >
		Element2DPtr FEModel::get<Element2D>(const Id& id) {
			return m_elem2D.getEntity(id);
		}


		template< >
		const Element2DPtr FEModel::get<Element2D>(const Id& id) const {
			return m_elem2D.getEntity(id);
		}

		template< >
		Element3DPtr FEModel::get<Element3D>(const Id& id) {
			return m_elem3D.getEntity(id);
		}

		template< >
		const Element3DPtr FEModel::get<Element3D>(const Id& id) const {
			return m_elem3D.getEntity(id);
		}



		template< >
                FramePtr FEModel::get<FEFrame>(const Id& id) {
			return m_frames.getEntity(id);
		}


		template< >
                const FramePtr FEModel::get<FEFrame>(const Id& id) const {
			return m_frames.getEntity(id);
		}



		Id FEModel::getMaxNodeId(){
			return m_nodes.getActive()->getMaxId();
		}

		template< >
		GroupNodePtr FEModel::get<GroupNode>(const Id& id) {
			return m_gnodes.getEntity(id);
		}



		template< >
		const GroupNodePtr FEModel::get<GroupNode>(const Id& id) const {
			return m_gnodes.getEntity(id);
		}


		template< >
		GroupElements1DPtr FEModel::get<GroupElements1D>(const Id& id) {
			return m_gelem1d.getEntity(id);
		}

		template< >
		const GroupElements1DPtr FEModel::get<GroupElements1D>(const Id& id) const {
			return m_gelem1d.getEntity(id);
		}

		template< >
		GroupElements2DPtr FEModel::get<GroupElements2D>(const Id& id) {
			return m_gelem2d.getEntity(id);
		}

		template< >
		const GroupElements2DPtr FEModel::get<GroupElements2D>(const Id& id) const {
			return m_gelem2d.getEntity(id);
		}


		template< >
		GroupElements3DPtr FEModel::get<GroupElements3D>(const Id& id) {
			return m_gelem3d.getEntity(id);
		}

		template< >
		const GroupElements3DPtr FEModel::get<GroupElements3D>(const Id& id) const {
			return m_gelem3d.getEntity(id);
		}

		template< >
		GroupGroupsPtr FEModel::get<GroupGroups>(const Id& id) {
			return m_ggroup.getEntity(id);
		}


		template< >
		const GroupGroupsPtr FEModel::get<GroupGroups>(const Id& id) const {
			return m_ggroup.getEntity(id);
		}

		template< >
		FEModelParameterPtr FEModel::get<FEModelParameter>(const Id& id) {
            return m_parameters.getActive()->getEntity(id);
		}

		template< >
		const FEModelParameterPtr FEModel::get<FEModelParameter>(const Id& id) const {
            return m_parameters.getActive()->getEntity(id);
		}

		void FEModel::writeModel(const string& filemesh) {
			// write mesh
			//setMeshDef_INDEX();
			//modelwriter->write( filemesh, this);
			//setMeshDef_NID();

			// update the vtk representation before using it to save the model. this is not ideal, as in many cases it will be redundant (when vtk rep is up to date)
			// also theoretically the vtk rep might be ahead causing this to discard changes (but in that case updateFEModel should be called)
			// it would be better if the modules that do some changes to nodes explicitly called updateVTKRepresentation from their own code
			updateVTKRepresentation();
			vtk_representation.write(filemesh);

		}

		void FEModel::writeModelMetadata(const string& filemetadata) {
			tinyxml2::XMLDocument model;
			model.LoadFile(filemetadata.c_str());
			tinyxml2::XMLElement* xmlPiper = model.FirstChildElement("piper");

			tinyxml2::XMLElement* element;

			// group nodes
			for (ContGroupNodes::const_iterator it = m_gnodes.begin(); it != m_gnodes.end(); ++it) {
				element = model.NewElement("GroupNode");
				m_gnodes.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}
			// group e3D
			for (ContGroupElements3D::const_iterator it = m_gelem3d.begin(); it != m_gelem3d.end(); ++it) {
				element = model.NewElement("GroupElements3D");
				m_gelem3d.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}
			// group e2D
			for (ContGroupElements2D::const_iterator it = m_gelem2d.begin(); it != m_gelem2d.end(); ++it) {
				element = model.NewElement("GroupElements2D");
				m_gelem2d.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}
			// group e1D
			for (ContGroupElements1D::const_iterator it = m_gelem1d.begin(); it != m_gelem1d.end(); ++it) {
				element = model.NewElement("GroupElements1D");
				m_gelem1d.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}
			// group groups
			for (ContGroupGroups::const_iterator it = m_ggroup.begin(); it != m_ggroup.end(); ++it) {
				element = model.NewElement("GroupGroups");
				m_ggroup.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}

			// parameters
            for (FEModelParameters::const_iterator it = m_parameters.getActive()->begin(); it != m_parameters.getActive()->end(); ++it) {
				element = model.NewElement("ModelParameters");
                m_parameters.getActive()->curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}

			// frames
			for (Frames::const_iterator it = m_frames.begin(); it != m_frames.end(); ++it) {
				element = model.NewElement("Frames");
				m_frames.curDef(it)->serializeXml(element);
				xmlPiper->InsertEndChild(element);
			}

			// id conversion node
			element = model.NewElement("conversionIdNode");
			m_registorNode.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion e3D
			element = model.NewElement("conversionIdElement3D");
			m_registorElement3D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion e2D
			element = model.NewElement("conversionIdElement2D");
			m_registorElement2D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion e1D
			element = model.NewElement("conversionIdElement1D");
			m_registorElement1D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion group
			element = model.NewElement("conversionIdGroupNode");
			m_registorGroupNode.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion group
			element = model.NewElement("conversionIdGroupE1D");
			m_registorGroupE1D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion group
			element = model.NewElement("conversionIdGroupE2D");
			m_registorGroupE2D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion group
			element = model.NewElement("conversionIdGroupE3D");
			m_registorGroupE3D.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id conversion group
			element = model.NewElement("conversionIdGroupGroup");
			m_registorGroupGroup.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id parameters
			element = model.NewElement("conversionIdParameters");
			m_registorModelparameter.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			// id frames
			element = model.NewElement("conversionIdFrames");
			m_registorFrame.serializeIdXml(element);
			xmlPiper->InsertEndChild(element);
			//
			model.SaveFile(filemetadata.c_str());

		}

		void FEModel::readModel(const string& file) {
			// clear data structure before reading
			//clear();
			//modelreader->read( file, this);

			vtk_representation = FEModelVTK(); // reiniti vtkRep
			vtk_representation.read(file);
			vtk_representation.generateFEmodelNodes(*this);
			vtk_representation.generateFEmodelElements(*this);
		}

		void FEModel::readModelMetadata(const string& filemetadata) {
			m_isFEmodel = true;
			tinyxml2::XMLDocument model;
			model.LoadFile(filemetadata.c_str());
			if (model.Error()) {
				clear();
				std::stringstream str;
                str << "Failed to load file: " << filemetadata << std::endl << model.ErrorStr() << std::endl;
				throw std::runtime_error(str.str().c_str());
			}


			tinyxml2::XMLElement* element;

			//GroupNode
			element = model.FirstChildElement("piper")->FirstChildElement("GroupNode");
			while (element != nullptr) {
				GroupNodePtr g = make_shared<GroupNode>(element);
				m_gnodes.setEntity(g);
				element = element->NextSiblingElement("GroupNode");
			}
			//GroupElements3D
			element = model.FirstChildElement("piper")->FirstChildElement("GroupElements3D");
			while (element != nullptr) {
				GroupElements3DPtr g = make_shared<GroupElements3D>(element);
				m_gelem3d.setEntity(g);
				element = element->NextSiblingElement("GroupElements3D");
			}
			//GroupElements2D
			element = model.FirstChildElement("piper")->FirstChildElement("GroupElements2D");
			while (element != nullptr) {
				GroupElements2DPtr g = make_shared<GroupElements2D>(element);
				m_gelem2d.setEntity(g);
				element = element->NextSiblingElement("GroupElements2D");
			}
			//GroupElements1D
			element = model.FirstChildElement("piper")->FirstChildElement("GroupElements1D");
			while (element != nullptr) {
				GroupElements1DPtr g = make_shared<GroupElements1D>(element);
				m_gelem1d.setEntity(g);
				element = element->NextSiblingElement("GroupElements1D");
			}
			//GroupGroups
			element = model.FirstChildElement("piper")->FirstChildElement("GroupGroups");
			while (element != nullptr) {
				GroupGroupsPtr g = make_shared<GroupGroups>(element);
				m_ggroup.setEntity(g);
				element = element->NextSiblingElement("GroupGroups");
			}
			//parameters
			element = model.FirstChildElement("piper")->FirstChildElement("ModelParameters");
			while (element != nullptr) {
				FEModelParameterPtr p = make_shared<FEModelParameter>(element);
                m_parameters.getActive()->setEntity(p);
				element = element->NextSiblingElement("ModelParameters");
			}

			//frames
			element = model.FirstChildElement("piper")->FirstChildElement("Frames");
			while (element != nullptr) {
                        FramePtr p = make_shared<FEFrame>(element);
				m_frames.setEntity(p);
				element = element->NextSiblingElement("Frames");
			}

			// id conversion node
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdNode");
			if (element != nullptr) {
				parseIdXml(element, m_registorNode, this);
			}

			// id conversion e3D
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdElement3D");
			if (element != nullptr) {
				parseIdXml(element, m_registorElement3D, this);
			}

			// id conversion e2D
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdElement2D");
			if (element != nullptr) {
				parseIdXml(element, m_registorElement2D, this);
			}

			// id conversion e1D
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdElement1D");
			if (element != nullptr) {
				parseIdXml(element, m_registorElement1D, this);
			}

			// id conversion group
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdGroupNode");
			if (element != nullptr) {
				parseIdXml(element, m_registorGroupNode, this);
			}

			// id conversion group
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdGroupE1D");
			if (element != nullptr) {
				parseIdXml(element, m_registorGroupE1D, this);
			}

			// id conversion group
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdGroupE2D");
			if (element != nullptr) {
				parseIdXml(element, m_registorGroupE2D, this);
			}

			// id conversion group
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdGroupE3D");
			if (element != nullptr) {
				parseIdXml(element, m_registorGroupE3D, this);
			}
			// id conversion group
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdGroupGroup");
			if (element != nullptr) {
				parseIdXml(element, m_registorGroupGroup, this);
			}

			// id conversion frames
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdFrames");
			if (element != nullptr) {
				parseIdXml(element, m_registorFrame, this);
			}

			// id conversion parameters
			element = model.FirstChildElement("piper")->FirstChildElement("conversionIdParameters");
			if (element != nullptr) {
				parseIdXml(element, m_registorModelparameter, this);
			}

		}

		Eigen::Quaternion<double> FEModel::getFrameOrientation(const Id& id) const {
			Eigen::Quaternion<double> q(1.0, 0.0, 0.0, 0.0);
 			const FEFrame& fr= getFrame( id);
			if (fr.isCompletelyDefined()) {
				Coord o = getNode(fr.getOriginId()).get();
				Coord f = getNode(fr.getFirstDirectionId()).get();
				Coord p = getNode(fr.getPlaneId()).get();
				fr.getQuatOrientation(o, f, p, fr.getFirstDirection(), fr.getSecondDirection(), q);
			}
			return q;
		}

		const Coord& FEModel::getFrameOrigin(const Id& id) const {
			return getNode(getFrame(id).getOriginId()).get();
		}


		void FEModel::cleanEmptyGroups() {
			vector<Id> idgrouptoerase, totalidgroup;
			//group Node
			for (auto itg = m_gnodes.begin(); itg != m_gnodes.end(); ++itg) {
				VId& vid = m_gnodes.curDef(itg)->get();
				sort(vid.begin(), vid.end());
				VId::iterator it = unique(vid.begin(), vid.end());
				vid.resize(distance(vid.begin(), it));
			}
			m_gnodes.delEmpty(idgrouptoerase);
			m_registorGroupNode.unregisteredId(idgrouptoerase);
			totalidgroup.insert(totalidgroup.end(), idgrouptoerase.begin(), idgrouptoerase.end());
			idgrouptoerase.clear();
			//group E1D
			for (auto itg = m_gelem1d.begin(); itg != m_gelem1d.end(); ++itg) {
				VId& vid = m_gelem1d.curDef(itg)->get();
				sort(vid.begin(), vid.end());
				VId::iterator it = unique(vid.begin(), vid.end());
				vid.resize(distance(vid.begin(), it));
			}
			m_gelem1d.delEmpty(idgrouptoerase);
			m_registorGroupE1D.unregisteredId(idgrouptoerase);
			totalidgroup.insert(totalidgroup.end(), idgrouptoerase.begin(), idgrouptoerase.end());
			idgrouptoerase.clear();
			//group E2D
			for (auto itg = m_gelem2d.begin(); itg != m_gelem2d.end(); ++itg) {
				VId& vid = m_gelem2d.curDef(itg)->get();
				sort(vid.begin(), vid.end());
				VId::iterator it = unique(vid.begin(), vid.end());
				vid.resize(distance(vid.begin(), it));
			}
			m_gelem2d.delEmpty(idgrouptoerase);
			m_registorGroupE2D.unregisteredId(idgrouptoerase);
			totalidgroup.insert(totalidgroup.end(), idgrouptoerase.begin(), idgrouptoerase.end());
			idgrouptoerase.clear();
			//group E3D
			for (auto itg = m_gelem3d.begin(); itg != m_gelem3d.end(); ++itg) {
				VId& vid = m_gelem3d.curDef(itg)->get();
				sort(vid.begin(), vid.end());
				VId::iterator it = unique(vid.begin(), vid.end());
				vid.resize(distance(vid.begin(), it));
			}
			m_gelem3d.delEmpty(idgrouptoerase);
			m_registorGroupE3D.unregisteredId(idgrouptoerase);
			totalidgroup.insert(totalidgroup.end(), idgrouptoerase.begin(), idgrouptoerase.end());
			idgrouptoerase.clear();
			// upadate group of groups
			sort(totalidgroup.begin(), totalidgroup.end());
			ContGroupGroups::iterator itg;
			for (itg = m_ggroup.begin(); itg != m_ggroup.end(); ++itg) {
				VId& vid = m_ggroup.curDef(itg)->get();
				VId newvid;
				sort(vid.begin(), vid.end());
				VId::iterator it = unique(vid.begin(), vid.end());
				vid.resize(distance(vid.begin(), it));
				set_difference(vid.begin(), vid.end(), totalidgroup.begin(), totalidgroup.end(), back_inserter(newvid));
				m_ggroup.curDef(itg)->set(newvid);
			}
		}

		VId FEModel::freenodes() const {
			VId freenodevid;
			VId nodevid = m_nodes.getActive()->listId();
			VId elemnodevid;
			for (Elements1D::const_iterator it = m_elem1D.begin(); it != m_elem1D.end(); ++it) {
				copy(m_elem1D.curDef(it)->get().begin(), m_elem1D.curDef(it)->get().end(), back_inserter(elemnodevid));
			}
			for (Elements2D::const_iterator it = m_elem2D.begin(); it != m_elem2D.end(); ++it) {
				copy(m_elem2D.curDef(it)->get().begin(), m_elem2D.curDef(it)->get().end(), back_inserter(elemnodevid));
			}
			for (Elements3D::const_iterator it = m_elem3D.begin(); it != m_elem3D.end(); ++it) {
				copy(m_elem3D.curDef(it)->get().begin(), m_elem3D.curDef(it)->get().end(), back_inserter(elemnodevid));
			}
			VId::iterator it;
			sort(nodevid.begin(), nodevid.end());
			it = unique(nodevid.begin(), nodevid.end());
			nodevid.resize(distance(nodevid.begin(), it));
			//
			sort(elemnodevid.begin(), elemnodevid.end());
			it = unique(elemnodevid.begin(), elemnodevid.end());
			elemnodevid.resize(distance(elemnodevid.begin(), it));
			set_difference(nodevid.begin(), nodevid.end(), elemnodevid.begin(), elemnodevid.end(), back_inserter(freenodevid));
			//
			return freenodevid;
		}


		void FEModel::setScale(double x, double y, double z)
		{
			t_scale[0] = x;
			t_scale[1] = y;
			t_scale[2] = z;
		}

		void FEModel::setTranslation(double x, double y, double z)
		{
			t_translation[0] = x;
			t_translation[1] = y;
			t_translation[2] = z;
		}

		void FEModel::setRotation(double x, double y, double z)
		{
			t_rotation[0] = x;
			t_rotation[1] = y;
			t_rotation[2] = z;
		}

		const Coord &FEModel::getScale() const
		{
			return t_scale;
		}

		const Coord &FEModel::getTranslation() const
		{
			return t_translation;
		}

		const Coord &FEModel::getRotation() const
		{
			return t_rotation;
		}

		template<>
		NodePtr FEModel::get<Node>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorNode.getIdfromIdFormat(idpiper, key, id))
				return get<Node>(idpiper);
			else return nullptr;
		}

		template<>
		const NodePtr FEModel::get<Node>(const string& key, const IdKey& id) const{
			Id idpiper;
			if (m_registorNode.getIdfromIdFormat(idpiper, key, id))
				return get<Node>(idpiper);
			else return nullptr;
		}

		template< >
		Element1DPtr FEModel::get<Element1D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorElement1D.getIdfromIdFormat(idpiper, key, id))
				return get<Element1D>(idpiper);
			else return nullptr;
		}

		template< >
		const Element1DPtr FEModel::get<Element1D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorElement1D.getIdfromIdFormat(idpiper, key, id))
				return get<Element1D>(idpiper);
			else return nullptr;
		}

		template< >
		Element2DPtr FEModel::get<Element2D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorElement2D.getIdfromIdFormat(idpiper, key, id))
				return get<Element2D>(idpiper);
			else return nullptr;
		}


		template< >
		const Element2DPtr FEModel::get<Element2D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorElement2D.getIdfromIdFormat(idpiper, key, id))
				return get<Element2D>(idpiper);
			else return nullptr;
		}

		template< >
		Element3DPtr FEModel::get<Element3D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorElement3D.getIdfromIdFormat(idpiper, key, id))
				return get<Element3D>(idpiper);
			else return nullptr;
		}

		template< >
		const Element3DPtr FEModel::get<Element3D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorElement3D.getIdfromIdFormat(idpiper, key, id))
				return get<Element3D>(idpiper);
			else return nullptr;
		}

		template< >
		FramePtr FEModel::get<FEFrame>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorFrame.getIdfromIdFormat(idpiper, key, id))
                             return get<FEFrame>(idpiper);
			else return nullptr;
		}


		template< >
		const FramePtr FEModel::get<FEFrame>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorFrame.getIdfromIdFormat(idpiper, key, id))
                            return get<FEFrame>(idpiper);
			else return nullptr;
		}

		template< >
		GroupPtr FEModel::get<Group>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupNode.getIdfromIdFormat(idpiper, key, id))
				return get<GroupNode>(idpiper);
			else if (m_registorGroupE1D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements1D>(idpiper);
			else if (m_registorGroupE2D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements2D>(idpiper);
			else if (m_registorGroupE3D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements3D>(idpiper);
			else return nullptr;
		}

		template< >
		const GroupPtr FEModel::get<Group>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupNode.getIdfromIdFormat(idpiper, key, id))
				return get<GroupNode>(idpiper);
			else if (m_registorGroupE1D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements1D>(idpiper);
			else if (m_registorGroupE2D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements2D>(idpiper);
			else if (m_registorGroupE3D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements3D>(idpiper);
			else return nullptr;
		}
		template< >
		GroupNodePtr FEModel::get<GroupNode>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupNode.getIdfromIdFormat(idpiper, key, id))
				return get<GroupNode>(idpiper);
			else return nullptr;
		}


		template< >
		const GroupNodePtr FEModel::get<GroupNode>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupNode.getIdfromIdFormat(idpiper, key, id))
				return get<GroupNode>(idpiper);
			else return nullptr;
		}

		template< >
		GroupElements1DPtr FEModel::get<GroupElements1D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupE1D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements1D>(idpiper);
			else return nullptr;
		}

		template< >
		const GroupElements1DPtr FEModel::get<GroupElements1D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupE1D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements1D>(idpiper);
			else return nullptr;
		}

		template< >
		GroupElements2DPtr FEModel::get<GroupElements2D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupE2D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements2D>(idpiper);
			else return nullptr;
		}

		template< >
		const GroupElements2DPtr FEModel::get<GroupElements2D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupE2D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements2D>(idpiper);
			else return nullptr;
		}

		template< >
		GroupElements3DPtr FEModel::get<GroupElements3D>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupE3D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements3D>(idpiper);
			else return nullptr;
		}


		template< >
		const GroupElements3DPtr FEModel::get<GroupElements3D>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupE3D.getIdfromIdFormat(idpiper, key, id))
				return get<GroupElements3D>(idpiper);
			else return nullptr;
		}

		template< >
		GroupGroupsPtr FEModel::get<GroupGroups>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorGroupGroup.getIdfromIdFormat(idpiper, key, id))
				return get<GroupGroups>(idpiper);
			else return nullptr;
		}


		template< >
		const GroupGroupsPtr FEModel::get<GroupGroups>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorGroupGroup.getIdfromIdFormat(idpiper, key, id))
				return get<GroupGroups>(idpiper);
			else return nullptr;
		}

		template< >
		FEModelParameterPtr FEModel::get<FEModelParameter>(const string& key, const IdKey& id) {
			Id idpiper;
			if (m_registorModelparameter.getIdfromIdFormat(idpiper, key, id))
				return get<FEModelParameter>(idpiper);
			else return nullptr;
		}

		template< >
		const FEModelParameterPtr FEModel::get<FEModelParameter>(const string& key, const IdKey& id) const {
			Id idpiper;
			if (m_registorModelparameter.getIdfromIdFormat(idpiper, key, id))
				return get<FEModelParameter>(idpiper);
			else return nullptr;
		}

		void FEModel::deleteHistory(std::vector<std::string> const& vnames) {
            m_nodes.deleteHistory(vnames);
            m_parameters.deleteHistory(vnames);
		}

		void FEModel::addNewHistory(string const& stringID) {
            m_nodes.addNewHistory(stringID);
            m_parameters.addNewHistory(stringID);
		}

		void FEModel::setActive(string const& stringID) {
			m_meshdefstate->setActive(this, stringID);
			updateVTKRepresentation();
            m_parameters.setActive(stringID);
        }

		const Nodes& FEModel::getNodes(string const& historyName) const {
			return *m_nodes.getHistory(historyName);
		}

		Nodes& FEModel::getNodes(string const& historyName) {
			return *m_nodes.getHistory(historyName);
		}

		void FEModel::renameActiveHistory(std::string const& stringID) {
            m_nodes.renameActiveHistory(stringID);
            m_parameters.renameActiveHistory(stringID);
		}

		GroupNodePtr FEModel::get_GroupNodePtr(){
			return std::make_shared<GroupNode>();
		}

		GroupElements1DPtr FEModel::get_GroupElements1DPtr(){
			return std::make_shared<GroupElements1D>();
		}

		GroupElements2DPtr FEModel::get_GroupElements2DPtr(){
			return std::make_shared<GroupElements2D>();
		}

		GroupElements3DPtr FEModel::get_GroupElements3DPtr(){
			return std::make_shared<GroupElements3D>();
		}

	}
}
