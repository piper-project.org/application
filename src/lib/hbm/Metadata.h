/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_METADATA_H
#define PIPER_HBM_METADATA_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "units/units.h"

#include "GenericMetadata.h"
#include "Landmark.h"
#include "ControlPoint.h"
#include "Entity.h"
#include "EntityJoint.h"
#include "EntityContact.h"
#include "ModCoord.h"
#include "HbmParameter.h"
#include "Plane.h"
#include "AnthropoMetadata.h"
#include "contourCL.h"
#include "Group.h"
#include "apriorKnowledge.h"
#include "History.h"

namespace piper {
namespace hbm {
    //class Contour;
    //class HBM_contours;
/**
 * @brief The Metadata class encapsulates reading/writing of the metadata model file.
 *
 * metadata are stored in xml format, tinyxml2 is used for reading and writing.
 *
 * \todo use length unit enum
 * \todo warn / throw an exception in case a landmark, entity, joint, contact,... with the same name already exists
 *
 * @author Thomas Lemaire @date 2015
 * \todo use camlCase for genericMetadata related methods
 * \ingroup libhbm
 */
class HBM_EXPORT Metadata {
public:

    typedef std::map<std::string, GenericMetadata> GenericMetadataCont; ///< Container type for genericmetadata
    typedef std::map<std::string, Landmark> LandmarkCont; ///< Container type for landmarks
    typedef std::map<std::string, InteractionControlPoint> InteractionControlPointCont; ///< Container type for control points
    typedef std::map<std::string, Entity> EntityCont; ///< Container type for entities
    typedef std::map<std::string, EntityJoint> JointCont; ///< Container type for joints
    typedef std::map<std::string, AnatomicalJoint> AnatomicalJointCont; ///< Container type for anatomical joints
    typedef std::map<std::string, EntityContact> ContactCont; ///< Container type for contacts
	typedef std::map<std::string, HbmParameter> HbmParameterCont; ///< Container type for HbmParameter

    /// Construct empty metadata
    Metadata();

    /// Clear all metadata
    void clear();

    void setSourceModelFile(std::string const& sourceModelFile);

    /// clear the metadata and set the new source
    void setSource(std::string const& modelrulesFile, std::string const& formatrulesFileParent="");

	/// set source for environment model
    void setSourceEnv(std::string const& envfile);

	/// set format name
	void setNameFormat( const std::string& name);

	/// set format name
	std::string getNameFormat() const;

    /// clear the metadata and read the @param file
    void read(std::string file);

    /**
     * Write the Metadata to @param file, take care of sourceModel and mesh path, they are relative to @param file
     * mesh has the same name as @param file with ".vtu" extension.
     * \todo check if the file can be written beforehand
     */
    void write(std::string const& file);


    /**
    * Write the Metadata to @param file, so that this file can be imported later on.
    * \todo check if the file can be written beforehand
    */
    void exportMetadata(std::string const& file, FEModel& fem,
        bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool controlpoint = true, bool HbmParameter = true);

    void importMetadata(std::string const& file, FEModel& fem, 
        bool anthropometry = true, bool entity = true, bool landmark = true, bool genericmetadata = true, bool contact = true, bool joint = true, bool anatomicalJoint = true, bool controlpoint = true, bool HbmParameter = true);


    std::string const& modelPath() const;

    /**
     * @return absolute path to the source model
     */
    std::string sourcePath() const;
    /**
    * @return absolute paths to additional include files that should be parsed along the source
    */
    std::vector<std::string> sourceIncludeFiles() const;
	std::string rulePath() const;
    std::string sourceFormat() const;

    /**
     * @return absolute path to the mesh
     */
    std::string meshPath() const;

    void setLengthUnit(units::Length unit);
    units::Length const& lengthUnit() const { return m_lengthUnit; }

    void setMassUnit(units::Mass unit);
    units::Mass const& massUnit() const { return m_massUnit; }

    void setAgeUnit(units::Age unit);
    units::Age const& ageUnit() const { return m_ageUnit; }

    /// \return gravity vector
    Coord const& gravity() const {return m_gravity;}
    bool isGravityDefined() const {return !m_gravity.isApprox(Coord::Zero());}
    void setGravity(Coord const& g = Coord::Zero()) {m_gravity = g;}

    GenericMetadataCont const& genericmetadatas() const;
    GenericMetadata& genericmetadata(std::string const& name);
    bool hasGenericmetadata(const std::string& name) const;
    bool addGenericmetadata(GenericMetadata const& genmetadata);
    
    
    LandmarkCont const& landmarks() const;
    Landmark& landmark(std::string const& name);
    Landmark const& landmark(std::string const& name) const;
	bool hasLandmark( const std::string& landmarkname) const;
    bool addLandmark(Landmark const& landmark);

    InteractionControlPointCont & interactionControlPoints();    

    /// <summary>
    /// Gets a specified set of control points.
    /// </summary>
    /// <param name="name">The name of the set of control points.</param>
    /// <returns>Set of control points. NULL if there is no set with the specified name.</returns>
    InteractionControlPoint *interactionControlPoint(std::string const& name);
    InteractionControlPoint const* interactionControlPoint(std::string const& name) const;
    bool hasInteractionControlPoint(std::string const& name) const;
    bool addInteractionControlPoint(InteractionControlPoint& ctrlPt);

    EntityCont const& entities() const {return m_entities;}
    Entity const& entity( const std::string& entityname) const;
	Entity& entity(const std::string& entityname);
	bool hasEntity( const std::string& entityname) const;
    bool addEntity(Entity const& entity);

    ContactCont const& contacts() const {return m_contacts;}
    EntityContact const& contact( const std::string& contactname) const;
	bool hasContact( const std::string& contactname) const;
    bool addContact(EntityContact const& contact);

    JointCont const& joints() const {return m_joints;}
    EntityJoint const& joint( const std::string& jointname) const;
	EntityJoint& joint(const std::string& jointname);
	bool hasJoint( const std::string& jointname) const;
    bool addJoint(EntityJoint const& joint);

    AnatomicalJointCont const& anatomicalJoints() const {return m_anatomicalJoints;}
    AnatomicalJoint const& anatomicalJoint( const std::string& jointname) const;
    bool hasAnatomicalJoint( const std::string& jointname) const;
    bool addAnatomicalJoint(AnatomicalJoint const& joint);

	
	Frames m_frames;
	HBM_contours m_hbm_contours;
	contour_detail cont_det;
	ContourCL ctrl_line;
	ApriorKnowledge apriorinfo;
	std::map<Id, ModCoord> map_mod_coord;
	std::set<Id> setids;
	bool m_state;  // True => modified coordinates in fem and original coordinates in modcoord

	Id m_origin;
    void addLandmark(std::string name, std::vector<Id> elt_adr);
	
	
	HbmParameterCont const& hbmParameters() const { return m_hbmparameters; }
	HbmParameter const& hbmParameter(const std::string& parametername) const;
	HbmParameterCont& hbmParameters() { return m_hbmparameters; }
	HbmParameter& hbmParameter(const std::string& parametername);
	bool hasHbmParameter(const std::string& parametername) const;
	bool addHbmparameter(HbmParameter const& parameter);

    Anthropometry const& anthropometry() const { return *m_anthropometry.getActive(); }
    Anthropometry& anthropometry() { return *m_anthropometry.getActive(); }
    HistoryBase& anthropometryHistory() { return m_anthropometry; }
    Anthropometry const& anthropometry(std::string const& historyName) const { return *m_anthropometry.getHistory(historyName); }
    Anthropometry& anthropometry(std::string const& historyName) { return *m_anthropometry.getHistory(historyName); }


	/**
     * @return @param path if @param path is absolute, or compose modelPath() with @param path if it is relative
     */
    std::string absolutePath(std::string const& path) const;

    void removeEntity(std::string);
	void removeLandmark(std::string);
	void removeGenericMetadata(std::string);
	void removeJoint(std::string);
	void removeInteractionControlPoint(std::string);

private:

    void updateModelPath(std::string const& file);


    std::string m_modelPath;
    std::string m_source; // relative path to the main file
    std::vector<std::string> m_additionalInclude; // relative paths to additional include files
    std::string m_mesh;
    units::Length m_lengthUnit;
    units::Mass m_massUnit;
    units::Age m_ageUnit;
	std::string m_rules;
    std::string m_sourceFormat;
	std::string m_nameFormat;
    Coord m_gravity;
    LandmarkCont m_landmarks;
    InteractionControlPointCont m_controlPoints;
    EntityCont m_entities;
	JointCont m_joints;
    AnatomicalJointCont m_anatomicalJoints;
	ContactCont m_contacts;
	HbmParameterCont m_hbmparameters;
    GenericMetadataCont m_genmetadatas;
	
    History<Anthropometry> m_anthropometry;
};

}
}

#endif // PIPER_HBM_METADATA_H
