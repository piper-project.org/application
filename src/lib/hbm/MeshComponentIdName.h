/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHCOMPONENTIDNAME__H
#define MESHCOMPONENTIDNAME__H


#include "MeshComponent.h"

#include <string>

namespace piper { 
	namespace hbm {


		template< typename Tdef, typename Tid>
		class MeshComponentIdName: public MeshComponent<Tdef, Tid> {
		public:
			// ------------- uitilities  --------------
            void setName( const std::string& name) {m_name=name;}
            const std::string& getName() const {return m_name;}
		protected:
			std::string m_name;
			// ------------- constructor --------------
            explicit MeshComponentIdName( ): MeshComponent< Tdef, Tid>( ), m_name("Undefined") {}
            explicit MeshComponentIdName(const Id id): MeshComponent< Tdef, Tid>( id), m_name("Undefined") {}
		};

	}
}

#endif
