/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "EnvironmentModels.h"

#include "ParserModelFile.h"
#include "Helper.h"

#include "version.h"

namespace piper { 
	namespace hbm {

		EnvironmentModels::~EnvironmentModels() {
			clear();
		}

		void EnvironmentModels::clear() {
			for (MapEnvironmentModel::iterator it=m_models.begin(); it!=m_models.end(); ++it) {
				it->second->clear();
			}
			m_models.clear();
		}

		void EnvironmentModels::deleteEnvironmentModel( std::string const& name) {
			if (isExistingName( name)) {
				MapEnvironmentModel::iterator it=m_models.find( name);
				it->second->clear();
				m_models.erase( it);
			}
		}


		bool EnvironmentModels::isExistingName( std::string const& name) const {
			if (m_models.find( name) == m_models.end())
				return false;
			else return true;

		}

        EnvironmentModelPtr EnvironmentModels::addModel(std::string const& name, std::string const& file, std::string const& formatrulesFile, units::Length lengthUnit) {
            std::string nameok = space2underscore(name);
			if (!isExistingName( nameok)) {
                EnvironmentModelPtr env = std::make_shared<EnvironmentModel>(nameok, file, formatrulesFile, lengthUnit);
				m_models[nameok] = env;
                return env;
			}
            else
                throw std::runtime_error("Environment already exists "+name);
		}

        bool EnvironmentModels::addModel(std::string const& name, std::string const& file, std::string const& formatrulesFile, units::Length lengthUnit, units::Length targetlenghtunit) {
			std::string nameok = space2underscore(name);
            if (!isExistingName(nameok)) {
                EnvironmentModelPtr env = std::make_shared<EnvironmentModel>(nameok, file, formatrulesFile, lengthUnit);
                env->scaleLength(targetlenghtunit);
				m_models[nameok] = env;
				return true;
			}
            else {
                return false;
            }
        }

		EnvironmentModelPtr EnvironmentModels::getEnvironmentModel(std::string const& name) const {
			if (isExistingName( name))
				return (m_models.at(name));
            return NULL; // if the model with the name does not exist, return the first - perhaps print a warning? also does not solve the issue if there is no model at all...
		}

		std::vector<std::string> EnvironmentModels::getListNames() const {
			std::vector<std::string> list;
			for (MapEnvironmentModel::const_iterator it=m_models.begin(); it!=m_models.end(); ++it) {
				list.push_back( it->first);
			}
			return list;
		}

		void EnvironmentModels::write(const std::string& file) const {
			// build tinyxml document
			tinyxml2::XMLDocument project;

			project.InsertEndChild(project.NewDeclaration());
			project.InsertEndChild(project.NewUnknown("DOCTYPE piper SYSTEM \"piper.dtd\""));

			tinyxml2::XMLElement* xmlPiper = project.NewElement("piper");
			std::ostringstream version;
            version << PIPER_VERSION_MAJOR << "." << PIPER_VERSION_MINOR << "." << PIPER_VERSION_PATCH;
			xmlPiper->SetAttribute("version", version.str().c_str());
			project.InsertEndChild(xmlPiper);

			boost::filesystem::path filePath(file);
			serializeXml(xmlPiper, filePath.parent_path().string());

			// write tinyxml document
			project.SaveFile(file.c_str());
		}

		void EnvironmentModels::serializeXml(tinyxml2::XMLElement* element, std::string const& modelpath) const{


			std::vector<std::string> vlist=getListNames();
			for (std::vector<std::string>::const_iterator it=vlist.begin(); it!=vlist.end(); ++it) 
            {
				tinyxml2::XMLElement* elementadd;
                EnvironmentModelPtr model = getEnvironmentModel(*it);
				elementadd = element->GetDocument()->NewElement("EnvironmentModel");
				elementadd->SetAttribute("name", model->name().c_str());
				element->InsertEndChild(elementadd);

				//formatrules
				tinyxml2::XMLElement* xmlFormat = element->GetDocument()->NewElement("format_rules");
				boost::filesystem::path sourceFormatt(model->sourceFormat());
				std::string relativesourceFormatpath = relativePath(sourceFormatt, modelpath).string();
				xmlFormat->SetText(relativesourceFormatpath.c_str());
				elementadd->InsertEndChild(xmlFormat);

				//file
				tinyxml2::XMLElement* xmlFile = element->GetDocument()->NewElement("file");
				boost::filesystem::path sourceEnv(model->file());
				std::string relativesourcepath = relativePath(sourceEnv, modelpath).string();
				xmlFile->SetText(relativesourcepath.c_str());
				elementadd->InsertEndChild(xmlFile);

				// units
				tinyxml2::XMLElement* xmlUnit = element->GetDocument()->NewElement("units");
                xmlUnit->SetAttribute(units::LengthUnit::type().c_str(), units::LengthUnit::unitToStr(model->lengthUnit()).c_str());
				elementadd->InsertEndChild(xmlUnit);

                // transformation
                if (!model->envmodel().getTranslation().isApprox(Coord::Zero())) {
                    tinyxml2::XMLElement* xmlTranslation = element->GetDocument()->NewElement("translation");
                    serializeCoord(xmlTranslation, model->envmodel().getTranslation());
                    elementadd->InsertEndChild(xmlTranslation);
                }
                if (!model->envmodel().getRotation().isApprox(Coord::Zero())) {
                    tinyxml2::XMLElement* xmlRotation = element->GetDocument()->NewElement("rotation");
                    serializeCoord(xmlRotation, model->envmodel().getRotation());
                    elementadd->InsertEndChild(xmlRotation);
                }
                if (!model->envmodel().getScale().isApprox(Coord::Ones())) {
                    tinyxml2::XMLElement* xmlScale = element->GetDocument()->NewElement("scale");
                    serializeCoord(xmlScale, model->envmodel().getScale());
                    elementadd->InsertEndChild(xmlScale);
                }
			}

		}

        void EnvironmentModels::read(const std::string& file) {
			tinyxml2::XMLDocument model;
			model.LoadFile(file.c_str());
            if (model.Error()) {
                clear();
                std::stringstream str;
                str << "Failed to load file: " << file << std::endl << model.ErrorStr() << std::endl;
                throw std::runtime_error(str.str().c_str());
            }

			clear();
			tinyxml2::XMLElement* element;

			//
			boost::filesystem::path filePath(file);
			std::string filepath = boost::filesystem::absolute(filePath).parent_path().string();

            element = model.RootElement()->FirstChildElement("EnvironmentModel");
			while(element!=nullptr) {
				std::string name = element->Attribute("name");
				std::string fileenv = absolutePath(element->FirstChildElement("file")->GetText(), filepath);
				std::string sourceformat = absolutePath(element->FirstChildElement("format_rules")->GetText(), filepath);
                std::string unit = element->FirstChildElement("units")->Attribute(units::LengthUnit::type().c_str());
                EnvironmentModelPtr env = addModel(name, fileenv, sourceformat, units::LengthUnit::strToUnit(unit));
                tinyxml2::XMLElement* xmlTranslation = element->FirstChildElement("translation");
                if (nullptr != xmlTranslation) {
                    Coord t;
                    parseCoord(t, xmlTranslation);
                    env->envmodel().setTranslation(t[0],t[1],t[2]);
                }
                tinyxml2::XMLElement* xmlRotation = element->FirstChildElement("rotation");
                if (nullptr != xmlRotation) {
                    Coord r;
                    parseCoord(r, xmlRotation);
                    env->envmodel().setRotation(r[0],r[1],r[2]);
                }
                tinyxml2::XMLElement* xmlScale = element->FirstChildElement("scale");
                if (nullptr != xmlScale) {
                    Coord s;
                    parseCoord(s, xmlScale);
                    env->envmodel().setScale(s[0],s[1],s[2]);
                }

				element = element->NextSiblingElement("EnvironmentModel");
			}
		}

        void EnvironmentModels::readandImport(const std::string& file, units::Length targetlenghtunit) {
            read(file);
            scaleEnvModels(targetlenghtunit);
		}

        void EnvironmentModels::scaleEnvModels(units::Length targetlenghtunit) {
            for (MapEnvironmentModel::const_iterator it = m_models.begin(); it != m_models.end(); ++it) {
                it->second->scaleLength(targetlenghtunit);
            }
        }


	}
}
