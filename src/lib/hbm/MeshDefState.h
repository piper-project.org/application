/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHDEFSTATE__H
#define MESHDEFSTATE__H
#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "Node.h"

namespace piper {
	namespace hbm {

	// **********************************************************
	// MESHDEF STATE
	// **********************************************************
	/*typedef unsigned int Id;*/
	//typedef std::set<Id> SId;
	HBM_EXPORT enum MeshDefType {INDEX, NID};

	class FEModel;

	class MeshDefState {
	public:
        MeshDefState(const MeshDefType meshdfetype): m_meshdeftype(meshdfetype) {}
        MeshDefType getMeshDefState() {return m_meshdeftype;}
		virtual ~MeshDefState() {};
		// ------------- Utilities: Nodes -------------
		//virtual void printNodes(const FEModel* mesh) const =0;
		virtual VId getListNid(const FEModel* mesh)=0;
        virtual void setNode(FEModel* mesh, std::vector<std::shared_ptr<Node>>& vnode) = 0;
		virtual void delNode(FEModel* mesh, const std::vector<Id>& vnid)=0;
		virtual void updateMeshDef(FEModel* mesh)=0;
        virtual void setActive(FEModel* mesh, std::string const& stringID) = 0;


	protected:
		MeshDefType m_meshdeftype;
	};

    /** Identification of nodes by id (piper id)
     */
	class MeshDefNID : public MeshDefState {
	public:
        MeshDefNID(): MeshDefState(MeshDefType::NID) {}

		//void printNodes(const FEModel* mesh) const;
		VId getListNid(const FEModel* mesh);
        void setNode(FEModel* mesh, std::vector<std::shared_ptr<Node>>& vnode);
		void delNode(FEModel* mesh, const std::vector<Id>& vnid);
		void updateMeshDef(FEModel* mesh);
        void setActive(FEModel* mesh, std::string const& stringID);

	};

    /** Identification of nodes by index (used to save in VTK format)
     */
	class MeshDefINDEX : public MeshDefState {
	public:
        MeshDefINDEX(): MeshDefState(MeshDefType::INDEX) {}

		//void printNodes(const FEModel* mesh) const;
		VId getListNid(const FEModel* mesh);
        void setNode(FEModel* mesh, std::vector<std::shared_ptr<Node>>& vnode);
		void delNode(FEModel* mesh, const std::vector<Id>& vnid);
		void updateMeshDef(FEModel* mesh);
        void setActive(FEModel* mesh, std::string const& stringID);
	};

	}
}
#endif
