/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef FEMODELPARAMETER_H
#define FEMODELPARAMETER_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "MeshComponentContainer.h"
#include "tinyxml/tinyxml2.h"

#include <string>
#include <map>

namespace piper {
	namespace hbm {
		typedef std::map<std::string, std::vector<double>> map_parameter;
		/**
		* @brief Class to store parameter to scale from FE model.
		*
		* Container for parameter to be scaled 
		*
		* @author Erwan Jolivet @date 2015
		*/
		class HBM_EXPORT FEModelParameter: public MeshComponent< map_parameter, FEModelParameter> {
		public:
			FEModelParameter();
			explicit FEModelParameter(tinyxml2::XMLElement* element);
	

            void setValue(const std::string& valuename, const std::vector<double>& value);
            void appendValue(const std::string& valuename, const std::vector<double>& value);
			std::vector<double> get( const std::string& valuename) const; 
			void deleteParam( const std::string& valuename);
			std::set<std::string> getListParameterNames() const;
			void serializeXml(tinyxml2::XMLElement* element) const; 
			
			// ------------- interface --------------
            virtual void convId(std::unordered_map<Id,Id> const& map_id) {}


		protected:
			explicit FEModelParameter(const Id& nid);

		private:
			void parseXml(tinyxml2::XMLElement* element);
		};

		class HBM_EXPORT FEModelParameters : public MeshComponentContainer< FEModelParameter> {
		public:
            FEModelParameters(): MeshComponentContainer< FEModelParameter>( "FEModelParameter")  {}
            void setEntity(std::shared_ptr<FEModelParameter> param);

			//const std::map<std::string, SId>& getListParameters() const {return m_listparam;};
            void setValue(const Id& id, const std::string& valuename, const std::vector<double>& value);
            void appendValue(const Id& id, const std::string& valuename, const std::vector<double>& value);


		private:
			//void updateListParameters( const Id& id);
			//std::map<std::string, SId> m_listparam;
		};


	}
}
#endif
