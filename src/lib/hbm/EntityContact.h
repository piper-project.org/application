/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef ENTITYCONTACT_H
#define ENTITYCONTACT_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <array>

#include "BaseMetadata.h"

namespace piper {
namespace hbm {

class fem;

static const std::array<std::string, 3> EntityContactType_str={{"NOTYPE","ATTACHED", "SLIDING"}};

/**
* @brief metadata for contact.
* @todo more details about a list greater than two
*
* @author Thomas Lemaire @date 2015
*/
class HBM_EXPORT EntityContact: public BaseMetadata {
public:

    /// Type of the contact
    enum class Type {NOTYPE=0,
                     ATTACHED, ///< the entities are attached at their contact surface
                     SLIDING,  ///< the entities can slide at their contact surface
                     SIZE }; // SIZE must be the last element

    EntityContact();
    EntityContact(std::string const& name);
    EntityContact(tinyxml2::XMLElement* element);

    /// @todo
    void serializeXml(tinyxml2::XMLElement* element) const;


    Type type() const {return m_type;}
    void setType(Type type) {m_type = type;}
	void setType(const std::string& type);

	/// set the entity 2 of the contact and id of group of nodes involed in contact
    void setEntity2(std::string const& name, const Id& id=hbm::ID_UNDEFINED);
	/// set the entity 1 of the contact and id of group of nodes involed in contact
    void setEntity1(std::string const& name, const Id& id=hbm::ID_UNDEFINED);

    std::string const& entity2() const {return m_entity2;}
    std::string const& entity1() const {return m_entity1;}

	Id group2() const {return m_group2;}
	Id group1() const {return m_group1;}

	void setkeepThickness() {m_keepThickness=true;}
	bool keepThickness() const {return m_keepThickness;}

    /// @return the contact gap or thickness
    double thickness() const {return m_thickness;}
    /// @todo do something for negative thickness ?
    void setThickness(double thickness) {m_thickness = thickness;}

    virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;


private:

    Type m_type;
    double m_thickness;
	std::string m_entity1, m_entity2;
	Id m_group1, m_group2;
	bool m_keepThickness;

    //
    void parseXml(tinyxml2::XMLElement* element);
};


}
}


#endif // ENTITYCONTACT_H
