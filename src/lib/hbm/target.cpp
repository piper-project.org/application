/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "target.h"

#include <iostream>
#include <exception>

#include "version.h"

#include "xmlTools.h"

namespace piper {
namespace hbm {

template class TargetUnit<piper::units::Length>;
template class TargetUnit<piper::units::Age>;
template class TargetUnit<piper::units::Mass>;

static std::map<std::string, unsigned int> frameDofToIndex = {{"x",0},{"y",1},{"z",2},{"rx",3},{"ry",4},{"rz",5}};
static std::map<unsigned int, std::string> indexToframeDof = {{0,"x"},{1,"y"},{2,"z"},{3,"rx"},{4,"ry"},{5,"rz"}};

/*
 * AbstractTarget
 */

void AbstractTarget::parseXml(const tinyxml2::XMLElement* element)
{
    auto name = element->FirstChildElement("name");
    if (name)
        m_name = name->GetText();
}

void AbstractTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
    xmlName->SetText(name().c_str());
    element->InsertEndChild(xmlName);
}

/*
 * TargetUnit
 */

//void TargetUnit::parseXml(const tinyxml2::XMLElement* element)
//{
//    if (element->FirstChildElement("units") != nullptr)
//        lengthUnit = units::strToLength.at(element->FirstChildElement("units")->Attribute("length"));
//}
//
//void TargetUnit::serializeXml(tinyxml2::XMLElement* element) const
//{
//    tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("units");
//    xmlName->SetAttribute("length", units::lengthToStr.at(lengthUnit).c_str());
//    element->InsertEndChild(xmlName);
//}
//
//void TargetUnit::convertToUnit(units::Length targetLengthUnit)
//{
//    if (lengthUnit==targetLengthUnit)
//        return;
//    doConvertToUnit(targetLengthUnit);
//    lengthUnit = targetLengthUnit;
//}


/*
 * FixedBoneTarget
 */

FixedBoneTarget::FixedBoneTarget(std::string _bone)
    : bone(_bone)
{
    setName(_bone);
}

void FixedBoneTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    bone = element->FirstChildElement("bone")->GetText();
}

void FixedBoneTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    tinyxml2::XMLElement* xmlBone = element->GetDocument()->NewElement("bone");
    xmlBone->SetText(bone.c_str());
    element->InsertEndChild(xmlBone);
}

/*
 * Landmark
 */
LandmarkTarget::LandmarkTarget()
    : m_kriging_use_bone(0.5)
    , m_kriging_use_skin(0.5)

{}


double const& LandmarkTarget::getKrigingUseBone() const { 
    return m_kriging_use_bone; 
}

void LandmarkTarget::setKrigingUseBone(double const& value) {
    m_kriging_use_bone = value;
}


double const& LandmarkTarget::getKrigingUseSkin() const {
    return m_kriging_use_skin;
}

void LandmarkTarget::setKrigingUseSkin(double const& value) {
    m_kriging_use_skin = value;
}

void LandmarkTarget::setValue(const ValueType &value)
{
    // check for index
    for (auto const& v : value)
        if (v.first>2)
            throw std::runtime_error("Invalid LandmarkTarget value, index out of bound [0,2]");
    m_value = value;
}

LandmarkTarget::MaskType LandmarkTarget::mask() const
{
    MaskType mask={false,false,false};
    for (auto const& v : value()) {
        mask[v.first]=true;
    }
    return mask;
}

void LandmarkTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);


    landmark = element->FirstChildElement("landmark")->GetText();

    ValueType value;
    const tinyxml2::XMLElement* elValue = element->FirstChildElement("value");
    while (elValue != nullptr) {
        std::stringstream ss(elValue->GetText());
        ss >> value[frameDofToIndex.at(elValue->Attribute("dof"))];
        elValue = elValue->NextSiblingElement("value");
    }
    setValue(value);

    const tinyxml2::XMLElement* elParam = element->FirstChildElement("krigingParameter");
    if (elParam != nullptr && elParam->FirstChildElement("use_bone") != nullptr) {
        std::stringstream ss(elParam->FirstChildElement("use_bone")->GetText());
        ss >> m_kriging_use_bone;
        std::stringstream ss2(elParam->FirstChildElement("use_skin")->GetText());
        ss2 >> m_kriging_use_skin;
    }
}

void LandmarkTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    for (auto const& v : value()) {
        tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
        xmlValue->SetAttribute("dof", indexToframeDof.at(v.first).c_str());
        std::stringstream ss; ss << v.second;
        xmlValue->SetText(ss.str().c_str());
        element->InsertEndChild(xmlValue);
    }

    tinyxml2::XMLElement* xmlParam = element->GetDocument()->NewElement("krigingParameter");
    element->InsertEndChild(xmlParam);
    tinyxml2::XMLElement* xmlParamValue = element->GetDocument()->NewElement("use_bone");
    xmlParam->InsertEndChild(xmlParamValue);
    std::stringstream ss; ss << m_kriging_use_bone;
    xmlParamValue->SetText(ss.str().c_str());
    tinyxml2::XMLElement* xmlParamValue2 = element->GetDocument()->NewElement("use_skin");
    xmlParam->InsertEndChild(xmlParamValue2);
    std::stringstream ss2; ss2 << m_kriging_use_skin;
    xmlParamValue2->SetText(ss2.str().c_str());

    tinyxml2::XMLElement* xmlLandmark = element->GetDocument()->NewElement("landmark");
    xmlLandmark->SetText(landmark.c_str());
    element->InsertEndChild(xmlLandmark);
}

void LandmarkTarget::doConvertToUnit(units::Length targetLengthUnit)
{
    for(auto & val : m_value )
        units::convert(m_unit, targetLengthUnit, val.second);
}

/*
 * ScalingParameterTarget
 */

void ScalingParameterTarget::parseXml(const tinyxml2::XMLElement* element)
{
	AbstractTarget::parseXml(element);
    parseValueCont(m_value, element->FirstChildElement("value"));
}

void ScalingParameterTarget::serializeXml(tinyxml2::XMLElement* element) const
{
	AbstractTarget::serializeXml(element);

    tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
    serializeValueCont(xmlValue, m_value);
    element->InsertEndChild(xmlValue);
}
/*
 * AbstractRigidTransformationTarget
 */

void AbstractRigidTransformationTarget::setValue(const ValueType &value)
{
    // check for index
    for (auto const& v : value)
        if (v.first>5)
            throw std::runtime_error("Invalid RigidTransformation value, index out of bound [0,5]");
    m_value = value;
}

AbstractRigidTransformationTarget::MaskType AbstractRigidTransformationTarget::mask() const
{
    MaskType mask={false,false,false,false,false,false};
    for (auto const& v : value()) {
        mask[v.first]=true;
    }
    return mask;
}

void AbstractRigidTransformationTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    ValueType value;
    const tinyxml2::XMLElement* elValue = element->FirstChildElement("value");
    while (elValue != nullptr) {
        std::stringstream ss(elValue->GetText());
        ss >> value[frameDofToIndex.at(elValue->Attribute("dof"))];
        elValue = elValue->NextSiblingElement("value");
    }
    setValue(value);
}

void AbstractRigidTransformationTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    for (auto const& v : value()) {
        tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
        xmlValue->SetAttribute("dof", indexToframeDof.at(v.first).c_str());
        std::stringstream ss; ss << v.second;
        xmlValue->SetText(ss.str().c_str());
        element->InsertEndChild(xmlValue);
    }
}

void AbstractRigidTransformationTarget::doConvertToUnit(units::Length targetLengthUnit)
{
    for(auto & val : m_value )
        if (val.first < 3)
            units::convert(m_unit, targetLengthUnit, val.second);
}

/*
 * Joint
 */

JointTarget::JointTarget(std::string _joint, ValueType const& value)
    : joint(_joint)
{
    setName(_joint);
    setValue(value);
}

void JointTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractRigidTransformationTarget::parseXml(element);

    joint = element->FirstChildElement("joint")->GetText();
}

void JointTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractRigidTransformationTarget::serializeXml(element);

    tinyxml2::XMLElement* xmlJoint = element->GetDocument()->NewElement("joint");
    xmlJoint->SetText(joint.c_str());
    element->InsertEndChild(xmlJoint);
}

/*
 * FrameToFrame
 */

void FrameToFrameTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractRigidTransformationTarget::parseXml(element);

    if (nullptr != element->FirstChildElement("isRelative"))
        isRelative = true;
    else
        isRelative = false;

    frameSource = element->FirstChildElement("frameSource")->GetText();
    frameTarget = element->FirstChildElement("frameTarget")->GetText();
}

void FrameToFrameTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractRigidTransformationTarget::serializeXml(element);

    if (isRelative) {
        element->InsertEndChild( element->GetDocument()->NewElement("isRelative") );
    }


    tinyxml2::XMLElement* xmlFrameSource = element->GetDocument()->NewElement("frameSource");
    xmlFrameSource->SetText(frameSource.c_str());
    element->InsertEndChild(xmlFrameSource);
    tinyxml2::XMLElement* xmlFrameTarget = element->GetDocument()->NewElement("frameTarget");
    xmlFrameTarget->SetText(frameTarget.c_str());
    element->InsertEndChild(xmlFrameTarget);
}


/*
* AnthropometricDimensionTarget
*/

AnthropometricDimensionTarget::AnthropometricDimensionTarget()
    : m_value(-1)
    , m_scale(-1) {}



void AnthropometricDimensionTarget::setValue(double value)
{
    if (value < 0)
        throw std::runtime_error("AnthropometricDimensionTarget::setValue: negative value");
    m_value = value;
}

void AnthropometricDimensionTarget::setScale(double scale)
{
    if (scale < 0)
        throw std::runtime_error("AnthropometricDimensionTarget::setScale: negative value");
    m_scale = scale;
}

void AnthropometricDimensionTarget::setPosture(std::string const& posture) {
    m_posture = posture;
}


void AnthropometricDimensionTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    const tinyxml2::XMLElement* elValue = element->FirstChildElement("value");
    while (elValue != nullptr) {
        double val;
        std::stringstream ss(elValue->GetText());
        if (elValue->Attribute("scale", "yes")) {
            ss >> val;
            setScale(val);
        }
        else {
            ss >> val;
            setValue(val);
        }
        elValue = elValue->NextSiblingElement("value");
    }

    if (element->FirstChildElement("posture") != nullptr && element->FirstChildElement("posture")->Attribute("type") != nullptr)
        m_posture = element->FirstChildElement("posture")->Attribute("type");
}

void AnthropometricDimensionTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);


    if (hasAbsoluteValue()) {
        tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
        xmlValue->SetText(value());
        element->InsertEndChild(xmlValue);
    }
    if (hasScaleValue()) {
        tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
        xmlValue->SetAttribute("scale", "yes");
        xmlValue->SetText(scale());
        element->InsertEndChild(xmlValue);
    }

    if (m_posture.size() > 0) {
        tinyxml2::XMLElement* xmlPosture = element->GetDocument()->NewElement("posture");
        xmlPosture->SetAttribute("type", m_posture.c_str());
        //xmlPosture->SetText(m_posture.c_str());
        element->InsertEndChild(xmlPosture);
    }
}

void AnthropometricDimensionTarget::doConvertToUnit(units::Length targetLengthUnit)
{
    units::convert(m_unit, targetLengthUnit, m_value);
}

/*
* AgeTarget
*/

void AgeTarget::setValue(double value)
{
    if (value < 0)
        throw std::runtime_error("AgeTarget::setValue: negative value");
    m_value = value;
}

void AgeTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    double value;
    if (element->FirstChildElement("value") != nullptr && element->FirstChildElement("value")->GetText() != nullptr){
        std::stringstream ss(element->FirstChildElement("value")->GetText());
        ss >> value;
        setValue(value);
    }
}

void AgeTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
    xmlValue->SetText(value());
    element->InsertEndChild(xmlValue);
}

void AgeTarget::doConvertToUnit(units::Age targetUnit)
{
    units::convert(m_unit, targetUnit, m_value);
}

/*
* WeightTarget
*/

void WeightTarget::setValue(double value)
{
    if (value < 0)
        throw std::runtime_error("WeightTarget::setValue: negative value");
    m_value = value;
}

void WeightTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    double value;
    if (element->FirstChildElement("value") != nullptr && element->FirstChildElement("value")->GetText() != nullptr){
        std::stringstream ss(element->FirstChildElement("value")->GetText());
        ss >> value;
        setValue(value);
    }
}

void WeightTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
    xmlValue->SetText(value());
    element->InsertEndChild(xmlValue);
}

void WeightTarget::doConvertToUnit(units::Mass targetUnit)
{
    units::convert(m_unit, targetUnit, m_value);
}

/*
* HeightTarget
*/

void HeightTarget::setValue(double value)
{
    if (value < 0)
        throw std::runtime_error("HeightTarget::setValue: negative value");
    m_value = value;
}

void HeightTarget::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    double value;
    if (element->FirstChildElement("value") != nullptr && element->FirstChildElement("value")->GetText() != nullptr){
        std::stringstream ss(element->FirstChildElement("value")->GetText());
        ss >> value;
        setValue(value);
    }
}

void HeightTarget::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
    xmlValue->SetText(value());
    element->InsertEndChild(xmlValue);
}

void HeightTarget::doConvertToUnit(units::Length targetUnit)
{
    units::convert(m_unit, targetUnit, m_value);
}




/*
* ControlPointTarget
*/

void ControlPoint::parseXml(const tinyxml2::XMLElement* element)
{
    AbstractTarget::parseXml(element);
    TargetUnit::parseXml(element);

    parseVecCoord(this->m_value, element->FirstChildElement("value"));
}

void ControlPoint::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractTarget::serializeXml(element);
    TargetUnit::serializeXml(element);

    tinyxml2::XMLElement* xmlPoint3d;
    xmlPoint3d = element->GetDocument()->NewElement("value");
    serializeVecCoord(xmlPoint3d, this->m_value);
    element->InsertEndChild(xmlPoint3d);
}

void ControlPoint::doConvertToUnit(units::Length targetLengthUnit) {
    for (auto & cur : m_value)
        units::convert(m_unit, targetLengthUnit, cur);
}


/*
* TargetList
*/

bool TargetList::empty() const
{
    return fixedBone.empty()
        && landmark.empty()
        && joint.empty()
        && frameToFrame.empty()
        && scalingparameter.empty()
        && anthropometricDimension.empty()
        && age.empty()
        && weight.empty()
        && height.empty()
        && controlPoint.empty();
}

void TargetList::clear()
{
    anthropometricDimension.clear();
    fixedBone.clear();
    landmark.clear();
    joint.clear();
    frameToFrame.clear();
    scalingparameter.clear();
    age.clear();
    weight.clear();
    height.clear();
    controlPoint.clear();
}


size_t TargetList::size() const
{
    return fixedBone.size() +
            landmark.size() +
            joint.size() +
            frameToFrame.size() +
            scalingparameter.size() +
            anthropometricDimension.size() +
            age.size() +
            weight.size() +
            height.size() +
            controlPoint.size();
}


void TargetList::addTarget(std::shared_ptr<AbstractTarget> t, std::string type)
{
    if (0 == type.compare(FixedBoneTarget::type())) {
        fixedBone.push_back(*static_cast<FixedBoneTarget *>(t.get()));
    }
    else if (0 == type.compare(LandmarkTarget::type())) {
        landmark.push_back(*static_cast<LandmarkTarget *>(t.get()));
    }
    else if (0 == type.compare(JointTarget::type())) {
        joint.push_back(*static_cast<JointTarget *>(t.get()));
    }
    else if (0 == type.compare(FrameToFrameTarget::type())) {
        frameToFrame.push_back(*static_cast<FrameToFrameTarget *>(t.get()));
    }
    else if (0 == type.compare(ScalingParameterTarget::type())) {
        scalingparameter.push_back(*static_cast<ScalingParameterTarget *>(t.get()));
    }
    else if (0 == type.compare(AnthropometricDimensionTarget::type())) {
        anthropometricDimension.push_back(*static_cast<AnthropometricDimensionTarget *>(t.get()));
    }
    else if (0 == type.compare(AgeTarget::type())) {
        age.push_back(*static_cast<AgeTarget *>(t.get()));
    }
    else if (0 == type.compare(HeightTarget::type())) {
        height.push_back(*static_cast<HeightTarget *>(t.get()));
    }
    else if (0 == type.compare(WeightTarget::type())) {
        weight.push_back(*static_cast<WeightTarget *>(t.get()));
    }
    else if (0 == type.compare(ControlPoint::type())) {
        controlPoint.push_back(*static_cast<ControlPoint *>(t.get()));
    }
}

void TargetList::removeFixedBone(std::string name)
{
    for (auto it = fixedBone.begin(); it != fixedBone.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            fixedBone.erase(it);
            break;
        }
    }
}

void TargetList::removeLandmark(std::string name)
{
    for (auto it = landmark.begin(); it != landmark.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            landmark.erase(it);
            break;
        }
    }
}

void TargetList::removeJoint(std::string name)
{
    for (auto it = joint.begin(); it != joint.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            joint.erase(it);
            break;
        }
    }
}

void TargetList::removeFrameToFrame(std::string name)
{
    for (auto it = frameToFrame.begin(); it != frameToFrame.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            frameToFrame.erase(it);
            break;
        }
    }
}

void TargetList::removeScalingParameter(std::string name)
{
    for (auto it = scalingparameter.begin(); it != scalingparameter.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            scalingparameter.erase(it);
            break;
        }
    }
}

void TargetList::removeAnthropometricDimension(std::string name)
{
    for (auto it = anthropometricDimension.begin(); it != anthropometricDimension.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            anthropometricDimension.erase(it);
            break;
        }
    }
}

void TargetList::removeControlPoint(std::string name)
{
    for (auto it = controlPoint.begin(); it != controlPoint.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            controlPoint.erase(it);
            break;
        }
    }
}

void TargetList::removeAge(std::string name)
{
    for (auto it = age.begin(); it != age.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            age.erase(it);
            break;
        }
    }
}

void TargetList::removeWeight(std::string name)
{
    for (auto it = weight.begin(); it != weight.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            weight.erase(it);
            break;
        }
    }
}

void TargetList::removeHeight(std::string name)
{
    for (auto it = height.begin(); it != height.end(); it++)
    {
        if (it->name().compare(name) == 0)
        {
            height.erase(it);
            break;
        }
    }
}

AbstractTarget *TargetList::addTarget(std::string type)
{
    if (0==type.compare(FixedBoneTarget::type())) {
        fixedBone.push_back(FixedBoneTarget());
        return &fixedBone.back();
    }
    else if (0==type.compare(LandmarkTarget::type())) {
        landmark.push_back(LandmarkTarget());
        return &landmark.back();
    }
    else if (0==type.compare(JointTarget::type())) {
        joint.push_back(JointTarget());
        return &joint.back();
    }
    else if (0==type.compare(FrameToFrameTarget::type())) {
        frameToFrame.push_back(FrameToFrameTarget());
        return &frameToFrame.back();
    }
	else if (0 == type.compare(ScalingParameterTarget::type())) {
		scalingparameter.push_back(ScalingParameterTarget());
		return &scalingparameter.back();
	}
    else if (0 == type.compare(AnthropometricDimensionTarget::type())) {
        anthropometricDimension.push_back(AnthropometricDimensionTarget());
        return &anthropometricDimension.back();
    }
    else if (0 == type.compare(AgeTarget::type())) {
        age.push_back(AgeTarget());
        return &age.back();
    }
    else if (0 == type.compare(HeightTarget::type())) {
        height.push_back(HeightTarget());
        return &height.back();
    }
    else if (0 == type.compare(WeightTarget::type())) {
        weight.push_back(WeightTarget());
        return &weight.back();
    }
    else if (0 == type.compare(ControlPoint::type())) {
        controlPoint.push_back(ControlPoint());
        return &controlPoint.back();
    }
    else
        return nullptr;
}


std::shared_ptr<AbstractTarget> TargetList::createTarget(std::string type)
{
    if (0 == type.compare(FixedBoneTarget::type())) {
        return std::make_shared<FixedBoneTarget>();
    }
    else if (0 == type.compare(LandmarkTarget::type())) {
        return std::make_shared<LandmarkTarget>();
    }
    else if (0 == type.compare(JointTarget::type())) {
        return std::make_shared<JointTarget>();
    }
    else if (0 == type.compare(FrameToFrameTarget::type())) {
        return std::make_shared<FrameToFrameTarget>();
    }
    else if (0 == type.compare(ScalingParameterTarget::type())) {
        return std::make_shared<ScalingParameterTarget>();
    }
    else if (0 == type.compare(AnthropometricDimensionTarget::type())) {
        return std::make_shared<AnthropometricDimensionTarget>();
    }
    else if (0 == type.compare(AgeTarget::type())) {
        return std::make_shared<AgeTarget>();
    }
    else if (0 == type.compare(HeightTarget::type())) {
        return std::make_shared<HeightTarget>();
    }
    else if (0 == type.compare(WeightTarget::type())) {
        return std::make_shared<WeightTarget>();
    }
    else if (0 == type.compare(ControlPoint::type())) {
        return std::make_shared<ControlPoint>();
    }
    else
        return nullptr;
}


std::string TargetList::parseXml(const tinyxml2::XMLElement *parentElement)
{
    if (parentElement->FirstChildElement("targetList")==nullptr)
        return "";
    std::stringstream errors;
    errors.str("");
    const tinyxml2::XMLElement* xmlTarget = parentElement->FirstChildElement("targetList")->FirstChildElement("target");
    while(xmlTarget!=nullptr) {
        std::string type = xmlTarget->Attribute("type");
        std::shared_ptr<AbstractTarget> target = createTarget(type);
        if (target == nullptr)
            errors << std::endl << "TargetList::addTarget: unknown target type: " + type;
        else
        {
            try
            {
                target->parseXml(xmlTarget);
                addTarget(target, type); // add only if it was succesfully parsed
            }
            catch (std::exception ex)
            {
                errors << std::endl << "Error reading target: " << ex.what() << "; this target will be ignored.";
            }

        }
        xmlTarget = xmlTarget->NextSiblingElement("target");
    }
    return errors.str();
}

std::string TargetList::read(std::string const& filename)
{
    clear();
    tinyxml2::XMLDocument xmlTarget;
    xmlTarget.LoadFile(filename.c_str());
    if (xmlTarget.Error()) {
        clear();
        std::stringstream str;
        str << "Error while loading target file: " << filename << std::endl << xmlTarget.ErrorStr() << std::endl;
        throw std::runtime_error(str.str().c_str());
    }

    return parseXml(xmlTarget.RootElement());
}

void TargetList::serializeXml(tinyxml2::XMLElement* parentElement) const
{
    tinyxml2::XMLElement* xmlTargetList = parentElement->GetDocument()->NewElement("targetList");
    parentElement->InsertEndChild(xmlTargetList);

    // fixed bone
    for (FixedBoneTarget const& target : fixedBone)
        targetSerializeXml(target, xmlTargetList);
    // landmark
    for (LandmarkTarget const& target : landmark)
        targetSerializeXml(target, xmlTargetList);
    // joint
    for (JointTarget const& target : joint)
        targetSerializeXml(target, xmlTargetList);
    // frameToFrame
    for (FrameToFrameTarget const& target : frameToFrame)
        targetSerializeXml(target, xmlTargetList);
    // scalingParameter
    for (ScalingParameterTarget const& target : scalingparameter)
        targetSerializeXml(target, xmlTargetList);
    // AnthropometricDimension
    for (AnthropometricDimensionTarget const& target : anthropometricDimension)
        targetSerializeXml(target, xmlTargetList);
    //age
    for (AgeTarget const& target : age)
        targetSerializeXml(target, xmlTargetList);
    // weight
    for (WeightTarget const& target : weight)
        targetSerializeXml(target, xmlTargetList);
    // height
    for (HeightTarget const& target : height)
        targetSerializeXml(target, xmlTargetList);
    // control point
    for (ControlPoint const& target : controlPoint)
        targetSerializeXml(target, xmlTargetList);
}

void TargetList::write(std::string const& filename) const
{
    // build tinyxml document
    tinyxml2::XMLDocument xmlDocument;

    xmlDocument.InsertEndChild(xmlDocument.NewDeclaration());
//    model.InsertEndChild(model.NewUnknown("DOCTYPE piper SYSTEM \"target.dtd\""));

    tinyxml2::XMLElement* xmlTarget = xmlDocument.NewElement("piper-target");
    std::ostringstream version;
    version << PIPER_VERSION_MAJOR << "." << PIPER_VERSION_MINOR << "." << PIPER_VERSION_PATCH;
    xmlTarget->SetAttribute("version", version.str().c_str());
    xmlDocument.InsertEndChild(xmlTarget);

    serializeXml(xmlDocument.RootElement());

    // write tinyxml document
    xmlDocument.SaveFile(filename.c_str());
    if (xmlDocument.Error()) {
        std::stringstream ss;
        ss << "Error while saving target file: " << filename << std::endl << xmlDocument.ErrorStr() << std::endl;
        throw std::runtime_error(ss.str());
    }
}

void TargetList::convertToUnit(units::Length targetLengthUnit)
{
    // landmark
    for (LandmarkTarget & target : landmark)
        target.convertToUnit(targetLengthUnit);
    // joint
    for (JointTarget & target : joint)
        target.convertToUnit(targetLengthUnit);
    // frameToFrame
    for (FrameToFrameTarget & target : frameToFrame)
        target.convertToUnit(targetLengthUnit);
    // frameToFrame
    for (FrameToFrameTarget & target : frameToFrame)
        target.convertToUnit(targetLengthUnit);
    // AnthropometricDimension
    for (AnthropometricDimensionTarget & target : anthropometricDimension)
        target.convertToUnit(targetLengthUnit);
    for (ControlPoint & target : controlPoint)
        target.convertToUnit(targetLengthUnit);
    for (HeightTarget & target : height)
        target.convertToUnit(targetLengthUnit);
}

void TargetList::convertToUnit(units::Mass targetMassUnit)
{
    for (WeightTarget & target : weight)
        target.convertToUnit(targetMassUnit);
}

void TargetList::convertToUnit(units::Age targetAgeUnit)
{
    for (AgeTarget & target : age)
        target.convertToUnit(targetAgeUnit);
}

} // namespace hbm
} // namespace piper
