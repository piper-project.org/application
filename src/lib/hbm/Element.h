/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef ELEMENT_H
#define ELEMENT_H

#include "MeshComponentContainer.h"

#include "types.h"

#include <string>
#include <assert.h>
#include <map>

#include <vtkCellType.h>

namespace piper {
	namespace hbm {
		class FEModel;
		//class Reader;

		// **********************************************************
		// Element
		// **********************************************************

		HBM_EXPORT enum  ElementType {ELT_HEXA, ELT_TETRA, ELT_PENTA, ELT_PYRA, ELT_QUAD, ELT_QUAD_THICK, ELT_TRI, ELT_TRI_THICK, ELT_BAR, ELT_UNDEFINED};

        // helper method to determine whether a given element is 3D or not
        inline bool is3DElType(ElementType const& type)
        {
            return type == ELT_HEXA || type == ELT_TETRA || type == ELT_PENTA || type == ELT_PYRA;
        }
        inline bool is3DElType(VTKCellType const& type)
        {
            return type == VTK_HEXAHEDRON || type == VTK_WEDGE || type == VTK_TETRA || type == VTK_PYRAMID;
        }
        inline bool is2DElType(VTKCellType const& type)
        {
            return type == VTK_QUAD || type == VTK_TRIANGLE;
        }
        inline bool is1DElType(VTKCellType const& type)
        {
            return type == VTK_LINE;
        }

		template <typename T>
		class Element : public MeshComponent< ElemDef, T>  {
        public:
            explicit Element(const Id& nid) : MeshComponent< ElemDef, T>(nid) {}

			//Element(const ElementType& type, const Id& eid, const ElemDef& elemdef);
			// ------------- interface --------------
            virtual void convId(std::unordered_map<Id,Id> const& map_id);

			// ------------- utilities --------------
            const ElementType& getType() const {return m_type;}
			static void setOrderForType( ElementType type, const VId& vorder) ;
			void orderElement();



		protected:
			ElementType m_type;

			virtual void setElemDef( const ElemDef& elemdef);

            Element(): MeshComponent< ElemDef, T>( ) {}
		
		private:
			//struct Order {
			//	Order(): hasOrder(false) {}
			//	bool hasOrder;
			//	std::vector< Id> order;
			//};

			typedef std::map<ElementType, std::vector< Id>> ElmentOrderCont;
			static ElmentOrderCont m_order;
		};


	}
}

#include "Element.inl" // [TL] TODO: do not include .inl in .h - this is the opposite

namespace piper {
	namespace hbm {

		class HBM_EXPORT Element3D: public Element<Element3D> {
			//friend class ModelReader;
            friend class FEModelVTK;
        public:
            Element3D();
            explicit Element3D(const Id& nid);
            virtual ~Element3D() {}

			static std::vector<ElemDef> extractElement2DfromElement3Ddefinition( std::vector<ElemDef>& elem3d);

			void setElemDef( const ElemDef& elemdef);
            void set( const ElemDef& elemdef) {setElemDef( elemdef);}
		protected:
			static ElementType getTypeFromN( std::size_t n);
		};

		class HBM_EXPORT Element2D: public Element<Element2D> {
			//friend class ModelReader;
            friend class FEModelVTK;
        public:
            Element2D();
            explicit Element2D(const Id& nid);
            virtual ~Element2D() {}

            /*!
            *  \brief define if a \a vecElemDef forms a closed surfaces. check if each edge is shared only by two 2D elements.
            * It can not detect if it forms one or several closed surfaces
            *
            *  \param elem2d: vector of \a ElemDef
            *  \return true if \a vecElemDef forms a closed surface
            */
            static bool checkClosedSurface(std::vector<ElemDef>& elem2d, const FEModel& femodel, bool& hasInvertedElements);
            static double computeVolume(std::vector<ElemDef>const& elem2d, const FEModel& femodel);
			void setElemDef( const ElemDef& elemdef);
            void set( const ElemDef& elemdef) {setElemDef( elemdef);}

		protected:
			static ElementType getTypeFromN( const int& n);


		};

		class HBM_EXPORT Element1D: public Element<Element1D> {
			//friend class ModelReader;
            friend class FEModelVTK;
        public:
            Element1D();
            explicit Element1D(const Id& nid);
            virtual ~Element1D() {}

			void setElemDef( const ElemDef& elemdef);
            void set( const ElemDef& elemdef) {setElemDef( elemdef);}

		protected:
			static ElementType getTypeFromN( const int& n);

		};






		class Elements3D : public MeshComponentContainer< Element3D> {

		public:
            Elements3D(): MeshComponentContainer< Element3D>( "ELEMENTS3D")  {}
		};

		class Elements2D : public MeshComponentContainer< Element2D> {

		public:
            Elements2D(): MeshComponentContainer< Element2D>( "ELEMENTS2D")  {}
		};

		class Elements1D : public MeshComponentContainer< Element1D> {

		public:
            Elements1D(): MeshComponentContainer< Element1D>( "ELEMENTS1D")  {}
		};

		//struct CompElem
		//{
		//	bool operator() ( ElemDef e1,ElemDef e2) ;
		//	static bool compareElem( ElemDef e1,ElemDef e2);
		//};


		inline bool TestEqualElemDef(ElemDef& e1, ElemDef& e2) {
			if (e1.size() != e2.size())
				return false;
			else
				return std::is_permutation(e1.begin(), e1.end(), e2.begin());
		}

        struct ComparatorEdgeElem {
            inline bool operator () (const ElemDef& e1, const ElemDef& e2) {
                if (std::is_permutation(e1.begin(), e1.end(), e2.begin()))
                    return false;
                else {
                    auto it1 = std::min_element(e1.begin(), e1.end());
                    auto it2 = std::min_element(e2.begin(), e2.end());
                    return *it1 < *it2;
                }
            }
        };

        struct OrderElemDef {
			bool operator () (const ElemDef& e1, const ElemDef& e2) const {
				if (e1.size() == e2.size() &&
					std::is_permutation(e1.begin(), e1.end(), e2.begin()))
					return false;
				ElemDef e1sort(e1), e2sort(e2);
				sort(e1sort.begin(), e1sort.end());
				sort(e2sort.begin(), e2sort.end());

				size_t mini = std::min(e1.size(), e2.size());
				for (size_t n = 0; n < mini; ++n) {
					if (e1sort[n] != e2sort[n]) {
						return e1sort[n] < e2sort[n];
					}
				}
				return e1sort.size() < e2sort.size();
			}
		};

		//inline bool CompareElemDefIsoSize(const ElemDef& e1, const ElemDef& e2) {
		//	ElemDef e1sort, e2sort;
		//	std::unique_copy(e1.begin(), e1.end(), std::back_inserter(e1sort));
		//	std::unique_copy(e2.begin(), e2.end(), std::back_inserter(e2sort));
		//	bool spy = false;
		//	for (size_t n = 0; n<e1sort.size(); ++n) {
		//		if (e1[n] != e2[n]) {
		//			spy = true;
		//			return e1[n] < e2[n];
		//		}
		//	}
		//}

	}
}



#endif
