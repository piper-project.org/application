/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
//#include "History.h"

namespace piper {
    namespace hbm {

        template<class T>
        History<T>::History() {
            init();
        }

        template<class T>
        History<T>::~History() {
        }

        template<class T>
        History<T>& History<T>::operator=(const History<T>& other)
        {
            if (this != &other)
            {
                // copy m_orderhistory and m_maphistory
                clear();
                for (auto it = other.m_maphistory.begin(); it != other.m_maphistory.end(); it++)
                {
                    m_maphistory[it->first] = std::make_shared<T>(*it->second);
                }
                // copy current active ID and set it as active
                m_active = m_maphistory[other.m_activestr];
                m_activestr = other.m_activestr;
            }
            return *this;
        }

        template<class T>
        void History<T>::init() {
            m_maphistory["init"] = std::make_shared<T>();
            setActive("init");
        }

        template<class T>
        void History<T>::clear() {
            for (auto it = m_maphistory.begin(); it != m_maphistory.end(); it++) {
                it->second->clear();
            }
            m_maphistory.clear();
            init();
        }

        template<class T>
        void History<T>::addNewHistory(std::string const& stringID) {
            if (m_maphistory.find(stringID) == m_maphistory.end()) {
                if (m_maphistory.size() > 0) {
                    m_maphistory[stringID] = std::make_shared<T>(*m_maphistory[m_activestr]);
                }
                else {
                    m_maphistory[stringID] = std::make_shared<T>();
                }
            }
            else {
                std::stringstream str;
                str << "Can not add new model : " << stringID << " already exists in history." << std::endl;
                throw std::runtime_error(str.str().c_str());
            }

        }

        template<class T>
        void History<T>::deleteHistory(std::vector<std::string> const& vnames) {
            for (auto const& cur : vnames) {
                if (m_maphistory.find(cur) != m_maphistory.end()) {
                    m_maphistory[cur]->clear();
                    m_maphistory.erase(cur);
                }
            }
        }


        template<class T>
        void History<T>::renameActiveHistory(std::string const& stringID) {
            if (m_maphistory.find(stringID) == m_maphistory.end()) {
                std::shared_ptr<T> nd = m_maphistory[m_activestr];
                m_maphistory.erase(m_activestr);
                m_maphistory[stringID] = nd;
                setActive(stringID);
            }
            else {
                std::stringstream str;
                str << "Can not rename current model : " << stringID << " already exists in history." << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
        }

        template<class T>
        void History<T>::setActive(std::string const& stringID) {
            if (m_maphistory.find(stringID) != m_maphistory.end()) {
                m_activestr = stringID;
                m_active = m_maphistory[stringID];
            }
            else {
                std::stringstream str;
                str << "Can not set model : " << stringID << " not exists in history." << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
        }


        template<class T>
        std::shared_ptr<T> History<T>::getHistory(std::string const& stringID) {
            if (m_maphistory.find(stringID) != m_maphistory.end())
                return m_maphistory[stringID];
            else
                return nullptr;
        }

        template<class T>
        std::shared_ptr<T> const History<T>::getHistory(std::string const& stringID) const {
            if (m_maphistory.find(stringID) != m_maphistory.end())
                return m_maphistory.at(stringID);
            else
                return nullptr;
        }


    }
}
