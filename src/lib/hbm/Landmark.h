/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_LANDMARK_H
#define PIPER_HBM_LANDMARK_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "BaseMetadata.h"
#include "Node.h"

namespace piper {
namespace hbm {

class FEModel;

/**
 * A Landmark defines a named point, its coordinate can be computed. There are several types of landmarks :
 * Type::POINT, Type::SPHERE and Type::BARYCENTER.
 *
 * \todo currently BARYCENTER computes the isobarycenter, extend it to barycentric coordinates
 *
 * @author Thomas Lemaire @date 2015
 */
class HBM_EXPORT Landmark: public BaseMetadataGroup {
public :
    enum class Type { POINT,     ///< defined by a single node, its coordinate is obvious
                      SPHERE,    ///< defined by several nodes, its coordinate is the center of the least-squared sphere computed throught the nodes
                      BARYCENTER, ///< defined by several nodes, its coordinate is the barycenter of the nodes
                    };

    Landmark();
    Landmark(std::string const& name, Type type=Type::POINT);
    Landmark(tinyxml2::XMLElement* element);

    void clear(FEModel& fem);
    Type type() const {return m_type;}
    void setType(Type type) {m_type = type;}
    
    /// <summary>
    /// Copies the provided node IDs and sets them as the defining points of this landmark
    /// </summary>
    /// <param name="nodes">IDs of nodes that should define this landmark. Warning - all previous nodes that defined this landmark will be discarded.</param>
    void setNodes(VId &nodes);

    /// compute the position of the landmark using nodes \a fem
    Coord position(FEModel const& fem) const;

    /// add the xml representation of this landmark under \a element
    void serializeXml(tinyxml2::XMLElement* element) const;
    virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;

    VId node;      ///< ids of the nodes


    const VId& get_groupNode() const { return groupNode; }
    VId& get_groupNode() { return groupNode; }
    void gatherNodesCoord(const FEModel &fem, VCoord& coords) const;

private :
    void parseXml(tinyxml2::XMLElement* element);
    void setLandmarkType(const char* type);
    bool getLandmarkTypeString(std::string& iLandmarkType) const;

    virtual bool has_groupNode() const { return true; }
    virtual bool has_groupElement1D() const { return false; }
    virtual bool has_groupElement2D() const { return false; }
    virtual bool has_groupElement3D() const { return false; }
    virtual bool has_groupGroup() const { return false; }

    Type m_type;

};

}
}

#endif // PIPER_HBM_LANDMARK_H
