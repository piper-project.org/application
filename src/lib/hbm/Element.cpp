/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Element.h"

#include "FEModel.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;
namespace piper { 
	namespace hbm {

		Element3D::Element3D(): Element<Element3D>( ) {};

		Element3D::Element3D(const Id& nid): Element<Element3D>( nid) {};


		void Element3D::setElemDef(const ElemDef& elemdef) {
			m_type = Element3D::getTypeFromN( elemdef.size());
			Element<Element3D>::setElemDef( elemdef);
		}


		vector<ElemDef> Element3D::extractElement2DfromElement3Ddefinition( vector<ElemDef>& elem3d) {
			// extract 2D free faces of this set
			ElemDef facecur;
			vector<ElemDef> vface;
			for (auto it=elem3d.begin(); it!=elem3d.end(); ++it) {
				ElemDef elemcur=*it;
				std::size_t nb=elemcur.size();
				if (nb==8) { // HEXA
					//face1
					facecur.clear();
					facecur.resize(4);
					facecur[0] = elemcur[3];
					facecur[1] = elemcur[2];
					facecur[2] = elemcur[1];
					facecur[3] = elemcur[0];
					vface.push_back(facecur);
					//face2
					facecur[0] = elemcur[5];
					facecur[1] = elemcur[6];
					facecur[2] = elemcur[7];
					facecur[3] = elemcur[4];
					vface.push_back(facecur);
					//face3
					facecur[0] = elemcur[2];
					facecur[1] = elemcur[6];
					facecur[2] = elemcur[5];
					facecur[3] = elemcur[1];
					vface.push_back(facecur);
					//face4
					facecur[0] = elemcur[4];
					facecur[1] = elemcur[7];
					facecur[2] = elemcur[3];
					facecur[3] = elemcur[0];
					vface.push_back(facecur);
					//face5
					facecur[0] = elemcur[1];
					facecur[1] = elemcur[5];
					facecur[2] = elemcur[4];
					facecur[3] = elemcur[0];
					vface.push_back(facecur);
					//face6
					facecur[0] = elemcur[7];
					facecur[1] = elemcur[6];
					facecur[2] = elemcur[2];
					facecur[3] = elemcur[3];
					vface.push_back(facecur);
				}
				else if (nb==6) { //PENTA 
					//face1
					facecur.clear();
					facecur.resize(4);
					facecur[0] = elemcur[0];
					facecur[1] = elemcur[3];
					facecur[2] = elemcur[4];
					facecur[3] = elemcur[1];
					vface.push_back(facecur);
					//face2
					facecur[0] = elemcur[0];
					facecur[1] = elemcur[2];
					facecur[2] = elemcur[5];
					facecur[3] = elemcur[3];
					vface.push_back(facecur);
					//face3
					facecur[0] = elemcur[1];
					facecur[1] = elemcur[4];
					facecur[2] = elemcur[5];
					facecur[3] = elemcur[2];
					vface.push_back(facecur);
					//face4
					facecur.clear();
					facecur.resize(3);
					facecur[0] = elemcur[0];
					facecur[1] = elemcur[1];
					facecur[2] = elemcur[2];
					vface.push_back(facecur);
					//face5
					facecur[0] = elemcur[3];
					facecur[1] = elemcur[5];
					facecur[2] = elemcur[4];
					vface.push_back(facecur);
				}
				else if (nb==4) { //TETA 
					//face1
					facecur.clear();
					facecur.resize(3);
					facecur[0] = elemcur[2];
					facecur[1] = elemcur[1];
					facecur[2] = elemcur[0];
					vface.push_back(facecur);
					//face2
					facecur[0] = elemcur[0];
					facecur[1] = elemcur[1];
					facecur[2] = elemcur[3];
					vface.push_back(facecur);
					//face3
					facecur[0] = elemcur[1];
					facecur[1] = elemcur[2];
					facecur[2] = elemcur[3];
					vface.push_back(facecur);
					//face4
					facecur[0] = elemcur[2];
					facecur[1] = elemcur[0];
					facecur[2] = elemcur[3];
					vface.push_back(facecur);
				}
			}
			// 
			vector<ElemDef> freeface;
			vector<ElemDef> vface_unique;
			if (vface.size()>0) {
				sort(vface.begin(), vface.end(), OrderElemDef());
				unique_copy(vface.begin(), vface.end(), std::back_inserter(vface_unique), TestEqualElemDef);

				std::vector<ElemDef>::iterator low, up;
				for (auto it = vface_unique.begin(); it != vface_unique.end(); ++it) {
					low = std::lower_bound(vface.begin(), vface.end(), *it, OrderElemDef());
					up = std::upper_bound(vface.begin(), vface.end(), *it, OrderElemDef());
					if ((up-low)==1)
						freeface.push_back(*it);
				}
			}
			return freeface;
		}

		ElementType Element3D::getTypeFromN(std::size_t n) {
			switch(n) {
			case 8: return ElementType::ELT_HEXA;
			case 4: return ElementType::ELT_TETRA;
			case 6: return ElementType::ELT_PENTA;
			case 5: return ElementType::ELT_PYRA;
			default: return ElementType::ELT_UNDEFINED;
			}
		}

		Element2D::Element2D(): Element<Element2D>( ) {};

		Element2D::Element2D(const Id& nid): Element<Element2D>( nid) {};


		void Element2D::setElemDef(const ElemDef& elemdef) {
			m_type = Element2D::getTypeFromN( (int) elemdef.size());
			Element<Element2D>::setElemDef( elemdef);
		}

		ElementType Element2D::getTypeFromN( const int& n){
			switch(n) {
			case 4: return ElementType::ELT_QUAD;
			case 3: return ElementType::ELT_TRI;
			default: return ElementType::ELT_UNDEFINED;
			}
		}

        bool Element2D::checkClosedSurface(std::vector<ElemDef>& elem2d, const FEModel& femodel, bool& hasInvertedElements) {
            // make only triangles
            vector<ElemDef> elem2d_tr;
            for (auto it = elem2d.begin(); it != elem2d.end(); ++it) {
                ElemDef elemcur = *it;
                size_t nb = elemcur.size();
                if (nb == 4) {
                    ElemDef tr1, tr2;
                    tr1.push_back(elemcur[0]);
                    tr1.push_back(elemcur[1]);
                    tr1.push_back(elemcur[2]);
                    tr2.push_back(elemcur[0]);
                    tr2.push_back(elemcur[2]);
                    tr2.push_back(elemcur[3]);
                    elem2d_tr.push_back(tr1);
                    elem2d_tr.push_back(tr2);
                }
                else if (nb == 3)
                    elem2d_tr.push_back(elemcur);
            }
            elem2d = elem2d_tr;
            elem2d_tr.clear();

            // extract 1D free edge of this set
            ElemDef edgecur;
            struct edgeinfo {
                void push(int const& n, ElemDef const& edge) {
                    elementnumber.push_back(n);
                    edgedef.push_back(edge);
                }
                std::vector<int> elementnumber;
                std::vector <ElemDef> edgedef;
            };
            std::map<ElemDef, edgeinfo, OrderElemDef> counts;
            //std::map<int, std::set<ElemDef, OrderElemDef>> element_edge_maps;
            //vector<ElemDef> vedge;
            int counter = 0;
            for (auto it = elem2d.begin(); it != elem2d.end(); ++it) {
                ElemDef elemcur = *it;
                size_t nb = elemcur.size();
                if (nb == 3) { // tri
                    //edge1
                    edgecur.clear();
                    edgecur.push_back(elemcur[0]);
                    edgecur.push_back(elemcur[1]);
                    counts[edgecur].push(counter, edgecur);
                    //element_edge_maps[counter].insert(edgecur);
                    //edge2
                    edgecur.clear();
                    edgecur.push_back(elemcur[1]);
                    edgecur.push_back(elemcur[2]);
                    counts[edgecur].push(counter, edgecur);
                    //element_edge_maps[counter].insert(edgecur);
                    //edge3
                    edgecur.clear();
                    edgecur.push_back(elemcur[2]);
                    edgecur.push_back(elemcur[0]);
                    counts[edgecur].push(counter, edgecur);
                    //element_edge_maps[counter].insert(edgecur);
                }
                counter++;
            }
            //std::cout << "vedge unique size " << counts.size() << std::endl;

            // check that all counts is eqaul to 2, otherwise the surface is not topologically closed
            for (auto const& cur : counts) {
                if (cur.second.elementnumber.size() != 2) {
                    //std::cout << "not closed surface" << std::endl;
                    return false;
                }
                // check that each edge is not traversed in same direction for the two shared elements
                ElemDef const& edge1 = cur.second.edgedef[0];
                ElemDef const& edge2 = cur.second.edgedef[1];
                if (edge1[0] == edge2[0] || edge1[1] == edge2[1]) {
                    //std::cout << "inverted elements" << std::endl;
                    hasInvertedElements = true;
                    return false;
                }
            }

            //// identify each close surface and compute their signed volume
            //std::vector<std::vector<ElemDef>> surface_pos, surface_neg;
            //std::vector<double> volume_pos, volume_neg;
            //// init while loop
            //std::set<int> list_e2D, add_e2D, new_add_e2d, list_e2D_open;
            //add_e2D.insert(element_edge_maps.begin()->first);
            ////element_edge_maps.erase(element_edge_maps.begin()->first);
            //// find adjacent e2D
            //while (element_edge_maps.size() > 0) {
            //    for (auto const& cur_e2D : add_e2D) {
            //        std::set<ElemDef, OrderElemDef> cur_list_edge = element_edge_maps[cur_e2D];
            //        //std::cout << "cur_list_edge " << cur_list_edge.size() << std::endl;
            //        for (auto const& cur_edge : cur_list_edge) {
            //            if (counts.find(cur_edge) != counts.end()) {
            //                //std::cout << "add e2d" << std::endl;
            //                if (add_e2D.find(counts[cur_edge].elementnumber[0]) == add_e2D.end()) new_add_e2d.insert(counts[cur_edge].elementnumber[0]);
            //                if (add_e2D.find(counts[cur_edge].elementnumber[1]) == add_e2D.end()) new_add_e2d.insert(counts[cur_edge].elementnumber[1]);
            //                counts.erase(cur_edge);
            //            }
            //        }
            //        element_edge_maps.erase(cur_e2D);
            //    }
            //    //std::cout << "added in surface : " << add_e2D.size() << std::endl;
            //    list_e2D.insert(add_e2D.begin(), add_e2D.end());
            //    add_e2D = new_add_e2d;
            //    new_add_e2d.clear();

            //    if (add_e2D.size() == 0) {//surface end (no more e2D to add)
            //        std::vector<ElemDef> newsurface;
            //        for (auto const& cur : list_e2D)
            //            newsurface.push_back(elem2d.at(cur));
            //        double surfacevolume = computeVolume(newsurface, femodel);
            //        if (surfacevolume > 0) {
            //            surface_pos.push_back(newsurface);
            //            volume_pos.push_back(surfacevolume);
            //        }
            //        else {
            //            surface_neg.push_back(newsurface);
            //            volume_neg.push_back(surfacevolume);
            //        }
            //        //std::cout << "find close surface with : " << newsurface.size() << " - " << elem2d.size() << " - " << surfacevolume << std::endl;
            //        list_e2D.clear();
            //        if (element_edge_maps.size() > 0) // need to re init
            //            add_e2D.insert(element_edge_maps.begin()->first);
            //    }
            //    else { //this is a open surface
            //        list_e2D_open.insert(add_e2D.begin(), add_e2D.end());
            //    }
            //}
            //numberClosePositiveSurface = (int)surface_pos.size();
            //numberCloseNegativeSurface = (int)surface_neg.size();
            ////std::cout << numberClosePositiveSurface << " - " << numberCloseNegativeSurface << std::endl;
            //    //if only surface negative
            //if (surface_pos.size() == 0 && surface_neg.size() > 0) {
            //    return false;
            //    //std::cout << "inversion" << std::endl;
            //    //std::vector<ElemDef> elem2D_pos;
            //    //for (auto & cursurf : surface_neg) {
            //    //    for (auto & cur : cursurf) {
            //    //        std::reverse(cur.begin(), cur.end());
            //    //        elem2D_pos.push_back(cur);
            //    //    }
            //    //}
            //    //elem2d = elem2D_pos;
            //    //numberClosePositiveSurface = numberCloseNegativeSurface;
            //    //numberCloseNegativeSurface = 0;
            //}
            //else if (surface_pos.size() > 0 && surface_neg.size() == 0) {
            //    return true;
            //}
            //else if (surface_pos.size() == 1 && surface_neg.size() != 0) { // one positive volume and others are negative and positive volume > max(abs(negative volume))
            //    for (auto const& cur : volume_neg) {
            //        if (std::abs(cur) > volume_pos[0])
            //            return false;
            //    }
            //    // isolated the positive volume as it is the biggest
            //    std::vector<ElemDef> elem2D_pos;
            //    //std::cout << "isolation" << std::endl;
            //    for (auto const& cur : surface_pos[0])
            //        elem2D_pos.push_back(cur);
            //    elem2d = elem2D_pos;
            //}
            //else { //mix of negative and positive, undefined conclusion
            //    return false;
            //}
            return true;
        }

        double Element2D::computeVolume(std::vector<ElemDef>const& elem2d, const FEModel& femodel) {
            double volume = 0;
            for (auto const& cur : elem2d) {
                Node const& p1 = femodel.getNode(cur[0]);
                Node const& p2 = femodel.getNode(cur[1]);
                Node const& p3 = femodel.getNode(cur[2]);
                double v321 = p3.getCoordX()*p2.getCoordY()*p1.getCoordZ();
                double v231 = p2.getCoordX()*p3.getCoordY()*p1.getCoordZ();
                double v312 = p3.getCoordX()*p1.getCoordY()*p2.getCoordZ();
                double v132 = p1.getCoordX()*p3.getCoordY()*p2.getCoordZ();
                double v213 = p2.getCoordX()*p1.getCoordY()*p3.getCoordZ();
                double v123 = p1.getCoordX()*p2.getCoordY()*p3.getCoordZ();
                volume += ((1.0 / 6.0) *  (-v321 + v231 + v312 - v132 - v213 + v123));
            }
            return volume;
        }

		Element1D::Element1D(): Element<Element1D>( ) {};

		Element1D::Element1D(const Id& nid): Element<Element1D>( nid) {};

		void Element1D::setElemDef(const ElemDef& elemdef) {
			m_type = Element1D::getTypeFromN( (int)elemdef.size());
			Element<Element1D>::setElemDef( elemdef);
		}

		ElementType Element1D::getTypeFromN( const int& n){
			switch(n) {
			case 2: return ElementType::ELT_BAR;
			default: return ElementType::ELT_UNDEFINED;
			}
		}


	}
}
