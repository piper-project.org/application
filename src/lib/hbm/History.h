/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HISTORY_H
#define PIPER_HISTORY_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <sstream>

namespace piper {
    namespace hbm {

        class HistoryBase {
        public:

            /// \brief rename active history
            /// 
            /// \param stringID: new string to identify active history 
            virtual void renameActiveHistory(std::string const& stringID) = 0;

            /// \brief erase history 
            /// 
            /// \param vnames: vector of history names to be erased
            virtual void  deleteHistory(std::vector<std::string> const& vnames) = 0;

            /// \brief set active a new node history with string identifier. It copies the top history in the new one.
            /// 
            /// \param stringID: The string that identifies the history 
            virtual void addNewHistory(std::string const& stringID) = 0;

            /// \brief set active the node history identified by a string identifier.
            /// 
            /// \param stringID: The string that identifies the node history to set active 
            virtual void setActive(std::string const& stringID) = 0;

        };

        /**
         * \todo add a textual description of an item in the history
         */
        template<class T>
        class History : public HistoryBase {
        public:

            History();
            ~History();

            History & operator=(const History& other);

            void clear();

            /// \brief set active a new node history with string identifier. It copies the top history in the new one.
            /// 
            /// \param stringID: The string that identifies the new node history 
            void addNewHistory(std::string const& stringID);

            /// \brief erase history 
            /// 
            /// \param vnames: vector of history names to be erased
            virtual void deleteHistory(std::vector<std::string> const& vnames);


            /// \brief set active the node history identified by a string identifier.
            /// 
            /// \param stringID: The string that identifies the node history to set active 
            virtual void setActive(std::string const& stringID);

            /// \brief rename active history
            /// 
            /// \param stringID: new string to identify active history 
            virtual void renameActiveHistory(std::string const& stringID);

            /// \brief get pointer to active history
            ///  
            /// \return pointer to active history 
            std::shared_ptr<T> getActive() { return m_active; }

            /// \brief get pointer to active history
            ///  
            /// \return pointer to active history 
            std::shared_ptr<T> const getActive() const { return m_active; }

            /// \brief get pointer to history with stringID
            /// 
			/// param: stringID identifier of the histroy
            /// \return pointer to history identified by \a stringID
            std::shared_ptr<T> getHistory(std::string const& stringID);

            /// \brief constt pointer to history with stringID
            ///  
            /// \return pointer to active history
            std::shared_ptr<T> const getHistory(std::string const& stringID) const;

            /// \brief Returns the current model identifier
            ///  
            /// \return string identifier 
            std::string const& getCurrent() const { return m_activestr; }

        private:
            std::unordered_map<std::string, std::shared_ptr<T>> m_maphistory;
            std::shared_ptr<T> m_active;
            std::string m_activestr;
            void init();
        };


    }
}
#include "History.inl"

#endif // PIPER_HISTORY_H
