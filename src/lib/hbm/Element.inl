/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

namespace piper {
	namespace hbm {

		template< typename T>
		void Element<T>::setElemDef( const ElemDef& elemdef)  {
				this->set(elemdef);
		}

		template< typename T>
		typename Element<T>::ElmentOrderCont Element<T>::m_order;

		template< typename T>
        void Element<T>::convId(std::unordered_map<Id,Id> const& map_id) {
            size_t s = m_def.size();
			for (size_t n = 0; n < s; n++)
				m_def[n] = map_id.at(m_def[n]);
		}

		template< typename T>
		void Element<T>::setOrderForType( ElementType type, const VId& vorder) {
			m_order[type] = vorder;
		}

		template< typename T>
		void Element<T>::orderElement() {
			ElmentOrderCont::const_iterator it = m_order.find(m_type);
			if (it != m_order.end()) {
				ElemDef newdef;
				VId order= m_order[m_type];
				for (size_t n = 0; n<this->get().size(); n++) {
					newdef.push_back(this->get().at(order.at(n) - 1));
				}
				this->set(newdef);
			}
		}

	}
}
