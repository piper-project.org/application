/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
namespace piper { 
	namespace hbm {


		template< typename Tcomponent>
		VId MeshComponentContainer< Tcomponent>::listId() const {
			std::vector<Id> vid;
			for(const_iterator it=begin(); it!=end(); ++it) {
				vid.push_back((*it)->getId());
			}
			return vid;
		}


		template< typename Tcomponent>
		void MeshComponentContainer< Tcomponent>::clear() {
			container.clear();
		}

		template< typename Tcomponent>
		MeshComponentContainer< Tcomponent>::MeshComponentContainer( const MeshComponentContainer< Tcomponent>& other ) {
            makeCopy(other);
		}

        template< typename Tcomponent>
        void MeshComponentContainer< Tcomponent>::makeCopy(const MeshComponentContainer& other) {
            for (MeshComponentContainer< Tcomponent>::const_iterator it = other.begin(); it != other.end(); ++it) {
                std::shared_ptr<Tcomponent> newT = std::make_shared<Tcomponent>(**it);
                setEntity(newT);
            }
        }

		template< typename Tcomponent>
		MeshComponentContainer< Tcomponent>& MeshComponentContainer< Tcomponent>::operator=(const MeshComponentContainer< Tcomponent>& other) {
			if (this != &other) {
				clear();
				for(MeshComponentContainer< Tcomponent>::const_iterator it=other.begin(); it!=other.end(); ++it) {
					setEntity( *it);
				}
			}
			return *this;
		}


		template< typename Tcomponent>
		MeshComponentContainer< Tcomponent>::~MeshComponentContainer() {
			//clear();
		}

		template< typename Tcomponent>
        void MeshComponentContainer< Tcomponent>::setEntity(std::shared_ptr<Tcomponent> entity) {
			if (!container.empty() && container.back()->getId() < entity->getId()) {
				container.push_back(entity);
			}
			else {
				MeshComponentContainer< Tcomponent>::iterator it = std::lower_bound(container.begin(), container.end(),
					entity->getId(), OrderContainer<Tcomponent>());
				if (it != container.end() && (*it)->getId() == entity->getId())
				{
                    container[it - container.begin()] = entity;
				}
				else container.insert(it, entity);
			}
		}

		template< typename Tcomponent>
		void MeshComponentContainer< Tcomponent>::delEntity( const Id id) {
			MeshComponentContainer< Tcomponent>::iterator it = std::lower_bound(container.begin(), container.end(),
				id, OrderContainer<Tcomponent>());
			if (it != container.end() && (*it)->getId() == id)
			{
				container.erase(it);
			}
		}

        template< typename Tcomponent>
        void MeshComponentContainer< Tcomponent>::delEntities(const std::vector<Id>& vid) 
        {
            if (vid.empty())
                return;
            // create a map of IDs to delete
            unsigned int size = *(std::max_element(vid.begin(), vid.end())) + 1;
            bool *toDelete = new bool[size];
            std::fill(toDelete, toDelete + size, false);
            for (Id cur : vid)
                toDelete[cur] = true;
            // Vector_container's erase takes a huge amount of time
            // instead we insert the entries we want to keep to a temporary vector and then clear the original one and reinsert them from the temp
            // -> about 60x faster in my experiments...still not great though
            Vector_container newContainer;
            for (auto it = container.begin(); it != container.end();it++)
            {
                Id &cur = (*it)->getId();
                if (cur >= size || !toDelete[cur])
                    newContainer.push_back(*it);
            }
            container.clear();
            container.insert(container.begin(), newContainer.begin(), newContainer.end());
            delete[] toDelete;
        }

		template< typename Tcomponent>
		void MeshComponentContainer< Tcomponent>::delEmpty( std::vector<Id>& vid) {
			std::vector<Id> curvid;
			MeshComponentContainer< Tcomponent>::iterator it;
			for (it=begin(); it!=end(); ++it) {
				if (curDef( it)->size() == 0) {
					curvid.push_back( curId( it));
				}
			}
			for (std::vector<Id>::const_iterator itv=curvid.begin(); itv!=curvid.end(); ++itv) 
				delEntity( *itv);
			vid.insert(vid.end(), curvid.begin(), curvid.end());
		}

		template< typename Tcomponent>
		void MeshComponentContainer< Tcomponent>::delEmpty( ) {
			std::vector<Id> vid;
			delEmpty( vid);
		}

		template< typename Tcomponent>
        std::shared_ptr<Tcomponent> MeshComponentContainer< Tcomponent>::getEntity(const Id& id) {
			MeshComponentContainer< Tcomponent>::iterator it = std::lower_bound(container.begin(), container.end(),
				id, OrderContainer<Tcomponent>());
            if (it != container.end() && (*it)->getId() == id) {
                return (*it);
			}
			else return nullptr;
		}

		template< typename Tcomponent>
        const std::shared_ptr<Tcomponent> MeshComponentContainer< Tcomponent>::getEntity(const Id& id) const {
			MeshComponentContainer< Tcomponent>::const_iterator it = std::lower_bound(container.begin(), container.end(),
				id, OrderContainer<Tcomponent>());
			if (it != container.end() && (*it)->getId() == id) {
				return (*it);
			}
			else
				return NULL;
		}

		template< typename Tcomponent>
		size_t MeshComponentContainer< Tcomponent>::size() const {
			return container.size();
		}



		template< typename Tcomponent>
		Id MeshComponentContainer< Tcomponent>::getMinId() const {
            if (container.empty())
                return 0;
            else
                return (*container.cbegin())->getId();
		}

		template< typename Tcomponent>
		Id MeshComponentContainer< Tcomponent>::getMaxId() const {
            if (container.empty())
                return 0;
            else
				return (*container.crbegin())->getId();
		}

		template< typename Tcomponent>
        void MeshComponentContainer< Tcomponent>::convId(std::unordered_map<Id,Id> const& map_id) {
			for (iterator it=begin(); it!=end(); ++it) {
				(*it)->convId(map_id);
			}	
			update();
		}


		template< typename Tcomponent>
		bool MeshComponentContainer< Tcomponent>::isValidId( const Id id) const {
			MeshComponentContainer< Tcomponent>::const_iterator it = std::lower_bound(container.begin(), container.end(),
				id, OrderContainer<Tcomponent>());
			if (it != container.end() && (*it)->getId() == id)
				return true;
			else return false;
		}


		template< typename Tcomponent>
		void MeshComponentContainer< Tcomponent>::update() {
			//need to sort vector
			std::sort(container.begin(), container.end(), ComparatorContainer<Tcomponent>);
		}

		//template< typename Tcomponent>
		//void MeshComponentContainer< Tcomponent>::concatenate(MeshComponentContainer<Tcomponent>& other) {
		//	for(MeshComponentContainer<Tcomponent>::const_iterator it=other.begin(); it!=other.end(); ++it) {
		//		this->setEntity( it->second);
		//	}
		//}
	}

}
