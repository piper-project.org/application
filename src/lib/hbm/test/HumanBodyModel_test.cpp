/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/HumanBodyModel.h"

#include <boost/filesystem.hpp>

#include "TestHelper.h"

using namespace piper::hbm;

class HumanBodyModel_test : public ::testing::Test {

protected:

	virtual void SetUp() {
	}
};

TEST(HumanBodyModel_test, read) {
	HumanBodyModel hbm;
	hbm.read("model_01.pmd");
	EXPECT_EQ(36, hbm.fem().getNbNode());
	EXPECT_EQ(4, hbm.metadata().entities().size());
}
TEST(HumanBodyModel_test, doubleread) {
	HumanBodyModel hbm;
	hbm.read("model_01.pmd");
	EXPECT_EQ(36, hbm.fem().getNbNode());
	EXPECT_EQ(4, hbm.metadata().entities().size());
    EXPECT_EQ(4, hbm.metadata().landmarks().size());
	EXPECT_EQ(3, hbm.metadata().joints().size());
	hbm.read("model_01.pmd");
	EXPECT_EQ(36, hbm.fem().getNbNode());
	EXPECT_EQ(4, hbm.metadata().entities().size());
    EXPECT_EQ(4, hbm.metadata().landmarks().size());
	EXPECT_EQ(3, hbm.metadata().joints().size());
}

TEST(HumanBodyModel_test, setSource) {
	HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");
	EXPECT_EQ(44, hbm.fem().getNbNode());
    EXPECT_EQ(piper::units::Length::mm, hbm.metadata().lengthUnit());
    EXPECT_EQ(4, hbm.metadata().entities().size());
    EXPECT_EQ(6, hbm.metadata().landmarks().size());
    EXPECT_EQ(3, hbm.metadata().joints().size());
    EXPECT_EQ(5, hbm.metadata().hbmParameters().size());
    EXPECT_EQ(4, hbm.metadata().genericmetadatas().size());
}

TEST(HumanBodyModel_test, new_model_parse) {
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");
	EXPECT_EQ(44, hbm.fem().getNbNode());
    EXPECT_EQ(piper::units::Length::mm, hbm.metadata().lengthUnit());
    // entities
	EXPECT_EQ(4, hbm.metadata().entities().size());
	EXPECT_EQ(1, hbm.metadata().entity("Entity_4_Auto").get_groupElement1D().size());
    EXPECT_EQ(1, hbm.metadata().entity("Entity_4_Auto").get_groupElement2D().size());
    EXPECT_EQ(1, hbm.metadata().entity("Entity_4_Auto").get_groupElement3D().size());
    EXPECT_EQ(0, hbm.metadata().entity("Entity_2").get_groupElement1D().size());
    EXPECT_EQ(2, hbm.metadata().entity("Entity_2").get_groupElement2D().size());
    EXPECT_EQ(0, hbm.metadata().entity("Entity_2").get_groupElement3D().size());
	//landmarks
    EXPECT_EQ(6, hbm.metadata().landmarks().size());
    EXPECT_TRUE(hbm.metadata().hasLandmark("pointcoord"));
    EXPECT_TRUE(hbm.metadata().landmark("pointcoord").type() == Landmark::Type::POINT);
    Coord coord = hbm.metadata().landmark("pointcoord").position(hbm.fem());
    EXPECT_NEAR(coord[0], 12, 1e-3);
    EXPECT_NEAR(coord[1], 13, 1e-3);
    EXPECT_NEAR(coord[2], 14, 1e-3);
    EXPECT_TRUE(hbm.metadata().hasLandmark("barycoord"));
    EXPECT_TRUE(hbm.metadata().landmark("barycoord").type() == Landmark::Type::BARYCENTER);
    coord = hbm.metadata().landmark("barycoord").position(hbm.fem());
    EXPECT_NEAR(coord[0], 55, 1e-3);
    EXPECT_NEAR(coord[1], 110, 1e-3);
    EXPECT_NEAR(coord[2], 165, 1e-3);
    EXPECT_TRUE(hbm.metadata().hasLandmark("LandMark_1_point"));
	EXPECT_TRUE( hbm.metadata().landmark("LandMark_1_point").type()==Landmark::Type::POINT);
	EXPECT_TRUE( hbm.metadata().hasLandmark("LandMark_11_point"));
	EXPECT_TRUE( hbm.metadata().landmark("LandMark_11_point").type()==Landmark::Type::POINT);
	EXPECT_TRUE( hbm.metadata().hasLandmark("Landmark_2"));
	EXPECT_TRUE( hbm.metadata().landmark("Landmark_2").type()==Landmark::Type::SPHERE);
    //control point
    EXPECT_EQ(4, hbm.metadata().interactionControlPoints().size());
    EXPECT_TRUE(hbm.metadata().hasInteractionControlPoint("CP_0"));
    EXPECT_TRUE(hbm.metadata().hasInteractionControlPoint("CP_1"));
    EXPECT_EQ(1, hbm.metadata().interactionControlPoint("CP_0")->getGroupNodeId().size());
    EXPECT_EQ(10, hbm.metadata().interactionControlPoint("CP_1")->getGroupNodeId().size());
    EXPECT_EQ(5, hbm.metadata().interactionControlPoint("CP_2")->getGroupNodeId().size());
    EXPECT_EQ(1, hbm.metadata().interactionControlPoint("CP_0")->getNbControlPt());
    EXPECT_EQ(10, hbm.metadata().interactionControlPoint("CP_1")->getNbControlPt());
    EXPECT_EQ(5, hbm.metadata().interactionControlPoint("CP_2")->getNbControlPt());
    EXPECT_EQ(-45, hbm.metadata().interactionControlPoint("CP_2")->getWeight()[4]);
    EXPECT_EQ(0, hbm.metadata().interactionControlPoint("CP_2")->getAssociationBones()[4]);
    EXPECT_EQ(1, hbm.metadata().interactionControlPoint("CP_2")->getAssociationSkin()[4]);

    //joint
	EXPECT_EQ(3, hbm.metadata().joints().size());
	EXPECT_TRUE( hbm.metadata().hasJoint("Joint_1"));
	std::array<bool,6> dof1=hbm.metadata().joint("Joint_1").getDof();
	std::array<bool,6> dof1_check={false,false,false,true,true,true};
	for (int n=0; n<6; n++) {
		EXPECT_TRUE(dof1.at(n)==dof1_check.at(n));
	}
	EXPECT_TRUE( hbm.metadata().hasJoint("Joint_2"));
	EXPECT_TRUE( hbm.metadata().hasJoint("Joint_3_generalized_spring"));
	Eigen::Quaternion<double> q = hbm.fem().getFrameOrientation( hbm.metadata().joint("Joint_3_generalized_spring").getEntity1FrameId());
    EXPECT_DOUBLE_EQ(1.0, q.w());
    EXPECT_DOUBLE_EQ(0.0, q.x());
    EXPECT_DOUBLE_EQ(0.0, q.y());
    EXPECT_DOUBLE_EQ(0.0, q.z());
	//contact
	EXPECT_EQ(2, hbm.metadata().contacts().size());
	EXPECT_TRUE( hbm.metadata().hasContact("contact_1"));
    EXPECT_EQ(EntityContact::Type::SLIDING, hbm.metadata().contact("contact_1").type());
    EXPECT_EQ("Entity_1", hbm.metadata().contact("contact_1").entity1());
    EXPECT_EQ("Entity_2", hbm.metadata().contact("contact_1").entity2());
    EXPECT_EQ(6, hbm.metadata().contact("contact_1").group1());
    EXPECT_EQ(14, hbm.metadata().contact("contact_1").group2());
	EXPECT_TRUE( hbm.metadata().contact("contact_1").keepThickness());
	EXPECT_TRUE( hbm.metadata().hasContact("contact_2"));
    EXPECT_EQ(EntityContact::Type::SLIDING, hbm.metadata().contact("contact_2").type());
    EXPECT_EQ("Entity_1", hbm.metadata().contact("contact_2").entity1());
    EXPECT_EQ("Entity_4_Auto", hbm.metadata().contact("contact_2").entity2());
    EXPECT_EQ(6, hbm.metadata().contact("contact_2").group1());
    EXPECT_EQ(7, hbm.metadata().contact("contact_2").group2());
	EXPECT_FALSE( hbm.metadata().contact("contact_2").keepThickness());
    EXPECT_DOUBLE_EQ(0.5, hbm.metadata().contact("contact_2").thickness());
    // generic metadata
    EXPECT_EQ(4, hbm.metadata().genericmetadatas().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("Gen_1").get_groupNode().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("Gen_1").get_groupElement1D().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("Gen_1").get_groupElement2D().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("Gen_1").get_groupElement3D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("group1D").get_groupNode().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("group1D").get_groupElement1D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("group1D").get_groupElement2D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("group1D").get_groupElement3D().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("LandMark_2_barycenter").get_groupNode().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("LandMark_2_barycenter").get_groupElement1D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("LandMark_2_barycenter").get_groupElement2D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("LandMark_2_barycenter").get_groupElement3D().size());
    EXPECT_EQ(1, hbm.metadata().genericmetadata("Gen_2_node").get_groupNode().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("Gen_2_node").get_groupElement1D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("Gen_2_node").get_groupElement2D().size());
    EXPECT_EQ(0, hbm.metadata().genericmetadata("Gen_2_node").get_groupElement3D().size());
    // anthropometry
    EXPECT_EQ(28, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(77, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1749, hbm.metadata().anthropometry().height.value());
}


TEST(HumanBodyModel_test, write) {
	HumanBodyModel hbm01("model_01.pmd");
	boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
	tmp /= "piper_hbm_test_HumanBodyModel.pmd";
    hbm01.history().renameCurrentModel("hist_01");
    EXPECT_TRUE(hbm01.history().getCurrent() == "hist_01");
	hbm01.write(tmp.string());
	hbm01.clear();

    HumanBodyModel hbm02(tmp.string());
    EXPECT_TRUE(hbm02.history().getCurrent() == "hist_01");
    hbm02.history().addNewHistory("hist_02");
    hbm02.history().addNewHistory("hist_03");
    std::vector<std::string> check_history = hbm02.history().getHistoryList();
    EXPECT_TRUE(check_history[0] == "hist_01");
    EXPECT_TRUE(check_history[1] == "hist_02");
    EXPECT_TRUE(check_history[2] == "hist_03");
    EXPECT_TRUE(check_history.size() == 3);
    hbm02.history().setCurrentModel("hist_01");
    hbm02.history().addNewHistory("hist_04");
    check_history = hbm02.history().getHistoryList();
    EXPECT_TRUE(check_history[0] == "hist_01");
    EXPECT_TRUE(check_history[3] == "hist_04");
    EXPECT_TRUE(check_history.size() == 4);

    // modify a node in current history
    hbm02.history().setCurrentModel("hist_01");
    VId nidlist = hbm02.fem().getNodes().listId();
    double value_test_before = hbm02.fem().getNode(nidlist[0]).get()[2];
    hbm02.fem().getNode(nidlist[0]).get()[2] += 10.0;
    double value_test_after = hbm02.fem().getNode(nidlist[0]).get()[2];
    // modify height metadata
    double age_before = hbm02.metadata().anthropometry().height.value();
    double age_after = 2150;
    hbm02.metadata().anthropometry().height.setValue(age_after);
    // 
    EXPECT_EQ(hbm02.fem().getNodes("hist_04").getEntity(nidlist[0])->get()[2], value_test_before);
    EXPECT_EQ(hbm02.fem().getNodes("hist_01").getEntity(nidlist[0])->get()[2], value_test_after);
    EXPECT_EQ(hbm02.metadata().anthropometry("hist_04").height.value(), age_before);
    EXPECT_EQ(hbm02.metadata().anthropometry("hist_01").height.value(), age_after);
    //
    hbm02.history().setCurrentModel("hist_01");
    boost::filesystem::path tmp01 = boost::filesystem::temp_directory_path();
    tmp01 /= "piper_hbm_test_HumanBodyModel_hist_01.pmd";
    hbm02.write(tmp01.string());
    //
    hbm02.history().setCurrentModel("hist_04");
    boost::filesystem::path tmp04 = boost::filesystem::temp_directory_path();
    tmp04 /= "piper_hbm_test_HumanBodyModel_hist_04.pmd";
    hbm02.write(tmp04.string());
    hbm02.clear();
    //
    HumanBodyModel hbm_hist01(tmp01.string());
    EXPECT_EQ(hbm_hist01.history().getCurrent() , "hist_01");
    EXPECT_NEAR(hbm_hist01.fem().getNodes().getEntity(nidlist[0])->get()[2], value_test_after, 1e-4);
    EXPECT_EQ(hbm_hist01.metadata().anthropometry().height.value(), age_after);
    //
    HumanBodyModel hbm_hist04(tmp04.string());
    EXPECT_EQ(hbm_hist04.history().getCurrent() , "hist_04");
    EXPECT_NEAR(hbm_hist04.fem().getNodes().getEntity(nidlist[0])->get()[2], value_test_before, 1e-4);
    EXPECT_EQ(hbm_hist04.metadata().anthropometry().height.value(), age_before);


}

// test to check that a newly created model from a vendor file is equivalent to its saved version in the piper format
TEST(HumanBodyModel_test, new_write_read)
{
	HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");

	//read ls-dyna
	std::size_t nbNode = hbm.fem().getNbNode();
	double coordX = hbm.fem().get<Node>( "*NODE",7020563)->getCoordX();
	double coordY = hbm.fem().get<Node>( "*NODE",7020563)->getCoordY();
	double coordZ = hbm.fem().get<Node>( "*NODE",7020563)->getCoordZ();
	std::size_t nbElement1D = hbm.fem().getNbElement1D();
	std::size_t nbElement2D = hbm.fem().getNbElement2D();
	std::size_t nbElement3D = hbm.fem().getNbElement3D();
	std::size_t nbGroupNode = hbm.fem().getNbGroupNode();
	std::size_t SizecontentGroupNode1 = hbm.fem().get<GroupNode>("*SET_NODE_LIST_TITLE", 1999990)->size();
	std::size_t SizecontentGroupNode2 = hbm.fem().get<GroupNode>("*SET_NODE_LIST_TITLE", 1)->size();
	std::size_t nbGroupElement1D = hbm.fem().getNbGroupElements1D();
	std::size_t SizecontentGroupE1d = hbm.fem().get<GroupElements1D>("*SET_BEAM_TITLE", 10)->size();
	std::size_t nbGroupElement2D = hbm.fem().getNbGroupElements2D();
	std::size_t SizecontentGroupE2d = hbm.fem().get<GroupElements2D>("*SET_SHELL_LIST_TITLE", 11)->size();
	std::size_t nbGroupElement3D = hbm.fem().getNbGroupElements3D();
	std::size_t SizecontentGroupE3d = hbm.fem().get<GroupElements3D>("*SET_SOLID_TITLE", 1)->size();
	std::size_t nbGroupGroup = hbm.fem().getNbGroupGroups();
	std::size_t SizecontentGroupG = hbm.fem().get<GroupGroups>("*SET_PART_LIST_TITLE", 1)->size();
	std::size_t SizeFrame = hbm.fem().getNbFrames();

	boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
	tmp /= "model_new_write_read.pmd";
	hbm.write(tmp.string());
	hbm.clear();

	HumanBodyModel hbm2;
	hbm2.read(tmp.string());



	EXPECT_EQ(nbNode, hbm2.fem().getNbNode());
    NodePtr nd = hbm2.fem().get<Node>("*NODE", IdKey(7020563));
    EXPECT_TRUE(nd!=nullptr);
	//EXPECT_TRUE(Node::searchId(id, "*NODE", 7020563));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020563))->getCoordX() - coordX));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020563))->getCoordY() - coordY));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020563))->getCoordZ() - coordZ));
	EXPECT_EQ(nbElement1D, hbm2.fem().getNbElement1D());
	EXPECT_EQ(nbElement2D, hbm2.fem().getNbElement2D());
	EXPECT_EQ(nbElement3D, hbm2.fem().getNbElement3D());
	EXPECT_EQ(nbGroupNode, hbm2.fem().getNbGroupNode());
	EXPECT_EQ(SizecontentGroupNode1, hbm2.fem().get<GroupNode>( "*SET_NODE_LIST_TITLE",1999990)->size());
	EXPECT_EQ(SizecontentGroupNode2, hbm2.fem().get<GroupNode>( "*SET_NODE_LIST_TITLE",1)->size());
	EXPECT_EQ(nbGroupElement1D, hbm2.fem().getNbGroupElements1D());
	EXPECT_EQ(SizecontentGroupE1d, hbm2.fem().get<GroupElements1D>( "*SET_BEAM_TITLE",10)->size());
	EXPECT_EQ(nbGroupElement2D, hbm2.fem().getNbGroupElements2D());
	EXPECT_EQ(SizecontentGroupE2d, hbm2.fem().get<GroupElements2D>( "*SET_SHELL_LIST_TITLE",11)->size());
	EXPECT_EQ(nbGroupElement3D, hbm2.fem().getNbGroupElements3D());
	EXPECT_EQ(SizecontentGroupE3d, hbm2.fem().get<GroupElements3D>( "*SET_SOLID_TITLE",1)->size());
	EXPECT_EQ(nbGroupGroup, hbm2.fem().getNbGroupGroups());
	EXPECT_EQ(SizecontentGroupG, hbm2.fem().get<GroupGroups>( "*SET_PART_LIST_TITLE",1)->size());
	EXPECT_EQ(SizeFrame, hbm2.fem().getNbFrames());
    Frames const& frames = hbm2.fem().getFrames();
    for (auto const& frame : frames) {
        if (!frame->hasOnlyOrigin())
            EXPECT_TRUE(frame->isCompletelyDefined());
    }
 
    EXPECT_TRUE(hbm2.fem().get<GroupElements3D>("*ELEMENT_SOLID", 7000005)->isPart());

}

TEST(getEntityEnvelop, HumanBodyModel_test) {
	HumanBodyModel hbm;
	hbm.read("model_01.pmd");
	Entity const& entity = hbm.metadata().entity("Entity_4_Auto");

	std::vector<ElemDef> v3d, v2d;
    for (auto it = entity.get_groupElement3D().begin(); it != entity.get_groupElement3D().end(); ++it) {
		VId sid = hbm.fem().getGroupElements3D(*it).get();
		for(auto it=sid.begin(); it!=sid.end(); ++it) {
			v3d.push_back( hbm.fem().getElement3D(*it).get());	
		}
	}
	v2d = Element3D::extractElement2DfromElement3Ddefinition( v3d);
    std::vector<ElemDef> const& env2d = entity.getEnvelopElements(hbm.fem());
	EXPECT_EQ( 9, v2d.size());
	EXPECT_EQ( 22, env2d.size());
    EXPECT_EQ( 18, entity.getEnvelopNodes(hbm.fem()).size());
    EXPECT_EQ( 18, entity.getEnvelopNodesToIndexMap(hbm.fem()).size());
    EXPECT_EQ( 7, entity.getEnvelopNodesToIndexMap(hbm.fem()).at(15));
    ElemDef vcheck0 = { 11, 10, 9, 8 };
    ElemDef vcheck3 = { 17, 9, 10 };
    ElemDef vcheck7 = { 13, 14, 15, 12 };
    ElemDef vcheck8 = { 16, 14, 13 };
    EXPECT_TRUE(VectorMatch(vcheck0, v2d[0]));
    EXPECT_TRUE(VectorMatch(vcheck3, v2d[3]));
    EXPECT_TRUE(VectorMatch(vcheck7, v2d[7]));
    EXPECT_TRUE(VectorMatch(vcheck8, v2d[8]));
}

TEST(getEntityNodesIds, HumanBodyModel_test) {
    HumanBodyModel hbm;
    hbm.read("model_01.pmd");
    Entity const& entity = hbm.metadata().entity("Entity_4_Auto");
    VId vid = entity.getEntityNodesIds(hbm.fem());
    EXPECT_TRUE(vid.size()==20);
}

TEST(HumanBodyModel_test, updated_model)
{
	HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");

	//read ls-dyna
	size_t nbNode = hbm.fem().getNbNode();
    double coordX = hbm.fem().get<Node>("*NODE", 7020603)->getCoordX();
    double coordY = hbm.fem().get<Node>("*NODE", 7020603)->getCoordY();
    double coordZ = hbm.fem().get<Node>("*NODE", 7020603)->getCoordZ();
	// check freenode used in frame
	//
	std::size_t nbElement1D = hbm.fem().getNbElement1D();
	std::size_t nbElement2D = hbm.fem().getNbElement2D();
	std::size_t nbElement3D = hbm.fem().getNbElement3D();
	std::size_t nbGroupNode = hbm.fem().getNbGroupNode();
	std::size_t SizecontentGroupNode1 = hbm.fem().get<GroupNode>("*SET_NODE_LIST_TITLE", 1999990)->size();
	std::size_t SizecontentGroupNode2 = hbm.fem().get<GroupNode>("*SET_NODE_LIST_TITLE", 1)->size();
	std::size_t nbGroupElement1D = hbm.fem().getNbGroupElements1D();
	std::size_t SizecontentGroupE1d = hbm.fem().get<GroupElements1D>("*SET_BEAM_TITLE", 10)->size();
	std::size_t nbGroupElement2D = hbm.fem().getNbGroupElements2D();
	std::size_t SizecontentGroupE2d = hbm.fem().get<GroupElements2D>("*SET_SHELL_LIST_TITLE", 11)->size();
	std::size_t nbGroupElement3D = hbm.fem().getNbGroupElements3D();
	std::size_t SizecontentGroupE3d = hbm.fem().get<GroupElements3D>("*SET_SOLID_TITLE", 1)->size();
	std::size_t nbGroupGroup = hbm.fem().getNbGroupGroups();
	std::size_t SizecontentGroupG = hbm.fem().get<GroupGroups>("*SET_PART_LIST_TITLE", 1)->size();
	std::size_t SizeFrame = hbm.fem().getNbFrames();

	boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
	hbm.exportModel(tmp.string());
	hbm.clear();

	HumanBodyModel hbm2;
	tmp /= "model_01.dyn";
    hbm2.setSource("model_01_description_LSDyna.pmr", tmp.string());
	EXPECT_EQ(nbNode, hbm2.fem().getNbNode());
    NodePtr nd = hbm2.fem().get<Node>("*NODE", IdKey(7020603));
    EXPECT_TRUE(nd != nullptr);
    //EXPECT_TRUE(Node::searchId(id, "*NODE", 7020563));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020603))->getCoordX() - coordX));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020603))->getCoordY() - coordY));
    EXPECT_GE(1e-5, std::abs(hbm2.fem().get<Node>("*NODE", IdKey(7020603))->getCoordZ() - coordZ));
	EXPECT_EQ(nbElement1D, hbm2.fem().getNbElement1D());
	EXPECT_EQ(nbElement2D, hbm2.fem().getNbElement2D());
	EXPECT_EQ(nbElement3D, hbm2.fem().getNbElement3D());
	EXPECT_EQ(nbGroupNode, hbm2.fem().getNbGroupNode());
	EXPECT_EQ(SizecontentGroupNode1, hbm2.fem().get<GroupNode>( "*SET_NODE_LIST_TITLE",1999990)->size());
	EXPECT_EQ(SizecontentGroupNode2, hbm2.fem().get<GroupNode>( "*SET_NODE_LIST_TITLE",1)->size());
	EXPECT_EQ(nbGroupElement1D, hbm2.fem().getNbGroupElements1D());
	EXPECT_EQ(SizecontentGroupE1d, hbm2.fem().get<GroupElements1D>( "*SET_BEAM_TITLE",10)->size());
	EXPECT_EQ(nbGroupElement2D, hbm2.fem().getNbGroupElements2D());
	EXPECT_EQ(SizecontentGroupE2d, hbm2.fem().get<GroupElements2D>( "*SET_SHELL_LIST_TITLE",11)->size());
	EXPECT_EQ(nbGroupElement3D, hbm2.fem().getNbGroupElements3D());
	EXPECT_EQ(SizecontentGroupE3d, hbm2.fem().get<GroupElements3D>( "*SET_SOLID_TITLE",1)->size());
	EXPECT_EQ(nbGroupGroup, hbm2.fem().getNbGroupGroups());
	EXPECT_EQ(SizecontentGroupG, hbm2.fem().get<GroupGroups>( "*SET_PART_LIST_TITLE",1)->size());
	EXPECT_EQ(SizeFrame, hbm2.fem().getNbFrames());
}


TEST(HumanBodyModel_test, readmodel_andreloadbaseline) {
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");
    // get coordinates of nodes
    double xmodif, ymodif, zmodif;
    xmodif = hbm.fem().getNode(0).getCoordX() + 10.0;
    ymodif = hbm.fem().getNode(0).getCoordY() + 20.0;
    zmodif = hbm.fem().getNode(0).getCoordZ() + 30.0;
    hbm.fem().getNode(0).setCoord(xmodif, ymodif, zmodif);
    std::string key;
    piper::hbm::IdKey idkey;
    hbm.fem().getInfoId<Node>(0, key, idkey);
    // write file with modified node
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_HumanBodyModel.pmd";
    hbm.write(tmp.string());
    hbm.clear();
    // open file with a reload of the baseline model (with unmodified node)
	hbm.read(tmp.string());
	hbm.reloadFEMesh();
    // check history
    std::vector<std::string> history = hbm.history().getHistoryList();
    EXPECT_EQ(2, history.size());
    EXPECT_EQ("*root", history[0]);
    // 
    hbm.history().setCurrentModel("*root");
    NodePtr noderoot = hbm.fem().get<Node>(key, idkey);
    hbm.history().setCurrentModel(history[1]);
    NodePtr nodemodif = hbm.fem().get<Node>(key, idkey);
    EXPECT_NEAR(noderoot->getCoordX() + 10.0, nodemodif->getCoordX(),1e-5);
    EXPECT_NEAR(noderoot->getCoordY() + 20.0, nodemodif->getCoordY(),1e-5);
    EXPECT_NEAR(noderoot->getCoordZ() + 30.0, nodemodif->getCoordZ(),1e-5);
    hbm.clear();
}

TEST(HumanBodyModel_test, geometric_model) {
    HumanBodyModel hbm;
    hbm.setSource("./RefGeom_description.pmr");
    EXPECT_EQ(piper::units::Length::dm, hbm.metadata().lengthUnit());
    // FEM
    EXPECT_EQ((301+1010+773), hbm.fem().getNodes().size());
    EXPECT_EQ((572 + 1976 + 1497), hbm.fem().getElements2D().size());
    EXPECT_EQ(3, hbm.fem().getGroupElements2D().size());
    EXPECT_EQ(1976, hbm.fem().get < GroupElements2D>("obj", IdKey("RefGeom_CCTs635_BP091_Humerus_R"))->size());
    EXPECT_EQ(572, hbm.fem().get < GroupElements2D>("obj", IdKey("RefGeom_CCTs635_BP095_Radius_R"))->size());
    EXPECT_EQ(1497, hbm.fem().get < GroupElements2D>("obj", IdKey("RefGeom_CCTs635_BP093_Ulna_R"))->size());
    // metadata
    EXPECT_EQ(2, hbm.metadata().entities().size());
    EXPECT_EQ(1, hbm.metadata().entity("Entity_1").get_groupElement2D().size());
    EXPECT_EQ(2, hbm.metadata().entity("Entity_2").get_groupElement2D().size());
    EXPECT_EQ(2, hbm.metadata().landmarks().size());
    EXPECT_EQ(Landmark::Type::POINT, hbm.metadata().landmark("land_1").type());
    EXPECT_EQ(1, hbm.metadata().landmark("land_1").node.size());
    EXPECT_EQ(Landmark::Type::BARYCENTER, hbm.metadata().landmark("land_2").type());
    EXPECT_EQ(4, hbm.metadata().landmark("land_2").node.size());
}

TEST(HumanBodyModel_test, EntityEnvelop) {
    HumanBodyModel hbm;
    hbm.setSource("model_entity_envelop.pmr");
    //
    std::vector<ElemDef> elem_entity_1, elem_entity_2, elem_entity_3, elem_entity_env1, elem_entity_env2;
    double vol1, vol2, vol_env1, vol_env2, vol_env3;
    Entity const& entity_1 = hbm.metadata().entity("Entity_1_2Douter");
    entity_1.getEntityElemDef(hbm.fem(), elem_entity_1);
    vol1 = Element2D::computeVolume(elem_entity_1, hbm.fem());
    elem_entity_env1 = entity_1.getEnvelopElements(hbm.fem());
    EXPECT_TRUE(entity_1.isEnvelopValid);
    vol_env1 = Element2D::computeVolume(elem_entity_env1, hbm.fem());
    EXPECT_NEAR(vol1, 8.0, 1e-4);
    EXPECT_NEAR(vol_env1, 8.0, 1e-4);
    //
    Entity const& entity_2 = hbm.metadata().entity("Entity_2_2Dinner");
    entity_2.getEntityElemDef(hbm.fem(), elem_entity_2);
    vol2 = Element2D::computeVolume(elem_entity_2, hbm.fem());
    elem_entity_env2 = entity_2.getEnvelopElements(hbm.fem());
    EXPECT_TRUE(entity_2.isEnvelopValid);
    vol_env2 = Element2D::computeVolume(elem_entity_env2, hbm.fem());
    EXPECT_NEAR(vol2, -8.0,1e-4);
    EXPECT_NEAR(vol_env2, 8.0, 1e-4);
    //
    Entity const& entity_3 = hbm.metadata().entity("Entity_3_3D");
    entity_3.getEntityElemDef(hbm.fem(), elem_entity_3);
    elem_entity_3 = entity_3.getEnvelopElements(hbm.fem());
    EXPECT_TRUE(entity_3.isEnvelopValid);
    vol_env3 = Element2D::computeVolume(elem_entity_3, hbm.fem());
    EXPECT_NEAR(vol_env3, 8.0, 1e-4);
}

TEST(HumanBodyModel_test, history) {
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");
    EXPECT_TRUE(hbm.history().getCurrent() == "import_model");
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_HumanBodyModel_history.pmd";
    hbm.write(tmp.string());
    hbm.clear();
    EXPECT_TRUE(hbm.history().getCurrent() == "init");
    EXPECT_TRUE(hbm.history().getHistoryList().size() == 1);



}

TEST(HumanBodyModel_test, history_unicity) {
    HumanBodyModel hbm;
    hbm.read("model_01.pmd");
    hbm.history().addNewHistory("test_10");
    hbm.history().addNewHistory("test_1b");
    hbm.history().addNewHistory("test");
    hbm.history().addNewHistory("test_123");
    hbm.history().addNewHistory("test_test_124");
    std::string newhistoryname = hbm.history().getValidHistoryName("test");
    EXPECT_TRUE(newhistoryname == "test_124");
    hbm.history().clear();
}

TEST(EntityEnvelop, getInfoId_NodeEntity) {
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");

    Entity const& entity = hbm.metadata().entity("Entity_4_Auto");
    VId vid = entity.getEntityNodesIds(hbm.fem());

    std::vector<std::string> key;
    std::vector<IdKey> id;
    hbm.fem().getInfoId<Node>(vid, key, id);
    int check=0;
    for (auto const& cur : id)
        check += cur.id;
    EXPECT_EQ(350, check);
}


TEST(HumanBodyModel_test, ImportExportMetadataLSDyna) {
    std::string pmrImportFile = "model_01_description_LSDyna_importTest.pmr";
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");

    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_HumanBodyModel.pmr";
    hbm.metadata().exportMetadata(tmp.string(), hbm.fem());

    hbm.metadata().importMetadata(pmrImportFile, hbm.fem());
    // check metadata after import
    EXPECT_EQ(38, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(2, hbm.metadata().entities().size());
    //landmarks
    EXPECT_EQ(2, hbm.metadata().landmarks().size());
    //control point
    EXPECT_EQ(2, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(1, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(1, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(2, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(2, hbm.metadata().hbmParameters().size());
    // anthropometry
    EXPECT_EQ(32, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(50, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1500, hbm.metadata().anthropometry().height.value());

    hbm.metadata().importMetadata(tmp.string(), hbm.fem());
    //reimporte exported metadata + check
    EXPECT_EQ(44, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(4, hbm.metadata().entities().size());
    //landmarks
    EXPECT_EQ(6, hbm.metadata().landmarks().size());
    //control point
    EXPECT_EQ(4, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(3, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(2, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(4, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(5, hbm.metadata().hbmParameters().size());
    // anthropometry
    EXPECT_EQ(28, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(77, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1749, hbm.metadata().anthropometry().height.value());
}

TEST(HumanBodyModel_test, ImportExportMetadataPamCrash) {
    std::string pmrImportFile = "model_01_description_PamCrash_importTest.pmr";
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_PamCrash.pmr");

    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_HumanBodyModel.pmr";
    hbm.metadata().exportMetadata(tmp.string(), hbm.fem());

    hbm.metadata().importMetadata(pmrImportFile, hbm.fem());
    // check metadata after import
    EXPECT_EQ(35, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(1, hbm.metadata().entities().size());
    EXPECT_TRUE(hbm.metadata().hasEntity("'Entity_4_Auto'"));
    //landmarks
    EXPECT_EQ(1, hbm.metadata().landmarks().size());
    EXPECT_TRUE(hbm.metadata().hasLandmark("Landmark_Node2"));
    EXPECT_TRUE(hbm.metadata().landmark("Landmark_Node2").type() == Landmark::Type::BARYCENTER);
    //control point
    EXPECT_EQ(0, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(0, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(0, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(0, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(2, hbm.metadata().hbmParameters().size());
    EXPECT_TRUE(hbm.metadata().hasHbmParameter("Density_1"));
    EXPECT_TRUE(hbm.metadata().hasHbmParameter("ModuleG_1"));
    // anthropometry
    EXPECT_EQ(32, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(50, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1500, hbm.metadata().anthropometry().height.value());

    hbm.metadata().importMetadata(tmp.string(), hbm.fem());
    //reimporte exported metadata + check
    EXPECT_EQ(35, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(2, hbm.metadata().entities().size());
    EXPECT_TRUE(hbm.metadata().hasEntity("'Entity_4_Auto'"));
    EXPECT_TRUE(hbm.metadata().hasEntity("'Entity_5_Auto'"));
    //landmarks
    EXPECT_EQ(1, hbm.metadata().landmarks().size());
    EXPECT_TRUE(hbm.metadata().hasLandmark("Landmark_Node"));
    EXPECT_TRUE(hbm.metadata().landmark("Landmark_Node").type() == Landmark::Type::POINT);
    //control point
    EXPECT_EQ(0, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(0, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(0, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(0, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(3, hbm.metadata().hbmParameters().size());
    EXPECT_TRUE(hbm.metadata().hasHbmParameter("Density_1"));
    EXPECT_TRUE(hbm.metadata().hasHbmParameter("ModuleG_1"));
    EXPECT_TRUE(hbm.metadata().hasHbmParameter("Thickness_1"));
    // anthropometry
    EXPECT_EQ(28, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(77, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1749, hbm.metadata().anthropometry().height.value());
}

TEST(HumanBodyModel_test, ExportMetadataImportModelLSDyna) {
    std::string pmrImportFile = "model_01_description_LSDyna_importTest.pmr";
    HumanBodyModel hbm;
    hbm.setSource("model_01_description_LSDyna.pmr");

    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_HumanBodyModel.pmr";
    hbm.metadata().exportMetadata(tmp.string(), hbm.fem());

    hbm.clear();
    hbm.setSource(pmrImportFile);

    // check metadata after import
    EXPECT_EQ(38, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(2, hbm.metadata().entities().size());
    //landmarks
    EXPECT_EQ(2, hbm.metadata().landmarks().size());
    //control point
    EXPECT_EQ(2, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(1, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(1, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(2, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(2, hbm.metadata().hbmParameters().size());
    // anthropometry
    EXPECT_EQ(32, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(50, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1500, hbm.metadata().anthropometry().height.value());

    hbm.metadata().importMetadata(tmp.string(), hbm.fem());
    //reimporte exported metadata + check
    EXPECT_EQ(44, hbm.fem().getNbNode());
    // entities
    EXPECT_EQ(4, hbm.metadata().entities().size());
    //landmarks
    EXPECT_EQ(6, hbm.metadata().landmarks().size());
    //control point
    EXPECT_EQ(4, hbm.metadata().interactionControlPoints().size());
    //joint
    EXPECT_EQ(3, hbm.metadata().joints().size());
    //contact
    EXPECT_EQ(2, hbm.metadata().contacts().size());
    // generic metadata
    EXPECT_EQ(4, hbm.metadata().genericmetadatas().size());
    //hbmparameters
    EXPECT_EQ(5, hbm.metadata().hbmParameters().size());
    // anthropometry
    EXPECT_EQ(28, hbm.metadata().anthropometry().age.value());
    EXPECT_EQ(77, hbm.metadata().anthropometry().weight.value());
    EXPECT_EQ(1749, hbm.metadata().anthropometry().height.value());
}


TEST(HumanBodyModel_test, ImportExportMetadataRadioss) {
	std::string pmrImportFile = "model_cylinder_Radioss_importTest.pmr";
	HumanBodyModel hbm;
	hbm.setSource(pmrImportFile);

	boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
	tmp /= "piper_hbm_test_HumanBodyModel.pmr";
	hbm.metadata().exportMetadata(tmp.string(), hbm.fem());

	hbm.metadata().importMetadata(pmrImportFile, hbm.fem());
	// check metadata after import
	EXPECT_EQ(1126, hbm.fem().getNbNode());
	// entities
	EXPECT_EQ(2, hbm.metadata().entities().size());
	EXPECT_TRUE(hbm.metadata().hasEntity("Entity_2"));
	//landmarks
	EXPECT_EQ(3, hbm.metadata().landmarks().size());
	EXPECT_TRUE(hbm.metadata().hasLandmark("testsphere"));
	EXPECT_TRUE(hbm.metadata().landmark("testsphere").type() == Landmark::Type::SPHERE);
	// generic metadata
	EXPECT_EQ(1, hbm.metadata().genericmetadatas().size());

	//reimporte exported metadata + check
	hbm.metadata().importMetadata(tmp.string(), hbm.fem());
	EXPECT_EQ(1126, hbm.fem().getNbNode());
	// entities
	EXPECT_EQ(2, hbm.metadata().entities().size());
	EXPECT_TRUE(hbm.metadata().hasEntity("Entity_2"));
	//landmarks
	EXPECT_EQ(3, hbm.metadata().landmarks().size());
	EXPECT_TRUE(hbm.metadata().hasLandmark("testsphere"));
	EXPECT_TRUE(hbm.metadata().landmark("testsphere").type() == Landmark::Type::SPHERE);
	// generic metadata
	EXPECT_EQ(1, hbm.metadata().genericmetadatas().size());
}