/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <boost/filesystem.hpp>

#include "hbm/target.h"
#include "TestHelper.h"

#include "units/units.h"

using namespace piper;
using namespace piper::hbm;

class target_test : public ::testing::Test {

protected:

    virtual void SetUp()
    {
        targetList.read("target_01.ptt");
    }

    void checkSize()
    {
        EXPECT_EQ(15,targetList.size());
    }

    //void checkAnthropometricLengthTarget()
    //{
    //    EXPECT_EQ(1, targetList.anthropometricDimension.size());
    //    AnthropometricDimensionTarget const& target = targetList.anthropometricDimension.front();
    //    EXPECT_EQ("Left_femur_length", target.name());
    //    EXPECT_EQ(units::Length::m, target.unit());
    //    EXPECT_EQ(300., target.value());
    //}

    void checkFixedBone()
    {
        EXPECT_EQ(1, targetList.fixedBone.size());
        FixedBoneTarget const& target = targetList.fixedBone.front();
        EXPECT_EQ("Fixed_pelvis", target.name());
        EXPECT_EQ("Pelvic_bone", target.bone);
    }

    void checkLandmarkTarget()
    {
        EXPECT_EQ(2, targetList.landmark.size());
        LandmarkTarget const& target = targetList.landmark.front();
        EXPECT_EQ("Tip_of_the_nose", target.name());
        EXPECT_EQ(units::Length::m, target.unit());
        EXPECT_EQ("Tip_of_the_nose", target.landmark);
        EXPECT_EQ(2, target.value().size());
        EXPECT_TRUE(target.value().find(0)!=target.value().end());
        EXPECT_DOUBLE_EQ(50, target.value().at(0));
        EXPECT_TRUE(target.value().find(1)!=target.value().end());
        EXPECT_DOUBLE_EQ(0, target.value().at(1));
        EXPECT_TRUE(target.value().find(2)==target.value().end());
        EXPECT_EQ(LandmarkTarget::MaskType({true,true,false}), target.mask());
        // check paramter for kriging
        EXPECT_EQ(0.5, target.getKrigingUseBone());
        EXPECT_EQ(0.5, target.getKrigingUseBone());
        LandmarkTarget const& targetlast = targetList.landmark.back();
        EXPECT_EQ("LandmarkKrigingParam", targetlast.name());
        EXPECT_EQ(0.321, targetlast.getKrigingUseBone());
        EXPECT_EQ(0.123, targetlast.getKrigingUseSkin());


    }

    void checkJointTarget()
    {
        EXPECT_EQ(1, targetList.joint.size());
        JointTarget const& target = targetList.joint.front();
        EXPECT_EQ("Left_hip", target.name());
        EXPECT_EQ(units::Length::cm, target.unit());
        EXPECT_EQ("Left_hip_joint", target.joint);
        EXPECT_TRUE(target.value().find(0)==target.value().end());
        ASSERT_TRUE(target.value().find(1)!=target.value().end());
        EXPECT_DOUBLE_EQ(45., target.value().at(1));
        EXPECT_TRUE(target.value().find(2)==target.value().end());
        ASSERT_TRUE(target.value().find(3)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.5, target.value().at(3));
        EXPECT_TRUE(target.value().find(4)==target.value().end());
        ASSERT_TRUE(target.value().find(5)!=target.value().end());
        EXPECT_DOUBLE_EQ(0., target.value().at(5));
        EXPECT_EQ(JointTarget::MaskType({false,true,false,true,false,true}),target.mask());
    }

    void checkFrameToFrameTarget()
    {
        EXPECT_EQ(2, targetList.frameToFrame.size());
        auto it = targetList.frameToFrame.begin();
        FrameToFrameTarget const& target = *it;
        EXPECT_EQ("Head_position", target.name());
        EXPECT_EQ(units::Length::mm, target.unit());
        ASSERT_FALSE(target.isRelative);
        EXPECT_EQ("W_origin", target.frameSource);
        EXPECT_EQ("B_skull", target.frameTarget);

        EXPECT_TRUE(target.value().find(0)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.15, target.value().at(0));
        EXPECT_TRUE(target.value().find(1)==target.value().end());
        EXPECT_TRUE(target.value().find(2)==target.value().end());
        ASSERT_TRUE(target.value().find(3)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.2, target.value().at(3));
        EXPECT_TRUE(target.value().find(4)==target.value().end());
        EXPECT_TRUE(target.value().find(5)==target.value().end());
        EXPECT_EQ(FrameToFrameTarget::MaskType({true,false,false,true,false,false}),target.mask());

        ++it;
        FrameToFrameTarget const& target2 = *it;
        EXPECT_EQ("Head_position_relative", target2.name());
        EXPECT_TRUE(target2.isRelative);
    }

	void checkScalingParameterTarget()
	{
		EXPECT_EQ(1, targetList.scalingparameter.size());
		ScalingParameterTarget const& target = targetList.scalingparameter.front();
		EXPECT_EQ("YoungModulusMaterialtarget", target.name());
		double checkvalue = 0.52;
		EXPECT_EQ(checkvalue, target.value()[0]);
	}

    void checkAnthropometricDimensiontarget()
    {
        EXPECT_EQ(3, targetList.anthropometricDimension.size());
        AnthropometricDimensionTarget const& target = targetList.anthropometricDimension.front();
        EXPECT_EQ("CHEST_DEPTH", target.name());
        double checkvalue = 276.2486;
        EXPECT_NEAR(checkvalue, target.value(), 1e-4);
        std::string checkposture = "standingANSUR88";
        EXPECT_EQ(checkposture, target.posture());
        EXPECT_TRUE(target.hasAbsoluteValue());
        EXPECT_FALSE(target.hasScaleValue());
        auto it = targetList.anthropometricDimension.end();
        AnthropometricDimensionTarget const& targetlast = *(--it);
        EXPECT_EQ("Left_femur_length", targetlast.name());
        EXPECT_TRUE(targetlast.hasAbsoluteValue());
        EXPECT_TRUE(targetlast.hasScaleValue());
        EXPECT_EQ(300, targetlast.value());
        EXPECT_EQ(0.3, targetlast.scale());
    }

    void checkAgeWeightHeighttarget()
    {
        EXPECT_EQ(1, targetList.age.size());
        EXPECT_EQ(1, targetList.weight.size());
        EXPECT_EQ(1, targetList.height.size());
        EXPECT_EQ(targetList.age.front().value(), 48);
        EXPECT_EQ(targetList.weight.front().value(), 50.4);
        EXPECT_EQ(targetList.height.front().value(), 188.6);
        EXPECT_EQ(targetList.age.front().unit(), units::Age::year);
        EXPECT_EQ(targetList.weight.front().unit(), units::Mass::kg);
        EXPECT_EQ(targetList.height.front().unit(), units::Length::cm);
    }

    void checkControlPointTarget() {
        EXPECT_EQ(2, targetList.controlPoint.size());
        ControlPoint const& target1 = targetList.controlPoint.front();
        EXPECT_EQ("ControlPointTarget_0", target1.name());
        ControlPoint const& target2 = targetList.controlPoint.back();
        EXPECT_EQ("ControlPointTarget_1", target2.name());
        EXPECT_EQ(1, target1.value().size());
        EXPECT_EQ(2, target2.value().size());
        EXPECT_EQ(target1.unit(), units::Length::mm);
        EXPECT_EQ(target2.unit(), units::Length::m);
    }


	
	TargetList targetList;
};

TEST_F(target_test, size) {
    checkSize();
}

TEST_F(target_test, clear) {
    checkSize();
    targetList.clear();
    EXPECT_EQ(0,targetList.size());
}

//TEST_F(target_test, AnthropometricLengthTarget) {
//    checkAnthropometricLengthTarget();
//}

TEST_F(target_test, FixedBoneTarget) {
    checkFixedBone();
}

TEST_F(target_test, ScalingParameterTarget) {
	checkScalingParameterTarget();
}


TEST_F(target_test, LandmarkTarget) {
	checkLandmarkTarget();
}

TEST_F(target_test, JointTarget) {
    checkJointTarget();
}

TEST_F(target_test, FrameToFrameTarget) {
    checkFrameToFrameTarget();
}

TEST_F(target_test, AnthropometricDimensionTarget) {
    checkAnthropometricDimensiontarget();
}

TEST_F(target_test, ControlPointTarget) {
    checkControlPointTarget();
}

TEST_F(target_test, age_weight_height_target) {
    checkAgeWeightHeighttarget();
}

TEST_F(target_test, write) {
    // write current target to tmp
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_target.xml";
    targetList.write(tmp.string());

    // read it
    targetList.clear();
    targetList.read(tmp.string());
    checkSize();
    checkFixedBone();
    checkLandmarkTarget();
    checkJointTarget();
    checkFrameToFrameTarget();
	checkScalingParameterTarget();
    checkAnthropometricDimensiontarget();
    checkAgeWeightHeighttarget();
    checkControlPointTarget();
}

TEST_F(target_test, convertToUnit) {
    targetList.convertToUnit(units::Length::mm);
    targetList.convertToUnit(units::Mass::g);
    targetList.convertToUnit(units::Age::month);
    //{
    //    AnthropometricLengthTarget const& target = targetList.anthropometricDimension.front();
    //    EXPECT_EQ(units::Length::mm, target.unit());
    //    EXPECT_DOUBLE_EQ(300000.,target.value());
    //}
    {
        LandmarkTarget const& target = targetList.landmark.front();
        EXPECT_EQ(units::Length::mm, target.unit());

//        EXPECT_EQ(LandmarkTarget::ValueType({{0,50000},{1,0}}),target.value());
        ASSERT_TRUE(target.value().find(0)!=target.value().end());
        EXPECT_DOUBLE_EQ(50000., target.value().at(0));
        ASSERT_TRUE(target.value().find(1)!=target.value().end());
        EXPECT_DOUBLE_EQ(0., target.value().at(1));
        ASSERT_TRUE(target.value().find(2)==target.value().end());
    }
    {
        JointTarget const& target = targetList.joint.front();
        EXPECT_EQ(units::Length::mm, target.unit());
        EXPECT_TRUE(target.value().find(0)==target.value().end());
        ASSERT_TRUE(target.value().find(1)!=target.value().end());
        EXPECT_DOUBLE_EQ(450., target.value().at(1));
        EXPECT_TRUE(target.value().find(2)==target.value().end());
        ASSERT_TRUE(target.value().find(3)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.5, target.value().at(3));
        EXPECT_TRUE(target.value().find(4)==target.value().end());
        ASSERT_TRUE(target.value().find(5)!=target.value().end());
        EXPECT_DOUBLE_EQ(0., target.value().at(5));
    }
    {
        FrameToFrameTarget const& target = targetList.frameToFrame.front();
        EXPECT_EQ(units::Length::mm, target.unit());
        ASSERT_TRUE(target.value().find(0)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.15, target.value().at(0));
        EXPECT_TRUE(target.value().find(1)==target.value().end());
        EXPECT_TRUE(target.value().find(2)==target.value().end());
        ASSERT_TRUE(target.value().find(3)!=target.value().end());
        EXPECT_DOUBLE_EQ(0.2, target.value().at(3));
        EXPECT_TRUE(target.value().find(4)==target.value().end());
        EXPECT_TRUE(target.value().find(5)==target.value().end());
    }
    {
        AnthropometricDimensionTarget const& target = targetList.anthropometricDimension.front();
        EXPECT_EQ(units::Length::mm, target.unit());
        EXPECT_NEAR(276248.6, target.value(), 1e-4);
    }

    {
        EXPECT_EQ(1, targetList.age.size());
        EXPECT_EQ(1, targetList.weight.size());
        EXPECT_EQ(1, targetList.height.size());
        EXPECT_EQ(targetList.age.front().unit(), units::Age::month);
        EXPECT_EQ(targetList.weight.front().unit(), units::Mass::g);
        EXPECT_EQ(targetList.height.front().unit(), units::Length::mm);
        EXPECT_EQ(targetList.age.front().value(), 48*12);
        EXPECT_EQ(targetList.weight.front().value(), 50400);
        EXPECT_EQ(targetList.height.front().value(), 1886);
    }

}

