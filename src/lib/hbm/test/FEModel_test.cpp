/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/FEModel.h"
#include "hbm/ParserModelFile.h"

#include <vector>
#include <algorithm>

#include <boost/filesystem.hpp>

#pragma warning(push, 0) // disable all warnings from gtest
#include "gtest/gtest-param-test.h"
#pragma warning(pop)

#include "TestHelper.h"

using namespace piper::hbm;



class FEModel_test : public ::testing::TestWithParam<const char*> {

protected:

	virtual void SetUp() {

		if (strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_fixed", 12)==0) {
			modelfile="model_01.dyn";
			rulefile = "../formatrules/Formatrules_LSDyna_Fixed.pfr";
			modeldescrfile = "model_01_description_LSDyna.pmr";
			//Node
			kwnode = "*NODE";
			nbnode=36;
			kwe3d.push_back("*ELEMENT_SOLID");
			kwe2d.push_back("*ELEMENT_SHELL");
			kwe2d.push_back("*ELEMENT_SHELL_THICKNESS");
			kwe1d.push_back("*ELEMENT_BEAM");
			kwe1d.push_back("*ELEMENT_DISCRETE");
			kwgn.push_back("*SET_NODE_LIST_TITLE");
			nbgn=5;
			kwge1d.push_back("*SET_BEAM_TITLE");
			nbg1d=4;
			kwge2d.push_back("*SET_SHELL_LIST_TITLE");
			nbg2d=3;
			kwge3d.push_back("*SET_SOLID_TITLE");
			nbg3d=3;
			kwgg.push_back("*SET_PART_LIST_TITLE");
			nbgg=3;
			// nb frame
			nbfr=4;
            kwframe.push_back("*DEFINE_COORDINATE_NODES_TITLE");
            kwframe.push_back("*DEFINE_COORDINATE_NODES");
		}
		else if (strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_separator", 16)==0) {
			modelfile="model_01_separator.dyn";
			rulefile = "../formatrules/Formatrules_LSDyna_Separator.pfr";
			modeldescrfile = "model_01_description_LSDyna_Separator.pmr";
			//Node
			kwnode = "*NODE";
			nbnode=36;
			kwe3d.push_back("*ELEMENT_SOLID");
			kwe2d.push_back("*ELEMENT_SHELL");
			kwe2d.push_back("*ELEMENT_SHELL_THICKNESS");
			kwe1d.push_back("*ELEMENT_BEAM");
			kwe1d.push_back("*ELEMENT_DISCRETE");
			kwgn.push_back("*SET_NODE_LIST_TITLE");
			nbgn=5;
			kwge1d.push_back("*SET_BEAM_TITLE");
			nbg1d=4;
			kwge2d.push_back("*SET_SHELL_LIST_TITLE");
			nbg2d=3;
			kwge3d.push_back("*SET_SOLID_TITLE");
			nbg3d=3;
			kwgg.push_back("*SET_PART_LIST_TITLE");
			nbgg=3;
			// nb frame
			nbfr=4;
            kwframe.push_back("*DEFINE_COORDINATE_NODES_TITLE");
            kwframe.push_back("*DEFINE_COORDINATE_NODES");
		}
		else if (strncmp(TestWithParam<const char*>::GetParam(), "Pamcrash", 8)==0) {
			modelfile="model_01.pc";
			rulefile = "../formatrules/Formatrules_PamCrash.pfr";
			modeldescrfile = "model_01_description_PamCrash.pmr";
			//Node
			kwnode = "NODE";
			nbnode=35;
			kwe3d.push_back("SOLID");
			kwe2d.push_back("SHELL");
			kwe1d.push_back("BEAM");
			kwe1d.push_back("SPRING/");
			kwgn.push_back("GROUP");
			nbgn=5;
			kwge1d.push_back("GROUP");
			nbg1d=4;
			kwge2d.push_back("GROUP");
			nbg2d=3;
			kwge3d.push_back("GROUP");
			nbg3d=3;
			kwgg.push_back("GROUP");
			nbgg=12;
			// nb frame
			nbfr=5;
            kwframe.push_back("FRAME");
            kwframe.push_back("FRAME");
		}	

		//~FEModel_test() {
		//	delete p;
		//}
		femodel.clear();
		p = new parser::ParserModelFile( rulefile, modeldescrfile);
        femodel.parse(p, modelfile, std::vector<std::string>());
        std::map<piper::hbm::Id, std::pair<std::string, IdKey>> tmp = femodel.computePiperIdToIdKeyMap<FEModelParameter>();
		// expected values
		nodeid=7020563; //node FE id to test and cord
		coordx= 3.84251;
		coordy=-94.412003;
		coordz=13.2169;
		// nb element by type
		nbe3d=3;
		nbe2d=4;
		nbe1d=3;
		// e1d to test
		e1did.push_back(7043463);
		e1did.push_back(123);
		Id tmp1[] = { 7513573, 17019074 };
		elem1d.push_back( VId ( tmp1, tmp1+2 ));
		Id tmp2[] = { 7048469, 17019074 };
		elem1d.push_back( VId ( tmp2, tmp2+2 ));
		//frame id to test
        frameid.push_back(60010);
        frameid.push_back(1992101);

	}

    void TearDown() {
        femodel.clear();
    }

	void checkNode() {
		EXPECT_EQ(nbnode, femodel.getNbNode());
		EXPECT_EQ(0, femodel.getNodes().listId()[0]);
        NodePtr cur = femodel.get<Node>(kwnode, IdKey(nodeid));
		EXPECT_TRUE(cur!=nullptr);
		EXPECT_GE(1e-5,std::abs(cur->getCoordX() - coordx));
        EXPECT_GE(1e-5, std::abs(cur->getCoordY() - coordy));
        EXPECT_GE(1e-5, std::abs(cur->getCoordZ() - coordz));
        NodePtr nd = femodel.get<Node>(kwnode, 0);
        EXPECT_TRUE(nd == nullptr);
    }

	void checkElement() {
		//e1d
		EXPECT_EQ(nbe1d, femodel.getNbElement1D());
		EXPECT_EQ(0, femodel.getElements1D().listId()[0]);
		for(size_t n=0; n<kwe1d.size(); n++) {
            Element1DPtr cur = femodel.get<Element1D>(kwe1d[n], IdKey(e1did[n]));
            EXPECT_TRUE(cur!=nullptr);
			//EXPECT_TRUE(Element1D::searchId(id, kwe1d[n], e1did[n]));
            VId elem = cur->get();
			EXPECT_TRUE(elem1d[n].size() == elem.size());
			VId elemcheck;
			for(size_t nn=0; nn<elem1d[n].size(); nn++) {
				elemcheck.push_back( femodel.get<Node>(  kwnode,elem1d[n][nn])->getId());
			}
			EXPECT_TRUE(VectorMatch(elemcheck, elem));
		}
		//e2d
		EXPECT_EQ(nbe2d, femodel.getNbElement2D());
		EXPECT_EQ(0, femodel.getElements2D().listId()[0]);
		//EXPECT_TRUE( Element2D::checkMapSize());
		//EXPECT_EQ(femodel.getNbElement2D(), Element2D::getMapSize());
		// to complete
		//e3d
		EXPECT_EQ(nbe3d, femodel.getNbElement3D());
		EXPECT_EQ(0, femodel.getElements3D().listId()[0]);
		//EXPECT_TRUE( Element3D::checkMapSize());
		//EXPECT_EQ(femodel.getNbElement3D(), Element3D::getMapSize());
		//to complete
	}

	void checkGroup() {
		EXPECT_EQ(nbgn, femodel.getNbGroupNode());
		EXPECT_EQ(nbg1d, femodel.getNbGroupElements1D());
		EXPECT_EQ(nbg2d, femodel.getNbGroupElements2D());
		EXPECT_EQ(nbg3d, femodel.getNbGroupElements3D());
		EXPECT_EQ(nbgg, femodel.getNbGroupGroups());
		//EXPECT_TRUE( GroupGroups::checkMapSize());
		//EXPECT_EQ(nbgn+nbg1d+nbg2d+nbg3d+nbgg, GroupGroups::getMapSize());
		VId vid, vid_gn,vid_ge1d,vid_ge2d,vid_ge3d,vid_gg;
		vid_gn=femodel.getGroupNodes().listId();
		vid.insert(vid.end(), vid_gn.begin(), vid_gn.end());
		vid_ge1d=femodel.getGroupElements1D().listId();
		vid.insert(vid.end(), vid_ge1d.begin(), vid_ge1d.end());
		vid_ge2d=femodel.getGroupElements2D().listId();
		vid.insert(vid.end(), vid_ge2d.begin(), vid_ge2d.end());
		vid_ge3d=femodel.getGroupElements3D().listId();
		vid.insert(vid.end(), vid_ge3d.begin(), vid_ge3d.end());
		vid_gg=femodel.getGroupGroups().listId();
		vid.insert(vid.end(), vid_gg.begin(), vid_gg.end());
		std::sort(vid.begin(), vid.end());  
		EXPECT_EQ(nbgn+nbg1d+nbg2d+nbg3d+nbgg, vid.size());
		EXPECT_EQ(0, vid[0]);
		//
		if ((strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_separator", 16)==0) || 
			(strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_fixed", 12)==0)) {
				EXPECT_EQ(8, femodel.get<GroupNode>( "*SET_NODE_LIST_TITLE",1999990)->size());
				EXPECT_EQ(10, femodel.get<GroupNode>( "*SET_NODE_LIST_TITLE",1)->size());
				EXPECT_EQ(1, femodel.get<GroupElements1D>( "*SET_BEAM_TITLE",10)->size());
				EXPECT_EQ(2, femodel.get<GroupElements2D>( "*SET_SHELL_LIST_TITLE",11)->size());
				EXPECT_EQ(1, femodel.get<GroupElements3D>( "*SET_SOLID_TITLE",1)->size());
				EXPECT_EQ(3, femodel.get<GroupGroups>( "*SET_PART_LIST_TITLE",1)->size());
                EXPECT_EQ(3, femodel.get<GroupGroups>("*SET_PART_LIST_TITLE", 1)->size());
                EXPECT_TRUE(femodel.get<GroupElements3D>("*ELEMENT_SOLID", 7000005)->isPart());
                EXPECT_TRUE(femodel.get<GroupElements3D>("*ELEMENT_SOLID", 7000033)->isPart());
        }
		else if (strncmp(TestWithParam<const char*>::GetParam(), "Pamcrash", 8)==0) {
			EXPECT_EQ(1, femodel.getGroupElements3D( femodel.getGroupElements3D().listId()[0]).size());
			EXPECT_EQ(2, femodel.getGroupElements3D( femodel.getGroupElements3D().listId()[1]).size());
			EXPECT_EQ(4, femodel.getGroupElements2D( femodel.getGroupElements2D().listId()[0]).size());
			EXPECT_EQ(3, femodel.getGroupElements2D( femodel.getGroupElements2D().listId()[1]).size());
			EXPECT_EQ(1, femodel.getGroupElements1D( femodel.getGroupElements1D().listId()[0]).size());
			EXPECT_EQ(1, femodel.getGroupElements1D( femodel.getGroupElements1D().listId()[1]).size());
			EXPECT_EQ(2, femodel.getGroupGroups( femodel.getGroupGroups().listId()[0]).size());
			EXPECT_EQ(3, femodel.getGroupGroups( femodel.getGroupGroups().listId()[1]).size());
			EXPECT_EQ(9, femodel.getGroupNode( femodel.getGroupNodes().listId()[0]).size());
			EXPECT_EQ(8, femodel.getGroupNode( femodel.getGroupNodes().listId()[3]).size());
            EXPECT_TRUE(femodel.get<GroupElements3D>("SOLID", 7000005)->isPart());
            EXPECT_TRUE(femodel.get<GroupElements3D>("SOLID", 7000033)->isPart());
        }
	}

	void checkFrame()  {
		EXPECT_EQ(nbfr, femodel.getNbFrames());
		EXPECT_EQ(0, femodel.getFrames().listId()[0]);
		FramePtr fr=femodel.get<FEFrame>( kwframe[0], frameid[0]);
        EXPECT_TRUE(fr!=nullptr);
        EXPECT_EQ(femodel.get<Node>(kwnode, 6000011)->getId(), fr->getOriginId());
		EXPECT_EQ(AxisName::X, fr->getFirstDirection());
		EXPECT_EQ( femodel.get<Node>( kwnode, 6000013)->getId(), fr->getFirstDirectionId());
		EXPECT_EQ( femodel.get<Node>( kwnode, 6000014)->getId(), fr->getPlaneId());
		EXPECT_EQ(AxisName::Y, fr->getSecondDirection());
        fr = femodel.get<FEFrame>(kwframe[1], frameid[1]);
        EXPECT_TRUE(fr != nullptr);
        EXPECT_EQ(femodel.get<Node>(kwnode, 7048467)->getId(), fr->getOriginId());
        EXPECT_EQ(AxisName::Z, fr->getFirstDirection());
        EXPECT_EQ(femodel.get<Node>(kwnode, 7048788)->getId(), fr->getFirstDirectionId());
        EXPECT_EQ(femodel.get<Node>(kwnode, 7048466)->getId(), fr->getPlaneId());
        EXPECT_EQ(AxisName::X, fr->getSecondDirection());
        Frames const& frames = femodel.getFrames();
        for (auto const& frame : frames) {
            if (!frame->hasOnlyOrigin())
                EXPECT_TRUE(frame->isCompletelyDefined());
        }
	}

	void checkParameter() {
		EXPECT_EQ(0, femodel.getParameters().listId()[0]);
		if ((strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_separator", 16)==0) || 
            (strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_fixed", 12) == 0)) {
            EXPECT_EQ(24, femodel.getNbParameters());
            EXPECT_EQ(0.6, femodel.get<FEModelParameter>("*MAT_PLASTIC_KINEMATIC_TITLE", 7000015)->get("YoungModulus")[0]);
            EXPECT_EQ(0.007, femodel.get<FEModelParameter>("*MAT_PLASTIC_KINEMATIC_TITLE", 7000015)->get("YeldStress")[0]);
            EXPECT_EQ(1.1e-006, femodel.get<FEModelParameter>("*MAT_PLASTIC_KINEMATIC_TITLE", 7000015)->get("Density")[0]);

            std::vector<double> valuesThick = femodel.get<FEModelParameter>("*ELEMENT_SHELL_THICKNESS", 7000410)->get("Thickness");
            EXPECT_TRUE(fabs(0.674551 - valuesThick[0]) < 1e-7);
            EXPECT_TRUE(fabs(0.801332 - valuesThick[1]) < 1e-7);
            EXPECT_TRUE(fabs(0.716657 - valuesThick[2]) < 1e-7);
            EXPECT_TRUE(fabs(0.701641 - valuesThick[3]) < 1e-7);
            EXPECT_EQ(1.5, femodel.get<FEModelParameter>("*MAT_ELASTIC_FLUID_TITLE", 1502)->get("BulkModulus")[0]);
            EXPECT_EQ(0.50000E-6, femodel.get<FEModelParameter>("*MAT_ELASTIC_FLUID_TITLE", 1502)->get("Density")[0]);
            EXPECT_EQ(0.15, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("BulkModulus")[0]);
            EXPECT_EQ(1.22000E-6, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("Density")[0]);
            EXPECT_EQ(3, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("ShearModulus").size());
            EXPECT_EQ(3, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("BulkRelaxationModulus").size());
            EXPECT_EQ(0.1234, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("ShearModulus")[0]);
            EXPECT_EQ(0, femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("BulkRelaxationModulus")[0]);
            EXPECT_EQ(0.1234, femodel.get<FEModelParameter>("*MAT_FABRIC_TITLE", 1508)->get("YoungModulusA")[0]);
            EXPECT_EQ(0, femodel.get<FEModelParameter>("*MAT_FABRIC_TITLE", 1508)->get("YoungModulusB")[0]);
            EXPECT_EQ(0, femodel.get<FEModelParameter>("*MAT_FABRIC_TITLE", 1508)->get("YoungModulusC")[0]);
            EXPECT_EQ(1.25600E-6, femodel.get<FEModelParameter>("*MAT_FABRIC_TITLE", 1508)->get("Density")[0]);
            EXPECT_EQ(0.01234, femodel.get<FEModelParameter>("*MAT_VISCOELASTIC_TITLE", 4157)->get("BulkModulus")[0]);
            EXPECT_EQ(0.00139, femodel.get<FEModelParameter>("*MAT_VISCOELASTIC_TITLE", 4157)->get("InstantaneousShearModulus")[0]);
            EXPECT_EQ(4.30000E-4, femodel.get<FEModelParameter>("*MAT_VISCOELASTIC_TITLE", 4157)->get("InfiniteShearModulus")[0]);
            EXPECT_EQ(0.21, femodel.get<FEModelParameter>("*MAT_VISCOELASTIC_TITLE", 4157)->get("DecayConstant")[0]);
            EXPECT_EQ(1.10000E-6, femodel.get<FEModelParameter>("*MAT_VISCOELASTIC_TITLE", 4157)->get("Density")[0]);
            EXPECT_EQ(0.12, femodel.get<FEModelParameter>("*MAT_PIECEWISE_LINEAR_PLASTICITY_TITLE", 7103)->get("YeldStress")[0]);
            EXPECT_EQ(5.0, femodel.get<FEModelParameter>("*MAT_PIECEWISE_LINEAR_PLASTICITY_TITLE", 7103)->get("YoungModulus")[0]);
            EXPECT_EQ(1.70000E-6, femodel.get<FEModelParameter>("*MAT_PIECEWISE_LINEAR_PLASTICITY_TITLE", 7103)->get("Density")[0]);
            EXPECT_EQ(1.0, femodel.get<FEModelParameter>("*DEFINE_CURVE_TITLE", 7999074)->get("ScaleFactorAbscissa")[0]);
            EXPECT_EQ(0.75, femodel.get<FEModelParameter>("*DEFINE_CURVE_TITLE", 7999074)->get("ScaleFactorOrdinate")[0]);
            EXPECT_EQ(0.6, femodel.get<FEModelParameter>("*DEFINE_CURVE", 7000004)->get("ScaleFactorAbscissa")[0]);
            EXPECT_EQ(1.0, femodel.get<FEModelParameter>("*DEFINE_CURVE", 7000004)->get("ScaleFactorOrdinate")[0]);
        }
		else if (strncmp(TestWithParam<const char*>::GetParam(), "Pamcrash", 8)==0) {
			EXPECT_EQ(0, femodel.getParameters().listId()[0]);
			EXPECT_EQ(2, femodel.getNbParameters());
			std::vector<double> values=femodel.get<FEModelParameter>( "MATER",101)->get("Density");
			EXPECT_EQ(7.85e-006, values[0] );
			std::vector<double> valuesThick=femodel.get<FEModelParameter>( "PART",7000000)->get("Thickness");
			EXPECT_EQ(0.83, valuesThick[0] );
		}
	}


	FEModel femodel;
	parser::ParserModelFile* p;
	std::string modelfile;
	std::string rulefile;
	std::string modeldescrfile;
	std::string kwnode;
	std::vector<std::string> kwe1d;
	std::vector<std::string> kwe2d;
	std::vector<std::string> kwe3d;
	std::vector<std::string> kwgn, kwge3d, kwge2d, kwge1d, kwgg;
	std::vector<std::string> kwframe;
	unsigned int nbnode,nbe1d, nbe2d, nbe3d, nbgn, nbg1d, nbg2d, nbg3d, nbgg, nbfr;
	Id nodeid;
	VId e1did, e2did, e3did, frameid;
	std::vector<VId> elem1d, elem2d, elem3d;
	double coordx, coordy, coordz;
};

TEST_P(FEModel_test, parse)
{
	checkNode();
	checkElement();
	checkGroup();
	checkFrame();
	checkParameter();
}

TEST_P(FEModel_test, vtufile)
{
	boost::filesystem::path tmp01 = boost::filesystem::temp_directory_path();
	tmp01 /= "model_01.vtu";
	femodel.writeModel(tmp01.string());
	femodel.clear();
	femodel.readModel(tmp01.string());
	EXPECT_EQ(nbnode, femodel.getNbNode());
	EXPECT_EQ(nbe3d, femodel.getNbElement3D());
	EXPECT_EQ(nbe2d, femodel.getNbElement2D());
	EXPECT_EQ(nbe1d, femodel.getNbElement1D());
}


TEST_P(FEModel_test, updatemodel)
{
	// modify one coordinate
	double newx=10.0;
	double newy=20.0;
	double newz=30.0;
    femodel.get<Node>(kwnode, 7020563)->setCoord(newx, newy, newz);

	// modify parameter
	std::vector<double> newValue1;
	std::vector<double> newValue2;
	std::vector<double> newValueThick;
    std::vector<double> newValueParam;
    if ((strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_separator", 16) == 0) ||
		(strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_fixed", 12)==0)) {
			//modify parameter for material keyword
			newValue1.push_back( 10.0);
			newValue2.push_back( 20.0);
			femodel.get<FEModelParameter>( "*MAT_PLASTIC_KINEMATIC_TITLE",7000015)->setValue("Density",newValue1);
			femodel.get<FEModelParameter>( "*MAT_PLASTIC_KINEMATIC_TITLE",7000015)->setValue("YoungModulus",newValue2);
			//modify parameter for shell thickness
			newValueThick.push_back( 10.0);
			newValueThick.push_back( 11.0);
			newValueThick.push_back( 12.0);
			newValueThick.push_back( 13.5);
			femodel.get<FEModelParameter>( "*ELEMENT_SHELL_THICKNESS",7000410)->setValue("Thickness",newValueThick);
            newValueParam.push_back(10.0);
            newValueParam.push_back(11.0);
            newValueParam.push_back(12.0);
            femodel.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->setValue("ShearModulus", newValueParam);
	}
	else if (strncmp(TestWithParam<const char*>::GetParam(), "Pamcrash", 8)==0) {
		//modify parameter for material keyword
		std::vector<double> tmpp=femodel.get<FEModelParameter>( "MATER",101)->get("ModuleG");
		newValue1.push_back( 10.0);
		newValue2.push_back( 20.0);
		femodel.get<FEModelParameter>( "MATER",101)->setValue("Density",newValue1);
		femodel.get<FEModelParameter>( "MATER",101)->setValue("ModuleG",newValue2);
		//modify parameter for shell thickness
		newValueThick.push_back( 13.5);
		femodel.get<FEModelParameter>( "PART",7000000)->setValue("Thickness",newValueThick);
	}

	// update model
	boost::filesystem::path updatefiles = boost::filesystem::temp_directory_path();
	boost::filesystem::path modelfilepath(modelfile);
	//p->setTypeAction( parser::ActionType::UPDATE);
    femodel.update(p, modelfile, std::vector<std::string>(), updatefiles.string());

	// reload to check
	femodel.clear();
	piper::hbm::FEModel mcheck;
	std::string modelfileupdated = updatefiles.string() + "/" + modelfilepath.filename().string();
	//
    mcheck.parse(p, modelfileupdated, std::vector<std::string>());

	//check new value for node coordinate
	EXPECT_GE(1e-6,std::abs(mcheck.get<Node>( kwnode,7020563)->getCoordX() - 10.0));
	EXPECT_GE(1e-6,std::abs(mcheck.get<Node>( kwnode,7020563)->getCoordY() - 20.0));
	EXPECT_GE(1e-6,std::abs(mcheck.get<Node>( kwnode,7020563)->getCoordZ() - 30.0));

	//check new value parameter
	if ((strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_separator", 16)==0) || 
        (strncmp(TestWithParam<const char*>::GetParam(), "LSDyna_fixed", 12) == 0)) {
        EXPECT_GE(newValue1[0], mcheck.get<FEModelParameter>("*MAT_PLASTIC_KINEMATIC_TITLE", 7000015)->get("Density")[0]);
        EXPECT_GE(newValue2[0], mcheck.get<FEModelParameter>("*MAT_PLASTIC_KINEMATIC_TITLE", 7000015)->get("YoungModulus")[0]);
        std::vector<double> checkvaluethick = mcheck.get<FEModelParameter>("*ELEMENT_SHELL_THICKNESS", 7000410)->get("Thickness");
        EXPECT_TRUE(VectorMatch(newValueThick, checkvaluethick));
        std::vector<double> checkvalueParam = mcheck.get<FEModelParameter>("*MAT_GENERAL_VISCOELASTIC_TITLE", 1506)->get("ShearModulus");
        EXPECT_TRUE(VectorMatch(newValueParam, checkvalueParam));
    }
	else if (strncmp(TestWithParam<const char*>::GetParam(), "Pamcrash", 8)==0) {
		std::vector<double> tmp=mcheck.get<FEModelParameter>( "MATER",101)->get("ModuleG");
		EXPECT_GE(1e-6,std::abs(mcheck.get<FEModelParameter>( "MATER",101)->get("Density")[0] - newValue1[0]));
		EXPECT_GE(1e-6,std::abs(mcheck.get<FEModelParameter>( "MATER",101)->get("ModuleG")[0] - newValue2[0]));
		std::vector<double> checkvaluethick=mcheck.get<FEModelParameter>( "PART",7000000)->get("Thickness");
		EXPECT_TRUE(VectorMatch(newValueThick, checkvaluethick));
	}


}

INSTANTIATE_TEST_CASE_P(InstantiationName,
						FEModel_test,
						::testing::Values("LSDyna_fixed", "LSDyna_separator","Pamcrash"));

TEST(FEModel_test, doubleRead)
{
	FEModel model;
	model.readModel("model_01.vtu");
	EXPECT_EQ(36, model.getNbNode());
	model.clear();
	model.readModel("model_01.vtu");
	EXPECT_EQ(36, model.getNbNode());
    model.clear();
}

TEST(FEModel_test, readWritePreserveModelIntegrity)
{
	piper::hbm::FEModel modelRead;
	std::string modelReadPath = "model_readWritePreserveModelIntegrity.vtu";

	modelRead.readModel(modelReadPath);
	EXPECT_EQ(21,modelRead.getNbNode());
	EXPECT_EQ(2,modelRead.getNbElement3D());
	EXPECT_EQ(4,modelRead.getNbElement2D());
	EXPECT_EQ(1,modelRead.getNbElement1D());

	piper::hbm::FEModel modelWrote;
	boost::filesystem::path modelWrotePath = boost::filesystem::temp_directory_path();
	modelWrotePath /= "model_readWritePreserveModelIntegrity_check.vtu";

	modelRead.writeModel(modelWrotePath.string()); 

	modelWrote.readModel(modelWrotePath.string());

	EXPECT_EQ(21,modelWrote.getNbNode());
	EXPECT_EQ(2,modelWrote.getNbElement3D());
	EXPECT_EQ(4,modelWrote.getNbElement2D());
	EXPECT_EQ(1,modelWrote.getNbElement1D());

	double referenceNodeCoord[] = {4.101410e+02, -1.556230e+02, -1.151200e+02, 4.134390e+02, -1.552800e+02, -1.152620e+02, 4.108330e+02, -1.550700e+02, -1.136720e+02, 4.137080e+02, -1.547510e+02, -1.138630e+02, 4.123230e+02, -1.544230e+02, -1.130940e+02, 4.079580e+02, -1.536110e+02, -1.156160e+02, 4.105960e+02, -1.533350e+02, -1.158280e+02, 4.094850e+02, -1.527440e+02, -1.141590e+02, 4.118570e+02, -1.524400e+02, -1.140220e+02, 4.113460e+02, -1.521330e+02, -1.135080e+02, 4.101190e+02, -1.291080e+02, -1.184450e+02, 4.109890e+02, -1.284570e+02, -1.163530e+02, 4.120520e+02, -1.273770e+02, -1.191910e+02, 4.104800e+02, -1.300260e+02, -1.202670e+02, 4.122360e+02, -1.281130e+02, -1.207960e+02, 4.113390e+02, -1.307490e+02, -1.217670e+02, 4.128320e+02, -1.287370e+02, -1.221690e+02, 4.128050e+02, -1.269710e+02, -1.176660e+02, 3.698415e+02, -1.509127e+02, -1.143146e+02, 2.427890e+01, -1.320570e+02, -2.412040e+01, 3.759534e+02, -1.503724e+02, -1.196512e+02};
	int referenceNodeId[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};

	double referenceCoordMaxRelError=0;
	double rwModelCoordMaxRelError=0;
	int indexMaxError=0;
	int indexProofMaxError=0;
	auto itWrote = modelWrote.getNodes().begin();
	int itProof = 0;
	if(modelRead.getNbNode()==21){
		for(auto itRead =  modelRead.getNodes().begin(); itRead!=modelRead.getNodes().end(); ++itRead, ++itWrote,++itProof){
			rwModelCoordMaxRelError = std::max(rwModelCoordMaxRelError, sqrt(pow((*itRead)->getCoordX() - (*itWrote)->getCoordX(),2)
				+ pow((*itRead)->getCoordY() - (*itWrote)->getCoordY(),2)
				+ pow((*itRead)->getCoordZ() - (*itWrote)->getCoordZ(),2))
				/sqrt(pow((*itRead)->getCoordX() ,2)
				+ pow((*itRead)->getCoordY() ,2)
				+ pow((*itRead)->getCoordZ() ,2))
				);
			referenceCoordMaxRelError = std::max(referenceCoordMaxRelError,sqrt(pow(referenceNodeCoord[itProof*3+0] - (*itWrote)->getCoordX(),2)
				+ pow(referenceNodeCoord[itProof*3+1] - (*itWrote)->getCoordY(),2)
				+ pow(referenceNodeCoord[itProof*3+2] - (*itWrote)->getCoordZ(),2))
				/sqrt(pow(referenceNodeCoord[itProof*3+0] ,2)
				+ pow(referenceNodeCoord[itProof*3+1] ,2)
				+ pow(referenceNodeCoord[itProof*3+2] ,2))
				);
			indexMaxError = std::max(indexMaxError, std::abs((int)((*itRead)->getId() - (*itWrote)->getId())));
			indexProofMaxError = std::max(indexMaxError, std::abs((int)(referenceNodeId[itProof] - (*itWrote)->getId())));
		}
		EXPECT_GE(1e-6,rwModelCoordMaxRelError);
		EXPECT_GE(1e-6,indexMaxError);
		EXPECT_GE(1e-6,referenceCoordMaxRelError);
		EXPECT_GE(1e-6,indexProofMaxError);
        modelRead.clear();
        modelWrote.clear();
	}
}



TEST(FEModel_test, defaultvalue)
{
    FEModel femodel;
    parser::ParserModelFile* p;
    p = new parser::ParserModelFile("../formatrules/Formatrules_LSDyna_Separator.pfr", "model_01_description_LSDyna_Separator.pmr");
    femodel.parse(p, "model_01_separator.dyn", std::vector<std::string>());

    FEModelParameterPtr pp = femodel.get<FEModelParameter>("*SECTION_SHELL_TITLE", 7000022);
    std::vector<double> cur = pp->get("Thickness");
    std::vector<double> checkvaluethick = { 0, 0, 0, 0 };
    EXPECT_TRUE(VectorMatch(cur, checkvaluethick));

    //export et reimport to check
    boost::filesystem::path updatefiles = boost::filesystem::temp_directory_path();
    femodel.update(p, "model_01_separator.dyn", std::vector<std::string>(), updatefiles.string());
    femodel.clear();
    p = new parser::ParserModelFile("../formatrules/Formatrules_LSDyna_Separator.pfr", "model_01_description_LSDyna_Separator.pmr");
    femodel.parse(p, updatefiles.string() + "/model_01_separator.dyn", std::vector<std::string>());
    FEModelParameterPtr curr = femodel.get<FEModelParameter>("*SECTION_SHELL_TITLE", 7000022);
    std::vector<double> cur2 = curr->get("Thickness");
    EXPECT_TRUE(VectorMatch(cur2, checkvaluethick));
    femodel.clear();
}

TEST(FEModel_test, parse_nodeonly) {
    FEModel femodel;
    parser::ParserModelFile* p;
    p = new parser::ParserModelFile("../formatrules/Formatrules_LSDyna_Separator.pfr", "model_01_description_LSDyna_Separator.pmr");
    femodel.parse(p, "model_01_separator.dyn", std::vector<std::string>(), PARSER_OPTION::NODES_ONLY);
    EXPECT_EQ(33, femodel.getNbNode());
    EXPECT_EQ(0, femodel.getNbElement1D());
    EXPECT_EQ(0, femodel.getNbElement2D());
    EXPECT_EQ(0, femodel.getNbElement3D());
    EXPECT_EQ(0, femodel.getNbGroupNode());
    EXPECT_EQ(0, femodel.getNbGroupElements1D());
    EXPECT_EQ(0, femodel.getNbGroupElements2D());
    EXPECT_EQ(0, femodel.getNbGroupElements3D());
    EXPECT_EQ(0, femodel.getNbGroupGroups());
    EXPECT_EQ(0, femodel.getNbFrames());
    EXPECT_EQ(0, femodel.getNbParameters());
    femodel.clear();
}

TEST(FEModel_test, parse_meshonly) {
    FEModel femodel;
    parser::ParserModelFile* p;
    femodel.clear();
    p = new parser::ParserModelFile("../formatrules/Formatrules_LSDyna_Separator.pfr", "model_01_description_LSDyna_Separator.pmr");
    femodel.parse(p, "model_01_separator.dyn", std::vector<std::string>(), PARSER_OPTION::MESH_ONLY);
    EXPECT_EQ(33, femodel.getNbNode());
    EXPECT_EQ(3, femodel.getNbElement1D());
    EXPECT_EQ(4, femodel.getNbElement2D());
    EXPECT_EQ(3, femodel.getNbElement3D());
    EXPECT_EQ(0, femodel.getNbGroupNode());
    EXPECT_EQ(3, femodel.getNbGroupElements1D());
    EXPECT_EQ(1, femodel.getNbGroupElements2D());
    EXPECT_EQ(2, femodel.getNbGroupElements3D());
    EXPECT_EQ(0, femodel.getNbGroupGroups());
    EXPECT_EQ(0, femodel.getNbFrames());
    EXPECT_EQ(4, femodel.getNbParameters());
    femodel.clear();
}

TEST(FEModel_test, checkClosedSurface) {
    FEModel surface_ok, surface_invertedelem;
    parser::ParserModelFile* p;
    p = new parser::ParserModelFile("../formatrules/Formatrules_LSDyna_Fixed.pfr", "hbm_cube_entity.pmr");
    surface_ok.parse(p, "hbm_cube_entity.dyn", std::vector<std::string>());
    std::vector<ElemDef> e2D;
    bool isEnvelopValid = false;;
    int numberClosePositiveSurface = 0;
    int numberCloseNegativeSurface = 0;
    bool hasInvertedElements = false;
    for (auto it_e2D = surface_ok.getElements2D().begin(); it_e2D != surface_ok.getElements2D().end(); ++it_e2D)
        e2D.push_back(it_e2D->get()->get());
    isEnvelopValid = Element2D::checkClosedSurface(e2D, surface_ok, hasInvertedElements);
    EXPECT_TRUE(isEnvelopValid);
    EXPECT_FALSE(hasInvertedElements);

    surface_invertedelem.parse(p, "hbm_cube_entity_inverted.dyn", std::vector<std::string>());
    e2D.clear();
    isEnvelopValid = false;;
    numberClosePositiveSurface = 0;
    numberCloseNegativeSurface = 0;
    hasInvertedElements = false;
    for (auto it_e2D = surface_invertedelem.getElements2D().begin(); it_e2D != surface_invertedelem.getElements2D().end(); ++it_e2D)
        e2D.push_back(it_e2D->get()->get());
    isEnvelopValid = Element2D::checkClosedSurface(e2D, surface_ok, hasInvertedElements);
    EXPECT_FALSE(isEnvelopValid);
    EXPECT_TRUE(hasInvertedElements);


}
