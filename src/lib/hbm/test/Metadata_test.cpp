/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <iostream>

#include <boost/filesystem.hpp>

#include "hbm/Metadata.h"
#include "hbm/FEModel.h"
#include "TestHelper.h"
#include "units/units.h"

using namespace piper::hbm;

class Metadata_test : public ::testing::Test {

protected:

    virtual void SetUp() {
        metadata.read("model_01.pmd");
    }

    void checkSource() {
        EXPECT_TRUE(boost::filesystem::equivalent("model_01.dyn", metadata.sourcePath()));
        EXPECT_TRUE(boost::filesystem::equivalent("model_01_description_LSDyna.pmr", metadata.rulePath()));
		boost::filesystem::path full_path_check = boost::filesystem::system_complete("../formatrules/Formatrules_LSDyna_Fixed.pfr");
        EXPECT_TRUE(boost::filesystem::equivalent( full_path_check, metadata.sourceFormat()));
    }

    void checkUnits() {
        EXPECT_EQ(piper::units::Length::cm, metadata.lengthUnit());
        EXPECT_EQ(piper::units::Mass::kg, metadata.massUnit());
        EXPECT_EQ(piper::units::Age::year, metadata.ageUnit());
    }

    void checkGravity() {
        EXPECT_TRUE(metadata.isGravityDefined());
        Coord g; g << 0., 0., 9.81;
        EXPECT_TRUE(metadata.gravity().isApprox(g));
    }

    void checkGenmetadata() {
        EXPECT_EQ(3, metadata.genericmetadatas().size());
        EXPECT_TRUE(metadata.hasGenericmetadata("Gen_1"));
        GenericMetadata const& gen = metadata.genericmetadata("Gen_1");
        EXPECT_EQ(gen.get_groupNode().size(), 1);
        EXPECT_EQ(gen.get_groupElement1D().size(), 1);
        EXPECT_EQ(gen.get_groupElement2D().size(), 1);
        EXPECT_EQ(gen.get_groupElement3D().size(), 1);
        EXPECT_TRUE(metadata.hasGenericmetadata("group1D"));
        GenericMetadata const& gen2 = metadata.genericmetadata("group1D");
        EXPECT_EQ(gen2.get_groupNode().size(), 0);
        EXPECT_EQ(gen2.get_groupElement1D().size(), 1);
        EXPECT_EQ(gen2.get_groupElement2D().size(), 0);
        EXPECT_EQ(gen2.get_groupElement3D().size(), 0);
    }

    void checkEntity() {
        EXPECT_EQ(4, metadata.entities().size());
        EXPECT_EQ(metadata.entities().end(), metadata.entities().find("Test_entity_does_not_exist"));
        EXPECT_NE(metadata.entities().end(), metadata.entities().find("Entity_4_Auto"));
        Entity const& Entity_4_Auto = metadata.entities().find("Entity_4_Auto")->second;
        EXPECT_EQ(1, Entity_4_Auto.get_groupElement1D().size());
        auto p = std::find(Entity_4_Auto.get_groupElement1D().begin(), Entity_4_Auto.get_groupElement1D().end(), 3);
        EXPECT_NE(Entity_4_Auto.get_groupElement1D().end(), p);
        EXPECT_EQ(1, Entity_4_Auto.get_groupElement2D().size());
        p = std::find(Entity_4_Auto.get_groupElement2D().begin(), Entity_4_Auto.get_groupElement2D().end(), 2);
        EXPECT_NE(Entity_4_Auto.get_groupElement2D().end(), p);
        EXPECT_EQ(1, Entity_4_Auto.get_groupElement3D().size());
        p = std::find(Entity_4_Auto.get_groupElement3D().begin(), Entity_4_Auto.get_groupElement3D().end(), 0);
        EXPECT_NE(Entity_4_Auto.get_groupElement3D().end(), p);
        EXPECT_TRUE(Entity_4_Auto.get_groupGroup().empty());
	}

	void checkLandmark() {
        EXPECT_EQ(4, metadata.landmarks().size());
		EXPECT_NE(metadata.landmarks().end(), metadata.landmarks().find("LandMark_1_point"));
        EXPECT_EQ(metadata.landmark("LandMark_1_point").type(),Landmark::Type::POINT);
		EXPECT_EQ(1, metadata.landmark("LandMark_1_point").get_groupNode().size());
		auto p = std::find(metadata.landmark("LandMark_1_point").get_groupNode().begin(), metadata.landmark("LandMark_1_point").get_groupNode().end(), 6);
		EXPECT_NE(metadata.landmark("LandMark_1_point").get_groupNode().end(), p);
		EXPECT_NE(metadata.landmarks().end(), metadata.landmarks().find("Landmark_2"));
        EXPECT_EQ(metadata.landmark("Landmark_2").type(),Landmark::Type::SPHERE);
        EXPECT_TRUE(metadata.landmark("Landmark_Node").get_groupNode().size() == 0);
        EXPECT_TRUE(metadata.landmark("Landmark_Node").node.size() == 1);
        EXPECT_TRUE(metadata.landmark("Landmark_Node").node[0] == 4);
    }

	void checkJoint() {
		EXPECT_EQ(3, metadata.joints().size());
		EXPECT_NE(metadata.joints().end(), metadata.joints().find("Joint_1"));
		EXPECT_EQ("Entity_1", metadata.joint("Joint_1").getEntity1());
		EXPECT_EQ("Entity_2", metadata.joint("Joint_1").getEntity2());
		EXPECT_EQ(2, metadata.joint("Joint_1").getEntity1FrameId());
		EXPECT_EQ(2, metadata.joint("Joint_1").getEntity2FrameId());
        EXPECT_EQ(EntityJoint::ConstrainedDofType::HARD, metadata.joint("Joint_1").getConstrainedDofType());
		EXPECT_NE(metadata.joints().end(), metadata.joints().find("Joint_2"));
		EXPECT_EQ("Entity_1", metadata.joint("Joint_2").getEntity1());
		EXPECT_EQ("Entity_5_Auto", metadata.joint("Joint_2").getEntity2());
		EXPECT_EQ(1, metadata.joint("Joint_2").getEntity1FrameId());
		EXPECT_EQ(1, metadata.joint("Joint_2").getEntity2FrameId());
        EXPECT_EQ(EntityJoint::ConstrainedDofType::HARD, metadata.joint("Joint_2").getConstrainedDofType());
		EXPECT_NE(metadata.joints().end(), metadata.joints().find("Joint_3_generalized_spring"));
		EXPECT_EQ("Entity_1", metadata.joint("Joint_3_generalized_spring").getEntity1());
		EXPECT_EQ("Entity_5_Auto", metadata.joint("Joint_3_generalized_spring").getEntity2());
		EXPECT_EQ(3, metadata.joint("Joint_3_generalized_spring").getEntity1FrameId());
		EXPECT_EQ(3, metadata.joint("Joint_3_generalized_spring").getEntity2FrameId());
        EXPECT_EQ(EntityJoint::ConstrainedDofType::SOFT, metadata.joint("Joint_3_generalized_spring").getConstrainedDofType());
	}

    void checkAnatomicalJoint() {
        EXPECT_EQ(1, metadata.anatomicalJoints().size());
        EXPECT_NE(metadata.anatomicalJoints().end(), metadata.anatomicalJoints().find("Left_hip_joint"));
//        EXPECT_EQ((std::array<bool,6>){false,false,false,true,true,true}, metadata.anatomicalJoint("Left_hip_joint").getDof())
    }

	void checkContacts() {
        EXPECT_EQ(3, metadata.contacts().size());
		EXPECT_TRUE( metadata.hasContact( "contact_1"));
		EXPECT_EQ("SLIDING", EntityContactType_str[(unsigned int) metadata.contact( "contact_1").type()]);
		EXPECT_EQ("Entity_1", metadata.contact( "contact_1").entity1());
		EXPECT_EQ("Entity_2", metadata.contact( "contact_1").entity2());
		EXPECT_EQ(6, metadata.contact( "contact_1").group1());
		EXPECT_EQ(14, metadata.contact( "contact_1").group2());
		EXPECT_TRUE( metadata.contact( "contact_1").keepThickness());
		EXPECT_TRUE( metadata.hasContact( "contact_2"));
		EXPECT_EQ("SLIDING", EntityContactType_str[(unsigned int) metadata.contact( "contact_2").type()]);
		EXPECT_EQ("Entity_1", metadata.contact( "contact_2").entity1());
		EXPECT_EQ("Entity_4_Auto", metadata.contact( "contact_2").entity2());
		EXPECT_EQ(6, metadata.contact( "contact_2").group1());
		EXPECT_EQ(7, metadata.contact( "contact_2").group2());
		EXPECT_FALSE( metadata.contact( "contact_2").keepThickness());
		EXPECT_FLOAT_EQ(0.5, metadata.contact("contact_2").thickness());
        EXPECT_TRUE( metadata.hasContact( "contact_3"));
        EXPECT_EQ("ATTACHED", EntityContactType_str[(unsigned int) metadata.contact( "contact_3").type()]);
        EXPECT_EQ("Entity_1", metadata.contact( "contact_3").entity1());
        EXPECT_EQ("Entity_5_Auto", metadata.contact( "contact_3").entity2());
        EXPECT_EQ(piper::hbm::ID_UNDEFINED, metadata.contact( "contact_3").group1());
        EXPECT_EQ(piper::hbm::ID_UNDEFINED, metadata.contact( "contact_3").group2());
	}

	void checkHbmParameters() {
		EXPECT_EQ(4, metadata.hbmParameters().size());
		
		EXPECT_TRUE(metadata.hasHbmParameter("density_1"));
		EXPECT_EQ("density", metadata.hbmParameter("density_1").source());
		VId vcheck;
		vcheck.push_back(2);
		EXPECT_TRUE(VectorMatch(vcheck, metadata.hbmParameter("density_1").FEparameters));
		
		EXPECT_TRUE(metadata.hasHbmParameter("density_2"));
		EXPECT_EQ("density", metadata.hbmParameter("density_2").source());
		vcheck.clear();
		vcheck.push_back(0);
		vcheck.push_back(3);
		EXPECT_TRUE(VectorMatch(vcheck, metadata.hbmParameter("density_2").FEparameters));
		
		EXPECT_TRUE(metadata.hasHbmParameter("youngmodulus_1"));
		EXPECT_EQ("youngmodulus", metadata.hbmParameter("youngmodulus_1").source());
		vcheck.clear();
		vcheck.push_back(2);
		vcheck.push_back(3);
		EXPECT_TRUE(VectorMatch(vcheck, metadata.hbmParameter("youngmodulus_1").FEparameters));
		
		EXPECT_TRUE(metadata.hasHbmParameter("thickness_1"));
		EXPECT_EQ("thickness", metadata.hbmParameter("thickness_1").source());
		vcheck.clear();
		vcheck.push_back(4);
		vcheck.push_back(5);
		vcheck.push_back(6);
		vcheck.push_back(7);
		EXPECT_TRUE(VectorMatch(vcheck, metadata.hbmParameter("thickness_1").FEparameters));

	}
	
    void checkAnthropometry() {
        EXPECT_TRUE(metadata.anthropometry().age.isDefined());
        EXPECT_TRUE(metadata.anthropometry().age.value()==45.5);
        EXPECT_TRUE(metadata.anthropometry().age.unit() == metadata.ageUnit());
        EXPECT_TRUE(metadata.anthropometry().weight.isDefined());
        EXPECT_TRUE(metadata.anthropometry().weight.value() == 75);
        EXPECT_TRUE(metadata.anthropometry().weight.unit() == metadata.massUnit());
        EXPECT_TRUE(metadata.anthropometry().height.isDefined());
        EXPECT_TRUE(metadata.anthropometry().height.value() == 165.0);
        EXPECT_TRUE(metadata.anthropometry().height.unit() == metadata.lengthUnit());
    }

    Metadata metadata;

};

TEST_F(Metadata_test, version) {
    // for now nothing to test
}

TEST_F(Metadata_test, parameter) {
    checkHbmParameters();
}

TEST_F(Metadata_test, source) {
    checkSource();
}

TEST_F(Metadata_test, units) {
    checkUnits();
}

TEST_F(Metadata_test, gravity) {
    checkGravity();
}

TEST_F(Metadata_test, entity) {
    checkEntity();
    Entity newEntity("Test_entity_123");
    newEntity.get_groupElement1D().push_back(123);
    EXPECT_TRUE(metadata.addEntity(newEntity));
    Metadata::EntityCont::const_iterator it = metadata.entities().find("Test_entity_123");
    ASSERT_TRUE(it!=metadata.entities().end());
    auto p = std::find(it->second.get_groupElement1D().begin(), it->second.get_groupElement1D().end(), 123);
    EXPECT_NE(it->second.get_groupElement1D().end(), p);
}

TEST_F(Metadata_test, genericmetadata) {
    checkGenmetadata();
}

TEST_F(Metadata_test, landmarks) {
    checkLandmark();
    Landmark newLandmark("added_landmark",Landmark::Type::POINT);
    newLandmark.get_groupNode().push_back( 10);
	EXPECT_TRUE(metadata.addLandmark(newLandmark));
    Metadata::LandmarkCont::const_iterator it = metadata.landmarks().find("added_landmark");
    ASSERT_TRUE(it!=metadata.landmarks().end());
//	EXPECT_EQ(10,*metadata.landmark("added_landmark").getGroupNodeId().begin());

}

TEST_F(Metadata_test, landmarkPoint) {
    Metadata metadata;
    metadata.read("model_landmarkPosition.pmd");
    FEModel fem;
    fem.readModel("model_landmarkPosition.vtu");
    Coord p; p << 1. , 2. , 3. ;
    EXPECT_TRUE(metadata.landmark("testPoint").position(fem).isApprox(p));
    fem.clear();
}

TEST_F(Metadata_test, landmarkBarycenter) {
    Metadata metadata;
    metadata.read("model_landmarkPosition.pmd");
    FEModel fem;
    fem.readModel("model_landmarkPosition.vtu");
    Coord p; p << 3. , 1. , 5. ;
    EXPECT_TRUE(metadata.landmark("testBarycenter").position(fem).isApprox(p));
    fem.clear();
}

TEST_F(Metadata_test, landmarkSphere) {
    Metadata metadata;
    metadata.read("model_landmarkPosition.pmd");
    FEModel fem;
    fem.readModel("model_landmarkPosition.vtu");
    Coord p; p << 2. , 2. , -2. ;
    EXPECT_TRUE(metadata.landmark("testSphere").position(fem).isApprox(p));
    fem.clear();
}

TEST_F(Metadata_test, joints) {
	checkJoint();
}

TEST_F(Metadata_test, anatomicalJoints) {
    checkAnatomicalJoint();
}

TEST_F(Metadata_test, contacts) {
	checkContacts();
}

TEST_F(Metadata_test, parameters) {
	checkHbmParameters();
}

TEST_F(Metadata_test, anthropometry) {
    checkAnthropometry();
} 

TEST_F(Metadata_test, write) {
    // write current model to tmp
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_hbm_test_metadata.pmd";
    metadata.write(tmp.string());

    // read it
    metadata.clear();
    metadata.read(tmp.string());
    checkSource();
    checkUnits();
    checkGravity();
    checkEntity();
	checkLandmark();
	checkJoint();
    checkAnatomicalJoint();
	checkContacts();
    checkGenmetadata();
    checkAnthropometry();
}
