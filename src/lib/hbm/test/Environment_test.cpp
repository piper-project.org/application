/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/EnvironmentModels.h"

#include <vector>

#include <boost/filesystem.hpp>

#include "TestHelper.h"

using namespace piper::hbm;

class Environment_test : public ::testing::Test {

protected:

	EnvironmentModels env;

	void check() {
        EXPECT_TRUE(env.isExistingName("Env_test"));
        EnvironmentModelPtr envTest = env.getEnvironmentModel("Env_test");
        EXPECT_EQ(piper::units::Length::cm , envTest->lengthUnit());
        EXPECT_EQ(68, envTest->envmodel().getNbNode());
        EXPECT_EQ(0, envTest->envmodel().getNbElement1D());
        EXPECT_EQ(32+32, envTest->envmodel().getNbElement2D());
        EXPECT_EQ(0, envTest->envmodel().getNbElement3D());
	}

    void checkTransformation() {
        EXPECT_TRUE(env.isExistingName("Env_test"));
        EnvironmentModelPtr envTest = env.getEnvironmentModel("Env_test");
        Coord translation; translation << 0.,10.,-20.;
        EXPECT_TRUE(envTest->envmodel().getTranslation().isApprox(translation));
        Coord rotation; rotation << 15.,-50.,0.;
        EXPECT_TRUE(envTest->envmodel().getRotation().isApprox(rotation));
        Coord scale; scale << 1.,1.,1.5;
        EXPECT_TRUE(envTest->envmodel().getScale().isApprox(scale));
    }

};


TEST_F(Environment_test, addModel) {
    env.addModel("Env_test", "env.dyn", "../formatrules/Formatrules_LSDyna_Fixed.pfr", piper::units::Length::cm);
	check();
}

TEST_F(Environment_test, read) {
    env.readandImport("model_01.ppj", piper::units::Length::cm);
	check();
    checkTransformation();
}

TEST_F(Environment_test, write) {
    env.readandImport("model_01.ppj", piper::units::Length::cm);
	// write current model to tmp
	boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
	tmp /= "piper_hbm_test_EnvModel.ppj";
	env.write(tmp.string());
	env.clear();
	// read it
    env.readandImport(tmp.string(), piper::units::Length::cm);
	check();
    checkTransformation();
}

TEST_F(Environment_test, scaleLength) {
    env.addModel("Env_test", "env.dyn", "../formatrules/Formatrules_LSDyna_Fixed.pfr", piper::units::Length::cm);
	VId vid = env.getEnvironmentModel("Env_test")->envmodel().getNodes().listId();
	Id id = vid[10];
	Coord coord = env.getEnvironmentModel("Env_test")->envmodel().getNode(id).get();
    env.getEnvironmentModel("Env_test")->scaleLength(piper::units::Length::mm);
	Coord coordupdate = env.getEnvironmentModel("Env_test")->envmodel().getNode(id).get();
	EXPECT_EQ(10, coordupdate[0] / coord[0]);
	EXPECT_EQ(10, coordupdate[1] / coord[1]);
	EXPECT_EQ(10, coordupdate[2] / coord[2]);
}
