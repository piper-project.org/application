/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_ANTHROPOMETADATA_H
#define PIPER_HBM_ANTHROPOMETADATA_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "BaseMetadata.h"
#include "units/units.h"


namespace piper {
    namespace hbm {

        ///**
        //* @brief Anthropometric caracteristics of the hbm (height, age and weight).
        //*
        //* @author Erwan Jolivet @date 2016
        //*/
        template <typename UnitType>
        class HBM_EXPORT AnthropoMetadata : public BaseMetadata {
        public:

            typedef typename units::UnitTraits<UnitType> UnitTraits;

            AnthropoMetadata()
                : BaseMetadata("undefined")
                , m_unit(UnitTraits::defaultUnit())
                , m_value(0.0)
            {}
            AnthropoMetadata(std::string const& name)
                : BaseMetadata(name)
                , m_unit(UnitTraits::defaultUnit())
                , m_value(0.0)
            {}
            AnthropoMetadata(tinyxml2::XMLElement* element)
                : m_unit(UnitTraits::defaultUnit())
                , m_value(0.0)
            {
                parseXml(element);
            }

            UnitType const& unit() const { return m_unit; }
            UnitType& unit() { return m_unit; }

            void serializeXml(tinyxml2::XMLElement* element) {
                tinyxml2::XMLElement* xmlAnthropo = element->GetDocument()->NewElement("anthropometry");
                BaseMetadata::serializeXml(xmlAnthropo);
                //AnthropoMetadataUnit<typeunit>::serializeXml(xmlAnthropo);
                tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
                std::stringstream ss;
                ss << value();
                xmlValue->SetText(ss.str().c_str());
                xmlAnthropo->InsertEndChild(xmlValue);
                element->InsertEndChild(xmlAnthropo);
            }

            void serializePmr(tinyxml2::XMLElement* element) {
                tinyxml2::XMLElement* xmlAnthropo = element->GetDocument()->NewElement("anthropometry");
                BaseMetadata::serializePmr(xmlAnthropo);
                tinyxml2::XMLElement* xmlUnits = element->GetDocument()->NewElement("units");
                xmlUnits->SetAttribute(units::UnitTraits<UnitType>::type().c_str(), units::UnitTraits<UnitType>::unitToStr(m_unit).c_str());
                xmlAnthropo->InsertEndChild(xmlUnits);
                tinyxml2::XMLElement* xmlValue = element->GetDocument()->NewElement("value");
                std::stringstream ss;
                ss << value();
                xmlValue->SetText(ss.str().c_str());
                xmlAnthropo->InsertEndChild(xmlValue);
                element->InsertEndChild(xmlAnthropo);
            }

            bool isDefined() const { return (m_value != 0.0); }

            void setValue(double const& value) {
                if (value < 0)
                    throw std::runtime_error("AnthropometricMetadata::setValue: negative value not allowed");
                m_value = value;
            }
            double value() const { return m_value; }

            /// convert the metadata value
            void convertToUnit(UnitType targetUnit) {
                if (m_unit == targetUnit)
                    return;
                units::convert(m_unit, targetUnit, m_value);
                m_unit = targetUnit;
            }
        protected:

            UnitType m_unit; ///< unit of the metadata
            double m_value; ///< value of the metadata

            void parseXml(tinyxml2::XMLElement* element) {
                BaseMetadata::parseXml(element);
                //AnthropoMetadataUnit<typeunit>::parseXml(element);
                double value;
                if (element->FirstChildElement("value") != nullptr && element->FirstChildElement("value")->GetText() != nullptr){
                    std::stringstream ss(element->FirstChildElement("value")->GetText());
                    ss >> value;
                    setValue(value);
                }
            }

        };

        class HBM_EXPORT AnthropoMetadataHeight : public AnthropoMetadata<units::Length> {
        public:
            AnthropoMetadataHeight();
            AnthropoMetadataHeight(tinyxml2::XMLElement* element);
        };

        class HBM_EXPORT AnthropoMetadataWeight : public AnthropoMetadata<units::Mass> {
        public:
            AnthropoMetadataWeight();
            AnthropoMetadataWeight(tinyxml2::XMLElement* element);
        };

        class HBM_EXPORT AnthropoMetadataAge : public AnthropoMetadata<units::Age> {
        public:
            AnthropoMetadataAge();
            AnthropoMetadataAge(tinyxml2::XMLElement* element);
        };

        class HBM_EXPORT Anthropometry {

        public:
            Anthropometry() {}
            void setAnthropometry(tinyxml2::XMLElement* element);
            void serializeXml(tinyxml2::XMLElement* element);
            void serializePmr(tinyxml2::XMLElement* element);
            void convertToUnit(units::Length targetLengthUnit, units::Mass targetMassUnit, units::Age targetAgeUnit);
            void convertToUnit(units::Length targetLengthUnit);
            void convertToUnit(units::Mass targetMassUnit);
            void convertToUnit(units::Age targetAgeUnit);
            AnthropoMetadataAge age;
            AnthropoMetadataWeight weight;
            AnthropoMetadataHeight height;
            void clear();
        };
    }
}

#endif
