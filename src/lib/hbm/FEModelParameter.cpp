/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "FEModelParameter.h"

#include "xmlTools.h"

#include <sstream>
#include <algorithm>

using namespace std;
namespace piper { 
	namespace hbm {
		FEModelParameter::FEModelParameter(): MeshComponent< map_parameter, FEModelParameter>( ) {};

		FEModelParameter::FEModelParameter(tinyxml2::XMLElement* element):
			MeshComponent< map_parameter, FEModelParameter>( true) {
				parseXml( element);
		}

		FEModelParameter::FEModelParameter(const Id& nid): MeshComponent< map_parameter, FEModelParameter>( nid) {};

		void FEModelParameter::setValue( const std::string& valuename, const std::vector<double>& value) {
			string lowervaluename( valuename);
			std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
            m_def[lowervaluename] = value;
		}

        void FEModelParameter::appendValue(const std::string& valuename, const std::vector<double>& value) {
            string lowervaluename(valuename);
            std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
            m_def[lowervaluename].insert(m_def[lowervaluename].end(), value.begin(), value.end());
        }

		vector<double> FEModelParameter::get( const string& valuename) const {
			string lowervaluename( valuename);
			std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
			return m_def.at(lowervaluename);
		}

		void FEModelParameter::deleteParam( const std::string& valuename) {
			string lowervaluename( valuename);
			std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
			m_def.erase( lowervaluename);
		}

		set<string> FEModelParameter::getListParameterNames() const {
			std::set<std::string> names;
			for (map_parameter::const_iterator it=m_def.begin(); it!=m_def.end(); ++it) {
				std::string curname=it->first;
				names.insert( curname);
			}
			return names;
		}

		void FEModelParameter::serializeXml(tinyxml2::XMLElement* element) const {
			tinyxml2::XMLElement* xmlParamId;
			xmlParamId = element->GetDocument()->NewElement("Id");
			xmlParamId->SetText( m_id);
			element->InsertEndChild(xmlParamId);
			for (map_parameter::const_iterator it=m_def.begin(); it!=m_def.end(); ++it) {
				tinyxml2::XMLElement* xmlParam = element->GetDocument()->NewElement("Parameter");
				element->InsertEndChild(xmlParam);
				tinyxml2::XMLElement* elementparam = element->GetDocument()->NextSiblingElement("Parameter");
				tinyxml2::XMLElement* xmlParamName = xmlParam->GetDocument()->NewElement("name");
				xmlParamName->SetText( it->first.c_str());
				xmlParam->InsertEndChild(xmlParamName);
				tinyxml2::XMLElement* xmlParamValue = xmlParam->GetDocument()->NewElement("values");
                serializeValueCont( xmlParamValue, it->second);
				xmlParam->InsertEndChild(xmlParamValue);
			}
		}

		void FEModelParameter::parseXml(tinyxml2::XMLElement* element) {
			std::stringstream ids(element->FirstChildElement("Id")->GetText());
			ids >> m_id;
			tinyxml2::XMLElement* elementparam=element->FirstChildElement("Parameter");
			while(elementparam!=nullptr) {
				string name;
				stringstream names(elementparam->FirstChildElement("name")->GetText());
				vector<double> values;
                parseValueCont(values, elementparam->FirstChildElement("values"));
				if (names >> name) {
					string lowervaluename( name);
					std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
					m_def[lowervaluename] = values;
				}
				elementparam=elementparam->NextSiblingElement("Parameter");
			}
		}


		
		void FEModelParameters::setEntity(std::shared_ptr<FEModelParameter> param) {
			MeshComponentContainer< FEModelParameter>::setEntity( param);
			/*updateListParameters( param->getId());*/
		}		
		
		//void FEModelParameters::updateListParameters( const Id& id) {
		//	set<string> curdef=this->getEntity( id)->getListParameterNames();
		//	for (set<string>::const_iterator itt=curdef.begin(); itt!=curdef.end(); ++itt) {
		//		if (m_listparam.find( *itt) == m_listparam.end()) {
		//			SId sid;
		//			sid.insert( id);
		//			m_listparam.insert( pair<string, SId>( *itt, sid));
		//		}
		//		else {
		//			m_listparam[ *itt].insert( id);
		//		}
		//	}
		//}

		void FEModelParameters::setValue( const Id& id, const std::string& valuename, const std::vector<double>& value) {
			string lowervaluename( valuename);
			std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
			this->getEntity( id)->setValue( lowervaluename, value);
			//updateListParameters( id);
		}

        void FEModelParameters::appendValue(const Id& id, const std::string& valuename, const std::vector<double>& value) {
            string lowervaluename(valuename);
            std::transform(lowervaluename.begin(), lowervaluename.end(), lowervaluename.begin(), ::tolower);
            this->getEntity(id)->appendValue(lowervaluename, value);
            //updateListParameters( id);
        }


	}
}
