/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_ENTITY_H
#define PIPER_HBM_ENTITY_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "BaseMetadata.h"


#include <map>

namespace piper {
namespace hbm {

class FEModel;

/**
 * @brief Anatomical entity.
 *
 * @author Thomas Lemaire @date 2015
 */
class HBM_EXPORT Entity: public BaseMetadataGroup {
public:

    Entity();
    Entity(std::string const& name);
    Entity(tinyxml2::XMLElement* element);

    /// Returns any vertex which belongs to that entity
    std::vector<double> getOneVertex(FEModel const* fem) const;

    /// @return the ids of the nodes of the 2D envelop of the entity
    VId const& getEnvelopNodes(FEModel const& femodel) const;
    /// @return a map to get the index from the id of a node of the envelop
    std::map<Id, unsigned int> const& getEnvelopNodesToIndexMap(FEModel const& femodel) const;
    /// @return the ids of the 2D elements which make up the envelop of the entity
    std::vector<ElemDef> const& getEnvelopElements(FEModel const& femodel) const;
    
	/// gather nodes ids that composed the entity in \a vecId
    /// ids are sorted by increasing value and appear only once.
    ///
	/** gather element definition that composed the entity in \a vecElemDef
	*/
	void getEntityElemDef(const FEModel& femodel, std::vector<ElemDef> & vecElemDef) const;

    /// <param name="femodel">The femodel.</param>
	/// <param name="skip1D">If true, nodes belonging to 1D elements will not be added to the output list.</param>
	/// <param name="skip2D">If true, nodes belonging to 2D elements will not be added to the output list.</param>
	/// <param name="skip3D">If true, nodes belonging to 3D elements will not be added to the output list.</param>
    /// \returns the entity nodes id
    VId getEntityNodesIds(const FEModel& femodel, bool skip1D = false, bool skip2D = false, bool skip3D = false) const;

    /// \deprecated prefer the version which returns the VId
    void getEntityNodesIds(const FEModel& femodel, VId & vecNodeId, bool skip1D = false, bool skip2D = false, bool skip3D = false) const;

    mutable bool isEnvelopValid;
    mutable int numberClosePositiveSurface;
    mutable int numberCloseNegativeSurface;
    mutable bool hasInvertedElements;

    void setFlagNormalesInner(bool const& truefalse) { m_innernormal = truefalse; }

    virtual void serializeXml(tinyxml2::XMLElement* element) const;
    virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;
private:


    /// fill a vector of element definition with 2D elements that constitute the envelopp of the entity
    void computeEntityEnvelop(const FEModel& femodel) const;

    bool m_innernormal;
    mutable bool m_isEnvelopComputed;
    mutable VId m_envelopNodes;
    mutable std::map<Id, unsigned int> m_envelopNodesToIndex;
    mutable std::vector<ElemDef> m_envelopElements;

    virtual bool has_groupNode() const { return false; }
    virtual bool has_groupElement1D() const { return true; }
    virtual bool has_groupElement2D() const { return true; }
    virtual bool has_groupElement3D() const { return true; }
    virtual bool has_groupGroup() const { return true; }

    virtual void parseXml(tinyxml2::XMLElement* element);
};

}
}

#endif // PIPER_HBM_ENTITY_H
