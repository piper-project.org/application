/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef CONTOUR_H
#define CONTOUR_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "Node.h"
#include <map>

namespace piper {
	namespace hbm {

		
		//!  Contour Class. 
		/*!
		Contours : set of points
		can vbe made through :
		1) Polyline : contours made up of a set of points so here the points are already known and we don't have to compute them
		2) Splinec : points on a spline , the u ratio and the no of desired points
		3) Circle : points on a circle , just need to know the radius1 , radius 2 and no of desired points and you can generate the contour points.		
		*/

	
		class HBM_EXPORT Contour  {

			public:

				//! A constructor.
				Contour();

				//! A constructor.
				/*!
				A contour constructor for POLYLINE contours.
				*/
				Contour(int , int , int , int , int  , std::vector<NodePtr> *   );     

				//! A constructor.
				/*!
				A contour constructor for polyline contours.
				*/
				Contour(int , int , int , int , int  , std::vector<NodePtr> * , std::vector<double>* , int  );  

				//! A constructor.
				/*!
				A contour constructor for CIRCLE_CONTOUR contours.
				*/
				Contour(int , int , int , NodePtr , int  , double , double , int   );  //constructor3
				//Constructor

				//! A destructor.
//				~Contour();
				
				
				//! Basic member variables

				int contid;		/*!< ID of contour */ 

				int sequence_no;	/*!< Index in a particular set of vector<Contour*> */	 

				int body_region;     /*!< No to identify to which body region contour belongs */   

				int planeid;			/*!< plane on which contour is defined */  

				int lcsid;		/*!< Local Co-ordinate system */  

                std::map< std::string, std::vector<NodePtr> > mcontour;

				std::vector<NodePtr> contour_points;  /*!< vector of all contour points */   

				std::vector<Node> initial_contour_points;  /*!< vector of all contour points final state */    

				std::vector<Node> final_contour_points;   /*!< vector of all contour points initial state */  

				std::vector<Node> contour_points_d;   /*!< vector of all contour points */  

				std::vector<NodePtr> modified_contour_points;   /*!< vector of modified contour points */  

				std::vector<NodePtr> spline_points;           /*!< vectorn of spline contour points */  

				std::vector<NodePtr> plane_points;

				std::vector<double> spline_u;             /*!< vector of four u points while reading the contour_input.txt */       

				int no_of_desired_points;                      /*!< total desired points we want on the contours */       

				enum contour_type_t { CIRCLE_CONTOUR, POLYLINE_CONTOUR, CUBIC_INTERP_SPLINE_CONTOUR, PIECEWISE_CUBIC_SPLINE_CONTOUR }; /*!< type of contour */       

				enum contour_type_t contour_type;       /*!< type of contour */

				double radius1;       /*!< radius1 for circle contour */  

				double radius2;        /*!< radius2 for circle contour */  

				NodePtr centre;     /*!< centre for circle contour */  
 								
				Node centroid;   /*!< centroid of contour points */ 

				Node modified_centroid;   /*!< modified centroid of contour points */ 
				
				double uvalue;
				std::vector<double> abcd_contplane;

				//! Basic member functions

				//! A normal member taking reference of vector of pointer to the Nodes of contour points and generate a contour.
				/*!
				\param a vector of pointer to Node.
				\generate a contour
				*/
				void generate_contour(const std::vector<Node*>&);

				//! A normal member to calculate the centroid from contour points.
				Node centroid_calculation();

				Coord getCentroidCoord();

				std::vector<double> ABCDforPlane(NodePtr p1, NodePtr p2, NodePtr p3);


				void setABCDcontPlane();

				void setContourCentroid();

				void setUvalue(double);
				
				//! A normal member taking a vector of pointer to the nodes of contour points and a double ratio and enlarge the contour.
				/*!
				\param a vector of pointer to Node.
				\param double ratio.
				\enlarge a contour
				*/
				std::vector<NodePtr> enlarge_contour(std::vector<NodePtr> con_pts, double del);

				void get_points_for_plane();

				int get_id(){ return contid; };	  /// get the unique contour id

				int get_sequence_no(){ return sequence_no; };		/// get the sequence no of contour in the body region

				int& get_body_region(){ return body_region; };       /// The body region to which the contour belongs
	
				int get_planeid(){ return planeid; };			/// get the plane id 
	
				int get_lcsid(){ return lcsid; };		/// get the local coordinate system id
		
				std::vector<NodePtr> get_contour_points(){ return contour_points; };         /// get the vector points
	
				std::vector<NodePtr> get_modified_contour_points(){ return modified_contour_points; };   /// get the modified contour points

				int get_no_of_desired_points(){ return no_of_desired_points; };     /// get the no of desired points for a given contour

				enum contour_type_t get_contour_type(){ return contour_type; };   /// get the contour type

				void set_body_region(int region){ body_region = region; };    /// set the body region

				void set_planeid(const int pid){ planeid = pid; };     /// set the plane id

				void set_lcs(const int lid){ lcsid = lid;};         // set the local coordinate system id

				void set_sequence_no(int num){ sequence_no = num; };      /// set the sequence no of the contours

				void set_no_of_desired_points(int num){ no_of_desired_points = num; };    /// set the no of desired no of contour points

				std::vector<Node> get_contour_points_d(){ return contour_points_d; };         /// get the vector points

				double getUparamForSplineContour(){
					return uvalue;
				}
				std::vector<double> getABCDofContourPlane(){
					return abcd_contplane;
				}

		};



		//!  Human Body Model Contour(HBM_contours) Class. 
		/*!
		A Class which will help store contours in respective body region so that we can select the contours on the basis of body region
		HBM_contours : Contours divided according to body regions
		There are 34 body regions. Each Body regions contains a set of contours.
		HBM_contours will be used to reference the contours in a body region
		*/

		
	class HBM_contours {

			public:

				//! Basic member variables

				//!  vector of pointer to the contours
				/*!  Vector of all pointer to the contours in respective boduy regions */
				std::vector<Contour*> right_hand_palm, left_hand_palm, left_elbow_back, right_elbow_back;   
				std::vector<Contour*> right_hand_arm, right_elbow_front, right_hand_forearm, right_thorax_lower, right_hand_arm_upper, right_thorax_upper;
				std::vector<Contour*> left_hand_arm, left_elbow_front, left_hand_forearm, left_thorax_lower, left_hand_arm_upper, left_thorax_upper;
				std::vector<Contour*> left_leg_femur, left_leg_knee, left_leg_tibia, right_leg_femur, right_leg_knee, right_leg_tibia;
				std::vector<Contour*> joint_contours, joint_contours_a, joint_contours_b, joint_contours_j1, joint_contours_j2;
				std::vector<Contour*> left_thigh, left_ankle_to_calf, left_foot, left_ankle, left_foot_for, knee_left1;

				std::vector<Contour*>	Right_Foot;                   /// body_region = 1
				std::vector<Contour*>	Left_Foot;					  /// body_region = 2
				std::vector<Contour*>	Right_Ankle_to_Calf;		  /// body_region = 3
				std::vector<Contour*>	Left_Ankle_to_Calf;			  /// body_region = 4
				std::vector<Contour*>	Right_Knee_lower;			  /// body_region = 5
				std::vector<Contour*>	Left_Knee_lower;			  /// body_region = 6
				std::vector<Contour*>	Right_Knee_upper;			  /// body_region = 7
				std::vector<Contour*>	Left_Knee_upper;			  /// body_region = 8
				std::vector<Contour*>	Right_Thigh;				  /// body_region = 9
				std::vector<Contour*>	Left_Thigh;					  /// body_region = 10
				std::vector<Contour*>	Hip;						  /// body_region = 11
				std::vector<Contour*>	Abdomen;					  /// body_region = 12
				std::vector<Contour*>	Thorax;						  /// body_region = 13
				std::vector<Contour*>	Neck;						  /// body_region = 14
				std::vector<Contour*>	Head;						  /// body_region = 15
				std::vector<Contour*>	Right_Palm;					  /// body_region = 16
				std::vector<Contour*>	Left_Palm;					  /// body_region = 17
				std::vector<Contour*>	Right_Forearm;				  /// body_region = 18
				std::vector<Contour*>	Left_Forearm;				  /// body_region = 19
				std::vector<Contour*>	Right_Elbow_lower;			  /// body_region = 20
				std::vector<Contour*>	Left_Elbow_lower;			  /// body_region = 21
				std::vector<Contour*>	Right_Elbow_upper;			  /// body_region = 22
				std::vector<Contour*>	Left_Elbow_upper;			  /// body_region = 23
				std::vector<Contour*>	Right_Biceps;				  /// body_region = 24
				std::vector<Contour*>	Left_Biceps;				  /// body_region = 25
				std::vector<Contour*>	Right_biceps_to_shoulder;	  /// body_region = 26
				std::vector<Contour*>	Left_biceps_to_shoulder;	  /// body_region = 27
				std::vector<Contour*>	Hip_right;   				  /// body_region = 28
				std::vector<Contour*>	Hip_left;   				  /// body_region = 29
				std::vector<Contour*>	Keypoints;   				  /// body_region = 30
				std::vector<Contour*>	Right_Abdomen;   		      /// body_region = 31
				std::vector<Contour*>	Left_Abdomen;   			  /// body_region = 32
				std::vector<Contour*>	Right_Thorax;   			  /// body_region = 33
				std::vector<Contour*>	Left_Thorax;   				  /// body_region = 34
				std::vector<std::vector<Contour*>> full_body;         /*!< Vector of all contour points in the Full Body */  

				// For doing the sequencing of the contours in a body region
				static bool sequencing(Contour* a, Contour* b){
					return (a->get_sequence_no() < b->get_sequence_no());
				};
	
				// by sort_all all the contours in each body region we be sorted according to their body region
				void sort_all()
				{

					sort(Right_Foot.begin(), Right_Foot.end(), sequencing);
					sort(Left_Foot.begin(), Left_Foot.end(), sequencing);
					sort(Right_Ankle_to_Calf.begin(), Right_Ankle_to_Calf.end(), sequencing);
					sort(Left_Ankle_to_Calf.begin(), Left_Ankle_to_Calf.end(), sequencing);
					sort(Right_Knee_lower.begin(), Right_Knee_lower.end(), sequencing);
					sort(Left_Knee_lower.begin(), Left_Knee_lower.end(), sequencing);
					sort(Right_Knee_upper.begin(), Right_Knee_upper.end(), sequencing);
					sort(Left_Knee_upper.begin(), Left_Knee_upper.end(), sequencing);
					sort(Right_Thigh.begin(), Right_Thigh.end(), sequencing);
					sort(Left_Thigh.begin(), Left_Thigh.end(), sequencing);
					sort(Hip.begin(), Hip.end(), sequencing);
					sort(Abdomen.begin(), Abdomen.end(), sequencing);
					sort(Thorax.begin(), Thorax.end(), sequencing);
					sort(Neck.begin(), Neck.end(), sequencing);
					sort(Head.begin(), Head.end(), sequencing);
					sort(Right_Palm.begin(), Right_Palm.end(), sequencing);
					sort(Left_Palm.begin(), Left_Palm.end(), sequencing);
					sort(Right_Forearm.begin(), Right_Forearm.end(), sequencing);
					sort(Left_Forearm.begin(), Left_Forearm.end(), sequencing);
					sort(Right_Elbow_lower.begin(), Right_Elbow_lower.end(), sequencing);
					sort(Left_Elbow_lower.begin(), Left_Elbow_lower.end(), sequencing);
					sort(Right_Elbow_upper.begin(), Right_Elbow_upper.end(), sequencing);
					sort(Left_Elbow_upper.begin(), Left_Elbow_upper.end(), sequencing);
					sort(Right_Biceps.begin(), Right_Biceps.end(), sequencing);
					sort(Left_Biceps.begin(), Left_Biceps.end(), sequencing);
					sort(Right_biceps_to_shoulder.begin(), Right_biceps_to_shoulder.end(), sequencing);
					sort(Left_biceps_to_shoulder.begin(), Left_biceps_to_shoulder.end(), sequencing);
					sort(Hip_right.begin(), Hip_right.end(), sequencing);
					sort(Hip_left.begin(), Hip_left.end(), sequencing);
					sort(Keypoints.begin(), Keypoints.end(), sequencing);
					sort(Right_Abdomen.begin(), Right_Abdomen.end(), sequencing);
					sort(Left_Abdomen.begin(), Left_Abdomen.end(), sequencing);
					sort(Right_Thorax.begin(), Right_Thorax.end(), sequencing);
					sort(Left_Thorax.begin(), Left_Thorax.end(), sequencing);

					full_body.push_back(Right_Foot);
					full_body.push_back(Left_Foot);
					full_body.push_back(Right_Ankle_to_Calf);
					full_body.push_back(Left_Ankle_to_Calf);
					full_body.push_back(Right_Knee_lower);
					full_body.push_back(Left_Knee_lower);
					full_body.push_back(Right_Knee_upper);
					full_body.push_back(Left_Knee_upper);
					full_body.push_back(Right_Thigh);
					full_body.push_back(Left_Thigh);
					full_body.push_back(Hip);
					full_body.push_back(Abdomen);
					full_body.push_back(Thorax);
					full_body.push_back(Neck);
					full_body.push_back(Head);
					full_body.push_back(Right_Palm);
					full_body.push_back(Left_Palm);
					full_body.push_back(Right_Forearm);
					full_body.push_back(Left_Forearm);
					full_body.push_back(Right_Elbow_lower);
					full_body.push_back(Left_Elbow_lower);
					full_body.push_back(Right_Elbow_upper);
					full_body.push_back(Left_Elbow_upper);
					full_body.push_back(Right_Biceps);
					full_body.push_back(Left_Biceps);
					full_body.push_back(Right_biceps_to_shoulder);
					full_body.push_back(Left_biceps_to_shoulder);
					full_body.push_back(Hip_right);
					full_body.push_back(Hip_left);
					full_body.push_back(Keypoints);
					full_body.push_back(Right_Abdomen);
					full_body.push_back(Left_Abdomen);
					full_body.push_back(Right_Thorax);
					full_body.push_back(Left_Thorax);

				}

					// It will clear all the body region vectors which contain the contours  
					void clear()
					{
						full_body.clear();
						Right_Foot.clear();                 
						Left_Foot.clear();					
						Right_Ankle_to_Calf.clear();		
						Left_Ankle_to_Calf.clear();			
						Right_Knee_lower.clear();			
						Left_Knee_lower.clear();			
						Right_Knee_upper.clear();			
						Left_Knee_upper.clear();			
						Right_Thigh.clear();				
						Left_Thigh.clear();					
						Hip.clear();						
						Abdomen.clear();					
						Thorax.clear();						
						Neck.clear();						
						Head.clear();						
						Right_Palm.clear();					
						Left_Palm.clear();					
						Right_Forearm.clear();				
						Left_Forearm.clear();				
						Right_Elbow_lower.clear();			
						Left_Elbow_lower.clear();			
						Right_Elbow_upper.clear();			
						Left_Elbow_upper.clear();			
						Right_Biceps.clear();				
						Left_Biceps.clear();				
						Right_biceps_to_shoulder.clear();	
						Left_biceps_to_shoulder.clear();	
						Hip_right.clear();   				
						Hip_left.clear();   				
						Keypoints.clear();   				
						Right_Abdomen.clear();   		    
						Left_Abdomen.clear();   			
						Right_Thorax.clear();   			
						Left_Thorax.clear();   				
					}
	
			};


	
		//!  Contour Class. 
		/*!
		Contours : set of points
		can vbe made through :
		1) Polyline : contours made up of a set of points so here the points are already known and we don't have to compute them
		2) Splinec : points on a spline , the u ratio and the no of desired points
		3) Circle : points on a circle , just need to know the radius1 , radius 2 and no of desired points and you can generate the contour points.
		*/
		// A Class which will help store contours in respective body region so that we can select the contours on the basis of body region
	

	class contour_detail
{

public:
	std::map<int, Contour*> contour_map;

};




	class HBM_EXPORT Plane  {
	public:

		Plane() {};
		Plane(int pid) {};

        Plane(const int pid, NodePtr n1, NodePtr n2, NodePtr n3);
		Plane(std::string const& name);


		// ------------- utlities --------------
        void setPlanePoints(NodePtr n1, NodePtr n2, NodePtr n3);
		double& getPontA();






		/**********************        Aditya         ******************/



		int plane_id;
		void read_plane(std::string file);
//@		static int max_id;

		mutable int id;


		double a, b, c, d;		// coefficients of Plane equation "ax + by + cz + d = 0"
        NodePtr p1, p2, p3, p4;
        NodePtr disp_p1, disp_p2, disp_p3, disp_p4;




//@		static int get_next_id();

		void set_id(int) const;

//@		void set_max_id(int id){ if (max_id<id){ max_id = id; }; };

		//void set_base_cs(const coord_system_t&);

        std::vector<NodePtr> get_3points(){
            std::vector<NodePtr> points;
			points.push_back(p1);
			points.push_back(p2);
			points.push_back(p3);

			return points;
		}


        std::vector<double> generate_abcd(NodePtr p1, NodePtr p2, NodePtr p3)
		{
			double x1 = p1->getCoordX(); double x2 = p2->getCoordX(); double x3 = p3->getCoordX();
			double y1 = p1->getCoordY(); double y2 = p2->getCoordY(); double y3 = p3->getCoordY();
			double z1 = p1->getCoordZ(); double z2 = p2->getCoordZ(); double z3 = p3->getCoordZ();

			a = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
			b = -((x2 * z3 - x3 * z2) - (x1 * z3 - x3 * z1) + (x1 * z2 - x2 * z1));
			c = (x2 * y3 - x3 * y2) - (x1 * y3 - x3 * y1) + (x1 * y2 - x2 * y1);
			d = -(x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1));

			std::vector<double> zk;
			zk.push_back(a);
			zk.push_back(b);
			zk.push_back(c);
			zk.push_back(d);


			// determine display quad from 3 points
			/*disp_p1 = p1;
			disp_p2 = p2;
			disp_p3 = p3;
			disp_p4 = p3;	// for now, just display a triangle*/
			return zk;
		}



	};



	}
	
	
}

#endif
