/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHCOMPONENT__H
#define MESHCOMPONENT__H

#pragma warning(disable:4251) // this should eventually be removed and dealt with properly through pImpl or other means

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include<unordered_map>
#include<iostream>

#include "IdGenerator.h"

namespace piper { 
	namespace hbm {

		class IBase {
		public:
			virtual void convId(std::unordered_map<Id,Id> const& map_id)=0;
		};

		template< typename Tdef, typename Tid>
		class MeshComponent: public IBase, public IdGenerator<Tid> {
		public:

			// ------------- copy-constructor --------------
            MeshComponent(const MeshComponent& other);
            explicit MeshComponent(const Id id);
			// ------------- assignement-operator --------------
			MeshComponent<Tdef, Tid>& operator=( const MeshComponent<Tdef, Tid>& other );
			// ------------- destructor --------------
			~MeshComponent();

			// ------------- interface --------------
            virtual void convId(std::unordered_map<Id,Id> const& map_id) {}
			// ------------- uitilities Id --------------
            const Id& getId() const {return m_id;}
			Id& getId() {return m_id;};
            void setId( const Id& id) {m_id=id;}
			// ------------- utilities --------------
			// ------------- uitilities Def --------------
            Tdef& get() {return m_def;}
            const Tdef& get() const {return m_def;}
			size_t size() const {return m_def.size();}
            void set( const Tdef& def) {m_def=def;}

		protected:
			Id m_id;
			Tdef m_def;

			// ------------- constructor --------------
			MeshComponent( );

		};


	}
}

#include "MeshComponent.inl"
#endif

