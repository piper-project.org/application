/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHCOMPONENTCONTAINER__H
#define MESHCOMPONENTCONTAINER__H


#include "MeshComponent.h"

#include <algorithm>
#include <memory>

namespace piper { 
	namespace hbm {

        template< typename Tcomponent>
        class MeshComponentContainer: public IBase  {

            typedef typename std::vector   < std::shared_ptr<Tcomponent>> Vector_container;

		public:
            // ------------- constructor --------------
            MeshComponentContainer() {}
            MeshComponentContainer( const std::string& type): m_type(type) {}
            // ------------- copy-constructor --------------
            MeshComponentContainer( const MeshComponentContainer& other );
            // ------------- Assignment operator --------------
            MeshComponentContainer & operator=(const MeshComponentContainer& other);
            // ------------- destructor --------------
            virtual ~MeshComponentContainer() ;
            // ------------- interface --------------
            virtual void convId(std::unordered_map<Id,Id> const& map_id);
            // like the copy constructor, but allows to use the same, already initialized pointer
            void makeCopy(const MeshComponentContainer& other);

            virtual void clear();
            // ------------- utilities --------------

            void setEntity(std::shared_ptr<Tcomponent> entity);
            //void concatenate(MeshComponentContainer<Tcomponent>& other);
            std::shared_ptr<Tcomponent> getEntity(const Id& id);
            const std::shared_ptr<Tcomponent> getEntity(const Id& id) const;
            void delEntity(const Id id);            
            void delEntities(const std::vector<Id>& vid);
            void delEmpty();
            void delEmpty( std::vector<Id>& vid);
            size_t size() const;
            Id getMinId() const;
            Id getMaxId() const;
            VId listId() const;

            bool isValidId( const Id id) const;

            Vector_container const& getContainer() const { return container; } //todo ej: to remove
            Vector_container& getContainer() { return container; } //todo ej: to remove

            typedef typename Vector_container::iterator iterator;
            typedef typename Vector_container::const_iterator const_iterator;

            iterator begin() { return container.begin(); }
            const_iterator begin() const { return container.begin(); }
            iterator end() { return container.end(); }
            const_iterator end() const { return container.end(); }
            Id curId(const_iterator it) const {return (*it)->getId();}
            std::shared_ptr<Tcomponent> curDef(const_iterator it) const { return (*it); }
            Id curId(iterator it) { return (*it)->getId(); }
            std::shared_ptr<Tcomponent> curDef(iterator it) { return (*it); }
            
		protected:
            Vector_container container;
            const std::string m_type;

            /// <summary>
            /// Updates this instance.
            /// </summary>
            void update();

		};

        template< typename Tcomponent>
        struct OrderContainer {
            inline bool operator () (const std::shared_ptr<Tcomponent> _left, const Id &id) {
		        return _left->getId() < id;
	        }
            inline bool operator () (const Id &id, const std::shared_ptr<Tcomponent> _right) {
		        return id < _right->getId();
	        }
        };

        template< typename Tcomponent>
        inline bool ComparatorContainer(const std::shared_ptr<Tcomponent> pair1, const std::shared_ptr<Tcomponent> pair2)
        {
	        return pair1->getId() < pair2->getId();
        }

	}
}
#include "MeshComponentContainer.inl"

#endif
