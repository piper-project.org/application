/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef IDGENERATOR__H
#define IDGENERATOR__H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "types.h"

namespace piper { 
	namespace hbm {

		/**
		* @brief The IdGenerator class count mesh components of type T and provides an unique id
		*
		* @author Erwan Jolivet @date 2015
		* \ingroup libhbm
		*/
		template <typename T>
		class HBM_EXPORT IdGenerator {

		public:
			IdGenerator() {
				++counter;
				++lastId;
			}
			~IdGenerator() {
				 --counter;
                 if (counter == 0)
                     lastId = -1;
			 }
			static int getLastId() { 
				return lastId; 
			}
			static int getCounter() { 
				return counter; 
			}

		protected:
			/// last id generate to contrust a mesh component of type T
			static int lastId;
			// counter of mesh component of type T
			static unsigned int counter;

			void setLastId(int const& value) {
                if (lastId<value)
                    lastId = value;
            }
		};
		//template <typename T>
		//int IdGenerator<T>::lastId = -1;
		//template <typename T>
		//unsigned int IdGenerator<T>::counter = 0;
	}
}
//#include "IdGenerator.inl"

#endif
