/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Entity.h"

#include <set>
#include <algorithm>

#include "xmlTools.h"
#include "FEModel.h"


namespace piper {
namespace hbm {

    Entity::Entity() 
        : BaseMetadataGroup()
        , m_isEnvelopComputed(false)
        , isEnvelopValid(false)
        , numberClosePositiveSurface(0)
        , numberCloseNegativeSurface(0)
        , hasInvertedElements(false)
        , m_innernormal(false)
{}

Entity::Entity(std::string const& name)
    : BaseMetadataGroup(name)
    , m_isEnvelopComputed(false)
    , isEnvelopValid(false)
    , numberClosePositiveSurface(0)
    , numberCloseNegativeSurface(0)
    , hasInvertedElements(false)
    , m_innernormal(false)
{}

Entity::Entity(tinyxml2::XMLElement* element)
    : m_isEnvelopComputed(false)
    , numberClosePositiveSurface(0)
    , hasInvertedElements(false)
    , numberCloseNegativeSurface(0)
    , isEnvelopValid(false)
    , m_innernormal(false)
{
    parseXml(element);
}


std::vector<double> Entity::getOneVertex(const FEModel *fem) const
{
    Id idNode;
    if (groupElement2D.size()>0) {
        Id idElem2D = *(fem->getGroupElements2D(*(groupElement2D.begin())).get().begin());
        idNode = *(fem->getElement2D(idElem2D).get().begin());
    }
    else if (groupElement3D.size()>0) {
        Id idElem3D = *(fem->getGroupElements3D(*(groupElement3D.begin())).get().begin());
        idNode = *(fem->getElement3D(idElem3D).get().begin());
    }
    else
        throw std::runtime_error("Cannot get a vertex on Entity: "+name());
    Node const& node = fem->getNode(idNode);
    std::vector<double> vertex;
    vertex.push_back(node.getCoordX());
    vertex.push_back(node.getCoordY());
    vertex.push_back(node.getCoordZ());
    return vertex;
}


VId const& Entity::getEnvelopNodes(const FEModel& femodel) const
{
    if (!m_isEnvelopComputed)
        computeEntityEnvelop(femodel);
    return m_envelopNodes;
}

std::map<Id, unsigned int> const& Entity::getEnvelopNodesToIndexMap(FEModel const& femodel) const
{
    if (!m_isEnvelopComputed)
        computeEntityEnvelop(femodel);
    return m_envelopNodesToIndex;
}

std::vector<ElemDef> const& Entity::getEnvelopElements(const FEModel& femodel) const
{
    if (!m_isEnvelopComputed)
        computeEntityEnvelop(femodel);
    return m_envelopElements;
}

void Entity::computeEntityEnvelop( const FEModel& femodel) const
{
    m_envelopElements.clear();
    m_envelopNodes.clear();
    m_envelopNodesToIndex.clear();
    isEnvelopValid = false;
    numberClosePositiveSurface = -1;
    hasInvertedElements = false;

    // group element 2D
    std::vector<ElemDef> v2d;

    if (groupElement2D.size() > 0 ) {
        //std::cout << "use 2D " << m_name << std::endl;
        for (auto it1 = groupElement2D.begin(); it1 != groupElement2D.end(); ++it1) {
            const VId& sid = femodel.getGroupElements2D(*it1).get();
            for (auto it2 = sid.begin(); it2 != sid.end(); ++it2) {
                if (m_innernormal) {
                    ElemDef curelem = femodel.getElement2D(*it2).get();
                    std::reverse(curelem.begin(), curelem.end());
                    v2d.push_back(curelem);
                }
                else
                    v2d.push_back(femodel.getElement2D(*it2).get());
            }
        }
        isEnvelopValid = Element2D::checkClosedSurface(v2d, femodel, hasInvertedElements); // true if 2D generate a closed surface
        //std::cout << "checkClosedSurface " << m_name << " " << spy << std::endl;

    }


    //std::cout << "v2d.size() : " << v2d.size() << std::endl;

    if (!isEnvelopValid && !hasInvertedElements && groupElement3D.size() > 0){ //no 2D ou 2D not generate a valid closed surface(s)
        //std::cout << "use 3D " << m_name << std::endl;
        std::vector<ElemDef> v3d, v2dadd;
        for (auto it = groupElement3D.begin(); it != groupElement3D.end(); ++it) {
            const VId& sid = femodel.getGroupElements3D(*it).get();
            for(auto it=sid.begin(); it!=sid.end(); ++it) {
                v3d.push_back( femodel.getElement3D(*it).get());
            }
        }
        v2dadd = Element3D::extractElement2DfromElement3Ddefinition( v3d);
        // sort
        if (v2d.size() > 0) {
			sort(v2d.begin(), v2d.end(), OrderElemDef());
			sort(v2dadd.begin(), v2dadd.end(), OrderElemDef());
			std::set_difference(v2dadd.begin(), v2dadd.end(), v2d.begin(), v2d.end(), back_inserter(m_envelopElements), OrderElemDef());
            m_envelopElements.insert(m_envelopElements.end(), v2d.begin(), v2d.end());
        }
        else {
            m_envelopElements = v2dadd;
        }
        isEnvelopValid = Element2D::checkClosedSurface(m_envelopElements, femodel, hasInvertedElements);
    }
    else { // only 2D
        m_envelopElements = v2d;
    }

    //if (!isEnvelopValid)
    //    std::cout << "env issue for : " << m_name << std::endl;

    // get the uniques nodes
    std::set<Id> envelopNodesUnique;
    for (auto it = m_envelopElements.begin(); it != m_envelopElements.end(); ++it)
        envelopNodesUnique.insert(it->begin(), it->end());
    std::move(envelopNodesUnique.begin(), envelopNodesUnique.end(), std::back_inserter(m_envelopNodes));

    int i = 0;
    for (Id envelopNode : m_envelopNodes)
        m_envelopNodesToIndex[envelopNode] = i++;

    m_isEnvelopComputed = true;
}

void Entity::getEntityNodesIds(const FEModel& femodel, VId & vecNodeId, bool skip1D, bool skip2D, bool skip3D) const
{
   vecNodeId =  getEntityNodesIds(femodel, skip1D, skip2D, skip3D);
}

VId Entity::getEntityNodesIds(const FEModel& femodel, bool skip1D, bool skip2D, bool skip3D) const
{
    VId vecNodeId;
	if (!skip1D)
	{
        for (auto it = groupElement1D.begin(); it != groupElement1D.end(); ++it) {
			const VId& sid = femodel.getGroupElements1D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
		            vecNodeId.insert(vecNodeId.end(), femodel.getElement1D(*it).get().begin(), femodel.getElement1D(*it).get().end());
			}
		}
	}
	if (!skip2D)
	{
		for (auto it = groupElement2D.begin(); it != groupElement2D.end(); ++it) {
			const VId& sid = femodel.getGroupElements2D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
				vecNodeId.insert(vecNodeId.end(), femodel.getElement2D(*it).get().begin(), femodel.getElement2D(*it).get().end());
			}
		}
	}
	if (!skip3D)
	{
        for (auto it = groupElement3D.begin(); it != groupElement3D.end(); ++it) {
			const VId& sid = femodel.getGroupElements3D(*it).get();
			for (auto it = sid.begin(); it != sid.end(); ++it) {
		            vecNodeId.insert(vecNodeId.end(), femodel.getElement3D(*it).get().begin(), femodel.getElement3D(*it).get().end());
			}
		}
	}
    std::sort(vecNodeId.begin(), vecNodeId.end());
    VId::iterator it;
    it = std::unique(vecNodeId.begin(), vecNodeId.end());
    vecNodeId.resize(std::distance(vecNodeId.begin(), it));
    return vecNodeId;
}

void Entity::getEntityElemDef(const FEModel& femodel, std::vector<ElemDef> & vecElemDef) const {
	vecElemDef.clear();
	for (auto it = groupElement3D.begin(); it != groupElement3D.end(); ++it) {
		const VId& sid = femodel.getGroupElements3D(*it).get();
		for (auto it = sid.begin(); it != sid.end(); ++it)
			vecElemDef.push_back(femodel.getElement3D(*it).get());
	}
	for (auto it = groupElement2D.begin(); it != groupElement2D.end(); ++it) {
		const VId& sid = femodel.getGroupElements2D(*it).get();
		for (auto it = sid.begin(); it != sid.end(); ++it)
			vecElemDef.push_back(femodel.getElement2D(*it).get());

	}
	for (auto it = groupElement1D.begin(); it != groupElement1D.end(); ++it) {
		const VId& sid = femodel.getGroupElements1D(*it).get();
		for (auto it = sid.begin(); it != sid.end(); ++it)
			vecElemDef.push_back(femodel.getElement1D(*it).get());
	}
}

void Entity::serializeXml(tinyxml2::XMLElement* element) const {
    BaseMetadataGroup::serializeXml(element);
    if (m_innernormal) {
        tinyxml2::XMLElement* xmlBool = element->GetDocument()->NewElement("innerNormal");
        xmlBool->SetText("true");
        element->InsertEndChild(xmlBool);
    }
}

void Entity::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
    BaseMetadataGroup::serializePmr(element, fem);
    if (m_innernormal) {
        element->SetAttribute("innernormals", "true");
    }
}

void Entity::parseXml(tinyxml2::XMLElement* element) {
    BaseMetadataGroup::parseXml(element);
    if (element->FirstChildElement("innerNormal")) {
        std::string boolstr = element->FirstChildElement("innerNormal")->GetText();
        if (boolstr == "true")
            m_innernormal = true;
        else
            m_innernormal = false;
    }

}

}
}


