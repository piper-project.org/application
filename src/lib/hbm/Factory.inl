/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef FACTORY_INL
#define FACTORY_INL

#include "Factory.h"

namespace piper { 
	namespace hbm {

		template <class Object,class Key>
		std::map<Key, std::function<Object*(void)>> Factory<Object,Key>::factoryFunctionRegistry=std::map<Key,std::function<Object*(void)>>();

		template <class Object,class Key>
		Object * Factory<Object, Key>::CreateInstance(Key key)
		{
			Object * instance = nullptr;
			typename std::map<Key, std::function<Object*(void)>>::iterator it=factoryFunctionRegistry.find(key);
			if(it!=factoryFunctionRegistry.end())
			{
				instance=it->second();
			}
			return instance;
		}

		template <class Object,class Key>
		Factory<Object, Key> * Factory<Object, Key>::Instance()
		{
			static Factory<Object, Key> factory;
			return &factory;
		}

		template <class Object,class Key>
		void Factory<Object, Key>::RegisterFactoryFunction(Key key, std::function<Object*(void)> classFactoryFunction)
		{
			// register the class factory function
			factoryFunctionRegistry[key] = classFactoryFunction;
			//std::cout << "Registering class '" << key << "'\n";

		}
	}
}

#endif
