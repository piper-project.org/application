/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_FRAME_H
#define PIPER_HBM_FRAME_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <array>
#include <map>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "MeshComponentIdName.h"
#include "Node.h"
#include "tinyxml/tinyxml2.h"

namespace piper {
	namespace hbm {
            HBM_EXPORT enum class AxisName {X=0, Y, Z};
            static const std::map<std::string, AxisName> strToAxisName = { { "x", AxisName::X }, { "X", AxisName::X },
                                                                           { "y", AxisName::Y }, { "Y", AxisName::Y },
                                                                           { "z", AxisName::Z }, { "Z", AxisName::Z } };
            static const std::array<std::string,3> AxisNameToStr={{"x","y","y"}};

		/**
		* @brief A Frame.
		*
		* Container for a frame defined by three nodes and store information to be able to generate orientation quaternion 
		*
		* @author Erwan Jolivet @date 2015
		*/
		class HBM_EXPORT FEFrame : public MeshComponentIdName< VId, FEFrame> 
        {
		public :

			//enum AxisName {
			//	X, /*!<  defines local x axis */
			//	Y, /*!<  defines local y axis */
			//	Z  /*!<  defines local z axis */
			//};

			/*!
			*  \brief Constructor
			*
			*  Constructor of Frame
			*
			*/
            explicit FEFrame(tinyxml2::XMLElement* element);
            FEFrame();

            ~FEFrame() {};

			/*!
			*  \brief set origin of the frame
			*
			*  \param id:  node id for origin
			*
			*/
			void setOrigin( const Id& id);
			/*!
			*  \brief set first direction of the frame
			*
			*  \param id: node id along the first direction
			*  \param dir: axis name of the first direction
			*/
			void setFirstDirection( const Id& id, AxisName dir);
			void setFirstDirection( const Id& id);
			void setFirstDirection( AxisName dir);
			void setFirstDirection( const std::string& dir);
			/*!
			*  \brief set node id to defined plane
			*
			*  \param id: node id to defined plane with origin and first direction node
			*  \param dir: definition of axis to define local plan with first axis
			*/
			void setSecondDirection( const Id& id, AxisName dir);
			void setSecondDirection( const Id& id);
			void setSecondDirection( AxisName dir);
			void setSecondDirection( const std::string& dir);
			/*!
			*  \brief normalized second direction, defined new node coord
			*
			*/
			Coord normSecondDirection( const Coord& org, const Coord& coord, const double& norm);
			/*!
			*  \brief normalized first direction, defined new node coord
			*
			*/
			Coord normFirstDirection( const Coord& org, const Coord& coord, const double& norm);
			/*!
			*  \brief get node id of origin
			*
			*  \return id: node id for origin
			*/
			const Id& getOriginId() const {return m_origin;}
            bool hasOrigin() const {return m_hasorigin;}
			/*!
			*  \brief get node id of first direction
			*
			*  \return id: node id of first direction
			*/
			const Id& getFirstDirectionId() const {return m_firstd;}
			/*!
			*  \brief get axis of the first direction
			*
			*  \return id: axis of the first direction
			*/
			AxisName getFirstDirection() const {return m_firstdirection;}
			/*!
			*  \brief get node id for plane definition
			*
			*  \return id: node id for plane definition
			*/
			const Id& getPlaneId() const {return m_planedef;}
			/*!
			*  \brief get plane description
			*
			*  \return id: plane description
			*/
			AxisName getSecondDirection() const {return m_plane;}
			/*!
			*  \brief get matrix of orientation of the frame
			*
			*  \param org: coord of origin
			*  \param cfirst: coord of point along first direction
			*  \param cplane: coord of point on plane
			*  \param firstdirection: description of the first direction
			*  \param SecondDirection: description of the plane
			*  \param orient: array of 9 double, define vectors of orientation in row-major format
			*/
		static void getMatOrientation( const Coord& org, const Coord& cfirst, const Coord& cplane,
				AxisName firstdirection,
				AxisName SecondDirection,
				Eigen::Matrix3d& orient);
			/*!
			*  \brief get quaternion of orientation of the frame
			*
			*  \param org: coord of origin
			*  \param cfirst: coord of point along first direction
			*  \param cplane: coord of point on plane
			*  \param firstdirection: description of the first direction
			*  \param SecondDirection: description of the plane
			*  \param quat: quaternion to represent frame orientation
			*/
			static void getQuatOrientation( const Coord& org, const Coord& cfirst, const Coord& cplane,
				AxisName firstdirection,
				AxisName SecondDirection,
				Eigen::Quaternion<double>& quat);

			bool isCompletelyDefined() const;
			bool hasOnlyOrigin() const;
            bool isNormalizedFirstDirection() const {return m_normalizedFirstdirection;}
            bool isNormalizedSecondDirection() const {return m_normalizedSeconddirection;}
            double getNormFirstDirection() const {return m_normFirstdirection;}
            double getNormSecondDirection() const {return m_normSeconddirection;}

			void serializeXml(tinyxml2::XMLElement* element) const; 

			void register_frameinfo(std::vector<Id>);

			// ------------- interface --------------
			void convId(std::unordered_map<Id,Id> const& map_id);



		private :
			Id m_origin; /*!< node Id for origin*/
			bool m_hasorigin;
			Id m_firstd; /*!< node Id for first axis*/
			bool m_hasfirstd;
			Id m_planedef;  /*!< node Id for plane*/
			bool m_hasplanedef;
			AxisName m_firstdirection; /*!< first axis description*/
			bool m_hasfirstdirection;
			AxisName m_plane;  /*!< second axis to constitue plane*/
			bool m_hasplane;

			bool m_normalizedFirstdirection, m_normalizedSeconddirection;
			double m_normFirstdirection, m_normSeconddirection;

			void parseXml(tinyxml2::XMLElement* element);

			explicit FEFrame(const Id& nid);



		};


        class Frames : public MeshComponentContainer< FEFrame> {

		public:
            Frames() : MeshComponentContainer< FEFrame>("FRAMES")  {}
		};

	}
}

#endif
