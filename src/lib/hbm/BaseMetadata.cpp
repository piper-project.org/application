/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "BaseMetadata.h"

#include "FEModel.h"

#include <vector>

#include "xmlTools.h"

namespace piper {
    namespace hbm {

    BaseMetadata::BaseMetadata() 
    {
    }

    BaseMetadata::BaseMetadata(std::string const& name) : m_name(name)
    {
    }

    BaseMetadata::~BaseMetadata()
    {
    }

    std::string const& BaseMetadata::name() const
    {
        return m_name;
    }


    void BaseMetadata::setName( const std::string& name) {
        m_name = name;
    }

    void BaseMetadata::parseXml(tinyxml2::XMLElement* element)
    {
        m_name = element->FirstChildElement("name")->GetText();
    }

    void BaseMetadata::serializeXml(tinyxml2::XMLElement* element) const
    {
        tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
        xmlName->SetText(name().c_str());
        element->InsertEndChild(xmlName);
    }
    void BaseMetadata::serializePmr(tinyxml2::XMLElement* element) const
    {
        element->SetAttribute("name", m_name.c_str());
    }

    void BaseMetadataGroup::parseXml(tinyxml2::XMLElement* element)
    {
        BaseMetadata::parseXml(element);

	    if (has_groupNode())
		    parseValueCont(this->groupNode, element->FirstChildElement("groupNode"));
	    if (has_groupElement1D())
		    parseValueCont(this->groupElement1D, element->FirstChildElement("groupElement1D"));
	    if (has_groupElement2D())
		    parseValueCont(this->groupElement2D, element->FirstChildElement("groupElement2D"));
	    if (has_groupElement3D())
		    parseValueCont(this->groupElement3D, element->FirstChildElement("groupElement3D"));
	    if (has_groupGroup())
		    parseValueCont(this->groupGroup, element->FirstChildElement("groupGroup"));

    }

    void BaseMetadata::serializePmr_FEids(tinyxml2::XMLElement* element, std::map<std::string, std::vector<IdKey>> const& mapids) {
        for (auto const& curid : mapids) {
            tinyxml2::XMLElement* xmlKw = element->GetDocument()->NewElement("keyword");
            xmlKw->SetAttribute("kw", curid.first.c_str());
            std::vector<std::string> vnames;
            VId vid;
            for (auto const& idkey : curid.second) {
                if (idkey.id != -1 && idkey.idstr.size() == 0) vid.push_back(idkey.id);
                else if (idkey.id == -1 && idkey.idstr.size() != 0) vnames.push_back(idkey.idstr);
            }
            if (vid.size() > 0) {
                tinyxml2::XMLElement* xmlId = element->GetDocument()->NewElement("id");
                serializeValueCont(xmlId, vid);
                xmlKw->InsertEndChild(xmlId);
            }
            else if (vnames.size() > 0) {
                tinyxml2::XMLElement* xmlname = element->GetDocument()->NewElement("name");
                serializeValueCont(xmlname, vnames);
                xmlKw->InsertEndChild(xmlname);
            }
            element->InsertEndChild(xmlKw);
        }
    }


    void BaseMetadataGroup::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
        BaseMetadata::serializePmr(element);

        std::map<std::string, std::vector<IdKey>> fe_ids;
        VId vid_gg;
        if (has_groupGroup() && !this->groupGroup.empty()) {
            std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupGroups>();
            for (Id const& id : this->groupGroup) {
                VId const& vid_cur = fem.getGroupGroups(id).get();
                vid_gg.insert(vid_gg.end(), vid_cur.begin(), vid_cur.end());
                fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
            }
            serializePmr_FEids(element, fe_ids);
            std::sort(vid_gg.begin(), vid_gg.end());
            vid_gg.erase(std::unique(vid_gg.begin(), vid_gg.end()), vid_gg.end());
            fe_ids.clear();
        }
        if (has_groupNode() && !this->groupNode.empty()) {
            VId vid = groupNode;
            if (vid_gg.size() > 0) {//remove id from alreday written groups group std::back_inserter(tmp.begin())
                VId tmp;
                VId::iterator it;
                std::set_difference(vid.begin(), vid.end(), vid_gg.begin(), vid_gg.end(), std::back_inserter(tmp));
                vid = tmp;
            }
            if (vid.size() > 0) { //still has groups to serialize
                VCoord coord3D;
                std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupNode>();
                for (Id const& id : vid) {
                    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(id);
                    if (it != mpIds.end())
                        fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
                    else {
                        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Node>();
                        //get group content
                        VId vidcontent = fem.get<GroupNode>(id)->get();
                        for (Id const& idcontent : vidcontent) {
                            it = mpIdsContent.find(idcontent);
                            if (it == mpIdsContent.end()) // this node is defined as coord in metadata
                                coord3D.push_back(fem.getNode(idcontent).get());
                            else
                                fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
                        }
                    }
                }
                if (coord3D.size() > 0) {
                    tinyxml2::XMLElement* xmlCoord = element->GetDocument()->NewElement("coord");
                    serializeVecCoord(xmlCoord, coord3D);
                    element->InsertEndChild(xmlCoord);
                }
                if (fe_ids.size() > 0)
                    serializePmr_FEids(element, fe_ids);
                fe_ids.clear();
            }
        }
        if (has_groupElement1D() && !this->groupElement1D.empty()) {
            VId vid = groupElement1D;
            if (vid_gg.size() > 0) {//remove id from alreday written groups group std::back_inserter(tmp.begin())
                VId tmp;
                VId::iterator it;
                std::set_difference(vid.begin(), vid.end(), vid_gg.begin(), vid_gg.end(), std::back_inserter(tmp));
                vid = tmp;
            }
            if (vid.size() > 0) { //still has groups to serialize
                std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupElements1D>();
                for (Id const& id : vid) {
                    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(id);
                    if (it != mpIds.end())
                        fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
                    else {
                        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Element1D>();
                        //get group content
                        VId vidcontent = fem.get<GroupElements1D>(id)->get();
                        for (Id const& idcontent : vidcontent) {
                            fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
                        }
                    }
                }
                serializePmr_FEids(element, fe_ids);
                fe_ids.clear();
            }
        }
        if (has_groupElement2D() && !this->groupElement2D.empty()) {
            VId vid = groupElement2D;
            if (vid_gg.size() > 0) {//remove id from alreday written groups group std::back_inserter(tmp.begin())
                VId tmp;
                VId::iterator it;
                std::set_difference(vid.begin(), vid.end(), vid_gg.begin(), vid_gg.end(), std::back_inserter(tmp));
                vid = tmp;
            }
            if (vid.size() > 0) { //still has groups to serialize
                std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupElements2D>();
                for (Id const& id : vid) {
                    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(id);
                    if (it != mpIds.end())
                        fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
                    else {
                        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Element2D>();
                        //get group content
                        VId vidcontent = fem.get<GroupElements2D>(id)->get();
                        for (Id const& idcontent : vidcontent) {
                            fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
                        }
                    }
                }
                serializePmr_FEids(element, fe_ids);
                fe_ids.clear();
            }
        }
        if (has_groupElement3D() && !this->groupElement3D.empty()) {
            VId vid = groupElement3D;
            if (vid_gg.size() > 0) {//remove id from alreday written groups group std::back_inserter(tmp.begin())
                VId tmp;
                VId::iterator it;
                std::set_difference(vid.begin(), vid.end(), vid_gg.begin(), vid_gg.end(), std::back_inserter(tmp));
                vid = tmp;
            }
            if (vid.size() > 0) { //still has groups to serialize
                std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<GroupElements3D>();
                for (Id const& id : vid) {
                    std::map<hbm::Id, std::pair<std::string, IdKey>>::const_iterator it = mpIds.find(id);
                    if (it != mpIds.end())
                        fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
                    else {
                        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIdsContent = fem.computePiperIdToIdKeyMap<Element3D>();
                        //get group content
                        VId vidcontent = fem.get<GroupElements3D>(id)->get();
                        for (Id const& idcontent : vidcontent) {
                            fe_ids[mpIdsContent.at(idcontent).first].push_back(mpIdsContent.at(idcontent).second);
                        }
                    }
                }
                serializePmr_FEids(element, fe_ids);
                fe_ids.clear();
            }
        }
    }


    void BaseMetadataGroup::serializeXml(tinyxml2::XMLElement* element) const
    {
        BaseMetadata::serializeXml(element);

        tinyxml2::XMLElement* xmlIds;
        if (has_groupNode() && !this->groupNode.empty()) {
            xmlIds = element->GetDocument()->NewElement("groupNode");
            serializeValueCont(xmlIds, this->groupNode);
            element->InsertEndChild(xmlIds);
        }
        if (has_groupElement1D() && !this->groupElement1D.empty()) {
            xmlIds = element->GetDocument()->NewElement("groupElement1D");
            serializeValueCont(xmlIds, this->groupElement1D);
            element->InsertEndChild(xmlIds);
        }
        if (has_groupElement2D() && !this->groupElement2D.empty()) {
            xmlIds = element->GetDocument()->NewElement("groupElement2D");
            serializeValueCont(xmlIds, this->groupElement2D);
            element->InsertEndChild(xmlIds);
        }
        if (has_groupElement3D() && !this->groupElement3D.empty()) {
            xmlIds = element->GetDocument()->NewElement("groupElement3D");
            serializeValueCont(xmlIds, this->groupElement3D);
            element->InsertEndChild(xmlIds);
        }
        if (has_groupGroup() && !this->groupGroup.empty()) {
            xmlIds = element->GetDocument()->NewElement("groupGroup");
            serializeValueCont(xmlIds, this->groupGroup);
            element->InsertEndChild(xmlIds);
        }
    }

    BaseMetadataGroup::BaseMetadataGroup() : BaseMetadata()
    {
    }

    BaseMetadataGroup::BaseMetadataGroup(std::string const& name): BaseMetadata(name)
    {
    }

    BaseMetadataGroup::~BaseMetadataGroup()
    {
    }

    }
}


