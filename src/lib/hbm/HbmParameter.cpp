/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "HbmParameter.h"

#include "FEModel.h"
#include "xmlTools.h"

namespace piper {
	namespace hbm {

        HbmParameter::HbmParameter() : BaseMetadata() {}

        HbmParameter::HbmParameter(std::string const& name)
            : BaseMetadata(name)
		{}

        HbmParameter::HbmParameter(tinyxml2::XMLElement* element)
		{
			parseXml(element);
		}

		void HbmParameter::setSource(const std::string& source)
		{
			m_source = source;
		}

		std::string const& HbmParameter::source() const
		{
			return m_source;
		}

		void HbmParameter::serializeXml(tinyxml2::XMLElement* element) const
		{
            BaseMetadata::serializeXml(element);
			tinyxml2::XMLElement* xmlSource = element->GetDocument()->NewElement("source");
			xmlSource->SetText(source().c_str());
			element->InsertEndChild(xmlSource);
            tinyxml2::XMLElement* xmlIds = element->GetDocument()->NewElement("feparameter");
			if (!this->FEparameters.empty()) {
                serializeValueCont(xmlIds, this->FEparameters);
				element->InsertEndChild(xmlIds);
			}
		}

        void HbmParameter::parseXml(tinyxml2::XMLElement* element)
		{
            BaseMetadata::parseXml(element);

			m_source = element->FirstChildElement("source")->GetText();
            std::vector<double> tmp;
            parseValueCont(this->FEparameters, element->FirstChildElement("feparameter"));
        }

        void HbmParameter::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
            BaseMetadata::serializePmr(element);
            element->SetAttribute("source", m_source.c_str());
            std::map<std::string, std::vector<IdKey>> fe_ids;
            std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<FEModelParameter>();
            for (Id const& id : FEparameters) {
                fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
            }
            for (auto& cur : fe_ids) {
                VId vtmp = fem.m_registorModelparameter.getIdfromIdFormat(cur.first);
                if (vtmp.size() == cur.second.size())
                    cur.second.clear();
            }
            serializePmr_FEids(element, fe_ids);
        }

	}
}
