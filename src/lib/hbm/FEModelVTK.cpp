/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

#include "FEModel.h"
#include "VtkSelectionTools.h"
#include "anatomyDB/query.h"

#include <vtkFloatArray.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>

#include <vtkMeshQuality.h>

#include <boost/filesystem.hpp>
#include <boost/container/list.hpp>


namespace piper {
    namespace hbm {
        FEModelVTK::FEModelVTK()
        {
            m_mesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
            m_mesh->Allocate();
            vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
            m_mesh->SetPoints(points);
            m_2DElementMesh = nullptr;
            m_mesh2Drep = nullptr;
        }

        FEModelVTK::FEModelVTK(FEModel& femodel)
        {
            m_mesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
            m_mesh->Allocate();
            vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
            m_mesh->SetPoints(points);
            setMesh(femodel);
        }

        FEModelVTK::~FEModelVTK()
        {
        }

        void FEModelVTK::clear() {
            m_mesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
            m_mesh->Allocate();
            vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
            m_mesh->SetPoints(points);

            m_entitymesh.clear();
            m_enventitymesh.clear();			
            m_landmarkMap.clear();
            m_landmarkPoints.clear();
            m_landmarkMap.clear();
            m_landmarkIndexMap.clear();
			m_genericmetadatamesh.clear();

            m_2DElementMesh = nullptr;
            m_mesh2Drep = nullptr;
            m_skin = nullptr;
        }
        
        void FEModelVTK::read(std::string const& filename) {
            vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
            reader->SetFileName(filename.c_str());
            reader->Update();
            m_mesh->DeepCopy(reader->GetOutput());
            // make sure that points array is initialized
            if (!m_mesh->GetPoints())
            {
                vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                m_mesh->SetPoints(points);
            }
            // convert eid and nid arrays to 64-bits 
            // this is for conversion of old projects that are still used in automated tests and were created using 32 bit indexing
            // and also for converting vtkLongLongArrays to vtkIdTypeArrays (see note below)
            for (int i = 0; i < m_mesh->GetPointData()->GetNumberOfArrays(); i++) // go through point arrays
            {
                vtkDataArray *curArray = m_mesh->GetPointData()->GetArray(i);
                if (((std::string(curArray->GetName()) == "nid") && (curArray->GetDataType() != VTK_ID_TYPE))
#if VTK_ID_TYPE_IMPL == VTK_LONG_LONG
                    // convert all longlong arrays to vtkIdTypeArrays as well - the XMLReaded uses longlong, but most other vtk classes use vtkIdType
                    // and although those two types are the same, the array types are not easily casted to the other type
                    // if vtkIdType != long long...the file was created using other version of piper (other version of VTK)...all we can do is hope it will work
                    || (curArray->GetDataType() == VTK_LONG_LONG)
#endif
                    )
                {
                    vtkSmartPointer<vtkIdTypeArray> ar = vtkSmartPointer<vtkIdTypeArray>::New();
                    ar->SetName(curArray->GetName());
                    ar->DeepCopy(curArray);
                    m_mesh->GetPointData()->AddArray(ar);
                }    
            }
            for (int i = 0; i < m_mesh->GetCellData()->GetNumberOfArrays(); i++) // go through cell arrays
            {
                vtkDataArray *curArray = m_mesh->GetCellData()->GetArray(i);
                if (((std::string(curArray->GetName()) == "eid") && (curArray->GetDataType() != VTK_ID_TYPE))
#if VTK_ID_TYPE_IMPL == VTK_LONG_LONG
                    || (curArray->GetDataType() == VTK_LONG_LONG)
#endif
                    )
                {
                    vtkSmartPointer<vtkIdTypeArray> ar = vtkSmartPointer<vtkIdTypeArray>::New();
                    ar->SetName(curArray->GetName());
                    ar->DeepCopy(curArray);
                    m_mesh->GetCellData()->AddArray(ar);
                }

            }
            m_2DElementMesh = nullptr;
            m_mesh2Drep = nullptr;
            m_skin = nullptr;
        }

        void FEModelVTK::write(std::string const& filename) const
        {
            if (m_mesh)
            {
                vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
                // do not write bit arrays - vtkXMLWriter does not handle them for some reason
                boost::container::list<vtkSmartPointer<vtkDataArray>> pointBitArrays;
                for (int i = 0; i < m_mesh->GetPointData()->GetNumberOfArrays(); i++)
                {
                    vtkSmartPointer<vtkDataArray> a = m_mesh->GetPointData()->GetArray(i);
                    if (a->GetDataType() == VTK_BIT) // if it is a bit array, store it and remove it from the mesh
                    {
                        pointBitArrays.push_back(a);
                        m_mesh->GetPointData()->RemoveArray(i);
                        i--; // decrease i to account for the removed array
                    }
                }
                boost::container::list<vtkSmartPointer<vtkDataArray>> cellBitArrays;
                for (int i = 0; i < m_mesh->GetCellData()->GetNumberOfArrays(); i++)
                {
                    vtkSmartPointer<vtkDataArray> a = m_mesh->GetCellData()->GetArray(i);
                    if (a->GetDataType() == VTK_BIT) // if it is a bit array, store it and remove it from the mesh
                    {
                        cellBitArrays.push_back(a);
                        m_mesh->GetCellData()->RemoveArray(i);
                        i--; // decrease i to account for the removed array
                    }
                }
                writer->SetFileName(filename.c_str());
                writer->SetInputData(m_mesh);
                writer->Write();
                //restore the removed bit arrays
                for (auto it = pointBitArrays.begin(); it != pointBitArrays.end(); it++)
                    m_mesh->GetPointData()->AddArray(*it);
                for (auto it = cellBitArrays.begin(); it != cellBitArrays.end(); it++)
                    m_mesh->GetCellData()->AddArray(*it);
            }
        }

        vtkSmartPointer<vtkPolyData> FEModelVTK::get2DElements(bool rebuild) 
        {
            if (m_2DElementMesh == nullptr || rebuild) { // if it wasnt created yet, create it
                m_2DElementMesh = vtkSmartPointer<vtkPolyData>::New();
                m_2DElementMesh->Allocate();
                // copy pointers to vertex data (the arrays are shared for both polydata and unstrgrid, not copied)
                m_2DElementMesh->SetPoints(m_mesh->GetPoints());
                for (int i = 0; i < m_mesh->GetPointData()->GetNumberOfArrays(); i++)
                    m_2DElementMesh->GetPointData()->AddArray(m_mesh->GetPointData()->GetArray(i));

                // copy connectivity - extract only 2D elements from the cell data
                vtkIdType nCells = m_mesh->GetNumberOfCells();
                for (vtkIdType i = 0; i < nCells; i++)
                {
                    vtkCell *curCell = m_mesh->GetCell(i);
                    if (curCell->GetCellType() == VTK_TRIANGLE || curCell->GetCellType() == VTK_QUAD)
                        m_2DElementMesh->InsertNextCell(curCell->GetCellType(), curCell->GetPointIds());
                }
            }

            return m_2DElementMesh;
        }

        vtkSmartPointer<vtkPolyData> FEModelVTK::getSkin(bool rebuild, hbm::Metadata::EntityCont const& entities, FEModel& fem)
        {
            if (rebuild || m_skin == nullptr)
            {
                // get list of skin entities
                std::vector<std::string> listskins;
                if (entities.size() > 0) {
                    for (auto const& cur : entities) {
                        if (anatomydb::isSkin(cur.first))
                            listskins.push_back(cur.first);
                    }
                }

                // get points that will be used
                hbm::VId skinnodeids;
                vtkSmartPointer<vtkIdList> cellPoints = vtkSmartPointer<vtkIdList>::New();
                for (auto const& cur : listskins)
                {
                    vtkSmartPointer<vtkUnstructuredGrid> entity = m_entitymesh[cur];
                    vtkIdType nCells = entity->GetNumberOfCells();
                    for (vtkIdType i = 0; i < nCells; i++) // insert the skin entity's cell into the concatenated mesh along with the orig ID
                    {
                        vtkIdList *curCellPoints = entity->GetCell(i)->GetPointIds();
                        vtkIdType nCellPoints = curCellPoints->GetNumberOfIds();
                        for (vtkIdType j = 0; j < nCellPoints; j++) // write down the orig node IDs used for the cell
                            skinnodeids.push_back(curCellPoints->GetId(j));
                    }
                }
                // a point can be used in multiple entites -> get unique ids of the points
                hbm::SId s(skinnodeids.begin(), skinnodeids.end());
                skinnodeids.assign(s.begin(), s.end());
                
                vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                points->Allocate(skinnodeids.size());
                points->SetNumberOfPoints(skinnodeids.size());
                vtkSmartPointer<vtkIdTypeArray> skinOrigPoints = vtkSmartPointer<vtkIdTypeArray>::New();
                skinOrigPoints->SetNumberOfComponents(1);
                skinOrigPoints->SetName("nid");
                skinOrigPoints->SetNumberOfValues(points->GetNumberOfPoints());
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
                vtkIdType i = 0;
                std::map<vtkIdType, vtkIdType> origIdMap;
                for (Id id : s)
                {
                    Coord &nodeCoord = fem.get<hbm::Node>(nid->GetValue(id))->get();
                    points->SetPoint(i, nodeCoord[0], nodeCoord[1], nodeCoord[2]);
                    skinOrigPoints->SetValue(i, nid->GetValue(id));
                    origIdMap[id] = i; // store the new index of the point based on its original index (which is used to define the tempCells)
                    i++;
                }

                // all cell points are defined by point IDs to the original mesh - have to re-index them for the new, squeezed points
                m_skin = vtkSmartPointer<vtkPolyData>::New();
                m_skin->Allocate();
                // get elements
                vtkSmartPointer<vtkIdTypeArray> skinOrigCells = vtkSmartPointer<vtkIdTypeArray>::New();
                skinOrigCells->SetName(ORIGINAL_CELL_IDS);
                skinOrigCells->SetNumberOfComponents(1);
                vtkIdType mappedPoints[10]; // allocated a big enough storage for element point IDs
                for (auto const& cur : listskins)
                {
                    vtkSmartPointer<vtkUnstructuredGrid> entity = m_entitymesh[cur];
                    vtkSmartPointer<vtkIdTypeArray> origCellIds = vtkIdTypeArray::SafeDownCast(entity->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                    vtkIdType nCells = entity->GetNumberOfCells();
                    for (vtkIdType i = 0; i < nCells; i++) // insert the skin entity's cell into the concatenated mesh along with the orig ID
                    {
                        vtkCell *curCell = entity->GetCell(i);
                        vtkIdList* cellPoints = curCell->GetPointIds();
                        vtkIdType nCellPoints = cellPoints->GetNumberOfIds();
                        for (vtkIdType j = 0; j < nCellPoints; j++)
                            mappedPoints[j] = origIdMap[cellPoints->GetId(j)]; // map the new point ID based on the original ID the point has in m_mesh
                        m_skin->InsertNextCell(curCell->GetCellType(), nCellPoints, mappedPoints);
                        skinOrigCells->InsertNextValue(origCellIds->GetValue(i));
                    }
                }

                m_skin->GetCellData()->AddArray(skinOrigCells);
                m_skin->SetPoints(points);
                m_skin->GetPointData()->AddArray(skinOrigPoints);
            }
            return m_skin;
        }

        vtkSmartPointer<vtkPolyData> FEModelVTK::getEntity2DElements(std::string entityName) 
        {
            if (m_entitymesh.find(entityName) != m_entitymesh.end())
            {
                vtkSmartPointer<vtkPolyData> ret = vtkSmartPointer<vtkPolyData>::New();
                vtkSmartPointer<vtkUnstructuredGrid> entityMesh = m_entitymesh[entityName];
                ret->Allocate();
                ret->SetPoints(m_mesh->GetPoints());
                // copy pointers to vertex data (the arrays are shared for both polydata and unstrgrid, not copied)
                for (int i = 0; i < entityMesh->GetPointData()->GetNumberOfArrays(); i++)
                    ret->GetPointData()->AddArray(entityMesh->GetPointData()->GetArray(i));

                vtkCellData* cellData = entityMesh->GetCellData();

                vtkSmartPointer<vtkIdTypeArray> origCells = vtkSmartPointer<vtkIdTypeArray>::New();
                origCells->Initialize();
                origCells->SetName(ORIGINAL_CELL_IDS);
                ret->GetCellData()->AddArray(origCells);
                // copy scalar data
                for (int i = 0; i < cellData->GetNumberOfArrays(); i++)
                {
                    vtkDataArray *curArray = cellData->GetArray(i);
                    if (std::string(curArray->GetName()) != ORIGINAL_CELL_IDS)
                    {
                        vtkDataArray *temp = vtkDataArray::CreateDataArray(curArray->GetDataType());
                        vtkSmartPointer<vtkDataArray> targetData = vtkSmartPointer<vtkDataArray>::NewInstance(temp);
                        temp->Delete();

                        targetData->Initialize(); // reset to empty state
                        targetData->SetName(curArray->GetName());
                        targetData->SetNumberOfComponents(targetData->GetNumberOfComponents());
                        ret->GetCellData()->AddArray(targetData);
                    }
                }

                // copy connectivity - extract only 2D elements from the cell data
                vtkIdType nCells = entityMesh->GetNumberOfCells();
                for (vtkIdType i = 0; i < nCells; i++)
                {
                    vtkCell *curCell = entityMesh->GetCell(i);
                    if (curCell->GetCellType() == VTK_TRIANGLE || curCell->GetCellType() == VTK_QUAD)
                    {
                        ret->InsertNextCell(curCell->GetCellType(), curCell->GetPointIds());
                        origCells->InsertNextValue(i);
                        for (int j = 0; j < cellData->GetNumberOfArrays(); j++)
                        {
                            std::string curName = cellData->GetArray(j)->GetName();
                            if (curName != ORIGINAL_CELL_IDS) 
                                ret->GetCellData()->GetArray(curName.c_str())->InsertNextTuple(cellData->GetArray(j)->GetTuple(i));
                        }
                    }
                }
                return ret;
            }
            return nullptr;
        }

        vtkPolyData *FEModelVTK::getPolyDataRep(bool rebuild) {
            if (m_mesh2Drep == nullptr || rebuild) { // if it wasnt created yet, create it
                m_mesh2Drep = vtkSmartPointer<vtkPolyData>::New();
                m_mesh2Drep->Allocate();
                // copy pointers to vertex data (the arrays are shared for both polydata and unstrgrid, not copied)
                m_mesh2Drep->SetPoints(m_mesh->GetPoints());
                for (int i = 0; i < m_mesh->GetPointData()->GetNumberOfArrays(); i++)
                    m_mesh2Drep->GetPointData()->AddArray(m_mesh->GetPointData()->GetArray(i));

                // an array to map the cells of the new mesh to cells of the original mesh
                vtkSmartPointer<vtkIdTypeArray> cell_map = vtkSmartPointer<vtkIdTypeArray>::New();
                cell_map->SetNumberOfComponents(1);
                cell_map->Initialize();

                // copy connectivity - extract only 2D elements from the cell data
                vtkIdType nCells = m_mesh->GetNumberOfCells();
                for (vtkIdType i = 0; i < nCells; i++)
                {
                    vtkCell *curCell = m_mesh->GetCell(i);
                    auto cellType = curCell->GetCellType();
                    if (cellType == VTK_TRIANGLE || cellType == VTK_QUAD
                        || cellType == VTK_VERTEX || cellType == VTK_POLY_VERTEX
                        || cellType == VTK_LINE || cellType == VTK_POLY_LINE
                        || cellType == VTK_POLYGON || cellType == VTK_TRIANGLE_STRIP)
                    {
                        m_mesh2Drep->InsertNextCell(cellType, curCell->GetPointIds());
                        cell_map->InsertNextValue(i); // the added cell belongs to cell "i"
                    }
                    else // else it is a 3D cell - break it into individual faces
                    {
                        int nFaces = curCell->GetNumberOfFaces();
                        for (int faceIndex = 0; faceIndex < nFaces; faceIndex++)
                        {
                            vtkCell *face = curCell->GetFace(faceIndex);
                            m_mesh2Drep->InsertNextCell(face->GetCellType(), face->GetPointIds());
                            cell_map->InsertNextValue(i); // the added cell belongs to cell "i"
                        }
                    }
                }

                // copy cell data
                for (int i = 0; i < m_mesh->GetCellData()->GetNumberOfArrays(); i++)
                {
                    vtkSmartPointer<vtkDataArray> curArray = m_mesh->GetCellData()->GetArray(i);
                    vtkDataArray *temp = vtkDataArray::CreateDataArray(curArray->GetDataType());
                    vtkSmartPointer<vtkDataArray> targetData = vtkSmartPointer<vtkDataArray>::NewInstance(temp);
                    temp->Delete();
                    targetData->SetNumberOfComponents(1);
                    targetData->SetNumberOfTuples(cell_map->GetNumberOfTuples());

                    vtkIdType nTuples = cell_map->GetNumberOfTuples();
                    for (vtkIdType j = 0; j < nTuples; j++)
                        targetData->SetTuple(j, curArray->GetTuple(cell_map->GetValue(j)));

                    // use the same name
                    targetData->SetName(curArray->GetName());
                    m_mesh2Drep->GetCellData()->AddArray(targetData);
                }
            }

            return m_mesh2Drep;
        }

        hbm::VId FEModelVTK::getSelectedElements(int chosenEleDimension) const
        {
            hbm::VId selEle;
            vtkSmartPointer<vtkBitArray> sel_ele = vtkSelectionTools::ObtainSelectionArrayCells(m_mesh, false);
            vtkSmartPointer<vtkIdTypeArray> eid = vtkIdTypeArray::SafeDownCast(m_mesh->GetCellData()->GetArray("eid"));
            vtkIdType nTuples = sel_ele->GetNumberOfTuples();
            for (vtkIdType i = 0; i < nTuples; i++)
            {
                if (m_mesh->GetCell(i)->GetCellDimension() == chosenEleDimension && sel_ele->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    selEle.push_back(eid->GetValue(i));
            }
            for (auto entity : m_entitymesh)
            {
                vtkSmartPointer<vtkBitArray> sel_eleEnt = vtkSelectionTools::ObtainSelectionArrayCells(entity.second, false);
                vtkSmartPointer<vtkIdTypeArray> origCells = vtkIdTypeArray::SafeDownCast(entity.second->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                vtkIdType nEntTuples = sel_eleEnt->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nEntTuples; i++)
                {
                    if (entity.second->GetCell(i)->GetCellDimension() == chosenEleDimension && sel_eleEnt->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        selEle.push_back(eid->GetValue(origCells->GetValue(i)));
                }
            }
            VId::iterator it = std::unique(selEle.begin(), selEle.end());
            selEle.resize(std::distance(selEle.begin(), it));
            return selEle;
        }

        std::map<std::string, hbm::VId> FEModelVTK::getSelectedElementsByEntities(int chosenEleDimension) const
        {
            std::map<std::string, hbm::VId> selEleByEnt;
            hbm::VId selEleFlesh;
            hbm::VId allSelEntiEle;
            vtkSmartPointer<vtkBitArray> sel_ele = vtkSelectionTools::ObtainSelectionArrayCells(m_mesh, false);
            vtkSmartPointer<vtkIdTypeArray> eid = vtkIdTypeArray::SafeDownCast(m_mesh->GetCellData()->GetArray("eid"));
            vtkIdType nCells = sel_ele->GetNumberOfTuples();
            for (vtkIdType i = 0; i < nCells; i++)
            {
                if (m_mesh->GetCell(i)->GetCellDimension() == chosenEleDimension && sel_ele->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    selEleFlesh.push_back(eid->GetValue(i));
            }
            for (auto entity : m_entitymesh)
            {
                vtkSmartPointer<vtkBitArray> sel_eleEnt = vtkSelectionTools::ObtainSelectionArrayCells(entity.second, false);
                vtkSmartPointer<vtkIdTypeArray> origCells = vtkIdTypeArray::SafeDownCast(entity.second->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                vtkIdType nEntTuples = sel_eleEnt->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nEntTuples; i++)
                {
                    if (entity.second->GetCell(i)->GetCellDimension() == chosenEleDimension && sel_eleEnt->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    {
                        int eleID = eid->GetValue(origCells->GetValue(i));
                        selEleByEnt[entity.first].push_back(eleID);
                        allSelEntiEle.push_back(eleID); // store all in one array to later use for set difference with the full mesh elements
                    }
                }
            }
            hbm::VId selEleFleshFinal;
            // removed elements that are part of some entities from the list of elements selected on the full mesh
            hbm::VId::iterator end = std::set_difference(selEleFlesh.begin(), selEleFlesh.end(), allSelEntiEle.begin(), allSelEntiEle.end(), selEleFleshFinal.begin());
            selEleFleshFinal.resize(std::distance(selEleFleshFinal.begin(), end));
            // register elements that are not part of any entity as "undefinedEntity"
            selEleByEnt["undefinedEntity"] = selEleFleshFinal;
            return selEleByEnt;
        }

        hbm::VId FEModelVTK::getSelectedNodes() const
        {
            hbm::VId selNodes;
            vtkSmartPointer<vtkBitArray> sel_ele = vtkSelectionTools::ObtainSelectionArrayPoints(m_mesh, false);
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
            vtkIdType nElems = sel_ele->GetNumberOfTuples();
            for (vtkIdType i = 0; i < nElems; i++)
            {
                if (sel_ele->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    selNodes.push_back(nid->GetValue(i));
            }
            return selNodes;
        }

        void FEModelVTK::updateFEmodelNodes(FEModel* femodel) {
            if (m_mesh->GetPointData()->HasArray("nid"))
            {
                bool isMeshDefIndex = femodel->getMeshDefType() == MeshDefType::INDEX;
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
                double coord[3];
                vtkIdType nNodes = nid->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nNodes; i++) // for each node in the vtk representation
                {
                    Id feNodeId = static_cast<Id>(isMeshDefIndex ? i : nid->GetValue(i));
                    m_mesh->GetPoint(i, coord);
                    femodel->getNode(feNodeId).setCoord(coord[0], coord[1], coord[2]); // read the ID of the node from the nid array
                }
            }
        }

        void FEModelVTK::generateFEmodelNodes(FEModel& femodel) 
        {
            bool spy = femodel.setMeshDef_NID(); // the nodes are added based on the "nid" array which contains the FE ids, so the mesh must be in the NID mode
            int nb = m_mesh->GetNumberOfPoints();
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
            for (vtkIdType i = 0; i < nb; i++) {
                Id curid = nid->GetValue(i);
                double coord[3];
                m_mesh->GetPoint(i, coord);

                NodePtr curnode = std::make_shared<Node>(curid);
                curnode->setCoord(coord[0], coord[1], coord[2]);
                femodel.set(curnode);
            }
            if (spy)
                femodel.setMeshDef_INDEX();
        }

        void FEModelVTK::generateFEmodelElements(FEModel& femodel)
        {
            bool isMeshDefIndex = femodel.getMeshDefType() == MeshDefType::INDEX;
            vtkIdType nb = m_mesh->GetNumberOfPoints();
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
            nb = m_mesh->GetNumberOfCells();
            vtkSmartPointer<vtkIdList> pointids = vtkSmartPointer<vtkIdList>::New();
            ElemDef elemdef;
            for (vtkIdType i = 0; i < nb; i++) {
                elemdef.clear();
                Id curid = m_mesh->GetCellData()->GetArray("eid")->GetComponent(i, 0);
                int type = m_mesh->GetCellType(i);
                vtkCell *cell = m_mesh->GetCell(i);
                pointids = cell->GetPointIds();
                //cell->Delete();
                for (vtkIdType n = 0; n < pointids->GetNumberOfIds(); n++)
                    elemdef.push_back(isMeshDefIndex ? pointids->GetId(n) : nid->GetValue(pointids->GetId(n)));
                if (type == VTK_LINE) {//1D
                    Element1DPtr elem = std::make_shared<Element1D>(curid);
                    elem->setElemDef(elemdef);
                    femodel.set(elem);
                }
                else if ((type == VTK_TRIANGLE) || (type == VTK_QUAD)) {//2D
                    Element2DPtr elem = std::make_shared<Element2D>(curid);
                    elem->setElemDef(elemdef);
                    femodel.set(elem);
                }
                else { //3D
                    Element3DPtr elem = std::make_shared<Element3D>(curid);
                    elem->setElemDef(elemdef);
                    femodel.set(elem);
                }
            }
        }

        void FEModelVTK::setPoints(FEModel& femodel)
        {
            if (m_mesh)
            {
                bool isMeshDefIndex = femodel.getMeshDefType() == MeshDefType::INDEX;
                vtkSmartPointer<vtkPoints> points = m_mesh->GetPoints();
                if (!m_mesh->GetPointData()->HasArray("nid")) // if it does not already have the nid array, create it
                {
                    vtkSmartPointer<vtkIdTypeArray> nid_tmp = vtkIdTypeArray::New();
                    nid_tmp->SetName("nid");
                    m_mesh->GetPointData()->AddArray(nid_tmp);
                }
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_mesh->GetPointData()->GetArray("nid"));
                points->SetNumberOfPoints(femodel.getNbNode());
                nid->SetNumberOfValues(femodel.getNbNode());
                // insert Points coordinates & nid scalardata
                Nodes feNodes = femodel.getNodes();
                int i = 0;
                for (std::shared_ptr<Node> n : feNodes) {
                    double coord[3];
                    int feNodeId = isMeshDefIndex ? i : n->getId();
                    coord[0] = n->getCoordX();
                    coord[1] = n->getCoordY();
                    coord[2] = n->getCoordZ();
                    points->SetPoint(i, coord);
                    nid->SetValue(i, n->getId());
                    i++;
                }
                // udpate the landmark positions
                for (auto landmark : m_landmarkMap)
                {
                    Coord pos = landmark->position(femodel);
                    m_landmarksMesh->GetPoints()->SetPoint(m_landmarkIndexMap[landmark->name()], pos[0], pos[1], pos[2]);
                }
                // create the point selection array if it does not exist
                vtkSelectionTools::ObtainSelectionArrayPoints(m_mesh, false);
                m_2DElementMesh = nullptr;
                m_mesh2Drep = nullptr;
                m_skin = nullptr;
            }
        }

        void FEModelVTK::setMesh(FEModel& femodel)
        {
            // reset the mesh
            m_mesh->Allocate();
            vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
            m_mesh->SetPoints(points);

            setPoints(femodel);

            bool isMeshDefIndex = femodel.getMeshDefType() == MeshDefType::INDEX;
            
            std::vector< Id> vid = femodel.getListNid();
            std::unordered_map<Id, Id> inverseNid;
            int c = 0;
            for (VId::const_iterator it = vid.begin(); it != vid.end(); ++it)
            {
                inverseNid[*it] = c++;
            }
            vtkIdType list[8];
            VId veid1D;
            // insert elements 1D
            const Elements1D& elem1D = femodel.getElements1D();
            for (Elements1D::const_iterator ite = elem1D.begin(); ite != elem1D.end(); ++ite) {
                ElemDef &curelem = elem1D.curDef(ite)->get();
                for (auto n = 0; n < curelem.size(); ++n)
                    list[n] = isMeshDefIndex ? curelem[n] : inverseNid[curelem[n]];
                m_mesh->InsertNextCell(VTK_LINE, 2, list);
                
                veid1D.push_back((*ite)->getId());
            }

            // insert elements 2D
            VId veid2D;
            const Elements2D& elem2D = femodel.getElements2D();
            for (Elements2D::const_iterator ite = elem2D.begin(); ite != elem2D.end(); ++ite) {
                ElemDef &curelem = elem2D.curDef(ite)->get();
                for (auto n = 0; n<curelem.size(); ++n)
                    list[n] = isMeshDefIndex ? curelem[n] : inverseNid[curelem[n]];
                if ((elem2D.curDef(ite)->getType() == ELT_TRI) || (elem2D.curDef(ite)->getType() == ELT_TRI_THICK))
                    m_mesh->InsertNextCell(VTK_TRIANGLE, 3, list);
                else
                    m_mesh->InsertNextCell(VTK_QUAD, curelem.size(), list);
                veid2D.push_back((*ite)->getId());
            }

            // insert elements 3D
            VId veid3D;
            const Elements3D& elem3D = femodel.getElements3D();
            for (Elements3D::const_iterator ite = elem3D.begin(); ite != elem3D.end(); ++ite) {
                ElemDef &curelem = (*ite)->get();
                for (auto n = 0; n<curelem.size(); ++n)
                    list[n] = isMeshDefIndex ? curelem[n] : inverseNid[curelem[n]];
                auto & type = (*ite)->getType();
                if (type == ELT_HEXA)
                    m_mesh->InsertNextCell(VTK_HEXAHEDRON, curelem.size(), list);
                else if (type == ELT_PENTA)
                    m_mesh->InsertNextCell(VTK_WEDGE, curelem.size(), list);
                else if (type == ELT_TETRA)
                    m_mesh->InsertNextCell(VTK_TETRA, curelem.size(), list);
                else if (type == ELT_PYRA)
                    m_mesh->InsertNextCell(VTK_PYRAMID, curelem.size(), list);
                else continue;
                veid3D.push_back((*ite)->getId());
            }
            // insert eid as celldata
            vtkSmartPointer<vtkIdTypeArray> eid = vtkSmartPointer<vtkIdTypeArray>::New();
            eid->SetNumberOfComponents(1);
            eid->SetNumberOfValues(m_mesh->GetNumberOfCells());
            eid->SetName("eid");

            m_mapFEIdToVtkId1D.clear();
            m_mapFEIdToVtkId2D.clear();
            m_mapFEIdToVtkId3D.clear();

            vtkIdType *ptreid;
            ptreid = eid->GetPointer(0);
            vtkIdType i = 0;
            for (VId::const_iterator it = veid1D.begin(); it != veid1D.end(); ++it, i++)
            {
                m_mapFEIdToVtkId1D[*it] = i;
                *ptreid++ = *it;
            }
            for (VId::const_iterator it = veid2D.begin(); it != veid2D.end(); ++it, i++)
            {
                m_mapFEIdToVtkId2D[*it] = i;
                *ptreid++ = *it;
            }
            for (VId::const_iterator it = veid3D.begin(); it != veid3D.end(); ++it, i++)
            {
                m_mapFEIdToVtkId3D[*it] = i;
                *ptreid++ = *it;
            }
            m_mesh->GetCellData()->AddArray(eid);


            m_2DElementMesh = nullptr;
            m_mesh2Drep = nullptr;
            m_skin = nullptr;
        }

#pragma region EntityHandling
        void FEModelVTK::setMeshAsEntities(Metadata::EntityCont const& entities, FEModel& fem, bool resetNodes) {
            if (m_mesh->GetNumberOfPoints() != fem.getNbNode()) // if the main mesh is not setup, do it
                setMesh(fem);
            else if (resetNodes) // if we want to reset the nodes, do it first
                setPoints(fem);

            bool spy = fem.setMeshDef_INDEX();

            m_entitymesh.clear(); // remove references to all possible previous entites
            for (Metadata::EntityCont::const_iterator it = entities.begin(); it != entities.end(); ++it) 
                addEntityMeshInternal(it->second, fem, false); // false because we need to extract the nodes only once and it will be done on the first rune because m_points will be null
            if (spy)
                fem.setMeshDef_NID();
        }

	
		vtkSmartPointer<vtkUnstructuredGrid> FEModelVTK::getEntity(std::string name)
        {
            auto it = m_entitymesh.find(name);
            if (it == m_entitymesh.end()) return nullptr;
            return (*it).second;
        }

        void FEModelVTK::getEntityNodesIDs(std::string name, VId &entityNodes)
        {
            entityNodes.clear();
            vtkSmartPointer<vtkUnstructuredGrid> entity = m_entitymesh[name];
            vtkIdType nCells = entity->GetNumberOfCells();
            for (vtkIdType i = 0; i < nCells; i++) // for each cell of the entity
            {
                vtkIdList *cellPoints = entity->GetCell(i)->GetPointIds();
                vtkIdType nCellPoints = cellPoints->GetNumberOfIds();
                for (vtkIdType j = 0; j < nCellPoints; j++) // for each point of that cell
                    entityNodes.push_back(cellPoints->GetId(j)); // add that point to that list
            }
            // remove duplicated points from the list
            std::sort(entityNodes.begin(), entityNodes.end());
            VId::iterator it = std::unique(entityNodes.begin(), entityNodes.end());
            entityNodes.resize(std::distance(entityNodes.begin(), it));
        }


		vtkSmartPointer<vtkUnstructuredGrid> FEModelVTK::addEntityMesh(Entity entity, FEModel& m_fem, bool resetNodes) {
            bool spy = m_fem.setMeshDef_INDEX();
            addEntityMeshInternal(entity, m_fem, resetNodes);
            if (spy)
                m_fem.setMeshDef_NID();

			return m_entitymesh[entity.name()];
        }

        /// meshDef_INDEX must be turned on because otherwise node IDs of cells will not match correctly the node IDs stored as the mesh points.
		void FEModelVTK::addEntityMeshInternal(Entity entity, FEModel& m_fem, bool resetNodes)
        {
            //Get the elements for an Entity
            VId const& groupElement1D = entity.get_groupElement1D();
            VId const& groupElement2D = entity.get_groupElement2D();
            VId const& groupElement3D = entity.get_groupElement3D();

            vtkSmartPointer<vtkUnstructuredGrid> entityMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
            entityMesh->Allocate();

            vtkSmartPointer<vtkIdTypeArray> origCellIds = vtkSmartPointer<vtkIdTypeArray>::New();
            origCellIds->SetName(ORIGINAL_CELL_IDS);
            origCellIds->Initialize();
            if (m_mapFEIdToVtkId1D.empty() && m_mapFEIdToVtkId2D.empty() && m_mapFEIdToVtkId3D.empty()) // if at least one of them is not empty it means it has been set
            {
                setMesh(m_fem);
            }
            vtkIdType list[8];
            //Push 1D elements for a particular entity
            
            for (auto it = groupElement1D.begin(); it != groupElement1D.end(); ++it) {
                const VId& sid = m_fem.getGroupElements1D(*it).get();
                for (Id elID : sid) {
                    Element1D const& elem = m_fem.getElement1D(elID);
                    ElemDef const& elemDef = elem.get();
                    for (auto n = 0; n < elemDef.size(); ++n)
                        list[n] = elemDef[n];

                    entityMesh->InsertNextCell(VTK_LINE, 2, list);
                    origCellIds->InsertNextValue(m_mapFEIdToVtkId1D[elem.getId()]);
                }
            }
            

            //Push 2D elements for a particular entity
            for (auto it = groupElement2D.begin(); it != groupElement2D.end(); ++it) {
                const VId& sid = m_fem.getGroupElements2D(*it).get();
                for (Id elID : sid) {
                    Element2D const& elem = m_fem.getElement2D(elID);
                    ElemDef const& elemDef = elem.get();
                    for (auto n = 0; n < elemDef.size(); ++n)
                        list[n] = elemDef[n];

                    if ((elem.getType() == ELT_TRI) || (elem.getType() == ELT_TRI_THICK))
                        entityMesh->InsertNextCell(VTK_TRIANGLE, elemDef.size(), list);
                    else
                        entityMesh->InsertNextCell(VTK_QUAD, elemDef.size(), list);
                    origCellIds->InsertNextValue(m_mapFEIdToVtkId2D[elem.getId()]);
                }
            }
            //Push 3D elements for a particular entity
            for (auto it = groupElement3D.begin(); it != groupElement3D.end(); ++it) {
                const VId& sid = m_fem.getGroupElements3D(*it).get();
                for (Id elID :sid) {
                    Element3D const& elem = *(m_fem.get<Element3D>(elID));
                    ElemDef const& elemDef = elem.get();
                    for (auto n = 0; n < elemDef.size(); ++n)
                        list[n] = elemDef[n];

                    if (elem.getType() == ELT_HEXA)
                        entityMesh->InsertNextCell(VTK_HEXAHEDRON, elemDef.size(), list);
                    else if (elem.getType() == ELT_PENTA)
                        entityMesh->InsertNextCell(VTK_WEDGE, elemDef.size(), list);
                    else if (elem.getType() == ELT_TETRA)
                        entityMesh->InsertNextCell(VTK_TETRA, elemDef.size(), list);
                    else if (elem.getType() == ELT_PYRA)
                        entityMesh->InsertNextCell(VTK_PYRAMID, elemDef.size(), list);
                    else continue;
                    origCellIds->InsertNextValue(m_mapFEIdToVtkId3D[elem.getId()]);
                }
            }
            entityMesh->GetCellData()->AddArray(origCellIds);
            // extracts the nodes if needed
            if (resetNodes)
                setPoints(m_fem);
            // set the nodes - use the same nodes as the "parent" full mesh to save memory
            entityMesh->SetPoints(m_mesh->GetPoints());
            // also use the same point scalar arrays
            for (int i = 0; i < m_mesh->GetPointData()->GetNumberOfArrays(); i++)
                entityMesh->GetPointData()->AddArray(m_mesh->GetPointData()->GetArray(i));

            // adds the entity to the list based on its name
            m_entitymesh[entity.name()] = entityMesh;
        }
#pragma endregion

        std::vector<vtkSmartPointer<vtkUnstructuredGrid>> FEModelVTK::extractSelectedParts(bool ignoreBones, 
            hbm::Metadata::EntityCont const& entities, FEModel& fem, bool exclude1D, bool exclude2D, bool exclude3D)
        {
            vtkSmartPointer<vtkBitArray> sel_ele = vtkSelectionTools::ObtainSelectionArrayCells(m_mesh, false);            
            vtkSmartPointer<vtkBitArray> sel_ele_filtered = vtkSmartPointer<vtkBitArray>::New();
            sel_ele_filtered->DeepCopy(sel_ele);
            sel_ele_filtered->SetName(sel_ele->GetName()); // deep copy does not seem to copy array names...
            VId boneIDs1D{}, boneIDs2D{}, boneIDs3D{};
            for (auto &entity : entities)
            {
                // concatenate all IDs of elements that belong to a bony entity into a single vector
                if (anatomydb::isBone(entity.first))
                {
                    const VId& groupElement3D = entity.second.get_groupElement3D();
                    for (Id groupID : groupElement3D) {
                        const VId& sid = fem.getGroupElements3D(groupID).get();
                        boneIDs3D.insert(boneIDs3D.end(), sid.begin(), sid.end());
                    }
                    const VId& groupElement2D = entity.second.get_groupElement2D();
                    for (Id groupID : groupElement2D) {
                        const VId& sid = fem.getGroupElements2D(groupID).get();
                        boneIDs2D.insert(boneIDs2D.end(), sid.begin(), sid.end());

                    }
                    const VId& groupElement1D = entity.second.get_groupElement1D();
                    for (Id groupID : groupElement1D) {
                        const VId& sid = fem.getGroupElements1D(groupID).get();
                        boneIDs1D.insert(boneIDs1D.end(), sid.begin(), sid.end());
                    }
                }
            }
            if (ignoreBones)
            {
                // we will deselect the bone elements
                for (Id elID : boneIDs1D)
                    sel_ele_filtered->SetValue(m_mapFEIdToVtkId1D[elID], IS_NOT_PRIMITIVE_SELECTED);
                for (Id elID : boneIDs2D)
                    sel_ele_filtered->SetValue(m_mapFEIdToVtkId2D[elID], IS_NOT_PRIMITIVE_SELECTED);
                for (Id elID : boneIDs3D)
                    sel_ele_filtered->SetValue(m_mapFEIdToVtkId3D[elID], IS_NOT_PRIMITIVE_SELECTED);
            }
            if (exclude1D || exclude2D || exclude3D)
            {
                vtkIdType nCells = sel_ele->GetNumberOfTuples();
                for (vtkIdType i = 0; i < nCells; i++)
                {
                    if (sel_ele_filtered->GetValue(i) == IS_PRIMITIVE_SELECTED) // most of them will usually not be so checking this should be more efficient then not
                    {
                        VTKCellType curCellType = static_cast<VTKCellType>(m_mesh->GetCell(i)->GetCellType());
                        if ((exclude1D && is1DElType(curCellType)) || (exclude2D && is2DElType(curCellType)) || (exclude3D && is3DElType(curCellType)))
                            sel_ele_filtered->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                    }
                }
            }
            m_mesh->GetCellData()->AddArray(sel_ele_filtered); // rewrite the original selection array with the modified one
            std::vector<vtkSmartPointer<vtkUnstructuredGrid>> ret = vtkSelectionTools::ExtractSelectedParts(m_mesh);
            m_mesh->GetCellData()->AddArray(sel_ele); // return the original selection array if we were removing bones from it
            return ret;
        }

        void FEModelVTK::writeSelectedElements(std::string folderPath, hbm::Metadata::EntityCont const& entities, FEModel& fem)
        {

            auto parts = extractSelectedParts(false, entities, fem, true, true); // TODO - make element selection by element type somehow possible
            vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
            int partID = 0;
            for (auto &part : parts)
            {
                writer->SetInputData(part);
                std::stringstream path;
                path << folderPath << "/" << (partID++) << ".vtu";
                writer->SetFileName(path.str().c_str());
                writer->Write();
            }
        }

        void FEModelVTK::deselectElementsByEid(const VId& eid1D, const VId& eid2D, const VId& eid3D)
        {
            vtkSmartPointer<vtkBitArray> sel_ele = vtkSelectionTools::ObtainSelectionArrayCells(m_mesh, false);
            for (Id elID : eid3D)
            {
                sel_ele->SetValue(m_mapFEIdToVtkId3D[elID], IS_NOT_PRIMITIVE_SELECTED);
            }
            for (Id elID : eid2D)
                sel_ele->SetValue(m_mapFEIdToVtkId2D[elID], IS_NOT_PRIMITIVE_SELECTED);
            for (Id elID : eid1D)
                sel_ele->SetValue(m_mapFEIdToVtkId1D[elID], IS_NOT_PRIMITIVE_SELECTED);
        }


#pragma region LandmarkHandling
        void FEModelVTK::addLandmark(Landmark const *landmark, FEModel& m_fem)
        {
            VId const& LandmarkgroupNodes = landmark->get_groupNode();
            VId const& LandmarkNodes = landmark->node;
            Landmark::Type type = landmark->type();
            std::string LandmarkType;

            switch (type)
            {
                case Landmark::Type::POINT:
                {
                    LandmarkType = "POINT";
                    break;
                }
                case Landmark::Type::BARYCENTER:
                {
                    LandmarkType = "BARYCENTER";
                    break;
                }
                case Landmark::Type::SPHERE:
                {
                    LandmarkType = "SPHERE";
                    break;
                }
            }

            VId list;
            for (auto it1 = LandmarkgroupNodes.begin(); it1 != LandmarkgroupNodes.end(); ++it1) {
                VId currentGroupNode = m_fem.getGroupNode(*it1).get();
                for (VId::const_iterator it2 = currentGroupNode.begin(); it2 != currentGroupNode.end(); ++it2){
                    list.push_back(*it2);
                }
            }

            // adds the entity to the list based on its name
            m_landmarkPoints[landmark->name()] = list;
            // store the position of the new landmark in both the landmark mesh and the landmark map
            Coord pos = landmark->position(m_fem);
            int index = m_landmarksMesh->GetPoints()->InsertNextPoint(pos[0], pos[1], pos[2]);
            m_landmarkMap.push_back(landmark);
            // mark the index and name in the maps
            m_landmarkIndexMap[landmark->name()] = index;

            // resize the active landmark arrays if needed
            vtkSmartPointer<vtkBitArray> activeLandmarks = vtkBitArray::SafeDownCast(m_landmarksMesh->GetPointData()->GetArray(ACTIVE_LANDMARK_POINTS));
            vtkIdType landmarkMapSize = static_cast<vtkIdType>(m_landmarkMap.size());
            activeLandmarks->Resize(landmarkMapSize);
            for (vtkIdType i = activeLandmarks->GetNumberOfTuples(); i < landmarkMapSize; i++)
                activeLandmarks->SetValue(i, 0); // set the new landmark as not active
        }


        void FEModelVTK::setModelLandmarks(Metadata::LandmarkCont const &landmarks, FEModel& fem, bool resetNodes)
        {
            // if we want to reset the nodes, do it first
            if (resetNodes)
                setPoints(fem);

            bool spy = fem.setMeshDef_INDEX();
            // initialize the structures for landmarks
            m_landmarksMesh = vtkSmartPointer<vtkPolyData>::New();
            vtkSmartPointer<vtkPoints> landmarkpoints = vtkSmartPointer<vtkPoints>::New();
            landmarkpoints->Allocate(landmarks.size());
            m_landmarksMesh->SetPoints(landmarkpoints);
            // create the active landmark points array - used for visualization
            vtkSmartPointer<vtkBitArray> activeLandmarkPoints = vtkSmartPointer<vtkBitArray>::New();
            activeLandmarkPoints->SetNumberOfValues(m_mesh->GetNumberOfPoints());
            activeLandmarkPoints->SetName(ACTIVE_LANDMARK_POINTS);
            activeLandmarkPoints->FillComponent(0, 0);
            m_mesh->GetPointData()->AddArray(activeLandmarkPoints);

            // create active landmarks array
            vtkSmartPointer<vtkBitArray> activeLandmarks = vtkSmartPointer<vtkBitArray>::New();
            activeLandmarks->SetNumberOfValues(m_landmarksMesh->GetNumberOfPoints());
            activeLandmarks->SetName(ACTIVE_LANDMARK_POINTS);
            activeLandmarks->FillComponent(0, 0);
            m_landmarksMesh->GetPointData()->AddArray(activeLandmarks);

            // add all landmarks
            for (Metadata::LandmarkCont::const_iterator it = landmarks.begin(); it != landmarks.end(); ++it)
                addLandmark(&(it->second), fem);

            if (spy)
                fem.setMeshDef_NID();
        }

        hbm::Landmark const *FEModelVTK::getLandmarkFromLandmarkMesh(int pointID) {
            if (pointID >= 0 && pointID < m_landmarkMap.size())
                return m_landmarkMap[pointID];
            return nullptr;
        }

        int FEModelVTK::getLandmarkIndexByName(std::string landmarkName) {
            auto ret = m_landmarkIndexMap.find(landmarkName);
            if (ret != m_landmarkIndexMap.end())
                return ret->second;
            return -1;
        }

        VId *FEModelVTK::getLandmarkPoints(std::string landmarkName) {
            auto ret = m_landmarkPoints.find(landmarkName);
            if (ret != m_landmarkPoints.end())
                return &(ret->second);
            return nullptr;
        }

        boost::container::vector<std::string> FEModelVTK::getLandmarkAssociatedWithPoint(int pointID)
        {
            boost::container::vector<std::string> ret;
            for (auto & landmark : m_landmarkPoints) // for each landmark
            {
                for (auto landPoint : landmark.second) // for each point that defines the landmark
                {
                    if (landPoint == pointID) // if at least one of them is the specified point
                    {
                        ret.push_back(landmark.first); // store the name of the landmark
                        break;
                    }
                }
            }
            return ret;
        }

        void FEModelVTK::activateLandmarkPoints(std::string name, bool activate)
        {
            VId *landmark = getLandmarkPoints(name);
            // use the list as a mask 
            vtkSmartPointer<vtkBitArray> activeLandmarks = vtkBitArray::SafeDownCast(m_mesh->GetPointData()->GetArray(ACTIVE_LANDMARK_POINTS));
            vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(m_mesh, false);
            vtkSmartPointer<vtkBitArray> sel_groupHighlights = vtkSelectionTools::ObtainArrayHighlightedGroupOfPoints(m_mesh, false);
            for (auto it = landmark->begin(); it != landmark->end(); it++)
            {
                activeLandmarks->SetValue(*it, activate);
                if (!activate) // deselect on deactivating
                {
                    sel_prim->SetValue(*it, IS_NOT_PRIMITIVE_SELECTED);
                    sel_groupHighlights->SetValue(*it, IS_NOT_PRIMITIVE_SELECTED);
                }
            }
            // mark the changed arrays as modified to notify the mappers to refresh the data
            sel_groupHighlights->Modified();
            sel_prim->Modified();
            activeLandmarks->Modified();
        }

        void FEModelVTK::activateLandmarkPositions(std::string name, bool activate)
        {
            // get the list of point indices the landmark is made of
            // use the list as a mask 
            vtkSmartPointer<vtkBitArray> activeLandmarks = vtkBitArray::SafeDownCast(m_landmarksMesh->GetPointData()->GetArray(ACTIVE_LANDMARK_POINTS));
            int index = getLandmarkIndexByName(name);
            activeLandmarks->SetValue(index, activate);
            if (!activate)
            {
                vtkSmartPointer<vtkBitArray> sel_prim = vtkSelectionTools::ObtainSelectionArrayPoints(m_landmarksMesh, false);
                sel_prim->SetValue(index, IS_NOT_PRIMITIVE_SELECTED);
                sel_prim->Modified(); // mark the changed arrays as modified to notify the mappers to refresh the data
            }            
            activeLandmarks->Modified();
        }

        void FEModelVTK::activateLandmarkPoints(std::string name)
        {
            activateLandmarkPoints(name, true);
        }

        void FEModelVTK::deactivateLandmarkPoints(std::string name)
        {
            activateLandmarkPoints(name, false);
        }

        void FEModelVTK::activateLandmarkPositions(std::string name)
        {
            activateLandmarkPositions(name, true);
        }

        void FEModelVTK::deactivateMeshLandmarkPositions(std::string name)
        {
            activateLandmarkPositions(name, false);
        }

#pragma endregion // landmarks

#pragma region GenericMetadataHandling
		void FEModelVTK::setMeshAsGenericMetadatas(Metadata::GenericMetadataCont const& genericMetadatas, FEModel& fem, bool resetNodes) {
			// if we want to reset the nodes, do it first
			if (resetNodes)
				setPoints(fem);
            bool spy = fem.setMeshDef_INDEX();

			m_genericmetadatamesh.clear(); // remove references to all possible previous genericMetadatas
			for (Metadata::GenericMetadataCont::const_iterator it = genericMetadatas.begin(); it != genericMetadatas.end(); ++it)
				addGenericMetadataMeshInternal(it->second, fem, false); // false because we need to extract the nodes only once and it will be done on the first run because m_points will be null
            
			if (spy)
				fem.setMeshDef_NID();
		}


		vtkSmartPointer<vtkUnstructuredGrid> FEModelVTK::getGenericMetadata(std::string name)
		{
			auto it = m_genericmetadatamesh.find(name);
			if (it == m_genericmetadatamesh.end()) return nullptr; // if there is no such genericMetadata, return nullptr
			return (*it).second;  // else return the genericMetadata
		}

		void FEModelVTK::getGenericMetadataNodesIDs(std::string name, VId &genericMetadataNodes)
		{
			genericMetadataNodes.clear();
			vtkSmartPointer<vtkUnstructuredGrid> genericMetadata = m_genericmetadatamesh[name];
            vtkIdType nCells = genericMetadata->GetNumberOfCells();
			for (vtkIdType i = 0; i < nCells; i++) // for each cell of the genericMetadata
			{
                vtkIdList *cellPoints = genericMetadata->GetCell(i)->GetPointIds();
                vtkIdType nCellPoints = cellPoints->GetNumberOfIds();
				for (vtkIdType j = 0; j < nCellPoints; j++) // for each point of that cell
					genericMetadataNodes.push_back(cellPoints->GetId(j)); // add that point to that list
			}
			// remove duplicated points from the list
			std::sort(genericMetadataNodes.begin(), genericMetadataNodes.end());
			VId::iterator it = std::unique(genericMetadataNodes.begin(), genericMetadataNodes.end());
			genericMetadataNodes.resize(std::distance(genericMetadataNodes.begin(), it));
		}


		void FEModelVTK::addGenericMetadataMesh(GenericMetadata genericMetadata, FEModel& m_fem, bool resetNodes) {
			bool spy = m_fem.setMeshDef_INDEX();
			addGenericMetadataMeshInternal(genericMetadata, m_fem, resetNodes);
			if (spy)
				m_fem.setMeshDef_NID();
		}

		void FEModelVTK::addGenericMetadataMeshInternal(GenericMetadata genericMetadata, FEModel& m_fem, bool resetNodes)
		{
			//Get the elements for an GenericMetadata
			VId const& groupElement1D = genericMetadata.get_groupElement1D();
			VId const& groupElement2D = genericMetadata.get_groupElement2D();
			VId const& groupElement3D = genericMetadata.get_groupElement3D();

			unsigned int nb1Delementnodes = 0;
			unsigned int nb2Delementnodes = 0;
			unsigned int nb3Delementnodes = 0;

			vtkSmartPointer<vtkUnstructuredGrid> genericMetadataMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
			genericMetadataMesh->Allocate();

			vtkIdType list[8];
			//Push 1D elements for a particular genericMetadata
			for (auto groupID : groupElement1D) {
                const VId & sid = m_fem.getGroupElements1D(groupID).get();
				for (auto elemID : sid) {
					Element1D const& elem = m_fem.getElement1D(elemID);
					ElemDef const& elemDef = elem.get();
                    size_t elemSize = elemDef.size();
					for (size_t n = 0; n < elemSize; ++n)
					{
						list[n] = elemDef[n];
						nb1Delementnodes++;
					}

					genericMetadataMesh->InsertNextCell(VTK_LINE, 2, list);
				}
			}


			//Push 2D elements for a particular genericMetadata
            for (auto groupID : groupElement2D) {
                const VId & sid = m_fem.getGroupElements2D(groupID).get();
                for (auto elemID : sid) {
                    Element2D const& elem = m_fem.getElement2D(elemID);
                    ElemDef const& elemDef = elem.get();
                    size_t elemSize = elemDef.size();
                    for (size_t n = 0; n < elemSize; ++n)
                    {
                        list[n] = elemDef[n];
						nb2Delementnodes++;
					}

					if ((elem.getType() == ELT_TRI) || (elem.getType() == ELT_TRI_THICK))
						genericMetadataMesh->InsertNextCell(VTK_TRIANGLE, elemDef.size(), list);
					else
						genericMetadataMesh->InsertNextCell(VTK_QUAD, elemDef.size(), list);
				}
			}

			//Push 3D elements for a particular genericMetadata
            for (auto groupID : groupElement3D) {
                const VId & sid = m_fem.getGroupElements3D(groupID).get();
                for (auto elemID : sid) {
                    Element3D const& elem = m_fem.getElement3D(elemID);
                    ElemDef const& elemDef = elem.get();
                    size_t elemSize = elemDef.size();
                    for (size_t n = 0; n < elemSize; ++n)
                    {
						list[n] = elemDef[n];
						nb3Delementnodes++;
					}
					if (elem.getType() == ELT_HEXA)
						genericMetadataMesh->InsertNextCell(VTK_HEXAHEDRON, elemDef.size(), list);
					else if (elem.getType() == ELT_PENTA)
						genericMetadataMesh->InsertNextCell(VTK_WEDGE, elemDef.size(), list);
					else if (elem.getType() == ELT_TETRA)
						genericMetadataMesh->InsertNextCell(VTK_TETRA, elemDef.size(), list);
					else if (elem.getType() == ELT_PYRA)
						genericMetadataMesh->InsertNextCell(VTK_PYRAMID, elemDef.size(), list);
				}
			}

			// extracts the nodes if needed
			if (resetNodes)
				setPoints(m_fem);
			// set the nodes
			genericMetadataMesh->SetPoints(m_mesh->GetPoints());
			// adds the genericMetadata to the list based on its name
			m_genericmetadatamesh[genericMetadata.name()] = genericMetadataMesh;
		}
#pragma endregion //genericMetadata

        void FEModelVTK::computeMeshQuality(int indexMetricFunctionUsed, vtkSmartPointer<vtkPointSet> data)
        {
            //New instance of mesh quality object
            vtkSmartPointer<vtkMeshQuality> meshQuality = vtkSmartPointer<vtkMeshQuality>::New();
            //Read the input value and cast it as a piper::METRIC_FUNCTION
            piper::METRIC_FUNCTION index = static_cast<piper::METRIC_FUNCTION>(indexMetricFunctionUsed);

            //Check wich metric function was selected and apply the corresponding metric function
            switch (index)
            {
                case METRIC_FUNCTION::ASPECT_FROBENIUS:
                    meshQuality->SetTriangleQualityMeasureToAspectFrobenius();
                    meshQuality->SetQuadQualityMeasureToMedAspectFrobenius();
                    meshQuality->SetTetQualityMeasureToAspectFrobenius();
                    meshQuality->SetHexQualityMeasureToMedAspectFrobenius();
                    break;
                case METRIC_FUNCTION::MIN_ANGLE:
                    meshQuality->SetTriangleQualityMeasureToMinAngle();
                    meshQuality->SetQuadQualityMeasureToMinAngle();
                    break;
                case METRIC_FUNCTION::MAX_ANGLE:
                    meshQuality->SetTriangleQualityMeasureToMaxAngle();
                    meshQuality->SetQuadQualityMeasureToMaxAngle();
                    break;
                case METRIC_FUNCTION::CONDITION:
                    meshQuality->SetTriangleQualityMeasureToCondition();
                    meshQuality->SetQuadQualityMeasureToCondition();
                    meshQuality->SetTetQualityMeasureToCondition();
                    meshQuality->SetHexQualityMeasureToCondition();
                    break;
                case METRIC_FUNCTION::SCALED_JACOBIAN:
                    meshQuality->SetTriangleQualityMeasureToScaledJacobian();
                    meshQuality->SetQuadQualityMeasureToScaledJacobian();
                    meshQuality->SetTetQualityMeasureToScaledJacobian();
                    meshQuality->SetHexQualityMeasureToScaledJacobian();
                    break;
                case METRIC_FUNCTION::RELATIVE_SIZE_SQUARED:
                    meshQuality->SetTriangleQualityMeasureToRelativeSizeSquared();
                    meshQuality->SetQuadQualityMeasureToRelativeSizeSquared();
                    meshQuality->SetTetQualityMeasureToRelativeSizeSquared();
                    meshQuality->SetHexQualityMeasureToRelativeSizeSquared();
                    break;
                case METRIC_FUNCTION::SHAPE:
                    meshQuality->SetTriangleQualityMeasureToShape();
                    meshQuality->SetQuadQualityMeasureToShape();
                    meshQuality->SetTetQualityMeasureToShape();
                    meshQuality->SetHexQualityMeasureToShape();
                    break;
                case METRIC_FUNCTION::SHAPE_AND_SIZE:
                    meshQuality->SetTriangleQualityMeasureToShapeAndSize();
                    meshQuality->SetQuadQualityMeasureToShapeAndSize();
                    meshQuality->SetTetQualityMeasureToShapeAndSize();
                    meshQuality->SetHexQualityMeasureToShapeAndSize();
                    break;
                case METRIC_FUNCTION::DISTORTION:
                    meshQuality->SetTriangleQualityMeasureToDistortion();
                    meshQuality->SetQuadQualityMeasureToDistortion();
                    meshQuality->SetTetQualityMeasureToDistortion();
                    meshQuality->SetHexQualityMeasureToDistortion();
                    break;
                case METRIC_FUNCTION::AREA:
                    meshQuality->SetTriangleQualityMeasureToArea();
                    meshQuality->SetQuadQualityMeasureToArea();
                    break;
                case METRIC_FUNCTION::EDGE_RATIO:
                    meshQuality->SetTriangleQualityMeasureToEdgeRatio();
                    meshQuality->SetQuadQualityMeasureToEdgeRatio();
                    meshQuality->SetTetQualityMeasureToEdgeRatio();
                    meshQuality->SetHexQualityMeasureToEdgeRatio();
                    break;
                case METRIC_FUNCTION::ASPECT_RATIO: 
                    meshQuality->SetTriangleQualityMeasureToAspectRatio();
                    meshQuality->SetQuadQualityMeasureToAspectRatio();
                    meshQuality->SetTetQualityMeasureToAspectRatio();
                    break;
                case METRIC_FUNCTION::RADIUS_RATIO:
                    meshQuality->SetTriangleQualityMeasureToRadiusRatio();
                    meshQuality->SetQuadQualityMeasureToRadiusRatio();
                    meshQuality->SetTetQualityMeasureToRadiusRatio();
                    break;
                case METRIC_FUNCTION::VOLUME:
                    meshQuality->SetTetQualityMeasureToVolume();
                    meshQuality->SetHexQualityMeasureToVolume();
                    break;
                default:
                    meshQuality->SetTriangleQualityMeasureToScaledJacobian();
                    meshQuality->SetQuadQualityMeasureToScaledJacobian();
                    meshQuality->SetTetQualityMeasureToScaledJacobian();
                    meshQuality->SetHexQualityMeasureToScaledJacobian();
                    break;
            }

            meshQuality->SetInputData(data);
            //compute the mesh quality
            meshQuality->Update();

            //Get result in an array
            vtkSmartPointer<vtkDoubleArray> qualityArray =
                vtkDoubleArray::SafeDownCast(meshQuality->GetOutput()->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));

            //Save the quality array in the mesh
            data->GetCellData()->AddArray(qualityArray);
            qualityArray->Modified();
        }

        void FEModelVTK::writeEntitiesQualityIntoFile(std::string const& filename, int indexMetricUsed)
        {

            //Generate the title
            std::vector<std::string> title;
            title.push_back(" cell ID ");
            title.push_back(" Quality Value ");
            title.push_back(std::string(" " + QualityMetrics_str[indexMetricUsed] + " "));

            /*  Not used anymore, but may be usefull in the future (I don't know)
            bool writeFEInfo = m_fem->isFEmodel(); // te original implementation
            writeFEInfo = false; // writing the FEinfo takes incredibly long time + it is useless for comparing two meshes
            if (writeFEInfo) {
                title.push_back("\"FE Keywords\"");
                title.push_back("\"FE Id\"");
            }
            */

            //Create and open the excell file we want to write in
            boost::filesystem::path filePath(filename);
            ofstream file(filePath.string(), ios::out | ios::trunc);
            if (file.is_open()) 
            {
                //Write the title on the first line
                for (int i = 0; i < title.size(); ++i) 
                {
                    if (i == title.size() - 1)
                        file << title[i] << std::endl;
                    else
                        file << title[i] << ",";
                }

                if (!(m_mesh->GetCellData()->HasArray(QUALITY_SCALAR_ARRAY))) // if quality is not computed for the whole mesh, compute it
                    computeMeshQuality(indexMetricUsed, m_mesh);

                vtkSmartPointer<vtkDoubleArray> qualityArray = vtkDoubleArray::SafeDownCast(m_mesh->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));
                for (int x = 0; x < qualityArray->GetNumberOfTuples(); x++)
                {
                    double val = qualityArray->GetValue(x);
                    file << x << " , " << val << std::endl;
                }
                /* Writing the quality per entity leads to mismatch element IDs so it probably does not have a lot of utility
                for (auto it = m_entitymesh.begin(); it != m_entitymesh.end(); it++) // for each entity
                {
                    if (it->second->GetCellData()->HasArray(QUALITY_SCALAR_ARRAY))
                    {
                        vtkSmartPointer<vtkDoubleArray> qualityArray = vtkDoubleArray::SafeDownCast(it->second->GetCellData()->GetArray(QUALITY_SCALAR_ARRAY));
                        for (int x = 0; x < qualityArray->GetNumberOfTuples(); x++)
                        {
                            double val = qualityArray->GetValue(x);
                            file << x << " , " << val << std::endl;
                        }
                    }
                }*/
                file.close();
            }           
        }

    } //hbm
} //piper
