/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "FEFrame.h"

#include "xmlTools.h"
#include <sstream>
#include <string>
#include <algorithm>
#include <iostream>

namespace piper {
	namespace hbm {

        FEFrame::FEFrame(tinyxml2::XMLElement* element) :MeshComponentIdName< VId, FEFrame>(true), m_hasorigin(false),
            m_hasfirstd(false), m_hasplanedef(false), m_hasfirstdirection(false), m_hasplane(false),
			m_normalizedFirstdirection(false), m_normalizedSeconddirection(false) {parseXml( element);}

        FEFrame::FEFrame() : MeshComponentIdName< VId, FEFrame>(), m_hasorigin(false), m_hasfirstd(false), m_hasplanedef(false), m_hasfirstdirection(false), m_hasplane(false),
			m_normalizedFirstdirection(false), m_normalizedSeconddirection(false) {};

        FEFrame::FEFrame(const Id& nid) : MeshComponentIdName< VId, FEFrame>(nid) {};

		void FEFrame::convId(std::unordered_map<Id,Id> const& map_id) {
			for (size_t n=0; n<size(); n++) {
				m_def[n] = map_id.at(m_def[n]);
			}
		}


		void FEFrame::setOrigin( const Id& id) {
			m_origin = id;
			m_hasorigin = true;
		}

		void FEFrame::setFirstDirection( const Id& id, AxisName dir) {
			m_firstd = id;
			m_firstdirection=dir;
			m_hasfirstd=true;
			m_hasfirstdirection=true;
		}

		void FEFrame::setFirstDirection( const Id& id) {
			m_hasfirstd=true;
			m_firstd = id;
		}

		void FEFrame::setFirstDirection(AxisName dir) {
			m_hasfirstdirection=true;
			m_firstdirection=dir;
		}

		void FEFrame::setFirstDirection( const std::string& dir) {
			m_hasfirstdirection=true;
			std::string dirlower( dir);
			std::transform(dirlower.begin(), dirlower.end(), dirlower.begin(), ::tolower);
            if (dirlower == "x")
                m_firstdirection = AxisName::X;
            else if (dirlower == "y")
                m_firstdirection = AxisName::Y;
            else if (dirlower == "z")
                m_firstdirection = AxisName::Z;
            else {
                std::stringstream str;
                str << "Frame : " << std::endl;
                str << "Undefined axis name in setFirstDirection method: " << dirlower << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
            m_hasfirstdirection=true;
		}

		void FEFrame::setSecondDirection( const Id& id, AxisName dir) {
			m_planedef = id;
			m_plane=dir;
			m_hasplanedef=true;
			m_hasplane=true;
		}

		void FEFrame::setSecondDirection( const Id& id) {
			m_hasplanedef=true;
			m_planedef = id;
		}

		void FEFrame::setSecondDirection( AxisName dir) {
			m_plane=dir;
			m_hasplane=true;
		}

		void FEFrame::setSecondDirection( const std::string& dir) {
			m_hasplane=true;
			std::string dirlower( dir);
			std::transform(dirlower.begin(), dirlower.end(), dirlower.begin(), ::tolower);
            if (dirlower == "x")
                m_plane = AxisName::X;
            else if (dirlower == "y")
                m_plane = AxisName::Y;
            else if (dirlower == "z")
                m_plane = AxisName::Z;
            else {
                std::stringstream str;
                str << "Frame : " << std::endl;
                str << "Undefined axis name in setFirstDirection method: " << dirlower << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
        }

		bool FEFrame::isCompletelyDefined() const {
			return (m_hasorigin && m_hasfirstd && m_hasplanedef && m_hasfirstdirection && m_hasplane);
		}

		bool FEFrame::hasOnlyOrigin() const {
			return (m_hasorigin && (!m_hasfirstd | !m_hasplanedef | !m_hasfirstdirection | !m_hasplane));
		}


		void FEFrame::getMatOrientation(const Coord& org, const Coord& cfirst, const Coord& cplane,
			AxisName firstdirection,
			AxisName SecondDirection,
			Eigen::Matrix3d& orient) {

				Eigen::Vector3d v1= cfirst - org;
				v1.normalize();
				Eigen::Vector3d v2= cplane - org;
				v2.normalize();
				Eigen::Vector3d v3=v1.cross(v2);
				v3.normalize();
				v2 = v3.cross(v1);
				v2.normalize();
				if (firstdirection==AxisName::X) {
					orient(0,0)=v1[0]; orient(0,1)=v1[1]; orient(0,2)=v1[2];
					if (SecondDirection==AxisName::Y) {
						orient(1,0)=v2[0]; orient(1,1)=v2[1]; orient(1,2)=v2[2];
						orient(2,0)=v3[0]; orient(2,1)=v3[1]; orient(2,2)=v3[2];
					}
					else if (SecondDirection==AxisName::Z) {
						v3=-v3;
						//orient(1,0)=v2[0]; orient(1,1)=v2[1]; orient(1,2)=v2[2];
						//orient(2,0)=v3[0]; orient(2,1)=v3[1]; orient(2,2)=v3[2];
						orient(1,0)=v3[0]; orient(1,1)=v3[1]; orient(1,2)=v3[2];
						orient(2,0)=v2[0]; orient(2,1)=v2[1]; orient(2,2)=v2[2];
					}
				}
				else if (firstdirection==AxisName::Y) {
					orient(1,0)=v1[0]; orient(1,1)=v1[1]; orient(1,2)=v1[2];
					if (SecondDirection==AxisName::X) {
						v3=-v3;
						orient(0,0)=v2[0]; orient(0,1)=v2[1]; orient(0,2)=v2[2];
						orient(2,0)=v3[0]; orient(2,1)=v3[1]; orient(2,2)=v3[2];
					}
					else if (SecondDirection==AxisName::Z) {
						orient(0,0)=v3[0]; orient(0,1)=v3[1]; orient(0,2)=v3[2];
						orient(2,0)=v2[0]; orient(2,1)=v2[1]; orient(2,2)=v2[2];
					}
				}
				else if (firstdirection==AxisName::Z) {
					orient(2,0)=v1[0]; orient(2,1)=v1[1]; orient(2,2)=v1[2];
					if (SecondDirection==AxisName::X) {
						orient(0,0)=v2[0]; orient(0,1)=v2[1]; orient(0,2)=v2[2];
						orient(1,0)=v3[0]; orient(1,1)=v3[1]; orient(1,2)=v3[2];
					}
					else if (SecondDirection==AxisName::Y) {
						v3=-v3;
						orient(0,0)=v3[0]; orient(0,1)=v3[1]; orient(0,2)=v3[2];
						orient(1,0)=v2[0]; orient(1,1)=v2[1]; orient(1,2)=v2[2];
					}
				}
				orient.transposeInPlace();
		}


		void FEFrame::getQuatOrientation(const Coord& org, const Coord& cfirst, const Coord& cplane,
			AxisName firstdirection,
			AxisName SecondDirection,
			Eigen::Quaternion<double>& quat) {
				Eigen::Matrix3d orient;
				getMatOrientation( org, cfirst, cplane, firstdirection, SecondDirection, orient);
				Eigen::Quaternion<double> quatt( orient);
				quat=quatt;
				quat = quat.normalized();
		}


		void FEFrame::serializeXml(tinyxml2::XMLElement* element) const {
			tinyxml2::XMLElement* xmlId;
			xmlId = element->GetDocument()->NewElement("Id");
			xmlId->SetText( m_id);
			element->InsertEndChild(xmlId);
			if (m_hasorigin) {
				tinyxml2::XMLElement* xml;
				xml = element->GetDocument()->NewElement("OriginId");
				xml->SetText( m_origin);
				element->InsertEndChild(xml);
			}
			if (m_hasfirstd) {
				tinyxml2::XMLElement* xml;
				xml = element->GetDocument()->NewElement("FirstDirectionId");
				xml->SetText( m_firstd);
				element->InsertEndChild(xml);
				xml = element->GetDocument()->NewElement("FirstDirectionNorm");
				xml->SetAttribute("normalized",m_normalizedFirstdirection);
				if (m_normalizedFirstdirection)
					xml->SetText( m_normFirstdirection);				
				element->InsertEndChild(xml);
			}
			if (m_hasfirstdirection) {
				tinyxml2::XMLElement* xml;
				xml = element->GetDocument()->NewElement("FirstDirectionAxis");
                std::string axisstr  =  AxisNameToStr.at((unsigned int) m_firstdirection);
				xml->SetText( axisstr.c_str());
				element->InsertEndChild(xml);
			}
			if (m_hasplanedef) {
				tinyxml2::XMLElement* xml;
				xml = element->GetDocument()->NewElement("PlaneId");
				xml->SetText( m_planedef);
				element->InsertEndChild(xml);
				xml = element->GetDocument()->NewElement("SecondDirectionNorm");
				xml->SetAttribute("normalized",m_normalizedSeconddirection);
				if (m_normalizedSeconddirection)
					xml->SetText( m_normSeconddirection);				
				element->InsertEndChild(xml);
			}
			if (m_hasplane) {
				tinyxml2::XMLElement* xml;
				xml = element->GetDocument()->NewElement("PlaneAxis");
                std::string axisstr  =  AxisNameToStr.at((unsigned int) m_plane) ;
				xml->SetText( axisstr.c_str());
				element->InsertEndChild(xml);
			}
		}

		void FEFrame::parseXml(tinyxml2::XMLElement* element) {
			std::stringstream id(element->FirstChildElement("Id")->GetText());
			id >> m_id;
			if (element->FirstChildElement("OriginId")) {
				m_hasorigin=true;
				std::stringstream id(element->FirstChildElement("OriginId")->GetText());
				id >> m_origin;
			}
			if (element->FirstChildElement("FirstDirectionId")) {
				m_hasfirstd=true;
				std::stringstream id(element->FirstChildElement("FirstDirectionId")->GetText());
				id >> m_firstd;
				if (element->FirstChildElement("FirstDirectionNorm")->BoolAttribute("normalized")) {
					m_normalizedFirstdirection=true;
					std::vector<double> val;
                    parseValueCont( val, element->FirstChildElement("FirstDirectionNorm"));
					m_normFirstdirection = val[0];
				}
				else
					m_normalizedFirstdirection=false;
			}

			//
			if (element->FirstChildElement("FirstDirectionAxis")) {
				m_hasfirstdirection=true;
				setFirstDirection( element->FirstChildElement("FirstDirectionAxis")->GetText());
			}
			if (element->FirstChildElement("PlaneId")) {
				m_hasplanedef=true;
				std::stringstream id(element->FirstChildElement("PlaneId")->GetText());
				id >> m_planedef;
				if (element->FirstChildElement("SecondDirectionNorm")->BoolAttribute("normalized")) {
					m_normalizedSeconddirection=true;
					std::vector<double> val;
                    parseValueCont( val, element->FirstChildElement("SecondDirectionNorm"));
					m_normSeconddirection = val[0];
				}
				else
                    m_normalizedSeconddirection = false;
			}

			if (element->FirstChildElement("PlaneAxis")) {
				m_hasplane=true;
				setSecondDirection( element->FirstChildElement("PlaneAxis")->GetText());
			}

		}

		Coord FEFrame::normFirstDirection( const Coord& org, const Coord& coord, const double& norm) {
			Eigen::Vector3d v1= coord - org;
			m_normFirstdirection = v1.norm();
			v1.normalize();
			Coord newcoord = org + v1*norm;
			m_normalizedFirstdirection = true;
			return newcoord;
		}

		void FEFrame::register_frameinfo(std::vector<Id> rf){
			m_origin = rf.at(0);
			m_firstd = rf.at(1);
			m_planedef = rf.at(2);
			std::cout << "m_origin:  " << m_origin << std::endl;
			std::cout << "m_firstd:  " << m_firstd << std::endl;
			std::cout << "m_planedef:  " << m_planedef << std::endl;
		}

		Coord FEFrame::normSecondDirection( const Coord& org, const Coord& coord, const double& norm) {
			Eigen::Vector3d v1= coord - org;
			m_normSeconddirection = v1.norm();
			v1.normalize();
			Coord newcoord = org + v1*norm;
			m_normalizedSeconddirection = true;
			return newcoord;
		}

	}
}
