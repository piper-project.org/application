/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "FEJoint.h"
#include "FEModel.h"

#include <string>

namespace piper {
	namespace hbm {

		FEJoint::FEJoint(): m_type(JointType::NOTYPE) {
			std::array<bool,6> m_dof={false,false,false,false,false,false};
		}

		//FEJoint::FEJoint( const Id id): m_type(JointType::NOTYPE), MeshComponentIdName< SId, FEJoint>( id) {
		//	std::array<bool,6> m_dof={false,false,false,false,false,false};
		//}


		void FEJoint::setType(const std::string& type){
			std::string typelower( type);
			std::transform(typelower.begin(), typelower.end(), typelower.begin(), ::tolower);
			if(!typelower.compare("spherical")){
				m_type=JointType::SPHERICAL;
				std::array<bool,6> dof={false,false,false,true,true,true};
				setDoF( dof);
			}
			else if(!typelower.compare("revolute")){
				// default: X axis 
				std::array<bool,6> dof={false,false,false,true,false,false};
				setDoF( dof);
				m_type=JointType::REVOLUTE;
			}
			else if(!typelower.compare("6dof")){
				m_type=JointType::SIXDOF;
				std::array<bool,6> dof={true,true,true,true,true,true};
				setDoF( dof);
			}
			else
				m_type=JointType::NOTYPE;
		}

		JointType FEJoint::getJointType( ) const {
			return m_type;
		}

		void FEJoint::setJointFrameId( const Id& id ) {
			m_def.clear();
			m_def.insert( id);
		}

		void FEJoint::serializeXml(tinyxml2::XMLElement* element) const {
			tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
			xmlName->SetText(m_name.c_str());
			element->InsertEndChild(xmlName);
			tinyxml2::XMLElement* xmlId;
			xmlId = element->GetDocument()->NewElement("Id");
			xmlId->SetText( m_id);
			element->InsertEndChild(xmlId);
			tinyxml2::XMLElement* xmlType;
			xmlType = element->GetDocument()->NewElement("Type");
			std::string typstr  =  JointType_str[(unsigned int) getJointType()] ;
			xmlType->SetText( typstr.c_str());
			element->InsertEndChild(xmlType);
			tinyxml2::XMLElement* xmlFrame;
			xmlFrame = element->GetDocument()->NewElement("FrameId");
			serializeIds(xmlFrame, m_def);
			element->InsertEndChild(xmlFrame);
			tinyxml2::XMLElement* xmlDof;
			xmlDof = element->GetDocument()->NewElement("Dof");
			std::vector<bool> vdof;
			for (size_t n=0; n<6; n++)
				vdof.push_back( m_dof.at(n));
			serializeValue(xmlDof, vdof);
			element->InsertEndChild(xmlDof);
		}

		void FEJoint::parseXml(tinyxml2::XMLElement* element) {
			m_name = element->FirstChildElement("name")->GetText();
			std::stringstream id(element->FirstChildElement("Id")->GetText());
			id >> m_id;
			setType( element->FirstChildElement("Type")->GetText());
			parseIds(m_def, element->FirstChildElement("FrameId"));
			std::vector<bool> vdof;
			parseValue(vdof, element->FirstChildElement("Dof"));
			int n=0;
			for (std::vector<bool>::const_iterator it=vdof.begin(); it!=vdof.end(); ++it) {
				 m_dof[n] = *it;
				 n++;
			}
		}


	}
}
