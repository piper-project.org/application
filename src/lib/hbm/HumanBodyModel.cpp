/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "HumanBodyModel.h"
#include "anatomyDB/query.h"

#include <boost/filesystem.hpp>

namespace piper {
namespace hbm {

HumanBodyModel::HumanBodyModel() {
    m_historydata.setData(&m_fem);
    m_historydata.setData(&m_metadata.anthropometryHistory());

}

HumanBodyModel::HumanBodyModel(std::string const& file)
{
    m_historydata.setData(&m_fem);
    m_historydata.setData(&m_metadata.anthropometryHistory());
    read(file);
}

void HumanBodyModel::clear()
{
    m_metadata.clear();
    m_fem.clear();
    m_historydata.clear();
}

bool HumanBodyModel::empty() const
{
    return m_metadata.sourcePath().empty();
}

void HumanBodyModel::setSource(std::string const& modelrulesFile, std::string const& sourceModelFile) {
    clear();
    m_metadata.setSource(modelrulesFile);
    applySource(sourceModelFile);
}

void HumanBodyModel::initSource(std::string const& modelrulesFile, std::string const& formatrulesFileParent) {
    clear();
    m_metadata.setSource(modelrulesFile, formatrulesFileParent);
}


void HumanBodyModel::applySource(std::string const& sourceModelFile)
{
    if (!sourceModelFile.empty())
        m_metadata.setSourceModelFile(sourceModelFile);
    if (m_metadata.getNameFormat()=="obj") 
        m_fem.readGeomMesh(m_metadata);
    else 
        m_fem.parseMesh(m_metadata);
    m_fem.normalizedFrame(m_metadata.lengthUnit());
    m_fem.getFEModelVTK()->setMeshAsEntities(this->m_metadata.entities(), m_fem, true);
    m_historydata.renameCurrentModel("import_model");
}

void HumanBodyModel::read(std::string const& file)
{
    clear();
    m_metadata.read(file);
    m_fem.readModel(m_metadata.meshPath());
    m_fem.readModelMetadata( file);
    m_historydata.read(file);
	m_fem.getFEModelVTK()->setMeshAsEntities(this->m_metadata.entities(), m_fem, true);
}

void HumanBodyModel::reloadFEMesh() {
    std::string modelname = m_historydata.getCurrent();
    m_historydata.renameCurrentModel("*root");
    m_historydata.addNewHistory(modelname);
    m_historydata.setCurrentModel("*root");
    m_fem.reloadFEmesh(m_metadata);
    m_historydata.setCurrentModel(modelname);
}

void HumanBodyModel::write(std::string const& file)
{
    m_metadata.write(file);
    m_fem.writeModel(m_metadata.meshPath());
    m_fem.writeModelMetadata( file);
    m_historydata.write(file);
}

void HumanBodyModel::exportModel(std::string const& directory, std::vector<std::string> const& msg) {
	m_fem.restoreFrame();
	m_fem.update( m_metadata, directory, msg);
    m_fem.normalizedFrame(m_metadata.lengthUnit());
}

std::size_t HumanBodyModel::exportLandmarks(std::string const& filename)
{
    std::ofstream data;
    data.open(filename);
    data << "# name x y z node_0_x node_0_y node_0_z node_1_x node_1_y node_1_z [...]" << std::endl;
    std::size_t nb=0;
    hbm::Coord coord;
    for (auto const& landmarkPair: metadata().landmarks()) {
        coord = landmarkPair.second.position(fem());
        data << landmarkPair.second.name() << " " << coord[0] << " " << coord[1] << " " << coord[2] << " ";
        VCoord vcoord;
        landmarkPair.second.gatherNodesCoord(fem(), vcoord);
        data << vcoord.size() << " ";
        for (Coord const& c: vcoord)
            data << c[0] << " " << c[1] << " " << c[2] << " ";
        data << std::endl;
        ++nb;
    }
    return nb;
}

void HumanBodyModel::exportEntityNodes(std::string const& directory)
{
    hbm::VId entityIds;
    hbm::Node node;
    std::map<hbm::Id, std::pair<std::string, IdKey>> const& piperIdToIdKey = fem().computePiperIdToIdKeyMap<Node>();
    for(auto const& name_entity : metadata().entities()) {
        std::ofstream data;
        data.open(directory+"/"+name_entity.first+".dat");
        data << "# Entity: " << name_entity.first << std::endl;
        data << "# nodeId x y z" << std::endl;
        name_entity.second.getEntityNodesIds(fem(), entityIds);
        for (hbm::Id id: entityIds) {
            node = fem().getNode(id);
            data << piperIdToIdKey.at(id).second.id << " " << node.getCoordX() << " " << node.getCoordY() << " " << node.getCoordZ() << std::endl;
        }
    }
}

VId HumanBodyModel::findBoneElements()
{
	hbm::VId ret;
	Metadata::EntityCont entities = this->metadata().entities();
	for (auto it = entities.begin(); it != entities.end(); ++it) {
		//std::cout << it->first << "isBones : " << anatomydb::isBone(it->first)  << std::endl;
		if (anatomydb::isBone(it->first)) {
			for (auto itt = it->second.get_groupElement3D().begin(); itt != it->second.get_groupElement3D().end(); ++itt) {
				const hbm::VId& vidcur = this->m_fem.get<hbm::GroupElements3D>(*itt)->get();
				ret.insert(ret.end(), vidcur.begin(), vidcur.end());
			}
			for (auto itt = it->second.get_groupElement2D().begin(); itt != it->second.get_groupElement2D().end(); ++itt) {
				const hbm::VId& vidcur = this->m_fem.get<hbm::GroupElements2D>(*itt)->get();
				ret.insert(ret.end(), vidcur.begin(), vidcur.end());
			}
			for (auto itt = it->second.get_groupElement1D().begin(); itt != it->second.get_groupElement1D().end(); ++itt) {
				const hbm::VId& vidcur = this->m_fem.get<hbm::GroupElements1D>(*itt)->get();
				ret.insert(ret.end(), vidcur.begin(), vidcur.end());
			}
		}
	}
	return ret;
}

VId HumanBodyModel::findBoneNodes(bool skip1D, bool skip2D, bool skip3D)
{
	hbm::VId ret;
	Metadata::EntityCont entities = this->metadata().entities();
	for (auto it = entities.begin(); it != entities.end(); ++it) {
	//	std::cout << it->first << "isBones : " << anatomydb::isBone(it->first)  << std::endl;
		if (anatomydb::isBone(it->first))
		{
			hbm::VId vidcur;
			it->second.getEntityNodesIds(m_fem, vidcur, skip1D, skip2D, skip3D);
			ret.insert(ret.end(), vidcur.begin(), vidcur.end());
		}
	}
	return ret;
}

    void HumanBodyModel::exportModelCustom(std::string directory, std::vector<std::string> const& msg, FEModel fem, Metadata met)
    {
       
        fem.restoreFrame();
       
        fem.update(met, directory, msg);
       
        fem.normalizedFrame(met.lengthUnit());
        
    }
}
}
