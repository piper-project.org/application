/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_PARAMETER_H
#define PIPER_HBM_PARAMETER_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include "BaseMetadata.h"

namespace piper {
	namespace hbm {
        class FEModel;
		/**
		* @brief HBM parameter.
		*
		* @author Erwan Jolivet @date 2015
		*/
        class HBM_EXPORT HbmParameter : public BaseMetadata {
		public:

			HbmParameter();
			HbmParameter(std::string const& name);
			HbmParameter(tinyxml2::XMLElement* element);

			void serializeXml(tinyxml2::XMLElement* element) const;
            virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;

			void setSource(const std::string& source);
			std::string const& source() const;

			VId FEparameters;

		private:
			void parseXml(tinyxml2::XMLElement* element);

			std::string m_source;

		};

	}
}

#endif
