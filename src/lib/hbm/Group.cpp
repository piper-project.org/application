/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Group.h"

#include "FEModel.h"

#include <iostream>

using namespace std;
namespace piper { 
	namespace hbm {

		GroupNode::GroupNode(): Group() {}

		GroupNode::GroupNode(tinyxml2::XMLElement* element): Group( element) {}

		GroupNode::GroupNode(const Id& nid): Group( nid) {};

		GroupElements1D::GroupElements1D(): Group() {}

		GroupElements1D::GroupElements1D(tinyxml2::XMLElement* element): Group( element) {}

		GroupElements1D::GroupElements1D(const Id& nid): Group( nid) {};

		GroupElements2D::GroupElements2D(): Group() {}

		GroupElements2D::GroupElements2D(tinyxml2::XMLElement* element): Group( element) {}

		GroupElements2D::GroupElements2D(const Id& nid): Group( nid) {};	
		
		GroupElements3D::GroupElements3D(): Group() {}

		GroupElements3D::GroupElements3D(tinyxml2::XMLElement* element): Group( element) {}

		GroupElements3D::GroupElements3D(const Id& nid): Group( nid) {};	

        GroupGroups::GroupGroups() : Group() {}

        GroupGroups::GroupGroups(tinyxml2::XMLElement* element) : Group(element) {}

        GroupGroups::GroupGroups(const Id& nid) : Group(nid) {};



	}
}
