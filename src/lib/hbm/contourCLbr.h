/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_CONTOURCLBR
#define PIPER_HBM_CONTOURCLBR

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif
#include "Contour.h"

namespace piper {
	namespace hbm {

		/**
		* @brief A class which defines the basic datastructure of Contour based Body Regions in HumanBody.
		* Each body region is associated with a vector of Children Joints.
		*
		* @author Aditya Chhabra @date 2016
		*/

		class ContourCLj;

		class HBM_EXPORT ContourCLbr {
		public:

			ContourCLbr();
			~ContourCLbr();


			std::string name;
			std::vector<std::string> cplandmarks;
			std::vector<Eigen::Vector3d> ctrlpts;
			std::vector<ContourCLj*> childrenjts;
			std::string circumf;
			std::vector<std::string> circumfvec;
			std::map<std::string, std::map<std::string, std::string>> dimensions;
			std::vector<Contour*> brcont;
			bool flexible;
			std::string skingenericmetadata;
			std::string fleshgenericmetadata;
			std::string description;
			std::vector<std::string> cplandmarks_info;
			std::vector<std::string> circumfvec_info;
			std::string brtype;
			std::vector<std::string> axisdof1;
			std::vector<std::string> axisdof2;
			std::vector<std::string> axisdof3;
			std::vector<std::string> vecskinentity;
			std::vector<std::string> vecskinentity_info;
			std::vector<std::string> dof1;
			std::vector<std::string> dof2;
			std::vector<std::string> dof3;
			std::vector<std::string> dof1_info;
			std::vector<std::string> dof2_info;
			std::vector<std::string> dof3_info;
			std::vector<std::string> reference_landmark;
			std::vector<std::string> reference_landmark_info;
			bool Isprocessed;
			double brangle;

			std::map<std::string, std::string> metadata_names;

			std::string display_name;
			int degreesOfFreedom;
			std::vector<std::pair<std::string, std::string>> required_metadata;
			
			std::vector<std::string> dof1_target;
			std::vector<std::string> dof1_ref;
			std::vector<std::string> dof1_normal;
			std::vector<std::string> dof1_target_info;
			std::vector<std::string> dof1_ref_info;
			std::vector<std::string> dof1_normal_info;
			double dof1_limit_max=0;
			double dof1_limit_min=0;
				
			std::vector<std::string> dof2_target;
			std::vector<std::string> dof2_ref;
			std::vector<std::string> dof2_normal;
			std::vector<std::string> dof2_target_info;
			std::vector<std::string> dof2_ref_info;
			std::vector<std::string> dof2_normal_info;
			double dof2_limit_max=0;
			double dof2_limit_min=0;

			std::vector<std::string> dof3_target;
			std::vector<std::string> dof3_ref;
			std::vector<std::string> dof3_normal;
			std::vector<std::string> dof3_target_info;
			std::vector<std::string> dof3_ref_info;
			std::vector<std::string> dof3_normal_info;
			double dof3_limit_max=0;
			double dof3_limit_min=0;

		public:
			void setName(std::string name);
			void setBodyRegionLandmarks(std::vector<std::string>);
			void addChildJoint(ContourCLj*);
		};



	}
}
#endif

