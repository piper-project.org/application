/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_HUMANBODYMODEL_H
#define PIPER_HBM_HUMANBODYMODEL_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>

#include "Metadata.h"
#include "FEModel.h"
#include "HistoryManager.h"

namespace piper {
namespace hbm {

/**
 * @brief The HumanBodyModel class is composed of Metadata and FEmodel.
 *
 * @author Thomas Lemaire @date 2015
 * \ingroup libhbm
 */
class HBM_EXPORT HumanBodyModel {
public:

    HumanBodyModel();
    HumanBodyModel(std::string const& file);
    ~HumanBodyModel() { clear(); }

    /// clear the model, metadata and fem
    void clear();

    bool empty() const;


    /// initialize source info: clear the model set source info (format rule path, source model file and model units)
    void initSource(std::string const& modelrulesFile, std::string const& formatrulesFileParent = "");

    /// apply source and set a new surce model file. source must initialized before
    void applySource(std::string const& sourceModelFile = "");

    /// set source info and apply it (with optional another source model file)
    void setSource(std::string const& modelrulesFile, std::string const& sourceModelFile = "");

    /// read model from @param file
    void read(std::string const& file);

	/// reload FE mesh
	void reloadFEMesh();

    Metadata& metadata() {return m_metadata;}
    Metadata const& metadata() const {return m_metadata;}
    FEModel& fem() {return m_fem;}
    FEModel const& fem() const {return m_fem;}
    HistoryManager& history() { return m_historydata; }
    HistoryManager const& history() const { return m_historydata; }


    /// write the current model to @param file
    void write(std::string const& file);

    /// export the current model to @param directory
    void exportModel(std::string const& directory, std::vector<std::string> const& msg = std::vector<std::string>());

    /** Export landmarks position to a plain text file \a filename
     * \returns the number of exported landmarks
     */
    std::size_t exportLandmarks(std::string const& filename);

    /** Export entities nodes coordinates along with their original id into \a directory.
     *  Each entity nodes are exported in a separate file.
     */
    void exportEntityNodes(std::string const& directory);
	
	/// <summary>
	/// Finds all elements that belong to any bone.
	/// </summary>
	/// <returns>IDs of all elements belonging to a bone.</returns>
	VId findBoneElements();

	/// <summary>
	/// Finds all nodes that belong to any bone.
	/// </summary>
	/// <param name="skip1D">If true, nodes belonging to 1D elements will not be added to the output list.</param>
	/// <param name="skip2D">If true, nodes belonging to 2D elements will not be added to the output list.</param>
	/// <param name="skip3D">If true, nodes belonging to 3D elements will not be added to the output list.</param>
	/// <returns>IDs of all nodes belonging to a bone.</returns>
	VId findBoneNodes(bool skip1D = false, bool skip2D = false, bool skip3D = false);

    void exportModelCustom(std::string directory, std::vector<std::string> const& msg, FEModel fem, Metadata met);


private:

    Metadata m_metadata;
    FEModel m_fem;
    HistoryManager m_historydata;

};

}
}
#endif // PIPER_HBM_HUMANBODYMODEL_H
