/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef ENTITYJOINT_H
#define ENTITYJOINT_H

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

#include <string>
#include <array>
#include <map>


#include "BaseMetadata.h"

namespace piper {
namespace hbm {
    class FEModel;
/**
 * Base class for joint metadata.
 *
 * \author Thomas Lemaire \date 2016
 */
class HBM_EXPORT BaseJointMetadata: public BaseMetadata {
public:
    enum class ConstrainedDofType {HARD, ///< The constrained dof cannot move appart
                                   SOFT  ///< The constrained dof are authorized to move appart to meet other more important constraints
                                  };
    /// string representation to ConstrainedDofType map
    static const std::map<std::string, ConstrainedDofType> strToConstrainedDofType;
    /// ConstrainedDofType to string representation map
    static const std::map<ConstrainedDofType, std::string> constrainedDofTypeToStr;

    BaseJointMetadata();
    BaseJointMetadata(std::string const& name);

    virtual void serializeXml(tinyxml2::XMLElement* element) const;
    virtual void serializePmr(tinyxml2::XMLElement* element) const;

    void setDof( const std::array<bool,6> dof) {m_dof=dof;}
    void setConstrainedDofType(ConstrainedDofType type) {m_constrainedDofType=type;}

    /** (TX, TY, TZ, RX, RY RZ)
    * O: blocked; 1: free
    **/
    const std::array<bool,6>& getDof() const {return m_dof;}
    ConstrainedDofType getConstrainedDofType() const {return m_constrainedDofType;}

protected:
    std::array<bool,6> m_dof;
    ConstrainedDofType m_constrainedDofType;

    virtual void parseXml(tinyxml2::XMLElement* element);
};


/**
 * Metadata for robotic joint definition.
 *
 * \author Thomas Lemaire \date 2015
 */
class HBM_EXPORT EntityJoint: public BaseJointMetadata {
public:

    EntityJoint();
    EntityJoint(std::string const& name);
    EntityJoint(tinyxml2::XMLElement* element);

    void serializeXml(tinyxml2::XMLElement* element) const;
    virtual void serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const;

    void setEntity1( const std::string& name);
    void setEntity2( const std::string& name);
    void setEntity1FrameId(Id const& id) {m_entity1FrameId = id;}
    void setEntity2FrameId(Id const& id) {m_entity2FrameId = id;}

    const std::string& getEntity1() const {return m_entity1Name;}
    const std::string& getEntity2() const {return m_entity2Name;}
    Id const& getEntity1FrameId() const {return m_entity1FrameId;}
    Id const& getEntity2FrameId() const {return m_entity2FrameId;}

private:

    std::string m_entity1Name, m_entity2Name;
    Id m_entity1FrameId, m_entity2FrameId;

    /// @todo error checking
    void parseXml(tinyxml2::XMLElement* element);

};

/**
 * Metadata for a robotic joint defined according to the ISB frames.
 *
 * \todo define and document ISB frames and joint
 * \author Thomas Lemaire \date 2016
 */
class HBM_EXPORT AnatomicalJoint : public BaseJointMetadata {
public:
    AnatomicalJoint();
    AnatomicalJoint(std::string const& name);
    AnatomicalJoint(tinyxml2::XMLElement* element);

};

}
}
#endif
