/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Landmark.h"

#include <exception>

#include "xmlTools.h"
#include "FEModel.h"
#include "Helper.h"

namespace piper {
namespace hbm {


Landmark::Landmark() {}

Landmark::Landmark(std::string const& name, Type type)
    : BaseMetadataGroup(name)
    , m_type(type)
{}

Landmark::Landmark(tinyxml2::XMLElement* element)
{
    parseXml(element);
}

void Landmark::parseXml(tinyxml2::XMLElement* element)
{
    BaseMetadataGroup::parseXml(element);

    if (element && element->Attribute("type")){
        setLandmarkType(element->Attribute("type"));
    }
    parseValueCont(this->node, element->FirstChildElement("node"));
}

void Landmark::serializeXml(tinyxml2::XMLElement* element) const
{
    std::string type;
    if(getLandmarkTypeString(type)){
        element->SetAttribute("type",type.c_str());
    }
    BaseMetadataGroup::serializeXml(element);

    tinyxml2::XMLElement* xmlIds;
    if (!this->node.empty()) {
        xmlIds = element->GetDocument()->NewElement("node");
        serializeValueCont(xmlIds, this->node);
        element->InsertEndChild(xmlIds);
    }
}

void Landmark::setNodes(VId &nodes)
{
    node.clear();
    node.insert(node.begin(), nodes.begin(), nodes.end());
}

Coord Landmark::position(FEModel const& fem) const
{
    VCoord allCoord;
    gatherNodesCoord(fem, allCoord);
    switch(type()) {
        case Type::POINT: {
            if (allCoord.size()!=1)
                throw std::runtime_error("Cannot compute position - landmark: "+name());
            return allCoord[0];
        }
        case Type::SPHERE: {
            if (allCoord.size()<4)
                throw std::runtime_error("Cannot compute position - landmark: "+name());
            Coord position; double radius;
            fitSphere(allCoord, position, radius);
            return position;
        }
        case Type::BARYCENTER: {
            if (allCoord.empty())
                throw std::runtime_error("Cannot compute position - landmark: "+name());
            Coord position = allCoord[0];
            for (unsigned int i=1; i<allCoord.size(); ++i)
                position += allCoord[i];
            position /= allCoord.size();
            return position;
        }
    }
    throw std::runtime_error("Unknown Landmark type");
}

bool Landmark::getLandmarkTypeString(std::string& iLandmarkType) const{
    switch(m_type){
    case Type::POINT : {
            iLandmarkType = "point";
            return true;}
        case Type::SPHERE : {
            iLandmarkType = "sphere";
            return true;}
        case Type::BARYCENTER : {
            iLandmarkType = "barycenter";
            return true;}
    }
    return false; // if it unkonwn type, return false - not throw an exception as with setLandmarkType?
}

void Landmark::setLandmarkType(const char* type){
    if(!type)
        throw std::runtime_error("Unknown Landmark type");

    std::string typeStr(type);
    std::transform(typeStr.begin(), typeStr.end(), typeStr.begin(), ::tolower);
    if(!typeStr.compare("point")){
        m_type=Type::POINT;
        return;}
    if(!typeStr.compare("sphere")){
        m_type=Type::SPHERE;
        return;}
    if(!typeStr.compare("barycenter")){
        m_type=Type::BARYCENTER;
        return;}
    throw std::runtime_error("Unknown Landmark type");
}

void Landmark::gatherNodesCoord(FEModel const& fem, VCoord& coords) const
{
    for (auto it=node.begin(); it!=node.end(); ++it)
        coords.push_back(fem.getNode(*it).get());
    for (auto it1 = groupNode.begin(); it1 != groupNode.end(); ++it1) {
        const VId& currentGroupNode = fem.getGroupNode(*it1).get();
        for (VId::const_iterator it2=currentGroupNode.begin(); it2!=currentGroupNode.end(); ++it2)
            coords.push_back(fem.getNode(*it2).get());
    }
}

void Landmark::serializePmr(tinyxml2::XMLElement* element, FEModel& fem) const {
    BaseMetadataGroup::serializePmr(element, fem);
    std::string type;
    if (getLandmarkTypeString(type)){
        element->SetAttribute("type", type.c_str());
    }
    if (node.size() > 0) {
        std::map<std::string, std::vector<IdKey>> fe_ids;
        std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<Node>();
        for (Id const& id : node)
            fe_ids[mpIds.at(id).first].push_back(mpIds.at(id).second);
        serializePmr_FEids(element, fe_ids);
    }

}

void Landmark::clear(FEModel& fem) {
    // detect landmarks that are defined using coord (= node are not registered
    std::map<hbm::Id, std::pair<std::string, IdKey>> const& mpIds = fem.computePiperIdToIdKeyMap<Node>();
    for (Id const& idg : get_groupNode()) {
        VId vidcontent = fem.get<GroupNode>(idg)->get();
        for (Id const& idn : vidcontent) {
            if (mpIds.find(idn) == mpIds.end())
                fem.delNode(idn);
        }
    }
    groupNode.clear();
    for (Id const& idn : node) {
        if (mpIds.find(idn) == mpIds.end())
            fem.delNode(idn);
    }
    node.clear();
}


}
}

