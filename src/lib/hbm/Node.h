/*******************************************************************************
* Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar                         *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv          *
* Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla        *
* (FITT); Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear    *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef NODE__H
#define NODE__H
#include <iostream>

#ifdef WIN32
#	ifdef hbm_EXPORTS
#		define HBM_EXPORT __declspec( dllexport )
#	else
#		define HBM_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif



#include "MeshComponentContainer.h"

namespace piper {
	namespace hbm {
        
		// **********************************************************
		// Nodes
		// **********************************************************

		
		//class ModelReader;
		class Node : public MeshComponent< Coord, Node> {
			//friend class ModelReader;
            friend class FEModelVTK;
		public:
            HBM_EXPORT Node();
            explicit Node(const Id& nid);
            HBM_EXPORT ~Node() { }


			// ------------- interface --------------
            HBM_EXPORT virtual void convId(std::unordered_map<Id,Id> const& map_id);
			// ------------- utlities --------------
			HBM_EXPORT double& getCoordX();
			HBM_EXPORT double& getCoordY();
			HBM_EXPORT double& getCoordZ();
			HBM_EXPORT const double& getCoordX() const;
			HBM_EXPORT const double& getCoordY() const;
			HBM_EXPORT const double& getCoordZ() const;
            HBM_EXPORT void setCoord(double x, double y, double z) {
                m_def << x , y , z;
            }
			void setCoord( const Coord& def);

		};

		class Nodes : public MeshComponentContainer< Node> {

		public:
            Nodes(): MeshComponentContainer< Node>( "NODES")  {}
		};


        typedef std::shared_ptr<Node> NodePtr;
	}
}


#endif // NODES__H
