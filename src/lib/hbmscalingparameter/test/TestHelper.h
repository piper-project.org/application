/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef TESTHELPER_H
#define TESTHELPER_H

#include "gtest/gtest.h"

#include <vector>

template<typename T>
::testing::AssertionResult VectorMatch(const std::vector<T>& expected,
									   const std::vector<T>&  actual) {
										   size_t size =  expected.size();
										   for (size_t i(0); i < size; ++i){
											   if (expected[i] != actual[i]){
												   return ::testing::AssertionFailure() << "actual[" << i
													   << "] (" << actual[i] << ") != expected[" << i
													   << "] (" << expected[i] << ")";
											   }
										   }

										   return ::testing::AssertionSuccess();
}

template<typename T>
::testing::AssertionResult VectorMatchNear(const std::vector<T>& expected,
    const std::vector<T>&  actual,double const& precision) {
    size_t size = expected.size();
    for (size_t i(0); i < size; ++i){
        if (std::abs(expected[i] - actual[i])>precision){
            return ::testing::AssertionFailure() << "actual[" << i
                << "] (" << actual[i] << ") != expected[" << i
                << "] (" << expected[i] << ")";
        }
    }

    return ::testing::AssertionSuccess();
}
#endif
