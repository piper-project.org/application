/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/HumanBodyModel.h"
#include "hbm/target.h"
#include "hbmscalingparameter/HbmScalingParameter.h"

#include <boost/filesystem.hpp>

#include "gtest/gtest.h"
#include "TestHelper.h"


using namespace piper::hbm;

class ScalingParameter_test : public ::testing::Test {

protected:

    virtual void SetUp() {
    }
};


TEST(ScalingParameter_test, applyScalingValue) {
    HumanBodyModel hbm, hbmexport;
    double scale1, scale2, scale3;
    Id idparam1, idparam2, idparam3;
    std::string source1, source2, source3;
    std::vector<double> valuesIntbefore3, valuesIntafter3, valuesupdated3, valuesafter3;
    std::vector<double> valuesbefore1, valuesbefore2, valuesafter1, valuesafter2, valuesupdated1, valuesupdated2;
    hbm.setSource("model_01_description_LSDyna.pmr");
    //get parameter before scaling
    idparam1 = hbm.metadata().hbmParameter("Density_1").FEparameters[0];
    source1 = hbm.metadata().hbmParameter("Density_1").source();
    valuesbefore1 = hbm.fem().get<FEModelParameter>(idparam1)->get(source1);
    idparam2 = hbm.metadata().hbmParameter("Thickness_1").FEparameters[0];
    source2 = hbm.metadata().hbmParameter("Thickness_1").source();
    valuesbefore2 = hbm.fem().get<FEModelParameter>(idparam2)->get(source2);
    idparam3 = hbm.metadata().hbmParameter("test_MATID").FEparameters[0];
    source3 = hbm.metadata().hbmParameter("test_MATID").source();
    valuesIntbefore3 = hbm.fem().get<FEModelParameter>(idparam3)->get(source3);  
    //appply scaling
    scale1 = 2;
    TargetList target;
    ScalingParameterTarget newtarget1;
    newtarget1.setName("Density_1");
    newtarget1.setValue(scale1);
    target.add(newtarget1);
    std::vector<double> valuescheck1(valuesbefore1);
    for (double &v : valuescheck1) { v *= scale1; }
    scale2 = 10;
    ScalingParameterTarget newtarget2;
    newtarget2.setName("Thickness_1");
    newtarget2.setValue(scale2);
    target.add(newtarget2);
    std::vector<double> valuescheck2(valuesbefore2);
    for (double &v : valuescheck2) { v *= scale2; }
    scale3 = 3;
    ScalingParameterTarget newtarget3;
    newtarget3.setName("test_MATID");
    newtarget3.setValue(scale3);
    target.add(newtarget3);
    piper::hbmscalingparameter::applyScalingParameterTarget(hbm, target);
    std::vector<double> valuescheck3(valuesIntbefore3);
    for (double &v : valuescheck3) { v *= scale3; }
    // check new values
    valuesafter1 = hbm.fem().get<FEModelParameter>(idparam1)->get(source1);
    valuesafter2 = hbm.fem().get<FEModelParameter>(idparam2)->get(source2);
    valuesafter3 = hbm.fem().get<FEModelParameter>(idparam3)->get(source3);
    // 
    EXPECT_EQ(valuesafter1, valuescheck1);
    EXPECT_EQ(valuesafter2, valuescheck2);
    EXPECT_EQ(valuesafter3, valuescheck3);
    //export
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    hbm.exportModel(tmp.string());
    hbm.clear();
    //re import and check updated value
    tmp /= "model_01.dyn";
    hbmexport.setSource("model_01_description_LSDyna.pmr", tmp.string());
    //re import and check updated value
    valuesupdated1 = hbmexport.fem().get<FEModelParameter>(idparam1)->get(source1);
    EXPECT_TRUE(VectorMatch(valuesupdated1, valuescheck1));
    valuesupdated2 = hbmexport.fem().get<FEModelParameter>(idparam2)->get(source2);
    EXPECT_TRUE(VectorMatchNear(valuesupdated2, valuescheck2,1e-5));
    valuesupdated3 = hbmexport.fem().get<FEModelParameter>(idparam3)->get(source3);
    EXPECT_FALSE(VectorMatchNear(valuesupdated3, valuescheck3, 1e-5));

}
