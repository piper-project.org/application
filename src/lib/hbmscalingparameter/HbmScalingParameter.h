/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_HBM_SCALINGPARAMETER_H
#define PIPER_HBM_SCALINGPARAMETER_H

#include <string>
#include <vector>

#ifdef WIN32
#	ifdef HbmScalingParameter_EXPORTS
#		define HBMSCALINGPARAMETER_EXPORT __declspec( dllexport )
#	else
#		define HBMSCALINGPARAMETER_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBMSCALINGPARAMETER_EXPORT
#endif

namespace piper
{
namespace hbm
{
    class TargetList;
    class HumanBodyModel;
}
namespace hbmscalingparameter
{
    HBMSCALINGPARAMETER_EXPORT void applyScalingParameterTarget(piper::hbm::HumanBodyModel& hbm, piper::hbm::TargetList const& target);
}
}

#endif
