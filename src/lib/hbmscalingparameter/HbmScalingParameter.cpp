/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "HbmScalingParameter.h"

#include "hbm/target.h"
#include "hbm/HumanBodyModel.h"

namespace piper
{
namespace hbmscalingparameter
{

    void applyScalingParameterTarget(piper::hbm::HumanBodyModel& hbm, piper::hbm::TargetList const& target) {
        for (hbm::ScalingParameterTarget const& scalingParametertarget : target.scalingparameter) {
            if (hbm.metadata().hasHbmParameter(scalingParametertarget.name())) {
                piper::hbm::HbmParameter hbmmeta = hbm.metadata().hbmParameter(scalingParametertarget.name());
                std::string cursource = hbmmeta.source();
                piper::hbm::VId curFEparam = hbmmeta.FEparameters;
                for (piper::hbm::VId::const_iterator itt = curFEparam.begin(); itt != curFEparam.end(); ++itt) {
                    std::vector<double> values = hbm.fem().getFEModelParameter(*itt).get(cursource);
                    if (scalingParametertarget.value().size() == 1) {
                        for (std::vector<double>::iterator itv = values.begin(); itv != values.end(); ++itv) {
                            *itv *= scalingParametertarget.value()[0];
                        }
                    }
                    else if (scalingParametertarget.value().size() == values.size()) {
                        for (int n = 0; n < values.size(); n++)
                            values[n] *= scalingParametertarget.value()[n];
                    }
                    else {
                        std::stringstream str;
                        str << "ScalingParameter: " << scalingParametertarget.name() << ": number of scliang factors is not consistent with number of parameter values." << std::endl;
                        throw std::runtime_error(str.str().c_str());
                        return;
                    }
                    hbm.fem().setParametersValue(*itt, cursource, values);
                }
            }
            else {
                std::stringstream str;
                str << "ScalingParameter: " << scalingParametertarget.name() << ": not exists in metadata of the project." << std::endl;
                throw std::runtime_error(str.str().c_str());
                return;
            }
        }
    }


}
}
