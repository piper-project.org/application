/*******************************************************************************
* Copyright (C) 2017 CEESAR, INRIA                                             *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Thomas Lemaire (INRIA)          *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef UNITS_UNITS_H
#define UNITS_UNITS_H

#ifdef WIN32
#	ifdef units_EXPORTS
#		define UNITS_EXPORT __declspec( dllexport )
#	else
#		define UNITS_EXPORT __declspec( dllimport )
#	endif
#else
#	define UNITS_EXPORT
#endif

#include <map>
#include <string>

namespace piper {
    namespace units {

    /** length units
     * \warning make sure you have initialized the units before using it
     * \sa UnitTraits::defaultUnit()
     */
    enum class UNITS_EXPORT Length { m, dm, cm, mm };

    /** mass units
     * \warning make sure you have initialized the units before using it
     * \sa UnitTraits::defaultUnit()
     */
    enum class UNITS_EXPORT Mass { kg, hg, g, dg, cg, mg };
    /** age units
     * \warning make sure you have initialized the units before using it
     * \sa UnitTraits::defaultUnit()
     * \todo "age" is not a standard unit, should rather be called "time"
     */
    enum class UNITS_EXPORT Age { year, month };


    template<typename UnitType>
    class ConvertMaps
    {
    public:
        /// string to unit map to convert a string into a unit
        static std::map<std::string, UnitType> strToUnitMap;
        /// unit to string map to convert a unit into a string
        static std::map<UnitType, std::string> unitToStrMap;
        /// values for different units, used for convertion
        static std::map<UnitType, double> unitValue;
    };
    
    /** Unit type traits class
     * \authors Erwan Jolivet, Thomas Lemaire \date 2016
     */
    template <typename UnitType>
    class UNITS_EXPORT UnitTraits {
    public:

        static UnitType defaultUnit();

        /// \return the string representation of this unit
        static std::string type();

        static UnitType strToUnit(std::string const& unitstr);

        static std::string unitToStr(UnitType const& unit);

        static void checkStringUnit(std::string const& unit);

        /// @return \a value converted from \a sourceUnit to \a targetUnit
        template <typename DataType>
        static double convert(DataType const& value, UnitType sourceUnit, UnitType targetUnit) {
            DataType result = value;
            convert(sourceUnit, targetUnit, result);
            return result;
        }

        /// convert l \a value from \a sourceUnit to \a targetUnit for Length (inplace convertion)
        template <typename DataType>
        static void convert(UnitType sourceUnit, UnitType targetUnit, DataType& value) {
            value *= getValueAt(sourceUnit) / getValueAt(targetUnit);
        }

    protected:

        static double getValueAt(UnitType valKey);

    };

    /*
     * utility functions
     */

    /// @return \a value converted from \a sourceUnit to \a targetUnit
    template <typename UnitType, typename DataType>
    double convert(DataType const& value, UnitType sourceUnit, UnitType targetUnit)
    {
        return UnitTraits<UnitType>::convert(value, sourceUnit, targetUnit);
    }

    /// convert l \a value from \a sourceUnit to \a targetUnit for Length (inplace convertion)
    template <typename UnitType, typename DataType>
    void convert(UnitType sourceUnit, UnitType targetUnit, DataType& value)
    {
        UnitTraits<UnitType>::convert(sourceUnit, targetUnit, value);
    }

    /*
     * specialization for Length
     */

    template<>
    inline Length UnitTraits<Length>::defaultUnit() {
        return Length::m;
    }

    template<>
    inline std::string UnitTraits<Length>::type() {
        return "length";
    }

    /*
     * specialization for Mass
     */

    template<>
    inline Mass UnitTraits<Mass>::defaultUnit() {
        return Mass::kg;
    }

    template<>
    inline std::string UnitTraits<Mass>::type() {
        return "mass";
    }

    /*
     * specialization for Age
     */

    template<>
    inline Age UnitTraits<Age>::defaultUnit() {
        return Age::year;
    }

    template<>
    inline std::string UnitTraits<Age>::type() {
        return "age";
    }

    /*
     * typesdefs and extern templates
     */

    typedef UnitTraits< Length > LengthUnit;
    typedef UnitTraits< Mass > MassUnit;
    typedef UnitTraits< Age > AgeUnit;


    }
}

#endif
