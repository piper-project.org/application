/*******************************************************************************
* Copyright (C) 2017 CEESAR, INRIA                                             *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Thomas Lemaire (INRIA)          *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <units/units.h>

#include "gtest/gtest.h"

using namespace piper;

TEST(units_test, convertLength) {
    EXPECT_DOUBLE_EQ(1500., units::convert(1.5, units::Length::m, units::Length::mm)  );
    EXPECT_DOUBLE_EQ(1.5, units::convert(15., units::Length::cm, units::Length::dm)  );
}

TEST(units_test, convertWeight) {
    EXPECT_DOUBLE_EQ(1500., units::convert(1.5, units::Mass::kg, units::Mass::g));
    EXPECT_DOUBLE_EQ(1500000., units::convert(1.5, units::Mass::kg, units::Mass::mg));
    EXPECT_DOUBLE_EQ(1.5, units::convert(15., units::Mass::cg, units::Mass::dg));
}

TEST(units_test, convertAge) {
    EXPECT_DOUBLE_EQ(18., units::convert(1.5, units::Age::year, units::Age::month));
    EXPECT_DOUBLE_EQ(1.5, units::convert(18.0, units::Age::month, units::Age::year));
}


TEST(units_test, LengthUnit) {
    typedef units::UnitTraits<units::Length> UnitType;
    units::Length test = UnitType::defaultUnit();
    EXPECT_EQ("m", UnitType::unitToStr(test));
    EXPECT_EQ(units::Length::m, test);
    double testvalue = 7;
    units::convert(test, units::Length::mm, testvalue);
    EXPECT_EQ(7000, testvalue);
}

TEST(units_test, AgeUnit) {
    typedef units::UnitTraits<units::Age> UnitType;
    units::Age test = UnitType::defaultUnit();
    EXPECT_EQ("year", UnitType::unitToStr(test));
    EXPECT_EQ(units::Age::year, test);
    double testvalue = 7;
    units::convert(test, units::Age::month, testvalue);
    EXPECT_EQ(7*12, testvalue);
}


TEST(units_test, MassUnit) {
    typedef units::UnitTraits<units::Mass> UnitType;
    units::Mass test = UnitType::defaultUnit();
    EXPECT_EQ("kg", UnitType::unitToStr(test));
    EXPECT_EQ(units::Mass::kg, test);
    double testvalue = 7;
    units::convert(test, units::Mass::mg, testvalue);
    EXPECT_EQ(7000000, testvalue);
}
