/*******************************************************************************
* Copyright (C) 2017 CEESAR, INRIA                                             *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR); Thomas Lemaire (INRIA)          *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "units.h"

namespace piper {
    namespace units {

    /*
        * Length
        */
        template<>
        std::map<std::string, Length> ConvertMaps<Length>::strToUnitMap = { { "m", Length::m }, { "dm", Length::dm }, { "cm", Length::cm }, { "mm", Length::mm } };

        template<>
        std::map<Length, std::string> ConvertMaps<Length>::unitToStrMap = { { Length::m, "m" }, { Length::dm, "dm" }, { Length::cm, "cm" }, { Length::mm, "mm" } };

        template<>
        std::map<Length, double> ConvertMaps<Length>::unitValue =
            { { Length::m, 1.0 },
            { Length::dm, 0.1 },
            { Length::cm, 0.01 },
            { Length::mm, 0.001 } };


    /*
        * Mass
        */

        template<>
        std::map<std::string, Mass> ConvertMaps<Mass>::strToUnitMap = { { "kg", Mass::kg },
            { "hg", Mass::hg }, { "g", Mass::g }, { "dg", Mass::dg }, { "cg", Mass::cg }, { "mg", Mass::mg } };

        template<>
        std::map<Mass, std::string> ConvertMaps<Mass>::unitToStrMap = { { Mass::kg, "kg" },
            { Mass::hg, "hg" }, { Mass::g, "g" },{ Mass::dg, "dg" },  { Mass::cg, "cg" }, { Mass::mg, "mg" } };

        template<>
        std::map<Mass, double> ConvertMaps<Mass>::unitValue =
            { { Mass::kg, 1.0 },
            { Mass::hg, 0.1 },
            { Mass::g, 0.001 },
            { Mass::dg, 0.0001 },
            { Mass::cg, 0.00001 },
            { Mass::mg, 0.000001 } };

    /*
        * Age
        */
        template<>
        std::map<std::string, Age> ConvertMaps<Age>::strToUnitMap = { { "year", Age::year }, { "month", Age::month } };

        template<>
        std::map<Age, std::string> ConvertMaps<Age>::unitToStrMap = { { Age::year, "year" }, { Age::month, "month" } };

        template<>
        std::map<Age, double> ConvertMaps<Age>::unitValue =
            { { Age::year, 365.25*24.0*3600.0 },
            { Age::month, 365.25 * 24.0 * 3600.0 / 12.0 } };


        template <typename UnitType>
        UnitType UnitTraits<UnitType>::strToUnit(std::string const& unitstr) {
            checkStringUnit(unitstr);
            return ConvertMaps<UnitType>::strToUnitMap.at(unitstr);
        }

        template <typename UnitType>
        std::string UnitTraits<UnitType>::unitToStr(UnitType const& unit) {
            return ConvertMaps<UnitType>::unitToStrMap.at(unit);
        }

        template <typename UnitType>
        void UnitTraits<UnitType>::checkStringUnit(std::string const& unit) {
            if (ConvertMaps<UnitType>::strToUnitMap.find(unit) == ConvertMaps<UnitType>::strToUnitMap.end())
                throw std::runtime_error(type() + " invalid unit: " + unit);
        }
    
        template <typename UnitType>
        double UnitTraits<UnitType>::getValueAt(UnitType valKey)
        {
            return ConvertMaps<UnitType>::unitValue.at(valKey);
        }

        /*
        * explicit template instantiation
        */

        template UNITS_EXPORT class UnitTraits<Length>;
        template UNITS_EXPORT class UnitTraits<Age>;
        template UNITS_EXPORT class UnitTraits<Mass>;

    }
}
