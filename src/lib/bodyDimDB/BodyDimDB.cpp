/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Dupeux (UCBL-Ifsttar)                                    *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "BodyDimDB.h"
#include <iostream>

namespace piper {
namespace BodyDimDB {

bool DataBaseReader::isDbLoaded(){
	return _isDBLoaded;
}

bool DataBaseReader::read(std::string filename, BodyDimDB& db){
    
    tinyxml2::XMLDocument dbfile;
	dbfile.LoadFile(filename.c_str());
	if (dbfile.Error()) {
		_isDBLoaded=false;
        std::stringstream errorMsg;
        errorMsg << "[FAILED] Load database file: " << filename.c_str() << std::endl << dbfile.ErrorStr() << std::endl;
		throw std::runtime_error(errorMsg.str().c_str());
		return false;
	}

    

	tinyxml2::XMLElement* db_root;

    db_root=dbfile.FirstChildElement("dimdatabase");
    
    tinyxml2::XMLElement* subjectElem=db_root->FirstChildElement("subject");

    while(subjectElem!=NULL){
        BodyDimDB::Subject subject(&db._conv);

        parseSubject(subjectElem,subject);
        auto res = db.m_subjects.insert(std::make_pair(subject._type,subject));
        if(!res.second)
            std::cout<<"[Warning] BodyDimDB : second definition of "<<subject._type<<" ignored"<<std::endl;
               
        subjectElem=subjectElem->NextSiblingElement("subject");
    }

    dbFileName = filename;
	_isDBLoaded=true;
    return true;
}

bool DataBaseReader::parseSubject(tinyxml2::XMLElement* subjectElem,  BodyDimDB::Subject& subject){
    if( subjectElem != nullptr){
        subject._type = subjectElem->Attribute("type");

        tinyxml2::XMLElement* equationElem = subjectElem->FirstChildElement("equation");

        while(equationElem != nullptr){
            BodyDimDB::Equation equation(subject._conv);  
            if(parseEquation(equationElem, equation)){
                auto res = subject.m_equations.insert(std::make_pair(equation._name,equation));
                if(!res.second)
                    std::cout<<"[Warning] BodyDimDB : second definition of "<<subject._type<<" : "<<equation._name<<" ignored"<<std::endl;
            }
            equationElem = equationElem->NextSiblingElement("equation");
        }
        return true;
    }
    return false;
}
bool DataBaseReader::parseEquation(tinyxml2::XMLElement* equationElem,  BodyDimDB::Equation& equation){
    if( equationElem != nullptr){
        equation._name = equationElem->Attribute("name");
        equation._nbVariables = std::stoi(equationElem->Attribute("nbVariables"));

        tinyxml2::XMLElement* variableElem = equationElem->FirstChildElement("variable");

        while(variableElem != nullptr){
            BodyDimDB::Variable variable;
            if(parseVariable(variableElem, variable)){
                auto res = equation.m_variables.insert(std::make_pair(variable._name,variable));
                if(!res.second)
                    std::cout<<"[Warning] BodyDimDB : second definition of "<<equation._name<<" : "<<variable._name<<" ignored"<<std::endl;
            }
            variableElem = variableElem->NextSiblingElement("variable");
        }
        return true;
    }
    return false;
}

bool DataBaseReader::parseVariable(tinyxml2::XMLElement* variableElem,  BodyDimDB::Variable& variable){
    if (variableElem != nullptr){
        variable._id = std::stoi(variableElem->Attribute("id"));
        variable._name = variableElem->Attribute("name");
        variable._unit = variableElem->Attribute("unit");
        parseRange(variableElem->FirstChildElement("range"),variable);

        tinyxml2::XMLElement* dimensionElem = variableElem->FirstChildElement("dimension");

        while(dimensionElem != nullptr){
            BodyDimDB::Dimension dim;
            if(parseDimension(dimensionElem,dim)){
                auto res = variable.m_dimensions.insert(std::make_pair(dim._name,dim));
                if(!res.second)
                    std::cout<<"[Warning] BodyDimDB : second definition of "<<variable._name<<" : "<<dim._name<<" ignored"<<std::endl;
            }
            dimensionElem = dimensionElem->NextSiblingElement("dimension");
        }
        return true;
    }
    return false;
}
bool DataBaseReader::parseRange(tinyxml2::XMLElement* rangeElem,  BodyDimDB::Variable& variable){
    if (rangeElem != nullptr ) {
        variable._range._min = std::stod(rangeElem->Attribute("min")); //TODOThomasD :verifier locale pour les histoire de separateur de decimale virgules ou point
        variable._range._max = std::stod(rangeElem->Attribute("max"));
        return true;
    }
    return false;
}
bool DataBaseReader::parseDimension(tinyxml2::XMLElement* dimensionElem,  BodyDimDB::Dimension& dim){
    if(dimensionElem != nullptr){
        dim._name = dimensionElem->Attribute("name");
		dim._unit = dimensionElem->Attribute("unit");
        dim._nbcoeff = std::stoi(dimensionElem->Attribute("nbCoeff"));
        tinyxml2::XMLElement* coeffElem = dimensionElem->FirstChildElement("coeff");

        while(coeffElem != nullptr){
            dim._coeffVec.push_back(BodyDimDB::Coeff());
            parseCoeff(coeffElem,dim._coeffVec.back());
            coeffElem = coeffElem->NextSiblingElement("coeff");
        }
        return true;
    }
    return false;
}

bool DataBaseReader::parseCoeff(tinyxml2::XMLElement* coeffElem,  BodyDimDB::Coeff& coeff){
    if(coeffElem != nullptr){
        coeff._order = std::stoi(coeffElem->Attribute("order"));
        coeff._value = std::stod(coeffElem->GetText());
        return true;
    }
    return false;
}

 double BodyDimDB::Dimension::solve(double variableValue){
     double result=0;
     for(auto coeffIt=_coeffVec.begin(); coeffIt!=_coeffVec.end(); ++coeffIt){
         if((*coeffIt)._order==0){
             result+=(*coeffIt)._value;
         }
         else if ((*coeffIt)._order==1){
             result+=(*coeffIt)._value * variableValue;
         }
         else{
             double tempVariableOrder=variableValue;
             for(unsigned int i=1; i<(*coeffIt)._order; ++i)
                 tempVariableOrder*=variableValue;
             result += (*coeffIt)._value * tempVariableOrder;
             }
     }
     return result;
 }

void BodyDimDB::Equation::computeDimensions(std::vector<std::pair<std::string,double > >& variablesValue, std::map<std::string,double>& m_dimensionResult){
    m_dimensionResult.clear();

    if(variablesValue.size() != _nbVariables)
        throw std::runtime_error("[BodyDimDB] : wrong number of provided variables.");
    
    for(auto itProvidedVariable = variablesValue.begin(); itProvidedVariable!= variablesValue.end() ; ++itProvidedVariable){
        if(m_variables.find((*itProvidedVariable).first)==m_variables.end())
            throw std::runtime_error("[BodyDimDB] : wrong variable name provided");

        BodyDimDB::Variable &variableEq = m_variables.at((*itProvidedVariable).first);

        if((*itProvidedVariable).second < _conv->toOutputUnit(variableEq._unit,variableEq._range._min)
            || (*itProvidedVariable).second > _conv->toOutputUnit(variableEq._unit,variableEq._range._max))
            throw std::runtime_error("[BodyDimDB] : value provided is out of bound");

        for(auto itDimension = variableEq.m_dimensions.begin() ; itDimension != variableEq.m_dimensions.end(); ++itDimension){
            double resultValue = (*itDimension).second.solve(_conv->fromOutputUnit(variableEq._unit,(*itProvidedVariable).second));
            resultValue = _conv->toOutputUnit((*itDimension).second._unit,resultValue);
            auto resultDimension = m_dimensionResult.insert(std::make_pair((*itDimension).first,resultValue));
            if(!resultDimension.second)
                m_dimensionResult.at((*itDimension).first)+=resultValue;
        }
    }

}

void BodyDimDB::Equation::computeDimensions(std::vector<std::pair<std::string,double > >& variablesValue){
    m_dimensionSourceResult.clear();
    m_dimensionTargetResult.clear();
    
    computeDimensions(variablesValue,m_dimensionTargetResult);
}

void BodyDimDB::Equation::computeDimensions(std::vector<std::pair<std::string,double > >& variablesSourceValue, std::vector<std::pair<std::string,double > >& variablesTargetValue){
    m_dimensionSourceResult.clear();
    m_dimensionTargetResult.clear();
    
    computeDimensions(variablesSourceValue,m_dimensionSourceResult);
    computeDimensions(variablesTargetValue,m_dimensionTargetResult);
}

BodyDimDB::BodyDimDB(){
    _conv.outputWeightUnit=std::string("kg");
    _conv.outputLengthUnit=std::string("mm");
	_conv.outputAgeUnit=std::string("month");
}

void BodyDimDB::clear(){
    m_subjects.clear();
}


double UnitConverter::toOutputUnit(std::string dbUnit, double data){
    return convertWithOutputUnit(dbUnit,data,true);
}

double UnitConverter::fromOutputUnit(std::string dbUnit, double data){
    return convertWithOutputUnit(dbUnit,data,false);
}

std::string UnitConverter::getCorrespondingOutputUnit(std::string dbUnit) {
    evaluateDbUnit(dbUnit);
    if (_unitType == unitType::length){
        return outputLengthUnit;
    }
    else if (_unitType == unitType::weight){
        return outputWeightUnit;
    }
    else if (_unitType == unitType::age){
        return outputAgeUnit;
    }

    return "undefined";
}

void UnitConverter::evaluateDbUnit(std::string dbUnit){
    if (lastDbLengthUnit.compare(dbUnit) == 0)
        _unitType = unitType::length;
    else if (lastDbWeightUnit.compare(dbUnit) == 0)
        _unitType = unitType::weight;
    else if (lastDbAgeUnit.compare(dbUnit) == 0)
        _unitType = unitType::age;
    else
    {
        if (dbUnit.compare("in") == 0 || dbUnit.compare("inch") == 0){ //todo use tolower
            dbConvertionLengthUnit = 0.0254;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else if (dbUnit.compare("lb") == 0 || dbUnit.compare("pound") == 0 || dbUnit.compare("poundmass") == 0){
            dbConvertionWeightUnit = 0.45359237;
            lastDbWeightUnit = dbUnit;
            _unitType = unitType::weight;
        }
        else if (dbUnit.compare("month") == 0){
            dbConversionAgeUnit = 1;
            lastDbAgeUnit = dbUnit;
            _unitType = unitType::age;
        }
        else if (dbUnit.compare("year") == 0){
            dbConversionAgeUnit = 1 / 12;
            lastDbAgeUnit = dbUnit;
            _unitType = unitType::age;
        }
        else if (dbUnit.compare("ft") == 0 || dbUnit.compare("foot") == 0){
            dbConvertionLengthUnit = 0.3048;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else if (dbUnit.compare("mm") == 0 || dbUnit.compare("millimeter") == 0 || dbUnit.compare("millimetre") == 0){
            dbConvertionLengthUnit = 0.001;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else if (dbUnit.compare("m") == 0 || dbUnit.compare("meter") == 0 || dbUnit.compare("metre") == 0){
            dbConvertionLengthUnit = 1;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else if (dbUnit.compare("g") == 0 || dbUnit.compare("gram") == 0){
            dbConvertionWeightUnit = 0.001;
            lastDbWeightUnit = dbUnit;
            _unitType = unitType::weight;
        }
        else if (dbUnit.compare("kg") == 0 || dbUnit.compare("kilogram") == 0){
            dbConvertionWeightUnit = 1;
            lastDbWeightUnit = dbUnit;
            _unitType = unitType::weight;
        }
        else if (dbUnit.compare("cm") == 0 || dbUnit.compare("centimeter") == 0 || dbUnit.compare("centimetre") == 0){
            dbConvertionLengthUnit = 0.01;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else if (dbUnit.compare("dm") == 0 || dbUnit.compare("decimeter") == 0 || dbUnit.compare("decimetre") == 0){
            dbConvertionLengthUnit = 0.1;
            lastDbLengthUnit = dbUnit;
            _unitType = unitType::length;
        }
        else
        {
            throw std::runtime_error("[BodyDimensionDB] unitconversion toOutputUnit : wrong db unit name.");
        }
    }
}

double UnitConverter::convertWithOutputUnit(std::string dbUnit, double data, bool toOutput){
//	enum class unitType {length, weight, age} ;
//	unitType _unitType;

    evaluateDbUnit(dbUnit);

    if (_unitType==unitType::length){
        if(lastOutputLengthUnit.compare(outputLengthUnit)!=0){
            if (outputLengthUnit.compare("mm")==0 ||outputLengthUnit.compare("millimeter")==0 ||outputLengthUnit.compare("millimetre")==0 ){
                outputConversionLengthUnit=0.001;
                lastOutputLengthUnit = lastOutputLengthUnit;
            }
            else if(outputLengthUnit.compare("m")==0 ||outputLengthUnit.compare("meter")==0 ||outputLengthUnit.compare("metre")==0 ){
                outputConversionLengthUnit=1;
                lastOutputLengthUnit = lastOutputLengthUnit;
            }
            else if (outputLengthUnit.compare("cm")==0 ||outputLengthUnit.compare("centimeter")==0 ||outputLengthUnit.compare("centimetre")==0 ){
                outputConversionLengthUnit=0.01;
                lastOutputLengthUnit = lastOutputLengthUnit;
            }
            else if (outputLengthUnit.compare("dm")==0 ||outputLengthUnit.compare("decimeter")==0 ||outputLengthUnit.compare("decimetre")==0 ){
                outputConversionLengthUnit=0.1;
                lastOutputLengthUnit = lastOutputLengthUnit;
            }
            else if(outputLengthUnit.compare("in")==0||outputLengthUnit.compare("inch")==0){ //todo use tolower
                outputConversionLengthUnit = 0.0254;
                lastOutputLengthUnit=outputLengthUnit;
            }
            else if (outputLengthUnit.compare("ft")==0||outputLengthUnit.compare("foot")==0){
                outputConversionLengthUnit = 0.3048;
                lastOutputLengthUnit=outputLengthUnit;
            }
            else{
                throw std::runtime_error("[BodyDimensionDB] unitconversion toOutputUnit : wrong length unit name.");
                return 0;
            }
        }
    }
	else if (_unitType==unitType::weight){
        if(lastOutputWeightUnit.compare(outputWeightUnit)!=0){
            if (outputWeightUnit.compare("g")==0 ||outputWeightUnit.compare("gram")==0 ){
                outputConversionWeightUnit=0.001;
                lastOutputWeightUnit=outputWeightUnit;
            }
            else if(outputWeightUnit.compare("kg")==0 ||outputWeightUnit.compare("kilogram")==0 ){
                outputConversionWeightUnit=1;
                lastOutputWeightUnit=outputWeightUnit;
            }
            else if (outputWeightUnit.compare("lb")==0||outputWeightUnit.compare("pound")==0||outputWeightUnit.compare("poundmass")==0){
                outputConversionWeightUnit = 0.45359237;
                lastOutputWeightUnit=outputWeightUnit;
            }
            else{
                throw std::runtime_error("[BodyDimensionDB] unitconversion toOutputUnit : wrong weight unit name.");
                return 0;
            }
        }
	}
	else if (_unitType==unitType::age){
		if (outputAgeUnit.compare("month")==0){
			outputConversionAgeUnit = 1;
			lastOutputAgeUnit=dbUnit;
		}
		else if (outputAgeUnit.compare("year")==0){
			outputConversionAgeUnit = 1/12;
			lastOutputAgeUnit=dbUnit;
		}
		else{
            throw std::runtime_error("[BodyDimensionDB] unitconversion toOutputUnit : wrong age unit name.");
            return 0;
        }
	}
    

    if(toOutput){
        if(_unitType==unitType::length)
            return data * dbConvertionLengthUnit / outputConversionLengthUnit;
        else if(_unitType==unitType::weight)
            return data * dbConvertionWeightUnit / outputConversionWeightUnit;
		else if(_unitType==unitType::age)
			return data * dbConversionAgeUnit / outputConversionAgeUnit;
	}
    else{
        if(_unitType==unitType::length)
            return data * outputConversionLengthUnit / dbConvertionLengthUnit;
        else if(_unitType==unitType::weight)
            return data * outputConversionWeightUnit / dbConvertionWeightUnit;
		else if(_unitType==unitType::age)
			return data * outputConversionAgeUnit / dbConversionAgeUnit;
	}

    return data; // if unit type is not defined, make no conversions
}

/*
BodyDimDB::Subject::Subject(){
}

BodyDimDB::Equation::Equation(){
}*/

}
}



