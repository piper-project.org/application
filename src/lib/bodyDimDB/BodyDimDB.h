/*******************************************************************************
* Copyright (C) 2017 UCBL                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Dupeux (UCBL)                                    *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef BODY_DIMENSION_DATA_BASE
#define BODY_DIMENSION_DATA_BASE

#include <tinyxml/tinyxml2.h>
#include <sstream>
#include <string>
#include <map>
#include <vector>

namespace piper {
namespace BodyDimDB {

/**
 * \todo let's use piper::units lib
 */
class UnitConverter {
    public:
       std::string outputWeightUnit;
       std::string outputLengthUnit;
	   std::string outputAgeUnit;

      double toOutputUnit(std::string dbUnit, double data);
      double fromOutputUnit(std::string dbUnit, double data);
      std::string getCorrespondingOutputUnit(std::string dbUnit);

    protected: 
         double dbConvertionWeightUnit; // from dbUnit to KG
         double outputConversionWeightUnit; //from outputUnit to KG
		 double dbConversionAgeUnit; //from dbUnit to month
		 double outputConversionAgeUnit; //from outputUnit to month
         double dbConvertionLengthUnit; // from dbUnit to KG
         double outputConversionLengthUnit; //from outputUnit to KG
         std::string lastOutputWeightUnit;
         std::string lastOutputLengthUnit;
         std::string lastDbWeightUnit;
         std::string lastDbLengthUnit;
		 std::string lastDbAgeUnit;
		 std::string lastOutputAgeUnit;

    private :
        double convertWithOutputUnit(std::string dbUnit, double data, bool toOutput);
        void evaluateDbUnit(std::string dbUnit);

        enum class unitType { length, weight, age };
        unitType _unitType;
    };

class BodyDimDB{
public :
    typedef struct {
        unsigned int _order;
        double _value;
    } Coeff;

   class Dimension {
   public:
        Dimension(){};

        double solve(double variableValue);

        std::string _name;
        unsigned int _nbcoeff;
		std::string _unit;
        std::vector<Coeff> _coeffVec;
    };

   typedef struct {
        double _min;
        double _max;
    } Range;

   typedef struct {
        unsigned int _id;
        std::string _name;
        std::string _unit;
        Range _range;
        std::map<std::string, Dimension> m_dimensions;
    } Variable;

    class Equation {
    public:
        Equation(UnitConverter* conv):_conv(conv){}

        std::string _name;
        unsigned int _nbVariables;
        std::map<std::string, Variable> m_variables;

        void computeDimensions(std::vector<std::pair<std::string,double > >& variablesValue);
        void computeDimensions(std::vector<std::pair<std::string,double > >& variablesSourceValue, std::vector<std::pair<std::string,double > >& variablesTargetValue);


        std::map<std::string, double> m_dimensionSourceResult;
        std::map<std::string, double> m_dimensionTargetResult;

    private :

        void computeDimensions(std::vector<std::pair<std::string,double > >& variablesValue, std::map<std::string,double>& m_dimensionResult);
        UnitConverter* _conv;
        //TODOThomasD : add method that compute the result based on variable name + value
    };

    class Subject{
    public:
        Subject(UnitConverter* conv):_conv(conv){}

        std::map<std::string, Equation> m_equations;


        std::string _type;

        UnitConverter* _conv;
    };

public:
    BodyDimDB();
    void clear();

    std::map<std::string,Subject> m_subjects;

    UnitConverter _conv;

    
};



class DataBaseReader{

public :
    DataBaseReader():_isDBLoaded(false){}

	bool isDbLoaded();

    bool read(std::string filename, BodyDimDB& db);

private:

    bool parseSubject(tinyxml2::XMLElement* subjectElem,  BodyDimDB::Subject& subject);
    bool parseEquation(tinyxml2::XMLElement* equationElem,  BodyDimDB::Equation& equation);
    bool parseVariable(tinyxml2::XMLElement* variableElem,  BodyDimDB::Variable& variable);
    bool parseRange(tinyxml2::XMLElement* rangeElem,  BodyDimDB::Variable& variable);
    bool parseDimension(tinyxml2::XMLElement* dimensionElem,  BodyDimDB::Dimension& dim);
    bool parseCoeff(tinyxml2::XMLElement* coeffElem,  BodyDimDB::Coeff& coeff);
    
	bool _isDBLoaded;
    std::string dbFileName;
};

}
}

#endif
