/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_COMPONENTSEGMENT_H
#define PIPER_COMPONENTSEGMENT_H

#ifdef WIN32
#	ifdef AnthropoModel_EXPORTS
#		define ANTHROPOMODEL_EXPORT __declspec( dllexport )
#	else
#		define ANTHROPOMODEL_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANTHROPOMODEL_EXPORT
#endif

#include "AnthropoComponent.h"

namespace piper {

    namespace hbm {
        class HumanBodyModel;
    }
    namespace anthropometricmodel {

        /**
        * @brief The AnthropoBodySegment class stores the description of a body segment to define an anthropometric length.
        *
        * @author Erwan Jolivet @date 2016
        */
        class ANTHROPOMODEL_EXPORT AnthropoBodySegment : public AbstractAnthropoComponentNode {

        public:
            /// @return a string representation of the target type
            AnthropoComponentType type() const { return AnthropoComponentType::SEGMENT; }

            AnthropoBodySegment();

            /*!
            *  \brief write the AnthropoBodySegment in xml format
            *
            */
            virtual void serializeXml(tinyxml2::XMLElement* element) const;
            /*!
            *  \brief write the AnthropoBodySegment in xml format
            *
            */
            void parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);
            /*!
            *  \brief check if metadata that support body segment definition are defined
            *
            * \param model: a human body model
            */
            virtual void checkmetadata(piper::hbm::HumanBodyModel const * const model);
            /*!
            *  \brief get list of landmark names
            *
            * \return: vector of set of landmark names
            */
            std::vector<std::set<std::string>> const& getLandmarks() const;
            /*!
            *  \brief add a landmark in a via point definition
            *
            * \param n: number of the via point
            * \param landname: name of the landmark
            */
            void addLandmark(size_t n, std::string const& landname);
            /*!
            *  \brief remove a landmark in a via point definition
            *
            * \param n: number of the via point
            * \param landname: name of the landmark
            */
            void removeLandmark(size_t n, std::string const& landname);
            /*!
            *  \brief insert new empty viapoint 
            *
            * \param n: place of the new via point
            */
            void insertViapoint(size_t n);
            /*!
            *  \brief remove viapoint
            *
            * \param n: place of the new via point to remove
            */
            void removeViapoint(size_t n);
            /*!
            *  \brief get the value of the body section for a AnthropoDimensionType
            *
            * \param hbm: pointer to an hbm
            * \param type: type of dimension
            * \return value: value of the dimension
            */
            virtual double getValue(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type);
            virtual double getValueTarget(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type);
            /*!
            *  \brief check if the component is defined (with minimal description available)
            *
            * \return: true is component is defined
            */
            virtual bool isDefined() const;
            /*!
            *  \brief define a frame on the segment at normalized position
            *
            * \param model: a human body model
            * \param position: normalized position between 0.0 and 1.0
            * \return: local frame at the requested position
            */
            AnthropoFrame getPointatPosition(piper::hbm::HumanBodyModel *const model, double const& position);
            /*!
            *  \brief set control points are defined at viapoint location
            *
            * \param truefalse: true to define control points at viapoint location
            */
            void setControlPointViaPoint(size_t n, bool const& truefalse);
            /*!
            *  \brief is control points  defined at viapoint location 
            *
            * \param n: index of viapoint
            * \return: true if control points is defined at viapoint location
            */
            bool getControlPointViaPoint(size_t n) const;
            /*!
            *  \brief set control points are defined at landmark location
            *
            * \param n: index of viapoint
            * \param truefalse: true to define control points at landmark location
            */
            void setControlPointtLandmark(size_t n, bool const& truefalse);
            /*!
            *  \brief is control points  defined at landmark location
            *
            * \param n: index of viapoint
            * \return: true if control points is defined at landmark location
            */
            bool getControlPointtLandmark(size_t n) const;

            virtual bool checkComponentDimensions() const;
            virtual bool setLandmarksTarget(std::map<std::string, Landmarkconstraint> const& landtargets, std::set<std::string>& used);

        private:
            typedef std::set<std::string> listlandmark;
            std::vector<listlandmark> m_viapoint;
            std::vector<bool> m_cp_viapoint, m_cp_landmark;
            virtual void doCompute(piper::hbm::HumanBodyModel *const model);
            void doComputeTarget(piper::hbm::HumanBodyModel *const model);
            bool doComputeScaleValue(piper::hbm::HumanBodyModel *const model, Eigen::Vector3d & values);

            static void getPointatPosition(Eigen::Matrix3Xd& segmentcoord, double const& position, AnthropoFrame& frame);

            hbm::Coord getCenterFromParentIntersection(AbstractAnthropoComponentNode* parent, piper::hbm::HumanBodyModel *const model) const;
        };


    }
}

#endif
