/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoModel.h"

#include "version.h"
#include "hbm/HumanBodyModel.h"
#include "anatomyDB/query.h"

using namespace piper::hbm;

namespace piper
{
    namespace anthropometricmodel
    {
        AnthropoModel::AnthropoModel() 
            : m_hbm(nullptr)
            , m_lastSourceMTime(-1)
            , m_lastTargetMTime(-1)
        {
            m_krigingInterface = std::make_shared<IntermediateTargetsInterface>();
        }

        AnthropoModel::AnthropoModel(piper::hbm::HumanBodyModel* hbmref, std::shared_ptr<IntermediateTargetsInterface> krigInterface)
            : m_hbm(hbmref)
            , m_krigingInterface(krigInterface)
            , m_targetHeight(0.0)
            , m_controlpointset("")
            , m_lastSourceMTime(-1)
            , m_lastTargetMTime(-1)
        {
            m_krigingInterface->setRefHBM(m_hbm);
        }

        void AnthropoModel::setModel(piper::hbm::HumanBodyModel* hbmref) {
            m_hbm = hbmref;
            m_krigingInterface->setRefHBM(hbmref);
            resetModel();
        }

        void AnthropoModel::resetModel() {
            for (auto& comp : m_components)
                comp.second->resetComponent();
            m_krigingInterface->sourcesReset();
            freeland_source.clear();
            freeland_target.clear();
            m_lastSourceMTime = -1;
        }


        void AnthropoModel::serialize(tinyxml2::XMLElement* element) const {
            tinyxml2::XMLElement* xmlAnthropoModel = element->GetDocument()->NewElement("anthropometricModel");
            element->InsertEndChild(xmlAnthropoModel);

            for (auto const& component : m_components) {
                if (component.second->type() == AnthropoComponentType::SEGMENT)
                    AnthropoComponentSerializeXml(component.second, xmlAnthropoModel);
            }
            for (auto const& component : m_components) {
                if (component.second->type() == AnthropoComponentType::SECTION)
                    AnthropoComponentSerializeXml(component.second, xmlAnthropoModel);
            }
            for (auto const& component : m_bodydimension)
                AnthropoComponentSerializeXml(component.second, xmlAnthropoModel);

            if (m_roots.size() > 0) {
                tinyxml2::XMLElement* xmlTree = element->GetDocument()->NewElement("anthropoTree");
                xmlAnthropoModel->InsertEndChild(xmlTree);
                for (auto const& root : m_roots) {
                    m_components.at(root)->serializeXmlNode(xmlTree);
                }
            }
        }

        void AnthropoModel::parseXml(tinyxml2::XMLElement* parentElement, AnthropoModel& anthropomodel) {
            clearAnthropoModel();
            if (parentElement->FirstChildElement("anthropometricModel") == nullptr)
                return;

            const tinyxml2::XMLElement* xmlAnthropoComponent = parentElement->FirstChildElement("anthropometricModel")->FirstChildElement("anthropoComponent");
            while (xmlAnthropoComponent != nullptr) {
                std::string type = xmlAnthropoComponent->Attribute("type");
                std::string name = xmlAnthropoComponent->FirstChildElement("name")->GetText();
                std::shared_ptr<AbstractAnthropoComponent> component = addAnthropoComponent(type, name);
                component->parseXml(xmlAnthropoComponent, anthropomodel);
                xmlAnthropoComponent = xmlAnthropoComponent->NextSiblingElement("anthropoComponent");
            }

            if (parentElement->FirstChildElement("anthropometricModel")->FirstChildElement("anthropoTree") != nullptr) {
                const tinyxml2::XMLElement* xmlNode = parentElement->FirstChildElement("anthropometricModel")->FirstChildElement("anthropoTree")->FirstChildElement("node");
                while (xmlNode != nullptr) {
                    std::string compname = xmlNode->FirstChildElement("name")->GetText();
                    m_roots.insert(compname);
                    anthropomodel.getAnthropoComponent(xmlNode->FirstChildElement("name")->GetText())->parseXmlNode(xmlNode, anthropomodel);
                    xmlNode = xmlNode->NextSiblingElement("node");
                }
            }

            std::stringstream ss;
            ss << "Error while importing Scalable Model file: " << std::endl;
            bool spyerror = false;

            // check if body dimensions refer to existing anthropo component 
            if (m_bodydimension.size() > 0) {
                std::map<std::string, std::vector<std::string>> missingcomp;
                std::map<std::string, std::vector<std::string>> wrongtype;
                std::map<std::string, std::string> missingrefcomp;
                for (auto const& cur : m_bodydimension) {
                    std::map<AbstractAnthropoComponentNode*, double> const& comp = cur.second->getAnthropoComponent();
                    std::string compref = cur.second->getReferenceAnthropoComponent();
                    AnthropoDimensionType type = cur.second->getType();
            if (!compref.empty() && m_bodysection.find(compref) == m_bodysection.end() &&
                        m_bodysegment.find(compref) == m_bodysegment.end())
                        missingrefcomp[cur.first] = compref;
            if (!compref.empty() && (type == AnthropoDimensionType::SEGMENT_LENGTH || type == AnthropoDimensionType::SEGMENT_MULTILENGTH)
                        && m_bodysection.find(compref) != m_bodysection.end())
                        wrongtype[cur.first].push_back(compref);
            else if (!compref.empty() && (type != AnthropoDimensionType::SEGMENT_LENGTH && type != AnthropoDimensionType::SEGMENT_MULTILENGTH)
                        && m_bodysegment.find(compref) != m_bodysegment.end())
                        wrongtype[cur.first].push_back(compref);
                    if (comp.size() > 0) {
                        for (auto const& curcomp : comp) {
                            if (m_bodysection.find(curcomp.first->name()) == m_bodysection.end() &&
                                m_bodysegment.find(curcomp.first->name()) == m_bodysegment.end())
                                missingcomp[cur.first].push_back(curcomp.first->name());
                    if (!compref.empty() &&  (type == AnthropoDimensionType::SEGMENT_LENGTH || type == AnthropoDimensionType::SEGMENT_MULTILENGTH) &&
                                m_bodysection.find(compref) != m_bodysection.end())
                                wrongtype[cur.first].push_back(curcomp.first->name());
                    else if (!compref.empty() &&  (type != AnthropoDimensionType::SEGMENT_LENGTH && type != AnthropoDimensionType::SEGMENT_MULTILENGTH) &&
                                m_bodysegment.find(compref) != m_bodysegment.end())
                                wrongtype[cur.first].push_back(curcomp.first->name());
                        }
                    }
                }
                if (wrongtype.size() > 0) {
                    spyerror = true;
                    for (auto const& cur : wrongtype) {
                        for (auto const& curcomp : cur.second)
                            ss << "Type of Anthropometric component " << curcomp << "is not compatible with type of " << cur.first << " body dimensions." << std::endl;
                    }
                }
                if (missingrefcomp.size() > 0) {
                    spyerror = true;
                    for (auto const& cur : missingrefcomp) {
                        ss << "Anthropometric component " << cur.second << "for body dimension " << cur.first << " is not defined" << std::endl;
                    }
                }
                if (missingcomp.size() > 0) {
                    spyerror = true;
                    for (auto const& cur : missingcomp) {
                        for (auto const& curcomp : cur.second)
                            ss << "Anthropometric component " << curcomp << "for body dimension " << cur.first << " is not defined" << std::endl;
                    }
                }
            }
            if (spyerror) {
                clearAnthropoModel();
                throw std::runtime_error(ss.str());
            }
        }

        void AnthropoModel::write(std::string const& file) {
            // build tinyxml document
            tinyxml2::XMLDocument xmlDocument;

            xmlDocument.InsertEndChild(xmlDocument.NewDeclaration());
            //    model.InsertEndChild(model.NewUnknown("DOCTYPE piper SYSTEM \"target.dtd\""));

            tinyxml2::XMLElement* xmlTarget = xmlDocument.NewElement("piper-anthropometricModel");
            std::ostringstream version;
            version << PIPER_VERSION_MAJOR << "." << PIPER_VERSION_MINOR << "." << PIPER_VERSION_PATCH;
            xmlTarget->SetAttribute("version", version.str().c_str());
            xmlDocument.InsertEndChild(xmlTarget);

            serialize(xmlDocument.RootElement());

            // write tinyxml document
            xmlDocument.SaveFile(file.c_str());
            if (xmlDocument.Error()) {
                std::stringstream ss;
                ss << "Error while saving Scalable Model file: " << file << std::endl << xmlDocument.ErrorStr() << std::endl;
                throw std::runtime_error(ss.str());
            }
        }

        void AnthropoModel::read(std::string const& file) {
            clearAnthropoModel();
            tinyxml2::XMLDocument model;
            model.LoadFile(file.c_str());
            if (model.Error()) {
                std::stringstream str;
                str << "Failed to load Scalable Model file: " << file << std::endl << model.ErrorStr() << std::endl;
                throw std::runtime_error(str.str().c_str());
            }
            parseXml(model.RootElement(), *this);
            check();
        }

        void AnthropoModel::check()
        {
            for (auto & cur : m_components) {
                if (!cur.second->checkComponentDimensions()) {
                    std::stringstream str;
                    str << "[FAILED]  Anthropometric component " << cur.second->name() << " is over constrained." << std::endl;
                    clearAnthropoModel();
                    throw std::runtime_error(str.str().c_str());
                }
            }
            if (m_hbm != nullptr) 
            {
                try
                {
                    // for each section & segment, check metadata, check also component overconstrained
                    for (auto & cur : m_components) {
                        cur.second->checkmetadata(m_hbm);
                    }
                    for (auto & cur : m_bodydimension)
                        cur.second->checkmetadata(m_hbm);
                }
                catch (std::runtime_error e)
                {
                    clearAnthropoModel();
                    throw e;
                }
            }
        }

        std::shared_ptr<AbstractAnthropoComponent> AnthropoModel::addAnthropoComponent(std::string const& type, std::string const& name) {
            AnthropoComponentType comptype = AbstractAnthropoComponent::AnthropoComponentTypefromString(type);
            if (comptype == AnthropoComponentType::SEGMENT) {
                m_components.insert(std::pair<std::string, std::shared_ptr<AnthropoBodySegment>>(name, std::shared_ptr<AnthropoBodySegment>(new AnthropoBodySegment())));
                m_bodysegment.insert(name);
                return m_components[name];
            }
            else if (comptype == AnthropoComponentType::SECTION) {
                m_components.insert(std::pair<std::string, std::shared_ptr<AnthropoBodySection>>(name, std::shared_ptr<AnthropoBodySection>(new AnthropoBodySection())));
                m_bodysection.insert(name);
                return m_components[name];
            }
            else if (comptype == AnthropoComponentType::DIMENSION) {
                m_bodydimension.insert(std::pair<std::string, std::shared_ptr<AnthropoBodyDimension>>(name, std::shared_ptr<AnthropoBodyDimension>(new AnthropoBodyDimension())));
                return m_bodydimension[name];
            }
            else
                return nullptr;
        }

        void AnthropoModel::clearAnthropoModel() {
            m_bodysection.clear();
            m_bodydimension.clear();
            m_bodysegment.clear();
            m_components.clear();
            m_roots.clear();
            resetModel();
        }

        std::set<std::string> const& AnthropoModel::listAnthropoBodySegment() const {
            return m_bodysegment;
        }

        bool AnthropoModel::hasAnthropoComponent(std::string const& name) const {
            return (m_components.find(name) != m_components.end());
        }

        AbstractAnthropoComponentNode* AnthropoModel::getAnthropoComponent(std::string const& compname) {
            if (m_components.find(compname) != m_components.end())
                return m_components.at(compname).get();
            else
                throw std::runtime_error("Unknown body component: " + compname);
        }

        AbstractAnthropoComponentNode const*const AnthropoModel::getAnthropoComponent(std::string const& compname) const {
            if (m_components.find(compname) != m_components.end())
                return m_components.at(compname).get();
            else
                throw std::runtime_error("Unknown body component: " + compname);
        }

        bool AnthropoModel::hasAnthropoBodySegment(std::string const& name) const {
            return (m_bodysegment.find(name) != m_bodysegment.end());
        }

        AnthropoBodySegment* AnthropoModel::getAnthropoBodySegment(std::string const& name) {
            if (m_bodysegment.find(name) != m_bodysegment.end())
                return dynamic_cast<AnthropoBodySegment*>(m_components.at(name).get());
            else
                throw std::runtime_error("Unknown body segment: " + name);
        }

        AnthropoBodySegment const*const AnthropoModel::getAnthropoBodySegment(std::string const& name) const {
            if (m_bodysegment.find(name) != m_bodysegment.end())
                return dynamic_cast<AnthropoBodySegment const*>(m_components.at(name).get());
            else
                throw std::runtime_error("Unknown body segment: " + name);
        }


        std::set<std::string> const& AnthropoModel::listAnthropoBodySection() const {
            return m_bodysection;
        }

        bool AnthropoModel::hasAnthropoBodySection(std::string const& name) const {
            return (m_bodysection.find(name) != m_bodysection.end());
        }


        AnthropoBodySection* AnthropoModel::getAnthropoBodySection(std::string const& name) {
            if (m_bodysection.find(name) != m_bodysection.end())
                return dynamic_cast<AnthropoBodySection*>(m_components.at(name).get());
            else
                throw std::runtime_error("Unknown body section: " + name);
        }


        AnthropoBodySection const*const AnthropoModel::getAnthropoBodySection(std::string const& name) const {
            if (m_bodysection.find(name) != m_bodysection.end())
                return dynamic_cast<AnthropoBodySection const*>(m_components.at(name).get());
            else
                throw std::runtime_error("Unknown body section: " + name);
        }


        std::set<std::string> AnthropoModel::listAnthropoBodyDimension() const {
            std::set<std::string> list;
            for (auto const& cur : m_bodydimension)
                list.insert(cur.first);
            return list;
        }
        bool AnthropoModel::hasAnthropoBodyDimension(std::string const& name) const {
            return (m_bodydimension.find(name) != m_bodydimension.end());
        }


        AnthropoBodyDimension* AnthropoModel::getAnthropoBodyDimension(std::string const& name) {
            if (m_bodydimension.find(name) != m_bodydimension.end())
                return m_bodydimension.at(name).get();
            else
                throw std::runtime_error("Unknown body section: " + name);
        }


        AnthropoBodyDimension const*const AnthropoModel::getAnthropoBodyDimension(std::string const& name) const {
            if (m_bodydimension.find(name) != m_bodydimension.end())
                return m_bodydimension.at(name).get();
            else
                throw std::runtime_error("Unknown body section: " + name);
        }

        bool AnthropoModel::isSegmentDimensionType(AnthropoDimensionType const& type) {
            std::string type_str = AnthropoDimensionType_str[(unsigned int)type];
            if (type_str.substr(0, 7) == "SECTION")
                return false;
            return true;
        }

        void AnthropoModel::setTargetHeight(double const& value) {
            m_targetHeight = value;
        }

        double const& AnthropoModel::getTargetHeight() const {
            return m_targetHeight;
        }

        void AnthropoModel::newAnthropoComponent(AnthropoComponentType const& comptype, std::string const& compname) {
            switch (comptype) {
            case AnthropoComponentType::SEGMENT: {
                m_components[compname] = std::shared_ptr<AnthropoBodySegment>(new AnthropoBodySegment());
                m_components[compname]->setName(compname);
                m_bodysegment.insert(compname);
                m_roots.insert(compname);
                break;
            }
            case AnthropoComponentType::SECTION: {
                m_components[compname] = std::shared_ptr<AnthropoBodySection>(new AnthropoBodySection());
                m_components[compname]->setName(compname);
                m_bodysection.insert(compname);
                m_roots.insert(compname);
                break;
            }
            default: { //AnthropoComponentType::DIMENSION
                m_bodydimension[compname] = std::shared_ptr<AnthropoBodyDimension>(new AnthropoBodyDimension());
                m_bodydimension[compname]->setName(compname);
                break;
            }
            }
        }

        void AnthropoModel::deleteAnthropoComponent(AnthropoComponentType const& comptype, std::string const& compname, bool const& children) {
            if (comptype != AnthropoComponentType::DIMENSION) {
                if (m_bodysegment.find(compname) != m_bodysegment.end() ||
                    m_bodysection.find(compname) != m_bodysection.end()) 
                {
                    for (auto & cur : m_bodydimension) {
                        cur.second->removeAnthropoComponent(compname);
                        if (cur.second->getReferenceAnthropoComponent() == compname)
                            cur.second->removeReferenceAnthropoComponent();
                    }
                    // delete children if required
                    if (children) {
                        for (auto & child : m_components[compname]->getChildren()) {
                            deleteAnthropoComponent(child->type(), child->name(), true);
                        }
                    }
                    else {
                        for (auto & child : m_components[compname]->getChildren()) {
                            child->removeParent();
                            m_roots.insert(child->name());
                        }
                    }
                    m_components[compname]->removeParent();
                    if (m_roots.find(compname) != m_roots.end())
                        m_roots.erase(compname);
                    m_components.erase(compname);
                    if (comptype == AnthropoComponentType::SEGMENT)
                        m_bodysegment.erase(compname);
                    else
                        m_bodysection.erase(compname);
                }
            }
            else if (m_bodydimension.find(compname) != m_bodydimension.end()) //AnthropoComponentType::DIMENSION
                m_bodydimension.erase(compname);
        }

        void AnthropoModel::renameAnthropoComponent(AnthropoComponentType const& comptype, std::string const& oldname, std::string const& newname) {
            if (comptype != AnthropoComponentType::DIMENSION) {
                if (m_components.find(newname) != m_components.end())
                    return;
                if (m_components.find(oldname) != m_components.end()) {
                    std::swap(m_components[newname], m_components.find(oldname)->second);
                    m_components[newname]->setName(newname);
                    m_components.erase(oldname);
                }
                else return;
                for (auto const& cur : m_roots)
                if (m_roots.find(oldname) != m_roots.end()) {
                    m_roots.erase(oldname);
                    m_roots.insert(newname);
                }
            }

            switch (comptype) {
            case AnthropoComponentType::SEGMENT: {
                m_bodysegment.erase(oldname);
                m_bodysegment.insert(newname);
                break;
            }
            case AnthropoComponentType::SECTION: {
                m_bodysection.erase(oldname);
                m_bodysection.insert(newname);
                break;
            }
            default: { //AnthropoComponentType::DIMENSION
                if (m_bodydimension.find(newname) != m_bodydimension.end())
                    break;
                if (m_bodydimension.find(oldname) == m_bodydimension.end())
                    break;
                else {
                    std::swap(m_bodydimension[newname], m_bodydimension.find(oldname)->second);
                    m_bodydimension[newname]->setName(newname);
                    //m_bodydimension[newname] = m_bodydimension[oldname];
                    m_bodydimension.erase(oldname);
                }
                break;
            }

            }
        }

        void AnthropoModel::setScalingRelative(bool const& truefalse) {
            for (auto & dim : m_bodydimension) {
                dim.second.get()->setScalingRelative(truefalse);
            }
        }

        void AnthropoModel::getControlPoints(piper::hbm::VCoord& controlPointsSource, piper::hbm::VCoord& controlPointsTarget, unsigned int allowedCompTypes) {
            controlPointsSource.clear();
            controlPointsTarget.clear();

            for (auto const& curdim : m_bodydimension) {
                std::map<AbstractAnthropoComponentNode*, double> const& components = curdim.second->getAnthropoComponent();
                for (auto const& curcomp : components) {
                    if (curcomp.first->isDefined() && ((curcomp.first->type() & allowedCompTypes) != 0))
                    {
                        Eigen::Matrix3Xd const& cursource = curcomp.first->getComponentControlPoints(m_hbm);
                        for (int n = 0; n < cursource.cols(); n++)
                            controlPointsSource.push_back(hbm::Coord(cursource.col(n)(0), cursource.col(n)(1), cursource.col(n)(2)));
                        Eigen::Matrix3Xd const& curtarget = curcomp.first->getComponentControlPointsTarget(m_hbm);
                        for (int n = 0; n < curtarget.cols(); n++)
                            controlPointsTarget.push_back(hbm::Coord(curtarget.col(n)(0), curtarget.col(n)(1), curtarget.col(n)(2)));
                    }
                }
            }
        }

        InteractionControlPoint const& AnthropoModel::getControlPointsSource()
        {
            return m_controlPointsSource;
        }

        InteractionControlPoint const& AnthropoModel::getControlPointsTarget()
        {
            return m_controlPointsTarget;
        }

        void AnthropoModel::gatherTargetControlPoints(unsigned int allowedCompTypes)
        {
            m_controlPointsTarget.clear(m_hbm->fem());
            m_controlPointsTarget.setControlPointRole(InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET);
            std::vector<double> &as_bones = m_controlPointsTarget.getAssociationBones();
            std::vector<double> &as_skin = m_controlPointsTarget.getAssociationSkin();
            std::vector<double> &nuggets = m_controlPointsTarget.getWeight();
            VCoord &cpsTarget = m_controlPointsTarget.getControlPt3dCont();

            for (auto const& curdim : m_bodydimension) {
                std::map<AbstractAnthropoComponentNode*, double> const& components = curdim.second->getAnthropoComponent();
                for (auto const& curcomp : components) {
                    if (curcomp.first->isDefined() && ((curcomp.first->type() & allowedCompTypes) != 0))
                    {
                        Eigen::Matrix3Xd const& cursource = curcomp.first->getComponentControlPoints(m_hbm);
                        Eigen::Matrix3Xd const& curtarget = curcomp.first->getComponentControlPointsTarget(m_hbm);
                        if (cursource.cols() == curtarget.cols())
                        {
                            Eigen::Matrix3Xd const& curtarget = curcomp.first->getComponentControlPointsTarget(m_hbm);
                            for (int n = 0; n < curtarget.cols(); n++)
                                cpsTarget.push_back(hbm::Coord(curtarget.col(n)(0), curtarget.col(n)(1), curtarget.col(n)(2)));

                            // append bone/skin association
                            double boneness = curcomp.first->getControlPointParameter(AnthropoComponentParam::USE_BONE);
                            double skinness = curcomp.first->getControlPointParameter(AnthropoComponentParam::USE_SKIN);
                            as_bones.insert(as_bones.end(), curtarget.cols(), boneness);
                            as_skin.insert(as_skin.end(), curtarget.cols(), skinness);

                            // append the global nugget as nugget of the component - TODO, allow setting nugget to each component through GUI
                            nuggets.insert(nuggets.end(), curtarget.cols(), m_globalNugget);
                        }
                    }
                }
            }

            // gather landmark-based control points
            auto sourceIt = freeland_source.begin();
            for (auto const& l : freeland_target)
            {
                as_bones.push_back(l.second.use_bone);
                as_skin.push_back(l.second.use_skin);
                cpsTarget.push_back(l.second.coord);
                sourceIt++;
            }
        }

        void AnthropoModel::gatherControlPoints(unsigned int allowedCompTypes)
        {
            m_controlPointsSource.clear(m_hbm->fem());
            m_controlPointsTarget.clear(m_hbm->fem());
            m_controlPointsSource.setControlPointRole(InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE);
            m_controlPointsTarget.setControlPointRole(InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET);
            std::vector<double> &as_bones = m_controlPointsSource.getAssociationBones();
            std::vector<double> &as_skin = m_controlPointsSource.getAssociationSkin();
            std::vector<double> &nuggets = m_controlPointsSource.getWeight();
            VCoord &cpsSource = m_controlPointsSource.getControlPt3dCont();
            VCoord &cpsTarget = m_controlPointsTarget.getControlPt3dCont();

            for (auto const& curdim : m_bodydimension) {
                std::map<AbstractAnthropoComponentNode*, double> const& components = curdim.second->getAnthropoComponent();
                for (auto const& curcomp : components) {
                    if (curcomp.first->isDefined() && ((curcomp.first->type() & allowedCompTypes) != 0))
                    {
                        Eigen::Matrix3Xd const& cursource = curcomp.first->getComponentControlPoints(m_hbm);
                        Eigen::Matrix3Xd const& curtarget = curcomp.first->getComponentControlPointsTarget(m_hbm);
                        if (cursource.cols() == curtarget.cols())
                        {                           
                            // append coordinates
                            for (int n = 0; n < cursource.cols(); n++)
                                cpsSource.push_back(hbm::Coord(cursource.col(n)(0), cursource.col(n)(1), cursource.col(n)(2)));

                            for (int n = 0; n < curtarget.cols(); n++)
                                cpsTarget.push_back(hbm::Coord(curtarget.col(n)(0), curtarget.col(n)(1), curtarget.col(n)(2)));

                            // append bone/skin association
                            double boneness = curcomp.first->getControlPointParameter(AnthropoComponentParam::USE_BONE);
                            double skinness = curcomp.first->getControlPointParameter(AnthropoComponentParam::USE_SKIN);
                            as_bones.insert(as_bones.end(), cursource.cols(), boneness);
                            as_skin.insert(as_skin.end(), cursource.cols(), skinness);

                            // append the global nugget as nugget of the component - TODO, allow setting nugget to each component through GUI
                            nuggets.insert(nuggets.end(), cursource.cols(), m_globalNugget);
                        }
                    }
                }
            }

            // gather landmark-based control points
            auto sourceIt = freeland_source.begin();
            for (auto const& l : freeland_target)
            {
                cpsSource.push_back(sourceIt->second);
                as_bones.push_back(l.second.use_bone);
                as_skin.push_back(l.second.use_skin);
                cpsTarget.push_back(l.second.coord);
                sourceIt++;
            }

            m_controlPointsTarget.setAssociation(as_bones, as_skin);
            m_controlPointsTarget.setWeights(nuggets);
        }

        std::list<std::string> AnthropoModel::setLandmarksTarget(std::list<hbm::LandmarkTarget> const& landtargets) {
            std::list<std::string> missingland;
            // replace landname by metadata landname
            std::map<std::string, Landmarkconstraint> landtarget;
            for (auto const& cur : landtargets) {
                hbm::LandmarkTarget::ValueType coordval = cur.value();
                if (coordval.size() != 3) {
                    std::stringstream str;
                    str << "[FAILED] Set target landmarks for segment: " << std::endl;
                    str << "Target coordinates for landmark " << cur.landmark << " is incomplete." << endl;
                    throw std::runtime_error(str.str().c_str());
                }
                Coord coord(coordval[0], coordval[1], coordval[2]);

                std::vector<std::string> syn;
                if (anatomydb::isLandmark(cur.landmark))
                    syn = anatomydb::getSynonymList(cur.landmark);
                else
                    syn.push_back(cur.landmark);
                 bool spy=false;
                for (auto const& cursyn : syn) {
                    if (m_hbm->metadata().hasLandmark(cursyn)) {
                Landmarkconstraint landconstraint(cursyn, coord, cur.getKrigingUseBone(), cur.getKrigingUseSkin());
                landtarget[cursyn] = landconstraint;
                spy = true;
                        break;
                    }
                }
                if (!spy)
                    missingland.push_back(cur.landmark);
            }
            std::set<std::string> used_landmarks;
            for (auto & comp : m_components) {
                if (!comp.second->setLandmarksTarget(landtarget, used_landmarks)) {
                    std::stringstream str;
                    str << "[FAILED] Set target landmarks for segment: " << std::endl;
                    str << "Missing landmarks target to fully describe the segment " << comp.first << "." << endl;
                    throw std::runtime_error(str.str().c_str());
                }
            }
            for (auto const& cur : used_landmarks) {
                m_segment_landmarks[cur] = landtarget[cur];
            }

            // store unsed landmarks target and source position in anthropomodel to be able to display them and to pass them for defomration
            freeland_source.clear();
            freeland_target.clear();
            for (auto const& cur : landtarget) {
                if (used_landmarks.find(cur.first) == used_landmarks.end() && m_hbm->metadata().hasLandmark(cur.first)) { //it is a unsued landmarks
                    freeland_target[cur.first] = cur.second;
                    freeland_source[cur.first] = m_hbm->metadata().landmark(cur.first).position(m_hbm->fem());
                }
            }
            m_lastSourceMTime = -1; // invalidate control points
            return missingland;
        }


        std::list<std::string> AnthropoModel::listComponents() const {
            std::list<std::string> list;
            for (auto const& cur : m_components)
                list.push_back(cur.first);
            return list;
        }

        std::set<std::string> AnthropoModel::getRootComponentName() const {
            m_roots.clear();
            for (auto const & cur : m_components) {
                if (!cur.second->hasParent())
                    m_roots.insert(cur.second->name());
            }
            return m_roots;
        }

        void AnthropoModel::setGlobalNugget(double nugget)
        {
            m_globalNugget = nugget;
        }

        void AnthropoModel::updateControlPoints()
        {
            clock_t latestUpdateSource = -1;
            clock_t latestUpdateTarget = -1;
            for (auto const& curdim : m_bodydimension) {
                std::map<AbstractAnthropoComponentNode*, double> const& components = curdim.second->getAnthropoComponent();
                for (auto const& curcomp : components) {
                    curcomp.first->getComponentControlPointsTarget(m_hbm); // re-computes the source and target if it has not been computed yet
                    if (curcomp.first->getSourceMTime() > latestUpdateSource)
                        latestUpdateSource = curcomp.first->getSourceMTime();
                    if (curcomp.first->getTargetMTime() > latestUpdateTarget)
                        latestUpdateTarget = curcomp.first->getTargetMTime();
                }
            }
            if (!m_krigingInterface->getSourcesAreLoaded() || latestUpdateSource > m_lastSourceMTime) // source has changed, has to update everything
            {
                gatherControlPoints();
                m_krigingInterface->setControlPoints(m_controlPointsSource, m_controlPointsTarget);
            }
            else if (latestUpdateTarget > m_lastTargetMTime)// only targets are invalid, set only those
            {
                gatherTargetControlPoints();
                m_krigingInterface->setTargetControlPoints(m_controlPointsTarget);
            }
            m_lastSourceMTime = latestUpdateSource;
            m_lastTargetMTime = latestUpdateTarget;
        }

        void AnthropoModel::setKrigingInterface(std::shared_ptr<IntermediateTargetsInterface> krigI)
        {
            if (krigI)
            {
                m_krigingInterface = krigI;
                m_lastSourceMTime = -1;
            }
        }

        void AnthropoModel::clearTargetsValue() {
            std::set<std::string> const listdim = listAnthropoBodyDimension();
            for (auto const& dim : m_bodydimension) {
                dim.second->setTargetValue(-1);
            }
            for (auto const& comp : m_components) {
                comp.second->clearLandmarksTarget();
            }
            freeland_source.clear();
            freeland_target.clear(); 
            m_lastTargetMTime = -1;
        }

        std::map<std::string, piper::hbm::Coord>const& AnthropoModel::getFreeLandmarksSource() const {
            return freeland_source;
        }

        std::map<std::string, Landmarkconstraint>const& AnthropoModel::getFreeLandmarksTargets() const {
            return freeland_target;
        }

        std::map<std::string, Landmarkconstraint>const& AnthropoModel::getSegmentLandmarksTargets() const {
            return m_segment_landmarks;
        }

    Landmarkconstraint::Landmarkconstraint(std::string const& name, Coord const& coord, double const& use_bone, double const& use_skin)
        : name(name)
        , coord(coord)
        , use_bone(use_bone)
        , use_skin(use_skin)
    {}
    
    }
}



