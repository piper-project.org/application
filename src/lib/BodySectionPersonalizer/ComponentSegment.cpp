/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ComponentSegment.h"

#include "hbm/xmlTools.h"
#include "anatomyDB/query.h"
#include "hbm/HumanBodyModel.h"

using namespace piper::anthropometricmodel;
using namespace piper::hbm;


#pragma region AnthropoBodySegment

AnthropoBodySegment::AnthropoBodySegment()
    : AbstractAnthropoComponentNode(0.0, 1.0)
{}

void AnthropoBodySegment::serializeXml(tinyxml2::XMLElement* element) const {

    AbstractAnthropoComponentNode::serializeXml(element);

    if (m_viapoint.size() > 0) {
        std::vector<bool>::const_iterator it_viapoint = m_cp_viapoint.begin();
        std::vector<bool>::const_iterator it_landmark = m_cp_landmark.begin();
        for (auto const& cur : m_viapoint) {
            tinyxml2::XMLElement* xmlseginter = element->GetDocument()->NewElement("viapoint");
            hbm::serializeValueCont(xmlseginter, cur);
            xmlseginter->SetAttribute("cp_viapoint", *it_viapoint);
            xmlseginter->SetAttribute("cp_landmark", *it_landmark);
            it_viapoint++;
            it_landmark++;
            element->InsertEndChild(xmlseginter);
        }
    }
}

void AnthropoBodySegment::parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel)
{
    AbstractAnthropoComponentNode::parseXml(element, anthropomodel);

    const tinyxml2::XMLElement* xmlInterLand = element->FirstChildElement("viapoint");
    bool boolvalue;
    while (xmlInterLand != nullptr) {
        listlandmark newinter;
        hbm::parseValueCont(newinter, xmlInterLand);
        m_viapoint.push_back(newinter);
        boolvalue = true;
        xmlInterLand->QueryBoolAttribute("cp_viapoint", &boolvalue);
        m_cp_viapoint.push_back(boolvalue);
        boolvalue = false;
        xmlInterLand->QueryBoolAttribute("cp_landmark", &boolvalue);
        m_cp_landmark.push_back(boolvalue);
        xmlInterLand = xmlInterLand->NextSiblingElement("viapoint");
    }
    setComputed(false);
}

AnthropoFrame AnthropoBodySegment::getPointatPosition(piper::hbm::HumanBodyModel *const model, double const& position) {
    AnthropoFrame frame;
    frame.setIdentity();
    if (compute(model)) {
        getPointatPosition(m_component, position, frame);
    }
    return frame;
}

void AnthropoBodySegment::getPointatPosition(Eigen::Matrix3Xd& segmentcoord, double const& position, AnthropoFrame& frame) {
    //compute norm of inter segment
    Eigen::VectorXd normseg(segmentcoord.size());
    normseg[0] = 0;
    for (Eigen::Matrix3Xd::Index n = 0; n < segmentcoord.cols() - 1; n++)
        normseg[n + 1] = (segmentcoord.col(n + 1) - segmentcoord.col(n)).norm() + normseg[n];
    normseg = normseg / normseg[segmentcoord.cols() - 1];
    //
    for (int n = 0; n < normseg.size() - 1; n++) {
        if (position >= normseg[n] && position <= normseg[n + 1])  {
            hbm::Coord normalsection = segmentcoord.col(n + 1) - segmentcoord.col(n);
            normalsection.normalize();
            Eigen::Vector3d axisref = normalsection.cross(frame.linear().col(2));
            axisref.normalize();
            double angle = acos(normalsection.dot(frame.linear().col(2)));
            Eigen::AngleAxisd realign(-angle, axisref);
            Eigen::Isometry3d transformation;
            transformation.setIdentity();
            transformation = realign * transformation;
            frame = transformation * frame;
            //
            double pos = (position - normseg[n]) / (normseg[n + 1] - normseg[n]);
            double axisnorm = normalsection.norm();
            normalsection.normalize();
            // Define position along axis to define section
            frame.translation() = segmentcoord.col(n) + (segmentcoord.col(n + 1) - segmentcoord.col(n)) * axisnorm * pos;
            break;
        }
    }
}

void AnthropoBodySegment::checkmetadata(piper::hbm::HumanBodyModel const * const model) {
    // landmarks
    std::vector<listlandmark> m_viapoint_old = m_viapoint;
    m_viapoint.clear();
    for (auto const& viapoint : m_viapoint_old) {
        listlandmark checked_viapoint;
        for (auto const& landmark : viapoint) {
            std::vector<std::string> syn;
            if (anatomydb::isLandmark(landmark))
                syn = anatomydb::getSynonymList(landmark);
            else
                syn.push_back(landmark);
            for (auto const& cur : syn) {
                if (model->metadata().hasLandmark(cur)) {
                    checked_viapoint.insert(cur);
                    break;
                }
                std::stringstream str;
                str << "[FAILED] Definition of Anthropometric Body Segment: " << std::endl;
                str << "For Anthropometric Body Segment " << m_name << ": Landmark " << cur << " (or synonym) is not defined." << endl;
                throw std::runtime_error(str.str().c_str());
                return;
            }
        }
        m_viapoint.push_back(checked_viapoint);
    }
}



std::vector<std::set<std::string>> const& AnthropoBodySegment::getLandmarks() const {
    return m_viapoint;
}


void AnthropoBodySegment::addLandmark(size_t n, std::string const& landname) {
    m_viapoint[n].insert(landname);
    setComputed(false);
}


void AnthropoBodySegment::removeLandmark(size_t n, std::string const& landname) {
    m_viapoint[n].erase(landname);
    setComputed(false);
}

void AnthropoBodySegment::insertViapoint(size_t n) {
    if (m_viapoint.size() == n) {
        m_viapoint.push_back(std::set<std::string>());
        m_cp_viapoint.push_back(false);
        m_cp_landmark.push_back(false);
    }
    else {
        m_viapoint.insert(m_viapoint.begin() + n, std::set<std::string>());
        m_cp_viapoint.insert(m_cp_viapoint.begin() + n, false);
        m_cp_landmark.insert(m_cp_landmark.begin() + n, false);
    }
    setComputed(false);
}

void AnthropoBodySegment::removeViapoint(size_t n) {
    if (m_viapoint.size() == 1 && n == 0) {
        m_viapoint.clear();
        m_cp_viapoint.clear();
        m_cp_landmark.clear();
    }
    else {
        m_viapoint.erase(m_viapoint.begin() + n);
        m_cp_viapoint.erase(m_cp_viapoint.begin() + n);
        m_cp_landmark.erase(m_cp_landmark.begin() + n);
    }
    setComputed(false);
}

bool AnthropoBodySegment::isDefined() const {
    // count how many via_point are valid
    int validViapoint = 0;
    for (auto const& cur : m_viapoint) {
        if (cur.size() == 0)
            return false;
        validViapoint += 1;
    }
    if (m_fromParent || m_centerParent) {
        return (validViapoint >= 1 && parent != nullptr && parent->isDefined());
    }
    else {
        if (parent == nullptr)
            return (validViapoint >= 2);
        else
            return (validViapoint >= 2 && parent->isDefined());
    }
}

double AnthropoBodySegment::getValueTarget(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) {
    double value = -1.0;
    computeTarget(model);
    if (!m_isTargetComputed)
        return value;
    if (type == AnthropoDimensionType::SEGMENT_MULTILENGTH) {
        //compute norm of inter segment
        Eigen::VectorXd normseg(m_componentTarget.cols());
        value = 0;
        for (Eigen::Matrix3Xd::Index n = 0; n < m_componentTarget.cols() - 1; n++)
            value += (m_componentTarget.col(n + 1) - m_componentTarget.col(n)).norm();
    }
    else {
        value = (m_componentTarget.col(m_componentTarget.cols() - 1) - m_componentTarget.col(0)).norm();
    }
    return value;
}

double AnthropoBodySegment::getValue(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) {
    double value = -1.0;
    if (!isDefined() || !compute(model))
        return value;
    if (type == AnthropoDimensionType::SEGMENT_MULTILENGTH) {
        //compute norm of inter segment
        Eigen::VectorXd normseg(m_component.cols());
        value = 0;
        for (Eigen::Matrix3Xd::Index n = 0; n < m_component.cols() - 1; n++)
            value += (m_component.col(n + 1) - m_component.col(n)).norm();
    }
    else {
        value = (m_component.col(m_component.cols() - 1) - m_component.col(0)).norm();
    }
    return value;
}

void AnthropoBodySegment::doCompute(piper::hbm::HumanBodyModel *const model) {
    m_component.resize(3, 0);
    m_controlpoints.resize(3, 0);
    m_sectionentity.setIdentity();
    int numbercp = 0;
    int number = 0;
    if (m_fromParent && m_cp_onparent)
        numbercp++;
    // how many control point and component points we have to define
    std::vector<bool>::const_iterator it_viapoint = m_cp_viapoint.begin();
    std::vector<bool>::const_iterator it_landmark = m_cp_landmark.begin(); 
    for (auto const& cur : m_viapoint) {
        if (cur.size() > 0) {
            number++;
            if (*it_viapoint)
                numbercp++;
        }
        if (*it_landmark)
            numbercp += (int)cur.size();
        it_viapoint++;
        it_landmark++;
    }
    m_controlpoints.resize(3, numbercp);

    if (m_fromParent)
        m_component.resize(3, number + 1);
    else
        m_component.resize(3, number);

    int count = 0;
    int count_cp = 0;
    if (m_fromParent) {
        AnthropoFrame local = parent->getPointatPosition(model, m_position);
        m_component.col(count) = local.translation();
        if (m_cp_onparent) {
            m_controlpoints.col(count_cp) = local.translation();
            count_cp++;
        }
        count++;
    }

    it_viapoint = m_cp_viapoint.begin();
    it_landmark = m_cp_landmark.begin();
    for (auto const& cur : m_viapoint) {
        Eigen::Matrix3Xd mat;
        mat.resize(3, cur.size());
        if (cur.size() > 0) {
            Eigen::Matrix3Xd::Index n = 0;
            for (auto const& curl : cur) {
                mat.col(n) = model->metadata().landmark(curl).position(model->fem());
                if (*it_landmark) {
                    m_controlpoints.col(count_cp) = mat.col(n);
                    count_cp++;
                }
                n++;
            }
            m_component.col(count) = mat.rowwise().sum().array() / cur.size();
            if (*it_viapoint) {
                m_controlpoints.col(count_cp) = mat.rowwise().sum().array() / cur.size();
                count_cp++;
            }
        }

        count++;
        it_landmark++;
        it_viapoint++;
    }
    //
    hbm::Coord normalsection = m_component.col(m_component.cols() - 1) - m_component.col(0);
    normalsection.normalize();
    Eigen::Vector3d axisref = normalsection.cross(m_sectionentity.linear().col(2));
    axisref.normalize();
    double angle = acos(normalsection.dot(m_sectionentity.linear().col(2)));
    Eigen::AngleAxisd realign(-angle, axisref);
    Eigen::Isometry3d transformation;
    transformation.setIdentity();
    transformation = realign * transformation;
    m_sectionentity = transformation * m_sectionentity;

    if (!m_centerParent)
        m_sectionentity.translation() = m_component.col(0);
    else 
        m_sectionentity.translation() = getCenterFromParentIntersection(parent, model);
    setComputed(true);
}

void AnthropoBodySegment::setControlPointViaPoint(size_t n, bool const& truefalse) {
    if (n >= 0 && n < m_cp_viapoint.size() && m_cp_viapoint[n] != truefalse) {
        m_cp_viapoint[n] = truefalse;
        setComputed(false);
    }
}
 
void AnthropoBodySegment::setControlPointtLandmark(size_t n, bool const& truefalse) {
    if (n >= 0 && n < m_cp_landmark.size() && m_cp_landmark[n] != truefalse) {
        m_cp_landmark[n] = truefalse;
        setComputed(false);
    }
}

bool AnthropoBodySegment::getControlPointtLandmark(size_t n) const {
    if (n >= 0 && n < m_cp_landmark.size())
        return m_cp_landmark[n];
    else return false;
}

bool AnthropoBodySegment::getControlPointViaPoint(size_t n) const {
    if (n >= 0 && n < m_cp_viapoint.size())
        return m_cp_viapoint[n];
    else return false;
}


bool AnthropoBodySegment::checkComponentDimensions() const {
    // if only one dim, it is ok
    if (m_dim.size() > 1)
        return false;
    else
        return true;
}


bool AnthropoBodySegment::doComputeScaleValue(piper::hbm::HumanBodyModel *const model, Eigen::Vector3d & scales) {
    for (auto const& cur : m_dim) {
        if (cur->isDefined() && cur->isTargetValueDefined()) {
            double current = cur->getValue(model);
            double target = cur->getTargetValue(model);
            double weight = cur->getScalingWeight(this);
            double scale = weight * target / current;
            switch (cur->getType()) {
            case AnthropoDimensionType::SEGMENT_LENGTH: {
                scales[2] += scale;
                break;
            }
            case AnthropoDimensionType::SEGMENT_MULTILENGTH: {
                scales[0] += scale;
                scales[1] += scale;
                scales[2] += scale;
                break;
            }
            }
        }
        else
            return false;
    }
    return true;
}


Coord AnthropoBodySegment::getCenterFromParentIntersection(AbstractAnthropoComponentNode* parent, HumanBodyModel *const model) const {
    // from http://geomalgorithms.com/a07-_distance.html

    double const SMALL_NUM = 0.00000001;
    if (parent->type() == AnthropoComponentType::SEGMENT) {
        Eigen::Matrix3Xd const& segmentcoord = parent->getComponentCoord(model);
        Coord S1P0 = m_component.col(0);
        Coord S1P1 = m_component.col(m_component.cols() - 1);
        Coord S2P0 = segmentcoord.col(0);
        Coord S2P1 = segmentcoord.col(segmentcoord.cols() - 1);

        Coord u = S1P1 - S1P0;
        Coord v = S2P1 - S2P0;
        Coord w = S1P0 - S2P0;

        double a = u.dot(u);
        double b = u.dot(v);
        double c = v.dot(v);
        double d = u.dot(w);
        double e = v.dot(w);
        double D = a*c - b*b;
        double sc, sN, sD = D;
        double tc, tN, tD = D;;

        // compute the line parameters of the two closest points
        if (D < SMALL_NUM) { // the lines are almost parallel
            sN = 0.0;         // force using point P0 on segment S1
            sD = 1.0;         // to prevent possible division by 0.0 later
            tN = e;
            tD = c;
        }
        else {                 // get the closest points on the infinite lines
            sN = (b*e - c*d);
            tN = (a*e - b*d);
            if (sN < 0.0) {        // sc < 0 => the s=0 edge is visible
                sN = 0.0;
                tN = e;
                tD = c;
            }
            else if (sN > sD) {  // sc > 1  => the s=1 edge is visible
                sN = sD;
                tN = e + b;
                tD = c;
            }
        }

        if (tN < 0.0) {            // tc < 0 => the t=0 edge is visible
            tN = 0.0;
            // recompute sc for this edge
            if (-d < 0.0)
                sN = 0.0;
            else if (-d > a)
                sN = sD;
            else {
                sN = -d;
                sD = a;
            }
        }
        else if (tN > tD) {      // tc > 1  => the t=1 edge is visible
            tN = tD;
            // recompute sc for this edge
            if ((-d + b) < 0.0)
                sN = 0;
            else if ((-d + b) > a)
                sN = sD;
            else {
                sN = (-d + b);
                sD = a;
            }
        }
        // finally do the division to get sc and tc
        sc = (abs(sN) < SMALL_NUM ? 0.0 : sN / sD);
        tc = (abs(tN) < SMALL_NUM ? 0.0 : tN / tD);

        Coord P1inter = S1P0 + (sc * u);
        Coord P2inter = S2P0 + (tc * v);

        // get the difference of the two closest points
        double ShortDist = (w + P1inter - P2inter).norm();  // =  S1(sc) - S2(tc)
        return P1inter;
    }
    else {
        // compute insertection between xy plane parent section frame and segment
        Coord S1P0 = m_component.col(0);
        Coord S1P1 = m_component.col(m_component.cols() - 1);

        Coord PVO = parent->getLocalFrame(model).translation();
        Coord PVn = parent->getLocalFrame(model).rotation().col(2);
        
        Coord u = S1P1 - S1P0;
        Coord w = S1P0 - PVO;

        double D = PVn.dot(u);
        double N = -PVn.dot(w);

        if (fabs(D) < SMALL_NUM) {           // segment is parallel to plane
            if (N == 0)                      // segment lies in plane
                return S1P0;
            else
                return S1P0;                    // no intersection
        }
        // they are not parallel
        // compute intersect param
        float sI = N / D;
        if (sI < 0 || sI > 1)
            return S1P0;                        // no intersection

        //*I = S.P0 + sI * u;                  // compute segment intersect point
        return S1P0 + sI * u;

    }
}

bool AnthropoBodySegment::setLandmarksTarget(std::map<std::string, Landmarkconstraint> const& landtargets, std::set<std::string>& used) {
    std::set<std::string> my_used;
    // we can set landmark target for mono segment
    m_landtargets.clear();
    if ((!m_fromParent && m_viapoint.size() != 2) || (m_viapoint.size() != 1 && m_fromParent))
        return true;

    // check if all required landmarks are defined
    size_t nland=0;
    for (auto const& landlist : m_viapoint) {
        nland += landlist.size();
        for (auto const& land : landlist) {
            if (landtargets.find(land) != landtargets.end()) {
                m_landtargets[land] = landtargets.at(land).coord;
                my_used.insert(land);
            }
        }
    }
    if (m_landtargets.size() == 0 || m_landtargets.size() == nland) {
        used.insert(my_used.begin(), my_used.end());
        return true;
    }
    else {
        m_landtargets.clear();
        my_used.clear();
        return false;
    }
    setTargetComputed(false);
}

void AnthropoBodySegment::doComputeTarget(piper::hbm::HumanBodyModel *const model) {
    m_componentTarget.resize(3, m_component.cols());
    m_controlpointsTarget.resize(3, m_controlpoints.cols());

    int count = 0;
    int count_cp = 0;
    if (m_fromParent) {
        AnthropoFrame local = parent->getPointatPosition(model, m_position);
        m_componentTarget.col(count) = local.translation();
        if (m_cp_onparent) {
            m_controlpointsTarget.col(count_cp) = local.translation();
            count_cp++;
        }
        count++;
    }
    std::vector<bool>::const_iterator it_viapoint = m_cp_viapoint.begin();
    std::vector<bool>::const_iterator it_landmark = m_cp_landmark.begin();
    for (auto const& cur : m_viapoint) {
        Eigen::Matrix3Xd mat;
        mat.resize(3, cur.size());
        if (cur.size() > 0) {
            Eigen::Matrix3Xd::Index n = 0;
            for (auto const& curl : cur) {
                mat.col(n) = m_landtargets.at(curl);
                if (*it_landmark) {
                    m_controlpointsTarget.col(count_cp) = mat.col(n);
                    count_cp++;
                }
                n++;
            }
            m_componentTarget.col(count) = mat.rowwise().sum().array() / cur.size();
            if (*it_viapoint) {
                m_controlpointsTarget.col(count_cp) = mat.rowwise().sum().array() / cur.size();
                count_cp++;
            }
        }

        count++;
        it_landmark++;
        it_viapoint++;
    }
    //if (m_componentTarget.cols() == 2) {
        double scalefactor = (m_componentTarget.col(m_componentTarget.cols() - 1) - m_componentTarget.col(0)).norm() / (m_component.col(m_component.cols() - 1) - m_component.col(0)).norm();
        Eigen::Vector3d scales(1, 1, scalefactor);
        //define transformation + parent offset
        ComponentTransformation offsetparent;
        offsetparent.setIdentity();
        if (hasParent()) {
            offsetparent = parent->getTransformationTarget(model);
        }
        AnthropoFrame sectionTarget;
        sectionTarget.setIdentity();

        hbm::Coord normalsection = m_componentTarget.col(m_componentTarget.cols() - 1) - m_componentTarget.col(0);
        normalsection.normalize();
        Eigen::Vector3d axisref = normalsection.cross(m_sectionentity.linear().col(2));
        axisref.normalize();
        double angle = acos(normalsection.dot(m_sectionentity.linear().col(2)));
        Eigen::AngleAxisd realign(-angle, axisref);
        Eigen::Isometry3d transformation;
        transformation.setIdentity();
        transformation = realign * transformation;
        sectionTarget = transformation * m_sectionentity;
        sectionTarget.translation() = m_componentTarget.col(0);

        m_transf.set(offsetparent, scales, m_sectionentity, sectionTarget);
    //}
    setTargetComputed(true);
}

#pragma endregion AnthropoBodySegment
