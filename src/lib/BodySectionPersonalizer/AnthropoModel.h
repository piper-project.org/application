/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANTHROPOMODEL_H
#define PIPER_ANTHROPOMODEL_H

#ifdef WIN32
#	ifdef AnthropoModel_EXPORTS
#		define ANTHROPOMODEL_EXPORT __declspec( dllexport )
#	else
#		define ANTHROPOMODEL_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANTHROPOMODEL_EXPORT
#endif

#include "AnthropoComponent.h"
#include "ComponentSegment.h"
#include "kriging/IntermediateTargetsInterface.h"

#include <map>


namespace piper {

    namespace hbm {
        class HumanBodyModel;
        class Nodes;
    }
    namespace anthropometricmodel {

        typedef std::map<std::string, std::shared_ptr<AbstractAnthropoComponentNode>> AnthropoComponentCont;
        typedef std::set<std::string> ComponentList;
        typedef std::map<std::string, std::shared_ptr<AnthropoBodyDimension>> AnthropoBodyDimensionCont;


        class ANTHROPOMODEL_EXPORT AnthropoModel {
        public:
            AnthropoModel();
            AnthropoModel(std::shared_ptr<IntermediateTargetsInterface> krigInterface) : m_hbm(nullptr), m_krigingInterface(krigInterface) {}
            AnthropoModel(piper::hbm::HumanBodyModel* hbmref, std::shared_ptr<IntermediateTargetsInterface> krigInterface);
            void setModel(piper::hbm::HumanBodyModel* hbmref);
            
            /// <summary>
            /// Sets the interface to use for kriging. If not set through the constructor or this method, AnthropoModel will create its own.
            /// </summary>
            /// <param name="krigI">The kriging interface to use. If it is NULL, it is ignored and not set.</param>
            void setKrigingInterface(std::shared_ptr<IntermediateTargetsInterface> krigI);
            std::shared_ptr<IntermediateTargetsInterface> getKrigingInterface() { return m_krigingInterface; }

            void resetModel();
            void read(std::string const& file);
            void write(std::string const& file);
            void serialize(tinyxml2::XMLElement* element) const;
            void parseXml(tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);
            void check();
            void clearAnthropoModel();

            std::list<std::string> listComponents() const;
            bool hasAnthropoComponent(std::string const& name) const;
            AbstractAnthropoComponentNode* getAnthropoComponent(std::string const& compname);
            AbstractAnthropoComponentNode const*const getAnthropoComponent(std::string const& compname) const;

            std::set<std::string> const& listAnthropoBodySegment() const;
            bool hasAnthropoBodySegment(std::string const& name) const;
            AnthropoBodySegment* getAnthropoBodySegment(std::string const& sectionname);
            AnthropoBodySegment const*const getAnthropoBodySegment(std::string const& sectionname) const;

            std::set<std::string> const& listAnthropoBodySection() const;
            bool hasAnthropoBodySection(std::string const& name) const;
            AnthropoBodySection* getAnthropoBodySection(std::string const& sectionname);
            AnthropoBodySection const*const getAnthropoBodySection(std::string const& sectionname) const;

            std::set<std::string> listAnthropoBodyDimension() const;
            bool hasAnthropoBodyDimension(std::string const& name) const;
            AnthropoBodyDimension * getAnthropoBodyDimension(std::string const& sectionname);
            AnthropoBodyDimension const*const getAnthropoBodyDimension(std::string const& sectionname) const;


            /*!
            *  \brief return true is the anthropometric model is defined
            *
            * \return true: if at least one dimension
            */
            bool isDefined() { return (m_bodysegment.size() > 0 || m_bodysection.size() > 0); }
            /*!
            *  \brief set target height
            *
            * \param value: the target height
            */
            void setTargetHeight(double const& value);
            /*!
            *  \brief get target height
            *
            * \return return the target height
            */
            double const& getTargetHeight() const;
            /*!
            *  \brief add a new anthropometric component
            *
            * \param comptype: the component type
            * \param compname: the component name
            */
            void newAnthropoComponent(AnthropoComponentType const& comptype, std::string const& compname);
            /*!
            *  \brief delete a anthropometric component
            *
            * \param comptype: the component type
            * \param compname: the component name
            */
            void deleteAnthropoComponent(AnthropoComponentType const& comptype, std::string const& compname, bool const& children=false);
            /*!
            *  \brief rename a anthropometric component
            *
            * \param comptype: the component type
            * \param compname: the component name
            */
            void renameAnthropoComponent(AnthropoComponentType const& comptype, std::string const& oldname, std::string const& newname);
            /*!
            *  \brief set the type of scaling for all dimensions of the model
            *
            * \param type: boolean equal to false if the dimension defines an absolute value and true for a relative scale value
            */

            void setScalingRelative(bool const& truefalse);

            std::list<std::string> setLandmarksTarget(std::list<hbm::LandmarkTarget> const& landtargets);
            std::set<std::string> getRootComponentName() const;



            /// <summary>
            /// Gets the control points.
            /// </summary>
            /// <param name="controlPointsSource">This container will be filled with source positions.</param>
            /// <param name="controlPointsTarget">This container will be filled with target positions.</param>
            /// <param name="allowedCompTypes">Only control points with the specified AnthropoComponentType will be added to the result.
            /// Use bitwise operations on AnthropoComponentType to allow multiple types (e.g (AnthropoComponentType::SEGMENT | AnthropoComponentType::SECTION) will
            /// get control points from all segment and section-typed components). Default is all types are allowed.</param>
            void getControlPoints(piper::hbm::VCoord& controlPointsSource, piper::hbm::VCoord& controlPointsTarget,
                unsigned int allowedCompTypes = (unsigned int)(AnthropoComponentType::ALL));
                        
            /// <summary>
            /// Gets the current source control points.
            /// This does NOT call the updateControlPoints method before returning - do that yourself as needed in order to ensure that the points are up to date.
            /// </summary>
            /// <returns>The current set of source control points.</returns>
            piper::hbm::InteractionControlPoint const& getControlPointsSource();

            /// <summary>
            /// Gets the current target control points.
            /// This does NOT call the updateControlPoints method before returning - do that yourself as needed in order to ensure that the points are up to date.
            /// </summary>
            /// <returns>The current set of target control points.</returns>
            piper::hbm::InteractionControlPoint const& getControlPointsTarget();

            void clearTargetsValue();

            // get source free landmarks  coordinates on current hbm
            std::map<std::string, piper::hbm::Coord>const& getFreeLandmarksSource() const;
            // get freee landmarks target 
            std::map<std::string, Landmarkconstraint>const& getFreeLandmarksTargets() const;

            // get segment landmarks target 
            std::map<std::string, Landmarkconstraint>const& getSegmentLandmarksTargets() const;

             

            
            /// <summary>
            /// Gathers source and target control points from all components and sets them to the kriging interface.
            /// Relies on the modification times of the model's components - only updates sources/targets 
            /// if some component has been modified since the last time this method was called.
            /// </summary>
            void updateControlPoints();
                        
            /// <summary>
            /// Sets the nugget to use for all control points.
            /// </summary>
            /// <param name="nugget">The nugget.</param>
            void setGlobalNugget(double nugget);

                        
        private:

            /// <summary>
            /// Fills both the m_controlPointsSource and m_controlPointsTarget including the use_skin and use_bone parameters.
            /// </summary>
            /// <param name="allowedCompTypes">Only control points with the specified AnthropoComponentType will be added to the result.
            /// Use bitwise operations on AnthropoComponentType to allow multiple types (e.g (AnthropoComponentType::SEGMENT | AnthropoComponentType::SECTION) will
            /// get control points from all segment and section-typed components). Default is all types are allowed.</param>
            void gatherControlPoints(unsigned int allowedCompTypes = (unsigned int)(AnthropoComponentType::ALL));
            
            /// <summary>
            /// Fills the m_controlPointsTarget including the use_skin and use_bone parameters.
            /// </summary>
            /// <param name="allowedCompTypes">Only control points with the specified AnthropoComponentType will be added to the result.
            /// Use bitwise operations on AnthropoComponentType to allow multiple types (e.g (AnthropoComponentType::SEGMENT | AnthropoComponentType::SECTION) will
            /// get control points from all segment and section-typed components). Default is all types are allowed.</param>
            void gatherTargetControlPoints(unsigned int allowedCompTypes = (unsigned int)(AnthropoComponentType::ALL));

            /// create a new anthropometric component of the given \a type and add it to the corresponding container
            std::shared_ptr<AbstractAnthropoComponent> addAnthropoComponent(std::string const& type, std::string const& name);

            template<class AnthropoComponentType>
            void AnthropoComponentSerializeXml(AnthropoComponentType const& AnthropoComponent, tinyxml2::XMLElement* xmlAnthropoModel) const
            {
                tinyxml2::XMLElement* xmlComponent = xmlAnthropoModel->GetDocument()->NewElement("anthropoComponent");
                xmlComponent->SetAttribute("type", AnthropoComponentType_str.at(AnthropoComponent->type()).c_str());
                AnthropoComponent->serializeXml(xmlComponent);
                xmlAnthropoModel->InsertEndChild(xmlComponent);
            }

            static bool isSegmentDimensionType(AnthropoDimensionType const& type);

            piper::hbm::HumanBodyModel* m_hbm;
            ComponentList m_bodysegment, m_bodysection;
            AnthropoComponentCont m_components;
            AnthropoBodyDimensionCont m_bodydimension;
            mutable std::set<std::string> m_roots;
            //height scaling
            double m_targetHeight;
            std::string m_controlpointset;
            std::shared_ptr<IntermediateTargetsInterface> m_krigingInterface;
            std::map<std::string, piper::hbm::Coord> freeland_source;
            std::map<std::string, Landmarkconstraint> freeland_target, m_segment_landmarks;
            double m_globalNugget = 0;
            piper::hbm::InteractionControlPoint m_controlPointsSource, m_controlPointsTarget;
            clock_t m_lastSourceMTime, m_lastTargetMTime; // modification times of control points when they were last gathered from the components            
        };


    }
}
#endif
