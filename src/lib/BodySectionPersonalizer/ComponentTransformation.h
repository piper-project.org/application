/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_COMPONENTTRANSF_H
#define PIPER_COMPONENTTRANSF_H

//#ifdef WIN32
//#	ifdef AnthropoModel_EXPORTS
//#		define ANTHROPOMODEL_EXPORT __declspec( dllexport )
//#	else
//#		define ANTHROPOMODEL_EXPORT __declspec( dllimport )
//#	endif
//#else
//#	define ANTHROPOMODEL_EXPORT
//#endif

#include "kriging/KrigingPiperInterface.h"

namespace piper {

    namespace hbm {
        class HumanBodyModel;
    }
    namespace anthropometricmodel {
        class AbstractAnthropoComponentNode;
        typedef Eigen::Affine3d Transf;
        typedef Eigen::Isometry3d IsoTransf;
        /*!
        *  \brief Defined type of trasnformation
        *
        */
        enum class TransformationType {
            SCALING = 0,     ///< 
            KRIGING,     ///< 
        };

        /**
        * @brief Store transformation to be applied on a component to generate target component
        *
        * @author Erwan Jolivet @date 2017
        */
        class /*ANTHROPOMODEL_EXPORT*/ ComponentTransformation  {

        public:


            ComponentTransformation();
            ~ComponentTransformation();
            /*!
            *  \brief set transformation to identity
            *
            */
            void setIdentity();
            /*!
            *  \brief apply transformation to points
            *
            * \param points: eigen matrix with coordinates of points to transform
            * \return: eigen matrix with coordinates of transfomed points
            */
            Eigen::Matrix3Xd apply(Eigen::Matrix3Xd const& points, Transf const& local) const;
            /*!
            *  \brief set scaling factors of the transformation
            *
            * \param scalefactors: vector with the three scale factors
            * \param local: reference frame
            * \return: eigen matrix with coordinates of transfomed points
            */
            void set(ComponentTransformation const& offset, Eigen::Vector3d const& scalefactors, Transf const& local);
            void set(ComponentTransformation const& offset, Eigen::Vector3d const& scalefactors, Transf const& local, Transf const& localtarget);
            /*!
            *  \brief set points in local target frame
            *
            * \param points: eigen matrix with coordinates of points to transform
            * \param local: reference frame
            * \return: eigen matrix with coordinates of transfomed points
            */
            Eigen::Matrix3Xd TargetintoLocal(Eigen::Matrix3Xd const& points, Transf const& local);
        private:
            TransformationType m_type;
            Transf  m_scalingTransf; //store scaling transformation
            KrigingPiperInterface* m_kriging; //store kriging transformation
        };


    }
}

#endif
