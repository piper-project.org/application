/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ComponentTransformation.h"



using namespace piper::anthropometricmodel;

ComponentTransformation::ComponentTransformation()
    : m_type(TransformationType::SCALING)
    , m_kriging(new KrigingPiperInterface())
{
    setIdentity();
}

ComponentTransformation::~ComponentTransformation() {
    //delete m_kriging;
}

void ComponentTransformation::setIdentity() {
    m_scalingTransf.setIdentity();
    m_kriging->clearSystemMatrix();
}


Eigen::Matrix3Xd ComponentTransformation::apply(Eigen::Matrix3Xd const& points, Transf const& local) const {
    Eigen::Matrix3Xd transf_points;
    if (TransformationType::SCALING == m_type) {
        transf_points = m_scalingTransf * points;
    }
    else {

    }
    return transf_points;
}

void ComponentTransformation::set(ComponentTransformation const& offset, Eigen::Vector3d const& scalefactors, Transf const& local) {
    //
    setIdentity();
    m_type = TransformationType::SCALING;
    // scaling
    Eigen::Vector3d sc;
    Transf tmp;
    tmp.setIdentity();
    sc[0] = ((scalefactors[0] == 0.0) ? 1.0 : scalefactors[0]);
    sc[1] = ((scalefactors[1] == 0.0) ? 1.0 : scalefactors[1]);
    sc[2] = ((scalefactors[2] == 0.0) ? 1.0 : scalefactors[2]);
    tmp = tmp * sc.asDiagonal();
    // apply offset 
    Transf rotate, translate, translate2, parenttransf;
    parenttransf = offset.m_scalingTransf;
    rotate.setIdentity();
    translate.setIdentity();
    translate2.setIdentity();
    rotate.linear() = parenttransf.rotation();
    translate.translation() = parenttransf * local.translation();
    translate2.translation() = local.translation();
    m_scalingTransf = translate * rotate * translate2.inverse();
    m_scalingTransf = m_scalingTransf * local * tmp * local.inverse();
}

void ComponentTransformation::set(ComponentTransformation const& offset, Eigen::Vector3d const& scalefactors, Transf const& local, Transf const& localtarget) {
    // don't care of history scaling transformation, it is broken by setting landmarks
    setIdentity();
    m_type = TransformationType::SCALING;
    // scaling
    Eigen::Vector3d sc;
    Transf tmp;
    tmp.setIdentity();
    sc[0] = ((scalefactors[0] == 0.0) ? 1.0 : scalefactors[0]);
    sc[1] = ((scalefactors[1] == 0.0) ? 1.0 : scalefactors[1]);
    sc[2] = ((scalefactors[2] == 0.0) ? 1.0 : scalefactors[2]);
    tmp = tmp * sc.asDiagonal();
    // apply offset 
    //Transf rotate, translate, translate2, parenttransf;
    //parenttransf = offset.m_scalingTransf;
    //rotate.setIdentity();
    //translate.setIdentity();
    //translate2.setIdentity();
    //rotate.linear() = parenttransf.rotation();
    //translate.translation() = parenttransf * local.translation();
    //translate2.translation() = local.translation();
    //m_scalingTransf = translate * rotate * translate2.inverse();
    m_scalingTransf.setIdentity();
    m_scalingTransf = m_scalingTransf * localtarget * tmp * local.inverse();
}

Eigen::Matrix3Xd ComponentTransformation::TargetintoLocal(Eigen::Matrix3Xd const& points, Transf const& local) {
    Eigen::Isometry3d b;
    Eigen::Matrix3Xd transf_points(points);
    b.translation() = m_scalingTransf.translation();
    b.linear() = m_scalingTransf.rotation();
    b = b * local.matrix();
    transf_points = b.inverse() * transf_points;
    return transf_points;
}



