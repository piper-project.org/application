project(test_BodySectionPersonalizer)

if(MSVC)
    find_package(Boost COMPONENTS system filesystem REQUIRED)
    if(Boost_FOUND)
        link_directories(${Boost_LIBRARY_DIRS} ${Boost_LIBRARY_DIR}) #somehow compulsory on windows VS to link with hbm
    endif()
endif()

# set(HEADER_FILES
# )

set(SOURCE_FILES
	bodysectionpersonalizer_test.cpp
    anthropomodel_test.cpp
)

if (MSVC AND MSVC_VERSION EQUAL 1700)
	add_definitions(/D _VARIADIC_MAX=10)
endif()

add_executable(${PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES} hbm AnthropoModel gtest_main vtkPIPERFilters)
add_definitions(-DSQLITE_DIR="${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

if(MSVC_IDE)
    foreach(CONFIGURATION ${CMAKE_CONFIGURATION_TYPES})
        add_test(NAME ${PROJECT_NAME}_${CONFIGURATION} WORKING_DIRECTORY ${PIPER_DATA_PATH} COMMAND ${PROJECT_NAME} CONFIGURATIONS ${CONFIGURATION})
    endforeach()
else()
    add_test(NAME ${PROJECT_NAME} WORKING_DIRECTORY ${PIPER_DATA_PATH} COMMAND ${PROJECT_NAME} --gtest_output=${GTEST_OUTPUT})
    if(WIN32)
        set_tests_properties(${PROJECT_NAME} PROPERTIES ENVIRONMENT "PATH=${VTK_INSTALL_PREFIX}/bin\;${Boost_LIBRARY_DIRS}\;${PIPER_QT_INSTALL_PREFIX}/bin\;$ENV{PATH}")
    endif()
endif()

find_package(VTK 7.0.0 EXACT REQUIRED)
include(${VTK_USE_FILE})

#setting MSVC project properties for Environment variable (to ease setting project debugging environment)
IF(WIN32)    
    foreach(CONFIGURATION ${CMAKE_CONFIGURATION_TYPES})
        set (PATH_WINDOWS $ENV{PATH})
        STRING(REPLACE ";" "\\;" PATH_WINDOWS "${PATH_WINDOWS}")
        set (PATH_WINDOWS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CONFIGURATION}/\;${XMLLINT_DIR}\;${Mesquite_BINARY_DIR}\;${Boost_LIBRARY_DIRS}\;${VTK_INSTALL_PREFIX}/bin\;${_qt5Core_install_prefix}/bin\;${PATH_WINDOWS};") #escape semicolon in the string, otherwise only the first path from the semicolon separated list would be taken into account
        set_tests_properties(${PROJECT_NAME}_${CONFIGURATION} PROPERTIES ENVIRONMENT  "PATH=${PATH_WINDOWS}")
    endforeach()    
    IF(MSVC_IDE)
        SET(USERFILE_ENVIRONMENT "PATH=${PATH_WINDOWS}/
		QML_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
		QML2_IMPORT_PATH=${_qt5Core_install_prefix}/qml/
		QT_QPA_PLATFORM_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/platforms
		QT_PLUGIN_PATH=${_qt5Core_install_prefix}/plugins/
		SOFA_ROOT=${SOFA_ROOT}
		PYTHONPATH=${PIPER_PACKAGE_DIR}/Python27/
		PYTHONHOME=${PIPER_PACKAGE_DIR}/Python27/
		PYTHONUSERBASE=${PIPER_PACKAGE_DIR}/Python27/")
        
        SET(USERFILE_WORKINGDIR "${PIPER_DATA_PATH}")

        # Configure the template file
        SET(USER_FILE ${PROJECT_NAME}.vcxproj.user)
        SET(OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR}/${USER_FILE})
        CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/piper.vcxproj.user.in ${USER_FILE} @ONLY)
    ENDIF(MSVC_IDE)
    
ENDIF(WIN32)


