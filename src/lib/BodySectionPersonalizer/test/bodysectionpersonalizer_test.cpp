/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "hbm/HumanBodyModel.h"
#include "hbm/target.h"
#include "BodySectionPersonalizer/AnthropoModel.h"
#include "anatomyDB/query.h"

#include <boost/filesystem.hpp>

#include "gtest/gtest.h"


using namespace piper::hbm;
using namespace piper::anthropometricmodel;

static std::string sqliteDir = std::string(SQLITE_DIR);
static bool isanatomyDBInit = false;


class BodySectionPersonalizer_test : public ::testing::Test {
    
protected:

    virtual void SetUp() {
        if (!isanatomyDBInit) {
            anatomydb::init(sqliteDir);
            isanatomyDBInit = true;
        }
        mypersonalizer.read("anthropoModelTest.xml");
    }

    void checkAnthropoModel() {
        EXPECT_EQ(2, mypersonalizer.listAnthropoBodySegment().size());
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodySegment("forearm_hand"));
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodySegment("shoulder_elbow"));
        EXPECT_EQ(2, mypersonalizer.listAnthropoBodySection().size());
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodySection("section_1"));
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodySection("section_2"));
        EXPECT_EQ(2, mypersonalizer.listAnthropoBodyDimension().size());
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodyDimension("dim_1"));
        EXPECT_TRUE(mypersonalizer.hasAnthropoBodyDimension("dim_2"));
        EXPECT_TRUE(mypersonalizer.getAnthropoBodyDimension("dim_1")->isScalingRelative());
        EXPECT_FALSE(mypersonalizer.getAnthropoBodyDimension("dim_2")->isScalingRelative());
        EXPECT_TRUE(mypersonalizer.getAnthropoComponent("section_1")->getParent()->name() == "shoulder_elbow");
        EXPECT_TRUE(mypersonalizer.getAnthropoComponent("shoulder_elbow")->getChildren().size() == 1);
        EXPECT_FALSE(mypersonalizer.getAnthropoComponent("shoulder_elbow")->getChildren().find(mypersonalizer.getAnthropoBodySection("section_1")) == mypersonalizer.getAnthropoComponent("shoulder_elbow")->getChildren().end());
        EXPECT_TRUE(mypersonalizer.getAnthropoBodySection("section_1")->centerOnParent());
        //control point parameters
        EXPECT_EQ(0.3, mypersonalizer.getAnthropoBodySegment("shoulder_elbow")->getControlPointParameter(AnthropoComponentParam::USE_BONE));
        EXPECT_EQ(0.6, mypersonalizer.getAnthropoBodySegment("shoulder_elbow")->getControlPointParameter(AnthropoComponentParam::USE_SKIN));
        EXPECT_EQ(1.0, mypersonalizer.getAnthropoBodySegment("forearm_hand")->getControlPointParameter(AnthropoComponentParam::USE_BONE));
        EXPECT_EQ(0.0, mypersonalizer.getAnthropoBodySegment("forearm_hand")->getControlPointParameter(AnthropoComponentParam::USE_SKIN));
        EXPECT_EQ(0.241, mypersonalizer.getAnthropoBodySection("section_1")->getControlPointParameter(AnthropoComponentParam::USE_BONE));
        EXPECT_EQ(0.87459, mypersonalizer.getAnthropoBodySection("section_1")->getControlPointParameter(AnthropoComponentParam::USE_SKIN));
        EXPECT_EQ(0.0, mypersonalizer.getAnthropoBodySection("section_2")->getControlPointParameter(AnthropoComponentParam::USE_BONE));
        EXPECT_EQ(1.0, mypersonalizer.getAnthropoBodySection("section_2")->getControlPointParameter(AnthropoComponentParam::USE_SKIN));
    }

    AnthropoModel mypersonalizer;

};

TEST_F(BodySectionPersonalizer_test, read) {
    checkAnthropoModel();
}

TEST_F(BodySectionPersonalizer_test, write) {
    // write current model to tmp
    boost::filesystem::path tmp = boost::filesystem::temp_directory_path();
    tmp /= "piper_body_section_export.xml";
    mypersonalizer.write(tmp.string());

    // read it
    mypersonalizer.clearAnthropoModel();
    mypersonalizer.read(tmp.string());
    checkAnthropoModel();
}

TEST_F(BodySectionPersonalizer_test, setRelactiveScaling) {
    mypersonalizer.setScalingRelative(true);
    EXPECT_TRUE(mypersonalizer.getAnthropoBodyDimension("dim_1")->isScalingRelative());
    EXPECT_TRUE(mypersonalizer.getAnthropoBodyDimension("dim_2")->isScalingRelative());
    mypersonalizer.setScalingRelative(false);
    EXPECT_FALSE(mypersonalizer.getAnthropoBodyDimension("dim_1")->isScalingRelative());
    EXPECT_FALSE(mypersonalizer.getAnthropoBodyDimension("dim_2")->isScalingRelative());
}

TEST_F(BodySectionPersonalizer_test, overconstrained) {
    //mypersonalizer.read("anthropoModelTest_overconstrainted1.xml");
    mypersonalizer.clearAnthropoModel();
    EXPECT_NO_THROW(mypersonalizer.read("anthropoModelTest_overconstrainted1.xml"));
    mypersonalizer.clearAnthropoModel();
    EXPECT_THROW(mypersonalizer.read("anthropoModelTest_overconstrainted2.xml"), std::runtime_error);
}

TEST_F(BodySectionPersonalizer_test, test_controlParameters) {
    EXPECT_THROW(mypersonalizer.getAnthropoBodySegment("shoulder_elbow")->setControlPointParameter(AnthropoComponentParam::USE_BONE, -0.1), std::runtime_error);
    EXPECT_THROW(mypersonalizer.getAnthropoBodySegment("shoulder_elbow")->setControlPointParameter(AnthropoComponentParam::USE_BONE, 1.1), std::runtime_error);
}

