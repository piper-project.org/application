/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "BodySectionPersonalizer/AnthropoModel.h"
#include "anatomyDB/query.h"
#include "hbm/target.h"

#include <boost/filesystem.hpp>

#include "gtest/gtest.h"


using namespace piper::hbm;
using namespace piper::anthropometricmodel;

class anthropomodel_test : public ::testing::Test {

protected:

    virtual void SetUp() {
    }
};

TEST(anthropomodel_test, widthScaling) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_widthScaling.xml");
    anthropomodel.setModel(&hbm);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->getValue(&hbm), 40, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getValue(&hbm), 40, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
    anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->setTargetValue(50);
    anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->setTargetValue(60);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(133);
    double targetlengthvalue = anthropomodel.getAnthropoComponent("compSegment")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_segment")->getType());
    double targetwidthuppervalue = anthropomodel.getAnthropoComponent("upperSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->getType());
    double targetwidthmiddlevalue = anthropomodel.getAnthropoComponent("middleSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getType());
    double targetwidthlowervalue = anthropomodel.getAnthropoComponent("lowerSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getType());
    EXPECT_NEAR(targetwidthlowervalue, 50, 1e-5);
    EXPECT_NEAR(targetwidthmiddlevalue, 53, 1e-5);
    EXPECT_NEAR(targetwidthuppervalue, 60, 1e-5);
    EXPECT_NEAR(targetlengthvalue, 133, 1e-5);
}

TEST(anthropomodel_test, circScaling) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_circScaling.xml");
    anthropomodel.setModel(&hbm);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->getValue(&hbm), 125.14756676, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getValue(&hbm), 125.14756676, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
    anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->setTargetValue(140);
    anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->setTargetValue(180);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(112);
    double targetlengthvalue = anthropomodel.getAnthropoComponent("compSegment")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_segment")->getType());
    double targetwidthuppervalue = anthropomodel.getAnthropoComponent("upperSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->getType());
    double targetwidthmiddlevalue = anthropomodel.getAnthropoComponent("middleSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getType());
    double targetwidthlowervalue = anthropomodel.getAnthropoComponent("lowerSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getType());
    EXPECT_NEAR(targetwidthlowervalue, 140, 1e-5);
    EXPECT_NEAR(targetwidthmiddlevalue, 152, 1e-5);
    EXPECT_NEAR(targetwidthuppervalue, 180, 1e-5);
    EXPECT_NEAR(targetlengthvalue, 112, 1e-5);
}

//TEST(anthropomodel_test, circwidthScaling) {
//    HumanBodyModel hbm;
//    AnthropoModel anthropomodel;
//    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
//    hbm.fem().updateVTKRepresentation();
//    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
//    anthropomodel.read("anthropoModelCylinder_circwidthScaling.xml");
//    anthropomodel.setModel(&hbm);
//    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->getValue(&hbm), 125.14756676, 1e-5);
//    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getValue(&hbm), 125.14756676, 1e-5);
//    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->getValue(&hbm), 40, 1e-5);
//    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getValue(&hbm), 40, 1e-5);
//    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
//    anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->setTargetValue(140);
//    anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->setTargetValue(180);
//    anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->setTargetValue(50);
//    anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->setTargetValue(60);
//    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(112);
//    double targetwidthuppervalue = anthropomodel.getAnthropoComponent("upperSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_lower_Circ")->getType());
//    double targetwidthmiddlevalue = anthropomodel.getAnthropoComponent("middleSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getType());
//    double targetwidthlowervalue = anthropomodel.getAnthropoComponent("lowerSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Circ")->getType());
//    double targetlengthvalue = anthropomodel.getAnthropoComponent("compSegment")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_segment")->getType());
//    EXPECT_NEAR(targetwidthlowervalue, 140, 1e-5);
//    EXPECT_NEAR(targetwidthmiddlevalue, 152, 1e-5);
//    EXPECT_NEAR(targetwidthuppervalue, 180, 1e-5);
//    EXPECT_NEAR(targetlengthvalue, 112, 1e-5);
//    targetwidthuppervalue = anthropomodel.getAnthropoComponent("upperSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_lower_Width")->getType());
//    targetwidthmiddlevalue = anthropomodel.getAnthropoComponent("middleSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getType());
//    targetwidthlowervalue = anthropomodel.getAnthropoComponent("lowerSection")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_upper_Width")->getType());
//    EXPECT_NEAR(targetwidthlowervalue, 50, 1e-5);
//    EXPECT_NEAR(targetwidthmiddlevalue, 53, 1e-5);
//    EXPECT_NEAR(targetwidthuppervalue, 60, 1e-5);
//}

TEST(anthropomodel_test, segmentintersect) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_segmentintersect.xml");
    anthropomodel.setModel(&hbm);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment2")->getValue(&hbm), 74.2041031, 1e-5);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(80);
    anthropomodel.getAnthropoBodyDimension("Dim_segment2")->setTargetValue(150);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[0], -19.02113, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[1], -19.8438899, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[2], 72.5907399, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[0], -44.4542313, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[1], -30.8778960, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[2], 127.0679066, 1e-4);
}

TEST(anthropomodel_test, segmentintersect2) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_segmentintersect2.xml");
    anthropomodel.setModel(&hbm);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_segment")->getValue(&hbm), 100, 1e-5);
    EXPECT_NEAR(anthropomodel.getAnthropoBodyDimension("Dim_middle_Width")->getValue(&hbm), 40, 1e-5);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(80);
    anthropomodel.getAnthropoBodyDimension("Dim_segment2")->setTargetValue(150);
    anthropomodel.getAnthropoBodyDimension("Dim_middle_Width")->setTargetValue(60);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[0], -19.02113, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[1], -19.8438899, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoord(&hbm).col(1)[2], 72.5907399, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[0], -28.3471239, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[1], -23.8885394, 1e-4);
    EXPECT_NEAR(anthropomodel.getAnthropoComponent("compSegment2")->getComponentCoordTarget(&hbm).col(1)[2], 95.9371109, 1e-4);
}


TEST(anthropomodel_test, test_landmarktargets) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_landTarget.xml");
    anthropomodel.setModel(&hbm);
    // load target file
    TargetList taretlist;
    taretlist.read("target_scaling.ptt");
    //set landmark target for segment
    anthropomodel.setLandmarksTarget(taretlist.landmark);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksSource().size()== 4);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksTargets().size() == 4);
    EXPECT_TRUE(anthropomodel.getSegmentLandmarksTargets().size() == 2);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksTargets().at("end_seg2").use_bone == 0.321);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksTargets().at("end_seg2").use_skin == 0.123);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksTargets().at("point_inf").use_bone == 0.5);
    EXPECT_TRUE(anthropomodel.getFreeLandmarksTargets().at("point_inf").use_skin == 0.5);
    double seg1value = anthropomodel.getAnthropoComponent("seg_1")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("dim_1")->getType());

 }

TEST(anthropomodel_test, test_relativedimension) {
    HumanBodyModel hbm;
    AnthropoModel anthropomodel;
    hbm.setSource("model_Cylinder_description_LSDyna.pmr");
    hbm.fem().updateVTKRepresentation();
    hbm.fem().getFEModelVTK()->setMeshAsEntities(hbm.metadata().entities(), hbm.fem(), true);
    anthropomodel.read("anthropoModelCylinder_widthScaling.xml");
    anthropomodel.setModel(&hbm);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setScalingRelative(true);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(1.5);
    double targetlengthvalue = anthropomodel.getAnthropoComponent("compSegment")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_segment")->getType());
    EXPECT_NEAR(targetlengthvalue, 150, 1e-5);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setScalingRelative(false);
    anthropomodel.getAnthropoBodyDimension("Dim_segment")->setTargetValue(140);
    double targetlengthvalue2 = anthropomodel.getAnthropoComponent("compSegment")->getValueTarget(&hbm, anthropomodel.getAnthropoBodyDimension("Dim_segment")->getType());
    EXPECT_NEAR(targetlengthvalue2, 140, 1e-5);
}
