/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "AnthropoComponent.h"
#include "AnthropoModel.h"

#include "hbm/xmlTools.h"
#include "anatomyDB/query.h"
#include "hbm/HumanBodyModel.h"

#include <vtkAppendFilter.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkPointData.h>
#include <vtkStripper.h>

#include <boost/math/tools/minima.hpp>


#ifdef _MSC_VER
    #define _USE_MATH_DEFINES
    #include <math.h>
#else
    #include <cmath>
#endif

using namespace piper::anthropometricmodel;
using namespace piper::hbm;


AbstractAnthropoComponent::AbstractAnthropoComponent()
 {}

void AbstractAnthropoComponent::parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel)
{
    auto name = element->FirstChildElement("name");
    if (name)
        m_name = name->GetText();
}

void AbstractAnthropoComponent::serializeXml(tinyxml2::XMLElement* element) const
{
    tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
    xmlName->SetText(name().c_str());
    element->InsertEndChild(xmlName);
}

AnthropoComponentType AbstractAnthropoComponent::AnthropoComponentTypefromString(std::string const& typestr) {
    AnthropoComponentType type;
    std::string typestrcopy(typestr);
    std::transform(typestrcopy.begin(), typestrcopy.end(), typestrcopy.begin(), ::tolower);
    if (!typestrcopy.compare("section")){
        type = AnthropoComponentType::SECTION;
    }
    else if (!typestrcopy.compare("segment")){
        type = AnthropoComponentType::SEGMENT;
    }
    else if (!typestrcopy.compare("dimension")){
        type = AnthropoComponentType::DIMENSION;
    }
    else
        throw std::runtime_error("Unknown dimensions type");
    return type;

}

AbstractAnthropoComponentNode::AbstractAnthropoComponentNode(double used_skin, double used_bone)
    : AbstractAnthropoComponent()
    , m_isComputed(false)
    , m_isTargetComputed(false)
    , parent(nullptr)
    , m_position(0.0)
    , m_cp_onparent(false)
    , m_centerParent(false)
    , m_fromParent(false)
    , m_used_bones(used_bone)
    , m_used_skin(used_skin )
    , m_timeModifiedSource(-1)
    , m_timeModifiedTarget(-1)
{
    m_sectionentity.setIdentity();
}

void AbstractAnthropoComponentNode::parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel)
{
    AbstractAnthropoComponent::parseXml(element, anthropomodel);
    if (element->FirstChildElement("controlParameters") != nullptr) {
        element->FirstChildElement("controlParameters")->QueryDoubleAttribute("used_bone", &m_used_bones);
        element->FirstChildElement("controlParameters")->QueryDoubleAttribute("used_skin", &m_used_skin);
    }

    //position on parent
    if (element->FirstChildElement("parentPosition") != nullptr) {
        std::stringstream elemValue(element->FirstChildElement("parentPosition")->GetText());
        element->FirstChildElement("parentPosition")->QueryBoolAttribute("cp_defined", &m_cp_onparent);
        elemValue >> m_position;
        m_fromParent = true;
    }
    if (element->FirstChildElement("centerPosition") != nullptr)
        element->FirstChildElement("centerPosition")->QueryBoolAttribute("onParent", &m_centerParent);

}

void AbstractAnthropoComponentNode::serializeXml(tinyxml2::XMLElement* element) const
{
    AbstractAnthropoComponent::serializeXml(element);
    // control points paremeters
    tinyxml2::XMLElement* xmlParam = element->GetDocument()->NewElement("controlParameters");
    element->InsertEndChild(xmlParam);
    xmlParam->SetAttribute("used_bone", m_used_bones);
    xmlParam->SetAttribute("used_skin", m_used_skin);
    //position on parent
    if (m_fromParent) {
        std::stringstream ss;
        tinyxml2::XMLElement* xmlseg = element->GetDocument()->NewElement("parentPosition");
        ss << std::fixed << std::setprecision(3) << m_position;
        xmlseg->SetText(ss.str().c_str());
        xmlseg->SetAttribute("cp_defined", m_cp_onparent);
        element->InsertEndChild(xmlseg);
    }
    if (m_centerParent) {
        tinyxml2::XMLElement* xmlcenter = element->GetDocument()->NewElement("centerPosition");
        xmlcenter->SetAttribute("onParent", m_centerParent);
        element->InsertEndChild(xmlcenter);
    }
}

void AbstractAnthropoComponentNode::parseXmlNode(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel, std::string const& parentstr) {
    if (parentstr == "none") {
        parent = nullptr;
    }
    else {
        parent = anthropomodel.getAnthropoComponent(parentstr);
    }
    //
    const tinyxml2::XMLElement* xmlNode = element->FirstChildElement("node");
    while (xmlNode != nullptr) {
        children.insert(anthropomodel.getAnthropoComponent(xmlNode->FirstChildElement("name")->GetText()));
        anthropomodel.getAnthropoComponent(xmlNode->FirstChildElement("name")->GetText())->parseXmlNode(xmlNode, anthropomodel, m_name);
        xmlNode = xmlNode->NextSiblingElement("node");
    }
}

void AbstractAnthropoComponentNode::serializeXmlNode(tinyxml2::XMLElement* element) const {
    tinyxml2::XMLElement* xmlNode = element->GetDocument()->NewElement("node");
    element->InsertEndChild(xmlNode);

    tinyxml2::XMLElement* xmlName = element->GetDocument()->NewElement("name");
    xmlName->SetText(name().c_str());
    xmlNode->InsertEndChild(xmlName);
    for (auto const& child : children) {
        child->serializeXmlNode(xmlNode);
    }
}


double AbstractAnthropoComponentNode::getPosition() const { 
    return m_position;
}

void AbstractAnthropoComponentNode::setPosition(double const& value) {
    m_position = value;
    setComputed(false);
}

bool AbstractAnthropoComponentNode::controlPointOnParent() const {
    return m_cp_onparent ;
}

void AbstractAnthropoComponentNode::setControlPointOnParent(bool const& value) {
    if (m_cp_onparent != value) {
        m_cp_onparent = value;
        if (m_cp_onparent)
            m_fromParent = true;
        setComputed(false);
    }
}

void AbstractAnthropoComponentNode::setParent(AbstractAnthropoComponentNode * component) {
    parent = component;
    if (component!= nullptr)
        component->addChild(this);
    setComputed(false);
}

AbstractAnthropoComponentNode* AbstractAnthropoComponentNode::getParent() {
    return parent;
}

void AbstractAnthropoComponentNode::removeParent() {
    if (parent != nullptr) {
        parent->removeChild(this);
        parent = nullptr;
        setComputed(false);
    }
}

void AbstractAnthropoComponentNode::addChild(AbstractAnthropoComponentNode * component) {
    children.insert(component);
}

void AbstractAnthropoComponentNode::removeChild(AbstractAnthropoComponentNode * component) {
    children.erase(component);
}

std::set<AbstractAnthropoComponentNode*>&  AbstractAnthropoComponentNode::getChildren() {
    return children;
}

std::set<AbstractAnthropoComponentNode*>const&  AbstractAnthropoComponentNode::getChildren() const {
    return children;
}

std::list<std::string> AbstractAnthropoComponentNode::getRecursiveChildrenList() const {
    std::list<std::string> list_child;
    for (auto const cur : children) {
        list_child.push_back(cur->name());
        if (cur->getChildren().size() > 0) {
            std::list<std::string> list_curchild = cur->getRecursiveChildrenList();
            list_child.insert(list_child.end(), list_curchild.begin(), list_curchild.end());
        }
    }
    return list_child;
}

bool const& AbstractAnthropoComponentNode::isComputed() const {
    return m_isComputed;
}

bool const& AbstractAnthropoComponentNode::isTargetComputed() const {
    return m_isTargetComputed;
}

void AbstractAnthropoComponentNode::setComputed(bool const& isComputed) {
    m_isComputed = isComputed;
    if (!isComputed) {//need to set childre not computed 
        for (auto & child : children)
            child->setComputed(isComputed);
    }
    if (m_isComputed)
    {        
        m_timeModifiedSource = clock();
        m_timeModifiedTarget = -1;
    }
    else
        setTargetComputed(false);
}

void AbstractAnthropoComponentNode::setTargetComputed(bool const& isComputed) {
    // if components have landtargets, the target is fully described by them, if first via point is not defined on parent components
    if (!isComputed && !m_fromParent && m_landtargets.size() != 0)
        m_isTargetComputed = true;
    else {
        m_isTargetComputed = isComputed;
        if (!isComputed) {
            for (auto & cur : children)
                cur->setTargetComputed(isComputed);
        }
    }
    if (m_isTargetComputed)
        m_timeModifiedTarget = clock();
    else
        m_timeModifiedTarget = -1;
}

clock_t AbstractAnthropoComponentNode::getSourceMTime()
{
    return m_timeModifiedSource;
}

clock_t AbstractAnthropoComponentNode::getTargetMTime()
{
    return m_timeModifiedTarget;
}

void AbstractAnthropoComponentNode::setCenterOnParent(bool const& truefalse) {
    m_centerParent = truefalse;
    setComputed(false);
}

void AbstractAnthropoComponentNode::setAnthropoBodyDimension(AnthropoBodyDimension* dim) {
    m_dim.insert(dim);
    setTargetComputed(false);
}

void AbstractAnthropoComponentNode::removeAnthropoBodyDimension(std::string const& dimname) {
    for (auto & cur : m_dim) {
        if (cur->name() == dimname) {
            m_dim.erase(cur);
            setTargetComputed(false);
        }
        break;
    }
}

Eigen::Matrix3Xd const& AbstractAnthropoComponentNode::getComponentControlPoints(piper::hbm::HumanBodyModel *const model) {
    compute(model);
    return m_controlpoints;
}

Eigen::Matrix3Xd const& AbstractAnthropoComponentNode::getComponentCoord(piper::hbm::HumanBodyModel *const model) {
    compute(model);
    return m_component;
}

Eigen::Matrix3Xd const& AbstractAnthropoComponentNode::getComponentControlPointsTarget(piper::hbm::HumanBodyModel *const model) {
    computeTarget(model);
    return m_controlpointsTarget;
}

Eigen::Matrix3Xd const& AbstractAnthropoComponentNode::getComponentCoordTarget(piper::hbm::HumanBodyModel *const model) {
    computeTarget(model);
    return m_componentTarget;
}

bool AbstractAnthropoComponentNode::compute(piper::hbm::HumanBodyModel *const model) {
    if (isDefined() && !isComputed()) {
        m_component.resize(3, 0);
        m_controlpoints.resize(3, 0);
        m_sectionentity.setIdentity();
        doCompute(model);
    }
    if (!isDefined() || !isComputed()) {
        m_component.resize(3, 0);
        m_controlpoints.resize(3, 0);
        m_sectionentity.setIdentity();
        return false;
    }
    return true;
}

ComponentTransformation const& AbstractAnthropoComponentNode::getTransformationTarget(piper::hbm::HumanBodyModel *const model) {
    computeTarget(model);
    return m_transf;
}

void AbstractAnthropoComponentNode::computeTarget(piper::hbm::HumanBodyModel *const model) {
    compute(model);
    if (isComputed() && !m_isTargetComputed) {
        if (m_landtargets.size() == 0) { // component is driven from target dimension value
            // get weighted scaled factor for each dimension associate with the segment
            Eigen::Vector3d scales(0, 0, 0);
            // compute scaling factors 
            doComputeScaleValue(model, scales);
            //define transformation + parent offset
            ComponentTransformation offsetparent;
            offsetparent.setIdentity();
            if (hasParent()) {
                offsetparent = parent->getTransformationTarget(model);
            }
            m_transf.set(offsetparent, scales, m_sectionentity);
            //apply transformation
            m_componentTarget = m_transf.apply(m_component, m_sectionentity);
            m_controlpointsTarget = m_transf.apply(m_controlpoints, m_sectionentity);
        }
        else // component is driven from target landmarks position
            doComputeTarget(model);
        setTargetComputed(true);
    }
}

double const* AbstractAnthropoComponentNode::getLocalFrameMatrix(piper::hbm::HumanBodyModel *const model) {
    compute(model);
    return &(m_sectionentity.matrix()(0));
}


AnthropoFrame const& AbstractAnthropoComponentNode::getLocalFrame(piper::hbm::HumanBodyModel *const model) {
    compute(model);
    return m_sectionentity;
}

void AbstractAnthropoComponentNode::setIsDefinedFromParent(bool const& truefalse) {
    if (this->type() == AnthropoComponentType::SEGMENT && truefalse != m_fromParent) {
        m_fromParent = truefalse;
        if (!truefalse) {
            m_position = 0.0;
            m_cp_onparent = false;
        }
        setComputed(false);
    }

}

void AbstractAnthropoComponentNode::clearLandmarksTarget() {
    m_landtargets.clear();
    setTargetComputed(false);
}

double const& AbstractAnthropoComponentNode::getControlPointParameter(AnthropoComponentParam const& paramtype) const {
    if (paramtype == AnthropoComponentParam::USE_BONE)
        return m_used_bones;
    else return m_used_skin;
}

void AbstractAnthropoComponentNode::setControlPointParameter(AnthropoComponentParam const& paramtype, double const& value) {
    if (value < 0.0 || value > 1.0)
        throw std::runtime_error("Control point parameters must between 0.0 and 1.0");
    if (paramtype == AnthropoComponentParam::USE_BONE && m_used_bones != value)
    {
        m_used_bones = value;
        m_timeModifiedSource = clock();
        m_timeModifiedTarget = -1;
    }
    else if (paramtype == AnthropoComponentParam::USE_SKIN && m_used_skin != value)
    {
        m_used_skin = value;
        m_timeModifiedSource = clock();
        m_timeModifiedTarget = -1;
    }
}

void AbstractAnthropoComponentNode::resetComponent() {
    setTargetComputed(false);
    setComputed(false);
}

void AbstractAnthropoComponentNode::resetTarget()
{
    setTargetComputed(false);
}

#pragma region AnthropoBodySection

AnthropoBodySection::AnthropoBodySection()
    : AbstractAnthropoComponentNode(1.0, 0.0)
    , m_minmaxWidth(false)
    , m_minmaxDepth(false)
    , m_center(false)
    , m_rotY(0.0)
    , m_rotaxis(0.0)
{
    m_envVTK = vtkSmartPointer<vtkUnstructuredGrid>::New();
    m_envVTK->Allocate();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    m_envVTK->SetPoints(points);
    m_fromParent = true;
}

void AnthropoBodySection::serializeXml(tinyxml2::XMLElement* element) const {

    AbstractAnthropoComponentNode::serializeXml(element);

    // skin region
    tinyxml2::XMLElement* xmlSkin = element->GetDocument()->NewElement("entity");
    hbm::serializeValueCont(xmlSkin, m_entities);
    element->InsertEndChild(xmlSkin);

    // control points
    tinyxml2::XMLElement* xmlCp = element->GetDocument()->NewElement("controlPoint");
    xmlCp->SetAttribute("center", m_center);
    xmlCp->SetAttribute("minmaxWidth", m_minmaxWidth);
    xmlCp->SetAttribute("minmaxDepth", m_minmaxDepth);
    hbm::serializeValueCont(xmlCp, m_pos_controlpoints);
    element->InsertEndChild(xmlCp);
    //oreint
    tinyxml2::XMLElement* xmlroty = element->GetDocument()->NewElement("orient");
    xmlroty->SetText(m_rotY);
    element->InsertEndChild(xmlroty);
    tinyxml2::XMLElement* xmlrotaxis = element->GetDocument()->NewElement("rotaxis");
    xmlrotaxis->SetText(m_rotaxis);
    element->InsertEndChild(xmlrotaxis);
}

void AnthropoBodySection::parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel)
{
    AbstractAnthropoComponentNode::parseXml(element, anthropomodel);
    hbm::parseValueCont(m_entities, element->FirstChildElement("entity"));
    //

    if (element->FirstChildElement("orient") != nullptr) {
        std::stringstream elemValue(element->FirstChildElement("orient")->GetText());
        elemValue >> m_rotY;
    }
    if (element->FirstChildElement("rotaxis") != nullptr) {
        std::stringstream elemValue(element->FirstChildElement("rotaxis")->GetText());
        elemValue >> m_rotaxis;
    }
    //
    element->FirstChildElement("controlPoint")->QueryBoolAttribute("center", &m_center);
    element->FirstChildElement("controlPoint")->QueryBoolAttribute("minmaxWidth", &m_minmaxWidth);
    element->FirstChildElement("controlPoint")->QueryBoolAttribute("minmaxDepth", &m_minmaxDepth);
    hbm::parseValueCont(m_pos_controlpoints, element->FirstChildElement("controlPoint")); //todo, have a unit for angle
    for (auto & cur : m_pos_controlpoints) {
        if (cur > 180)
            cur -= 360;
    }
    setComputed(false);
}

void AnthropoBodySection::checkmetadata(piper::hbm::HumanBodyModel const * const model) {
    // section entity
    std::set<std::string> m_entitiesmodel;
    if (m_entities.size() > 0) {
        for (auto const& cur : m_entities) {
            // deal with synonyms
            std::vector<std::string> syn;
            if (anatomydb::isAnatomicalEntity(cur))
                syn = anatomydb::getSynonymList(cur);
            else
                syn.push_back(cur);
            for (auto const& cur : syn) {
                if (model->metadata().hasEntity(cur)) {
                    m_entitiesmodel.insert(cur);
                    break;
                }
                std::stringstream str;
                str << "[FAILED] Definition of Body section section: " << std::endl;
                str << "For Body section " << m_name << ": Entity " << cur << " is not defined." << endl;
                throw std::runtime_error(str.str().c_str());
                return;
            }
        }
        m_entities = m_entitiesmodel;
    }
}

bool AnthropoBodySection::checkComponentDimensions() const {
    // if only one dim, it is ok
    if (m_dim.size() <= 1)
        return true;

    int nb_circ = 0;
    int nb_depth = 0;
    int nb_width = 0;
    for (auto const& cur : m_dim) {
        AnthropoDimensionType const& dimtyp = cur->getType();
        if (dimtyp == AnthropoDimensionType::SECTION_CIRC)
            nb_circ += 1;
        else if (dimtyp == AnthropoDimensionType::SECTION_WIDTH)
            nb_width += 1;
        else
            nb_depth += 1;
    }
    if ((nb_circ > 0 && nb_width>0 && nb_depth>0)) {
        return false;
    }
    return true;
}

bool AnthropoBodySection::hasControlPointatCenter() const { return m_center; }

void AnthropoBodySection::setControlPointatCenter(bool const& truefalse) {
    m_center = truefalse;
    setComputed(false);
}

bool AnthropoBodySection::hasControlPointatMinMaxWidth() const { return m_minmaxWidth; }

void AnthropoBodySection::setControlPointatMinMaxWidth(bool const& truefalse) {
    m_minmaxWidth = truefalse; 
    setComputed(false);
}

bool AnthropoBodySection::hasControlPointatMinMaxDepth() const { return m_minmaxDepth; }

void AnthropoBodySection::setControlPointatMinMaxDepth(bool const& truefalse) {
    m_minmaxDepth = truefalse; 
    setComputed(false);
}

std::vector<double> const& AnthropoBodySection::getcontrolPointsPosition() const { return m_pos_controlpoints; }

void AnthropoBodySection::setcontrolPointsPosition(std::vector<double> const& values) { 
    m_pos_controlpoints = values; 
    setComputed(false);
}

std::set<std::string> const& AnthropoBodySection::getEntity() const {
    return m_entities;
}

void AnthropoBodySection::setEntity(std::vector<std::string> const& entitiesname) {
    for (auto const& cur : entitiesname)
        m_entities.insert(cur);
    m_envVTK = vtkSmartPointer<vtkUnstructuredGrid>::New();
    setComputed(false);
}

void AnthropoBodySection::setEntity(std::string const& entityname) {
    std::vector<std::string> ventityname;
    ventityname.push_back(entityname);
    setEntity(ventityname);
}

void AnthropoBodySection::removeEntity(std::string const& entityname) {
    m_entities.erase(entityname);
    setComputed(false);
}

double AnthropoBodySection::getRotationAngleAxial() const { return m_rotaxis; }

void AnthropoBodySection::setRotationAngleAxial(double const& value) {
    m_rotaxis = value;
    setComputed(false);
}

double AnthropoBodySection::getTiltangle() const { return m_rotY; }

void AnthropoBodySection::setTiltangle(double const& value) {
    m_rotY = value;
    setComputed(false);
}


AnthropoFrame AnthropoBodySection::getPointatPosition(piper::hbm::HumanBodyModel *const model, double const& position) {
    AnthropoFrame local;
    local.setIdentity();
    double positiondegree = position * 360;
    if (positiondegree >= 0 && positiondegree <= 360 && compute(model)) {
        m_component = m_sectionentity.inverse() * m_component;
        Eigen::VectorXd angle, radius;
        radius.resize(m_component.cols());
        angle.resize(m_component.cols());
        for (int n = 0; n < m_component.cols(); ++n) {
            angle(n) = std::atan2(m_component.col(n)(1), m_component.col(n)(0)) * 180 / M_PI;
            if (angle(n) < 0)
                angle(n) += 360;
            radius(n) = std::sqrt((std::pow(m_component.col(n)(1), 2) + std::pow(m_component.col(n)(0), 2)));
        }
        double weight, weight2;
        int index, index2, index_first, index_second;
        (angle.array() - positiondegree).abs().matrix().minCoeff(&index);
        if (index == 0) {
            index_first = angle.size() - 1;
            index_second = 1;
        }
        else if (index == angle.size() - 1) {
            index_first = angle.size() - 2;;
            index_second = 0;
        }
        else {
            index_first = index - 1;
            index_second = index + 1;
        }
        index2 = (angulardistance(angle(index_first), positiondegree) >= 0 && angulardistance(angle(index), positiondegree) <= 0) ? index_first : index_second;
        weight = fabs(angulardistance(angle(index2), positiondegree) / angulardistance(angle(index), angle(index2)));
        weight2 = fabs(angulardistance(angle(index), positiondegree) / angulardistance(angle(index), angle(index2)));
        m_component = m_sectionentity * m_component;
        local = m_sectionentity;
        local.translation() = m_component.col(index) * weight + m_component.col(index2) * weight2;
    }
    return local;
}
double AnthropoBodySection::getValueTarget(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) {
    double value = -1.0;
    computeTarget(model);    
    if (!m_isTargetComputed)
        return value;

    Eigen::Matrix3Xd m_componentTargetLocal = m_transf.TargetintoLocal(m_componentTarget, m_sectionentity);
    switch (type) {
    case AnthropoDimensionType::SECTION_CIRC: {
        value = computeSectionCircumference(m_componentTargetLocal);
        break;
    }
    case AnthropoDimensionType::SECTION_DEPTH: {
        value = computeSectionDetph(m_componentTargetLocal);
        break;
    }
    case AnthropoDimensionType::SECTION_WIDTH: {
        value = computeSectionWidth(m_componentTargetLocal);
        break;
    }
    default:
        throw std::runtime_error("Unknown dimensions type for a body section");
    }
    return value;
}

double AnthropoBodySection::getValue(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) {
    double value = -1.0;
    if (!compute(model))
        return value;
    if (m_component.cols() > 0) //a bodysection contour really exists 
    {
        m_component = m_sectionentity.inverse() * m_component;
        switch (type) {
        case AnthropoDimensionType::SECTION_CIRC: {
            value = computeSectionCircumference(m_component);
            break;
        }
        case AnthropoDimensionType::SECTION_DEPTH: {
            value = computeSectionDetph(m_component);
            break;
        }
        case AnthropoDimensionType::SECTION_WIDTH: {
            value = computeSectionWidth(m_component);
            break;
        }
        default:
            throw std::runtime_error("Unknown dimensions type for a body section");
        }
        m_component = m_sectionentity * m_component;
    }
    return value;
}

void AnthropoBodySection::optimScalingSection(double const& scaleX, double const& scaleY, double& scaleCirc, Eigen::Matrix3Xd component, double const& targevalue) {
    Eigen::Vector3d _scales;
    if (scaleX == 0.0) {
        component.row(0).swap(component.row(1));
        _scales[0] = scaleY;
        _scales[1] = scaleX;
        _scales[2] = 1;
    }
    else {
        _scales[0] = scaleX;
        _scales[1] = scaleY;
        _scales[2] = 1;
    }
    optimObjectiveFuntion myOptim(component, _scales, targevalue);
    int bits = std::numeric_limits<double>::digits;
    std::pair<double, double> r = boost::math::tools::brent_find_minima(myOptim, 0.2, 4.0, bits);
    scaleCirc = r.first;
    }

double AnthropoBodySection::computeSectionCircumference(Eigen::Matrix3Xd const& component) {
    double value = 0;
    for (int n = 0; n < component.cols() - 1; ++n)
        value += (component.col(n) - component.col(n + 1)).norm();
    value += (component.col(component.cols() - 1) - component.col(0)).norm();
    return value;
}

double AnthropoBodySection::computeSectionDetph(Eigen::Matrix3Xd const& component) {
    double value = 0;
    value = component.row(0).maxCoeff() - component.row(0).minCoeff();
    return value;
}

double AnthropoBodySection::computeSectionWidth(Eigen::Matrix3Xd const& component) {
    double value = 0;
    value = component.row(1).maxCoeff() - component.row(1).minCoeff();
    return value;
}


bool AnthropoBodySection::doComputeScaleValue(piper::hbm::HumanBodyModel *const model, Eigen::Vector3d & scales) {
    // first apply non circ dimgetTargetValue
    for (auto const& cur : m_dim) {
        if (cur->isDefined() && cur->isTargetValueDefined()) {
            if (cur->getType() != AnthropoDimensionType::SECTION_CIRC) {
                double current = cur->getValue(model);
                double target = cur->getTargetValue(model);
                double weight = cur->getScalingWeight(this);
                double scale = weight * target / current;
                if (cur->getType() == AnthropoDimensionType::SECTION_DEPTH) {
                    scales[0] += scale;
                }
                else if (cur->getType() == AnthropoDimensionType::SECTION_WIDTH) {
                    scales[1] += scale;
                }
            }
        }
        else
            return false;
    }
    double scale = 0.0;
    double target = 0.0;
    for (auto const& cur : m_dim) {
        if (cur->isDefined() && cur->isTargetValueDefined()) {
            if (cur->getType() == AnthropoDimensionType::SECTION_CIRC) {
                double current = cur->getValue(model);
                double curTarget = cur->getTargetValue(model);
                double weight = cur->getScalingWeight(this);
                target += weight * curTarget;
                scale += weight * curTarget / current;
            }
        }
        else
            return false;
    }
    if (scale != 0.0) {
        if (scales[0] != 0 || scales[1] != 0) {
            optimScalingSection(scales[0], scales[1], scale, m_sectionentity.inverse() * m_component, target);
            if (scales[0] != 0)
                scales[1] = scale;
            if (scales[1] != 0)
                scales[0] = scale;
        }
        else {
            scales[0] += scale;
            scales[1] += scale;
        }
    }

    return true;
}

void AnthropoBodySection::doComputeTarget(piper::hbm::HumanBodyModel *const model) {
    /// nothing, should never go there as we don't have section target define by landmark..but wu could have one
}

void AnthropoBodySection::doCompute(piper::hbm::HumanBodyModel *const model) {
    setComputed(false);
    m_sectionentity = parent->getPointatPosition(model, m_position);
    // position of center and axis of the section
    if (m_rotaxis != 0.0) {
        double m_rotaxis_rad = m_rotaxis * M_PI / 180;
        Eigen::AngleAxisd rotation(m_rotaxis_rad, m_sectionentity.linear().col(2));
        Eigen::Isometry3d transformation;
        transformation.setIdentity();
        transformation = rotation * transformation;
        hbm::Coord origin = m_sectionentity.translation();
        m_sectionentity.translation() = hbm::Coord(0, 0, 0);
        m_sectionentity = rotation * m_sectionentity;
        m_sectionentity.translation() = origin;
    }
    if (m_rotY != 0.0) {
        double m_rotY_rad = m_rotY * M_PI / 180;
        Eigen::AngleAxisd rotation(m_rotY_rad, m_sectionentity.linear().col(1));
        Eigen::Isometry3d transformation;
        transformation.setIdentity();
        transformation = rotation * transformation;
        hbm::Coord origin = m_sectionentity.translation();
        m_sectionentity.translation() = hbm::Coord(0, 0, 0);
        m_sectionentity = transformation * m_sectionentity;
        m_sectionentity.translation() = origin;
    }
    // identify if we delas with a bone entity or a skin one

    //define vtk object that gather all skin entities
    vtkSmartPointer<vtkAppendFilter> appendFilter = vtkSmartPointer<vtkAppendFilter>::New();
    if (m_envVTK->GetNumberOfPoints() == 0) {
        // combine skin regions entioty meshes
        for (auto const& cur : m_entities) {
            if (anatomydb::isSkin(cur)) {
                appendFilter->AddInputData(model->fem().getFEModelVTK()->getEntity(cur));
            }
            else { // need to extract "real env" in a VTK mesh
                std::vector<ElemDef> elemdef;
                VId envNodeId;
                bool isMeshDefIndex = model->fem().getMeshDefType() == MeshDefType::INDEX;
                elemdef = model->metadata().entity(cur).getEnvelopElements(model->fem());
                envNodeId = model->metadata().entity(cur).getEnvelopNodes(model->fem());
                vtkSmartPointer<vtkUnstructuredGrid> curentityVTKenv = vtkSmartPointer<vtkUnstructuredGrid>::New();
                curentityVTKenv->Allocate();
                vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                curentityVTKenv->SetPoints(points);
                points->SetNumberOfPoints(envNodeId.size());
                int i = 0;
                std::map<Id, Id> inverseNid;
                for (auto const& curid : envNodeId) {
                    //for (std::shared_ptr<Node> n : model->fem().getNodes()) {
                    double coord[3];
                    std::shared_ptr<Node> n = model->fem().get<Node>(curid);
                    int feNodeId = isMeshDefIndex ? i : n->getId();
                    coord[0] = n->getCoordX();
                    coord[1] = n->getCoordY();
                    coord[2] = n->getCoordZ();
                    points->SetPoint(i, coord);
                    inverseNid[curid] = i;
                    i++;
                }
                vtkIdType list[4];
                for (auto & cur : elemdef) {
                    for (auto n = 0; n < cur.size(); ++n)
                        list[n] = isMeshDefIndex ? cur[n] : inverseNid[cur[n]];
                    curentityVTKenv->InsertNextCell(cur.size() == 3 ? VTK_TRIANGLE : VTK_QUAD, cur.size(), list);
                }
                appendFilter->AddInputData(curentityVTKenv);
            }
        }
        appendFilter->Update();
        if (appendFilter->GetOutput()->GetNumberOfPoints() > 0) {
            m_envVTK->DeepCopy(appendFilter->GetOutput());
        }
    }
    // compute intersection between skin entities entity and plan section
    vtkSmartPointer<vtkPlane> plane = vtkSmartPointer<vtkPlane>::New();
    plane->SetOrigin(&m_sectionentity.translation()[0]);
    plane->SetNormal(&m_sectionentity.linear().col(2)[0]);
    vtkSmartPointer<vtkCutter> bodycutter = vtkSmartPointer<vtkCutter>::New();
    bodycutter->SetCutFunction(plane);
    bodycutter->SetInputData(m_envVTK);
    bodycutter->Update();
    // use of stripper to define intersection contours
    vtkSmartPointer<vtkStripper> stripper = vtkSmartPointer<vtkStripper>::New();
    stripper->SetInputConnection(bodycutter->GetOutputPort());
    stripper->Update();
    vtkIdType numberOfLines = stripper->GetOutput()->GetNumberOfLines();
    // get points of each contour, using topology ordering
    std::vector<Eigen::Matrix3Xd> contours;
    Coord parentCenterTranslation(0, 0, 0);
    if (bodycutter->GetOutput()->GetPoints()->GetNumberOfPoints() > 0) {
        vtkPoints *points = stripper->GetOutput()->GetPoints();
        vtkCellArray *cells = stripper->GetOutput()->GetLines();
        vtkIdType *indices;
        vtkIdType numberOfPoints;
        unsigned int lineCount = 0;
        double dist = 1e18;
        for (cells->InitTraversal();
            cells->GetNextCell(numberOfPoints, indices);
            lineCount++)  {
            Coord centroid(0.0, 0.0, 0.0);
            Eigen::Matrix3Xd tmp;
            tmp.resize(3, (int)numberOfPoints);
            for (vtkIdType i = 0; i < numberOfPoints; i++) {
                double point[3];
                points->GetPoint(indices[i], point);
                centroid[0] += point[0];
                centroid[1] += point[1];
                centroid[2] += point[2];
                tmp.col(i)[0] = point[0];
                tmp.col(i)[1] = point[1];
                tmp.col(i)[2] = point[2];
            }
            centroid[0] = centroid[0] / numberOfPoints;
            centroid[1] = centroid[1] / numberOfPoints;
            centroid[2] = centroid[2] / numberOfPoints;
            centroid = m_sectionentity.inverse() * centroid;
            if (centroid.norm() < dist) {
                dist = centroid.norm();
                m_component.resize(3, numberOfPoints);
                m_component = tmp;
            }
        }
        // define section center on centroid contour
        parentCenterTranslation = m_sectionentity.translation();
        m_sectionentity.translation() = m_component.rowwise().mean();
        AnthropoFrame sectionentityInv = m_sectionentity.inverse();
        m_component = sectionentityInv * m_component;
        //compute control points
        int n = 0;
        if (m_center)
            n++;
        if (m_minmaxWidth) {
            n++;
            n++;
        }
        if (m_minmaxDepth) {
            n++;
            n++;
        }
        if (m_cp_onparent)
            n++;
        n += (int)m_pos_controlpoints.size();
        if (n > 0) {
            m_controlpoints.resize(3, n);
            int count = 0;
            if (m_cp_onparent) {
                //AnthropoFrame tmp = parent->getPointatPosition(model, m_position);
                m_controlpoints.col(count) = parentCenterTranslation;
                count++;
            }

            if (m_center) {
                m_controlpoints.col(count) = m_sectionentity * hbm::Coord(0.0, 0.0, 0.0);
                count++;
            }
            if (m_minmaxWidth) {
                int index;
                m_component.row(1).maxCoeff(&index); // max 
                m_controlpoints.col(count) = m_sectionentity*m_component.col(index);
                count++;
                m_component.row(1).minCoeff(&index); // min 
                m_controlpoints.col(count) = m_sectionentity*m_component.col(index);
                count++;
            }

            if (m_minmaxDepth) {
                int index;
                m_component.row(0).maxCoeff(&index); // max X
                m_controlpoints.col(count) = m_sectionentity*m_component.col(index);
                count++;
                m_component.row(0).minCoeff(&index); // min X
                m_controlpoints.col(count) = m_sectionentity*m_component.col(index);
                count++;
            }

            if (m_pos_controlpoints.size() > 0) {
                Eigen::VectorXd angle, radius;
                radius.resize(m_component.cols());
                angle.resize(m_component.cols());
                for (int n = 0; n < m_component.cols(); ++n) {
                    angle(n) = std::atan2(m_component.col(n)(1), m_component.col(n)(0));
                    radius(n) = std::sqrt((std::pow(m_component.col(n)(1), 2) + std::pow(m_component.col(n)(0), 2)));
                }
                for (auto const& cur : m_pos_controlpoints) {
                    double curangle = cur * M_PI / 180;
                    double weight, weight2;
                    int index, index2, index_first, index_second;
                    (angle.array() - curangle).abs().matrix().minCoeff(&index);
                    if (index == 0) {
                        index_first = angle.size() - 1;
                        index_second = 1;
                    }
                    else if (index == angle.size() - 1) {
                        index_first = angle.size() - 2;;
                        index_second = 0;
                    }
                    else {
                        index_first = index - 1;
                        index_second = index + 1;
                    }
                    index2 = (angulardistance(angle(index_first), curangle) >= 0 && angulardistance(angle(index), curangle) <= 0) ? index_first : index_second;
                    weight = fabs(angulardistance(angle(index2), curangle) / angulardistance(angle(index), angle(index2)));
                    weight2 = fabs(angulardistance(angle(index), curangle) / angulardistance(angle(index), angle(index2)));
                    hbm::Coord new_pos1, new_pos2;
                    new_pos1(0) = radius(index) * std::cos(angle(index));
                    new_pos1(1) = radius(index) * std::sin(angle(index));
                    new_pos1(2) = 0.0;
                    new_pos2(0) = radius(index2) * std::cos(angle(index2));
                    new_pos2(1) = radius(index2) * std::sin(angle(index2));
                    new_pos2(2) = 0.0;

                    hbm::Coord new_pos = new_pos1 * weight + new_pos2 * weight2;
                    m_controlpoints.col(count) = m_sectionentity * new_pos;
                    count++;
                }
            }
        }
    }
    else {
        m_controlpoints.resize(0, 0);
        m_component.resize(0, 0);
    }
    m_component = m_sectionentity * m_component;
    if (m_centerParent)
        m_sectionentity.translation() = parentCenterTranslation;
    setComputed(true);
}

double AnthropoBodySection::angulardistance(double const& v1, double const& v2) {
    double dist = v2 - v1;
    dist += (dist > 180) ? -360 : (dist < -180) ? 360 : 0;
    return dist;
}

bool AnthropoBodySection::isDefined() const {
    return (parent != nullptr && m_entities.size() > 0 && parent->isDefined());

}

void AnthropoBodySection::resetComponent() {
    AbstractAnthropoComponentNode::resetComponent();
    m_envVTK = vtkSmartPointer<vtkUnstructuredGrid>::New();
    m_envVTK->Allocate();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    m_envVTK->SetPoints(points);
}

#pragma endregion AnthropoBodySection

#pragma region AnthropoBodyDimension

AnthropoBodyDimension::AnthropoBodyDimension()
    : AbstractAnthropoComponent()
    , m_targetvalue(-1.0)
    , m_componentReference(nullptr)
    , m_relativescaling(false) {}

void AnthropoBodyDimension::serializeXml(tinyxml2::XMLElement* element) const {

    AbstractAnthropoComponent::serializeXml(element);

    //type
    tinyxml2::XMLElement* xmltype = element->GetDocument()->NewElement("type");
    std::string lowertype(AnthropoDimensionType_str[(unsigned int)m_type]);
    std::transform(lowertype.begin(), lowertype.end(), lowertype.begin(), ::tolower);
    xmltype->SetText(lowertype.c_str());
    element->InsertEndChild(xmltype);
    // relative scaling
    tinyxml2::XMLElement* xmlscaling = element->GetDocument()->NewElement("scaling");
    xmlscaling->SetAttribute("relative", m_relativescaling);
    element->InsertEndChild(xmlscaling);
    //component ref
    if (m_componentReference != nullptr) {
        tinyxml2::XMLElement* xmlcompref = element->GetDocument()->NewElement("componentReference");
        xmlcompref->SetAttribute("name", m_componentReference->name().c_str());
        element->InsertEndChild(xmlcompref);
    }
    // component & weight
    if (m_component.size() > 0) {
        for (auto const& comp : m_component) {
            tinyxml2::XMLElement* xmlcomp = element->GetDocument()->NewElement("component");
            tinyxml2::XMLElement* xmlcompname = element->GetDocument()->NewElement("name");
            xmlcompname->SetText(comp.first->name().c_str());
            xmlcomp->InsertEndChild(xmlcompname);
            std::stringstream ss;            
            tinyxml2::XMLElement* xmlcompweight = element->GetDocument()->NewElement("weight");
            ss << std::fixed << std::setprecision(3) << comp.second;
            xmlcompweight->SetText(ss.str().c_str());
            xmlcomp->InsertEndChild(xmlcompweight);
            element->InsertEndChild(xmlcomp);
        }
    }
}

void AnthropoBodyDimension::parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel) {
    AbstractAnthropoComponent::parseXml(element, anthropomodel);

    setType(element->FirstChildElement("type")->GetText());
    if (element->FirstChildElement("scaling") != nullptr)
        element->FirstChildElement("scaling")->QueryBoolAttribute("relative", &m_relativescaling);
        //m_relativescaling = element->FirstChildElement("scaling")->Attribute("relative");
    if (element->FirstChildElement("componentReference") != nullptr) {
        std::string componentname = element->FirstChildElement("componentReference")->Attribute("name");
        if ((anthropomodel.hasAnthropoBodySegment(componentname) && isSegmentDimensionType(m_type)) ||
            (anthropomodel.hasAnthropoBodySection(componentname) && !isSegmentDimensionType(m_type))) {
            m_componentReference = anthropomodel.getAnthropoComponent(componentname);
        }
        else {
            std::stringstream str;
            str << "[FAILED] Definition of Body dimension: " << std::endl;
            str << "For Body dimension " << m_name << ": component " << componentname << " is not defined or is not of the right type (segment vs section)." << endl;
            throw std::runtime_error(str.str().c_str());
            return;
        }
        const tinyxml2::XMLElement* xmlComp = element->FirstChildElement("component");
        while (xmlComp != nullptr) {
            componentname = xmlComp->FirstChildElement("name")->GetText();
            std::stringstream elemValue(xmlComp->FirstChildElement("weight")->GetText());
            double weight;
            elemValue >> weight;
            if ((anthropomodel.hasAnthropoBodySegment(componentname) && isSegmentDimensionType(m_type)) ||
                (anthropomodel.hasAnthropoBodySection(componentname) && !isSegmentDimensionType(m_type))) {
                m_component[anthropomodel.getAnthropoComponent(componentname)] = weight;
                anthropomodel.getAnthropoComponent(componentname)->setAnthropoBodyDimension(this);
            }
            else {
                std::stringstream str;
                str << "[FAILED] Definition of Body dimension: " << std::endl;
                str << "For Body dimension " << m_name << ": component " << componentname << " is not defined or is not of the right type (segment vs section)." << endl;
                throw std::runtime_error(str.str().c_str());
                return;
            }
            xmlComp = xmlComp->NextSiblingElement("component");
        }
    }
}

double AnthropoBodyDimension::getTargetValue(piper::hbm::HumanBodyModel *const model) const {
    if (!m_relativescaling)
        return m_targetvalue;
    else
        return std::abs(m_targetvalue) * this->getValue(model);
}

std::map<AbstractAnthropoComponentNode*, double> const& AnthropoBodyDimension::getAnthropoComponent() const {
    return m_component;
}

bool AnthropoBodyDimension::setType(std::string const& type) {
    return setType(AnthropoBodyDimensionTypefromString(type));
}

bool AnthropoBodyDimension::setType(AnthropoDimensionType const& type) {
    AnthropoDimensionType oldtype = m_type;
    m_type = type;
    for (auto & cur : m_component) {
        if (!cur.first->checkComponentDimensions()) {
            m_type = oldtype;
            return false;
        }
    }
    for (auto & cur : m_component)
        cur.first->resetTarget();
    return true;
}

AnthropoDimensionType const& AnthropoBodyDimension::getType() const {
    return m_type;
}

bool AnthropoBodyDimension::setAnthropoComponent(AbstractAnthropoComponentNode* component, double const& weight) {
    // first check if it define an over constrained component
    component->setAnthropoBodyDimension(this);
    if (!component->checkComponentDimensions()) {
        component->removeAnthropoBodyDimension(this->name());
        return false;
    }
    else {
        m_component[component] = weight;
        //component->setTargetComputed(false);
        return true;
    }
}

void AnthropoBodyDimension::removeAnthropoComponent(std::string const& name) {
    //if (this->m_componentReference != nullptr && this->m_componentReference->name() == name)
    //    this->removeReferenceAnthropoComponent();
    for (auto cur : m_component) {
        if (cur.first->name() == name) {
            cur.first->removeAnthropoBodyDimension(this->m_name);
            m_component.erase(cur.first);
            break;
        }
    }
}

bool AnthropoBodyDimension::setReferenceAnthropoComponent(AbstractAnthropoComponentNode* component) {
    m_componentReference = component;
    if (!setAnthropoComponent(m_componentReference))
        return false;
    for (auto & cur : m_component)
        cur.first->resetTarget();
    return true;
}

std::string AnthropoBodyDimension::getReferenceAnthropoComponent() const {
    if (m_componentReference != nullptr)
        return m_componentReference->name();
    else
        return "";
}

void AnthropoBodyDimension::removeReferenceAnthropoComponent() {
    m_componentReference = nullptr;
    for (auto & cur : m_component)
        cur.first->resetTarget();
    //m_component.clear();
}

bool AnthropoBodyDimension::setTargetValue(double const& value) {
    m_targetvalue = value;
    if (value != -1) {
        m_componentReference->clearLandmarksTarget();
        for (auto & comp : m_component)
            comp.first->clearLandmarksTarget();
    }
    for (auto & cur : m_component)
        cur.first->resetTarget();
    return true;
}

bool AnthropoBodyDimension::setTargetValue(piper::hbm::AnthropometricDimensionTarget const& target) {
    bool spy = false;
    // if relative scaling dimension
    if (m_relativescaling && target.hasScaleValue()) {
        m_targetvalue = target.scale();
        spy = true;
    }
    else if (!m_relativescaling && target.hasAbsoluteValue()) {
        m_targetvalue = target.value();
        spy = true;
    }
    for (auto & cur : m_component)
        cur.first->resetTarget();
    return spy;
}

bool AnthropoBodyDimension::isTargetValueDefined() const {
    return m_targetvalue != -1.0;
}

AnthropoDimensionType AnthropoBodyDimension::AnthropoBodyDimensionTypefromString(std::string const& typestr) {
    AnthropoDimensionType type;
    std::string typestrcopy(typestr);
    std::transform(typestrcopy.begin(), typestrcopy.end(), typestrcopy.begin(), ::tolower);
    if (!typestrcopy.compare("section_circ")){
        type = AnthropoDimensionType::SECTION_CIRC;
    }
    else if (!typestrcopy.compare("section_width")){
        type = AnthropoDimensionType::SECTION_WIDTH;
    }
    else if (!typestrcopy.compare("section_depth")){
        type = AnthropoDimensionType::SECTION_DEPTH;
    }
    else if (!typestrcopy.compare("segment_length")){
        type = AnthropoDimensionType::SEGMENT_LENGTH;
    }
    else if (!typestrcopy.compare("segment_multilength")){
        type = AnthropoDimensionType::SEGMENT_MULTILENGTH;
    }
    else
        throw std::runtime_error("Unknown dimensions type");
    return type;
}

bool AnthropoBodyDimension::isDefined() const {
    return (m_componentReference != nullptr && m_componentReference->isDefined());
}

double AnthropoBodyDimension::getScalingWeight(AbstractAnthropoComponentNode* comp) const {
    double value = -1.0;

    if ((m_component.find(comp) != m_component.end()) && isTargetValueDefined()) {
        value = m_component.at(comp);
    }
    return value;
}

bool AnthropoBodyDimension::isSegmentDimensionType(AnthropoDimensionType const& type) {
    std::string type_str = AnthropoDimensionType_str[(unsigned int)type];
    if (type_str.substr(0, 7) == "SECTION")
        return false;
    return true;
}

double AnthropoBodyDimension::getValue(piper::hbm::HumanBodyModel *const model) const {
    double value = -1.0;
    if (isDefined()) {
        value = m_componentReference->getValue(model, m_type);
    }
    return value;
}

bool const& AnthropoBodyDimension::isScalingRelative() const {
    return m_relativescaling;
}


void AnthropoBodyDimension::setScalingRelative(bool const& truefalse) {
    m_relativescaling = truefalse;
    setTargetValue(-1.0);
}


#pragma endregion AnthropoBodyDimension

optimObjectiveFuntion::optimObjectiveFuntion(Eigen::Matrix3Xd component, Eigen::Vector3d scalingmatrix, double const& targetvalue)
    : _component(component)
    , _scalingmatrix(scalingmatrix)
    , _target(targetvalue) {}

optimObjectiveFuntion::optimObjectiveFuntion(const optimObjectiveFuntion &obj) {
    _component = obj._component;
    _scalingmatrix = obj._scalingmatrix;
    _target = obj._target;
}
