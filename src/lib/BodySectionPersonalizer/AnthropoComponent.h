/*******************************************************************************
* Copyright (C) 2017 CEESAR                                                    *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Erwan Jolivet (CEESAR)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_ANTHROPOCOMPONENT_H
#define PIPER_ANTHROPOCOMPONENT_H

#ifdef WIN32
#	ifdef AnthropoModel_EXPORTS
#		define ANTHROPOMODEL_EXPORT __declspec( dllexport )
#	else
#		define ANTHROPOMODEL_EXPORT __declspec( dllimport )
#	endif
#else
#	define ANTHROPOMODEL_EXPORT
#endif

#include "ComponentTransformation.h"

#include "hbm/target.h"

namespace piper {

    namespace hbm {
        class HumanBodyModel;
    }
    namespace anthropometricmodel {
        typedef Eigen::Affine3d AnthropoFrame;
        typedef Eigen::DiagonalMatrix<double, 3> ScalingMatrix;
        class AnthropoModel;
        /*!
        *  \brief Defined type of body section dimension
        *
        */
        enum class AnthropoDimensionType {                  
            SECTION_WIDTH = 0,     ///< defined a  width
            SECTION_DEPTH,     ///< defined a  depth
            SECTION_CIRC,   ///< defined a skin circumference
            SEGMENT_LENGTH,     ///< defined a  length
            SEGMENT_MULTILENGTH     ///< defined a length on multi linear segment
        };

        /*!
        *  \brief Defined string equivalent to AnthropoDimensionType
        *
        */
        static const std::array<std::string, 5> AnthropoDimensionType_str = { { "SECTION_WIDTH", "SECTION_DEPTH", "SECTION_CIRC", "SEGMENT_LENGTH", "SEGMENT_MULTILENGTH" } };

        enum class AnthropoComponentType {
            SEGMENT = 1,     ///< 
            SECTION = 2,     ///< 
            DIMENSION = 4,     ///< 
            ALL = 0x7FFFFFFF
        };


        ANTHROPOMODEL_EXPORT inline unsigned int operator&(AnthropoComponentType a, unsigned int b)
        {
            return static_cast<unsigned int>(static_cast<unsigned int>(a) & b);
        }

        /*!
        *  \brief Defined string equivalent to AnthropoDimensionType
        *
        */
        static const std::map<AnthropoComponentType, std::string> AnthropoComponentType_str = { std::make_pair(AnthropoComponentType::SEGMENT, "SEGMENT"),
            std::make_pair(AnthropoComponentType::SECTION, "SECTION"), std::make_pair(AnthropoComponentType::DIMENSION, "DIMENSION") };
        
        enum class AnthropoComponentParam {
            USE_BONE = 0,     ///< 
            USE_SKIN,     ///< 
        };

        struct Landmarkconstraint {
            Landmarkconstraint() {};
            Landmarkconstraint(std::string const& name, hbm::Coord const& coord, double const& use_bone = 0.5, double const& use_skin = 0.5);
            std::string name;
            double use_bone, use_skin;
            hbm::Coord coord;
        };


        /** Base class for classes which anthropometric component (body section & body segment &  body dimension).
        *
        * A anthropometric component classe should define
        * - a static AnthropoComponentType type(); to return a string representation of their type.
        * - a checkmetadata to check if metadata used to defined the anthropometric component exist
        * - parseXml/serializeXml the target to/from an xml file.
        * - a isDefined(); equal to true when the component is defined (meaning it can be display for example)
        */
        class ANTHROPOMODEL_EXPORT AbstractAnthropoComponent {
        public:

            AbstractAnthropoComponent();
            virtual ~AbstractAnthropoComponent() {}

            /// set \a name of the component 
            void setName(std::string const& name) { m_name = name; }

            /// get \a name of the component 
            std::string const& name() const { return m_name; }

            /// parse this target from \a element
            virtual void parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);

            /// add xml representation of this component to \a element
            virtual void serializeXml(tinyxml2::XMLElement* element) const;

            /*!
            *  \brief get type of the component
            *
            * \return: the AnthropoComponentType
            */
            virtual AnthropoComponentType type() const = 0;
            /*!
            *  \brief check if metadata that support body segment definition are defined
            *
            * \param model: a human body model
            */
            virtual void checkmetadata(piper::hbm::HumanBodyModel const * const model) = 0;
            /*!
            *  \brief check if the component is defined (with minimal description available)
            *
            * \return: true is component is defined
            */
            virtual bool isDefined() const=0;

            /// get AnthropoComponentType from string that define the type
            static AnthropoComponentType AnthropoComponentTypefromString(std::string const& typestr);

        protected:
            /// a component has a name
            std::string m_name;
        };

        class AnthropoBodyDimension;
        /** Base class for classes which anthropometric component Node (body section & body segment).
        *
        * A anthropometric component class should define
        *  
        */
        class ANTHROPOMODEL_EXPORT AbstractAnthropoComponentNode: public AbstractAnthropoComponent {
        public:
            AbstractAnthropoComponentNode(double used_skin, double used_bone);
            /// parse this target from \a element
            virtual void parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);

            /// add xml representation of this target to \a element
            virtual void serializeXml(tinyxml2::XMLElement* element) const;

            /// parse this target from \a element
            virtual void parseXmlNode(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel, std::string const& parentstr = "none");

            /// add xml representation of this target to \a element
            virtual void serializeXmlNode(tinyxml2::XMLElement* element) const;
            /*!
            *  \brief get components points
            *
            * \param model: a human body model
            * \return: eigen matrix with coordinates
            */
            virtual Eigen::Matrix3Xd const& getComponentCoord(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get control points sources
            *
            * \param model: a human body model
            * \return: eigen matrix with coordinates
            */
            virtual Eigen::Matrix3Xd const& getComponentControlPoints(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get target components points
            *
            * \param model: a human body model
            * \return: eigen matrix with coordinates
            */
            virtual Eigen::Matrix3Xd const& getComponentCoordTarget(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get control points target
            * If the targets were not computed yet, it computes them.
            *
            * \param model: a human body model
            * \return: eigen matrix with coordinates
            */
            virtual Eigen::Matrix3Xd const& getComponentControlPointsTarget(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get matrix 4*4 that defines the component local frame
            *
            * \return: pointer to the first element of the matrix
            */
            double const* getLocalFrameMatrix(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get local frame
            *
            * \return: const reference to the AnthropoFrame
            */
            AnthropoFrame const& getLocalFrame(piper::hbm::HumanBodyModel *const model);
            /*!
            *  \brief get the value of the component dimensions according to a AnthropoDimensionType
            *
            * \param hbm: pointer to an hbm
            * \param type: type of dimension
            * \return value: value of the dimension
            */
            virtual double getValue(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) = 0;
            virtual double getValueTarget(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type) = 0;
            bool isDefinedFromParent() const { return m_fromParent; }
            void setIsDefinedFromParent(bool const& truefalse);
            /*!
            *  \brief get the normalized position of the reference frame on parent component
            *
            * \return: the normalized position
            */
            double getPosition() const;
            /*!
            *  \brief set the normalized position of the origin of the reference frame on parent component
            *
            * \param value: the normalized position
            */
            void setPosition(double const& value);
            /*!
            *  \brief is a control point defined n parent
            *
            * \return: true or false
            */
            bool controlPointOnParent() const;
            /*!
            *  \brief defines if a control point is defined on parent
            *
            * \param value: the normalized position
            */
            void setControlPointOnParent(bool const& value);

            /*!
            *  \brief define a frame on the component at normalized position (the definition of the position depends of the componentNode Type
            *
            * \param model: a human body model
            * \param position: normalized position between 0.0 and 1.0
            * \return: local frame at the requested position
            */
            virtual AnthropoFrame getPointatPosition(piper::hbm::HumanBodyModel *const model, double const& position) = 0;
            /*!
            *  \brief check if center of section is on parent
            *
            * \return: true if center of section is on parent
            */
            bool const& centerOnParent() const { return m_centerParent; }
            /*!
            *  \brief set center of section on parent
            *
            * \param truefalse: true to define center of section on parent
            */
            void setCenterOnParent(bool const& truefalse);
            
            /// <summary>
            /// Gets the last time when the source control points changed position or some was added or deleted
            /// from this component. The time is noted using the ctime's clock() function.
            /// </summary>
            /// <returns>The last time source control points were updated. Negative if they were never updated yet.</returns>
            clock_t getSourceMTime();

            /// <summary>
            /// Gets the last time when the target for any of the control points of this component were modified.
            /// The time is noted using the ctime's clock() function.
            /// </summary>
            /// <returns>The last time target control points were updated. Negative if they were never updated yet.</returns>
            clock_t getTargetMTime();

            //TODO_EJ: add doc
            void setAnthropoBodyDimension(AnthropoBodyDimension* dim);
            void removeAnthropoBodyDimension(std::string const& dimname);
            bool hasParent() { return parent != nullptr; };
            void setParent(AbstractAnthropoComponentNode * component);
            AbstractAnthropoComponentNode* getParent();
            void removeParent();
            void addChild(AbstractAnthropoComponentNode * component);
            void removeChild(AbstractAnthropoComponentNode * component);
            std::set<AbstractAnthropoComponentNode*>&  getChildren();
            std::set<AbstractAnthropoComponentNode*>const&  getChildren() const;
            std::list<std::string> getRecursiveChildrenList() const;

            virtual ComponentTransformation const& getTransformationTarget(piper::hbm::HumanBodyModel *const model);

            virtual bool checkComponentDimensions() const = 0;
            std::map<std::string, piper::hbm::Coord> const& getLandmarksTarget() const { return m_landtargets; };
            virtual bool setLandmarksTarget(std::map<std::string, Landmarkconstraint> const& landtargets, std::set<std::string>& used) = 0;
            virtual void clearLandmarksTarget();

            double const& getControlPointParameter(AnthropoComponentParam const& paramtype) const;
            void setControlPointParameter(AnthropoComponentParam const& paramtype, double const& value);

            virtual void resetComponent();
            
            /// <summary>
            /// Invalidates the target of this component so that it is recomputed next time it is updated.
            /// </summary>
            void resetTarget();

        protected:
            std::set<AnthropoBodyDimension*> m_dim;
            bool m_fromParent;
            Eigen::Matrix3Xd m_controlpoints, m_component;
            Eigen::Matrix3Xd m_controlpointsTarget, m_componentTarget;
            AnthropoFrame m_sectionentity;
            double m_position;
            bool m_centerParent;
            bool m_cp_onparent, m_isComputed, m_isTargetComputed;
            std::clock_t m_timeModifiedSource, m_timeModifiedTarget; // keep track of when were source or target control points changed using clock()
            std::map<std::string, piper::hbm::Coord> m_landtargets;
            double m_used_bones, m_used_skin;
            ComponentTransformation m_transf;

            virtual bool const& isComputed() const;
            virtual bool compute(piper::hbm::HumanBodyModel *const model);

            bool const& isTargetComputed() const;
            virtual void doCompute(piper::hbm::HumanBodyModel *const model) = 0;

            virtual void computeTarget(piper::hbm::HumanBodyModel *const model);
            virtual void doComputeTarget(piper::hbm::HumanBodyModel *const model)=0;

            virtual bool doComputeScaleValue(piper::hbm::HumanBodyModel *const model, Eigen::Vector3d & values) = 0;

            void setComputed(bool const& isComputed);
            void setTargetComputed(bool const& isComputed);

            AbstractAnthropoComponentNode* parent;
            std::set<AbstractAnthropoComponentNode*> children;


        };

        /**
        * @brief The AnthropoBodySection class stores the description of a body section to define an anthropometric body width or depth or circ.
        *
        * @author Erwan Jolivet @date 2016
        */
        class ANTHROPOMODEL_EXPORT AnthropoBodySection : public AbstractAnthropoComponentNode {

        public:
            /// @return a string representation of the target type
            AnthropoComponentType type() const { return AnthropoComponentType::SECTION; }

            AnthropoBodySection();


            /*!
            *  \brief write the AnthropoBodySection in xml format
            *
            */
            virtual void serializeXml(tinyxml2::XMLElement* element) const;
            /*!
            *  \brief write the AnthropoBodySection in xml format
            *
            */
            void parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);
            /*!
            *  \brief check if metadata that support body section definition are defined
            *
            * \param model: a human body model
            */
            virtual void checkmetadata(piper::hbm::HumanBodyModel const * const model);
            virtual bool checkComponentDimensions() const;
            /*!
            *  \brief check if a control point is defined at center of the body section
            *
            * \return: return true if a control point is defined at center of the body section
            */
            bool hasControlPointatCenter() const;
            /*!
            *  \brief set control pointsat center of the body section
            *
            * \param truefalse: true to define a control point
            */
            void setControlPointatCenter(bool const& truefalse);
            /*!
            *  \brief check if control points are defined on min/max width of the body section contour
            *
            * \return: return true if control points are defined on min/max width of the body section contour
            */
            bool hasControlPointatMinMaxWidth() const;
            /*!
            *  \brief set control points on min/max width of the body section contour
            *
            * \param truefalse: true to define control points
            */
            void setControlPointatMinMaxWidth(bool const& truefalse);
            /*!
            *  \brief check if control points are defined on min/max depth of the body section contour
            *
            * \return: return true if control points are defined on min/max depth of the body section contour
            */
            bool hasControlPointatMinMaxDepth() const;
            /*!
            *  \brief set control points on min/max depth of the body section contour
            *
            * \param truefalse: true to define control points
            */
            void setControlPointatMinMaxDepth(bool const& truefalse);
            /*!
            *  \brief get angular position of control points defined on the body section contour
            *
            * \return: return vector of angles in degre that define control points on the body section contour
            */
            std::vector<double> const& getcontrolPointsPosition() const;
            /*!
            *  \brief get angular position of control points defined on the body section contour
            *
            * \param values: vector of angles in degree that define control points on the body section contour
            */
            void setcontrolPointsPosition(std::vector<double> const& values);
            /*!
            *  \brief get name  of the entities used to define the skin region
            *
            * \return: vector of entity names
            */
            std::set<std::string> const& getEntity() const;
            /*!
            *  \brief set name of the skin entities used to define the skin region
            *
            * \param entitiesname: vector of entity names
            */
            void setEntity(std::vector<std::string> const& entitiesname);
            /*!
            *  \brief add skin entity used to define the skin region
            *
            * \param entityname: name of skin entity to add
            */ 
            void setEntity(std::string const& entityname);
            /*!
            *  \brief remove skin entity used to define the skin region
            *
            * \param entityname: name of skin entity to remove
            */
            void removeEntity(std::string const& entityname);
            /*!
            *  \brief get angle that defines axial rotation of the body section
            *
            * \return: the angle value in degre
            */
            double getRotationAngleAxial() const;
            /*!
            *  \brief set angle that defines axial rotation of the body section
            *
            * \param value: the angle value in degre
            */
            void setRotationAngleAxial(double const& value);
            /*!
            *  \brief get angle that defines the tilt of of the body section
            *
            * \return: the angle value in degre
            */
            double getTiltangle() const;
            /*!
            *  \brief set angle that defines tilt of the body section
            *
            * \param value: the angle value in degre
            */
            void setTiltangle(double const& value);
            /*!
            *  \brief get the value of the body section for a AnthropoDimensionType
            *
            * \param hbm: pointer to an hbm
            * \param type: type of dimension
            * \return value: value of the dimension
            */
            virtual double getValue(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type);
            virtual double getValueTarget(piper::hbm::HumanBodyModel *const model, AnthropoDimensionType const& type);
            /*!
            *  \brief check if minimal information is provided to compute a body section
            *
            * \return: true is a body section contour can be computed
            */
            bool isDefined() const;
            /*!
            *  \brief define a frame on the segment at normalized position
            *
            * \param model: a human body model
            * \param position: normalized position between 0.0 and 1.0
            * \return: local frame at the requested position
            */
            virtual AnthropoFrame getPointatPosition(piper::hbm::HumanBodyModel *const model, double const& position);


            static double computeSectionCircumference(Eigen::Matrix3Xd const& component);
            static double computeSectionDetph(Eigen::Matrix3Xd const& component);
            static double computeSectionWidth(Eigen::Matrix3Xd const& component);

            virtual bool setLandmarksTarget(std::map<std::string, Landmarkconstraint> const& /*landtargets*/, std::set<std::string>& /*used*/) { return true; }

            virtual void resetComponent();

        private:
            std::set<std::string> m_entities;
            //control point
            bool m_minmaxWidth, m_minmaxDepth, m_center;
            std::vector<double> m_pos_controlpoints;
            //section center and normale
            double m_rotY, m_rotaxis;
            vtkSmartPointer<vtkUnstructuredGrid> m_envVTK;

            void doCompute(piper::hbm::HumanBodyModel *const model);
            bool doComputeScaleValue(piper::hbm::HumanBodyModel *const model, Eigen::Vector3d & values);
            void doComputeTarget(piper::hbm::HumanBodyModel *const model);

            struct sort_pred {
                bool operator()(const std::pair<size_t, double> &left, const std::pair<size_t, double> &right) {
                    return left.second < right.second;
                }
            };

            static double angulardistance(double const& v1, double const& v2);
            static void optimScalingSection(double const& scaleX, double const& scaleY, double& scaleCirc, Eigen::Matrix3Xd component, double const& targevalue);
        };

        /**
        * @brief The BodySegment class stores the description of a body dimension.
        *
        * @author Erwan Jolivet @date 2016
        */
        class ANTHROPOMODEL_EXPORT AnthropoBodyDimension : public AbstractAnthropoComponent {

        public:
            /// @return a string representation of the target type
            AnthropoComponentType type() const { return AnthropoComponentType::DIMENSION; }

            AnthropoBodyDimension();

            /*!
            *  \brief write the AnthropoBodyDimension in xml format
            *
            */
            virtual void serializeXml(tinyxml2::XMLElement* element) const;
            /*!
            *  \brief write the AnthropoBodyDimension in xml format
            *
            */
            void parseXml(const tinyxml2::XMLElement* element, AnthropoModel& anthropomodel);
            /*!
            *  \brief check if metadata that support body section definition are defined
            *
            * \param model: a human body model
            */
            virtual void checkmetadata(piper::hbm::HumanBodyModel const * const model) {};
            /*!
            *  \brief get the target value for body dimension
            *
            * \param model: a human body model
            * \return: the target value
            */
            double getTargetValue(piper::hbm::HumanBodyModel *const model) const;
            /*!
            *  \brief get anthropometric component associated wth the body dimension with weight
            *
            * \return: map of anthropometric component weight with anthropometric component name
            */
            std::map<AbstractAnthropoComponentNode*, double> const& getAnthropoComponent() const;
            /*!
            *  \brief set anthropometric dimension type
            *
            * \param type: the AnthropoDimensionType
            */
            bool setType(AnthropoDimensionType const& type);
            /*!
            *  \brief set anthropometric dimension type
            *
            * \param type: string that defines the type
            */
            bool setType(std::string const& type);
            /*!
            *  \brief get the type of the anthropometric dimension
            *
            * \return: the AnthropoDimensionType
            */
            AnthropoDimensionType const& getType() const;
            /*!
            *  \brief set a anthropometric component with weight
            *
            * \param name: the name of the anthropometric component
            * \param weight: weight associated with the anthropometric component
            */
            bool setAnthropoComponent(AbstractAnthropoComponentNode* component, double const& weight = 1.0);
            /*!
            *  \brief remove a anthropometric component
            *
            * \param name: the name of the anthropometric component
            */
            void removeAnthropoComponent(std::string const& name);
            /*!
            *  \brief set a anthropometric component used to compute scale factor (target value / value of anthropometric dimensions on current hbm)
            *
            * \param name: the name of the anthropometric component
            */
            bool setReferenceAnthropoComponent(AbstractAnthropoComponentNode* component);
            /*!
            *  \brief get the anthropometric component used to compute scale factor (target value / value of anthropometric dimensions on current hbm)
            *
            * \return: the name of the anthropometric component
            */
            std::string getReferenceAnthropoComponent() const;
            /*!
            *  \brief remove the anthropometric component used to compute scale factor (target value / value of anthropometric dimensions on current hbm)
            *
            */
            void removeReferenceAnthropoComponent();
            /*!
            *  \brief set target value of the body section
            *
            * \param value: the target value
            */
            bool setTargetValue(double const& value);
            /*!
            *  \brief set target value of the body section
            *
            * \param value: AnthropometricDimensionTarget
            */
            bool setTargetValue(hbm::AnthropometricDimensionTarget const& target);
            /*!
            *  \brief return true if a target value is defined
            *
            */
            bool isTargetValueDefined() const;
            /*!
            *  \brief get scale factor for a component associated with the dimension
            *
            */
            double getScalingWeight(AbstractAnthropoComponentNode* comp) const;
            /*!
            *  \brief get the value of the body section for a AnthropoDimensionType
            *
            * \param hbm: pointer to an hbm
            * \param type: type of dimension
            * \return value: value of the dimension
            */
            double getValue(piper::hbm::HumanBodyModel *const model) const;
            /*!
            *  \brief check if the component is defined (with minimal description available)
            *
            * \return: true is component is defined
            */
            virtual bool isDefined() const;

            /*!
            *  \brief return false if the dimension defines an absolute value and true for a relative scale value
            *
            */
            bool const& isScalingRelative() const;
            /*!
            *  \brief set the type of scaling
            *
            * \param type: boolean equal to false if the dimension defines an absolute value and true for a relative scale value
            */
            void setScalingRelative(bool const& truefalse);


            static AnthropoDimensionType AnthropoBodyDimensionTypefromString(std::string const& typestr);

        private:
            //member with accessor
            AnthropoDimensionType m_type;
            std::map<AbstractAnthropoComponentNode*, double> m_component;
            AbstractAnthropoComponentNode* m_componentReference;
            double m_targetvalue;
            bool m_relativescaling;


            static bool isSegmentDimensionType(AnthropoDimensionType const& type);

        };


        struct optimObjectiveFuntion
        {
            optimObjectiveFuntion(Eigen::Matrix3Xd component, Eigen::Vector3d scalingmatrix, double const& targetvalue);
            optimObjectiveFuntion(const optimObjectiveFuntion &obj);
            double operator()(double const& x)
            { //
                _scalingmatrix[1] = x;
                AnthropoFrame scaling;
                scaling.setIdentity();
                ScalingMatrix scalingmatrix = _scalingmatrix.asDiagonal();
                scaling = scalingmatrix * scaling;
                double crit = _target - AnthropoBodySection::computeSectionCircumference(scaling * _component);
                return abs(crit);
            }
            double _target;
            Eigen::Vector3d _scalingmatrix;
            Eigen::Matrix3Xd _component;
        };

    }
}

#endif
