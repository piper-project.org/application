/*******************************************************************************
* Copyright (C) 2019 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Yoann Lafon (UCBL-Ifsttar)                 *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef vtkSurfaceRegistrationPIPER_h
#define vtkSurfaceRegistrationPIPER_h

#include <vtkPolyDataAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkPolyDataNormals.h>
#include <vtkIdTypeArray.h>

#include <Eigen/Core>

#include <memory>


/// <summary>
/// A vtkPolyData filter that registers the source surface onto a target surface a surface distance and output it as a new vtkPolydata.
/// In layman's terms, the output mesh is the same as the source (connectivity, number and order of vertices...), but in the shape of the target (up to a certain error margin).
/// INPUT: This filter requires TWO input data objects - the source mesh on port 0 and the target mesh on port 1 
/// (so use e.g. vtkSurfaceRegistrationPIPER::SetInputData(0, source_mesh); vtkSurfaceRegistrationPIPER::SetInputData(1, target_mesh);).
/// </summary>
/// <author>Tomas Janak, Yoann Lafon</author>
/// <date>2019</date>
/// <seealso cref="vtkPolyDataAlgorithm" />
class vtkSurfaceRegistrationPIPER : public vtkPolyDataAlgorithm
{
public:
    vtkTypeMacro(vtkSurfaceRegistrationPIPER, vtkPolyDataAlgorithm);

    static vtkSurfaceRegistrationPIPER *New();
    
    // number of points must match number of points in source mesh
    // for each point of source mesh contains ID of point in target mesh that should match it
    // for points with no prescribed points, set it to -1 (or any negative number)
    vtkSetMacro(ControlPoints, vtkSmartPointer<vtkIdTypeArray>);
    vtkGetMacro(ControlPoints, vtkSmartPointer<vtkIdTypeArray>);

    // if set to true, registration will be rigid, if false, it will deform the source mesh. Default false.
    vtkSetMacro(RigidOnly, bool);
    vtkGetMacro(RigidOnly, bool);

    // How many buckets will the surface descriptor histogram use. The histogram is used to disretize the information about 
    // curvature of the surface (normals of neighbours etc.) around a given point. The higher the number, the more
    // sensitive will the registration be, i.e. smaller differences in curvature will be considered as "the same".
    // Also increases computation time. Default is 5.
    // Must be at least 2, if less is specified, 2 will be used.
    vtkSetMacro(HistogramBuckets, int);
    vtkGetMacro(HistogramBuckets, int);

    // Number of iterations of the registration process. Default 20.
    // It can still end in less iterations if there are only small differences between subsequent iterations.
    vtkSetMacro(NumberOfIterations, int);
    vtkGetMacro(NumberOfIterations, int);

    // Number of iterations of smoothing by vtkWindowedSincPolyDataFilter that will be applied after every iteration of registration.
    // Used only for non-rigid registration.
    // Recommended low numbers, both for precision and computation time. Default 2.
    vtkSetMacro(PostProcessSmooth, int);
    vtkGetMacro(PostProcessSmooth, int);

    /// <summary>
    /// Checks if the specified inputs are compatible and can be used for registration.
    /// vtkError is raised if the meshes: are not specified, do not contain faces, or contain multiple disconnected components
    /// </summary>
    /// <returns>True if the inputs are valid (no errors), false otherwise.</returns>
    bool CheckInputValidity();

private:

    /// <summary>
    /// A helper data structure encapsulating several metrics about a given vtkPolyData that are used during the registration process.
    /// The class relies on raw pointers and other shortcuts for efficiency because it is being used in a very limited scope
    /// -> do not attempt to use it elsewhere (it's defined as private for a reason).
    /// </summary>
    class VtkPolyDataMetrics
    {

    public:

        /// <summary>
        /// Creates a new instance of vtkPolyDataMetrics. Call Initialize before using it.
        /// </summary>
        VtkPolyDataMetrics();

        /// <summary>
        /// Destructor; disposes of the histogram data.
        /// </summary>
        ~VtkPolyDataMetrics();

        /// <summary>
        /// Deletes all histogram-related pointers.
        /// </summary>
        void deleteHistogram();

        /// <summary>
        /// Initializes this instance of vtkPolyDataMetrics.
        /// </summary>
        /// <param name="mesh">The mesh for which the metrics are being computed.</param>
        /// <param name="nb_histogramBuckets">Number of intervals the curvature histogram will use. 
        /// It must be at least 2, if less is specified, it will be changed to 2.</param>
        /// <returns>True if initialization was succesful, false if it was not (mesh contains vertices with no neighbors, i.e. unreferenced
        /// by any cell).</returns>
        bool Initialize(vtkSmartPointer<vtkPolyData> mesh, int nb_histogramBuckets);

        /// <summary>
        /// Gets min and max bounds of the point normals in each dimension.
        /// </summary>
        /// <returns>Bounds on the normals in the form of {X_min, X_max, Y_min, Y_max, Z_min, Z_max}.</returns>
        const double* GetPointNormalBounds();

        /// <summary>
        /// Gets min and max bounds of the point positions in each dimension.
        /// </summary>
        /// <returns>Bounds on the positions in the form of {X_min, X_max, Y_min, Y_max, Z_min, Z_max}.</returns>
        const double* GetPointBounds();

        /// <summary>
        /// Gets min and max bounds of the curvature histogram in each dimension (bucket).
        /// </summary>
        /// <returns>Bounds on the normals in the form of {D1_min, D1_max, D2_min, D2_max, ...., D_nbBuckets_min, D_nbBuckets_max},
        /// where nbBuckets is TWICE the parameter specified in constructor - the histogram actually consists of two histograms
        /// concatenated together. This means that the size of this array is:
        /// nb_histogramBuckets (constructor parameter) * 4 - twice the size, min and max for each bucket.</returns>
        const double* GetCurvatureHistogramBounds();

        /// <summary>
        /// Gets the mesh for which the metrics were built.
        /// The pointData of the mesh will also contain the normals, but only if they have been already built - this method
        /// does not check that. Use GetNormals() to ensure the normals have been computed.
        /// </summary>
        /// <returns>The mesh.</returns>
        vtkSmartPointer<vtkPolyData> GetPolyData();

        /// <summary>
        /// Gets the point normals of the mesh. If they haven't been computed yet, this method will trigger their computation.
        /// </summary>
        /// <returns>Normalized normals of each point of the current mesh.</returns>
        vtkSmartPointer<vtkFloatArray> GetPointNormals();

        /// <summary>
        /// Gets the cell normals of the mesh. If they haven't been computed yet, this method will trigger their computation.
        /// </summary>
        /// <returns>Normalized normals of each cell of the current mesh.</returns>
        vtkSmartPointer<vtkFloatArray> GetCellNormals();

        /// <summary>
        /// Gets the curvature histograms for each point the mesh. it is a concatenation of two histogram:
        /// The first nb_histogramBuckets entries contains a histogram of scalar (dot) products between the normal of
        /// the given point and the normals in each neighbor in two-ring of the point.
        /// The second nb_histogramBuckets entries contains a histogram of scalar (dot) products between the normal of
        /// the given point and the directional vectors to each neighbor in two-ring of the point (pn - p0, where p0 is 
        /// the point for which the histogram is computed and pn is n-th neighbor in two ring).
        /// After computation, the histogram is also smoothed by local (two-ring) mean averaging.
        /// </summary>
        /// <returns>The combined histograms for each point of mesh. N-th COLUMN belongs to N-th point in the mesh, each
        /// ROW belongs to one bucket of the histogram. Since it is a concatenation of two histograms, the total size is then:
        /// (2 x nb_histogramBuckets) x nb_points. So histogram[i][j] gives you i-th bucket for j-th point.
        /// The returned pointer points directly to the internal storage of the histogram. If the histogram is re-computed,
        /// the pointed-to data will change. Call this method to trigger re-computation (e.g. after modifying the point coordinates).
        /// WARNING: If this instance of vtkPolyDataMetrics is disposed, so is this pointer.</returns>
        double **GetCurvatureHistograms();

        /// <summary>
        /// Gets the size of the histogram array.
        /// </summary>
        /// <returns>The total size of the full histogram - it is twice the number specified at initialization (see GetCurvatureHistograms() for details).</returns>
        int GetHistogramSize();

        /// <summary>
        /// Gets the one-ring neighborhood of each point, i.e. list of point IDs of points that are connected by an edge to the given point.
        /// </summary>
        /// <returns>For each point in the mesh this instance is based one contains a list of point IDs in the same mesh.</returns>
        const vtkSmartPointer<vtkIdList> *GetOneRingPoints();

        /// <summary>
        /// Gets the two-ring neighborhood of each point, i.e. list of point IDs of points that are at most two edges away from the given point.
        /// </summary>
        /// <returns>For each point in the mesh this instance is based one contains a list of point IDs in the same mesh.</returns>
        const vtkSmartPointer<vtkIdList> *GetTwoRingPoints();

        /// <summary>
        /// Perform a smoothing of a specified vertex quality by averaging of values in the one-ring neighborhood 
        /// (and the value in the point itself) of each point.
        /// </summary>
        /// <param name="field">For each point in the mesh that this instance is based on must contain a vector (dimension is arbitrary).</param>
        /// <param name="nbIterations">How many iterations of averaging should be performed.</param>
        /// <param name="fixedPoints">For each point contains negative value if the point should not be fixed 
        /// and non-negative if it should be (== will not be smoothed). Leave it as nullptr to have nothing fixed.</param>
        /// <returns>The field after smoothing. If the number of tuples in field does not match the number of points in the mesh,
        /// the original field is returned with no modifications (the same pointer).</returns>
        vtkSmartPointer<vtkDoubleArray> SmoothFieldOneRing(vtkSmartPointer<vtkDoubleArray> field, 
            int nbIterations, vtkSmartPointer<vtkIdTypeArray> fixedPoints = nullptr);

        /// <summary>
        /// Deforms this mesh in such a way to avoid sharp dihedral angles between triangles.
        /// </summary>
        void SmoothSharpAngles();

        /// <summary>
        /// For each point, finds the closest point to it within this mesh and enforces a gap between them.
        /// </summary>
        void AvoidSelfPenetration();

        /// <summary>
        /// Performs a rigid registration (based on https://igl.ethz.ch/projects/ARAP/svd_rot.pdf) of this mesh toward a specified target.
        /// Does not modify the mesh, just provides the displacement field (pointer passed as parameter).
        /// </summary>
        /// <param name="target">The target version of this mesh. It must have the same number of vertices
        /// as the mesh this VtkPolyDataMetrics is based on and also is assumed to have the same connectivity and points order.</param>
        /// <param name="displacement">Upon completion, this will contain a displacement vector for each point.</param>
        void AlignRigid(vtkSmartPointer<vtkPoints> target, vtkSmartPointer<vtkDoubleArray> displacement);

        /// <summary>
        /// Simply adds the specified displacement field to each point of this mesh.
        /// </summary>
        /// <param name="displacement">Defines the target of the deformation, i.e. the desired end position is
        /// (currentPosition + displacement).</param>
        void ApplyDisplacementDirect(vtkSmartPointer<vtkDoubleArray> displacement);
        
        /// <summary>
        /// Performs a non-rigid registration of this mesh based on the specified displacement such that the transformation
        /// is "as rigid as possible", based on Olga Sorkine and Marc Alexa: "As-Rigid-As-Possible Surface Modeling"
        /// https://www.igl.ethz.ch/projects/ARAP/arap_web.pdf and S. Yamazaki: "Markerless landmark localization on body shape scans by non-rigid model".
        /// </summary>
        /// <param name="displacement">Defines the target of the deformation, i.e. the desired end position is
        /// (currentPosition + displacement). However, this method will use the ARAP approach to create smooth deformation.</param>
        /// <param name="fixedPoints">For each point contains negative value if the point should not be fixed 
        /// and non-negative if it should be (== will not be smoothed).</param>
        /// <param name="nugget">Uncertainty factor (variance) to use when dealing with the target. Negative number, usually between 0 to -100.</param>
        void ApplyDisplacementARAP(vtkSmartPointer<vtkDoubleArray> displacement,
            vtkSmartPointer<vtkIdTypeArray> fixedPoints, double nugget = -20.0);
        
    protected:

        /// <summary>
        /// Checks whether the histogram has been computed after the last modification of the point positions and normals and if not, recomputes it.
        /// Also computes the bounds of the histogram.
        /// </summary>
        void computeHistogram();

        /// <summary>
        /// Checks whether the normals has been computed after the last modification of the point positions and if not, recomputes it. Also computes the bounds of normals.
        /// </summary>
        void computeNormals();

        /// <summary>
        /// Computes the dihedral angles of the mesh for each cell, stores them in the member variable "dihAngles".
        /// </summary>
        void computeDihedralAngles();

        /// <summary>
        /// Gets all unique point IDs of points in a neighborhood of a given point in the mesh this instance is tied to.
        /// </summary>
        /// <param name="pointId">Id of the point for whose neighbors we are looking for. This is used only to ensure that the point is not included in the list.</param>
        /// <param name="cellIds">List of cells that form the neighborhood.</param>
        /// <param name="data">The neighboring points will be added to the list. The method does not reset this list - 
        /// whatever is in it prior to this call will remain there. The InsertUnique method
        /// is used to ensure no points is inserted twice -> do not provide a list already filled with many entries (unless they are relevant),
        /// otherwise it will slow the computation down.</param>
        void getNeighborhoodPoints(vtkIdType pointId, vtkSmartPointer<vtkIdList> &cellIds, vtkSmartPointer<vtkIdList> &ringPoints);

        vtkSmartPointer<vtkPolyData> mesh;
        double pointBounds[6];
        vtkSmartPointer<vtkIdList> *oneRingPoints; // for each point, contains a list of ids of points in a one-ring neighborhood
        vtkSmartPointer<vtkIdList> *twoRingPoints; // for each point, contains a list of ids of points in a two-ring neighborhood (includes one-ring points)
        vtkSmartPointer<vtkIdList> *fourRingPoints;

        // cosines of dihedral angles ( = dot product of face normals), used for smoothing sharp angles
        // for each point contains an array with angles between cells shared by a neighbor. The order is the same as order of neighbors in oneRingPoints
        // -> contains angles between cells shared by the i-th point and it's j-th neighbor
        vtkSmartPointer<vtkDoubleArray> *dihAngles;
                
        double normalBounds[6]; 
        vtkSmartPointer<vtkPolyDataNormals> normals;
        unsigned long normalComputeTime; // marks the last time normals were recomputed - to be able to check whether we also need to recompute the bounds
        
        double *curvatureHistogramBounds;
        double **curvatureHistogram;
        double **curvatureHistogramTemp; // used only internally for the un-smoothed version
        double *bucket_limit; // contains the boundary of the intervals (buckets)
        int nbBuckets; // number of intervals the curvature histogram uses
        unsigned long histComputeTime; // marks the time when histogram was last computed - to be able to check if normals has changed since then (which would mean we need to recompute histogram)
    };

    
    // structures used only internally when computing pointToPointRegistration based on all the metrics 
    // it is re-computed many times, but is always the same size -> defined as member to be able to allocate it outside the
    // method and not having to pass it as parameter all the time
    double *allPointsMatrix;
    double *histBound;
    double *minMaxDifHist;
    bool *maskDimension; // contains true for dimension that are to be used and false for those that should be skipped
    int nbUnmaskedDim = 0; // number of "true" entries in maskDimension

protected:
    vtkSurfaceRegistrationPIPER();
    ~vtkSurfaceRegistrationPIPER();

    virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
            
    /// <summary>
    /// For each point from data1 looks for closest points in data2 based on provided distance metrics.
    /// </summary>
    /// <param name="source">Data object that is based on the mesh with "source" points, i.e. those for
    /// which we want to find the closest points in the second dataset.</param>
    /// <param name="target">Data object that is based on the mesh with "target" points, i.e. those among which
    /// we will look for when looking for closest points to each source point.</param>
    /// <param name="weights">Weights for the individual distance metrics: 
    /// weights[0] is for positional difference, weights[1] is for normals difference and weights[2] is for the difference
    /// in curvature histogram.</param>
    /// <param name="controlPoints">Array with values for each source point, value is the ID of a target point
    /// that should always be associated with the given source point. -1 for points that should be computed.</param>
    /// <returns>List of point IDs in the target mesh: for each point X in source, the ID in the list is the closest point in target to X.</returns>
    vtkSmartPointer<vtkIdList> pointToPointRegistration(VtkPolyDataMetrics &source,
        VtkPolyDataMetrics &target, double weights[3], vtkSmartPointer<vtkIdTypeArray> controlPoints);
    
    /// <summary>
    /// ???
    /// The normalization happens in place, i.e. the input data is modified.
    /// </summary>
    /// <param name="field">The field to normalize. Upon exiting this method, they will be normalized.
    /// Field is expected to be a matrix of points, one row per point, one column per dimension.</param>
    /// <param name="option">Passage de [min,max] du champs "f0" � l'intervalle [0,1] if option = 0;
    /// If option = 1 => passage � [-1,1] de toutes les colonnes. If option = -1 => passage de [max,min] � [0,1] en inversant.
    ///  If option = 2 => passage � [-1,1] de toutes les colonnes � partir du min() et max() sur l'ensemble des colonnes</param>
    void field_normalize(Eigen::MatrixXd &field, int option = 0);

    /// <summary>
    /// Uses least square error method to align the source data toward the target as closely as possible using only rigid transformaion.
    /// </summary>
    /// <param name="pointsToTransform">Points that are to be transformed. This method modifies their position.</param>
    /// <param name="displacement">Displacement imposed on each point. This method will try to approximate it with least square error.
    /// For each of the points to transform, this array must include a displacement (i.e. size of displacement = size of pointsToTransform).</param>
    void rigidRegistration(vtkSmartPointer<vtkPoints> pointsToTransform, vtkSmartPointer<vtkDoubleArray> displacement);


    VtkPolyDataMetrics metricsOutput, metricsTarget;

    // prescribed pairs of source-target points that should be matched regardless of what the matching algorithm computes
    vtkSmartPointer<vtkIdTypeArray> ControlPoints;

    bool RigidOnly = false; // true = use rigid registration, false = use nonlinear registration
    int HistogramBuckets = 5; // how many buckets to count with in the (normalized) histogram
    int NumberOfIterations = 20;
    int PostProcessSmooth = 2;
};

#endif
