/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak (UCBL-Ifsttar)                              *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "vtkSurfaceDistance.h"

#include <vtkObjectFactory.h>
#include <vtkCell.h>
#include <vtkMath.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>

#include <spectra/SymEigsSolver.h>
#include <spectra/MatOp/SparseSymMatProd.h>

/**
* Part of this (computation of geodesic distance by heat) code is a slightly modified transcription of the ANSI C code provided by Keenan Crane
* on the website: https://www.cs.cmu.edu/~kmcrane/Projects/GeodesicsInHeat/.
* The following is a licence statement by the author which, by its article 1, extends to this source code as well:
*
* Copyright 2012 Keenan Crane. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
* SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* The views and conclusions contained in the software and documentation are those
* of the author and should not be interpreted as representing official policies,
* either expressed or implied, of any other person or institution.
*
*/

using namespace Spectra;

vtkStandardNewMacro(vtkSurfaceDistance);

const std::string vtkSurfaceDistance::surfaceDistanceMapName = "heatDistanceMap";

vtkSurfaceDistance::vtkSurfaceDistance()
{
    this->SetNumberOfInputPorts(1);
    this->SetNumberOfOutputPorts(1);
    TimeStepHeat = -1;
    dtHeat = -1;
    BoundaryConditions = 0;
    lastRunTime = 0;
    lastModeChangeTime = 0;
    EigenPrecision = 100;
    matricesBuilt = false;
    Mode = DistanceComputeSubset::AllToAll;
    inputData = vtkSmartPointer<vtkLaplacianPolyData>::New();
    ComputeDistance = Biharmonic;
}

vtkSurfaceDistance::~vtkSurfaceDistance()
{

}


void vtkSurfaceDistance::UseMeanEdgeLengthForTimeStep()
{
    TimeStepHeat = -1;
}

void vtkSurfaceDistance::SetMode(DistanceComputeSubset _arg)
{
    if (Mode != _arg)
    {
        Mode = _arg;
        if (lastRunTime == GetMTime()) // if this is the only modification, prevent matrices being rebuild
        {
            Modified();
            lastModeChangeTime = GetMTime();
        }
        else
            Modified();
    }
}

void vtkSurfaceDistance::SetEigenPrecision(double _arg)
{
    if (_arg != EigenPrecision)
    {
        EigenPrecision = _arg;
        Modified();
    }
}

int vtkSurfaceDistance::RequestData(vtkInformation *vtkNotUsed(request),
    vtkInformationVector **inputVector,
    vtkInformationVector *outputVector)
{
    // get the input and output
    vtkPolyData* input = vtkPolyData::GetData(inputVector[0], 0);
    vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);
    vtkIdType npts = input->GetNumberOfPoints();
    if (input->GetNumberOfCells() < 1 || npts < 3)
        return 0; // false means failure
    if (Mode == DistanceComputeSubset::SourceToAll)
    {
        if (SourcePoints == NULL || SourcePoints->GetNumberOfIds() < 1)
        {
            vtkErrorMacro(<< "ComputeForSource set to true, but no sources were specified, aborting.");
            return 0; // false means failure
        }
    }

    inputData->SetMesh(input);

    // compute geodesics
    // 1. estimate time step if not set
    if (ComputeDistance == SurfaceDistance::GeodesicByHeat)
    {
        if (TimeStepHeat > 0) // if the setting is overriden by the user, use the hard-set value
            dtHeat = TimeStepHeat;
        else if (dtHeat <= 0 || inputData->GetMTime() != inputLastMTime) // otherwise, if it has not been estimated yet, or the input changed, estimate it
        {
            dtHeat = inputData->ComputeMeanEdgeLength();
            dtHeat *= dtHeat; // set time step as square of mean edge length+
            matricesBuilt = false; // make sure the matrices are rebuilt
        }
    }
    // 2. build matrices and mesh data
    if ((lastRunTime != GetMTime() && lastModeChangeTime != GetMTime()) || // if there was some change to the filter, except for Mode (which does not affect matrices)
        (inputData->GetMTime() != inputLastMTime) || !matricesBuilt) // if the input changed since last time or the matrices have not been built yet
    {
        if (ComputeDistance == GeodesicByHeat)
            buildMatricesGeodesicByHeat();
        else
            buildMatricesBiharmonic();
    }
    output->ShallowCopy(inputData);

    // 3. solve 
    // set up structures for writing the results
    if (Mode == DistanceComputeSubset::SourceToAll) // setup the one vector
    {
        vtkSmartPointer<vtkDoubleArray> result = vtkSmartPointer<vtkDoubleArray>::New();
        result->SetNumberOfComponents(1);
        result->SetNumberOfTuples(npts);
        result->SetName(surfaceDistanceMapName.c_str());
        if (ComputeDistance == GeodesicByHeat)
        {
            Eigen::VectorXd isSource(npts);
            isSource.fill(0);
            int validSrc = 0;
            // setup the source array - 1 for source nodes, 0 for the rest
            for (vtkIdType i = 0; i < SourcePoints->GetNumberOfIds(); i++)
            {
                vtkIdType id = SourcePoints->GetId(i);
                if (id <= npts) // to make sure it doesn't crash on wrong input, ignore wrong IDs
                {
                    isSource[id] = 1;
                    validSrc++;
                }
            }
            if (validSrc == 0)
                return 0;
            // get the solution
            Eigen::VectorXd distance = solveGeodesicByHeat(isSource);
            // 4. write output
            double minDistance = distance.minCoeff(); // the minimum value of distance - to be subtracted from the results

            for (vtkIdType i = 0; i < npts; i++)
                result->SetValue(i, distance[i] - minDistance);

        }
        else // if biharmonic distance, take only the first source - biharmonic distance cannot compute distance to a group of nodes
        {
            std::vector<vtkIdType> chosenIDs;
            // setup the source array - 1 for source nodes, 0 for the rest
            for (vtkIdType i = 0; i < SourcePoints->GetNumberOfIds(); i++)
            {
                if (SourcePoints->GetId(i) <= npts) // to make sure it doesn't crash on wrong input, ignore wrong IDs
                {
                    chosenIDs.push_back(SourcePoints->GetId(i));
                    break;
                }
            }
            if (chosenIDs.empty())
                return 0;
            DistanceMatrix = std::make_shared<Eigen::MatrixXd>(npts, 1);
            processSingleSourcesBiharmonic(chosenIDs); 

            for (vtkIdType i = 0; i < npts; i++)
                result->SetValue(i, (*DistanceMatrix)(i, 0));
        }
        output->GetPointData()->AddArray(result);
    }
    else // prepare matrix for all the distance fields
    {
        vtkSmartPointer<vtkBitArray> isCtrlPoint = vtkBitArray::SafeDownCast(input->GetPointData()->GetArray(_GEODESIC_CHOSENPOINTS));
        std::vector<vtkIdType> chosenIDs;
        if ((Mode == DistanceComputeSubset::EachChosenToEachChosen || Mode == DistanceComputeSubset::EachChosenToAll)
            && isCtrlPoint != NULL) // use only the specified points if that was requested
        {
            for (vtkIdType i = 0; i < isCtrlPoint->GetNumberOfTuples(); i++)
            {
                if (isCtrlPoint->GetValue(i))
                    chosenIDs.push_back(i);
            }
            size_t nptsChosen = chosenIDs.size();
            DistanceMatrix = std::make_shared<Eigen::MatrixXd>(Mode == DistanceComputeSubset::EachChosenToEachChosen ? nptsChosen : npts,
                nptsChosen);
            if (ComputeDistance == GeodesicByHeat)
                processSingleSourcesGeodesicByHeat(chosenIDs);
            else
                processSingleSourcesBiharmonic(chosenIDs);
        }
        else
        {
            DistanceMatrix = std::make_shared<Eigen::MatrixXd>(npts, npts);
            if (ComputeDistance == GeodesicByHeat)
                processSingleSourcesGeodesicByHeat(chosenIDs);
            else
                processSingleSourcesBiharmonic(chosenIDs);
        }
    }
    inputLastMTime = inputData->GetMTime(); // note the MTime of the input to know if time step and matrices need to be updated in case of future querries
    lastRunTime = GetMTime();
    return 1;
}


void vtkSurfaceDistance::processSingleSourcesBiharmonic(std::vector<vtkIdType> &sources)
{
    vtkIdType npts = inputData->GetNumberOfPoints();

    // solve
    laplaceEigenValues(0) = 0;
    Eigen::MatrixXd lEig(laplaceEigenVectors.rows(), laplaceEigenVectors.cols()); // make a copy so that the computed eigen vectors can be kept for future runs
    lEig.col(0).fill(0);

    #pragma omp parallel for shared(lEig)
    for (int i = 1; i < lEig.cols(); i++) // 0-th is 0 anyway -> start the loop from 1
    {
        if (laplaceEigenValues(i) != 0)
            lEig.col(i) = laplaceEigenVectors.col(i) * (1.0 / 
                (ComputeDistance == SurfaceDistance::Biharmonic ? laplaceEigenValues(i) : sqrt(laplaceEigenValues(i))));
        else
            lEig.col(i).fill(0);
    }
    Eigen::VectorXd rowSums;
    rowSums.resize(lEig.rows());
    #pragma omp parallel for shared(lEig, rowSums)
    for (int i = 0; i < lEig.rows(); i++)
        rowSums(i) = lEig.row(i).dot(lEig.row(i));

    vtkIdType j;
    if (sources.size() == 0) // AllToAll
    {
        Eigen::MatrixXd lEig2 = lEig * lEig.transpose();
        #pragma omp parallel for shared(rowSums, lEig2) private(j)
        for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++)
        {
            for (j = 0; j < DistanceMatrix->cols(); j++)
                // fabs gets around occasional artifacts in close proximity of the point coming from the eigenvalues being only approximated
                (*DistanceMatrix)(i, j) = rowSums(i) + rowSums(j) - 2 * lEig2(i, j);
        }
    }
    else  // ChosenToAll or ChosenToChosen
    {
        Eigen::MatrixXd lEigChosenRowsOnly(lEig.cols(), sources.size()); // it will be transposed in the end -> set the chosen rows as columns
        for (j = 0; j < lEigChosenRowsOnly.cols(); j++)
            lEigChosenRowsOnly.col(j) = lEig.row(sources[j]);
        if (DistanceMatrix->rows() == npts) // computing EachChosenToAll
        {
            Eigen::MatrixXd lEig2 = lEig * lEigChosenRowsOnly;
            #pragma omp parallel for shared(rowSums) private(j)
            for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++)
            {
                for (j = 0; j < DistanceMatrix->cols(); j++)
                    // fabs gets around occasional artifacts in close proximity of the point coming from the eigenvalues being only approximated
                    (*DistanceMatrix)(i, j) = rowSums(i) + rowSums(sources[j]) - 2 * lEig2(i, j);
            }
        }
        else // ChosenToChosen
        {
            Eigen::MatrixXd lEig2 = lEigChosenRowsOnly.transpose() * lEigChosenRowsOnly;
            #pragma omp parallel for shared(rowSums) private(j)
            for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++)
            {
                for (j = 0; j < DistanceMatrix->cols(); j++)
                    // fabs gets around occasional artifacts in close proximity of the point coming from the eigenvalues being only approximated
                    (*DistanceMatrix)(i, j) = rowSums(sources[i]) + rowSums(sources[j]) - 2 * lEig2(i, j);
            }
        }
    }

    double minC = DistanceMatrix->minCoeff();
    #pragma omp parallel for private(j)
    for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++)
    {
        for (j = 0; j < DistanceMatrix->cols(); j++)
            (*DistanceMatrix)(i, j) = sqrt((*DistanceMatrix)(i, j) - minC);
    }

    if (ComputeDistance == SurfaceDistance::BiharmGreenCombined)
    {
        // in this case, green was computed, so now compute biharmonic and then combine
        ComputeDistance = SurfaceDistance::Biharmonic;
        std::shared_ptr<Eigen::MatrixXd> green = DistanceMatrix;
        DistanceMatrix = std::make_shared<Eigen::MatrixXd>();
        DistanceMatrix->resize(green->rows(), green->cols());
        processSingleSourcesBiharmonic(sources);
        ComputeDistance = SurfaceDistance::BiharmGreenCombined; // revert back
        double maxBi = DistanceMatrix->maxCoeff();

        #pragma omp parallel for private(j)
        for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++)
        {
            for (j = 0; j < DistanceMatrix->cols(); j++)
            {
                double w = (*DistanceMatrix)(i, j) / maxBi;
                (*DistanceMatrix)(i, j) = (*DistanceMatrix)(i, j) * w + (*green)(i, j) * (1 - w);
            }
        }
    }

 /*  
     THIS IS THE "EXACT" COMPUTATION BASED ON LIPMAN'S MATLAB CODE
     However, it is in it's current form inefficient for any other mode than AllToAll. 
     TODO - In theory, it should be possible to adapt it for other modes

     // prepare the right hand side based on the input
     Eigen::MatrixXd RHS;
     if (nptsChosen == npts)
     {
     RHS.resize(npts, npts);
     RHS.fill(-1.0 / npts);
     RHS.diagonal().fill(1.0 - (1.0 / npts));
     RHS.row(0).fill(0);
     }
     else
     {
     RHS.resize(npts, nptsChosen);
     RHS.fill(-1.0 / npts);
     double diagonalEntry = 1.0 - (1.0 / npts);
     // RHS contains only columns of the chosen points. Each column has diagonalEntry on the diagonal and the -1/npts elsewhere
     // -> to construct the RHS, left out the columns that are not chosen points -> the diagonal entries will shift to non-diagonal positions
     for (int i = 0; i < nptsChosen; i++)
     RHS(sources[i], i) = diagonalEntry;
     RHS.row(0).fill(0);
     }

    Eigen::MatrixXd solution;
    solution.resize(npts, nptsChosen);
    solution = choleskyLaplacian.solve(RHS);
    // By Lipman: "shift the solution so that add up to zero, but weighted" (g = g - repmat(sum(g) / nopts, nopts, 1); )
    Eigen::VectorXd columnSums;
    columnSums.resize(solution.cols());
    #pragma omp parallel for shared(solution, columnSums)
    for (int i = 0; i < solution.cols(); i++)
        columnSums(i) = solution.col(i).sum() / (double)npts; // compute column sums: (sum(g) / nopts)
    #pragma omp parallel for shared(solution, columnSums)
    for (int i = 0; i < npts; i++) // do the shift: g = g - columnSums for each row (repmat replicates columnSum for each row)
        solution.row(i) -= columnSums;

    // fill the distance matrix
    double solII;
    int j;
    if (sources.size() == 0)
    {
        #pragma omp parallel for shared(solution) private(j, solII)
        for (vtkIdType i = 0; i < npts; i++)
        {
            solII = solution(i, i);
            for (j = 0; j < npts; j++)
                (*DistanceMatrix)(i, j) = sqrt(solII + solution(j, j) - 2 * solution(i, j));
        }
    }
    else if (DistanceMatrix->rows() == npts) // computing EachChosenToAll
    {
        #pragma omp parallel for shared(solution) private(j, solII)
        for (vtkIdType i = 0; i < npts; i++)
        {
            solII = solution(i, i);
            for (j = 0; j < DistanceMatrix->cols(); j++)
                (*DistanceMatrix)(i, j) = sqrt(solII + solution(sources[j], sources[j]) - 2 * solution(i, sources[j]));
        }
    }
    else // EachChosenToEachChosen
    {
        #pragma omp parallel for shared(solution) private(j, solII)
        for (vtkIdType i = 0; i < nptsChosen; i++)
        {
            solII = solution(sources[i], i);
            for (j = 0; j < nptsChosen; j++)
                (*DistanceMatrix)(i, j) = sqrt(solII + solution(sources[j], j) - 2 * solution(sources[i], j));
        }
    }*/
}


void vtkSurfaceDistance::buildMatricesBiharmonic()
{
    vtkIdType npts = inputData->GetNumberOfPoints();
    // build mesh data
    Eigen::SparseMatrix<double> &laplacian = inputData->GetLaplacian(true);
  /*  
    THESE ARE STRUCTURES NEEDED FOR THE "EXACT" COMPUTATION BASED ON LIPMAN'S MATLAB CODE
    However, it is in it's current form inefficient for any other mode than AllToAll. 
    TODO - In theory, it should be possible to adapt it for other modes.

    vtkSmartPointer<vtkDoubleArray> vertexAreas = vtkDoubleArray::SafeDownCast(inputData->GetPointData()->GetArray(vtkLaplacianPolyData::ArrNamePointAreas.c_str()));
    std::vector<Eigen::Triplet<double>> nonzerosLap2;
    // weight the laplacian by inverse areas
    // = do matrix multiplication with the (diagonal) inverse area matrix - need to do it only for the non-zero elements of the laplacian
    // basically we want the result to be lap2 = L*W^(-1)*L. First we do lap2 = L*W^(-1) - just copy non-zero laplacian entries weighted by the inverse area
    for (int k = 0; k < laplacian.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(laplacian, k); it; ++it) // for each nonzero entry in the laplacian
        {
            // according to Lipman: 
            // "make the first row&col of A zeros, this forces the first entry of
            // the solution vector to be zero; takes care of inf many solns"
            // -> do not set the non-zero elements in the first row and column
            if (it.row() != 0 && it.col() != 0)
            {
                double value = it.value() * (1.0 / vertexAreas->GetValue(it.col())); // multiply the value by the inverted area of j-th vertex
                nonzerosLap2.push_back(Eigen::Triplet<double>(it.row(), it.col(), value)); // the vertexAreas matrix is diagonal -> (i,j)-th value is used only to compute (i,j)-th value of the result
            }
        }
    }

    Eigen::SparseMatrix<double> lap2; // to store the resulting laplacian^2 weighted by inverse areas
    lap2.resize(npts, npts);
    lap2.setFromTriplets(nonzerosLap2.begin(), nonzerosLap2.end());
    // now lap2 = L*W^(-1), do the remaining *L
    lap2 = lap2 * laplacian;
    lap2.coeffRef(0, 0) = 1; // this is to compensate the diagonal entry on 0,0 for zeoring-out the entire first row and column

    // now factorize the laplacian
    choleskyLaplacian.analyzePattern(lap2);
    choleskyLaplacian.factorize(lap2);*/
    
    // APPROXIMATION BY N SMALLEST EIGEN VALUES
    // Construct matrix operation object using the wrapper class DenseGenMatProd
    SparseSymMatProd<double> op(laplacian);
    // Construct eigen solver object, requesting the largest three eigenvalues
    vtkIdType n = vtkMath::Max((vtkIdType)(EigenPrecision / npts), vtkMath::Min((vtkIdType)10, npts));
    SymEigsSolver< double, SMALLEST_ALGE, SparseSymMatProd<double> > eigs(&op, n, vtkMath::Min(npts, n * 3));
    // Initialize and compute
    eigs.init();
    eigs.compute();
    int discard = 0;
    for (int i = 0; i < eigs.eigenvalues().count(); i++)
    {
        if (eigs.eigenvalues()(i) < 0.00001)
        {
            discard++;
            eigs.eigenvalues()(i) = 0;
        }
    }
    laplaceEigenValues.resize(eigs.eigenvalues().count() - discard);
    laplaceEigenVectors.resize(eigs.eigenvectors().rows(), eigs.eigenvalues().count() - discard);
    int counter = 0;
    for (int i = 0; i < eigs.eigenvalues().count(); i++)
    {
        if (eigs.eigenvalues()(i) > 0.00001)
        {
            laplaceEigenValues(counter) = eigs.eigenvalues()(i);
            laplaceEigenVectors.col(counter) = eigs.eigenvectors().col(i);
            counter++;
        }
    }
    matricesBuilt = true;
}


void vtkSurfaceDistance::processSingleSourcesGeodesicByHeat(std::vector<vtkIdType> &sources)
{
    vtkIdType npts = inputData->GetNumberOfPoints();
    vtkIdType nptsChosen = sources.size();

    if (nptsChosen == 0)
    {
        #pragma omp parallel for
        for (vtkIdType i = 0; i < npts; i++) // for each node
        {
            Eigen::VectorXd isSource(npts);
            isSource.fill(0);
            isSource[i] = 1; // set that node as source

            // get the solution
            Eigen::VectorXd distance;
            distance = solveGeodesicByHeat(isSource);
            double sourceEnergy = distance[i];
            // 4. write output - fill the solution into the matrix
            for (vtkIdType j = 0; j < npts; j++)
                (*DistanceMatrix)(i, j) = abs(distance[j] - sourceEnergy); // we want 0 in the source and increasing with distance to source
        }
    }
    else
    {
        #pragma omp parallel for
        for (vtkIdType i = 0; i < nptsChosen; i++) // for each node
        {
            Eigen::VectorXd isSource(npts);
            isSource.fill(0);
            isSource[sources[i]] = 1; // set that node as source

            // get the solution     
            Eigen::VectorXd distance = solveGeodesicByHeat(isSource);
            double sourceEnergy = distance[sources[i]];
            // 4. write output - fill the solution into the matrix
            if (DistanceMatrix->cols() == DistanceMatrix->rows()) // ChosenToChosen mode
            {
                for (vtkIdType j = i; j < nptsChosen; j++)
                {
                    (*DistanceMatrix)(i, j) = abs(distance[sources[j]] - sourceEnergy);// we want 0 in the source and increasing with distance to source
                    (*DistanceMatrix)(j, i) = abs(distance[sources[j]] - sourceEnergy);
                }
            }
            else // ChosenToAll mode
            {
                for (vtkIdType j = 0; j < DistanceMatrix->rows(); j++)
                    (*DistanceMatrix)(j, i) = abs(distance[j] - sourceEnergy);
            }
        }
    }
}

Eigen::VectorXd vtkSurfaceDistance::solveGeodesicByHeat(Eigen::VectorXd& isSource)
{
    vtkIdType npts = inputData->GetNumberOfPoints();
    vtkIdType nCells = inputData->GetNumberOfCells();
    Eigen::VectorXd potential(npts);
    potential.fill(0);
    Eigen::VectorXd heatNeumann;
    Eigen::VectorXd heatDirichlet;
    // allocate space only for solutions that will be actually computed
    if (BoundaryConditions < 1)
        heatNeumann.resize(npts);
    if (BoundaryConditions > 0)
        heatDirichlet.resize(npts);

    // 3.a - solve heat equation
    // only compute both solutions if necessary
    if (BoundaryConditions < 1) // partial Neumann
        heatNeumann = choleskyNeumann.solve(isSource);
    if (BoundaryConditions > 0) // partial Dirichlet
        heatDirichlet = choleskyDirichlet.solve(isSource);

    // store the final solution, combining the two solutions if necessary
    if (BoundaryConditions > 0 && BoundaryConditions < 1)
    {
        double rBC = 1 - BoundaryConditions;
        // compute interpolated solution - write over the dirichlet solution
        for (vtkIdType i = 0; i < npts; i++)
            heatDirichlet[i] = rBC * heatNeumann[i] + BoundaryConditions * heatDirichlet[i];
    }

    // 3.b - compute the potential
    // in case of the combined solution, it is also in dirichlet, so use heatNeumann only for pure neumann
    Eigen::VectorXd &heat = (BoundaryConditions == 0 ? heatNeumann : heatDirichlet);
    // current triangle data 
    double u[3]; // heat values 
    double eN[9]; // edge normals
    double e[9]; // cotan-weighted edge vectors
    double X[3]; // normalized gradient
    double e0DotX, e1DotX, e2DotX;

    vtkSmartPointer<vtkDoubleArray> edgeNormals = vtkDoubleArray::SafeDownCast(inputData->GetCellData()->GetArray(vtkLaplacianPolyData::ArrNameEdgeNormals.c_str()));
    vtkSmartPointer<vtkDoubleArray> weightedEdges = vtkDoubleArray::SafeDownCast(inputData->GetCellData()->GetArray(vtkLaplacianPolyData::ArrNameWeightedEdges.c_str()));

    vtkIdType cellNpts; //not really used, we know it's 3
    vtkIdType *cellPoints;
    // add contribution from each face
    for (vtkIdType i = 0; i < nCells; i++)
    {
        edgeNormals->GetTuple(i, eN);
        weightedEdges->GetTuple(i, e);
        inputData->GetCellPoints(i, cellNpts, cellPoints);
        // get heat values at three vertices
        vtkIdType id0 = cellPoints[0];
        vtkIdType id1 = cellPoints[1];
        vtkIdType id2 = cellPoints[2];
        u[0] = fabs(heat[id0]);
        u[1] = fabs(heat[id1]);
        u[2] = fabs(heat[id2]);

        // normalize heat values so that they have roughly unit magnitude 
        double den = vtkMath::Normalize(u);
        if (den != 0)
        {
            // compute normalized gradient
            X[0] = u[0]*eN[0] + u[1]*eN[3] + u[2]*eN[6]; // x
            X[1] = u[0]*eN[1] + u[1]*eN[4] + u[2]*eN[7]; // y
            X[2] = u[0]*eN[2] + u[1]*eN[5] + u[2]*eN[8]; // z
            vtkMath::Normalize(X);

            // add contribution to divergence
            e0DotX = vtkMath::Dot(&(e[0]), X);
            e1DotX = vtkMath::Dot(&(e[3]), X);
            e2DotX = vtkMath::Dot(&(e[6]), X);

            // ignore values that would result in potential being nan
            double potPotential0 = potential[id0] - (e1DotX - e2DotX); // potential potential :)
            double potPotential1 = potential[id1] - (e2DotX - e0DotX);
            double potPotential2 = potential[id2] - (e0DotX - e1DotX);

            if (std::isnan(potPotential0) || std::isnan(potPotential1) || std::isnan(potPotential2))
                continue;

            potential[id0] = potPotential0;
            potential[id1] = potPotential1;
            potential[id2] = potPotential2;
        }
    }

    // remove mean value so that the potential is in the range of the Laplace operator 
    // compute mean 
    double mean = 0;
    for (vtkIdType i = 0; i < npts; i++)
        mean += potential[i];
    mean /= npts;
    // subtract mean from each entry
    for (vtkIdType i = 0; i < npts; i++)
        potential[i] -= mean;

    // 3.c - solve Poisson equation
    return choleskyLaplacian.solve(potential);
}


void vtkSurfaceDistance::buildMatricesGeodesicByHeat()
{
    vtkIdType npts = inputData->GetNumberOfPoints();

    // build mesh data - weights etc. - needed to fill the matrices
    inputData->ComputeLaplacian(false);
    // get the required mesh data
    std::vector<std::map<vtkIdType, double>>& neighborsWithWeights = inputData->GetNeighborsWithWeights();
    vtkSmartPointer<vtkBitArray> boundaryVertices = vtkBitArray::SafeDownCast(inputData->GetPointData()->GetArray(vtkLaplacianPolyData::ArrNameBoundaryPoints.c_str()));
    vtkSmartPointer<vtkDoubleArray> vertexAreas = vtkDoubleArray::SafeDownCast(inputData->GetPointData()->GetArray(vtkLaplacianPolyData::ArrNamePointAreas.c_str()));

    // fill nonzero entries
    std::vector<Eigen::Triplet<double>> nonzerosNeumann;
    std::vector<Eigen::Triplet<double>> nonzerosDirichlet;
    int nz = 0;
    for (vtkIdType i = 0; i < npts; i++)
    {
        // get the (duplicated) neighbors of this vertex
        std::map<vtkIdType, double> neighbors = neighborsWithWeights[i];

        // get sum of neighbors' weights
        double columnSum = vtkLaplacianPolyData::regularizationTerm;

        for (auto neighbor : neighbors)
            columnSum += neighbor.second;
        
        // set off-diagonal entries below the diagonal (we are filling per-column -> filling column i, row currentNeighbor)
        for (auto neighbor : neighbors)
        {
            if (neighbor.first > i) // fill only rows below the diagonal - the matrices are symetric
            {
                if (BoundaryConditions < 1) 
                    nonzerosNeumann.push_back(Eigen::Triplet<double>(neighbor.first, i, -neighbor.second * dtHeat));
                if (BoundaryConditions > 0)
                {
                    if (boundaryVertices->GetValue(i) || boundaryVertices->GetValue(neighbor.first))
                        // set off-diagonals to zero so that we retain the same sparsity pattern as other matrices
                        nonzerosDirichlet.push_back(Eigen::Triplet<double>(neighbor.first, i, 0));
                    else
                        nonzerosDirichlet.push_back(Eigen::Triplet<double>(neighbor.first, i, -neighbor.second * dtHeat));
                }
            }
        }
        // set the diagonal entry (for eigen it does not matter where in the initializer list it is)
        if (BoundaryConditions < 1)
            nonzerosNeumann.push_back(Eigen::Triplet<double>(i, i, vertexAreas->GetValue(i) + dtHeat * columnSum));
        if (BoundaryConditions > 0)
        {
            if (boundaryVertices->GetValue(i))
                nonzerosDirichlet.push_back(Eigen::Triplet<double>(i, i, vertexAreas->GetValue(i)));
            else
                nonzerosDirichlet.push_back(Eigen::Triplet<double>(i, i, vertexAreas->GetValue(i) + dtHeat * columnSum));
        }
    }
    // finally set the matrices
    if (BoundaryConditions < 1)
    {
        heatFlowNeumann.resize(npts, npts);
        heatFlowNeumann.setFromTriplets(nonzerosNeumann.begin(), nonzerosNeumann.end());
    }
    if (BoundaryConditions > 0)
    {
        heatFlowDirichlet.resize(npts, npts);
        heatFlowDirichlet.setFromTriplets(nonzerosDirichlet.begin(), nonzerosDirichlet.end());
    }
    // factor the matrices
    // Laplacian
    choleskyLaplacian.analyzePattern(inputData->GetLaplacian(false));
    choleskyLaplacian.factorize(inputData->GetLaplacian(false));

   // only factor both heat flow operators if necessary 
   // (note that the symbolic factorization for Laplace can be reused in both
   // cases since all three matrices have the same sparsity pattern) 
    if(BoundaryConditions < 1) // partial Neumann
    {
        choleskyNeumann.analyzePattern(heatFlowNeumann); // how to just copy the pattern created from laplacian?
        choleskyNeumann.factorize(heatFlowNeumann); 
    }
    if(BoundaryConditions > 0) // partial Dirichlet
    {
        choleskyDirichlet.analyzePattern(heatFlowDirichlet);
        choleskyDirichlet.factorize(heatFlowDirichlet);
    }
    matricesBuilt = true;
}

void vtkSurfaceDistance::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
    if (DistanceMatrix != NULL &&
        DistanceMatrix->cols() == DistanceMatrix->rows()) // if this is false, the distance matrix was computed just for one point - no way to compare the metrics
    {
        int nonzero = 0;
        int posi = 0;
        int tri = 0;
        int asymm = 0;
        for (vtkIdType i = 0; i < DistanceMatrix->rows(); i++) // for each node
        {
            for (vtkIdType j = 0; j < DistanceMatrix->cols(); j++)
            {
                double testDist = (*DistanceMatrix)(i, j);
                if (testDist != (*DistanceMatrix)(j, i))
                    asymm++;
                if (i == j && testDist != 0)
                    nonzero++;
                else if (i != j && testDist <= 0)
                    posi++;
                if (DistanceMatrix->cols() < 200) // to be able to finish this within some reasonable time - it's a n^3 algorithm...
                {
                    for (vtkIdType k = 0; k < DistanceMatrix->cols(); k++)
                    {
                        if (k != i && k != j)
                        {
                            if (testDist >((*DistanceMatrix)(i, k) + (*DistanceMatrix)(k, j)))
                                tri++;
                        }
                    }
                }
            }
        }
        os << "Distance metric check:" << std::endl
            << "# of points with non-zero distance to themselves: " << nonzero << std::endl
            << "# of non-positive distances (for distinct points): " << posi << std::endl
            << "# of assymetric distances: " << asymm << std::endl;
        if (DistanceMatrix->cols() < 200)
            os << "# of triangle inequality violations: " << tri << std::endl;
        else
            os << "# of triangle inequality violations was not checked - the number of points is too high, it would take long."<< std::endl;
    }
}
