/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak                                             *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

#include "vtkPIPERFilters/vtkSurfaceRegistrationPIPER.h"
#include "vtkOBJReader.h"
#include <vtkMath.h>

#include "gtest/gtest.h"

class registration_test : public ::testing::Test {

protected:

    virtual void SetUp()
    {
        vtkSmartPointer<vtkOBJReader> reader = vtkSmartPointer<vtkOBJReader>::New();
        reader->SetFileName("child_skin_502vert.obj");
        reader->Update();
        child500 = reader->GetOutput();
    }
    
    // target = source - reveals numerical bugs (surfaces are close -> zeros are appearing for every distance, makes sure there is not zero division etc.)
    void registerIdentical() 
    {
        vtkSmartPointer<vtkSurfaceRegistrationPIPER> regist = vtkSmartPointer<vtkSurfaceRegistrationPIPER>::New();
        regist->SetInputData(0, child500);
        regist->SetInputData(1, child500);
        EXPECT_TRUE(regist->CheckInputValidity());
        regist->SetNumberOfIterations(5);
        regist->SetPostProcessSmooth(0); // with smoothing the result can be far away from the target, would be difficult to compare
        regist->Update(); 
        // each point should be registered to itself -> their position should be almost the same
        auto outputPoints = regist->GetOutput()->GetPoints();
        auto inputPoints = child500->GetPoints();
        vtkIdType npts = inputPoints->GetNumberOfPoints();
        double pointA[3], pointB[3];
        for (vtkIdType i = 0; i < npts; i++)
        {
            inputPoints->GetPoint(i, pointA);
            outputPoints->GetPoint(i, pointB);
            EXPECT_LE(vtkMath::Distance2BetweenPoints(pointA, pointB), 0.1);
        }
    }

    void registrationQuality() 
    {
        // TODO: VTK 9.0 will have Hausdorff distance filter, we could use it to compare input and output of registration
        // to see if the result is close enough
    }

    vtkSmartPointer<vtkPolyData> child500;

};


TEST_F(registration_test, test_registration) {
    registerIdentical();
    registrationQuality();
}