/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak (UCBL-Ifsttar)                              *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "vtkLaplacianPolyData.h"

#include <vtkObjectFactory.h>
#include <vtkCell.h>
#include <vtkMath.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkSortDataArray.h>
#include <vtkTriangleFilter.h>

vtkStandardNewMacro(vtkLaplacianPolyData);

const double vtkLaplacianPolyData::regularizationTerm = 1e-10;

vtkLaplacianPolyData::vtkLaplacianPolyData()
{
    parent = NULL;
    parentlastMT = 0;
    laplacianBuilt = false;
    isSymmetric = false;
}

vtkLaplacianPolyData::~vtkLaplacianPolyData()
{
}

const std::string vtkLaplacianPolyData::ArrNameCotWeights = "lplPolyData_CotWeights";
const std::string vtkLaplacianPolyData::ArrNameEdgeNormals = "lplPolyData_EdgeNormals";
const std::string vtkLaplacianPolyData::ArrNameWeightedEdges = "lplPolyData_WeightedEdges";
const std::string vtkLaplacianPolyData::ArrNamePointAreas = "lplPolyData_PointAreas";
const std::string vtkLaplacianPolyData::ArrNameBoundaryPoints = "lplPolyData_BoundaryPoints";


double vtkLaplacianPolyData::ComputeMeanEdgeLength()
{
    double p0[3], p1[3], p2[3];
    double e01[3], e12[3], e20[3];
    double meanEdgeLength = 0.;
    vtkPoints *points = this->GetPoints();
    // iterate over faces 
    for (vtkIdType i = 0; i < this->GetNumberOfCells(); i++)
    {
        // get vertex coordinates p0, p1, p2 
        vtkIdList *curCellPoints = this->GetCell(i)->GetPointIds();
        points->GetPoint(curCellPoints->GetId(0), p0);
        points->GetPoint(curCellPoints->GetId(1), p1);
        points->GetPoint(curCellPoints->GetId(2), p2);

        // add edge lengths to mean
        vtkMath::Subtract(p1, p0, e01);
        vtkMath::Subtract(p2, p1, e12);
        vtkMath::Subtract(p0, p2, e20);
        meanEdgeLength += vtkMath::Norm(e01);
        meanEdgeLength += vtkMath::Norm(e12);
        meanEdgeLength += vtkMath::Norm(e20);
    }
    meanEdgeLength /= (this->GetNumberOfCells() * 3);

    return meanEdgeLength;
}

void vtkLaplacianPolyData::SetMesh(vtkPolyData *mesh)
{
    if (mesh != parent || mesh->GetMTime() != parentlastMT)
    {
        // triangulate 2D cells
        vtkSmartPointer<vtkTriangleFilter> makeTris = vtkSmartPointer<vtkTriangleFilter>::New();
        makeTris->SetInputData(mesh);
        makeTris->PassLinesOff();
        makeTris->PassVertsOff();
        makeTris->Update();
        vtkPolyData *output = makeTris->GetOutput();
        // copy points from the input, cells from the makeTris output...though should be the same, otherwise it won't work anyway
        this->Allocate(output->GetNumberOfCells());
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        points->DeepCopy(mesh->GetPoints());
        this->SetPoints(points);
        for (vtkIdType i = 0; i < output->GetNumberOfCells(); i++)
        {
            vtkCell *curCell = output->GetCell(i);
            this->InsertNextCell(curCell->GetCellType(), curCell->GetPointIds());
        }

        laplacianBuilt = false;
        parentlastMT = mesh->GetMTime();
        parent = mesh;
        this->Modified();
    }
}

std::vector<std::map<vtkIdType, double>>& vtkLaplacianPolyData::GetNeighborsWithWeights()
{
    return neighborsWithWeights;
}

Eigen::SparseMatrix<double>& vtkLaplacianPolyData::GetLaplacian(bool keepSymmetry)
{
    if (!laplacianBuilt || laplacian.rows() != GetNumberOfPoints() || keepSymmetry != isSymmetric)
        ComputeLaplacian(keepSymmetry);
    return laplacian;
}

void vtkLaplacianPolyData::ComputeLaplacian(bool keepSymmetry)
{
    vtkIdType npts = GetNumberOfPoints();

    // build mesh data - weights etc. - needed to fill the matrix
    cacheMeshData();

    // fill nonzero entries
    std::vector<Eigen::Triplet<double>> nonzerosLaplacian;
    int nz = 0;
    vtkSmartPointer<vtkIdList> adjCells = vtkSmartPointer<vtkIdList>::New();
    for (vtkIdType i = 0; i < npts; i++)
    {
        // get the (duplicated) neighbors of this vertex
        std::map<vtkIdType, double> neighbors = neighborsWithWeights[i];

        // get sum of neighbors' weights
        double columnSum = regularizationTerm;
        for (auto neighbor : neighbors)
            columnSum += neighbor.second;
        
        // set off-diagonal entries below the diagonal (we are filling per-column -> filling column i, row currentNeighbor)
        for (auto neighbor : neighbors)
        {
            if (keepSymmetry || neighbor.first > i) // fill only rows below the diagonal unless keepSymmetry was specified - the matrices are symetric
                nonzerosLaplacian.push_back(Eigen::Triplet<double>(neighbor.first, i, -neighbor.second));
        }
        // set the diagonal entry (for eigen it does not matter where in the initializer list it is)
        nonzerosLaplacian.push_back(Eigen::Triplet<double>(i, i, columnSum)); 
    }
    // finally set the matrices
    laplacian.resize(npts, npts);
    laplacian.setFromTriplets(nonzerosLaplacian.begin(), nonzerosLaplacian.end());

    laplacianBuilt = true;
    isSymmetric = keepSymmetry;
}


void vtkLaplacianPolyData::cacheMeshData()
{
    vtkIdType nCells = GetNumberOfCells();
    vtkIdType npts = GetNumberOfPoints();

    vtkSmartPointer<vtkDoubleArray> edgeNormals = vtkSmartPointer<vtkDoubleArray>::New();
    edgeNormals->SetNumberOfComponents(9);
    edgeNormals->SetNumberOfTuples(nCells);
    edgeNormals->SetName(ArrNameEdgeNormals.c_str());
    GetCellData()->AddArray(edgeNormals);

    vtkSmartPointer<vtkDoubleArray> weightedEdges = vtkSmartPointer<vtkDoubleArray>::New();
    weightedEdges->SetNumberOfComponents(9);
    weightedEdges->SetNumberOfTuples(nCells);
    weightedEdges->SetName(ArrNameWeightedEdges.c_str());
    GetCellData()->AddArray(weightedEdges);

    vtkSmartPointer<vtkDoubleArray> cotWeights = vtkSmartPointer<vtkDoubleArray>::New();
    cotWeights->SetNumberOfComponents(3);
    cotWeights->SetNumberOfTuples(nCells);
    cotWeights->SetName(ArrNameCotWeights.c_str());
    GetCellData()->AddArray(cotWeights);

    vtkSmartPointer<vtkDoubleArray> vertexAreas = vtkSmartPointer<vtkDoubleArray>::New();
    vertexAreas->SetNumberOfComponents(1);
    vertexAreas->SetNumberOfTuples(npts);
    vertexAreas->FillComponent(0, 0); // initialize areas to zero for all vertices
    vertexAreas->SetName(ArrNamePointAreas.c_str());
    GetPointData()->AddArray(vertexAreas);

    std::vector<vtkSmartPointer<vtkIdList>> duplVertexNeighbors;
    duplVertexNeighbors.resize(npts);
    for (int i = 0; i < npts; i++)
        duplVertexNeighbors[i] = vtkSmartPointer<vtkIdList>::New();

    double p[3][3];
    double e[9], eN[9], N[3], w[3], u[3], v[3];
    vtkPoints *points = GetPoints();
    vtkIdType cellNpts; //not really used, we know it's 3
    vtkIdType *curCellPoints;
    for (vtkIdType i = 0; i < nCells; i++)
    {
        // get vertex coordinates 
        GetCellPoints(i, cellNpts, curCellPoints);
        int id0 = curCellPoints[0];
        int id1 = curCellPoints[1];
        int id2 = curCellPoints[2];
        points->GetPoint(id0, p[0]);
        points->GetPoint(id1, p[1]);
        points->GetPoint(id2, p[2]);
        
        // iterate over triangle corners to compute weights
        for (int k = 0; k < 3; k++)
        {
            // get outgoing edge vectors u, v at current corner
            int j0 = k % 3;
            int j1 = (1 + k) % 3;
            int j2 = (2 + k) % 3;
            vtkMath::Subtract(p[j1], p[j0], u);
            vtkMath::Subtract(p[j2], p[j0], v);

            // compute (one-half of) the cotangent weight
            vtkMath::Cross(u, v, N);

            // compute cotangent weights as 0.5 * (cos / sin)
            w[k] = 0.5 * (vtkMath::Dot(u, v) / vtkMath::Norm(N));
        }

        // compute edge vectors 
        vtkMath::Subtract(p[2], p[1], &(e[0]));
        vtkMath::Subtract(p[0], p[2], &(e[3]));
        vtkMath::Subtract(p[1], p[0], &(e[6]));

        // compute normal
        vtkMath::Cross(&(e[3]), &(e[6]), N);

        // compute (one-third of) the triangle area A = |u x v| 
        double area = vtkMath::Norm(N) / 6.0;

        // add contribution to each of the three corner vertices 
        vertexAreas->SetValue(id0, area + vertexAreas->GetValue(id0));
        vertexAreas->SetValue(id1, area + vertexAreas->GetValue(id1));
        vertexAreas->SetValue(id2, area + vertexAreas->GetValue(id2));

        // compute rotated edge vectors
        vtkMath::Cross(N, &(e[0]), &(eN[0]));
        vtkMath::Cross(N, &(e[3]), &(eN[3]));
        vtkMath::Cross(N, &(e[6]), &(eN[6]));

        // scale edge vectors by cotangent weights at opposing corners 
        vtkMath::MultiplyScalar(&(e[0]), w[0]);
        vtkMath::MultiplyScalar(&(e[3]), w[1]);
        vtkMath::MultiplyScalar(&(e[6]), w[2]);

        // store the results
        weightedEdges->SetTuple(i, e);
        edgeNormals->SetTuple(i, eN);
        cotWeights->SetTuple(i, w);

        // to each vertex, add the other two as neighbors. they will be duplicated (for each face they share), but that is later used to find out which are boundary
        duplVertexNeighbors[id0]->InsertNextId(id1);
        duplVertexNeighbors[id0]->InsertNextId(id2);
        duplVertexNeighbors[id1]->InsertNextId(id0);
        duplVertexNeighbors[id1]->InsertNextId(id2);
        duplVertexNeighbors[id2]->InsertNextId(id0);
        duplVertexNeighbors[id2]->InsertNextId(id1);
    }

    // make neighbors list unique and mark boundary neighbors
    neighborsWithWeights.resize(npts);

    vtkSmartPointer<vtkBitArray> boundaryVertices = vtkSmartPointer<vtkBitArray>::New();
    boundaryVertices->SetNumberOfComponents(1);
    boundaryVertices->SetNumberOfTuples(npts);
    boundaryVertices->SetName(ArrNameBoundaryPoints.c_str());
    GetPointData()->AddArray(boundaryVertices);

    boundaryVertices->FillComponent(0, 1); // at the beggining, set all as boundary
    for (vtkIdType i = 0; i < npts; i++)
    {
        if (duplVertexNeighbors[i]->GetNumberOfIds() == 0)
            continue;
        vtkSortDataArray::Sort(duplVertexNeighbors[i]);
        vtkIdType neiID = duplVertexNeighbors[i]->GetId(0);
        neighborsWithWeights[i][neiID] = 0; // set the weight to 0 for now - compute it later
        vtkIdType lastId = neiID; // use the last ID and the fact that it is sorted to know if we encountered the same ID twice or not
        int isBoundary = 0;
        for (vtkIdType j = 1; j < duplVertexNeighbors[i]->GetNumberOfIds(); j++)
        {
            neiID = duplVertexNeighbors[i]->GetId(j);
            if (neiID != lastId) // if this is different then previous one, it is a new unique neighbor -> add it
            {
                neighborsWithWeights[i][neiID] = 0;
                // the next one must be the same, if it is not -> this is the only time this neighbor was met -> this (i-th) vertex is on the boundary
                if (j == duplVertexNeighbors[i]->GetNumberOfIds() - 1 || duplVertexNeighbors[i]->GetId(j + 1) != neiID)
                    isBoundary = 1;
                lastId = neiID;
            }
        }
        boundaryVertices->SetValue(i, isBoundary);
    }

    // store sum of weights of each neighbor vertex across faces that it shares with the current vertex
    BuildLinks();
    vtkSmartPointer<vtkIdList> adjCells = vtkSmartPointer<vtkIdList>::New();
    for (vtkIdType i = 0; i < npts; i++)
    {
        GetPointCells(i, adjCells);
        for (vtkIdType j = 0; j < adjCells->GetNumberOfIds(); j++) // for each adjacent cell to vertex i
        {
            vtkIdType cellID = adjCells->GetId(j);
            vtkIdList* cellPoints = GetCell(cellID)->GetPointIds();
            cotWeights->GetTuple(cellID, w);
            int nei1 = -1;
            double w1;
            for (vtkIdType k = 0; k < cellPoints->GetNumberOfIds(); k++) // for each vertex of that cell
            {
                vtkIdType neiID = cellPoints->GetId(k);
                if (neiID != i) // if it's not the current vertex -> it is neighbor -> add the contribution
                {
                    // the weigths need to be flipped - the weight at w[indexOfFirtsNeighbor] must be assigned to the other neighbor and vice versa
                    // because the weight for neighbor A is the weight at vertex opposite to the edge between A and the current vertex -> the weight at the other neighbor
                    if (nei1 == -1)
                    {
                        nei1 = neiID;
                        w1 = w[k];
                    }
                    else
                    {
                        neighborsWithWeights[i][nei1] += w[k]; // give the current weight to the first neighbor
                        neighborsWithWeights[i][neiID] += w1; // give the first weight to the current (second) neighbor
                    }
                }
            }
        }
    }
}


void vtkLaplacianPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
