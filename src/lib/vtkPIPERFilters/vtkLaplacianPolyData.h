/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak (UCBL-Ifsttar)                              *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef vtkLaplacianPolyData_h
#define vtkLaplacianPolyData_h

#include <vtkSmartPointer.h>
#include <vtkVector.h>
#include <vtkBitArray.h>
#include <vtkPolyData.h>

#include <Eigen/Sparse>


/// <summary>
/// A modification of vtkPolyData that works only with triangle meshes,
/// but is able to compute and store the Laplace operator for the mesh in form of a sparse matrix using Eigen,
/// plus some by-product data relevant for the laplacian computation (cotangent weights of each triangle, edge normals, weighted edge vectors etc.).
/// </summary>
/// <author>Tomas Janak</author>
/// <date>2017</date>
/// <seealso cref="vtkPolyDataAlgorithm" />
class vtkLaplacianPolyData : public vtkPolyData
{
public:
    vtkTypeMacro(vtkLaplacianPolyData, vtkPolyData);
    void PrintSelf(ostream& os, vtkIndent indent);

    static vtkLaplacianPolyData *New();
    
    /// <summary>
    /// Computes the mean length of all edges in the mesh
    /// </summary>
    /// <returns>The mean of lengths of all edges.</returns>
    double ComputeMeanEdgeLength();
    
    /// <summary>
    /// Sets a mesh to use as a templace for this vtkLaplacianPolyData.
    /// The points array will be copied, 1D elements will be ignored, 2D elements will be converted to triangles (the input mesh will remain unchanged).
    /// No cell or point data arrays are copied, only the points and (relevant) cells.
    /// </summary>
    /// <param name="mesh">The geometry to use for this model. If the provided mesh is the same as the one previously used to set up this vtkLaplacianPolyData
    /// and it was not modified since then, this will do nothing - be sure to call Modified() on the mesh if you want the rebuild prior to invoking this method.</param>
    void SetMesh(vtkPolyData *mesh);
    
    /// <summary>
    /// Computes the Laplace operator for this mesh.
    /// This will allways rebuild the current laplacian even if the data (seemingly) did not change.
    /// <seealso cref="GetLaplacian" />
    /// </summary>
    /// <param name="keepSymmetry">If set to <c>true</c>, both triangles of the matrix will be filled. Otherwise, 
    /// only the lower triangle (under diagonal + diagonal) will be filled. Since Laplacian is symmetric, this will often be enough and saves some computation time.</param>
    void ComputeLaplacian(bool keepSymmetry);
    
    /// <summary>
    /// Gets the laplacian of this mesh. If laplacian was not built yet, this will call the ComputeLaplacian,
    /// however in some cases it will not be able to notice that the data changed (e.g. when you change coordinates of some of the points
    /// by directly callint getPoints()->SetPoint(...) ) -> in such cases it is better to directly call ComputeLaplacian before calling GetLaplacian.
    /// </summary>
    /// <param name="keepSymmetry"> If set to <c>true</c>, the both triangles of the matrix will be filled. Otherwise, 
    /// only the lower triangle (under diagonal + diagonal) will be filled. Since Laplacian is symmetric, this will often be enough and saves some computation time.</param>
    /// <returns>The laplacian of this mesh.</returns>
    /// <seealso cref="ComputeLaplacian" />
    Eigen::SparseMatrix<double> &GetLaplacian(bool keepSymmetry);
    
    /// <summary>
    /// This returns the one-ring neighbors of each vertex together with accumulated cotangent weights for each neighbor entry.
    /// </summary>
    /// <returns>A pointer to vector with a map of neighbors for each points. The key to the map is the neighbor's ID, 
    /// the second value is the sum of cotangent weights of that neighbor across all faces it shares with the given vertex it neighbors with.</returns>
    std::vector<std::map<vtkIdType, double>>& GetNeighborsWithWeights();

    // names of mesh data arrays storing various additional information about the mesh

    // cellData array, 3-dimensional (3 values per tuple), first is the weight for vertex 0 (index in the cell), then 1 then 2.
    static const std::string ArrNameCotWeights;
    // cellData array, 9-dimensional (9 values per tuple), first 3 are the normal for edge opposite to vertex 0, then opposite to 1 and opposite to 2
    static const std::string ArrNameEdgeNormals;
    // cellData array, 9-dimensional (9 values per tuple), first 3 are edge opposite to vertex 0, then opposite to 1 and opposite to 2
    static const std::string ArrNameWeightedEdges;
    // pointData array, 1-dimensional, area associated with that vertex (sum of 1/3 of areas of adjacent triangles)
    static const std::string ArrNamePointAreas;
    // pointData array, marks 0 if the vertex is not on the boundary and 1 if it is
    static const std::string ArrNameBoundaryPoints;

    // used to fill "zero" elements in the laplacian - pure zeros with some meshes cause the matrix to not be strictly positive definite which results in numerical errors
    static const double regularizationTerm; // = 1e-10
protected:
    vtkLaplacianPolyData();
    ~vtkLaplacianPolyData();
        
    /// <summary>
    /// Computes and stores the required mesh data, namely the cotangent weights (0.5 * cot(a), where a is the angle between respective edges),
    /// edge normals and weighted edge vectors (scaled by the cotangent weights of opposing vertices) for each edge pair of each triangle,
    /// and the vertex areas as a sum of one third of the area of each adjacent triangles.
    /// </summary>
    void cacheMeshData();
        
    Eigen::SparseMatrix<double> laplacian;
    bool laplacianBuilt; // boolean flag tracing if the laplacian was built or not, it resets when input data change
    bool isSymmetric; // flag that hold whether the built laplacian is symmetric or not. If laplacianBuilt is false, this has no meaning

    // if setMesh is used, the pointer of the provided vtkPolyData is stored in this variable for future reference to avoid unnecessary re-setting of this vtkLaplacianPolyData
    vtkPolyData *parent;
    unsigned long parentlastMT; // last modification time of the parent (when it was set through SetMesh())

    // contains lists of IDs of neighbor vertices for each vertex + their cotangent weight, summed over each face they share
    std::vector<std::map<vtkIdType, double>> neighborsWithWeights;
};

#endif
