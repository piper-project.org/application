/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak (UCBL-Ifsttar)                              *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef vtkSurfaceDistance_h
#define vtkSurfaceDistance_h

#include "vtkLaplacianPolyData.h"

#include <vtkPolyDataAlgorithm.h>

#include <memory>


#define _GEODESIC_CHOSENPOINTS "_geodesic_chosenpoints" // name of the pointdata vtkBitArray that marks for which point to compute the distance

enum SurfaceDistance {
    Green = 0,
    Biharmonic = 1,
    BiharmGreenCombined = 2, // distance is an interpolation between Green and Biharmonic computed as a (1-w(i,j)) * Green(i,j) + w(i,j) * Biharmonic(i,j), where w(i,k) = Biharmonic(i, j) / maxBiharmonic
    GeodesicByHeat = 3
};

enum DistanceComputeSubset { 
    AllToAll, // computes for all points to all points of the input; results in a npts x npts distance matrix
    SourceToAll, // computes only for points in the source list to all other point; use SetSourcePoints to set the list; results in a pointDataArray written to the output
    EachChosenToEachChosen, // computes distances only among points selected in the _GEODESIC_CHOSENPOINTS vtkBitArray; 
                            // results in nChosen x nChosen distance matrix, where nChosen is the number of nodes for which the array has 1
    EachChosenToAll  // computes from each chosen point (_GEODESIC_CHOSENPOINTS pointData array) to each other node of the mesh; results in nChosen x npts distance matrix 
};

/// <summary>
/// A vtkPolyData filter that computes a surface distance (i.e. topology-aware distance, such as geodesic distance)
/// for the input mesh's vertices. The implemented distances are:
/// 1) Geodesics in Heat: A New Approach to Computing Distance Based on Heat Flow
/// by Keenan Crane and Clarisse Weischedel and Max Wardetzky,
/// published in ACM Transactions on Graphics 32, issue 5, 2013.
/// 2) Biharmonic Distance
/// by Yaron Lipman, Raif Rustamov, Thomas Funkhouser, 
/// published in ACM Transactions on Graphics 29, issue 3, 2010.
/// </summary>
/// <author>Tomas Janak</author>
/// <date>2017</date>
/// <seealso cref="vtkPolyDataAlgorithm" />
class vtkSurfaceDistance : public vtkPolyDataAlgorithm
{
public:
    vtkTypeMacro(vtkSurfaceDistance, vtkPolyDataAlgorithm);
    void PrintSelf(ostream& os, vtkIndent indent);

    static vtkSurfaceDistance *New();
    
    /// <summary>
    /// Sets the time step for the GeodesicByHeat method to a specified value.
    /// It is recommended to NOT set this - if not set, it will computed automatically as the square of mean edge length of the input.
    /// </summary>
    /// <param name="_arg">The time step. Must be a positive number, 
    /// otherwise will be ignored and time step will be estimated by mean edge length.</param>
    vtkSetMacro(TimeStepHeat, double);
    vtkGetMacro(TimeStepHeat, double);
    
    /// <summary>
    /// Uses the square of mean edge length as the time step. This is the default behaviour - 
    /// you don't need to call this unless you have previously called SetTimeStep and want to go back to the default.
    /// </summary>
    void UseMeanEdgeLengthForTimeStep();

    /// <summary>
    /// Sets the boundary conditions for the heat equation.
    /// </summary>
    /// <param name="_arg">The boundary condition. bc = 0 -> pure Neumann, 1 > bc > 0 -> Dirichlet/Neumann interpolated, bc > 1 -> pure Dirichlet. Default 0.</param>
    vtkSetMacro(BoundaryConditions, double);
    vtkGetMacro(BoundaryConditions, double);
    
    /// <summary>
    /// Sets the list of point IDs to be used as sources for the distance computations, i.e. for them distance = 0;
    /// Typically will contain only a single node to get the distance field for it.
    /// !!! Call SetComputeForSource(true) to use these points, otherwise they will be ignored and distance fields for all singular points will be computed.
    /// !!! If the used distance type is Biharmonic (default), only the first point in the list will be taken into account - it is not possible 
    /// to have multiple "sources" for Biharmonic distance.
    /// </summary>
    /// <param name="_arg">List of IDs to the input mesh. The pointer itself is stored - if you modify it after setting it here,
    /// the modification will be taken into account.</param>
    /// <seealso cref="SetComputeForSource" />
    vtkSetMacro(SourcePoints, vtkIdList *);
    vtkGetMacro(SourcePoints, vtkIdList *);
    
    /// <summary>
    /// Specifies the mode of the filter. Default is AllToAll - computes distance fields for all points.
    /// </summary>
    /// <param name="_arg">  
    /// ----
    /// For the AlltoAll mode, result is a npts x npts symmetric matrix, where npts is the total number of points in the mesh,
    /// that in i-th row and j-th column contains the surface distance of the point i to the point j, obtainable through GetDistanceMatrix().
    /// ----
    /// Result in the SourceToAll mode is a single distance field,
    /// stored as pointData named using the vtkSurfaceDistance::surfaceDistanceMapName of the output mesh, 
    /// that is equal to zero for the source points and grows with surface distance from them.
    /// Note that for the biharmonic distance, there is no simplified way to compute the distance from one point to each other,
    /// so all distances are computed and only the distance for the chosen point written in the output array - 
    /// this means the computation is significantly longer, but at least the complete DistanceMatrix can be obtained.
    /// ---
    /// For the EachChosenToEachChosen and EachChosenToAll, the _GEODESIC_CHOSENPOINTS vtkBitArray pointData array
    /// must be defined on the input, otherwise the filter will switch to AllToAll mode. In the first case,
    /// distance is computed only for the subset of the selected points in this array (marked by 1), to each other such point,
    /// resulting in a nChosen x nChosen distance matrix. For the second mode, the distance from each such point to each other point of the mesh
    /// is computed, resulting in npts x nChosen distance matrix.
    /// </param>
    /// <seealso cref="SetSourcePoints" />
    void SetMode(DistanceComputeSubset _arg);
    vtkGetMacro(Mode, DistanceComputeSubset);
        
    /// <summary>
    /// Specifies the distance that will be computed. Default is BiharmonicDistance.
    /// </summary>
    /// <param name="_arg">Distance algorithm to use. GeodesicByHeat computes the distance according to 
    /// Geodesics in Heat : A New Approach to Computing Distance Based on Heat Flow
    /// by Keenan Crane and Clarisse Weischedel and Max Wardetzky
    /// published in ACM Transactions on Graphics 32, issue 5, 2013.
    /// Biharmonic/Green computes the distance according to 
    /// Biharmonic Distance by Yaron Lipman, Raif Rustamov, Thomas Funkhouser, 
    /// published in ACM Transactions on Graphics 29, issue 3, 2010.
    /// </param>
    vtkSetMacro(ComputeDistance, SurfaceDistance);
    vtkGetMacro(ComputeDistance, SurfaceDistance);

    /// <summary>
    /// Specifies precision of biharmonic/green distance, which are approximated by using only some eigen values
    /// rather than all of them in the computation. This parameter controls how many is that.
    /// </summary>
    /// <param name="_arg">Only <c>(npts / EigenPrecision)</c> eigenvalues will be used, therefore the lower the number, the more precise (but longer) computation.
    /// If it is lower or equal to 1, the default number will be used (50) (TODO - this case should instead activate the exact mode).</param>
    void SetEigenPrecision(double _arg);
    vtkGetMacro(EigenPrecision, double);
    
    /// <summary>
    /// Gets the distance matrix. Use SetComputeForSource(false) (the default) to turn on computation of this matrix.
    /// </summary>
    /// <returns>The matrix containing the distances from point i to point j on index (i,j) for each points of the input mesh.</returns>
    /// <seealso cref="SetComputeForSource" />
    std::shared_ptr<Eigen::MatrixXd> GetDistanceMatrix() { return this->DistanceMatrix; } // do not use vtkGetMacro, does not work well with the Eigen typdefs for some reason
    
    static const std::string surfaceDistanceMapName;
protected:
    vtkSurfaceDistance();
    ~vtkSurfaceDistance();
    
    /// <summary>
    /// Builds the laplacian of the input mesh and heat equation matrices and factorizes them.
    /// </summary>
    void buildMatricesGeodesicByHeat();
    
    /// <summary>
    /// Solves the built heat method system - computes the heat, then potential and finally the distance. Make sure buildMatricesGeodesicByHeat was called before.
    /// </summary>
    /// <param name="isSource">Vector containing 1 for source point(s) and 0 for all other points.</param>
    /// <returns>The distance from source(s) for each point.</returns>
    Eigen::VectorXd solveGeodesicByHeat(Eigen::VectorXd& isSource);        

    /// <summary>
    /// Builds the laplacian^2 of the input mesh; weighs it by inverse vertex areas and factorizes it.
    /// </summary>
    void buildMatricesBiharmonic();

    /// <summary>
    /// Performs computation of geodesic distance by heat for the provided nodes.
    /// Each node is treated as a single source, i.e. this is not useful for when you want to compute distance to a group of nodes.
    /// </summary>
    /// <param name="sources">The IDs of nodes for which to compute the distance to each other. If it is empty, 
    /// distances will be computed for each pair.</param>
    void processSingleSourcesGeodesicByHeat(std::vector<vtkIdType> &sources);

    /// <summary>
    /// Performs computation of biharmonic distance for the provided nodes.
    /// Also processes Green or GreenBiharmonic combined distances, since they are based on the same algorithm.
    /// Each node is treated as a single source, i.e. this is not useful for when you want to compute distance to a group of nodes.
    /// </summary>
    /// <param name="sources">The IDs of nodes for which to compute the distance to each other. If it is empty, 
    /// distances will be computed for each pair.</param>
    void processSingleSourcesBiharmonic(std::vector<vtkIdType> &sources);

    virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
    
    DistanceComputeSubset Mode;
    SurfaceDistance ComputeDistance;

    unsigned long inputLastMTime; // the modification time of the input when it was used to compute the time step - to know whether it needs to be recomputed or not
    unsigned long lastRunTime; // to compare with current modification time to know if some parameters changed or not
    unsigned long lastModeChangeTime; // used to ensure that the matrices will not be rebuild if only change since lastRunTime was changing the Mode of the filter (which does not affect the matrices)
    vtkIdList *SourcePoints;
    bool matricesBuilt;

    vtkSmartPointer<vtkLaplacianPolyData> inputData; // input data transformed to vtkLaplacianPolyData
    std::shared_ptr<Eigen::MatrixXd> DistanceMatrix;

    // parameters for the heat method
    double TimeStepHeat; // time step to set to override estimation
    double dtHeat; // time step actually used in the last update call
    double BoundaryConditions;
    double EigenPrecision;
    Eigen::SparseMatrix<double> heatFlowDirichlet;
    Eigen::SparseMatrix<double> heatFlowNeumann;
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double>> choleskyLaplacian; // factors of the matrices
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double>> choleskyNeumann;
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double>> choleskyDirichlet;
    Eigen::VectorXd laplaceEigenValues;
    Eigen::MatrixXd laplaceEigenVectors;



};

#endif
