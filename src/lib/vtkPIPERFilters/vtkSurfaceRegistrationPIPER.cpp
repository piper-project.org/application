/*******************************************************************************
* Copyright (C) 2019 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Yoann Lafon (UCBL-Ifsttar)                 *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "vtkSurfaceRegistrationPIPER.h"
#include "vtkLaplacianPolyData.h"
#include "vtkOBJWriter.h"

#include <vtkObjectFactory.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkMath.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkKdTreePointLocator.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkMeshQuality.h>

#include <Eigen/SVD>
#include <Eigen/Eigen>

#include <unordered_set>

vtkStandardNewMacro(vtkSurfaceRegistrationPIPER);

namespace // just some simple helper routines
{
    // helper structure used by vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::SmoothSharpAngles 
    struct DihAngleEntry
    {
    public:

        DihAngleEntry()
        {
            angle = 0.0;
            p[0] = p[1] = p[2] = 0.0;
        }

        DihAngleEntry(double point[3], double angle)
        {
            p[0] = point[0];
            p[1] = point[1];
            p[2] = point[2];
            this->angle = angle;
        }

        double p[3];
        double angle;
    };

    // Computes per-comoponent division of two vectors and stores in c: c = a ./ b
    // i.e. c[i] = a[i] / b[i]
    // ! Does not check for division by 0, make sure all components of b are non-zero.
    inline void vectorDividePerComponent(double a[3], double b[3], double c[3])
    {
        c[0] = a[0] / b[0];
        c[1] = a[1] / b[1];
        c[2] = a[2] / b[2];
    }

    // Normalizes a given value to <-1,1> i.e.:
    // 1. transfer to 0 (by subtracting min of that dimension), 2. scale to <0,1> (divide by minMaxDif) , 3. stretch to <-1,1> by doing (2*X - 1)
    // min is the minimal value among all values that are being normalized.
    // minMaxDif is then max - min
    inline double normalizeValue(double valueToNormalize, double minMaxDif, double min)
    {
        return ((valueToNormalize - min) / minMaxDif) * 2 - 1;
    }

    // Compares "value" to numbers in minMax and sets minMax[0] = value if value < minMax[0]
    // and sets minMax[1] = value if value > minMax[1]. 
    // No pointer checks, use with caution.
    void setMinMax(double *minMax, double value)
    {
        if (value < minMax[0]) minMax[0] = value;
        if (value > minMax[1]) minMax[1] = value;
    }
}



#pragma region VtkPolyDataMetrics

vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::VtkPolyDataMetrics()
{
    mesh = nullptr;
    nbBuckets = 0;
    curvatureHistogram = nullptr;
    curvatureHistogramBounds = nullptr;
    oneRingPoints = nullptr;
    twoRingPoints = nullptr;
    fourRingPoints = nullptr;
    dihAngles = nullptr;
    normals = vtkSmartPointer<vtkPolyDataNormals>::New();
    normals->ComputeCellNormalsOn(); // used for computing point normals anyway and we sometimes need it for computing dihedral angles
    normals->ComputePointNormalsOn();
    normalComputeTime = 0;
    histComputeTime = 0;
}


vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::~VtkPolyDataMetrics()
{
    if (curvatureHistogram != nullptr)
    {
        deleteHistogram();
    }
    if (oneRingPoints != nullptr)
        delete[] oneRingPoints;
    if (twoRingPoints != nullptr)
        delete[] twoRingPoints;
    if (fourRingPoints != nullptr)
        delete[] fourRingPoints;
    if (dihAngles != nullptr)
        delete[] dihAngles;
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::deleteHistogram()
{
    unsigned int histSize = nbBuckets * 2;
    for (unsigned int i = 0; i < histSize; i++)
    {
        delete[] curvatureHistogram[i];
        delete[] curvatureHistogramTemp[i];
    }
    delete[] curvatureHistogram;
    delete[] curvatureHistogramTemp;
    delete[] curvatureHistogramBounds; // curvatureHistogramBounds is always created together with curvature histogram - if that is not nullptr, so are the bounds
    delete[] bucket_limit;
    curvatureHistogram = nullptr;
}

bool vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::Initialize(vtkSmartPointer<vtkPolyData> mesh, int nb_histogramBuckets)
{
    if (nb_histogramBuckets < 2) // there must be at least 1 bucket
        nb_histogramBuckets = 2;
    vtkIdType oldNbPoints = -1;
    vtkIdType npts = mesh->GetNumberOfPoints();
    int histogramSize = 2 * nb_histogramBuckets;  // histogram is actually concatenation of two histograms
    if (this->mesh != nullptr
        && (this->mesh->GetNumberOfPoints() != mesh->GetNumberOfPoints() || nb_histogramBuckets != nbBuckets)) // if the new mesh or nbofbuckets is different, we need to re-allocate histogram
        deleteHistogram();     // delete the old histogram - we will need a new one
    if (curvatureHistogram == nullptr)
    {
        curvatureHistogram = new double*[histogramSize];
        curvatureHistogramTemp = new double*[histogramSize];
        for (int i = 0; i < histogramSize; i++)
        {
            curvatureHistogram[i] = new double[npts];
            curvatureHistogramTemp[i] = new double[npts];
        }
        curvatureHistogramBounds = new double[histogramSize * 2]; // bounds is min, max for each bucket -> *2
        
        // histogram is based on dot products, which are <-1;1> -> create HistogramBuckets buckets that span this interval evenly
        // bucket_limit has the lower boundary of each bucket
        bucket_limit = new double[nb_histogramBuckets];
        double bucketSize = 2.0 / nb_histogramBuckets;
        bucket_limit[0] = -1;
        for (int i = 1; i < nb_histogramBuckets; i++)
            bucket_limit[i] = -1.0 + i * bucketSize;
    }
    this->mesh = mesh;
    this->mesh->ComputeBounds();
    this->mesh->BuildLinks();

    // build the list of points in neighborhood for each point
    if (oneRingPoints != nullptr)
        delete[] oneRingPoints;
    if (twoRingPoints != nullptr)
        delete[] twoRingPoints;
    if (fourRingPoints != nullptr)
        delete[] fourRingPoints;
    if (dihAngles != nullptr)
        delete[] dihAngles;
    oneRingPoints = new vtkSmartPointer<vtkIdList>[npts];
    twoRingPoints = new vtkSmartPointer<vtkIdList>[npts];
    fourRingPoints = new vtkSmartPointer<vtkIdList>[npts];
    dihAngles = new vtkSmartPointer<vtkDoubleArray>[npts];
    for (vtkIdType i = 0; i < npts; i++)
    {
        vtkSmartPointer<vtkIdList> oneRingCells = vtkSmartPointer<vtkIdList>::New(); // contains all cells on one-ring
        oneRingPoints[i] = vtkSmartPointer<vtkIdList>::New();

        // get all cells tied to the specified point
        this->mesh->GetPointCells(i, oneRingCells);
        getNeighborhoodPoints(i, oneRingCells, oneRingPoints[i]);
        if (oneRingPoints[i]->GetNumberOfIds() == 0)
            return false;
        // oneRingPoints[i] now has all the unique points in one-ring, extend it by adding the one-ring of all of those points
        vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
        // first collect a (unique) list of all the cells in two-ring by going through cells tied to points on one-ring
        vtkSmartPointer<vtkIdList> twoRingCells = vtkSmartPointer<vtkIdList>::New(); // contains all cells in two-ring which are NOT in one-ring
        for (vtkIdType j = 0; j < oneRingPoints[i]->GetNumberOfIds(); j++)
        {
            cellIds->Reset();
            mesh->GetPointCells(oneRingPoints[i]->GetId(j), cellIds);
            for (vtkIdType k = 0; k < cellIds->GetNumberOfIds(); k++)
            {
                vtkIdType twoRingCellId = cellIds->GetId(k);
                // look if this cell is in one ring, if not, add it to the list of two-ring cells
                for (vtkIdType oneRingIDs = 0; oneRingIDs < oneRingCells->GetNumberOfIds(); oneRingIDs++)
                {
                    if (oneRingCells->GetId(oneRingIDs) == twoRingCellId)
                    {
                        twoRingCellId = -1;
                        break;
                    }
                }
                if (twoRingCellId > -1) // it is -1 if it is part of one ring - in that case we dont want it among the two-ring cells
                    twoRingCells->InsertUniqueId(twoRingCellId);
            }
        }
        // now we have a list of cells that are in the two-ring, but not in one ring -> add their points to the twoRingPoints list
        // those cells surely also contain all points in one ring -> twoRingPoints have both
        twoRingPoints[i] = vtkSmartPointer<vtkIdList>::New();
        getNeighborhoodPoints(i, twoRingCells, twoRingPoints[i]);
    }
    for (vtkIdType i = 0; i < npts; i++)
    {
        std::unordered_set<double> fourRing; // using unordered set to merge the neighborhoods efficiently (InsertUniqueId is N^2)
        for (vtkIdType j = 0; j < twoRingPoints[i]->GetNumberOfIds(); j++)
        {
            vtkIdType nei = twoRingPoints[i]->GetId(j);
            fourRing.insert(nei);
            for (vtkIdType k = 0; k < twoRingPoints[nei]->GetNumberOfIds(); k++)
            {
                vtkIdType id = twoRingPoints[nei]->GetId(k);
                if (id != i)
                    fourRing.insert(id);
            }
        }
        // now insert the results from the set to the vtkIdList by using O(1) SetId()
        fourRingPoints[i] = vtkSmartPointer<vtkIdList>::New();
        fourRingPoints[i]->SetNumberOfIds(fourRing.size());
        vtkIdType idIndex = 0;
        for (auto id : fourRing)
            fourRingPoints[i]->SetId(idIndex++, id);

        // while we're here, let's allocate the dihAngles
        dihAngles[i] = vtkSmartPointer<vtkDoubleArray>::New();
        dihAngles[i]->SetNumberOfComponents(1);
        dihAngles[i]->SetNumberOfValues(oneRingPoints[i]->GetNumberOfIds());
    }


    nbBuckets = nb_histogramBuckets;    
    normals->SetInputData(this->mesh);
    return true;
}


const double* vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetPointNormalBounds()
{
    computeNormals();
    return normalBounds;
}

const double* vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetPointBounds()
{
    mesh->ComputeBounds();
    mesh->GetBounds(pointBounds);
    return pointBounds;
}

const double* vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetCurvatureHistogramBounds()
{
    computeHistogram();
    return curvatureHistogramBounds;
}

vtkSmartPointer<vtkPolyData> vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetPolyData()
{
    return mesh;
}

vtkSmartPointer<vtkFloatArray> vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetPointNormals()
{
    computeNormals();
    return vtkFloatArray::SafeDownCast(mesh->GetPointData()->GetNormals());
}

vtkSmartPointer<vtkFloatArray> vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetCellNormals()
{
    computeNormals();
    return vtkFloatArray::SafeDownCast(mesh->GetCellData()->GetNormals());
}

double **vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetCurvatureHistograms()
{
    computeHistogram();
    return curvatureHistogram;
}

int vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetHistogramSize()
{
    return 2 * nbBuckets;
}

const vtkSmartPointer<vtkIdList> *vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetOneRingPoints()
{
    return oneRingPoints;
}

const vtkSmartPointer<vtkIdList> *vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::GetTwoRingPoints()
{
    return twoRingPoints;
}

vtkSmartPointer<vtkDoubleArray> vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::SmoothFieldOneRing
    (vtkSmartPointer<vtkDoubleArray> field, int nbIterations, vtkSmartPointer<vtkIdTypeArray> fixedPoints)
{
    vtkIdType npts = mesh->GetNumberOfPoints();
    if (field->GetNumberOfTuples() == npts)
    {
        vtkIdType ncomp = field->GetNumberOfComponents();
        double *value = new double[ncomp];
        double *temp = new double[ncomp];
        vtkSmartPointer<vtkDoubleArray> smoothed[2]; // have two arrays we can swap between iterations
        smoothed[0] = vtkSmartPointer<vtkDoubleArray>::New();
        smoothed[0]->DeepCopy(field);
        smoothed[1] = vtkSmartPointer<vtkDoubleArray>::New();
        smoothed[1]->SetNumberOfComponents(ncomp);
        smoothed[1]->SetNumberOfTuples(npts);
        int prevIt = 0, curIt = 1; // prev it = index of array containing results of the previous iteration, curIt = after smoothing
        for (int iter = 0; iter < nbIterations; iter++)
        {
            for (vtkIdType i = 0; i < npts; i++)
            {
                smoothed[prevIt]->GetTuple(i, value);
                int div = 1; // sum of all weights, start with +1 because it also includes the value in the point itself
                if (fixedPoints == nullptr || fixedPoints->GetValue(i) < 0) // if the target position is prescribed, just copy the position, do not smooth it
                {
                    vtkIdType nNeighbors = oneRingPoints[i]->GetNumberOfIds();
                    for (vtkIdType j = 0; j < nNeighbors; j++)
                    {
                        vtkIdType neiID = oneRingPoints[i]->GetId(j);
                        smoothed[prevIt]->GetTuple(neiID, temp);
                        div++;

                        for (vtkIdType c = 0; c < ncomp; c++)
                            value[c] += temp[c];
                    }
                    for (vtkIdType c = 0; c < ncomp; c++)
                        value[c] /= div;
                }

                smoothed[curIt]->SetTuple(i, value);
            }
            // swap the buffers
            prevIt = curIt;
            curIt = 1 - prevIt;
        }
        delete[] value;
        delete[] temp;
        return smoothed[prevIt]; // at the end of last iteration curIt and prevIt get switched -> last computed field is at prevIt index
    }
    return field;
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::SmoothSharpAngles()
{
    // compute mean cell area (imho overkill, estimation based on a smaller number of cells should be OK)
    vtkIdType nCells = mesh->GetNumberOfCells();
    double cellAreaFactor = 0;
    vtkSmartPointer<vtkMeshQuality> areaComputer = vtkSmartPointer<vtkMeshQuality>::New();
    areaComputer->SetInputData(mesh);
    areaComputer->SetTriangleQualityMeasureToArea();
    areaComputer->SetQuadQualityMeasureToArea();
    areaComputer->Update();
    vtkSmartPointer<vtkDoubleArray> qualityArray = vtkDoubleArray::SafeDownCast(areaComputer->GetOutput()->GetCellData()->GetArray("Quality"));
    for (vtkIdType i = 0; i < qualityArray->GetNumberOfTuples(); i++) // compute sum of all areas
        cellAreaFactor += qualityArray->GetValue(i);

    cellAreaFactor = sqrt(cellAreaFactor / nCells) * 0.5;

    vtkIdType npts = mesh->GetNumberOfPoints();
    std::map<vtkIdType, DihAngleEntry> displacement; // only for points that are on cells that have too sharp angles
    double tempNormal[3], tempNormal2[3];
    vtkSmartPointer<vtkFloatArray> pointNormals = GetPointNormals();
    vtkSmartPointer<vtkDoubleArray> dispField = vtkSmartPointer<vtkDoubleArray>::New();
    dispField->SetNumberOfComponents(3);
    dispField->SetNumberOfTuples(npts);
    for (int i = 0; i < 20; i++)
    {
        computeDihedralAngles();
        for (vtkIdType j = 0; j < npts; j++) // for each point
        {
            vtkIdType nNei = oneRingPoints[j]->GetNumberOfIds();
            for (vtkIdType k = 0; k < nNei; k++) // for each of its neighbors
            {
                double angle = dihAngles[i]->GetValue(k);
                if (angle < -0.6) // if the angle between their shared cells is too sharp // why not absolute value?
                {
                    vtkIdType neiID = oneRingPoints[j]->GetId(k);
                    pointNormals->GetTuple(j, tempNormal);
                    vtkMath::MultiplyScalar(tempNormal, 0.2 * cellAreaFactor);
                    displacement[j] = DihAngleEntry(tempNormal, angle);
                    break; // one neighbor is enough to move this point
                }
            }
        }
        vtkSmartPointer<vtkPoints> origPoints = mesh->GetPoints();
        vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();
        newPoints->DeepCopy(origPoints);
        for (auto sharpPoint : displacement) // iterate only through the point that were modified
        {
            origPoints->GetPoint(sharpPoint.first, tempNormal);
            vtkMath::Add(tempNormal, sharpPoint.second.p, tempNormal);
            newPoints->SetPoint(sharpPoint.first, tempNormal);
        }
        mesh->SetPoints(newPoints);
        newPoints->Modified();
        computeDihedralAngles();
        // verifying sign of the displacement - if it worsened the angles, switch it
        dispField->FillComponent(0, 0);
        dispField->FillComponent(1, 0);
        dispField->FillComponent(2, 0);
        for (auto sharpPoint : displacement) // iterate only through the point that were modified
        {
            vtkIdType nNei = oneRingPoints[sharpPoint.first]->GetNumberOfIds();
            for (vtkIdType k = 0; k < nNei; k++) // for each of its neighbors
            {
                if (sharpPoint.second.angle > dihAngles[sharpPoint.first]->GetValue(k)) // if the angle sharpened
                {
                    vtkMath::MultiplyScalar(sharpPoint.second.p, -1);
                    break;
                }
            }
            // write the final displacement into the dispField array
            vtkMath::MultiplyScalar(sharpPoint.second.p, 20); // why * 20?
            dispField->SetTuple(sharpPoint.first, sharpPoint.second.p);
        }
        dispField = SmoothFieldOneRing(dispField, 4);
        // apply the displacement to the orig point positions, set the final positions
        for (vtkIdType j = 0; j < npts; j++) // this time iterate all the points - smoothing likely caused non-zero displacement on many points
        {
            origPoints->GetPoint(j, tempNormal);
            dispField->GetTuple(j, tempNormal2);
            vtkMath::Add(tempNormal, tempNormal2, tempNormal);
            origPoints->SetPoint(j, tempNormal);
        }
        mesh->SetPoints(origPoints);
        newPoints->Modified();
    }
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::AvoidSelfPenetration()
{
    // n = normales_vertex_area(p, t, ngb_t);
    // a = faire(p, t);
    // v = sqrt(mean(a))*0.5;
    // v = roughly size of elements, used as the maximum gap to put between two points
    // original code sets it based on mean of all element areas, but we can get similar estimates more efficiently
    vtkIdType nCells = mesh->GetNumberOfCells();
    double cellAreaFactor = 0;
    vtkSmartPointer<vtkMeshQuality> areaComputer = vtkSmartPointer<vtkMeshQuality>::New();
    areaComputer->SetInputData(mesh);
    areaComputer->SetTriangleQualityMeasureToArea();
    areaComputer->SetQuadQualityMeasureToArea();
    areaComputer->Update();
    vtkSmartPointer<vtkDoubleArray> qualityArray = vtkDoubleArray::SafeDownCast(areaComputer->GetOutput()->GetCellData()->GetArray("Quality"));
    for (vtkIdType i = 0; i < qualityArray->GetNumberOfTuples(); i++) // compute sum of all areas
        cellAreaFactor += qualityArray->GetValue(i);

    cellAreaFactor = std::sqrt(cellAreaFactor / nCells) * 0.5;
    
    double point[3], curPoint[3], normal[3]{0,0,0};
    vtkIdType npts = mesh->GetNumberOfPoints();
    vtkSmartPointer<vtkDoubleArray> dispField = vtkSmartPointer<vtkDoubleArray>::New();
    dispField->SetNumberOfComponents(3);
    dispField->SetNumberOfTuples(npts);
    for (int iter = 0; iter < 20; iter++) // run at most 20 iterations of depenetration
    {
        vtkSmartPointer<vtkFloatArray> normals = GetPointNormals();
        vtkSmartPointer<vtkKdTreePointLocator> pl = vtkSmartPointer<vtkKdTreePointLocator>::New();
        pl->SetDataSet(mesh);
        pl->BuildLocator();
        int nbCorrected = 0;
        for (vtkIdType i = 0; i < npts; i++)
        {
            mesh->GetPoint(i, curPoint);
            // set tolerance == the smallest edge among all edges the current point is connected to
            // this is the maximum distance we will enforce as a gap between points
            double tolerance = std::numeric_limits<double>::max();
            for (vtkIdType nei = 0; nei < oneRingPoints[i]->GetNumberOfIds(); nei++)
            {
                mesh->GetPoint(oneRingPoints[i]->GetId(nei), point);
                tolerance = vtkMath::Min(tolerance, vtkMath::Distance2BetweenPoints(point, curPoint));
            }
            tolerance = std::sqrt(tolerance) * 0.33;
            // for each point of the mesh, find the closest point that is at most "tolerance" away from it
            // if there is no point as close, we surely do not need to move this point, as we would be moving by at most "tolerance" anyway
            double correctiveDistance = 0;
            vtkSmartPointer<vtkIdList> result = vtkSmartPointer<vtkIdList>::New();
            pl->FindClosestNPoints(2, curPoint, result);
            if (result->GetNumberOfIds() >= 2)
            {
                vtkIdType closestPointID = result->GetId(1); // the first one is always the point itself, the second is the closest other point
                // project the displacement vector between the current point and the closest one onto the normal of the current point
                // this way if the closest point is in the direction of the normal, we get high value (-> potential penetration)
                // while if it is not, we get small value (nothing to correct). So if the point would be moving in the normal direction,
                // it would be causing penetrations (there is some other point in the way so likely some other side of the surface)
                // -> we prevent this penetration by enforcing the gap between them
                mesh->GetPoint(closestPointID, point);
                if (std::sqrt(vtkMath::Distance2BetweenPoints(point, curPoint)) >  tolerance)
                    correctiveDistance = 0;
                else
                {
                    // d = p(wm0, :) - p;
                    vtkMath::Subtract(point, curPoint, point); // point now has the displacement
                    // d = sum(n.*d, "c");
                    normals->GetTuple(i, normal);
                    correctiveDistance = vtkMath::Dot(normal, point);

                    // d(d <= 0) = v;
                    // d(d>v) = v;
                    // d = d - v;
                    // this leads to some strange results...if the nearest point is nearly coplanar, it will set the distance to 0
                    // if it projects as negative, but to almost -tolerance if it projects as positive, so there is large discontinuity around 0
                    if (correctiveDistance <= 0 || correctiveDistance > tolerance)
                        correctiveDistance = 0;
                    else
                    {
                        correctiveDistance = correctiveDistance - tolerance;
                        nbCorrected++;
                    }
                }
            }
            dispField->SetTuple3(i, normal[0] * correctiveDistance, normal[1] * correctiveDistance, normal[2] * correctiveDistance);
        }
        vtkSmartPointer<vtkDoubleArray> smoothed = SmoothFieldOneRing(dispField, 4);
        // p = p + d;
        for (vtkIdType i = 0; i < npts; i++)
        {
            mesh->GetPoint(i, curPoint);
            smoothed->GetTuple(i, point);
            vtkMath::Add(curPoint, point, curPoint);
            mesh->GetPoints()->SetPoint(i, curPoint);
        }
        mesh->GetPoints()->Modified();
        if (nbCorrected == 0)
            break;
    }
}


void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::AlignRigid(vtkSmartPointer<vtkPoints> target, vtkSmartPointer<vtkDoubleArray> displacement)
{
    vtkIdType npts = mesh->GetNumberOfPoints();
    if (target->GetNumberOfPoints() == npts)
    {
        if (displacement->GetNumberOfComponents() != 3)
            displacement->SetNumberOfComponents(3);
        if (displacement->GetNumberOfTuples() != npts)
            displacement->SetNumberOfTuples(npts);

        double zeros[3]{ 0, 0, 0 };
        for (vtkIdType i = 0; i < npts; i++)
            displacement->SetTuple(i, zeros);
        double sourceP[3], targetP[3], tempVector[3];
        for (vtkIdType i = 0; i < npts; i++)
        {
            target->GetPoint(i, targetP);
            mesh->GetPoint(i, sourceP);
            //centered vectors
            vtkIdType nbNei = oneRingPoints[i]->GetNumberOfIds();
            // matrices containing directional vectors from the current point to each neighbor
            Eigen::MatrixXd neighborMatrixS(nbNei, 3);
            Eigen::MatrixXd neighborMatrixT(nbNei, 3);
            for (vtkIdType j = 0; j < nbNei; j++)
            {
                vtkIdType neiID = oneRingPoints[i]->GetId(j);
                mesh->GetPoint(neiID, tempVector);
                vtkMath::Subtract(tempVector, sourceP, tempVector);
                neighborMatrixS.row(j) << tempVector[0], tempVector[1], tempVector[2];
                target->GetPoint(neiID, tempVector);
                vtkMath::Subtract(tempVector, targetP, tempVector);
                neighborMatrixT.row(j) << tempVector[0], tempVector[1], tempVector[2];
            }

            Eigen::MatrixXd neighborMatrixTtranspose = neighborMatrixT.transpose();
            Eigen::MatrixXd covLSQ = (neighborMatrixTtranspose * neighborMatrixT).inverse() * neighborMatrixTtranspose * neighborMatrixS;
            Eigen::JacobiSVD<Eigen::MatrixXd> svd = covLSQ.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
            Eigen::MatrixXd V = svd.matrixV();
            V.row(2) = V.row(2) * (svd.matrixU() * V).determinant();
            Eigen::MatrixXd M = svd.matrixU() * V.transpose();
            
            for (vtkIdType j = 0; j < nbNei; j++)
            {
                vtkIdType neiID = oneRingPoints[i]->GetId(j);
                displacement->GetTuple(neiID, targetP); // get it in targetP - we don't need it anymore at this point

                // do the LSQ alignement for j-th neighbor's displacement
                for (int k = 0; k < 3; k++)
                    tempVector[k] = M(0, k) * neighborMatrixT(j, 0) + M(1, k) * neighborMatrixT(j, 1) + M(2, k) * neighborMatrixT(j, 2);

                vtkMath::Add(targetP, tempVector, targetP);
                displacement->SetTuple(neiID, targetP);
            }
        }
        // average all the accumulated displacements
        for (vtkIdType i = 0; i < npts; i++)
        {
            displacement->GetTuple(i, tempVector);
            // one-ring neighborhood is transitive - for each neighbor, there must have been exactly one displacement added to this point
            vtkMath::MultiplyScalar(tempVector, 1.0 / oneRingPoints[i]->GetNumberOfIds());
            displacement->SetTuple(i, tempVector);
        }
    }
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::ApplyDisplacementDirect(vtkSmartPointer<vtkDoubleArray> displacement)
{
    vtkIdType npts = mesh->GetNumberOfPoints();
    double pointTemp[3];
    for (vtkIdType j = 0; j < npts; j++)
    {
        mesh->GetPoint(j, pointTemp);
        vtkMath::Add(pointTemp, displacement->GetTuple(j), pointTemp);
        mesh->GetPoints()->SetPoint(j, pointTemp);
    }
    mesh->GetPoints()->Modified();
}


void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::ApplyDisplacementARAP(vtkSmartPointer<vtkDoubleArray> displacement,
    vtkSmartPointer<vtkIdTypeArray> fixedPoints, double nugget)
{
    vtkIdType npts = mesh->GetNumberOfPoints();
    vtkSmartPointer<vtkLaplacianPolyData> lap = vtkSmartPointer<vtkLaplacianPolyData>::New();
    lap->SetMesh(mesh);

    Eigen::SparseMatrix<double> &laplacian = lap->GetLaplacian(true);
    // system == matrix with triple laplacian (top left corner, middle and bottom right corner),
    // normals as three diagonal matrices (one for X component, one for Y, one for Z) on the last N rows and last N columns
    // and nugget on the bottom right corner of the matrix as npts x npts diagonal matrix
    std::vector<Eigen::Triplet<double>> tripletList;
    tripletList.reserve(laplacian.nonZeros() * 3 + npts * 6); // npts * 6 = 3 non-zeros values for each normal, in last rows and last column (=> x2)

    // iterate over all non-zero values of the laplacian
    for (int k = 0; k < laplacian.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(laplacian, k); it; ++it)
        {
         //   std::cout << "(" << it.row() << ", " << it.col() << ") " << it.value() << std::endl;
            tripletList.push_back(Eigen::Triplet<double>(it.row(), it.col(), it.value())); // top left corner == just a copy of the original laplacian
            tripletList.push_back(Eigen::Triplet<double>(it.row() + npts, it.col() + npts, it.value())); // middle
            tripletList.push_back(Eigen::Triplet<double>(it.row() + 2 * npts, it.col() + 2 * npts, it.value())); // bottom left corner
        }
    }

    vtkSmartPointer<vtkFloatArray> normals = GetPointNormals();
    double temp[3];
    int normalOffset = 3 * npts; // normals are inserted at the end of the matrix, after 3x laplacian values
    for (vtkIdType i = 0; i < npts; i++)
    {
        normals->GetTuple(i, temp);
        // X - first N rows/N columns
        tripletList.push_back(Eigen::Triplet<double>(normalOffset + i, i, temp[0]));
        tripletList.push_back(Eigen::Triplet<double>(i, normalOffset + i, temp[0]));
        // Y
        tripletList.push_back(Eigen::Triplet<double>(normalOffset + i, i + npts, temp[1]));
        tripletList.push_back(Eigen::Triplet<double>(i + npts, normalOffset + i, temp[1]));
        // Z
        tripletList.push_back(Eigen::Triplet<double>(normalOffset + i, i + 2 * npts, temp[2]));
        tripletList.push_back(Eigen::Triplet<double>(i + 2 * npts, normalOffset + i, temp[2]));
    }

    if (nugget != 0.0)
    {
        for (int i = 0; i < npts; i++)
        {
            if (fixedPoints->GetValue(i) < 0) // only not fixed points have nugget, fixed points = nugget 0
                tripletList.push_back(Eigen::Triplet<double>(normalOffset + i, normalOffset + i, nugget));
        }
    }
    Eigen::SparseMatrix<double> system(npts * 3 + npts, npts * 3 + npts);
    system.setFromTriplets(tripletList.begin(), tripletList.end());

    std::cout << "Factorizing system for ARAP..." << std::endl;
    time_t start = clock();
    Eigen::SparseLU<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>> lapFactor;
   // Eigen::SimplicialCholesky<Eigen::SparseMatrix<double>> lapFactor;
    lapFactor.analyzePattern(system);
    std::cout << "Analyzed system's pattern, factorizing..." << std::endl;
    lapFactor.factorize(system);
    std::cout << "Factorized in " << ((double)(clock() - start) / CLOCKS_PER_SEC) << std::endl;

    Eigen::MatrixXd pointsCur(npts, 3); // the points in the current iteration of the interpolation
    Eigen::MatrixXd pointsOrig(npts, 3);
    // Right hand side of the equation has bottom N rows filled with dot product of normal and target position for each point
    Eigen::VectorXd RHS(npts * 3 + npts);
    std::vector<Eigen::MatrixXd> neighborsDirectionsInit; // for each point, contains matrix with direction vectors to each neighbor
    neighborsDirectionsInit.reserve(npts);
    double temp2[3];
    for (vtkIdType i = 0; i < npts; i++) // precompute anything we can
    {
        // store the point coordinates
        mesh->GetPoint(i, temp);
        pointsCur.row(i) << temp[0], temp[1], temp[2];
        pointsOrig.row(i) << temp[0], temp[1], temp[2];

        // generate the neighborsDirectionsInit
        vtkIdType numNei = oneRingPoints[i]->GetNumberOfIds();
        Eigen::MatrixXd mat(numNei, 3);
        for (vtkIdType nei = 0; nei < numNei; nei++)
        {
            vtkIdType neiIndex = oneRingPoints[i]->GetId(nei);
            mesh->GetPoint(neiIndex, temp2);
            mat.row(nei) = Eigen::Vector3d((temp2[0] - temp[0]), (temp2[1] - temp[1]), (temp2[2] - temp[2]));
        }
        neighborsDirectionsInit.push_back(mat);

        // create the bottom part of RHS
        displacement->GetTuple(i, temp2);
        vtkMath::Add(temp, temp2, temp); // temp now has target
        normals->GetTuple(i, temp2);
        RHS(npts * 3 + i) = vtkMath::Dot(temp, temp2);
    }
    std::vector<Eigen::Matrix3d> rotMats;
    rotMats.reserve(npts);
    Eigen::Matrix3d difference(npts, 3); // for comparing differences between subsequent iterations, when too small, quit
    double initNorm = 0;
    for (int iter = 0; iter < 30; iter++)
    {
        // rotation from current points to target for each point
        for (vtkIdType i = 0; i < npts; i++)
        {
            // matrices with difference vectors between the current point and all its neighbors
            vtkIdType numNei = oneRingPoints[i]->GetNumberOfIds();
            Eigen::MatrixXd neighborsDirectionsCur(numNei, 3);
            auto &rowI = pointsCur.row(i);
            for (vtkIdType nei = 0; nei < numNei; nei++)
            {
                neighborsDirectionsCur.row(nei) = pointsCur.row(oneRingPoints[i]->GetId(nei)) - rowI;
            }
            Eigen::JacobiSVD<Eigen::MatrixXd> svd = 
                (neighborsDirectionsCur.transpose() * neighborsDirectionsInit[i]).jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
            Eigen::MatrixXd V = svd.matrixV();
            Eigen::MatrixXd const& U = svd.matrixU();
            V.col(2) = V.col(2) * (U * V).determinant(); // if determinant is 1, nothing happens, if it is -1, we switch it
            rotMats[i] = Eigen::Matrix3d(V * U.transpose());
        }
        // now compute the top 3N rows of RHS for each point - they have X, Y and Z components of 
        // Sum { (wij/2)*(Ri+Rj)*(pi-pj) }, for each neighbour j of the current point i, wij being the laplacian value of pair (i,j)
        // => taking Ri out (and ignoring the /2): Sum{ wij*Ri*(pi-pj) + wij*Rj*(pi-pj) }
        // => Ri * Sum{ wij*(pi-pj) } + Sum { Rj*wij*(pi - pj) }
        for (vtkIdType i = 0; i < npts; i++)
        {
            Eigen::Vector3d sumRi(0, 0, 0); // sum for the first half which needs to be rotated by Ri in the end
            Eigen::Vector3d sumRj(0, 0, 0); // sum for the other half, which is rotated by Rj for each j
            Eigen::Vector3d curPoint = pointsOrig.row(i);
            for (Eigen::SparseMatrix<double>::InnerIterator it(laplacian, i); it; ++it) // laplacian is column-major -> we get iterator through one column
            {
                Eigen::Vector3d difWeighted = it.value() * (curPoint - (Eigen::Vector3d)pointsOrig.row(it.row())); // wij*(pi-pj)
                sumRi += difWeighted; // Sum{ wij*(pi-pj) }
                sumRj += rotMats[it.row()] * difWeighted; // Sum { Rj * wij*(pi - pj) }
            }
            sumRi = rotMats[i] * sumRi; // Ri * Sum{ wij*(pi-pj) }
            
            // finally sum the two terms and divide by two (wij / 2)
            sumRi = sumRi + sumRj;
            RHS(i) = sumRi(0) * -0.5;
            RHS(i + npts) = sumRi(1) * -0.5;
            RHS(i + 2 * npts) = sumRi(2) * -0.5;
        }
        auto result = lapFactor.solve(RHS);
        // result is X,Y,Z in single column -> separate it to three columns
        // but before rewriting the current point, check the difference from last iteration and later compare norms
        difference.col(0) = result.topRows(npts) - pointsCur.col(0);
        difference.col(1) = result.middleRows(npts, npts) - pointsCur.col(1);
        difference.col(2) = result.middleRows(2 * npts, npts) - pointsCur.col(2);
        pointsCur.col(0) = result.topRows(npts);
        pointsCur.col(1) = result.middleRows(npts, npts);
        pointsCur.col(2) = result.middleRows(2 * npts, npts);
        if (iter == 0)
            initNorm = difference.norm() / 100; // use the first difference as a measure, if we get to differences 100x times smaller, quit
        else if (difference.norm() < initNorm)
            break;
    }

 //   std::cout << pointsCur << std::endl;
    // write the last result
    for (vtkIdType i = 0; i < npts; i++)
    {
        temp[0] = pointsCur(i, 0);
        temp[1] = pointsCur(i, 1);
        temp[2] = pointsCur(i, 2);
        mesh->GetPoints()->SetPoint(i, temp);
    }
    mesh->GetPoints()->Modified();
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::getNeighborhoodPoints(vtkIdType pointId, 
    vtkSmartPointer<vtkIdList> &cellIds, vtkSmartPointer<vtkIdList> &ringPoints)
{
    // insert all points from these cells other than the current point into the list of unique point ids
    for (vtkIdType j = 0; j < cellIds->GetNumberOfIds(); j++)
    {
        vtkIdList *cellPoints = mesh->GetCell(cellIds->GetId(j))->GetPointIds();
        for (vtkIdType k = 0; k < cellPoints->GetNumberOfIds(); k++)
        {
            vtkIdType id = cellPoints->GetId(k);
            if (id != pointId)
                ringPoints->InsertUniqueId(id);
        }
    }
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::computeHistogram()
{
    computeNormals();
    // if normals changed, we have to recompute histogram
    if (normalComputeTime > histComputeTime)
    {        
        vtkIdType npts = mesh->GetNumberOfPoints();

        // initialize all values to 0 - we can do that per component -> do it in parallel here
        // (histogram arrays are allocated per component, so should be faster this way for large meshes)
        int histSize = nbBuckets * 2;
        #pragma omp parallel for num_threads(histSize)
        for (int i = 0; i < histSize; i++)
        {
            for (vtkIdType j = 0; j < npts; j++)
                curvatureHistogramTemp[i][j] = 0;
        }
        // this will access the same vtkPolyData, some of the methods might not be thread safe -> do not do this in parallel
        double currentNorm[3], tempVector[3], currentPoint[3];
        vtkSmartPointer<vtkFloatArray> normals = vtkFloatArray::SafeDownCast(mesh->GetPointData()->GetNormals());
        for (vtkIdType i = 0; i < npts; i++)  // for each point 
        {
            normals->GetTuple(i, currentNorm);
            mesh->GetPoint(i, currentPoint);
            vtkIdType nNeighbors = fourRingPoints[i]->GetNumberOfIds();
            for (vtkIdType j = 0; j < nNeighbors; j++) // for each neighboring point
            {
                vtkIdType neiID = fourRingPoints[i]->GetId(j);
                // first half of the histogram - based on scalar product of current point's normal and the normals of his two-ring neighbors
                normals->GetTuple(neiID, tempVector);
                double dot = vtkMath::Dot(currentNorm, tempVector);
                // increment count in the appropriate bucket of the histogram
                if (dot >= bucket_limit[nbBuckets - 1])
                    curvatureHistogramTemp[nbBuckets - 1][i]++;
                else
                {
                    for (int k = 1; k < nbBuckets; k++) // we know nbBuckets is at least 2 (if Initialize was called...), so this is OK
                    {
                        if (dot < bucket_limit[k])
                        {
                            curvatureHistogramTemp[k - 1][i]++;
                            break;
                        }
                    }
                }
                // second half of the histogram - scalar product of the normal and directional vector to the neighbor
                mesh->GetPoint(neiID, tempVector);
                vtkMath::Subtract(tempVector, currentPoint, tempVector);
                vtkMath::Normalize(tempVector);
                dot = vtkMath::Dot(currentNorm, tempVector);
                // increment count in the appropriate bucket of the histogram
                if (dot >= bucket_limit[nbBuckets - 1])
                    curvatureHistogramTemp[nbBuckets + nbBuckets - 1][i]++;
                else
                {
                    for (int k = 1; k < nbBuckets; k++) // we know nbBuckets is at least 2 (if Initialize was called...), so this is OK
                    {
                        if (dot < bucket_limit[k])
                        {
                            curvatureHistogramTemp[nbBuckets + k - 1][i]++; // nbBuckets + ... because this is the second part of the concatenated histogram
                            break;
                        }
                    }
                }
            }
            // normalize the values by the number of neighbours
            for (int k = 0; k < histSize; k++)
                curvatureHistogramTemp[k][i] /= nNeighbors;
        }
        // now smooth the histogram by averaging values in the entire two-ring of each point and each bucket
        #pragma omp parallel for 
        for (vtkIdType i = 0; i < npts; i++)
        {
            for (int bucketID = 0; bucketID < histSize; bucketID++)
            {
                curvatureHistogram[bucketID][i] = curvatureHistogramTemp[bucketID][i];
                vtkIdType nNeighbors = oneRingPoints[i]->GetNumberOfIds();
                for (vtkIdType j = 0; j < nNeighbors; j++)
                {
                    vtkIdType neiID = oneRingPoints[i]->GetId(j);
                    curvatureHistogram[bucketID][i] += curvatureHistogramTemp[bucketID][neiID];
                }
                curvatureHistogram[bucketID][i] /= (nNeighbors + 1); // +1 because it also includes the value in the point itself
            }
        }
        // compute histogram per-bucket bounds
        for (int i = 0; i < histSize; i++) // initialize bounds
        {
            curvatureHistogramBounds[i * 2] = std::numeric_limits<double>::max();
            curvatureHistogramBounds[i * 2 + 1] = std::numeric_limits<double>::min();
        }
        #pragma omp parallel for num_threads(histSize)
        for (int i = 0; i < histSize; i++)
        {
            for (vtkIdType j = 0; j < npts; j++)
                setMinMax(&(curvatureHistogramBounds[i * 2]), curvatureHistogram[i][j]);
        }
        /*
        std::cout << "histogram bounds: ";
        for (int i = 0; i < histSize; i++)
        {
            std::cout << curvatureHistogramBounds[i * 2] << " " << curvatureHistogramBounds[i * 2 + 1] << " ";
        }
        std::cout << std::endl;
        */
        histComputeTime = normalComputeTime;
    }
}


void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::computeNormals()
{
    // recompute normals - the vtk piperline will check whether it actually needs to be recomputed or not
    if (normals->GetMTime() < mesh->GetPoints()->GetMTime())
        normals->Modified();
    normals->Update();
    if (normals->GetMTime() > normalComputeTime)
    {
   //     std::cout << "computing normals" << std::endl;
        // move the newly computed normal array from the output of the filter to the mesh polydata
        vtkSmartPointer<vtkFloatArray> normal_array = vtkFloatArray::SafeDownCast(normals->GetOutput()->GetPointData()->GetNormals());
        mesh->GetPointData()->SetNormals(normal_array);
        mesh->GetCellData()->SetNormals(normals->GetOutput()->GetCellData()->GetNormals());
        // recompute normal bounds
        normalBounds[0] = normalBounds[2] = normalBounds[4] = std::numeric_limits<double>::max();
        normalBounds[1] = normalBounds[3] = normalBounds[5] = std::numeric_limits<double>::min();
        vtkIdType npts = normal_array->GetNumberOfTuples();       
        float *raw = normal_array->GetPointer(0);
        for (vtkIdType i = 0; i < npts; i += 3)
        {
            setMinMax(normalBounds, raw[i]); // X
            setMinMax(&(normalBounds[2]), raw[i + 1]); // Y
            setMinMax(&(normalBounds[4]), raw[i + 2]); // Z
        }
     //   std::cout << "normal bounds: " << normalBounds[0] << " " << normalBounds[1]
       //     << normalBounds[2] << " " << normalBounds[3]
         //   << normalBounds[4] << " " << normalBounds[5] << std::endl;
        normalComputeTime = normals->GetMTime();
    }
}

void vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics::computeDihedralAngles()
{
    vtkSmartPointer<vtkFloatArray> cellNormals = GetCellNormals();
    vtkIdType npts = mesh->GetNumberOfPoints();
    vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
    double normal1[3], normal2[3];
    for (vtkIdType i = 0; i < npts; i++)
    {
        vtkIdType nNei = oneRingPoints[i]->GetNumberOfIds();
        for (vtkIdType j = 0; j < nNei; j++)
        {
            vtkIdType neiID = oneRingPoints[i]->GetId(j);
            mesh->GetCellEdgeNeighbors(-1, i, neiID, cellIds);
            // we already assume mesh is manifold so it can't be more than 2, but make sure there is not only 1 (edge of mesh)
            if (cellIds->GetNumberOfIds() == 2) 
            {
                cellNormals->GetTuple(cellIds->GetId(0), normal1);
                cellNormals->GetTuple(cellIds->GetId(1), normal2);
                dihAngles[i]->SetValue(j, vtkMath::Dot(normal1, normal2)); // set angle between cells shared by the i-th point and it's j-th neighbor
            }
            else
                dihAngles[i]->SetValue(j, std::numeric_limits<double>::max()); // dot is <-1,1> -> let's put high value to mark that there aren't two cells
        }
    }
}


#pragma endregion


vtkSurfaceRegistrationPIPER::vtkSurfaceRegistrationPIPER()
{
    SetNumberOfInputPorts(2);
    ControlPoints = vtkSmartPointer<vtkIdTypeArray>::New();
    allPointsMatrix = nullptr;
}

vtkSurfaceRegistrationPIPER::~vtkSurfaceRegistrationPIPER()
{
    // normally will be deleted, only for cases when execution of the filter terminates prematurely with an error
    if (allPointsMatrix != nullptr)
        delete[] allPointsMatrix;
}

bool vtkSurfaceRegistrationPIPER::CheckInputValidity()
{
    auto source = vtkPolyData::SafeDownCast(GetInputDataObject(0, 0));
    auto target = vtkPolyData::SafeDownCast(GetInputDataObject(1, 0));
    // check that input data are correct
    if (source == nullptr)
    {
        vtkErrorMacro(<< "Source mesh not specified.");
        return false;
    }
    if (target == nullptr)
    {
        vtkErrorMacro(<< "Target mesh not specified.");
        return false;
    }
    if (source->GetNumberOfCells() < 1 || source->GetNumberOfPoints() < 3)
    {
        vtkErrorMacro(<< "Source mesh does not contain any faces, registration cannot be performed.");
        return false;
    }
    if (target->GetNumberOfCells() < 1 || target->GetNumberOfPoints() < 3)
    {
        vtkErrorMacro(<< "Target mesh does not contain any faces, registration cannot be performed.");
        return false;
    }
    vtkSmartPointer<vtkPolyDataConnectivityFilter> conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
    conn->SetInputData(source);
    conn->SetExtractionModeToLargestRegion();
    conn->Update();
    if (source->GetNumberOfCells() != conn->GetOutput()->GetNumberOfCells())
    {
        vtkErrorMacro(<< "Source mesh contains multiple disconnected components, provide mesh with exactly one connected component.");
        return false;
    }

    conn->SetInputData(target);
    conn->Update();
    if (target->GetNumberOfCells() != conn->GetOutput()->GetNumberOfCells())
    {
        vtkErrorMacro(<< "Target mesh contains multiple disconnected components, provide mesh with exactly one connected component.");
        return false;
    }

    vtkIdType nptsSource = source->GetNumberOfPoints();
    vtkIdType nptsTarget = target->GetNumberOfPoints();
    if (abs(nptsSource - nptsTarget) > nptsSource * 0.05)
    {
        vtkWarningMacro(<< "Large difference between number of source and target points ("
            << nptsSource << " and " << nptsTarget << "), likely to produce low quality output. Consider decimating one of the meshes.");
    }
    return true;
}


int vtkSurfaceRegistrationPIPER::RequestData(vtkInformation *vtkNotUsed(request),
    vtkInformationVector **inputVector,
    vtkInformationVector *outputVector)
{
    // get the input and output
    vtkPolyData* source = vtkPolyData::GetData(inputVector[0], 0);
    vtkPolyData* target = vtkPolyData::GetData(inputVector[1], 0);
    vtkPolyData* output = vtkPolyData::GetData(outputVector, 0);

    if (!CheckInputValidity())
        return 0;

    vtkIdType nptsSource = source->GetNumberOfPoints();
    vtkIdType nptsTarget = target->GetNumberOfPoints();

    output->DeepCopy(source); // output will copy source in everyway except for vertex coordinates
    int NumberOfIterations_max = NumberOfIterations + 3;
    int nb_smooth_min = 24; // ?? why 24

    // if control points are not set, set all points to have no prescribed points
    vtkSmartPointer<vtkIdTypeArray> targetControlPoints = vtkSmartPointer<vtkIdTypeArray>::New();
    targetControlPoints->SetNumberOfValues(nptsTarget);
    targetControlPoints->FillComponent(0, -1);
    if (ControlPoints->GetNumberOfTuples() != nptsSource)
    {
        ControlPoints->SetNumberOfValues(nptsSource);
        ControlPoints->FillComponent(0, -1);
    }
    else // check that values are valid and fill int the targetControlPoints array
    {
        for (vtkIdType i = 0; i < nptsSource; i++)
        {
            vtkIdType targetId = ControlPoints->GetValue(i);
            if (targetId > nptsTarget)
            {
                vtkWarningMacro(<< "Control point specified for source point #" << i
                    << " is not valid (it is higher than number of target points). Will be ignored");
                ControlPoints->SetValue(i, -1);
            }
            else if (targetId >= 0)
                targetControlPoints->SetValue(targetId, i);
        }
    }

    // ------------------------------------------------------------------------
    // initialize memory for pointToPointRegistration (depends on number of buckets for the histograms)
    if (!metricsOutput.Initialize(output, HistogramBuckets))
    {
        vtkErrorMacro(<< "Source mesh contains unused vertices, remove them in order to perform registration.");
        return 0;
    }
    if (!metricsTarget.Initialize(target, HistogramBuckets))
    {
        vtkErrorMacro(<< "Target mesh contains unused vertices, remove them in order to perform registration.");
        return 0;
    }
    int histSize = HistogramBuckets * 2;
    minMaxDifHist = new double[histSize]; // the histogram actually concatenates two histograms
    histBound = new double[HistogramBuckets * 4]; // min max for each bucket of the histogram for each of the two histograms
    // which dimensions of the metrics should be ignored - actually used only for the histogram dimensions,
    // but for faster processing contains "true" also for the first six dimensions (position and normal)
    // it will now be set such that only those dimensions of the target's histogram that contains more than 95% of values will be used
    // allPointsMatrix holds source + target points in each row and a column for position, normals and histogram
    int totalDimensions = 6 + histSize; // 6 = 3 for position, 3 for normal, HistogramBuckets * 2 = combined histogram
    maskDimension = new bool[totalDimensions];
    for (int i = 0; i < 6; i++)
        maskDimension[i] = true;
    for (int i = 6; i < totalDimensions; i++)
        maskDimension[i] = false;
    nbUnmaskedDim = 6;
    double **targetHist = metricsTarget.GetCurvatureHistograms();
    double *histSums = new double[histSize];

    #pragma omp parallel for
    for (int i = 0; i < histSize; i++)
    {
        histSums[i] = 0;
        double *hist = targetHist[i]; // the component of the histogram the current thread is to process
        for (int j = 0; j < nptsTarget; j++)
            histSums[i] += hist[j];
    }
    // for each of the two histograms compute the 95% separately
    for (int histOffset = 0; histOffset <= HistogramBuckets; histOffset += HistogramBuckets)
    {
        double totalHistSum = 0;
        for (int i = 0; i < HistogramBuckets; i++)
            totalHistSum += histSums[histOffset + i];
        double cumulSum = 0;
        totalHistSum *= 0.95; // this is the target value we want to achieve - 95% of total
        while (cumulSum < totalHistSum)
        {
            int maxID = 6;
            double max = std::numeric_limits<double>::min();
            // we don't expect many buckets -> this inefficient sort is OK and probably faster than initializing something more sophisticated
            for (int i = 0; i < HistogramBuckets; i++) // start from the second entry
            {
                int dimID = 6 + i + histOffset;
                if (!maskDimension[dimID] && // it has not been yet accounted for - mask is still false
                    histSums[dimID - 6] > max)
                {
                    maxID = dimID;
                    max = histSums[maxID - 6];
                }
            }
            maskDimension[maxID] = true;
            cumulSum += histSums[maxID - 6];
            nbUnmaskedDim++;
        }
    }
    delete[] histSums;
    if (allPointsMatrix != nullptr)
        delete[] allPointsMatrix;
    allPointsMatrix = new double[(nptsSource + nptsTarget) * nbUnmaskedDim];
    
    // ------------------------------------------------------------------------
    // initialize weights
    double weights[3]{2, 1, 1}; // 0th - position; 1th normal; 2nd angle  
    vtkSmartPointer<vtkKdTreePointLocator> targetLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
    targetLocator->SetDataSet(target);
    targetLocator->BuildLocator();
    // use bounding box diagonal as an initial estimate to the size of weight for the positional metric
    source->ComputeBounds();
    target->ComputeBounds();
    double bounds[6];
    source->GetBounds(bounds);
    for (int i = 0; i < 6; i += 2)
    {
        bounds[i] = vtkMath::Min(bounds[i], target->GetBounds()[i]);
        bounds[i + 1] = vtkMath::Max(bounds[i + 1], target->GetBounds()[i + 1]);
    }
    // compute length of the diagonal of the bounding box
    double pointTemp[3], pointTemp2[3];
    pointTemp[0] = bounds[0]; pointTemp[1] = bounds[2]; pointTemp[2] = bounds[4]; // minX, minY, minZ
    pointTemp2[0] = bounds[1]; pointTemp2[1] = bounds[3]; pointTemp2[2] = bounds[5]; // maxX, maxY, maxZ
    double diagLength = std::sqrt(vtkMath::Distance2BetweenPoints(pointTemp, pointTemp2));
    // find the highest distance to target among the source points
    double maxDist = std::numeric_limits<double>::min();
    for (vtkIdType i = 0; i < nptsSource; i++)
    {
        source->GetPoint(i, pointTemp);
        maxDist = std::max(maxDist, vtkMath::Distance2BetweenPoints(pointTemp, target->GetPoint(targetLocator->FindClosestPoint(pointTemp))));
    }
    weights[0] = weights[0] * (diagLength / (10 * std::sqrt(maxDist)));
    // for very similar meshes, maxDist will be close to 0 which will make weights[0] close to infinity and cause numrical problems
    // in those cases let's just use only the positional descriptor, the registration is likely to end after the first iteration anyway
    if (weights[0] > 1000)
    {
        weights[0] = 1;
        weights[1] = weights[2] = 0;
    }
    
    // ------------------------------------------------------------------------
    // allocate objects that are used reccurently during the iterative registration
    // keep one array for the temporary displacement so that we don't have to reallocate it all the time
    vtkSmartPointer<vtkDoubleArray> displacementSource = vtkSmartPointer<vtkDoubleArray>::New();
    displacementSource->SetNumberOfComponents(3);
    displacementSource->SetNumberOfTuples(nptsSource);
    vtkSmartPointer<vtkDoubleArray> displacementTarget = vtkSmartPointer<vtkDoubleArray>::New();
    displacementTarget->SetNumberOfComponents(3);
    displacementTarget->SetNumberOfTuples(nptsTarget);
    // the "displaced target" is used in the second part of the registration process to register the target ONTO the source
    // as a mechanism to ensure that some parts of the target won't be "left out" by the registration
    vtkSmartPointer<vtkPolyData> displacedTarget = vtkSmartPointer<vtkPolyData>::New();
    displacedTarget->ShallowCopy(target); // the connectivity is the same as Target, we will just change positions of vertices
    vtkSmartPointer<vtkPoints> dispTargetPoints = vtkSmartPointer<vtkPoints>::New();
    dispTargetPoints->SetNumberOfPoints(nptsTarget);
    displacedTarget->SetPoints(dispTargetPoints);
    // only positional distance is used for this corrective part of the registration -> prepare a locator for it
    vtkSmartPointer<vtkKdTreePointLocator> dispTargetLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
    dispTargetLocator->SetDataSet(displacedTarget); // do not build it yet, it needs to be rebuilt in each iteration
    // keep info about previous iteration results to end if it is getting progressively worse
    double minMean = std::numeric_limits<double>::max();
    double minRMS = std::numeric_limits<double>::max();
    vtkSmartPointer<vtkPoints> prevOutputPoints = vtkSmartPointer<vtkPoints>::New();// backup points array to keep results from previous iteration
    prevOutputPoints->DeepCopy(output->GetPoints());

    // ------------------------------------------------------------------------
    // start the iterative registration process
    for (int i = 0; i < NumberOfIterations_max; i++)
    {
        int nb_smooth = nb_smooth_min * (NumberOfIterations - i);
        if (nb_smooth < nb_smooth_min)
            nb_smooth = nb_smooth_min / 2;
        vtkSmartPointer<vtkIdList> closestPoints = pointToPointRegistration(metricsOutput, metricsTarget, weights, ControlPoints);
        
        // for each point in source, compute displacement towards the closest point in target
        double a = vtkMath::Min(1.0, i / (double)NumberOfIterations); // some kind of interpolation factors?
        double b = 1 - a; // some kind of interpolation factors?
        vtkSmartPointer<vtkFloatArray> normals = metricsOutput.GetPointNormals();
        for (vtkIdType j = 0; j < nptsSource; j++)
        {
            // displacement from source to the closest target
            output->GetPoint(j, pointTemp);

           // This should never happen...but most numerical problems usually result in this, so it's a good debugging print in case you are modifying the method 
           /* if (closestPoints->GetId(j) > nptsTarget || closestPoints->GetId(j) < 0)
            {
                std::cout << "error ID " << closestPoints->GetId(j) << std::endl;
                continue;
            }*/
            
            vtkMath::Subtract(target->GetPoint(closestPoints->GetId(j)), pointTemp, pointTemp);
            // weigh the displacement by the following formula
            // d = d * b + a * n.*(sum(n.*d,"c")*[1 1 1]); 
            normals->GetTuple(j, pointTemp2);
            // at this point, pointTemp = displacement, pointTemp2 = normal
            vtkMath::MultiplyScalar(pointTemp2, vtkMath::Dot(pointTemp, pointTemp2) * a); // == a * n.*(sum(n.*d,"c")*[1 1 1])?
            vtkMath::MultiplyScalar(pointTemp, b); // == d * b
            vtkMath::Add(pointTemp, pointTemp2, pointTemp);
            displacementSource->SetTuple(j, pointTemp);
        }
        vtkSmartPointer<vtkDoubleArray> smoothedDisp = metricsOutput.SmoothFieldOneRing(displacementSource, nb_smooth, ControlPoints);
        
        if (RigidOnly)
        {
            auto pts = output->GetPoints();
            rigidRegistration(pts, smoothedDisp);
            output->SetPoints(pts);
        }
        else // non-linear transformation
        {
            // each point will be translated by the smoothed displacement
            if (i < 10000) // TODO: the ARAP registration is buggy, for now let's turn it off completely, needs more testing, perhaps there is some bug?
                metricsOutput.ApplyDisplacementDirect(smoothedDisp);
            else
                metricsOutput.ApplyDisplacementARAP(smoothedDisp, ControlPoints);
        }

        // now register the target onto the source and create displacement field for the target, but don't apply it
        closestPoints = pointToPointRegistration(metricsTarget, metricsOutput, weights, targetControlPoints);

        for (vtkIdType j = 0; j < nptsTarget; j++)
        {
          /*  if (closestPoints->GetId(j) > nptsSource || closestPoints->GetId(j) < 0)
            {
                std::cout << "error ID " << closestPoints->GetId(j) << std::endl;
                continue;
            }*/
            target->GetPoint(j, pointTemp);
            vtkMath::Subtract(output->GetPoint(closestPoints->GetId(j)), pointTemp, pointTemp);
            displacementTarget->SetTuple(j, pointTemp);
        }
        smoothedDisp = metricsTarget.SmoothFieldOneRing(displacementTarget, NumberOfIterations - i);
        for (vtkIdType j = 0; j < nptsTarget; j++)
        {
            target->GetPoint(j, pointTemp);
            vtkMath::Add(pointTemp, smoothedDisp->GetTuple(j), pointTemp);
            dispTargetPoints->SetPoint(j, pointTemp);
        }
        dispTargetLocator->FreeSearchStructure();
        dispTargetLocator->BuildLocator();

        // create displacement field from the source toward the target based on which point would be closest if the target was displaced
        for (vtkIdType j = 0; j < nptsSource; j++)
        {
            output->GetPoint(j, pointTemp);
            // we are looking for closest point in the displaced target, but for the displacement, we take the position from the undisplaced one
            vtkMath::Subtract(target->GetPoint(dispTargetLocator->FindClosestPoint(pointTemp)), pointTemp, pointTemp);
            displacementSource->SetTuple(j, pointTemp);
        }
        smoothedDisp = metricsOutput.SmoothFieldOneRing(displacementSource, nb_smooth, ControlPoints);
        
        if (RigidOnly)
        {
            auto pts = output->GetPoints();
            rigidRegistration(pts, smoothedDisp);
            output->SetPoints(pts);
        }            
        else // non-linear transformation
        {
            // each point will be translated by the smoothed displacement
            if (i < 10000)// TODO: the ARAP registration is behaving strangely, for now let's turn it off completely, needs more testing, perhaps there is some bug?
                metricsOutput.ApplyDisplacementDirect(smoothedDisp);
            else
                metricsOutput.ApplyDisplacementARAP(smoothedDisp, ControlPoints);
        }

        if (!RigidOnly)
        {
       /*   Does not have significant impact -> not used 
            metricsOutput.AlignRigid(source->GetPoints(), displacementSource);
            smoothedDisp = metricsOutput.SmoothFieldOneRing(displacementSource, nb_smooth / 2);

            for (vtkIdType j = 0; j < nptsSource; j++)
            {
                output->GetPoint(j, pointTemp);
                vtkMath::Add(pointTemp, smoothedDisp->GetTuple(j), pointTemp);
                output->GetPoints()->SetPoint(j, pointTemp);
            }*/

            // do small smoothing
            metricsOutput.AvoidSelfPenetration();
            // ...and general surface smoothing
            if (PostProcessSmooth > 0) // vtkWindowedSincPolyDataFilter throws a warning otherwise, this is the easiest way to catch it
            {
                vtkSmartPointer<vtkWindowedSincPolyDataFilter> outputSmoother = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
                outputSmoother->SetInputData(output);
                outputSmoother->SetNumberOfIterations(PostProcessSmooth);
                outputSmoother->Update();

                output->SetPoints(outputSmoother->GetOutput()->GetPoints());
                output->Modified();
                output->GetPoints()->Modified();
            }
        }

        // compare current state with state from last run, if it got worse, end
        normals = metricsOutput.GetPointNormals();
        double dispToNormalDotsMean = 0;
        for (vtkIdType j = 0; j < nptsSource; j++)
        {
            output->GetPoint(j, pointTemp);
            target->GetPoint(targetLocator->FindClosestPoint(pointTemp), pointTemp2);
            vtkMath::Subtract(pointTemp2, pointTemp, pointTemp); // pointTemp now has displacement to closest target point
            normals->GetTuple(j, pointTemp2);
            dispToNormalDotsMean += fabs(vtkMath::Dot(pointTemp, pointTemp2));
        }
        dispToNormalDotsMean /= nptsSource;
        if (dispToNormalDotsMean < minMean)
        {
            minMean = dispToNormalDotsMean;
            prevOutputPoints->DeepCopy(output->GetPoints());
        }
        else
        {
            output->SetPoints(prevOutputPoints);
            break;
        }
        
        weights[0] *= 2;
    }
    delete[] histBound;
    delete[] minMaxDifHist;
    delete[] allPointsMatrix;
    allPointsMatrix = nullptr;
    delete[] maskDimension;
    return 1;
}

vtkSmartPointer<vtkIdList> vtkSurfaceRegistrationPIPER::pointToPointRegistration(vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics &source,
    vtkSurfaceRegistrationPIPER::VtkPolyDataMetrics &target, double weights[3], vtkSmartPointer<vtkIdTypeArray> cps)
{
    // we will normalize all metrics to <-1,1> PER DIMENSION, i.e. for each dimension of a point (/normal/histogram):
    // 1. transfer to 0 (subtracting min of that dimension), 2. scale to <0,1> (divide by max - min) , 3. stretch to <-1,1> by doing (2*X - 1)

    vtkSmartPointer<vtkPoints> sPoints = source.GetPolyData()->GetPoints();
    vtkSmartPointer<vtkPoints> tPoints = target.GetPolyData()->GetPoints();
    vtkSmartPointer<vtkFloatArray> sNormals = source.GetPointNormals();
    vtkSmartPointer<vtkFloatArray> tNormals = target.GetPointNormals();
    double **sHistogram = source.GetCurvatureHistograms();
    double **tHistogram = target.GetCurvatureHistograms();
    vtkIdType nptsSource = sPoints->GetNumberOfPoints();
    vtkIdType nptsTarget = tPoints->GetNumberOfPoints();
    // generate the global (source + target) bounds 
    const double *spBounds = source.GetPointBounds();
    const double *tpBounds = target.GetPointBounds();
    const double *snBounds = source.GetPointNormalBounds();
    const double *tnBounds = target.GetPointNormalBounds();
    double posiBounds[6], normalBounds[6];
    posiBounds[0] = vtkMath::Min(spBounds[0], tpBounds[0]);
    posiBounds[1] = vtkMath::Max(spBounds[1], tpBounds[1]);
    posiBounds[2] = vtkMath::Min(spBounds[2], tpBounds[2]);
    posiBounds[3] = vtkMath::Max(spBounds[3], tpBounds[3]);
    posiBounds[4] = vtkMath::Min(spBounds[4], tpBounds[4]);
    posiBounds[5] = vtkMath::Max(spBounds[5], tpBounds[5]);

    normalBounds[0] = vtkMath::Min(snBounds[0], tnBounds[0]);
    normalBounds[1] = vtkMath::Max(snBounds[1], tnBounds[1]);
    normalBounds[2] = vtkMath::Min(snBounds[2], tnBounds[2]);
    normalBounds[3] = vtkMath::Max(snBounds[3], tnBounds[3]);
    normalBounds[4] = vtkMath::Min(snBounds[4], tnBounds[4]);
    normalBounds[5] = vtkMath::Max(snBounds[5], tnBounds[5]);

    double minMaxDifPosi[3] {posiBounds[1] - posiBounds[0], posiBounds[3] - posiBounds[2], posiBounds[5] - posiBounds[4]};
    double minMaxDifNormals[3] {normalBounds[1] - normalBounds[0], normalBounds[3] - normalBounds[2], normalBounds[5] - normalBounds[4]};
    /*
    std::cout << "normal bounds global: " << normalBounds[0] << " " << normalBounds[1]
        << normalBounds[2] << " " << normalBounds[3]
        << normalBounds[4] << " " << normalBounds[5] << std::endl << std::endl;

    std::cout << "point bounds global: " << posiBounds[0] << " " << posiBounds[1]
        << posiBounds[2] << " " << posiBounds[3]
        << posiBounds[4] << " " << posiBounds[5] << std::endl << std::endl;    

    std::cout << "minMaxDifPosi: " << minMaxDifPosi[0] << " " << minMaxDifPosi[1] << " " << minMaxDifPosi[2] << std::endl;
    std::cout << "minMaxDifNormals: " << minMaxDifNormals[0] << " " << minMaxDifNormals[1] << " " << minMaxDifNormals[2] << std::endl;
*/
    const double *shBounds = source.GetCurvatureHistogramBounds();
    const double *thBounds = target.GetCurvatureHistogramBounds();
    int histSize = source.GetHistogramSize();
//    std::cout << "hist bounds global: ";
    for (int i = 0; i < histSize; i++)
    {
        histBound[i * 2] = vtkMath::Min(shBounds[i * 2], thBounds[i * 2]);
        histBound[i * 2 + 1] = vtkMath::Max(shBounds[i * 2 + 1], thBounds[i * 2 + 1]);
        minMaxDifHist[i] = histBound[i * 2 + 1] - histBound[i * 2];
//        std::cout << histBound[i * 2] << " " << histBound[i * 2 + 1] << " ";
    }
//    std::cout << std::endl;
    double tempPoint[3], tempPoint2[3];
    double *allTargetPointsMatrix = &(allPointsMatrix[nptsSource * nbUnmaskedDim]); // allPoints has first all source points, then all target points -> this points at the first target
    #pragma omp parallel sections
    {
        // normalize and weight the values of the individual metrics and write them into the allPointsMatrix
        #pragma omp section
        {
            for (vtkIdType i = 0; i < nptsSource; i++)
            {
                double *curPoint = &(allPointsMatrix[i * nbUnmaskedDim]);
                // first 3 dimensions = position
                sPoints->GetPoint(i, tempPoint);
                curPoint[0] = normalizeValue(tempPoint[0], minMaxDifPosi[0], posiBounds[0]) * weights[0];
                curPoint[1] = normalizeValue(tempPoint[1], minMaxDifPosi[1], posiBounds[2]) * weights[0];
                curPoint[2] = normalizeValue(tempPoint[2], minMaxDifPosi[2], posiBounds[4]) * weights[0];

                // next 3 dimensions = normals
                sNormals->GetTuple(i, tempPoint);
                curPoint[3] = normalizeValue(tempPoint[0], minMaxDifNormals[0], normalBounds[0]) * weights[1];
                curPoint[4] = normalizeValue(tempPoint[1], minMaxDifNormals[1], normalBounds[2]) * weights[1];
                curPoint[5] = normalizeValue(tempPoint[2], minMaxDifNormals[2], normalBounds[4]) * weights[1];

                // normalize and insert histogram values - they are the remaining dimensions
                int skipped = 0; // we have to skip dimensions that were masked earlier
                for (int k = 0; k < histSize; k++)
                {
                    // account for the skipped dimensions here to make the matrix more compact and avoid branching in the more computationally demanding part below
                    if (maskDimension[6 + k])
                        curPoint[6 + k - skipped] =
                            normalizeValue(sHistogram[k][i], minMaxDifHist[k], histBound[k * 2]) * weights[2];
                    else
                        skipped++;
                }
            }
        }
        #pragma omp section
        {
            for (vtkIdType i = 0; i < nptsTarget; i++)
            {
                double *curPoint = &(allTargetPointsMatrix[i * nbUnmaskedDim]);
                // first 3 dimensions = position
                tPoints->GetPoint(i, tempPoint2);
                curPoint[0] = normalizeValue(tempPoint2[0], minMaxDifPosi[0], posiBounds[0]) * weights[0];
                curPoint[1] = normalizeValue(tempPoint2[1], minMaxDifPosi[1], posiBounds[2]) * weights[0];
                curPoint[2] = normalizeValue(tempPoint2[2], minMaxDifPosi[2], posiBounds[4]) * weights[0];

                // next 3 dimensions = normals
                tNormals->GetTuple(i, tempPoint2);
                curPoint[3] = normalizeValue(tempPoint2[0], minMaxDifNormals[0], normalBounds[0]) * weights[1];
                curPoint[4] = normalizeValue(tempPoint2[1], minMaxDifNormals[1], normalBounds[2]) * weights[1];
                curPoint[5] = normalizeValue(tempPoint2[2], minMaxDifNormals[2], normalBounds[4]) * weights[1];

                // normalize and insert histogram values - they are the remaining dimensions
                int skipped = 0; // we have to skip dimensions that were masked earlier
                for (int k = 0; k < histSize; k++)
                {
                    // account for the skipped dimensions here to make the matrix more compact and avoid branching in the more computationally demanding part below
                    if (maskDimension[6 + k])
                        curPoint[6 + k - skipped] =
                            normalizeValue(tHistogram[k][i], minMaxDifHist[k], histBound[k * 2]) * weights[2];
                    else
                        skipped++;
                }
            }
        }
    }

    // now find closest point in target for each point in source, in terms of euclidean distance using all the metrics as dimensions
    // we are using brute force since it is probably not worth building a search structure for problem with so many dimensions
    vtkSmartPointer<vtkIdList> closestPoints = vtkSmartPointer<vtkIdList>::New();
    closestPoints->SetNumberOfIds(nptsSource);
//    std::cout << "starting point to point distance computation" << std::endl;
  //  time_t start = clock();

    #pragma omp parallel for num_threads(nptsSource)
    for (vtkIdType i = 0; i < nptsSource; i++)
    {
        vtkIdType targetID = cps->GetValue(i);
        if (targetID >= 0)
            closestPoints->SetId(i, targetID);
        else // if there is no control point prescribed for this point, we have to compute the closest point
        {
            double minDist = std::numeric_limits<double>::max();
            double *sourceP = &(allPointsMatrix[i * nbUnmaskedDim]);
            for (vtkIdType j = 0; j < nptsTarget; j++)
            {
                double *targetP = &(allTargetPointsMatrix[j * nbUnmaskedDim]);
                // compute squared euclidean distance
                double dist = 0;
                for (int dim = 0; dim < nbUnmaskedDim; dim++) // for each unmasked dimension (the masked dimensions are not even in the matrix)
                {
                    double val = targetP[dim] - sourceP[dim]; // dimension = individual component of a directional vector from source to target
                    dist += val * val;
                }
                if (dist < minDist)
                {
                    minDist = dist;
                    closestPoints->SetId(i, j);
                }
            }
        }
    }
//    time_t end = clock();
  //  std::cout << "brute force search time: " << ((float)(end - start) / CLOCKS_PER_SEC) << std::endl;
    return closestPoints;
}


void vtkSurfaceRegistrationPIPER::rigidRegistration(vtkSmartPointer<vtkPoints> pointsToTransform, vtkSmartPointer<vtkDoubleArray> displacement)
{
    vtkIdType nbpts = pointsToTransform->GetNumberOfPoints();
    //https://igl.ethz.ch/projects/ARAP/svd_rot.pdf

    double centroidPTT[3]{0, 0, 0}; // centroid for pointsToTransform
    double centroidPTTD[3]{0, 0, 0}; // centroid for pointsToTransform + displacement
    double pointTemp[3];

    Eigen::MatrixXd pointsTTD, pointsTT;
    pointsTTD.resize(nbpts, 3);
    pointsTT.resize(nbpts, 3);

    for (vtkIdType i = 0; i < nbpts; i++)
    {
        pointsToTransform->GetPoint(i, pointTemp);
        pointsTT(i, 0) = pointTemp[0];
        pointsTT(i, 1) = pointTemp[1];
        pointsTT(i, 2) = pointTemp[2];
        vtkMath::Add(pointTemp, centroidPTT, centroidPTT);
        vtkMath::Add(pointTemp, displacement->GetTuple(i), pointTemp);// pointTemp is now p0[i] + displacement[i] == p[i]
        vtkMath::Add(pointTemp, centroidPTTD, centroidPTTD);
        // store the p0 + d in pointsTranslated -> pointsTranslated == p
        pointsTTD(i, 0) = pointTemp[0];
        pointsTTD(i, 1) = pointTemp[1];
        pointsTTD(i, 2) = pointTemp[2];
    }
    vtkMath::MultiplyScalar(centroidPTT, 1.0 / nbpts);
    vtkMath::MultiplyScalar(centroidPTTD, 1.0 / nbpts);

    for (vtkIdType i = 0; i < nbpts; i++)
    {
        pointsTT(i, 0) -= centroidPTT[0];
        pointsTT(i, 1) -= centroidPTT[1];
        pointsTT(i, 2) -= centroidPTT[2];

        pointsTTD(i, 0) -= centroidPTTD[0];
        pointsTTD(i, 1) -= centroidPTTD[1];
        pointsTTD(i, 2) -= centroidPTTD[2];
    }

    Eigen::MatrixXd pointsTTtranspose = pointsTT.transpose();
    Eigen::MatrixXd covLSQ = (pointsTTtranspose * pointsTT).inverse() * pointsTTtranspose * pointsTTD; // is it never singular?

    Eigen::JacobiSVD<Eigen::MatrixXd> svd = covLSQ.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    Eigen::MatrixXd V = svd.matrixV();
    V.row(2) = V.row(2) * (svd.matrixU() * svd.matrixV()).determinant();
    Eigen::MatrixXd M = (svd.matrixU() * V.transpose()) * (V * svd.singularValues() * V.transpose());
    // write the results back to the pointsToTransform array
    for (vtkIdType i = 0; i < nbpts; i++)
    {
        //p1 = p1*M + un*t2;    // p1 was originally shifted by t1, but now we are adding t2 to it?
        pointsTT.row(i) = pointsTT.row(i) - M * pointsTT.row(i);
        pointsToTransform->SetPoint(i, pointsTT(i, 0) + centroidPTTD[0], pointsTT(i, 1) + centroidPTTD[1], pointsTT(i, 2) + centroidPTTD[2]);
    }
    pointsToTransform->Modified();
}