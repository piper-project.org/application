# Copyright (C) 2017 INRIA
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Thomas Lemaire (INRIA)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import piper.app
import piper.hbm
import piper.sofa

def meshLoader(parentNode, filename, name=None, **args):
    """ designed to replace SofaPython.Tools.meshLoader
    """
    entityNames = filename
    if name is None:
        _name="loader_"+entityNames.replace(" ", "_")
    else:
        _name=name
    if len(_name)>30:
        _name = _name[:30]
    return parentNode.createObject('PiperMeshLoader', entityNames=entityNames, name=_name, envelopOnly=True, **args) # no where else to put the envelopOnly=True option

def getFirstVertex(mesh):
    """ designed to replace Anatomy.Tools.getFirstVertex
    """
    hbmModel = piper.sofa.project().model()
    entity = hbmModel.metadata().entity(mesh)
    if len(entity.get_groupElement2D()) > 0 or len(entity.get_groupElement3D()) > 0:
        vertex = entity.getOneVertex(hbmModel.fem())
        return vertex
    else:
        print "[getFirstVertex] ERROR: no groupElement2D or groupElement3D in entity", mesh
        print "DEBUG:", entity.groupElement2D, entity.groupElement3D
    return

def RigidMassInfo_setFromMesh(self, entityName, density = 1000, scale3d=[1,1,1], rotation=[0,0,0]):
    """ designed to replace SofaPython.mass.RigidMassInfo.setFromMesh
    """
    self.density = density
    hbmModel = piper.sofa.project().model()
    rigidInfo = piper.sofa.generateRigid(hbmModel, entityName, density)
    self.mass = rigidInfo[0]
    self.com = rigidInfo[1:4]
    self.diagonal_inertia = rigidInfo[4:7]
    self.inertia_rotation = rigidInfo[7:11]
