# Copyright (C) 2017 INRIA
# This file is part of the PIPER Framework.
# Version: 1.0.0
# 
# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option) any
# later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. You should have received a copy of the GNU General Public
# License along with the PIPER Framework.
# If not, see <http://www.gnu.org/licenses/>.
# 
# Contributors include Thomas Lemaire (INRIA)
# 
# This work has received funding from the European Union Seventh Framework
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
# 
import Sofa

import piper.app
import piper.hbm
import piper.sofa

import piper.anatomyDB

import SofaPython.units
import SofaPython.sml

class Model(SofaPython.sml.Model):

    # skin solids to be created if possible: head, body, left/right arms and left/right legs
    skinSolids = {"Skin_of_head", "Skin_of_body_proper", "Skin_of_left_free_upper_limb", "Skin_of_right_free_upper_limb", "Skin_of_left_free_lower_limb", "Skin_of_right_free_lower_limb"}

    # entities in these regions are attached to several skin entities
    boundaryRegionToSkinSolids = {
        "Neck":        ["Skin_of_body_proper", "Skin_of_head"],
        "Left_arm":    ["Skin_of_body_proper", "Skin_of_left_free_upper_limb"],
        "Left_pectoral_girdle" : ["Skin_of_body_proper"],
        "Right_arm":   ["Skin_of_body_proper", "Skin_of_right_free_upper_limb"],
        "Right_pectoral_girdle" : ["Skin_of_body_proper"],
        "Left_thigh":  ["Skin_of_body_proper", "Skin_of_left_free_lower_limb"],
        "Right_thigh": ["Skin_of_body_proper", "Skin_of_right_free_lower_limb"] }

    def __init__(self, doPaintBoneCapsule=False, doAddJoint=True, doAddContact=True, doAddCapsule=True, doAddLigament=True):
        SofaPython.sml.Model.__init__(self)
        self.name="piper"
        hbmModel = piper.sofa.project().model()
        self.units["length"] = piper.hbm.unitToStr(hbmModel.metadata().lengthUnit())
        # Get metadata from the GenericMetadata
        self.articularCapsuleBoneMetadata = dict() # bone nodes inside articular capsule
        for name, metadata in hbmModel.metadata().genericmetadatas().iteritems() :
            nameList = name.split("__")
            if len(nameList) == 3 and "bone_region" == nameList[0]:
                if not nameList[1] in self.articularCapsuleBoneMetadata:
                    self.articularCapsuleBoneMetadata[nameList[1]] = []
                self.articularCapsuleBoneMetadata[nameList[1]].append(metadata)
        # create solids from piper entities
        self.isSkinMultiEntities = False
        # gather model entities into skin solids
        skinSolidToEntities = {}
        for skinSolid in self.skinSolids :
            skinSolidToEntities[skinSolid] = []
        # gather cartilage entities to try to include them in a relevant bone
        cartilageEntities = []
        for entityName in hbmModel.metadata().entities().keys():
            if not piper.anatomyDB.exists(entityName):
                piper.app.logDebug("Unknown entity {0} - ignored".format(entityName))
                continue
            elif piper.anatomyDB.isEntitySubClassOf(entityName, "Skin") and not piper.anatomyDB.isSynonymOf("Skin", entityName):
                self.isSkinMultiEntities = True
                for skinSolid in skinSolidToEntities.iterkeys() :
                    if piper.anatomyDB.isSynonymOf(entityName, skinSolid) or piper.anatomyDB.isEntityPartOf(entityName, skinSolid, True):
                        skinSolidToEntities[skinSolid].append(entityName)
                continue
            elif piper.anatomyDB.isEntitySubClassOf(entityName, "Cartilage"):
                cartilageEntities.append(entityName)
                continue
            entity = hbmModel.metadata().entity(entityName)
            # sml.Mesh
            mesh = SofaPython.sml.Model.Mesh()
            mesh.format = "piper"
            mesh.id = entity.name()+"_mesh"
            mesh.name = entity.name()
            mesh.source = entity.name()
            self.meshes[mesh.id] = mesh
            # sml.Solid
            solid = SofaPython.sml.Model.Solid()
            solid.id = piper.anatomyDB.getReferenceName(entity.name()) if piper.anatomyDB.exists(entityName) else entity.name()
            solid.name = entity.name()
            solid.position = [0,0,0,0,0,0,1]
            solid.addMesh(mesh)
            if piper.anatomyDB.isEntitySubClassOf(entity.name(),"Bone"):
                if not len(entity.get_groupElement2D())+len(entity.get_groupElement3D())>0 :
                    piper.app.logWarning("no 2D or 3D element in bone "+entity.name()+" - ignored")
                else:
                    solid.tags.add("bone")
                    if doPaintBoneCapsule and solid.name in self.articularCapsuleBoneMetadata:
                        for metadata in self.articularCapsuleBoneMetadata[solid.name]:
                            group = self.getMeshGroupFromGenericMetadata(hbmModel.fem(), entity, metadata)
                            group.tags.add("capsule")
                            group.tags.add("toBePainted")
                            groupId = metadata.name().split("__")[2]
                            mesh.group[groupId] = group
#                            piper.app.logDebug("group {0} size {1} on bone {2}".format(groupId, len(group), solid.name)) # TODO once sofa updated
                            piper.app.logDebug("group {0} on bone {1}".format(groupId, solid.name))
            elif piper.anatomyDB.isEntitySubClassOf(entity.name(),"Articular_capsule"):
                if not doAddCapsule:
                    continue
                if not len(entity.get_groupElement2D())+len(entity.get_groupElement3D())>0 :
                    piper.app.logWarning("no 2D or 3D element in articular capsule "+entity.name()+" - ignored")
                else:
                    solid.tags.add("capsule")
            elif piper.anatomyDB.isEntitySubClassOf(entity.name(),"Ligament"):
                if not doAddLigament:
                    continue
                if not len(entity.get_groupElement2D())+len(entity.get_groupElement3D())>0 :
                    piper.app.logWarning("no 2D or 3D element in ligament "+entity.name()+" - ignored")
                else:
                    solid.tags.add("ligament")
            elif piper.anatomyDB.isSynonymOf("Skin", entity.name()):
                solid.tags.add("skin")
                # solid.meshAttributes[mesh.id].collision = False # TODO optimization | modify oglmodel create so that it loads mesh using meshLoader...
            else:
                piper.app.logWarning("Unknown entity: "+entity.name()+" - ignored")
                continue
            self.solids[solid.id] = solid
        # cartilage entities
        self.addCartilageToBone(cartilageEntities)
        if self.isSkinMultiEntities:
            for region, piperEntities in skinSolidToEntities.iteritems():
                if (not len(piperEntities)):
                    piper.app.logWarning("No skin entity defined for region {0}".format(region))
                    continue
                solid = SofaPython.sml.Model.Solid()
                solid.id = piper.anatomyDB.getReferenceName(region)
                solid.name = piper.anatomyDB.getReferenceName(region)
                solid.position = [0,0,0,0,0,0,1]
                solid.tags.add("skin")
                for meshName in piperEntities:
                    # sml.Mesh
                    mesh = SofaPython.sml.Model.Mesh()
                    mesh.format = "piper"
                    mesh.id = meshName+"_mesh"
                    mesh.name = meshName
                    mesh.source = meshName
                    self.meshes[mesh.id] = mesh
                    solid.addMesh(mesh)
                self.solids[solid.id] = solid
            piper.app.logDebug("Simulated skin defined in body parts: {0}".format(skinSolidToEntities))
        else:
            piper.app.logDebug("Simulated skin defined in a single Skin entity")

        if self.isSkinMultiEntities:
            # attach the inner entities (bones...) to the flesh (the inside of the skin)
            # smallest skin region for each skin solid
            regionToSkin = {}
            for skinSolid in self.skinSolids :
                regionToSkin[piper.anatomyDB.getPartOfSubClassList(skinSolid, "Region")[0]] = skinSolid
            # gather solids without region, attach them to all skin solids
            solidsWithoutRegion = []
            # set to compute set intersection
            boundaryRegionToSkinSolidsSet = set(self.boundaryRegionToSkinSolids.keys())
            skinRegionSet = set(regionToSkin.keys())
            for solid in self.solids.itervalues() :
                if "skin" in solid.tags :
                    continue
                # find solid region
                solidRegions = []
                if piper.anatomyDB.exists(solid.id):
                    solidRegions = piper.anatomyDB.getPartOfSubClassList(solid.id, "Region", True)
                solidboundaryRegionToSkinSolids = set(solidRegions) & boundaryRegionToSkinSolidsSet
                if len(solidboundaryRegionToSkinSolids) > 0:
                    for region in solidboundaryRegionToSkinSolids:
                        for skinSolidName in self.boundaryRegionToSkinSolids[region] :
                            if not skinSolidName in self.solids:
                                piper.app.logWarning("Missing skin entity {0} - {1} cannot be attached to it".format(skinSolidName, solid.name))
                                continue
                            skinSolid = self.solids[skinSolidName]
                            contact = SofaPython.sml.Model.SurfaceLink()
                            contact.id = solid.id+"_to_"+skinSolid.id
                            contact.name = contact.id
                            contact.tags.add("attached")
                            contact.surfaces[0] = SofaPython.sml.Model.Surface()
                            contact.surfaces[0].solid = solid
                            contact.surfaces[0].group = "surface"
                            contact.surfaces[1] = SofaPython.sml.Model.Surface()
                            contact.surfaces[1].solid = skinSolid
                            self.surfaceLinks[contact.id]=contact
                else :
                    solidSkinRegion = set(solidRegions) & skinRegionSet
                    if 0 == len(solidSkinRegion) :
                        solidsWithoutRegion.append(solid)
                        continue
                    for region in solidSkinRegion:
                        skinSolidName = regionToSkin[region]
                        if not skinSolidName in self.solids:
                            piper.app.logWarning("Missing skin entity {0} - {1} cannot be attached to it".format(skinSolidName, solid.name))
                            continue
                        skinSolid = self.solids[skinSolidName]
                        contact = SofaPython.sml.Model.SurfaceLink()
                        contact.id = solid.id+"_to_"+skinSolid.id
                        contact.name = contact.id
                        contact.tags.add("attached")
                        contact.surfaces[0] = SofaPython.sml.Model.Surface()
                        contact.surfaces[0].solid = solid
                        contact.surfaces[0].group = "surface"
                        contact.surfaces[1] = SofaPython.sml.Model.Surface()
                        contact.surfaces[1].solid = skinSolid
                        self.surfaceLinks[contact.id]=contact

            for solid in solidsWithoutRegion :
                piper.app.logWarning("Entity {0} attached to all possible flesh".format(solid.name))
                for skinSolidName in self.skinSolids :
                    if not skinSolidName in self.solids:
                        continue
                    skinSolid = self.solids[skinSolidName]
                    contact = SofaPython.sml.Model.SurfaceLink()
                    contact.id = solid.id+"_to_"+skinSolid.id
                    contact.name = contact.id
                    contact.tags.add("attached")
                    contact.surfaces[0] = SofaPython.sml.Model.Surface()
                    contact.surfaces[0].solid = solid
                    contact.surfaces[0].group = "surface"
                    contact.surfaces[1] = SofaPython.sml.Model.Surface()
                    contact.surfaces[1].solid = skinSolid
                    self.surfaceLinks[contact.id]=contact

        self.logEntityInfo("bone")
        self.logEntityInfo("capsule")
        self.logEntityInfo("ligament")

        # create joints from piper entities joints
        if doAddJoint:
            for entityJoint in hbmModel.metadata().joints().values():
                joint = SofaPython.sml.Model.JointGeneric()
                joint.id = entityJoint.name()
                joint.name = entityJoint.name()
                # more precise warning message
                if not piper.anatomyDB.exists(entityJoint.getEntity1()) or not piper.anatomyDB.isBone(entityJoint.getEntity1()):
                    piper.app.logWarning("unknown bone entity {0} in joint {1} - ignored".format(entityJoint.getEntity1(), entityJoint.name()))
                    continue
                if not piper.anatomyDB.exists(entityJoint.getEntity2()) or not piper.anatomyDB.isBone(entityJoint.getEntity2()):
                    piper.app.logWarning("unknown bone entity {0} in joint {1} - ignored".format(entityJoint.getEntity2(), entityJoint.name()))
                    continue
                if not (piper.anatomyDB.getReferenceName(entityJoint.getEntity1()) in self.solids and piper.anatomyDB.getReferenceName(entityJoint.getEntity2()) in self.solids):
                    piper.app.logWarning("missing entity in joint {0} - ignored".format(entityJoint.name()))
                    continue
                joint.solids[0] = self.solids[piper.anatomyDB.getReferenceName(entityJoint.getEntity1())]
                joint.solids[1] = self.solids[piper.anatomyDB.getReferenceName(entityJoint.getEntity2())]

                joint.offsets[0] = SofaPython.sml.Model.Offset()
                joint.offsets[0].name = entityJoint.name()+"_"+entityJoint.getEntity1()
                joint.offsets[0].value = list(hbmModel.fem().getFrameOriginVec(entityJoint.getEntity1FrameId()) + hbmModel.fem().getFrameOrientationVec(entityJoint.getEntity1FrameId()))
                joint.offsets[1] = SofaPython.sml.Model.Offset()
                joint.offsets[1].name = entityJoint.name()+"_"+entityJoint.getEntity2()
                joint.offsets[1].value = list(hbmModel.fem().getFrameOriginVec(entityJoint.getEntity2FrameId()) + hbmModel.fem().getFrameOrientationVec(entityJoint.getEntity2FrameId()))
                for index,isDof in enumerate(entityJoint.getDofVec()):
                    if isDof:
                        dof = SofaPython.sml.Model.Dof()
                        dof.index = index
                        joint.dofs.append(dof)
                if entityJoint.getConstrainedDofType() == piper.hbm.EntityJoint.ConstrainedDofType_SOFT:
                    joint.tags.add("soft")
                self.genericJoints[joint.id]=joint
            piper.app.logInfo("{0} joints".format(len(self.genericJoints)))
            if len(self.genericJoints)>0:
                piper.app.logDebug(", ".join(self.genericJoints.keys()))

        if doAddContact:
            # create contacts from piper entities contact
            for entityContact in hbmModel.metadata().contacts().values():
                entity1Name = piper.anatomyDB.getReferenceName(entityContact.entity1()) if piper.anatomyDB.exists(entityContact.entity1()) else entityContact.entity1()
                entity2Name = piper.anatomyDB.getReferenceName(entityContact.entity2()) if piper.anatomyDB.exists(entityContact.entity2()) else entityContact.entity2()
                if not ( entity1Name in self.solids and entity2Name in self.solids ):
                    piper.app.logWarning("missing entity in contact " + entityContact.name() + " - ignored")
                    continue
                # add a surfaceLink to represent the contact
                contact = SofaPython.sml.Model.SurfaceLink()
                contact.id = entityContact.name()
                contact.name = entityContact.name()
                if (entityContact.typeStr()=="ATTACHED"):
                    contact.tags.add("attached")
                elif (entityContact.typeStr()=="SLIDING"):
                    contact.tags.add("sliding")
                else:
                    piper.app.logWarning("unknown contact type "+ entityContact.typeStr() + " in contact " + entityContact.name() + " - ignored")
                    continue
                # contact with entity1
                contact.surfaces[0] = SofaPython.sml.Model.Surface()
                contact.surfaces[0].solid = self.solids[entity1Name]
                contact.surfaces[0].mesh = contact.surfaces[0].solid.mesh[0]
                if entityContact.group1() != piper.hbm.ID_UNDEFINED:
                    groupName = "contact_"+entityContact.name()+"_"+entityContact.entity1()
                    contact.surfaces[0].group = groupName
                    # create a group in the entity mesh to represent the contact surface
                    meshGroup = SofaPython.sml.Model.Mesh.Group()
                    try:
                        meshGroup.index = piper.sofa.getEntityNodesIndex(entityContact.entity1(), entityContact.group1())
                    except BaseException, e:
                        piper.app.logWarning(str(e)+" - ignored contact "+entityContact.name())
                        continue
                    contact.surfaces[0].mesh.group[groupName] = meshGroup
                # contact with entity2
                contact.surfaces[1] = SofaPython.sml.Model.Surface()
                contact.surfaces[1].solid = self.solids[entity2Name]
                contact.surfaces[1].mesh = contact.surfaces[1].solid.mesh[0]
                if entityContact.group2() != piper.hbm.ID_UNDEFINED:
                    groupName = "contact_"+entityContact.name()+"_"+entityContact.entity2()
                    contact.surfaces[1].group = groupName
                    # create a group in the entity mesh to represent the contact surface
                    meshGroup = SofaPython.sml.Model.Mesh.Group()
                    try:
                        meshGroup.index = piper.sofa.getEntityNodesIndex(entityContact.entity2(), entityContact.group2())
                    except BaseException, e:
                        piper.app.logWarning(str(e)+" - ignored contact "+entityContact.name())
                        continue
                    contact.surfaces[1].mesh.group[groupName] = meshGroup
                self.surfaceLinks[contact.id]=contact
            slidingContacts = self.getSurfaceLinksByTags({"sliding"})
            piper.app.logInfo("{0} sliding contacts".format(len(slidingContacts)))
            if len(slidingContacts)>0:
                piper.app.logDebug(", ".join(map(lambda contact:contact.name, slidingContacts)))

    def logEntityInfo(self, tag):
        infoSolids = self.getSolidsByTags({tag})
        piper.app.logInfo("{0} {1} entities".format(len(infoSolids), tag))
        if len(infoSolids)>0:
            piper.app.logDebug(", ".join(map(lambda solid:solid.name, infoSolids)))

    def getMeshGroupFromGenericMetadata(self, fem, entity, metadata):
        nodeIds = metadata.getNodeIdVec(fem)
        nodeIdToIndex = entity.getEnvelopNodesToIndexMap(fem)
        group = SofaPython.sml.Model.Mesh.Group()
        for id in nodeIds:
            if not id in nodeIdToIndex:
                piper.app.logWarning("No node {0} in entity {1} as requested by {2} - ignored".format(id, entity.name(), metadata.name()))
                continue
            group.index.append(nodeIdToIndex[id])
        return group

    def addCartilageToBone(self, cartilageEntities):
        hbmModel = piper.sofa.project().model()
        addedCartilage = list()
        for cartilage in cartilageEntities:
            boneList = piper.anatomyDB.getInsertOnList(cartilage, "Bone")
            if len(boneList)>1:
                piper.app.logWarning("{0} inserts on more than one bone, skipped".format(cartilage))
                continue
            for boneName in boneList[0]:
                if boneName in self.solids:
                    piper.app.logInfo("Add {0} to {1}".format(cartilage, boneName))
                    bone = self.solids[boneName]
                    mesh = SofaPython.sml.Model.Mesh()
                    mesh.format = "piper"
                    mesh.id = cartilage+"_mesh"
                    mesh.name = cartilage
                    mesh.source = cartilage
                    self.meshes[mesh.id] = mesh
                    bone.addMesh(mesh)
                    addedCartilage.append(cartilage)
                    break # TODO warning when no bone is found
        piper.app.logInfo("{0} cartilage entities".format(len(addedCartilage)))
        if len(addedCartilage)>0:
            piper.app.logDebug(", ".join(addedCartilage))


    def addBoneCollision(self):
        # create collision between bones for bones anatomically involved in the same joint
        boneCollisions = list()
        for joint in piper.anatomyDB.getSubClassOfFromBibliographyList("Joint", "ISB_JCS"):
            bones = piper.anatomyDB.getSubPartOfList(joint, "Bone")
            if len(bones) != 2:
                continue
            # look for bones in the current model, also look for partOf
            boneSolid0=None
            if bones[0] in self.solids:
                boneSolid0 = self.solids[bones[0]]
            else:
                for bone in piper.anatomyDB.getPartOfSubClassList(bones[0], "Bone"):
                    if bone in self.solids:
                        boneSolid0 = self.solids[bone]
            boneSolid1=None
            if bones[1] in self.solids:
                boneSolid1 = self.solids[bones[1]]
            else:
                for bone in piper.anatomyDB.getPartOfSubClassList(bones[1], "Bone"):
                    if bone in self.solids:
                        boneSolid1 = self.solids[bone]

            if not boneSolid0 is None and not boneSolid1 is None and not boneSolid0 is boneSolid1:
                for (i0,mesh0) in enumerate(boneSolid0.mesh):
                    for (i1,mesh1) in enumerate(boneSolid1.mesh):
                        contact = SofaPython.sml.Model.SurfaceLink()
                        contact.id = "collision__{0}_{1}__{2}_{3}".format(bones[0], i0, bones[1], i1)
                        contact.name = contact.id
                        contact.tags.add("collision")
                        contact.tags.add("symmetric")
                        # contact with bone[0]
                        contact.surfaces[0] = SofaPython.sml.Model.Surface()
                        contact.surfaces[0].solid = boneSolid0
                        contact.surfaces[0].mesh = mesh0
                        # contact with bone[1]
                        contact.surfaces[1] = SofaPython.sml.Model.Surface()
                        contact.surfaces[1].solid = boneSolid1
                        contact.surfaces[1].mesh = mesh1
                        self.surfaceLinks[contact.id]=contact
                boneCollisions.append("{0}/{1} ({2})".format(contact.surfaces[0].solid.name, contact.surfaces[1].solid.name, joint))
        if len(boneCollisions):
            piper.app.logInfo("{0} bone collisions".format(len(boneCollisions)))
            piper.app.logDebug(', '.join(boneCollisions))
        else :
            piper.app.logInfo("No bone collision")

    def addSoftTissueContact(self, tags, doAttach=True, doCollision="insert"):
        # create attached contact and collisions between soft tissues (capsule, ligaments) and bones
        # attach articular capsules to their bones on closing + collisions
        # doCollision in {"none", "insert", "all"}
        # TODO optimize using bone capsule roi
        softTissueCollisions = list()
        for capsule in self.getSolidsByTags(tags):
            # bones to which the capsule is inserted on
            insertOnList = piper.anatomyDB.getInsertOnList(capsule.id, "Bone");
            # find the bones in the model related to this capsule
            modelBoneSet = set()
            for boneList in insertOnList:
                isModelBoneFound = False
                for boneName in boneList:
                    if boneName in self.solids:
                        modelBoneSet.add(self.solids[boneName])
                        isModelBoneFound = True
                        break
                if not isModelBoneFound:
                    piper.app.logWarning("None of the bone {0} can be found, collision and attachment on that bone for {1} are skipped".format(boneList, capsule.name))
            for bone in modelBoneSet:
                if doAttach:
                    # attach soft tissue to bones
                    contact = SofaPython.sml.Model.SurfaceLink()
                    contact.id = capsule.id+"_to_"+bone.id
                    contact.name = contact.id
                    contact.tags.add("attached")
                    contact.surfaces[0] = SofaPython.sml.Model.Surface()
                    contact.surfaces[0].solid = capsule
                    contact.surfaces[0].group = "closing"
                    contact.surfaces[1] = SofaPython.sml.Model.Surface()
                    contact.surfaces[1].solid = bone
                    self.surfaceLinks[contact.id]=contact
#                    piper.app.logDebug("Capsule attached to bone {0}".format(contact.id))
                if "insert" == doCollision:
                    # soft tissue in collision with bones in insert on
                    contact = SofaPython.sml.Model.SurfaceLink()
                    contact.id = "collision__{0}__{1}".format(capsule.id, bone.id)
                    contact.name = contact.id
                    contact.tags.add("collision")
                    contact.tags.add("symmetric")
                    contact.tags.add("ignoreInitialPenetration")
                    contact.surfaces[0] = SofaPython.sml.Model.Surface()
                    contact.surfaces[0].solid = capsule
                    contact.surfaces[0].mesh = contact.surfaces[0].solid.mesh[0]
                    contact.surfaces[1] = SofaPython.sml.Model.Surface()
                    contact.surfaces[1].solid = bone
                    contact.surfaces[1].mesh = contact.surfaces[1].solid.mesh[0]
                    if not capsule.name in contact.surfaces[1].solid.mesh[0].group:
                        if not bone.id in {"Left_patella", "Right_patella"}: # for patella this is normal
                            piper.app.logWarning("No inner capsule bone surface defined for capsule {0} on bone {1}, collision with full bone".format(capsule.name, bone.name))
                    else:
                        contact.surfaces[1].group = capsule.name
                    self.surfaceLinks[contact.id] = contact
                    softTissueCollisions.append("{0}/{1}".format(contact.surfaces[0].solid.name, contact.surfaces[1].solid.name))
            if "all" == doCollision:
                jointName = piper.anatomyDB.getPartOfSubClassList(capsule.id, "Joint")[0]
                jointBones = list(piper.anatomyDB.getSubPartOfList(jointName, "Bone"))
                jointBonesPartOf = ()
                for boneName in jointBones:
                    jointBonesPartOf += piper.anatomyDB.getPartOfSubClassList(boneName, "Bone", True)
                jointBones += jointBonesPartOf
                for boneName in jointBones:
                    if not boneName in self.solids:
                        continue
                    bone = self.solids[boneName]
                    contact = SofaPython.sml.Model.SurfaceLink()
                    contact.id = "collision__{0}__{1}".format(capsule.id, bone.id)
                    contact.name = contact.id
                    contact.tags.add("collision")
                    contact.tags.add("symmetric")
                    contact.tags.add("ignoreInitialPenetration")
                    contact.surfaces[0] = SofaPython.sml.Model.Surface()
                    contact.surfaces[0].solid = capsule
                    contact.surfaces[0].mesh = contact.surfaces[0].solid.mesh[0]
                    contact.surfaces[1] = SofaPython.sml.Model.Surface()
                    contact.surfaces[1].solid = bone
                    contact.surfaces[1].mesh = contact.surfaces[1].solid.mesh[0]
                    if not capsule.name in contact.surfaces[1].solid.mesh[0].group:
                        if not bone.id in {"Left_patella", "Right_patella"}: # for patella this is normal
                            piper.app.logWarning("No inner capsule bone surface defined for capsule {0} on bone {1}".format(capsule.name, bone.name))
                    else:
                        contact.surfaces[1].group = capsule.name
#                    piper.app.logDebug("capsule collision bone groups: {0}".format(contact.surfaces[1].mesh.group))
                    self.surfaceLinks[contact.id] = contact
                    softTissueCollisions.append("{0}/{1}".format(contact.surfaces[0].solid.name, contact.surfaces[1].solid.name))
        if len(softTissueCollisions):
            piper.app.logInfo("{0} [{1}] collisions".format(len(softTissueCollisions), ", ".join(tags)))
            piper.app.logDebug(', '.join(softTissueCollisions))
        else :
            piper.app.logInfo("No [{0}] collision".format(", ".join(tags)))

    def addAnatomicalJoint(self):
        metadata = piper.sofa.project().model().metadata()
        if 0 == len(metadata.anatomicalJoints()):
            return
        # fill the frame factory with landmarks
        ff = piper.anatomyDB.FrameFactory.instance()
        # create the joints
        for joint in metadata.anatomicalJoints().values():
            smlJoint = SofaPython.sml.Model.JointGeneric()
            smlJoint.id = joint.name()
            smlJoint.name = joint.name()
            frames = piper.anatomyDB.getSubPartOfList(joint.name(),"Frame") # TODO check also for "ISB_JCS"
            if not len(frames) == 2:
                piper.app.logWarning("In joint {0} incorrect number of frames {1} - ignored".format(joint.name(), frames))
                continue
            smlJoint.solids=[] # initialized with [None,None]
            for frame in frames:
                # get bone to which the frame belongs, look for group of bones too
                bone = piper.anatomyDB.getPartOfSubClassList(frame,"Bone")
                while len(bone) > 0 :
                    if bone[0] in self.solids:
                        # if found, append the solid to the smlJoint
                        smlJoint.solids.append(self.solids[bone[0]])
                        break
                    else:
                        bone = piper.anatomyDB.getPartOfSubClassList(bone[0],"Bone")
            if not len(smlJoint.solids) == 2:
                piper.app.logWarning("In joint {0} articulated solids not found - ignored".format(jointName))
                continue
            # now compute offset
            smlJoint.offsets=[] # initialized with [None,None]
            for frame in frames:
                try:
                    vec = ff.computeFrame(frame)
                except:
                    piper.app.logWarning("In joint {0} frame {1} cannot be computed - error {2}".format(jointName,frame, sys.exc_value))
                else:
                    smlOffset = SofaPython.sml.Model.Offset()
                    smlOffset.name = frame
                    smlOffset.value = vec.toVec().flatten().tolist()
                    smlJoint.offsets.append(smlOffset)

            if not len(smlJoint.offsets) == 2:
                continue
            # and lastly the dof
            for index,isDof in enumerate(joint.getDofVec()):
                if isDof:
                    dof = SofaPython.sml.Model.Dof()
                    dof.index = index
                    smlJoint.dofs.append(dof)
            if joint.getConstrainedDofType() == piper.hbm.AnatomicalJoint.ConstrainedDofType_SOFT:
                smlJoint.tags.add("soft")
            self.genericJoints[smlJoint.id]=smlJoint




