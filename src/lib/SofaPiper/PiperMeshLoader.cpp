/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "tools.h"
#include "PiperMeshLoader.h"
#include <hbm/HumanBodyModel.h>
#include <hbm/FEModel.h>
#include <hbm/EnvironmentModels.h>

#pragma warning(push, 0)
#include <sofa/core/ObjectFactory.h>
#pragma warning(pop)

namespace sofa
{

namespace component
{

namespace loader
{

using namespace sofa::defaulttype;
using namespace piper::hbm;

SOFA_DECL_CLASS(PiperMeshLoader)

int PiperMeshLoaderClass = core::RegisterObject("Load geometry from the Piper model")
        .add<PiperMeshLoader>()
        ;

PiperMeshLoader::PiperMeshLoader()
    : d_loadModel(initData(&d_loadModel, false, "loadModel", "flag to load the full model"))
    , d_envelopOnly(initData(&d_envelopOnly, false, "envelopOnly", "flag to load only the 2d envelop of the entity"))
    , d_loadAllNodes(initData(&d_loadAllNodes, false, "loadAllNodes", "flag to load all nodes from the model"))
    , d_entityNames(initData(&d_entityNames, "entityNames", "List of entities"))
    , d_environmentName(initData(&d_environmentName, "environmentName", "Name of the environment"))
    , d_landmarkName(initData(&d_landmarkName, "landmarkName", "Name of the landmark"))
    , d_positionsId(initData(&d_positionsId, "positionId", "ids of the positions"))
    , m_lastNodeIndex(0)
{ }

bool PiperMeshLoader::canLoad()
{
    if (d_entityNames.isSet()) {
        auto const& entities = sofaPiper::project().model().metadata().entities();
        for (std::string const& name : d_entityNames.getValue())
            if (entities.find(name) == entities.end()) return false;
        return true;
    }
    else if (d_environmentName.isSet()) {
        return sofaPiper::project().environment().isExistingName(d_environmentName.getValue());
    }
    else if (d_landmarkName.isSet())
        return sofaPiper::project().model().metadata().hasLandmark(d_landmarkName.getValue());
    else if (d_loadModel.getValue()||d_loadAllNodes.getValue())
        return true;
    else
        serr << "canLoad(): nothing to load" << sendl;
    return false;
}


void PiperMeshLoader::addNode(Node const& node)
{
    if (m_piperIdToIndex.count(node.getId()) == 0) {
        m_editPosition->push_back(Vector3(node.getCoordX(), node.getCoordY(), node.getCoordZ()));
        m_editPositionsId->push_back(node.getId());
        m_piperIdToIndex.insert(std::make_pair(node.getId(), m_lastNodeIndex));
        ++m_lastNodeIndex;
    }
}

void PiperMeshLoader::addElement1D(Element1D const& elem, bool doAddNodes)
{
    FEModel const& fem = sofaPiper::project().model().fem();
    ElemDef const& elemDef = elem.get();

    if (doAddNodes) {
        // add element nodes
        if (m_editPosition!=nullptr && m_editPositionsId!=nullptr){
            for (auto it = elemDef.begin(); it!=elemDef.end(); ++it)
                addNode(fem.getNode(*it));
        }
    }

    // add element
    switch(elem.getType()) {
    case ElementType::ELT_BAR:{
        addEdge(m_editBar, Edge(m_piperIdToIndex.at(elemDef[0]),m_piperIdToIndex.at(elemDef[1])));
        break;
    }
    default:
        serr << "ElementType not handled: " << (int)elem.getType() << sendl;
        break;
    }
}

void PiperMeshLoader::addElement2D(Element2D const& elem, piper::hbm::FEModel const& fem, bool doAddNodes)
{
    ElemDef const& elemDef = elem.get();

    if (doAddNodes) {
        // add element nodes
        if (m_editPosition!=nullptr && m_editPositionsId!=nullptr){
            for (auto it = elemDef.begin(); it!=elemDef.end(); ++it)
                addNode(fem.getNode(*it));
        }
    }

    // add element
    switch(elem.getType()) {
    case ElementType::ELT_TRI:
    case ElementType::ELT_TRI_THICK:
        addTriangle(m_editTriangles, Triangle(m_piperIdToIndex.at(elemDef[0]), m_piperIdToIndex.at(elemDef[1]), m_piperIdToIndex.at(elemDef[2])));
        break;
    case ElementType::ELT_QUAD:
    case ElementType::ELT_QUAD_THICK:
        addQuad(m_editQuads, Quad(m_piperIdToIndex.at(elemDef[0]), m_piperIdToIndex.at(elemDef[1]), m_piperIdToIndex.at(elemDef[2]), m_piperIdToIndex.at(elemDef[3])));
        break;
     default:
        serr << "ElementType not handled: " << (int)elem.getType() << sendl;
        break;
    }
}

void PiperMeshLoader::addElement3D(Element3D const& elem, bool doAddNodes)
{
    FEModel const& fem = sofaPiper::project().model().fem();
    ElemDef const& elemDef = elem.get();

    if (doAddNodes) {
        // add element nodes
        if (m_editPosition!=nullptr && m_editPositionsId!=nullptr){
            for (auto it = elemDef.begin(); it!=elemDef.end(); ++it)
                addNode(fem.getNode(*it));
        }
    }

    // add element
    switch(elem.getType()) {
    case ElementType::ELT_TETRA:{
        addTetrahedron(m_editTetra, Tetrahedron(m_piperIdToIndex.at(elemDef[0]),m_piperIdToIndex.at(elemDef[1]),m_piperIdToIndex.at(elemDef[2]),m_piperIdToIndex.at(elemDef[3])));
        break;
    }               
    case ElementType::ELT_HEXA:{
        addHexahedron(m_editHexa, Hexahedron(m_piperIdToIndex.at(elemDef[0]),m_piperIdToIndex.at(elemDef[1]),m_piperIdToIndex.at(elemDef[2]),m_piperIdToIndex.at(elemDef[3]),m_piperIdToIndex.at(elemDef[4]),m_piperIdToIndex.at(elemDef[5]),m_piperIdToIndex.at(elemDef[6]),m_piperIdToIndex.at(elemDef[7])));
        break;
    }
    case ElementType::ELT_PENTA:{
        addPentahedron(m_editPenta, Pentahedron(m_piperIdToIndex.at(elemDef[0]),m_piperIdToIndex.at(elemDef[1]),m_piperIdToIndex.at(elemDef[2]),m_piperIdToIndex.at(elemDef[3]),m_piperIdToIndex.at(elemDef[4]),m_piperIdToIndex.at(elemDef[5])));
        break;
    }
    case ElementType::ELT_PYRA:{
        addPyramid(m_editPyramids, Pyramid(m_piperIdToIndex.at(elemDef[0]),m_piperIdToIndex.at(elemDef[1]),m_piperIdToIndex.at(elemDef[2]),m_piperIdToIndex.at(elemDef[3]),m_piperIdToIndex.at(elemDef[4])));
        break;
    }
    default:
        serr << "ElementType not handled: " << (int)elem.getType() << sendl;
        break;
    }
}

void PiperMeshLoader::loadEntity(std::string const& entityName, bool envelopOnly)
{
    sout << "loadEntity(): " << entityName << sendl;
    FEModel const& fem = sofaPiper::project().model().fem();
    Entity const& entity = sofaPiper::project().model().metadata().entity(entityName);

    // group of nodes
//    for (auto it = entity.groupNode.begin(); it != entity.groupNode.end(); ++it) {
//        SId sid = fem.getGroupNode(*it).get();
//        for(auto it=sid.begin(); it!=sid.end(); ++it) {
//            addNode(myPosition, fem.getNode(*it));
//        }
//    }

    if (!envelopOnly) {
		// group element 2D
        for (auto it = entity.get_groupElement2D().begin(); it != entity.get_groupElement2D().end(); ++it) {
			VId sid = fem.getGroupElements2D().getEntity(*it)->get();
			for(auto it=sid.begin(); it!=sid.end(); ++it) {
				Element2D const& elem = fem.getElement2D(*it);
                addElement2D(elem, fem);
			}
		}


		// group element 3D
        for (auto it = entity.get_groupElement3D().begin(); it != entity.get_groupElement3D().end(); ++it) {
			VId sid = fem.getGroupElements3D().getEntity(*it)->get();
			for(auto it=sid.begin(); it!=sid.end(); ++it) {
				Element3D const& elem = fem.getElement3D(*it);
				addElement3D(elem);
			}
		}

		//group element 1D
        for (auto it = entity.get_groupElement1D().begin(); it != entity.get_groupElement1D().end(); ++it) {
			VId sid = fem.getGroupElements1D().getEntity(*it)->get();
			for(auto it=sid.begin(); it!=sid.end(); ++it) {
				Element1D const& elem = fem.getElement1D(*it);
				addElement1D(elem);
			}
		}
	}
	else {
        // for the envelop, make sure we use the same nodes indices as the one defined in the piper entity
        piper::hbm::VId const& nodes = entity.getEnvelopNodes(fem);
        for (auto it=nodes.begin(); it!=nodes.end(); ++it)
            addNode(fem.getNode(*it));

        std::vector<ElemDef> const& env2d = entity.getEnvelopElements(fem);

		// add element 2D
		for (auto it=env2d.begin(); it!=env2d.end(); ++it) {
			Element2D elem;
			elem.set( *it);
            addElement2D(elem, fem, false);
		}
	}
}

void PiperMeshLoader::loadEnvironment(std::string const& environmentName)
{
    piper::hbm::FEModel const& envModel = sofaPiper::project().environment().getEnvironmentModel(environmentName)->envmodel();
    const piper::hbm::Coord& translation = envModel.getTranslation();
    d_translation.setValue(sofa::defaulttype::Vector3(translation[0],translation[1],translation[2]));
    const piper::hbm::Coord& rotation = envModel.getRotation();
    d_rotation.setValue(sofa::defaulttype::Vector3(rotation[0],rotation[1],rotation[2]));
    const piper::hbm::Coord& scale = envModel.getScale();
    d_scale.setValue(sofa::defaulttype::Vector3(scale[0],scale[1],scale[2]));

    for (auto const& idElem : envModel.getElements2D().getContainer())
		addElement2D((*idElem), envModel, true);

}

void PiperMeshLoader::loadLandmark(std::string const& landMarkName){
    sout << "loadLandmark(): " << landMarkName << sendl;
    FEModel const& fem = sofaPiper::project().model().fem();
    if(sofaPiper::project().model().metadata().hasLandmark(landMarkName)) {
        Landmark const& landmark = sofaPiper::project().model().metadata().landmark(landMarkName);
        for (auto it=landmark.node.begin(); it!=landmark.node.end(); ++it)
            addNode(fem.getNode(*it));
        for (auto it1 = landmark.get_groupNode().begin(); it1 != landmark.get_groupNode().end(); ++it1) {
            VId currentGroupNode = fem.getGroupNode(*it1).get();
            for (VId::const_iterator it2=currentGroupNode.begin(); it2!=currentGroupNode.end(); ++it2)
                addNode(fem.getNode(*it2));
        }
    }
}

void PiperMeshLoader::loadModel()
{
    sout << "loadModel()" << sendl;
    FEModel const& fem = sofaPiper::project().model().fem();

    Elements2D const& elements = fem.getElements2D();
    for (auto it=elements.getContainer().begin(); it!=elements.getContainer().end(); ++it)
        addElement2D(*(*it), fem);

    Elements3D const& elements3 = fem.getElements3D();
    for (auto it=elements3.getContainer().begin(); it!=elements3.getContainer().end(); ++it)
		addElement3D(*(*it));

    Elements1D const& elements1 = fem.getElements1D();
    for (auto it=elements1.getContainer().begin(); it!=elements1.getContainer().end(); ++it)
		addElement1D(*(*it));
  
}

void PiperMeshLoader::loadAllNodes()
{
    sout << "loadAllNodes()" << sendl;
    FEModel const& fem = sofaPiper::project().model().fem();

    if (m_editPosition!=nullptr && m_editPositionsId!=nullptr){
        for (auto it = fem.getNodes().begin(); it!=fem.getNodes().end(); ++it)
            addNode(*(*it));
    }

}


bool PiperMeshLoader::load()
{
    m_editTriangles = d_triangles.beginEdit();
    m_editQuads = d_quads.beginEdit();
    m_editTetra = d_tetrahedra.beginEdit();
    m_editHexa = d_hexahedra.beginEdit();
    m_editPenta = d_pentahedra.beginEdit();
    m_editPyramids = d_pyramids.beginEdit();
    m_editBar = d_edges.beginEdit();
    m_editPosition =  d_positions.beginEdit();
    m_editPositionsId =  d_positionsId.beginEdit();
    
    m_editTriangles->clear();
    m_editQuads->clear();
    m_editHexa->clear();
    m_editTetra->clear();
    m_editPenta->clear();
    m_editPyramids->clear();
    m_editBar->clear();
    m_editPosition->clear();
    m_editPositionsId->clear();

    m_piperIdToIndex.clear();

    if (d_entityNames.isSet())
        for (std::string const& name: d_entityNames.getValue())
            loadEntity(name, d_envelopOnly.getValue());
    else if (d_environmentName.isSet())
        loadEnvironment(d_environmentName.getValue());
    else if (d_landmarkName.isSet())
        loadLandmark(d_landmarkName.getValue());
    else if (d_loadModel.getValue())
        loadModel();
    else if (d_loadAllNodes.getValue())
        loadAllNodes();
    else
        serr << "load(): nothing to load" << sendl;

    d_triangles.endEdit();
    d_quads.endEdit();
    d_tetrahedra.endEdit();
    d_hexahedra.endEdit();
    d_pentahedra.endEdit();
    d_pyramids.endEdit();
    d_edges.endEdit();

    d_positions.endEdit();
    d_positionsId.endEdit();

    m_piperIdToIndex.clear(); // to free memory

    return true;
}

} // namespace loader

} // namespace component

} // namespace sofa
