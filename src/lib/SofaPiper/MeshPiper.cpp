/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include <hbm/FEModel.h>
#include "MeshPiper.h"


namespace sofa {
namespace helper {
namespace io {

using namespace sofa::defaulttype;
using namespace piper::hbm;

MeshPiper::MeshPiper(piper::hbm::HumanBodyModel const& model, std::string const& entityName)
    : m_lastNodeIndex(0)
    , m_model(model)
    , m_entityName(entityName)
{
    FEModel const& fem = model.fem();
	Entity const& entity = model.metadata().entity(entityName);

    std::vector<ElemDef> const& env2d = entity.getEnvelopElements(fem);

	// add element 2D
	for (auto it=env2d.begin(); it!=env2d.end(); ++it) {
		Element2D elem;
		elem.set( *it);
		addElement(elem);
	}
}

void MeshPiper::addNode(Node const& node)
{
    if (m_piperIdToIndex.count(node.getId()) == 0) {
        vertices.push_back(Vector3(node.getCoordX(), node.getCoordY(), node.getCoordZ()));
        m_piperIdToIndex.insert(std::make_pair(node.getId(), m_lastNodeIndex));
        ++m_lastNodeIndex;
    }
}

void MeshPiper::addElement(Element2D const& elem)
{
    vector< vector<int> > vertNormTexIndices;
    vector<int> vIndices, nIndices, tIndices;

    FEModel const& fem = m_model.fem();
    ElemDef const& elemDef = elem.get();

    // add element nodes
    for (auto it = elemDef.begin(); it!=elemDef.end(); ++it)
        addNode(fem.getNode(*it));

    // add element
    switch(elem.getType()) {
    case ElementType::ELT_TRI:
        vIndices.push_back(m_piperIdToIndex[elemDef[0]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[1]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[2]]);
        vertNormTexIndices.push_back(vIndices);
        vertNormTexIndices.push_back(nIndices);
        vertNormTexIndices.push_back(tIndices);
        facets.push_back(vertNormTexIndices);
        break;
	case ElementType::ELT_TRI_THICK:
        vIndices.push_back(m_piperIdToIndex[elemDef[0]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[1]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[2]]);
        vertNormTexIndices.push_back(vIndices);
        vertNormTexIndices.push_back(nIndices);
        vertNormTexIndices.push_back(tIndices);
        facets.push_back(vertNormTexIndices);
        break;
    case ElementType::ELT_QUAD:
        vIndices.push_back(m_piperIdToIndex[elemDef[0]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[1]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[2]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[3]]);
        vertNormTexIndices.push_back(vIndices);
        vertNormTexIndices.push_back(nIndices);
        vertNormTexIndices.push_back(tIndices);
        facets.push_back(vertNormTexIndices);
        break;
	case ElementType::ELT_QUAD_THICK:
        vIndices.push_back(m_piperIdToIndex[elemDef[0]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[1]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[2]]);
        vIndices.push_back(m_piperIdToIndex[elemDef[3]]);
        vertNormTexIndices.push_back(vIndices);
        vertNormTexIndices.push_back(nIndices);
        vertNormTexIndices.push_back(tIndices);
        facets.push_back(vertNormTexIndices);
        break;
    default:
        std::cerr << "ElementType not handled: " << (int)elem.getType() << std::endl;
        break;
    }
}

}
}
}
