/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_LOADER_MESHPIPERLOADER_H
#define SOFA_COMPONENT_LOADER_MESHPIPERLOADER_H

#include <string>

#include "initPlugin.h"

#pragma warning(push, 0)
#include <sofa/core/loader/MeshLoader.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/topology/Topology.h>
#include <sofa/helper/map.h>
#include <sofa/defaulttype/Vec3Types.h>
#pragma warning(pop)
#include <hbm/Node.h>
#include <hbm/Element.h>

namespace sofa
{

namespace component
{

namespace loader
{

/** This component loads geometry from the piper model.
 * It can load from:
 * - an entity
 * - a model
 * It loads the following geometry elements:
 * - nodes
 * - triangles
 * - quads
 * \warning filename data is ignored
 * \warning this component is designed to be used in the Piper application
 * \author Thomas Lemaire \date 2015
 */
class SOFA_SofaPiper_API PiperMeshLoader : public virtual sofa::core::loader::MeshLoader
{
public:
    SOFA_CLASS(PiperMeshLoader, sofa::core::loader::MeshLoader);

    typedef helper::vector<sofa::defaulttype::Vector3> VecPosition;
    typedef unsigned int Id;
    typedef helper::vector<Id> VecId;

protected:
    PiperMeshLoader();
    virtual ~PiperMeshLoader() {}

    void loadEntity(std::string const& entityName, bool envelopOnly=false);
    void loadEnvironment(std::string const& environmentName);
    void loadCtrlPt(std::string const& ctrlPtName);
    void loadLandmark(std::string const& landMarkName);
    void loadModel();
    void loadAllNodes();

public:

    Data<bool> d_loadModel;
    Data<bool> d_envelopOnly;
    Data<bool> d_loadAllNodes;

    /// name of the entity to be loaded
    Data< helper::vector<std::string> > d_entityNames;

    /// name of environment to be loaded
    Data<std::string> d_environmentName;

    Data<std::string> d_landmarkName;

    /// ids of the positions
    Data<VecId> d_positionsId;

    virtual bool canLoad();

    virtual bool load();
    
protected:

    /// append \param node to the mesh
    void addNode(piper::hbm::Node const& node);
    /// add \param elem to the mesh, if \param doAddNodes is true, also add its nodes
    void addElement1D(piper::hbm::Element1D const& elem, bool doAddNodes=true);
    void addElement2D(piper::hbm::Element2D const& elem, piper::hbm::FEModel const& fem, bool doAddNodes=true);
    void addElement3D(piper::hbm::Element3D const& elem, bool doAddNodes=true);

    unsigned int m_lastNodeIndex; ///< last inserted node index
    std::map<piper::hbm::Id, unsigned int> m_piperIdToIndex; ///< map to convert node piper id to their index in the entity mesh

    mutable VecPosition *m_editPosition;
    mutable VecId *m_editPositionsId;
         
    helper::vector<Triangle > *m_editTriangles;
    helper::vector<Quad > *m_editQuads;
    helper::vector<Tetrahedron > *m_editTetra;
    helper::vector<Hexahedron > *m_editHexa;
    helper::vector<Pentahedron > *m_editPenta;
    helper::vector<Pyramid > *m_editPyramids;
    helper::vector<Edge > *m_editBar;

};

} // namespace loader

} // namespace component

} // namespace sofa

#endif
