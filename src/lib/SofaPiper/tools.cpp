/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "tools.h"

#include <exception>
#include <sstream>

#include "hbm/HumanBodyModel.h"
#include "hbm/FEModel.h"
#include "hbm/Metadata.h"

namespace sofaPiper {

std::vector<unsigned int> getEntityNodesIndex(std::string const& entityName, piper::hbm::Id groupId)
{
    // get the necessary data
    piper::hbm::HumanBodyModel const& model = project().model();
    piper::hbm::Entity const& entity = model.metadata().entity(entityName);

    piper::hbm::VId const& nodesId = model.fem().getGroupNode(groupId).get();
    std::map<piper::hbm::Id, unsigned int> const& nodesIdToIndex = entity.getEnvelopNodesToIndexMap(model.fem());

    // fill the index vector
    std::vector<unsigned int> nodesIndex;
    nodesIndex.reserve(nodesId.size());
    for (auto it=nodesId.begin(); it!=nodesId.end(); ++it) {
        std::map<piper::hbm::Id, unsigned int>::const_iterator indexIt = nodesIdToIndex.find(*it);
        if (indexIt == nodesIdToIndex.end()) {
            std::stringstream ss;
            ss << "Missing node " << *it << " in the envelope of entity " << entityName << ", included in group " << groupId;
            throw std::runtime_error(ss.str());
        }
        nodesIndex.push_back(indexIt->second);
    }
    return nodesIndex;
}

static piper::Project* _project=nullptr;
void setProject(piper::Project & project)
{
    _project = & project;
}
piper::Project & project()
{
    return *_project;
}

}

