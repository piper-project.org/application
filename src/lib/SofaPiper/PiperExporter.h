/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 beta 4      *
*                (c) 2006-2009 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_LOADER_PIPEREXPORTER_H
#define SOFA_COMPONENT_LOADER_PIPEREXPORTER_H

#include <string>
#pragma warning(push, 0)
#include <sofa/core/objectmodel/BaseObject.h>
#pragma warning(pop)

namespace sofa
{

namespace component
{

namespace misc
{

/** This component export the positions to the piper model.
 * @warning this component is designed to be used in the Piper application
 * @author Thomas Lemaire @date 2015
 */
template<class DataTypes>
class PiperExporter : public virtual core::objectmodel::BaseObject
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(PiperExporter, DataTypes), core::objectmodel::BaseObject);

    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef unsigned int Id;
    typedef helper::vector<Id> VecId;

    PiperExporter();
    virtual ~PiperExporter() {}

    void updatePiperModel();

    void handleEvent(sofa::core::objectmodel::Event *event);

    virtual std::string getTemplateName() const;
    static std::string templateName(const PiperExporter<DataTypes> * = NULL);

    Data<VecCoord> d_positions; ///< positions to be exported
    Data<VecId> d_positionsId;  ///< ids of the positions
    Data<std::string> d_guiEventName; ///< gui event name to react to

};

} // namespace misc

} // namespace component

} // namespace sofa

#endif
