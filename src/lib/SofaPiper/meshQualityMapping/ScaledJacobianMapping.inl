/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_SCALEDJACOBIANMAPPING_INL
#define SOFA_COMPONENT_MAPPING_SCALEDJACOBIANMAPPING_INL

#include "ScaledJacobianMapping.h"
#include <sofa/core/visual/VisualParams.h>

#include <sofa/core/Mapping.inl>

//#include <sofa/simulation/common/Simulation.h>

#include <sofa/defaulttype/VecTypes.h>
#include <sofa/defaulttype/RigidTypes.h>


#include <sofa/helper/gl/template.h>

#include <string>
#include <iostream>
#include <math.h>

//#include <vtkverdict/verdict.h>

namespace sofa
{

namespace component
{

namespace mapping
{

using namespace sofa::defaulttype;
using namespace sofa::core::topology;

template <class TIn, class TOut>
ScaledJacobianMapping<TIn, TOut>::ScaledJacobianMapping()
    : Inherit()
//    , _extPiperMeshLoaderLnk (initLink("extPiperMeshLoaderPath", "Path to a pipermeshloader component"))
	, qualityMetric ( initData ( &qualityMetric,"qualityMetric","mesh quality metric" ) )
	, elementKind ( initData ( &elementKind,"elementKind","kind of element to evaluate" ) )
    , elementKindStr ( initData ( &elementKindStr,"elementKindStr","kind of element to evaluate" ) )
    ,piperMeshLoaderPath ( initData (&piperMeshLoaderPath, "extPiperMeshLoaderPath", "Path to a pipermeshloader component"))
    , _extPiperMeshLoaderPtr(nullptr)
    
{
    metricOptions=helper::OptionsGroup(2,"0 - Scale Jacobian","1 - Shape and Size");
    elementsOptions=helper::OptionsGroup(5,"0 - TetraHedron","1 - HexaHedron","2 - Triangle","3 - Quad","4 - Tri2");

    metricOptions.setSelectedItem(0);
    qualityMetric.setValue(metricOptions);

    elementsOptions.setSelectedItem(0);
    elementKind.setValue(elementsOptions);
    
//BaseMechanicalState *dofFrom = static_cast< simulation::Node* >(this->fromModel->getContext())->mechanicalState;
//BaseMechanicalState *dofTo = static_cast< simulation::Node* >(this->toModel->getContext())->mechanicalState;
}


template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::init()
{
    if(elementKindStr.isSet()){
        std::string elemKind(elementKindStr.getValue());
        std::transform(elemKind.begin(), elemKind.end(), elemKind.begin(), ::tolower);
        if(elemKind.compare(0,8,"triangle")==0){
            selectedElementType=TRI_Elem;
        }
        else if (elemKind.compare(0,4,"quad")==0){
            selectedElementType=QUAD_Elem;
        }
        else if (elemKind.compare(0,9,"tetrahedr")==0){
            selectedElementType=TETRA_Elem;
        }
        else if (elemKind.compare(0,8,"hexahedr")==0){
            selectedElementType=HEXA_Elem;
        }
        elementsOptions.setSelectedItem(selectedElementType);
    }
    else{
	    selectedElementType = elementKind.getValue().getSelectedId();
    }
	selectedQuality = qualityMetric.getValue().getSelectedId();
	int nbelem=0;
	topology_from = this->getContext()->getMeshTopology();
    //_extPiperMeshLoaderPtr = _extPiperMeshLoaderLnk.get();
    if(piperMeshLoaderPath.isSet() && !piperMeshLoaderPath.getValue().empty()){
    this->getContext()->getRootContext()->get(_extPiperMeshLoaderPtr,piperMeshLoaderPath.getValue());
    if(_extPiperMeshLoaderPtr==nullptr)
        this->getContext()->get(_extPiperMeshLoaderPtr,piperMeshLoaderPath.getValue());
    }

    if(_extPiperMeshLoaderPtr == nullptr) {
	    if(!topology_from) {
            serr<<"No piperMeshLoaderPtr found !"<<sendl;
            serr<<"No MeshTopology found ! "<<sendl; return;}
    }

	switch(selectedElementType){
	case TETRA_Elem : {
		nbelem=(_extPiperMeshLoaderPtr==nullptr)?topology_from->getNbTetrahedra():_extPiperMeshLoaderPtr->getNbTetrahedra();
		if(nbelem==0) sout<<"No TetraHedron found ! "<<sendl;
			break;
		   }
	case HEXA_Elem : {
		nbelem=(_extPiperMeshLoaderPtr==nullptr)?topology_from->getNbHexahedra():_extPiperMeshLoaderPtr->getNbHexahedra();
		if(nbelem==0) sout<<"No HexaHedron found ! "<<sendl;
			break;
		   }
	case TRI_Elem : {
		nbelem=(_extPiperMeshLoaderPtr==nullptr)?topology_from->getNbTriangles():_extPiperMeshLoaderPtr->getNbTriangles();
		if(nbelem==0) sout<<"No Triangle found ! "<<sendl;
			break;
		   }
	case QUAD_Elem : {
		nbelem=(_extPiperMeshLoaderPtr==nullptr)?topology_from->getNbQuads():_extPiperMeshLoaderPtr->getNbQuads();
		if(nbelem==0) sout<<"No Quad found ! "<<sendl;
			break;
		   }
	case 4 : {
		nbelem=(_extPiperMeshLoaderPtr==nullptr)?topology_from->getNbTriangles():_extPiperMeshLoaderPtr->getNbTriangles();
		if(nbelem==0) sout<<"No Triangle found ! "<<sendl;
			break;
		   }

	}
	this->getToModel()->resize(nbelem);
    if(_extPiperMeshLoaderPtr==nullptr){
        jacobian.resizeBlocks(nbelem,this->getFromModel()->read(sofa::core::ConstVecCoordId::position())->getValue().size()); 
    }
    else 
        jacobian.resizeBlocks(nbelem,_extPiperMeshLoaderPtr->getPosition()->size()); 
	
    baseMatrices.resize( 1 );
    baseMatrices[0] = &jacobian;

    this->Inherit::init();
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::apply ( const core::MechanicalParams* /*mparams*/, Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{ 
    if(topology_from||_extPiperMeshLoaderPtr){
   switch(selectedElementType){
	case TETRA_Elem : {
			switch(selectedQuality){
			case SCALE_JACOBIAN: {
				applyTetScaleJacob(dOut,dIn);
				break;}
			case SHAPE_N_SIZE: {
				break;}
			}
			break;
		   }
	case HEXA_Elem : {
			switch(selectedQuality){
			case SCALE_JACOBIAN: {
				applyHexScaleJacob(dOut,dIn);
				break;}
			case SHAPE_N_SIZE: {
				break;}
			}
			break;
		   }
	case TRI_Elem : {
			switch(selectedQuality){
			case SCALE_JACOBIAN: {
				applyTriScaleJacob(dOut,dIn);
				break;}
			case SHAPE_N_SIZE: {
				break;}
			}
			break;
		   }
	case QUAD_Elem : {
			switch(selectedQuality){
			case SCALE_JACOBIAN: {
                applyQuadScaleJacob(dOut,dIn);
				break;}
			case SHAPE_N_SIZE: {
				break;}
			}
			break;
		   }
	 case 4 : {
			switch(selectedQuality){
			case SCALE_JACOBIAN: {
				applyTriScaleJacob2(dOut,dIn);
				break;}
			case SHAPE_N_SIZE: {
				break;}
			}
			break;
		   }

	}
    }
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyTetScaleJacob ( Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{
    const sofa::core::topology::BaseMeshTopology::SeqTetrahedra* tetrahedra;
    if(_extPiperMeshLoaderPtr==nullptr) 
        tetrahedra = &topology_from->getTetrahedra();
    else
        tetrahedra = &_extPiperMeshLoaderPtr->getTetrahedra();
	helper::WriteAccessor< Data<OutVecCoord> >  q = dOut;
    
    const InVecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr){
        helper::ReadAccessor< Data<InVecCoord> >  readx = dIn;
        x = &readx.ref();
    }
    else 
        x = (const InVecCoord*)_extPiperMeshLoaderPtr->getPosition();

	InCoord L0,L1,L2,L3,L4,L5;
	Real lambdaSq1,lambdaSq2,lambdaSq3,lambdaSq4,lambdaSqMax,tetJacob,lambdaMax;
	q.resize(tetrahedra->size());

	static const double root_of_2 = sqrt(2.0);

    for (unsigned int t=0; t<tetrahedra->size(); t++ )
    {
		L0 =(*x)[tetrahedra->at(t).at(1)] -(*x)[tetrahedra->at(t).at(0)] ;
		L1 =(*x)[tetrahedra->at(t).at(2)] -(*x)[tetrahedra->at(t).at(1)] ;
		L2 =(*x)[tetrahedra->at(t).at(0)] -(*x)[tetrahedra->at(t).at(2)] ;
		L3 =(*x)[tetrahedra->at(t).at(3)] -(*x)[tetrahedra->at(t).at(0)] ;
		L4 =(*x)[tetrahedra->at(t).at(3)] -(*x)[tetrahedra->at(t).at(1)] ;
		L5 =(*x)[tetrahedra->at(t).at(3)] -(*x)[tetrahedra->at(t).at(2)] ;

		lambdaSq1 = L0.norm2() * L2.norm2() * L3.norm2();
		lambdaSq2 = L0.norm2() * L1.norm2() * L4.norm2();
		lambdaSq3 = L1.norm2() * L2.norm2() * L5.norm2();
		lambdaSq4 = L3.norm2() * L4.norm2() * L5.norm2();

		lambdaSqMax = std::max(lambdaSq1,std::max(lambdaSq2,std::max(lambdaSq3,lambdaSq4)));

		tetJacob = L2.cross(L0) * L3;

		lambdaMax = std::max (sqrt(lambdaSqMax), fabs(tetJacob));

		if (lambdaMax < VERDICT_DBL_MIN ) 
			q[t] = VERDICT_DBL_MAX ; 
		else
			q[t] = tetJacob * root_of_2 / lambdaMax ; 
		
	}
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyHexScaleJacob ( Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{
	const sofa::core::topology::BaseMeshTopology::SeqHexahedra* hexahedra ;
    if(_extPiperMeshLoaderPtr==nullptr) 
        hexahedra  = &topology_from->getHexahedra();
    else
        hexahedra = &_extPiperMeshLoaderPtr->getHexahedra();

	helper::WriteAccessor< Data<OutVecCoord> >  q = dOut;
    const InVecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr){
        helper::ReadAccessor< Data<InVecCoord> >  readx = dIn;
         x = &readx.ref();
    }
    else 
        x = (const InVecCoord*)_extPiperMeshLoaderPtr->getPosition();

	InCoord nL0,nL1,nL2,nL3,nL4,nL5,nL6,nL7,nL8,nL9,nL10,nL11,nX1,nX2,nX3;
	Real nSqMin,nSq0,nSq1,nSq2,nSq3,nSq4,nSq5,nSq6,nSq7,nSq8,nSq9,nSq10,nSq11,nAlpha0,nAlpha1,nAlpha2,nAlpha3,nAlpha4,nAlpha5,nAlpha6,nAlpha7,nAlpha8;
	q.resize(hexahedra->size());

    for (unsigned int h=0; h<hexahedra->size(); h++ )
    {
		nL0 =(*x)[hexahedra->at(h).at(1)] -(*x)[hexahedra->at(h).at(0)] ;
		nL1 =(*x)[hexahedra->at(h).at(2)] -(*x)[hexahedra->at(h).at(1)] ;
		nL2 =(*x)[hexahedra->at(h).at(3)] -(*x)[hexahedra->at(h).at(2)] ;
		nL3 =(*x)[hexahedra->at(h).at(3)] -(*x)[hexahedra->at(h).at(0)] ;
		nL4 =(*x)[hexahedra->at(h).at(4)] -(*x)[hexahedra->at(h).at(0)] ;
		nL5 =(*x)[hexahedra->at(h).at(5)] -(*x)[hexahedra->at(h).at(1)] ;
		nL6 =(*x)[hexahedra->at(h).at(6)] -(*x)[hexahedra->at(h).at(2)] ;
		nL7 =(*x)[hexahedra->at(h).at(7)] -(*x)[hexahedra->at(h).at(3)] ;
		nL8 =(*x)[hexahedra->at(h).at(5)] -(*x)[hexahedra->at(h).at(4)] ;
		nL9 =(*x)[hexahedra->at(h).at(6)] -(*x)[hexahedra->at(h).at(5)] ;
		nL10 =(*x)[hexahedra->at(h).at(7)] -(*x)[hexahedra->at(h).at(6)] ;
		nL11 =(*x)[hexahedra->at(h).at(7)] -(*x)[hexahedra->at(h).at(4)] ;

		nSq0=nL0.norm2();nSq1=nL1.norm2();nSq2=nL2.norm2();nSq3=nL3.norm2();
		nSq4=nL4.norm2();nSq5=nL5.norm2();nSq6=nL6.norm2();nSq7=nL7.norm2();
		nSq8=nL8.norm2();nSq9=nL9.norm2();nSq10=nL10.norm2();nSq11=nL11.norm2();

		nSqMin=std::min(nSq0,nSq1);nSqMin=std::min(nSqMin,nSq2);nSqMin=std::min(nSqMin,nSq3);nSqMin=std::min(nSqMin,nSq4);
		nSqMin=std::min(nSqMin,nSq5);nSqMin=std::min(nSqMin,nSq6);nSqMin=std::min(nSqMin,nSq7);nSqMin=std::min(nSqMin,nSq8);
		nSqMin=std::min(nSqMin,nSq9);nSqMin=std::min(nSqMin,nSq10);nSqMin=std::min(nSqMin,nSq11);

		if(nSqMin<=VERDICT_DBL_MIN)
			q[h]=VERDICT_DBL_MAX;
		else
			{
		nL0.normalizeWithNorm(sqrt(nSq0)); nL1.normalizeWithNorm(sqrt(nSq1)); nL2.normalizeWithNorm(sqrt(nSq2)); nL3.normalizeWithNorm(sqrt(nSq3)); 
		nL4.normalizeWithNorm(sqrt(nSq4)); nL5.normalizeWithNorm(sqrt(nSq5)); nL6.normalizeWithNorm(sqrt(nSq6)); nL7.normalizeWithNorm(sqrt(nSq7)); 
		nL8.normalizeWithNorm(sqrt(nSq8)); nL9.normalizeWithNorm(sqrt(nSq9)); nL10.normalizeWithNorm(sqrt(nSq10)); nL11.normalizeWithNorm(sqrt(nSq11)); 

		nX1 =(*x)[hexahedra->at(h).at(1)] -(*x)[hexahedra->at(h).at(0)]
				+(*x)[hexahedra->at(h).at(2)] -(*x)[hexahedra->at(h).at(3)] 
					+(*x)[hexahedra->at(h).at(5)] -(*x)[hexahedra->at(h).at(4)]
						+(*x)[hexahedra->at(h).at(6)] -(*x)[hexahedra->at(h).at(7)];

		nX2 =(*x)[hexahedra->at(h).at(3)] -(*x)[hexahedra->at(h).at(0)]
				+(*x)[hexahedra->at(h).at(2)] -(*x)[hexahedra->at(h).at(1)] 
					+(*x)[hexahedra->at(h).at(7)] -(*x)[hexahedra->at(h).at(4)]
						+(*x)[hexahedra->at(h).at(6)] -(*x)[hexahedra->at(h).at(5)];
				
		nX3 =(*x)[hexahedra->at(h).at(4)] -(*x)[hexahedra->at(h).at(0)]
				+(*x)[hexahedra->at(h).at(5)] -(*x)[hexahedra->at(h).at(1)] 
					+(*x)[hexahedra->at(h).at(6)] -(*x)[hexahedra->at(h).at(2)]
						+(*x)[hexahedra->at(h).at(7)] -(*x)[hexahedra->at(h).at(3)];
		
		nX1.normalize(); nX2.normalize(); nX3.normalize();

		nAlpha0 = (nL0) * ((nL3).cross(nL4));
		nAlpha1 = (nL1) * ((-nL0).cross(nL5));
		nAlpha2 = (nL2) * ((-nL1).cross(nL6));
		nAlpha3 = (-nL3) * ((-nL2).cross(nL7));
		nAlpha4 = (nL11) * ((nL8).cross(-nL4));
		nAlpha5 = (-nL8) * ((nL9).cross(-nL5));
		nAlpha6 = (-nL9) * ((nL10).cross(-nL6));
		nAlpha7 = (-nL10) * ((-nL11).cross(-nL7));
		nAlpha8 = (nX1) * ((nX2).cross(nX3));

		q[h][0] = std::min(nAlpha0,nAlpha1); q[h] = std::min(q[h][0],nAlpha2); q[h] = std::min(q[h][0],nAlpha3); q[h] = std::min(q[h][0],nAlpha4);
		q[h][0] = std::min(q[h][0],nAlpha5); q[h] = std::min(q[h][0],nAlpha6); q[h] = std::min(q[h][0],nAlpha7); q[h] = std::min(q[h][0],nAlpha8);

			}

	}
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyTriScaleJacob ( Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{
    const sofa::core::topology::BaseMeshTopology::SeqTriangles* triangles ;
    if(_extPiperMeshLoaderPtr==nullptr) 
        triangles  = &topology_from->getTriangles();
    else
        triangles = &_extPiperMeshLoaderPtr->getTriangles();

	helper::WriteAccessor< Data<OutVecCoord> >  q = dOut;
    const InVecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr){
        helper::ReadAccessor< Data<InVecCoord> >  readx = dIn;
         x = &readx.ref();
    }
    else 
        x = (const InVecCoord*)_extPiperMeshLoaderPtr->getPosition();

	InCoord L0,L1,L2,edgeMax,edgeMin,edgeMed;
	Real jacob,nSqMax,nSqMed,nSqMin,nSq0,nSq1,nSq2,LMax;

	static const double detw = 2.0*sqrt(3.0)/3.0;

	q.resize(triangles->size());

    for (unsigned int t=0; t<triangles->size(); t++ )
    {
		L0 =(*x)[triangles->at(t).at(2)] -(*x)[triangles->at(t).at(1)] ;
		L1 =(*x)[triangles->at(t).at(0)] -(*x)[triangles->at(t).at(2)] ;
		L2 =(*x)[triangles->at(t).at(1)] -(*x)[triangles->at(t).at(0)] ;

		nSq0 = L0.norm2(); nSq1 = L1.norm2(); nSq2 = L2.norm2(); 
		
		if(nSq0>nSq1) {
			nSqMax=nSq0; nSqMin=nSq1;
			edgeMax=L0; edgeMin=L1;}
		else {
			nSqMax=nSq1; nSqMin=nSq0;
			edgeMax=L1; edgeMin=L0;}
		if(nSq2>nSqMax){
			nSqMed=nSqMax; nSqMax=nSq2;
			edgeMed=edgeMax; edgeMax=L2;}
		else{
			if(nSq2>nSqMin) {
				nSqMed=nSq2;
				edgeMed=L2;}
			else {
				nSqMed=nSqMin; nSqMin=nSq2;
				edgeMed=edgeMin; edgeMin=L2;}
			}
		jacob = (edgeMed.cross(edgeMin)).norm();
		LMax=sqrt(nSqMax*nSqMed); 
		if(LMax<VERDICT_DBL_MIN)
			q[t][0]=0;
		else
			q[t][0] = detw * jacob / LMax;
	}
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyTriScaleJacob2 ( Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{
    const sofa::core::topology::BaseMeshTopology::SeqTriangles* triangles ;
    if(_extPiperMeshLoaderPtr==nullptr) 
        triangles  = &topology_from->getTriangles();
    else
        triangles = &_extPiperMeshLoaderPtr->getTriangles();

	helper::WriteAccessor< Data<OutVecCoord> >  q = dOut;
    const InVecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr){
        helper::ReadAccessor< Data<InVecCoord> >  readx = dIn;
         x = &readx.ref();
    }
    else 
        x = (const InVecCoord*)_extPiperMeshLoaderPtr->getPosition();

	InCoord L0,L1,L2,edgeMax,edgeMin,edgeMed;
	Block j1,j2,j3;

	Real jacob,nSqMax,nSqMed,nSqMin,nSq0,nSq1,nSq2,LMax;

	static const double detw = 2.0*sqrt(3.0)/3.0; // calcul en double dans computeJacobian

	q.resize(triangles->size());
	jacobian.resizeBlocks(triangles->size(),x->size()); 

    for (unsigned int t=0; t<triangles->size(); t++ )
    {
		L0 =(*x)[triangles->at(t).at(2)] -(*x)[triangles->at(t).at(1)] ;
		L1 =(*x)[triangles->at(t).at(0)] -(*x)[triangles->at(t).at(2)] ;
		L2 =(*x)[triangles->at(t).at(1)] -(*x)[triangles->at(t).at(0)] ;

		nSq0 = L0.norm2(); nSq1 = L1.norm2(); nSq2 = L2.norm2(); 
		
		if(nSq0>nSq1) {
			nSqMax=nSq0; nSqMin=nSq1;
			edgeMax=L0; edgeMin=L1;}
		else {
			nSqMax=nSq1; nSqMin=nSq0;
			edgeMax=L1; edgeMin=L0;}
		if(nSq2>nSqMax){
			nSqMed=nSqMax; nSqMax=nSq2;
			edgeMed=edgeMax; edgeMax=L2;}
		else{
			if(nSq2>nSqMin) {
				nSqMed=nSq2;
				edgeMed=L2;}
			else {
				nSqMed=nSqMin; nSqMin=nSq2;
				edgeMed=edgeMin; edgeMin=L2;}
			}
		jacob = (edgeMed.cross(edgeMin)).norm();
		LMax=sqrt(nSqMax*nSqMed);
		
		if(LMax<VERDICT_DBL_MIN)
			q[t][0]=0;
		else
			q[t][0] = detw * jacob / LMax;


		computeTriScaledJacobMetricJacobian(edgeMax,edgeMed,edgeMin,j1,j2,j3);

		//Real test1 = 0.0;
		//Real test2 = 0.0;
		//Real test3 = 0.0;
		//Real machin;
		//test1 = j1[0][0] *(*x)[triangles->at(t).at(0)][0] + j1[0][1]  *(*x)[triangles->at(t).at(0)][1] +j1[0][2]  *(*x)[triangles->at(t).at(0)][2];
		//test2 = j2[0][0]  *(*x)[triangles->at(t).at(1)][0] + j2[0][1]  *(*x)[triangles->at(t).at(1)][1] +j2[0][2]  *(*x)[triangles->at(t).at(1)][2];
		//test3 = j3[0][0]  *(*x)[triangles->at(t).at(2)][0] + j3[0][1]  *(*x)[triangles->at(t).at(2)][1] +j3[0][2]  *(*x)[triangles->at(t).at(2)][2];
		//
		//machin = test1+test2+test3;

		//serr<<q[t][0]<<"  :  "<<machin<<sendl;
		//
		//

		jacobian.beginBlockRow(t);
        jacobian.createBlock( triangles->at(t).at(0), j1);
        jacobian.createBlock( triangles->at(t).at(1), j2);
        jacobian.createBlock( triangles->at(t).at(2), j3);
        jacobian.endBlockRow();



	}
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::computeTriScaledJacobMetricJacobian (const InCoord& Emax, const InCoord& Emed, const InCoord& Emin, Block& j1, Block& j2, Block& j3)
{ 
	Real t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,t31,t32,t33,t34,t35,t36,t37,t38 ;
	Real t13Sqrt,t18Sqrt, t13xt18Sqrt, t8Sqrt,t2xt4xt6;

	t1 = sqrt(0.3e1);

	t2 =  ( Emed[1] *  Emin[2] -  Emed[2] *  Emin[1]);
	t3 = fabs(t2);
	t4 =  ( Emed[0] *  Emin[2] -  Emed[2] *  Emin[0]);
	t5 = fabs(t4);
	t6 =  ( Emed[0] *  Emin[1] -  Emed[1] *  Emin[0]);
	t7 = fabs(t6);
	t2xt4xt6 = t2 * t4 * t6;

	t8 =  pow( t3, (Real) 2) +  pow( t5,  (Real)2) +  pow( t7,  (Real)2);
	t8Sqrt = pow(t8, (Real)(0.1e1 / 0.2e1));
	t9 = pow( t8Sqrt, (Real)-0.1e1 );
	t8 =  ( t8 * t9);

	t10 = fabs(Emax[0]);
	t11 = fabs(Emax[1]);
	t12 = fabs(Emax[2]);
	t13 =  pow( t10,  (Real)2) +  pow( t11, (Real) 2) +  pow( t12,  (Real)2);
	t13Sqrt = pow(t13, (Real)(0.1e1 / 0.2e1));
	t14 = pow( t13Sqrt, (Real)-0.3e1 );

	t15 = fabs(Emed[0]);
	t16 = fabs(Emed[1]);
	t17 = fabs(Emed[2]);
	t18 =  pow( t15,  (Real)2) +  pow( t16,  (Real)2) +  pow( t17,  (Real)2);
	t18Sqrt = pow(t18, (Real)(0.1e1 / 0.2e1 ));
	t19 = pow( t18Sqrt, (Real)-0.3e1 );

	t13xt18Sqrt=t13Sqrt*t18Sqrt;
	
	t18 =  ( t18 * t19);
	t6 =  ( t7 * fabs( t6) /  t6);
	t4 =  ( t5 * fabs( t4) /  t4);
	t5 =  (t9 *  t18);
	t7 =  ( t8 * t19);
	t9 = t1 *  t13 * t14;
	t2 =  ( t3 * fabs( t2) /  t2);
	t3 =  (0.2e1 / 0.3e1 *  t5 * t9);
	t1 = 0.2e1 / 0.3e1 * t1 *  t8 * t14 *  t18;


	if(t13xt18Sqrt>=VERDICT_DBL_MIN && t8Sqrt>=VERDICT_DBL_MIN && t13xt18Sqrt*t8Sqrt>=VERDICT_DBL_MIN ){
		j1[0][0] = (Emax[0]!=0.0) ? -t1 *  t10 * fabs( Emax[0]) /  Emax[0] : 0.0;
		j1[0][1] = (Emax[1]!=0.0) ? -t1 *  t11 * fabs( Emax[1]) /  Emax[1] : 0.0; 
		j1[0][2] = (Emax[2]!=0.0) ? -t1 *  t12 * fabs( Emax[2]) /  Emax[2] : 0.0;

		if(t2xt4xt6!=0.0){
			j2[0][0] = (Emed[0]!=0.0) ? 0.2e1 / 0.3e1 * t9 * (- t7 *  t15 * fabs( Emed[0]) /  Emed[0] +  (t5 * (t4 * Emin[2] + t6 * Emin[1]))) : 0.0; 
			j2[0][1] = (Emed[1]!=0.0) ? 0.2e1 / 0.3e1 * t9 * ( (t5 * (t2 * Emin[2] - t6 * Emin[0])) -  t7 *  t16 * fabs( Emed[1]) /  Emed[1]) : 0.0;
			j2[0][2] = (Emed[2]!=0.0) ? -0.2e1 / 0.3e1 * t9 * ( (t5 * (t2 * Emin[1] + t4 * Emin[0])) +  t7 *  t17 * fabs( Emed[2]) /  Emed[2]) : 0.0;

			j3[0][0] = -t3 * (t4 * Emed[2] + t6 * Emed[1]);
			j3[0][1] = -t3 * (t2 * Emed[2] - t6 * Emed[0]);
			j3[0][2] =  t3 * (t2 * Emed[1] + t4 * Emed[0]);
			}
	}
	//serr<<"jcalc " <<(Real)(j1[0][0])<<" , "<<(Real)j1[0][1]<<" , "<<(Real)j1[0][2]<<" , "<<(Real)j2[0][0]<<" , "<<(Real)j2[0][1]<<" , "<<(Real)j2[0][2]<<" , "<<(Real)j3[0][0]<<" , "<<(Real)j3[0][1]<<" , "<<(Real)j3[0][2]<<sendl;
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyQuadScaleJacob ( Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn)
{
    const sofa::core::topology::BaseMeshTopology::SeqQuads* quads ;
    if(_extPiperMeshLoaderPtr==nullptr) 
        quads  = &topology_from->getQuads();
    else
        quads = &_extPiperMeshLoaderPtr->getQuads();

	helper::WriteAccessor< Data<OutVecCoord> >  q = dOut;
    const InVecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr){
        helper::ReadAccessor< Data<InVecCoord> >  readx = dIn;
         x = &readx.ref();
    }
    else 
        x = (const InVecCoord*)_extPiperMeshLoaderPtr->getPosition();

	InCoord L0,L1,L2,L3,cN0,cN1,cN2,cN3,cNc,X1,X2;

	Real nSqMin,nSq0,nSq1,nSq2,nSq3,Alpha0,Alpha1,Alpha2,Alpha3,nAlpha0,nAlpha1,nAlpha2,nAlpha3;

	q.resize(quads->size());

    for (unsigned int quadId=0; quadId<quads->size(); quadId++ )
    {
        L0 =(*x)[quads->at(quadId).at(1)] -(*x)[quads->at(quadId).at(0)] ;
		L1 =(*x)[quads->at(quadId).at(2)] -(*x)[quads->at(quadId).at(1)] ;
		L2 =(*x)[quads->at(quadId).at(3)] -(*x)[quads->at(quadId).at(2)] ;
		L3 =(*x)[quads->at(quadId).at(0)] -(*x)[quads->at(quadId).at(3)] ;
		    
		nSq0=L0.norm2();nSq1=L1.norm2();nSq2=L2.norm2();nSq3=L3.norm2();
		
		nSqMin=std::min(nSq0,nSq1);nSqMin=std::min(nSqMin,nSq2);nSqMin=std::min(nSqMin,nSq3);

        if(nSqMin<VERDICT_DBL_MIN)
            q[quadId][0]=0;
        else{
        
            cN0=L3.cross(L0); 
            cN1=L0.cross(L1); 
            cN2=L1.cross(L2); 
            cN3=L2.cross(L3); 

            X1 = L0 - L2; 
            X2 = L1 - L3;

            cNc = X1.cross(X2); 
            cNc.normalize();

            Alpha0 = cNc * cN0; 
            Alpha1 = cNc * cN1; 
            Alpha2 = cNc * cN2; 
            Alpha3 = cNc * cN3;

            nAlpha0 = Alpha0 / sqrt(nSq0 * nSq3);
            nAlpha1 = Alpha1 / sqrt(nSq1 * nSq0);
            nAlpha2 = Alpha2 / sqrt(nSq2 * nSq1);
            nAlpha3 = Alpha3 / sqrt(nSq3 * nSq2);

            q[quadId][0] = std::min ( std::min ( std::min ( nAlpha0, nAlpha1), nAlpha2), nAlpha3);
        }
    }

}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyTetShapeNSize ( Data<OutVecCoord>& /*dOut*/, const Data<InVecCoord>& /*dIn*/)
{
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyHexShapeNSize ( Data<OutVecCoord>& /*dOut*/, const Data<InVecCoord>& /*dIn*/)
{
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyTriShapeNSize ( Data<OutVecCoord>& /*dOut*/, const Data<InVecCoord>& /*dIn*/)
{
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyQuadShapeNSize ( Data<OutVecCoord>& /*dOut*/, const Data<InVecCoord>& /*dIn*/)
{
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyJ ( const core::MechanicalParams* /*mparams*/, Data<OutVecDeriv>& dOut, const Data<InVecDeriv>& dIn )
{    
    if(topology_from || _extPiperMeshLoaderPtr){

	/*
    	const sofa::core::topology::BaseMeshTopology::SeqTriangles& triangles = topology_from->getTriangles();
		helper::WriteAccessor< Data<OutVecDeriv> >  q = dOut;
  

	q.resize(tetrahedra->size()); */
	if(selectedElementType==4){
			if(selectedQuality==SCALE_JACOBIAN){
				    if( jacobian.rowSize() > 0 )
						jacobian.mult(dOut,dIn);
			}
    }
	}
}


template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::applyJT (const core::MechanicalParams* /*mparams*/, Data<InVecDeriv>& dIn, const Data<OutVecDeriv>& dOut )
{
        if(topology_from || _extPiperMeshLoaderPtr){

 //  	const sofa::core::topology::BaseMeshTopology::SeqTriangles& triangles = topology_from->getTriangles();
	//helper::WriteAccessor< Data<InVecDeriv> >  q = dIn;
 //  
	//q.resize(tetrahedra->size());
	if(selectedElementType==4){
			if(selectedQuality==SCALE_JACOBIAN){
		        jacobian.addMultTranspose(dIn,dOut);
				}
	}
        }
}


template <class TIn, class TOut>
const sofa::defaulttype::BaseMatrix* ScaledJacobianMapping<TIn, TOut>::getJ()
{
    return &jacobian;
}

template <class TIn, class TOut>
const sofa::helper::vector<sofa::defaulttype::BaseMatrix*>* ScaledJacobianMapping<TIn, TOut>::getJs()
{
    return &baseMatrices;
}

template <class TIn, class TOut>
void ScaledJacobianMapping<TIn, TOut>::draw(const core::visual::VisualParams* vparams)
{
    /*
    const typename In::VecCoord &X = this->fromModel->read(sofa::core::ConstVecCoordId::position())->getValue();

    std::vector< Vector3 > points;
    Vector3 point1,point2;
    for(unsigned int i=0 ; i<In::Coord::spatial_dimensions ; i++)
    {
        InCoord v;
        v[i] = (Real)0.1;
        point1 = InDataTypes::getCPos((X[0] -v));
        point2 = InDataTypes::getCPos((X[0] +v));
        points.push_back(point1);
        points.push_back(point2);
    }

    vparams->drawTool()->drawLines(points, 1, Vec<4,float>(0,1,0,1));
    */
}


} // namespace mapping

} // namespace component

} // namespace sofa

#endif
