/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_MAPPING_SCALEDJACOBIANMAPPING_H
#define SOFA_COMPONENT_MAPPING_SCALEDJACOBIANMAPPING_H


#define VERDICT_DBL_MIN 1.0E-30
#define VERDICT_DBL_MAX 1.0E+30


#include <sofa/core/Mapping.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <SofaEigen2Solver/EigenSparseMatrix.h>
#include <sofa/defaulttype/Mat.h>
#include <sofa/defaulttype/Vec.h>
#include <sofa/core/behavior/BaseMass.h>
#include <sofa/defaulttype/RigidTypes.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/helper/OptionsGroup.h>
#include "../PiperMeshLoader.h"

namespace sofa
{

namespace component
{

namespace mapping
{

using namespace sofa::core::behavior;
using namespace sofa::core::topology;

template <class TIn, class TOut>
class ScaledJacobianMapping : public core::Mapping<TIn, TOut>
{
private :
	enum ElementType {TETRA_Elem,HEXA_Elem,TRI_Elem,QUAD_Elem};
	enum QualityMetric {SCALE_JACOBIAN,SHAPE_N_SIZE};
public:
    SOFA_CLASS(SOFA_TEMPLATE2(ScaledJacobianMapping,TIn,TOut), SOFA_TEMPLATE2(core::Mapping,TIn,TOut));

    typedef core::Mapping<TIn, TOut> Inherit;
    typedef TIn In;
    typedef TOut Out;
    typedef Out OutDataTypes;
    typedef typename Out::Coord OutCoord;
    typedef typename Out::Deriv OutDeriv;
	typedef typename Out::VecCoord OutVecCoord;
    typedef typename Out::VecDeriv OutVecDeriv;
    typedef typename Out::MatrixDeriv OutMatrixDeriv;
    typedef In InDataTypes;
    typedef typename In::Coord InCoord;
    typedef typename In::Deriv InDeriv;
	typedef typename In::VecCoord InVecCoord;
    typedef typename In::VecDeriv InVecDeriv;
    typedef typename In::MatrixDeriv InMatrixDeriv;
    typedef typename OutCoord::value_type Real;

	//from flexible triangledeformationmapping
	typedef linearsolver::EigenSparseMatrix<TIn,TOut>    SparseMatrixEigen;
    enum {Nin = In::deriv_total_size, Nout = Out::deriv_total_size };
    typedef defaulttype::Mat<In::deriv_total_size, In::deriv_total_size,Real>  InBlock;
    typedef defaulttype::Mat<Out::deriv_total_size, In::deriv_total_size,Real>  Block;
//    typedef topology::TriangleSetTopologyContainer::SeqTriangles SeqTriangles;




    void init(); 

	virtual void apply(const core::MechanicalParams* /*mparams*/, Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyJ(const core::MechanicalParams* /*mparams*/, Data<OutVecDeriv>& dOut, const Data<InVecDeriv>& dIn); //{    }
    virtual void applyJT(const core::MechanicalParams* /*mparams*/, Data<InVecDeriv>& dIn, const Data<OutVecDeriv>& dOut);// {     }
	virtual void applyJT(const core::ConstraintParams* /*mparams*/,Data<InMatrixDeriv>& /*dIn*/, const Data<OutMatrixDeriv>& /*dOut*/) {}

	virtual const sofa::defaulttype::BaseMatrix* getJ();
    virtual const sofa::helper::vector<sofa::defaulttype::BaseMatrix*>* getJs();

	

    void draw(const core::visual::VisualParams* vparams);


protected :
    ScaledJacobianMapping ( ) ;

    virtual ~ScaledJacobianMapping()
    {}

	virtual void applyTetScaleJacob(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyHexScaleJacob(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyTriScaleJacob(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyTriScaleJacob2(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyQuadScaleJacob(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);

	virtual void computeTriScaledJacobMetricJacobian (const InCoord& Emax, const InCoord& Emed, const InCoord& Emin, Block& j1, Block& j2, Block& j3);

	virtual void applyTetShapeNSize(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyHexShapeNSize(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyTriShapeNSize(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);
	virtual void applyQuadShapeNSize(Data<OutVecCoord>& dOut, const Data<InVecCoord>& dIn);


	int selectedQuality, selectedElementType;
	
	SparseMatrixEigen jacobian;                         ///< Jacobian of the mapping
    sofa::helper::vector<defaulttype::BaseMatrix*> baseMatrices;      ///< Jacobian of the mapping, in a vector

    Data< std::string > elementKindStr;
	Data< helper::OptionsGroup > qualityMetric;
	Data< helper::OptionsGroup > elementKind;
    Data <std::string> piperMeshLoaderPath;

    helper::OptionsGroup metricOptions;
    helper::OptionsGroup elementsOptions;
    sofa::core::topology::BaseMeshTopology* topology_from;
    sofa::core::topology::BaseMeshTopology* topology_to;
    sofa::component::loader::PiperMeshLoader* _extPiperMeshLoaderPtr;

    //SingleLink<ScaledJacobianMapping<TIn,TOut>, sofa::component::loader::PiperMeshLoader, BaseLink::FLAG_STOREPATH|BaseLink:: FLAG_STRONGLINK> _extPiperMeshLoaderLnk; //|BaseLink:: FLAG_STRONGLINK ??

};

using namespace sofa::defaulttype;
#if defined(SOFA_EXTERN_TEMPLATE) && !defined(SOFA_COMPONENT_MAPPING_SCALEDJACOBIANMAPPING_CPP)
#ifndef SOFA_FLOAT
extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3dTypes, Vec1dTypes >;
extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3dTypes, ExtVec1dTypes >;
#endif
#ifndef SOFA_DOUBLE
//extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3fTypes, Vec1fTypes >;
//extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3fTypes, ExtVec1fTypes >;
#endif

#ifndef SOFA_FLOAT
#ifndef SOFA_DOUBLE
extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3dTypes, Vec1fTypes >;
extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3dTypes, ExtVec1fTypes >;
//extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3fTypes, Vec1dTypes >;
//extern template class SOFA_SofaPiper_API ScaledJacobianMapping< Vec3fTypes, ExtVec1dTypes >;
#endif
#endif
#endif

} // namespace mapping

} // namespace component

} // namespace sofa

#endif
