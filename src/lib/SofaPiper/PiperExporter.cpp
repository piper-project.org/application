/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
 
#include "PiperExporter.inl"
#pragma warning(push, 0)
#include <sofa/core/ObjectFactory.h>
#include <sofa/defaulttype/VecTypes.h>
#pragma warning(pop)

namespace sofa {
namespace component {
namespace misc {

using namespace sofa::defaulttype;

SOFA_DECL_CLASS(PiperExporter)

int PiperExporterClass = core::RegisterObject("Export nodes position to the Piper model")
#ifndef SOFA_FLOAT
        .add< PiperExporter<Vec3dTypes> >(true)
#endif
        .add< PiperExporter<Vec3fTypes> >()
        ;
#ifndef SOFA_FLOAT
template class SOFA_SofaPiper_API PiperExporter<Vec3dTypes>;
#endif
template class SOFA_SofaPiper_API PiperExporter<Vec3fTypes>;

} // namespace misc
} // namespace component
} // namespace sofa
