/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                              SOFA :: Framework                              *
*                                                                             *
* Authors: The SOFA Team (see Authors.txt)                                    *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_HELPER_IO_MESHPIPER_H
#define SOFA_HELPER_IO_MESHPIPER_H

#pragma warning(push, 0)
#include <sofa/helper/io/Mesh.h>
#include <sofa/helper/helper.h>
#pragma warning(pop)

#include <hbm/Node.h>
#include <hbm/Element.h>
#include <hbm/HumanBodyModel.h>

namespace sofa {
namespace helper {
namespace io {

/** A sofa::helper::io::Mesh for a piper Entity. To be used with generateRigid()
 * @warning: only the 2D envelop of the entity is loaded.
 */
class MeshPiper : public Mesh
{
public:

    MeshPiper(piper::hbm::HumanBodyModel const& model, std::string const& entityName);

private:

    piper::hbm::HumanBodyModel const& m_model;
    std::string m_entityName;
    unsigned int m_lastNodeIndex; ///< last inserted node index
    std::map<piper::hbm::Id, unsigned int> m_piperIdToIndex; ///< map to convert node piper id to their index in the entity mesh

    void addNode(piper::hbm::Node const& node);
    void addElement(piper::hbm::Element2D const& elem);

};

} // namespace io

} // namespace helper

} // namespace sofa

#endif
