/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Plugins                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef INITSofaPiper_H
#define INITSofaPiper_H

#pragma warning(push, 0)
#include <sofa/helper/system/config.h>
#pragma warning(pop)

#ifdef SOFA_BUILD_SOFAPIPER
#define SOFA_SofaPiper_API SOFA_EXPORT_DYNAMIC_LIBRARY
#else
#define SOFA_SofaPiper_API  SOFA_IMPORT_DYNAMIC_LIBRARY
#endif

/** \mainpage
  This module provides components to do the link between the piper application and a sofa simulation.
  */

#endif // INITSofaPiper_H
