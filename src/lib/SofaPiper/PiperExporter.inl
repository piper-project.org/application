/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 beta 4      *
*                (c) 2006-2009 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "tools.h"
#include "PiperExporter.h"

#include <hbm/HumanBodyModel.h>
#include <hbm/FEModel.h>
#include <hbm/Node.h>

#pragma warning(push, 0)
#include <sofa/core/objectmodel/GUIEvent.h>
#pragma warning(pop)

namespace sofa {
namespace component {
namespace misc {


template<class DataTypes>
PiperExporter<DataTypes>::PiperExporter()
    : d_positions(initData(&d_positions, "positions", "positions to be exported"))
    , d_positionsId(initData(&d_positionsId, "positionsId", "ids of the positions"))
    , d_guiEventName(initData(&d_guiEventName, std::string("piperExport"), "guiEventName", "gui event name to react to"))
{
    this->f_listening.setValue(true);
}

template <class DataTypes>
std::string PiperExporter<DataTypes>::getTemplateName() const
{
    return templateName(this);
}

template <class DataTypes>
std::string PiperExporter<DataTypes>::templateName(const PiperExporter<DataTypes>*)
{
    return DataTypes::Name();
}

template<class DataTypes>
void PiperExporter<DataTypes>::updatePiperModel()
{
    piper::hbm::FEModel& fem = sofaPiper::project().model().fem();
    helper::ReadAccessor< Data<VecCoord> > positions(d_positions);
    helper::ReadAccessor< Data<VecId> > positionsId(d_positionsId);
    if (positions.size() != positionsId.size())
        serr << "Size of positions and positionsId does not match" << sendl;
    auto itP=positions.begin();
    auto itPId=positionsId.begin();
    while (itP != positions.end() && itPId != positionsId.end()) {
        piper::hbm::Node& node = fem.getNode(*itPId);
        node.getCoordX() = (*itP)[0];
        node.getCoordY() = (*itP)[1];
        node.getCoordZ() = (*itP)[2];
        ++itP; ++itPId;
    }
    sout << "(" << this->name.getValue() << ") Piper model updated" << sendl;
}

template<class DataTypes>
void PiperExporter<DataTypes>::handleEvent(sofa::core::objectmodel::Event *event)
{
    helper::ReadAccessor< Data<std::string> > guiEventName(d_guiEventName);
    if (sofa::core::objectmodel::GUIEvent * guiEvent = dynamic_cast<sofa::core::objectmodel::GUIEvent*>(event)) {
        if (guiEvent->getValueName().compare(guiEventName) == 0)
            updatePiperModel();
    }
}

} // namespace misc
} // namespace component
} // namespace sofa

