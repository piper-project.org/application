/*******************************************************************************
* Copyright (C) 2017 INRIA                                                     *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Thomas Lemaire (INRIA)                                  *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_SOFAPIPER_GENERATERIGID_H
#define PIPER_SOFAPIPER_GENERATERIGID_H

#include <string>
#include <vector>

#include "initPlugin.h"

#include "hbm/HumanBodyModel.h"

namespace sofaPiper {

/** This method wrap the sofa::helper::generateRigid() function
 * It returns [mass, com_x, com_y, com_z, I_xx, I_xy, I_xz, I_yy, I_yz, I_zz]
 */
SOFA_SofaPiper_API std::vector<double> generateRigid(piper::hbm::HumanBodyModel const& model, std::string const& entityName, double density);

}

#endif
