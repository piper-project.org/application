%module sofa

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "std_string.i"
%include "std_vector.i"

%{
#include "hbm/types.h"
#include "hbm/HumanBodyModel.h"
#include "generateRigid.h"
#include "tools.h"
%}

%exception {
  try {
    $action
  } catch (const std::runtime_error& e) {
    SWIG_exception(SWIG_RuntimeError,const_cast<char*>(e.what()));
  }
}

namespace std {
%template(Vecd) std::vector<double>;
%template(Vecui) std::vector<unsigned int>;
}

namespace piper {
namespace hbm {
    typedef unsigned int Id;
}
}

namespace sofaPiper {

std::vector<double> generateRigid(piper::hbm::HumanBodyModel const& model, std::string const& entityName, double density);

std::vector<unsigned int> getEntityNodesIndex(std::string const& entityName, piper::hbm::Id groupId);

piper::Project & project();

}
