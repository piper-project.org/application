/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#if defined(WIN32) && (_MSC_VER < 1800) // for all version anterior to Visual Studio 2013
# include <float.h>
# define isnan(x)  (_isnan(x))
#else
# include <cmath>
# define isnan(x) (std::isnan(x))
#endif

#include "DataDisplayTopo.h"

#pragma warning(push, 0)
#include <sofa/core/ObjectFactory.h>
#include <SofaBaseTopology/TriangleSetTopologyContainer.h>
#pragma warning(pop)

namespace sofa
{

namespace component
{

namespace visualmodel
{

using namespace sofa::defaulttype;
using sofa::helper::ColorMap;

SOFA_DECL_CLASS(DataDisplayTopo)

int DataDisplayTopoClass = core::RegisterObject("Rendering of meshes colored by data")
        .add< DataDisplayTopo >()
        ;

void DataDisplayTopo::init()
{
    if(piperMeshLoaderPath.isSet() && !piperMeshLoaderPath.getValue().empty()){
        this->getContext()->getRootContext()->get(_extPiperMeshLoaderPtr,piperMeshLoaderPath.getValue());
        if(_extPiperMeshLoaderPtr==nullptr)
            this->getContext()->get(_extPiperMeshLoaderPtr,piperMeshLoaderPath.getValue());
    }

    if(_extPiperMeshLoaderPtr == nullptr) {
	     topology = this->getContext()->getMeshTopology();
        if (!topology) {
            serr<<"No piperMeshLoaderPtr found ! ( "<<piperMeshLoaderPath.getValue()<<" )" <<sendl;
            serr << "No topology information, drawing just points." << sendl;
        }
    }

    if(extElementKindStr.isSet()){
        std::string elemKind(extElementKindStr.getValue());
        std::transform(elemKind.begin(), elemKind.end(), elemKind.begin(), ::tolower);
        if(elemKind.compare(0,8,"triangle")==0){
            selectedElementType=ElementType::TRI_Elem;
        }
        else if (elemKind.compare(0,4,"quad")==0){
            selectedElementType=ElementType::QUAD_Elem;
        }
        else if (elemKind.compare(0,9,"tetrahedr")==0){
            selectedElementType=ElementType::TETRA_Elem;
        }
        else if (elemKind.compare(0,8,"hexahedr")==0){
            selectedElementType=ElementType::HEXA_Elem;
        }
    }

    triangleTopo = dynamic_cast<topology::TriangleSetTopologyContainer*>(topology);
    tetraTopo = dynamic_cast<topology::TetrahedronSetTopologyContainer*>(topology);
    hexaTopo = dynamic_cast<topology::HexahedronSetTopologyContainer*>(topology);
    quadTopo = dynamic_cast<topology::QuadSetTopologyContainer*>(topology);
    
/*    sout<<" / found : "<<sendl;
    
    if(tetraTopo)
        sout<<"\t"<<tetraTopo->getNbTetras()<<" tetras"<<sendl;
    if(hexaTopo)
        sout<<"\t"<<hexaTopo->getNbHexas()<<" hexas"<<sendl;
    if(triangleTopo)
        sout<<"\t"<<triangleTopo->getNbTriangles()<<" triangles "<<sendl;
     if(quadTopo)
        sout<<"\t"<<quadTopo->getNbQuads()<<" quads"<<sendl;
        */
    this->getContext()->get(colorMap);
	if (!colorMap) {
        sout << "No ColorMap found, using default." << sendl;
        colorMap = ColorMap::getDefault();
    }
    colorMap1=ColorMap::getDefault();
	colorMap2=ColorMap::getDefault();
	colorMap3=ColorMap::getDefault();

	if(f_hasSuppColorMap.getValue()){
	colorMap1=colorMap;
	if(!f_colorMapName2.getValue().empty())
		this->getContext()->get(colorMap2,f_colorMapName2.getValue());
	if(!f_colorMapName3.getValue().empty())
		this->getContext()->get(colorMap3,f_colorMapName3.getValue());
	if(!colorMap2)
		colorMap2=ColorMap::getDefault();
	if(!colorMap3)
		colorMap3=ColorMap::getDefault();
	}

    updateMinMax();

    if(!f_showPositionsInMenu.getValue()){
		f_cellData.setDisplayed(false);
		m_positions.setDisplayed(false);
        m_restPositions.setDisplayed(false);
        m_vnormals.setDisplayed(false);
    }

    f_hasAlpha.setGroup("Visualization");
    f_hasSuppColorMap.setGroup("Visualization");
    f_displayRange1.setGroup("Visualization");
    f_displayRange2.setGroup("Visualization");
    f_displayRange3.setGroup("Visualization");
    f_drawNormal.setGroup("Visualization");
    f_normalSize.setGroup("Visualization");
    f_drawCullBackFace.setGroup("Visualization");
    
    createGlLocalBuffer();
}

DataDisplayTopo::~DataDisplayTopo(){
    if((tetraTopo || hexaTopo || quadTopo || triangleTopo || ( _extPiperMeshLoaderPtr && selectedElementType!=ElementType::NONE) ) && nbTopoElem)
        if(vertices!=nullptr) {delete [] vertices;}
        if(normals!=nullptr) {delete [] normals;}
        if(colors!=nullptr) {delete [] colors;}
}

void DataDisplayTopo::clearVisual(){
    if(vboId!=0 && drawWithVBO)
        glDeleteBuffersARB(1, &vboId); //TODOThomasD : use clearVisual
}

void DataDisplayTopo::initVisual(){
    drawWithVBO = CanUseGlExtension( "GL_ARB_vertex_buffer_object" );

    if(vboId==0 && drawWithVBO)
        glGenBuffersARB(1, &vboId);
}


void DataDisplayTopo::updateVisual()
{
        createGlLocalBuffer();
        computeNormals();
        computeCellDataToDraw();
}

void DataDisplayTopo::createGlLocalBuffer()
{
    int oldNbTopoElem = nbTopoElem;
    nbTopoElem=0;
    if(tetraTopo || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TETRA_Elem) ){
        nbTopoElem = (_extPiperMeshLoaderPtr==nullptr)?tetraTopo->getNbTetrahedra():_extPiperMeshLoaderPtr->getNbTetrahedra();
        if(nbTopoElem){
            nbVertices=nbTopoElem*3*4*3;
            nbNormals=nbTopoElem*3*4*3;
            nbColors=nbTopoElem*3*4*4;
            normalCenter.reserve(nbTopoElem*4);
        }
    }
    else if(hexaTopo|| (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::HEXA_Elem)){
        nbTopoElem = (_extPiperMeshLoaderPtr==nullptr)?hexaTopo->getNbHexas():_extPiperMeshLoaderPtr->getNbHexahedra();
        if(nbTopoElem){
            nbVertices=nbTopoElem*3*12*3;
            nbNormals=nbTopoElem*3*12*3;
            nbColors=nbTopoElem*3*12*4;
            normalCenter.reserve(nbTopoElem*6);
        }
    }
    else if ((triangleTopo && !tetraTopo) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TRI_Elem) ){
        nbTopoElem= (_extPiperMeshLoaderPtr==nullptr)?triangleTopo->getNbTriangles():_extPiperMeshLoaderPtr->getNbTriangles();
        if(nbTopoElem){
            nbVertices=nbTopoElem*3*3;
            nbNormals=nbTopoElem*3*3;
            nbColors=nbTopoElem*3*4;
            normalCenter.reserve(nbTopoElem);
        }
    }
    else if ((quadTopo && !hexaTopo) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::QUAD_Elem)){
        nbTopoElem = (_extPiperMeshLoaderPtr==nullptr)?quadTopo->getNbQuads():_extPiperMeshLoaderPtr->getNbQuads();
        if(nbTopoElem){
            nbVertices=nbTopoElem*3*2*3;
            nbNormals=nbTopoElem*3*2*3;
            nbColors=nbTopoElem*3*2*4;
            normalCenter.reserve(nbTopoElem);
        }
    }

    if(nbTopoElem != oldNbTopoElem)
    {
        if(vertices!=nullptr) {delete [] vertices;}
        if(normals!=nullptr) {delete [] normals;}
        if(colors!=nullptr) {delete [] colors;}
    }

    if(vertices==nullptr) { vertices = new GLfloat[nbVertices];}
    if(normals==nullptr) { normals = new GLfloat[nbNormals];}
    if(colors==nullptr) { colors = new GLfloat[nbColors];}
    verticesPtr=vertices; normalsPtr=normals; colorsPtr=colors; 
}

void DataDisplayTopo::computeCellDataToDraw(){
    const VecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr)
        x = &(this->read(sofa::core::ConstVecCoordId::position())->getValue());
    else
        x = &_extPiperMeshLoaderPtr->getExtVertices();
    const VecPointData &ptData = f_pointData.getValue();
    const VecCellData &clData = f_cellData.getValue();

    bDrawPointData = false;
    bDrawCellData = false;

	// Safety checks
    if (ptData.size() > 0) {
        if (ptData.size() != x->size()) {
            serr << "Size of pointData does not mach number of nodes" << sendl;
        } else {
            bDrawPointData = true;
        }
    } else if (clData.size() > 0) {
        if ((!_extPiperMeshLoaderPtr && (!topology || (!triangleTopo && !tetraTopo && !quadTopo && !hexaTopo))) ||  (!topology && (_extPiperMeshLoaderPtr==nullptr || selectedElementType==ElementType::NONE) )) {
            sout << "Triangular, tetrahedron, quad or hexahedron settopology is necessary for drawing cell data" << sendl;
		} else if ((tetraTopo && (int)clData.size() != tetraTopo->getNbTetrahedra()) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TETRA_Elem && (int)clData.size() != _extPiperMeshLoaderPtr->getNbTetrahedra())) {
			sout << "Size of cellData ( "<<clData.size()<<" ) does not match number of tetra( "<<((_extPiperMeshLoaderPtr==nullptr)?tetraTopo->getNbTetrahedra():_extPiperMeshLoaderPtr->getNbTetrahedra())<<" )"  << sendl;
        } else if ((hexaTopo && (int)clData.size() != hexaTopo->getNbHexas()) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::HEXA_Elem && (int)clData.size() != _extPiperMeshLoaderPtr->getNbHexahedra())) {
            sout << "Size of cellData ( "<<clData.size()<<" ) does not match number of hexa ( "<<((_extPiperMeshLoaderPtr==nullptr)?hexaTopo->getNbHexas():_extPiperMeshLoaderPtr->getNbHexahedra())<<" )"  << sendl;
        } else if ((!tetraTopo && triangleTopo && (int)clData.size() != triangleTopo->getNbTriangles()) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TRI_Elem && (int)clData.size() != _extPiperMeshLoaderPtr->getNbTriangles()) ){
			sout << "Size of cellData ( "<<clData.size()<<" ) does not match number of triangles ( "<<((_extPiperMeshLoaderPtr==nullptr)?triangleTopo->getNbTriangles():_extPiperMeshLoaderPtr->getNbTriangles())<<" )" << sendl;
        } else if ((!hexaTopo && quadTopo && (int)clData.size() != quadTopo->getNbQuads()) || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::QUAD_Elem && (int)clData.size() != _extPiperMeshLoaderPtr->getNbQuads()) ){
            sout << "Size of cellData ( "<<clData.size()<<" ) does not match number of quads ( "<<((_extPiperMeshLoaderPtr==nullptr)?quadTopo->getNbQuads():_extPiperMeshLoaderPtr->getNbQuads())<<" )" << sendl;
        } else {
            bDrawCellData = true;
        }
    }

    //udate range values
    updateMinMax();
	if(!f_hasUserRange.getValue()){
	    // Range for points
	    min=0.0; max=0.0;	
        if (bDrawPointData) {
            VecPointData::const_iterator i = ptData.begin();
            min = *i;
            max = *i;
            while (++i != ptData.end()) {
                if (min > *i) min = *i;
                if (max < *i) max = *i;
            }
        }
        // Range for cells
        if (bDrawCellData) {
            VecCellData::const_iterator i = clData.begin();
            min = *i;
            max = *i;
            while (++i != clData.end()) {
                if (min > *i) min = *i;
                if (max < *i) max = *i;
            }
        }
        if (max > oldMax) oldMax = max;
        if (min < oldMin) oldMin = min;

        if (f_maximalRange.getValue()) {
            max = oldMax;
            min = oldMin;
        }
	}

    //Prepare TOPO vertex + index + color arrays 
    if (bDrawCellData) {
        ColorMap::evaluator<Real> eval = colorMap->getEvaluator(min, max);
        ColorMap::evaluator<Real> eval1 = colorMap1->getEvaluator(minmed, maxmed);
        ColorMap::evaluator<Real> eval2 = colorMap2->getEvaluator(min, minmed);
        ColorMap::evaluator<Real> eval3 = colorMap3->getEvaluator(maxmed, max);
        if(f_drawNormal.getValue()){
            normalCenter.clear();
            normalColor[0] = 1.0f; normalColor[1] = 0.0f; normalColor[2] = 0.0f; normalColor[3] = 1.0f ;
        }
           
        //Triangles
		if((!tetraTopo && triangleTopo  && nbTopoElem==triangleTopo->getNbTriangles())
            ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TRI_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbTriangles())){ 
            nbTriToDraw=0;  

            int nbIndex=0;
            int nbColorIndex=0;
            for (int i=0; i<nbTopoElem; i++)
            {
                Vec4f color;
                updateColor(eval,eval1,eval2,eval3,clData[i],color);
                Triangle t ;
                if(_extPiperMeshLoaderPtr==nullptr)
                    t = triangleTopo->getTriangle(i);
                else
                    t = _extPiperMeshLoaderPtr->getTriangles()[i];
                if((f_displayRange1.getValue() && clData[i]<minmed) || (f_displayRange3.getValue() && clData[i]>maxmed) || (f_displayRange2.getValue() && clData[i]>=minmed && clData[i]<=maxmed) ){
                    nbIndex = 3*3*nbTriToDraw;
                    nbColorIndex = 3*4*nbTriToDraw;
                    for(int m=0;m<3;m++){
                        for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*m+n] = (*x)[t[m]][n];
                        }
                    }
                    for(int m=0 ; m<3 ; m++){
                        for(int n=0; n<4;n++)
                            colors[nbColorIndex+4*m+n] = color[n]; 
                        for(int n=0 ; n<3 ; n++)
                            normals[nbIndex+3*m+n] = m_normals[ i ][n];
                    }

                    if(f_drawNormal.getValue()){
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[2] ] + (*x)[ t[1]])/3), i)); 
                    }

                    nbTriToDraw++;
                }
            }

            if(vboId!=0 && drawWithVBO){
                verticesPtr=0; 
                normalsPtr=(GLfloat*)(nbVertices*sizeof(GL_FLOAT)); 
                colorsPtr=(GLfloat*)((nbVertices+nbNormals)*sizeof(GL_FLOAT));  //TODOThomasD : potentiellement ici il faut adapter pour drawrangeelem
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
                glBufferDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals+nbColors)*sizeof(GL_FLOAT), 0, GL_STATIC_DRAW_ARB);
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, nbVertices*sizeof(GL_FLOAT), vertices);                             // copy vertices starting from 0 offest
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, nbVertices*sizeof(GL_FLOAT), nbNormals*sizeof(GL_FLOAT), normals);                // copy normals after vertices
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals)*sizeof(GL_FLOAT), nbColors*sizeof(GL_FLOAT), colors);  // copy colours after normals
                delete [] vertices; delete []normals; delete []colors;
                vertices=nullptr; normals=nullptr; colors=nullptr; 
                //TODOThomas : selon si on utilise draw range elem ou autre : on peut potentiellement faire un delete sur vertices / normals / colors
            }
		}

        //Tetra
		else if ((tetraTopo && nbTopoElem==tetraTopo->getNbTetrahedra())
            ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TETRA_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbTetrahedra())){ 
            nbTetraToDraw=0;

            int nbIndex=0;
            int nbColorIndex=0;
            for (int i=0; i<nbTopoElem; ++i)
            {
                Vec4f color;
                updateColor(eval,eval1,eval2,eval3,clData[i],color);
                if((f_displayRange1.getValue() && clData[i]<minmed) || (f_displayRange3.getValue() && clData[i]>maxmed) || (f_displayRange2.getValue() && clData[i]>=minmed && clData[i]<=maxmed) ){
                    Tetra t ;
                    if(_extPiperMeshLoaderPtr==nullptr)
                        t= tetraTopo->getTetrahedron(i);
                    else
                        t = _extPiperMeshLoaderPtr->getTetrahedra()[i];
                    nbIndex = 3*4*3*nbTetraToDraw;
                    nbColorIndex = 3*4*4*nbTetraToDraw;

                    for(int k=0 ; k<4 ; k++){
                        for(int m=0 ; m<3 ; m++){
                            for(int n=0; n<4 ; n++)
                                colors[nbColorIndex+(3*k+m)*4+n] = color[n]; 
                            for(int n=0 ; n<3 ; n++)
                                normals[nbIndex+(3*k+m)*3+n] = m_normals[ 4*i+k ][n];
                        }
                    }
                    for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*0+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*1+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*2+n] = (*x)[t[1]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*3+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*4+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*5+n] = (*x)[t[3]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*6+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*7+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*8+n] = (*x)[t[3]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*9+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*10+n] = (*x)[t[3]][n];
                        vertices[nbIndex+3*11+n] = (*x)[t[2]][n];
                    }


                    if(f_drawNormal.getValue()){
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[2] ] + (*x)[ t[1]])/3), 4*i + 0));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[1] ] + (*x)[ t[3]])/3), 4*i + 1));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[1] ] + (*x)[ t[2] ] + (*x)[ t[3]])/3), 4*i + 2));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[3] ] + (*x)[ t[2]])/3), 4*i + 3));
                    }
                    nbTetraToDraw++;
                }
            }
            if(vboId!=0 && drawWithVBO){
                verticesPtr=0; 
                normalsPtr=(GLfloat*)(nbVertices*sizeof(GL_FLOAT)); 
                colorsPtr=(GLfloat*)((nbVertices+nbNormals)*sizeof(GL_FLOAT));  //TODOThomasD : potentiellement ici il faut adapter pour drawrangeelem
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
                glBufferDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals+nbColors)*sizeof(GL_FLOAT), 0, GL_STATIC_DRAW_ARB);
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, nbVertices*sizeof(GL_FLOAT), vertices);                             // copy vertices starting from 0 offest
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, nbVertices*sizeof(GL_FLOAT), nbNormals*sizeof(GL_FLOAT), normals);                // copy normals after vertices
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals)*sizeof(GL_FLOAT), nbColors*sizeof(GL_FLOAT), colors);  // copy colours after normals
                delete [] vertices; delete []normals; delete []colors;
                vertices=nullptr; normals=nullptr; colors=nullptr; 
            }
        }
        

        //Quad
		else if ((!hexaTopo && quadTopo && nbTopoElem==quadTopo->getNbQuads())
            ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::QUAD_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbQuads())){ 
            nbQuadToDraw=0;


            int nbIndex=0;
            int nbColorIndex=0;
            for (int i=0; i<nbTopoElem; ++i)
            {
                Vec4f color;
                updateColor(eval,eval1,eval2,eval3,clData[i],color);
                if((f_displayRange1.getValue() && clData[i]<minmed) || (f_displayRange3.getValue() && clData[i]>maxmed) || (f_displayRange2.getValue() && clData[i]>=minmed && clData[i]<=maxmed) ){
                    Quad t ;
                    if(_extPiperMeshLoaderPtr==nullptr)
                        t = quadTopo->getQuad(i);
                    else
                        t = _extPiperMeshLoaderPtr->getQuads()[i];
                    nbIndex = 3*2*3*nbQuadToDraw;
                    nbColorIndex = 3*2*4*nbQuadToDraw;

                    for(int k=0; k<2 ; k++){
                        for(int m=0 ; m<3 ; m++){
                            for(int n=0; n<4;n++)
                                colors[nbColorIndex+(k*3+m)*4+n] = color[n]; 
                            for(int n=0 ; n<3 ; n++)
                                normals[nbIndex+(k*3+m)*3+n] = m_normals[i][n]; 
                        }
                    }
                    for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*0+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*1+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*2+n] = (*x)[t[2]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*3+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*4+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*5+n] = (*x)[t[3]][n];
                    }

                    if(f_drawNormal.getValue()){
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[1]]+ (*x)[ t[2] ] + (*x)[ t[3] ] )/4), i));
                    }
                    nbQuadToDraw++;
                }
            }
            if(vboId!=0 && drawWithVBO){
                verticesPtr=0; 
                normalsPtr=(GLfloat*)(nbVertices*sizeof(GL_FLOAT)); 
                colorsPtr=(GLfloat*)((nbVertices+nbNormals)*sizeof(GL_FLOAT));  //TODOThomasD : potentiellement ici il faut adapter pour drawrangeelem
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
                glBufferDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals+nbColors)*sizeof(GL_FLOAT), 0, GL_STATIC_DRAW_ARB);
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, nbVertices*sizeof(GL_FLOAT), vertices);                             // copy vertices starting from 0 offest
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, nbVertices*sizeof(GL_FLOAT), nbNormals*sizeof(GL_FLOAT), normals);                // copy normals after vertices
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals)*sizeof(GL_FLOAT), nbColors*sizeof(GL_FLOAT), colors);  // copy colours after normals
                delete [] vertices; delete []normals; delete []colors;
                vertices=nullptr; normals=nullptr; colors=nullptr; 
            }
        }

        //Hexa
        else  if ((hexaTopo && nbTopoElem==hexaTopo->getNbHexahedra())
            ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::HEXA_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbHexahedra())){ 
            nbHexaToDraw=0;

            int nbIndex=0;
            int nbColorIndex=0;
            for (int i=0; i<nbTopoElem; ++i)
            {
                Vec4f color;
                updateColor(eval,eval1,eval2,eval3,clData[i],color);
                if((f_displayRange1.getValue() && clData[i]<minmed) || (f_displayRange3.getValue() && clData[i]>maxmed) || (f_displayRange2.getValue() && clData[i]>=minmed && clData[i]<=maxmed) ){
                    Hexa t ;
                    if(_extPiperMeshLoaderPtr==nullptr)
                        t = hexaTopo->getHexa(i);
                    else 
                        t = _extPiperMeshLoaderPtr->getHexahedra()[i];
                    nbIndex = 3*12*3*nbHexaToDraw;
                    nbColorIndex = 3*12*4*nbHexaToDraw;

                    for(int k=0 ; k<6 ; k++){
                        for(int m=0 ; m<6 ; m++){
                            for(int n=0; n<4;n++)
                                colors[nbColorIndex+(k*6+m)*4+n] = color[n]; 
                            for(int n=0 ; n<3 ; n++)
                                normals[nbIndex+(k*6+m)*3+n] = m_normals[ 6*i+k ][n];
                        }
                    }
                    for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*0+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*1+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*2+n] = (*x)[t[1]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*3+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*4+n] = (*x)[t[3]][n];
                        vertices[nbIndex+3*5+n] = (*x)[t[2]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*6+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*7+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*8+n] = (*x)[t[5]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*9+n] = (*x)[t[2]][n];
                        vertices[nbIndex+3*10+n] = (*x)[t[6]][n];
                        vertices[nbIndex+3*11+n] = (*x)[t[5]][n];
                    }
                    for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*12+n] = (*x)[t[3]][n];
                        vertices[nbIndex+3*13+n] = (*x)[t[6]][n];
                        vertices[nbIndex+3*14+n] = (*x)[t[2]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*15+n] = (*x)[t[3]][n];
                        vertices[nbIndex+3*16+n] = (*x)[t[7]][n];
                        vertices[nbIndex+3*17+n] = (*x)[t[6]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*18+n] = (*x)[t[4]][n];
                        vertices[nbIndex+3*19+n] = (*x)[t[7]][n];
                        vertices[nbIndex+3*20+n] = (*x)[t[3]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*21+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*22+n] = (*x)[t[4]][n];
                        vertices[nbIndex+3*23+n] = (*x)[t[3]][n];
                    }
                    for(int n=0 ; n<3 ; n++){
                        vertices[nbIndex+3*24+n] = (*x)[t[7]][n];
                        vertices[nbIndex+3*25+n] = (*x)[t[4]][n];
                        vertices[nbIndex+3*26+n] = (*x)[t[5]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*27+n] = (*x)[t[6]][n];
                        vertices[nbIndex+3*28+n] = (*x)[t[7]][n];
                        vertices[nbIndex+3*29+n] = (*x)[t[5]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*30+n] = (*x)[t[0]][n];
                        vertices[nbIndex+3*31+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*32+n] = (*x)[t[4]][n];
                    }
                    for(int n=0;n<3;n++){
                        vertices[nbIndex+3*33+n] = (*x)[t[1]][n];
                        vertices[nbIndex+3*34+n] = (*x)[t[5]][n];
                        vertices[nbIndex+3*35+n] = (*x)[t[4]][n];
                    }

                    if(f_drawNormal.getValue()){
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[1]]  + (*x)[ t[2] ]+ (*x)[ t[3] ])/4), 6*i + 0));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[1] ] + (*x)[ t[2] ] + (*x)[ t[5]] + (*x)[ t[6] ])/4), 6*i + 1));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[2] ] + (*x)[ t[3] ] + (*x)[ t[6]] + (*x)[ t[7] ])/4), 6*i + 2));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[3] ] + (*x)[ t[4]] + (*x)[ t[7] ])/4), 6*i + 3));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[4] ] + (*x)[ t[5] ] + (*x)[ t[6]] + (*x)[ t[7] ])/4), 6*i + 4));
                        normalCenter.push_back(std::make_pair(Vec3f(((*x)[t[0] ] + (*x)[ t[1] ] + (*x)[ t[4]] + (*x)[ t[5] ])/4), 6*i + 5));
                    }
                    nbHexaToDraw++;
                }
            }
            if(vboId!=0 && drawWithVBO){
                verticesPtr=0; 
                normalsPtr=(GLfloat*)(nbVertices*sizeof(GL_FLOAT)); 
                colorsPtr=(GLfloat*)((nbVertices+nbNormals)*sizeof(GL_FLOAT));  //TODOThomasD : potentiellement ici il faut adapter pour drawrangeelem
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
                glBufferDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals+nbColors)*sizeof(GL_FLOAT), 0, GL_STATIC_DRAW_ARB);
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, nbVertices*sizeof(GL_FLOAT), vertices);                             // copy vertices starting from 0 offest
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, nbVertices*sizeof(GL_FLOAT), nbNormals*sizeof(GL_FLOAT), normals);                // copy normals after vertices
                glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, (nbVertices+nbNormals)*sizeof(GL_FLOAT), nbColors*sizeof(GL_FLOAT), colors);  // copy colours after normals
                delete [] vertices; delete []normals; delete []colors;
                vertices=nullptr; normals=nullptr; colors=nullptr;
            }
        }

    }
}

void DataDisplayTopo::drawVisual(const core::visual::VisualParams* vparams)
{
    if (!vparams->displayFlags().getShowVisualModels()) return;

    const VecCoord* x ;
    if(_extPiperMeshLoaderPtr==nullptr)
        x = &(this->read(sofa::core::ConstVecCoordId::position())->getValue());
    else
        x = &_extPiperMeshLoaderPtr->getExtVertices();
    const VecPointData &ptData = f_pointData.getValue();
    const VecCellData &clData = f_cellData.getValue();


    //udate range values
    updateMinMax();
	if(!f_hasUserRange.getValue()){
	    // Range for points
	    min=0.0; max=0.0;	
        if (bDrawPointData) {
            VecPointData::const_iterator i = ptData.begin();
            min = *i;
            max = *i;
            while (++i != ptData.end()) {
                if (min > *i) min = *i;
                if (max < *i) max = *i;
            }
        }
        // Range for cells
        if (bDrawCellData) {
            VecCellData::const_iterator i = clData.begin();
            min = *i;
            max = *i;
            while (++i != clData.end()) {
                if (min > *i) min = *i;
                if (max < *i) max = *i;
            }
        }
        if (max > oldMax) oldMax = max;
        if (min < oldMin) oldMin = min;

        if (f_maximalRange.getValue()) {
            max = oldMax;
            min = oldMin;
        }
	}

    //TOPO
    if (bDrawCellData) {
        if(!f_drawCullBackFace.getValue())
            glDisable(GL_CULL_FACE);
        else{
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
        }
        if(f_hasAlpha.getValue()){
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        vparams->drawTool()->setLightingEnabled(f_drawWithLight.getValue());
        
             if(vboId!=0 && drawWithVBO)
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboId);
            glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
            glEnable(GL_COLOR_MATERIAL);
            glEnableClientState(GL_NORMAL_ARRAY);
            glEnableClientState(GL_COLOR_ARRAY);
            glEnableClientState(GL_VERTEX_ARRAY);
            glNormalPointer(GL_FLOAT, 0, normalsPtr);
            glColorPointer(4, GL_FLOAT, 0, colorsPtr);
            glVertexPointer(3, GL_FLOAT, 0, verticesPtr);
            if((!tetraTopo && triangleTopo  && nbTopoElem==triangleTopo->getNbTriangles())  //Triangles
                ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TRI_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbTriangles()))
                    glDrawArrays(GL_TRIANGLES, 0, nbTriToDraw*3);
            else if ((tetraTopo && nbTopoElem==tetraTopo->getNbTetrahedra()) //tetras
                ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::TETRA_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbTetrahedra()))
                    glDrawArrays(GL_TRIANGLES, 0, nbTetraToDraw*3*4);
            else if ((!hexaTopo && quadTopo && nbTopoElem==quadTopo->getNbQuads()) //quads
                ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::QUAD_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbQuads()))
                    glDrawArrays(GL_TRIANGLES, 0, nbQuadToDraw*3*2);
            else if ((hexaTopo && nbTopoElem==hexaTopo->getNbHexahedra())//hexas
                ||(_extPiperMeshLoaderPtr!=nullptr && selectedElementType==ElementType::HEXA_Elem && nbTopoElem==_extPiperMeshLoaderPtr->getNbHexahedra()))
                    glDrawArrays(GL_TRIANGLES, 0, nbHexaToDraw*3*12);
            glDisableClientState(GL_VERTEX_ARRAY);
            glDisableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_NORMAL_ARRAY);
            glDisable(GL_COLOR_MATERIAL);
            if(vboId!=0 && drawWithVBO)
                glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);  

        //Normal
        if(f_drawNormal.getValue()){ 
            glLineWidth(1);
            glDisable(GL_LIGHTING);
            glBegin(GL_LINES);

            if((tetraTopo || hexaTopo ||  quadTopo || triangleTopo)
                || (_extPiperMeshLoaderPtr!=nullptr && selectedElementType!=ElementType::NONE)){
                for (unsigned int i=0; i<normalCenter.size(); i++)
                {
                        vparams->drawTool()->drawPoint(normalCenter.at(i).first  , normalColor );
                        vparams->drawTool()->drawPoint(normalCenter.at(i).first+m_normals[ normalCenter.at(i).second ]*f_normalSize.getValue(), normalColor );

                }
            }

            glEnd();
        }
        glDisable(GL_CULL_FACE);
        glDisable(GL_BLEND);
        vparams->drawTool()->setLightingEnabled(false);
    }

    if ((bDrawCellData || (!topology && !_extPiperMeshLoaderPtr)) && bDrawPointData) {
        ColorMap::evaluator<Real> eval = colorMap->getEvaluator(min, max);
        // Just the points
        glPointSize(10);
        glBegin(GL_POINTS);
        for (unsigned int i=0; i<x->size(); ++i)
        {
            Vec4f color = isnan(ptData[i])
                ? f_colorNaN.getValue()
                : eval(ptData[i]);
            vparams->drawTool()->drawPoint((*x)[i], color);
        }
        glEnd();

    } else if (bDrawPointData) {
        ColorMap::evaluator<Real> eval = colorMap->getEvaluator(min, max);

        glPushAttrib ( GL_LIGHTING_BIT );
        glEnable ( GL_LIGHTING );
        glEnable( GL_COLOR_MATERIAL );

        // Triangles
        glBegin(GL_TRIANGLES);
        for (int i=0; i<topology->getNbTriangles(); ++i)
        {
            const Triangle &t = topology->getTriangle(i);
            Vec4f color[3];
            for (int j=0; j<3; j++) {
                color[j] = isnan(ptData[t[j]])
                    ? f_colorNaN.getValue()
                    : eval(ptData[t[j]]);
            }

            vparams->drawTool()->drawTriangle(
                (*x)[ t[0] ], (*x)[ t[1] ], (*x)[ t[2] ],
                m_normals[ t[0] ], m_normals[ t[1] ], m_normals[ t[2] ],
                color[0], color[1], color[2]);
        }
        glEnd();

        // Quads
        glBegin(GL_QUADS);
        for (int i=0; i<topology->getNbQuads(); ++i)
        {
            const Quad &q = topology->getQuad(i);
            Vec4f color[4];
            for (int j=0; j<4; j++)
            {
                color[j] = isnan(ptData[q[j]])
                ? f_colorNaN.getValue()
                : eval(ptData[q[j]]);
            }

            vparams->drawTool()->drawQuad(
                (*x)[ q[0] ], (*x)[ q[1] ], (*x)[ q[2] ], (*x)[ q[3] ],
                m_normals[ q[0] ], m_normals[ q[1] ], m_normals[ q[2] ], m_normals[ q[3] ],
                color[0], color[1], color[2], color[3]);

        }
        glEnd();

        glPopAttrib();
    }

}

void DataDisplayTopo::computeNormals()
{
    if( !topology && (!_extPiperMeshLoaderPtr || selectedElementType == ElementType::NONE)) return;

    const VecCoord* x;
    if(!_extPiperMeshLoaderPtr)
        x = &this->read(sofa::core::ConstVecCoordId::position())->getValue();
    else
        x = &_extPiperMeshLoaderPtr->getExtVertices();

	if ((tetraTopo && (int)f_cellData.getValue().size() == tetraTopo->getNbTetrahedra())
        || (_extPiperMeshLoaderPtr && selectedElementType==ElementType::TETRA_Elem && (int)f_cellData.getValue().size() == _extPiperMeshLoaderPtr->getNbTetrahedra()) )
    { 
		if(!_extPiperMeshLoaderPtr)
            m_normals.resize(tetraTopo->getNbTetrahedra()*4,Vec3f(0,0,0));
        else
            m_normals.resize(_extPiperMeshLoaderPtr->getNbTetrahedra()*4,Vec3f(0,0,0));

        for (int i=0; i<nbTopoElem; ++i)
		{
            Tetra t;
            if(!_extPiperMeshLoaderPtr)
                t = topology->getTetrahedron(i);
            else
                t = _extPiperMeshLoaderPtr->getTetrahedra()[i];

            Coord edge0 = ((*x)[t[1]]-(*x)[t[0]]); //edge0.normalize();
            Coord edge1 = ((*x)[t[2]]-(*x)[t[1]]); //edge1.normalize();
            Coord edge3 = ((*x)[t[3]]-(*x)[t[0]]); //edge3.normalize();
            Coord edge5 = ((*x)[t[3]]-(*x)[t[2]]); //edge5.normalize();
		
            Vec3f triangleNormal1 = cross( edge1, edge0 ) ;
            triangleNormal1.normalize();
		    triangleNormal1 = triangleNormal1 * edge3 <= 0 ? triangleNormal1 : - triangleNormal1;
            Vec3f triangleNormal2 = cross( edge0, edge3 ) ;
            triangleNormal2.normalize();
		    triangleNormal2 = triangleNormal2 * edge1 <= 0 ? triangleNormal2 : - triangleNormal2;
            Vec3f triangleNormal3 = cross( edge1, edge5 ) ;
            triangleNormal3.normalize();
		    triangleNormal3 = triangleNormal3 * (-edge0) <= 0 ? triangleNormal3 : - triangleNormal3;
            Vec3f triangleNormal4 = cross( edge5, edge3 ) ;
		    triangleNormal4.normalize();
		    triangleNormal4 = triangleNormal4 * edge0 <= 0 ? triangleNormal4 : - triangleNormal4;
        
             m_normals[4*i+0] = triangleNormal1;
		     m_normals[4*i+1] = triangleNormal2;
		     m_normals[4*i+2] = triangleNormal3;
		     m_normals[4*i+3] = triangleNormal4;
        }
	}
    else if ((hexaTopo && (int)f_cellData.getValue().size() == hexaTopo->getNbHexas())
        || (_extPiperMeshLoaderPtr && selectedElementType==ElementType::HEXA_Elem && (int)f_cellData.getValue().size() == _extPiperMeshLoaderPtr->getNbHexahedra()) )
    { 
        if(!_extPiperMeshLoaderPtr)
		    m_normals.resize(hexaTopo->getNbHexas()*6,Vec3f(0,0,0));
        else
            m_normals.resize(_extPiperMeshLoaderPtr->getNbHexahedra()*6,Vec3f(0,0,0));

		for (int i=0; i<nbTopoElem; ++i)
		{
            Hexa t;
            if (!_extPiperMeshLoaderPtr)
                t = topology->getHexa(i);
            else
                t = _extPiperMeshLoaderPtr->getHexahedra()[i];
            Coord edge0 = ((*x)[t[1]]-(*x)[t[0]]);// edge0.normalize();
            Coord edge1 = ((*x)[t[2]]-(*x)[t[1]]);// edge1.normalize();
            Coord edge5 = ((*x)[t[5]]-(*x)[t[1]]); //edge5.normalize();
            Coord edge7 =  ((*x)[t[7]]-(*x)[t[3]]); //edge7.normalize();
            Coord edge10 = ((*x)[t[7]]-(*x)[t[6]]); //edge10.normalize();
            Coord edge11 = ((*x)[t[7]]-(*x)[t[4]]); //edge11.normalize();
            		
            
            Vec3f triangleNormal1 = cross( edge1, edge0 ) ;
            triangleNormal1.normalize();
            triangleNormal1 = triangleNormal1 * edge5 <= 0 ? triangleNormal1 : - triangleNormal1;
            
            Vec3f triangleNormal2 = cross( edge1, edge5 ) ;
            triangleNormal2.normalize();
		    triangleNormal2 = triangleNormal2 * edge10 <= 0 ? triangleNormal2 : - triangleNormal2;
            
            Vec3f triangleNormal3 = cross( edge10, edge7 ) ;
            triangleNormal3.normalize();
		    triangleNormal3 = triangleNormal3 * (-edge1) <= 0 ? triangleNormal3 : - triangleNormal3;
		    
            Vec3f triangleNormal4 = cross( edge7, edge11 ) ;
            triangleNormal4.normalize();
		    triangleNormal4 = triangleNormal4 * edge0 <= 0 ? triangleNormal4 : - triangleNormal4;
            
            Vec3f triangleNormal5 = cross( edge11, edge10 ) ;
            triangleNormal5.normalize();
		    triangleNormal5 = triangleNormal5 * (-edge5) <= 0 ? triangleNormal5 : - triangleNormal5;
            
            Vec3f triangleNormal6 = cross( edge0, edge5 ) ;
            triangleNormal6.normalize();
    	    triangleNormal6 = triangleNormal6 * edge1 <= 0 ? triangleNormal6 : - triangleNormal6;
        
             m_normals[6*i+0] = triangleNormal1;
		     m_normals[6*i+1] = triangleNormal2;
		     m_normals[6*i+2] = triangleNormal3;
		     m_normals[6*i+3] = triangleNormal4;
             m_normals[6*i+4] = triangleNormal5;
             m_normals[6*i+5] = triangleNormal6;
        }
	}
    else if ((triangleTopo && (int)f_cellData.getValue().size() == triangleTopo->getNbTriangles())
        || (_extPiperMeshLoaderPtr && selectedElementType==ElementType::TRI_Elem && (int)f_cellData.getValue().size() == _extPiperMeshLoaderPtr->getNbTriangles()) )
    { 
        if(!_extPiperMeshLoaderPtr)
            m_normals.resize(triangleTopo->getNbTriangles(),Vec3f(0,0,0));
        else
            m_normals.resize(_extPiperMeshLoaderPtr->getNbTriangles(),Vec3f(0,0,0));

        for (int i=0; i<nbTopoElem; ++i)
		{
            Triangle t;
            if(!_extPiperMeshLoaderPtr)
                t = topology->getTriangle(i);
            else
                t = _extPiperMeshLoaderPtr->getTriangles()[i];

            Coord edge0 = ((*x)[t[1]]-(*x)[t[0]]);// edge0.normalize();
            Coord edge1 = ((*x)[t[2]]-(*x)[t[0]]);// edge1.normalize();
            Vec3f triangleNormal = cross( edge0, edge1 ) ;
            triangleNormal.normalize();
            m_normals[i] = triangleNormal;     
        }
	}
    else if ((quadTopo && (int)f_cellData.getValue().size() == quadTopo->getNbQuads())
        || (_extPiperMeshLoaderPtr && selectedElementType==ElementType::QUAD_Elem && (int)f_cellData.getValue().size() == _extPiperMeshLoaderPtr->getNbQuads()) )
    { 
        if(!_extPiperMeshLoaderPtr)
		    m_normals.resize( quadTopo->getNbQuads(),Vec3f(0,0,0));
        else
            m_normals.resize( _extPiperMeshLoaderPtr->getNbQuads(),Vec3f(0,0,0));

        for (int i=0; i<nbTopoElem; ++i)
		{
            Quad t;
            if(!_extPiperMeshLoaderPtr)
                t = topology->getQuad(i);
            else
                t = _extPiperMeshLoaderPtr->getQuads()[i];
            Coord edge0 = ((*x)[t[1]]-(*x)[t[0]]); //edge0.normalize();
            Coord edge1 = ((*x)[t[2]]-(*x)[t[1]]); //edge1.normalize();
            Vec3f triangleNormal = cross( edge0, edge1 ) ;
            triangleNormal.normalize();
            m_normals[i] = triangleNormal;     
        }
	}
	else if(!_extPiperMeshLoaderPtr) // computing vertex normals
    {
		m_normals.resize(x->size(),Vec3f(0,0,0));

        for (int i=0; i<topology->getNbTriangles(); ++i)
        {
            const Triangle &t = topology->getTriangle(i);

            Coord edge0 = ((*x)[t[1]]-(*x)[t[0]]); edge0.normalize();
            Coord edge1 = ((*x)[t[2]]-(*x)[t[0]]); edge1.normalize();
            Real triangleSurface = edge0*edge1; // *0.5; - that is not necessary - it is done for each contributing normal and then the result is normalized anyway
            Vec3f triangleNormal = cross( edge0, edge1 ) * triangleSurface;

            for( int j=0 ; j<3 ; ++j )
            {
                m_normals[t[j]] += triangleNormal;
            }
        }
	
        for (int i=0; i<topology->getNbQuads(); ++i)
        {
            const Quad &q = topology->getQuad(i);

            for( int j=0 ; j<4 ; ++j )
            {
                Coord edge0 = ((*x)[q[(j+1)%3]]-(*x)[q[j]]); edge0.normalize();
                Coord edge1 = ((*x)[q[(j+2)%3]]-(*x)[q[j]]); edge1.normalize();
                Real triangleSurface = edge0*edge1; // *0.5; - that is not necessary - it is done for each contributing normal and then the result is normalized anyway
                Vec3f quadNormal = cross( edge0, edge1 ) * triangleSurface;

                m_normals[q[j]] += quadNormal;
            }
        }
		
        // normalization
        for (size_t i=0; i<x->size(); ++i)
            m_normals[i].normalize();
	}
}

void DataDisplayTopo::updateMinMax()
{
    minmed=0; maxmed=0;
    if(f_hasUserRange.getValue()){
        float temp;
        min=f_userRange.getValue()[0];
        max=f_userRange.getValue()[0];
        for(unsigned int i=0;i<f_userRange.getValue().size();++i){
            temp= f_userRange.getValue()[i];
            min=(min>temp)?temp:min;
            max=(max<temp)?temp:max;
        }
        minmed=max;
        maxmed=min;
        for(unsigned int i=0;i<f_userRange.getValue().size();++i){
            temp= f_userRange.getValue()[i];
            minmed=(min<temp && temp<max && temp < minmed )?temp:minmed;
            maxmed=(min<temp && temp<max && temp > maxmed )?temp:maxmed;
        }
        temp=std::min(minmed,maxmed);
        maxmed=std::max(minmed,maxmed);
        minmed=temp;
    }
}

void DataDisplayTopo::updateColor(ColorMap::evaluator<Real>& eval,ColorMap::evaluator<Real>& eval1,ColorMap::evaluator<Real>& eval2,ColorMap::evaluator<Real>& eval3, Real clData, Vec4f& color) //eval,color, clData[i], 
{
    if(!f_hasSuppColorMap.getValue()){
        color = isnan(clData)
            ? f_colorNaN.getValue()
            : eval(clData);
        if(f_hasAlpha.getValue())
            color[3]=f_userAlpha.getValue()[0];
    }
    else{
        if(clData<minmed){
            color = isnan(clData)
                ? f_colorNaN.getValue()
                : eval2(clData);
            if(f_hasAlpha.getValue())
                color[3]=f_userAlpha.getValue()[0];
        }
        else if(clData>maxmed){
            color = isnan(clData)
                ? f_colorNaN.getValue()
                : eval3(clData);
            if(f_hasAlpha.getValue())
                color[3]=f_userAlpha.getValue()[2];
        }
        else {
            color = isnan(clData)
                ? f_colorNaN.getValue()
                : eval1(clData);
            if(f_hasAlpha.getValue())
                color[3]=f_userAlpha.getValue()[1];
        }

    }

}



} // namespace visualmodel

} // namespace component

} // namespace sofa
