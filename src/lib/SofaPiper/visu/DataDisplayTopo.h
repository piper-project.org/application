/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, version 1.0 RC 1        *
*                (c) 2006-2011 MGH, INRIA, USTL, UJF, CNRS                    *
*                                                                             *
* This library is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This library is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this library; if not, write to the Free Software Foundation,     *
* Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA.          *
*******************************************************************************
*                               SOFA :: Modules                               *
*                                                                             *
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_VISUALMODEL_DATADISPLAYTOPO_H
#define SOFA_COMPONENT_VISUALMODEL_DATADISPLAYTOPO_H

#pragma warning(push, 0)
#include <sofa/core/visual/VisualModel.h>
#include <sofa/core/visual/VisualParams.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <sofa/helper/ColorMap.h>
#include <SofaBaseVisual/VisualModelImpl.h>
#pragma warning(pop)
#include "../PiperMeshLoader.h"

using namespace sofa::defaulttype;

namespace sofa
{

namespace component
{

namespace visualmodel
{

class SOFA_SofaPiper_API DataDisplayTopo : core::visual::VisualModel, public ExtVec3fState
{
private:
    	enum class ElementType {NONE = -1,TETRA_Elem,HEXA_Elem,TRI_Elem,QUAD_Elem};
public:
    SOFA_CLASS2(DataDisplayTopo, core::visual::VisualModel, ExtVec3fState);

    typedef core::topology::BaseMeshTopology::Triangle    Triangle;
    typedef core::topology::BaseMeshTopology::Quad        Quad;
	typedef core::topology::BaseMeshTopology::Tetrahedron Tetra;
    typedef core::topology::BaseMeshTopology::Hexahedron  Hexa;

    

    typedef helper::vector<Real> VecPointData;
    typedef helper::vector<Real> VecCellData;

protected:

    DataDisplayTopo()
        : f_maximalRange(initData(&f_maximalRange, true, "maximalRange", "Keep the maximal range through all timesteps"))
		  ,f_hasUserRange(initData(&f_hasUserRange, false, "hasUserRange", "Let the user specify the maximum range"))
		  ,f_userRange(initData(&f_userRange, Vec4f(-1.0f,-0.15f,0.15f,1.0f), "userRange", "Range used"))
		  ,f_userAlpha(initData(&f_userAlpha, Vec3f(1.0f,1.0f,1.0f), "userAlpha", "alpha used"))
		  ,f_displayRange1(initData(&f_displayRange1, true, "displayRange1", "Enable display first data Range elements"))
		  ,f_displayRange2(initData(&f_displayRange2, true, "displayRange2", "Enable display second data Range elements"))
		  ,f_displayRange3(initData(&f_displayRange3, true, "displayRange3", "Enable display third data Range elements"))
          ,f_drawNormal(initData(&f_drawNormal, false, "drawNormal", "draw normals as recomputed by this component"))
          ,f_normalSize(initData(&f_normalSize, (Real)1.0, "normalSize", "draw normal with magnifier"))
          ,f_drawWithLight(initData(&f_drawWithLight, true, "drawWithLight", "activates lighting"))
          ,f_drawCullBackFace(initData(&f_drawCullBackFace, true, "CullBackFace", "activates backface culling"))
		  ,f_hasAlpha(initData(&f_hasAlpha, false, "hasAlpha", "Enable setting Alpha param"))
          ,f_pointData(initData(&f_pointData, "pointData", "Data associated with nodes"))
          ,f_cellData(initData(&f_cellData, "cellData", "Data associated with elements"))
          ,f_colorNaN(initData(&f_colorNaN, Vec4f(0.0f,0.0f,0.0f,1.0f), "colorNaN", "Color used for NaN values"))
		  ,f_hasSuppColorMap(initData(&f_hasSuppColorMap, false, "hasSuppColorMap", "Enable supplementary ColorMap"))
		  ,f_colorMapName2(initData(&f_colorMapName2, "colorMapName2", "link to first supplementary ColorMap (inf)"))
		  ,f_colorMapName3(initData(&f_colorMapName3, "colorMapName3", "link to second supplementary ColorMap (sup)"))
          ,f_showPositionsInMenu(initData(&f_showPositionsInMenu, false, "showPositionsInMenu", "display full list of position and cell in gui menus",false))
          ,piperMeshLoaderPath ( initData (&piperMeshLoaderPath, "extPiperMeshLoaderPath", "Path to a pipermeshloader component"))
          , extElementKindStr ( initData ( &extElementKindStr,"elementKindStr","kind of element to display when using extPiperMeshLoader" ) )

          , state(NULL)
          , topology(NULL)
          , oldMin(0)
          , oldMax(0)
          , vboId(0)
          , nbVertices(0)
          , nbNormals(0)
          , nbColors(0)
          , nbTopoElem(0)
          , drawWithVBO(false)
          ,triangleTopo(NULL)
          ,tetraTopo(NULL)
          ,hexaTopo(NULL)
          ,quadTopo(NULL)
          ,_extPiperMeshLoaderPtr(nullptr)
          ,vertices(nullptr)
          ,normals(nullptr)
          ,colors(nullptr)
          ,colorsPtr(nullptr)
          ,verticesPtr(nullptr)
          ,normalsPtr(nullptr)
          ,selectedElementType(ElementType::NONE)
    {}

    ~DataDisplayTopo();
public:

    Data<bool> f_maximalRange;
	Data<bool> f_hasUserRange;
	Data<bool> f_hasSuppColorMap;
	Data<bool> f_hasAlpha;
	Data<bool> f_displayRange1;
	Data<bool> f_displayRange2;
	Data<bool> f_displayRange3;
    Data<bool> f_drawNormal;
    Data<bool> f_drawWithLight;
    Data<bool> f_drawCullBackFace;
    Data<bool> f_showPositionsInMenu;



    Data<Real> f_normalSize;
    Data<Vec4f> f_userRange;
	Data<Vec3f> f_userAlpha;
    Data<VecPointData> f_pointData;
    Data<VecCellData> f_cellData;
    Data<defaulttype::Vec4f> f_colorNaN; // Color for NaNs (alpha channel is not used)
	Data<std::string> f_colorMapName2, f_colorMapName3;
    Data <std::string> piperMeshLoaderPath;
    Data< std::string > extElementKindStr;
    sofa::helper::ColorMap *colorMap, *colorMap1, *colorMap2, *colorMap3;
    core::State<DataTypes> *state;
    core::topology::BaseMeshTopology* topology;
    Real oldMin, oldMax;

    void init();
    //void reinit();

    void initVisual();
    void clearVisual();
    //void initTextures() {}
    void drawVisual(const core::visual::VisualParams* vparams);
    //void drawTransparent(const VisualParams* /*vparams*/)
    void updateVisual();

    virtual bool insertInNode( core::objectmodel::BaseNode* node ) { Inherit1::insertInNode(node); Inherit2::insertInNode(node); return true; }
    virtual bool removeInNode( core::objectmodel::BaseNode* node ) { Inherit1::removeInNode(node); Inherit2::removeInNode(node); return true; }

protected:

     GLfloat *vertices,*normals,*colors;
     GLfloat *verticesPtr, *normalsPtr, *colorsPtr;
     int nbVertices, nbNormals, nbColors;
     int nbTopoElem;

    topology::TriangleSetTopologyContainer* triangleTopo ; 
	topology::TetrahedronSetTopologyContainer* tetraTopo ;
    topology::HexahedronSetTopologyContainer* hexaTopo;
    topology::QuadSetTopologyContainer* quadTopo;

    sofa::component::loader::PiperMeshLoader* _extPiperMeshLoaderPtr;
    ElementType selectedElementType;
    void computeNormals();
    void computeCellDataToDraw();

    helper::vector<defaulttype::Vec3f> m_normals;
	Real min, max,minmed,maxmed;

    void createGlLocalBuffer();
    void updateMinMax();
    void updateColor(sofa::helper::ColorMap::evaluator<Real>& eval, sofa::helper::ColorMap::evaluator<Real>& eval1, sofa::helper::ColorMap::evaluator<Real>& eval2, sofa::helper::ColorMap::evaluator<Real>& eval3, Real clData, Vec4f& color);
		
private:
    bool bDrawPointData ;
    bool bDrawCellData ;
    bool drawWithVBO;
    std::vector< std::pair< Vec3f,int > > normalCenter;
    Vec4f normalColor;
    int nbTriToDraw;
    int nbTetraToDraw;
    int nbQuadToDraw;
    int nbHexaToDraw;
    GLuint vboId;
};

} // namespace visualmodel

} // namespace component

} // namespace sofa

#endif // #ifndef SOFA_COMPONENT_VISUALMODEL_DATADISPLAYTOPO_H
