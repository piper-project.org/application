/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshQualityVTK.h"

#include "hbm/FEModel.h"
#include "vtkSmartPointer.h"
#include "vtkMeshQuality.h"
#include "vtkDoubleArray.h"
#include "vtkCellIterator.h"
#include "vtkCellType.h"
#include "vtkArray.h"
#include "vtkCellData.h"
#include "vtkIdList.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>

using namespace Eigen;

namespace piper {
    namespace meshoptimizer {
        using namespace hbm;

        meshQualityVTK::meshQualityVTK() : meshQualityAbstract(),  m_mesh(new FEModelVTK()) {
            m_metrics.insert(std::pair<QualityMetrics, int>(QualityMetrics::VTK_SCALED_JACOBIAN, 2));
            m_metrics.insert(std::pair<QualityMetrics, int>(QualityMetrics::VTK_SIZE_SHAPE, 3));
            m_validmetrics.insert(QualityMetrics::VTK_SCALED_JACOBIAN);
            m_validmetrics.insert(QualityMetrics::VTK_SIZE_SHAPE);
        }

        meshQualityVTK::~meshQualityVTK() {
            int a = 1;
            //delete m_mesh;
        }



        void meshQualityVTK::reset() {
            delete m_mesh;
            m_mesh = new FEModelVTK();
            m_meshquality2D.resize(0, 0);
            m_meshquality3D.resize(0, 0);
        }

        void meshQualityVTK::setMesh(piper::hbm::FEModel& fem) {
            meshQualityAbstract::setMesh(fem);
            // piper id elements
            VId vid2D, vid3D;
            m_mesh->setMesh(fem);
            //std::cout << "VTK mesh :" << m_mesh->getVTKMesh()->GetNumberOfPoints() << " - " << m_mesh->getVTKMesh()->GetNumberOfCells() << std::endl;
            //cell iterator
            vtkCellIterator *iter = m_mesh->getVTKMesh()->vtkDataSet::NewCellIterator();
            iter = m_mesh->getVTKMesh()->vtkUnstructuredGrid::NewCellIterator();
            iter->InitTraversal();
            vtkIdType cellId = 0;
            vtkSmartPointer<vtkIdList> cellId_2D = vtkSmartPointer<vtkIdList>::New();
            vtkSmartPointer<vtkIdList> cellId_3D = vtkSmartPointer<vtkIdList>::New();
            while (!iter->IsDoneWithTraversal()) {
                if (iter->GetCellType() != VTK_LINE) {// no quality metrocs for 1 elements
                    if ((iter->GetCellType() == VTK_TRIANGLE) || (iter->GetCellType() == VTK_QUAD)) {
                        cellId_2D->InsertNextId(cellId);
                        vid2D.push_back(m_mesh->getVTKMesh()->GetCellData()->GetArray("eid")->GetComponent(cellId, 0));
                    }
                    else if ((iter->GetCellType() == VTK_HEXAHEDRON) || (iter->GetCellType() == VTK_TETRA)) {
                        cellId_3D->InsertNextId(cellId);
                        vid3D.push_back(m_mesh->getVTKMesh()->GetCellData()->GetArray("eid")->GetComponent(cellId, 0));
                    }
                }
                iter->GoToNextCell();
                ++cellId;
            }
            // resize matrics for quality results
            //2D
            if (fem.getNbElement2D() > 0) {
                m_meshquality2D.resize(vid2D.size(), 2 + m_metrics.size());
                for (size_t i = 0; i < vid2D.size(); ++i) {
                    m_meshquality2D(i, 0) = cellId_2D->GetId(i);
                    m_meshquality2D(i, 1) = vid2D[i];
                }
            }
            //3D
            if (fem.getNbElement3D() > 0) {
                m_meshquality3D.resize(vid3D.size(), 2 + m_metrics.size());
                for (size_t i = 0; i < vid3D.size(); ++i) {
                    m_meshquality3D(i, 0) = cellId_3D->GetId(i);
                    m_meshquality3D(i, 1) = vid3D[i];
                }
            }
        }

        void meshQualityVTK::computeQuality() {
            vtkSmartPointer<vtkMeshQuality> qualityFilter = vtkSmartPointer<vtkMeshQuality>::New();
            qualityFilter->SetInputData(m_mesh->getVTKMesh());
            // ScaledJacobian
            qualityFilter->SetTriangleQualityMeasureToScaledJacobian();
            qualityFilter->SetQuadQualityMeasureToScaledJacobian();
            qualityFilter->SetHexQualityMeasureToScaledJacobian();
            qualityFilter->SetTetQualityMeasureToScaledJacobian();
            qualityFilter->Update();
            //
            vtkSmartPointer<vtkDoubleArray> scaledJacobian = vtkDoubleArray::SafeDownCast(qualityFilter->GetOutput()->GetCellData()->GetArray("Quality"));
            // fill  m_meshquality2D with ScaledJacobian alues
            for (int i = 0; i < m_meshquality2D.rows(); ++i)
                m_meshquality2D(i, m_metrics[QualityMetrics::VTK_SCALED_JACOBIAN]) = scaledJacobian->GetValue(m_meshquality2D(i, 0));
            // fill  m_meshquality3D with ScaledJacobian alues
            for (int i = 0; i < m_meshquality3D.rows(); ++i)
                m_meshquality3D(i, m_metrics[QualityMetrics::VTK_SCALED_JACOBIAN]) = scaledJacobian->GetValue(m_meshquality3D(i, 0));

            // ScaledJacobian
            qualityFilter->SetTriangleQualityMeasureToShapeAndSize();
            qualityFilter->SetQuadQualityMeasureToShapeAndSize();
            qualityFilter->SetHexQualityMeasureToShapeAndSize();
            qualityFilter->SetTetQualityMeasureToShapeAndSize();
            qualityFilter->Update();
            //
            vtkSmartPointer<vtkDoubleArray> ShapeSize = vtkDoubleArray::SafeDownCast(qualityFilter->GetOutput()->GetCellData()->GetArray("Quality"));
            // fill  m_meshquality2D with ScaledJacobian alues
            for (int i = 0; i < m_meshquality2D.rows(); ++i)
                m_meshquality2D(i, m_metrics[QualityMetrics::VTK_SIZE_SHAPE]) = ShapeSize->GetValue(m_meshquality2D(i, 0));
            // fill  m_meshquality3D with ScaledJacobian alues
            for (int i = 0; i < m_meshquality3D.rows(); ++i)
                m_meshquality3D(i, m_metrics[QualityMetrics::VTK_SIZE_SHAPE]) = ShapeSize->GetValue(m_meshquality3D(i, 0));
        }

        void meshQualityVTK::writeQualityResults(std::string const& filename) {

            //
            std::vector<std::string> title;
            title.push_back("\"dim\"");
            title.push_back("\"Elements PIPER Id\"");
			bool writeFEInfo = m_fem->isFEmodel(); // te original implementation
			writeFEInfo = false; // writing the FEinfo takes incredibly long time + it is useless for comparing two meshes
            if (writeFEInfo) {
                title.push_back("\"FE Keywords\"");
                title.push_back("\"FE Id\"");
            }
            for (std::set<QualityMetrics>::const_iterator it = m_validmetrics.begin(); it != m_validmetrics.end(); ++it) {
                title.push_back(std::string("\"" + QualityMetrics_str[(unsigned int)*it] + "\""));
            }
            //
            boost::filesystem::path filePath(filename);
            ofstream file(filePath.string(), ios::out | ios::trunc);
            if (file.is_open()) {
                for (int i = 0; i < title.size(); ++i) {
                    if (i == title.size() - 1)
                        file << title[i] << std::endl;
                    else
                        file << title[i] << ",";
                }
                for (int i = 0; i < m_meshquality2D.rows(); ++i) {
                    if (writeFEInfo) {
                        std::string key;
                        hbm::IdKey idfe;
                        m_fem->getInfoId<hbm::Element2D>(m_meshquality2D(i, 1), key, idfe);
                        file << "\"2D\" , " << m_meshquality2D(i, 1) << "," << key << ",";
                        if (idfe.idstr == "")
                            file << idfe.id << ",";
                        else
                            file << idfe.idstr << ",";
                    }
                    file << m_meshquality2D(i, m_metrics[QualityMetrics::VTK_SCALED_JACOBIAN]) << ", " <<
                    m_meshquality2D(i, m_metrics[QualityMetrics::VTK_SIZE_SHAPE]) << std::endl;
                }
                for (int i = 0; i < m_meshquality3D.rows(); ++i) {
                    if (writeFEInfo) {
                        std::string key;
                        hbm::IdKey idfe;
                        m_fem->getInfoId<hbm::Element3D>(m_meshquality3D(i, 1), key, idfe);
                        file << "\"3D\" , " << m_meshquality3D(i, 1) << "," << key << ",";
                        if (idfe.idstr == "")
                            file << idfe.id << ",";
                        else
                            file << idfe.idstr << ",";
                    }
                    file << m_meshquality3D(i, m_metrics[QualityMetrics::VTK_SCALED_JACOBIAN]) << "," <<
                        m_meshquality3D(i, m_metrics[QualityMetrics::VTK_SIZE_SHAPE]) << std::endl;
                }
                file.close();
            }
        }


        bool meshQualityVTK::getQualityMetrics_2D(QualityMetrics const& metric, Id& id, double& metricvalue) const {
            // 
            if (!isValidMetrics(metric))
                return false;
            for (int i = 0; i < m_meshquality2D.rows(); ++i) {
                if (m_meshquality2D(i, 1) == id) {
                    metricvalue = m_meshquality2D(i, m_metrics.at(metric));
                    return true;
                }
            }
            return false;
        }

        bool meshQualityVTK::getQualityMetrics_3D(QualityMetrics const& metric, Id& id, double& metricvalue) const {
            if (!isValidMetrics(metric))
                return false;
            for (int i = 0; i < m_meshquality3D.rows(); ++i) {
                if (m_meshquality3D(i, 1) == id) {
                    metricvalue = m_meshquality3D(i, m_metrics.at(metric));
                    return true;
                }
            }
            return false;
        }

        bool meshQualityVTK::getQualityMetrics_2D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) const {
            if (!isValidMetrics(metric))
                return false;
            for (size_t n = 0; n < id.size(); n++) {
                Eigen::VectorXd vv = (m_meshquality2D.col(1).array() - id[n]).matrix().cwiseAbs();
                Eigen::MatrixXd::Index row;
                double mini = vv.minCoeff(&row);
                if (mini != 0.0) {
                    metricvalue.clear();
                    return false;
                }
                metricvalue.push_back(m_meshquality2D(row, m_metrics.at(metric)));
            }
            return true;
        }

        bool meshQualityVTK::getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) const {
            if (!isValidMetrics(metric))
                return false;

            for (size_t n = 0; n < id.size(); n++) {
                Eigen::VectorXd vv = (m_meshquality3D.col(1).array() - id[n]).matrix().cwiseAbs();
                Eigen::MatrixXd::Index row;
                double mini = vv.minCoeff(&row);
                if (mini != 0.0) {
                    metricvalue.clear();
                    return false;
                }
                metricvalue.push_back(m_meshquality3D(row, m_metrics.at(metric)));
            }
            return true;
        }
        
        bool meshQualityVTK::getQualityMetrics_2D(QualityMetrics const& metric, std::vector<double>& metricvalue) const {
            if (!isValidMetrics(metric))
                return false;
            metricvalue.clear();
            metricvalue.resize(m_meshquality2D.rows());
            VectorXd::Map(&metricvalue[0], m_meshquality2D.rows()) = m_meshquality3D.col(m_metrics.at(metric));
            //for (size_t n = 0; n < m_meshquality2D.rows(); n++) {
            //    metricvalue.push_back(m_meshquality2D(n, m_metrics.at(metric)));
            //}
            return true;
        }

        bool meshQualityVTK::getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue) const {
            if (!isValidMetrics(metric))
                return false;
            metricvalue.clear();
            metricvalue.resize(m_meshquality3D.rows());
            VectorXd::Map(&metricvalue[0], m_meshquality3D.rows()) = m_meshquality3D.col(m_metrics.at(metric));
            //for (size_t n = 0; n < m_meshquality3D.rows(); n++) {
            //    metricvalue.push_back(m_meshquality3D(n, m_metrics.at(metric)));
            //}
            return true;
        }
        
		bool meshQualityVTK::findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
			std::function<bool(double, double)> compareOperation) const {
            if (!isValidMetrics(metric))
                return false;
            for (int i = 0; i < m_meshquality3D.rows(); ++i) {
                if (compareOperation(m_meshquality3D(i, m_metrics.at(metric)), threshold)) {
                    vid.push_back(m_meshquality3D(i, 1));
                }
            }
            return true;
        }

        double meshQualityVTK::getMaxValueMetric3D(QualityMetrics const& metric) const {
            return m_meshquality3D.col(m_metrics.at(metric)).maxCoeff();
        }

        double meshQualityVTK::getMinValueMetric3D(QualityMetrics const& metric) const {
           return m_meshquality3D.col(m_metrics.at(metric)).minCoeff();
        }
    }//meshoptimizer
}//piper
