/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshQualityAbstract.h"

#include "meshOptimizer.h"

namespace piper {
    namespace meshoptimizer {


        boost::container::vector<vtkOBBNodePtr> meshQualityAbstract::generateSelectionBoxes(bool) { return boost::container::vector<vtkOBBNodePtr>(); };

        meshQualityAbstract::meshQualityAbstract() : m_dim(DIM::ALL), m_fem(nullptr) {};
        bool meshQualityAbstract::isValidMetrics(QualityMetrics const& metric) const {
            return (m_validmetrics.find(metric) != m_validmetrics.end());
        }
    }
}
