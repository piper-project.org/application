/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHMESQUITETAG_PIPER_H
#define MESHMESQUITETAG_PIPER_H

#pragma warning(disable:4251) // to suppress warnings from mesquite

// disable compilation warnings coming from mesquite
#pragma warning(push)
#pragma warning(disable:4267)
#pragma warning(disable:4005)
#pragma warning(disable:4309)
#include "Mesquite_all_headers.hpp"
#pragma warning(pop)



//using namespace Mesquite2;
namespace piper {
    namespace meshoptimizer {

        struct TagDescription {

            std::string name;   //!< Tag name 
            Mesquite2::Mesh::TagType type;     //!< Tag data type
            size_t size;            //!< Size of tag data (sizeof(type)*array_length)

            inline TagDescription(std::string n,
                Mesquite2::Mesh::TagType t,
                size_t s)
                : name(n), type(t), size(s) {}

            inline TagDescription()
                : type(Mesquite2::Mesh::TagType::BYTE), size(0) {}

            inline bool operator==(const TagDescription& o) const
            {
                return name == o.name && type == o.type && size == o.size;
            }
            inline bool operator!=(const TagDescription& o) const
            {
                return name != o.name || type != o.type || size != o.size;
            }
        };

        /**\class MeshImplTags
        *
        * Store tags and tag data for Mesquite's native mesh representation.
        * Stores for each tag: properties, element data, and vertex data.
        * The tag element and vertex data sets are maps between some element
        * or vertex index and a tag value.
        */
        class MeshMesquiteTag{
        public:

            ~MeshMesquiteTag() { clear(); }

            /** \class TagData
            * Store data for a single tag
            */
            struct TagData  {

                //! tag meta data
                const TagDescription desc;

                //! per-element data, or NULL if none has been set.
                void* elementData;

                //! number of entries in elementData
                size_t elementCount;

                //! per-vertex data, or NULL if none has been set.
                void* vertexData;

                //! number of entries in vertexData
                size_t vertexCount;

                //! Default value for tag
                void* defaultValue;

                /** \brief Construct tag
                *\param name Tag name
                *\param type Tag data type
                *\param length Tag array length (1 for scalar/non-array)
                *\param default_val Default value for tag
                */
                inline TagData(const std::string& name,
                    Mesquite2::Mesh::TagType type, unsigned length,
                    void* default_val = 0)
                    : desc(name, type, length*size_from_tag_type(type)),
                    elementData(0), elementCount(0),
                    vertexData(0), vertexCount(0),
                    defaultValue(default_val) {}

                /** \brief Construct tag
                *\param desc Tag description object
                */
                inline TagData(const TagDescription& descr)
                    : desc(descr), elementData(0), elementCount(0),
                    vertexData(0), vertexCount(0),
                    defaultValue(0) {}

                ~TagData();
            };

            /** \brief Get the size of the passed data type */
            static size_t size_from_tag_type(Mesquite2::Mesh::TagType  type);

            /** \brief Clear all data */
            void clear();

            /** \brief Get tag index from name */
            size_t handle(const std::string& name, Mesquite2::MsqError& err) const;

            /** \brief Get tag properties */
            const TagDescription& properties(size_t tag_handle, Mesquite2::MsqError& err) const;

            /** \brief Create a new tag
            *
            * Create a new tag with the passed properties
            *\param name Tag name (must be unique)
            *\param type Tag data type
            *\param length Number of values in tag (array length, 1 for scalar)
            *\param defval Optional default value for tag
            */
            size_t create(const std::string& name,
                Mesquite2::Mesh::TagType type,
                unsigned length,
                const void* defval,
                Mesquite2::MsqError& err);

            /** \brief Create a new tag
            *
            * Create a new tag with the passed properties
            */
            size_t create(const TagDescription& desc,
                const void* defval,
                Mesquite2::MsqError& err);

            /**\brief Remove a tag */
            void destroy(size_t tag_index, Mesquite2::MsqError& err);

            /**\brief Set tag data on elements */
            void set_element_data(size_t tag_handle,
                size_t num_indices,
                const size_t* elem_indices,
                const void* tag_data,
                Mesquite2::MsqError& err);

            /**\brief Set tag data on vertices */
            void set_vertex_data(size_t tag_handle,
                size_t num_indices,
                const size_t* elem_indices,
                const void* tag_data,
                Mesquite2::MsqError& err);

            /**\brief Get tag data on elements */
            void get_element_data(size_t tag_handle,
                size_t num_indices,
                const size_t* elem_indices,
                void* tag_data,
                Mesquite2::MsqError& err) const;

            /**\brief Get tag data on vertices */
            void get_vertex_data(size_t tag_handle,
                size_t num_indices,
                const size_t* elem_indices,
                void* tag_data,
                Mesquite2::MsqError& err) const;

            /**\brief find handle of value in tag data on vertices */
            bool find_vertex_data(size_t& vertex_handle, 
                size_t tag_handle,
                size_t num_indices,
                void* tag_data,
                Mesquite2::MsqError& err) const;

            /**\brief find handle of value in tag data on elements */
            bool find_element_data(size_t& element_handle,
                size_t tag_handle,
                size_t num_indices,
                void* tag_data,
                Mesquite2::MsqError& err) const;

            /**\class TagIterator
            *
            * Iterate over list of valid tag handles
            */
            class TagIterator
            {
            public:
                TagIterator() : tags(0), index(0) {}
                TagIterator(MeshMesquiteTag* d, size_t i) : tags(d), index(i) {}
                size_t operator*() const  { return index + 1; }
                TagIterator operator++();
                TagIterator operator--();
                TagIterator operator++(int);
                TagIterator operator--(int);
                bool operator==(TagIterator other) const { return index == other.index; }
                bool operator!=(TagIterator other) const { return index != other.index; }
            private:
                MeshMesquiteTag* tags;
                size_t index;
            };
            TagIterator tag_begin();
            TagIterator tag_end()   { return TagIterator(this, tagList.size()); }

            /**\brief Check if any vertices have tag */
            bool tag_has_vertex_data(size_t index, Mesquite2::MsqError& err);
            /**\brief Check if any elements have tag */
            bool tag_has_element_data(size_t index, Mesquite2::MsqError& err);

        private:

            friend class MeshMesquiteTag::TagIterator;

            std::vector<TagData*> tagList;
        }; // class MeshImplTags

        }
    }

#endif
