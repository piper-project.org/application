/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHQUALITYABSTRACT_PIPER_H
#define MESHQUALITYABSTRACT_PIPER_H

#include "hbm/types.h"
#include "hbm/VtkSelectionTools.h"

#include <array>
#include <list>

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define MESHOPTIMIZER_EXPORT
#endif

namespace piper {
    namespace hbm {
        class FEModel;
    }
    namespace meshoptimizer {
        enum class QualityMetrics {
            VTK_SCALED_JACOBIAN=0,
            VTK_SIZE_SHAPE,
            MESQUITE_UntangleBeta,
            MESQUITE_InverseMeanRatio,
            MESQUITE_ConditionNumber,
            MESQUITE_Inverted,
			MSHDMGDETECTOR_DihedralAngleThreshold,
			MSHDMGDETECTOR_ClusterSizeBoundaryPercentage,
            SIZE,
        };
        static const std::array<std::string, 7> QualityMetrics_str = {
            { "VTK_SCALED_JACOBIAN", "VTK_SIZE_SHAPE",
            "MESQUITE_UntangleBeta", "MESQUITE_InverseMeanRatio", "MESQUITE_ConditionNumber", "MESQUITE_Inverted" }
        };

        enum class DIM;
        class MESHOPTIMIZER_EXPORT meshQualityAbstract {
        public:

            meshQualityAbstract();
            meshQualityAbstract(DIM dim) : m_dim(dim) {};
            virtual ~meshQualityAbstract() {
                int a = 1;

            };

            virtual void setDim(DIM dim) { m_dim = dim; };
            virtual void reset() = 0;
            virtual void setMesh(hbm::FEModel& fem) {
                m_fem = &fem;
            };            
            /// <summary>
            /// Specifies the baseline model so that the quality computer can extract whatever information it needs from it.
            /// The mesh is not neccessarily stored or copied though, so changing it after calling this method may not change
            /// the information that was extracted by the quality computer. I.e. this method should be called directly before
            /// calling the operation that uses the information.
            /// </summary>
            /// <param name="fem">The fem from which to extract all required data.</param>
            virtual void processBaselineMesh(hbm::FEModel& fem) {};
            virtual void setEntitiesToProcess(std::list<std::string> entityNames) {
                entitiesToProcess = entityNames;
            };
            virtual std::list<std::string> getEntitiesToProcess() {
                return entitiesToProcess;
            };
            virtual void computeQuality()=0;
            virtual void writeQualityResults(std::string const& filename)=0;
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) =0;
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) = 0;
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) = 0;
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) = 0;
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, std::vector<double>& metricvalue) = 0;
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue) = 0;
            virtual double getMaxValueMetric3D(meshoptimizer::QualityMetrics const& metric) = 0;
            virtual double getMinValueMetric3D(meshoptimizer::QualityMetrics const& metric) = 0;
			/// Finds all 3D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
			virtual bool findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
				std::function<bool(double, double)> compareOperation)  = 0;

			/// Finds all 2D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
			virtual bool findmetrics_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
				std::function<bool(double, double)> compareOperation)  = 0;
            
            /// <summary>
            /// Defines whether the quality should be computed relative to the baseline model.
            /// Call this before calling processBaselineMesh, otherwise the baseline mesh will be ignored (i.e. if this is set to false,
            /// there is no reason to waste time calling the processBaselineMesh with some optimizers so it will be skipped)
            /// </summary>
            /// <param name="compareToBaseline">If set to <c>true</c>, quality computed by this optimizer will be relative to the set baseline mesh.</param>
            void setQualityUseBaselineModel(bool compareToBaseline) { this->qualityCompareToBaseline = compareToBaseline; }

			bool findmetricsbelow_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_3D(metric, threshold, vid, std::less<double>());
			};

			bool findmetricsabove_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_3D(metric, threshold, vid, std::greater_equal<double>());
			};

			bool findmetricsequal_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_3D(metric, threshold, vid, std::equal_to<double>());
			};

			bool findmetricsbelow_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_2D(metric, threshold, vid, std::less<double>());
			};

			bool findmetricsabove_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_2D(metric, threshold, vid, std::greater_equal<double>());
			};

			bool findmetricsequal_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid) {
				return findmetrics_2D(metric, threshold, vid, std::equal_to<double>());
			};
            
            /// <summary>
            /// Gets the selection boxes - these are boxes generated around low quality elements or other area of interest.
            /// They may serve for further processing or simply as a highlighting mechanism.
            /// </summary>
            /// <param name="mergeOverlapping">If set to <c>true</c>, overlapping boxes will be merged.</param>
            /// <returns>
            /// List of boxes around interesting areas of the mesh.
            /// </returns>
            virtual boost::container::vector<vtkOBBNodePtr> generateSelectionBoxes(bool mergeOverlapping);

        protected:
            DIM m_dim;
            hbm::FEModel* m_fem; // the current mesh
            std::set<QualityMetrics> m_validmetrics;
            std::list<std::string> entitiesToProcess; // names of entities to process if only some should be processed
            bool qualityCompareToBaseline = false; // whether quality should be computed relative to the baseline model or absolute

            bool isValidMetrics(QualityMetrics const& metric) const;
        };



    }//meshoptimizer
}//piper



#endif
