/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshDamageDetector.h"

#include <vtkWindowedSincPolyDataFilter.h>

#include <vtkFloatArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkObjectFactory.h>
#include <vtkPointData.h>
#include <vtkTriangle.h>
#include <vtkTriangleFilter.h>

#include <vtkExtractSelectedIds.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>

using namespace boost::container;


namespace piper {
	namespace meshoptimizer {

		meshDamageDetector::meshDamageDetector(double dihedralAngleDifThreshold, double damagedClusterSizeDef) : meshQualityAbstract()
		{
			origNormals = nullptr;
			deformed = nullptr;
			this->dihedralAngleDifThreshold = dihedralAngleDifThreshold;
			this->damagedClusterSizeDef = damagedClusterSizeDef;
		}

		meshDamageDetector::meshDamageDetector(vtkPolyData *original, double dihedralAngleDifThreshold, double damagedClusterSizeDef)
		{
			this->deformed = deformed;
			this->dihedralAngleDifThreshold = dihedralAngleDifThreshold;
			this->damagedClusterSizeDef = damagedClusterSizeDef;
		}


		meshDamageDetector::~meshDamageDetector()
		{
		}

		void meshDamageDetector::reset()
		{
            origNormals = nullptr;
			deformed = nullptr;
			clusters.clear();
			damagedClusters.clear();
		}

		void meshDamageDetector::SetDeformedModel(vtkPolyData *deformed) { this->deformed = deformed; }
		vtkSmartPointer<vtkPolyData> meshDamageDetector::GetDeformedModelPolys() { return this->deformed; }
		piper::hbm::FEModelVTK *meshDamageDetector::GetDeformedModel() { return this->m_model; }

		void meshDamageDetector::SetDihedralAngleDifThreshold(double threshold) { this->dihedralAngleDifThreshold = threshold; }
		void meshDamageDetector::SetDamageSizeDef(double damagedClusterSizeDef) { this->damagedClusterSizeDef = damagedClusterSizeDef; }

		vector<vector<int>> meshDamageDetector::GetClusters() { return this->clusters; }
		vector<int> meshDamageDetector::GetDamagedClustersIDs() { return this->damagedClusters; }

        void meshDamageDetector::processBaselineMesh(hbm::FEModel& fem)
        {
            bool spy = fem.setMeshDef_INDEX();

            vtkSmartPointer<vtkPolyData> original = vtkPolyData::New();
            original->SetPoints(fem.getFEModelVTK()->getVTKMesh()->GetPoints()); // the points are the same across all entities
            original->Allocate();
            for (auto it = entitiesToProcess.begin(); it != entitiesToProcess.end(); it++)
            {
                vtkSmartPointer<vtkPolyData> ent = fem.getFEModelVTK()->getEntity2DElements(*it);
                if (ent != NULL)
                {
                    // push back the cells of the entity to "original"
                    for (vtkIdType i = 0; i < ent->GetNumberOfCells(); i++)
                    {
                        vtkCell *cell = ent->GetCell(i);
                        original->InsertNextCell(cell->GetCellType(), cell->GetPointIds());
                    }
                }
            }
            hbm::vtkSelectionTools::computeNormalizedNormalsInPlace(original);
            origNormals = vtkDoubleArray::SafeDownCast(original->GetCellData()->GetNormals());
            if (spy) fem.setMeshDef_NID();
        }

        boost::container::vector<vtkOBBNodePtr> meshDamageDetector::generateSelectionBoxes(bool mergeOverlapping)
        {
            // for each damaged cluster, compute its bounding box
            vector<vtkSmartPointer<vtkUnstructuredGrid>> damagedClusterMeshes = CreateDamagedClustersAsUnstrGrids();
            vtkSmartPointer<hbm::vtkSelectionTools> st = vtkSmartPointer<hbm::vtkSelectionTools>::New();
            return st->GenerateOBBs(damagedClusterMeshes, mergeOverlapping);
        }

		/// <summary>
		/// Creates a new vtkUnstructuredGrid instance for each of the damaged clusters that contains only points and cells of the damaged elements.
		/// </summary>
		/// <returns>An array of pointers to the meshes representing the damaged clusters.
		/// Note that each has separate and unique points and cells arrays, so their indices do not match the indices of the parent mesh.</returns>
		vector<vtkSmartPointer<vtkUnstructuredGrid>> meshDamageDetector::CreateDamagedClustersAsUnstrGrids()
		{
			vector<vtkSmartPointer<vtkUnstructuredGrid>> ret;

			vtkSmartPointer<vtkSelection> selector = vtkSmartPointer<vtkSelection>::New();
			vtkSmartPointer<vtkSelectionNode> selectNode = vtkSmartPointer<vtkSelectionNode>::New();

			selectNode->SetFieldType(vtkSelectionNode::CELL);
			selectNode->SetContentType(vtkSelectionNode::INDICES);
			selector->AddNode(selectNode);
			for (auto iter = damagedClusters.begin(); iter != damagedClusters.end(); iter++)
			{
				auto currCluster = clusters[*iter];
				vtkSmartPointer<vtkIdTypeArray> ids =
					vtkSmartPointer<vtkIdTypeArray>::New();
				ids->SetNumberOfComponents(1);

				// Set elements
				for (auto it = currCluster.begin(); it != currCluster.end(); it++)
					ids->InsertNextValue(*it);
				selectNode->SetSelectionList(ids);

				// extract the selected cells from the deformed mesh
				vtkSmartPointer<vtkExtractSelectedIds> extractor = vtkSmartPointer<vtkExtractSelectedIds>::New();
				extractor->SetInputData(0, deformed);
				extractor->SetInputData(1, selector);
				extractor->Update();

				vtkSmartPointer<vtkUnstructuredGrid> clusterMesh = vtkSmartPointer<vtkUnstructuredGrid>::New();
				clusterMesh->DeepCopy(extractor->GetOutput()); // make a copy of the result
				ret.push_back(clusterMesh); // store the pointer to the result
			}
			return ret;
		}

		void meshDamageDetector::computeQuality()
		{
            if (m_fem == nullptr || origNormals == nullptr)	return;
            std::map<std::string, vtkSmartPointer<vtkPolyData>> selectedEntities;
			// create geometrical representation of the fem mesh
			m_model = m_fem->getFEModelVTK();
            deformed = vtkPolyData::New();
            deformed->SetPoints(m_model->getVTKMesh()->GetPoints()); // the points are the same across all entities
            deformed->Allocate();
            for (auto it = entitiesToProcess.begin(); it != entitiesToProcess.end(); it++)
            {
                vtkSmartPointer<vtkPolyData> ent = m_model->getEntity2DElements(*it);
                if (ent != NULL)
                {
                    selectedEntities[*it] = ent;
                    // push back the cells of the entity to "deformed"
                    for (vtkIdType i = 0; i < ent->GetNumberOfCells(); i++)
                    {
                        vtkCell *cell = ent->GetCell(i);
                        deformed->InsertNextCell(cell->GetCellType(), cell->GetPointIds());
                    }
                }
            }
            if (selectedEntities.size() > 0) // process only if the target entity exists
            {
                generateClusters();
                identifyDamagedClusters();
                // set the point selection to the full mesh
                auto selectedPointData = hbm::vtkSelectionTools::ObtainSelectionArrayPoints(deformed, false);
                m_model->getVTKMesh()->GetPointData()->AddArray(selectedPointData);
                vtkSmartPointer<vtkBitArray> origMeshselection = hbm::vtkSelectionTools::ObtainSelectionArrayCells(m_model->getVTKMesh(), true);
                vtkSmartPointer<vtkBitArray> detected_cells = hbm::vtkSelectionTools::ObtainSelectionArrayCells(deformed, false);
                int cellGlobalIndex = 0; 
                for (auto it = selectedEntities.begin(); it != selectedEntities.end(); it++) // for each processed entity, update cell and point selection according to the results
                {
                    // if the mapping to the original mesh exists, mark the selected cells also in the original mesh
                    vtkSmartPointer<vtkIdTypeArray> origCells = vtkIdTypeArray::SafeDownCast(it->second->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                    if (origCells)
                    {
                        vtkSmartPointer<vtkBitArray> origMeshselectionEntity = hbm::vtkSelectionTools::ObtainSelectionArrayCells(m_model->getEntity(it->first), true);
                        vtkSmartPointer<vtkIdTypeArray> origCellsEntity = vtkIdTypeArray::SafeDownCast(m_model->getEntity(it->first)->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                        for (int i = 0; i < it->second->GetNumberOfCells(); i++)
                        {
                            if (detected_cells->GetValue(i + cellGlobalIndex) == IS_PRIMITIVE_SELECTED) // if the cell is selected in the aggregated "deformed" polydata
                            {
                                origMeshselectionEntity->SetValue(origCells->GetValue(i), IS_PRIMITIVE_SELECTED); // mark it in the entity
                                origMeshselection->SetValue(origCellsEntity->GetValue(origCells->GetValue(i)), IS_PRIMITIVE_SELECTED); // mark it in the full mesh
                            }
                        }
                        cellGlobalIndex += it->second->GetNumberOfCells(); // move onto the next entity in the aggregated "deformed" polydata
                    }
                    m_model->getEntity(it->first)->GetPointData()->AddArray(selectedPointData); // the points are the same for all data sub-sets -> the same point selection array as well
                }
            }
		}

		void meshDamageDetector::generateClusters()
		{
			// check if models have normals, if not, compute them
			hbm::vtkSelectionTools::computeNormalizedNormalsInPlace(deformed);

            vtkSmartPointer<vtkDoubleArray> defNormals = vtkDoubleArray::SafeDownCast(deformed->GetCellData()->GetNormals());
            int noOfElem = deformed->GetNumberOfCells();

			// build adjacency information
			deformed->BuildLinks();

			vtkSmartPointer<vtkIntArray> isBoundary = vtkSmartPointer<vtkIntArray>::New(); // map of elements on the boundary of their clusters
			isBoundary->SetNumberOfValues(noOfElem); // for each element, -1 if is not, ID of the neighbor element if it is
			isBoundary->SetName(IS_ON_CLUSTER_BOUND);
			for (int i = 0; i < noOfElem; i++)
				isBoundary->SetValue(i, -1); // init each elem as non-boundary

			clusters.clear(); // resets the clusters array

			vector<int> newCluster;
			newCluster.push_back(0); // put the first element into the first cluster - a starting point
			clusters.push_back(newCluster);

			// map of processed elements - used if there are more components in the mesh and need to restart the process from unprocessed element
			bool *processedEleMap = new bool[noOfElem]; // map of processed elements
			for (int i = 0; i < noOfElem; i++) processedEleMap[i] = false;
			int lastSeed = 0; // a pointer to the last element from which the clustering was started

			// map of element that have been definately assigned to some cluster, even though they might have not been "processed" yet
			// processed means that the neighbors of that element were checked against that element
			// this is to prevent the same element being added to the same cluster multiple times (it can be visited from more elements)
			// note that this flag is not set to true for elements that are being used to create new clusters (because the dihedral angle check failed)
			// because the new clusters may not be definite - they can be merged with other new clusters. so set this only 
			// when the element is being added due to the dihedral angle check being positive (less than threshold)
			bool *assignedEleMap = new bool[noOfElem]; // map of processed elements
			for (int i = 0; i < noOfElem; i++) assignedEleMap[i] = false;
				assignedEleMap[0] = true; // first element will not move from the first cluster

			vtkSmartPointer<vtkIdList> neighborIDs = vtkSmartPointer<vtkIdList>::New();
			// put all elements into clusters. noOfProcessedElem should always be equal to amout of true values in processedEleMap
			for (int noOfProcessedElem = 0, currClusterID = 0, currClusterEnd = 0; noOfProcessedElem < noOfElem; )
			{
				if (currClusterEnd >= clusters[currClusterID].size()) // move to next cluster if all elem have been processed from the current one
				{
					currClusterEnd = 0;
					currClusterID++; // next cluster will contain an item, or all ele have been processed, or there are more components in the mesh

					while (true) // iterate until we find a suitable cluster with which to continue processing
					{
						// new cluster will always have one element or be nonexistent
						// if they dont exist, it means there are more components, create a new cluster based on one so far unprocessed element
						if (currClusterID >= clusters.size())
						{
						//	std::cout << "need to create new cluster ";
							while (processedEleMap[lastSeed]) lastSeed++; // finds the first unprocessed element
							newCluster.clear();
							newCluster.push_back(lastSeed); // and sets it as a beggining of the empty cluster
							clusters.push_back(newCluster);
							break; // process this cluster
						}
						else if (processedEleMap[clusters[currClusterID][0]]) // else there is an element, but we need to check if the element has not been already added to other cluster
						{ //if the element has already been processed, it means it has already been assigned to a different cluster - remove this cluster
							// (this can happen when the same triangle is met from two different ones and each of them put it in a new cluster before it is processed - 
							// then it gets processed from one of the clusters and then we meet it again)
							clusters.erase(clusters.begin() + currClusterID);
						}
						else break; // the cluster has an element and it has not been processed yet, process it
					}
				}

				int currCellID = clusters[currClusterID][currClusterEnd];

				vtkCell *currCell = deformed->GetCell(currCellID); // pts and npts contain the current element now

				double n1[3], n2[3];
				// get the neighbors of the element - for each edge, get the edge neighbor
				for (int i = 0; i < currCell->GetNumberOfEdges(); i++) 
				{
					// it will always add at least 1 neighbor assuming there are no boundaries
					deformed->GetCellEdgeNeighbors(currCellID, currCell->GetEdge(i)->GetPointId(0), currCell->GetEdge(i)->GetPointId(1), neighborIDs);

					//process the neighbours, in case there is a boundary, noOfNei == 0, so just continue
					for (int j = 0; j < neighborIDs->GetNumberOfIds(); j++) // the mesh does not have to be manifold - more than one neighbor is possible
					{
						int neiID = neighborIDs->GetId(j);

						// if this element was already processed or assigned to the same cluster, no point in processing it again 
						// (this will happen when two neighbours on the boundary of two clusters meet)
						if (!processedEleMap[neiID] && !assignedEleMap[neiID])
                        { // if it was not proceesed yet, do it

                            origNormals->GetTupleValue(currCellID, n1);
                            origNormals->GetTupleValue(neiID, n2);
                            double origDihedralAngle = vtkMath::Dot(n1, n2); //cosine of dihedral angle

                            defNormals->GetTupleValue(currCellID, n1);
                            defNormals->GetTupleValue(neiID, n2);
                            double defDihedralAngle = vtkMath::Dot(n1, n2); //cosine of dihedral angle

                            if (fabs((acos(origDihedralAngle) - acos(defDihedralAngle))) > dihedralAngleDifThreshold)
                            { // difference is bigger than tolerated, put it new cluster
								newCluster.clear();
								newCluster.push_back(neiID);
								clusters.push_back(newCluster);
								// mark both elements as boundary - set the ID of the neighbor
								isBoundary->SetValue(currCellID, neiID);
								isBoundary->SetValue(neiID, currCellID);
							}
							else
							{ // difference is acceptable, put it in this cluster
								assignedEleMap[neiID] = true; // mark that this element already has a cluster it belongs to that will definately not change
								clusters[currClusterID].push_back(neiID);
							}
						}
					}
				}
				processedEleMap[currCellID] = true;
				noOfProcessedElem++;
				currClusterEnd++; // move to the next element in the list
			}

			delete[]processedEleMap;
			delete[]assignedEleMap;
			deformed->GetCellData()->AddArray(isBoundary);
		}

		void meshDamageDetector::identifyDamagedClusters()
		{
			vtkSmartPointer<vtkIntArray> isBoundary = vtkIntArray::SafeDownCast(deformed->GetCellData()->GetArray(IS_ON_CLUSTER_BOUND));
			damagedClusters.clear();

			// initialize array for marking damaged vertices and cells
			vtkSmartPointer<vtkBitArray> selectedPoints = piper::hbm::vtkSelectionTools::ObtainSelectionArrayPoints(deformed, true);

			vtkSmartPointer<vtkIntArray> cellClusterAssociation = vtkSmartPointer<vtkIntArray>::New();
			cellClusterAssociation->SetNumberOfValues(deformed->GetNumberOfCells());
			for (int i = 0; i < deformed->GetNumberOfCells(); i++)
				cellClusterAssociation->SetValue(i, IS_NOT_PRIMITIVE_SELECTED); // in the beggining, mark all cells as not selected

			for (int i = 0; i < clusters.size(); i++)
			{
				int noOfBoundaryEle = 0;
				for (int j = 0; j < clusters[i].size(); j++)
				{
					// -1 marks internal elements, positive marks the neighbor elem ID. if the information is not used anywhere, maybe change it to bool for faster processing
					if (isBoundary->GetValue(clusters[i][j]) > -1) noOfBoundaryEle++;
				}
				if (((double)noOfBoundaryEle / clusters[i].size()) > damagedClusterSizeDef) // more than allowed percentage of elements is on the boundary
				{
					damagedClusters.push_back(i); // store the cluster ID

					// mark the vertices and cells of this cluster in the output mesh
					for (auto it = clusters[i].begin(); it != clusters[i].end(); it++) // for each cell in the cluster
					{
						cellClusterAssociation->SetValue(*it, i); // mark the cell as selected but use cluster ID instead of the default 1 - useful in merging
						vtkIdList *nodes = deformed->GetCell(*it)->GetPointIds();
						for (int j = 0; j < nodes->GetNumberOfIds(); j++) // for each node of the cell
							selectedPoints->SetValue(nodes->GetId(j), IS_PRIMITIVE_SELECTED); // mark the vertex as selected 
					}
				}
			}

			// second pass - merge neighbouring damaged clusters - we dont care that they are damaged in regards to each other (have high angle difference),
			// if they are damaged and neighbouring, lets make them as one cluster
			vtkSmartPointer<vtkIdList> neighborIDs = vtkSmartPointer<vtkIdList>::New();
			for (int currClIndex = 0; currClIndex < damagedClusters.size(); currClIndex++) // for each damaged cluster
			{
				auto currCluster = clusters[damagedClusters[currClIndex]];
				for (int c = 0; c < currCluster.size(); c++) // for each element in that cluster
				{
					int currCellIndex = currCluster[c];
					if (isBoundary->GetValue(currCellIndex) > -1) // it is a boundary element, let's check if its neighbours are also part of a damaged cluster
					{
						vtkCell *currCell = deformed->GetCell(currCellIndex);
						int boundaryCount = currCell->GetNumberOfEdges(); // start by assuming it is boundary across all edges
						// get the neighbors of the element - for each edge, get the edge neighbor
						for (int i = 0; i < currCell->GetNumberOfEdges(); i++)
						{
							deformed->GetCellEdgeNeighbors(currCellIndex, currCell->GetEdge(i)->GetPointId(0), currCell->GetEdge(i)->GetPointId(1), neighborIDs);
							bool hasAtLeastOneDamagedNeighbour = false;
							//process the neighbours
							for (int j = 0; j < neighborIDs->GetNumberOfIds(); j++) // the mesh does not have to be manifold - more than one neighbor is possible
							{
								int neiID = neighborIDs->GetId(j);
								int neiCellClusterAssocIndex = cellClusterAssociation->GetValue(neiID);
								if (neiCellClusterAssocIndex != IS_NOT_PRIMITIVE_SELECTED) // merge if the neighbor is selected as damaged and belogns to a different cluster
								{
									hasAtLeastOneDamagedNeighbour = true;
									if (neiCellClusterAssocIndex != damagedClusters[currClIndex])
									{
										// merge clusters currClIndex and cellClusterAssociation->GetValue(neiID). 
										// the second one should not have been processed yet - the damageClusters array is implicitly ordered as ascending

										// renumber the value in selectedCells for the cluster that is being concatenated
										for (auto it = clusters[neiCellClusterAssocIndex].begin(); it != clusters[neiCellClusterAssocIndex].end(); it++)
											cellClusterAssociation->SetValue(*it, damagedClusters[currClIndex]); // set the current ID instead

										// do the merge
										vector<int> concatVector;
										concatVector.reserve(currCluster.size() + clusters[neiCellClusterAssocIndex].size());
										concatVector.insert(concatVector.end(), currCluster.begin(), currCluster.end());
										concatVector.insert(concatVector.end(), clusters[neiCellClusterAssocIndex].begin(), clusters[neiCellClusterAssocIndex].end());
										clusters[damagedClusters[currClIndex]] = concatVector;
										currCluster = clusters[damagedClusters[currClIndex]];

										// erase everything from the merged cluster
										clusters[neiCellClusterAssocIndex].clear();
									}
								}
							}
							if (hasAtLeastOneDamagedNeighbour) boundaryCount--;
						}
						if (boundaryCount <= 0)
							isBoundary->SetValue(currCellIndex, -1); // it is no longer on the boundary of the cluster
					}
				}
			}

            vtkSmartPointer<vtkBitArray> selectedCells = piper::hbm::vtkSelectionTools::ObtainSelectionArrayCells(deformed, false);

			// remove the now empty clusters (remnants of the one that were merged) and reindex them correctly in the damagedClusters vector
			int indexChange = 0;
			for (int currClIndex = 0; currClIndex  < damagedClusters.size(); currClIndex++) // for each damaged cluster
			{
				damagedClusters[currClIndex] = damagedClusters[currClIndex] - indexChange;
				if (clusters[damagedClusters[currClIndex]].empty()) // if its empty, remove it
				{
					clusters.erase(clusters.begin() + damagedClusters[currClIndex]);
					damagedClusters.erase(damagedClusters.begin() + currClIndex);
					indexChange++; // following clusters will have to change their indices
					currClIndex--; // stay at this position of the vector - another cluster fall through to this index (which will be incremented by the for loop)
				}
				else
				{
					// set the SELECTED_PRIMITIVES cell values to the default +1/-1
					for (auto it = clusters[damagedClusters[currClIndex]].begin(); it != clusters[damagedClusters[currClIndex]].end(); it++)
						selectedCells->SetValue(*it, cellClusterAssociation->GetValue(*it) == IS_NOT_PRIMITIVE_SELECTED ?
							IS_NOT_PRIMITIVE_SELECTED : IS_PRIMITIVE_SELECTED);
				}
			}
		}

// The following code region is an almost exact copy of a VTK library source code "vtkWindowedSincPolyDataFilter.cxx", with only several small changes.
// As per VTK's copyright requirements, the VTK's copyright (see notice below) regarding source code re-distribution also applies on this code.

/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkWindowedSincPolyDataFilter.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#pragma region vtkWindowedSincPolyDataFilterSelective


#define VTK_SIMPLE_VERTEX 0
#define VTK_FIXED_VERTEX 1
#define VTK_FEATURE_EDGE_VERTEX 2
#define VTK_BOUNDARY_EDGE_VERTEX 3

        vtkStandardNewMacro(vtkWindowedSincPolyDataFilterSelective);

		// Special structure for marking vertices
		typedef struct _vtkMeshVertex
		{
			char      type;
			vtkIdList *edges; // connected edges (list of connected point ids)
		} vtkMeshVertex, *vtkMeshVertexPtr;

		int vtkWindowedSincPolyDataFilterSelective::RequestData(
			vtkInformation *vtkNotUsed(request),
			vtkInformationVector **inputVector,
			vtkInformationVector *outputVector)
		{
			// get the info objects
			vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
			vtkInformation *outInfo = outputVector->GetInformationObject(0);

			// get the input and output
			vtkPolyData *input = vtkPolyData::SafeDownCast(
				inInfo->Get(vtkDataObject::DATA_OBJECT()));
			vtkPolyData *output = vtkPolyData::SafeDownCast(
				outInfo->Get(vtkDataObject::DATA_OBJECT()));

			vtkIdType numPts, numCells, numPolys, numStrips, i;
			int j, k;
			vtkIdType npts = 0;
			vtkIdType *pts = 0;
			vtkIdType p1, p2;
			double x[3], y[3], deltaX[3], xNew[3];
			double x1[3], x2[3], x3[3], l1[3], l2[3];
			double CosFeatureAngle; //Cosine of angle between adjacent polys
			double CosEdgeAngle; // Cosine of angle between adjacent edges
			int iterationNumber;
			vtkIdType numSimple = 0, numBEdges = 0, numFixed = 0, numFEdges = 0;
			vtkPolyData *inMesh = NULL, *Mesh;
			vtkPoints *inPts;
			vtkTriangleFilter *toTris = NULL;
			vtkCellArray *inVerts, *inLines, *inPolys, *inStrips;
			vtkPoints *newPts[4];
			vtkMeshVertexPtr Verts;

			// variables specific to windowed sinc interpolation
			double theta_pb, k_pb, sigma, p_x0[3], p_x1[3], p_x3[3];
			double *w, *c, *cprime;
			int zero, one, two, three;

			//
			// Check input
			//
			numPts = input->GetNumberOfPoints();
			numCells = input->GetNumberOfCells();
			if (numPts < 1 || numCells < 1)
			{
				vtkErrorMacro(<< "No data to smooth!");
				return 1;
			}

			CosFeatureAngle = cos(vtkMath::RadiansFromDegrees(this->FeatureAngle));
			CosEdgeAngle = cos(vtkMath::RadiansFromDegrees(this->EdgeAngle));

			vtkDebugMacro(<< "Smoothing " << numPts << " vertices, " << numCells
				<< " cells with:\n"
				<< "\tIterations= " << this->NumberOfIterations << "\n"
				<< "\tPassBand= " << this->PassBand << "\n"
				<< "\tEdge Angle= " << this->EdgeAngle << "\n"
				<< "\tBoundary Smoothing "
				<< (this->BoundarySmoothing ? "On\n" : "Off\n")
				<< "\tFeature Edge Smoothing "
				<< (this->FeatureEdgeSmoothing ? "On\n" : "Off\n")
				<< "\tNonmanifold Smoothing "
				<< (this->NonManifoldSmoothing ? "On\n" : "Off\n")
				<< "\tError Scalars "
				<< (this->GenerateErrorScalars ? "On\n" : "Off\n")
				<< "\tError Vectors "
				<< (this->GenerateErrorVectors ? "On\n" : "Off\n"));

			if (this->NumberOfIterations <= 0) //don't do anything!
			{
				output->CopyStructure(input);
				output->GetPointData()->PassData(input->GetPointData());
				output->GetCellData()->PassData(input->GetCellData());
				vtkWarningMacro(<< "Number of iterations == 0: passing data through unchanged");
				return 1;
			}

			// SELECTIVE SMOOTHING
			vtkSmartPointer<vtkBitArray> selectedPoints = vtkBitArray::SafeDownCast(input->GetPointData()->GetArray(SELECTED_PRIMITIVES));


			//
			// Peform topological analysis. What we're gonna do is build a connectivity
			// array of connected vertices. The outcome will be one of three
			// classifications for a vertex: VTK_SIMPLE_VERTEX, VTK_FIXED_VERTEX. or
			// VTK_EDGE_VERTEX. Simple vertices are smoothed using all connected
			// vertices. FIXED vertices are never smoothed. Edge vertices are smoothed
			// using a subset of the attached vertices.
			//
			vtkDebugMacro(<< "Analyzing topology...");
			Verts = new vtkMeshVertex[numPts];
			for (i = 0; i<numPts; i++)
			{
				Verts[i].type = VTK_SIMPLE_VERTEX; //can smooth
				Verts[i].edges = NULL;
			}

			inPts = input->GetPoints();

			// check vertices first. Vertices are never smoothed_--------------
			for (inVerts = input->GetVerts(), inVerts->InitTraversal();
				inVerts->GetNextCell(npts, pts);)
			{
				for (j = 0; j<npts; j++)
				{
					Verts[pts[j]].type = VTK_FIXED_VERTEX;
				}
			}

			this->UpdateProgress(0.10);

			// now check lines. Only manifold lines can be smoothed------------
			for (inLines = input->GetLines(), inLines->InitTraversal();
				inLines->GetNextCell(npts, pts);)
			{
				for (j = 0; j<npts; j++)
				{
					if (Verts[pts[j]].type == VTK_SIMPLE_VERTEX)
					{
						if (j == (npts - 1)) //end-of-line marked FIXED
						{
							Verts[pts[j]].type = VTK_FIXED_VERTEX;
						}
						else if (j == 0) //beginning-of-line marked FIXED
						{
							Verts[pts[0]].type = VTK_FIXED_VERTEX;
							inPts->GetPoint(pts[0], x2);
							inPts->GetPoint(pts[1], x3);
						}
						else //is edge vertex (unless already edge vertex!)
						{
							Verts[pts[j]].type = VTK_FEATURE_EDGE_VERTEX;
							Verts[pts[j]].edges = vtkIdList::New();
							Verts[pts[j]].edges->SetNumberOfIds(2);
							//Verts[pts[j]].edges = new vtkIdList(2,2);
							Verts[pts[j]].edges->SetId(0, pts[j - 1]);
							Verts[pts[j]].edges->SetId(1, pts[j + 1]);
						}
					} //if simple vertex

					else if (Verts[pts[j]].type == VTK_FEATURE_EDGE_VERTEX)
					{ //multiply connected, becomes fixed!
						Verts[pts[j]].type = VTK_FIXED_VERTEX;
						Verts[pts[j]].edges->Delete();
						Verts[pts[j]].edges = NULL;
					}

				} //for all points in this line
			} //for all lines

			this->UpdateProgress(0.25);

			// now polygons and triangle strips-------------------------------
			inPolys = input->GetPolys();
			numPolys = inPolys->GetNumberOfCells();
			inStrips = input->GetStrips();
			numStrips = inStrips->GetNumberOfCells();

			if (numPolys > 0 || numStrips > 0)
			{ //build cell structure
				vtkCellArray *polys;
				vtkIdType cellId;
				int numNei, nei, edge;
				vtkIdType numNeiPts;
				vtkIdType *neiPts;
				double normal[3], neiNormal[3];
				vtkIdList *neighbors;

				inMesh = vtkPolyData::New();
				inMesh->SetPoints(inPts);
				inMesh->SetPolys(inPolys);
				Mesh = inMesh;
				neighbors = vtkIdList::New();
				neighbors->Allocate(VTK_CELL_SIZE);

				if ((numStrips = inStrips->GetNumberOfCells()) > 0)
				{ // convert data to triangles
					inMesh->SetStrips(inStrips);
					toTris = vtkTriangleFilter::New();
					toTris->SetInputData(inMesh);
					toTris->Update();
					Mesh = toTris->GetOutput();
				}

				Mesh->BuildLinks(); //to do neighborhood searching
				polys = Mesh->GetPolys();

				for (cellId = 0, polys->InitTraversal(); polys->GetNextCell(npts, pts);
					cellId++)
				{
					for (i = 0; i < npts; i++)
					{
						p1 = pts[i];
						p2 = pts[(i + 1) % npts];

						if (Verts[p1].edges == NULL)
						{
							Verts[p1].edges = vtkIdList::New();
							Verts[p1].edges->Allocate(16, 6);
							// Verts[p1].edges = new vtkIdList(6,6);
						}
						if (Verts[p2].edges == NULL)
						{
							Verts[p2].edges = vtkIdList::New();
							Verts[p2].edges->Allocate(16, 6);
							// Verts[p2].edges = new vtkIdList(6,6);
						}

						Mesh->GetCellEdgeNeighbors(cellId, p1, p2, neighbors);
						numNei = neighbors->GetNumberOfIds();

						edge = VTK_SIMPLE_VERTEX;
						if (numNei == 0)
						{
							edge = VTK_BOUNDARY_EDGE_VERTEX;
						}

						else if (numNei >= 2)
						{
							// non-manifold case, check nonmanifold smoothing state
							if (!this->NonManifoldSmoothing)
							{
								// check to make sure that this edge hasn't been marked already
								for (j = 0; j < numNei; j++)
								{
									if (neighbors->GetId(j) < cellId)
									{
										break;
									}
								}
								if (j >= numNei)
								{
									edge = VTK_FEATURE_EDGE_VERTEX;
								}
							}
						}

						else if (numNei == 1 && (nei = neighbors->GetId(0)) > cellId)
						{
							if (this->FeatureEdgeSmoothing)
							{
								vtkPolygon::ComputeNormal(inPts, npts, pts, normal);
								Mesh->GetCellPoints(nei, numNeiPts, neiPts);
								vtkPolygon::ComputeNormal(inPts, numNeiPts, neiPts, neiNormal);

								if (vtkMath::Dot(normal, neiNormal) <= CosFeatureAngle)
								{
									edge = VTK_FEATURE_EDGE_VERTEX;
								}
							}
						}
						else // a visited edge; skip rest of analysis
						{
							continue;
						}

						if (edge && Verts[p1].type == VTK_SIMPLE_VERTEX)
						{
							Verts[p1].edges->Reset();
							Verts[p1].edges->InsertNextId(p2);
							Verts[p1].type = edge;
						}
						else if ((edge && Verts[p1].type == VTK_BOUNDARY_EDGE_VERTEX) ||
							(edge && Verts[p1].type == VTK_FEATURE_EDGE_VERTEX) ||
							(!edge && Verts[p1].type == VTK_SIMPLE_VERTEX))
						{
							Verts[p1].edges->InsertNextId(p2);
							if (Verts[p1].type && edge == VTK_BOUNDARY_EDGE_VERTEX)
							{
								Verts[p1].type = VTK_BOUNDARY_EDGE_VERTEX;
							}
						}

						if (edge && Verts[p2].type == VTK_SIMPLE_VERTEX)
						{
							Verts[p2].edges->Reset();
							Verts[p2].edges->InsertNextId(p1);
							Verts[p2].type = edge;
						}
						else if ((edge && Verts[p2].type == VTK_BOUNDARY_EDGE_VERTEX) ||
							(edge && Verts[p2].type == VTK_FEATURE_EDGE_VERTEX) ||
							(!edge && Verts[p2].type == VTK_SIMPLE_VERTEX))
						{
							Verts[p2].edges->InsertNextId(p1);
							if (Verts[p2].type && edge == VTK_BOUNDARY_EDGE_VERTEX)
							{
								Verts[p2].type = VTK_BOUNDARY_EDGE_VERTEX;
							}
						}
					}
				}

				//    delete inMesh; // delete this later, windowed sinc smoothing needs it
				if (toTris)
				{
					toTris->Delete();
				}
				neighbors->Delete();
			}//if strips or polys

			this->UpdateProgress(0.50);

			//post-process edge vertices to make sure we can smooth them
			for (i = 0; i<numPts; i++)
			{
				if (selectedPoints && selectedPoints->GetValue(i) == IS_NOT_PRIMITIVE_SELECTED)
					Verts[i].type = VTK_FIXED_VERTEX;

				if (Verts[i].type == VTK_SIMPLE_VERTEX)
				{
					numSimple++;
				}

				else if (Verts[i].type == VTK_FIXED_VERTEX)
				{
					numFixed++;
				}
				else if (Verts[i].type == VTK_FEATURE_EDGE_VERTEX ||
					Verts[i].type == VTK_BOUNDARY_EDGE_VERTEX)
				{ //see how many edges; if two, what the angle is

					if (!this->BoundarySmoothing &&
						Verts[i].type == VTK_BOUNDARY_EDGE_VERTEX)
					{
						Verts[i].type = VTK_FIXED_VERTEX;
						numBEdges++;
					}

					else if ((npts = Verts[i].edges->GetNumberOfIds()) != 2)
					{
						// can only smooth edges on 2-manifold surfaces
						Verts[i].type = VTK_FIXED_VERTEX;
						numFixed++;
					}

					else //check angle between edges
					{
						inPts->GetPoint(Verts[i].edges->GetId(0), x1);
						inPts->GetPoint(i, x2);
						inPts->GetPoint(Verts[i].edges->GetId(1), x3);

						for (k = 0; k<3; k++)
						{
							l1[k] = x2[k] - x1[k];
							l2[k] = x3[k] - x2[k];
						}
						if ((vtkMath::Normalize(l1) >= 0.0) && (vtkMath::Normalize(l2) >= 0.0)
							&& (vtkMath::Dot(l1, l2) < CosEdgeAngle))
						{
							numFixed++;
							Verts[i].type = VTK_FIXED_VERTEX;
						}
						else
						{
							if (Verts[i].type == VTK_FEATURE_EDGE_VERTEX)
							{
								numFEdges++;
							}
							else
							{
								numBEdges++;
							}
						}
					}//if along edge
				}//if edge vertex
			}//for all points

			vtkDebugMacro(<< "Found\n\t" << numSimple << " simple vertices\n\t"
				<< numFEdges << " feature edge vertices\n\t"
				<< numBEdges << " boundary edge vertices\n\t"
				<< numFixed << " fixed vertices\n\t");
			//
			// Perform Windowed Sinc function interpolation
			//
			vtkDebugMacro(<< "Beginning smoothing iterations...");

			// need 4 vectors of points
			zero = 0; one = 1; two = 2; three = 3;

			newPts[0] = vtkPoints::New();
			newPts[0]->SetNumberOfPoints(numPts);
			newPts[1] = vtkPoints::New();
			newPts[1]->SetNumberOfPoints(numPts);
			newPts[2] = vtkPoints::New();
			newPts[2]->SetNumberOfPoints(numPts);
			newPts[3] = vtkPoints::New();
			newPts[3]->SetNumberOfPoints(numPts);

			// Get the center and length of the input dataset
			double *inCenter = input->GetCenter();
			double inLength = input->GetLength();

			if (!this->NormalizeCoordinates)
			{
				for (i = 0; i<numPts; i++) //initialize to old coordinates
				{
					newPts[zero]->SetPoint(i, inPts->GetPoint(i));
				}
			}
			else
			{
				// center the data and scale to be within unit cube [-1, 1]
				double normalizedPoint[3];
				for (i = 0; i<numPts; i++) //initialize to old coordinates
				{
					inPts->GetPoint(i, normalizedPoint);
					for (j = 0; j<3; ++j)
					{
						normalizedPoint[j] = (normalizedPoint[j] - inCenter[j]) / inLength;
					}
					newPts[zero]->SetPoint(i, normalizedPoint);
				}
			}

			// Smooth with a low pass filter defined as a windowed sinc function.
			// Taubin describes this methodology is the IBM tech report RC-20404
			// (#90237, dated 3/12/96) "Optimal Surface Smoothing as Filter Design"
			// G. Taubin, T. Zhang and G. Golub. (Zhang and Golub are at Stanford
			// University)

			// The formulas here follow the notation of Taubin's TR, i.e.
			// newPts[zero], newPts[one], etc.

			// calculate weights and filter coefficients
			k_pb = this->PassBand;   // reasonable default for k_pb in [0, 2] is 0.1
			theta_pb = acos(1.0 - 0.5 * k_pb); // theta_pb in [0, M_PI/2]

			//vtkDebugMacro(<< "theta_pb = " << theta_pb);

			w = new double[this->NumberOfIterations + 1];
			c = new double[this->NumberOfIterations + 1];
			cprime = new double[this->NumberOfIterations + 1];

			double zerovector[3];
			zerovector[0] = zerovector[1] = zerovector[2] = 0.0;

			//
			// Calculate the weights and the Chebychev coefficients c.
			//

			// Windowed sinc function weights. This is for a Hamming window. Other
			// windowing function could be implemented here.
			for (i = 0; i <= (this->NumberOfIterations); i++)
			{
				w[i] = 0.54 + 0.46*cos(((double)i)*vtkMath::Pi()
					/ (double)(this->NumberOfIterations + 1));
			}

			// Calculate the optimal sigma (offset or fudge factor for the filter).
			// This is a Newton-Raphson Search.
			double f_kpb = 0.0, fprime_kpb;
			int done = 0;
			sigma = 0.0;

			for (j = 0; !done && (j<500); j++)
			{
				// Chebyshev coefficients
				c[0] = w[0] * (theta_pb + sigma) / vtkMath::Pi();
				for (i = 1; i <= this->NumberOfIterations; i++)
				{
					c[i] = 2.0*w[i] * sin(((double)i)*(theta_pb + sigma)) /
						(((double)i)*vtkMath::Pi());
				}

				// calculate the Chebyshev coefficients for the derivative of the filter
				cprime[this->NumberOfIterations] = 0.0;
				cprime[this->NumberOfIterations - 1] = 0.0;
				if (this->NumberOfIterations > 1)
				{
					cprime[this->NumberOfIterations - 2] = 2.0*(this->NumberOfIterations - 1)
						* c[this->NumberOfIterations - 1];
				}
				for (i = this->NumberOfIterations - 3; i >= 0; i--)
				{
					cprime[i] = cprime[i + 2] + 2.0*(i + 1)*c[i + 1];
				}
				// Evaluate the filter and its derivative at k_pb (note the discrepancy
				// of calculating the c's based on theta_pb + sigma and evaluating the
				// filter at k_pb (which is equivalent to theta_pb)
				f_kpb = 0.0;
				fprime_kpb = 0.0;
				f_kpb += c[0];
				fprime_kpb += cprime[0];
				for (i = 1; i <= this->NumberOfIterations; i++)
				{
					if (i == 1)
					{
						f_kpb += c[i] * (1.0 - 0.5*k_pb);
						fprime_kpb += cprime[i] * (1.0 - 0.5*k_pb);
					}
					else
					{
						f_kpb += c[i] * cos(((double)i)*acos(1.0 - 0.5*k_pb));
						fprime_kpb += cprime[i] * cos(((double)i)*acos(1.0 - 0.5*k_pb));
					}
				}
				// if f_kpb is not close enough to 1.0, then adjust sigma
				if (this->NumberOfIterations > 1)
				{
					if (fabs(f_kpb - 1.0) >= 1e-3)
					{
						sigma -= (f_kpb - 1.0) / fprime_kpb;   // Newton-Rhapson (want f=1)
					}
					else
					{
						done = 1;
					}
				}
				else
				{
					// Order of Chebyshev is 1. Can't use Newton-Raphson to find an
					// optimal sigma. Object will most likely shrink.
					done = 1;
					sigma = 0.0;
				}
			}
			if (fabs(f_kpb - 1.0) >= 1e-3)
			{
				vtkErrorMacro(<< "An optimal offset for the smoothing filter could not be found.  Unpredictable smoothing/shrinkage may result.");
			}

			// first iteration
			for (i = 0; i<numPts; i++)
			{
				if (Verts[i].edges != NULL &&
					(npts = Verts[i].edges->GetNumberOfIds()) > 0)
				{
					// point is allowed to move
					newPts[zero]->GetPoint(i, x); //use current points
					deltaX[0] = deltaX[1] = deltaX[2] = 0.0;

					// calculate the negative of the laplacian
					for (j = 0; j<npts; j++) //for all connected points
					{
						newPts[zero]->GetPoint(Verts[i].edges->GetId(j), y);
						for (k = 0; k<3; k++)
						{
							deltaX[k] += (x[k] - y[k]) / npts;
						}
					}
					// newPts[one] = newPts[zero] - 0.5 newPts[one]
					for (k = 0; k<3; k++)
					{
						deltaX[k] = x[k] - 0.5*deltaX[k];
					}
					newPts[one]->SetPoint(i, deltaX);

					// calculate newPts[three] = c0 newPts[zero] + c1 newPts[one]
					for (k = 0; k < 3; k++)
					{
						deltaX[k] = c[0] * x[k] + c[1] * deltaX[k];
					}
					if (Verts[i].type == VTK_FIXED_VERTEX)
					{
						newPts[three]->SetPoint(i, newPts[zero]->GetPoint(i));
					}
					else
					{
						newPts[three]->SetPoint(i, deltaX);
					}
				}//if can move point
				else
				{
					// point is not allowed to move, just use the old point...
					// (zero out the Laplacian)
					newPts[one]->SetPoint(i, zerovector);
					newPts[three]->SetPoint(i, newPts[zero]->GetPoint(i));
				}
			}//for all points

			// for the rest of the iterations
			for (iterationNumber = 2;
				iterationNumber <= this->NumberOfIterations;
				iterationNumber++)
			{
				if (iterationNumber && !(iterationNumber % 5))
				{
					this->UpdateProgress(0.5 + 0.5*iterationNumber / this->NumberOfIterations);
					if (this->GetAbortExecute())
					{
						break;
					}
				}

				for (i = 0; i<numPts; i++)
				{
					if (Verts[i].edges != NULL &&
						(npts = Verts[i].edges->GetNumberOfIds()) > 0)
					{
						// point is allowed to move
						newPts[zero]->GetPoint(i, p_x0); //use current points
						newPts[one]->GetPoint(i, p_x1);

						deltaX[0] = deltaX[1] = deltaX[2] = 0.0;

						// calculate the negative laplacian of x1
						for (j = 0; j<npts; j++)
						{
							newPts[one]->GetPoint(Verts[i].edges->GetId(j), y);
							for (k = 0; k<3; k++)
							{
								deltaX[k] += (p_x1[k] - y[k]) / npts;
							}
						}//for all connected points

						// Taubin:  x2 = (x1 - x0) + (x1 - x2)
						for (k = 0; k<3; k++)
						{
							deltaX[k] = p_x1[k] - p_x0[k] + p_x1[k] - deltaX[k];
						}
						newPts[two]->SetPoint(i, deltaX);

						// smooth the vertex (x3 = x3 + cj x2)
						newPts[three]->GetPoint(i, p_x3);
						for (k = 0; k<3; k++)
						{
							xNew[k] = p_x3[k] + c[iterationNumber] * deltaX[k];
						}
						if (Verts[i].type != VTK_FIXED_VERTEX)
						{
							newPts[three]->SetPoint(i, xNew);
						}
					}//if can move point
					else
					{
						// point is not allowed to move, just use the old point...
						// (zero out the Laplacian)
						newPts[one]->SetPoint(i, zerovector);
						newPts[two]->SetPoint(i, zerovector);
					}
				}//for all points

				// update the pointers. three is always three. all other pointers
				// shift by one and wrap.
				zero = (1 + zero) % 3;
				one = (1 + one) % 3;
				two = (1 + two) % 3;

			}//for all iterations or until converge

			// move the iteration count back down so that it matches the
			// actual number of iterations executed
			--iterationNumber;

			// set zero to three so the correct set of positions is outputted
			zero = three;

			delete[] w;
			delete[] c;
			delete[] cprime;

			vtkDebugMacro(<< "Performed " << iterationNumber << " smoothing passes");

			// if we scaled the data down to the unit cube, then scale data back
			// up to the original space
			if (this->NormalizeCoordinates)
			{
				// Re-position the coordinated
				double repositionedPoint[3];
				for (i = 0; i<numPts; i++)
				{
					newPts[zero]->GetPoint(i, repositionedPoint);
					for (j = 0; j<3; ++j)
					{
						repositionedPoint[j] = repositionedPoint[j] * inLength + inCenter[j];
					}
					newPts[zero]->SetPoint(i, repositionedPoint);
				}
			}

			//
			// Update output. Only point coordinates have changed.
			//
			output->GetPointData()->PassData(input->GetPointData());
			output->GetCellData()->PassData(input->GetCellData());

			if (this->GenerateErrorScalars)
			{
				vtkFloatArray *newScalars = vtkFloatArray::New();
				newScalars->SetNumberOfTuples(numPts);
				for (i = 0; i<numPts; i++)
				{
					inPts->GetPoint(i, x1);
					newPts[zero]->GetPoint(i, x2);
					newScalars->SetComponent(i, 0,
						sqrt(vtkMath::Distance2BetweenPoints(x1, x2)));
				}
				int idx = output->GetPointData()->AddArray(newScalars);
				output->GetPointData()->SetActiveAttribute(idx, vtkDataSetAttributes::SCALARS);
				newScalars->Delete();
			}

			if (this->GenerateErrorVectors)
			{
				vtkFloatArray *newVectors = vtkFloatArray::New();
				newVectors->SetNumberOfComponents(3);
				newVectors->SetNumberOfTuples(numPts);
				for (i = 0; i<numPts; i++)
				{
					inPts->GetPoint(i, x1);
					newPts[zero]->GetPoint(i, x2);
					for (j = 0; j<3; j++)
					{
						x3[j] = x2[j] - x1[j];
					}
					newVectors->SetTuple(i, x3);
				}
				output->GetPointData()->SetVectors(newVectors);
				newVectors->Delete();
			}

			output->SetPoints(newPts[zero]);
			newPts[0]->Delete();
			newPts[1]->Delete();
			newPts[2]->Delete();
			newPts[3]->Delete();

			output->SetVerts(input->GetVerts());
			output->SetLines(input->GetLines());
			output->SetPolys(input->GetPolys());
			output->SetStrips(input->GetStrips());

			// finally delete the constructed (local) mesh
			inMesh->Delete();

			//free up connectivity storage
			for (i = 0; i<numPts; i++)
			{
				if (Verts[i].edges != NULL) { Verts[i].edges->Delete(); }
			}
			delete[] Verts;

			return 1;
		}

#pragma endregion

	}
}
