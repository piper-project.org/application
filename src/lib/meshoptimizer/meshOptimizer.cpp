/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshOptimizer.h"

#include <Eigen/Dense>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkCellData.h>
#include <vtkKdTreePointLocator.h>
#include <vtkMath.h>

#include "meshDamageDetector.h"

namespace piper {
    namespace meshoptimizer {

        const std::vector<size_t> dummy_list;

        template <typename T> struct cast_handle : public std::unary_function<size_t, T>
        {
            T operator()(size_t idx) const
            {
                return reinterpret_cast<T>(idx);
            }

        };

        const char* MESQUITE_FIELD_TAG = "MesquiteTags";

        /**\brief Helper function for meshOptimizer::mark_skin_fixed */
        static bool is_side_boundary(meshOptimizer* myMesh,
            size_t elem,
            unsigned side_dim,
            unsigned side_num,
            Mesquite2::MsqError& err)
        {
            // Get the vertices of the side as indices into the above 'verts' list.
            const Mesquite2::EntityTopology type = myMesh->get_element_topology(elem, err);
            MSQ_ERRZERO(err);
            unsigned n; // number of vertices 
            const unsigned* conn = Mesquite2::TopologyInfo::side_vertices(type, side_dim, side_num, n, err);
            MSQ_ERRZERO(err);

            // start with the assumption that the side is on the bounary
            bool boundary = true;

            // get vertices in element connectivity
            const std::vector<size_t>& verts = myMesh->element_connectivity(elem, err);
            MSQ_ERRZERO(err);

            // Choose one vertex in face, and get adjacent elements to that vertex
            const std::vector<size_t>& elems = myMesh->vertex_adjacencies(verts[conn[0]], err);
            MSQ_ERRZERO(err);

            // For each adjacent element
            for (size_t i = 0; i < elems.size(); ++i) {
                // we want *other* adjacent elements
                if (elems[i] == elem)
                    continue;

                // skip elements of smaller dimension
                Mesquite2::EntityTopology type2 = myMesh->get_element_topology(elems[i], err);
                if (Mesquite2::TopologyInfo::dimension(type2) <= side_dim)
                    continue;

                // get number of 'sides' of the appropriate dimension.
                const std::vector<size_t>& verts2 = myMesh->element_connectivity(elems[i], err);
                MSQ_ERRZERO(err);
                int sides2 = Mesquite2::TopologyInfo::adjacent(type2, side_dim);
                for (int j = 0; j < sides2; ++j) {
                    if (Mesquite2::TopologyInfo::compare_sides((const size_t*)Mesquite2::arrptr(verts), type, side_num,
                        (const size_t*)Mesquite2::arrptr(verts2), type2, j,
                        side_dim, err))
                        boundary = false;
                    MSQ_ERRZERO(err);
                }
            }

            return boundary;
        }


        //void meshOptimizer::setMesh(std::vector<hbm::ElemDef>& elemdef, hbm::Nodes& nd) {
        //    //hbm::VId listnid = nd.listId();
        //    //std::cout << nd.size() << " -" << listnid[0] << " - " << listnid[listnid.size() - 1] << std::endl;
        //    elements.clear();
        //    coord.clear();
        //    vid.clear();
        //    eid.clear();
        //    int count = 0;
        //    adjacencies.resize(nd.size());
        //    for (std::vector<hbm::ElemDef>::iterator it = elemdef.begin(); it != elemdef.end(); ++it) {
        //        std::vector<size_t> elemconv(it->begin(), it->end());
        //        elements.push_back(elemconv);
        //        if (elemconv.size() == 4)
        //            element_topology.push_back(Mesquite2::EntityTopology::TETRAHEDRON);
        //        else if (elemconv.size() == 5)
        //            element_topology.push_back(Mesquite2::EntityTopology::PYRAMID);
        //        else if (elemconv.size() == 6)
        //            element_topology.push_back(Mesquite2::EntityTopology::PRISM);
        //        else if (elemconv.size() == 8)
        //            element_topology.push_back(Mesquite2::EntityTopology::HEXAHEDRON);
        //        eid.push_back((ElementHandle)count);
        //        //
        //        for (std::vector<size_t>::const_iterator itn = elemconv.begin(); itn != elemconv.end(); ++itn) {
        //            adjacencies[*itn].push_back(count);
        //        }
        //        count++;
        //    }
        //    for (hbm::Nodes::const_iterator it = nd.begin(); it != nd.end(); ++it) {
        //        //for (hbm::VId::const_iterator it = nid.begin(); it != nid.end(); ++it) {
        //        vid.push_back((VertexHandle)nd.curId(it));
        //        double *coordarray = &(it->second->get()[0]);
        //        Eigen::Map<Eigen::Vector3d> mapvector(coordarray, 3);
        //        coord.push_back(&coordarray[0]);
        //        flag_fixed_vertices.push_back(false);
        //        vertices_bytes.push_back('\0');
        //    }


        //}

        void meshOptimizer::setMesh(hbm::FEModel& femodel, DIM dim) {
            bool spy = femodel.setMeshDef_INDEX();
            elements.clear();
            coord.clear();
            vid.clear();
            eid.clear();
            hbm::VId nid, nidtmp;


            // create tag for nid et eid
            Mesquite2::MsqError err;
            tag_create("nid", Mesquite2::Mesh::TagType::INT, 1, NULL, err);
            tag_create("eid", Mesquite2::Mesh::TagType::INT, 1, NULL, err);

            veid.clear();
            if (dim != DIM::only3D) {
                veid = femodel.getElements2D().listId();
                setMesh2D(femodel.getElements2D());
            }
            if (dim != DIM::only2D) {
                setMesh3D(femodel.getElements3D());
                if (dim == DIM::ALL) {
                    hbm::VId tmp = femodel.getElements3D().listId();
                    veid.insert(veid.end(), tmp.begin(), tmp.end());
                }
                else veid = femodel.getElements3D().listId();
            }

            if (spy)
				femodel.setMeshDef_NID();
            setNodes(femodel.getNodes());
            Mesquite2::TagHandle tag_eid = tag_get("eid", err);
            tag_set_element_data(tag_eid, veid.size(), &eid[0], &veid[0], err);
        }

        void meshOptimizer::setMesh2D(hbm::Elements2D const& elem2D) {


            size_t count = eid.size();
            for (hbm::Elements2D::const_iterator it = elem2D.begin(); it != elem2D.end(); ++it) {
                hbm::ElemDef& elem = elem2D.curDef(it)->get(); //elem = vertex IDs
                std::vector<size_t> elemconv(elem.begin(), elem.end());
                elements.push_back(elemconv);
                if (elem.size() == 3)
                    element_topology.push_back(Mesquite2::EntityTopology::TRIANGLE);
                else if (elem.size() == 4)
                    element_topology.push_back(Mesquite2::EntityTopology::QUADRILATERAL);
                eid.push_back((ElementHandle)count);
                //
                for (hbm::ElemDef::const_iterator itn = elem.begin(); itn != elem.end(); ++itn) {
                    if ((*itn + 1) > adjacencies.size())
                        adjacencies.resize((*itn + 1));
                    adjacencies[*itn].push_back(count);
                }
                count++;
            }
        }

        void meshOptimizer::setMesh3D(hbm::Elements3D const& elem3D) {

            size_t count = eid.size();
            for (hbm::Elements3D::const_iterator it = elem3D.begin(); it != elem3D.end(); ++it) {
                hbm::ElemDef& elem = elem3D.curDef(it)->get();
                std::vector<size_t> elemconv(elem.begin(), elem.end());
                elements.push_back(elemconv);
                if (elem.size() == 4)
                    element_topology.push_back(Mesquite2::EntityTopology::TETRAHEDRON);
                else if (elem.size() == 5)
                    element_topology.push_back(Mesquite2::EntityTopology::PYRAMID);
                else if (elem.size() == 6)
                    element_topology.push_back(Mesquite2::EntityTopology::PRISM);
                else if (elem.size() == 8)
                    element_topology.push_back(Mesquite2::EntityTopology::HEXAHEDRON);
                eid.push_back((ElementHandle)count);
                //
                for (hbm::ElemDef::const_iterator itn = elem.begin(); itn != elem.end(); ++itn) {
                    if ((*itn + 1) > adjacencies.size())
                        adjacencies.resize((*itn + 1));
                    adjacencies[*itn].push_back(count);
                }
                count++;

            }
        }

        void meshOptimizer::setNodes(hbm::Nodes& nodes) {
            Mesquite2::MsqError err;
            Mesquite2::TagHandle tag_nid = tag_get("nid", err);

            hbm::VId nnid;
            int count = 0;
            for (hbm::Nodes::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
                hbm::NodePtr curnode = nodes.curDef(it);
                vid.push_back((VertexHandle)count);
                nnid.push_back(nodes.curId(it));
                double *coordarray = &curnode->get()[0];
                Eigen::Map<Eigen::Vector3d> mapvector(coordarray, curnode->get().size());
                coord.push_back(&coordarray[0]);
                flag_fixed_vertices.push_back(false);
                vertices_bytes.push_back('\0');
                count++;
            }
            tag_set_vertex_data(tag_nid, nnid.size(), &vid[0], &nnid[0], err);
        }

        int meshOptimizer::get_NumberNoFixedNodes() {
            int count = 0;
            for (size_t i = 0; i < getNumberVertices(); ++i) {
                if (!flag_fixed_vertices.at(i))
                    count++;
            }
            return count;
        }

        int meshOptimizer::get_geometric_dimension(Mesquite2::MsqError &err) {
            return 3;
        }

        void meshOptimizer::get_all_elements(std::vector<ElementHandle>& elements, Mesquite2::MsqError& err) {
            elements = eid;
        }

        void meshOptimizer::get_all_vertices(std::vector<VertexHandle>& vertices,
            Mesquite2::MsqError& err) {
            vertices = vid;
        }

        void meshOptimizer::vertices_get_fixed_flag(const VertexHandle vert_array[],
            std::vector<bool>& fixed_flag_array,
            size_t num_vtx,
            Mesquite2::MsqError &err) {
            for (size_t i = 0; i < num_vtx; ++i) {
                fixed_flag_array.push_back(flag_fixed_vertices.at((size_t)vert_array[i]));
                MSQ_ERRRTN(err);
            }
        }

        void meshOptimizer::vertices_get_coordinates(const VertexHandle vert_array[],
            Mesquite2::MsqVertex* coordinates,
            size_t num_vtx,
            Mesquite2::MsqError &err) {
            for (size_t i = 0; i < num_vtx; ++i) {
                if (!is_vertex_valid((size_t)vert_array[i])) {
                    MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                }
                coordinates[i].set(coord[(size_t)vert_array[i]]);
            }
        }

        void meshOptimizer::vertex_set_coordinates(VertexHandle vertex,
            const Mesquite2::Vector3D &coordinates,
            Mesquite2::MsqError &err) {
            if (!is_vertex_valid((size_t)vertex)) {
                MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                return;
            }
            coord.at((size_t)vertex)[0] = coordinates.x();
            coord.at((size_t)vertex)[1] = coordinates.y();
            coord.at((size_t)vertex)[2] = coordinates.z();
        }

        void meshOptimizer::vertex_set_byte(VertexHandle vertex,
            unsigned char byte,
            Mesquite2::MsqError &err) {
            vertices_set_byte(&vertex, &byte, 1, err); MSQ_CHKERR(err);
        }

        void meshOptimizer::vertices_set_byte(const VertexHandle *vert_array,
            const unsigned char *byte_array,
            size_t array_size,
            Mesquite2::MsqError &err) {
            for (size_t i = 0; i < array_size; i++)
            {
                if (!is_vertex_valid((size_t)vert_array[i]))
                {
                    MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                    return;
                }
                vertices_bytes[(size_t)vert_array[i]] = byte_array[i];
            }
        }

        void meshOptimizer::vertex_get_byte(const VertexHandle vertex,
            unsigned char *byte,
            Mesquite2::MsqError &err) {
            vertices_get_byte(&vertex, byte, 1, err); MSQ_CHKERR(err);
        }

        void meshOptimizer::vertices_get_byte(const VertexHandle *vertex,
            unsigned char *byte_array,
            size_t array_size,
            Mesquite2::MsqError &err) {
            for (size_t i = 0; i < array_size; i++)
            {
                if (!is_vertex_valid((size_t)vertex[i]))
                {
                    MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                    return;
                }
                byte_array[i] = vertices_bytes[(size_t)vertex[i]];
            }
        }

        void meshOptimizer::vertices_get_attached_elements(
            const VertexHandle* vertex_array,
            size_t num_vertex,
            std::vector<ElementHandle>& elements,
            std::vector<size_t>& offsets,
            Mesquite2::MsqError& err) {

            elements.clear();
            offsets.clear();
            size_t prev_offset = 0;
            offsets.reserve(num_vertex + 1);
            offsets.push_back(prev_offset);
            const VertexHandle *const vtx_end = vertex_array + num_vertex;
            for (; vertex_array < vtx_end; ++vertex_array) {

                if (!is_vertex_valid((size_t)*vertex_array))
                {
                    MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                    return;
                }
                const std::vector<size_t>& adj = adjacencies[(size_t)*vertex_array];

                prev_offset = prev_offset + adj.size();
                offsets.push_back(prev_offset);

                std::transform(adj.begin(), adj.end(), std::back_inserter(elements), cast_handle<ElementHandle>());
            }
        }

        void meshOptimizer::elements_get_attached_vertices(
            const ElementHandle *elementsh,
            size_t num_elems,
            std::vector<VertexHandle>& vertices,
            std::vector<size_t>& offsets,
            Mesquite2::MsqError &err) {


            vertices.clear();
            offsets.clear();
            size_t prev_offset = 0;
            offsets.reserve(num_elems + 1);
            offsets.push_back(prev_offset);
            const ElementHandle *const elem_end = elementsh + num_elems;
            for (; elementsh < elem_end; ++elementsh) {
                if (!is_element_valid((size_t)*elementsh)) {
                    MSQ_SETERR(err)("Invalid element handle", Mesquite2::MsqError::INVALID_ARG);
                    return;
                }
                const std::vector<size_t>& conn = elements[(size_t)*elementsh];

                prev_offset = prev_offset + conn.size();
                offsets.push_back(prev_offset);
                std::transform(conn.begin(), conn.end(), std::back_inserter(vertices), cast_handle<VertexHandle>());
            }
        }

        void meshOptimizer::elements_get_topologies(const ElementHandle *element_handle_array,
            Mesquite2::EntityTopology *element_topologies,
            size_t num_elements,
            Mesquite2::MsqError& err) {
            for (size_t i = 0; i < num_elements; i++) {
                if (!is_element_valid((size_t)element_handle_array[i])) {
                    MSQ_SETERR(err)("Invalid element handle", Mesquite2::MsqError::INVALID_ARG);
                }
                element_topologies[i] = element_topology[(size_t)element_handle_array[i]];
            }
        }

        void meshOptimizer::optimizeMesh_ShapeSizeImprover(double timeOutSeconds) {
            Mesquite2::MsqError err;
            Mesquite2::ShapeImprovementWrapper myShapeImprover{ timeOutSeconds };
            myShapeImprover.quality_assessor().disable_printing_results();
            myShapeImprover.set_fixed_vertex_mode(Mesquite2::Settings::FixedVertexMode::FIXED_FLAG);
            myShapeImprover.run_instructions(this, err);
        }


        int meshOptimizer::optimizeMesh_Untangle(double timeOutSeconds) {
            Mesquite2::MsqError err;
            Mesquite2::UntangleWrapper myUntangle;
            myUntangle.set_untangle_metric(Mesquite2::UntangleWrapper::UntangleMetric::SHAPESIZE);
            myUntangle.set_cpu_time_limit(timeOutSeconds);
            myUntangle.quality_assessor().disable_printing_results();
            myUntangle.run_instructions(this, err);
            int nbinverted, tmp;
            nbinverted = 0;
            tmp = 0;
            myUntangle.quality_assessor().get_inverted_element_count(nbinverted, tmp, err);
            return (nbinverted + tmp);
        }
				
        void meshOptimizer::optimizeMesh_SurfaceLaplaceSmooth(vtkSmartPointer<vtkPolyData> mesh)
		{
			vtkSmoothPolyDataFilter *smoother = vtkSmoothPolyDataFilter::New();
			smoother->SetInputData(mesh);
			smoother->SetRelaxationFactor(0.1);
			smoother->SetNumberOfIterations(50);
			smoother->Update();
			// copy new vertex positions from the output object back to the original mesh
            // do deep copy, not shallow - other things might depend on the address of the points array of mesh, so don't change it
            mesh->GetPoints()->GetData()->DeepCopy(smoother->GetOutput()->GetPoints()->GetData());
			smoother->Delete();
		}

        void meshOptimizer::optimizeMesh_SurfaceTaubinSmooth(vtkSmartPointer<vtkPolyData> mesh, bool useSelectiveSmoothing, int numberOfIterations, double passBandValue)
		{
			vtkWindowedSincPolyDataFilter *smoother;
			if (useSelectiveSmoothing)
				smoother = vtkWindowedSincPolyDataFilterSelective::New();
			else
				smoother = vtkWindowedSincPolyDataFilter::New();
			smoother->SetInputData(mesh);
			smoother->SetNumberOfIterations(numberOfIterations);
            smoother->SetPassBand(passBandValue);
            smoother->Update();
			// copy new vertex positions from the output object back to the original mesh
            // do deep copy, not shallow - other things might depend on the address of the points array of mesh, so don't change it
            mesh->GetPoints()->GetData()->DeepCopy(smoother->GetOutput()->GetPoints()->GetData());
			smoother->Delete();
		}

        void meshOptimizer::optimizeTransformation_LocalAverageSmooth(vtkSmartPointer<vtkUnstructuredGrid> targetModel, vtkSmartPointer<vtkPoints> referencePoints,
            std::vector<bool> &fixedNodes, bool useAutostop, double autostopThreshold, int maxNumberOfIterations, int noOfNeighbors)
        {
            vtkSmartPointer<vtkUnstructuredGrid> sourceModel = vtkSmartPointer<vtkUnstructuredGrid>::New(); // really just a holder for the reference points
            sourceModel->SetPoints(referencePoints);
            // do moving average smoothing
            vtkSmartPointer<vtkIdList> *neiLists = new vtkSmartPointer<vtkIdList>[referencePoints->GetNumberOfPoints()];
            double curPoint[3];
            vtkSmartPointer<vtkKdTreePointLocator> sourceLoc = vtkSmartPointer<vtkKdTreePointLocator>::New();
            sourceLoc->SetDataSet(sourceModel);
            sourceLoc->BuildLocator();
            // create neighbor lists for each point based on the source positions
            for (vtkIdType i = 0; i < sourceModel->GetNumberOfPoints(); i++)
            {
                if (!fixedNodes[i]) // do not process fixed nodes - waste of time
                {
                    neiLists[i] = vtkSmartPointer<vtkIdList>::New();
                    sourceModel->GetPoint(i, curPoint);
                    sourceLoc->FindClosestNPoints(noOfNeighbors, curPoint, neiLists[i]);
                }
            }
            double temp[3], prevDispl[3];
            bool goOn = true;
            for (int iteration = 0; iteration < maxNumberOfIterations && goOn; iteration++)
            {
                goOn = !useAutostop; // if autostop is on, switch goOn to false, it will be switched to true only if some point is to be processed next iteration
                for (vtkIdType i = 0; i < targetModel->GetNumberOfPoints(); i++)
                {
                    if (!fixedNodes[i])
                    {
                        // compute average displacement target-source
                        targetModel->GetPoint(i, curPoint);
                        vtkMath::Subtract(curPoint, sourceModel->GetPoint(i), curPoint);
                        prevDispl[0] = curPoint[0];
                        prevDispl[1] = curPoint[1];
                        prevDispl[2] = curPoint[2];
                        for (vtkIdType j = 0; j < neiLists[i]->GetNumberOfIds(); j++)
                        {
                            vtkMath::Subtract(targetModel->GetPoint(neiLists[i]->GetId(j)), sourceModel->GetPoint(neiLists[i]->GetId(j)), temp);
                            vtkMath::Add(temp, curPoint, curPoint);
                        }
                        vtkMath::MultiplyScalar(curPoint, 1.0 / (1 + neiLists[i]->GetNumberOfIds()));
                        if (useAutostop)// evaluate autostop criteria
                        {
                            vtkMath::Subtract(prevDispl, curPoint, prevDispl);
                            if (vtkMath::Norm(prevDispl) > autostopThreshold)
                                goOn = true; // displacement change is large, keep going
                            else
                                fixedNodes[i] = true; // displacement change is small, no longer update this point
                        }
                        vtkMath::Add(curPoint, sourceModel->GetPoint(i), curPoint); // add the displacement to the source
                        targetModel->GetPoints()->SetPoint(i, curPoint);
                    }
                }
            }
            delete[] neiLists;
        }

        void meshOptimizer::set_all_fixed_flags(bool value, Mesquite2::MsqError& err) {
            auto maxIndex = max_vertex_index();
            for (size_t i = 0; i < maxIndex; ++i) {
                if (is_vertex_valid(i)) {
                    flag_fixed_vertices[i] = value; MSQ_ERRRTN(err);
                }
            }
        }

        const Mesquite2::EntityTopology meshOptimizer::get_element_topology(size_t index, Mesquite2::MsqError& err) {
            if (!is_element_valid(index)) {
                MSQ_SETERR(err)("Invalid element handle", Mesquite2::MsqError::INVALID_ARG);
                return Mesquite2::EntityTopology::MIXED;
            }
            return element_topology[index];
        }

        const std::vector<size_t>& meshOptimizer::element_connectivity(size_t index, Mesquite2::MsqError& err) const {
            if (!is_element_valid(index))
            {
                MSQ_SETERR(err)("Invalid element handle", Mesquite2::MsqError::INVALID_ARG);
                return dummy_list;
            }

            return elements[index];
        }

        const std::vector<size_t>& meshOptimizer::vertex_adjacencies(size_t index, Mesquite2::MsqError& err) const {
            if (!is_vertex_valid(index))
            {
                MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                return dummy_list;
            }

            return adjacencies[index];
        }

        void meshOptimizer::fix_vertex(size_t index, bool flag, Mesquite2::MsqError& err) {
            if (!is_vertex_valid(index))
            {
                MSQ_SETERR(err)("Invalid vertex handle", Mesquite2::MsqError::INVALID_ARG);
                return;
            }

            flag_fixed_vertices[index] = flag;
        }

        void meshOptimizer::set_skin_flags(bool corner_fixed_flag, Mesquite2::MsqError& err) {
            // For each element, for each side of that element, check for
            // an adjacent element.
            auto maxIndex = max_element_index();
            for (size_t i = 0; i < maxIndex; ++i) {
                if (!is_element_valid(i))
                    continue;

                // Get element connectivity
                const std::vector<size_t>& verts = elements[i];

                // Get element properties
                Mesquite2::EntityTopology type = element_topology[i];
                unsigned dim = Mesquite2::TopologyInfo::dimension(type);
                int sides = Mesquite2::TopologyInfo::adjacent(type, dim - 1);
                bool midedge, midface, midvol;
                Mesquite2::TopologyInfo::higher_order(type, (unsigned int)verts.size(), midedge, midface, midvol, err);
                MSQ_ERRRTN(err);
                const bool midside = (dim == 2 && midedge) || (dim == 3 && midface);
                const bool midsubside = dim == 3 && midedge;

                // For each side of the element (each edge for surface elems, 
                // each face for volume elements)..
                for (int j = 0; j < sides; ++j) {
                    // Get the vertices of the side as indices into the above 'verts' list.
                    unsigned n; // number of vertices 
                    const unsigned* conn = Mesquite2::TopologyInfo::side_vertices(type, dim - 1, j, n, err);
                    MSQ_ERRRTN(err);

                    // if side is on boundary, mark side vertices appropriately
                    bool boundary = piper::meshoptimizer::is_side_boundary(this, i, dim - 1, j, err);
                    MSQ_ERRRTN(err);
                    if (boundary) {
                        // mark corner vertices as fixed
                        for (unsigned k = 0; k < n; ++k) {
                            this->fix_vertex(verts[conn[k]], corner_fixed_flag, err);
                            MSQ_ERRRTN(err);
                        }

                        //// mark higher-order node in center of side as fixed
                        //if (midside) {
                        //    unsigned idx = Mesquite2::TopologyInfo::higher_order_from_side(type, verts.size(), dim - 1, j, err);
                        //    MSQ_ERRRTN(err);
                        //    myMesh->fix_vertex(verts[idx], midnode_fixed_flag, err);
                        //    MSQ_ERRRTN(err);
                        //    myMesh->slave_vertex(verts[idx], midnode_slaved_flag, err);
                        //    MSQ_ERRRTN(err);
                        //}

                        //// if side is a face, mark nodes on edges of face as fixed
                        //if (midsubside) {
                        //    for (unsigned k = 0; k < n; ++k) {
                        //        unsigned edge[2] = { conn[k], conn[(k + 1) % n] };
                        //        bool r;
                        //        unsigned edge_num = TopologyInfo::find_edge(type, edge, r, err);
                        //        MSQ_ERRRTN(err);

                        //        unsigned idx = TopologyInfo::higher_order_from_side(type, verts.size(), 1, edge_num, err);
                        //        MSQ_ERRRTN(err);
                        //        myMesh->fix_vertex(verts[idx], midnode_fixed_flag, err);
                        //        MSQ_ERRRTN(err);
                        //        myMesh->slave_vertex(verts[idx], midnode_slaved_flag, err);
                        //        MSQ_ERRRTN(err);
                        //    }
                    }
                }
            } // for (j in sides)
        } // for (i in elems)
        //}

        void meshOptimizer::mark_skin_fixed(Mesquite2::MsqError& err, bool clear_existing) {
            if (clear_existing) {
                set_all_fixed_flags(false, err); MSQ_ERRRTN(err);
            }

            set_skin_flags(true, err); MSQ_ERRRTN(err);
        }

        Mesquite2::VertexIterator* meshOptimizer::vertex_iterator(Mesquite2::MsqError &err) {
            return new meshOptimizerVertIter(this);
        }

        Mesquite2::ElementIterator* meshOptimizer::element_iterator(Mesquite2::MsqError &err) {
            return new meshOptimizerElemIter(this);
        }


        meshOptimizerVertIter::~meshOptimizerVertIter()
        {}

        void meshOptimizerVertIter::restart()
        {
            index = 0;
            if (!mesh->is_vertex_valid(index))
                operator++();
        }

        void meshOptimizerVertIter::operator++()
        {
            auto maxIndex = mesh->max_vertex_index();
            ++index;
            while (index < maxIndex &&
                (!mesh->is_vertex_valid(index) ||
                !mesh->is_corner_node(index)))
                ++index;
        }

        Mesquite2::Mesh::VertexHandle meshOptimizerVertIter::operator*() const
        {
            return reinterpret_cast<Mesquite2::Mesh::VertexHandle>(index);
        }

        bool meshOptimizerVertIter::is_at_end() const
        {
            return index >= mesh->max_vertex_index();
        }



        meshOptimizerElemIter::~meshOptimizerElemIter()
        {}

        void meshOptimizerElemIter::restart()
        {
            index = 0;
            if (!mesh->is_element_valid(index))
                operator++();
        }

        void meshOptimizerElemIter::operator++()
        {
            auto maxIndex = mesh->max_element_index();
            ++index;
            while (index < maxIndex && !mesh->is_element_valid(index))
                ++index;
        }

        Mesquite2::Mesh::ElementHandle meshOptimizerElemIter::operator*() const
        {
            return reinterpret_cast<Mesquite2::Mesh::ElementHandle>(index);
        }

        bool meshOptimizerElemIter::is_at_end() const
        {
            return index >= mesh->max_element_index();
        }

        Mesquite2::TagHandle meshOptimizer::tag_create(const std::string& tag_name,
            TagType type, unsigned length,
            const void* default_value,
            Mesquite2::MsqError &err) {
            size_t size = MeshMesquiteTag::size_from_tag_type(type);
            TagDescription desc(tag_name, type, length*size);
            size_t index = myTags->create(desc, default_value, err); MSQ_ERRZERO(err);
            return (Mesquite2::TagHandle)index;
        }

        void meshOptimizer::tag_delete(Mesquite2::TagHandle handle, Mesquite2::MsqError& err)
        {
            myTags->destroy((size_t)handle, err); MSQ_CHKERR(err);
        }

        Mesquite2::TagHandle meshOptimizer::tag_get(const std::string& name, Mesquite2::MsqError& err)
        {
            size_t index = myTags->handle(name, err); MSQ_ERRZERO(err);
            if (!index)
                MSQ_SETERR(err)(Mesquite2::MsqError::TAG_NOT_FOUND, "could not find tag \"%s\"", name.c_str());
            return (Mesquite2::TagHandle)index;
        }

        void meshOptimizer::tag_properties(Mesquite2::TagHandle handle,
            std::string& name,
            TagType& type,
            unsigned& length,
            Mesquite2::MsqError& err)
        {
            const TagDescription& desc
                = myTags->properties((size_t)handle, err); MSQ_ERRRTN(err);

            name = desc.name;
            type = desc.type;
            length = (unsigned)(desc.size / MeshMesquiteTag::size_from_tag_type(desc.type));
        }


        void meshOptimizer::tag_set_element_data(Mesquite2::TagHandle handle,
            size_t num_elems,
            const ElementHandle* elem_array,
            const void* values,
            Mesquite2::MsqError& err)
        {
            myTags->set_element_data((size_t)handle,
                num_elems,
                (const size_t*)elem_array,
                values,
                err);  MSQ_CHKERR(err);
        }

        void meshOptimizer::tag_get_element_data(Mesquite2::TagHandle handle,
            size_t num_elems,
            const ElementHandle* elem_array,
            void* values,
            Mesquite2::MsqError& err)
        {
            myTags->get_element_data((size_t)handle,
                num_elems,
                (const size_t*)elem_array,
                values,
                err);  MSQ_CHKERR(err);
        }

        void meshOptimizer::tag_set_vertex_data(Mesquite2::TagHandle handle,
            size_t num_elems,
            const VertexHandle* elem_array,
            const void* values,
            Mesquite2::MsqError& err)
        {
            myTags->set_vertex_data((size_t)handle,
                num_elems,
                (const size_t*)elem_array,
                values,
                err);  MSQ_CHKERR(err);
        }

        void meshOptimizer::tag_get_vertex_data(Mesquite2::TagHandle handle,
            size_t num_elems,
            const VertexHandle* elem_array,
            void* values,
            Mesquite2::MsqError& err)
        {
            myTags->get_vertex_data((size_t)handle,
                num_elems,
                (const size_t*)elem_array,
                values,
                err);  MSQ_CHKERR(err);
        }


        bool meshOptimizer::getVertexHandle(Mesquite2::Mesh::VertexHandle& handle, hbm::Id& id, Mesquite2::MsqError &err) {
            Mesquite2::TagHandle tag_nid = tag_get("nid", err);
            size_t index;
            if (myTags->find_vertex_data(index, (size_t)tag_nid, getNumberVertices(), &id, err)) {
                handle = vid[index];
                return true;
            }
            else return false;
        }

        bool meshOptimizer::getElementHandle(Mesquite2::Mesh::VertexHandle& handle, hbm::Id& id, Mesquite2::MsqError &err) {
            Mesquite2::TagHandle tag_nid = tag_get("eid", err);
            size_t index;
            if (myTags->find_element_data(index, (size_t)tag_nid, getNumberElements(), &id, err)) {
                handle = vid[index];
                return true;
            }
            else return false;
        }

        void meshOptimizer::release_skin_fixed(hbm::VId& idskin) {
            Mesquite2::MsqError err;

            std::vector<Mesquite2::Mesh::VertexHandle> vverth;
            std::vector<bool> fixed_flag_array;
            get_all_vertices(vverth, err);
            vertices_get_fixed_flag(&vverth[0], fixed_flag_array, getNumberVertices(), err);
            int count1 = 0;
            for (auto it = fixed_flag_array.begin(); it != fixed_flag_array.end(); ++it) {
                if (*it)
                    count1++;
            }
            //std::cout << "nb fixed nodes : " << count1 << std::endl;

            // get vertex handle for node skin id
            int count = 0;
            Mesquite2::TagHandle tag_nid = tag_get("nid", err);
            std::vector<Mesquite2::Mesh::VertexHandle> vertexH;
            for (hbm::VId::iterator it = idskin.begin(); it != idskin.end(); ++it) {
                Mesquite2::Mesh::VertexHandle tmpH;
                getVertexHandle(tmpH, *it, err);
                vertexH.push_back(tmpH);
                fix_vertex((size_t)tmpH, false, err);
                count++;
                std::vector<size_t> adv;
                std::vector<size_t> adj = vertex_adjacencies((size_t)tmpH, err);
                for (auto ita = adj.begin(); ita != adj.end(); ++ita) {
                    std::vector<size_t> conn = element_connectivity(*ita, err);
                    for (auto itc = conn.begin(); itc != conn.end(); ++itc) {
                        std::vector<Mesquite2::Mesh::VertexHandle>::const_iterator itv;
                        itv = std::find(vertexH.begin(), vertexH.end(), (Mesquite2::Mesh::VertexHandle)*itc);
                        if (itv != vertexH.end()) {
                            fix_vertex(*itc, false, err);
                            adv.push_back(*itc);
                            count++;
                        }
                    }
                }
                //for (auto itv = adv.begin(); itv != adv.end(); ++itv) {
                //    std::vector<size_t> adj = vertex_adjacencies(*itv, err);
                //    for (auto ita = adj.begin(); ita != adj.end(); ++ita) {
                //        std::vector<size_t> conn = element_connectivity(*ita, err);
                //        for (auto itc = conn.begin(); itc != conn.end(); ++itc) {
                //            std::vector<Mesquite2::Mesh::VertexHandle>::const_iterator itvv;
                //            itvv = std::find(vertexH.begin(), vertexH.end(), (Mesquite2::Mesh::VertexHandle)*itc);
                //            if (itvv != vertexH.end()) {
                //                fix_vertex(*itc, false, err);
                //                adv.push_back(*itc);
                //                count++;
                //            }
                //        }
                //    }
                //}
            }
            //std::cout << "nb release nodes : " << count << std::endl;

        }

    }//meshoptimizer
}//piper
