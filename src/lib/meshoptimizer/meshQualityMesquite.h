/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHQUALITYMESQUITE_PIPER_H
#define MESHQUALITYMESQUITE_PIPER_H

#include "meshQualityAbstract.h"

#include <map>
#include <string>

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

namespace piper {
    namespace meshoptimizer {
        class meshOptimizer;



        class MESHOPTIMIZER_EXPORT meshQualityMesquite : public meshQualityAbstract {
        public:

            meshQualityMesquite();
            ~meshQualityMesquite();

            //implemente pure vitual function
            void reset();
            void setMesh(hbm::FEModel& fem);
            void computeQuality();
            
            /// <summary>
            /// Make each quality value become the difference between it and the quality value of the baseline model.
            /// processBaselineMesh must have been called before this!
            /// </summary>
            /// <param name="curValues">The current quality values.</param>
            /// <param name="metricName">Name of the metric.</param>
            void makeQualityValuesRelative(std::vector<double> *curValues, std::string metricName);

            void writeQualityResults(std::string const& filename);
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) override { return false; };
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) override { return false; };
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, std::vector<double>& metricvalue) override { return false; };

            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) override;
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue) override;
			virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) override;
            
            virtual double getMaxValueMetric3D(QualityMetrics const& metric) override;
            virtual double getMinValueMetric3D(QualityMetrics const& metric) override;
            
            /// <summary>
            /// If <c>setQualityUseBaselineModel(<b>true</b>)</c> was called before this call, 
            /// quality of the specified fem will be compute and stored to be later used for comparison with the quality of the current mesh.
            /// </summary>
            /// <param name="fem">The baseline model to use for quality comparison.</param>
            virtual void processBaselineMesh(hbm::FEModel& fem) override;

        protected:
            //std::map<QualityMetrics, std::string> m_metrics;
			meshOptimizer* m_mesh;
            meshOptimizer* m_BaselineMesh;
			/// Finds all 3D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
			virtual bool findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
				std::function<bool(double, double)> compareOperation) override;

			/// Finds all 2D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
            virtual bool findmetrics_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
                std::function<bool(double, double)> compareOperation) override {
                return false;
            };
        };



    }//meshoptimizer
}//piper



#endif
