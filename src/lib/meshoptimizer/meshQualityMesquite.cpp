/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshQualityMesquite.h"

#include "hbm/FEModel.h"
#include "meshoptimizer/meshOptimizer.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include <boost/operators.hpp>
#include <iostream>
#include <fstream>


using namespace Eigen;

namespace piper {
    namespace meshoptimizer {
        using namespace hbm;

        meshQualityMesquite::meshQualityMesquite() : meshQualityAbstract(DIM::only3D), m_mesh(new meshOptimizer()), m_BaselineMesh(new meshOptimizer()) {
            //m_metrics.insert(std::pair<QualityMetrics, std::string>(QualityMetrics::MESQUITE_UntangleBeta, "untangle_metric"));
            //m_metrics.insert(std::pair<QualityMetrics, std::string>(QualityMetrics::MESQUITE_InverseMeanRatio, "InverseMeanRatio_metric"));
            //m_metrics.insert(std::pair<QualityMetrics, std::string>(QualityMetrics::MESQUITE_ConditionNumber, "ConditionNumber_metric"));            
            //m_metrics.insert(std::pair<QualityMetrics, std::string>(QualityMetrics::MESQUITE_Inverted, "Inverted_metric"));
            m_validmetrics.insert(QualityMetrics::MESQUITE_Inverted);
            m_validmetrics.insert(QualityMetrics::MESQUITE_InverseMeanRatio);
            m_validmetrics.insert(QualityMetrics::MESQUITE_UntangleBeta);
        }

        meshQualityMesquite::~meshQualityMesquite() {
            //delete m_mesh;
        }




        void meshQualityMesquite::reset() {
            delete m_mesh;
            m_mesh = new meshOptimizer();
        }

        void meshQualityMesquite::setMesh(piper::hbm::FEModel& fem) {
            meshQualityAbstract::setMesh(fem);
            m_mesh->setMesh(fem, m_dim);
        }

        void meshQualityMesquite::processBaselineMesh(hbm::FEModel& fem) {
            if (qualityCompareToBaseline)
            {
                // store the originally set mesh
                meshOptimizer *cur = m_mesh;
                // load the baseline
                m_mesh = m_BaselineMesh;
                setMesh(fem);
                computeQuality();
                // restore the originally set mesh
                m_mesh = cur;
            }
        }

        void meshQualityMesquite::computeQuality() {
            Mesquite2::MsqError err;
            Mesquite2::Settings setting;
            Mesquite2::PlanarDomain myPlanar;
            Mesquite2::MeshDomainAssoc mesh_domain = Mesquite2::MeshDomainAssoc(m_mesh, 0/*&myPlanar*/);

            Mesquite2::UntangleBetaQualityMetric untangle_metric(1e-8);
            Mesquite2::QualityAssessor qa(&untangle_metric, 0, 0.0, false, 
				QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_UntangleBeta].c_str(), 
				true, QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_Inverted].c_str());

            //Mesquite2::QualityAssessor qa(false, false, QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_Inverted].c_str(), "myAssessor");
            //if (isValidMetrics(QualityMetrics::MESQUITE_UntangleBeta)) {
            //    Mesquite2::UntangleBetaQualityMetric untangle_metric(1e-8);
            //    qa.add_quality_assessment(&untangle_metric, 0, 0.0, QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_UntangleBeta].c_str(), 0);
            //    std::cout << "é i am here1-1" << std::endl;
            //}
            Mesquite2::IdealWeightInverseMeanRatio inverse_mean_ratio;
            inverse_mean_ratio.set_averaging_method(Mesquite2::QualityMetric::LINEAR);
            if (isValidMetrics(QualityMetrics::MESQUITE_InverseMeanRatio)) {
                qa.add_quality_assessment(&inverse_mean_ratio, 0, 0, QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_InverseMeanRatio].c_str(), 0);
            }
            Mesquite2::ConditionNumberQualityMetric conditionnumber;
            if (isValidMetrics(QualityMetrics::MESQUITE_ConditionNumber)) {
                qa.add_quality_assessment(&conditionnumber, 0, 0, QualityMetrics_str[(unsigned int)QualityMetrics::MESQUITE_ConditionNumber].c_str(), 0);
            }
            qa.disable_printing_results();
            qa.loop_over_mesh(&mesh_domain, &setting, err);
            //qa.print_summary(std::cout);
            //
        }

        void meshQualityMesquite::makeQualityValuesRelative(std::vector<double> *curValues, std::string metricName)
        {
            if (m_BaselineMesh->getNumberElements() == curValues->size())
            {
                Mesquite2::MsqError err;
                Mesquite2::TagHandle tag_MetricBase = m_BaselineMesh->tag_get(metricName, err);
                std::vector<double> metric_valueBase;
                std::vector<Mesquite2::Mesh::ElementHandle> velth;
                m_BaselineMesh->get_all_elements(velth, err);
                metric_valueBase.resize(m_BaselineMesh->getNumberElements());
                m_BaselineMesh->tag_get_element_data(tag_MetricBase, m_BaselineMesh->getNumberElements(), &velth[0], &metric_valueBase[0], err);
                for (int i = 0; i < metric_valueBase.size(); i++)
                {
                    (*curValues)[i] = (*curValues)[i] - metric_valueBase[i];
                }
            }
        }

        void meshQualityMesquite::writeQualityResults(std::string const& filename) {
            // get metrics for the mesh
            Mesquite2::MsqError err;
            std::vector<Mesquite2::Mesh::ElementHandle> velth;
            m_mesh->get_all_elements(velth, err);
            //
            std::vector<std::string> title;
            title.push_back("\"dim\"");
            title.push_back("\"Elements PIPER Id\"");
            if (m_fem->isFEmodel()) {
                title.push_back("\"FE Keywords\"");
                title.push_back("\"FE Id\"");
            }
            //
            std::vector<std::vector<double>> values;
            for (std::set<QualityMetrics>::const_iterator it = m_validmetrics.begin(); it != m_validmetrics.end(); ++it) {
                Mesquite2::TagHandle tag_Metric = m_mesh->tag_get(QualityMetrics_str[(unsigned int)*it], err);
                std::vector<double> metric_value;
                metric_value.resize(m_mesh->getNumberElements());
                m_mesh->tag_get_element_data(tag_Metric, m_mesh->getNumberElements(), &velth[0], &metric_value[0], err);

                if (qualityCompareToBaseline)
                    makeQualityValuesRelative(&metric_value, QualityMetrics_str[(unsigned int)*it]);

                title.push_back(std::string("\"" + QualityMetrics_str[(unsigned int)*it] + "\""));
                values.push_back(metric_value);
            }
            Mesquite2::TagHandle tag_eid = m_mesh->tag_get("eid", err);
            std::vector<int> eid;
            eid.resize(m_mesh->getNumberElements());
            m_mesh->tag_get_element_data(tag_eid, m_mesh->getNumberElements(), &velth[0], &eid[0], err);

            boost::filesystem::path filePath(filename);
            std::ofstream file(filePath.string(), std::ios::out | std::ios::trunc);
            if (file.is_open()) {
                for (int i = 0; i < title.size(); ++i) {
                    if (i == title.size()-1)
                        file << title[i] << std::endl;
                    else
                        file << title[i] << ",";
                }
                for (int i = 0; i < m_mesh->getNumberElements(); ++i) {
                    if (m_fem->isFEmodel()) {
                        std::string key;
                        hbm::IdKey idfe;
                        m_fem->getInfoId<hbm::Element3D>(eid.at(i), key, idfe);
                        file << "3D ; " << eid[i] << "," << key << ",";
                        if (idfe.idstr == "")
                            file << idfe.id << ",";
                        else
                            file << idfe.idstr << ",";
                    }
                    for (int n = 0; n < values.size(); ++n) {
                        if (n == values.size() - 1)
                            file << values[n][i] << std::endl;
                        else
                            file << values[n][i] << ",";
                    }
                }
                file.close();
            }



            //Mesquite2::TagHandle tag_UntangleBeta = m_mesh->tag_get(m_metrics[QualityMetrics::MESQUITE_UntangleBeta], err);
            //Mesquite2::TagHandle tag_InverseMeanRatio = m_mesh->tag_get(m_metrics[QualityMetrics::MESQUITE_InverseMeanRatio], err);
            //Mesquite2::TagHandle tag_ConditionNumber = m_mesh->tag_get(m_metrics[QualityMetrics::MESQUITE_ConditionNumber], err);
            //Mesquite2::TagHandle tag_eid = m_mesh->tag_get("eid", err);
            //Mesquite2::TagHandle tag_inverted = m_mesh->tag_get(m_metrics[QualityMetrics::MESQUITE_Inverted], err);
            //std::vector<double> metrics_UntangleBeta, metrics_InverseMeanRatio, metrics_ConditionNumber;
            //std::vector<int> eid;
            //std::vector<int> inverted;
            //metrics_UntangleBeta.resize(m_mesh->getNumberElements());
            //metrics_InverseMeanRatio.resize(m_mesh->getNumberElements());
            //metrics_ConditionNumber.resize(m_mesh->getNumberElements());
            //eid.resize(m_mesh->getNumberElements());
            //inverted.resize(m_mesh->getNumberElements());
            //m_mesh->tag_get_element_data(tag_UntangleBeta, m_mesh->getNumberElements(), &velth[0], &metrics_UntangleBeta[0], err);
            //m_mesh->tag_get_element_data(tag_InverseMeanRatio, m_mesh->getNumberElements(), &velth[0], &metrics_InverseMeanRatio[0], err);
            //m_mesh->tag_get_element_data(tag_ConditionNumber, m_mesh->getNumberElements(), &velth[0], &metrics_ConditionNumber[0], err);
            //m_mesh->tag_get_element_data(tag_eid, m_mesh->getNumberElements(), &velth[0], &eid[0], err);
            //m_mesh->tag_get_element_data(tag_inverted, m_mesh->getNumberElements(), &velth[0], &inverted[0], err);

            //boost::filesystem::path filePath(filename);
            //std::ofstream file(filePath.string(), std::ios::out | std::ios::trunc);



            //if (m_fem->isFEmodel()) {
            //    if (file.is_open()) {
            //        file << "dim ; id piper ; FE keyword ; FE id ; inverted (=1) ; MESQUITE_UntangleBeta ; MESQUITE_InverseMeanRatio ; MESQUITE_ConditionNumber" << std::endl;
            //        for (int i = 0; i < m_mesh->getNumberElements(); ++i) {
            //            std::string key, idstr;
            //            hbm::Id idfe;
            //            m_fem->getInfoId<hbm::Element3D>(eid.at(i), key, idfe, idstr);
            //            file << "3D ; " << eid[i] << ";" << key << ";";
            //            if (idstr == "none")
            //                file << idfe << ";";
            //            else
            //                file << idstr << ";";
            //            file << inverted[i] << ";" <<
            //                metrics_UntangleBeta[i] << ";" <<
            //                metrics_InverseMeanRatio[i] << ";" <<
            //                metrics_ConditionNumber[i] << std::endl;
            //        }
            //        file.close();
            //    }
            //}
            //else {
            //    if (file.is_open()) {
            //        file << "dim ; id piper ; inverted (=1) ; MESQUITE_UntangleBeta ; MESQUITE_InverseMeanRatio ; MESQUITE_ConditionNumber" << std::endl;
            //        for (int i = 0; i < m_mesh->getNumberElements(); ++i) {
            //            file << "3D ; " << eid[i] << ";" ;
            //            file << inverted[i] << ";" <<
            //                metrics_UntangleBeta[i] << ";" <<
            //                metrics_InverseMeanRatio[i] << ";" <<
            //                metrics_ConditionNumber[i] << std::endl;
            //        }
            //        file.close();
            //    }
            //}
        }


        bool meshQualityMesquite::getQualityMetrics_3D(QualityMetrics const& metric, Id& id, double& metricvalue) {
            // 
            if (!isValidMetrics(metric))
                return false;
            // get element handle for id
            Mesquite2::MsqError err;
            Mesquite2::Mesh::ElementHandle elementH;
            if (!m_mesh->getElementHandle(elementH, id, err))
                return false;

            // get tag handle
            Mesquite2::TagHandle tag_metric = m_mesh->tag_get(QualityMetrics_str[(unsigned int)metric], err);
            // get value metric
            if (metric == QualityMetrics::MESQUITE_Inverted) {
                int value;
                m_mesh->tag_get_element_data(tag_metric, 1, &elementH, &value, err);
                metricvalue = value;
            }
            else
                m_mesh->tag_get_element_data(tag_metric, 1, &elementH, &metricvalue, err);

            if (qualityCompareToBaseline)
            {
                double baseValue;
                Mesquite2::TagHandle tag_metricBase = m_BaselineMesh->tag_get(QualityMetrics_str[(unsigned int)metric], err);
                m_BaselineMesh->tag_get_element_data(tag_metricBase, 1, &elementH, &baseValue, err);
                metricvalue -= baseValue;
            }

            if (!MSQ_CHKERR(err))
                return true;
            else return false;
        }

        bool meshQualityMesquite::getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) {
            // 
            if (!isValidMetrics(metric))
                return false;
            //
            metricvalue.clear();
            // get element handle for id
            Mesquite2::MsqError err;
            std::vector<Mesquite2::Mesh::ElementHandle> elementH;
            for (hbm::VId::iterator it = id.begin(); it != id.end(); ++it) {
                Mesquite2::Mesh::ElementHandle tmpH;
                if (!m_mesh->getElementHandle(tmpH, *it, err))
                    return false;
                elementH.push_back(tmpH);
            }

            // get tag handle
            Mesquite2::TagHandle tag_metric = m_mesh->tag_get(QualityMetrics_str[(unsigned int)metric], err);
            // get value metric
            if (metric == QualityMetrics::MESQUITE_Inverted) {
                std::vector<int> value;
                value.resize(elementH.size());
                m_mesh->tag_get_element_data(tag_metric, elementH.size(), &elementH[0], &value[0], err);
                metricvalue.insert(metricvalue.end(), value.begin(), value.end());
            }
            else {
                metricvalue.resize(elementH.size());
                m_mesh->tag_get_element_data(tag_metric, elementH.size(), &elementH[0], &metricvalue[0], err);
            }

            if (qualityCompareToBaseline)
                makeQualityValuesRelative(&metricvalue, QualityMetrics_str[(unsigned int)metric]);

            if (!MSQ_CHKERR(err))
                return true;
            else return false;
        }

        bool meshQualityMesquite::getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue) {
            // 
            if (!isValidMetrics(metric))
                return false;
            //
            metricvalue.clear();
            // get all element handle
            Mesquite2::MsqError err;
            // get all element handle
            std::vector<Mesquite2::Mesh::ElementHandle> elementH;
            m_mesh->get_all_elements(elementH, err);
            // get tag handle
            Mesquite2::TagHandle tag_metric = m_mesh->tag_get(QualityMetrics_str[(unsigned int)metric], err);
            // get value metric
            if (metric == QualityMetrics::MESQUITE_Inverted) {
                std::vector<int> value;
                value.resize(elementH.size());
                m_mesh->tag_get_element_data(tag_metric, elementH.size(), &elementH[0], &value[0], err);
                metricvalue.insert(metricvalue.end(), value.begin(), value.end());
            }
            else {
                metricvalue.resize(elementH.size());
                m_mesh->tag_get_element_data(tag_metric, elementH.size(), &elementH[0], &metricvalue[0], err);
            }

            if (qualityCompareToBaseline)
                makeQualityValuesRelative(&metricvalue, QualityMetrics_str[(unsigned int)metric]);

            if (!MSQ_CHKERR(err))
                return true;
            else return false;
        }

       
        
		/// <summary>
		/// Finds all elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal etc.
		/// </summary>
		/// <param name="metric">The metric.</param>
		/// <param name="threshold">The threshold.</param>
		/// <param name="vid">The vid.</param>
		/// <param name="compareOperation">The compare operation. See http://www.cplusplus.com/reference/functional/ for a reference list of acceptable operators.</param>
		/// <returns>False in case of a Mesquite error or invalid metrics.</returns>
		bool meshQualityMesquite::findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
			std::function<bool(double, double)> compareOperation)  {
            // 
            if (!isValidMetrics(metric))
                return false;
            //

            Mesquite2::MsqError err;
            // get all element handle
            std::vector<Mesquite2::Mesh::ElementHandle> elementH;
            m_mesh->get_all_elements(elementH, err);
            // get eid tag data
            VId eid;
            eid.resize(m_mesh->getNumberElements());
            Mesquite2::TagHandle tag_eid = m_mesh->tag_get("eid", err);
            m_mesh->tag_get_element_data(tag_eid, m_mesh->getNumberElements(), &elementH[0], &eid[0], err);
            // get metric values
            //std::vector<double> metricvalues;
            //getQualityMetrics_3D(metric, eid, metricvalues);
            //
            std::vector<double> metricvalues;
            getQualityMetrics_3D(metric, metricvalues);

            //
            int count = 0;
            VId::const_iterator iteid = eid.begin();
            for (std::vector<double>::const_iterator it = metricvalues.begin(); it != metricvalues.end(); ++it) {
				if (compareOperation(*it, threshold))
					vid.push_back(*iteid);				
                iteid++;
            }
            if (!MSQ_CHKERR(err))
                return true;
            else return false;
			
        }


        double meshQualityMesquite::getMaxValueMetric3D(QualityMetrics const& metric) {
            std::vector<double> values;
            if (getQualityMetrics_3D(metric, values))
            {
                //
                Mesquite2::MsqError err;
                // get all element handle
                std::vector<Mesquite2::Mesh::ElementHandle> elementH;
                m_mesh->get_all_elements(elementH, err);
                // get eid tag data
                VId eid;
                eid.resize(m_mesh->getNumberElements());
                Mesquite2::TagHandle tag_eid = m_mesh->tag_get("eid", err);
                //std::vector<double>::const_iterator itt=std::max_element(values.begin(), values.end());
                //size_t index = itt - values.begin();
                //std::string key, idstr;
                //hbm::Id id;
                //m_fem->getInfoId<hbm::Element3D>(eid[index], key, id, idstr);
                //std::cout << index << " - " << eid[index] << " - " << key << " - " << id << " - " << *std::max_element(values.begin(), values.end()) << std::endl;
                //
                return *std::max_element(values.begin(), values.end());
            }
            return 1;
        }

        double meshQualityMesquite::getMinValueMetric3D(QualityMetrics const& metric) {
            std::vector<double> values;
            if (getQualityMetrics_3D(metric, values))
                return *std::min_element(values.begin(), values.end());
            return 0;
        }



    }//meshoptimizer
}//piper
