/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHDAMAGEDETECTOR_PIPER_H
#define MESHDAMAGEDETECTOR_PIPER_H

#include "hbm/FEModel.h"
#include "meshQualityAbstract.h"
#include <vtkWindowedSincPolyDataFilter.h>

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif


namespace piper {
	namespace meshoptimizer {

		/// <summary>
		/// Detects unwanted deformation in a mesh after it has been transoformed based on
		/// the mesh before the transformation. 
		/// The algorithm clusters deformed triangles based on the change of dihedral angles after the deformation.
		/// If the angle change is low between two triangles, they are considered as the same cluster, so in an
		/// undeformed model, the whole mesh is one cluster. After deformation, several clusters can appear, with 
		/// small (meaning majority of elements on the boundary of the cluster) clusters marking a potential error.
		/// The detection is processed in the ComputeQuality method, results can be obtained by getClusters and getDamagedClustersIDs methods.
		/// Nodes and cells that are part of damaged clusters are marked in the output polydata, in the input unstructured grid and in the parent entity mesh in the 
		/// SELECTED_PRIMITIVES (macro defined in hbm/VtkSelectionTools.h) scalar cell data array.
		/// @author Tomas Janak
		/// </summary>
		class MESHOPTIMIZER_EXPORT meshDamageDetector : public meshQualityAbstract
		{

		public:

			// identifier of a map of elements on the boundary of their clusters. it marks each element with -1 if is not, ID of the neighbor element if it is
			const char *IS_ON_CLUSTER_BOUND = "is_on_cluster_boundary_meshDamageDetector";

			/// <summary>
			/// Initializes a new instance of the <see cref="meshDamageDetector"/> class.
			/// </summary>
			/// <param name="dihedralAngleThreshold">The dihedral angle difference threshold, meaning the maximal allowed change in
			/// dihedral angle before and after the deformation. It is used to decide whether to put neighboring
			/// triangles into the same cluster or not. In radians. Default = 0.09 ~ 5 degrees.</param>
			/// <param name="damagedClusterSizeDef">Percentage of elements that have to be on the boundary of a cluster
			/// for that cluster to be considered damaged.</param>
			meshDamageDetector(double dihedralAngleDifThreshold = 0.09, double damagedClusterSizeDef = 0.6);
			~meshDamageDetector();

			/// <summary>
			/// Initializes a new instance of the <see cref="meshDamageDetector"/> class.
			/// </summary>
			/// <param name="original">The original mesh before the deformation.</param>
			/// <param name="dihedralAngleThreshold">The dihedral angle difference threshold, meaning the maximal allowed change in
			/// dihedral angle before and after the deformation. It is used to decide whether to put neighboring
			/// triangles into the same cluster or not. In radians. Default = 0.09 ~ 5 degrees.</param>
			/// <param name="damagedClusterSizeDef">Percentage of elements that have to be on the boundary of a cluster
			/// for that cluster to be considered damaged. Default 60%.</param>
			meshDamageDetector(vtkPolyData *original, double dihedralAngleDifThreshold = 0.09, double damagedClusterSizeDef = 0.6);

			void SetDeformedModel(vtkPolyData *deformed);			
			/// <summary>
			/// Gets the deformed model polygons. After the detection, this mesh will contain
			/// the scalar arrays SELECTED_PRIMITIVES (macro defined in hbm/vtkSelectionTool.h) for both cells and points
			/// marking the primitives that are part of damaged clusters.
			/// </summary>
			/// <returns>2D elements of the deformed model</returns>
			vtkSmartPointer<vtkPolyData> GetDeformedModelPolys();

			piper::hbm::FEModelVTK *GetDeformedModel();

			/// <summary>
			/// Sets the dihedral angle difference threshold. If the angle between two neighbouring elements
			/// have not changed after the deformation by more than this threshold, they will be considered
			/// as part of the same cluster. Large change marks a potential damage - they are put in separate clusters.
			/// </summary>
			/// <param name="threshold">The threshold in radians.</param>
			void SetDihedralAngleDifThreshold(double threshold);

			/// <summary>
			/// Sets a governing variable that define what clusters are to be considered as errors.
			/// </summary>
			/// <param name="damagedClusterSizeDef">Minimal percentage of elements that have to be on a 
			/// boundary of the cluster for it to be considered erroneus.
			/// E.g. if set to 0.6, clusters that have 60% or more of elements on their boundaries will be considered
			/// as damage (this should localize small and narrow clusters of elements).</param>
			void SetDamageSizeDef(double damagedClusterSizeDef);
            
            /// <summary>
            /// Sets the name of the target entity that will be processed by this detector.
            /// </summary>
            /// <param name="entityName">Name of the entity.</param>
            void SetTargetEntityName(std::string entityName);

			/// <summary>
			/// Gets the clusters.
			/// </summary>
			/// <returns>Vector of clusters. Each cluster is a vector of IDs of elements from the input vtkPolyData (it assumed that both 
			/// original and deformed model have exactly the same elements, IDs of them and connectivity, otherwise the algorithm does not work!).</returns>
			boost::container::vector<boost::container::vector<int>> GetClusters();

			/// <summary>
			/// Gets the IDs of damaged clusters.
			/// </summary>
			/// <returns>Vector of IDs of clusters marked as damaged by the damageSizeDef. The IDs point to the vector obtainable by getClusters().
			/// (so getClusters()[getDamagedClustersIDs()[0]] gets you the first damaged cluster etc.).</returns>
			boost::container::vector<int> GetDamagedClustersIDs();
			
			/// <summary>
			/// Creates a new vtkUnstructuredGrid instance for each of the damaged clusters that contains only points and cells of the damaged elements.
			/// </summary>
			/// <returns>An array of pointers to the meshes representing the damaged clusters.
			/// Note that each has separate and unique points and cells arrays, so their indices do not match the indices of the parent mesh.</returns>
			boost::container::vector<vtkSmartPointer<vtkUnstructuredGrid>> CreateDamagedClustersAsUnstrGrids();

            virtual boost::container::vector<vtkOBBNodePtr> generateSelectionBoxes(bool mergeOverlapping);

			/// <summary>
			/// Executes the damage detection.
			/// </summary>
			void computeQuality();

			void reset();
			// None of these is used - perhaps remove them from the abstract class as pure virtual??
            void writeQualityResults(std::string const& filename)  { return; }
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue)  override { return false; }
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) override  { return false; }
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) override  { return false; }
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) override  { return false; }
            virtual bool getQualityMetrics_2D(QualityMetrics const& metric, std::vector<double>& metricvalue) override  { return false; }
            virtual bool getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue)override  { return false; }
            virtual double getMaxValueMetric3D(meshoptimizer::QualityMetrics const& metric) override   { return false; }
            virtual double getMinValueMetric3D(meshoptimizer::QualityMetrics const& metric) override { return false; }
			/// Finds all 3D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
            virtual bool findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
                std::function<bool(double, double)> compareOperation)  { return false;	}
			/// Finds all 2D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
            virtual bool findmetrics_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
                std::function<bool(double, double)> compareOperation) { return false;	}
            
            /// <summary>
            /// Computes the normals of the specified mesh and stores them for future reference. The entityToProcess MUST be set before calling this.
            /// </summary>
            /// <param name="fem">The baseline fem.</param>
            virtual void processBaselineMesh(hbm::FEModel& fem) override;

		protected:
			vtkSmartPointer<vtkPolyData> deformed; // the model after deformation
			piper::hbm::FEModelVTK *m_model; // FEModelVTK representation of the deformed model			
			double dihedralAngleDifThreshold; // The maximal allowed dihedral angle difference after deformation
			double damagedClusterSizeDef; // definition of what should be considered a damaged cluster in percentage of elements on its boundary
			boost::container::vector<boost::container::vector<int>> clusters; // vector containing individual clusters (clusters = lists of element IDs)
			boost::container::vector<int> damagedClusters; // IDs into the "clusters" vector of clusters that are damaged
            vtkSmartPointer<vtkDoubleArray> origNormals; // normals of the baseline mesh

            /// <summary>
            /// Traverse the mesh and assigns each element into exactly one cluster.
            /// The clusters are stored in the "clusters" vector; each is a vector of element IDs.
            /// Also adds a cell data array named IS_ON_CLUSTER_BOUND to the deformed model that contains
            /// -1 for each element that is not on a boundary of its cluster and element ID of the neighbor
            /// if it is on a boundary.
            /// </summary>
			void generateClusters();

            /// <summary>
            /// Analyzes the generated clusters and selects those that are damaged based on the damagedClusterSizeDef parameter.
            /// The IDs of damaged clusters are stored in the damagedClusters vector.
            /// Also marks the vertices and cells that are part of the damaged clusters using the SELECTED_PRIMITIVES (defined in hbm/VtkSelectionTools.h) scalar array.
            /// </summary>
			void identifyDamagedClusters();

		};
		
		/// <summary>
		/// Enhances vtkWindowedSincPolyDataFilter with the ability to exclude selected vertices from the smoothing process.
		/// This is done by setting the scalar array SELECTED_PRIMITIVES (macro defined in hbm/VtkSelectionTools.h) 
		/// as the point data of the input mesh - -1 to fix the vertex, 1 to not fix it.
		/// @author Tomas Janak
		/// </summary>
		/// <seealso cref="vtkWindowedSincPolyDataFilter" />
		class MESHOPTIMIZER_EXPORT vtkWindowedSincPolyDataFilterSelective : public vtkWindowedSincPolyDataFilter
		{
		public:
			static vtkWindowedSincPolyDataFilterSelective *New();

		protected:
			vtkWindowedSincPolyDataFilterSelective() {}
			~vtkWindowedSincPolyDataFilterSelective() {}
			int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

		};
	}
}

#endif
