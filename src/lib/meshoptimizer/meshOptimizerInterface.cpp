/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "meshOptimizerInterface.h"
#include "meshQualityMesquite.h"
#include "anatomyDB/query.h"
#include "kriging/KrigingPiperInterface.h"

#include <vtkPointData.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkCellQuality.h>
#include <vtkDoubleArray.h>
#include <vtkKdTreePointLocator.h>
#include <vtkMath.h>
#include <vtkCellData.h>

#include <mutex>


#include "meshOptimizer.h"

#include "hbm/HumanBodyModel.h"

#include <algorithm>
#include <set>

using namespace piper::hbm;
using namespace boost::container;

namespace piper {
    namespace meshoptimizer {
        meshOptimizerInterface::meshOptimizerInterface() :
            m_isOptimized(false),
            m_fem(nullptr),
            m_metricType(METRIC_TYPE::MESQUITE),
            m_quality(nullptr),
            m_isQualityComputed(false) {}

        meshOptimizerInterface::~meshOptimizerInterface() {
            if (m_quality != nullptr)
                delete m_quality;
        }

        void meshOptimizerInterface::reset() {
            m_fem = nullptr;
            m_groupToOptimize.clear();
            m_lowQualityElements.clear();
            m_isOptimized = false;
            m_isQualityComputed = false;
            if (m_quality != nullptr)
                delete m_quality;
            if (m_metricType == METRIC_TYPE::MESQUITE)
                m_quality = new meshQualityMesquite();
            else if (m_metricType == METRIC_TYPE::CREASEDET || m_metricType == METRIC_TYPE::SURFSMOOTH)
				m_quality = new meshDamageDetector();
        }

		void meshOptimizerInterface::updateThresholds(std::map<meshoptimizer::QualityMetrics, double>& thresholds)
		{
			switch (m_metricType)
			{
				case METRIC_TYPE::CREASEDET :
					((meshDamageDetector *)m_quality)->SetDamageSizeDef
						(thresholds.at(meshoptimizer::QualityMetrics::MSHDMGDETECTOR_ClusterSizeBoundaryPercentage));
					((meshDamageDetector *)m_quality)->SetDihedralAngleDifThreshold
						(thresholds.at(meshoptimizer::QualityMetrics::MSHDMGDETECTOR_DihedralAngleThreshold));
					break;
			}
		}

        void meshOptimizerInterface::setMetric(METRIC_TYPE metric) {
            m_metricType = metric;
            if (m_fem!=nullptr)
                initialize(*m_fem);
        }

        void meshOptimizerInterface::setOptimized(bool truefalse) {
			m_isOptimized = truefalse;
        }

		void meshOptimizerInterface::setIsQualityComputed(bool truefalse) {
            m_isQualityComputed = truefalse;
            if (!truefalse)
                m_lowQualityElements.clear();
		}
        
        
        METRIC_TYPE meshOptimizerInterface::getMetric() const {
            return m_metricType;
        }


        void meshOptimizerInterface::initialize(FEModel& fem) {
            reset();
            m_fem = &fem;
            m_quality->setMesh(*m_fem);
        }

        void meshOptimizerInterface::processBaselineMesh(FEModel& fem)
		{
			m_quality->processBaselineMesh(fem);
		}

        void meshOptimizerInterface::setQualityUseBaselineModel(bool compareToBaseline)
        {
            m_quality->setQualityUseBaselineModel(compareToBaseline);
        }

        void meshOptimizerInterface::setEntitiesToProcess(std::list<std::string> entityNames)
        {
            m_quality->setEntitiesToProcess(entityNames);
        }

        void meshOptimizerInterface::computeMeshQuality() {
            m_quality->computeQuality();
            m_isQualityComputed = true;
        }


        boost::container::vector<vtkOBBNodePtr> meshOptimizerInterface::generateSelectionBoxes(bool mergeOverlapping)
        {
            if (m_isQualityComputed)
            {
                return m_quality->generateSelectionBoxes(mergeOverlapping);
            }
            return boost::container::vector<vtkOBBNodePtr>();
        }

		std::map<Id, std::map<meshoptimizer::QualityMetrics, VId>> *meshOptimizerInterface::getLowQualityElements() {
			return &m_lowQualityElements; 
		}

        void meshOptimizerInterface::identifyLowQualityGroup(std::map<meshoptimizer::QualityMetrics, double> const& threshold,
			std::vector<Id>& Vid, std::vector<std::vector<int>>& numbelem, VId & vid_ignore) {
            m_lowQualityElements.clear();
            // 
            std::sort(vid_ignore.begin(), vid_ignore.end());
            //list of for part of 3D elements
            std::list<Id> vid3D;
            std::list<VId*> groupcontent;
            ContGroupElements3D const& g3d = m_fem->getGroupElements3D();
            for (ContGroupElements3D::const_iterator itg = g3d.begin(); itg != g3d.end(); ++itg){
                if (g3d.curDef(itg)->isPart()) {
                    // check if this group has to be ignore
                    vid3D.push_back(g3d.curId(itg));
                    groupcontent.push_back(&(g3d.curDef(itg)->get()));
                }
            }
            //
            // identified id of elements with quality does not respect threshold for each quality metrics
            VId vididentified;
            std::map<Id,std::vector<int>> vgroupeId;
            std::map<meshoptimizer::QualityMetrics, double>::const_iterator it;
            int count = 0;
            for (it = threshold.begin(); it != threshold.end(); ++it) {
                //identify element with poor quality
                VId vididentified;
                if (it->first == QualityMetrics::MESQUITE_Inverted)
                    m_quality->findmetricsequal_3D(it->first, it->second, vididentified);
                else
                    m_quality->findmetricsbelow_3D(it->first, it->second, vididentified);

                std::sort(vididentified.begin(), vididentified.end());
                // remove elements from bones etities (vid_ignore)
                if (vid_ignore.size() > 0) {
                    VId::iterator it_diff;
                    it_diff = std::set_difference(vididentified.begin(), vididentified.end(),
                        vid_ignore.begin(), vid_ignore.end(), vididentified.begin());
                    vididentified.resize(it_diff - vididentified.begin());
                }


                //get group
				std::list<VId*>::iterator itc = groupcontent.begin();
                SId intersect;
                
                for (std::list<Id>::iterator itg = vid3D.begin(); itg != vid3D.end(); ++itg){
                    std::set_intersection((*itc)->begin(), (*itc)->end(), vididentified.begin(), vididentified.end(), std::inserter(intersect, intersect.begin()));
                    //
                    if (intersect.size() > 0) {
                        // check if already groupid exist
                        if (vgroupeId.find(*itg) == vgroupeId.end()) {
                            std::vector<int> newv(threshold.size(), 0);
                            vgroupeId[*itg] = newv;
                        }
                        vgroupeId[*itg][count] = (int)intersect.size();
                        VId vintersect(intersect.begin(), intersect.end());
                        m_lowQualityElements[*itg][it->first] = vintersect;
                        intersect.clear();
                    }
                    itc++;
                }
                count++;
            }
            for (std::map<Id, std::vector<int>>::const_iterator it = vgroupeId.begin(); it != vgroupeId.end(); ++it) {
                Vid.push_back(it->first);
                numbelem.push_back(it->second);
            }
            m_groupToOptimize = Vid;

        }

        //void meshOptimizerInterface::optimizeMeshQuality(std::vector<ElemDef>& elemdef, Nodes& nd) {
        //    meshOptimizer myoptimizer;
        //    //myoptimizer.setMesh(elemdef, nd);
        //    Mesquite2::MsqError err;
        //    myoptimizer.mark_skin_fixed(err);
        //    myoptimizer.optimizeMesh_Untangle();
        //    myoptimizer.optimizeMesh_ShapeSizeImprover();
        //}

        bool meshOptimizerInterface::smoothMeshSurface(SURFSMOOTH_ALG alg, HumanBodyModel *hbm, 
            std::map<std::string, int> &paramInt, std::map<std::string, double> &paramDouble, std::map<std::string, bool> &paramBool,
            boost::container::vector<vtkOBBNodePtr> &selectionBoxes, vtkSmartPointer<vtkPoints> referencePoints)
        {
            meshOptimizer myoptimizer;
            std::list<std::string> controlEnts = m_quality->getEntitiesToProcess();
            vtkSmartPointer<vtkUnstructuredGrid> wholeMesh = m_fem->getFEModelVTK()->getVTKMesh();

			switch (alg)
			{
            {
            case SURFSMOOTH_ALG::TAUBIN_FIR:
                if (paramBool.find(SMOOTH_OPT_SELECTED_ONLY) == paramBool.end() ||
                    paramInt.find(SMOOTH_OPT_NO_OF_ITERATIONS) == paramInt.end() ||
                    paramDouble.find(SMOOTH_OPT_TAUBIN_PASS_BAND_VALUE) == paramDouble.end())
                    return false;
            case SURFSMOOTH_ALG::LAPLACE: // for both laplace and taubin, create the surface mesh
                // create the surface mesh as a aggregation of 2D elements of all entities that were selected
                vtkSmartPointer<vtkPolyData> surfaceMesh = vtkPolyData::New();
                surfaceMesh->SetPoints(m_fem->getFEModelVTK()->getVTKMesh()->GetPoints()); // the points are the same across all entities
                surfaceMesh->GetPointData()->AddArray(m_fem->getFEModelVTK()->getVTKMesh()->GetPointData()->GetArray(SELECTED_PRIMITIVES)); // copy the pointer to selection array
                surfaceMesh->Allocate();
                for (auto it = controlEnts.begin(); it != controlEnts.end(); it++)
                {
                    vtkSmartPointer<vtkPolyData> ent = m_fem->getFEModelVTK()->getEntity2DElements(*it);
                    if (ent != NULL)
                    {
                        // push back the cells of the entity to "deformed"
                        for (vtkIdType i = 0; i < ent->GetNumberOfCells(); i++)
                        {
                            vtkCell *cell = ent->GetCell(i);
                            surfaceMesh->InsertNextCell(cell->GetCellType(), cell->GetPointIds());
                        }
                    }
                }
                if (alg == SURFSMOOTH_ALG::LAPLACE)
				    myoptimizer.optimizeMesh_SurfaceLaplaceSmooth(surfaceMesh); // assumes meshDamageDetector is used as the metric, if there are other modules developed have to change this
                else // its taubin
                    myoptimizer.optimizeMesh_SurfaceTaubinSmooth(surfaceMesh, paramBool[SMOOTH_OPT_SELECTED_ONLY],
                        paramInt[SMOOTH_OPT_NO_OF_ITERATIONS], paramDouble[SMOOTH_OPT_TAUBIN_PASS_BAND_VALUE]);
				break;
			}
            case SURFSMOOTH_ALG::LOCAL_AVG:
            {
                if (paramDouble.find(SMOOTH_OPT_LOCAL_AVG_AUTOSTOP_THRESHOLD) == paramDouble.end() ||
                    paramBool.find(SMOOTH_OPT_LOCAL_AVG_AUTSTOP) == paramBool.end() ||
                    paramInt.find(SMOOTH_OPT_LOCAL_AVG_MAXITER) == paramInt.end() ||
                    paramInt.find(SMOOTH_OPT_LOCAL_AVG_NEIGHBORS) == paramInt.end())
                    return false;

                std::vector<bool> fixedNodes = std::vector<bool>(wholeMesh->GetNumberOfPoints(), false);
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(wholeMesh->GetPointData()->GetArray("nid"));
                std::map<vtkIdType, vtkIdType> inverseNid;
                for (vtkIdType i = 0; i < nid->GetNumberOfTuples(); i++)
                    inverseNid[nid->GetValue(i)] = i;
                for (auto it = controlEnts.begin(); it != controlEnts.end(); it++)
                {
                    VId temp = hbm->metadata().entity(*it).getEntityNodesIds(*m_fem);
                    // fix targets - smooth only the rest
                    for (Id nodeId : temp)
                        fixedNodes[inverseNid[nodeId]] = true;
                }
                myoptimizer.optimizeTransformation_LocalAverageSmooth(wholeMesh, referencePoints, fixedNodes,
                    paramBool[SMOOTH_OPT_LOCAL_AVG_AUTSTOP], paramDouble[SMOOTH_OPT_LOCAL_AVG_AUTOSTOP_THRESHOLD],
                    paramInt[SMOOTH_OPT_LOCAL_AVG_MAXITER], paramInt[SMOOTH_OPT_LOCAL_AVG_NEIGHBORS]);
                break;
            }
			case SURFSMOOTH_ALG::KRIG_BOX:
			{
                if (paramBool.find(SMOOTH_OPT_KRIG_SKIN_NUGGET_BY_DISTANCE_TO_BONE) == paramBool.end() ||
                    paramInt.find(SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP) == paramInt.end() ||
                    paramDouble.find(SMOOTH_OPT_KRIG_NUGGET_BONES) == paramDouble.end() ||
                    paramDouble.find(SMOOTH_OPT_KRIG_NUGGET_SKIN) == paramDouble.end() ||
                    paramDouble.find(SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP) == paramDouble.end() ||
                    selectionBoxes.empty()) 
                    return false;

#if PIPER_KRIGING_PROFILING
                time_t start = clock();
#endif
                // ==== set up control nodes and nuggets ====
                bool spy = hbm->fem().setMeshDef_INDEX();
                // create a list of control node candidates by concatenating node IDs of entities that were selected as "target" entites
				VId controlNodesCandidatesSkin;
                VId controlNodesCandidatesBones;
                std::vector<bool> fixedNodes = std::vector<bool>(wholeMesh->GetNumberOfPoints(), false);
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(wholeMesh->GetPointData()->GetArray("nid"));
                std::map<vtkIdType, vtkIdType> inverseNid;
                for (vtkIdType i = 0; i < nid->GetNumberOfTuples(); i++)
                    inverseNid[nid->GetValue(i)] = i;
                for (auto it = controlEnts.begin(); it != controlEnts.end(); it++)
                {
                    VId temp;
                    if (anatomydb::isBone(*it))
                    {
                        temp = hbm->metadata().entity(*it).getEnvelopNodes(*m_fem);
                        if (paramDouble[SMOOTH_OPT_KRIG_NUGGET_BONES] == 0) // if nugget is zero, use only surface nodes as control points
                        {
                            // fix all bone nodes
                            for (Id nodeId : temp)
                                fixedNodes[inverseNid[nodeId]] = true;
                        // use only surface nodes as control points
                        }
                        controlNodesCandidatesBones.insert(controlNodesCandidatesBones.end(), temp.begin(), temp.end());
                    }
                    else // treat everything that is not a bone as skin
                    {
                        temp = hbm->metadata().entity(*it).getEntityNodesIds(*m_fem);
                        controlNodesCandidatesSkin.insert(controlNodesCandidatesSkin.end(), temp.begin(), temp.end());
                    }
                }
#if PIPER_KRIGING_PROFILING
                time_t end = clock();
                std::cout << "Control node candidates selection done: " << ((end - start) / (double)CLOCKS_PER_SEC) << "s" << std::endl;
                start = clock();
#endif
				// nuggets
				vector<double> ctrlPtWeight;
                ctrlPtWeight.resize(wholeMesh->GetNumberOfPoints());
                // if either skin or flesh nodes are to have nuggets based on distance, we need to compute the distances
                if (controlNodesCandidatesBones.size() > 0 &&
                    paramBool[SMOOTH_OPT_KRIG_SKIN_NUGGET_BY_DISTANCE_TO_BONE] && controlNodesCandidatesSkin.size() > 0)
                {
                    // create a point locator for bone control points - we will set nuggets based on distance to them
                    vtkSmartPointer<vtkPoints> controlPoints = vtkSmartPointer<vtkPoints>::New();
                    controlPoints->Initialize();
                    double point[3];
                    for (Id boneId : controlNodesCandidatesBones)
                    {
                        referencePoints->GetPoint(boneId, point);
                        controlPoints->InsertNextPoint(point);
                    }
                    vtkSmartPointer<vtkKdTreePointLocator> ctrlPointsLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
                    vtkSmartPointer<vtkPolyData> ctrlPointsHolder = vtkSmartPointer<vtkPolyData>::New();
                    ctrlPointsHolder->SetPoints(controlPoints);
                    ctrlPointsLocator->SetDataSet(ctrlPointsHolder);
                    ctrlPointsLocator->BuildLocator();
                    // set the nuggets of skin control points equal to the distance if that was selected                   
                    for (Id skinId : controlNodesCandidatesSkin)
                        ctrlPtWeight[skinId] = -1 * sqrt(vtkMath::Distance2BetweenPoints(referencePoints->GetPoint(skinId),
                            ctrlPointsHolder->GetPoint(ctrlPointsLocator->FindClosestPoint(referencePoints->GetPoint(skinId)))));
                }
                if (!paramBool[SMOOTH_OPT_KRIG_SKIN_NUGGET_BY_DISTANCE_TO_BONE]) // if skin nuggets are set by number, set it
                {
                    for (Id skinId : controlNodesCandidatesSkin)
                        ctrlPtWeight[skinId] = paramDouble[SMOOTH_OPT_KRIG_NUGGET_SKIN];
                }
                // set bone nuggets
                for (Id boneId : controlNodesCandidatesBones)
                    ctrlPtWeight[boneId] = paramDouble[SMOOTH_OPT_KRIG_NUGGET_BONES];
                // concatenate skin and bone control nodes
                controlNodesCandidatesBones.insert(controlNodesCandidatesBones.end(), controlNodesCandidatesSkin.begin(), controlNodesCandidatesSkin.end());

#if PIPER_KRIGING_PROFILING
                end = clock();
                std::cout << "Nuggets set: " << ((end - start) / (double)CLOCKS_PER_SEC) << "s" << std::endl;
                start = clock();
#endif
                // ==== perform the actual kriging ====
                KrigingPiperInterface k;
                k.applyDeformationInABox(wholeMesh, referencePoints, controlNodesCandidatesBones, fixedNodes, ctrlPtWeight, selectionBoxes,
                    paramInt[SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP], paramDouble[SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP],
                    paramBool[SMOOTH_OPT_KRIG_INTERP_DISPLACEMENT], paramBool[SMOOTH_OPT_KRIG_USE_DRIFT], false, wholeMesh);

                if (spy) hbm->fem().setMeshDef_NID();
				break;
			}
			case SURFSMOOTH_ALG::ELE_INV_KRIG:
            {
            /*    bool spy = hbm->fem().setMeshDef_INDEX();
                if (paramInt.find(SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP) == paramInt.end() ||
                    paramInt.find(SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_TP) == paramInt.end() ||
                    paramDouble.find(SMOOTH_OPT_KRIG_NUGGET_INTERNALP) == paramDouble.end() ||
                    paramDouble.find(SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP) == paramDouble.end() ||
                    selectionBoxes.empty())
                    return false;
				// first detect inverted elements using VTK

				vtkSmartPointer<vtkCellQuality> cellQuality = vtkSmartPointer<vtkCellQuality>::New();
				cellQuality->SetQualityMeasureToVolume();
				cellQuality->SetInputData(wholeMesh);
				cellQuality->Update();

				vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(cellQuality->GetOutput()->GetCellData()->GetArray("CellQuality"));
				wholeMesh->GetCellData()->AddArray(quality);

				int numberOfInvEle = 0;
				for (int i = 0; i < wholeMesh->GetNumberOfCells(); i++)
				{
					// check negative volume only for volumetric cells
					if ((wholeMesh->GetCell(i)->GetCellType() == VTKCellType::VTK_HEXAHEDRON ||
						wholeMesh->GetCell(i)->GetCellType() == VTKCellType::VTK_TETRA) && quality->GetValue(i) < 0)
						numberOfInvEle++;
				}

				std::cout << "inverted elements: " << numberOfInvEle << std::endl;

				// build an axis aligned box of the size 20x length of the element (diagonal of bounding box) around each element with negative value
				vector<vtkOBBNodePtr> OBBs;
				for (int i = 0; i < wholeMesh->GetNumberOfCells(); i++)
				{
					// check negative volume only for volumetric cells
					if ((wholeMesh->GetCell(i)->GetCellType() == VTKCellType::VTK_HEXAHEDRON ||
						wholeMesh->GetCell(i)->GetCellType() == VTKCellType::VTK_TETRA) && quality->GetValue(i) < 0)
					{
						vtkCell *cell = wholeMesh->GetCell(i);
						
						double bboxSize = 2 * sqrt(cell->GetLength2());// get length of the cell
						std::cout << "box size: " << bboxSize / 2 << std::endl;
						vtkOBBNodePtr node = std::make_shared<vtkOBBNode>();
						// take the 0-th point of the cell as a reference point around which to build the box 
						// taking barycenter would be more precise but should not matter too much
						double *refPoint = wholeMesh->GetPoint(cell->GetPointId(0));
						for (int j = 0; j < 3; j++)
						{
							node->Corner[j] = refPoint[j] - bboxSize / 2; // make it so that the reference point is in the middle of the box
							// set the axes to the bbox size in the direction of the main axis, 0 otherwise - we create an axis aligned bounding cube
							node->Axes[j][0] = j == 0 ? bboxSize : 0;
							node->Axes[j][1] = j == 1 ? bboxSize : 0;
							node->Axes[j][2] = j == 2 ? bboxSize : 0;
						}
						OBBs.push_back(node);
					}
				}
				std::cout << "number of boxes before merge : " << OBBs.size();
				// merge the boxes
				vtkSelectionTools::MergeOverlappingBoxes(OBBs);
				std::cout << ", number of boxes after merge : " << OBBs.size() << std::endl;


				// get all nodes on the bones - do not want to apply kriging to those
				VId boneNodes = hbm->findBoneNodes();
				vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();

				// nuggets
				VId surfaceBoneNodes = hbm->findBoneNodes(true, false, true);
				boost::container::vector<double> ctrlPtWeight;
				ctrlPtWeight.resize(wholeMesh->GetNumberOfPoints());
				// create a point locator for bones - we will set nuggets based on distance to bones
				vtkSmartPointer<vtkPoints> bonePoints = vtkSmartPointer<vtkPoints>::New();
				bonePoints->Initialize();
				for (auto it = surfaceBoneNodes.begin(); it < surfaceBoneNodes.end(); it++)
                {
                    double point[3];
                    referencePoints->GetPoint(*it, point);
                    bonePoints->InsertNextPoint(point);
                }
				vtkSmartPointer<vtkKdTreePointLocator> bonePointsLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
				vtkSmartPointer<vtkPolyData> bonePointsHolder = vtkSmartPointer<vtkPolyData>::New();
				bonePointsHolder->SetPoints(bonePoints);
				bonePointsLocator->SetDataSet(bonePointsHolder);
				bonePointsLocator->BuildLocator();
				// set the nuggets for all points as if they were internal flesh. set it equal to the squared distance to the closest surface bone node
				for (int i = 0; i < wholeMesh->GetNumberOfPoints(); i++)
                    ctrlPtWeight[i] = -1 * sqrt(vtkMath::Distance2BetweenPoints(referencePoints->GetPoint(i),
						bonePointsHolder->GetPoint(bonePointsLocator->FindClosestPoint(referencePoints->GetPoint(i)))));
				// now rewrite nugget values for nodes that belong to bones to 0 and skin nodes the right skin value
				for (auto it = boneNodes.begin(); it != boneNodes.end(); it++)
					ctrlPtWeight[*it] = 0;

                doKrigingInABox(wholeMesh, referencePoints, boneNodes, ctrlPtWeight, OBBs,
                    paramInt[SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP],
                    paramInt[SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_TP], paramDouble[SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP]);

                if (spy) hbm->fem().setMeshDef_NID();*/
				break;
			}
			}

			// propagate the changes back to the FE model
			m_fem->getFEModelVTK()->updateFEmodelNodes(m_fem);
            m_isQualityComputed = false; // invalidate quality
            return true;
		}

        void meshOptimizerInterface::optimizeMeshQuality(Id const id) {
            meshOptimizer myoptimizer;
            FEModel femoptim;
            const VId cur = m_fem->get<GroupElements3D>(id)->get();
            VId nid;
			for (VId::const_iterator ite = cur.begin(); ite != cur.end(); ++ite) {
                ElemDef& elem = m_fem->get<Element3D>(*ite)->get();
                nid.insert(nid.end(), elem.begin(), elem.end());
                femoptim.set<Element3D>(m_fem->get<Element3D>(*ite));
            }
            std::sort(nid.begin(), nid.end());
            VId::iterator it;
            it = std::unique(nid.begin(), nid.end());
            nid.resize(std::distance(nid.begin(), it));
            for (VId::const_iterator itn = nid.begin(); itn != nid.end(); ++itn){
                femoptim.set<Node>(m_fem->get<Node>(*itn));
            }
            myoptimizer.setMesh(femoptim);
            Mesquite2::MsqError err;
            myoptimizer.mark_skin_fixed(err);
            myoptimizer.optimizeMesh_Untangle(600);
            myoptimizer.optimizeMesh_ShapeSizeImprover();
        }

        void meshOptimizerInterface::optimizeMeshQuality(Id const id, VId const& vidSkin, std::vector<meshoptimizer::QualityMetrics> const& metrics) {
            //get nodes of elemets with poor quality
            VId vidn;
            for (auto it = metrics.begin(); it != metrics.end(); ++it) {
                vidn.clear();
                VId videlemets = m_lowQualityElements[id][*it];
                for (auto itt = videlemets.begin(); itt != videlemets.end(); ++itt) {
                    vidn.insert(vidn.end(), m_fem->get<Element3D>(*itt)->get().begin(), m_fem->get<Element3D>(*itt)->get().end());
                }

                //std::cout << "vidn size : " << vidn.size() <<std::endl;
                std::sort(vidn.begin(), vidn.end());
                VId::iterator itt;
                itt = std::unique(vidn.begin(), vidn.end());
                vidn.resize(std::distance(vidn.begin(), itt));
                // 
                //
                meshOptimizer myoptimizer;
                FEModel femoptim;
				VId cur = m_fem->get<GroupElements3D>(id)->get();
                VId nid;
                for (VId::const_iterator ite = cur.begin(); ite != cur.end(); ++ite) {
                    ElemDef& elem = m_fem->get<Element3D>(*ite)->get();
                    nid.insert(nid.end(), elem.begin(), elem.end());
                    femoptim.set<Element3D>(m_fem->get<Element3D>(*ite));
                }
                std::sort(nid.begin(), nid.end());
                VId::iterator itn;
                itn = std::unique(nid.begin(), nid.end());
                nid.resize(std::distance(nid.begin(), itn));
                for (VId::const_iterator itn = nid.begin(); itn != nid.end(); ++itn){
                    femoptim.set<Node>(m_fem->get<Node>(*itn));
                }
                myoptimizer.setMesh(femoptim);
                //
                Mesquite2::MsqError err;
                // apply untangle ir required
                int nbinverted = 0;
                if (*it == meshoptimizer::QualityMetrics::MESQUITE_Inverted) {
                    //std::cout << "apply untangler" << std::endl;
                    myoptimizer.mark_skin_fixed(err);
                    SId intersect;
                    std::set_intersection(vidSkin.begin(), vidSkin.end(), vidn.begin(), vidn.end(), std::inserter(intersect, intersect.begin()));
                    // add ddl for nodes on skin for inverted elements
                    if (intersect.size() > 0) {
                        VId vintersect(intersect.begin(), intersect.end());
                        myoptimizer.release_skin_fixed(vintersect);
                    }
                    nbinverted = myoptimizer.optimizeMesh_Untangle(600);
                }
                if ((*it == meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio) && (nbinverted<=0)) {
                    //std::cout << "apply shape improver" << std::endl;
                    // fix all nodes at mesh boundaries
                    myoptimizer.set_all_fixed_flags(false, err);
                    myoptimizer.mark_skin_fixed(err);
                    //myoptimizer.optimizeMesh_ShapeSizeImprover();
                }
            }
            m_isOptimized = true;

        }

        void meshOptimizerInterface::optimizeMeshQuality_selectedOnly(Metadata::EntityCont const& entities, bool ignoreBones)
        {
            if (!isOptimized() && m_metricType == METRIC_TYPE::MESQUITE)
            {
                MeshDefType meshdef = m_fem->getMeshDefType();
                auto parts = m_fem->getFEModelVTK()->extractSelectedParts(ignoreBones, entities, *m_fem, true, true, false); // extract only 3D elements
                    // origPoints/cell give us the original ID in the vtk mesh - but to get the FE IDs, we have to map those as well
                vtkIdTypeArray *nidArray = vtkIdTypeArray::SafeDownCast(m_fem->getFEModelVTK()->getVTKMesh()->GetPointData()->GetArray("nid"));
                vtkIdTypeArray *eidArray = vtkIdTypeArray::SafeDownCast(m_fem->getFEModelVTK()->getVTKMesh()->GetCellData()->GetArray("eid"));
                vtkIdType nParts = parts.size();
                FEModel *partsFE = new FEModel[nParts];
                for (vtkIdType partID = 0; partID < nParts; partID++)
                {
                    vtkIdTypeArray *origPoints = vtkIdTypeArray::SafeDownCast(parts[partID]->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                    vtkIdTypeArray *origCells = vtkIdTypeArray::SafeDownCast(parts[partID]->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                    vtkIdType nCells = origCells->GetNumberOfTuples();
                    vtkIdType nPoints = origPoints->GetNumberOfTuples();
                    for (vtkIdType i = 0; i < nCells; i++)
                        partsFE[partID].set<Element3D>(m_fem->get<Element3D>((meshdef == INDEX ? origCells->GetValue(i) :
                        eidArray->GetValue(origCells->GetValue(i)))));
                    for (vtkIdType i = 0; i < nPoints; i++)
                        partsFE[partID].set<Node>(m_fem->get<Node>((meshdef == INDEX ? origPoints->GetValue(i) :
                        nidArray->GetValue(origPoints->GetValue(i)))));
                }
                // use dynamic scheduling, we don't which part will be the bottleneck, so don't let it slow down everything else
                #pragma omp parallel for schedule(dynamic, 1)  shared(partsFE)
                for (vtkIdType partID = 0; partID < nParts; partID++)
                {
                    meshOptimizer myoptimizer;
                    size_t nCells = partsFE[partID].getNbElement3D();
                    myoptimizer.setMesh(partsFE[partID]);
                    Mesquite2::MsqError err;
                    myoptimizer.mark_skin_fixed(err);
                    myoptimizer.optimizeMesh_Untangle(nCells > 20000 ? 100.0 : nCells / 200.0);
                    myoptimizer.optimizeMesh_ShapeSizeImprover(nCells > 20000 ? 100.0 : nCells / 200.0);
                }

                m_isOptimized = true;
                delete[] partsFE;
            }
        }
        void meshOptimizerInterface::optimizeMeshQuality() {
            if (!isOptimized() && m_metricType == METRIC_TYPE::MESQUITE) {
                //for each of these group, launch optim
                for (VId::const_iterator it = m_groupToOptimize.begin(); it != m_groupToOptimize.end(); ++it){
                    optimizeMeshQuality(*it);
                }
                m_isOptimized = true;
            }
        }

        double meshOptimizerInterface::getMaxValueMetric3D(meshoptimizer::QualityMetrics const& metric) const{
            return m_quality->getMaxValueMetric3D(metric);
        }

        double meshOptimizerInterface::getMinValueMetric3D(meshoptimizer::QualityMetrics const& metric) const{
            return m_quality->getMinValueMetric3D(metric);
        }

        void meshOptimizerInterface::exportQuality(std::string const& filename) const {
            m_quality->writeQualityResults(filename);
        }

    }//meshoptimizer
}//piper
