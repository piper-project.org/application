/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include "TestHelper.h"

#include "meshoptimizer/meshOptimizer.h"
#include "hbm/HumanBodyModel.h"

#include <algorithm>

using namespace piper;

class meshoptimizer_test : public ::testing::Test {

protected:

    virtual void SetUp()
    {
        hbm::Node(); //to force offset id
        hbm.setSource("meshoptimizer_optim.pmr");

        femodel = hbm.fem();
        myOptimizer.setMesh(femodel);

    }

    void check() {
        Mesquite::MsqError err;
        EXPECT_EQ(femodel.getNbNode(), myOptimizer.getNumberVertices());
        EXPECT_EQ(femodel.getNbElement2D() + femodel.getNbElement3D(), myOptimizer.getNumberElements());
        EXPECT_EQ(3, myOptimizer.get_geometric_dimension(err));
        EXPECT_EQ(myOptimizer.max_vertex_index(), myOptimizer.getNumberVertices());
        EXPECT_EQ(myOptimizer.max_element_index(), myOptimizer.getNumberElements());
        hbm::VId tt2 = femodel.getElements3D().listId();
    }

    void checkMeshElement() {
        Mesquite::MsqError err;
        std::vector<Mesquite2::EntityTopology> topologies;
        topologies.resize(myOptimizer.getNumberElements());
        Mesquite2::EntityTopology  topology;
        std::vector<Mesquite2::Mesh::ElementHandle> velth;
        myOptimizer.get_all_elements(velth, err);
        EXPECT_EQ(velth.size(), femodel.getNbElement2D() + femodel.getNbElement3D());
        myOptimizer.elements_get_topologies(&velth[0], &topologies[0], myOptimizer.getNumberElements(), err);
        std::vector<Mesquite2::Mesh::VertexHandle> vertices;
        std::vector<size_t> offsets;
        myOptimizer.elements_get_attached_vertices(&velth[0], myOptimizer.getNumberElements(), vertices, offsets, err);
        //get tag eid
        std::vector<int> tageid;
        tageid.resize(myOptimizer.getNumberElements());
        Mesquite2::TagHandle tag_eid = myOptimizer.tag_get("eid", err);
        myOptimizer.tag_get_element_data(tag_eid, myOptimizer.getNumberElements(), &velth[0], &tageid[0], err);
        hbm::VId eid_fe, eid2D_fe, eid3D_fe;
        eid_fe = femodel.getElements2D().listId();
        eid3D_fe = femodel.getElements3D().listId();
        eid_fe.insert(eid_fe.end(), eid3D_fe.begin(), eid3D_fe.end());
        hbm::ElemDef curelem;
        for (int e = 0; e < myOptimizer.getNumberElements(); ++e) {
            // access by index
            std::vector<size_t> curelt = myOptimizer.element_connectivity(e, err);
            topology = myOptimizer.get_element_topology(e, err);
            //
            EXPECT_TRUE(topology == topologies[e]);
            for (size_t n = 0; n < curelt.size(); n++) {
                size_t tmp = (size_t)vertices[offsets[e]+ n];
                EXPECT_EQ(curelt[n], tmp);
            }
            EXPECT_EQ(tageid[e], eid_fe[e]);
            if (e < femodel.getNbElement2D())
                curelem = femodel.get<hbm::Element2D>(tageid[e])->get();
            else
                curelem = femodel.get<hbm::Element3D>(tageid[e])->get();
            // check size
            EXPECT_EQ(curelem.size(), curelt.size());
            //check topo
            if (e < femodel.getNbElement2D()) {
                if (curelem.size() == 3)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::TRIANGLE);
                else if (curelem.size() == 4)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::QUADRILATERAL);
            }
            else {
                if (curelem.size() == 4)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::TETRAHEDRON);
                else if (curelem.size() == 5)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::PYRAMID);
                else if (curelem.size() == 6)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::PRISM);
                else if (curelem.size() == 8)
                    EXPECT_TRUE(topology == Mesquite2::EntityTopology::HEXAHEDRON);
            }
            // check elemdef
            hbm::ElemDef cureltdef;
            for (size_t id : curelt)
                cureltdef.push_back((hbm::Id)id);
            EXPECT_TRUE(VectorMatch(curelem, cureltdef));
        }
    }

    void checkMeshVertex() {
        Mesquite::MsqError err;
        std::vector<Mesquite2::Mesh::VertexHandle> vverth;
        myOptimizer.get_all_vertices(vverth, err);
        EXPECT_EQ(vverth.size(), femodel.getNbNode());
        EXPECT_EQ((Mesquite2::Mesh::VertexHandle)0, vverth[0]);
        EXPECT_EQ(vverth[vverth.size() - 1], (Mesquite2::Mesh::VertexHandle)(myOptimizer.getNumberVertices()-1));
        //
        std::vector<Mesquite2::MsqVertex> coordinates;
        coordinates.resize(myOptimizer.getNumberVertices());
        myOptimizer.vertices_get_coordinates(&vverth[0], &coordinates[0], myOptimizer.getNumberVertices(), err);

        //get tag nid
        std::vector<int> tagnid;
        Mesquite2::TagHandle tag_nid = myOptimizer.tag_get("nid", err);
        tagnid.resize(myOptimizer.getNumberVertices());
        myOptimizer.tag_get_vertex_data(tag_nid, myOptimizer.getNumberVertices(), &vverth[0], &tagnid[0], err);
        hbm::VId nid_fe = femodel.getNodes().listId();

        for (int e = 0; e < myOptimizer.getNumberVertices(); ++e) {
            EXPECT_TRUE(nid_fe[e] == tagnid[e]);
            // compare to fem
            double coord[3];
            coordinates[e].get_coordinates(coord);
            EXPECT_EQ(coord[0], femodel.get<hbm::Node>(tagnid[e])->getCoordX());
            EXPECT_EQ(coord[1], femodel.get<hbm::Node>(tagnid[e])->getCoordY());
            EXPECT_EQ(coord[2], femodel.get<hbm::Node>(tagnid[e])->getCoordZ());

            if (e == 10) {
                double* coordold = coord;
                double coordnew[3] = { 1.2, 1.3, 1.4 };
                coordinates[e].set(coordnew);
                myOptimizer.vertex_set_coordinates(vverth[e], coordinates[e], err);
                myOptimizer.vertices_get_coordinates(&vverth[e], &coordinates[e], 1, err);
                coordinates[e].get_coordinates(coord);
                EXPECT_EQ(coord[0], coordnew[0]);
                EXPECT_EQ(coord[1], coordnew[1]);
                EXPECT_EQ(coord[2], coordnew[2]);
                EXPECT_EQ(coord[0], femodel.get<hbm::Node>(tagnid[e])->getCoordX());
                EXPECT_EQ(coord[1], femodel.get<hbm::Node>(tagnid[e])->getCoordY());
                EXPECT_EQ(coord[2], femodel.get<hbm::Node>(tagnid[e])->getCoordZ());
                //
                coordinates[e].set(coordold);
                myOptimizer.vertex_set_coordinates(vverth[e], coordinates[e], err);
                EXPECT_EQ(coordold[0], femodel.get<hbm::Node>(tagnid[e])->getCoordX());
                EXPECT_EQ(coordold[1], femodel.get<hbm::Node>(tagnid[e])->getCoordY());
                EXPECT_EQ(coordold[2], femodel.get<hbm::Node>(tagnid[e])->getCoordZ());
            }
        }
        //check elements attached to node id 33
        hbm::NodePtr cur = femodel.get<hbm::Node>("*NODE", 33);
        Mesquite2::Mesh::VertexHandle vh;
        EXPECT_TRUE(myOptimizer.getVertexHandle(vh, cur->getId(), err));
        EXPECT_EQ(26, (size_t)vh);
        std::vector<Mesquite2::Mesh::ElementHandle> elementsattached;
        std::vector<size_t> offsets;
        myOptimizer.vertices_get_attached_elements(&vh, 1, elementsattached, offsets, err);
        EXPECT_TRUE(elementsattached.size() == 4);
        //
        std::vector<Mesquite2::Mesh::ElementHandle> velth;
        myOptimizer.get_all_elements(velth, err);
        std::vector<int> tageid;
        tageid.resize(myOptimizer.getNumberElements());
        Mesquite2::TagHandle tag_eid = myOptimizer.tag_get("eid", err);
        myOptimizer.tag_get_element_data(tag_eid, myOptimizer.getNumberElements(), &velth[0], &tageid[0], err);
        //
        hbm::Element3DPtr cure = femodel.get<hbm::Element3D>("*ELEMENT_SOLID", 5);
        EXPECT_EQ(cure->getId(), tageid[(size_t)elementsattached[0]]);
        cure = femodel.get<hbm::Element3D>("*ELEMENT_SOLID", 6);
        EXPECT_EQ(cure->getId(), tageid[(size_t)elementsattached[1]]);
        cure = femodel.get<hbm::Element3D>("*ELEMENT_SOLID", 7);
        EXPECT_EQ(cure->getId(), tageid[(size_t)elementsattached[2]]);
        cure = femodel.get<hbm::Element3D>("*ELEMENT_SOLID", 8);
        EXPECT_EQ(cure->getId(), tageid[(size_t)elementsattached[3]]);

    }

    void checkFixSkin() {

        Mesquite2::MsqError err;
        std::vector<Mesquite2::Mesh::VertexHandle> vverth;
        myOptimizer.get_all_vertices(vverth, err);
        myOptimizer.mark_skin_fixed(err);
        std::vector<bool> fixed_flag_array;
        myOptimizer.vertices_get_fixed_flag(&vverth[0], fixed_flag_array, myOptimizer.getNumberVertices(), err);
        int nbtrue = 0;
        int nbfalse=0;
        for (size_t n = 0; n < myOptimizer.getNumberVertices(); n++) {
            if (fixed_flag_array[n]) nbtrue++;
            else nbfalse++;
        }
        EXPECT_TRUE(nbtrue==60);
        EXPECT_TRUE(nbfalse == 7);
    }

    void checkUntangle() {
        Mesquite2::MsqError err;
        //myOptimizer.mark_skin_fixed(err);
        myOptimizer.optimizeMesh_Untangle(600);
        // compute quality of improved mes mesh
        //myQuality.setMesh(femodel);
        //myQuality.computeQuality();
        //hbm::VId vid = femodel.getElements3D().listId();
        //double myJacobian3DImproved;
        //for (size_t i = 0; i < vid.size(); i++) {
        //    myQuality.getQualityMetrics_3D(meshoptimizer::QualityMetrics::SCALED_JACOBIAN, vid[i], myJacobian3DImproved);
        //    EXPECT_TRUE(myJacobian3DImproved >= myJacobian3D[i]);
        //}
    }

    //void checkShapeImprover() {
    //    Mesquite2::MsqError err;
    //    myOptimizer.mark_skin_fixed(err);
    //    myOptimizer.optimizeMesh_ShapeSizeImprover();
    //    // compute quality of improved mes mesh
    //    myQuality.setMesh(femodel);
    //    myQuality.computeQuality();
    //    hbm::VId vid = femodel.getElements3D().listId();
    //    double myShapeMetrics3DImproved;
    //    for (size_t i = 0; i < vid.size(); i++) {
    //        myQuality.getQualityMetrics_3D(meshoptimizer::QualityMetrics::SCALED_JACOBIAN, vid[i], myShapeMetrics3DImproved);
    //        EXPECT_TRUE(myShapeMetrics3DImproved >= myShapeMetrics3D[i]);
    //    }
    //}
    
    //void checkMeshVertexTopology() {

    //}

    //void checkMeshElementTopology() {

    //}

    hbm::HumanBodyModel hbm;
    hbm::FEModel femodel;
    meshoptimizer::meshOptimizer myOptimizer;

};


TEST_F(meshoptimizer_test, test_meshmesquite) {
    check();
    checkMeshElement();
    checkMeshVertex();
    checkFixSkin();
}


TEST_F(meshoptimizer_test, checkUntangle) {
    checkUntangle();
}

//TEST_F(meshoptimizer_test, checkShapeImprover) {
//    checkShapeImprover();
//}
//
//TEST_F(meshquality_test, checkScaledJacobian3D) {
//    checkScaledJacobian2D();
//}
//
//TEST_F(meshquality_test, checkShapeSize3D) {
//    checkShapeSize2D();
//}
