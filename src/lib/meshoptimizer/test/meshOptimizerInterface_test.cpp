/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include "TestHelper.h"

#include "meshoptimizer/meshOptimizerInterface.h"
#include "hbm/HumanBodyModel.h"

#include <algorithm>

using namespace piper;

class meshOptimizerInteface_test : public ::testing::Test {

protected:

    virtual void SetUp()
    {
        hbm.setSource("meshoptimizer_optim_quality_3.pmr");
        myInterface.reset();
        myInterface.setMetric(meshoptimizer::METRIC_TYPE::MESQUITE);
        myInterface.initialize(hbm.fem());
        myInterface.computeMeshQuality();
        //m_threshold[meshoptimizer::QualityMetrics::MESQUITE_Inverted] = 1;
        m_threshold[meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio] = 1;
        //
        idgcheck = *hbm.metadata().entity("Entity_3").get_groupElement3D().begin();
    }


    void checkLowQualityGroup() {
        hbm::VId vidg;
        hbm::VId vidignore;
        std::vector<std::vector<int>> numbelem; 
        myInterface.identifyLowQualityGroup(m_threshold, vidg, numbelem, vidignore);
        EXPECT_EQ(idgcheck, vidg[0]);
    }

    void checkOptimSkin() {
        hbm::VId vidSkin = hbm.metadata().entity("Skin").getEntityNodesIds(hbm.fem());
        EXPECT_EQ(vidSkin.size(), 26);
        hbm::IdKey idncheck, idfe;
        std::string key;
        hbm.fem().getInfoId<hbm::Node>(vidSkin.at(0), key, idfe);
        EXPECT_EQ(idfe.id, 51);
        hbm.fem().getInfoId<hbm::Node>(vidSkin.at(5), key, idfe);
        EXPECT_EQ(idfe.id, 74);
        hbm.fem().getInfoId<hbm::Node>(vidSkin.at(15), key, idfe);
        EXPECT_EQ(idfe.id, 55);
        //
        hbm::NodePtr nd = hbm.fem().get<hbm::Node>( "*NODE", hbm::IdKey(71));
        hbm::Coord coordbefore = nd->get();
        std::vector<meshoptimizer::QualityMetrics> metrics;
        metrics.push_back(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio);
        myInterface.optimizeMeshQuality(idgcheck, vidSkin, metrics);
        hbm::Coord coordafter = nd->get();
        EXPECT_NEAR(coordbefore[0], coordafter[0],1e-6);
        EXPECT_NEAR(coordbefore[1], coordafter[1], 1e-6);
        EXPECT_NEAR(coordbefore[2], coordafter[2], 1e-6);

    }





    hbm::HumanBodyModel hbm;
    hbm::FEModel femodel;
    meshoptimizer::meshOptimizerInterface myInterface;
    std::map<meshoptimizer::QualityMetrics, double> m_threshold;
    hbm::Id idgcheck;
    int numelemcheck;
};


TEST_F(meshOptimizerInteface_test, test_improveskin) {
    checkLowQualityGroup();
    checkOptimSkin();
}
