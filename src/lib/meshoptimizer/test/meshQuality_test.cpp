/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "gtest/gtest.h"
#include "TestHelper.h"

#include "meshoptimizer/meshQualityMesquite.h"

#include "hbm/HumanBodyModel.h"

using namespace piper;

class meshquality_test : public ::testing::TestWithParam<const char*> {

protected:

    virtual void SetUp()
    {
     /*   if (strncmp(TestWithParam<const char*>::GetParam(), "VTK", 3) == 0) {
            model.readModel("meshoptimizer_optim_quality.vtu");
            myQuality = new meshoptimizer::meshQualityVTK();
            myQuality->setMesh(model);
            valueCheck_VTK_SizeShape2D.clear();
            valueCheck_VTK_Jacobian2D.clear();

            valueCheck_VTK_SizeShape2D.push_back(0.716267);
            valueCheck_VTK_SizeShape2D.push_back(0.915382);
            valueCheck_VTK_SizeShape2D.push_back(0.880698);
            valueCheck_VTK_SizeShape2D.push_back(0.582712);

            valueCheck_VTK_Jacobian2D.push_back(0.829662);
            valueCheck_VTK_Jacobian2D.push_back(0.99558);
            valueCheck_VTK_Jacobian2D.push_back(0.981637);
            valueCheck_VTK_Jacobian2D.push_back(0.627562);

            valueCheck_VTK_SizeShape3D.clear();
            valueCheck_VTK_Jacobian3D.clear();

            valueCheck_VTK_Jacobian3D.push_back(0.904865);
            valueCheck_VTK_Jacobian3D.push_back(0.579457);

            valueCheck_VTK_SizeShape3D.push_back(0.291221);
            valueCheck_VTK_SizeShape3D.push_back(0.0732833);
        }

        else */
        if (strncmp(TestWithParam<const char*>::GetParam(), "MES1", 4) == 0) {
            model.readModel("meshoptimizer_optim_quality_1.vtu");
            myQuality = new meshoptimizer::meshQualityMesquite();
            myQuality->setMesh(model);

            valueCheck_MES_Inverted.clear();
            valueCheck_MES_Inverted.resize(16);
            std::fill(valueCheck_MES_Inverted.begin(), valueCheck_MES_Inverted.end(), 0);
            
            valueCheck_MES_UntangleBeta.clear();
            valueCheck_MES_UntangleBeta.resize(16);
            std::fill(valueCheck_MES_UntangleBeta.begin(), valueCheck_MES_UntangleBeta.end(), 0);

            valueCheck_MES_InverseMeanRatio.clear();
            valueCheck_MES_InverseMeanRatio.resize(16);
            std::fill(valueCheck_MES_InverseMeanRatio.begin(), valueCheck_MES_InverseMeanRatio.end(), 1.19055);
            valueCheck_MES_InverseMeanRatio[0]=1.26236;
            valueCheck_MES_InverseMeanRatio[1] = 1.17567;
            valueCheck_MES_InverseMeanRatio[2] = 1.26232;
            valueCheck_MES_InverseMeanRatio[3] = 1.20103;
            valueCheck_MES_InverseMeanRatio[4] = 1.26232;
            valueCheck_MES_InverseMeanRatio[5] = 1.20103;
            valueCheck_MES_InverseMeanRatio[6] = 1.27677;
            valueCheck_MES_InverseMeanRatio[7] = 1.22452;

            //valueCheck_MES_ConditionNumber.clear();
            //valueCheck_MES_ConditionNumber.resize(16);
            //std::fill(valueCheck_MES_ConditionNumber.begin(), valueCheck_MES_ConditionNumber.end(), 1.22474);
            //valueCheck_MES_ConditionNumber[0] = 1.31242;
            //valueCheck_MES_ConditionNumber[1] = 1.19718;
            //valueCheck_MES_ConditionNumber[2] = 1.3081;
            //valueCheck_MES_ConditionNumber[3] = 1.22667;
            //valueCheck_MES_ConditionNumber[4] = 1.3081;
            //valueCheck_MES_ConditionNumber[5] = 1.22667;
            //valueCheck_MES_ConditionNumber[6] = 1.32777;
            //valueCheck_MES_ConditionNumber[7] = 1.25717;

        }

        else if (strncmp(TestWithParam<const char*>::GetParam(), "MES2", 4) == 0) {
            model.readModel("meshoptimizer_optim_quality_2.vtu");
            myQuality = new meshoptimizer::meshQualityMesquite();
            myQuality->setMesh(model);

            valueCheck_MES_Inverted.clear();
            valueCheck_MES_Inverted.resize(16);
            std::fill(valueCheck_MES_Inverted.begin(), valueCheck_MES_Inverted.end(), 0);
            valueCheck_MES_Inverted[4] = 1;
            valueCheck_MES_Inverted[5] = 1;


            valueCheck_MES_UntangleBeta.clear();
            valueCheck_MES_UntangleBeta.resize(16);
            std::fill(valueCheck_MES_UntangleBeta.begin(), valueCheck_MES_UntangleBeta.end(), 0);
            valueCheck_MES_UntangleBeta[4] = 81.3173;
            valueCheck_MES_UntangleBeta[5] = 7.07107e-009;

            valueCheck_MES_InverseMeanRatio.clear();
            valueCheck_MES_InverseMeanRatio.resize(16);
            std::fill(valueCheck_MES_InverseMeanRatio.begin(), valueCheck_MES_InverseMeanRatio.end(), 1.19055);
            valueCheck_MES_InverseMeanRatio[0] = 6.41018;
            valueCheck_MES_InverseMeanRatio[1] = 2.79075;
            valueCheck_MES_InverseMeanRatio[2] = 2.98217;
            valueCheck_MES_InverseMeanRatio[3] = 1.66799;
            valueCheck_MES_InverseMeanRatio[4] = 0;
            valueCheck_MES_InverseMeanRatio[5] = 0;
            valueCheck_MES_InverseMeanRatio[6] = 6.41018;
            valueCheck_MES_InverseMeanRatio[7] = 2.79075;

            //valueCheck_MES_ConditionNumber.clear();
            //valueCheck_MES_ConditionNumber.resize(16);
            //std::fill(valueCheck_MES_ConditionNumber.begin(), valueCheck_MES_ConditionNumber.end(), 1.22474);
            //valueCheck_MES_ConditionNumber[0] = 13.0017;
            //valueCheck_MES_ConditionNumber[1] = 4.06383;
            //valueCheck_MES_ConditionNumber[2] = 5.04707;
            //valueCheck_MES_ConditionNumber[3] = 1.61842;
            //valueCheck_MES_ConditionNumber[4] = 1e+006;
            //valueCheck_MES_ConditionNumber[5] = 1e+006;
            //valueCheck_MES_ConditionNumber[6] = 13.0017;
            //valueCheck_MES_ConditionNumber[7] = 4.06383;
        }

        myQuality->computeQuality();
        //myQuality->writeQualityResults("data/model_quality.res");
    }

    void TearDown() {
        delete myQuality;
        model.clear();
    }

    void checkQualityResults() {
     /*   if (strncmp(TestWithParam<const char*>::GetParam(), "VTK", 3) == 0) {
            checkScaledJacobian2D();
            checkShapeSize2D();
            checkScaledJacobian3D();
            checkShapeSize3D();
        }
        else*/ {
            checkMES_Inverted();
            checkMES_UntangleBeta();
            checkMES_InverseMeanRatio();
            //checkMES_ConditionNumber();
        }
    }
    /* VTK Quality - deprecated
    void checkScaledJacobian2D() {
        hbm::VId vid = model.getElements2D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_2D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN, vid[i], value));
            EXPECT_NEAR(value, valueCheck_VTK_Jacobian2D[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_2D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_VTK_Jacobian2D, vvalue, precision));

    }

    void checkShapeSize2D() {
        hbm::VId vid = model.getElements2D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_2D(meshoptimizer::QualityMetrics::VTK_SIZE_SHAPE, vid[i], value));
            EXPECT_NEAR(value, valueCheck_VTK_SizeShape2D[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_2D(meshoptimizer::QualityMetrics::VTK_SIZE_SHAPE, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_VTK_SizeShape2D, vvalue, precision));
    }

    void checkScaledJacobian3D() {
        hbm::VId vid = model.getElements3D().listId();
        vid.resize(2);
        double value;
        for (size_t i = 0; i < 2; i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN, vid[i], value));
            EXPECT_NEAR(value, valueCheck_VTK_Jacobian3D[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::VTK_SCALED_JACOBIAN, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_VTK_Jacobian3D, vvalue, precision));
    }

    void checkShapeSize3D() {
        hbm::VId vid = model.getElements3D().listId();
        vid.resize(2);
        double value;
        for (size_t i = 0; i < 2; i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::VTK_SIZE_SHAPE, vid[i], value));
            EXPECT_NEAR(value, valueCheck_VTK_SizeShape3D[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::VTK_SIZE_SHAPE, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_VTK_SizeShape3D, vvalue, precision));
    }
    */

    void checkMES_Inverted() {
        hbm::VId vid = model.getElements3D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_Inverted, vid[i], value));
            EXPECT_NEAR(value, valueCheck_MES_Inverted[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_Inverted, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_MES_Inverted, vvalue, precision));
    }

    void checkMES_UntangleBeta() {
        hbm::VId vid = model.getElements3D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_UntangleBeta, vid[i], value));
            EXPECT_NEAR(value, valueCheck_MES_UntangleBeta[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_UntangleBeta, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_MES_UntangleBeta, vvalue, precision));
    }

    void checkMES_InverseMeanRatio() {
        hbm::VId vid = model.getElements3D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio, vid[i], value));
            (value, valueCheck_MES_InverseMeanRatio[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_InverseMeanRatio, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_MES_InverseMeanRatio, vvalue, precision));
    }

    void checkMES_ConditionNumber() {
        hbm::VId vid = model.getElements3D().listId();
        double value;
        for (size_t i = 0; i < vid.size(); i++) {
            EXPECT_TRUE(myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_ConditionNumber, vid[i], value));
            EXPECT_NEAR(value, valueCheck_MES_ConditionNumber[i], precision);
        }
        std::vector<double> vvalue;
        myQuality->getQualityMetrics_3D(meshoptimizer::QualityMetrics::MESQUITE_ConditionNumber, vid, vvalue);
        EXPECT_TRUE(VectorMatchNear(valueCheck_MES_ConditionNumber, vvalue, precision));
    }

    hbm::FEModel model;

    meshoptimizer::meshQualityAbstract* myQuality;

  /*  std::vector<double> valueCheck_VTK_Jacobian2D;
    std::vector<double> valueCheck_VTK_Jacobian3D;
    std::vector<double> valueCheck_VTK_SizeShape2D;
    std::vector<double> valueCheck_VTK_SizeShape3D;*/

    std::vector<double> valueCheck_MES_Inverted;
    std::vector<double> valueCheck_MES_UntangleBeta;
    std::vector<double> valueCheck_MES_InverseMeanRatio;
    std::vector<double> valueCheck_MES_ConditionNumber;
    //
    const double precision = 1e-4;
};


TEST_P(meshquality_test, checkQualityResults) {
    checkQualityResults();
}


INSTANTIATE_TEST_CASE_P(InstantiationName,
    meshquality_test,
    ::testing::Values("MES1","MES2"));
