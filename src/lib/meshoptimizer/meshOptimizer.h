/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHOPTIMIZER_PIPER_H
#define MESHOPTIMIZER_PIPER_H

#include "hbm/FEModel.h"
#include "MeshMesquiteTag.h"
#include <vtkPolyData.h>

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define MESHOPTIMIZER_EXPORT
#endif


//using namespace Mesquite2;
namespace piper {
    namespace meshoptimizer {
        enum class DIM {
            only2D,
            only3D,
            ALL,
        };
        class MESHOPTIMIZER_EXPORT meshOptimizer : public Mesquite2::Mesh{
        public:
			// 3D smoothing
            /// <param name="timeOutSeconds">Untagler will run for at most this many seconds.</param>
            int optimizeMesh_Untangle(double timeOutSeconds);
            /// <param name="timeOutSeconds">Optimizer will run for at most this many seconds. Leave at the default 0.0 for the time to be unbounded.</param>
            void optimizeMesh_ShapeSizeImprover(double timeOutSeconds = 0.0);

			// 2D smoothing
			/// <summary>
			/// Smoothes the mesh surface using Laplace smoothing (see http://www.vtk.org/doc/nightly/html/classvtkSmoothPolyDataFilter.html).
			/// </summary>
			/// <param name="mesh">The mesh to smooth.</param>
            void optimizeMesh_SurfaceLaplaceSmooth(vtkSmartPointer<vtkPolyData> mesh);

            /// <summary>
            /// Smoothes the mesh surface using vtkWindowedSincPolyDataFilter (see http://www.vtk.org/doc/nightly/html/classvtkWindowedSincPolyDataFilter.html).
            /// </summary>
            /// <param name="mesh">The mesh to smooth.</param>
            /// <param name="useSelectiveSmoothing">If set to <c>true</c>, only vertices marked by 1 in the point data scalar array
            /// SELECTED_PRIMITIVES (macro defined in hbm/VtkSelectionTools.h) will be smoothed.</param>
            /// <param name="numberOfIterations">The number of iterations.</param>
            /// <param name="passBandValue">The pass band value - should be between 0 - 2, the lower the smoother the result.</param>
            void optimizeMesh_SurfaceTaubinSmooth(vtkSmartPointer<vtkPolyData> mesh, bool useSelectiveSmoothing, int numberOfIterations, double passBandValue);
            
            /// <summary>
            /// Smoothes the transformation using a local average iterative approach on the displacement of the mesh:
            /// For each point of the mesh, a neighborhood is constructed based on the N nearest neighbors in the source mesh.
            /// In each iteration, the average displacement between the source and target positions is computed in this neighborhood and it replaces the current displacement.
            /// </summary>
            /// <param name="targetModel">The mesh containing the positions after tranformation has been applied. After this finishes, this mesh will contain the final positions.</param>
            /// <param name="referencePoints">The reference positions, i.e. before the transformation that is being smoothed.</param>
            /// <param name="fixedNodes">For each node of the model, true if the node should not be modified. If <c>autostop</c> is on,
            /// this array will also be used to mark nodes that were "stopped", so expect this array to change after the method finishes.</param>
            /// <param name="useAutostop">If set to <c>true</c>, displacement will no longer be updated for points for which the displacement changed by less 
            /// than <c>autostopThreshold</c>. If this is true for all points, the algorithm will end.</param>
            /// <param name="autostopThreshold">The autostop criterion, see <c>useAutostop</c>.</param>
            /// <param name="maxNumberOfIterations">The maximum number of iterations that will be made, regardless of the autostop settings.</param>
            /// <param name="noOfNeighbors">The number of closest points to each point that will be used for the smoothing of each point.</param>
            void optimizeTransformation_LocalAverageSmooth(vtkSmartPointer<vtkUnstructuredGrid> targetModel, vtkSmartPointer<vtkPoints> referencePoints,
                std::vector<bool> &fixedNodes, bool useAutostop, double autostopThreshold, int maxNumberOfIterations, int noOfNeighbors);

            meshOptimizer() : myTags(new MeshMesquiteTag){};
            //meshOptimizer(hbm::FEModel& femodel, hbm::VId eidordering);
            void setMesh(hbm::FEModel& femodel, DIM dim=DIM::ALL);
            //void setMesh(std::vector<hbm::ElemDef>& elemdef, hbm::Nodes& nd);
            int get_NumberNoFixedNodes();
            std::size_t getNumberVertices() const { return coord.size(); };
            std::size_t getNumberElements() const { return elements.size(); };
            void optmizeMesh();
            bool getVertexHandle(Mesquite2::Mesh::VertexHandle& handle, hbm::Id& id, Mesquite2::MsqError &err);
            bool getElementHandle(Mesquite2::Mesh::VertexHandle& handle, hbm::Id& id, Mesquite2::MsqError &err);

            void release_skin_fixed(hbm::VId& idskin);

        private:
            void setMesh2D(hbm::Elements2D const& elem2D);
            void setMesh3D(hbm::Elements3D const& elem3D);
            void setNodes(hbm::Nodes& nodes);

        public:
            //implemente interface for Mesquite
            //************ Operations on entire mesh ****************
            //! Returns whether this mesh lies in a 2D or 3D coordinate system.
            int get_geometric_dimension(Mesquite2::MsqError &err);

            /** \brief Get all elements in mesh
            *
            * Get the handles of every element in the active mesh.
            */
            void get_all_elements(std::vector<ElementHandle>& elements,
                Mesquite2::MsqError& err);

            /** \brief Get all vertices in mesh
            *
            * Get the handles of every vertex in the active mesh
            */
            void get_all_vertices(std::vector<VertexHandle>& vertices,
                Mesquite2::MsqError& err);

            // Returns a pointer to an iterator that iterates over the
            // set of all vertices in this mesh.  The calling code should
            // delete the returned iterator when it is finished with it.
            // If vertices are added or removed from the Mesh after obtaining
            // an iterator, the behavior of that iterator is undefined.
            virtual Mesquite2::VertexIterator* vertex_iterator(Mesquite2::MsqError &err);

            // Returns a pointer to an iterator that iterates over the
            // set of all top-level elements in this mesh.  The calling code should
            // delete the returned iterator when it is finished with it.
            // If elements are added or removed from the Mesh after obtaining
            // an iterator, the behavior of that iterator is undefined.
            virtual Mesquite2::ElementIterator* element_iterator(Mesquite2::MsqError &err);


            //************ Vertex Properties ********************
            //! Returns true or false, indicating whether the vertex
            //! is allowed to be repositioned.  True indicates that the vertex
            //! is fixed and cannot be moved.  Note that this is a read-only
            //! property; this flag can't be modified by users of the
            //! Mesquite::Mesh interface.
            void vertices_get_fixed_flag(const VertexHandle vert_array[],
                std::vector<bool>& fixed_flag_array,
                size_t num_vtx,
                Mesquite2::MsqError &err);

            //! Returns true or false, indicating whether the vertex
            //! is a higher-order node that should be slaved to the logical
            //! mid-point of the element side it lies on or not, respectively.  
            //!
            //! Note: This function will never be called unless this behavior is
            //! requested by calling:
            //! InstructionQueue::set_slaved_ho_node_mode( Settings::SLAVE_FLAG )
            void vertices_get_slaved_flag(const VertexHandle vert_array[],
                std::vector<bool>& slaved_flag_array,
                size_t num_vtx,
                Mesquite2::MsqError &err) {};

            //! Get/set location of a vertex
            void vertices_get_coordinates(const VertexHandle vert_array[],
                Mesquite2::MsqVertex* coordinates,
                size_t num_vtx,
                Mesquite2::MsqError &err);
            void vertex_set_coordinates(VertexHandle vertex,
                const Mesquite2::Vector3D &coordinates,
                Mesquite2::MsqError &err);

            //! Each vertex has a byte-sized flag that can be used to store
            //! flags.  This byte's value is neither set nor used by the mesh
            //! implementation.  It is intended to be used by Mesquite algorithms.
            //! Until a vertex's byte has been explicitly set, its value is 0.
            void vertex_set_byte(VertexHandle vertex,
                unsigned char byte,
                Mesquite2::MsqError &err);
            void vertices_set_byte(const VertexHandle *vert_array,
                const unsigned char *byte_array,
                size_t array_size,
                Mesquite2::MsqError &err);

            //! Retrieve the byte value for the specified vertex or vertices.
            //! The byte value is 0 if it has not yet been set via one of the
            //! *_set_byte() functions.
            void vertex_get_byte(const VertexHandle vertex,
                unsigned char *byte,
                Mesquite2::MsqError &err);
            void vertices_get_byte(const VertexHandle *vertex,
                unsigned char *byte_array,
                size_t array_size,
                Mesquite2::MsqError &err);


            //**************** Vertex Topology *****************    
            /** \brief get elements adjacent to vertices
            *
            * Get adjacency data for vertices
            *
            *\param vertex_array    Array of vertex handles specifying the
            *                       list of vertices to retrieve adjacency
            *                       data for.
            *\param num_vertex      Number of vertex handles in #vertex_array
            *\param elements     The array in which to place the handles of
            *                       elements adjacent to the input vertices.
            *\param offsets    For each vertex in #vertex_array, the
            *                       value in the corresponding position in this
            *                       array is the index into #elem_array at
            *                       which the adjacency list begins for that
            *                       vertex.
            */
            void vertices_get_attached_elements(
                const VertexHandle* vertex_array,
                size_t num_vertex,
                std::vector<ElementHandle>& elements,
                std::vector<size_t>& offsets,
                Mesquite2::MsqError& err);

            //*************** Element Topology *************

            /** \brief Get element connectivity
            *
            * Get the connectivity (ordered list of vertex handles) for
            * each element in the input array.
            *
            *\param elem_handles  The array of element handles for which to
            *                     retrieve the connectivity list.
            *\param num_elems     The length of #elem_handles
            *\param vert_handles  Array in which to place the vertex handles
            *                     in each elements connectivity.
            *\param offsets       For each element in #elem_handles, the
            *                     value in the same position in this array
            *                     is the index into #vert_handles at which
            *                     the connectivity list for that element begins.
            */
            void elements_get_attached_vertices(
                const ElementHandle *elementsh,
                size_t num_elems,
                std::vector<VertexHandle>& vert_handles,
                std::vector<size_t>& offsets,
                Mesquite2::MsqError &err);


            //! Returns the topologies of the given entities.  The "entity_topologies"
            //! array must be at least "num_elements" in size.
            void elements_get_topologies(const ElementHandle *element_handle_array,
                Mesquite2::EntityTopology *element_topologies,
                size_t num_elements, Mesquite2::MsqError &err);


            //***************  Tags  ***********

            /** \brief Create a tag
            *
            * Create a user-defined data type that can be attached
            * to any element or vertex in the mesh.  For an opaque or
            * undefined type, use type=BYTE and length=sizeof(..).
            *
            * \param tag_name  A unique name for the data object
            * \param type      The type of the data
            * \param length    Number of values per entity (1->scalar, >1 ->vector)
            * \param default_value Default value to assign to all entities - may be NULL
            * \return - Handle for tag definition
            */
            Mesquite2::TagHandle tag_create(const std::string& tag_name,
                TagType type, unsigned length,
                const void* default_value,
                Mesquite2::MsqError &err);

            /** \brief Remove a tag and all corresponding data
            *
            * Delete a tag.
            */
            void tag_delete(Mesquite2::TagHandle handle, Mesquite2::MsqError& err);


            /** \brief Get handle for existing tag, by name.
            *
            * Check for the existance of a tag given it's name and
            * if it exists return a handle for it.  If the specified
            * tag does not exist, zero should be returned WITHOUT
            * flagging an error.
            */
            Mesquite2::TagHandle tag_get(const std::string& name,
                Mesquite2::MsqError& err);

            /** \brief Get properites of tag
            *
            * Get data type and number of values per entity for tag.
            * \param handle     Tag to get properties of.
            * \param name_out   Passed back tag name.
            * \param type_out   Passed back tag type.
            * \param length_out Passed back number of values per entity.
            */
            void tag_properties(Mesquite2::TagHandle handle,
                std::string& name_out,
                TagType& type_out,
                unsigned& length_out,
                Mesquite2::MsqError& err);

            /** \brief Set tag values on elements
            *
            * Set the value of a tag for a list of mesh elements.
            * \param handle     The tag
            * \param num_elems  Length of elem_array
            * \param elem_array Array of elements for which to set the tag value.
            * \param tag_data   Tag data for each element, contiguous in memory.
            *                   This data is expected to be
            *                   num_elems*tag_length*sizeof(tag_type) bytes.
            */
            void tag_set_element_data(Mesquite2::TagHandle handle,
                size_t num_elems,
                const ElementHandle* elem_array,
                const void* tag_data,
                Mesquite2::MsqError& err);

            /** \brief Set tag values on vertices
            *
            * Set the value of a tag for a list of mesh vertices.
            * \param handle     The tag
            * \param num_elems  Length of node_array
            * \param node_array Array of vertices for which to set the tag value.
            * \param tag_data   Tag data for each element, contiguous in memory.
            *                   This data is expected to be
            *                   num_elems*tag_length*sizeof(tag_type) bytes.
            */
            void tag_set_vertex_data(Mesquite2::TagHandle handle,
                size_t num_elems,
                const VertexHandle* node_array,
                const void* tag_data,
                Mesquite2::MsqError& err);


            /** \brief Get tag values on elements
            *
            * Get the value of a tag for a list of mesh elements.
            * \param handle     The tag
            * \param num_elems  Length of elem_array
            * \param elem_array Array of elements for which to get the tag value.
            * \param tag_data   Return buffer in which to copy tag data, contiguous
            *                   in memory.  This data is expected to be
            *                   num_elems*tag_length*sizeof(tag_type) bytes.
            */
            void tag_get_element_data(Mesquite2::TagHandle handle,
                size_t num_elems,
                const ElementHandle* elem_array,
                void* tag_data,
                Mesquite2::MsqError& err);

            /** \brief Get tag values on vertices.
            *
            * Get the value of a tag for a list of mesh vertices.
            * \param handle     The tag
            * \param num_elems  Length of elem_array
            * \param elem_array Array of vertices for which to get the tag value.
            * \param tag_data   Return buffer in which to copy tag data, contiguous
            *                   in memory.  This data is expected to be
            *                   num_elems*tag_length*sizeof(tag_type) bytes.
            */
            void tag_get_vertex_data(Mesquite2::TagHandle handle,
                size_t num_elems,
                const VertexHandle* node_array,
                void* tag_data,
                Mesquite2::MsqError& err);

            //**************** Memory Management ****************
            //! Tells the mesh that the client is finished with a given
            //! entity handle.  
            void release_entity_handles(const EntityHandle *handle_array,
                size_t num_handles,
                Mesquite2::MsqError &err){}

            //! Instead of deleting a Mesh when you think you are done,
            //! call release().  In simple cases, the implementation could
            //! just call the destructor.  More sophisticated implementations
            //! may want to keep the Mesh object to live longer than Mesquite
            //! is using it.
            void release() {}

            virtual ~meshOptimizer() {
                elements.clear();
                eid.clear();
                element_topology.clear();
                coord.clear();
                vid.clear();
                flag_fixed_vertices.clear();
                vertices_bytes.clear();
                adjacencies.clear();
            };

            /**\brief Set the value returned by vertices_get_fixed_flag for all vertices */
            void set_all_fixed_flags(bool value, Mesquite2::MsqError& err);

            /**\brief Set values for vertices_get_fixed_flag and vertices_get_slaved_flag
            * on skin vertices
            *
            * Set flag values for vertices on the skin (i.e. boundary) of the mesh.
            * Does not modify flags on iterior vertices.  Call \c set_all_fixed _flags
            * and \c set_all_slaved_flags *before* calling this function to set values
            * on interior vertices.
            *\param corner_fixed_flag  Value for vertices_get_fixed_flag for vertices at element corners
            */
            void set_skin_flags(bool corner_fixed_flag,
                Mesquite2::MsqError& err);

            /**\brief Find the vertices in the skin (i.e bounary) of the mesh and
            * mark them as 'fixed'.
            *\param clear_existing  If true only skin vertices are marked as fixed.
            *                       If false, skin vertices will be marked as fixed
            *                       in addition to any other vertices already marked
            *                       as fixed.
            */
            void mark_skin_fixed(Mesquite2::MsqError& err, bool clear_existing = true);


            /** Check if passed vertex index is valid */
            inline bool is_vertex_valid(size_t index) const
            {
                return index < vid.size();
            }
            /** Check if passed element index is valid */
            inline bool is_element_valid(size_t index) const
            {
                return index < elements.size();
            }
            size_t max_vertex_index() const { return coord.size(); }
            size_t max_element_index() const { return elements.size(); }

            /** Get element topology */
            const Mesquite2::EntityTopology get_element_topology(size_t index, Mesquite2::MsqError& err);
 
            /** Get element connectivity list, including mid-nodes */
            const std::vector<size_t>& element_connectivity(size_t index, Mesquite2::MsqError& err) const;

            /** Get vertex adjacency list */
            const std::vector<size_t>& vertex_adjacencies(size_t index, Mesquite2::MsqError& err) const;

            /** Set vertex fixed flag */
            void fix_vertex(size_t index, bool flag, Mesquite2::MsqError& err);
 
            /** Check if the specified node is used as a corner vertex on any element */
            bool is_corner_node(size_t index) const {
                return true;
            };

       private:
            // elements
            std::vector<std::vector<size_t>> elements;
            //hbm::Id *eid;
            std::vector<ElementHandle> eid;
            hbm::VId veid; // original FE element IDs for each elementID in e
            std::vector<Mesquite2::EntityTopology> element_topology;


            //vertices
            std::vector<double *> coord;
            std::vector<VertexHandle> vid;
            std::vector<bool> flag_fixed_vertices;
            std::vector<unsigned char> vertices_bytes;
            std::vector<std::vector<size_t>> adjacencies;

            //tag
            MeshMesquiteTag* myTags;

        };


        /**\brief VertexIterator for meshOptimizer
        *
        * Iterate over valid vertex indices
        */
        class meshOptimizerVertIter : public Mesquite2::VertexIterator
        {
        private:
            meshOptimizer* mesh;
            size_t index;

        public:

            meshOptimizerVertIter(meshOptimizer* data)
                : mesh(data) {
                restart();
            }

            virtual ~meshOptimizerVertIter();

            virtual void restart();

            virtual void operator++();

            virtual Mesquite2::Mesh::VertexHandle operator*() const;

            virtual bool is_at_end() const;

        };


        /**\brief ElementIterator for meshOptimizer
        *
        * Iterate over valid element indices
        */
        class meshOptimizerElemIter : public Mesquite2::ElementIterator
        {
        private:
            meshOptimizer* mesh;
            size_t index;

        public:

            meshOptimizerElemIter(meshOptimizer* data)
                : mesh(data) {
                restart();
            }

            virtual ~meshOptimizerElemIter();

            virtual void restart();

            virtual void operator++();

            virtual Mesquite2::Mesh::ElementHandle operator*() const;

            virtual bool is_at_end() const;

        };

    }//meshoptimizer
}//piper



#endif
