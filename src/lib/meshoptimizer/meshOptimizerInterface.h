/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHOPTIMIZER_PIPER_INTERFACE_H
#define MESHOPTIMIZER_PIPER_INTERFACE_H

#include "meshDamageDetector.h"
#include "hbm/VtkSelectionTools.h"

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define MESHOPTIMIZER_EXPORT
#endif


namespace piper {

	namespace hbm {
		class HumanBodyModel;
	}

    namespace meshoptimizer {


        // names of the smoothing parameters
        const std::string SMOOTH_OPT_NO_OF_ITERATIONS = "numberOfIterations";
        const std::string SMOOTH_OPT_TAUBIN_PASS_BAND_VALUE = "passBandValue";
        const std::string SMOOTH_OPT_SELECTED_ONLY = "smoothSelectedOnly";
        const std::string SMOOTH_OPT_KRIG_SPLITBOX_OVERLAP = "overlapOfSplittedBoxes";
        const std::string SMOOTH_OPT_KRIG_SPLITBOX_THRESHOLD_CP = "controlPointsCountToSplitBox";
        const std::string SMOOTH_OPT_KRIG_SKIN_NUGGET_BY_DISTANCE_TO_BONE = "computeSkinNuggetByDistanceToBone";
        const std::string SMOOTH_OPT_KRIG_NUGGET_BONES = "nuggetForBones";
        const std::string SMOOTH_OPT_KRIG_NUGGET_SKIN = "nuggetForSkin";
        const std::string SMOOTH_OPT_KRIG_INTERP_DISPLACEMENT = "interpolateDisplacement";
        const std::string SMOOTH_OPT_KRIG_USE_DRIFT = "useDrift";
        const std::string SMOOTH_OPT_LOCAL_AVG_AUTOSTOP_THRESHOLD = "autoStopThreshold";
        const std::string SMOOTH_OPT_LOCAL_AVG_AUTSTOP = "useAutoStop";
        const std::string SMOOTH_OPT_LOCAL_AVG_NEIGHBORS = "noOfNeighbors";
        const std::string SMOOTH_OPT_LOCAL_AVG_MAXITER = "maxIterations";

        enum class METRIC_TYPE {
            MESQUITE = 0,
            CREASEDET,
            SURFSMOOTH,
            SIZE,
        };
		
		/// <summary>
		/// Algorithms used by the surface smoothing
		/// </summary>
		enum class SURFSMOOTH_ALG {
            LAPLACE,
            TAUBIN_FIR,
            LOCAL_AVG,
            KRIG_BOX,
            ELE_INV_KRIG
		};

        class MESHOPTIMIZER_EXPORT meshOptimizerInterface {
        public:
            meshOptimizerInterface();
            ~meshOptimizerInterface();

            void reset();
			// allows to write threshold values into the meshQuality objects
			void updateThresholds(std::map<meshoptimizer::QualityMetrics, double>& thresholds);
            void setMetric(METRIC_TYPE metric);
            void setOptimized(bool truefalse);
			void setIsQualityComputed(bool truefalse);
            METRIC_TYPE getMetric() const;

            void initialize(piper::hbm::FEModel& hbm);			
			/// <summary>
			/// Sets the model in the state before deformation. The mesh is not necessarily stored or copied, only relevant information is extracted from it,
            /// so all changes you make to it after calling this method will likely not affect the operation of the optimizer. So call it directly before.
            /// When relevant, don't forget to call setEntityToProcess before this call.
            /// Use this before invoking mesh optimization for quality computers that are based on comparing the model before and after deformation.
			/// </summary>
			/// <param name="fem">The FE model.</param>
            void processBaselineMesh(piper::hbm::FEModel& fem);
            
            /// <summary>
            /// Defines whether the quality should be computed relative to the baseline model.
            /// Call this before calling processBaselineMesh, otherwise the baseline mesh will be ignored (i.e. if this is set to false,
            /// there is no reason to waste time calling the processBaselineMesh with some optimizers so it will be skipped)
            /// </summary>
            /// <param name="compareToBaseline">If set to <c>true</c>, quality computed by this optimizer will be relative to the set baseline mesh.</param>
            void setQualityUseBaselineModel(bool compareToBaseline);

            /// <summary>
            /// Sets the names of the entities that should be processed for the cases when only a subset of entities is being used.
            /// </summary>
            /// <param name="entityNames">List of names of the entities.</param>
            void setEntitiesToProcess(std::list<std::string> entityNames);                     
            
            /// <summary>
            /// Generates the selection boxes based on the computed quality. If quality was not computed yet, does nothing.
            /// </summary>
            /// <param name="mergeOverlapping">If set to <c>true</c>, overlapping boxes will be merged.</param>
            /// <returns>A collection of boxes. Receiver of this return value is responsible for deleting the objects!</returns>
            boost::container::vector<vtkOBBNodePtr> generateSelectionBoxes(bool mergeOverlapping);

            void computeMeshQuality();		
            void identifyLowQualityGroup(std::map<meshoptimizer::QualityMetrics, double> const& threshold, std::vector<hbm::Id>& Vid, std::vector<std::vector<int>>& numbelem, hbm::VId & vid_ignore);

            /// <summary>
            /// Optimizes only selected 3D elements of the current model.
            /// </summary>
            /// <param name="entities">Entities matching the current fe model (set in the <seealso cref="initialize"> method). Used only if ignoreBones = true.</param>
            /// <param name="ignoreBones">If set to <c>true</c>, elements that are part of bones will not be optimized.</param>
            void optimizeMeshQuality_selectedOnly(hbm::Metadata::EntityCont const& entities, bool ignoreBones);
            void optimizeMeshQuality();

            /// <summary>
            /// Smoothes the mesh based on its surface.
            /// </summary>
            /// <param name="alg">The algorithm to use for the smoothing.</param>
            /// <param name="hbm">The HBM that is being smoothed.</param>
            /// <param name="paramInt">List of integer parameters mapped using the names defined by the macros SMOOTH_OPT.....</param>
            /// <param name="paramDouble">List of double parameters mapped using the names defined by the macros SMOOTH_OPT.....</param>
            /// <param name="paramBool">List of bool parameters mapped using the names defined by the macros SMOOTH_OPT.....</param>
            /// <param name="selectionBoxes">Collection of selection boxes to which the processing should be limited.</param>
            /// <param name="referencePoints">Points of a baseline model to use for smoothing techniques that require it. Keep at default (NULL) for techniques that don't.</param>
            /// <returns><c>True</c> in case of success, <c>false</c> in case smoothing was not performed (usually because the needed parameters are not set).</returns>
            bool smoothMeshSurface(SURFSMOOTH_ALG alg, hbm::HumanBodyModel *hbm, std::map<std::string, int> &paramInt,
                std::map<std::string, double> &paramDouble, std::map<std::string, bool> &paramBool, boost::container::vector<vtkOBBNodePtr> &selectionBoxes,
                vtkSmartPointer<vtkPoints> referencePoints = NULL);
            void optimizeMeshQuality(hbm::Id const id);
            void optimizeMeshQuality(hbm::Id const id, hbm::VId const& vidSkin, std::vector<meshoptimizer::QualityMetrics> const& metrics);


            double getMaxValueMetric3D(meshoptimizer::QualityMetrics const& metric) const;
            double getMinValueMetric3D(meshoptimizer::QualityMetrics const& metric) const;
			
			/// returns the object that computed the quality
			/// temporary, should not be needed once all mesh quality modules return output in the same format. TJ
			meshQualityAbstract* getMeshQualityComputer() { return m_quality; };

            bool isOptimized() const {return m_isOptimized; };
            bool isQualityComputed() const { return m_isQualityComputed; };

            void exportQuality(std::string const& filename) const;
			
			/// <summary>
			/// Returns sets of elements with quality below the selected threshold.
			/// Make sure quality was computed before calling this, otherwise it will be empty.
			/// </summary>
			/// <returns>A pointer to a map of element IDs. Each entry is indexed by the ID of the group to which the elements belong
			/// and each of these entries is itself a map of vectors with element IDs, mapped based on a quality metric.</returns>
			std::map<hbm::Id, std::map<meshoptimizer::QualityMetrics, hbm::VId>> *getLowQualityElements();

        private:

            bool optimizeSelectedOnly = true; // if set to true, only selected elements will be optimized TODO - interface for this variable
            piper::hbm::FEModel* m_fem;
            meshQualityAbstract* m_quality;
            bool m_isOptimized;
            bool m_isQualityComputed;
            hbm::VId m_groupToOptimize;
            METRIC_TYPE m_metricType;
            std::map<hbm::Id, std::map<meshoptimizer::QualityMetrics,hbm::VId>> m_lowQualityElements;
            hbm::VId m_skinnodeid;
        };

    }//meshoptimizer
}//piper



#endif
