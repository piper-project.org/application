/*******************************************************************************
* Copyright (C) 2017 CEESAR, UCBL-Ifsttar                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors to the mesh quality optimization module include: Mesquite       *
* based: Erwan Jolivet (CEESAR); VTK, surface and kriging based: Tomas Janak   *
* (UCBL-Ifsttar)                                                               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef MESHQUALITYVTK_PIPER_H
#define MESHQUALITYVTK_PIPER_H

#include "meshQualityAbstract.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>

#ifdef WIN32
#	ifdef meshoptimizer_EXPORTS
#		define MESHOPTIMIZER_EXPORT __declspec( dllexport )
#	else
#		define MESHOPTIMIZER_EXPORT __declspec( dllimport )
#	endif
#else
#	define HBM_EXPORT
#endif

using namespace Eigen;

namespace piper {
    namespace hbm {
        class FEModelVTK;
    }
    namespace meshoptimizer {



		class MESHOPTIMIZER_EXPORT meshQualityVTK : public meshQualityAbstract {
        public:


            meshQualityVTK();
            ~meshQualityVTK();

            void reset();
            void setMesh(hbm::FEModel& fem);
            void computeQuality();
            void writeQualityResults(std::string const& filename);
            bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) const;
            bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::Id& id, double& metricvalue) const;
            bool getQualityMetrics_2D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) const;
            bool getQualityMetrics_3D(QualityMetrics const& metric, hbm::VId& id, std::vector<double>& metricvalue) const;
            bool getQualityMetrics_2D(QualityMetrics const& metric, std::vector<double>& metricvalue) const;
            bool getQualityMetrics_3D(QualityMetrics const& metric, std::vector<double>& metricvalue) const;
            double getMaxValueMetric3D(QualityMetrics const& metric) const;
            double getMinValueMetric3D(QualityMetrics const& metric) const;

			/// Finds all 3D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
			bool findmetrics_3D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
				std::function<bool(double, double)> compareOperation)  const;

			/// Finds all 2D elements E for which [(E.metric (compareOperation) threshold) == true]. used by the user functions findmetricbelow/equal/above etc.
			bool findmetrics_2D(QualityMetrics const& metric, double const& threshold, hbm::VId& vid,
				std::function<bool(double, double)> compareOperation)  const {
				return false;
			};


        private:
            std::map<QualityMetrics, int> m_metrics;
            MatrixXd m_meshquality2D;
            MatrixXd m_meshquality3D;
            piper::hbm::FEModelVTK* m_mesh;
        };



    }//meshoptimizer
}//piper



#endif
