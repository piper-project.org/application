/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_PARSERCONTOURFILE_H
#define PIPER_CONTOURS_PARSERCONTOURFILE_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "HbmSkeleton.h"
#include "tinyxml/tinyxml2.h"
#define PI 3.1415926535897932384626433832795

namespace piper {
	namespace contours {
		/**
		* @brief A class which involves functions for parsing the contour input file.
		* It is intended for reading or writing thecontour file
		* it currently contains the parser for full ghbmc model.
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class CONTOURS_EXPORT ParserContourFile {
		public:

			ParserContourFile();
			~ParserContourFile();

			
			/// <summary>
			/// Read Contour Input File in xml format.
			/// </summary>
			/// <param name="file_name">Name of the contour input file to be read</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			void WriteContourXml(std::string filename, hbm::Metadata& c_meta);

			void ReadContXml(hbm::Metadata& c_meta);

			void WriteContourExampleXml();

			void WriteContourXml(hbm::Metadata& c_meta);

			void ReadHbmSkeleton();

			void ReadHbmSkeletonHierarchy();

			void parseBodyRegion(tinyxml2::XMLElement * element_addr);

			void parseJoint(tinyxml2::XMLElement * element_addr);

			void parserishabh(tinyxml2::XMLElement * element_addr);


			Joint* abcdef(tinyxml2::XMLElement * element_addr);


			void parseJoint2(tinyxml2::XMLElement * element_addr, Joint& next );

			void parseTree();

			void parseHbmSkeleton(hbm::FEModel& c_fem, hbm::Metadata& c_meta);

            HbmSkeleton skele;

			void xmlparser();

		};
	}
}
#endif

