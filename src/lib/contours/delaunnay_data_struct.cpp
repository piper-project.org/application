/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "delaunnay_data_struct.h"


using namespace std;

namespace piper {
	namespace contours {

		point::point()
		{
		}


        point::point(piper::hbm::Id id, double x, double y, double z){
			point_id = id;
			p_x = x;
			p_y = y;
			p_z = z;
		}


		tet_element::tet_element()
		{
		}


        tet_element::tet_element(hbm::Id l, vector<hbm::Id> vertices){
            if (vertices.size() != 4)
			{
				cout << "Invalid Mesh";
				exit(0);
			}
			id_label = l;

			for (int i = 0; i<4; i++)
			{
				v_label.push_back(vertices[i]);
			}

		}

		volume_cordinates::volume_cordinates(hbm::Id l, size_t t_id, vector<double> ratios){
            if (ratios.size() != 4)
			{
				cout << "Invalid Mesh";
				exit(0);
			}
			node_label = l;
			tet_id = t_id;
			for (int i = 0; i<4; i++)
			{
				v_ratio.push_back(ratios[i]);
			}
		}
    }
}
