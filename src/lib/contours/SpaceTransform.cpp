/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "SpaceTransform.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		SpaceTransform::SpaceTransform() {
		}

		SpaceTransform::~SpaceTransform()
		{
		}


		Eigen::Vector3d SpaceTransform::ApplyRotation3d(Eigen::Vector3d coordinate, double angle, Eigen::MatrixXd axis){
				double u1, u2, u3;
				double x, y, z;
				x = coordinate(0,0);
				y = coordinate(1,0);
				z = coordinate(2,0);

				double x1 = axis(0,0), y1 = axis(0,1), z1 = axis(0,2);
				double x2 = axis(1,0), y2 = axis(1,1), z2 = axis(1,2);

				double u = x2 - x1, v = y2 - y1, w = z2 - z1;
				double t = double(angle)*PI / 180;
				u1 = (x1*(v*v + w*w) + u*(-y1*v - z1*w + u*x + v*y + w*z) + (-x1*(v*v + w*w) + u*(y1*v + z1*w - v*y - w*z) + (v*v + w*w)*x)*cos(t) + sqrt(u*u + v*v + w*w)*(-z1*v + y1*w - w*y + v*z)*sin(t)) / (u*u + v*v + w*w);
				u2 = (y1*(u*u + w*w) + v*(-x1*u - z1*w + u*x + v*y + w*z) + (-y1*(u*u + w*w) + v*(x1*u + z1*w - u*x - w*z) + (u*u + w*w)*y)*cos(t) + sqrt(u*u + v*v + w*w)*(z1*u - x1*w + w*x - u*z)*sin(t)) / (u*u + v*v + w*w);
				u3 = (z1*(u*u + v*v) + w*(-x1*u - y1*v + u*x + v*y + w*z) + (-z1*(u*u + v*v) + w*(x1*u + y1*v - u*x - v*y) + (u*u + v*v)*z)*cos(t) + sqrt(u*u + v*v + w*w)*(-y1*u + x1*v - v*x + u*y)*sin(t)) / (u*u + v*v + w*w);
				Eigen::Vector3d rotatedpoint;
				rotatedpoint << u1
					, u2
					, u3;
				return rotatedpoint;
		}

		double SpaceTransform::lagrangeInterpolatePoly(double pos[], double val[], int degree, double desiredPos)
		{
			double retVal = 0;
			for (int i = 0; i < degree; ++i)
			{
				double weight = 1;
				for (int j = 0; j < degree; ++j)
				{
					// The i-th term has to be skipped
					if (j != i)
					{
						weight *= (desiredPos - pos[j]) / (pos[i] - pos[j]);
					}
				}
				retVal += weight * val[i];
			}
			return retVal;
		}


		Eigen::MatrixXd SpaceTransform::convertSplineToBezier(Eigen::MatrixXd spline_matrix){
			Eigen::Matrix4d M;  /// Cubic Tangent Matrix
			Eigen::Matrix4d N;  /// Bezier Matrix 
			Eigen::MatrixXd ip_pts(4, 3);
			ip_pts = spline_matrix;  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);
			N << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;
			M << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;
			Eigen::MatrixXd Bezier_points;
			Bezier_points = N.inverse()*M*ip_pts;
			return Bezier_points;
		}

		std::vector<double> SpaceTransform::getPlaneFromPoints(Eigen::MatrixXd three_points){
			double a, b, c, d;
			Eigen::MatrixXd three_poins(3, 3);
			double x1 = three_points(0, 0); double x2 = three_points(0, 1); double x3 = three_points(0, 2);
			double y1 = three_points(1, 0); double y2 = three_points(1, 1); double y3 = three_points(1, 2);
			double z1 = three_points(2, 0); double z2 = three_points(2, 1); double z3 = three_points(2, 2);

			a = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
			b = -((x2 * z3 - x3 * z2) - (x1 * z3 - x3 * z1) + (x1 * z2 - x2 * z1));
			c = (x2 * y3 - x3 * y2) - (x1 * y3 - x3 * y1) + (x1 * y2 - x2 * y1);
			d = -(x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1));

			std::vector<double> zk;
			zk.push_back(a);
			zk.push_back(b);
			zk.push_back(c);
			zk.push_back(d);
			// determine display quad from 3 points
			/*disp_p1 = p1;
			disp_p2 = p2;
			disp_p3 = p3;
			disp_p4 = p3;	// for now, just display a triangle*/
			return zk;
		}

		std::vector<double> SpaceTransform::getPlaneFromPointNormal(std::vector<double> plane_point, std::vector<double> plane_normal){

			double a, b, c, d;

			a = plane_normal.at(0);
			b = plane_normal.at(1);
			c = plane_normal.at(2);

			d = -(a*plane_point.at(0), b*plane_point.at(1), c*plane_point.at(2));

			std::vector<double> zk;
			zk.push_back(a);
			zk.push_back(b);
			zk.push_back(c);
			zk.push_back(d);
			return zk;

		}


		double SpaceTransform::uValueOfPlane(Eigen::MatrixXd bezier_points, std::vector<double> abcd) {

			double aod, bod, cod;
			aod = abcd.at(0) / abcd.at(3);
			bod = abcd.at(1) / abcd.at(3);
			cod = abcd.at(2) / abcd.at(3);

			Eigen::Matrix4d N; // Bezier Matrix
			N << -1, 3, -3, 1  //N
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			Eigen::MatrixXd Bx(4, 1);
			Eigen::MatrixXd By(4, 1);
			Eigen::MatrixXd Bz(4, 1);

			Bx << bezier_points(0, 0)
				, bezier_points(1, 0)
				, bezier_points(2, 0)
				, bezier_points(3, 0);

			By << bezier_points(0, 1)
				, bezier_points(1, 1)
				, bezier_points(2, 1)
				, bezier_points(3, 1);

			Bz << bezier_points(0, 2)
				, bezier_points(1, 2)
				, bezier_points(2, 2)
				, bezier_points(3, 2);

			Eigen::MatrixXd Ax, Ay, Az;

			Ax = abcd.at(0)*N*Bx;
			Ay = abcd.at(1)*N*By;
			Az = abcd.at(2)*N*Bz;

			double u3, u2, u1, u0;

			u3 = Ax(0, 0) + Ay(0, 0) + Az(0, 0);
			u2 = Ax(1, 0) + Ay(1, 0) + Az(1, 0);
			u1 = Ax(2, 0) + Ay(2, 0) + Az(2, 0);
			u0 = Ax(3, 0) + Ay(3, 0) + Az(3, 0);

			u0 = u0 + abcd.at(3);

			int result;
			double x11 = u2 / u3;
			double x22 = u1 / u3;
			double x33 = u0 / u3;
			double q[3];

			result = SolveP3(q, x11, x22, x33);
			std::cout << "q0" << q[0] << "   q1" << q[1] << "   q2" << q[2] << endl;
			/*			if (result == 1 && q[0] <= 1 && q[0] >= 0){
			std::cout << q[0] << "q0 " << std::endl;
			return q[0];
			}
			else if(result == 3){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			else if (q[1] <= 1 && q[1] >= 0){
			return q[1];
			}
			else if (q[2] <= 1 && q[2] >= 0){
			return q[2];
			}
			else{
			return q[0];
			}
			}
			else{
			return 50;
			}
			std::cout << q[0] << endl;
			if (result == 1){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			}


			}
			*/

			if (result == 1){
				return q[0];
				std::cout << q[0] << "q0 " << std::endl;
			}
			else if (result == 3){
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else if (q[2] <= 1 && q[2] >= 0){
					std::cout << q[2] << "q2 " << std::endl;
					return q[2];
				}
				else{
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
			}
			else{
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else{
					return q[0];
				}
			}


		}

		Node SpaceTransform::perform_rotation(Node p, double theta, vectors A, vectors B)
		{
			Node newp;
			double x = p.getCoordX(), y = p.getCoordY(), z = p.getCoordZ();
			double newx, newy, newz;
			//////////////////////
			double x1 = A.a[0], y1 = A.a[1], z1 = A.a[2];
			double x2 = B.a[0], y2 = B.a[1], z2 = B.a[2];

			double u = x2 - x1, v = y2 - y1, w = z2 - z1;
			double t = double(theta)*PI / 180;
			newx = (x1*(v*v + w*w) + u*(-y1*v - z1*w + u*x + v*y + w*z) + (-x1*(v*v + w*w) + u*(y1*v + z1*w - v*y - w*z) + (v*v + w*w)*x)*cos(t) + sqrt(u*u + v*v + w*w)*(-z1*v + y1*w - w*y + v*z)*sin(t)) / (u*u + v*v + w*w);
			newy = (y1*(u*u + w*w) + v*(-x1*u - z1*w + u*x + v*y + w*z) + (-y1*(u*u + w*w) + v*(x1*u + z1*w - u*x - w*z) + (u*u + w*w)*y)*cos(t) + sqrt(u*u + v*v + w*w)*(z1*u - x1*w + w*x - u*z)*sin(t)) / (u*u + v*v + w*w);
			newz = (z1*(u*u + v*v) + w*(-x1*u - y1*v + u*x + v*y + w*z) + (-z1*(u*u + v*v) + w*(x1*u + y1*v - u*x - v*y) + (u*u + v*v)*z)*cos(t) + sqrt(u*u + v*v + w*w)*(-y1*u + x1*v - v*x + u*y)*sin(t)) / (u*u + v*v + w*w);
			newp.setCoord(newx, newy, newz);
			return newp;
		}

		Node SpaceTransform::perform_translation(Node p, double amt, vectors n)
		{
			Node newp;
			double newx, newy, newz;
			newx = p.getCoordX() + amt*n.a[0];
			newy = p.getCoordY() + amt*n.a[1];
			newz = p.getCoordZ() + amt*n.a[2];
			return newp;
		}

		Coord SpaceTransform::pointProjectionOnPlane(std::vector<double> normal, std::vector<double> testpoint, std::vector<double> pointonplane){
			double a, b, c, d, e, f, x, y, z, t;
			a = normal.at(0);
			b = normal.at(1);
			c = normal.at(2);
			d = pointonplane.at(0);
			e = pointonplane.at(1);
			f = pointonplane.at(2);
			x = testpoint.at(0);
			y = testpoint.at(1);
			z = testpoint.at(2);

			t = (a*d - a*x + b*e - b*y + c*f - c*z)/(a*a + b*b + c*c);
			Coord result;
			result[0] = x + t*a;
			result[1] = y + t*b;
			result[2] = z + t*c;

			return result;

		}

		bool SpaceTransform::pointOnsameSideOfPlane(std::vector<double> point1, std::vector<double> point2, std::vector<double> plane_point, std::vector<double> plane_normal){

			std::vector<double> planeeq;
			planeeq = getPlaneFromPointNormal(plane_point, plane_normal);
			
			double signpoint1 = planeeq.at(0)*point1.at(0) + planeeq.at(1)*point1.at(1) + planeeq.at(2)*point1.at(2) + planeeq.at(3);
			double signpoint2 = planeeq.at(0)*point2.at(0) + planeeq.at(1)*point2.at(1) + planeeq.at(2)*point2.at(2) + planeeq.at(3);

			if ((signpoint1 >= 0 && signpoint2 >= 0) || (signpoint1 < 0 && signpoint2 < 0)){
				return true;
			}
			else{
				return false;
			}
		}

		bool SpaceTransform::pointOnsameSideOfPlane(std::vector<double> point1, std::vector<double> point2, std::vector<double> planeeq){

			double signpoint1 = planeeq.at(0)*point1.at(0) + planeeq.at(1)*point1.at(1) + planeeq.at(2)*point1.at(2) + planeeq.at(3);
			double signpoint2 = planeeq.at(0)*point2.at(0) + planeeq.at(1)*point2.at(1) + planeeq.at(2)*point2.at(2) + planeeq.at(3);

			if ((signpoint1 >= 0 && signpoint2 >= 0) || (signpoint1 < 0 && signpoint2 < 0)){
				return true;
			}
			else{
				return false;
			}
		}

	}
}
