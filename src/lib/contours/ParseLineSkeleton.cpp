/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParseLineSkeleton.h"

#ifndef XMLCheckResult
#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult); return a_eResult; }
#endif
using namespace tinyxml2;
#include "ParserContourFile.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

        ParseLineSkeleton::ParseLineSkeleton() {
		}

        ParseLineSkeleton::~ParseLineSkeleton()
		{
		}


		bool  ParseLineSkeleton::skeleParse(tinyxml2::XMLElement* element, Joint* temprj){
			
			cout << "internal func starts" << endl;

			BodyRegion* tempbr = new BodyRegion;
			
			Joint* tempj = new  Joint;
			string bname;
			string jtname;
			vector<string> landmarknames;
			
			bname = element->Attribute("name");
			cout << endl << "bname  " << bname << endl;
			tempbr->setName(bname);
			cout << "op:   " << tempbr->name << endl;

			tinyxml2::XMLElement* element1 = element->FirstChildElement("ctrlpointlandmark");
			if (element1 == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}

			while (element1 != nullptr){
				cout << element1->GetText() << endl;
				string ctrlpointlandmark = element1->GetText();
				landmarknames.push_back(ctrlpointlandmark);
				cout << "ctrlpointlandmark:  " << ctrlpointlandmark << endl;
				element1 = element1->NextSiblingElement("ctrlpointlandmark");
			}
//			tempbr->setBodyRegionLandmarks(landmarknames);
			landmarknames.clear();

			if (skele.root != NULL){
				temprj->setChildBodyRegion(tempbr);
			}

			element1 = element->FirstChildElement("joint");





			while (element1 != nullptr){
				cout << element1->Attribute("name");
				jtname = element1->Attribute("name");
				cout << "jtname:  " << jtname  << endl;
				tempj->setName(jtname);

				tinyxml2::XMLElement* element2 = element1->FirstChildElement("bodyregion");
				if (element2 == nullptr){
					return XML_ERROR_PARSING_ELEMENT;
				}

				if (skele.root == NULL){
					skele.root = tempbr;
				}

				bool k = skeleParse(element2, tempj);
				tempbr->addChildJoint(tempj);
				tempj->setParentBodyRegion(tempbr);
				if (k != true){
					return false;
				}
				element1 = element1->NextSiblingElement("joint");
			}


			return true;
		}

		BodyRegion* ParseLineSkeleton::parseSkeleInfo(tinyxml2::XMLElement * element){
			std::cout << "start " << std::endl;
			BodyRegion* tempbr = new BodyRegion;
			Joint* tempj = new  Joint;
			string bname;
			string jtname;
			vector<string> landmarknames;


			while (element != NULL){
				element = element->FirstChildElement("tiger");
				bname = element->Attribute("name");
				cout << bname << endl;
				tempbr->setName(bname);
				tinyxml2::XMLElement* element1 = element->FirstChildElement("tiger");
				if (element1 == NULL){
				}
				cout << "1uo" << endl;
				cout << element1->GetText() << endl;
/*				cout << "1";
				tinyxml2::XMLElement* element1 = element->FirstChildElement("ctrlpoint");
				if (element1 == NULL){
				}
				*/
/*				tinyxml2::XMLElement* element2 = element1->FirstChildElement("ctrlpointlandmark");
				if (element2 == NULL){
				}
				cout << element2->GetText() << endl;
				while (element2 != NULL){
					string ctrlpointlandmark = element2->GetText();
					landmarknames.push_back(ctrlpointlandmark);
					cout << ctrlpointlandmark << endl;
 					element2 = element2->NextSiblingElement("ctrlpointlandmark");
				}
				tempbr->setBodyRegionLandmarks(landmarknames);
				landmarknames.clear();
				while (element != NULL){
					element = element->FirstChildElement("joint");
					jtname = element->Attribute("name");
					cout << jtname;
					tempj->setName(jtname);
					if (skele.root == NULL){
						skele.root = tempbr;
					}
					tempbr = parseSkeleInfo(element);
					
					tempbr->addChildJoint(tempj);
					element = element->NextSiblingElement("joint");
				} */
//				element = element->NextSiblingElement("joint");
			} 
			return tempbr;
		}


		

		bool ParseLineSkeleton::readSkeletonXml(FEModel& c_fem, hbm::Metadata& c_meta){

			skele.root = NULL;
			tinyxml2::XMLDocument xmldoc;

			tinyxml2::XMLError eResult = xmldoc.LoadFile("testxml.xml");
			if (eResult != tinyxml2::XML_SUCCESS){
				return false;
			}

			XMLNode * pRoot = xmldoc.FirstChild();
			if (pRoot == nullptr){
				return XML_ERROR_FILE_READ_ERROR;
			}


			XMLElement * pElement = pRoot->FirstChildElement("bodyregion");
			if (pElement == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}

			Joint* temprj = new  Joint;
			bool k = skeleParse(pElement, temprj);

			BodyRegion* breg = new BodyRegion;
			breg = NULL;
			skele.printSkele(breg);

			return k;
		}


	}
}
