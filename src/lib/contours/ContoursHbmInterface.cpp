/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ContoursHbmInterface.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		ContoursHbmInterface::ContoursHbmInterface() {
		}

		ContoursHbmInterface::~ContoursHbmInterface()
		{
		}

		
		SId ContoursHbmInterface::getLandmarkNodeids(FEModel& c_femodel, Metadata& c_meta, string c_name){
			SId nid_content;
			if (c_meta.hasLandmark(c_name))
			{
				VId idgroupe = c_meta.landmark(c_name).get_groupNode();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(c_femodel.getGroupNode(*it).get().begin(), c_femodel.getGroupNode(*it).get().end());
			}
			return nid_content;
		}

		
		SId ContoursHbmInterface::getGenericMetadataNodeids(FEModel& c_femodel, Metadata& c_meta, string c_name){
			SId nid_content;
			if (c_meta.hasGenericmetadata(c_name))
			{
				VId idgroupe = c_meta.genericmetadata(c_name).get_groupNode();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(c_femodel.getGroupNode(*it).get().begin(), c_femodel.getGroupNode(*it).get().end());
			}
			return nid_content;
		}

		
		SId ContoursHbmInterface::getGenericMetadataElementids1d(FEModel& c_femodel, Metadata& c_meta, string c_name){
			SId nid_content;
			if (c_meta.hasGenericmetadata(c_name))
			{
				VId idgroupe = c_meta.genericmetadata(c_name).get_groupElement1D();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(c_femodel.getGroupElements1D(*it).get().begin(), c_femodel.getGroupElements1D(*it).get().end());
			}
			return nid_content;
		}


		SId ContoursHbmInterface::getGenericMetadataElementids2d(FEModel& c_femodel, Metadata& c_meta, string c_name){
			SId nid_content;
			if (c_meta.hasGenericmetadata(c_name))
			{
				VId idgroupe = c_meta.genericmetadata(c_name).get_groupElement2D();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(c_femodel.getGroupElements2D(*it).get().begin(), c_femodel.getGroupElements2D(*it).get().end());
			}
			return nid_content;
		}

		
		SId ContoursHbmInterface::getGenericMetadataElementids3D(FEModel& c_femodel, Metadata& c_meta, string c_name){
			SId nid_content;
			if (c_meta.hasGenericmetadata(c_name))
			{
				VId idgroupe = c_meta.genericmetadata(c_name).get_groupElement3D();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(c_femodel.getGroupElements3D(*it).get().begin(), c_femodel.getGroupElements3D(*it).get().end());
			}
			return nid_content;
		}

		
		SId ContoursHbmInterface::getEntityNodeids(FEModel& c_femodel, Metadata& c_meta, string c_name) {

			SId sidnodes;
			if (c_meta.hasEntity(c_name)){
				for (auto it = c_meta.entity(c_name).get_groupElement2D().begin(); it != c_meta.entity(c_name).get_groupElement2D().end(); ++it) {
					VId sid = c_femodel.getGroupElements2D(*it).get();
					for (auto it = sid.begin(); it != sid.end(); ++it) {
						Element2D const& elem = c_femodel.getElement2D(*it);
						vector<Id>node_elem = c_femodel.getElement2D(*it).get();
						for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
							sidnodes.insert(*itt);
						}
					}
				}

				for (auto it = c_meta.entity(c_name).get_groupElement3D().begin(); it != c_meta.entity(c_name).get_groupElement3D().end(); ++it) {
					VId sid = c_femodel.getGroupElements3D(*it).get();
					for (auto it = sid.begin(); it != sid.end(); ++it) {
						Element3D const& elem = c_femodel.getElement3D(*it);
						vector<Id>node_elem = c_femodel.getElement3D(*it).get();
						for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
							sidnodes.insert(*itt);
						}
					}

				}

			}
			return sidnodes;
		}


		void ContoursHbmInterface::writeDelaunayInput(string filename, bool k_file, bool modify, HBM_contours* m_hbm_contours, vector<Node> all_bones)
		{
            size_t lastDot = filename.find_last_of('.');
            string delaunay_file = filename.substr(0, lastDot) + ".node";
            string key_file = filename.substr(0, lastDot) + ".k";

			int num = 0;
			vector<Node> c_point;
			vector<piper::hbm::Contour*> cont_vec;
			if (modify){
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->final_contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->final_contour_points.at(k));
						}
					}
				}
			}
			else{
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->initial_contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->initial_contour_points.at(k));
						}
					}
				}
			}

			for (vector<Node>::iterator it = all_bones.begin(); it != all_bones.end(); ++it) {
				c_point.push_back(*it);
			}

			if (c_point.size() != 0)
			{

				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j).getCoordX() << " " << c_point.at(j).getCoordY() << " " << c_point.at(j).getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j).getCoordX() << setw(16) << c_point.at(j).getCoordY() << setw(16) << c_point.at(j).getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');

				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i].getCoordX() == c_point[j].getCoordX())
						{
							if (c_point[i].getCoordY() == c_point[j].getCoordY())
							{
								if (c_point[i].getCoordZ() == c_point[j].getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}
				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					Node a = c_point.at(i);
					data1 << endl << num++ << " " << a.getCoordX() << " " << a.getCoordY() << " " << a.getCoordZ();
				}
				data1.close();
				output_file.close();
				keyword_file.close();
			}
			cont_vec.clear();
			c_point.clear();
		}


		vector<double> ContoursHbmInterface::NodeCoordVector(hbm::Node coordinate){
			vector<double> coordvec;
			coordvec.push_back(coordinate.getCoordX());
			coordvec.push_back(coordinate.getCoordY());
			coordvec.push_back(coordinate.getCoordZ());
			return coordvec;
		}

		
		Eigen::Vector3d ContoursHbmInterface::NodeCoordEigenVector(hbm::Node coordinate){
			Eigen::Vector3d coordvec;
			coordvec << coordinate.getCoordX()
				, coordinate.getCoordY()
				, coordinate.getCoordZ();
			return coordvec;
		}

		
		hbm::Id ContoursHbmInterface::register_new_frame(std::vector<hbm::Id> rfid, hbm::FEModel& c_femodel){
			hbm::FramePtr xyz = std::make_shared<hbm::FEFrame>();
//			xyz->register_frameinfo(rfid);
			xyz->setOrigin(rfid.at(0));
			xyz->setFirstDirection(rfid.at(1));
			xyz->setSecondDirection(rfid.at(2));
			c_femodel.set(xyz);
			hbm::Id piku = xyz->getId();
			return piku;
		}

		hbm::Coord ContoursHbmInterface::landmarkProjectionOnPlane(string landmarkname, FEModel fem, hbm::Metadata c_meta, std::vector<double> normal, std::vector<double> pointonplane){
			
			SId nid_content;
			Coord projcoord;
			projcoord = c_meta.landmark(landmarkname).position(fem);
			std::vector<double> testpoint;
			testpoint.push_back(projcoord[0]);
			testpoint.push_back(projcoord[1]);
			testpoint.push_back(projcoord[2]);
			projcoord = sptrans.pointProjectionOnPlane(normal,testpoint,pointonplane);
			return projcoord;

		}
	}
}
