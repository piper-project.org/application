/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "Spline.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		Spline::Spline() {
		}

		Spline::~Spline()
		{
		}



		Spline::Spline(vector <Node> & points, vector <double> & u) : IPs(points), ui(u)
		{
			//IPs = points;
			//ui = u;

			/*
			if (IPs.size() == 4){
			AfxMessageBox(_T("Input 4 points and corresponding Uis: correct"),0,0);

			} else
			AfxMessageBox(_T("Input only 4 points and corresponding u values:wrong "),0,0);

			*/
			CPs.resize(0);
			// matrix M inverse for Bezier spline
			Eigen::MatrixXd m_inv(4, 4);	// M inverse for bezier spline
			// Row 1
			m_inv(0, 0) = 0.0;
			m_inv(0, 1) = 0.0;
			m_inv(0, 2) = 0.0;
			m_inv(0, 3) = 1.0;
			// Row 2
			m_inv(1, 0) = 0.0;
			m_inv(1, 1) = 0.0;
			m_inv(1, 2) = 1 / 3.0;
			m_inv(1, 3) = 1.0;
			// Row 3
			m_inv(2, 0) = 0.0;
			m_inv(2, 1) = 1 / 3.0;
			m_inv(2, 2) = 2 / 3.0;
			m_inv(2, 3) = 1.0;
			// Row 4
			m_inv(3, 0) = 1.0;
			m_inv(3, 1) = 1.0;
			m_inv(3, 2) = 1.0;
			m_inv(3, 3) = 1.0;

			Eigen::MatrixXd	u_mat(4, 4);	// U matrix using the input u values
			Eigen::MatrixXd u_mat_inv(4, 4);	// inverse U matrix above

			// Row 1
			u_mat(0, 0) = ui[0] * ui[0] * ui[0];
			u_mat(0, 1) = ui[0] * ui[0];
			u_mat(0, 2) = ui[0];
			u_mat(0, 3) = 1.0;
			// Row 2
			u_mat(1, 0) = ui[1] * ui[1] * ui[1];
			u_mat(1, 1) = ui[1] * ui[1];
			u_mat(1, 2) = ui[1];
			u_mat(1, 3) = 1.0;
			// Row 3
			u_mat(2, 0) = ui[2] * ui[2] * ui[2];
			u_mat(2, 1) = ui[2] * ui[2];
			u_mat(2, 2) = ui[2];
			u_mat(2, 3) = 1.0;
			// Row 4
			u_mat(3, 0) = ui[3] * ui[3] * ui[3];
			u_mat(3, 1) = ui[3] * ui[3];
			u_mat(3, 2) = ui[3];
			u_mat(3, 3) = 1.0;

	/*		u_mat_inv = u_mat.Inv();
			
			
			ofstream writefile_temp;
			writefile_temp.open("temp_data.txt");

			for(int i=0; i<4; i++) writefile_temp<<u_mat_inv(i,0)<<" "<<u_mat_inv(i,1)<<" "<<u_mat_inv(i,2)<<" "<<u_mat_inv(i,3)<<endl;

			writefile_temp.close();
			

		
			Matrix mat_inter(4, 4);	// m_inv * u_mat_inv   // u_mat_inv is the N matrix

			Matrix b_mat(4, 3);	// B the control points matrix

			mat_inter = m_inv * u_mat_inv;

			Matrix p_mat(4, 3); // Four points matrix

			p_mat(0, 0) = IPs[0].x;
			p_mat(0, 1) = IPs[0].y;
			p_mat(0, 2) = IPs[0].z;

			p_mat(1, 0) = IPs[1].x;
			p_mat(1, 1) = IPs[1].y;
			p_mat(1, 2) = IPs[1].z;

			p_mat(2, 0) = IPs[2].x;
			p_mat(2, 1) = IPs[2].y;
			p_mat(2, 2) = IPs[2].z;

			p_mat(3, 0) = IPs[3].x;
			p_mat(3, 1) = IPs[3].y;
			p_mat(3, 2) = IPs[3].z;

			b_mat = mat_inter * p_mat;

			point p = point(b_mat(0, 0), b_mat(0, 1), b_mat(0, 2));
			CPs.push_back(p);

			p = point(b_mat(1, 0), b_mat(1, 1), b_mat(1, 2));
			CPs.push_back(p);

			p = point(b_mat(2, 0), b_mat(2, 1), b_mat(2, 2));
			CPs.push_back(p);

			p = point(b_mat(3, 0), b_mat(3, 1), b_mat(3, 2));
			CPs.push_back(p);
			*/
		}
		
/*		void spline::compute_points_on_spline(int num)
		{

			ini_points.clear();
			//spline s = this;
			vector<double> u;

			Matrix b_mat(4, 3);

			b_mat(0, 0) = CPs[0].x;
			b_mat(0, 1) = CPs[0].y;
			b_mat(0, 2) = CPs[0].z;

			b_mat(1, 0) = CPs[1].x;
			b_mat(1, 1) = CPs[1].y;
			b_mat(1, 2) = CPs[1].z;

			b_mat(2, 0) = CPs[2].x;
			b_mat(2, 1) = CPs[2].y;
			b_mat(2, 2) = CPs[2].z;

			b_mat(3, 0) = CPs[3].x;
			b_mat(3, 1) = CPs[3].y;
			b_mat(3, 2) = CPs[3].z;

			/*
			b_mat(0,0) = IPs[0].x;
			b_mat(0,1) = IPs[0].y;
			b_mat(0,2) = IPs[0].z;

			b_mat(1,0) = IPs[1].x;
			b_mat(1,1) = IPs[1].y;
			b_mat(1,2) = IPs[1].z;

			b_mat(2,0) = IPs[2].x;
			b_mat(2,1) = IPs[2].y;
			b_mat(2,2) = IPs[2].z;

			b_mat(3,0) = IPs[3].x;
			b_mat(3,1) = IPs[3].y;
			b_mat(3,2) = IPs[3].z;
			*/

/*			Matrix m(4, 4);	// M for bezier spline
			// Row 1
			m(0, 0) = -1.0;
			m(0, 1) = 3.0;
			m(0, 2) = -3.0;
			m(0, 3) = 1.0;
			// Row 2
			m(1, 0) = 3.0;
			m(1, 1) = -6.0;
			m(1, 2) = 3.0;
			m(1, 3) = 0.0;
			// Row 3
			m(2, 0) = -3.0;
			m(2, 1) = 3.0;
			m(2, 2) = 0.0;
			m(2, 3) = 0.0;
			// Row 4
			m(3, 0) = 1.0;
			m(3, 1) = 0.0;
			m(3, 2) = 0.0;
			m(3, 3) = 0.0;

			Matrix Nf(4, 4);	// Nf for four point form
			// Row 1
			Nf(0, 0) = -9.0 / 2.0;
			Nf(0, 1) = 27.0 / 2.0;
			Nf(0, 2) = -27.0 / 2.0;
			Nf(0, 3) = 9.0 / 2.0;
			// Row 2
			Nf(1, 0) = 9.0;
			Nf(1, 1) = -45.0 / 2.0;
			Nf(1, 2) = 18.0;
			Nf(1, 3) = -9.0 / 2.0;
			// Row 3
			Nf(2, 0) = -11.0 / 2.0;
			Nf(2, 1) = 9.0;
			Nf(2, 2) = -9.0 / 2.0;
			Nf(2, 3) = 1.0;
			// Row 4
			Nf(3, 0) = 1.0;
			Nf(3, 1) = 0.0;
			Nf(3, 2) = 0.0;
			Nf(3, 3) = 0.0;

			//ini_points.resize(0);

			for (int i = 0; i<num; i++){

				double ui = (double)i / (num - 1);

				Matrix U(1, 4);

				U(0, 0) = ui*ui*ui;
				U(0, 1) = ui*ui;
				U(0, 2) = ui;
				U(0, 3) = 1.0;

				Matrix mat_temp(1, 4), mat_temp1(4, 3), p_u(1, 3);

				mat_temp = U * m;

				//mat_temp = U * Nf;
				p_u = mat_temp * b_mat;

				//mat_temp1 = Nf * b_mat;
				//p_u = U * mat_temp1 ;

				point p = point(p_u(0, 0), p_u(0, 1), p_u(0, 2), point_index);

				point_index++;

				ini_points.push_back(p);
			}

		}
*/


/*		void spline::compute_points_on_Upd_spline(int num)
		{

			trans_points.clear();
			vector<double> u;

			Matrix b_mat(4, 3);

			b_mat(0, 0) = Upd_CPs[0].x;
			b_mat(0, 1) = Upd_CPs[0].y;
			b_mat(0, 2) = Upd_CPs[0].z;

			b_mat(1, 0) = Upd_CPs[1].x;
			b_mat(1, 1) = Upd_CPs[1].y;
			b_mat(1, 2) = Upd_CPs[1].z;

			b_mat(2, 0) = Upd_CPs[2].x;
			b_mat(2, 1) = Upd_CPs[2].y;
			b_mat(2, 2) = Upd_CPs[2].z;

			b_mat(3, 0) = Upd_CPs[3].x;
			b_mat(3, 1) = Upd_CPs[3].y;
			b_mat(3, 2) = Upd_CPs[3].z;

			Matrix m(4, 4);	// M for bezier spline
			// Row 1
			m(0, 0) = -1.0;
			m(0, 1) = 3.0;
			m(0, 2) = -3.0;
			m(0, 3) = 1.0;
			// Row 2
			m(1, 0) = 3.0;
			m(1, 1) = -6.0;
			m(1, 2) = 3.0;
			m(1, 3) = 0.0;
			// Row 3
			m(2, 0) = -3.0;
			m(2, 1) = 3.0;
			m(2, 2) = 0.0;
			m(2, 3) = 0.0;
			// Row 4
			m(3, 0) = 1.0;
			m(3, 1) = 0.0;
			m(3, 2) = 0.0;
			m(3, 3) = 0.0;

			Matrix Nf(4, 4);	// Nf for four point form
			// Row 1
			Nf(0, 0) = -9.0 / 2.0;
			Nf(0, 1) = 27.0 / 2.0;
			Nf(0, 2) = -27.0 / 2.0;
			Nf(0, 3) = 9.0 / 2.0;
			// Row 2
			Nf(1, 0) = 9.0;
			Nf(1, 1) = -45.0 / 2.0;
			Nf(1, 2) = 18.0;
			Nf(1, 3) = -9.0 / 2.0;
			// Row 3
			Nf(2, 0) = -11.0 / 2.0;
			Nf(2, 1) = 9.0;
			Nf(2, 2) = -9.0 / 2.0;
			Nf(2, 3) = 1.0;
			// Row 4
			Nf(3, 0) = 1.0;
			Nf(3, 1) = 0.0;
			Nf(3, 2) = 0.0;
			Nf(3, 3) = 0.0;

			for (int i = 0; i<num; i++){

				double ui = (double)i / (num - 1);

				Matrix U(1, 4);

				U(0, 0) = ui*ui*ui;
				U(0, 1) = ui*ui;
				U(0, 2) = ui;
				U(0, 3) = 1.0;

				Matrix mat_temp(1, 4), mat_temp1(4, 3), p_u(1, 3);

				mat_temp = U * m;

				//mat_temp = U * Nf;
				p_u = mat_temp * b_mat;

				//mat_temp1 = Nf * b_mat;
				//p_u = U * mat_temp1 ;

				point p = point(p_u(0, 0), p_u(0, 1), p_u(0, 2), point_index);

				point_index++;

				trans_points.push_back(p);
			}

		}
*/


	}
}
