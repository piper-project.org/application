/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_SPINEPREDICTOR_H
#define PIPER_CONTOURS_SPINEPREDICTOR_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "SpaceTransform.h"
#include "processing_func.h"

namespace piper {
	namespace contours {

	/**
    * @brief A class which predicts the spine positioning
    *
	*
	* @author Aditya Chhabra @date 2016 
	*/        
    class CONTOURS_EXPORT SpinePredictor {
		public:
			ProcessingFunc repo;
            SpinePredictor();
            ~SpinePredictor();

			SpaceTransform trans;

			std::vector<double> cervicalTwist(std::vector<double>, double);
						
			/**
			* Cervical Region Lateral Rotation.  For spinal movement in the lateral plane, cervical flexion angle is defined as the angle between the line of T1-C1 and the superior direction.
			* @param centoids. C1-C7 centroids.
			* @param all_nodes. Vector of Vector of vertebral Nodes (C1 to C7)
			* @param angle double argument. Input angle by the user by which the Knee Joint is rotated.
			* @param fracAngle. Fraction of the angles for re
			* @return void
			*/
			std::pair<std::vector<hbm::NodePtr>, std::vector<double>> cervicalLateralFlexion(std::vector<hbm::NodePtr > centroids, double angle, hbm::Id frameId, std::vector<double>[3], hbm::FEModel& c_fem);

			std::pair<std::vector<Eigen::MatrixXd>, std::vector<double>> cervicalLateralFlexionBasic(std::vector<Eigen::MatrixXd> centroids, double angle, std::vector<double>[3], Eigen::MatrixXd axis);
	};
	}
}
#endif

