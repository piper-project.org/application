/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_COMPUTETARGET_H
#define PIPER_CONTOURS_COMPUTETARGET_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "hbm/HumanBodyModel.h"
#include "hbm/target.h"
#include "ContoursHbmInterface.h"
#include "processing_func.h"

namespace piper {
	namespace contours {

		/**
		* @brief A class which compute the target informtion so that it becomes suitable and usable for ContourDeformation Module to do the repositioning.
		* 
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class CONTOURS_EXPORT ComputeTarget {
		public:

			ComputeTarget();
			~ComputeTarget();

			ContoursHbmInterface conthbm;
			ProcessingFunc repo;
			SpaceTransform sptrans;
			std::map<std::string, std::string> targetlandmark_map;
			std::map<hbm::LandmarkTarget, std::string> targetlandmarkconatiner_map;

			// This map hold the values of reposFunc, reposType, reposSide, computedAngle and reposAngle for the landmark target
			std::map<std::string, std::vector<double>> targetlandmark_reposmap;

			hbm::Coord computeJoinntCenter(hbm::Metadata, hbm::FEModel, std::vector<std::string>);
			
			std::vector<double> computeNormalFromLandmarks(hbm::Metadata, hbm::FEModel, std::vector<std::string>);

			std::vector<hbm::Coord> projPointsOnPlane(hbm::Coord jointcenter, std::vector<std::string> flexionaxes, hbm::LandmarkTarget targetlmk, std::string lmkname, hbm::Metadata, hbm::FEModel);
			std::vector<hbm::Coord> projPointsOnPlane1(hbm::Coord jointcenter, std::vector<std::string> flexionaxes, std::string landmark_parent, std::string landmark_child, hbm::Metadata, hbm::FEModel);

			double angleTargetLandmark(std::vector<hbm::Coord>);

			double angleSTargetLandmark(std::vector<hbm::Coord>, std::vector<std::string> lname, hbm::Metadata, hbm::FEModel);

			double computeTargetAngle(hbm::Metadata, hbm::FEModel, std::vector<std::string> landmarkvec, hbm::LandmarkTarget targetlmk, std::string lmkname);

			double computeSignedTargetAngle(hbm::Metadata, hbm::FEModel, std::vector<std::string> landmarkvec, hbm::LandmarkTarget targetlmk, std::string lmkname);

			double computeJointAngle(hbm::Metadata& c_meta, hbm::FEModel& c_fem, std::vector<std::string> landmarkvec, std::string landmark_parent, std::string landmark_child);    

			double computeAbsoluteTwistAngle(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<std::string> target_landmarks, std::vector<std::string> reference_landmarks, std::vector<std::string> nomral_vector_landmarks);
		
			double computeTwistAngleForTarget(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<double> target1__coords, std::vector<double> target2__coords, std::vector<std::string> reference_landmarks, std::vector<std::string> nomral_vector_landmarks);
		};


	}
}
#endif

