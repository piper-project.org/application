/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_HBMSKELETON_H
#define PIPER_CONTOURS_HBMSKELETON_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "ContoursHbmInterface.h"
#include "Joint.h"

namespace piper {
	namespace contours {

		/**
		* @brief A class which defines the full HumanBodySkeleton.
		* It is prepopulated by the landmarks when we load the contour module.
		* It represents the hierarchical structure of Skeleton or Structure of Skeleton in tree format
		*
		/// linear representation of skeleton hierarchy 
		Root
		 Torso
		  Neck
		   Head
		  ShoulderL
		   ElbowL
		    WristL
		  ShoulderR
		    ElbowR
		     WristR
		 Pelvis
		  HipL
		   KneeL
		    AnkleL
		  HipR
		   KneeR
		    AnkleR

		*
		* @author Aditya Chhabra @date 2016 
		*/


		
		class CONTOURS_EXPORT HbmSkeleton {
		public:

			HbmSkeleton();
			~HbmSkeleton();

			bool datastruc;
			int noOfChildren;
			Joint* jT;

			Joint * head;  // pointer to joint

			BodyRegion* headd;

			std::vector<Joint*> children;
			BodyRegion* root;
            ContoursHbmInterface conthbm;

			void insertskeleton(std::string jointname);
			void insertjoint(std::string nodename);
			void printSkeleton();
			void insertFirstNode(std::string nodename);
			void printnode();
			void insertHbmSkelHead(std::string jname);
			void printHeadSkele();
			void printwhole(Joint* jnt);
			void printtreeparse(hbm::FEModel& c_fem12, hbm::Metadata& c_meta12);
			void finalprint();
		};



	}
}
#endif

