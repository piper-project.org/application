/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "processing_func.h"
#include "VtkDelaunay.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {


		Eigen::MatrixXd  ProcessingFunc::getBezierControlPointsnoTangent(std::vector<hbm::Coord> spline_info){

			Eigen::Matrix4d cubic_tangent_mat; //M
			Eigen::Matrix4d bezier_mat;  // N 
			Eigen::MatrixXd ip_pts(4, 3);  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);

			//Eigen::MatrixXd bezier_mat2(4, 4);
			bezier_mat << 1.00, 0.00, 0.00, 0.00
				, -0.833,	3 ,- 1.5,	0.333
				, 0.333, -1.5, 3, -0.833
				, 0.00, 0.00, 0.00, 1.00;
			//std::cout << "yoyoyoyyo "<<bezier_mat << std::endl;
			
			
			
			ip_pts << spline_info.at(0)[0], spline_info.at(0)[1], spline_info.at(0)[2]  // p0  Femur(knee) 1st control
				, spline_info.at(1)[0], spline_info.at(1)[1], spline_info.at(1)[2]  // p0 Tibia(Knee) Last Control
				, spline_info.at(2)[0], spline_info.at(2)[1], spline_info.at(2)[2]  // p0 tangent at first
				, spline_info.at(3)[0], spline_info.at(3)[1], spline_info.at(3)[2];  // p0 tangent at last
			
			//            A = cubic_tangent_mat*bezier_mat;   //For coefficients of the cubic equation

			///
			/// Returns a matrix of Bezier Control Points
			///
			Eigen::MatrixXd Bezier_points(4,3);
			Bezier_points = bezier_mat.operator*(ip_pts);
			/*
			Eigen::MatrixXd bezier_mat(3,2);
			Eigen::MatrixXd Bezier_points(4,3);
			Eigen::MatrixXd small_set(2, 2);
			Eigen::MatrixXd ip_pts(2, 3);
			small_set << 3,-1.5,-1.5,3;
			ip_pts << spline_info.at(1)[0] - spline_info.at(2)[0] / 27 - 8 * spline_info.at(0)[0] / 27, spline_info.at(1)[1] - spline_info.at(2)[1] / 27 - 8 * spline_info.at(0)[1] / 27, spline_info.at(1)[2] - spline_info.at(2)[2] / 27 - 8 * spline_info.at(0)[0] / 27  // p0  Femur(knee) 1st control
				, spline_info.at(2)[0] - spline_info.at(2)[0]*8 / 27 - spline_info.at(0)[0] / 27, spline_info.at(2)[1] - 8*spline_info.at(2)[1] / 27 - spline_info.at(0)[1] / 27, spline_info.at(2)[2] - 8*spline_info.at(2)[2] / 27 - spline_info.at(0)[0] / 27;
			bezier_mat = small_set.inverse()*ip_pts;
			Bezier_points << spline_info.at(0)[0], spline_info.at(0)[1], spline_info.at(0)[2]
				, bezier_mat(0, 0), bezier_mat(0, 1), bezier_mat(0, 2)
				, bezier_mat(1, 0), bezier_mat(1, 1), bezier_mat(1, 2)
				, spline_info.at(3)[0], spline_info.at(3)[1], spline_info.at(3)[2];
				*/
			//std::cout << Bezier_points << std::endl;
			return Bezier_points;
		}

		vector<double> ProcessingFunc::Centroid(vector<NodePtr> pts)
		{
			double meanx = 0;
			double meany = 0;
			double meanz = 0;
			for (int i = 0; i < pts.size(); i++)
			{
				meanx += pts[i]->getCoordX();
				meany += pts[i]->getCoordY();
				meanz += pts[i]->getCoordZ();
			};
			meanx = meanx / pts.size();
			meany = meany / pts.size();
			meanz = meanz / pts.size();
			vector<double> res;
			res.push_back(meanx);
			res.push_back(meany);
			res.push_back(meanz);
			return(res);
		}

		NodePtr ProcessingFunc::tpointOnCubicBezier(vector<NodePtr> spline_nodess, float t, int f){


			float   ax, bx, cx;
			float   az, bz, cz;
			float   tSquared, tCubed;
			double x1, y1, z1;
			/* calculate the polynomial coefficients */


			cx = 3.0 * (spline_nodess.at(1)->getCoordX() - spline_nodess.at(0)->getCoordX());
			bx = 3.0 * (spline_nodess.at(2)->getCoordX() - spline_nodess.at(1)->getCoordX()) - cx;
			ax = spline_nodess.at(3)->getCoordX() - spline_nodess.at(0)->getCoordX() - cx - bx;

			cz = 3.0 * (spline_nodess.at(1)->getCoordZ() - spline_nodess.at(0)->getCoordZ());
			bz = 3.0 * (spline_nodess.at(2)->getCoordZ() - spline_nodess.at(1)->getCoordZ()) - cz;
			az = spline_nodess.at(3)->getCoordZ() - spline_nodess.at(0)->getCoordZ() - cz - bz;
			/* calculate the curve point at parameter value t */

			tSquared = t * t;
			tCubed = tSquared * t;
			x1 = (ax * tCubed) + (bx * tSquared) + (cx * t) + spline_nodess.at(0)->getCoordX();
			z1 = (az * tCubed) + (bz * tSquared) + (cz * t) + spline_nodess.at(0)->getCoordZ();
			y1 = 0;

			NodePtr newnode = std::make_shared<Node>();
			newnode->setCoord(x1, y1, z1);

			return newnode;
		}

		NodePtr ProcessingFunc::diff_PointOnCubicBezier(vector<NodePtr> spline_nodess, float t, int k)
        {
			float   ax, bx, cx;
			float   az, bz, cz;
			float   tSquared, tCubed;
			double  x1, y1, z1;
			/* calculate the polynomial coefficients */
            
			cx = 3.0 * (spline_nodess.at(1)->getCoordX() - spline_nodess.at(0)->getCoordX());
			bx = 3.0 * (spline_nodess.at(2)->getCoordX() - spline_nodess.at(1)->getCoordX()) - cx;
			ax = spline_nodess.at(3)->getCoordX() - spline_nodess.at(0)->getCoordX() - cx - bx;

			cz = 3.0 * (spline_nodess.at(1)->getCoordZ() - spline_nodess.at(0)->getCoordZ());
			bz = 3.0 * (spline_nodess.at(2)->getCoordZ() - spline_nodess.at(1)->getCoordZ()) - cz;
			az = spline_nodess.at(3)->getCoordZ() - spline_nodess.at(0)->getCoordZ() - cz - bz;

			/* calculate the curve point at parameter value t */

			tSquared = t * t;
			tCubed = tSquared * t;

			x1 = (ax * tSquared) * 3 + (bx * t) * 2 + (cx);
			z1 = (az * tSquared) * 3 + (bz * t) * 2 + (cz);
			y1 = 0;


			NodePtr newnode = std::make_shared<Node>();
			newnode->setCoord(x1, y1, z1);
			return newnode;
		}

		void ProcessingFunc::perform_rotation(NodePtr p, double theta, vectors A, vectors B)
		{
			double u1, u2, u3;
			double x, y, z;
			x = p->getCoordX();
			y = p->getCoordY();
			z = p->getCoordZ();

			double x1 = A.a[0], y1 = A.a[1], z1 = A.a[2];
			double x2 = B.a[0], y2 = B.a[1], z2 = B.a[2];

			double u = x2 - x1, v = y2 - y1, w = z2 - z1;
			double t = double(theta)*PI / 180;
			u1 = (x1*(v*v + w*w) + u*(-y1*v - z1*w + u*x + v*y + w*z) + (-x1*(v*v + w*w) + u*(y1*v + z1*w - v*y - w*z) + (v*v + w*w)*x)*cos(t) + sqrt(u*u + v*v + w*w)*(-z1*v + y1*w - w*y + v*z)*sin(t)) / (u*u + v*v + w*w);
			u2 = (y1*(u*u + w*w) + v*(-x1*u - z1*w + u*x + v*y + w*z) + (-y1*(u*u + w*w) + v*(x1*u + z1*w - u*x - w*z) + (u*u + w*w)*y)*cos(t) + sqrt(u*u + v*v + w*w)*(z1*u - x1*w + w*x - u*z)*sin(t)) / (u*u + v*v + w*w);
			u3 = (z1*(u*u + v*v) + w*(-x1*u - y1*v + u*x + v*y + w*z) + (-z1*(u*u + v*v) + w*(x1*u + y1*v - u*x - v*y) + (u*u + v*v)*z)*cos(t) + sqrt(u*u + v*v + w*w)*(-y1*u + x1*v - v*x + u*y)*sin(t)) / (u*u + v*v + w*w);
			p->setCoord(u1, u2, u3);
		}

		set<Id> ProcessingFunc::getnodesbypart(const Entity& entity, set<Id> sidnodes, FEModel const& fem) {

			for (auto it = entity.get_groupElement2D().begin(); it != entity.get_groupElement2D().end(); ++it) {
				VId sid = fem.getGroupElements2D(*it).get();
				for (auto it = sid.begin(); it != sid.end(); ++it) {
					Element2D const& elem = fem.getElement2D(*it);
					vector<Id>node_elem = fem.getElement2D(*it).get();
					for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
						sidnodes.insert(*itt);
					}
				}
			}
            
			for (auto it = entity.get_groupElement3D().begin(); it != entity.get_groupElement3D().end(); ++it) {
				VId sid = fem.getGroupElements3D(*it).get();
				for (auto it = sid.begin(); it != sid.end(); ++it) {
					Element3D const& elem = fem.getElement3D(*it);
					vector<Id>node_elem = fem.getElement3D(*it).get();
					for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
						sidnodes.insert(*itt);
					}
				}

			}
			return sidnodes;
		}

		set<Id> ProcessingFunc::getnodesfromentity(const Entity& entity, set<Id> sidnodes, FEModel& fem) {

			for (auto it = entity.get_groupElement2D().begin(); it != entity.get_groupElement2D().end(); ++it) {
				VId sid = fem.getGroupElements2D(*it).get();
				for (auto it = sid.begin(); it != sid.end(); ++it) {
					Element2D const& elem = fem.getElement2D(*it);
					vector<Id>node_elem = fem.getElement2D(*it).get();
					for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
						sidnodes.insert(*itt);
					}
				}
			}

			for (auto it = entity.get_groupElement3D().begin(); it != entity.get_groupElement3D().end(); ++it) {
				VId sid = fem.getGroupElements3D(*it).get();
				for (auto it = sid.begin(); it != sid.end(); ++it) {
					Element3D const& elem = fem.getElement3D(*it);
					vector<Id>node_elem = fem.getElement3D(*it).get();
					for (auto itt = node_elem.begin(); itt != node_elem.end(); ++itt){
						sidnodes.insert(*itt);
					}
				}

			}
			return sidnodes;
		}

		double ProcessingFunc::distance(Node a, Node b)
		{
            return sqrt((a.getCoordX() - b.getCoordX())*(a.getCoordX() - b.getCoordX()) + (a.getCoordY() - b.getCoordY())*(a.getCoordY() - b.getCoordY()) + (a.getCoordZ() - b.getCoordZ())*(a.getCoordZ() - b.getCoordZ()));
		}

		Node ProcessingFunc::translate(Node c, Node a, Node b, double translate_distance)
		{
            double ratio = translate_distance / distance(a, b);

			Node d;
            d.setCoord((b.getCoordX() - a.getCoordX()) * ratio + c.getCoordX(),
                (b.getCoordY() - a.getCoordY()) * ratio + c.getCoordY(),
                (b.getCoordZ() - a.getCoordZ()) * ratio + c.getCoordZ());
			return d;
		}

		void ProcessingFunc::translate2(Node& c, Node a, Node b, double translate_distance)
		{
            double ratio = translate_distance / distance(a, b);
            c.setCoord((b.getCoordX() - a.getCoordX()) * ratio + c.getCoordX(),
                (b.getCoordY() - a.getCoordY()) * ratio + c.getCoordY(),
                (b.getCoordZ() - a.getCoordZ()) * ratio + c.getCoordZ());
		}

		void ProcessingFunc::translate1(NodePtr c, Node a, Node b, double translate_distance)
		{
			double dist = distance(a, b), ratio;
			double x, y, z;
			ratio = translate_distance / dist;

            x = (b.getCoordX() - a.getCoordX()) * ratio + c->getCoordX();
            y = (b.getCoordY() - a.getCoordY()) * ratio + c->getCoordY();
            z = (b.getCoordZ() - a.getCoordZ()) * ratio + c->getCoordZ();
			c->setCoord(x, y, z);
		}

		void ProcessingFunc::translate_contour(map<int, piper::hbm::Contour*> cont_map, int cont_id, double distance, Node a, Node b)
        {            
			map<int, piper::hbm::Contour*> cmap = cont_map;
			size_t total_points = cmap[cont_id]->contour_points.size();

			for (size_t i = 0; i < total_points; i++){
				NodePtr t = cmap[cont_id]->contour_points.at(i);
				translate1(t, a, b, distance);
			}
		}

		double ProcessingFunc::lagrangeInterpolatingPolynomial(double pos[], double val[], int degree, double desiredPos)
		{
			double retVal = 0;

			for (int i = 0; i < degree; ++i)
			{
				double weight = 1;

				for (int j = 0; j < degree; ++j)
				{
					// The i-th term has to be skipped
					if (j != i)
					{
						weight *= (desiredPos - pos[j]) / (pos[i] - pos[j]);
					}
				}
				retVal += weight * val[i];
			}

			return retVal;
		}

		void ProcessingFunc::rotate_points1(vector<NodePtr>* selectedPoints, double theta, vectors A, vectors B)
		{
            vector<NodePtr> *initial = selectedPoints;
			vector<NodePtr> final;

            for (NodePtr curNode : *initial)
            {
                perform_rotation(curNode, theta, A, B);
                final.push_back(curNode);
            }
			initial->clear();
		}

		vector<NodePtr> ProcessingFunc::rotate_points(vector<NodePtr> selectedPoints, double theta, vectors A, vectors B)
		{
			vector<NodePtr> final;
			for (NodePtr curNode : selectedPoints){
                perform_rotation(curNode, theta, A, B);
                final.push_back(curNode);
			}
			return final;
		}

		vectors ProcessingFunc::circumcentre(vectors A, vectors B)
		{
			double k1 = (B.mod())*(B.mod())*((A - B).dot(A)) / 2 / (((A - B)*B).mod()) / (((A - B)*B).mod());
			double k2 = (A.mod())*(A.mod())*((B - A).dot(A)) / 2 / (((A - B)*B).mod()) / (((A - B)*B).mod());
			return A.mult(k1) + B.mult(k2);
		}

		void ProcessingFunc::rotate_this_contour(vector<NodePtr>* v, double theta, vectors A, vectors B)
		{
			for (NodePtr curNode : *v)
			{
                perform_rotation(curNode, theta, A, B);
			}
		}

		vector<NodePtr> ProcessingFunc::translateContour(vector<NodePtr>* v, double amt, vectors A)
		{
			vector<NodePtr> res;
			res.clear();

            for (NodePtr curNode : *v)
			{
                res.push_back(perform_translation(curNode, amt, A));
			}
			return res;
		}


		void ProcessingFunc::rotate_contours(vector<vector<NodePtr>>::iterator FIRST, vector<vector<NodePtr>>::iterator LAST, double theta, vectors A, vectors B)
		{
			for (vector<vector<NodePtr>>::iterator iter = FIRST; iter <= LAST; iter++)
			{
                rotate_this_contour(&(*iter), theta, A, B);
			}
		}

		vectors ProcessingFunc::line_plane_intersect(vector<NodePtr> PLANE, vectors A, vectors B)
		{
			vectors normal = vectors(PLANE.at(0), PLANE.at(1)).cross(vectors(PLANE.at(1), PLANE.at(2)));
			normal = normal.unit();
			double x = 0;
			double y = 0;
			double z = 0;
			//Id newid = Node::getIdfromIdFormat(IdFormat("Contour_Nodes", 43));

			//contour_data << point_id << "  " << newid << endl;
			NodePtr a = std::make_shared<Node>();
			a->setCoord(x, y, z);

			double lambda = (normal.dot(vectors(PLANE.at(0), a) - A)) / (normal.dot(B - A));

			return A + (B - A).unit().mult(lambda*(B - A).mod());
		}

		void ProcessingFunc::write_delaunay_input_file(string filename, bool k_file, bool modify, HBM_contours* m_hbm_contours)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";


			int num = 0;
			vector<NodePtr> c_point;
			vector<piper::hbm::Contour*> cont_vec;
			if (modify){
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						if (m_hbm_contours->full_body.at(i).size() == 0){
							for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->contour_points.size(); k++){
								c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->contour_points.at(k));
							}
						}
						else{
							for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->modified_contour_points.size(); k++){
								c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->modified_contour_points.at(k));
							}
						}
					}
				}
			}
			else{
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->contour_points.at(k));
						}
					}
				}
			}


			if (c_point.size() != 0)
			{
				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j)->getCoordX() << " " << c_point.at(j)->getCoordY() << " " << c_point.at(j)->getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j)->getCoordX() << setw(16) << c_point.at(j)->getCoordY() << setw(16) << c_point.at(j)->getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				c_point.clear();
			}
		}

		void ProcessingFunc::write_delaunay_input_file_contourCL(string filename, bool k_file, bool modify, std::vector<hbm::Contour*> vectcont, vector<Node> all_bones)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";
			VtkDelaunay vtkdelaunay;

			int num = 0;
			vector<Node> c_point;
			vector<piper::hbm::Contour*> cont_vec;

			if (modify){
				for (int j = 0; j < vectcont.size(); j++){
					for (int k = 0; k < vectcont.at(j)->final_contour_points.size(); k++){
						c_point.push_back(vectcont.at(j)->final_contour_points.at(k));
						}
					}
				}
			else{
				for (int j = 0; j < vectcont.size(); j++){
					for (int k = 0; k < vectcont.at(j)->initial_contour_points.size(); k++){
						c_point.push_back(vectcont.at(j)->initial_contour_points.at(k));
						}
					}
				}

			for (vector<Node>::iterator it = all_bones.begin(); it != all_bones.end(); ++it) {
				c_point.push_back(*it);
			}


			if (c_point.size() != 0)
			{

				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j).getCoordX() << " " << c_point.at(j).getCoordY() << " " << c_point.at(j).getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j).getCoordX() << setw(16) << c_point.at(j).getCoordY() << setw(16) << c_point.at(j).getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');

				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i].getCoordX() == c_point[j].getCoordX())
						{
							if (c_point[i].getCoordY() == c_point[j].getCoordY())
							{
								if (c_point[i].getCoordZ() == c_point[j].getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}
				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					Node a = c_point.at(i);
					data1 << endl << num++ << " " << a.getCoordX() << " " << a.getCoordY() << " " << a.getCoordZ();
				}
				data1.close();
				output_file.close();
				keyword_file.close();
			}
			cont_vec.clear();
			c_point.clear();
		}

		/// Write Delaunnay For left Knee Contours
		void ProcessingFunc::write_delaunay_input_file_2(string filename, bool k_file, bool modify, HBM_contours* m_hbm_contours, vector<Node> all_bones)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";
            VtkDelaunay vtkdelaunay;

			int num = 0;
			vector<Node> c_point;
			vector<piper::hbm::Contour*> cont_vec;
			if (modify){
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->final_contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->final_contour_points.at(k));
						}
					}
				}
			}
			else{
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->initial_contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->initial_contour_points.at(k));
						}
					}
				}
			}

			for (vector<Node>::iterator it = all_bones.begin(); it != all_bones.end(); ++it) {
				c_point.push_back(*it);
			}



			if (c_point.size() != 0)
			{

				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j).getCoordX() << " " << c_point.at(j).getCoordY() << " " << c_point.at(j).getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j).getCoordX() << setw(16) << c_point.at(j).getCoordY() << setw(16) << c_point.at(j).getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');
				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i].getCoordX() == c_point[j].getCoordX())
						{
							if (c_point[i].getCoordY() == c_point[j].getCoordY())
							{
								if (c_point[i].getCoordZ() == c_point[j].getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}
				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					Node a = c_point.at(i);
					data1 << endl << num++ << " " << a.getCoordX() << " " << a.getCoordY() << " " << a.getCoordZ();
				}
				data1.close();
				output_file.close();
				keyword_file.close();
			}
			cont_vec.clear();
			c_point.clear();
		}

		void ProcessingFunc::write_delaunay_input_file_3(string filename, bool k_file, vector<Node> all_cont)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";


			int num = 0;
			vector<Node> c_point;
			vector<piper::hbm::Contour*> cont_vec;
			for (int k = 0; k < all_cont.size(); k++){
				c_point.push_back(all_cont.at(k));
			}

			if (c_point.size() != 0)
			{
				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j).getCoordX() << " " << c_point.at(j).getCoordY() << " " << c_point.at(j).getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j).getCoordX() << setw(16) << c_point.at(j).getCoordY() << setw(16) << c_point.at(j).getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				//		c_point.clear();

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');

				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i].getCoordX() == c_point[j].getCoordX())
						{
							if (c_point[i].getCoordY() == c_point[j].getCoordY())
							{
								if (c_point[i].getCoordZ() == c_point[j].getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}

				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					Node a = c_point.at(i);
					data1 << endl << num++ << " " << a.getCoordX() << " " << a.getCoordY() << " " << a.getCoordZ();
				}
				data1.close();


			}


		}

		void ProcessingFunc::write_delaunay_input_file_hip(string filename, bool k_file, vector<Node> c_point)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";


			int num = 0;

			if (c_point.size() != 0)
			{

				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j).getCoordX() << " " << c_point.at(j).getCoordY() << " " << c_point.at(j).getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j).getCoordX() << setw(16) << c_point.at(j).getCoordY() << setw(16) << c_point.at(j).getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				//		c_point.clear();

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');

				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i].getCoordX() == c_point[j].getCoordX())
						{
							if (c_point[i].getCoordY() == c_point[j].getCoordY())
							{
								if (c_point[i].getCoordZ() == c_point[j].getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}
				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					Node a = c_point.at(i);
					data1 << endl << num++ << " " << a.getCoordX() << " " << a.getCoordY() << " " << a.getCoordZ();
				}
				data1.close();
				output_file.close();
				keyword_file.close();
			}


			c_point.clear();


		}

		void ProcessingFunc::write_delaunay_input_file_1(string filename, bool k_file, bool modify, HBM_contours* m_hbm_contours, vector<NodePtr> all_bones)
		{
            size_t lastDot = filename.find_last_of('.');
            string filenameNoExt = filename.substr(0, lastDot);
            string delaunay_file = filenameNoExt + ".node";
            string key_file = filenameNoExt + ".k";


			int num = 0;
			vector<NodePtr> c_point;
			vector<piper::hbm::Contour*> cont_vec;
			if (modify){
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						if (m_hbm_contours->full_body.at(i).size() == 0){
							for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->contour_points.size(); k++){
								c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->contour_points.at(k));
							}
						}
						else{
							for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->modified_contour_points.size(); k++){
								c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->modified_contour_points.at(k));
							}
						}
					}
				}
			}
			else{
				for (int i = 0; i < m_hbm_contours->full_body.size(); i++){
					for (int j = 0; j < m_hbm_contours->full_body.at(i).size(); j++){
						for (int k = 0; k < m_hbm_contours->full_body.at(i).at(j)->contour_points.size(); k++){
							c_point.push_back(m_hbm_contours->full_body.at(i).at(j)->contour_points.at(k));
						}
					}
				}
			}

			for (vector<NodePtr>::iterator it = all_bones.begin(); it != all_bones.end(); ++it) {
				c_point.push_back(*it);
			}

			if (c_point.size() != 0)
			{
				ofstream output_file(delaunay_file);
				output_file.precision(12);
				ofstream keyword_file;
				int knum = 1;;
				if (k_file)
				{
					keyword_file.open(key_file);
					keyword_file << "*NODE";
				}

				output_file << c_point.size();
				for (int j = 0; j<c_point.size(); j++)
				{
					output_file << endl << num++ << " " << c_point.at(j)->getCoordX() << " " << c_point.at(j)->getCoordY() << " " << c_point.at(j)->getCoordZ();
					keyword_file << endl << setw(8) << knum++ << setw(16) << c_point.at(j)->getCoordX() << setw(16) << c_point.at(j)->getCoordY() << setw(16) << c_point.at(j)->getCoordZ();
				}
				output_file.close();
				if (k_file)
				{
					keyword_file << endl << "*END";
					keyword_file.close();
				}

				//		c_point.clear();

				ifstream data(delaunay_file);
				const int MAXLEN = 512;
				char line[MAXLEN];
				data.getline(line, MAXLEN, '\n');
				/*		while (!data.eof())
				{
				data.getline(line, MAXLEN, '\n');
				istringstream iss_line(line);
				Id node_id;
				iss_line >> node_id >> x >> y >> z;
				NodePtr a = fem.get<Node>(nid);
				point a(x, y, z, nid);
				c_point.push_back(a);
				}
				data.close();
				*/
				for (int i = 0; i<c_point.size(); i++)
				{
					for (int j = i + 1; j<c_point.size(); j++)
					{
						if (c_point[i]->getCoordX() == c_point[j]->getCoordX())
						{
							if (c_point[i]->getCoordY() == c_point[j]->getCoordY())
							{
								if (c_point[i]->getCoordZ() == c_point[j]->getCoordZ())
								{
									c_point.erase(c_point.begin() + i);
									i--;
									j--;
									break;
								}
							}
						}
					}
				}
				num = 0;
				ofstream data1(delaunay_file);
				data1.precision(12);
				data1 << c_point.size();
				for (int i = 0; i<c_point.size(); i++)
				{
					NodePtr a(new Node);
					a = c_point.at(i);
					data1 << endl << num++ << " " << a->getCoordX() << " " << a->getCoordY() << " " << a->getCoordZ();
				}
				data1.close();


			}


		}

		void ProcessingFunc::delaunayTetGen(string fileName)
		{
			string command("TetGen.exe ");
			command += fileName;
			ofstream data("Command.txt");
			data << command << endl;
			data.close();
			if (system(command.c_str())){
//				exit(EXIT_SUCCESS);
			}
			else{
				exit(EXIT_FAILURE);
			}
		}

		void ProcessingFunc::readTetNodeListFile(string node_fileName, map<Id, int>* hash_tet_node, vector<NodePtr>* tet_node_list){
			const int MAXLEN = 512;
			char line[MAXLEN];
			ifstream node_file(node_fileName);
			//ofstream data("tet_nodes.txt");
			node_file.getline(line, MAXLEN, '\n');
			int number_of_nodes;
			istringstream iss_line(line);
			iss_line >> number_of_nodes;

			int nodeid;
			double x, y, z;
			vector<NodePtr> tnl;
			map<Id, int> htn;
			for (int i = 0; i<number_of_nodes; i++)
			{
				node_file.getline(line, MAXLEN, '\n');
				istringstream iss_line(line);

				iss_line >> nodeid >> x >> y >> z;
				//data << node_id << "\t" << x << "\t" << y << "\t" << z << endl;
				NodePtr newnode(new Node);
				newnode->setCoord(x, y, z);
				//newid = Node::getIdfromIdFormat(IdFormat("delaunay_mapping", nodeid));
				//NodePtr newnode = new Node(newid, x, y, z);
				tnl.push_back(newnode);
				htn.insert((pair<Id, int>(newnode->getId(), i)));
			}
			*tet_node_list = tnl;
			*hash_tet_node = htn;

			node_file.close();
			//data.close();
		}

		void ProcessingFunc::readTetListFile(string fileName, vector<tet_element>* tet_list, map<Id, int>* hash_tet, vector<NodePtr>* tet_node_list, map<Id, int>* hash_tet_node, int calc_volume){
			const int MAXLEN = 512;
			char line[MAXLEN];
			ifstream input_file(fileName);
			//ofstream data("volume.txt");
			input_file.getline(line, MAXLEN, '\n');
			int number_of_tets;
			istringstream iss_line(line);
			iss_line >> number_of_tets;
			//	map<int, int> ht;
            Id tet_id, a, b, c, d;
			vector<Id> vertices;
			//	vector<tet_element> tl;
			for (int i = 0; i<number_of_tets; i++)
			{
				input_file.getline(line, MAXLEN, '\n');
				istringstream iss_line(line);
				iss_line >> tet_id >> a >> b >> c >> d;
				vertices.push_back(a);
				vertices.push_back(b);
				vertices.push_back(c);
				vertices.push_back(d);
				tet_element new_tet(tet_id, vertices);
				vertices.clear();
				tet_list->push_back(new_tet);
				//data << new_tet.volume << endl;
				hash_tet->insert(pair<Id, int>(tet_list->at(i).id_label, i));
			}
			//	*hash_tet = ht;
			input_file.close();
			//data.close();
		}

		SId ProcessingFunc::getLandmarks(FEModel& fem, Metadata& m_meta, string lname){
			SId nid_content;
			std::cout << lname;
			if (m_meta.hasLandmark(lname))
			{
				VId idgroupe = m_meta.landmark(lname).get_groupNode();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
                    nid_content.insert(fem.getGroupNode(*it).get().begin(), fem.getGroupNode(*it).get().end());
			}
			return nid_content;
		}

		SId ProcessingFunc::getGenericMetadata(FEModel& fem, Metadata& m_meta, string gmname){
			SId nid_content;
			if (m_meta.hasGenericmetadata(gmname))
			{
				VId idgroupe = m_meta.genericmetadata(gmname).get_groupNode();
				for (auto it = idgroupe.begin(); it != idgroupe.end(); ++it)
					nid_content.insert(fem.getGroupNode(*it).get().begin(), fem.getGroupNode(*it).get().end());
			}
			return nid_content;
		}

		int ProcessingFunc::orientation(Node p, Node q, Node r)
		{
			int val = (q.getCoordY() - p.getCoordY()) * (r.getCoordX() - q.getCoordX()) -
				(q.getCoordX() - p.getCoordX()) * (r.getCoordY() - q.getCoordY());

			if (val == 0) return 0;  // colinear
			return (val > 0) ? 1 : 2; // clock or counterclock wise
		}


		vector<Node> ProcessingFunc::convexHull(std::vector<Node> convnodes){

			size_t n = convnodes.size();
				// There must be at least 3 points
			if (n < 3){
				std::cout << "no of points are less than 3" << std::endl;
				std::vector<Node> a;
//				set<Id> null_set;
//				null_set.insert(0);
//				return null_set;
				a.at(0) = convnodes.at(0);
				return a;
			}

			// Initialize Result
			vector<Node> hull;

			// Find the leftmost point
			size_t l = 0;
			for (size_t i = 1; i < n; i++){
				if (convnodes[i].getCoordX() < convnodes[l].getCoordX()){
					l = i;
				}
			}
			// Start from leftmost point, keep moving counterclockwise
			// until reach the start point again.  This loop runs O(h)
			// times where h is number of points in result or output.
			size_t p = l, q;
			do
			{
				// Add current point to result
				hull.push_back(convnodes[p]);

				// Search for a point 'q' such that orientation(p, x,
				// q) is counterclockwise for all points 'x'. The idea
				// is to keep track of last visited most counterclock-
				// wise point in q. If any point 'i' is more counterclock-
				// wise than q, then update q.
				q = (p + 1) % n;
				for (size_t i = 0; i < n; i++)
				{
					// If i is more counterclockwise than current q, then
					// update q
					if (orientation(convnodes[p], convnodes[i], convnodes[q]) == 2)
						q = i;
					}

				// Now q is the most counterclockwise with respect to p
				// Set p as q for next iteration, so that q is added to
				// result 'hull'
				p = q;

			} while (p != l);  // While we don't come to first point

				// Print Result
            for (hbm::Node node : hull)
                cout << "(" << node.getCoordX() << ", " << node.getCoordY() << ")\n";

			return hull;
		}


		std::vector<double> ProcessingFunc::generate_abcd(NodePtr p1, NodePtr p2, NodePtr p3)
		{
			double a, b, c, d;
			double x1 = p1->getCoordX(); double x2 = p2->getCoordX(); double x3 = p3->getCoordX();
			double y1 = p1->getCoordY(); double y2 = p2->getCoordY(); double y3 = p3->getCoordY();
			double z1 = p1->getCoordZ(); double z2 = p2->getCoordZ(); double z3 = p3->getCoordZ();

			a = (y2 * z3 - y3 * z2) - (y1 * z3 - y3 * z1) + (y1 * z2 - y2 * z1);
			b = -((x2 * z3 - x3 * z2) - (x1 * z3 - x3 * z1) + (x1 * z2 - x2 * z1));
			c = (x2 * y3 - x3 * y2) - (x1 * y3 - x3 * y1) + (x1 * y2 - x2 * y1);
			d = -(x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1));

			std::vector<double> zk;
			zk.push_back(a);
			zk.push_back(b);
			zk.push_back(c);
			zk.push_back(d);


			// determine display quad from 3 points
			/*disp_p1 = p1;
			disp_p2 = p2;
			disp_p3 = p3;
			disp_p4 = p3;	// for now, just display a triangle*/
			return zk;
		}

		/**
		* Returns the bezier control points
		*/
		Eigen::MatrixXd ProcessingFunc::Control_pts_bezier(std::vector<hbm::NodePtr> bezier_vector_pts){

			Eigen::Matrix4d cubic_tangent_mat; //M
			Eigen::Matrix4d bezier_mat;  // N 
			Eigen::MatrixXd ip_pts(4, 3);  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);
			
			bezier_mat << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			cubic_tangent_mat << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;

/*			ip_pts << 289.937 - 161.449 / 3, -128.271 + 0.357 / 3, -68.875 + 34.948 / 3  // p0
				, 423.08 + 269.589 / 3, -138.72 + 27.377 / 3, -51.689 + 280.284 / 3  //p1
				, 161.449*4.6, -0.357*4.6, -34.948*4.6    //p'0
				, 269.589  , 27.377  , 280.284  ;   //p'1
				*/
			
			

			ip_pts << 434.65955 - 457.04715 / 4, -109.00997 + 30.77372 / 4, -57.960609 + 56.682649 / 4
				, 451.62830 + 206.63 /6, -136.33018 + 15.3426 / 6, -22.662910 + 235.45 / 6
				, 457.04715/1.5, -30.77372/1.5, -56.682649/1.5
				, +206.63 /1.2, +15.3426 /1.2, 235.45 /1.2;
			
			///
			/// Cubic tangent input Points
			///
/*			ip_pts << bezier_vector_pts.at(0)->getCoordX(), bezier_vector_pts.at(0)->getCoordY(), bezier_vector_pts.at(0)->getCoordZ()  // p0   1st control
				, bezier_vector_pts.at(1)->getCoordX(), bezier_vector_pts.at(1)->getCoordY(), bezier_vector_pts.at(1)->getCoordZ()  // p1  Last Control
				, bezier_vector_pts.at(2)->getCoordX(), bezier_vector_pts.at(2)->getCoordY(), bezier_vector_pts.at(2)->getCoordZ()  // p0u tangent at first
				, bezier_vector_pts.at(3)->getCoordX(), bezier_vector_pts.at(3)->getCoordY(), bezier_vector_pts.at(3)->getCoordZ();  // p1u tangent at last
	*/			
//			A = cubic_tangent_mat*bezier_mat;   //For coefficients of the cubic equation

			///
			/// Returns a matrix of Bezier Control Points
			///
			Eigen::MatrixXd Bezier_points;
			Bezier_points = bezier_mat.inverse()*cubic_tangent_mat*ip_pts;

			return Bezier_points;



		}


		Eigen::MatrixXd ProcessingFunc::Control_pts_bezier1(std::vector<hbm::NodePtr> bezier_vector_pts){

			Eigen::Matrix4d cubic_tangent_mat; //M
			Eigen::Matrix4d bezier_mat;  // N 
			Eigen::MatrixXd ip_pts(4, 3);  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);

			bezier_mat << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			cubic_tangent_mat << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;


			///
			/// Cubic tangent input Points
			///
						ip_pts << bezier_vector_pts.at(0)->getCoordX(), bezier_vector_pts.at(0)->getCoordY(), bezier_vector_pts.at(0)->getCoordZ()  // p0  Femur(knee) 1st control
			, bezier_vector_pts.at(1)->getCoordX(), bezier_vector_pts.at(1)->getCoordY(), bezier_vector_pts.at(1)->getCoordZ()  // p0 Tibia(Knee) Last Control
			, bezier_vector_pts.at(2)->getCoordX(), bezier_vector_pts.at(2)->getCoordY(), bezier_vector_pts.at(2)->getCoordZ()  // p0 tangent at first
			, bezier_vector_pts.at(3)->getCoordX(), bezier_vector_pts.at(3)->getCoordY(), bezier_vector_pts.at(3)->getCoordZ();  // p0 tangent at last
			
			//			A = cubic_tangent_mat*bezier_mat;   //For coefficients of the cubic equation

			///
			/// Returns a matrix of Bezier Control Points
			///
			Eigen::MatrixXd Bezier_points;
			Bezier_points = bezier_mat.inverse()*cubic_tangent_mat*ip_pts;

			return Bezier_points;



		}

		Eigen::MatrixXd ProcessingFunc::getBezierControlPoints(std::vector<hbm::Coord> spline_info){

			Eigen::Matrix4d cubic_tangent_mat; //M
			Eigen::Matrix4d bezier_mat;  // N 
			Eigen::MatrixXd ip_pts(4, 3);  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);

			bezier_mat << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			cubic_tangent_mat << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;


			///
			/// Cubic tangent input Points
			///
			ip_pts << spline_info.at(0)[0], spline_info.at(0)[1], spline_info.at(0)[2]  // p0  Femur(knee) 1st control
				, spline_info.at(1)[0], spline_info.at(1)[1], spline_info.at(1)[2]  // p0 Tibia(Knee) Last Control
				, spline_info.at(2)[0], spline_info.at(2)[1], spline_info.at(2)[2]  // p0 tangent at first
				, spline_info.at(3)[0], spline_info.at(3)[1], spline_info.at(3)[2];  // p0 tangent at last

			//			A = cubic_tangent_mat*bezier_mat;   //For coefficients of the cubic equation

			///
			/// Returns a matrix of Bezier Control Points
			///
			Eigen::MatrixXd Bezier_points;
			Bezier_points = bezier_mat.inverse()*cubic_tangent_mat*ip_pts;
			return Bezier_points;
		}


		std::vector<double> ProcessingFunc::getAbcdOfContourPlane(Contour cont){
			std::vector<double> abcd;
			vector<NodePtr> plane_temp;
			cont.get_points_for_plane();
			plane_temp = cont.plane_points;	/*!< member function of Contours class returns a vector of pointer to three nodes */
			abcd = generate_abcd(plane_temp.at(0), plane_temp.at(1), plane_temp.at(2));
			return abcd;
		}


		double ProcessingFunc::tparam_forrepos(Eigen::MatrixXd bezier_points, std::vector<double> abcd) {

			double aod, bod, cod;
			aod = abcd.at(0) / abcd.at(3);
			bod = abcd.at(1) / abcd.at(3);
			cod = abcd.at(2) / abcd.at(3);

			Eigen::Matrix4d bezier_mat;
			bezier_mat << -1, 3, -3, 1  //N
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			Eigen::MatrixXd Bx(4, 1);
			Eigen::MatrixXd By(4, 1);
			Eigen::MatrixXd Bz(4, 1);

			Bx << bezier_points(0, 0)
				, bezier_points(1, 0)
				, bezier_points(2, 0)
				, bezier_points(3, 0);

			By << bezier_points(0, 1)
				, bezier_points(1, 1)
				, bezier_points(2, 1)
				, bezier_points(3, 1);

			Bz << bezier_points(0, 2)
				, bezier_points(1, 2)
				, bezier_points(2, 2)
				, bezier_points(3, 2);

			Eigen::MatrixXd Ax, Ay, Az;

			Ax = abcd.at(0)*bezier_mat*Bx;
			Ay = abcd.at(1)*bezier_mat*By;
			Az = abcd.at(2)*bezier_mat*Bz;

			double u3, u2, u1, u0;

			u3 = Ax(0, 0) + Ay(0, 0) + Az(0, 0);
			u2 = Ax(1, 0) + Ay(1, 0) + Az(1, 0);
			u1 = Ax(2, 0) + Ay(2, 0) + Az(2, 0);
			u0 = Ax(3, 0) + Ay(3, 0) + Az(3, 0);

			u0 = u0 + abcd.at(3);

			int result;
			double x11 = u2 / u3;
			double x22 = u1 / u3;
			double x33 = u0 / u3;
			double q[3];

			result = SolveP3(q, x11, x22, x33);
/*			if (result == 1 && q[0] <= 1 && q[0] >= 0){
				std::cout << q[0] << "q0 " << std::endl;
				return q[0];
			}
			else if(result == 3){
				if (q[0] <= 1 && q[0] >= 0){
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					return q[1];
				}
				else if (q[2] <= 1 && q[2] >= 0){
					return q[2];
				}
				else{
					return q[0];
				}
			}
			else{
				return 50;
			}
			std::cout << q[0] << endl;
			if (result == 1){
				if (q[0] <= 1 && q[0] >= 0){
					return q[0];
				}
			}
			

		}
		*/

			if (result == 1){
					std::cout << "RESULT = 1" <<  std::endl;
			}
			else if (result == 3){
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << "RESULT = 3" << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << "RESULT = 3" << std::endl;
					return q[1];
				}
				else if (q[2] <= 1 && q[2] >= 0){
					std::cout << "RESULT = 3" << std::endl;
					return q[2];
				}
				else{
					std::cout << "RESULT = 3" << std::endl;
					return q[0];
				}
			}
			else{
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << "RESULT = 2" << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << "RESULT = 2" << std::endl;
					return q[1];
				}
				else{
					std::cout << "RESULT = 2" << std::endl;
					return q[0];
				}
			}
			std::cout << q[0] << endl;
			if (result == 1){
				if (q[0] <= 1 && q[0] >= 0){
					return q[0];
				}
				else{
					return 50;
				}
			}

            return 0;
		}


		double ProcessingFunc::tvalforflexaxis(hbm::Metadata& meta){
/*			int size_jointcont = meta.m_hbm_contours.joint_contours.size();
			for (int j = 0; j < size_jointcont; j++){

			}
			double x = 0;*/
			return 0;
		}

		double ProcessingFunc::shortest_distance(double u, Eigen::MatrixXd P, std::vector<double> p0){


			Eigen::MatrixXd U(4, 1);
			Eigen::Matrix4d M;
			Eigen::Matrix4d N;
			Eigen::MatrixXd A(4, 3);  //coefficients of spline equation
			Eigen::MatrixXd B(4, 3);  // cubic bezier matrix points
			Eigen::MatrixXd Bx(4, 1);  // bezier x
			Eigen::MatrixXd By(4, 1);  // bezier y
			Eigen::MatrixXd Bz(4, 1);  // bezier z
			Eigen::VectorXd fx1(1);
			Eigen::VectorXd fy1(1);
			Eigen::VectorXd fz1(1);

			N << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			M << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;

			U << u*u*u, u*u, u, 1;

/*			P << 434.65955 - 457.04715 / 4, -109.00997 + 30.77372 / 4, -57.960609 + 56.682649 / 4
				, 451.62830 + 206.63 / 6, -136.33018 + 15.3426 / 6, -22.662910 + 235.45 / 6
				, 457.04715 / 1.5, -30.77372 / 1.5, -56.682649 / 1.5
				, +206.63 / 1.2, +15.3426 / 1.2, 235.45 / 1.2;
				*/

			A = M*P;

			B = N.inverse()*M*P;

			Bx << B(0, 0)
				, B(1, 0)
				, B(2, 0)
				, B(3, 0);

			By << B(0, 1)
				, B(1, 1)
				, B(2, 1)
				, B(3, 1);

			Bz << B(0, 2)
				, B(1, 2)
				, B(2, 2)
				, B(3, 2);

			fx1 = U*N*Bx;
			fy1 = U*N*By;
			fz1 = U*N*Bz;
			double fx, fy, fz;
			fx = fx1.sum();
			fy = fy1.sum();
			fz = fz1.sum();

			double p0x, p0y, p0z, dist;

			
			p0x = p0.at(0);
			p0y = p0.at(1);
			p0z = p0.at(2);
			//
//			dist = (p0x - fx)*(p0x - fx) + (p0y - fy)*(p0y - fy) + (p0z - fz)*(p0z - fz);
			dist = (p0x - fx)*(p0x - fx) + (p0y - fy)*(p0y - fy) + (p0z - fz)*(p0z - fz);
			dist = sqrt(dist);

			return dist;

		}

		Eigen::MatrixXd ProcessingFunc::bezierMat(Eigen::MatrixXd splineMat){

			Eigen::Matrix4d M; /// Cubic Tangent Matrix
			Eigen::Matrix4d N;  /// Bezier Matrix 
			Eigen::MatrixXd ip_pts(4, 3);
			ip_pts = splineMat;  // P
			Eigen::MatrixXd B(4, 3);
			Eigen::MatrixXd A(4, 3);

			N << -1, 3, -3, 1
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			M << 2, -2, 1, 1
				, -3, 3, -2, -1
				, 0, 0, 1, 0
				, 1, 0, 0, 0;

			Eigen::MatrixXd Bezier_points;
			Bezier_points = N.inverse()*M*ip_pts;

			return Bezier_points;

		}

		double ProcessingFunc::uOfContour(Eigen::MatrixXd bezier_points, std::vector<double> abcd) {

			double aod, bod, cod;
			aod = abcd.at(0) / abcd.at(3);
			bod = abcd.at(1) / abcd.at(3);
			cod = abcd.at(2) / abcd.at(3);

			Eigen::Matrix4d N; // Bezier Matrix
			N << -1, 3, -3, 1  //N
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			Eigen::MatrixXd Bx(4, 1);
			Eigen::MatrixXd By(4, 1);
			Eigen::MatrixXd Bz(4, 1);

			Bx << bezier_points(0, 0)
				, bezier_points(1, 0)
				, bezier_points(2, 0)
				, bezier_points(3, 0);

			By << bezier_points(0, 1)
				, bezier_points(1, 1)
				, bezier_points(2, 1)
				, bezier_points(3, 1);

			Bz << bezier_points(0, 2)
				, bezier_points(1, 2)
				, bezier_points(2, 2)
				, bezier_points(3, 2);

			Eigen::MatrixXd Ax, Ay, Az;

			Ax = abcd.at(0)*N*Bx;
			Ay = abcd.at(1)*N*By;
			Az = abcd.at(2)*N*Bz;

			double u3, u2, u1, u0;

			u3 = Ax(0, 0) + Ay(0, 0) + Az(0, 0);
			u2 = Ax(1, 0) + Ay(1, 0) + Az(1, 0);
			u1 = Ax(2, 0) + Ay(2, 0) + Az(2, 0);
			u0 = Ax(3, 0) + Ay(3, 0) + Az(3, 0);

			u0 = u0 + abcd.at(3);

			int result;
			double x11 = u2 / u3;
			double x22 = u1 / u3;
			double x33 = u0 / u3;
			double q[3];

			result = SolveP3(q, x11, x22, x33);
			std::cout << "q0" << q[0] << "   q1" << q[1] << "   q2" << q[2] << endl;
			/*			if (result == 1 && q[0] <= 1 && q[0] >= 0){
			std::cout << q[0] << "q0 " << std::endl;
			return q[0];
			}
			else if(result == 3){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			else if (q[1] <= 1 && q[1] >= 0){
			return q[1];
			}
			else if (q[2] <= 1 && q[2] >= 0){
			return q[2];
			}
			else{
			return q[0];
			}
			}
			else{
			return 50;
			}
			std::cout << q[0] << endl;
			if (result == 1){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			}


			}
			*/

			if (result == 1){
				return q[0];
				std::cout << q[0] << "q0 " << std::endl;
			}
			else if (result == 3){
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else if (q[2] <= 1 && q[2] >= 0){
					std::cout << q[2] << "q2 " << std::endl;
					return q[2];
				}
				else{
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
			}
			else{
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else{
					return q[0];
				}
			}


		}


		NodePtr ProcessingFunc::perform_translation(NodePtr p, double amt, vectors n)
		{
			NodePtr newp = std::make_shared<Node>();
			double x, y, z;
			x = p->getCoordX() + amt*n.a[0];
			y = p->getCoordY() + amt*n.a[1];
			z = p->getCoordZ() + amt*n.a[2];
			newp->setCoord(x, y, z);
			return newp;
		}

		vector<NodePtr> ProcessingFunc::translate_points(vector<NodePtr> v, vectors direction, double dist)
		{
			vector<NodePtr> result;
			result.clear();

			for (unsigned int i = 0; i<v.size(); i++)
				result.push_back(perform_translation(v[i], dist, direction.unit()));

			return result;
		}
	

		Coord ProcessingFunc::getKeyPointCoord(hbm::FEModel& fem, hbm::Metadata& m_meta, const std::string name){
			Coord c(0, 0, 0);
			if (m_meta.hasLandmark(name))
				c = m_meta.landmark(name).position(fem);
			else if (m_meta.hasGenericmetadata(name)){
				VId nodeIds = m_meta.genericmetadata(name).getNodeIdVec(fem);
				if (nodeIds.size() > 1){
					std::cout << "This named-meta data has more than one nodes ";
				}
				NodePtr p;
				for (auto it = nodeIds.begin(); it != nodeIds.end(); it++){
					p = fem.get<Node>(*it);
					c[0] = p->getCoordX();
					c[1] = p->getCoordY();
					c[2] = p->getCoordZ();
				}
			}
			else
				std::cout << std::endl << name << " is neither defined as a landmark or as a named-metadata ";
			return c;
			//std::cout << "Coordinates :" << c[0] << "," << c[1] << "," << c[2] << std::endl;
		}


		vector<vectors> ProcessingFunc::intersection(vector<vector<vectors>> pairs, vector<vectors> plane)
		{
			vector<vectors> side;
			for (int i = 0; i < pairs.size(); i += 1)
			{
				double pt1 = (pairs[i][0].operator-(plane[0])).dot((plane[2].operator-(plane[0])).cross(plane[1].operator-(plane[0])));
				double pt2 = (pairs[i][1].operator-(plane[0])).dot((plane[2].operator-(plane[0])).cross(plane[1].operator-(plane[0])));
				if (pt1*pt2 < 0.000)
				{
					side.push_back((pt1>1e-9) ? pairs[i][0] : pairs[i][1]);
				}
			}
			return(side);
		}

		vector<vector<vectors>> ProcessingFunc::returnSets(vector<vectors> cnt)
		{
			vector<vector<vectors>> sol;
			for (int i = 0; i < cnt.size(); i += 1)
			{
				vector<vectors> ret;
				vectors paired = cnt[i];
				for (int j = 0; j < cnt.size(); j += 1)
				{
					if ((cnt[i].operator-(cnt[j])).mod()>(cnt[i].operator-(paired)).mod())
						paired = cnt[j];
				}
				ret.push_back(cnt[i]);
				ret.push_back(paired);
				sol.push_back(ret);
			}
			return(sol);
		}


		vector<vectors> ProcessingFunc::perform_rotation_new(vector<vectors> ps, double theta, vectors A, vectors B)
		{
			vector<vectors> ans;
			for (int i = 0; i < ps.size(); i += 1)
			{
				vectors p = ps[i];
				double x, y, z;
				x = p.a[0];
				y = p.a[1];
				z = p.a[2];

				double x1 = A.a[0], y1 = A.a[1], z1 = A.a[2];
				double x2 = B.a[0], y2 = B.a[1], z2 = B.a[2];

				double u = x2 - x1, v = y2 - y1, w = z2 - z1;
				double t = double(theta)*3.14159 / 180;
				//Node p;
				p.a[0] = (x1*(v*v + w*w) + u*(-y1*v - z1*w + u*x + v*y + w*z) + (-x1*(v*v + w*w) + u*(y1*v + z1*w - v*y - w*z) + (v*v + w*w)*x)*cos(t) + sqrt(u*u + v*v + w*w)*(-z1*v + y1*w - w*y + v*z)*sin(t)) / (u*u + v*v + w*w);
				p.a[1] = (y1*(u*u + w*w) + v*(-x1*u - z1*w + u*x + v*y + w*z) + (-y1*(u*u + w*w) + v*(x1*u + z1*w - u*x - w*z) + (u*u + w*w)*y)*cos(t) + sqrt(u*u + v*v + w*w)*(z1*u - x1*w + w*x - u*z)*sin(t)) / (u*u + v*v + w*w);
				p.a[2] = (z1*(u*u + v*v) + w*(-x1*u - y1*v + u*x + v*y + w*z) + (-z1*(u*u + v*v) + w*(x1*u + y1*v - u*x - v*y) + (u*u + v*v)*z)*cos(t) + sqrt(u*u + v*v + w*w)*(-y1*u + x1*v - v*x + u*y)*sin(t)) / (u*u + v*v + w*w);
				ans.push_back(p);
				//vectors(u1, u2, u3);
			}
			return(ans);

		}


		vector<vector<vectors>> ProcessingFunc::unique_sol(vector<vector<vectors>> contours, vector<vectors>center, int i, vectors ax1, vectors ax2, int max_rot)
		{
			auto pairs = returnSets(contours[i]);

			vector<vectors> side = intersection(pairs, contours[i + 1]);


			vector<size_t> cnts;
			for (int j = 0; j <= max_rot; j += 1)
			{

				vector<vectors> temp = perform_rotation_new(contours[i], double(j), center[i], center[i].operator+(ax2.operator-(ax1)));
				pairs = returnSets(temp);
				cnts.push_back((intersection(pairs, contours[i + 1])).size() / 2);

				if ((cnts[cnts.size() - 1] == 0) || (cnts[cnts.size() - 1] == contours[i].size()))
				{
					if ((cnts.size() == 1))
					{

						contours[i + 1] = temp;

						break;
					}
					else
					{
						if (cnts.size() < 1)
							continue;
						if ((cnts[cnts.size() - 2] == 0) || (cnts[cnts.size() - 2] == contours[i].size()))
						{
							contours[i + 1] = temp;

							break;
						}
					}

				}
				//	cout << " fixrot: " << j << "\n";
			}
			return(contours);
		}


		vector<vector<vectors>> ProcessingFunc::parallilize(vector<vector<vectors>> contours, vector<vectors> center)
		{
			int moved = 1;
			int iter = 0;
			while ((iter <= 20))
			{
				iter += 1;
				moved = 0;

				for (int i = 0; i < contours.size() - 1; i += 1)
				{
					auto pairs = returnSets(contours[i + 1]);
					if (i == 11)
						cout << "yo";

					vector<vectors> side = intersection(pairs, contours[i]);

					if (side.size() == 0 || (side.size() >= (pairs.size() - 2)))
						continue;
					// find the two farthest points in the set side
					pair<int, int> idx;
					idx.first = 0;
					idx.second = 0;
					for (int j = 0; j < side.size(); j += 1)
					{
						for (int k = 0; k < side.size(); k += 1)
						{
							if ((side[idx.first].operator-(side[idx.second])).mod() < (side[j].operator-(side[k])).mod())
							{
								idx.first = j;
								idx.second = k;
							}
						}
					}
					if (!side.size() || ((idx.first == 0) && (idx.second == 0)))
						continue;
					vectors ax1, ax2;
					ax1 = side[idx.first];
					ax2 = side[idx.second];
					vector<int> cnts;
					int last = 1e9;
					int times = 0;
					int mod = 1;
					for (int j = 0; j < 180 * mod; j += 1)
					{
						vectors axis = (contours[i][0].operator-(contours[i][contours[i].size() - 1])).cross(contours[i][0].operator-(contours[i][contours[i].size() / 2 - 1])).cross(contours[i + 1][0].operator-(contours[i + 1][contours[i + 1].size() - 1])).cross(contours[i + 1][0].operator-(contours[i + 1][contours[i + 1].size() / 2 - 1]));
						axis = axis.mult(-1);
						vector<vectors> temp = perform_rotation_new(contours[i + 1], double(j), center[i + 1], center[i + 1].operator+(axis));
						pairs = returnSets(temp);
						if ((!intersection(pairs, contours[i]).size()) || (intersection(pairs, contours[i]).size() == pairs.size()))
						{
							contours[i + 1] = temp;
							break;
						}
						//cout << "rot: " << j << "\n";
					}
				}
			}
			return(contours);
		}

		void ProcessingFunc::correct_intersections(std::vector<hbm::Contour*> contour_set){

			vector<vectors> center;
			vector<vector<vectors>> contours;
			for (int i = 0; i < contour_set.size(); i += 1)
			{
				
				vector<NodePtr> res;
				res.clear();

				res = contour_set.at(i)->contour_points;
				vector<vectors> cont;
				for (unsigned int j = 0; j<res.size(); j++)
				{
					cont.push_back(vectors(res.at(j)->getCoordX(), res.at(j)->getCoordY(), res.at(j)->getCoordZ()));
//					res.push_back(perform_rotation(v->at(i), theta, A, B));
				}

				contours.push_back(cont);
				double centx = contour_set.at(i)->centroid.getCoordX();
				double centy = contour_set.at(i)->centroid.getCoordY();
				double centz = contour_set.at(i)->centroid.getCoordZ();
				//vectors cent()
				center.push_back(vectors(centx, centy, centz));// << centx << setw(16) << centy << setw(16) << centz << "\n";
			}

			vector<vector<vectors>> result = parallilize(contours, center);
			ofstream otp("output.k");
			otp << "*NODE\n";
			int cnt = 0;
			std::vector<Contour*> final_cont;
			for (size_t i = 0; i < contours.size(); i++)
				for (size_t j = 0; j < contours[i].size(); j++)
				{
					otp << setw(8) << cnt++ << setw(16) << result[i][j].a[0] << setw(16) << result[i][j].a[1] << setw(16) << result[i][j].a[2] << "\n";
					contour_set[i]->contour_points[j]->getCoordX() = result[i][j].a[0];
					contour_set[i]->contour_points[j]->getCoordY() = result[i][j].a[1];
					contour_set[i]->contour_points[j]->getCoordZ() = result[i][j].a[2];
					//otp<< "       "<< cnt++ << "        " << result[i][j].a[0] << "        " << result[i][j].a[1] << "        " << result[i][j].a[2] << "\n";
				}
			otp << "*END\n";
		}
	}
}
