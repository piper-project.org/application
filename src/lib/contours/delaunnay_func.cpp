/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "delaunnay_func.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {


		DelaunnayFunc::DelaunnayFunc(std::string tetGenDir)
		{
            #ifdef WIN32
                tetGenPath = "\"" + tetGenDir + "/TetGenv.exe\" "; // enclosed in quotation marks to handle correctly paths with spaces
            #else
                tetGenPath = "\"" + tetGenDir + "/TetGenv\" ";
            #endif
		}

		double DelaunnayFunc::tet_volume(tet_element tet){

			std::vector<double> a, b, c;

			double volume;
			for (int i = 0; i < 4; i++)
			{
				a.push_back(tet_node_list[hash_tet_node[tet.v_label[i]]].p_x);
				b.push_back(tet_node_list[hash_tet_node[tet.v_label[i]]].p_y);
				c.push_back(tet_node_list[hash_tet_node[tet.v_label[i]]].p_z);
			}
			volume = fabs((a[0] - a[3])*((b[1] - b[3])*(c[2] - c[3]) - (b[2] - b[3])*(c[1] - c[3])) - (a[1] - a[3])*((b[0] - b[3])*(c[2] - c[3]) - (b[2] - b[3])*(c[0] - c[3])) + (a[2] - a[3])*((b[0] - b[3])*(c[1] - c[3]) - (b[1] - b[3])*(c[0] - c[3]))) / 6;
			return volume;
		}

		void DelaunnayFunc::delaunayTetGen(string fileName){
//			string command("TetGen.exe ");

			string command(tetGenPath);
			command += fileName;
			ofstream data("Command.txt");
			data << command << std::endl;
			data.close();
			system(command.c_str());
		}

		void DelaunnayFunc::readTetNodeListFile(string node_fileName){
			const int MAXLEN = 512;
			char line[MAXLEN];
			ifstream node_file(node_fileName);
			node_file.getline(line, MAXLEN, '\n');
			Id number_of_nodes;
			istringstream iss_line(line);
			iss_line >> number_of_nodes;
			Id nodeid;
			double x, y, z;


			std::ofstream tet_node_list_file("tet_node_list_file.txt");
			std::ofstream hash_tet_node_file("hash_tet_node_file.txt");

			for (Id i = 0; i < number_of_nodes; i++)
			{
				node_file.getline(line, MAXLEN, '\n');
				istringstream iss_line(line);

				iss_line >> nodeid >> x >> y >> z;
				//data << node_id << "\t" << x << "\t" << y << "\t" << z << std::endl;
				//					newid = Node::getIdfromIdFormat(IdFormat("delaunay_mapping", nodeid));
				//					NodePtr newnode = new Node(newid, x, y, z);
				point newpoint(nodeid, x, y, z);

				tet_node_list.push_back(newpoint);
				hash_tet_node.insert((pair<Id, Id>(nodeid, i)));
			}

			for (Id i = 0; i < number_of_nodes; i++){
				tet_node_list_file << tet_node_list.at(i).point_id << "    " << tet_node_list.at(i).p_x << "     " << tet_node_list.at(i).p_y << "     " << tet_node_list.at(i).p_z << std::endl;
			}

            for (std::map<Id, Id>::iterator it = hash_tet_node.begin(); it != hash_tet_node.end(); it++){
				hash_tet_node_file << "id : " << it->first << "    int: " << it->second << std::endl;
			}
			/***comments removed***/
			//				std::cout << "tet_node_list" << tet_node_list.size() << std::endl;
			//				std::cout << "hash_tet_node" << hash_tet_node.size() << std::endl;
			/***comments removed***/


			node_file.close();
			//data.close();
        }


		void DelaunnayFunc::DebugTets()
		{

			std::cout << "Inside   " << __FUNCTION__ << std::endl;

			std::cout << "tet_node_list.size()   " << tet_node_list.size() << std::endl;
			for (int i = 0; i < tet_node_list.size(); i++)
			{
				std::cout << tet_node_list[i].point_id << std::endl;
				std::cout << tet_node_list[i].p_x << " " << tet_node_list[i].p_y << " " << tet_node_list[i].p_z << std::endl;
			}

			std::cout << "tet_list.size()   " << tet_list.size() << std::endl;
			for (size_t i = 0; i < tet_list.size(); i++)
			{
				std::cout << tet_list[i].id_label << std::endl;
				std::cout << tet_list[i].v_label[0] << " " << tet_list[i].v_label[1] << " " << tet_list[i].v_label[2] << std::endl;
			}            
        }

		void DelaunnayFunc::readNodes(vector<Node> node_list)
		{
			Id nodeid = 0;
			//When I checked file the Nodeids were same. Not Sure why this Hash map of nodeids is created.
			//Since I know the results, I am assigning the iterator value as the node id hash map
			for (Node &n : node_list)
			{
				point newpoint(nodeid, n.getCoordX(),n.getCoordY(),n.getCoordZ());
				tet_node_list.push_back(newpoint);
				hash_tet_node.insert((pair<Id, Id>(nodeid, nodeid)));
                nodeid++;
			}
		}


		void DelaunnayFunc::readTets(vector<tet_element> vtk_tet_list, int calc_volume)
		{
			std::vector<Id> vertices;
            int i = 0;
            for (tet_element tE: vtk_tet_list)
			{
				vertices = tE.v_label;
                tet_element new_tet(tE.id_label, vertices);
				vertices.clear();
				if (calc_volume == 0)
				{
					new_tet.volume = tet_volume(new_tet);
				}
				tet_list.push_back(new_tet);
				hash_tet.insert(pair<Id, Id>(tet_list.at(i).id_label, i++));
			}
		}


		void DelaunnayFunc::readtetlistFiletetnewstruct(string fileName, int calc_volume)
        {
			const int MAXLEN = 512;
			char line[MAXLEN];
			std::ifstream input_file(fileName);
			input_file.getline(line, MAXLEN, '\n');
			Id number_of_tets;
			istringstream iss_line(line);
			iss_line >> number_of_tets;
			tet_list1.resize(number_of_tets);   // Changed here 
			Id tet_id, a, b, c, d;
			std::vector<Id> vertices;
			//				std::ofstream vol("vol.txt");
			//	std::vector<tet_element> tl;
			for (Id i = 0; i < number_of_tets; i++)
			{
				input_file.getline(line, MAXLEN, '\n');
				std::istringstream iss_line(line);
				iss_line >> tet_id >> a >> b >> c >> d;


				vertices.push_back(a);
				vertices.push_back(b);
				vertices.push_back(c);
				vertices.push_back(d);

				tet_element new_tet(tet_id, vertices);
				vertices.clear();
				if (calc_volume == 0)
				{
					new_tet.volume = tet_volume(new_tet);
				}
				//					vol << "volume" << i << " " <<  new_tet.volume << std::endl;
				tet_list1[i].t = new_tet;  //Changed here
				//					vol << new_tet.volume << std::endl;
				hash_tet.insert(pair<Id, Id>(tet_list1.at(i).t.id_label, i));  //Changed here
			}
			input_file.close();

		}
		void DelaunnayFunc::readTetListFile(string fileName, int calc_volume){

			const int MAXLEN = 512;
			char line[MAXLEN];
			std::ifstream input_file(fileName);
			input_file.getline(line, MAXLEN, '\n');
			Id number_of_tets;
			istringstream iss_line(line);
			iss_line >> number_of_tets;
			Id tet_id, a, b, c, d;
			std::vector<Id> vertices;
			//				std::ofstream vol("vol.txt");
			//	std::vector<tet_element> tl;
			for (Id i = 0; i < number_of_tets; i++)
			{
				input_file.getline(line, MAXLEN, '\n');
				std::istringstream iss_line(line);
				iss_line >> tet_id >> a >> b >> c >> d;


				vertices.push_back(a);
				vertices.push_back(b);
				vertices.push_back(c);
				vertices.push_back(d);

				tet_element new_tet(tet_id, vertices);
				vertices.clear();
				if (calc_volume == 0)
				{
					new_tet.volume = tet_volume(new_tet);
				}
				//					vol << "volume" << i << " " <<  new_tet.volume << std::endl;
				tet_list.push_back(new_tet);
				//					vol << new_tet.volume << std::endl;
				hash_tet.insert(pair<Id, Id>(tet_list.at(i).id_label, i));
			}
			input_file.close();
			//				vol.close();
			/*				std::ofstream tet_info("tet_info.txt");
			int j = 0;
			for (auto it = tet_list.begin(); it != tet_list.end(); it++){
			tet_info << tet_list.at(j).id_label << "    " << tet_list.at(j).v_label[0] << "    " << tet_list.at(j).v_label[1] << "    " << tet_list.at(j).v_label[2] << "    " << tet_list.at(j).v_label[3] << "    " << tet_list.at(j).volume << std::endl;
			j++;
			}

			*/
		}

		bool DelaunnayFunc::SortByZmin(tet_element_new &a, tet_element_new &b)
		{
			return(a.zmin < b.zmin);
		}

		void DelaunnayFunc::reverse(char str[], int length)
		{
			int start = 0;
			int end = length - 1;
			while (start < end)
			{
				swap(*(str + start), *(str + end));
				start++;
				end--;
			}
		}

		char* DelaunnayFunc::itoa(int num, char* str, int base)
		{
			int i = 0;
			bool isNegative = false;

			/* Handle 0 explicitely, otherwise empty string is printed for 0 */
			if (num == 0)
			{
				str[i++] = '0';
				str[i] = '\0';
				return str;
			}

			// In standard itoa(), negative numbers are handled only with
			// base 10. Otherwise numbers are considered unsigned.
			if (num < 0 && base == 10)
			{
				isNegative = true;
				num = -num;
			}

			// Process individual digits
			while (num != 0)
			{
				int rem = num % base;
				str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
				num = num / base;
			}

			// If number is negative, append '-'
			if (isNegative)
				str[i++] = '-';

			str[i] = '\0'; // Append string terminator

			// Reverse the string
			reverse(str, i);

			return str;
		}

		void DelaunnayFunc::clear_all(){
			node_list.clear();
			hash_node.clear();
			tet_list.clear();
			hash_tet.clear();
			hash_map_node.clear();
			tet_node_list.clear();
			hash_tet_node.clear();
		}


		std::vector<point> DelaunnayFunc::ProcessTetMapping(vector<Node> initial_node_list,vector<Node> final_node_list,
				vector<NodePtr>& mapping_nodes, std::vector<tet_element> vtk_tet_list)
		{
			//Read Initial Nodes
			readNodes(initial_node_list);

			//Read the tets
			readTets( vtk_tet_list);

			//Map the Nodes to Tets
			mappingNodesToTet4(mapping_nodes);

			//Print the Tet Details on console
			//DebugTets();

			//Clear all the buffers.
			clear_all();

			//Read Final Nodes
			readNodes(final_node_list);

			//Compute Volume as well
			readTets(vtk_tet_list, 1);

			//Picks the previously mapped file o/p of mappingNodesToTet4
			mappingNodesToTxTets(mapNode2TetOutputFileName);

			//Print the Tet Details on console
			//DebugTets();

			clear_all();
			//This is a ridiculous way of returning, just keeping it for Legacy reasons.
			return vec_pt;
		}

		std::vector<point> DelaunnayFunc::call_delaunay(string tetGen_file, string final_tet_file, vector<NodePtr>& mapping_nodes){

            size_t lastDot = tetGen_file.find_last_of('.');
            string filenameNoExt = tetGen_file.substr(0, lastDot);

            tetNodeListInputFileName = filenameNoExt + ".1." + "node";
            tetListInputFileName = filenameNoExt + ".1." + "ele";
            mapNode2TetOutputFileName = filenameNoExt + ".map." + "node";
			delaunayInputFileName = tetGen_file;
            keywordFileName = filenameNoExt + "_transformed.k";
            
			delaunayTetGen(delaunayInputFileName);
			readTetNodeListFile(tetNodeListInputFileName);
			//				std::ofstream size("size.txt");
			//				size << hash_tet_node.size() << std::endl;
			//				size << tet_node_list.size();
			readTetListFile(tetListInputFileName);
			//				std::ofstream size2("size2.txt");
			//				size2 << tet_list.size() << std::endl;
			//				size2 << hash_tet_node.size() << std::endl;
            

// version1			mappingNodesToTet4(mapping_nodes);
			/***********New Imple*******/
//			MappingNodes(mapping_nodes);
			/***********New Imple*******/
// version3
			MappingNodeshashtet(mapping_nodes);
// version 4
			MappingNodeshashtetserr(mapping_nodes);
			//mappingNodesToTxTets(mapNode2TetOutputFileName);
			clear_all();
			txTetNodeListInputFileName = final_tet_file;
			readTetNodeListFile(txTetNodeListInputFileName);
			readTetListFile(tetListInputFileName, 1);
			mappingNodesToTxTets(mapNode2TetOutputFileName);
			clear_all();
			return vec_pt;
		}


		void DelaunnayFunc::tetgentesting(string tetGen_file){

            size_t lastDot = tetGen_file.find_last_of('.');
            string filenameNoExt = tetGen_file.substr(0, lastDot) + ".node";

            tetNodeListInputFileName = filenameNoExt + ".1." + "node";
            tetListInputFileName = filenameNoExt + ".1." + "ele";
            mapNode2TetOutputFileName = filenameNoExt + ".map." + "node";
			delaunayInputFileName = tetGen_file;
            keywordFileName = filenameNoExt + "_transformed.k";
			delaunayTetGen(delaunayInputFileName);

			readTetNodeListFile(tetNodeListInputFileName);
			//				std::ofstream size("size.txt");
			//				size << hash_tet_node.size() << std::endl;
			//				size << tet_node_list.size();
			//				std::cout << "Reading Done";
			//				std::cout << "Now reading tetListfile ";
			//				std::cout << tetListInputFileName;
			
			readTetListFile(tetListInputFileName);
			//				std::ofstream size2("size2.txt");
			//				size2 << tet_list.size() << std::endl;
			//				size2 << hash_tet_node.size() << std::endl;

			//				std::cout << "\n\n""Reading Done""\n";
			//				std::cout << "\n""Mapping nodes to tet....""\n";
		}


		std::vector<point> DelaunnayFunc::call_delaunay_2(string tetGen_file, string final_tet_file, vector<NodePtr>& mapping_nodes){

            size_t lastDot = tetGen_file.find_last_of('.');
            string filenameNoExt = tetGen_file.substr(0, lastDot);

            tetNodeListInputFileName = filenameNoExt + ".1." + "node";
            tetListInputFileName = filenameNoExt + ".1." + "ele";
            mapNode2TetOutputFileName = filenameNoExt + ".map." + "node";
            keywordFileName = filenameNoExt + "_transformed.k";

			txTetNodeListInputFileName = final_tet_file;
			/***comments removed***/
			//				std::cout << "\n""Reading tet node file""\n";
			/***comments removed***/
			readTetNodeListFile(txTetNodeListInputFileName);
			/***comments removed***/
			//				std::cout << "\n""Reading Done""\n";
			//				std::cout << "\n""Now reading tetListfile ";
			//				std::cout << tetListInputFileName;
			/***comments removed***/
			readTetListFile(tetListInputFileName, 1);
			/***comments removed***/
			//				std::cout << "\n\n""Reading Done""\n";
			//				std::cout << "\n""Repositiining nodes...";
			/***comments removed***/
			mappingNodesToTxTets(mapNode2TetOutputFileName);
			/***comments removed***/
			//				std::cout << "\n""Repositiining Done";
			/***comments removed***/
			clear_all();
			return vec_pt;

		}

		void DelaunnayFunc::mappingNodesToTxTets(string node_fileName)
		{
			//cout << "\n\tDelaunay TetGen and Mapping Utility\n";
			ifstream node_file(node_fileName);
			ofstream data(keywordFileName);
			const int MAXLEN = 512;
			char line[MAXLEN];
			int node_id, tet_id;
			double v1, v2, v3, v4, X, Y, Z;
			std::vector<double> x, y, z;
			string A;
			data << "*NODE" << std::endl;
			while (!node_file.eof())
			{
				node_file.getline(line, MAXLEN, '\n');
				istringstream iss_line(line);
				A = line;
				A.erase(std::remove(A.begin(), A.end(), '\n'), A.end());
				if (!A.empty())
				{

					if (line[0] == '$')
					{
						break;
					}
					iss_line >> node_id >> tet_id >> v1 >> v2 >> v3 >> v4;

					for (int i = 0; i<4; i++)
					{
						x.push_back(tet_node_list[hash_tet_node[tet_list[hash_tet[tet_id]].v_label[i]]].p_x);
						y.push_back(tet_node_list[hash_tet_node[tet_list[hash_tet[tet_id]].v_label[i]]].p_y);
						z.push_back(tet_node_list[hash_tet_node[tet_list[hash_tet[tet_id]].v_label[i]]].p_z);
					}

					X = x[0] * v1 + x[1] * v2 + x[2] * v3 + x[3] * v4;
					Y = y[0] * v1 + y[1] * v2 + y[2] * v3 + y[3] * v4;
					Z = z[0] * v1 + z[1] * v2 + z[2] * v3 + z[3] * v4;
					vec_pt.push_back(point(node_id, X, Y, Z));
					//						node_list.push_back(point(node_id, X, Y, Z));
					data << setw(8) << node_id << setw(16) << X << setw(16) << Y << setw(16) << Z << std::endl;
					x.clear(); X = 0;
					y.clear(); Y = 0;
					z.clear(); Z = 0;
				}

			}
			data << "*END";
			node_file.close();
			data.close();
		}

		void DelaunnayFunc::mappingNodesToTet4(std::vector<NodePtr>& nodes_for_mapping)
		{
			std::ofstream nodes_for_map("nodes_for_mapping.txt");

/*			for (size_t i = 0; i < nodes_for_mapping.size(); i++){
				nodes_for_map << nodes_for_mapping.at(i)->getId() << "     " << nodes_for_mapping.at(i)->getCoordX() << "     " << nodes_for_mapping.at(i)->getCoordY() << "     " << nodes_for_mapping.at(i)->getCoordZ() << std::endl;
			}
			*/
			nodes_for_map.close();

			std::ofstream node_list_initial("node_list_initial.k");
			std::ofstream sorted_node_list_file("sorted_node_list.k");

			std::vector<point> nodes;
			map<int, int> hash_nodes;


			std::vector<size_t> skipped_nodes;

			int  node_id = 0, i = 0;


			int mesh_node_count = 0;
			/***comments removed***/
			//				std::cout << "Mapping Started" << std::endl;
			/***comments removed***/

			for (size_t i = 0; i < nodes_for_mapping.size(); i++)
			{
				node_list.push_back(nodes_for_mapping[i]);

				hash_node.insert((pair<int, int>(node_list[i]->getId(), mesh_node_count)));
				point newpoint(node_list[i]->getId(), node_list[i]->getCoordX(), node_list[i]->getCoordY(), node_list[i]->getCoordZ());
				nodes.push_back(newpoint);
				++mesh_node_count;
			}


			/***comments removed***/
			//				std::cout << "\n""Complete.....";
			/***comments removed***/

			std::vector <point> sorted_node_list = nodes;


			/*				int n = 40000;
			for (std::vector<point>::iterator it = sorted_node_list.begin(); it != sorted_node_list.end(); ++it) {
			point a = *it;
			node_list_initial << std::endl << std::setw(8) << n << std::setw(16) << a.p_x << std::setw(16) << a.p_y << std::setw(16) << a.p_z;
			n++;
			}
			*/

			/***comments removed***/
			//				std::cout << "\n""Sorting mesh node list..";
			/***comments removed***/

			std::sort(sorted_node_list.begin(), sorted_node_list.end(), point_z_comparator());	// sort node_list by z coordinates

			/*				int m = 10000;
			for (std::vector<point>::iterator it = sorted_node_list.begin(); it != sorted_node_list.end(); ++it) {
			point a = *it;
			node_list_initial << std::endl << std::setw(8) << m << std::setw(16) << a.p_x << std::setw(16) << a.p_y << std::setw(16) << a.p_z;
			m++;
			}
			*/


			/***comments removed***/
			//				std::cout << "complete.";
			/***comments removed***/






			/************************************sort tet_list by zmin of 4 vertices*********************************************/


			// --------------------------------------------------------------------------------------------------
			// sort tet_list by zmin of 4 vertices
			// we do this by generating an intermediate vector of <tet_list index, zmin> pairs,
			// sorting the same by zmin values, and then use the resulting sorted vector to generate sorted_tet_list from tet_list

			/***comments removed***/
			//				std::cout << "\n""Sorting tet list..";
			/***comments removed***/

			std::vector< pair<vector<int>::size_type, double> > tet_list_ilist(tet_list.size());


			for (int i = 0; i < tet_list.size(); ++i)
			{
				int tetj = 0;
				const point& node_zmin = tet_node_list[hash_tet_node[tet_list[i].v_label[tetj]]];
				for (int j = 1; j < 4; ++j)
				{
					if (tet_node_list[hash_tet_node[tet_list[i].v_label[j]]].p_z < tet_node_list[hash_tet_node[tet_list[i].v_label[tetj]]].p_z)
					{
						tetj = j;
					}
				}
				double zmin = tet_node_list[hash_tet_node[tet_list[i].v_label[tetj]]].p_z;
				pair<vector<int>::size_type, double> newi;
				newi.first = i;
				newi.second = zmin;
				tet_list_ilist[i] = newi;
			}

			std::sort(tet_list_ilist.begin(), tet_list_ilist.end(), zmin_comparator());
/*			std::ofstream tet_list_zmin("tet_list_zmin.k");
			for (std::vector< pair<vector<int>::size_type, double> >::iterator it = tet_list_ilist.begin(); it != tet_list_ilist.end(); ++it) {
				pair<vector<int>::size_type, double> a = *it;
				tet_list_zmin << std::endl << std::setw(8) << a.first << std::setw(16) << a.second;
			}
			*/


			/************************************sort tet_list by zmin of 4 vertices*********************************************/


			vector <tet_element> sorted_tet_list(tet_list.size());

			for (size_t i = 0; i < tet_list_ilist.size(); ++i)
			{
				sorted_tet_list[i] = tet_list[tet_list_ilist[i].first];
			}


			std::vector< pair<size_t, double> > tetzmin(tet_list.size()), tetzmax(tet_list.size());


			for (size_t i = 0; i < sorted_tet_list.size(); ++i)
			{
				int zmini = 0, zmaxi = 0;
				for (int j = 1; j < 4; ++j)
				{
					if (tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[j]]].p_z < tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[zmini]]].p_z)
					{
						zmini = j;
					}
					if (tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[j]]].p_z > tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[zmaxi]]].p_z)
					{
						zmaxi = j;
					}
				}
                pair<size_t, double> newtetzmin;
				newtetzmin.first = i;
				newtetzmin.second = tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[zmini]]].p_z;
				tetzmin[i] = newtetzmin;

                pair<size_t, double> newtetzmax;
				newtetzmax.first = i;
				newtetzmax.second = tet_node_list[hash_tet_node[sorted_tet_list[i].v_label[zmaxi]]].p_z;
				tetzmax[i] = newtetzmax;
			}

			// --------------------------------------------------------------------------------------------------
			// map node_list to tet_list (main loop)

			// tolerances
			double d_zmax = 10.0;
			double d_zmin = 10.0;

			typedef std::list< typename std::vector<tet_element>::size_type > atl_t;	// linked list of indices into sorted_tet_list

			atl_t atl;

			std::vector<tet_element>::size_type tetlist_next = 0;	// index of sorted_tet_list to start scanning for next node in phase 2
			point p0, p1, p2, p3;

			std::vector<double> vol_coord(4);
			int index = 0;
			std::ofstream map_file(mapNode2TetOutputFileName);
			std::ofstream skipped_nodes_file(skippedNodesOutputFileName);


			for (size_t i = 0; i < sorted_node_list.size(); ++i)
			{
				const point& p = sorted_node_list[i];

				atl_t::iterator j = atl.begin();
				bool zmaxtestfail;
				while (j != atl.end())			// phase 1
				{
					zmaxtestfail = false;
					if (tetzmax[*j].second + d_zmax < p.p_z)
					{
						j = atl.erase(j);
						zmaxtestfail = true;
					}
					else {
						const tet_element& next_tet = sorted_tet_list[*j];

						//---------------------------------------
						// test of p lies inside tet sorted_tet_list[*j]
						// if it does then output result in map

						const point& ntet0 = tet_node_list[hash_tet_node[next_tet.v_label[0]]];
						const point& ntet1 = tet_node_list[hash_tet_node[next_tet.v_label[1]]];
						const point& ntet2 = tet_node_list[hash_tet_node[next_tet.v_label[2]]];
						const point& ntet3 = tet_node_list[hash_tet_node[next_tet.v_label[3]]];

						//const node& ntet0 = tet_node_list[next_tet.v_label[0]];
						//const node& ntet1 = tet_node_list[next_tet.v_label[1]];
						//const node& ntet2 = tet_node_list[next_tet.v_label[2]];
						//const node& ntet3 = tet_node_list[next_tet.v_label[3]];

						//point p0, p1, p2, p3;
						p0.p_x = ntet0.p_x; p0.p_y = ntet0.p_y; p0.p_z = ntet0.p_z; p0.point_id = ntet0.point_id;
						p1.p_x = ntet1.p_x; p1.p_y = ntet1.p_y; p1.p_z = ntet1.p_z; p1.point_id = ntet1.point_id;
						p2.p_x = ntet2.p_x; p2.p_y = ntet2.p_y; p2.p_z = ntet2.p_z; p2.point_id = ntet2.point_id;
						p3.p_x = ntet3.p_x; p3.p_y = ntet3.p_y; p3.p_z = ntet3.p_z; p3.point_id = ntet3.point_id;



						if (tet_contains_point(p, p0, p1, p2, p3, vol_coord))
						{
							volume_cordinates new_coordinate(p.point_id, next_tet.id_label, vol_coord);
							mapped_node_list.push_back(new_coordinate);
							hash_map_node[mapped_node_list[index].node_label] = index;
							map_file << setw(8) << p.point_id << setw(8) << next_tet.id_label << setw(16) << vol_coord[0] << setw(16) << vol_coord[1] << setw(16) << vol_coord[2] << setw(16) << vol_coord[3] << std::endl;
							++index;

							break;
						}
						//---------------------------------------
					}

					// update j *only* if zmax test did *not* fail
					if (zmaxtestfail != true)
					{
						++j;
					}

				}


				if (j == atl.end())		// exhaustively searched ATL, now proceed to search in remaining part of sorted_tet_list
				{
					size_t k = tetlist_next;
					for (; k < sorted_tet_list.size(); ++k)				// phase 2
					{
						const tet_element& next_tet = sorted_tet_list[k];

						if (tetzmin[k].second - d_zmin > p.p_z)		// this tet (and all subsequent ones) lie above p, so declare p as skipped node
						{
							skipped_nodes.push_back(p.point_id);
							ostringstream iss;
							iss << "zmin [" << tetzmin[k].second << "] of next tet ID [" << sorted_tet_list[k].id_label << "] exceeds node z [" << p.p_z << "]";
							skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
							break;

						}
						else {

							++tetlist_next;

							if (tetzmax[k].second + d_zmax < p.p_z)
							{
								continue;

							}
							else {
								atl.push_back(k);

								//---------------------------------------
								// test of p lies inside tet sorted_tet_list[*j]
								// if it does then output result in map

								const point& ntet0 = tet_node_list[hash_tet_node[next_tet.v_label[0]]];
								const point& ntet1 = tet_node_list[hash_tet_node[next_tet.v_label[1]]];
								const point& ntet2 = tet_node_list[hash_tet_node[next_tet.v_label[2]]];
								const point& ntet3 = tet_node_list[hash_tet_node[next_tet.v_label[3]]];

								//const node& ntet0 = tet_node_list[next_tet.v_label[0]];
								//const node& ntet1 = tet_node_list[next_tet.v_label[1]];
								//const node& ntet2 = tet_node_list[next_tet.v_label[2]];
								//const node& ntet3 = tet_node_list[next_tet.v_label[3]];

								//point p0, p1, p2, p3;
								p0.p_x = ntet0.p_x; p0.p_y = ntet0.p_y; p0.p_z = ntet0.p_z; p0.point_id = ntet0.point_id;
								p1.p_x = ntet1.p_x; p1.p_y = ntet1.p_y; p1.p_z = ntet1.p_z; p1.point_id = ntet1.point_id;
								p2.p_x = ntet2.p_x; p2.p_y = ntet2.p_y; p2.p_z = ntet2.p_z; p2.point_id = ntet2.point_id;
								p3.p_x = ntet3.p_x; p3.p_y = ntet3.p_y; p3.p_z = ntet3.p_z; p3.point_id = ntet3.point_id;




								if (tet_contains_point(p, p0, p1, p2, p3, vol_coord))
								{
									volume_cordinates new_coordinate(p.point_id, next_tet.id_label, vol_coord);
									mapped_node_list.push_back(new_coordinate);
									hash_map_node[mapped_node_list[index].node_label] = index;
									map_file << setw(8) << p.point_id << setw(8) << next_tet.id_label << setw(16) << vol_coord[0] << setw(16) << vol_coord[1] << setw(16) << vol_coord[2] << setw(16) << vol_coord[3] << std::endl;
									++index;

									break;
								}
								//---------------------------------------
							}
						}
					}
					if (k == sorted_tet_list.size())
					{
						skipped_nodes.push_back(p.point_id);
						skipped_nodes_file << p.point_id << std::endl;
						ostringstream iss;
						iss << "cannot map node into any tet in tet list";
						skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
					}
				}
			}

			map_file.close();
			skipped_nodes_file.close();

		}


		void DelaunnayFunc::MappingNodes(vector<NodePtr> &nodes_for_mapping)
		{
			std::ofstream nodes_for_map("nodes_for_mapping.txt");
			for (size_t i = 0; i < nodes_for_mapping.size(); i++){
			nodes_for_map << nodes_for_mapping.at(i)->getId() << "     " << nodes_for_mapping.at(i)->getCoordX() << "     " << nodes_for_mapping.at(i)->getCoordY() << "     " << nodes_for_mapping.at(i)->getCoordZ() << std::endl;
			}
			

			std::ofstream testnodes("testnodes.k");
			testnodes << "*NODE" ;

			int knum = 0;
			for (size_t j = 0; j<nodes_for_mapping.size(); j++)
			{
				testnodes << endl << setw(8) << knum++ << setw(16) << nodes_for_mapping.at(j)->getCoordX() << setw(16) << nodes_for_mapping.at(j)->getCoordY() << setw(16) << nodes_for_mapping.at(j)->getCoordZ();
			}

			testnodes << "*END";
			
			vector<int> SkippedNodes;
			std::vector<point> nodes;

			int mesh_node_count = 0;

			for (size_t i = 0; i < nodes_for_mapping.size(); i++)
			{
				node_list.push_back(nodes_for_mapping[i]);

				hash_node.insert((pair<int, int>(node_list[i]->getId(), mesh_node_count)));
				point newpoint(node_list[i]->getId(), node_list[i]->getCoordX(), node_list[i]->getCoordY(), node_list[i]->getCoordZ());
				nodes.push_back(newpoint);
				++mesh_node_count;
			}

//thigh
//			point p1(7515847, 102.818062, -143.493729, -22.34267235);
//			point p2(7031575, 314.295013, -146.889008, -62.36410141);   // Given 2 points which would be the new Z axis


//ankle2calf points
//			point p1(7515847, 485.983 ,- 147.913, - 23.9447);
//			point p2(7031575, 672.603, - 129.886,  194.193);   // Given 2 points which would be the new Z axis


//foot
//			point p1(7515847, 769.059, - 135.548,  207.588);
//			point p2(7031575, 834.605, - 136.141,  185.004);   // Given 2 points which would be the new Z axis

//			t1 = clock();
//			Transform(p1, p2, nodes);
//			t2 = clock();

			vector<point>SortedNodeList = nodes;

		
			sort(SortedNodeList.begin(), SortedNodeList.end(), point_z_comparator());    //node_list sorted

		
			//cout<<"Nodes Sorted\n";

            vector<pair<size_t, double> > tet_listInt;      //tet_list sorted..To do so we take a list of pairs called tet_listInt where each pair has tetid and zmin
			//We sort this list according to increasing zmin
			for (size_t i = 0; i < tet_list.size(); i++)
			{
				double zmin = tet_node_list[hash_tet_node[tet_list[i].v_label[0]]].p_z;
				double zmax = tet_node_list[hash_tet_node[tet_list[i].v_label[0]]].p_z;
				for (int j = 1; j < 4; j++)
				{
					if (tet_node_list[hash_tet_node[tet_list[i].v_label[j]]].p_z < zmin)
					{
						zmin = tet_node_list[hash_tet_node[tet_list[i].v_label[j]]].p_z;
					}
				}

                pair<size_t, double> new_node;
				new_node.first = i;
				new_node.second = zmin;

				tet_listInt.push_back(new_node);
			}

			sort(tet_listInt.begin(), tet_listInt.end(), zmin_comparator());

			vector<tet_element> Sortedtet_list;

			for (size_t i = 0; i < tet_list.size(); i++) //The Sortedtet_list is according to tet_listInt
			{

				Sortedtet_list.push_back(tet_list[tet_listInt[i].first]);
			}

            vector< pair<size_t, double> > tetzmin(tet_list.size()), tetzmax(tet_list.size());
            vector< pair<size_t, double> > tetymin(tet_list.size()), tetymax(tet_list.size());
            vector< pair<size_t, double> > tetxmin(tet_list.size()), tetxmax(tet_list.size());

			//cout<<"Tets Sorted\n";

			//tetzmin and tetzmax are two vectors having pairs of tetid,zmin and tetid,zmax respectively
			for (size_t i = 0; i < Sortedtet_list.size(); ++i)
			{
				double zmin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_z;
				double zmax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_z;
				double ymin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_y;
				double ymax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_y;
				double xmin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_x;
				double xmax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[0]]].p_x;

				for (int j = 1; j < 4; ++j)
				{
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_z < zmin)
					{
						zmin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_z;
					}
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_z > zmax)
					{
						zmax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_z;
					}
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_y < ymin)
					{
						ymin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_y;
					}
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_y > ymax)
					{
						ymax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_y;
					}
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_x < xmin)
					{
						xmin = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_x;
					}
					if (tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_x > xmax)
					{
						xmax = tet_node_list[hash_tet_node[Sortedtet_list[i].v_label[j]]].p_x;
					}

				}
                pair<size_t, double> newtetzmin;
				newtetzmin.first = i;
				newtetzmin.second = zmin;
				tetzmin[i] = newtetzmin;

                pair<size_t, double> newtetzmax;
				newtetzmax.first = i;
				newtetzmax.second = zmax;
				tetzmax[i] = newtetzmax;

                pair<size_t, double> newtetymin;
				newtetymin.first = i;
				newtetymin.second = ymin;
				tetymin[i] = newtetymin;

                pair<size_t, double> newtetymax;
				newtetymax.first = i;
				newtetymax.second = ymax;
				tetymax[i] = newtetymax;

                pair<size_t, double> newtetxmin;
				newtetxmin.first = i;
				newtetxmin.second = xmin;
				tetxmin[i] = newtetxmin;

                pair<size_t, double> newtetxmax;
				newtetxmax.first = i;
				newtetxmax.second = xmax;
				tetxmax[i] = newtetxmax;
			}


			vector<tet_element>::size_type tetnext = 0;

			typedef list< typename vector<tet_element>::size_type > atl_type;

			atl_type atl;

			vector<double> ratios(4);

			std::ofstream map_file(mapNode2TetOutputFileName);
			std::ofstream skipped_nodes_file(skippedNodesOutputFileName);

			int index = 0;

			for (size_t i = 0; i < SortedNodeList.size(); i++)
			{
				atl_type::iterator j = atl.begin();

				const point & p = SortedNodeList[i];

				bool flag;

				while (j != atl.end())
				{

					flag = false;       //if zmax of tet is less than z coordinate of node p then it is deleted from atl otherwise the node is checked in atl
					if (tetzmax[*j].second<p.p_z)
					{
						j = atl.erase(j);
						flag = true;
					}
					else
					{
						if ((tetymin[*j].second <= p.p_y) && (tetymax[*j].second >= p.p_y) && (tetxmin[*j].second <= p.p_x) && (tetxmax[*j].second >= p.p_x))
						{

							const tet_element & t = Sortedtet_list[*j];

							const point p0 = tet_node_list[hash_tet_node[t.v_label[0]]];
							const point p1 = tet_node_list[hash_tet_node[t.v_label[1]]];
							const point p2 = tet_node_list[hash_tet_node[t.v_label[2]]];
							const point p3 = tet_node_list[hash_tet_node[t.v_label[3]]];


							if (PointInTet(p, p0, p1, p2, p3, ratios))
							{
								volume_cordinates v(p.point_id, t.id_label, ratios);

								map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
								mapped_node_list.push_back(v);
								hash_map_node[mapped_node_list[index].node_label] = index;
								index++;
								break;
							}
						}

					}
					if (flag != true) //j updated only when zmax of tet is not less than z coordinate of node
					{
						j++;
					}
				}

				//when the list is searched completely.. new tets are added from Sortedtet_list
				if (j == atl.end())
				{

					size_t k = tetnext;

					for (; k< Sortedtet_list.size(); k++)
					{
						tet_element & t = Sortedtet_list[k];



						if (tetzmin[k].second>p.p_z) //As the zmin of the tet is greater than z of node ...eventually all the following tets will have zmin larger than this,
							//therefore it becomes a skipped node
						{

							SkippedNodes.push_back(p.point_id);
							ostringstream iss;
							iss << "zmin [" << tetzmin[k].second << "] of next tet ID [" << Sortedtet_list[k].id_label << "] exceeds node z [" << p.p_z << "]";
							skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
							break;
						}
						else
						{
							tetnext++;         //If zmax of tet is smaller than z then we don't add it to atl
							if (tetzmax[k].second<p.p_z)
							{
								continue;
							}
							else //the tet is pushed in atl and checked
							{

								atl.push_back(k);

								if ((tetymin[k].second <= p.p_y) && (tetymax[k].second >= p.p_y) && (tetxmin[k].second <= p.p_x) && (tetxmax[k].second >= p.p_x))
								{

									const point p0 = tet_node_list[hash_tet_node[t.v_label[0]]];
									const point p1 = tet_node_list[hash_tet_node[t.v_label[1]]];
									const point p2 = tet_node_list[hash_tet_node[t.v_label[2]]];
									const point p3 = tet_node_list[hash_tet_node[t.v_label[3]]];


									if (PointInTet(p, p0, p1, p2, p3, ratios))
									{
										volume_cordinates v(p.point_id, t.id_label, ratios);
										map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
										mapped_node_list.push_back(v);
										break;
									}
								}
							}
						}
					}
					if (k == Sortedtet_list.size()) //When the pointer reaches the end of list it means the node could not be mapped to any tet
					{							//Thus it is a SkippedNode
						SkippedNodes.push_back(p.point_id);
						skipped_nodes_file << p.point_id << std::endl;
						ostringstream iss;
						iss << "cannot map node into any tet in tet list";
						skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
					}
				}
			}
		}

		//Hashing it before only so that the tet_list node ids are already hashed which can be used directly instead of calling hash_tet_node everytime!
		void DelaunnayFunc::MappingNodeshashtet(vector<NodePtr> &nodes_for_mapping)
		{
			vector<int> SkippedNodes;
			std::vector<point> nodes;

			int mesh_node_count = 0;

			for (size_t i = 0; i < nodes_for_mapping.size(); i++)
			{
				node_list.push_back(nodes_for_mapping[i]);

				hash_node.insert((pair<int, int>(node_list[i]->getId(), mesh_node_count)));
				point newpoint(node_list[i]->getId(), node_list[i]->getCoordX(), node_list[i]->getCoordY(), node_list[i]->getCoordZ());
				nodes.push_back(newpoint);
				++mesh_node_count;
			}


			/***************a2c*******************/
			//point p1(7515847,485.983 , -147.913 , -23.9447);
			//point p2(7031575,672.603 , -129.886 , 194.193);   // Given 2 points which would be the new Z axis


			/*********Thigh Points*********/
			//point p1(7515847,102.818062 , -143.493729 , -22.34267235 );
			//point p2(7031575,314.295013 ,  -146.889008 , -62.36410141);   // Given 2 points which would be the new Z axis



			/********Foot Points************/
			//point p1(7515847,769.059 , -135.548 , 207.588);
			//point p2(7031575,834.605 , -136.141 , 185.004);   // Given 2 points which would be the new Z axis




			//Transform(p1,p2,nodes);


			for (size_t i = 0; i < tet_list.size(); i++)                //Hashing it before only so that the tet_list node ids are already hashed which can be used directly instead of calling hash_tet_node everytime!
			{
				tet_list[i].v_label[0] = hash_tet_node[tet_list[i].v_label[0]];
				tet_list[i].v_label[1] = hash_tet_node[tet_list[i].v_label[1]];
				tet_list[i].v_label[2] = hash_tet_node[tet_list[i].v_label[2]];
				tet_list[i].v_label[3] = hash_tet_node[tet_list[i].v_label[3]];
			}

			vector<point>SortedNodeList = nodes;

			/*********************node_list sorted**************************/

			sort(SortedNodeList.begin(), SortedNodeList.end(), point_z_comparator());


			/************************************sort tet_list by zmin of 4 vertices*********************************************/

			//To do so we take a vector of pairs called tet_listInt where each pair has tetid and zmin
			//We sort this according to increasing zmin values, and then the resulting sorted vector is used to generate the Sortedtet_list



            vector<pair<size_t, double> > tet_listInt;
			for (size_t i = 0; i < tet_list.size(); i++)
			{
				double zmin = tet_node_list[tet_list[i].v_label[0]].p_z;
				double zmax = tet_node_list[tet_list[i].v_label[0]].p_z;
				for (int j = 1; j < 4; j++)
				{
					if (tet_node_list[tet_list[i].v_label[j]].p_z < zmin)
					{
						zmin = tet_node_list[tet_list[i].v_label[j]].p_z;
					}
				}

                pair<size_t, double> new_node;
				new_node.first = i;
				new_node.second = zmin;

				tet_listInt.push_back(new_node);
			}

			sort(tet_listInt.begin(), tet_listInt.end(), zmin_comparator());

			std::ofstream tet_list_zmin("tet_list_zmin.k");
            for (std::vector< pair<size_t, double> >::iterator it = tet_listInt.begin(); it != tet_listInt.end(); ++it) {
				tet_list_zmin << std::endl << std::setw(8) << it->first << std::setw(16) << it->second;
			}

			/**************The Sortedtet_list is according to tet_listInt****************************/

			vector<tet_element> Sortedtet_list;

			for (size_t i = 0; i < tet_list.size(); i++)
			{
				Sortedtet_list.push_back(tet_list[tet_listInt[i].first]);
			}


            vector< pair<size_t, double> > tetzmin(tet_list.size()), tetzmax(tet_list.size());
            vector< pair<size_t, double> > tetymin(tet_list.size()), tetymax(tet_list.size());
            vector< pair<size_t, double> > tetxmin(tet_list.size()), tetxmax(tet_list.size());



			//tetzmin and tetzmax are two vectors having pairs of tetid,zmin and tetid,zmax respectively
			//tetymin and tetymax are two vectors having pairs of tetid,ymin and tetid,ymax respectively
			//tetxmin and tetxmax are two vectors having pairs of tetid,xmin and tetid,xmax respectively

			for (size_t i = 0; i < Sortedtet_list.size(); ++i)
			{
				double zmin = tet_node_list[Sortedtet_list[i].v_label[0]].p_z;
				double zmax = tet_node_list[Sortedtet_list[i].v_label[0]].p_z;
				double ymin = tet_node_list[Sortedtet_list[i].v_label[0]].p_y;
				double ymax = tet_node_list[Sortedtet_list[i].v_label[0]].p_y;
				double xmin = tet_node_list[Sortedtet_list[i].v_label[0]].p_x;
				double xmax = tet_node_list[Sortedtet_list[i].v_label[0]].p_x;

				for (int j = 1; j < 4; ++j)
				{
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_z < zmin)
					{
						zmin = tet_node_list[Sortedtet_list[i].v_label[j]].p_z;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_z > zmax)
					{
						zmax = tet_node_list[Sortedtet_list[i].v_label[j]].p_z;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_y < ymin)
					{
						ymin = tet_node_list[Sortedtet_list[i].v_label[j]].p_y;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_y > ymax)
					{
						ymax = tet_node_list[Sortedtet_list[i].v_label[j]].p_y;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_x < xmin)
					{
						xmin = tet_node_list[Sortedtet_list[i].v_label[j]].p_x;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_x > xmax)
					{
						xmax = tet_node_list[Sortedtet_list[i].v_label[j]].p_x;
					}

				}
                pair<size_t, double> newtetzmin;
				newtetzmin.first = i;
				newtetzmin.second = zmin;
				tetzmin[i] = newtetzmin;

                pair<size_t, double> newtetzmax;
				newtetzmax.first = i;
				newtetzmax.second = zmax;
				tetzmax[i] = newtetzmax;

                pair<size_t, double> newtetymin;
				newtetymin.first = i;
				newtetymin.second = ymin;
				tetymin[i] = newtetymin;

                pair<size_t, double> newtetymax;
				newtetymax.first = i;
				newtetymax.second = ymax;
				tetymax[i] = newtetymax;

                pair<size_t, double> newtetxmin;
				newtetxmin.first = i;
				newtetxmin.second = xmin;
				tetxmin[i] = newtetxmin;

                pair<size_t, double> newtetxmax;
				newtetxmax.first = i;
				newtetxmax.second = xmax;
				tetxmax[i] = newtetxmax;
			}


			vector<tet_element>::size_type tetnext = 0;

			typedef vector< typename vector<tet_element>::size_type > atl_type;

			atl_type atl;

			vector<double> ratios(4);

			std::ofstream map_file(mapNode2TetOutputFileName);
			std::ofstream skipped_nodes_file(skippedNodesOutputFileName);

			int index = 0;

			for (size_t i = 0; i < SortedNodeList.size(); i++)
			{
				atl_type::iterator j = atl.begin();

				const point & p = SortedNodeList[i];

				bool flag;

				while (j != atl.end())
				{

					flag = false;       //if zmax of tet is less than z coordinate of node p then it is deleted from atl otherwise the node is checked in atl
					if (tetzmax[*j].second<p.p_z)
					{
						j = atl.erase(j);
						flag = true;
					}
					else
					{
						if ((tetymin[*j].second <= p.p_y) && (tetymax[*j].second >= p.p_y) && (tetxmin[*j].second <= p.p_x) && (tetxmax[*j].second >= p.p_x))
						{

							const tet_element & t = Sortedtet_list[*j];

							const point p0 = tet_node_list[t.v_label[0]];
							const point p1 = tet_node_list[t.v_label[1]];
							const point p2 = tet_node_list[t.v_label[2]];
							const point p3 = tet_node_list[t.v_label[3]];


							if (PointInTet(p, p0, p1, p2, p3, ratios))
							{
								volume_cordinates v(p.point_id, t.id_label, ratios);

								map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
								mapped_node_list.push_back(v);
								hash_map_node[mapped_node_list[index].node_label] = index;
								index++;
								break;
							}
						}
					}
					if (flag != true) //j updated only when zmax of tet is not less than z coordinate of node
					{
						j++;
					}
				}
				

				//when the list is searched completely.. new tets are added from Sortedtet_list
				if (j == atl.end())
				{

					size_t k = tetnext;

					for (; k< Sortedtet_list.size(); k++)
					{
						tet_element & t = Sortedtet_list[k];



						if (tetzmin[k].second>p.p_z) //As the zmin of the tet is greater than z of node ...eventually all the following tets will have zmin larger than this,
							//therefore it becomes a skipped node
						{

							SkippedNodes.push_back(p.point_id);
							ostringstream iss;
							iss << "zmin [" << tetzmin[k].second << "] of next tet ID [" << Sortedtet_list[k].id_label << "] exceeds node z [" << p.p_z << "]";
							skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
							break;
						}
						else
						{
							tetnext++;         //If zmax of tet is smaller than z then we don't add it to atl
							if (tetzmax[k].second<p.p_z)
							{
								continue;
							}
							else //the tet is pushed in atl and checked
							{

								atl.push_back(k);

								if ((tetymin[k].second <= p.p_y) && (tetymax[k].second >= p.p_y) && (tetxmin[k].second <= p.p_x) && (tetxmax[k].second >= p.p_x))
								{

									const point p0 = tet_node_list[t.v_label[0]];
									const point p1 = tet_node_list[t.v_label[1]];
									const point p2 = tet_node_list[t.v_label[2]];
									const point p3 = tet_node_list[t.v_label[3]];


									if (PointInTet(p, p0, p1, p2, p3, ratios))
									{
										volume_cordinates v(p.point_id, t.id_label, ratios);
										map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
										mapped_node_list.push_back(v);
										break;
									}
								}
							}
						}
					}
					if (k == Sortedtet_list.size()) //When the pointer reaches the end of list it means the node could not be mapped to any tet
					{							//Thus it is a SkippedNode
						SkippedNodes.push_back(p.point_id);
						skipped_nodes_file << p.point_id << std::endl;
						ostringstream iss;
						iss << "cannot map node into any tet in tet list";
						skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
					}
				}
			}
		}


		void DelaunnayFunc::MappingNodeshashtetserr(vector<NodePtr> &nodes_for_mapping)
		{
			float c1 = 0, c2 = 0;

			vector<int> SkippedNodes;
			std::vector<point> nodes;

			int mesh_node_count = 0;

			for (size_t i = 0; i < nodes_for_mapping.size(); i++)
			{
				node_list.push_back(nodes_for_mapping[i]);

				hash_node.insert((pair<int, int>(node_list[i]->getId(), mesh_node_count)));
				point newpoint(node_list[i]->getId(), node_list[i]->getCoordX(), node_list[i]->getCoordY(), node_list[i]->getCoordZ());
				nodes.push_back(newpoint);
				++mesh_node_count;
			}


			/***************a2c*******************/
			//point p1(7515847,485.983 , -147.913 , -23.9447);
			//point p2(7031575,672.603 , -129.886 , 194.193);   // Given 2 points which would be the new Z axis


			/*********Thigh Points*********/
			//point p1(7515847,102.818062 , -143.493729 , -22.34267235 );
			//point p2(7031575,314.295013 ,  -146.889008 , -62.36410141);   // Given 2 points which would be the new Z axis



			/********Foot Points************/
			//point p1(7515847,769.059 , -135.548 , 207.588);
			//point p2(7031575,834.605 , -136.141 , 185.004);   // Given 2 points which would be the new Z axis




			//Transform(p1,p2,nodes);


			for (size_t i = 0; i < tet_list.size(); i++)                //Hashing it before only so that the tet_list node ids are already hashed which can be used directly instead of calling hash_tet_node everytime!
			{
				tet_list[i].v_label[0] = hash_tet_node[tet_list[i].v_label[0]];
				tet_list[i].v_label[1] = hash_tet_node[tet_list[i].v_label[1]];
				tet_list[i].v_label[2] = hash_tet_node[tet_list[i].v_label[2]];
				tet_list[i].v_label[3] = hash_tet_node[tet_list[i].v_label[3]];
			}

			vector<point>SortedNodeList = nodes;

			/*********************node_list sorted**************************/

			sort(SortedNodeList.begin(), SortedNodeList.end(), point_z_comparator());


			/************************************sort tet_list by zmin of 4 vertices*********************************************/

			//To do so we take a vector of pairs called tet_listInt where each pair has tetid and zmin
			//We sort this according to increasing zmin values, and then the resulting sorted vector is used to generate the Sortedtet_list



            vector<pair<size_t, double> > tet_listInt;
			for (size_t i = 0; i < tet_list.size(); i++)
			{
				double zmin = tet_node_list[tet_list[i].v_label[0]].p_z;
				double zmax = tet_node_list[tet_list[i].v_label[0]].p_z;
				for (int j = 1; j < 4; j++)
				{
					if (tet_node_list[tet_list[i].v_label[j]].p_z < zmin)
					{
						zmin = tet_node_list[tet_list[i].v_label[j]].p_z;
					}
				}

                pair<size_t, double> new_node;
				new_node.first = i;
				new_node.second = zmin;

				tet_listInt.push_back(new_node);
			}

			sort(tet_listInt.begin(), tet_listInt.end(), zmin_comparator());

			std::ofstream tet_list_zmin("tet_list_zmin.k");
            for (std::vector< pair<size_t, double> >::iterator it = tet_listInt.begin(); it != tet_listInt.end(); ++it) {
				tet_list_zmin << std::endl << std::setw(8) << it->first << std::setw(16) << it->second;
			}

			/**************The Sortedtet_list is according to tet_listInt****************************/

			vector<tet_element> Sortedtet_list;

			for (size_t i = 0; i < tet_list.size(); i++)
			{

				Sortedtet_list.push_back(tet_list[tet_listInt[i].first]);
			}

            vector< pair<size_t, double> > tetzmin(tet_list.size()), tetzmax(tet_list.size());
            vector< pair<size_t, double> > tetymin(tet_list.size()), tetymax(tet_list.size());
            vector< pair<size_t, double> > tetxmin(tet_list.size()), tetxmax(tet_list.size());



			//tetzmin and tetzmax are two vectors having pairs of tetid,zmin and tetid,zmax respectively
			//tetymin and tetymax are two vectors having pairs of tetid,ymin and tetid,ymax respectively
			//tetxmin and tetxmax are two vectors having pairs of tetid,xmin and tetid,xmax respectively

			for (size_t i = 0; i < Sortedtet_list.size(); ++i)
			{
				double zmin = tet_node_list[Sortedtet_list[i].v_label[0]].p_z;
				double zmax = tet_node_list[Sortedtet_list[i].v_label[0]].p_z;
				double ymin = tet_node_list[Sortedtet_list[i].v_label[0]].p_y;
				double ymax = tet_node_list[Sortedtet_list[i].v_label[0]].p_y;
				double xmin = tet_node_list[Sortedtet_list[i].v_label[0]].p_x;
				double xmax = tet_node_list[Sortedtet_list[i].v_label[0]].p_x;

				for (int j = 1; j < 4; ++j)
				{
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_z < zmin)
					{
						zmin = tet_node_list[Sortedtet_list[i].v_label[j]].p_z;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_z > zmax)
					{
						zmax = tet_node_list[Sortedtet_list[i].v_label[j]].p_z;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_y < ymin)
					{
						ymin = tet_node_list[Sortedtet_list[i].v_label[j]].p_y;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_y > ymax)
					{
						ymax = tet_node_list[Sortedtet_list[i].v_label[j]].p_y;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_x < xmin)
					{
						xmin = tet_node_list[Sortedtet_list[i].v_label[j]].p_x;
					}
					if (tet_node_list[Sortedtet_list[i].v_label[j]].p_x > xmax)
					{
						xmax = tet_node_list[Sortedtet_list[i].v_label[j]].p_x;
					}

				}
                pair<size_t, double> newtetzmin;
				newtetzmin.first = i;
				newtetzmin.second = zmin;
				tetzmin[i] = newtetzmin;

                pair<size_t, double> newtetzmax;
				newtetzmax.first = i;
				newtetzmax.second = zmax;
				tetzmax[i] = newtetzmax;

                pair<size_t, double> newtetymin;
				newtetymin.first = i;
				newtetymin.second = ymin;
				tetymin[i] = newtetymin;

                pair<size_t, double> newtetymax;
				newtetymax.first = i;
				newtetymax.second = ymax;
				tetymax[i] = newtetymax;

                pair<size_t, double> newtetxmin;
				newtetxmin.first = i;
				newtetxmin.second = xmin;
				tetxmin[i] = newtetxmin;

                pair<size_t, double> newtetxmax;
				newtetxmax.first = i;
				newtetxmax.second = xmax;
				tetxmax[i] = newtetxmax;
			}


			vector<tet_element>::size_type tetnext = 0;

			typedef vector< typename vector<tet_element>::size_type > atl_type;

			atl_type atl;

			vector<double> ratios(4);

			std::ofstream map_file(mapNode2TetOutputFileName);
			std::ofstream skipped_nodes_file(skippedNodesOutputFileName);

			int index = 0;

			for (size_t i = 0; i < SortedNodeList.size(); i++)
			{
				//atl_type::iterator j = atl.begin();
				int j = 0;
				const point & p = SortedNodeList[i];

				bool flag;

                while (j < atl.size()){

					flag = false;       //if zmax of tet is less than z coordinate of node p then it is deleted from atl otherwise the node is checked in atl
					if (tetzmax[atl.at(j)].second < p.p_z){
						atl.erase(atl.begin() + j);
						flag = true;
					}
					else
					{
						if ((tetymin[atl.at(j)].second <= p.p_y) && (tetymax[atl.at(j)].second >= p.p_y) && (tetxmin[atl.at(j)].second <= p.p_x) && (tetxmax[atl.at(j)].second >= p.p_x))
						{

							const tet_element & t = Sortedtet_list[atl.at(j)];

							const point p0 = tet_node_list[t.v_label[0]];
							const point p1 = tet_node_list[t.v_label[1]];
							const point p2 = tet_node_list[t.v_label[2]];
							const point p3 = tet_node_list[t.v_label[3]];


							if (PointInTet(p, p0, p1, p2, p3, ratios))
							{
								volume_cordinates v(p.point_id, t.id_label, ratios);

								map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
								mapped_node_list.push_back(v);
								hash_map_node[mapped_node_list[index].node_label] = index;
								index++;
								break;
							}
						}

					}
					if (flag != true) //j updated only when zmax of tet is not less than z coordinate of node
					{
						j++;
					}
				}


				//when the list is searched completely.. new tets are added from Sortedtet_list
				if (j == atl.size())
				{

					size_t k = tetnext;

					for (; k< Sortedtet_list.size(); k++)
					{
						tet_element & t = Sortedtet_list[k];

						if (tetzmin[k].second>p.p_z) //As the zmin of the tet is greater than z of node ...eventually all the following tets will have zmin larger than this,
							//therefore it becomes a skipped node
						{

							SkippedNodes.push_back(p.point_id);
							ostringstream iss;
							iss << "zmin [" << tetzmin[k].second << "] of next tet ID [" << Sortedtet_list[k].id_label << "] exceeds node z [" << p.p_z << "]";
							skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
							break;
						}
						else
						{
							tetnext++;         //If zmax of tet is smaller than z then we don't add it to atl
							if (tetzmax[k].second<p.p_z)
							{
								continue;
							}
							else //the tet is pushed in atl and checked
							{

								atl.push_back(k);

								if ((tetymin[k].second <= p.p_y) && (tetymax[k].second >= p.p_y) && (tetxmin[k].second <= p.p_x) && (tetxmax[k].second >= p.p_x))
								{

									const point p0 = tet_node_list[t.v_label[0]];
									const point p1 = tet_node_list[t.v_label[1]];
									const point p2 = tet_node_list[t.v_label[2]];
									const point p3 = tet_node_list[t.v_label[3]];


									if (PointInTet(p, p0, p1, p2, p3, ratios))
									{
										volume_cordinates v(p.point_id, t.id_label, ratios);
										map_file << setw(8) << p.point_id << setw(8) << t.id_label << setw(16) << ratios[0] << setw(16) << ratios[1] << setw(16) << ratios[2] << setw(16) << ratios[3] << std::endl;
										mapped_node_list.push_back(v);
										break;
									}
								}
							}
						}
					}
					if (k == Sortedtet_list.size()) //When the pointer reaches the end of list it means the node could not be mapped to any tet
					{							//Thus it is a SkippedNode
						SkippedNodes.push_back(p.point_id);
						skipped_nodes_file << p.point_id << std::endl;
						ostringstream iss;
						iss << "cannot map node into any tet in tet list";
						skipped_nodes_file << p.point_id << "\t" << iss.str() << std::endl;
					}
				}
			}
		}


		void DelaunnayFunc::Transform(point & p1, point & p2, vector<point>& NodesToMap)
		{
			double z1 = p2.p_x - p1.p_x;  // New z coordinates which is from p1 to p2 is given by (z1 , z2 , z3)
			double z2 = p2.p_y - p1.p_y;  // Let it be denoted by Z
			double z3 = p2.p_z - p1.p_z;


			point n;

			double x1, x2, x3, y1, y2, y3;

			for (size_t i = 0; i<NodesToMap.size(); i++)   // From NodesToMap an arbitrary point is taken which should not be p1 or p2 or should be collinear to the line joining p1 and p2
			{
				n = NodesToMap[i];
				if (!((n.point_id == p1.point_id) || (n.point_id == p2.point_id)))
				{
					double x = n.p_x - p1.p_x;     //The vector from p1 to n is given by (x,y,z).. let it be denoted by N
					double y = n.p_y - p1.p_y;
					double z = n.p_z - p1.p_z;

					x1 = z2*z - y*z3;  // New X coordinates  
					x2 = x*z3 - z1*z;   // This is the result of cross product of Z and N
					x3 = z1*y - x*z2;   // Let it be denoted by X

					//If the cross product  comes out to be 0 then it means that n, p1, p2 are collinear So we check for the next node
					if (!((x1 == 0.0) && (x2 == 0.0) && (x3 == 0.0)))
					{
						y1 = z2*x3 - x2*z3;     // New Y coordinates
						y2 = x1*z3 - z1*x3;    // This is the result of cross product of Z and X
						y3 = z1*x2 - x1*z2;

						break;
					}
				}
			}

			/**************************Transforming nodes to local coordinate system**********************************/
			for (size_t i = 0; i<NodesToMap.size(); i++)
			{
				point s = NodesToMap[i];

				NodesToMap[i].p_x = s.p_x*x1 + s.p_y*x2 + s.p_z*x3;
				NodesToMap[i].p_y = s.p_x*y1 + s.p_y*y2 + s.p_z*y3;
				NodesToMap[i].p_z = s.p_x*z1 + s.p_y*z2 + s.p_z*z3;
			}


			/*******************************Transforming tet_node_list to local coordinate system**********************************/

			for (size_t i = 0; i< tet_node_list.size(); i++)
			{
				point s = tet_node_list[i];

				tet_node_list[i].p_x = s.p_x*x1 + s.p_y*x2 + s.p_z*x3;
				tet_node_list[i].p_y = s.p_x*y1 + s.p_y*y2 + s.p_z*y3;
				tet_node_list[i].p_z = s.p_x*z1 + s.p_y*z2 + s.p_z*z3;
			}
		}
    }
}
