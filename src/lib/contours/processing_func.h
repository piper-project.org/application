/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PROCESSINGFUNC_H
#define PROCESSINGFUNC_H

//#define CONTOURS_EXPORT __declspec( dllexport )

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include <iomanip>
#include "hbm/HumanBodyModel.h"
#include "delaunnay_data_struct.h"
#include "vector_func.h"
#include "poly34.h"
#define PI 3.1415926535897932384626433832795

namespace piper {
	namespace contours {
		class  CONTOURS_EXPORT ProcessingFunc
		{
		public:
			// Hip Repositioning 
			hbm::NodePtr tpointOnCubicBezier(std::vector<hbm::NodePtr> spline_nodess, float t, int f);
			Eigen::MatrixXd getBezierControlPointsnoTangent(std::vector<hbm::Coord> spline_info);
			hbm::NodePtr diff_PointOnCubicBezier(std::vector<hbm::NodePtr> spline_nodess, float t, int k);

            // modifies the point <c>p</c>
			void perform_rotation(hbm::NodePtr p, double theta, vectors A, vectors B);

			std::set<hbm::Id> getnodesbypart(const hbm::Entity& entity, std::set<hbm::Id> sidnodes, hbm::FEModel const& fem);
	
			std::set<hbm::Id> getnodesfromentity(const hbm::Entity& entity, std::set<hbm::Id> sidnodes, hbm::FEModel& fem);

			// Personalization

			double distance(hbm::Node a, hbm::Node b);

			std::vector<double> Centroid(std::vector<hbm::NodePtr> pts);

			hbm::Node translate(hbm::Node c, hbm::Node a, hbm::Node b, double translate_distance);

			void translate2(hbm::Node& c, hbm::Node a, hbm::Node b, double translate_distance);

			void translate1(hbm::NodePtr c, hbm::Node a, hbm::Node b, double translate_distance);

			void translate_contour(std::map<int, piper::hbm::Contour*> cont_map, int cont_id, double distance, hbm::Node a, hbm::Node b);

			// Personalization

			// Knee Repositioning

			double lagrangeInterpolatingPolynomial(double pos[], double val[], int degree, double desiredPos);

			void rotate_points1(std::vector<hbm::NodePtr>* selectedPoints, double theta, vectors A, vectors B);

			std::vector<hbm::NodePtr> rotate_points(std::vector<hbm::NodePtr> selectedPoints, double theta, vectors A, vectors B);

			vectors circumcentre(vectors A, vectors B);

            // modifies the content of v!
			void rotate_this_contour(std::vector<hbm::NodePtr>* v, double theta, vectors A, vectors B);

			std::vector<hbm::NodePtr> translateContour(std::vector<hbm::NodePtr>* v, double amt, vectors A);

			void rotate_contours(std::vector<std::vector<hbm::NodePtr>>::iterator FIRST, std::vector<std::vector<hbm::NodePtr>>::iterator LAST, double theta, vectors A, vectors B);

			vectors line_plane_intersect(std::vector<hbm::NodePtr> PLANE, vectors A, vectors B);

			void write_delaunay_input_file(std::string filename, bool k_file, bool modify, hbm::HBM_contours* m_hbm_contours);

			void write_delaunay_input_file_3(std::string filename, bool k_file, std::vector<hbm::Node> all_cont);

			void write_delaunay_input_file_hip(std::string filename, bool k_file, std::vector<hbm::Node> all_cont);

			/**
			* Write Input File For Delaunnay in .node format.
			* @param filename a string. Name of the file to be written in .k and .node format.
			* @param k_file a boolean. If true the .k file should also be written in addition to .node file
			* @param m_hbm_contours object of HBM_Contours class. Information about the initial and final contour points is contained in this object.
			* @param all_bones a vector of Node. If there are bone nodes present then this variable contains all information of all bone nodes.
			* @return void
			*/
			void write_delaunay_input_file_2(std::string filename, bool k_file, bool modify, hbm::HBM_contours* m_hbm_contours, std::vector<hbm::Node> all_bones);
			void write_delaunay_input_file_1(std::string filename, bool k_file, bool modify, hbm::HBM_contours* m_hbm_contours, std::vector<hbm::NodePtr> all_bones);

			void write_delaunay_input_file_contourCL(std::string filename, bool k_file, bool modify, std::vector<hbm::Contour*>, std::vector<hbm::Node> all_bones);


			void delaunayTetGen(std::string fileName);

			void readTetNodeListFile(std::string node_fileName, std::map<hbm::Id, int>* hash_tet_node, std::vector<hbm::NodePtr>* tet_node_list);

			void readTetListFile(std::string fileName, std::vector<tet_element>* tet_list, std::map<hbm::Id, int>* hash_tet, std::vector<hbm::NodePtr>* tet_node_list, std::map<hbm::Id, int>* hash_tet_node, int calc_volume = 0);

			/**
			* Get the set of nodeids from the name of lanmark.
			* @param fem an object of current FEModel instance.
			* @param m_meta an object of Metadata. To use the metadata of the current context instance.
			* @return set of nodeids
			* Knee Repositioning.
			*/
			hbm::SId getLandmarks(hbm::FEModel& fem, hbm::Metadata& m_meta, std::string lname);

			hbm::SId getGenericMetadata(hbm::FEModel& fem, hbm::Metadata& m_meta, std::string lgmname);

			std::vector<hbm::Node> convexHull(std::vector<hbm::Node>);

			int orientation(hbm::Node p, hbm::Node q, hbm::Node r);

			std::vector<double> generate_abcd(hbm::NodePtr p1, hbm::NodePtr p2, hbm::NodePtr p3);

			Eigen::MatrixXd Control_pts_bezier(std::vector<hbm::NodePtr> bezier_vector_pts);

			Eigen::MatrixXd Control_pts_bezier1(std::vector<hbm::NodePtr> bezier_vector_pts);

			/// returns a matrix of bezier control points
			Eigen::MatrixXd getBezierControlPoints(std::vector<hbm::Coord> spilne_info);

			std::vector<double> getAbcdOfContourPlane(hbm::Contour);

			bool pointOnSameSideOfPlane(std::vector<double> abcd, Eigen::MatrixXd refrence_point, Eigen::MatrixXd test_point);

			double tparam_forrepos(Eigen::MatrixXd bezier_points, std::vector<double> abcd);

			double tvalforflexaxis(hbm::Metadata& mmeta);

			double shortest_distance(double u, Eigen::MatrixXd P , std::vector<double> p0);


			/**
			* Convert Spline Matrix to Bezier Matrix
			* @param splineMat a spline matrix with 2 points and two corresponding tangents
			* @return bezierMat bezier matrix with four control points
			*/
			Eigen::MatrixXd bezierMat(Eigen::MatrixXd splineMat);

			/**
			* determine the u value of a plane if it lies between the first and last control points of bezier curve
			* @param abcd is a vector of double containing the A,B,C and D of plane
			* @param bezier_points a matrix of bezier control points
			* @return u value of the plane having A,B,C,D as given in the parameter 
			*/
			double uOfContour(Eigen::MatrixXd bezier_points, std::vector<double> abcd);


			hbm::NodePtr perform_translation(hbm::NodePtr p, double amt, vectors n);

			//part translation
			std::vector<hbm::NodePtr> translate_points(std::vector<hbm::NodePtr> v, vectors direction, double dist);

			hbm::Coord getKeyPointCoord(hbm::FEModel& fem, hbm::Metadata& m_meta, const std::string name);


			std::vector<std::vector<vectors>> returnSets(std::vector<vectors> cnt);

			std::vector<std::vector<vectors>> parallilize(std::vector<std::vector<vectors>> contours, std::vector<vectors> center);

			std::vector<vectors> intersection(std::vector<std::vector<vectors>> pairs, std::vector<vectors> plane);

			std::vector<vectors> perform_rotation_new(std::vector<vectors> ps, double theta, vectors A, vectors B);
				
			std::vector<std::vector<vectors>> unique_sol(std::vector<std::vector<vectors>> contours, std::vector<vectors>center, int i, vectors ax1, vectors ax2, int max_rot);

			void correct_intersections(std::vector <hbm::Contour*>);


		};
	}
}
#endif

