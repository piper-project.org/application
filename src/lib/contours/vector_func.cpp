/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "vector_func.h"
#include <iostream>
using namespace std;
using namespace piper::hbm;

namespace piper {
	namespace contours {




		vectors::vectors(){

		}


		vectors::vectors(Node s, Node t){

			a[0] = s.getCoordX() - t.getCoordX();
			a[1] = s.getCoordY() - t.getCoordY();
			a[2] = s.getCoordZ() - t.getCoordZ();

		}


		vectors::vectors(NodePtr s, NodePtr t){
			a[0] = s->getCoordX() - t->getCoordX();
			a[1] = s->getCoordY() - t->getCoordY();
			a[2] = s->getCoordZ() - t->getCoordZ();
		}


		vectors::vectors(NodePtr p){

			a[0] = p->getCoordX();
			a[1] = p->getCoordY();
			a[2] = p->getCoordZ();

		}

		vectors::vectors(Node* p){

			a[0] = p->getCoordX();
			a[1] = p->getCoordY();
			a[2] = p->getCoordZ();

		}


		vectors::vectors(Node p){

			a[0] = p.getCoordX();
			a[1] = p.getCoordY();
			a[2] = p.getCoordZ();

		}


		vectors::vectors(double x, double y, double z){

			a[0] = x;
			a[1] = y;
			a[2] = z;

		}


		vectors vectors::operator *(vectors v)
		{

			return vectors(a[1] * v.a[2] - a[2] * v.a[1], a[2] * v.a[0] - a[0] * v.a[2], a[0] * v.a[1] - a[1] * v.a[0]);

		}


		vectors vectors::operator +(vectors v){
			return vectors(a[0] + v.a[0], a[1] + v.a[1], a[2] + v.a[2]);
		}

		vectors vectors::operator -(vectors v){
			return vectors(a[0] - v.a[0], a[1] - v.a[1], a[2] - v.a[2]);
		}


		vectors vectors::operator ~(){
			return vectors(-a[0], -a[1], -a[2]);
		}
		void vectors::print(){
			std::cout << a[0] << " " << a[1] << " " << a[2] << std::endl;
		}


		vectors vectors::unit(){
			double d = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
			return vectors(a[0] / d, a[1] / d, a[2] / d);
		}

		double vectors::dot(vectors v){
			return a[0] * v.a[0] + a[1] * v.a[1] + a[2] * v.a[2];
		}


		double vectors::angle(vectors v){
			return rad_to_deg(acos(dot(v) / (mod()*v.mod())));
		}

		vectors vectors::cross(vectors v){
			vectors res;
			res.a[0] = (a[1] * v.a[2]) - (a[2] * v.a[1]);
			res.a[1] = (a[2] * v.a[0]) - (a[0] * v.a[2]);
			res.a[2] = (a[0] * v.a[1]) - (a[1] * v.a[0]);
			return res;
		}

		vectors vectors::mult(double k){
			return vectors(k*a[0], k*a[1], k*a[2]);
		}

		double vectors::mod(){
			return sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
		}

	}
}
	
