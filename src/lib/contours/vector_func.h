/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_VECTORFUNC_H
#define PIPER_VECTORFUNC_H

//#define CONTOURS_EXPORT __declspec( dllexport )
#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include <iostream>
#include <unordered_map>
#include <vector>
#include "hbm/Node.h"


namespace piper {
	namespace contours {

// -------------------------------- contour types --------------------------------

		class CONTOURS_EXPORT vectors
{

public:
		
		double a[3];

		vectors();


		vectors(piper::hbm::Node s, piper::hbm::Node t);


		vectors(piper::hbm::NodePtr s, piper::hbm::NodePtr t);


        vectors(piper::hbm::NodePtr p);

//        vectors(piper::hbm::NodePtr p) { vectors(p.get()); }

		vectors(piper::hbm::Node* p);

		vectors(piper::hbm::Node p);


		vectors(double x, double y, double z);


		vectors operator *(vectors v);
		vectors operator +(vectors v);

		vectors operator -(vectors v);
		vectors operator ~();

		void print();

		vectors unit();


		double dot(vectors v);

		double angle(vectors v);

		double rad_to_deg(double x){ return x*57.29578; }

		double deg_to_rad(double x){ return x / 57.29578; }
		vectors cross(vectors v);

		vectors mult(double k);




		double mod();



};

	
	}

}
#endif

