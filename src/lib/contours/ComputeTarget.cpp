/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ComputeTarget.h"


using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		ComputeTarget::ComputeTarget() {
		}

		ComputeTarget::~ComputeTarget()
		{
		}


		Coord ComputeTarget::computeJoinntCenter(Metadata c_meta, FEModel fem, vector<string> landmarkvec){
			
			Coord coorda, coordb;
			coorda = c_meta.landmark(landmarkvec.at(0)).position(fem);
			coordb = c_meta.landmark(landmarkvec.at(1)).position(fem);

			coorda[0] = (coorda[0] + coordb[0]) / 2;
			coorda[1] = (coorda[1] + coordb[1]) / 2;
			coorda[2] = (coorda[2] + coordb[2]) / 2;

			return coorda;

		}

		vector<double> ComputeTarget::computeNormalFromLandmarks(Metadata c_meta, FEModel fem, vector<string> landmarkvec){
			std::vector<double> normal;
			Coord coorda, coordb;

			coorda = c_meta.landmark(landmarkvec.at(0)).position(fem);
			coordb = c_meta.landmark(landmarkvec.at(1)).position(fem);

			coorda[0] = coorda[0] - coordb[0];
			coorda[1] = coorda[1] - coordb[1];
			coorda[2] = coorda[2] - coordb[2];

			normal.push_back(coorda[0]);
			normal.push_back(coorda[1]);
			normal.push_back(coorda[2]);

			return normal;
		}

		vector<hbm::Coord> ComputeTarget::projPointsOnPlane(Coord jointcenter, vector<string> flexionaxes, LandmarkTarget targetlmk, string lmkname, hbm::Metadata c_meta, FEModel c_fem){
			std::vector<double> normal;
			std::vector<double> pointonplane;
			std::vector<Coord> planepoints;
			pointonplane.push_back(jointcenter[0]);
			pointonplane.push_back(jointcenter[1]);
			pointonplane.push_back(jointcenter[2]);
			
			Coord proja, projb;
			normal = computeNormalFromLandmarks(c_meta, c_fem, flexionaxes);
			proja = conthbm.landmarkProjectionOnPlane(lmkname, c_fem, c_meta, normal, pointonplane);
//			projb = targetlmk.getCoord();
			std::map<unsigned int, double> coordMap = targetlmk.value();
			projb[0] = coordMap[0];
			projb[1] = coordMap[1];
			projb[2] = coordMap[2];
			std::vector<double> projbcord;
			projbcord.push_back(projb[0]);
			projbcord.push_back(projb[1]);
			projbcord.push_back(projb[2]);
			projb = sptrans.pointProjectionOnPlane(normal, projbcord, pointonplane);
			planepoints.push_back(jointcenter);
			planepoints.push_back(proja);
			planepoints.push_back(projb);
			return planepoints;
		}

		vector<hbm::Coord> ComputeTarget::projPointsOnPlane1(Coord jointcenter, vector<string> flexionaxes, std::string landmark_parent, string landmark_child, hbm::Metadata c_meta, FEModel c_fem){
			std::vector<double> normal;
			std::vector<double> pointonplane;
			std::vector<Coord> planepoints;
			pointonplane.push_back(jointcenter[0]);
			pointonplane.push_back(jointcenter[1]);
			pointonplane.push_back(jointcenter[2]);

			Coord proja, projb;
			std::cout << "A";
			normal = computeNormalFromLandmarks(c_meta, c_fem, flexionaxes);
			proja = conthbm.landmarkProjectionOnPlane(landmark_child, c_fem, c_meta, normal, pointonplane);
			projb = conthbm.landmarkProjectionOnPlane(landmark_parent, c_fem, c_meta, normal, pointonplane);
			planepoints.push_back(jointcenter);
			planepoints.push_back(proja);
			planepoints.push_back(projb);
			std::cout << "jc: " << jointcenter[0] << "  " << jointcenter[1] << "  " << jointcenter[2] << std::endl;
			std::cout << landmark_child << " :  " << proja[0] << "  " << proja[1] << "  " << proja[2] << std::endl;
			std::cout << landmark_parent << " :  "<< projb[0] << "  " << projb[1] << "  " << projb[2] << std::endl;
			return planepoints;
		}

		double ComputeTarget::angleTargetLandmark(vector<Coord> planepoints){
			Coord a1, a2, a3;

			a1 = planepoints.at(0);
			a2 = planepoints.at(1);
			a3 = planepoints.at(2);
			a2[0] = a2[0] - a1[0];
			a2[1] = a2[1] - a1[1];
			a2[2] = a2[2] - a1[2];
			a3[0] = a3[0] - a1[0];
			a3[1] = a3[1] - a1[1];
			a3[2] = a3[2] - a1[2];


			vectors vec1(a2[0], a2[1], a2[2]);
			vectors vec2(a3[0], a3[1], a3[2]);

			double angle;
/*			angle = a1.dot(a2);
			double d1 = sqrt(a1.a[0] * a1.a[0] + a1.a[1] * a1.a[1] + a1.a[2] * a1.a[2]);
			double d2 = sqrt(a2.a[0] * a2.a[0] + a2.a[1] * a2.a[1] + a2.a[2] * a2.a[2]);
			angle = angle / (d1*d2);
			if (angle > 1 || angle < -1){
				return 1000;
			}
			angle = acos(angle);
	*/		

            //Using Eigen lib to check if these are almost zero
            Coord vec_i, vec_f;
            vec_i << a2[0], a2[1], a2[2];
            vec_f << a3[0], a3[1], a3[2];

            if(vec_i.isApprox(Coord::Zero()) || vec_f.isApprox(Coord::Zero()))
                    angle = 0;
            else
                angle = vec1.angle(vec2);

			return angle;
		}

		double ComputeTarget::angleSTargetLandmark(vector<Coord> planepoints, vector<string> veclandmark, Metadata c_meta, FEModel fem){


			Coord coorda, coordb;
			coorda = c_meta.landmark(veclandmark.at(0)).position(fem);
			coordb = c_meta.landmark(veclandmark.at(1)).position(fem);

			

			coorda[0] = (coorda[0] - coordb[0]);
			coorda[1] = (coorda[1] - coordb[1]);
			coorda[2] = (coorda[2] - coordb[2]);

			vectors flexaxis(coorda[0], coorda[1], coorda[2]);
			flexaxis = flexaxis.unit();

			Coord a1, a2, a3;

			a1 = planepoints.at(0);
			a2 = planepoints.at(1);
			a3 = planepoints.at(2);
			a2[0] = a2[0] - a1[0];
			a2[1] = a2[1] - a1[1];
			a2[2] = a2[2] - a1[2];
			a3[0] = a3[0] - a1[0];
			a3[1] = a3[1] - a1[1];
			a3[2] = a3[2] - a1[2];

			vectors vec1(a2[0], a2[1], a2[2]);
			vectors vec2(a3[0], a3[1], a3[2]);

			Coord vec_i, vec_f;
			double angle;
			vec_i << a2[0], a2[1], a2[2];
			vec_f << a3[0], a3[1], a3[2];

			if (vec_i.isApprox(Coord::Zero()) || vec_f.isApprox(Coord::Zero()))
				angle = 0;
			else
				angle = vec1.angle(vec2);

			std::cout << "magnitude: " << angle << std::endl;

			double sign;

			vectors crossvec = vec1.cross(vec2);
			crossvec = crossvec.unit();

			
			
			sign = crossvec.dot(flexaxis);
			std::cout << "magnitude: " << sign << std::endl;

			angle = sign*angle;
			return angle;
		}


		double ComputeTarget::computeTargetAngle(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<std::string> landmarkvec, LandmarkTarget targetlmk, string lmkname){
			Coord jointcenter = computeJoinntCenter(c_meta, c_fem, landmarkvec);
			std::vector<Coord> planepoints = projPointsOnPlane(jointcenter, landmarkvec, targetlmk, lmkname, c_meta, c_fem);
			double angle = angleTargetLandmark(planepoints);
			return angle;
		}

		double ComputeTarget::computeSignedTargetAngle(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<std::string> landmarkvec, LandmarkTarget targetlmk, string lmkname){
			Coord jointcenter = computeJoinntCenter(c_meta, c_fem, landmarkvec);
			std::vector<Coord> planepoints = projPointsOnPlane(jointcenter, landmarkvec, targetlmk, lmkname, c_meta, c_fem);
//			double angle = angleTargetLandmark(planepoints);
			double angle1 = angleSTargetLandmark(planepoints, landmarkvec, c_meta, c_fem);

			return angle1;
		}


		double ComputeTarget::computeJointAngle(hbm::Metadata& c_meta, hbm::FEModel& c_fem, std::vector<std::string> landmarkvec, std::string landmark_parent, std::string landmark_child){
			//std::cout << "1";
			Coord jointcenter = computeJoinntCenter(c_meta, c_fem, landmarkvec);
			//std::cout << "2";
			std::vector<Coord> planepoints = projPointsOnPlane1(jointcenter, landmarkvec, landmark_parent, landmark_child, c_meta, c_fem);
			//std::cout << "3";

			double angle = angleTargetLandmark(planepoints);
			//std::cout << "4";

			return angle;
		}

		double ComputeTarget::computeAbsoluteTwistAngle(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<std::string> target_landmarks, std::vector<std::string> reference_landmarks, std::vector<std::string> nomral_vector_landmarks){
			std::cout << "\ncomputeSpineTwistAngle called\n";
			Coord normal_vector_center = computeJoinntCenter(c_meta, c_fem, nomral_vector_landmarks);
			vector<double> nomral_vector_coords = computeNormalFromLandmarks(c_meta, c_fem, nomral_vector_landmarks);
			vectors normal_vec(nomral_vector_coords.at(0), nomral_vector_coords.at(1), nomral_vector_coords.at(2));

			//std::cout << "\t1";
			std::vector<Coord> proj_target_points = projPointsOnPlane1(normal_vector_center, nomral_vector_landmarks, target_landmarks.at(0), target_landmarks.at(1), c_meta, c_fem);
			std::vector<Coord> proj_reference_points = projPointsOnPlane1(normal_vector_center, nomral_vector_landmarks, reference_landmarks.at(0), reference_landmarks.at(1), c_meta, c_fem);

			double a, b, c;
			Coord vec_i, vec_f;

			//std::cout << "\t2";
			a = proj_target_points.at(1)[0] - proj_target_points.at(2)[0];
			b = proj_target_points.at(1)[1] - proj_target_points.at(2)[1];
			c = proj_target_points.at(1)[2] - proj_target_points.at(2)[2];
			vectors proj_target_vec(a, b, c);
			vec_i << a, b, c;

			//std::cout << "\nTarget vector projected = " << a << "," << b << "," << c << "\t3";
			a = proj_reference_points.at(1)[0] - proj_reference_points.at(2)[0];
			b = proj_reference_points.at(1)[1] - proj_reference_points.at(2)[1];
			c = proj_reference_points.at(1)[2] - proj_reference_points.at(2)[2];
			vectors proj_reference_vec(a, b, c);
			vec_f << a, b, c;

			double magnitude = 0, sign = 0;

			//std::cout << "\nReference vector projected = " << a << "," << b << "," << c << "\t4";
			/*if (vec_i.isApprox(Coord::Zero()) || vec_f.isApprox(Coord::Zero()))
			magnitude = 0;
			else
			magnitude = proj_target_vec.angle(proj_reference_vec);
			*/

			magnitude = proj_target_vec.angle(proj_reference_vec);
			if (!std::isnormal(magnitude))
				magnitude = 0;
			else
				magnitude *= 1.15;

			//std::cout << "\t5";
			vectors crossvec = proj_target_vec.cross(proj_reference_vec);


			//std::cout << "\t6";
			sign = crossvec.unit().dot(normal_vec.unit());
			if (!std::isnormal(sign))
				sign = 0;
			if (sign)
				std::cout << "\n\tABSOLUTE ANGLE\n-----------------Sign Calculated = " << sign << std::endl;
			std::cout << "\n-----------------Angle Calculated = " << magnitude << std::endl;

			return sign*magnitude;;
		}

		double ComputeTarget::computeTwistAngleForTarget(hbm::Metadata c_meta, hbm::FEModel c_fem, std::vector<double> target1__coords, std::vector<double> target2__coords, std::vector<std::string> reference_landmarks, std::vector<std::string> nomral_vector_landmarks){
			SpaceTransform sptrans;
			std::cout << "\n\t\tcomputeTwistAngleForTarget() called\n";

			Coord normal_vector_center = computeJoinntCenter(c_meta, c_fem, nomral_vector_landmarks);
			vector<double> normal_vector_center_coords = { normal_vector_center[0], normal_vector_center[1], normal_vector_center[2] };

			vector<double> nomral_vector_coords = computeNormalFromLandmarks(c_meta, c_fem, nomral_vector_landmarks);
			vectors normal_vec(nomral_vector_coords.at(0), nomral_vector_coords.at(1), nomral_vector_coords.at(2));

			//std::cout << "\n1\n******Projecting reference points ";
			std::vector<Coord> proj_reference_points = projPointsOnPlane1(normal_vector_center, nomral_vector_landmarks, reference_landmarks.at(0), reference_landmarks.at(1), c_meta, c_fem);

			std::vector<Coord> proj_target_points;
			//std::cout << "\n******Proejcting target points";
			//std::cout << "\n**** Target1_coords = " << target1__coords.at(0) << "," << target1__coords.at(1) << "," << target1__coords.at(2);
			Coord projTargetCoord = sptrans.pointProjectionOnPlane(nomral_vector_coords, target1__coords, normal_vector_center_coords);
			//std::cout << "\nProj T1 : " << projTargetCoord[0] << "\t" << projTargetCoord[1] << "\t" << projTargetCoord[2];
			proj_target_points.push_back(projTargetCoord);

			//std::cout << "\n**** Target2_coords = " << target2__coords.at(0) << "," << target2__coords.at(1) << "," << target2__coords.at(2);
			projTargetCoord = sptrans.pointProjectionOnPlane(nomral_vector_coords, target2__coords, normal_vector_center_coords);
			proj_target_points.push_back(projTargetCoord);
			//std::cout << "\nProj T2 : " << projTargetCoord[0] << "\t" << projTargetCoord[1] << "\t" << projTargetCoord[2];




			double a, b, c;
			Coord vec_i, vec_f;

			//std::cout << "\t*****2";
			a = proj_target_points.at(0)[0] - proj_target_points.at(1)[0];
			b = proj_target_points.at(0)[1] - proj_target_points.at(1)[1];
			c = proj_target_points.at(0)[2] - proj_target_points.at(1)[2];
			vectors proj_target_vec(a, b, c);
			//std::cout << "\n*****Target vector projected = " << a << "," << b << "," << c;
			vec_i << a, b, c;


			a = proj_reference_points.at(1)[0] - proj_reference_points.at(2)[0];
			b = proj_reference_points.at(1)[1] - proj_reference_points.at(2)[1];
			c = proj_reference_points.at(1)[2] - proj_reference_points.at(2)[2];
			vectors proj_reference_vec(a, b, c);
			//std::cout << "\n*****Reference vector projected = " << a << "," << b << "," << c;
			vec_f << a, b, c;

			double magnitude = 0, sign = 0;

			magnitude = proj_target_vec.angle(proj_reference_vec);
			if (!std::isnormal(magnitude))
				magnitude = 0;
			else
				magnitude *= 1.15;

			//std::cout << "\t******5";
			vectors crossvec = proj_target_vec.cross(proj_reference_vec);


			//std::cout << "\t*******6";
			sign = crossvec.unit().dot(normal_vec.unit());
			if (!std::isnormal(sign))
				sign = 0;
			else if (sign > 0)
				sign = 1;
			else if (sign < 0)
				sign = -1;

			
			std::cout << "\n\tTARGET  ANGLE\n-----------------Sign Calculated = " << sign << std::endl;
			std::cout << "\n-----------------Angle Calculated = " << magnitude << std::endl;

			return sign*magnitude;;

		}
		

	}
}
