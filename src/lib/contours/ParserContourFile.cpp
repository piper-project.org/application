/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParserContourFile.h"

#ifndef XMLCheckResult
#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult); }
#endif
using namespace std;
using namespace piper::hbm;
using namespace tinyxml2;
#include "HbmSkeleton.h"
#include "hbm/Contour.h"
#include "ParserContourFile.h"
namespace piper {
	namespace contours {

		//HbmSkeleton skele;
		Joint* love;

		ParserContourFile::ParserContourFile() {
		}

		ParserContourFile::~ParserContourFile()
		{
		}




		void ParserContourFile::WriteContourXml(std::string filename, hbm::Metadata& c_meta){

			XMLDocument ContourFullBodyXml;
			XMLNode * hbmContours = ContourFullBodyXml.NewElement("hbmContours");
			ContourFullBodyXml.InsertFirstChild(hbmContours);

			XMLElement * bodyRegion = ContourFullBodyXml.NewElement("BodyRegion");
			bodyRegion->SetAttribute("brid", 1);
			for (int i = 0; i < c_meta.m_hbm_contours.Right_Ankle_to_Calf.size(); i++){
				XMLElement * contour = ContourFullBodyXml.NewElement("contour");
				contour->SetAttribute("contid", c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(i)->contid);
				contour->SetAttribute("seqno", i);
				for (int j = 0; j < c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(j)->contour_points.size(); j++){
					XMLElement * node = ContourFullBodyXml.NewElement("node");
					XMLElement * piperid = ContourFullBodyXml.NewElement("piperid");
					piperid->SetText(c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(j)->contour_points.at(j)->getId());
					XMLElement * xcord = ContourFullBodyXml.NewElement("xcord");
					xcord->SetText(c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(j)->contour_points.at(j)->getId());
					XMLElement * ycord = ContourFullBodyXml.NewElement("ycord");
					ycord->SetText(c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(j)->contour_points.at(j)->getId());
					XMLElement * zcord = ContourFullBodyXml.NewElement("zcord");
					zcord->SetText(c_meta.m_hbm_contours.Right_Ankle_to_Calf.at(j)->contour_points.at(j)->getId());
					hbmContours->InsertEndChild(node);
					hbmContours->InsertEndChild(piperid);
					hbmContours->InsertEndChild(xcord);
					hbmContours->InsertEndChild(ycord);
					hbmContours->InsertEndChild(zcord);
				}
					hbmContours->InsertEndChild(contour);
			}
			hbmContours->InsertEndChild(bodyRegion);
			XMLError eResult = ContourFullBodyXml.SaveFile("SavedData.xml");

		}

		void ParserContourFile::WriteContourExampleXml(){
			/// Creating an XMLDocument
			tinyxml2::XMLDocument xmlDoc;
			tinyxml2::XMLNode * pRoot = xmlDoc.NewElement("Root");
			xmlDoc.InsertFirstChild(pRoot);

			///Putting data into an XMLDocument
			tinyxml2::XMLElement * pElement = xmlDoc.NewElement("IntValue");
			pElement->SetText(10);
			pRoot->InsertEndChild(pElement);
			pElement = xmlDoc.NewElement("FloatValue");
			pElement->SetText(0.5f);
			pRoot->InsertEndChild(pElement);
			pElement = xmlDoc.NewElement("Date");
			pElement->SetAttribute("day", 26);
			pElement->SetAttribute("month", "April");
			pElement->SetAttribute("year", 2014);
			pElement->SetAttribute("dateFormat", "26/04/2014");

			pRoot->InsertEndChild(pElement);
			///Saving the XMLDocument to an XML file
			tinyxml2::XMLError eResult = xmlDoc.SaveFile("SavedData.xml");
		}

		void ParserContourFile::WriteContourXml(hbm::Metadata& c_meta){
		}

		void ParserContourFile::ReadHbmSkeleton(){
			XMLDocument xmlDoc;
			XMLError eResult = xmlDoc.LoadFile("SavedData.xml");
			XMLCheckResult(eResult);

			//extracting data
			XMLNode * pRoot = xmlDoc.FirstChild();
			if (pRoot == nullptr) cout << XML_ERROR_FILE_READ_ERROR;

			XMLElement * pElement = pRoot->FirstChildElement("bodyRegion");
			if (pElement == nullptr) cout <<  XML_ERROR_PARSING_ELEMENT;

			XMLElement * pElement1 = pElement->FirstChildElement("name");
			if (pElement1 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			string text;
			text = pElement1->GetText();
			cout << text;

			pElement1 = pElement->FirstChildElement("children");
			if (pElement1 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			int children;
			pElement1->QueryIntText(&children);
			cout << children;

			pElement1 = pElement->FirstChildElement("joint");
			if (pElement1 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;


			XMLElement * pElement2 = pElement1->FirstChildElement("name");
			if (pElement2 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			text = pElement2->GetText();
			cout << text;
			
			pElement2 = pElement1->FirstChildElement("children");
			if (pElement2 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			pElement2->QueryIntText(&children);
			cout << children;

			XMLElement * pElement3 = pElement2->FirstChildElement("bodyRegion");
			if (pElement3 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;

/*			XMLElement * pElement4 = pElement3->FirstChildElement("name");
			if (pElement4 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			text = pElement4->GetText();
			cout << text;
*/
			pElement->FirstChildElement("joint")->FirstChildElement("bodyRegion")->FirstChildElement("name");
//			if (pElement4 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
//			pElement4->QueryIntText(&children);
//			cout << children;
			cout << "finish";

			/*
			pElement4 = pElement3->FirstChildElement("joint");
			if (pElement4 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;


			XMLElement * pElement5 = pElement4->FirstChildElement("name");
			if (pElement5 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			text = pElement5->GetText();
			cout << text;

			pElement5 = pElement4->FirstChildElement("children");
			if (pElement5 == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			pElement5->QueryIntText(&children);
			cout << children;

			*/


/*			pElement = pRoot->FirstChildElement("name");
			if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
//			text = pElement->GetText();
			
			pElement = pRoot->FirstChildElement("depth");
			if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;

			pElement = pRoot->FirstChildElement("children");
			if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
			*/
//			eResult = pElement->QueryIntText(&child);
//			std::cout << child;
//			XMLCheckResult(eResult);
			/*
				pElement = pRoot->FirstChildElement("Joint");
				if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;

				XMLElement * pListElement = pElement->FirstChildElement("name");
				if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;

				pListElement = pElement->FirstChildElement("depth");
				if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;

				pListElement = pElement->FirstChildElement("children");
				if (pElement == nullptr) cout << XML_ERROR_PARSING_ELEMENT;
				*/


		}

		void ParserContourFile::ReadHbmSkeletonHierarchy(){

			skele.head = NULL;
			skele.root = NULL;
			tinyxml2::XMLDocument model;
			model.LoadFile("SavedData.xml");
			if (model.Error()) {
				std::stringstream str;
                str << "Failed to load model file: " << "SavedData.xml" << std::endl << model.ErrorStr() << std::endl;
				throw std::runtime_error(str.str().c_str());

			}

			tinyxml2::XMLElement* element;

			string brname = model.FirstChildElement("hbmSkeleton")->FirstChildElement("bodyRegion")->Attribute("name");
			cout << brname << endl;
			skele.insertFirstNode(brname);
			
			element = model.FirstChildElement("hbmSkeleton")->FirstChildElement("bodyRegion")->FirstChildElement("joint");
			while (element != nullptr) {
				string jname = model.FirstChildElement("hbmSkeleton")->FirstChildElement("bodyRegion")->FirstChildElement("joint")->Attribute("name");
				
				cout << jname << endl;
				tinyxml2::XMLElement* element1;
				element1 = element->FirstChildElement("bodyRegion");
				while (element1 != nullptr){
					brname = element1->Attribute("name");
					cout << brname << endl;
					XMLElement * element2;
					element2 = element1->FirstChildElement("joint");
					while (element2 != nullptr){
						jname = element2->Attribute("name");
						cout << jname << endl;
						XMLElement * element3;
						element3 = element2->FirstChildElement("bodyRegion");
						while (element3 != nullptr){
							brname = element3->Attribute("name");
							cout << brname << endl;
							XMLElement * element4;
							element4 = element3->FirstChildElement("joint");
							while (element4 != nullptr){
								jname = element4->Attribute("name");
								cout << jname << endl;
								XMLElement * element5;
								element5 = element4->FirstChildElement("bodyRegion");
								while (element5 != nullptr){
									brname = element5->Attribute("name");
									cout << brname << endl;
									element5 = element4->NextSiblingElement("bodyregion");
								}
								element4 = element3->NextSiblingElement("joint");
							}
							element3 = element2->NextSiblingElement("bodyRegion");
						}
						element2 = element2->NextSiblingElement("joint");
					}
					element1 = element1->NextSiblingElement("bodyRegion");
				}
				element = element->NextSiblingElement("joint");
			}

/*			element = model.FirstChildElement("hbmSkeleton")->FirstChildElement("bodyRegion")->FirstChildElement("joint")->FirstChildElement("bodyRegion");
			while (element != nullptr) {
				brname = model.FirstChildElement("hbmSkeleton")->FirstChildElement("bodyRegion")->FirstChildElement("joint")->FirstChildElement("bodyRegion")->Attribute("name");
				cout << brname << endl;
				element = element->NextSiblingElement("bodyRegion");
			}
			*/
	

			skele.printnode();

		}

		void ParserContourFile::parseJoint(tinyxml2::XMLElement* element){
			element = element->FirstChildElement("joint");
			while (element != nullptr){
				string jname = element->Attribute("name");
				cout << jname << endl;
				tinyxml2::XMLElement* el = element->FirstChildElement("depth");
				int iOutInt;
				el->QueryIntText(&iOutInt);
				parseJoint(element);
				element = element->NextSiblingElement("joint");
			}
		}


		void ParserContourFile::parserishabh(tinyxml2::XMLElement* element){
			element = element->FirstChildElement("joint");
			while (element != nullptr){
				string jname = element->Attribute("name");
				Joint* temp = new Joint();
				temp->j_name = jname;
				tinyxml2::XMLElement* el = element->FirstChildElement("depth");
				int iOutInt;
				el->QueryIntText(&iOutInt);
				parserishabh(element);
				if (skele.head != NULL) {
					temp->left = skele.head;
				}
				skele.head = temp;
				element = element->NextSiblingElement("joint");
				if (element != NULL){
					if (skele.head != NULL) {
						temp->right = skele.head;
					}
				}
			}
		}



		Joint* ParserContourFile::abcdef(tinyxml2::XMLElement* element){

			if(skele.head == NULL){
				element = element->FirstChildElement("joint");
			}

			int lef = 0;

			string jname = element->Attribute("name");
			Joint* temp = new Joint();

			tinyxml2::XMLElement* element1 = element->FirstChildElement("bodyregion");
			if (element1 != NULL){
				string bname = element1->Attribute("name");
				temp->brname = bname;
				tinyxml2::XMLElement* element2 = element1->FirstChildElement("proximal");
				string proximal = element2->Attribute("name");
				element2 = element1->FirstChildElement("distal");
				string distal = element2->Attribute("name");
				temp->proximal_landmark = proximal;
				temp->distal_landmark = distal;
				cout << temp->distal_landmark << endl;
				cout << temp->proximal_landmark << endl;
				cout << temp->brname << endl;
				element2 = element1->FirstChildElement("spline");
				if (element2 != NULL){
					temp->sp = true;
					tinyxml2::XMLElement* elem = element2->FirstChildElement("p0");
					string pt0 = elem->GetText();
					temp->joint_spline.point0 = pt0;
					cout << temp->joint_spline.point0 << endl;

					elem = element2->FirstChildElement("p1");
					string pt1 = elem->GetText();
					temp->joint_spline.point1 = pt1;
					cout << temp->joint_spline.point1 << endl;

					elem = element2->FirstChildElement("t1");
					string t1 = elem->GetText();
					temp->joint_spline.tangent1 = t1;
					cout << temp->joint_spline.tangent1 << endl;

					elem = element2->FirstChildElement("t2");
					string t2 = elem->GetText();
					temp->joint_spline.tangent2 = t2;
					cout << temp->joint_spline.tangent2 << endl;
					cout << "hey";
/*					float iOutInt;
					tinyxml2::XMLElement* el = element2->FirstChildElement("u0x");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u0x = iOutInt;
					el = element2->FirstChildElement("u0y");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u0y = iOutInt;
					el = element2->FirstChildElement("u0z");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u0z = iOutInt;
					el = element2->FirstChildElement("u1x");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u1x = iOutInt;
					el = element2->FirstChildElement("u1y");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u1y = iOutInt;
					el = element2->FirstChildElement("u1z");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.u1z = iOutInt;
					el = element2->FirstChildElement("t0x");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t1x = iOutInt;
					el = element2->FirstChildElement("t0y");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t1y = iOutInt;
					el = element2->FirstChildElement("t0z");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t1z = iOutInt;
					el = element2->FirstChildElement("t1x");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t2x = iOutInt;
					el = element2->FirstChildElement("t1y");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t2y = iOutInt;
					el = element2->FirstChildElement("t1z");
					el->QueryFloatText(&iOutInt);
					temp->joint_spline.t2z = iOutInt;
					*/
				}
				element2 = element1->FirstChildElement("entityjoint");
				if (element2 != NULL){
					temp->entityjointname = element2->GetText();
				}
				element2 = element1->FirstChildElement("circumference");
				if (element2 != NULL){
					temp->circum = element2->GetText();
				}

				element2 = element1->FirstChildElement("special_landmark1");
				if (element2 != NULL){
					temp->special_landmark1 = element2->GetText();
				}

				element2 = element1->FirstChildElement("special_landmark2");
				if (element2 != NULL){
					temp->special_landmark2 = element2->GetText();
				}

			}

			temp->j_name = jname;
//			temp->brname = bname;

			if (skele.head == NULL){
				skele.head = temp;
			}

			element = element->FirstChildElement("joint");


			temp->left = NULL;
			temp->right = NULL;
			temp->middle= NULL;
			while (element != nullptr){
				if (lef == 0)
				{
					temp->left = abcdef(element);
					cout << "lef" << temp->left->j_name << "par" << temp->j_name << endl;
					//lef++;
				}
				if (lef == 1)
				{
					temp->middle = abcdef(element);
					cout << "mid" << temp->middle->j_name << "par" << temp->j_name << endl;
					//lef++;
				}
				if (lef == 2)
				{
					temp->right = abcdef(element);
					cout << "right" << temp->right->j_name << "par" << temp->j_name << endl;
					//lef++;
				}
				lef++;
				element = element->NextSiblingElement("joint");
				
			}
			return temp;

		}


		void ParserContourFile::parseJoint2(tinyxml2::XMLElement* element, Joint& next){
			element = element->FirstChildElement("joint");
			while (element != nullptr){
				string jname = element->Attribute("name");
				cout << jname << endl;
				Joint* temp = new Joint();
				temp->j_name = jname;
				if (skele.head == NULL){
					skele.head = temp;
					parseJoint2(element, *skele.head->left);
				}
				else{
					next = *temp;
					tinyxml2::XMLElement* el = element->FirstChildElement("depth");
					int iOutInt;
					el->QueryIntText(&iOutInt);
					parseJoint2(element, *next.left);
				}
				element = element->NextSiblingElement("joint");
				/*if (element != NULL){
					if (next->right != NULL){
						next = next->right;
					}
					else{
						next = next->middle;
					}
				}*/
			}
		}


		void ParserContourFile::parseBodyRegion(tinyxml2::XMLElement* element){
			element = element->FirstChildElement("bodyRegion");
			while (element != nullptr){
				string brname = element->Attribute("name");
				cout << brname << endl;
				element = element->NextSiblingElement("bodyRegion");
			}
		}

		void ParserContourFile::parseTree(){
			skele.head = NULL;
			tinyxml2::XMLDocument model;
			model.LoadFile("hbmSkeleton1.xml");
			if (model.Error()) {
				std::stringstream str;
                str << "Failed to load model file: " << "hbmSkeleton1.xml" << std::endl << model.ErrorStr() << std::endl;
				throw std::runtime_error(str.str().c_str());

			}

			tinyxml2::XMLElement* element;
			element = model.FirstChildElement("hbmSkeleton");
			element = element->FirstChildElement("bodyRegion");
			while (element != nullptr){
				string brname = element->Attribute("name");
				cout << brname << endl;
				element = element->NextSiblingElement("bodyRegion");
			}

			element = model.FirstChildElement("hbmSkeleton")->FirstChildElement("joint");
			while (element != nullptr){
				string jname = element->Attribute("name");
//				skele.insertskeleton(jname);
				cout << jname << endl;
				element = element->NextSiblingElement("joint");
			}
//			skele.printSkeleton();
		}


		void ParserContourFile::parseHbmSkeleton(FEModel& c_fem,hbm::Metadata& c_meta){
			skele.head = NULL;
			tinyxml2::XMLDocument model;
			model.LoadFile("jointbackup.xml");
			if (model.Error()) {
				std::stringstream str;
                str << "Failed to load model file: " << "hbmSkeleton1.xml" << std::endl << model.ErrorStr() << std::endl;
				throw std::runtime_error(str.str().c_str());

			}

			tinyxml2::XMLElement* element;
			element = model.FirstChildElement("hbmSkeleton");
			std::string no = model.FirstChildElement("hbmSkeleton")->Attribute("name");
			
//			skele.insertHbmSkelHead(no);
//			skele.printHeadSkele();
//			element = element->FirstChildElement("joint");
			

//			parseJoint(element);
//			parseJoint(element);

//@			parserishabh(element);
			Joint* xe =  abcdef(element);


//			skele.finalprint();
			std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
			skele.printtreeparse(c_fem,c_meta);
//			skele.printwhole(skele.head);
//			cout << "second is " << skele.head->left->j_name << endl;
			
			/*			while (element != nullptr){
				string jname = element->Attribute("name");
				cout << jname << endl;
				parseJoint(element);
				element = element->NextSiblingElement("joint");
			}
			*/
		}



		void ParserContourFile::xmlparser(){

			XMLDocument xmlDoc;
			XMLError eResult = xmlDoc.LoadFile("testxml.xml");
			XMLCheckResult(eResult);

			XMLNode * pRoot = xmlDoc.FirstChild();
			if (pRoot == nullptr)
				cout << "error no root element" << endl;

			XMLElement * pElement = pRoot->FirstChildElement("IntValue");
			if (pElement == nullptr)
				cout << "no int value element" << endl;

			int iOutInt;
			eResult = pElement->QueryIntText(&iOutInt);
			XMLCheckResult(eResult);
			cout << iOutInt << endl;

			pElement = pRoot->FirstChildElement("FloatValue");
			if (pElement == nullptr)
				cout << "error" << endl;

			float fOutFloat;
			eResult = pElement->QueryFloatText(&fOutFloat);
			XMLCheckResult(eResult);
			cout << fOutFloat;

		}

	}
}
