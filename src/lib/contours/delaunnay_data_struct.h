/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_delaunnay_data_struct_H
#define PIPER_delaunnay_data_struct_H

#pragma warning(disable:4251) // this should eventually be removed and dealt with properly through pImpl or other means

//#define CONTOURS_EXPORT __declspec( dllexport )

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif


#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <hbm/types.h>


namespace piper {
	namespace contours {


		class point{
		public:


			//The vector list of vertices
			hbm::Id point_id;
			double p_x;
			double p_y;
			double p_z;


			//Constructor
			point();

            point(hbm::Id id, double x, double y, double z);

		};

		class CONTOURS_EXPORT tet_element{
				public:

					//Id of tet
					hbm::Id id_label;

					//The vector list of vertices
                    std::vector<hbm::Id> v_label;
					double volume;
	
					//Constructor
					tet_element();
	
                    tet_element(hbm::Id l, std::vector<hbm::Id> vertices);
			};

		class CONTOURS_EXPORT volume_cordinates{

				public:
					hbm::Id node_label;
					size_t tet_id;
					std::vector<double> v_ratio;
					volume_cordinates(hbm::Id l, size_t t_id, std::vector<double> ratios);
				};


		class CONTOURS_EXPORT tet_element_new
		{
		public:
			tet_element t;
			double zmin;
			double zmax;
			double ymin;
			double ymax;
			double xmin;
			double xmax;

			tet_element_new()
			{
			}

		};
	
	}

}
#endif

