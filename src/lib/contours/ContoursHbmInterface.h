/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_CONTOURSHBMINTERFACE_H
#define PIPER_CONTOURS_CONTOURSHBMINTERFACE_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "contours/SpaceTransform.h"

namespace piper {
	namespace contours {

		/**
		* @brief A class which involves functions which includes interactions between contours and hbm.
		* It is intended to work as an interface between contours and hbm,
		* it covers only the necessarry functionalities involving hbm contours interactions.
		*
		* @author Aditya Chhabra @date 2016 
		*/
		class CONTOURS_EXPORT ContoursHbmInterface {
		public:

			ContoursHbmInterface();
			~ContoursHbmInterface();
			SpaceTransform sptrans;

			/// <summary>
			/// Get the set of nodeids from the corresponding Landmark.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding landmark</param>
			/// <returns>A set of nodeids from corresponding Landmark.</returns>
			hbm::SId getLandmarkNodeids(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);


			/// <summary>
			/// Get the set of nodeids from the corresponding GenericMetadata.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding landmark</param>
			/// <returns>A set of nodeids from corresponding GenericMetadata.</returns>
			hbm::SId getGenericMetadataNodeids(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);


			/// <summary>
			/// Get the set of 1Delementids from the corresponding GenericMetadata.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding landmark</param>
			/// <returns>A set of 1Delement ids from corresponding GenericMetadata.</returns>
			hbm::SId getGenericMetadataElementids1d(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);


			/// <summary>
			/// Get the set of 2Delementids from the corresponding GenericMetadata.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding landmark</param>
			/// <returns>A set of 2Delement ids from corresponding GenericMetadata.</returns>
			hbm::SId getGenericMetadataElementids2d(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);


			/// <summary>
			/// Get the set of 3Delementids from the corresponding GenericMetadata.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding landmark</param>
			/// <returns>A set of 3Delement ids from corresponding GenericMetadata.</returns>
			hbm::SId getGenericMetadataElementids3D(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);

			
			/// <summary>
			/// Get the set of nodeids from the corresponding Entity.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="c_name">Name of the corresponding Entity</param>
			/// <returns>A set of nodeids from corresponding Entity.</returns>
			hbm::SId getEntityNodeids(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, std::string c_name);


			/// <summary>
			/// Write Input File For Delaunnay in .node format.
			/// </summary>
			/// <param name="filename">filename a string. Name of the file to be written in .k and .node format.</param>
			/// <param name="k_file">k_file a boolean. If true the .k file should also be written in addition to .node file.</param>
			/// <param name="m_hbm_contours">m_hbm_contours object of HBM_Contours class. Information about the initial and final contour points is contained in this object.</param>
			/// <param name="all_bones">all_bones a vector of Node. If there are bone nodes present then this param will contains all information of all bone nodes.</param>
			void writeDelaunayInput(std::string filename, bool k_file, bool modify, hbm::HBM_contours* m_hbm_contours, std::vector<hbm::Node> all_bones);

			/// <summary>
			/// Convert Node Corrdinate into a vector of double.
			/// </summary>
			/// <param name="coodinate">Node coordinate. Refers to a point</param>
			/// <returns>A vector of corresponding node coordinates.</returns>
			std::vector<double> NodeCoordVector(hbm::Node coordinate);


			/// <summary>
			/// Convert Node Corrdinate into an Eigen vectro3d.
			/// </summary>
			/// <param name="coodinate">Node coordinate. Refers to a point</param>
			/// <returns>An Eigen Vector3d of corresponding node coordinates.</returns>
			Eigen::Vector3d NodeCoordEigenVector(hbm::Node coordinate);

			hbm::Id register_new_frame(std::vector<hbm::Id>, hbm::FEModel& c_femodel);

			hbm::Coord landmarkProjectionOnPlane(std::string, hbm::FEModel fem, hbm::Metadata c_meta, std::vector<double> normal, std::vector<double> pointonplane);
		};
	}
}
#endif

