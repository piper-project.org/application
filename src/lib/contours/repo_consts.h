/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_REPOCONSTS_H
#define PIPER_REPOCONSTS_H

//#define CONTOURS_EXPORT __declspec( dllexport )

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "vector_func.h"

namespace piper {
	namespace contours {

// -------------------------------- contour types --------------------------------

//--------------------------For Full GHBMC Knee Flexion Left-------------------------------
/*
static vectors femur_flexion_axis_a=vectors(-409.632,-166.804,-58.5272);//changed   (node number: 7029659, lateral side) by Gangadhar
static vectors femur_flexion_axis_b=vectors(-420.939,-86.7701,-67.3747);//changed   (node number: 7031488, medial side)

static vectors femur_twist_axis_a=vectors(-434.66,-109.01,57.9606);//changed   (node number: 7917721)
static vectors femur_twist_axis_b=vectors(22.3876,-78.2362,1.27796);//changed  (node number: 7020724)

static vectors patella_flexion_axis_a=femur_flexion_axis_a;
static vectors patella_flexion_axis_b=femur_flexion_axis_b;

static vectors patella_twist_axis_a=vectors(-423.687,-138.618,105.626);//changed
static vectors patella_twist_axis_b=vectors(-429.368,-142.235,124.731);//changed

static vectors patella_inward_movement_axis_a=patella_twist_axis_a;
static vectors patella_inward_movement_axis_b=patella_twist_axis_b;

static vectors femur_anatomical_axis_a=vectors(-128.488,-127.918,33.927);//changed
static vectors femur_anatomical_axis_b=vectors(-289.937,-128.271,68.875);//changed



static vectors tibia_anatomical_axis_a=vectors(-692.669,-111.343,-228.595);//changed
static vectors tibia_anatomical_axis_b=vectors(-423.08,-138.72,51.689);//changed

@ Aditya 
static vectors femur_anatomical_axis_a=vectors(128.488,-127.918, -33.927);//changed (id : 7041232) near hip 
static vectors femur_anatomical_axis_b=vectors(289.937,-128.271, -68.875);//changed (id : 7036085) near knee

static vectors tibia_anatomical_axis_a=vectors(692.669,-111.343,228.595);//changed (id : 7069439) near foot
static vectors tibia_anatomical_axis_b=vectors(423.08,-138.72,-51.689);//changed (id : 7063804) near knee

static vectors femur_twist_axis_a = vectors(434.65955, -109.00997, -57.960609);//changed   (node number: 7917721)  //knee
static vectors femur_twist_axis_b = vectors(-22.387600, -78.236198, -1.277960);//changed  (node number: 7020724)  // hip


static vectors calf_contours_twist_axis_a=vectors(658.25879,	-120.98758,	212.78880);//changed  // foot
static vectors calf_contours_twist_axis_b=vectors(451.62830,	-136.33018,	-22.662910);//changed  // knee

static vectors foot_flexion_axis_a = vectors(708.2521,117.01614,256.70694) // lateral
static vectors foot_flexion_axis_b = vectors(708.2521,137.01614,256.70694) // medial

static vectors foot_abduction_axis_a = vectors(708.2521,117.01614,256.70694) // lateral
static vectors foot_abduction_axis_b = vectors(708.2521,117.01614,276.70694) // medial


static vectors femur_flexion_axis_a = vectors(-409.63199, -166.80400, 58.527199);//changed   (node number: 7029659, lateral side) by Aditya
static vectors femur_flexion_axis_b = vectors(-420.93900, -86.770103, 67.374702);//changed   (node number: 7031488, medial side)


		/*
		femur_flexion_axis_a   22893        -409.632        -166.804         58.5272
		femur_flexion_axis_b   22894        -420.939        -86.7701         67.3747
		*/


/***************Now in landmark******************/

		
		/*
static vectors femur_flexion_axis_a = vectors(409.63199, -166.80400, -58.527199);//changed   (node number: 7029659, lateral side) by Gangadhar
static vectors femur_flexion_axis_b = vectors(420.93900, -86.770103, -67.374702);//changed   (node number: 7031488, medial side)
*/



/***************Now in landmark******************/


		/*
static vectors femur_twist_axis_a = vectors(434.65955, -109.00997, -57.960609);//changed   (node number: 7917721)  //knee
static vectors femur_twist_axis_b = vectors(-22.387600, -78.236198, -1.277960);//changed  (node number: 7020724)  // hip
*/


/***************Now in landmark******************/

		/*
static vectors patella_flexion_axis_a = femur_flexion_axis_a;
static vectors patella_flexion_axis_b = femur_flexion_axis_b;
*/
/***************Now in landmark******************/

		/**************Not Used*************/
//static vectors patella_twist_axis_a = vectors(423.68701, -138.61800, -105.62600);//changed
//static vectors patella_twist_axis_b = vectors(429.36801, -142.23500, -124.73100);//changed
/**************Not Used*************/


/*
correct patella twist axis
patella_twist_axis_a        -423.687        -138.618         105.626
patella_twist_axis_b        -429.368        -142.235         124.731
*/


		/**************Not Used*************/
		/*
static vectors patella_inward_movement_axis_a=patella_twist_axis_a;
static vectors patella_inward_movement_axis_b=patella_twist_axis_b;

static vectors femur_anatomical_axis_a=vectors(128.488,-127.918,-33.927);//changed
static vectors femur_anatomical_axis_b=vectors(289.937,-128.271,-68.875);//changed

static vectors tibia_anatomical_axis_a=vectors(692.66888,	-111.34312,	228.59520);//changed
static vectors tibia_anatomical_axis_b=vectors(423.07999,	-138.72031,	-51.689079);//changed

static vectors calf_contours_twist_axis_a=vectors(658.25879,	-120.98758,	212.78880);//changed  // hip
static vectors calf_contours_twist_axis_b=vectors(451.62830,	-136.33018,	-22.662910);//changed  // knee

*/
/**************Not Used*************/


//		static vectors femur_anatomical_axis_a = vectors(128.488, -127.918, -33.927);//changed
//		static vectors femur_anatomical_axis_b = vectors(289.937, -128.271, -68.875);//changed

//		static vectors calf_contours_twist_axis_a = vectors(658.259, -120.988, 212.789);//changed
//		static vectors calf_contours_twist_axis_b = vectors(451.628, -136.33, -22.6629);//changed */

		static vectors contours_parallel_axis_a;
		static vectors contours_parallel_axis_b;



//--------------------------For Full GHBMC Knee Flexion Right-------------------------------


		/**************Not Used*************/

/*
static vectors femur_flexion_axis_a_right=vectors(-409.632,166.804,-58.5272);//changed   (node number: 7029659, lateral side) by Gangadhar
static vectors femur_flexion_axis_b_right=vectors(-420.939,86.7701,-67.3747);//changed   (node number: 7031488, medial side)

static vectors femur_twist_axis_a_right=vectors(-434.66,109.01,57.9606);//changed   (node number: 7917721)
static vectors femur_twist_axis_b_right=vectors(22.3876,78.2362,1.27796);//changed  (node number: 7020724)

static vectors patella_flexion_axis_a_right=femur_flexion_axis_a_right;
static vectors patella_flexion_axis_b_right=femur_flexion_axis_b_right;

static vectors patella_twist_axis_a_right=vectors(-423.687,138.618,105.626);//changed
static vectors patella_twist_axis_b_right=vectors(-429.368,142.235,124.731);//changed

static vectors patella_inward_movement_axis_a_right=patella_twist_axis_a_right;
static vectors patella_inward_movement_axis_b_right=patella_twist_axis_b_right;

static vectors femur_anatomical_axis_a_right=vectors(-128.488,127.918,33.927);//changed
static vectors femur_anatomical_axis_b_right=vectors(-289.937,128.271,68.875);//changed

static vectors tibia_anatomical_axis_a_right=vectors(-692.669,111.343,-228.595);//changed
static vectors tibia_anatomical_axis_b_right=vectors(-423.08,138.72,51.689);//changed

static vectors calf_contours_twist_axis_a_right=vectors(-658.259,120.988,-212.789);//changed
static vectors calf_contours_twist_axis_b_right=vectors(-451.628,136.33,22.6629);//changed
*/

/**************Not Used*************/





/********************** Knee Contour Reference Axis ***************************************/

// Add one more contour at the begining to avoid the skip nodes during the delaunay process
/*nouse*/
//				vectors femur_contours_ref_axis, forearm_contours_ref_axis_unit;
/*nouse*/
//femur_contours_ref_axis = femur_anatomical_axis_a - femur_anatomical_axis_b;
//forearm_contours_ref_axis_unit = femur_contours_ref_axis.unit();
//point p;


// Knee contour reference axes


/**************Not Used*************/
/*
static vectors Knee_C1_axis_a = vectors(393.898, -123.776, -58.449);
static vectors Knee_C1_axis_b = vectors(443.283, -131.509, -59.609);

static vectors Knee_C2_axis_a = vectors(405.114, -128.65, -55.5105);
static vectors Knee_C2_axis_b = vectors(355.79, -121.68, -59.8214);

static vectors Knee_C3_axis_a = vectors(417.886, -133.647, -50.3376);
static vectors Knee_C3_axis_b = vectors(369.532, -128.112, -61.7972);

static vectors Knee_C4_axis_a = vectors(429.382, -132.411, -41.3301);
static vectors Knee_C4_axis_b = vectors(382.791, -128.337, -59.0152);

static vectors Knee_C5_axis_a = vectors(437.471, -135.845, -26.2288);
static vectors Knee_C5_axis_b = vectors(481.096, -137.789, -1.875);
*/
/**************Not Used*************/


/********************** Knee Contour Reference Axis ***************************************/




// new femur anatomical axis , contour flexion axis , thigh contour twist axis
static vectors new_femur_anatomical_axis_a;
static vectors new_femur_anatomical_axis_b;

static vectors contours_flexion_axis_a;
static vectors contours_flexion_axis_b;

static vectors thigh_contours_twist_axis_a;
static vectors thigh_contours_twist_axis_b;

// new femur anatomical axis , contour flexion axis , thigh contour twist axis









/**************Not Used*************/

/*
// Forearm reference axis (left) for initial contours generation only
static vectors forearm_contours_ref_axis_a=vectors(53.9914,241.465,-221.6965);
static vectors forearm_contours_ref_axis_b=vectors(238.198, 333.076, -180.546);
// Arm reference axis (left) for initial contours generation only
static vectors arm_contours_ref_axis_a=vectors(-77.733, 297.9945, -220.9545);
static vectors arm_contours_ref_axis_b=vectors(-6.3269, 245.2465, -225.029);

// Glenohumeral joint rotation center (left)
//static vectors glenohumeral_rotation_c=vectors(-191.824, 445.366, -172.397);
static vectors glenohumeral_rotation_c=vectors(191.823975, -172.397049, 445.365997);

//static vectors glenohumeral_rotation_x=vectors(-189.4673589713,452.5996831573,-190.8934880732);
//static vectors glenohumeral_rotation_y=vectors(-204.5407388115,460.22291653678,-168.2069421222);
//static vectors glenohumeral_rotation_z=vectors(-176.5659952794,456.62452348316,-166.0378380326);

// Axis horizontal and passing through "glenohumeral_rotation_c"; means axis parallel to GHBMC Z-Axis
//static vectors glenohumeral_rotation_x=vectors(-191.824, 445.366, -250.0); //old
//static vectors glenohumeral_rotation_x=vectors(-100.0, 445.366, -172.397);
//static vectors glenohumeral_rotation_y=vectors(-191.824, 500.0, -172.397);
//static vectors glenohumeral_rotation_z=vectors(-191.824, 445.366, -100.0);
static vectors glenohumeral_rotation_x=vectors(99.999985, -172.397018, 445.365997);
static vectors glenohumeral_rotation_y=vectors(191.823975, -172.397064, 500.000000);
static vectors glenohumeral_rotation_z=vectors(191.824005, -100.000038, 445.365997);


//static vectors glenohumeral_rotation_z=vectors(-191.824, 445.366, -172.397);


// Forearm reference axis (right) for initial contours generation only
static vectors forearm_contours_ref_axis_a_RH=vectors(53.9914,241.465,221.6965);
static vectors forearm_contours_ref_axis_b_RH=vectors(238.198, 333.076, 180.546);
// Arm reference axis (right) for initial contours generation only
static vectors arm_contours_ref_axis_a_RH=vectors(-77.733, 297.9945, 220.9545);
static vectors arm_contours_ref_axis_b_RH=vectors(-6.3269, 245.2465, 225.029);

// Glenohumeral joint rotation center (right)
//static vectors glenohumeral_rotation_c_RH=vectors(-191.824, 445.366, 172.397);
static vectors glenohumeral_rotation_c_RH=vectors(191.824036, 172.396957, 445.365997);

// Axis horizontal and passing through "glenohumeral_rotation_c"; means axis parallel to GHBMC Z-Axis
//static vectors glenohumeral_rotation_x=vectors(-191.824, 445.366, -250.0); //old
//static vectors glenohumeral_rotation_x_RH=vectors(-100.0, 445.366, 172.397); //Abduction/Adduction
//static vectors glenohumeral_rotation_y_RH=vectors(-191.824, 500.0, 172.397); // Twisting rotation
//static vectors glenohumeral_rotation_z_RH=vectors(-191.824, 445.366, 100.0); //Flexion
static vectors glenohumeral_rotation_x_RH=vectors(100.000015, 172.396988, 445.365997); //Abduction/Adduction
static vectors glenohumeral_rotation_y_RH=vectors(191.824036, 172.396942, 500.000000); // Twisting rotation
static vectors glenohumeral_rotation_z_RH=vectors(191.824005, 99.999962, 445.365997); //Flexion

*/
/**************Not Used*************/


static const int interp_flexion_twist_SIZE=7;
static double interp_flexion_twist[2][interp_flexion_twist_SIZE]={{0,15,30,45,60,75,90},{0,5,11.1,13.9,15.4,16.6,18}};



/**************Not Used*************/

/*
static const int interp_flexion_patella_SIZE=6;
static double interp_flexion_patella[2][interp_flexion_patella_SIZE]={{0,20,40,60,80,100},{0,10,21.67,36.67,51.67,66.67}};

//change 1 - rahul
static const int interp_twist_patella_SIZE=2;
static double interp_twist_patella[2][interp_twist_patella_SIZE]={{-10,80},{0,10}};

//change 2 - rahul
static const int interp_inward_movement_patella_SIZE=3;
static double interp_inward_movement_patella[2][interp_inward_movement_patella_SIZE]={{-10,20,80},{8,8,10}};

static const int interp_multiplier_calf_scaling_SIZE=2;
static double interp_multiplier_calf_scaling[2][interp_multiplier_calf_scaling_SIZE]={{20,80},{0.95,1}};

static double swell_amount_for_delaunay=5;
*/
/**************Not Used*************/




/**************Not Used*************/

/*
static int uncommon_nodes[48]={
	9220387, 9220384 ,9220381 ,9220378 ,9220359 ,9220358 ,9220362 ,9220367 ,9220370 ,9220374 ,9220391 ,9220394 ,9220397 ,9220404 ,9220400 ,9220389 ,
	9220237 ,9220234 ,9220231 ,9220228 ,9220210 ,9220209 ,9220213 ,9220218 ,9220221 ,9220224 ,9220241 ,9220244 ,9220246 ,9220253 ,9220249 ,9220238 ,
	9220192 ,9220194 ,9220196 ,9220198 ,9220200 ,9220207 ,9220205 ,9220203 ,9220201 ,9220190 ,9220189 ,9220188 ,9220180 ,9220179 ,9220181 ,9220184
};

//contains the list of nodes (trhere labels) for LCL
static int LCL_LOWER[] ={9207336,9207337,9207339,9207341,9207335,9207334,9207338,9207340};
static int LCL_UPPER[] ={9207333,9207331,9207327,9207328,9207332,9207330,9207326,9207329};

//contains the list of nodes (trhere labels) for MCL 
static int MCL_LOWER[] ={9207576,9207577,9207579,9207581,9207583,9207575,9207574,9207578,9207580,9207582};
static int MCL_UPPER[] ={9207572,9207568,9207564,9207558,9207559,9207573,9207569,9207565,9207561,9207560};

//contains list of nodes(there labels) which form the contours in ACL
static int ACL_contour_label_list[10][9] = 
{	{9207761,9207806,9207804,9207805,9207759,9207756,9207757,9207760,9207758},
	{9207755,9207803,9207796,9207797,9207743,9207740,9207741,9207754,9207742},
	{9207753,9207802,9207794,9207795,9207739,9207736,9207737,9207752,9207738},
	{9207751,9207801,9207792,9207735,9207732,9207733,9207750,9207734,9207793},
	{9207749,9207800,9207790,9207791,9207731,9207728,9207729,9207748,9207730},
	{9207747,9207799,9207788,9207789,9207727,9207724,9207725,9207746,9207726},
	{9207745,9207798,9207786,9207787,9207723,9207722,9207720,9207721,9207744},
	{9207719,9207785,9207776,9207703,9207700,9207701,9207718,9207702,9207777},
	{9207717,9207784,9207774,9207775,9207699,9207696,9207697,9207716,9207698},
	{9207715,9207694,9207695,9207783,9207772,9207773,9207692,9207693,9207714}
};

//contains list of nodes(there labels) which form the contours in PCL
static int PCL_contour_label_list[10][9] = 
{	{9207667,9207666,9207664,9207665,9207668,9207669,9207670,9207671},
	{9207663,9207662,9207661,9207660,9207659,9207658,9207656,9207657},
	{9207654,9207655,9207653,9207652,9207651,9207650,9207649,9207648},
	{9207647,9207646,9207645,9207643,9207644,9207642,9207641,9207640},
	{9207639,9207638,9207637,9207636,9207635,9207634,9207633,9207632},
	{9207631,9207630,9207629,9207628,9207627,9207626,9207625,9207624},
	{9207623,9207622,9207621,9207620,9207619,9207618,9207617,9207616},
	{9207615,9207614,9207613,9207612,9207611,9207610,9207609,9207608},
	{9207607,9207606,9207605,9207604,9207603,9207602,9207601,9207600},
};

//Contains the sliding data (at flexion = 90 degree)for the different parts of menisci
//The sliding data at angles other tha 90 are calcultaed by linearly interpolating between 0 and 90 degrees of flexion
// The LAT_MEN_HALF_sliding_amounts[i] correspinds to LAT_MEN_HALF_NID_LISTS[i]  

static int LAT_MEN_HALF_NID_LISTS[4][11] = 
{
	 {9207129,9224228,9207127,9224236,9207125,9224244,9207123,9224252,9207121,9224260,9207119},
	 {9207113,9224230,9207111,9224238,9207109,9224246,9207107,9224254,9207105,9224262,9207103},
	 {9207094,9224232,9207090,9224240,9207086,9224248,9207082,9224256,9207078,9224264,9207074},
	 {9207095,9224234,9207091,9224242,9207087,9224250,9207083,9224258,9207079,9224268,9207075}
};

static int LAT_MEN_HALF_sliding_amounts [4] = { 1, 0.5, 0, 0.45 };
*/
/**************Not Used*************/

	
	}

}
#endif

