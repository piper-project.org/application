/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ContoursDeformation.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		ContoursDeformation::ContoursDeformation() {
		}

		ContoursDeformation::~ContoursDeformation()
		{
		}

		bool ContoursDeformation::VerifyPerpendicularContourConstraint(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, Eigen::MatrixXd axes, Eigen::MatrixXd spline_matrix){
			std::vector<Node> contour_initial_nodes;
			std::vector<Node> contour_final_nodes;
			std::vector<Node> all_bones_initial;
			std::vector<Node> all_bones_final;
			std::vector<double> plane_coeff;
			Contour* rotated_cont = new Contour;
			Contour* current_cont = new Contour;

			/**
			* Get the bezier matrix of control points from tangent cubic spline
			*/
			Eigen::MatrixXd bezier_cont_pts(4, 3);
			Eigen::MatrixXd pointx(3, 1);
			Eigen::MatrixXd pointy(3, 1);
			Eigen::MatrixXd pointz(3, 1);
			bezier_cont_pts = c_transform.convertSplineToBezier(spline_matrix);
            delete rotated_cont;
            delete current_cont;
            return true; // TODO Implement the rest of the function and return correct value
		}

		void ContoursDeformation::prePopulateHingeJointContours(std::vector<Contour*> A, std::vector<Contour*> B, std::vector<Contour*> J1, std::vector<Contour*> J2, hbm::Metadata& c_meta){
			c_meta.m_hbm_contours.joint_contours_a = A; /// Contours at fixed side
			c_meta.m_hbm_contours.joint_contours_b = B; /// Contours at moving side
			c_meta.m_hbm_contours.joint_contours_j1 = J1; /// Contours of joint near fixed side
			c_meta.m_hbm_contours.joint_contours_j2 = J2; /// Contours of joint near moving side
		}

		double ContoursDeformation::uValueOfContour(std::vector<double> abcd, Eigen::MatrixXd bezier_control_points) {

			double aod, bod, cod;
			aod = abcd.at(0) / abcd.at(3);
			bod = abcd.at(1) / abcd.at(3);
			cod = abcd.at(2) / abcd.at(3);

			Eigen::Matrix4d N; // Bezier Matrix
			N << -1, 3, -3, 1  //N
				, 3, -6, 3, 0
				, -3, 3, 0, 0
				, 1, 0, 0, 0;

			Eigen::MatrixXd Bx(4, 1);
			Eigen::MatrixXd By(4, 1);
			Eigen::MatrixXd Bz(4, 1);

			Bx << bezier_control_points(0, 0)
				, bezier_control_points(1, 0)
				, bezier_control_points(2, 0)
				, bezier_control_points(3, 0);

			By << bezier_control_points(0, 1)
				, bezier_control_points(1, 1)
				, bezier_control_points(2, 1)
				, bezier_control_points(3, 1);

			Bz << bezier_control_points(0, 2)
				, bezier_control_points(1, 2)
				, bezier_control_points(2, 2)
				, bezier_control_points(3, 2);

			Eigen::MatrixXd Ax, Ay, Az;

			Ax = abcd.at(0)*N*Bx;
			Ay = abcd.at(1)*N*By;
			Az = abcd.at(2)*N*Bz;

			double u3, u2, u1, u0;

			u3 = Ax(0, 0) + Ay(0, 0) + Az(0, 0);
			u2 = Ax(1, 0) + Ay(1, 0) + Az(1, 0);
			u1 = Ax(2, 0) + Ay(2, 0) + Az(2, 0);
			u0 = Ax(3, 0) + Ay(3, 0) + Az(3, 0);

			u0 = u0 + abcd.at(3);

			int result;
			double x11 = u2 / u3;
			double x22 = u1 / u3;
			double x33 = u0 / u3;
			double q[3];

			result = SolveP3(q, x11, x22, x33);
			std::cout << "q0" << q[0] << "   q1" << q[1] << "   q2" << q[2] << endl;
			/*			if (result == 1 && q[0] <= 1 && q[0] >= 0){
			std::cout << q[0] << "q0 " << std::endl;
			return q[0];
			}
			else if(result == 3){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			else if (q[1] <= 1 && q[1] >= 0){
			return q[1];
			}
			else if (q[2] <= 1 && q[2] >= 0){
			return q[2];
			}
			else{
			return q[0];
			}
			}
			else{
			return 50;
			}
			std::cout << q[0] << endl;
			if (result == 1){
			if (q[0] <= 1 && q[0] >= 0){
			return q[0];
			}
			}


			}
			*/

			if (result == 1){
				return q[0];
				std::cout << q[0] << "q0 " << std::endl;
			}
			else if (result == 3){
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else if (q[2] <= 1 && q[2] >= 0){
					std::cout << q[2] << "q2 " << std::endl;
					return q[2];
				}
				else{
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
			}
			else{
				if (q[0] <= 1 && q[0] >= 0){
					std::cout << q[0] << "q0 " << std::endl;
					return q[0];
				}
				else if (q[1] <= 1 && q[1] >= 0){
					std::cout << q[1] << "q1 " << std::endl;
					return q[1];
				}
				else{
					return q[0];
				}
			}


		}

		void ContoursDeformation::RepositionHingeJointContours(double flexion_angle, hbm::FEModel& c_femodel, hbm::Metadata& c_meta, Eigen::MatrixXd axes, Eigen::MatrixXd spline_matrix){
			size_t size;
			std::vector<Node> contour_initial_nodes;
			std::vector<Node> contour_final_nodes;
			std::vector<Node> all_bones_initial;
			std::vector<Node> all_bones_final;
			std::vector<double> plane_coeff;
			Contour* rotated_cont = new Contour;
			Contour* current_cont = new Contour;
			double tvalue;
			vectors flexion_axis_a(axes(0, 0), axes(0, 1), axes(0, 2));
			vectors flexion_axis_b(axes(1, 0), axes(1, 1), axes(1, 2));
			/**
			* Get the bezier matrix of control points from tangent cubic spline
			*/
			Eigen::MatrixXd bezier_cont_pts(4, 3);
			Eigen::MatrixXd pointx(3, 1);
			Eigen::MatrixXd pointy(3, 1);
			Eigen::MatrixXd pointz(3, 1);
			bezier_cont_pts = c_transform.convertSplineToBezier(spline_matrix);
			/**
			* Step 1
			* Rotation of Contours at fixed side "A"
			* All contours in initial state are saved in initial_contour_points member variable of contour class
			* Transform A region contour by Flexion angle
			* All contours in final state are saved in final_contour_points member variable of contour class
			*/
			size = c_meta.m_hbm_contours.joint_contours_a.size();		/*!< Size of "A" side Contours */
			for (int j = 0; j < size; j++){
				vector<NodePtr> cur_contour;
				cur_contour = c_meta.m_hbm_contours.joint_contours_a.at(j)->contour_points;
				for (std::vector<NodePtr>::iterator it = cur_contour.begin(); it != cur_contour.end(); ++it) {
					contour_initial_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_a.at(j)->initial_contour_points.push_back(**it);
				}

				for (std::vector<NodePtr>::iterator it = cur_contour.begin(); it != cur_contour.end(); ++it) {
					contour_final_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_a.at(j)->final_contour_points.push_back(**it);
				}
			}

			/**
			* Step 2
			* Rotation of Contours near fixed side "J1"
			* All contours in initial state are saved in initial_contour_points member variable of contour class
			* Transform (rotate) J1 region contour by Flexion angle
			* All contours in final state are saved i n final_contour_points member variable of contour class
			*/

			size = c_meta.m_hbm_contours.joint_contours_j1.size();		/*!< Size of Left Foot Contours */
			for (int j = 0; j < size; j++){
				current_cont = c_meta.m_hbm_contours.joint_contours_j1.at(j);
				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_initial_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_j1.at(j)->initial_contour_points.push_back(**it);
				}

				plane_coeff = ContourPlaneABCD(*c_meta.m_hbm_contours.joint_contours_j1.at(j));
				tvalue = uValueOfContour(plane_coeff, bezier_cont_pts);
				double centx = c_meta.m_hbm_contours.joint_contours_j1.at(j)->centroid.getCoordX();
				double centy = c_meta.m_hbm_contours.joint_contours_j1.at(j)->centroid.getCoordY();
				double centz = c_meta.m_hbm_contours.joint_contours_j1.at(j)->centroid.getCoordZ();
				vectors contours_parallel_axis_a1(centx, centy, centz);
				vectors contours_parallel_axis_b1 = contours_parallel_axis_a1 + flexion_axis_b - flexion_axis_a;

				current_cont = c_meta.m_hbm_contours.joint_contours_j1.at(j);
				rotated_cont = rotateContour(current_cont, tvalue*flexion_angle, axes);
				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_final_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_j1.at(j)->final_contour_points.push_back(**it);
				}
			}

			/**
			* Step 3
			* Rotation of Contours near moving side "J2"
			* All contours in initial state are saved in initial_contour_points member variable of contour class
			* Transform (rotate) J2 region contour by Flexion angle
			* All contours in final state are saved in final_contour_points member variable of contour class
			*/
			size = c_meta.m_hbm_contours.joint_contours_j2.size();		/*!< Size of Left Foot Contours */
			for (int j = 0; j<size; j++){
				current_cont = c_meta.m_hbm_contours.joint_contours_j2.at(j);
				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_initial_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_j2.at(j)->initial_contour_points.push_back(**it);
				}
				plane_coeff = ContourPlaneABCD(*c_meta.m_hbm_contours.joint_contours_j2.at(j));
				tvalue = uValueOfContour(plane_coeff, bezier_cont_pts);
				double centx = c_meta.m_hbm_contours.joint_contours_j2.at(j)->centroid.getCoordX();
				double centy = c_meta.m_hbm_contours.joint_contours_j2.at(j)->centroid.getCoordY();
				double centz = c_meta.m_hbm_contours.joint_contours_j2.at(j)->centroid.getCoordZ();
				vectors contours_parallel_axis_a1(centx, centy, centz);
				vectors contours_parallel_axis_b1 = contours_parallel_axis_a1 + flexion_axis_b - flexion_axis_a;

				rotated_cont = rotateContour(current_cont, flexion_angle, axes);
				double angle_corr = flexion_angle - tvalue*flexion_angle;
				rotated_cont = rotateContour(current_cont, -angle_corr, axes);

				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_final_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_j2.at(j)->final_contour_points.push_back(**it);
				}
				c_meta.m_hbm_contours.joint_contours_j2.at(j) = rotated_cont;
			}

			/**
			* Step 4
			* Rotation of Contours at moving side "B"
			* All contours in initial state are saved in initial_contour_points member variable of contour class
			* Transform (rotate) "B" region contour by Flexion angle
			* All contours in final state are saved in final_contour_points member variable of contour class
			*/
			size = c_meta.m_hbm_contours.joint_contours_b.size();		/*!< Size of Joint Contours at moving region "B" */
			for (int j = 0; j<size; j++){
				current_cont = c_meta.m_hbm_contours.joint_contours_b.at(j);
				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_initial_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_b.at(j)->initial_contour_points.push_back(**it);
				}
				plane_coeff = ContourPlaneABCD(*c_meta.m_hbm_contours.joint_contours_j2.at(j));
				tvalue = uValueOfContour(plane_coeff, bezier_cont_pts);
				rotated_cont = rotateContour(current_cont, flexion_angle,axes);
				for (std::vector<NodePtr>::iterator it = current_cont->contour_points.begin(); it != current_cont->contour_points.end(); ++it) {
					contour_final_nodes.push_back(**it);
					c_meta.m_hbm_contours.joint_contours_b.at(j)->final_contour_points.push_back(**it);
				}
				c_meta.m_hbm_contours.joint_contours_b.at(j) = rotated_cont;
			}

			/**
			* Step 5
			* Write Delaunnay Input File in .node format for initial and final contour points
			* all_bones_initial and all_bones_final contains nodes of bone if present
			*/

			c_hbm_interface.writeDelaunayInput("initial_contour.k", true, false, &c_meta.m_hbm_contours, all_bones_initial);
			c_hbm_interface.writeDelaunayInput("final_contour.k", true, true, &c_meta.m_hbm_contours, all_bones_final);
			/// clearing of all vectors

		}

		hbm::Contour* ContoursDeformation::rotateContour(hbm::Contour* c_contour, double theta, Eigen::MatrixXd axis)
		{
			for (unsigned int i = 0; i<c_contour->contour_points.size(); i++)
			{
				Eigen::Vector3d coordinate = c_hbm_interface.NodeCoordEigenVector(*c_contour->contour_points.at(i));
				coordinate = c_transform.ApplyRotation3d(coordinate, theta, axis);
				c_contour->contour_points.at(i)->getCoordX() = coordinate(0, 0);
				c_contour->contour_points.at(i)->getCoordY() = coordinate(1, 0);
				c_contour->contour_points.at(i)->getCoordZ() = coordinate(2, 0);
			}
			return c_contour;
		}

		double ContoursDeformation::PreprocessHingeJointAngle(double flexion_angle, double initial_angle){
			flexion_angle = initial_angle - flexion_angle;
			return flexion_angle;
		}

		std::vector<double> ContoursDeformation::ContourPlaneABCD(Contour cont){
			std::vector<double> abcd;
			vector<NodePtr> plane_temp;
			cout << "size " << cont.contour_points.size() << endl;
			cont.get_points_for_plane();
			plane_temp = cont.plane_points;	/// member function of Contours class returns a vector of pointer to three nodes
			///\todo have another eigen vector conversion function for pointer to nodes
			/*
			for (std::vector<NodePtr>::iterator it = plane_temp.begin(); it != plane_temp.end(); ++it) {
				c_hbm_interface.NodeCoordEigenVector(it);
			}
			*/
			Eigen::MatrixXd node_matrix(3, 3);
			for (std::vector<NodePtr>::size_type i = 0; i != plane_temp.size(); i++) {
				Eigen::Vector3d node_vec;
				node_vec = c_hbm_interface.NodeCoordEigenVector(*plane_temp[i]);
				///\todo make it more simpler in just one step
				node_matrix(i, 0) = node_vec(0, 0);
				node_matrix(i, 1) = node_vec(1, 0);
				node_matrix(i, 2) = node_vec(2, 0);
			}
			abcd = c_transform.getPlaneFromPoints(node_matrix);
			return abcd;
		}

		void ContoursDeformation::preProcessHingeJointContours(hbm::Metadata& c_meta){
		}
     

	}
}
