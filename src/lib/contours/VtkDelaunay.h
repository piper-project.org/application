/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOUR_VTKDELAUNAY_H
#define PIPER_CONTOUR_VTKDELAUNAY_H

/** @brief 	A class for using VTK Delaunay Implementation for Repositioning.
	*
	* @author Sukhraj Singh @date 2016
	*/
#include "hbm/HumanBodyModel.h"

#include "delaunnay_data_struct.h"

namespace piper {
    namespace contours {
    class VtkDelaunay {
        public:
            VtkDelaunay();
	        //Set VTK Unstructured Grid of points
	        void SetDataForDelaunay(const std::vector<hbm::Node>& node_list);
	        void ComputeDelaunay();
            void GetTets(std::vector<tet_element>& vtk_tet_list);

            private:
                vtkSmartPointer<vtkUnstructuredGrid> delaunay_input_mesh;
                vtkSmartPointer<vtkUnstructuredGrid> delaunay_output_mesh;

        };
    }
 
}
#endif//PIPER_CONTOUR_VTKDELAUNAY_H
