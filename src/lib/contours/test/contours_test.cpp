/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "contours/main_repositioning.h"
#include "contours/ContoursDeformation.h"
#include "contours/ParserContourFile.h"

#include "gtest/gtest.h"

using namespace piper::hbm;
using namespace piper::contours;

std::unique_ptr<HumanBodyModel> hbm_ptr;

class ContoursTest : public ::testing::Test{

public:
    ContoursTest() : repo(".") // note - this probably works only because tetgen is not called by any of the tests. if that changes in the future, this needs to be updated to correct path
    {}

	protected:  // either public or protected cannot be private
		MainRepositioning repo;
		ContoursDeformation cont_deform;
		ContoursHbmInterface c_test_hbm_interface;
		size_t size_RightFoot;                   /// body_region = 1
		size_t size_LeftFoot;                    /// body_region = 2
		size_t	size_Right_Ankle_to_Calf;		  /// body_region = 3
		size_t	size_Left_Ankle_to_Calf;	      /// body_region = 4
		size_t	size_Right_Knee_lower;			  /// body_region = 5
		size_t	size_Left_Knee_lower;			  /// body_region = 6
		size_t	size_Right_Knee_upper;			  /// body_region = 7
		size_t	size_Left_Knee_upper;			  /// body_region = 8
		size_t	size_Right_Thigh;				  /// body_region = 9
		size_t	size_Left_Thigh;				  /// body_region = 10
		size_t	size_Hip;						  /// body_region = 11
		size_t	size_Abdomen;					  /// body_region = 12
		size_t	size_Thorax;				      /// body_region = 13
		size_t	size_Neck;						  /// body_region = 14
		size_t size_Head;						  /// body_region = 15
		size_t	size_Right_Palm;				  /// body_region = 16
		size_t	size_Left_Palm;					  /// body_region = 17
		size_t	size_Right_Forearm;				  /// body_region = 18
		size_t	size_Left_Forearm;				  /// body_region = 19
		size_t	size_Right_Elbow_lower;			  /// body_region = 20
		size_t	size_Left_Elbow_lower;			  /// body_region = 21
		size_t	size_Right_Elbow_upper;			  /// body_region = 22
		size_t	size_Left_Elbow_upper;			  /// body_region = 23
		size_t	size_Right_Biceps;				  /// body_region = 24
		size_t	size_Left_Biceps;				  /// body_region = 25
		size_t	size_Right_biceps_to_shoulder;	  /// body_region = 26
		size_t	size_Left_biceps_to_shoulder;	  /// body_region = 27
		size_t	size_Hip_right;   				  /// body_region = 28
		size_t	size_Hip_left;   				  /// body_region = 29
		size_t	size_Keypoints;   				  /// body_region = 30
		size_t	size_Right_Abdomen;   		      /// body_region = 31
		size_t	size_Left_Abdomen;   			  /// body_region = 32
		size_t	size_Right_Thorax;   			  /// body_region = 33
		size_t	size_Left_Thorax;   			  /// body_region = 34
		std::vector<double> c_test_abcd;
		Eigen::Vector3d node_vec;
		virtual void SetUp() {
			contour_detail cont_map;
			std::vector<NodePtr> vec_spline_new;
			m_hbm = repo.readContour("contours.txt", &cont_map.contour_map, &vec_spline_new);
			size_RightFoot = m_hbm.Right_Foot.size();
			size_LeftFoot = m_hbm.Left_Foot.size();
			size_Right_Ankle_to_Calf = m_hbm.Right_Ankle_to_Calf.size();
			size_Left_Ankle_to_Calf = m_hbm.Left_Ankle_to_Calf.size();
			size_Left_Knee_lower = m_hbm.Left_Knee_lower.size();
			size_Right_Knee_lower = m_hbm.Right_Knee_lower.size();
			size_Left_Knee_upper = m_hbm.Right_Knee_upper.size();
			size_Right_Knee_upper = m_hbm.Right_Knee_upper.size();
			size_Right_Palm = m_hbm.Right_Palm.size();
			size_Left_Palm = m_hbm.Left_Palm.size();
			size_Left_Abdomen = m_hbm.Left_Abdomen.size();
			size_Right_Abdomen = m_hbm.Right_Abdomen.size();
			size_Hip = m_hbm.Hip.size();
			size_Abdomen = m_hbm.Abdomen.size();
			size_Thorax = m_hbm.Thorax.size();
			size_Neck = m_hbm.Neck.size();
			size_Head = m_hbm.Head.size();
			size_Left_Forearm = m_hbm.Left_Forearm.size();
			size_Right_Forearm = m_hbm.Right_Forearm.size();
			size_Right_Elbow_lower = m_hbm.Right_Elbow_lower.size();
			size_Left_Elbow_lower = m_hbm.Left_Elbow_lower.size();
			size_Right_Elbow_upper = m_hbm.Right_Elbow_upper.size();
			size_Left_Elbow_upper = m_hbm.Left_Elbow_upper.size();
			size_Right_Biceps = m_hbm.Right_Biceps.size();
			size_Left_Biceps = m_hbm.Left_Biceps.size();
			size_Left_biceps_to_shoulder = m_hbm.Left_biceps_to_shoulder.size();
			size_Right_biceps_to_shoulder = m_hbm.Right_biceps_to_shoulder.size();
			size_Hip_left = m_hbm.Hip_left.size();
			size_Hip_right = m_hbm.Hip_right.size();
			size_Left_Thorax = m_hbm.Left_Thorax.size();
			size_Right_Thorax = m_hbm.Right_Thorax.size();
			size_Left_Abdomen = m_hbm.Left_Abdomen.size();
			size_Right_Abdomen = m_hbm.Right_Abdomen.size();
			c_test_abcd = cont_deform.ContourPlaneABCD(*m_hbm.Right_Thigh.at(1));
			node_vec = c_test_hbm_interface.NodeCoordEigenVector(*m_hbm.Right_Thigh.at(1)->contour_points.at(1));
			//			size_RightFoot = 5;
			}
		virtual void TearDown(){

		}
		HBM_contours m_hbm;
};

// Test with Test Fixture

TEST_F(ContoursTest, test_m_hbm_contours_size){
	/// ContoursTest : test case name (test fixture class name)
	/// test_m_hbm_contours_size : test name
	EXPECT_EQ(15, size_RightFoot);
	EXPECT_EQ(15, size_LeftFoot);
	EXPECT_EQ(62, size_Right_Ankle_to_Calf);
	EXPECT_EQ(62, size_Left_Ankle_to_Calf);
	EXPECT_EQ(5, size_Right_Knee_lower);
	EXPECT_EQ(5, size_Left_Knee_lower);
	EXPECT_EQ(0, size_Left_Knee_upper);
	EXPECT_EQ(0, size_Right_Knee_upper);
	EXPECT_EQ(0, size_Left_Palm);
	EXPECT_EQ(0, size_Right_Palm);
	EXPECT_EQ(7, size_Left_Abdomen);
	EXPECT_EQ(7, size_Right_Abdomen);
	EXPECT_EQ(13, size_Hip);
	EXPECT_EQ(0, size_Abdomen);
	EXPECT_EQ(0, size_Thorax);
	EXPECT_EQ(4, size_Neck);
	EXPECT_EQ(21, size_Head);
	EXPECT_EQ(28, size_Left_Forearm);
	EXPECT_EQ(28, size_Right_Forearm);
	EXPECT_EQ(3, size_Left_Elbow_lower);
	EXPECT_EQ(3, size_Right_Elbow_lower);
	EXPECT_EQ(0, size_Left_Elbow_upper);
	EXPECT_EQ(0, size_Right_Elbow_upper);
	EXPECT_EQ(10, size_Right_Biceps);
	EXPECT_EQ(10, size_Left_Biceps);
	EXPECT_EQ(14, size_Left_biceps_to_shoulder);
	EXPECT_EQ(14, size_Right_biceps_to_shoulder);
	EXPECT_EQ(0, size_Hip_left);
	EXPECT_EQ(0, size_Hip_right);
	EXPECT_EQ(9, size_Left_Thorax);
	EXPECT_EQ(9, size_Right_Thorax);
	EXPECT_EQ(7, size_Left_Abdomen);
	EXPECT_EQ(7, size_Right_Abdomen);
}

TEST_F(ContoursTest, test_m_hbm_contours_empty){
	EXPECT_TRUE(m_hbm.Left_Ankle_to_Calf.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Ankle_to_Calf.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Foot.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Foot.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Knee_lower.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Knee_lower.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Knee_upper.size() == 0);
	EXPECT_TRUE(m_hbm.Left_Knee_upper.size() == 0);
	EXPECT_TRUE(m_hbm.Left_Thigh.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Thigh.size() != 0);
	EXPECT_TRUE(m_hbm.Hip.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Abdomen.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Abdomen.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Thorax.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Thorax.size() != 0);
	EXPECT_TRUE(m_hbm.Abdomen.size() == 0);
	EXPECT_TRUE(m_hbm.Thorax.size() == 0);
	EXPECT_TRUE(m_hbm.Neck.size() != 0);
	EXPECT_TRUE(m_hbm.Head.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Palm.size() == 0);
	EXPECT_TRUE(m_hbm.Left_Palm.size() == 0);
	EXPECT_TRUE(m_hbm.Right_Forearm.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Forearm.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Elbow_lower.size() != 0);
	EXPECT_TRUE(m_hbm.Right_Elbow_upper.size() == 0);
	EXPECT_TRUE(m_hbm.Left_Elbow_lower.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Elbow_upper.size() == 0);
	EXPECT_TRUE(m_hbm.Right_Biceps.size() != 0);
	EXPECT_TRUE(m_hbm.Left_Biceps.size() != 0);
	EXPECT_TRUE(m_hbm.Right_biceps_to_shoulder.size() != 0);
	EXPECT_TRUE(m_hbm.Left_biceps_to_shoulder.size() != 0);
}

TEST_F(ContoursTest, test_contour_deformation_contourPlaneABCD){
	EXPECT_TRUE(c_test_abcd.size() != 0);
	
}



class hbmSkeletonTest : public::testing::Test{
protected:  // either public or protected cannot be private
	MainRepositioning repo;
	ContoursDeformation cont_deform;
	ContoursHbmInterface c_test_hbm_interface;
	ParserContourFile cont_parse;
	
	
	virtual void SetUp() {
//		cont_parse.parseHbmSkeleton(&fem,&meta);
		//			size_RightFoot = 5;
		
	}
	virtual void TearDown(){

	}
	
	HbmSkeleton skl;
};

