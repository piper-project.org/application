/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef _MainRepositioning_H
#define _MainRepositioning_H

//#define CONTOURS_EXPORT __declspec( dllexport )

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "delaunnay_func.h"
#include "repo_consts.h"
#include "lsn.h"
#include "VtkDelaunay.h"
#include "ContoursHbmInterface.h"
#include "SpinePredictor.h"

namespace piper {
	namespace contours {
		class CONTOURS_EXPORT MainRepositioning
		{


		public:
            // tetGenDir = path to where TetGenv.exe is. Pass in "." for current directory
            MainRepositioning(std::string tetGenDir);

			void Personalization(hbm::FEModel const& fem, hbm::HBM_contours* m_hbm_contours, hbm::contour_detail cont_detail, std::vector<double> gebod_value, hbm::Metadata& meta_ent, std::string bodyDataFile);

			void DelaunayPersonalization(std::set<hbm::Id>* sidnodes, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, bool del_map);


			void DelaunayNeckFlexionExtension(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map);


			void Delaunay_neck(hbm::FEModel& fem, hbm::Metadata& meta_ent, std::set<hbm::Id>* neckhead, bool del_map);


			void srrrfootDelaunay_Left_Leg_Repositioning(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map);


			void srrrDelaunay_Left_Leg_Repositioning(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map);

			/**
			* Repositioning of the contours in Left Leg.
			* @param flexion_angle double argument. Input angle by the user by which the Knee Joint is rotated.
			* @param fem an object of current hbm::FEModel instance.
			* @param meta_ent an object of hbm::Metadata. To use the metadata of the current context instance.
			* @return void
			*/
			void Left_Leg_Repositioning(double flexion_angle, hbm::FEModel& fem,
				hbm::Metadata& meta_ent, string tmpPath);

			/**************Left Leg Flexion******************/

			void LeftKneeFlexionContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);


			void srrfootLeft_Leg_Repositioning(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void srrankle2calfLeft_Leg_Repositioning(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void srrkneeLeft_Leg_Repositioning(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void srrrLeft_Leg_Repositioning(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);



			void Delaunay_Right_Leg_Repositioning(std::set<hbm::Id>* sidnodes, hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map);



			/**
			* Repositioning of the contours in Right Leg.
			* @param flexion_angle double argument. Input angle by the user by which the Knee Joint is rotated.
			* @param fem an object of current hbm::FEModel instance.
			* @param meta_ent an object of hbm::Metadata. To use the metadata of the current context instance.
			* @return void
			*/
			void Right_Leg_Repositioning(double flexion_angle, hbm::FEModel const& fem, hbm::Metadata& meta_ent, hbm::HBM_contours* m_hbm_contours);

			void Delaunay_Right_Leg_Repositioning_child(double flexion_angle, std::set<hbm::Id>* sidnodes, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata const& meta_ent, std::vector<piper::hbm::Contour*>* contour_poly, hbm::HBM_contours* m_hbm_contours, std::map<int, hbm::Plane*> contour_plane_new, std::map<hbm::Id, hbm::ModCoord>* modcords, bool del_map);

			void Right_Leg_Repositioning_child(double flexion_angle, hbm::FEModel const& fem, hbm::Metadata& meta_ent, hbm::HBM_contours* m_hbm_contours);

			void Left_Hip_Repositioning(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);

			void HipContourRepos(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);

			void ReposContourHip(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);


			void ReposContourHipFlexion(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);


			void HipAbduction(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);

			void HipTwisting(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);

			void ReposContourFlexion(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);

			void HipTwistingLeft(double angle, std::vector<hbm::NodePtr> vec_spline_new, std::set<hbm::Id> sidnodes, std::set<hbm::Id> tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent);



			void curve_fitting(hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void curve_fitting_2(hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void convxhull_detection(hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void tetgenerationop();

			static hbm::HBM_contours readContour(std::string filename, std::map<int, piper::hbm::Contour*>* cmap, std::vector<hbm::NodePtr>* vec_spline);

			void CubicBezier(hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void Left_Knee_Spline_Repositioning(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);


			/**************Elbow Repositioning*********/

			/*******Right Elbow Flexion*******/




			/********Left Elbow Flexion**********/





			/************Right Elbow twist******************/


			/************Left Elbow twist******************/





			/**************Elbow Repositioning*********/


			void NeckLateralContourCL(double flexion_angle, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, hbm::FEModel& fem1, hbm::Metadata& meta_ent, std::vector<hbm::NodePtr > cstore, std::vector<std::vector<hbm::NodePtr> > all_nodes);


			void Left_Foot_Repos(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);

			void rightFootRepos(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent);




			void generic_repos_hinge_joint(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::vector<hbm::NodePtr> input_spline_points);

			double shortest_distance_of_point_from_spline(hbm::Metadata& meta_ent, std::vector<double> pointext, double tparam);

			void shortest_distance_of_line_from_spline(hbm::Metadata& meta_ent);

			void shortest_distance_of_point_from_spline_independent(hbm::Metadata& meta_ent);

			void preProcessHingJointContours(std::vector<hbm::Contour*> A, std::vector<hbm::Contour*> B, std::vector<hbm::Contour*> J1, std::vector<hbm::Contour*> J2, hbm::Metadata& meta_ent);

			double HingJointAnglePreprocessing(double flexion_angle, double initial_angle);

			void HingJointContourRepos(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string axis_name_a, std::string axis_name_b, Eigen::MatrixXd splineMat);

			void Right_arm_twist(std::string radius_axis_a, std::string radius_axis_b, std::string ulna_axis_a, std::string ulna_axis_b, double angle, std::set<hbm::Id> radius, std::set<hbm::Id> ulna, std::set<hbm::Id> hand, std::set<hbm::Id> skin, std::set<hbm::Id> flesh, std::set<hbm::Id> humerus, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void Right_arm_flex(std::vector<hbm::NodePtr> vec_spline_new, std::string flexion_axis_1, std::string flexion_axis_2, double angle, std::set<hbm::Id> radius, std::set<hbm::Id> ulna, std::set<hbm::Id> hand, std::set<hbm::Id> skin, std::set<hbm::Id> flesh, std::set<hbm::Id> humerus, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			std::vector<double> curve_fitCoeff(std::vector<hbm::NodePtr>p);

			hbm::NodePtr cent(std::vector<hbm::NodePtr> a);

			hbm::Node centroid(std::vector<hbm::NodePtr> a);

			void neck_twist(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateral(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateral2(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateral3(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, std::set<hbm::Id> neck_fleshID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateral4(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, std::set<hbm::Id> neck_fleshID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateral5(double angle, std::set<hbm::Id> skin_thorax, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, std::set<hbm::Id> neck_fleshID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateralContoursspinepredictor(double angle, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neck_lateralContours(double angle, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void neckTwistContours(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void cervicalSpinePredict(double angle, std::set<hbm::Id> skin, std::set<hbm::Id> octID, std::set<hbm::Id> c1ID, std::set<hbm::Id> c2ID, std::set<hbm::Id> c3ID, std::set<hbm::Id> c4ID, std::set<hbm::Id> c5ID, std::set<hbm::Id> c6ID, std::set<hbm::Id> c7ID, std::set<hbm::Id> t1ID, std::set<hbm::Id> headID, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void CervicalVertebraeLateral(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void NeckLateralFlexion(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void NeckFlexionExtensionContourCl(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

            
			bool is_file_exist(const char *fileName);


			void repositionContours(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, string tmpPath, std::string joint_name);




			/****************Main Repos*****************************/

			void Left_Knee_Spline_RepositioningcontourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, string tmpPath);

			void Right_Knee_Spline_RepositioningcontourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, string tmpPath);

			void leftFootReposContourCl(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void rightFootReposContourCl(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void leftFootTwistContourCl(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void rightFootTwistContourCl(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHip2(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHipRightFlexion(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHipAbductionLeft(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHipAbductionRight(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHipTwistLeft(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void ReposContourHipRightTwisting(double angle, std::vector<hbm::NodePtr> vec_spline_new, hbm::VId sidnodes, hbm::VId tempselector, hbm::FEModel const& fem, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, hbm::Metadata& meta_ent, std::string tmpPath);

			void CervicalVertebraeEntityFlexion(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void CervicalVertebraeEntityFlexion2(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void Neck_head_flexion(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);


			void ThoraxFlexion(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath, hbm::ContourCLbr* tempbr);


			void CervicalVertebraeTwist(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel& fem1, hbm::Metadata& meta_ent, std::string tmpPath);

			void CervicalVertebraeTwist12(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel& fem1, hbm::Metadata& meta_ent, std::string tmpPath);

			void CervicalVertebraeEntityLateral(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void CervicalVertebraeEntityLateral2(double angle, vector<hbm::NodePtr>* cstore, vector<vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void LeftElbowBoneRepos(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);

			void LeftElbowBoneRepos12(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);


			void LeftElbowContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void RightElbowBoneRepos(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);

			void RightElbowBoneRepos12(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);

			void RightElbowContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void LeftElbowTwist(double flexion_angle, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void LeftElbowTwistContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void RightElbowTwist(double flexion_angle, hbm::FEModel const& fem, hbm::FEModel& fem1, hbm::Metadata& meta_ent);

			void RightElbowTwistContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void LeftWristFlexion(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);

			void RightWristFlexion(double flexion_angle, hbm::FEModel& fem1, hbm::FEModel const& fem, hbm::Metadata& meta_ent);

			void LeftWristFlexionContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void RightWristFlexionContourCL(double flexion_angle, hbm::FEModel& fem, hbm::Metadata& meta_ent, std::string tmpPath);


			/**********************Delaunnay****************************/

			void Delaunay_Left_Leg_Repositioning(hbm::FEModel& fem, hbm::Metadata& meta_ent,
				bool del_map, std::string tmpPath);

			void DelaunayRightLegFlexion(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_Left_Foot_Repositioning( hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_Right_Foot_Repositioning(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayPersonalizationHip(hbm::VId* sidnodes, hbm::FEModel& fem1, std::map<hbm::Id, hbm::ModCoord>* modcords, bool del_map, std::string tmpPath);

			void Delaunay_neck_Repositioning(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_lumbarmotion(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_Shoulder(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_Shoulder_right(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_neck_twisting(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void Delaunay_neck_lateral(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayLeftElbow(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayLeftElbow12(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayRightElbow(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayRightElbow12(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayLeftElbowTwist(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayRightElbowTwist(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayLeftWrist(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void DelaunayRightWrist(hbm::FEModel& fem, hbm::Metadata& meta_ent, bool del_map, std::string tmpPath);

			void LumbarMotion(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);
			
			void LumbarMotion12(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void LumbarLateral(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);


			void ThoraxMotion(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void LumbarTwist(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void shoulderFlexion(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);
			void rightShoulderFlexion(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);
			
			void shoulderTwist(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);
			void shoulderLateralFlexion(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void rightShoulderTwist(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);
			void rightShoulderLateralFlexion(double angle, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);


			void ThoraxLateral(double angle, std::vector<hbm::NodePtr>* cstore, std::vector<std::vector<hbm::NodePtr> >* all_nodes, hbm::FEModel &fem1, hbm::FEModel const &fem, hbm::Metadata& meta_ent, std::string tmpPath);

			void hipcontourcorrection(hbm::FEModel &fem1, hbm::Metadata& meta_ent);

        private:
            DelaunnayFunc dmp;
            ProcessingFunc repo;
            LnReg StLn(int);
            ContoursHbmInterface contHbm;
            SpinePredictor spinep;
            SpaceTransform trans;
            vectors vec;
            //VTK Delaunay Related API's
            std::vector<hbm::Node> initial_node_list;
            std::vector<hbm::Node> final_node_list;
            std::vector<tet_element> vtk_tet_list;
            std::map<int, std::vector<double>> gebodValues;
            VtkDelaunay vtkdelaunay;
            hbm::ContourCL cont_cl;
            void ProcessDelaunay(bool IsModified, hbm::HBM_contours* m_hbm_contours, std::vector<hbm::Node> all_bones);

		};

	}
}
#endif


