/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_PARSELINESKELETON_H
#define PIPER_CONTOURS_PARSELINESKELETON_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif
#include "hbm/HumanBodyModel.h"
#include "ContoursHbmInterface.h"
#include "Joint.h"
#include "BodyRegion.h"
#include "Joint.h"
#include "LineSkeleton.h" 
#include <queue>
namespace piper {
	namespace contours {

		/**
		*
		* @author Aditya Chhabra @date 2016 
		*/


		
                class CONTOURS_EXPORT ParseLineSkeleton {
		public:

                        ParseLineSkeleton();
                        ~ParseLineSkeleton();

						bool readSkeletonXml(hbm::FEModel& c_fem, hbm::Metadata& c_meta);

						bool skeleParse(tinyxml2::XMLElement*, Joint*);
						LineSkeleton skele;

						BodyRegion* parseSkeleInfo(tinyxml2::XMLElement*);

						

		};



	}
}
#endif

