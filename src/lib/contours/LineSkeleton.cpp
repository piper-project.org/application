/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "LineSkeleton.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		LineSkeleton::LineSkeleton() {
		}

		LineSkeleton::~LineSkeleton()
		{
		}


		void LineSkeleton::printSkele(BodyRegion* breg){

			if (breg == nullptr){
				breg = root;
			}
			else{
				breg = breg;
			}

			

			cout << endl << breg->name << endl;
			for (int i = 0; i < breg->childrenjts.size(); i++){
				Joint* jt = new Joint;
				jt = breg->childrenjts.at(i);
				cout << jt->name << endl;
				BodyRegion* bri = new BodyRegion();
				bri = jt->childbodyregion;
				printSkele(bri);
			}
		}


	}
}
