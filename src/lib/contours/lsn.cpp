/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "lsn.h"
#include <iostream>
using namespace std;
namespace piper {
	namespace contours {

		// Initialize static variable
		unsigned LnReg::countInstances = 0;

		LnReg::LnReg(int nd)
		 {
		 countInstances++;
		 n=nd;
		 x=new long double[n];
		 y=new long double[n];
		 }
		LnReg::~LnReg()
		 {
		 countInstances--;
		 delete []x;
		 delete []y;
		 }
//		115
		void LnReg::SetData(int i, long double xd, long double yd)
		 {
		 x[i]=xd;
		 y[i]=yd;
		 }
		void LnReg::Solv(long double &a, long double &b)
		 {
		 long double sumx=0, sumy=0, sumx2=0, sumxy=0;
		 for(int i=0; i<n; i++)
		 {
		 sumx+=x[i];
		 sumy+=y[i];
		 sumx2+=x[i]*x[i];
		 sumxy+=x[i]*y[i];
		 }
		 long double D=n*sumx2-sumx*sumx;
		 a=(n*sumxy-sumy*sumx)/D;
		 b= (sumx2*sumy-sumx*sumxy)/D;
		 }

		unsigned LnReg::getCountInstances()
		{
			return countInstances;
		}

		//---------------------------------------------------------------------------

	
	}

}


