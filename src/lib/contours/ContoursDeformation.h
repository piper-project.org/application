/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_CONTOURSDEFORMATION_H
#define PIPER_CONTOURS_CONTOURSDEFORMATION_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "hbm/HumanBodyModel.h"
#include "ContoursHbmInterface.h"
#include "vector_func.h"
#define PI 3.1415926535897932384626433832795

namespace piper {
	namespace contours {


		
		/**
		* @brief A class which involves functions of transformation and translation of contours.
		* It is intended for repositioning of contours
		* it currently contains the repositioning of Joints at hip,knee and foot.
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class CONTOURS_EXPORT ContoursDeformation {
		public:

			ContoursDeformation();
			~ContoursDeformation();

			SpaceTransform c_transform;
			ContoursHbmInterface c_hbm_interface;


			/// <summary>
			/// Verify whether Contours are perpendicular to Spline.
			/// </summary>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="axes">The first two rows of the matrix contain the flexion axis a and flexion axis b</param>
			/// <param name="spline_matrix">The spline matrix</param>
			/// <returns>A boolean. true if contours are perpendicular to spline and false if they are not.</returns>
			bool VerifyPerpendicularContourConstraint(hbm::FEModel& c_femodel, hbm::Metadata& c_meta, Eigen::MatrixXd axes, Eigen::MatrixXd spline_matrix);


			/// <summary>
			/// Reposition the contours at a Hinge Joint.
			/// </summary>
			/// <param name="flexion_angle">Angle by which the repositiong of contours will occur</param>
			/// <param name="c_femodel">The femodel - an object of current FEModel instance</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			/// <param name="axes">The first two rows of the matrix contain the flexion axis a and flexion axis b</param>
			/// <param name="spline_matrix">The spline matrix</param>
			void RepositionHingeJointContours(double flexion_angle, hbm::FEModel& c_femodel, hbm::Metadata& c_meta, Eigen::MatrixXd axes, Eigen::MatrixXd spline_matrix);

			/// <summary>
			/// Prepopulating Hinge Joint Contours.
			/// </summary>
			/// <param name="A">a vector of pointer to the contour which are fixed while doing positioning at Hinge Joint</param>
			/// <param name="B">a vector of pointer to the contour which move by flexion angle</param>
			/// <param name="J1">a vector of pointer to the contour which are near fixed side "A" </param>
			/// <param name="J2">a vector of pointer to the contours whch are near movable side "B"</param>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			void prePopulateHingeJointContours(std::vector<hbm::Contour*> A, std::vector<hbm::Contour*> B, std::vector<hbm::Contour*> J1, std::vector<hbm::Contour*> J2, hbm::Metadata& c_meta);


			/// <summary>
			/// Preprocess Hinge Joint Contours.
			/// </summary>
			/// <param name="c_meta">an object of Metadata. To use the metadata of the current context instance.</param>
			void preProcessHingeJointContours(hbm::Metadata& c_meta);


			/// <summary>
			/// Preprocess Hinge Joint Angle.
			/// </summary>
			/// <param name="flexion_angle">a double to store the absolute angle by which the flexion is to be done</param>
			/// <param name="initial_angle">a double to store decimal value of initial angle of the joint</param>
			/// <returns>The angle by which the joint is to be rotated to get the desired rotation</returns>
			double PreprocessHingeJointAngle(double flexion_angle, double initial_angle);


			/// <summary>
			/// Get the plane equation of a Contour.
			/// \todo check if the points in the contour are in a single plane. Information can be stored in the Contour class 
			/// </summary>
			/// <param name="cont">cont a contour</param>
			/// <returns>A vector conating the information of the coefficients of euqtion of plane AX+BY+CZ+D=0 in the following sequaence : A,B,C,D</returns>
			std::vector<double> ContourPlaneABCD(hbm::Contour cont);


			/// <summary>
			/// Determinea the u value of a Plane if it lies between the first and last control points of bezier curve.
			/// </summary>
			/// <param name="abcd">coefficients of plane equation of Contour</param>
			/// <param name="bezier_control_points">Control Points of bezier Curve</param>
			/// <returns>u value of the plane of Contour having A,B,C,D as the coefficients of the plane equation</returns>
			double uValueOfContour(std::vector<double> abcd, Eigen::MatrixXd bezier_control_points);
			

			/// <summary>
			/// Roatate the contour by an angle theta about an axis.
			/// </summary>
			/// <param name="theta">The angle by which the contour is to be rotated</param>
			/// <param name="axis">The axis about which the rotataion will occur</param>
			/// <returns>final rotated contour</returns>
			hbm::Contour* rotateContour(hbm::Contour*, double theta, Eigen::MatrixXd axis);

		};
	}
}
#endif

