/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_DelaunayFunc_H
#define PIPER_DelaunayFunc_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include <math.h>
#include <algorithm>
#include <time.h>
#include <list>
#include "processing_func.h"

//#define CONTOURS_EXPORT __declspec( dllexport )
using namespace std;

namespace piper {
	namespace contours {

		class CONTOURS_EXPORT DelaunnayFunc
		{

		public:

            // tetGenDir = path to where TetGenv.exe is. Pass in "." for current directory
            DelaunnayFunc(std::string tetGenDir);


            double tet_volume(tet_element tet);

            std::string subDirPrefix;

            void delaunayTetGen(std::string fileName);



            void readTetNodeListFile(std::string node_fileName);


            void readTetListFile(std::string fileName, int calc_volume = 0);
            //This should be improved, added it to resolve the linux build error, because delaunay code is referring itoa.

            void readtetlistFiletetnewstruct(std::string fileName, int calc_volume = 0);
            

            void reverse(char str[], int length);
            // Implementation of itoa()
            char* itoa(int num, char* str, int base);


            void mappingNodesToTet4(std::vector<piper::hbm::NodePtr>& nodes_for_mapping);

            void mappingNodesToTxTets(std::string node_fileName);


            void clear_all();


            std::vector<point> call_delaunay(std::string tetGen_file, std::string final_tet_file, std::vector<piper::hbm::NodePtr>& mapping_nodes);

            void tetgentesting(std::string tetGen_file);

            std::vector<point> call_delaunay_2(std::string tetGen_file, std::string final_tet_file, std::vector<piper::hbm::NodePtr>& mapping_nodes);



            bool mapping;


            //Vtk Delaunay Related APIs
            void readNodes(std::vector<piper::hbm::Node> node_list);

            void readTets(std::vector<tet_element> tet_list, int calc_volume = 0);
            std::vector<point> ProcessTetMapping(std::vector<piper::hbm::Node> initial_node_list, std::vector<piper::hbm::Node> final_node_list,
                std::vector<piper::hbm::NodePtr>& mapping_nodes, std::vector<tet_element> vtk_tet_list);
            void DebugTets();


            /********New Implementation********/
            void MappingNodes(std::vector<hbm::NodePtr>&);
            /********New Implementation********/

            /********New Implementation********/
            void MappingNodeshashtet(std::vector<hbm::NodePtr>&);
            /********New Implementation********/

            /********New Implementation********/

            void MappingNodeswithnewtetstruct(std::vector<hbm::NodePtr>&);
            /********New Implementation********/

            void MappingNodeshashtetserr(std::vector<hbm::NodePtr> &nodes_for_mapping);


            void Transform(point & p1, point & p2, std::vector<point>& NodesToMap);

            bool SortByZmin(tet_element_new &a, tet_element_new &b);

			std::string delaunayInputFileName = "tetGen_input.node";

			std::string tetNodeListInputFileName = "keypoints.1.node";
			std::string tetListInputFileName = "keypoints.1.ele";
			std::string mapNode2TetOutputFileName = "map_node_file.txt";
			std::string nodeListForMappingFileName = "transform_node.txt";

			std::string txTetNodeListInputFileName = "GHBMC_Hip_Joint_DelaunayInputTx.1.node";
			std::string txNodeListOutputFileName = "keypointstx.node";
			std::string new_node_FileName = "new_nodes.txt";
			std::string skippedNodesOutputFileName = "skipped_nodes_file.txt";
			std::string keywordFileName = "model.k";
			std::string tranformedKeywordFileName = "model_transformed.k";

			ProcessingFunc repo;
		private:

            std::string tetGenPath;
			std::vector<piper::hbm::NodePtr> node_list;
			std::vector<point> tet_node_list;

			// old implement
			std::vector<tet_element> tet_list;


			//new datastruct tet_element_new_srr implement
			std::vector<tet_element_new> tet_list1;
			// new_iomplement

			//float c2 = 0;
			std::vector<volume_cordinates> mapped_node_list;
			//Mapping of node id's to the indices in node_list
			std::map<hbm::Id, hbm::Id> hash_node;
			std::map<hbm::Id, hbm::Id> hash_tet_node;
			std::vector<point> vec_pt;
			//Mapping of tet element id's to the indices in tet_list
			std::map<hbm::Id, hbm::Id> hash_tet;
			std::map<int, int> hash_map_node;
			point pt;
			//			double tet_volume(tet_element tet);
			//			double mtet_volume(tet_element tet, int j);
			// private functions for contour generation

			//double volume_tet(point p1, point p2, point p3, point p4);
			//			double volume_tet(const Node& p1, const Node& p2, const Node& p3, const Node& p4)


			inline double volume_tet(const point& p1, const point& p2, const point& p3, const point& p4)
			{
				//double volume = abs((a[0]-a[3])*((b[1]-b[3])*(c[2]-c[3])-(b[2]-b[3])*(c[1]-c[3]))-(a[1]-a[3])*((b[0]-b[3])*(c[2]-c[3])-(b[2]-b[3])*(c[0]-c[3]))+(a[2]-a[3])*((b[0]-b[3])*(c[1]-c[3])-(b[1]-b[3])*(c[0]-c[3])))/6;

				double volume = ((p1.p_x - p4.p_x)*((p2.p_y - p4.p_y)*(p3.p_z - p4.p_z) - (p3.p_y - p4.p_y)*(p2.p_z - p4.p_z)) - (p2.p_x - p4.p_x)*((p1.p_y - p4.p_y)*(p3.p_z - p4.p_z) - (p3.p_y - p4.p_y)*(p1.p_z - p4.p_z)) + (p3.p_x - p4.p_x)*((p1.p_y - p4.p_y)*(p2.p_z - p4.p_z) - (p2.p_y - p4.p_y)*(p1.p_z - p4.p_z))) / 6;

				return volume;
			}


			class point_z_comparator
			{
			public:
				bool operator() (point const& a, point const& b)
				{
					return (a.p_z < b.p_z);
				}
			};

			class zmin_comparator
			{
			public:

				bool operator() (std::pair<std::vector<int>::size_type, double> const& a, std::pair<std::vector<int>::size_type, double> const& b)
				{
					return (a.second < b.second);
				}
			};

			//			bool tet_contains_point(const NodePtr p, const NodePtr p0, const NodePtr p1, const NodePtr p2, const NodePtr p3, vector<double>& vol_coord);


			inline bool tet_contains_point(const point& p, const point& p0, const point& p1, const point& p2, const point& p3, std::vector<double>& vol_coord)
			{
				bool result = false;

				// tolerances
				double d_sum = 0.0;

				double vol_tet = volume_tet(p0, p1, p2, p3);

				vol_coord[0] = volume_tet(p, p1, p2, p3) / vol_tet;
				vol_coord[1] = volume_tet(p0, p, p2, p3) / vol_tet;
				vol_coord[2] = volume_tet(p0, p1, p, p3) / vol_tet;
				vol_coord[3] = volume_tet(p0, p1, p2, p) / vol_tet;
				/*
				if ( (vol_coord[0] >= 0.0 - d_sum && vol_coord[0] <= 1.0 + d_sum) &&
				(vol_coord[1] >= 0.0 - d_sum && vol_coord[1] <= 1.0 + d_sum) &&
				(vol_coord[2] >= 0.0 - d_sum && vol_coord[2] <= 1.0 + d_sum) &&
				(vol_coord[3] >= 0.0 - d_sum && vol_coord[3] <= 1.0 + d_sum) )*/

				if ((vol_coord[0] >= -d_sum) &&
					(vol_coord[1] >= -d_sum) &&
					(vol_coord[2] >= -d_sum) &&
					(vol_coord[3] >= -d_sum))

					/*
					if ( (vol_coord[0] <= 1.0 + d_sum) &&
					(vol_coord[1] <= 1.0 + d_sum) &&
					(vol_coord[2] <= 1.0 + d_sum) &&
					(vol_coord[3] <= 1.0 + d_sum) &&
					*/

					//( (vol_coord[0] + vol_coord[1] + vol_coord[2] + vol_coord[3]) > (1.0 - d_sum) ) && 
					//( (vol_coord[0] + vol_coord[1] + vol_coord[2] + vol_coord[3]) < (1.0 + d_sum) ) )
				{
					result = true;
				}

				return result;
			}


			/***************New Implementation***********/

			inline double FindVolume(const point & p1, const point & p2, const point & p3, const point & p4)
			{

				double volume = ((p1.p_x - p4.p_x)*((p2.p_y - p4.p_y)*(p3.p_z - p4.p_z) - (p3.p_y - p4.p_y)*(p2.p_z - p4.p_z)) - (p2.p_x - p4.p_x)*((p1.p_y - p4.p_y)*(p3.p_z - p4.p_z) - (p3.p_y - p4.p_y)*(p1.p_z - p4.p_z)) + (p3.p_x - p4.p_x)*((p1.p_y - p4.p_y)*(p2.p_z - p4.p_z) - (p2.p_y - p4.p_y)*(p1.p_z - p4.p_z))) / 6;
				return volume;
			}

			inline bool PointInTet(const point & n, const point & p0, const point& p1, const point & p2, const point & p3, std::vector<double>&ratios)
			{
				//	clock_t t1, t2;
				//		t1 = clock();
				double tetvol = FindVolume(p0, p1, p2, p3);

				//				ratios[0] = FindVolume(n, p1, p2, p3) / tetvol; //A point is in a tet if and only if the volume of the tet and the tet formed by the point 
				//				ratios[1] = FindVolume(p0, n, p2, p3) / tetvol; //and the other three points have the same sign
				//				ratios[2] = FindVolume(p0, p1, n, p3) / tetvol;
				//				ratios[3] = FindVolume(p0, p1, p2, n) / tetvol;
				bool result = false;

				if ((ratios[0] = (FindVolume(n, p1, p2, p3) / tetvol)) > 0.0 && (ratios[1] = (FindVolume(p0, n, p2, p3) / tetvol)) > 0.0 && (ratios[2] = FindVolume(p0, p1, n, p3) / tetvol) > 0.0 && (ratios[3] = FindVolume(p0, p1, p2, n) / tetvol) > 0.0)
				{
					result = true;
				}
				//	t2 = clock();
				return result;
			}

			/***************New Implementation***********/



			//			HANDLE out;


		};
	}

}
#endif

