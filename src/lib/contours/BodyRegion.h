/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_BODYREGION_H
#define PIPER_CONTOURS_BODYREGION_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif
#include "hbm/HumanBodyModel.h"
namespace piper {
	namespace contours {

		/**
		* @brief A class which defines the basic datastructure of Body Region in HumanBody.
		* Each body region is associated with a vector of Children Joints.
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class Joint;

		class CONTOURS_EXPORT BodyRegion {
		public:

			BodyRegion();
			~BodyRegion();

			std::string b_name;
			std::vector<Joint*> joint_children;
			std::vector<Eigen::Vector3d> control_points;
			bool flexible;
			std::string entity_name;
			Joint* child;
			std::vector<hbm::Contour*> brcontours;
//		private:
			//new
			std::string name;
			std::vector<Eigen::Vector3d> ctrlpts;
			std::vector<Joint*> childrenjts;
//		public:
			void setName(std::string name);
			void addChildJoint(Joint*);
		};

		class CONTOURS_EXPORT Bone {
		public:

			Bone();
			~Bone();

			std::string bone_name;
			std::string entity_name;
			std::vector<hbm::Landmark*> twist_axis;

		};




	}
}
#endif

