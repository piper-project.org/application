/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "ParseContourCL.h"

using namespace tinyxml2;

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		ParseContourCL::ParseContourCL() {
		}

		ParseContourCL::~ParseContourCL()
		{
		}


        tinyxml2::XMLError ParseContourCL::skeleParse(tinyxml2::XMLElement* element, ContourCLj* temprj, hbm::Metadata& c_meta){

			cout << "internal func starts" << endl;

			ContourCLbr* tempbr = new ContourCLbr;

			ContourCLj* tempj = new  ContourCLj;
			string bname;
			string jtname;
			vector<string> landmarknames;

			bname = element->Attribute("name");
			cout << endl << "bname  " << bname << endl;
			tempbr->setName(bname);
			cout << "op:   " << tempbr->name << endl;

			tinyxml2::XMLElement* element1 = element->FirstChildElement("ctrlpointlandmark");
			if (element1 == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}

			while (element1 != nullptr){
				cout << element1->GetText() << endl;
				string ctrlpointlandmark = element1->GetText();
				landmarknames.push_back(ctrlpointlandmark);
				cout << "ctrlpointlandmark:  " << ctrlpointlandmark << endl;
				element1 = element1->NextSiblingElement("ctrlpointlandmark");
			}
			tempbr->setBodyRegionLandmarks(landmarknames);
			landmarknames.clear();

			if (c_meta.ctrl_line.root != NULL){
				temprj->setChildBodyRegion(tempbr);
			}

			element1 = element->FirstChildElement("joint");
            
			while (element1 != nullptr){
				cout << element1->Attribute("name");
				jtname = element1->Attribute("name");
				cout << "jtname:  " << jtname << endl;
				tempj->setName(jtname);

				tinyxml2::XMLElement* element2 = element1->FirstChildElement("bodyregion");
				if (element2 == nullptr){
					return XML_ERROR_PARSING_ELEMENT;
				}

				if (c_meta.ctrl_line.root == NULL){
					c_meta.ctrl_line.root = tempbr;
				}

				tempbr->addChildJoint(tempj);
				tempj->setParentBodyRegion(tempbr);
                tinyxml2::XMLError eResult = skeleParse(element2, tempj, c_meta);
                if (eResult != tinyxml2::XML_SUCCESS){
                    return eResult;
                }
				element1 = element1->NextSiblingElement("joint");
			}


			return tinyxml2::XML_SUCCESS;
		}

		ContourCLbr* ParseContourCL::parseSkeleInfo(tinyxml2::XMLElement * element){
			std::cout << "start " << std::endl;
			ContourCLbr* tempbr = new ContourCLbr;
			ContourCLj* tempj = new  ContourCLj;
			string bname;
			string jtname;
			vector<string> landmarknames;


			while (element != NULL){
				element = element->FirstChildElement("tiger");
				bname = element->Attribute("name");
				cout << bname << endl;
				tempbr->setName(bname);
				tinyxml2::XMLElement* element1 = element->FirstChildElement("tiger");
				if (element1 == NULL){
				}
				//cout << "1uo" << endl;
				cout << element1->GetText() << endl;
				/*				cout << "1";
								tinyxml2::XMLElement* element1 = element->FirstChildElement("ctrlpoint");
								if (element1 == NULL){
								}
								*/
				/*				tinyxml2::XMLElement* element2 = element1->FirstChildElement("ctrlpointlandmark");
								if (element2 == NULL){
								}
								cout << element2->GetText() << endl;
								while (element2 != NULL){
								string ctrlpointlandmark = element2->GetText();
								landmarknames.push_back(ctrlpointlandmark);
								cout << ctrlpointlandmark << endl;
								element2 = element2->NextSiblingElement("ctrlpointlandmark");
								}
								tempbr->setBodyRegionLandmarks(landmarknames);
								landmarknames.clear();
								while (element != NULL){
								element = element->FirstChildElement("joint");
								jtname = element->Attribute("name");
								cout << jtname;
								tempj->setName(jtname);
								if (skele.root == NULL){
								skele.root = tempbr;
								}
								tempbr = parseSkeleInfo(element);

								tempbr->addChildJoint(tempj);
								element = element->NextSiblingElement("joint");
								} */
				//				element = element->NextSiblingElement("joint");
			}
			return tempbr;
		}
        
        tinyxml2::XMLError ParseContourCL::readSkeletonXml(FEModel& c_fem, hbm::Metadata& c_meta){

			c_meta.ctrl_line.root = NULL;
			tinyxml2::XMLDocument xmldoc;

			tinyxml2::XMLError eResult = xmldoc.LoadFile("testxml.xml");
			if (eResult != tinyxml2::XML_SUCCESS){
                return eResult;
			}

			XMLNode * pRoot = xmldoc.FirstChild();
			if (pRoot == nullptr){
				return XML_ERROR_FILE_READ_ERROR;
			}


			XMLElement * pElement = pRoot->FirstChildElement("bodyregion");
			if (pElement == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}

			ContourCLj* temprj = new ContourCLj;
            eResult = skeleParse(pElement, temprj, c_meta);
            if (eResult != tinyxml2::XML_SUCCESS){
                return eResult;
            }

			c_meta.ctrl_line.printSkele();

            return XML_SUCCESS;
		}

        tinyxml2::XMLError ParseContourCL::populateInfo(tinyxml2::XMLElement* element, hbm::Metadata& c_meta, hbm::FEModel& c_fem, ContourCLj* tj = NULL){

			ContourCLbr* tempbr = new ContourCLbr;
			if (tj != NULL){
				tj->childbodyregion = tempbr;
			}
			string bname;
			string bname_info;
			string bname_type;
			string jtname;
			string jtname_info;
			vector<string> landmarknames;
			vector<string> landmarknames_info;
			vector<string> circumferences;
			vector<string> circumferences_info;
			vector<string> j_circumferences;
			vector<string> j_circumferences_info;
			vector<string> flexionaxes;
			vector<string> twistaxes;
			vector<string> skinentity;
			vector<string> skinentity_info;
			vector<string> reflandmark;
			vector<string> reflandmarkinfo;
			vector<string> dofa;
			vector<string> dofb;
			vector<string> dofc;
			vector<string> dofa_info;
			vector<string> dofb_info;
			vector<string> dofc_info;
			string joint_type;
			vector<string> taxis_info;

			std::map<std::string, std::map<std::string, std::string>> dimensions;


			bname = element->Attribute("name");
			std::cout << "\n Parsing = " << bname;
			tempbr->name = bname;
//#define DEBUGGING

#pragma region BR_DESCRIPTION
			///br description
			if (element->Attribute("info")){
				bname_info = element->Attribute("info");
				tempbr->description = bname_info;
			}
#pragma endregion

#pragma region BR_TYPE
			///br type
			if (element->Attribute("type")){
				bname_type = element->Attribute("type");
				tempbr->brtype = bname_type;
#ifdef DEBUGGING
				std::cout << tempbr->brtype << std::endl;
#endif
			}
#pragma endregion

#pragma region BR_DOF
			///br type
			if (element->Attribute("dof")){
				int dof = int(stof(element->Attribute("dof")));
				tempbr->degreesOfFreedom = dof;
#ifdef DEBUGGING
				std::cout << tempbr->name << " : " << tempbr->degreesOfFreedom << std::endl;
#endif
			}
#pragma endregion

#pragma region BR_CONTROLPOINTLANDMARKS
			///br controlpoint landmarks
			tinyxml2::XMLElement* element1 = element->FirstChildElement("ctrlpointlandmark");
			if (element1 == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}

			while (element1 != nullptr){
				string ctrlpointlandmark = element1->GetText();
				string ctrlpointlandmark_info = "No description available";
				if (element1->Attribute("info")){
					ctrlpointlandmark_info = element1->Attribute("info");
					//std::cout << endl << ctrlpointlandmark_info;
					landmarknames_info.push_back(ctrlpointlandmark_info);
				}
				landmarknames.push_back(ctrlpointlandmark);
				if (!c_meta.hasLandmark(ctrlpointlandmark)){
					std::vector<std::string> values;
					values.push_back("Control Point Landmark");
					values.push_back(tempbr->name);
					values.push_back(ctrlpointlandmark_info);
					c_meta.ctrl_line.undefinedCLElements[ctrlpointlandmark] = values;
				}

				element1 = element1->NextSiblingElement("ctrlpointlandmark");
			}

			tempbr->Isprocessed = false;
			tempbr->cplandmarks = landmarknames;
			tempbr->cplandmarks_info = landmarknames_info;
			landmarknames.clear();
			landmarknames_info.clear();
#pragma endregion

#pragma region BR_SKINGMVEC
			///vector of skin entity
			element1 = element->FirstChildElement("skingm");
			string skingm_info;
			if (element1 != NULL){
				tempbr->skingenericmetadata = element1->GetText();
				if (element1->Attribute("info")){
					skingm_info = element1->Attribute("info");
					
				}
				if (!c_meta.hasEntity(tempbr->skingenericmetadata)){
					std::vector<std::string> values;
					values.push_back("Skin Entity");
					values.push_back(tempbr->name);
					values.push_back(skingm_info);
					c_meta.ctrl_line.undefinedCLElements[tempbr->skingenericmetadata] = values;
				}
			}


			while (element1 != nullptr){
				string vecskin = element1->GetText();
				skinentity.push_back(vecskin);
				element1 = element1->NextSiblingElement("skingm");
			}
			tempbr->vecskinentity = skinentity;

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->vecskinentity.size(); i++){
				std::cout << tempbr->vecskinentity.at(i) << " : " << std::endl;;

			}
#endif
			skinentity.clear();
			skinentity_info.clear();
#pragma endregion


#pragma region CIRCUMFERENCE 
			element1 = element->FirstChildElement("circumference");

			if (element1 != NULL){
				tempbr->circumf = element1->GetText();
			}

			while (element1 != nullptr){
				string circumfi = element1->GetText();
				string circumfi_info;
				if (element1->Attribute("info")){
					circumfi_info = element1->Attribute("info");
					std::cout << endl << circumfi_info;
					circumferences_info.push_back(circumfi_info);
				}
				circumferences.push_back(circumfi);

				if (!c_meta.hasLandmark(circumfi)){
					std::vector<std::string> values;
					values.push_back("Circumference Landmark");
					values.push_back(tempbr->name);
					values.push_back(circumfi_info);
					c_meta.ctrl_line.undefinedCLElements[circumfi] = values;
				}

				element1 = element1->NextSiblingElement("circumference");
			}

			tempbr->circumfvec = circumferences;
			tempbr->circumfvec_info = circumferences_info;
			circumferences.clear();
			circumferences_info.clear();
#pragma endregion


#pragma region DIMENSIONS
			element1 = element->FirstChildElement("dimensions");
			if (element1 != NULL){
				std::map<std::string, std::map<std::string, std::string>> dimensions;
				tinyxml2::XMLElement* ele = element1->FirstChildElement("dimension");
				while (ele != nullptr){
					if (ele->Attribute("type")){
						string type = ele->Attribute("type");
						tinyxml2::XMLElement* src = ele->FirstChildElement("source");
						while (src != nullptr){
							if (src->Attribute("name")){
								string src_name = src->Attribute("name");
								string val = src->GetText();
								dimensions[type][src_name] = val;
							}
							src = src->NextSiblingElement("source");
						}
					}
					ele = ele->NextSiblingElement("dimension");
				}
				tempbr->dimensions = dimensions;
			}
#pragma endregion


#pragma region reflandmark
			///degree of freedom 1 & 3
			element1 = element->FirstChildElement("reflandmark");

			while (element1 != nullptr){
				string ref = element1->GetText();
				if (element1->Attribute("info")){
					string ref_info = element1->Attribute("info");
					reflandmarkinfo.push_back(ref_info);
				}
				reflandmark.push_back(ref);

				element1 = element1->NextSiblingElement("reflandmark");
			}

			tempbr->reference_landmark = reflandmark;
			tempbr->reference_landmark_info = reflandmarkinfo;
			reflandmark.clear();
			reflandmarkinfo.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->reference_landmark.size(); i++){
				std::cout << tempbr->reference_landmark.at(i) << " : " << tempbr->reference_landmark_info.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF1
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);

				element1 = element1->NextSiblingElement("dof1");
			}

			tempbr->dof1 = dofa;
			tempbr->dof1_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof1.size(); i++){
				std::cout << tempbr->dof1.at(i) << " : " << tempbr->dof1_info.at(i) << std::endl;
			}
#endif

#pragma region POSSIBLE_DOF
			///The no of different degrees of freedom possible for the body region
			element1 = element->FirstChildElement("possible_dof");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				int limit = int(stof(dof_limit));
				tempbr->degreesOfFreedom = limit;
				element1 = element1->NextSiblingElement("possible_dof");
				//std::cout << "Yo Found dof3_limit_max - " << limit << " for " << tempbr->name;
			}

#ifdef DEBUGGING
			std::cout << tempbr->name << " possible_dof: " << tempbr->degreesOfFreedom << std::endl;
#endif

#pragma endregion


#pragma region DOF1_REF
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1_reference");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof1_reference");
			}
			
			tempbr->dof1_ref = dofa;
			tempbr->dof1_ref_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof1_ref.size(); i++){
				std::cout << tempbr->name << " dof1_ref: " << tempbr->dof1_ref.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF1_TARGET
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1_target");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof1_target - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof1_target");
			}
			
			tempbr->dof1_target = dofa;
			tempbr->dof1_target_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof1_target.size(); i++){
				std::cout << tempbr->name << " dof1_target: " << tempbr->dof1_target.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF1_NORMAL
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1_normal");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof1_normal- " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof1_normal");
			}
			
			tempbr->dof1_normal = dofa;
			tempbr->dof1_normal_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof1_normal.size(); i++){
				std::cout << tempbr->name << " dof1_normal: " << tempbr->dof1_normal.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF1_LIMIT_MIN
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1_limit_min");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof1_limit_min = limit;
				element1 = element1->NextSiblingElement("dof1_limit_min");
				//std::cout << "Yo Found dof1_limit_min - " << limit << " for " << tempbr->name << std::endl;
			}


#ifdef DEBUGGING
			if (tempbr->dof1_limit_min != 0 || tempbr->dof1_limit_max != 0)
				std::cout << tempbr->name << " dof1_limit_min: " << tempbr->dof1_limit_min << std::endl;
			
#endif

#pragma endregion

#pragma region DOF1_LIMIT_MAX
			///degree of freedom 1
			element1 = element->FirstChildElement("dof1_limit_max");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof1_limit_min = limit;
				element1 = element1->NextSiblingElement("dof1_limit_max");
				//std::cout << "Yo Found dof1_limit_max - " << limit << " for " << tempbr->name;
			}

#ifdef DEBUGGING
			if (tempbr->dof1_limit_min != 0 || tempbr->dof1_limit_max != 0)
				std::cout << tempbr->name << " dof1_limit_max: " << tempbr->dof1_limit_max << std::endl;
#endif

#pragma endregion

#pragma region DOF2_REF
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2_reference");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof2_reference");
			}

			tempbr->dof2_ref = dofa;
			tempbr->dof2_ref_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof2_ref.size(); i++){
				std::cout << tempbr->name << " dof1_ref: " << tempbr->dof2_ref.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF2_TARGET
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2_target");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof2_target - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof2_target");
			}

			tempbr->dof2_target = dofa;
			tempbr->dof2_target_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof2_target.size(); i++){
				std::cout << tempbr->name << " dof2_target: " << tempbr->dof2_target.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF2_NORMAL
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2_normal");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof2_normal- " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof2_normal");
			}

			tempbr->dof2_normal = dofa;
			tempbr->dof2_normal_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof2_normal.size(); i++){
				std::cout << tempbr->name << " dof2_normal: " << tempbr->dof2_normal.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF2_LIMIT_MIN
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2_limit_min");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof2_limit_min = limit;
				element1 = element1->NextSiblingElement("dof2_limit_min");
				//std::cout << "Yo Found dof2_limit_min - " << limit << " for " << tempbr->name << std::endl;
			}


#ifdef DEBUGGING
			if (tempbr->dof2_limit_min != 0 || tempbr->dof2_limit_max != 0)
				std::cout << tempbr->name << " dof2_limit_min: " << tempbr->dof2_limit_min << std::endl;

#endif

#pragma endregion

#pragma region DOF2_LIMIT_MAX
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2_limit_max");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof2_limit_min = limit;
				element1 = element1->NextSiblingElement("dof2_limit_max");
				//std::cout << "Yo Found dof2_limit_max - " << limit << " for " << tempbr->name;
			}

#ifdef DEBUGGING
			if (tempbr->dof2_limit_min != 0 || tempbr->dof2_limit_max != 0)
				std::cout << tempbr->name << " dof2_limit_max: " << tempbr->dof2_limit_max << std::endl;
#endif

#pragma endregion

#pragma region DOF3_REF
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3_reference");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof3_reference");
			}

			tempbr->dof3_ref = dofa;
			tempbr->dof3_ref_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof3_ref.size(); i++){
				std::cout << tempbr->name << " dof1_ref: " << tempbr->dof3_ref.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF3_TARGET
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3_target");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof3_target - " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof3_target");
			}

			tempbr->dof3_target = dofa;
			tempbr->dof3_target_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof3_target.size(); i++){
				std::cout << tempbr->name << " dof3_target: " << tempbr->dof3_target.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF3_NORMAL
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3_normal");

			while (element1 != nullptr){
				string axis1 = element1->GetText();
				if (element1->Attribute("info")){
					string axis1_info = element1->Attribute("info");
					dofa_info.push_back(axis1_info);
				}
				dofa.push_back(axis1);
				//std::cout << "Yo Found dof3_normal- " << axis1 << " for " << tempbr->name << std::endl;
				element1 = element1->NextSiblingElement("dof3_normal");
			}

			tempbr->dof3_normal = dofa;
			tempbr->dof3_normal_info = dofa_info;
			dofa.clear();
			dofa_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof3_normal.size(); i++){
				std::cout << tempbr->name << " dof3_normal: " << tempbr->dof3_normal.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF3_LIMIT_MIN
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3_limit_min");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof3_limit_min = limit;
				element1 = element1->NextSiblingElement("dof3_limit_min");
				//std::cout << "Yo Found dof3_limit_min - " << limit << " for " << tempbr->name << std::endl;
			}


#ifdef DEBUGGING
			if (tempbr->dof3_limit_min != 0 || tempbr->dof3_limit_max != 0)
				std::cout << tempbr->name << " dof3_limit_min: " << tempbr->dof3_limit_min << std::endl;

#endif

#pragma endregion

#pragma region DOF3_LIMIT_MAX
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3_limit_max");

			while (element1 != nullptr){
				string dof_limit = element1->GetText();
				float limit = stof(dof_limit);
				tempbr->dof3_limit_min = limit;
				element1 = element1->NextSiblingElement("dof3_limit_max");
				//std::cout << "Yo Found dof3_limit_max - " << limit << " for " << tempbr->name;
			}

#ifdef DEBUGGING
			if (tempbr->dof3_limit_min != 0 || tempbr->dof3_limit_max != 0)
				std::cout << tempbr->name << " dof3_limit_max: " << tempbr->dof3_limit_max << std::endl;
#endif

#pragma endregion

#pragma region DOF2
			///degree of freedom 1
			element1 = element->FirstChildElement("dof2");

			while (element1 != nullptr){
				string axis2 = element1->GetText();
				if (element1->Attribute("info")){
					string axis2_info = element1->Attribute("info");
					dofb_info.push_back(axis2_info);
				}
				dofb.push_back(axis2);

				element1 = element1->NextSiblingElement("dof2");
			}

			tempbr->dof2 = dofb;
			tempbr->dof2_info = dofb_info;
			dofb.clear();
			dofb_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof2.size(); i++){
				std::cout << tempbr->dof2.at(i) << " : " << tempbr->dof2_info.at(i) << std::endl;
			}
#endif

#pragma endregion

#pragma region DOF3
			///degree of freedom 1
			element1 = element->FirstChildElement("dof3");

			while (element1 != nullptr){
				string axis3 = element1->GetText();
				if (element1->Attribute("info")){
					string axis3_info = element1->Attribute("info");
					dofc_info.push_back(axis3_info);
				}
				dofc.push_back(axis3);

				element1 = element1->NextSiblingElement("dof3");
			}

			tempbr->dof3 = dofc;
			tempbr->dof3_info = dofc_info;
			dofc.clear();
			dofc_info.clear();

#ifdef DEBUGGING
			for (size_t i = 0; i < tempbr->dof3.size(); i++){
				std::cout << tempbr->dof3.at(i) << " : " << tempbr->dof3_info.at(i) << std::endl;
			}
#endif

#pragma endregion

			/// Joint
			element1 = element->FirstChildElement("joint");

			/// name attribute
			while (element1 != nullptr){
				jtname = element1->Attribute("name");
				std::cout << " Parsing " << jtname;
				ContourCLj* tempj = new  ContourCLj;
				tempj->name = jtname;

#pragma region J_TYPE
				///j type
				if (element1->Attribute("type")){
					joint_type = element1->Attribute("type");
					tempj->j_type = joint_type;
#ifdef DEBUGGING
					std::cout << tempj->j_type << std::endl;
#endif
				}
#pragma endregion

#pragma region J_DOF
				///br type
				std::cout << "\n For dof\t";
				if (element1->Attribute("dof")){
					int dof = int(stof(element->Attribute("dof")));
					tempj->degreesOfFreedom = dof;
#ifdef DEBUGGING
					std::cout << tempj->name << " : " << tempj->degreesOfFreedom << std::endl;
#endif
				}
#pragma endregion

				tinyxml2::XMLElement* element2 = element1->FirstChildElement("entityjoint");
				if (element2 != NULL){
					tempj->entityjointname = element2->GetText();
				}
				element2 = element1->FirstChildElement("circumference");
				if (element2 != NULL){
					tempj->circum = element2->GetText();
				}
				while (element2 != nullptr){
					string circumfi = element2->GetText();
					string circumfi_info;
					if (element2->Attribute("info")){
						circumfi_info = element2->Attribute("info");
						std::cout << endl << circumfi_info;
						j_circumferences_info.push_back(circumfi_info);
					}
					j_circumferences.push_back(circumfi);

					if (!c_meta.hasLandmark(circumfi)){
						std::vector<std::string> values;
						values.push_back("Circumference Landmark");
						values.push_back(tempj->name);
						values.push_back(circumfi_info);
						c_meta.ctrl_line.undefinedCLElements[circumfi] = values;
					}

					element2 = element2->NextSiblingElement("circumference");
				}


				element2 = element1->FirstChildElement("flexionaxis");
				while (element2 != nullptr){
					string flex = element2->GetText();
					flexionaxes.push_back(flex);
					string flexionaxis_info;
					if (element2->Attribute("info")){
						flexionaxis_info = element2->Attribute("info");
						tempj->flexionaxeslandmarks_info.push_back(flexionaxis_info);
					}

					if (!c_meta.hasLandmark(flex)){
						std::vector<std::string> values;
						values.push_back("Flexion Axis Landmark");
						values.push_back(tempj->name);
						values.push_back(flexionaxis_info);
						c_meta.ctrl_line.undefinedCLElements[flex] = values;
					}

					element2 = element2->NextSiblingElement("flexionaxis");
				}


				tempj->flexionaxeslandmarks = flexionaxes;

#pragma region TWISTAXES
				element2 = element1->FirstChildElement("twistaxis");
				while (element2 != nullptr){
					string twist = element2->GetText();
					twistaxes.push_back(twist);
					string twistaxes__info;
					if (element2->Attribute("info")){
						twistaxes__info = element2->Attribute("info");
						taxis_info.push_back(twistaxes__info);
					}

					if (!c_meta.hasLandmark(twist)){
						std::vector<std::string> values;
						values.push_back("Twist Axis Landmark");
						values.push_back(tempj->name);
						values.push_back(twistaxes__info);
						c_meta.ctrl_line.undefinedCLElements[twist] = values;
					}

					element2 = element2->NextSiblingElement("twistaxis");

				}
				tempj->twistaxeslandmarks = twistaxes;
				tempj->twistaxeslandmarks_info = taxis_info;

#pragma endregion

#pragma region DOF1_REF
				///degree of freedom 1
				element2 = element1->FirstChildElement("dof1_reference");
				//std::cout << "\n 1 ";
				while (element2 != nullptr){
					std::string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						std::string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof1_reference");
				}

				tempj->dof1_ref = dofa;
				tempj->dof1_ref_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof1_ref.size(); i++){
					std::cout << tempj->name << " dof1_ref: " << tempj->dof1_ref.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF1_TARGET
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof1_target");
				//std::cout << "\n 2 ";
				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof1_target - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof1_target");
				}

				tempj->dof1_target = dofa;
				tempj->dof1_target_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof1_target.size(); i++){
					std::cout << tempj->name << " dof1_target: " << tempj->dof1_target.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF1_NORMAL
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof1_normal");
				//std::cout << "\n 3 ";
				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof1_normal- " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof1_normal");
				}

				tempj->dof1_normal = dofa;
				tempj->dof1_normal_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof1_normal.size(); i++){
					std::cout << tempj->name << " dof1_normal: " << tempj->dof1_normal.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF1_LIMIT_MIN
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof1_limit_min");
				//std::cout << "\n 4 ";
				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof1_limit_min = limit;
					element2 = element2->NextSiblingElement("dof1_limit_min");
					//std::cout << "Yo Found dof1_limit_min - " << limit << " for " << tempj->name << std::endl;
				}


#ifdef DEBUGGING
				if (tempj->dof1_limit_min != 0 || tempj->dof1_limit_max != 0)
					std::cout << tempj->name << " dof1_limit_min: " << tempj->dof1_limit_min << std::endl;

#endif

#pragma endregion

#pragma region DOF1_LIMIT_MAX
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof1_limit_max");
				//std::cout << "\n 5 ";
				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof1_limit_min = limit;
					element2 = element2->NextSiblingElement("dof1_limit_max");
					//std::cout << "Yo Found dof1_limit_max - " << limit << " for " << tempj->name;
				}

#ifdef DEBUGGING
				if (tempj->dof1_limit_min != 0 || tempj->dof1_limit_max != 0)
					std::cout << tempj->name << " dof1_limit_max: " << tempj->dof1_limit_max << std::endl;
#endif

#pragma endregion

#pragma region DOF2_REF
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof2_reference");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof2_reference");
				}

				tempj->dof2_ref = dofa;
				tempj->dof2_ref_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof2_ref.size(); i++){
					std::cout << tempj->name << " dof1_ref: " << tempj->dof2_ref.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF2_TARGET
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof2_target");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof2_target - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof2_target");
				}

				tempj->dof2_target = dofa;
				tempj->dof2_target_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof2_target.size(); i++){
					std::cout << tempj->name << " dof2_target: " << tempj->dof2_target.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF2_NORMAL
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof2_normal");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof2_normal- " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof2_normal");
				}

				tempj->dof2_normal = dofa;
				tempj->dof2_normal_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof2_normal.size(); i++){
					std::cout << tempj->name << " dof2_normal: " << tempj->dof2_normal.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF2_LIMIT_MIN
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof2_limit_min");

				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof2_limit_min = limit;
					element2 = element2->NextSiblingElement("dof2_limit_min");
					//std::cout << "Yo Found dof2_limit_min - " << limit << " for " << tempj->name << std::endl;
				}


#ifdef DEBUGGING
				if (tempj->dof2_limit_min != 0 || tempj->dof2_limit_max != 0)
					std::cout << tempj->name << " dof2_limit_min: " << tempj->dof2_limit_min << std::endl;

#endif

#pragma endregion

#pragma region DOF2_LIMIT_MAX
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof2_limit_max");

				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof2_limit_min = limit;
					element2 = element2->NextSiblingElement("dof2_limit_max");
					//std::cout << "Yo Found dof2_limit_max - " << limit << " for " << tempj->name;
				}

#ifdef DEBUGGING
				if (tempj->dof2_limit_min != 0 || tempj->dof2_limit_max != 0)
					std::cout << tempj->name << " dof2_limit_max: " << tempj->dof2_limit_max << std::endl;
#endif

#pragma endregion

#pragma region DOF3_REF
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof3_reference");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof1_ref - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof3_reference");
				}

				tempj->dof3_ref = dofa;
				tempj->dof3_ref_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof3_ref.size(); i++){
					std::cout << tempj->name << " dof1_ref: " << tempj->dof3_ref.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF3_TARGET
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof3_target");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof3_target - " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof3_target");
				}

				tempj->dof3_target = dofa;
				tempj->dof3_target_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof3_target.size(); i++){
					std::cout << tempj->name << " dof3_target: " << tempj->dof3_target.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF3_NORMAL
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof3_normal");

				while (element2 != nullptr){
					string axis1 = element2->GetText();
					if (element2->Attribute("info")){
						string axis1_info = element2->Attribute("info");
						dofa_info.push_back(axis1_info);
					}
					dofa.push_back(axis1);
					//std::cout << "Yo Found dof3_normal- " << axis1 << " for " << tempj->name << std::endl;
					element2 = element2->NextSiblingElement("dof3_normal");
				}

				tempj->dof3_normal = dofa;
				tempj->dof3_normal_info = dofa_info;
				dofa.clear();
				dofa_info.clear();

#ifdef DEBUGGING
				for (size_t i = 0; i < tempj->dof3_normal.size(); i++){
					std::cout << tempj->name << " dof3_normal: " << tempj->dof3_normal.at(i) << std::endl;
				}
#endif

#pragma endregion

#pragma region DOF3_LIMIT_MIN
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof3_limit_min");

				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof3_limit_min = limit;
					element2 = element2->NextSiblingElement("dof3_limit_min");
					//std::cout << "Yo Found dof3_limit_min - " << limit << " for " << tempj->name << std::endl;
				}


#ifdef DEBUGGING
				if (tempj->dof3_limit_min != 0 || tempj->dof3_limit_max != 0)
					std::cout << tempj->name << " dof3_limit_min: " << tempj->dof3_limit_min << std::endl;

#endif

#pragma endregion

#pragma region DOF3_LIMIT_MAX
				///degree of freedom 1
				element2 =element1->FirstChildElement("dof3_limit_max");
				//std::cout << "\n Last";
				while (element2 != nullptr){
					string dof_limit = element2->GetText();
					float limit = stof(dof_limit);
					tempj->dof3_limit_min = limit;
					element2 = element2->NextSiblingElement("dof3_limit_max");
					//std::cout << "Yo Found dof3_limit_max - " << limit << " for " << tempj->name;
				}

#ifdef DEBUGGING
				if (tempj->dof3_limit_min != 0 || tempj->dof3_limit_max != 0)
					std::cout << tempj->name << " dof3_limit_max: " << tempj->dof3_limit_max << std::endl;
#endif

#pragma region Tangent Magnitude
				element2 = element1->FirstChildElement("tan_mag");
				double intval;
				while (element2 != nullptr){
					element2->QueryDoubleText(&intval);
					std::cout << intval << std::endl;
					tempj->spline_tangent.push_back(intval);
					element2 = element2->NextSiblingElement("tan_mag");
				}

#pragma endregion



				//std::cout << "\n a";
				tempj->joint_circum_vec = j_circumferences;
				tempj->joint_circum_vec_info = j_circumferences_info;

				int hjkl = 0;
				element2 = element1->FirstChildElement("special_landmark1");
				if (element2 != NULL){
					tempj->special_landmark1 = element2->GetText();
					hjkl = 1;
					string special_landmark_info;
					if (element2->Attribute("info")){
						special_landmark_info = element2->Attribute("info");
						std::cout << endl << special_landmark_info;
						tempj->special_landmarks_vec_info.push_back(special_landmark_info);
					}

					if (!c_meta.hasLandmark(tempj->special_landmark1)){
						std::vector<std::string> values;
						values.push_back("Special Landmark");
						values.push_back(tempj->name);
						values.push_back(special_landmark_info);
						c_meta.ctrl_line.undefinedCLElements[tempj->special_landmark1] = values;
					}
				}
				//std::cout << "\n b";
				int hjkl1 = 0;
				element2 = element1->FirstChildElement("special_landmark2");
				if (element2 != NULL){
					tempj->special_landmark2 = element2->GetText();
					hjkl1 = 1;
					string special_landmark_info;
					if (element2->Attribute("info")){
						special_landmark_info = element2->Attribute("info");
						std::cout << endl << special_landmark_info;
						tempj->special_landmarks_vec_info.push_back(special_landmark_info);
					}

					if (!c_meta.hasLandmark(tempj->special_landmark2)){
						std::vector<std::string> values;
						values.push_back("Special Landmark");
						values.push_back(tempj->name);
						values.push_back(special_landmark_info);
						c_meta.ctrl_line.undefinedCLElements[tempj->special_landmark2] = values;
					}
				}
				//std::cout << "\n d";
				element2 = element1->FirstChildElement("spline");
				if (element2 != NULL){
					tempj->sp = true;
					tinyxml2::XMLElement* elem = element2->FirstChildElement("p0");
					string pt0 = elem->GetText();
					tempj->joint_splinep0 = pt0;
					tempj->spline_vector.push_back(pt0);
					string spline_info;
					if (elem->Attribute("info")){
						spline_info = elem->Attribute("info");
						tempj->spline_vec_info.push_back(spline_info);
					}
					if (!c_meta.hasLandmark(pt0)){
						std::vector<std::string> values;
						values.push_back("Spline Landmark");
						values.push_back(tempj->name);
						values.push_back(spline_info);
						c_meta.ctrl_line.undefinedCLElements[pt0] = values;
					}
					elem = element2->FirstChildElement("p1");
					string pt1 = elem->GetText();
					tempj->joint_splinep1 = pt1;
					tempj->spline_vector.push_back(pt1);
					if (elem->Attribute("info")){
						spline_info = elem->Attribute("info");
						tempj->spline_vec_info.push_back(spline_info);
					}
					if (!c_meta.hasLandmark(pt1)){
						std::vector<std::string> values;
						values.push_back("Spline Landmark");
						values.push_back(tempj->name);
						values.push_back(spline_info);
						c_meta.ctrl_line.undefinedCLElements[pt1] = values;
					}
					elem = element2->FirstChildElement("t1");
					string t1 = elem->GetText();
					tempj->joint_splinet1 = t1;
					tempj->spline_vector.push_back(t1);
					if (elem->Attribute("info")){
						spline_info = elem->Attribute("info");
						std::cout << endl << spline_info;
						tempj->spline_vec_info.push_back(spline_info);
					}
					if (!c_meta.hasLandmark(t1)){
						std::vector<std::string> values;
						values.push_back("Spline Landmark");
						values.push_back(tempj->name);
						values.push_back(spline_info);
						c_meta.ctrl_line.undefinedCLElements[t1] = values;
					}
					elem = element2->FirstChildElement("t2");
					string t2 = elem->GetText();
					tempj->joint_splinet2 = t2;
					tempj->spline_vector.push_back(t2);
					if (elem->Attribute("info")){
						spline_info = elem->Attribute("info");
						tempj->spline_vec_info.push_back(spline_info);
					}
					if (!c_meta.hasLandmark(t2)){
						std::vector<std::string> values;
						values.push_back("Spline Landmark");
						values.push_back(tempj->name);
						values.push_back(spline_info);
						c_meta.ctrl_line.undefinedCLElements[t2] = values;
					}
				}
				//ABSOLUTE ANGLE
				/*				std::vector<std::string> landmarkvec;
								if (tempj->flexionaxeslandmarks.size() == 2){
								landmarkvec.push_back(tempj->flexionaxeslandmarks.at(0));
								landmarkvec.push_back(tempj->flexionaxeslandmarks.at(1));
								std::cout << "joint angle computation" << std::endl;
								double angle = comptargets.computeJointAngle(c_meta, c_fem, landmarkvec, tempj->joint_splinet1, tempj->joint_splinet2);
								tempj->jointangle = angle;
								}
								*/
				element2 = element1->FirstChildElement("bodyregion");

				if (element2 == nullptr){
					return XML_ERROR_PARSING_ELEMENT;
				}
				//std::cout << "\n 7";
				tempbr->addChildJoint(tempj);

				if (c_meta.ctrl_line.root == NULL){
					c_meta.ctrl_line.root = tempbr;
				}
				//std::cout << "\n 8";


                tinyxml2::XMLError eResult = populateInfo(element2, c_meta, c_fem, tempj);
                if (eResult != tinyxml2::XML_SUCCESS){
                    return eResult;
                }
				element1 = element1->NextSiblingElement("joint");
			}

            return tinyxml2::XML_SUCCESS;
		}
        
        tinyxml2::XMLError ParseContourCL::readContourCLxml(FEModel& c_fem, hbm::Metadata& c_meta){
			c_meta.ctrl_line.root = nullptr;
			tinyxml2::XMLDocument xmldoc;

			tinyxml2::XMLError eResult = xmldoc.LoadFile("contourCL.xml");
			if (eResult != tinyxml2::XML_SUCCESS){
                return eResult;
			}

            XMLNode * pRoot = xmldoc.FirstChildElement("hbmSkeleton");
			if (pRoot == nullptr){
				return XML_ERROR_FILE_READ_ERROR;
			}

			XMLElement * pElement = pRoot->FirstChildElement("bodyregion");
			if (pElement == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}
            return populateInfo(pElement, c_meta, c_fem);
		}

        tinyxml2::XMLError ParseContourCL::readContourCLxml(FEModel& c_fem, hbm::Metadata& c_meta, const char* file){
			c_meta.ctrl_line.root = nullptr;
			tinyxml2::XMLDocument xmldoc;

			tinyxml2::XMLError eResult = xmldoc.LoadFile(file);
			if (eResult != tinyxml2::XML_SUCCESS){
                return eResult;
			}

			XMLNode * pRoot = xmldoc.FirstChildElement("hbmSkeleton");
			if (pRoot == nullptr){
				return XML_ERROR_FILE_READ_ERROR;
			}

			XMLElement * pElement = pRoot->FirstChildElement("bodyregion");
			if (pElement == nullptr){
				return XML_ERROR_PARSING_ELEMENT;
			}
            return populateInfo(pElement, c_meta, c_fem);
		}
	}
}
