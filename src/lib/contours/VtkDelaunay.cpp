/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "VtkDelaunay.h"

#include <vtkDelaunay3D.h>

using namespace std;
namespace piper {
    namespace contours {

    VtkDelaunay::VtkDelaunay() : delaunay_input_mesh(vtkSmartPointer<vtkUnstructuredGrid>::New()),delaunay_output_mesh(vtkSmartPointer<vtkUnstructuredGrid>::New())
    {
    	delaunay_input_mesh->Allocate();
    	delaunay_output_mesh->Allocate();
    }
	//Set VTK Unstructured Grid of points
	void VtkDelaunay::SetDataForDelaunay(const vector<hbm::Node>& node_list)
    {
		std::cout<< __LINE__ << __FUNCTION__ << std::endl;

        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

        // insert Points coordinates
		points->SetNumberOfPoints(node_list.size());

		for (auto i = 0; i < node_list.size() ; i++) {
			double coord[3];
			coord[0] = node_list[i].getCoordX();
			coord[1] = node_list[i].getCoordY();
			coord[2] = node_list[i].getCoordZ();
			points->SetPoint(i, coord);
		}
        //Set the unstructured mesh
        delaunay_input_mesh->SetPoints(points);
	}

//Compute the tets using VTK delaunay.
	void VtkDelaunay::ComputeDelaunay()
	{
		std::cout<< __LINE__ << __FUNCTION__ << std::endl;
        // Generate a tetrahedral mesh from the input points 
        vtkSmartPointer<vtkDelaunay3D> delaunay3D =
            vtkSmartPointer<vtkDelaunay3D>::New(); 
        delaunay3D->SetInputData(delaunay_input_mesh);
        delaunay3D->Update();
        //Copy data to the output mesh
        delaunay_output_mesh = delaunay3D->GetOutput();
	}


    void VtkDelaunay::GetTets(vector<tet_element>& vtk_tet_list )
    {
    	std::vector<hbm::Id> vertices;

        //Get the tet list 
        vtkSmartPointer<vtkCellArray> cellArray =
            vtkSmartPointer<vtkCellArray>::New(); 
        cellArray = delaunay_output_mesh->GetCells();


        //Traverse the cells
        vtkIdType numCells = cellArray->GetNumberOfCells();
        vtkIdType cellLocation = 0; // the index into the cell array
        for (vtkIdType i = 0; i < numCells; i++)
        {
        	vtkIdType numIds; // to hold the size of the cell
        	vtkIdType *pointIds; // to hold the ids in the cell
        	cellArray->GetCell(cellLocation, numIds, pointIds);
        	cellLocation += 1 + numIds;

        	//Populate the Contour lib Tet Container
        	for(vtkIdType j = 0; j < numIds; j++ ){
        		vertices.push_back((hbm::Id)pointIds[j]);
        	}

            tet_element new_tet((hbm::Id)i, vertices);
        	vertices.clear();
        	vtk_tet_list.push_back(new_tet);
        }

#if 0 // Another way of cell traversal
        Tetlist->InitTraversal();
        for (int j = 0; j < Tetlist.GetNumberOfCells(); j++){
        	Cellarray.GetNextCell(idlist);
        	double a = idlist.GetId(0);
        	double b = idlist.GetId(1);
        	double c = idlist.GetId(2);
        }
#endif
    }

}

}
 
