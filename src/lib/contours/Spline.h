/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_SPLINE_H
#define PIPER_CONTOURS_SPLINE_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif
#include "hbm/HumanBodyModel.h"

namespace piper {
	namespace contours {

		/**
		* @brief A class which involves the data structure of Spline.
		* It is intended for defining the tangent spline.  
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class CONTOURS_EXPORT Spline {
		public:
			Spline(void);
			~Spline(void);
			Spline(std::vector <hbm::Node> & points, std::vector <double> & u);

			std::string point0;
			std::string point1;
			std::string tangent1;
			std::string tangent2;


			std::vector <hbm::Node> IPs;		//input// 4 Interpolation points (points through with the curve should pass
			std::vector <double> ui;		//input// Assumed or computed Uis of the 4 interpolation points

			std::vector <hbm::Node> CPs;		// Controls points in case of bezier curve // should be computed
			//Matrix b_mat(int, int);	 // B the control points matrix

			std::vector <hbm::Node> Upd_CPs;		// Updated controls points in case of bezier curve // modify CPs according to the requirement and store in Upd_CPs


			std::vector <hbm::Node> ini_points;
			std::vector <hbm::Node> trans_points;



			//void compute_points_on_spline(spline s, int num);
			void compute_points_on_spline(int num);
			void compute_points_on_Upd_spline(int num);



/*			vector <point> IPs;		//input// 4 Interpolation points (points through with the curve should pass
			vector <double> ui;		//input// Assumed or computed Uis of the 4 interpolation points

			vector <point> CPs;		// Controls points in case of bezier curve // should be computed
			//Matrix b_mat(int, int);	 // B the control points matrix

			vector <point> Upd_CPs;		// Updated controls points in case of bezier curve // modify CPs according to the requirement and store in Upd_CPs


			vector <point> ini_points;
			vector <point> trans_points;



			//void compute_points_on_spline(spline s, int num);
			void compute_points_on_spline(int num);
			void compute_points_on_Upd_spline(int num);

			*/
		};



	}
}
#endif

