/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_SPACETRANSFORM_H
#define PIPER_CONTOURS_SPACETRANSFORM_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "hbm/HumanBodyModel.h"
#include "poly34.h"
#include "vector_func.h"
#define PI 3.1415926535897932384626433832795

namespace piper {
	namespace contours {

		/**
		* @brief A class which involves functions of 3d space transformations.
		* It is intended to work as an api for spatial transformatial functionalities
		* it covers only the necessarry functionalities involved in the current algorithm of repositioning module of contours lib.
		*
		* @author Aditya Chhabra @date 2016 
		*/

		class CONTOURS_EXPORT SpaceTransform {
		public:

			SpaceTransform();
			~SpaceTransform();


			/// <summary>
			/// Get a vector of transformed coordinates. Rotation of coordinates in space by an angle about an axis
			/// </summary>
			/// <param name="coordinate">A vector of coordinates of a point</param>
			/// <param name="angle">an angle by which the spatial transformation is performed.</param>
			/// <param name="axis">A matrix containing the coordinate information of the two axis points</param>
			/// <returns>A vector of transformed coordinates.</returns>
			Eigen::Vector3d ApplyRotation3d(Eigen::Vector3d coordinate, double angle, Eigen::MatrixXd axis);

			
			/// <summary>
			/// Compuation of the angle from the clinical data
			/// </summary>
			/// <returns>An angle by which anatomical axis will be twisted.</returns>
			double lagrangeInterpolatePoly(double pos[], double val[], int degree, double desiredPos);


			/// <summary>
			/// Convert Spline Matrix to Bezier Matrix.
			/// </summary>
			/// <param name="spline_matrix">A spline matrix having two points and two tangent vectors</param>
			/// <returns>A bezier matrix.</returns>
			Eigen::MatrixXd convertSplineToBezier(Eigen::MatrixXd spline_matrix);

			/// <summary>
			/// Get the plane equation froma three points.
			/// </summary>
			/// <param name="three_points">A matrix OF three points</param>
			/// <returns>A vector containg A,B,C,D coefficients of plane equation AX+BY+CZ+D=0.</returns>
			std::vector<double> getPlaneFromPoints(Eigen::MatrixXd three_points);


			std::vector<double> getPlaneFromPointNormal(std::vector<double> plane_point, std::vector<double> plane_normal);


			/// <summary>
			/// Determine the u value of a plane if it lies between the first and last control points of bezier curve.
			/// </summary>
			/// <param name="abcd">abcd is a vector of double containing the A,B,C and D of plane having an equation : AX+BY+CZ+D=0.</param>
			/// <param name="bezier_points">a matrix of bezier control points.</param>
			/// <returns>u value of the perametric bezier curve at a point where plane equation is AX+BY+CZ+D=0.</returns>
			double uValueOfPlane(Eigen::MatrixXd bezier_points, std::vector<double> abcd);

			hbm::Node perform_rotation(hbm::Node p, double theta, vectors A, vectors B);
			hbm::Node perform_translation(hbm::Node p, double amt, vectors n);

			///<summary>
			/// find the projection of a point onto a plane
			/// To find t such that(x + ta, y + tb, z + tc), (x, y, z), and(d, e, f) form a right angled triangle,
			/// with the first of these(the point you are looking for) being the right angle.You can do this with dot products, and this will give you
			///	t = ad−ax + be−by + cf−cza2 + b2 + c2.
			///	t = ad−ax + be−by + cf−cza2 + b2 + c2.
			///	Substitute this into(x + ta, y + tb, z + tc)(x + ta, y + tb, z + tc) and you have your result.

			hbm::Coord pointProjectionOnPlane(std::vector<double> normal, std::vector<double> point, std::vector<double> pointonplane);


			bool pointOnsameSideOfPlane(std::vector<double> point1, std::vector<double> point2, std::vector<double> plane_point, std::vector<double> plane_normal);

			bool pointOnsameSideOfPlane(std::vector<double> point1, std::vector<double> point2, std::vector<double> planeeq);

		};
	}
}
#endif

