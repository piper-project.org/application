/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef PIPER_CONTOURS_JOINT_H
#define PIPER_CONTOURS_JOINT_H

#ifdef WIN32
#	ifdef contours_EXPORTS
#		define CONTOURS_EXPORT __declspec( dllexport )
#	else
#		define CONTOURS_EXPORT __declspec( dllimport )
#	endif
#else
#	define CONTOURS_EXPORT
#endif

#include "Spline.h"
#include "BodyRegion.h"

namespace piper {
	namespace contours {

		/**
		* @brief A class which involves the data structure of Joint.
		* It is intended for defining the joints and edges(bones). Object of HbmSkeleton should be prepopulated from the landmark information provided externally. 
		* it currently contains the datastructure for HingeJoint type which has 1 DOF.
		*
		* @author Aditya Chhabra @date 2016 
		*/
		class BodyRegion;
		class Bone;

		class CONTOURS_EXPORT Joint {
		public:

			Joint();
			~Joint();

			std::string j_name;
			Spline joint_spline;
			BodyRegion* parent_body_region; 
			BodyRegion* child_body_region;

			Joint* children;

			Joint* left;
			Joint* right;
			Joint* middle;

			bool sp = false;

			std::string brname;
			std::string circum;
			std::string special_landmark1;
			std::string special_landmark2;

			hbm::FEFrame fm;

			Eigen::MatrixXd proximalbr;
			Eigen::MatrixXd distalbr;

			std::string proximal_landmark;
			std::string distal_landmark;

			std::string entityjointname;
			std::array<bool, 3> hierarchy_side;
			int depth;
			/// \todo Define a function to compute the initial Joint Angle 
			double joint_angle;
			std::vector<hbm::Contour*> jcontour;
			std::string parent_entity_name, child_entity_name;
			std::array<bool, 6> c_dof;
			std::array<double, 3> c_max_angle;
			std::array<double, 3> c_min_angle;
			hbm::EntityJoint* c_entity_joint;
			double joint_center;
			std::vector<hbm::Landmark*> flexion_axis;
			std::vector<Bone*> associated_parent_bone;
			std::vector<Bone*> associated_child_bone;
			std::vector<Bone*> associated_bone;

//		private:
			std::string name;
			BodyRegion* parentbodyregion;
			BodyRegion* childbodyregion;
//		public:
			void setName(std::string jtname);
			void setChildBodyRegion(BodyRegion*);
			void setParentBodyRegion(BodyRegion*);

		};



	}
}
#endif

