/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "SpinePredictor.h"

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

        SpinePredictor::SpinePredictor() {
		}

        SpinePredictor::~SpinePredictor()
		{
		}
				
		std::vector<double> SpinePredictor::cervicalTwist(std::vector<double> fracAngle, double angle){

			//setting the y coordinate as zero

			double disfrac = 0;
            
			for (size_t i = 0; i<fracAngle.size(); i++)
				disfrac += fracAngle[i];			

			double rel_rotation = angle / disfrac;

			std::vector<double> twistAngle;

			for (size_t i = 0; i < fracAngle.size(); i++){
				twistAngle.push_back(rel_rotation*fracAngle.at(i));
			}

			return twistAngle;
		}

		std::pair<std::vector<NodePtr>, std::vector<double>> SpinePredictor::cervicalLateralFlexion(std::vector<hbm::NodePtr> cstore, double angle, Id FrameId,
            std::vector<double> fracAngle[3], hbm::FEModel& c_fem)
        {					
			FramePtr axis = std::make_shared<FEFrame>();
			NodePtr a = c_fem.get<Node>(axis->getOriginId());

			Coord axisa = c_fem.get<Node>(axis->getOriginId())->get();
			//					framein << std::endl << "frame2 origin " << klm[0] << "  " << klm[1] << "  " << klm[2] << std::endl;

			Coord axisb = c_fem.get<Node>(axis->getFirstDirectionId())->get();
			//					framein << std::endl << "frame2 1st dir " << klm1[0] << "  " << klm1[1] << "  " << klm1[2] << std::endl;

			Coord axisc = c_fem.get<Node>(axis->getPlaneId())->get();

			double vecx, vecy, vecz;
			vecx = axisa[0] + axisb[0];
			vecy = axisa[1] + axisb[1];
			vecz = axisa[2] + axisb[2];
	
			vectors a1(vecx, vecy, vecz);
					
			vecx = axisa[0] + axisc[0];
			vecy = axisa[1] + axisc[1];
			vecz = axisa[2] + axisc[2];

			vectors a2(vecx, vecy, vecz);

			vectors a3 = a1.cross(a2);


			//setting the y coordinate as zero
/*					for (int i = 0; i < cstore.size(); i++)
			{
				double y = 0;
				cstore[i]->setCoord(cstore[i]->getCoordX(), y, cstore[i]->getCoordZ());
			}
			*/
			// disfrac was just used for testing purposes
			double disfrac = 0;

			for (size_t i = 0; i < fracAngle[0].size(); i++)
			{
				disfrac += fracAngle[0][i];
			}
			// rel_rot is fraction of angle to be distributed to different rotation
			double rel_rotation = angle / disfrac;
					
			std::ofstream spinepredict("spinepredict.k");

			spinepredict << "*NODE";
				

			int knum = 0;
			//original
			for (size_t i = 0; i < cstore.size(); i++)
			{
				spinepredict << endl << setw(8) << knum++ << setw(16) << cstore.at(i)->getCoordX() << setw(16) << cstore.at(i)->getCoordY() << setw(16) << cstore.at(i)->getCoordZ();
			}
			// rotation of centroids
			for (size_t i = 0; i < cstore.size(); i++)
			{
//						cstore.at(i) = repo.perform_rotation(cstore.at(i), rel_rotation*fracAngle[0][i], vectors(cstore[cstore.size() - 1]), vectors(cstore[cstore.size() - 1]).operator+(vectors(2, 0, 0))); // comment here
				repo.perform_rotation(cstore.at(i), rel_rotation*fracAngle[0][i], vectors(cstore[cstore.size() - 1]), vectors(cstore[cstore.size() - 1]).operator+(a3)); // comment here
						spinepredict << endl << setw(8) << knum++ << setw(16) << cstore.at(i)->getCoordX() << setw(16) << cstore.at(i)->getCoordY() << setw(16) << cstore.at(i)->getCoordZ();
			}
			spinepredict << std::endl << "*End";

			std::ofstream spinepredict23("spinepredict23.k");
			spinepredict23 << "*NODE";
			knum = 0;
			for (size_t i = 0; i < cstore.size(); i++){
				spinepredict23 << endl << setw(8) << knum++ << setw(16) << cstore.at(i)->getCoordX() << setw(16) << cstore.at(i)->getCoordY() << setw(16) << cstore.at(i)->getCoordZ();
			}
			spinepredict23 << endl << "*END";


			std::vector<double> twistAngle;
			twistAngle = cervicalTwist(fracAngle[2], angle);

			std::pair<std::vector<NodePtr>, std::vector<double>> spineop;
			spineop = make_pair(cstore, twistAngle);

			return spineop;
	    }

		std::pair<std::vector<Eigen::MatrixXd>, std::vector<double>> SpinePredictor::cervicalLateralFlexionBasic(std::vector<Eigen::MatrixXd> cstore, double angle, std::vector<double> fracAngle[3], Eigen::MatrixXd axis){

			//setting the y coordinate as zero
			for (size_t i = 0; i < cstore.size(); i++)
			{
				double y = 0;
				cstore.at(i)(1,0) = 0.0;
			}


			std::vector<Eigen::MatrixXd> modcstore;
			modcstore = cstore;

			double disfrac = 0;

			for (size_t i = 0; i<fracAngle[0].size(); i++)
			{
				disfrac += fracAngle[0][i];
			}


			double rel_rotation = angle / disfrac;

			std::ofstream spinepredict("spinepredict.k");

			spinepredict << "*NODE";


			int knum = 0;
			//original
			for (size_t i = cstore.size() - 1; i>0; i--)
			{
				spinepredict << endl << setw(8) << knum++ << setw(16) << cstore.at(i)(0,0) << setw(16) << cstore.at(i)(1,0) << setw(16) << cstore.at(i)(2,0);
			}
			// rotation of centroids
			for (size_t i = cstore.size() - 1; i>0; i--)
			{
				modcstore.at(i) = trans.ApplyRotation3d(cstore.at(i),rel_rotation, axis);
				spinepredict << endl << setw(8) << knum++ << setw(16) << cstore.at(i)(0, 0) << setw(16) << cstore.at(i)(1, 0) << setw(16) << cstore.at(i)(2, 0);
			}
			spinepredict << std::endl << "*End";


			std::vector<double> twistAngle;
			twistAngle = cervicalTwist(fracAngle[2], angle);

			std::pair<std::vector<Eigen::MatrixXd>, std::vector<double>> spineop;
			spineop = make_pair(modcstore, twistAngle);

			return spineop;					
		}
	}
}
