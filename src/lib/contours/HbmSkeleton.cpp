/*******************************************************************************
* Copyright (C) 2017 FITT                                                      *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Aditya Chhabra, Sachiv Paruchuri, Dhruv Kaushik,        *
* Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT)                           *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "HbmSkeleton.h"
#include <queue>

using namespace std;
using namespace piper::hbm;
namespace piper {
	namespace contours {

		HbmSkeleton::HbmSkeleton() {
		}

		HbmSkeleton::~HbmSkeleton()
		{
		}

		void HbmSkeleton::insertskeleton(string jointname){
			Joint* temp = head;
			temp->j_name = jointname;
			if (head != NULL) temp->children = head;
			head = temp;
		}

		void HbmSkeleton::printSkeleton(){
			Joint* temp = head;
			cout << "list is: ";
			while (temp != NULL){
				cout << temp->j_name << endl;
				temp = temp->children;
			}
		}

		void HbmSkeleton::insertjoint(string nodename){
			Joint* temp = head;
			temp->j_name = nodename;
			if (head != NULL) temp->child_body_region = headd;
			head = temp;
		}

		void HbmSkeleton::insertFirstNode(std::string nodename){
			BodyRegion* temp =	NULL;
			temp->b_name = nodename;
			root = temp;
		}

		void HbmSkeleton::printnode(){
			BodyRegion* temp = root;
			Joint* tempo = temp->child;
			cout << "list is " << endl;
			while (tempo != NULL){
				cout << temp->b_name;
				tempo = temp->child;
			}
		}

		void HbmSkeleton::insertHbmSkelHead(string jname){
			Joint* temp = new Joint;
			temp->j_name = jname;
  			head = temp;
		}

		void HbmSkeleton::printHeadSkele(){
			Joint* temp = head;
			cout << endl << endl << temp->j_name << endl;
		}


		void HbmSkeleton::printwhole(Joint* jnt){
			Joint* temp = head;
			while (temp != NULL){
				cout << "list is " << endl;
				cout << temp->j_name << endl;
				temp = temp->right;
			}
		}

		void HbmSkeleton::printtreeparse(hbm::FEModel& c_fem, hbm::Metadata& c_meta){
			Joint* temp = head;
			std::ofstream piku("piku.txt");

			piku << "list is " << endl;
			std::queue<Joint*> local;
			local.push(head);
			while (local.size() != 0){
				Joint* temp1 = local.front();
				hbm::SId newnode;
				piku << temp1->j_name << endl;
				piku << temp1->brname << endl;
				if (temp1->sp){
					

					piku << endl << "ab nest" << endl;
					piku << temp1->joint_spline.point0;
					piku << temp1->joint_spline.point1;
					piku << endl << "ab nest" << endl;

					piku << "point0 spline" << endl;
					Eigen::MatrixXd p0(3,1);
					Eigen::MatrixXd p1(3, 1);
					Eigen::MatrixXd z1(3, 1);
					Eigen::MatrixXd z2(3, 1);
					Eigen::MatrixXd tant1(3, 1);
					Eigen::MatrixXd tant2(3, 1);

					Eigen::MatrixXd cir1(3, 1);


			

					//ContoursHbmInterface conthbm;
					hbm::SId gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->joint_spline.point0);
					piku << endl << "sizeee" << gnode.size() << endl;
					std::set<Id>::iterator it;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						piku << "  " << x << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						p0 << iter->getCoordX()
							, iter->getCoordY()
							, iter->getCoordZ();
					}

					piku << "point 1 spline" << endl;

					/********************** special landmark *****************************/

					NodePtr newnode1 = std::make_shared<Node>();
                    NodePtr newnode2 = std::make_shared<Node>();
                    NodePtr newnode3 = std::make_shared<Node>();

					//ContoursHbmInterface conthbm;
					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->special_landmark1);
					piku << endl << "special landmark1  " << temp1->special_landmark1 << "  "  << "size " << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						double ax, ay, az;
						ax = iter->getCoordX();
						ay = iter->getCoordY();
						az = iter->getCoordZ();
						piku << "  " << x << "     " << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						newnode1->setCoord(ax, ay, az);

					}


					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->distal_landmark);
					piku << endl << "distal landmark  " << temp1->distal_landmark << "  " << "size " << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						double ax, ay, az;
						ax = iter->getCoordX();
						ay = iter->getCoordY();
						az = iter->getCoordZ();
						piku << "  " << x << "     " << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						newnode3->setCoord(ax, ay, az);

					}

					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->special_landmark2);
					piku << endl << "special landmark2  " << temp1->special_landmark2 << "  " << "size " << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						double ax, ay, az;
						ax = iter->getCoordX();
						ay = iter->getCoordY();
						az = iter->getCoordZ();
						piku << "  " << x <<  "     " << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						newnode2->setCoord(ax, ay, az);
					}

					if (gnode.size() != 0){
						std::vector<hbm::Id> abc;
						abc.push_back(newnode1->getId());
						abc.push_back(newnode2->getId());
						abc.push_back(newnode3->getId());

						hbm::Id tu = conthbm.register_new_frame(abc, c_fem);

						piku << "/*************************************************/" << std::endl;
                        hbm::FramePtr as = std::make_shared<FEFrame>();
						as = c_fem.get<FEFrame>(tu);
						temp1->fm = *c_fem.get<FEFrame>(tu);

						piku << "    getId : " << as->getId();
						piku << " 1:  " << as->getFirstDirectionId();
						piku << " 2:  " << as->getPlaneId();
						piku << " 3:  " << as->getOriginId();


						/*					piku << "getId : " << temp1->fm.getId();
											gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->special_landmark1);
											if (gnode.size() != 0){
											piku << "1:  " << temp1->fm.getFirstDirectionId() << "  cordfirst  " << c_fem.getNode(temp1->fm.getFirstDirectionId()).getCoordX();
											piku << "2:  " << temp1->fm.getPlaneId() << "  cordplane  " << c_fem.getNode(temp1->fm.getPlaneId()).getCoordX();
											piku << "3:  " << temp1->fm.getOriginId() << " cordorigin " << c_fem.getNode(temp1->fm.getOriginId()).getCoordX();
											}
											*/
						piku << "/*************************************************/" << std::endl;

					}
					/********************** special landmark *****************************/

					//ContoursHbmInterface conthbm;
					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->joint_spline.point1);
					piku << endl << "sizeee" << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						piku << "  " << x << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						p1 << iter->getCoordX()
							, iter->getCoordY()
							, iter->getCoordZ();
					}


					//ContoursHbmInterface conthbm;
					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->joint_spline.tangent1);
					piku << endl << "sizeee" << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						piku << "  " << x << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						z1 << iter->getCoordX()
							, iter->getCoordY()
							, iter->getCoordZ();

					}


					//ContoursHbmInterface conthbm;
					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->joint_spline.tangent2);
					piku << endl << "sizeee" << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						piku << "  " << x << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						z2 << iter->getCoordX()
							, iter->getCoordY()
							, iter->getCoordZ();

					}

					
					// for circumcoordiinates
					gnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->circum);
					piku << endl << "sizeee of  " << temp1->circum << "  "  << gnode.size() << endl;
					for (it = gnode.begin(); it != gnode.end(); ++it)
					{
						int x = *it;
						NodePtr iter = c_fem.get<Node>(*it);
						piku << "  " << x << iter->getCoordX() << "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
						cir1 << iter->getCoordX()
							, iter->getCoordY()
							, iter->getCoordZ();
					}

					piku << endl << endl << cir1 << endl;



					tant1 << (p0(0, 0) - z1(0, 0)) / 1.5
						, (p0(1, 0) - z1(1, 0)) / 1.5
						, (p0(2, 0) - z1(2, 0)) / 1.5;


					tant2 << (z2(0, 0) - p1(0, 0)) / 1.2
						, (z2(1, 0) - p1(1, 0)) / 1.2
						, (z2(2, 0) - p1(2, 0)) / 1.2;


					piku << "/****************************************************************" << endl;
					piku << p0 <<endl;
					piku << p1 << endl;
					piku << tant1 << endl;
					piku << tant2 << endl;
					piku << "/****************************************************************" << endl;


					EntityJoint as1(temp1->entityjointname);
					std::array<bool, 6> mdof = as1.getDof();
//					cout << mdof[0,0] << endl;
//					cout << mdof[1] << endl;
//					cout << mdof[2] << endl;
//					cout << mdof[3] << endl;
					piku << "piperid" << as1.getEntity1FrameId() << endl;  
					piku << "piperid" << as1.getEntity2FrameId() << endl; //0
				}

				piku << temp1->proximal_landmark;


				//ContoursHbmInterface conthbm;
				newnode = conthbm.getLandmarkNodeids(c_fem,c_meta, temp1->proximal_landmark);
				std::set<Id>::iterator it;
				for (it = newnode.begin(); it != newnode.end(); ++it)
				{
					int x = *it; 
					NodePtr iter = c_fem.get<Node>(*it);
					piku << "  " << x << iter->getCoordX() << "  "  << iter->getCoordY() << "  " << iter->getCoordZ() << endl;

				}
				piku << temp1->distal_landmark;

				std::set<Id>::iterator it2;
				newnode = conthbm.getLandmarkNodeids(c_fem, c_meta, temp1->distal_landmark);
				for (it = newnode.begin(); it != newnode.end(); ++it)
				{
					int x = *it;
					NodePtr iter = c_fem.get<Node>(*it);
					piku << "  " << x << iter->getCoordX()
						<< "  " << iter->getCoordY() << "  " << iter->getCoordZ() << endl;
				}

				local.pop();
				if (temp1->left != NULL)
					local.push(temp1->left);
				if (temp1->middle != NULL)
					local.push(temp1->middle);
				if (temp1->right != NULL)
					local.push(temp1->right);
					
			}

		}


		void HbmSkeleton::finalprint(){
			Joint* temp = head;
			Joint* temp1;

			while (temp != NULL){
				cout << "list is " << endl;
				cout << temp->j_name << endl;
				temp = temp->left;
				if (temp->right != NULL){
					temp1 = temp->right;
					while (temp1 != NULL){
						temp1 = temp1->left;
						cout << temp1->j_name;
					}
				}
				if (temp->middle != NULL){
					temp1 = temp->right;
					while (temp1 != NULL){
						temp1 = temp1->left;
						cout << temp1->j_name;
					}
				}
			}

			

		}


	}
}
