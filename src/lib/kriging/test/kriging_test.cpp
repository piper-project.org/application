/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/

#include "kriging/KrigingPiperInterface.h"
#include <vtkMath.h>

#include <boost/filesystem.hpp>

#include "gtest/gtest.h"

// both of the following macros should be turned off in publicly distributed code, i.e. turn them off before pushing the code to the master branch of Piper git repository
// they are used for tests made for comparison of different Kriging settings rather then automated software tests or can be used to get data for designing more automated tests
#define PIPER_TRANS_TEST_ALLOWFILEWRITE 0 // turn on to allow the results of transformation test to be written to files; make sure to modify the output paths to suit you (in code; search for this macro...)
#define PIPER_TRANS_TEST_DEBUGVIS 0 // debug visualization for transformation tests
#if PIPER_TRANS_TEST_DEBUGVIS
#include <vtkOpenGLActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkGlyph3DMapper.h>
#include <vtkSphereSource.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkDataSetSurfaceFilter.h>
#endif

#include <vtkMeshQuality.h>

using namespace piper::hbm;

using namespace piper;

std::unique_ptr<HumanBodyModel> hbm_ptr;

TEST(Kriging_test, testMove3DPointsToFEModel){
    std::string modelFile = "model_01.pmd";
    HumanBodyModel hbmCOPY(modelFile);
	size_t initialnbnode = hbmCOPY.fem().getNbNode();
	int lastid = Node::getLastId();
    size_t nbnodeSrc = hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getControlPt3dCont().size();
    size_t nbnodeTarget = hbmCOPY.metadata().interactionControlPoint("controlpoint_1")->getControlPt3dCont().size();
    hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->move3DPointsToFEModel(hbmCOPY.fem());
    hbmCOPY.metadata().interactionControlPoint("controlpoint_1")->move3DPointsToFEModel(hbmCOPY.fem());
    EXPECT_EQ(nbnodeSrc, hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getGroupNodeId().size());
    EXPECT_EQ(nbnodeTarget, hbmCOPY.metadata().interactionControlPoint("controlpoint_1")->getGroupNodeId().size());
    EXPECT_EQ(hbmCOPY.fem().getNbNode(), initialnbnode + nbnodeSrc + nbnodeTarget);

    EXPECT_EQ(lastid + 1, *hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getGroupNodeId().begin());
	EXPECT_EQ(lastid + nbnodeSrc,
        *hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getGroupNodeId().rbegin());

    EXPECT_EQ(lastid + 1 + nbnodeSrc, *hbmCOPY.metadata().interactionControlPoint("controlpoint_1")->getGroupNodeId().begin());
	EXPECT_EQ(lastid + nbnodeSrc + nbnodeTarget,
        *hbmCOPY.metadata().interactionControlPoint("controlpoint_1")->getGroupNodeId().rbegin());

    EXPECT_EQ(0.3, hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getAssociationSkinGlobal());
    EXPECT_EQ(0.7, hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getAssociationBonesGlobal());
    EXPECT_EQ(5, hbmCOPY.metadata().interactionControlPoint("controlpoint_0")->getAssociationSkin()[4]);
}



TEST(Kriging_test, testModelDataForKriging){
    EXPECT_EQ(10160,hbm_ptr->fem().getNbNode());
    EXPECT_EQ(2,hbm_ptr->metadata().interactionControlPoints().size());
    Metadata::InteractionControlPointCont ctrlPtCont = hbm_ptr->metadata().interactionControlPoints();
    int nbTarget=0, nbSrc=0;
    for (auto i = begin(ctrlPtCont); i != end(ctrlPtCont); ++i) { 
        EXPECT_EQ(202,(*i).second.getControlPt3dCont().size());
        nbSrc+=(*i).second.getControlPointRole()==InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE?1:0;
        nbTarget+=(*i).second.getControlPointRole()==InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET?1:0;
    }
    EXPECT_EQ(1,nbSrc); 
    EXPECT_EQ(1,nbTarget);
    EXPECT_EQ(InteractionControlPoint::ControlPointRole::CONTROLPOINT_SOURCE, hbm_ptr->metadata().interactionControlPoints().at("controlpoint_0").getControlPointRole());
    EXPECT_EQ(InteractionControlPoint::ControlPointRole::CONTROLPOINT_TARGET, hbm_ptr->metadata().interactionControlPoints().at("controlpoint_1").getControlPointRole());
}

TEST(Kriging_test, testKrigingRandomData) {
        KrigingPiperInterface kriging;
        kriging.testWithRandomValues(10,3);
        EXPECT_GE(4e-14,kriging.getReprojectionRelativeError());
 }

TEST(Kriging_test, solveKrigingSystem){
    KrigingPiperInterface kriging;
    kriging.setControlPoints(hbm_ptr->metadata().interactionControlPoint("controlpoint_0")->getControlPt3dCont(),
        hbm_ptr->metadata().interactionControlPoint("controlpoint_1")->getControlPt3dCont(), 0, false, true);
    kriging.compute();
    EXPECT_GE(4e-14,kriging.getReprojectionRelativeError());
}

TEST(Kriging_test, applyDeformationKrigingSystem){
    KrigingPiperInterface kriging;
    kriging.setControlPoints(hbm_ptr->metadata().interactionControlPoint("controlpoint_0")->getControlPt3dCont(),
        hbm_ptr->metadata().interactionControlPoint("controlpoint_1")->getControlPt3dCont(), 0, false, true);
    kriging.compute();
    kriging.clearSystemMatrix();
    kriging.applyDeformation(hbm_ptr->fem().getNodes(),&hbm_ptr->fem().getNodes());
    boost::filesystem::path tmp02 = boost::filesystem::temp_directory_path();
    tmp02 /= "piper_kriging_test_applyDeformationKrigingSystem.vtu";

    piper::hbm::FEModel m;
    hbm_ptr->fem().writeModel(tmp02.string());
    m.readModel(tmp02.string());
    EXPECT_GE(1e-6,kriging.getRelativeError(m.getNodes()));
    piper::hbm::FEModel m2;
    std::string cmprModel = "Kriging_model_01_transformed_linear.vtu";
    m2.readModel(cmprModel);
    EXPECT_GE(3e-3,kriging.getRelativeError(m2.getNodes()));
    m.clear();
    m2.clear();
}

TEST(Kriging_test, applyDeformationKrigingSystemWithDuplicatedCtrlPt){
    std::string modelFile="Kriging_model_01-withCtrlPtDuplication.pmd";
    HumanBodyModel hbm(modelFile);
 
    KrigingPiperInterface kriging;
    kriging.setControlPoints(hbm.metadata().interactionControlPoint("controlpoint_0")->getControlPt3dCont(),
        hbm.metadata().interactionControlPoint("controlpoint_1")->getControlPt3dCont(), 0, false, true);
    kriging.compute(true,1e-04);
    kriging.clearSystemMatrix();
    kriging.applyDeformation(hbm.fem().getNodes(),&hbm.fem().getNodes());
    piper::hbm::FEModel m2;
    std::string cmprModel = "Kriging_model_01_transformed_linear.vtu";
    m2.readModel(cmprModel);
    EXPECT_GE(3e-3,kriging.getRelativeError(m2.getNodes()));
    m2.clear();
}

int evaluateMeshNegVolumes(vtkSmartPointer<vtkUnstructuredGrid> mesh)
{
    vtkSmartPointer<vtkMeshQuality> qual = vtkSmartPointer<vtkMeshQuality>::New();
    qual->SetInputData(mesh);
    qual->SetHexQualityMeasureToVolume();
    qual->Update();
    vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(qual->GetOutput()->GetCellData()->GetArray("Quality"));
    int nNeg = 0;
    for (vtkIdType i = 0; i < quality->GetNumberOfTuples(); i++)
    {
      //  std::cout << quality->GetValue(i) << " ";
        if (quality->GetValue(i) < 0)
            nNeg++;
    }
    return nNeg;
}

// for debugging / generating test data only
void writeScaledJacobian(vtkSmartPointer<vtkUnstructuredGrid> mesh, std::string outputFilePath)
{
    vtkSmartPointer<vtkMeshQuality> qual = vtkSmartPointer<vtkMeshQuality>::New();
    qual->SetInputData(mesh);
    qual->SetHexQualityMeasureToScaledJacobian();
    qual->Update();
    vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(qual->GetOutput()->GetCellData()->GetArray("Quality"));
    int nNeg = 0;
    ofstream f;
    f.open(outputFilePath);
    for (vtkIdType i = 0; i < quality->GetNumberOfTuples(); i++)
    {
        f << quality->GetValue(i) << "\n";
    }
    f.flush();
    f.close();
}

// for debugging / generating test data only
void meshDisplacementMap(vtkSmartPointer<vtkPoints> reference, vtkSmartPointer<vtkPoints> transformed, std::string filepath)
{
    std::map < vtkIdType, std::pair<double, double>> histogramDx, histogramDy, histogramDz; // displacements of each node coupled with their distance to the center
    double refPoint[3], transPoint[3];
    ofstream histogramFile;
    histogramFile.open(filepath);
    for (vtkIdType i = 0; i < reference->GetNumberOfPoints(); i++)
    {
        reference->GetPoint(i, refPoint);
        if (refPoint[2] < 0 || (refPoint[0] > 0.05 || refPoint[0] < -0.05))
            continue;
        transformed->GetPoint(i, transPoint);
        double distToCenter = refPoint[2];
        histogramDx[i] = std::pair<double, double>(distToCenter, refPoint[0] - transPoint[0]);
        histogramDy[i] = std::pair<double, double>(distToCenter, refPoint[1] - transPoint[1]);
        histogramDz[i] = std::pair<double, double>(distToCenter, refPoint[2] - transPoint[2]);
        histogramFile << distToCenter << " " << histogramDx[i].second << " " << histogramDy[i].second << " " << histogramDz[i].second << std::endl;
    }
    histogramFile.close();
}

// this functions is used with several test scenarios to check mesh quality after transformation using scaled jacobian
// the test is passed if the mesh quality is similar or better than what was measured before
// this means it will notify us if some change in the kriging code leads to worse meshes, but will not necessarily tell us when a given method is better
void testMeshQuality(vtkSmartPointer<vtkUnstructuredGrid> mesh, double expAvg, double expMin)
{
    vtkSmartPointer<vtkMeshQuality> qual = vtkSmartPointer<vtkMeshQuality>::New();
    qual->SetInputData(mesh);
    qual->SetHexQualityMeasureToScaledJacobian();
    qual->Update();
    vtkSmartPointer<vtkDoubleArray> quality = vtkDoubleArray::SafeDownCast(qual->GetOutput()->GetCellData()->GetArray("Quality"));
    double avg = 0, min = std::numeric_limits<double>::max();
    for (vtkIdType i = 0; i < quality->GetNumberOfTuples(); i++)
    {
        double val = quality->GetValue(i);
        if (val < min) min = val;
        avg += val;
    }
    avg /= quality->GetNumberOfTuples();
    // expect the same or better results to pass the test
    EXPECT_GE(avg, expAvg);
    EXPECT_GE(min, expMin);
}

TEST(Kriging_test, hollowCylinderTransformation){
    vtkIdType nEleHeight = 20;
    vtkIdType nEleDepth = 20;
    vtkIdType nEleCirc = 20;
    double rInternal = 10, rExternal = 50; // radiuses
    double height = 200;
    vtkIdType id = 0;

    double dR = 2 * vtkMath::Pi() / nEleCirc;
    double dDepth = (rExternal - rInternal) / nEleDepth;
    double dHeight = height / nEleHeight;
  //  if opt_rep>0 then w = r + (R - r)*linspace(0, 1, n_int).^opt_rep;
  //  if opt_rep<0 then w = R + (r - R)*linspace(0, 1, n_int).^abs(opt_rep); w = w($:-1 : 1);
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    // assemble the cylinder from bottom to top
    for (int curHeight = 0; curHeight <= nEleHeight; curHeight++) // <= because we need to build the top as well - there is nEleHeight+1 "levels"
    {
        double y = curHeight * dHeight;
        for (int i = 0; i <= nEleDepth; i++) // from inside to outside; <= because there is nEleDepth+1 circles on each level
        {
            double curRadius = rInternal + i * dDepth;
            for (int j = 0; j < nEleCirc; j++) // along the circle
            {
                double curAngle = j * dR;
                points->InsertNextPoint(curRadius * cos(curAngle), y, curRadius * sin(curAngle));
            }
        }
    }
    // create the elements
    vtkSmartPointer<vtkUnstructuredGrid> cylinder = vtkSmartPointer<vtkUnstructuredGrid>::New();
    cylinder->Allocate();
    cylinder->SetPoints(points);
    vtkIdType ptIds[8];
    vtkIdType nVertPerLevel = nEleCirc * (nEleDepth + 1);
    vtkIdType levelOffset = 0;
    for (int curLevel = 0; curLevel < nEleHeight; curLevel++, levelOffset += nVertPerLevel)
    {
        vtkIdType circleOffset = 0;
        for (int curDepth = 0; curDepth < nEleDepth; curDepth++, circleOffset += nEleCirc) // nEleCirc == number of vertices per one circle
        {
            vtkIdType offset = circleOffset + levelOffset;
            for (int curEle = 0; curEle < nEleCirc; curEle++)
            {
                bool isLast = curEle == (nEleCirc - 1);
                // orientation is important for computing quality metrics
                ptIds[0] = offset + curEle; // bottom front left corner
                ptIds[1] = ptIds[0] + nEleCirc; // bottom front right - it's on the next circle
                ptIds[2] = ptIds[1] + nVertPerLevel; // top front right
                ptIds[3] = ptIds[0] + nVertPerLevel; // top front left
                ptIds[4] = isLast ? offset : ptIds[0] + 1; // bottom left rear - the next one; if its the last, the index on the circle (curEle) is 0
                ptIds[5] = isLast ? offset + nEleCirc : ptIds[1] + 1; // bottom rear right - on the next circle, one dR ahead; if its the last, the index on the circle (curEle) is 0
                ptIds[6] = ptIds[5] + nVertPerLevel; // top rear right
                ptIds[7] = ptIds[4] + nVertPerLevel; // top rear left
                cylinder->InsertNextCell(VTK_HEXAHEDRON, 8, ptIds);
            }
        }
    }

    // get control point indices - take two sets, the surface points on two halves of the cylinder (+ and - z coordintes)
    vtkSmartPointer<vtkIdList> CPs1Inner = vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> CPs1Outer = vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> CPs2Inner = vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> CPs2Outer = vtkSmartPointer<vtkIdList>::New();
    levelOffset = 0;
    vtkIdType lastCircleOffset = nEleCirc * nEleDepth;
    vtkIdType nEleCircHalf = nEleCirc / 2;
    for (int curLevel = 0; curLevel <= nEleHeight; curLevel++, levelOffset += nVertPerLevel)
    {    
        for (vtkIdType curEle = 0; curEle < nEleCircHalf; curEle++)
            CPs1Inner->InsertNextId(levelOffset + curEle);
        for (vtkIdType curEle = 0; curEle < nEleCircHalf; curEle++)
            CPs1Outer->InsertNextId(lastCircleOffset + levelOffset + curEle);

        for (vtkIdType curEle = nEleCircHalf; curEle < nEleCirc; curEle++)
            CPs2Inner->InsertNextId(levelOffset + curEle);
        for (vtkIdType curEle = nEleCircHalf; curEle < nEleCirc; curEle++)
            CPs2Outer->InsertNextId(lastCircleOffset + levelOffset + curEle);
    }
    
    // TEST 1 - fix half of the points, move the other half
    // set the control points for transformation
    VCoord source, target;
    for (vtkIdType i = 0; i < CPs1Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1Inner->GetId(i)));
        source.push_back(c);
        target.push_back(c); // first test = fix half of the points, move the other half
    }
    for (vtkIdType i = 0; i < CPs2Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2Inner->GetId(i)));
        source.push_back(c);
        target.push_back(c); // first test = fix half of the points, move the other half
    }
    for (vtkIdType i = 0; i < CPs1Outer->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1Outer->GetId(i)));
        source.push_back(c);
        target.push_back(c); // first test = fix half of the points, move the other half
    }
    for (vtkIdType i = 0; i < CPs2Outer->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2Outer->GetId(i)));
        source.push_back(c);
        c[2] -= 20;// the second half points are in the negative Z direction (in regards to the center of the cylinder) -> move them even further away from the center
        target.push_back(c); // first test = fix half of the points, move the other half
    }

#if PIPER_TRANS_TEST_DEBUGVIS
    vtkSmartPointer<vtkRenderer> renderer =
        vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderWindow =
        vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
    vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    vtkSmartPointer<vtkDataSetSurfaceFilter> extractor = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    extractor->UseStripsOff();
    extractor->SetInputDataObject(cylinder);
    extractor->Update();

    actor1mapper->SetInputData(extractor->GetOutput());
    actor1->SetMapper(actor1mapper);
    actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
    actor1->GetProperty()->SetOpacity(0.5);
    actor1->GetProperty()->EdgeVisibilityOn();
    renderer->AddActor(actor1);

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

    renderWindowInteractor->SetInteractorStyle(style);

    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetCenter(0, 0, 0);
    sphereSource->SetRadius(1);

    vtkSmartPointer<vtkGlyph3DMapper> pointHighlightSrc = vtkSmartPointer<vtkGlyph3DMapper>::New();
    pointHighlightSrc->OrientOff();
    pointHighlightSrc->SetSourceConnection(sphereSource->GetOutputPort());
    pointHighlightSrc->SetScaleModeToNoDataScaling();

    vtkSmartPointer<vtkPolyData> pointHolderSrc = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkPoints> tPSrc = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : source)
        tPSrc->InsertNextPoint(c[0], c[1], c[2]);
    pointHolderSrc->SetPoints(tPSrc);
    pointHighlightSrc->SetInputData(pointHolderSrc); // uncomment to see only target control points

    vtkSmartPointer<vtkOpenGLActor> pointactorSrc = vtkSmartPointer<vtkOpenGLActor>::New();
    pointactorSrc->SetMapper(pointHighlightSrc);
    pointactorSrc->GetProperty()->SetDiffuseColor(1, 1, 1);
    pointactorSrc->GetProperty()->SetOpacity(1);
    renderer->AddActor(pointactorSrc);

    vtkSmartPointer<vtkGlyph3DMapper> pointHighlightTrgt = vtkSmartPointer<vtkGlyph3DMapper>::New();
    pointHighlightTrgt->OrientOff();
    pointHighlightTrgt->SetSourceConnection(sphereSource->GetOutputPort());
    pointHighlightTrgt->SetScaleModeToNoDataScaling();

    vtkSmartPointer<vtkPolyData> pointHolderTrgt = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkPoints> tPTrgt = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : target)
        tPTrgt->InsertNextPoint(c[0], c[1], c[2]);
    pointHolderTrgt->SetPoints(tPTrgt);
    pointHighlightTrgt->SetInputData(pointHolderTrgt); // uncomment to see only target control points

    vtkSmartPointer<vtkOpenGLActor> pointactorTrgt = vtkSmartPointer<vtkOpenGLActor>::New();
    pointactorTrgt->SetMapper(pointHighlightTrgt);
    pointactorTrgt->GetProperty()->SetDiffuseColor(0, 0xCC / 255.0, 1);
    pointactorTrgt->GetProperty()->SetOpacity(1);
    renderer->AddActor(pointactorTrgt);

    renderWindow->Render();

    // Begin mouse interaction
    renderWindowInteractor->Start();
#endif

    KrigingPiperInterface kriging;
    kriging.setControlPoints(source, target, 0, false, true);
    kriging.compute();
    VCoord cylinderCoords, output;
    for (vtkIdType i = 0; i < points->GetNumberOfPoints(); i++)
        cylinderCoords.push_back(Coord(points->GetPoint(i)));
    output.resize(cylinderCoords.size());
    kriging.applyDeformation(cylinderCoords, &output);
    vtkSmartPointer<vtkPoints> pointsTransformed = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : output)
        pointsTransformed->InsertNextPoint(c[0], c[1], c[2]);
    cylinder->SetPoints(pointsTransformed);
    testMeshQuality(cylinder, 0.9, 0.65);
    EXPECT_EQ(0, evaluateMeshNegVolumes(cylinder));
#if PIPER_TRANS_TEST_ALLOWFILEWRITE
    //meshDisplacementMap(points, pointsTransformed, "D:\\cylinder-TEST1.csv"); .
    writeScaledJacobian(cylinder, "D:\\kriging_test_1.csv");
#endif
#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    extractor->Update();

    renderer->AddActor(actor1);
    renderWindow->Render();
    renderWindowInteractor->SetInteractorStyle(style);

    renderWindowInteractor->Start();
#endif

    // TEST 2 - compression
    target.clear();
    for (vtkIdType i = 0; i < CPs1Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1Inner->GetId(i)));
        target.push_back(c); // keep inner 
    }
    for (vtkIdType i = 0; i < CPs2Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2Inner->GetId(i)));
        target.push_back(c); // keep inner
    }
    for (vtkIdType i = 0; i < CPs1Outer->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1Outer->GetId(i)));
        target.push_back(c); // keep inner
    }
    // move outer nodes towards the center of the circle by 1/2 of the depth
    double compressLength = (rExternal - rInternal) / 1.3;
    double displacement[3];
    for (vtkIdType i = 0; i < CPs2Outer->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2Outer->GetId(i)));
        // the vector towards center == -coordinates of the point -> normalize -> multiply by the distance (compressThird), add to source position
        displacement[0] = -c[0];
        displacement[1] = 0; // the height does not change
        displacement[2] = -c[2];
        vtkMath::Normalize(displacement);
        vtkMath::MultiplyScalar(displacement, compressLength);
        c[0] += displacement[0];
        c[2] += displacement[2];
        target.push_back(c); // compress outer
    }

#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    cylinder->SetPoints(points);
    extractor->Update();

    renderer->AddActor(actor1);

    renderWindowInteractor->SetInteractorStyle(style);

    renderer->AddActor(pointactorSrc);

    id = 0;
    for (Coord &c : target)
        tPTrgt->SetPoint(id++, c[0], c[1], c[2]);
    pointHolderTrgt->SetPoints(tPTrgt);
    pointHolderTrgt->Modified();
    pointHighlightTrgt->SetInputData(pointHolderTrgt); // uncomment to see only target control points
    renderer->AddActor(pointactorTrgt);

    renderWindow->Render();

    // Begin mouse interaction
    renderWindowInteractor->Start();
#endif

    kriging.clearSystemMatrix();
    kriging.setControlPoints(source, target, 0, false, true);
    kriging.compute();
    kriging.applyDeformation(cylinderCoords, &output);
    id = 0;
    for (Coord &c : output)
        pointsTransformed->SetPoint(id++, c[0], c[1], c[2]);
    cylinder->SetPoints(pointsTransformed);

    testMeshQuality(cylinder, 0.85, -0.9);
    EXPECT_EQ(0, evaluateMeshNegVolumes(cylinder));
#if PIPER_TRANS_TEST_ALLOWFILEWRITE
    writeScaledJacobian(cylinder, "D:\\kriging_test_2.csv");
    //meshDisplacementMap(points, pointsTransformed, "D:\\cylinder-TEST2.csv");
#endif
#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    extractor->Update();

    renderer->AddActor(actor1);
    renderWindow->Render();
    renderWindowInteractor->SetInteractorStyle(style);

    renderWindowInteractor->Start();
#endif

    // TEST 3 - uniform compression of outer
    target.clear();
    for (vtkIdType i = 0; i < CPs1Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1Inner->GetId(i)));
        target.push_back(c); // keep inner 
    }
    for (vtkIdType i = 0; i < CPs2Inner->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2Inner->GetId(i)));
        target.push_back(c); // keep inner
    }
    // move outer nodes towards the center of the circle by 1/2 of the depth
    vtkSmartPointer<vtkIdList> &cpSet = CPs1Outer;
    compressLength = (rExternal - rInternal) / 1.5;
    for (int set = 0; set < 2; set++)
    {
        for (vtkIdType i = 0; i < cpSet->GetNumberOfIds(); i++)
        {
            Coord c(points->GetPoint(cpSet->GetId(i)));
            // the vector towards center == -coordinates of the point -> normalize -> multiply by the distance (compressThird), add to source position
            displacement[0] = -c[0];
            displacement[1] = 0; // the height does not change
            displacement[2] = -c[2];
            vtkMath::Normalize(displacement);
            vtkMath::MultiplyScalar(displacement, compressLength);
            c[0] += displacement[0];
            c[2] += displacement[2];
            target.push_back(c); // compress outer
        }
        cpSet = CPs2Outer;
    }

#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    cylinder->SetPoints(points);
    extractor->Update();

    renderer->AddActor(actor1);

    renderWindowInteractor->SetInteractorStyle(style);

    renderer->AddActor(pointactorSrc);

    id = 0;
    tPTrgt->Initialize();
    tPTrgt->SetNumberOfPoints(target.size());
    for (Coord &c : target)
        tPTrgt->SetPoint(id++, c[0], c[1], c[2]);
    pointHolderTrgt->SetPoints(tPTrgt);
    pointHolderTrgt->Modified();
    pointHighlightTrgt->SetInputData(pointHolderTrgt); // uncomment to see only source control points
    renderer->AddActor(pointactorTrgt);

    renderWindow->Render();

    // Begin mouse interaction
    renderWindowInteractor->Start();
#endif

    kriging.clearSystemMatrix();
    kriging.setControlPoints(source, target, 0, false, true);
    kriging.compute();
    kriging.applyDeformation(cylinderCoords, &output);
    id = 0;
    for (Coord &c : output)
        pointsTransformed->SetPoint(id++, c[0], c[1], c[2]);
    cylinder->SetPoints(pointsTransformed);

    testMeshQuality(cylinder, 0.95, 0.95);
    EXPECT_EQ(0, evaluateMeshNegVolumes(cylinder));
#if PIPER_TRANS_TEST_ALLOWFILEWRITE
    //meshDisplacementMap(points, pointsTransformed, "D:\\cylinder-TEST3.csv");

    writeScaledJacobian(cylinder, "D:\\kriging_test_3.csv");
#endif
#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    extractor->Update();

    renderer->AddActor(actor1);
    renderWindow->Render();
    renderWindowInteractor->SetInteractorStyle(style);

    renderWindowInteractor->Start();
#endif
}

TEST(Kriging_test, UShapeTransformation){
    vtkIdType nEleHeight = 20;
    vtkIdType nEleDepth = 5;
    vtkIdType nEleCirc = 5;
    double rInternal = 20, rExternal = 50; // radiuses
    double height = 200;
    vtkIdType id = 0;

    double dR = vtkMath::Pi() / nEleCirc; // only half a cylinder
    double dDepth = (rExternal - rInternal) / nEleDepth;
    double dHeight = height / nEleHeight;
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    // assemble half of a cylinder from bottom to top
    for (int curHeight = 0; curHeight <= nEleHeight; curHeight++) // <= because we need to build the top as well - there is nEleHeight+1 "levels"
    {
        double y = curHeight * dHeight;
        for (int i = 0; i <= nEleDepth; i++) // from inside to outside; <= because there is nEleDepth+1 circles on each level
        {
            double curRadius = rInternal + i * dDepth;
            for (int j = 0; j <= nEleCirc; j++) // along the circle - unlike the full cylinder, the first point is not the last point -> must be nEleCirc+1 points
            {
                double curAngle = j * dR;
                points->InsertNextPoint(curRadius * cos(curAngle), y, curRadius * sin(curAngle));
            }
        }
    }
    // create the elements
    vtkSmartPointer<vtkUnstructuredGrid> cylinder = vtkSmartPointer<vtkUnstructuredGrid>::New();
    cylinder->Allocate();
    cylinder->SetPoints(points);
    vtkIdType ptIds[8];
    vtkIdType nVertPerLevel = (nEleCirc + 1) * (nEleDepth + 1);
    vtkIdType levelOffset = 0;
    for (int curLevel = 0; curLevel < nEleHeight; curLevel++, levelOffset += nVertPerLevel)
    {
        vtkIdType circleOffset = 0;
        for (int curDepth = 0; curDepth < nEleDepth; curDepth++, circleOffset += (nEleCirc + 1)) // nEleCirc + 1 == number of vertices per one circle
        {
            vtkIdType offset = circleOffset + levelOffset;
            for (int curEle = 0; curEle < nEleCirc; curEle++)
            {
                ptIds[0] = offset + curEle; // bottom front left corner
                ptIds[1] = ptIds[0] + nEleCirc + 1; // bottom front right - it's on the next circle
                ptIds[2] = ptIds[1] + nVertPerLevel; // top front right
                ptIds[3] = ptIds[0] + nVertPerLevel; // top front left
                ptIds[4] = ptIds[0] + 1; // bottom rear left - on the next circle, one dR ahead; if its the last, the index on the circle (curEle) is 0
                ptIds[5] = ptIds[1] + 1; // bottom rear right - the next one; if its the last, the index on the circle (curEle) is 0
                ptIds[6] = ptIds[5] + nVertPerLevel; // top rear right
                ptIds[7] = ptIds[4] + nVertPerLevel; // top rear left
                cylinder->InsertNextCell(VTK_HEXAHEDRON, 8, ptIds);
            }
        }
    }

    // now prolong both endings of the half-cylinder to make a U-shape by appending new points and elements
    vtkIdType nEleU = 5; // the number of elements of the prolonged part
    double uHeight = (rExternal - rInternal) * 3; // make the size of the appended part 3x the width of the U
    double dU = uHeight / nEleU;

    for (int curHeight = 0; curHeight <= nEleHeight; curHeight++) // <= because we need to build the top as well - there is nEleHeight+1 "levels"
    {
        double y = curHeight * dHeight;
        for (int leg = 0; leg < 2; leg++) // for each of the two "legs" of the U
        {
            int legXCoeff = (1 - 2 * leg); // one leg is on the positive and the other on the negative X-side of the coordinate system
            for (int i = 0; i <= nEleDepth; i++) // from inside to outside; <= because there is nEleDepth+1 circles on each level
            {
                double x = (rInternal + i * dDepth) * legXCoeff;
                for (int j = 1; j <= nEleU; j++) // from the cylinder outside - the first added point is the "end" of the first added element - the element's other points are on the cylinder
                {
                    points->InsertNextPoint(x, y, -j * dU); // the U grows in the negative Z coordinate
                }
            }
        }
    }

    vtkIdType cylinderPointsOffset = nVertPerLevel * (nEleHeight + 1); // (nEleHeight + 1) == number of point-levels
    vtkIdType nUPointsPerLevel = nEleU * (nEleDepth + 1) * 2;
    levelOffset = cylinderPointsOffset;
    for (int curLevel = 0; curLevel < nEleHeight; curLevel++, levelOffset += nUPointsPerLevel)
    {
        vtkIdType circleOffset = 0;
        for (int curLeg = 0; curLeg < 2; curLeg++, circleOffset += nEleU) // we need to offset one more row of U-verts - otherwise the next line of elements would begin right at the end of the last line of the previous leg
        {
            for (int curDepth = 0; curDepth < nEleDepth; curDepth++, circleOffset += nEleU) // nEleU == number of vertices per one "circle extension"
            {
                vtkIdType offset = circleOffset + levelOffset;
                // build the first element in each row of the leg specially - half of its elements are on the cylinder
                vtkIdType boundaryNodeID = curLevel * nVertPerLevel + curLeg * nEleCirc + curDepth * (nEleCirc + 1);
                ptIds[curLeg] = offset; // to keep the same "orientation" - bottom left node as 0 etc., since the added elements are going in the negative Z, the bottom left is the newly added node
                ptIds[1 - curLeg] = offset + nEleU; // bottom front right
                ptIds[2 + curLeg] = ptIds[1 - curLeg] + nUPointsPerLevel; // top front right
                ptIds[3 - curLeg] = ptIds[curLeg] + nUPointsPerLevel; // top front left
                ptIds[4 + curLeg] = boundaryNodeID; // bottom rear left
                ptIds[5 - curLeg] = boundaryNodeID + nEleCirc + 1; // bottom rear right
                ptIds[6 + curLeg] = ptIds[5 - curLeg] + nVertPerLevel; // top rear right
                ptIds[7 - curLeg] = ptIds[4 + curLeg] + nVertPerLevel; // top rear left
                cylinder->InsertNextCell(VTK_HEXAHEDRON, 8, ptIds);
                for (int curEle = 1; curEle < nEleU; curEle++)
                {
                    ptIds[curLeg] = offset + curEle; // bottom front left corner
                    ptIds[1 - curLeg] = ptIds[curLeg] + nEleU; // bottom front right - it's on the next added line
                    ptIds[2 + curLeg] = ptIds[1 - curLeg] + nUPointsPerLevel; // top front right
                    ptIds[3 - curLeg] = ptIds[curLeg] + nUPointsPerLevel; // top front left
                    ptIds[4 + curLeg] = ptIds[curLeg] - 1; // bottom rear left - the previous one
                    ptIds[5 - curLeg] = ptIds[1 - curLeg] - 1; // bottom rear right - on the next line, one point behind
                    ptIds[6 + curLeg] = ptIds[5 - curLeg] + nUPointsPerLevel; // top rear right
                    ptIds[7 - curLeg] = ptIds[4 + curLeg] + nUPointsPerLevel; // top rear left
                    cylinder->InsertNextCell(VTK_HEXAHEDRON, 8, ptIds);
                }
            }
        }
    }
    // get control point indices - take two sets, the end points of each of the U-"legs"
    vtkSmartPointer<vtkIdList> CPs1 = vtkSmartPointer<vtkIdList>::New();
    vtkSmartPointer<vtkIdList> CPs2 = vtkSmartPointer<vtkIdList>::New();
    levelOffset = cylinderPointsOffset;
    for (int curLevel = 0; curLevel <= nEleHeight; curLevel++, levelOffset += nUPointsPerLevel)
    {
        for (vtkIdType curRow = 0; curRow < nEleDepth; curRow++)
        {
            CPs1->InsertNextId(levelOffset + (curRow + 1) * nEleU - 1);
            CPs2->InsertNextId(levelOffset + (curRow + 1) * nEleU - 1 + (nUPointsPerLevel / 2));
        }
    }

    // TEST 1 - move endpoints of both legs towards each other
    // set the control points for transformation
    VCoord source, target;
    for (vtkIdType i = 0; i < CPs1->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs1->GetId(i)));
        source.push_back(c);
        c[0] -= 20;
        c[1] -= 1; // to avoid having deformation in only one axis
        target.push_back(c);
    }
    for (vtkIdType i = 0; i < CPs2->GetNumberOfIds(); i++)
    {
        Coord c(points->GetPoint(CPs2->GetId(i)));
        source.push_back(c);
        c[0] += 20; 
        c[1] += 1;
        target.push_back(c);
    }
    
#if PIPER_TRANS_TEST_DEBUGVIS
    vtkSmartPointer<vtkRenderer> renderer =
        vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> renderWindow =
        vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
    vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    vtkSmartPointer<vtkDataSetSurfaceFilter> extractor = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    extractor->UseStripsOff();
    extractor->SetInputDataObject(cylinder);
    extractor->Update();

    actor1mapper->SetInputData(extractor->GetOutput());
    actor1->SetMapper(actor1mapper);
    actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
    actor1->GetProperty()->SetOpacity(0.5);
    actor1->GetProperty()->EdgeVisibilityOn();
    renderer->AddActor(actor1);

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

    renderWindowInteractor->SetInteractorStyle(style);

    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
    sphereSource->SetCenter(0, 0, 0);
    sphereSource->SetRadius(1);

    vtkSmartPointer<vtkGlyph3DMapper> pointHighlightSrc = vtkSmartPointer<vtkGlyph3DMapper>::New();
    pointHighlightSrc->OrientOff();
    pointHighlightSrc->SetSourceConnection(sphereSource->GetOutputPort());
    pointHighlightSrc->SetScaleModeToNoDataScaling();

    vtkSmartPointer<vtkPolyData> pointHolderSrc = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkPoints> tPSrc = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : source)
        tPSrc->InsertNextPoint(c[0], c[1], c[2]);
    pointHolderSrc->SetPoints(tPSrc);
    pointHighlightSrc->SetInputData(pointHolderSrc); // uncomment to see only target control points

    vtkSmartPointer<vtkOpenGLActor> pointactorSrc = vtkSmartPointer<vtkOpenGLActor>::New();
    pointactorSrc->SetMapper(pointHighlightSrc);
    pointactorSrc->GetProperty()->SetDiffuseColor(1, 1, 1);
    pointactorSrc->GetProperty()->SetOpacity(1);
    renderer->AddActor(pointactorSrc);

    vtkSmartPointer<vtkGlyph3DMapper> pointHighlightTrgt = vtkSmartPointer<vtkGlyph3DMapper>::New();
    pointHighlightTrgt->OrientOff();
    pointHighlightTrgt->SetSourceConnection(sphereSource->GetOutputPort());
    pointHighlightTrgt->SetScaleModeToNoDataScaling();

    vtkSmartPointer<vtkPolyData> pointHolderTrgt = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkPoints> tPTrgt = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : target)
        tPTrgt->InsertNextPoint(c[0], c[1], c[2]);
    pointHolderTrgt->SetPoints(tPTrgt);
    pointHighlightTrgt->SetInputData(pointHolderTrgt); // uncomment to see only target control points

    vtkSmartPointer<vtkOpenGLActor> pointactorTrgt = vtkSmartPointer<vtkOpenGLActor>::New();
    pointactorTrgt->SetMapper(pointHighlightTrgt);
    pointactorTrgt->GetProperty()->SetDiffuseColor(0, 0xCC / 255.0, 1);
    pointactorTrgt->GetProperty()->SetOpacity(1);
    renderer->AddActor(pointactorTrgt);
    
    renderWindow->Render();

    // Begin mouse interaction
    renderWindowInteractor->Start();
#endif
    
    KrigingPiperInterface kriging;
    kriging.setControlPoints(source, target, 0, true, false);
    kriging.compute();
    VCoord cylinderCoords, output;
    for (vtkIdType i = 0; i < points->GetNumberOfPoints(); i++)
        cylinderCoords.push_back(Coord(points->GetPoint(i)));
    output.resize(cylinderCoords.size());
    kriging.applyDeformation(cylinderCoords, &output);
    vtkSmartPointer<vtkPoints> pointsTransformed = vtkSmartPointer<vtkPoints>::New();
    for (Coord &c : output)
        pointsTransformed->InsertNextPoint(c[0], c[1], c[2]);
    cylinder->SetPoints(pointsTransformed);

    testMeshQuality(cylinder, 0.9, 0.8);
    EXPECT_EQ(0, evaluateMeshNegVolumes(cylinder));
#if PIPER_TRANS_TEST_ALLOWFILEWRITE

    writeScaledJacobian(cylinder, "D:\\kriging_test_4.csv");
    //meshDisplacementMap(points, pointsTransformed, "D:\\UShape-TEST1.csv");
#endif
#if PIPER_TRANS_TEST_DEBUGVIS
    renderer = vtkSmartPointer<vtkRenderer>::New();
    renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
    renderWindow->AddRenderer(renderer);

    renderWindow->SetAlphaBitPlanes(1);
    renderWindow->SetMultiSamples(0);
    renderer->SetUseDepthPeeling(1);
    renderer->SetMaximumNumberOfPeels(10);
    renderer->SetOcclusionRatio(0.01);

    // An interactor
    renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    renderWindowInteractor->SetRenderWindow(renderWindow);

    extractor->Update();

    renderer->AddActor(actor1);
    renderWindow->Render();
    renderWindowInteractor->SetInteractorStyle(style);

    renderWindowInteractor->Start();
#endif
}


int main(int argc, char **argv) {
  std::string modelFile="Kriging_model_01.pmd";
  hbm_ptr = std::unique_ptr<HumanBodyModel>(new HumanBodyModel(modelFile));
  
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
  hbm_ptr.release();
}


//test 
// test random matrices supp a taille memoire et exception correctement catchees ?

//test matrice mal conditionnee avec ctrl point identiques 

