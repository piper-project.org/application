/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef _PIPER_INTERMEDIATE_KRIG_INTERFACE
#define _PIPER_INTERMEDIATE_KRIG_INTERFACE

#include "kriging/KrigingPiperInterface.h"

#define _PIPER_NUGGET_MAP "nuggetMap" // name of the scalar array containing nuggets
#define _PIPER_DECIMATION_MAP "decimationMap" // name of the scalar array marking decimated points by IS_NOT_PRIMITIVE_SELECTED, not-decimated by IS_PRIMITIVE_SELECTED

namespace piper
{    
    /// <summary>
    /// Describes a subpart of some mesh that is defined by some closed surface.
    /// </summary>
    struct HBMKrigingDomain {
        // Boundary surface of the domain. 
        // Contains ORIGINAL_POINT_IDS and ORIGINAL_CELL_IDS data arrays mapping the mesh to the mesh from which it was created (e.g. skin obtainable through FEModelVTK::getSkin)
        vtkSmartPointer<vtkPolyData> boundary;
        // has points from the parent hbm's VTK mesh that belong to the domain - that is not only points of the domain itself, but also points inside it.
        // ORIGINAL_POINT_IDS pointData array maps those points to the VTK IDs in the hbm's FEModelVTK mesh
        // Can also contain points outside of the boundary in case some overlap with other domains is requested 
        vtkSmartPointer<vtkUnstructuredGrid> pointsInDomain;

        // locators build upon subset of the points that are control points. The input dataset is vtkPolyData with points that belong to this domain
        // and also are control point candidates. The set has a ORIGINAL_POINT_IDS pointData array mapping its indices to the CP indices
        vtkSmartPointer<vtkKdTreePointLocator> CPLocatorSource;
        vtkSmartPointer<vtkKdTreePointLocator> CPLocatorTarget;
    };

    /// <summary>
    /// Provides a high level kriging interface for processing multiple step kriging, namely transforming skin and bones separately 
    /// and using them as an intermediate target for the "final" dense kriging that uses all points of the intermediate targets as control points.
    /// It is implemented as a second level of kriging interface build on top of (several instances of) KrigingPiperInterface.
    /// </summary>
    class IntermediateTargetsInterface {
    public:

        IntermediateTargetsInterface() : m_hbm(nullptr) {}
        IntermediateTargetsInterface(hbm::HumanBodyModel* hbmref);
        ~IntermediateTargetsInterface();

        void setRefHBM(hbm::HumanBodyModel* hbmref);

        /// <summary>
        /// Sets the functions that will be called when the interface wants to write out some message.
        /// This mainly allows to indirectly set a stream to write messages about the execution to. By default, they will be written to std::cout.
        /// </summary>
        /// <param name="infoMessage">The function for processing messages regarding the progress of the work currently done by the interface,
        /// such as what lenghty operation is currently being done, how many control points are being used etc.</param>
        /// <param name="warningMessage">The function for processing warning messages, such as when part of the input is not correctly defined.</param>
        void setMessageFunctions(std::function<void(std::string const&)> infoMessage, std::function<void(std::string const&)> warningMessage);

        /// <summary>
        /// Sets the control points for this kriging. Internally, all the points are divided into two groups, 
        /// skin and bones, based on the value of UseBonesThreshold and UseSkinTreshold parameters (see setUseSkin/BonesThreshold).
        /// Nuggets are also read from the subsets (on source) and stored.
        /// Calling this always resets the whole interface (calls <c>sourceReset</c>).
        /// </summary>
        /// <param name="cpsSource">The set of source control points to use for kriging.</param>
        /// <param name="cpsTarget">The set of target control points to use for kriging. Subsets must match (name, number of points) with the cpsSource's subsets.</param>
        void setControlPoints(hbm::InteractionControlPoint &cpsSource, hbm::InteractionControlPoint &cpsTarget);

        /// <summary>
        /// Sets only the target control points. This assumes that setControlPoints was called before and provided the matching source control points.
        /// </summary>
        /// <param name="cpsTarget">The target points for kriging.</param>
        void setTargetControlPoints(hbm::InteractionControlPoint &cpsTarget);

        void resetKrigingInterface();
        // should be called when source points of the model changed (components added/removed/modified)
        void sourcesReset();
        
        /// <summary>
        /// Indicates whether some control points are set.
        /// </summary>
        /// <returns><c>True</c> if some source points have already been loaded since the last time sourcesReset was called.</returns>
        bool getSourcesAreLoaded();
        
        /// <summary>
        /// Sets whether kriging based on geodesic distance should be used for skin intermediate target.
        /// </summary>
        /// <param name="usegeo">If set to <c>true</c>, kriging based on geodesic distance will be used to create the skin intermediate target.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setUseGeodesicSkin(bool usegeo);

        bool getUseGeodesicSkin() { return m_useGeodesicSkin; }
        
        /// <summary>
        /// Sets the geodesic distance to be used when UseGeodesicSkin is set to true.
        /// </summary>
        /// <param name="geoDistType">Type of the geodesic distance.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setGeodesicDistanceType(SurfaceDistance geoDistType);
        
        /// <summary>
        /// Gets the status of automatic nugget computation (based on deviation from local average of displacement).
        /// </summary>
        /// <returns>True if and only if an automatic nugget was set to use AND computed according to the current parameters.</returns>
        bool getIsNuggetComputed() { return m_nuggetsValid; }
        
        /// <summary>
        /// Gets the status of automatic points decimation (based on deviation from local average of displacement).
        /// </summary>
        /// <returns>True if control points were decimated according to the current parameters.</returns>
        bool getIsDecimationComputed() { return m_decimationValid; }

        double getNuggetBoneWeight();

        /// <summary>
        /// Only control points with use_skin higher than this threshold will be used for creating the skin intermediate target.
        /// </summary>
        /// <returns>Used skin threshold.</returns>
        double getUseSkinThreshold();
        /// <summary>
        /// Only control points with use_skin higher than this threshold will be used for creating the skin intermediate target.
        /// </summary>
        /// <param name="threshold">Used skin threshold.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setUseSkinThreshold(double threshold);

        /// <summary>
        /// Only control points with use_bones higher than this threshold will be used for creating the bone intermediate target.
        /// </summary>
        /// <returns>Used bone threshold.</returns>
        double getUseBonesThreshold();

        /// <summary>
        /// Only control points with use_bones higher than this threshold will be used for creating the bones intermediate target.
        /// </summary>
        /// <param name="threshold">Used bones threshold.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setUseBonesThreshold(double threshold);

        /// <summary>
        /// Sets the decimation radius.
        /// </summary>
        /// <param name="radius">What radius is considered around each control node candidate to compare
        /// the average displacement of the points within that radius with the displacement of the candidate point when evaluating decimation.</param>
        /// <seealso cref="setDecimationUseDisplacement"/>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setDecimationRadius(double radius);
        double getDecimationRadius();

        /// <summary>
        /// Turns on decimation by relative displacement.
        /// </summary>
        /// <param name="value">If set to <c>true</c>, the control points will be decimated based on the relative displacement of the surroundings
        /// of each point - if CPs in neighborhood have similar displacement, the candidate point will be decimated.</param>
        /// <seealso cref="setDecimationRadius"/>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setDecimationUseDisplacement(bool value);
        bool getDecimationUseDisplacement();

        /// <summary>
        /// Turns on homogenous decimation of control points.
        /// </summary>
        /// <param name="value">If set to <c>true</c>, the control points will be decimated homogenously.</param>
        /// <seealso cref="setDecimationGridSize"/>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setDecimationUseHomogenous(bool value);
        bool getDecimationUseHomogenous();

        /// <summary>
        /// Sets the parameter controlling density of control points decimation if the homogenous decimation is turned on.
        /// </summary>
        /// <param name="sizeX">The bounding box of the CPs will be subdivided into a cubical grid. This parameter is the number
        /// of cells along the X axis - the number along other axes is given based on the fact that the grid is cubical, not rectangular.
        /// The algorithm will than ensure that there is at most one CP inside each cells (the closest one to the center of the cell),
        /// remaining CPs will be decimated.</param>
        /// <seealso cref="setDecimationUseHomogenous"/>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setDecimationGridSize(unsigned int sizeX);
        unsigned int getDecimationGridSize();

        /// <summary>
        /// Sets the nugget radius.
        /// </summary>
        /// <param name="radius">What radius is considered around each control node candidate to compare
        /// the average displacement of the points within that radius with the displacement of the candidate point when evaluating nugget.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setNuggetRadius(double radius);
        double getNuggetRadius();

        /// <summary>
        /// Sets the decimation deviation.
        /// </summary>
        /// <param name="deviation">The maximum deviation between the average displacement of points in surroundings
        /// of a given point and the displacement of that point for which the point is considered inconsequential for kriging (and therefore is not used).</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setDecimationDeviation(double deviation);
        double getDecimationDeviation();

        /// <summary>
        /// Sets the nugget scale.
        /// </summary>
        /// <param name="scale">The multiplier that will be applied to the nugget after it has been automatically computed.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setNuggetScaleSkin(double scale);
        double getNuggetScaleSkin();
        bool setNuggetScaleBones(double scale);
        double getNuggetScaleBones();

        /// <summary>
        /// Sets if default nugget should be used or not.
        /// </summary>
        /// <param name="useDefault">If set to <c>true</c>, no smart nugget will be computed, instead the default nugget will be used for all points.</param>
        void setUseDefaultNugget(bool useDefault);
        bool getUseDefaultNugget();
        
        /// <summary>
        /// Sets whether to use displacement or position for interpolation.
        /// </summary>
        /// <param name="displacement">If set to <c>true</c>, displacement will be used as the quality to interpolate, otherwise position.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setInterpolateDisplacement(bool displacement);

        /// <summary>
        /// Sets whether to use the drift part of Kriging or only fluctuation.
        /// </summary>
        /// <param name="useDrift">If set to <c>true</c>, drift will be used during Kriging.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setUseDrift(bool useDrift);

        /// <summary>
        /// Sets the precision to use when topology aware distance is used for scaling the skin.
        /// </summary>
        /// <param name="precision">Precision of geodesic distance computation (i.e. EigenPrecision of vtkSurfaceDistance).</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setGeoDistancePrecision(double precision);
        double getGeoDistancePrecision();

        /// <summary>
        /// Sets the overlap of boxes that are split during kriging.
        /// </summary>
        /// <param name="overlap">The overlap.</param>
        void setSplitBoxOverlap(double overlap);
        
        /// <summary>
        /// Sets the nugget bone weight.
        /// </summary>
        /// <param name="weight">The bone to skin importance weight - takes value 0-1:
        /// if it is 0.5, both are treated equally, 1 = bones are assigned 0 nugget (are perfectly respected) while
        /// skin is assigned high nugget, i.e. all discontinuities in the deformation field are treated by moving the skin; if 0 = the other way around..</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setNuggetBoneWeight(double weight);

        /// <summary>
        /// Scales only skin of the HBM using geodesic distance-based kriging or standard kriging based on the useGeodesicSkin flag and all SECTION components as control points.
        /// </summary>
        void scaleSkin();

        /// <summary>
        /// Gets the mesh of the skin after it was scaled.
        /// </summary>
        /// <returns>Scaled skin.</returns>
        vtkSmartPointer<vtkPolyData> getScaledSkin();

        /// <summary>
        /// Scales only the bones of this HBM using standard kriging and all SEGMENT and DIMENSION components as control points.
        /// </summary>
        void scaleBones();

        /// <summary>
        /// Gets the meshes of bones after they were scaled.
        /// </summary>
        /// <returns>Scaled bone meshes, mapped by the name of the entity they belong to.</returns>
        boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> getScaledBones();

        /// <summary>
        /// Decimates the control points on skin and bones. If skin and bones were not scaled yet, this will scale them.
        /// Each point is evaluated based on the deviation from average displacement of other points in it's vicinity.
        /// </summary>
        void decimateCtrlPoints();

        /// <summary>
        /// Computes the nuggets for kriging automatically.
        /// Based on the displacements of skin and bones nodes around each node it detects areas of potential problems (large shear, penetration...) and assigns high nugget to them.
        /// </summary>
        void computeNuggetsInterm();

        /// <summary>
        /// Scales the hbm set as the referenced model using the "dense control points kriging with intermediate targets",
        /// i.e. scales bones and skin separately using only control points respective to bones/skin and then
        /// using all skin and bone points of the generated surfaces as control points (hence "dense" - there
        /// is significantly more of them then in the initial set of control points) to get the final transformation.
        /// Also, non-euclidean distance and other tools are used to ensure correct area of infulence for each control point
        /// to avoid artifacts such as control points on arms affecting trunk etc.
        /// !!! This method relies on having the anatomyDB set up correctly for the model to be able to distinguish parts that are skin and bones.
        /// </summary>
        /// <param name="outputNodes">Upon completion, these nodes will hold the deformed coordinates of the HBM's nodes.
        /// This will be filled by the method, you only need to provide a valid pointer.
        /// The nodes coordinates of the hbm instance set as reference model will not be directly changed - do it yourself using this container.</param>
        /// <param name="splitBoxThresholdCP">Kriging in a box parameter - see KrigingPiperInterface::applyDeformationInABox.</param>
        /// <param name="globalNugget">The nugget to be assigned to each control point.</param>
        /// <seealso cref="applyDeformation"/>
        void scaleHBMDenseWIntermediates(piper::hbm::Nodes* outputNodes, unsigned int splitBoxThresholdCP, double globalNugget);

        /// <summary>
        /// Scales the specified set of nodes using the transformation field generated by all control points (regardless of their skin/bone association)
        /// using standard euclidean distance-based dual kriging.
        /// </summary>
        /// <param name="srcNodes">Nodes in the initial position.</param>
        /// <param name="outputNodes">Upon completion, this will hold the deformed coordinates of the srcNodes.
        /// !!! WARNING - the IDs of the nodes are not taken into account when setting the output positions - 
        /// the order and number of the output nodes must exactly match the order and number of the srcNodes!</param>
        /// <seealso cref="scaleHBMDenseSmartKriging"/>
        void scaleSparseAllCPs(hbm::Nodes& srcNodes, hbm::Nodes* outputNodes);
        
        /// <summary>
        /// Scales the whole HBM model (set through setRefHBM) using the box-splitting kriging strategy.
        /// </summary>
        /// <param name="outputNodes">Upon completion, these nodes will hold the deformed coordinates of the HBM's nodes.
        /// This will be filled by the method, you only need to provide a valid pointer.
        /// The nodes coordinates of the hbm instance set as reference model will not be directly changed - do it yourself using this container.</param>
        /// <param name="splitBoxThresholdCP">Kriging in a box parameter - see KrigingPiperInterface::applyDeformationInABox.</param>
        /// <param name="transCPs">If it is not NULL, the transformed control points will be filled into this array in the same order as they were provided.</param>
        void scaleSparseAllCPs(piper::hbm::Nodes* outputNodes, unsigned int boxSplitCP, vtkSmartPointer<vtkPoints> transCPs = nullptr);

        bool getIsSkinValid() { return m_skinTransValid; }

        bool getIsBonesValid() { return m_bonesTransValid; }
        
        /// <summary>
        /// Sets the fix bones parameters. When bones are fixed, they will not be transformed during the intermediate targets kriging,
        /// but they can still be used as control points (in fact, they should be, otherwise if they are fixed and not used as constraint, they are likely to get penetrated).
        /// </summary>
        /// <param name="fix">If set to <c>true</c>, bones will not be transformed during kriging with intermediate targets.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setFixBones(bool fix);
        bool getFixBones();

        KrigingPiperInterface *getKrigingInterfaceEuclidSkin() { return &m_krigingEuclidSkin; }
        KrigingPiperInterface *getKrigingInterfaceEuclidBones() { return &m_krigingEuclidBones; }
        KrigingPiperInterface *getKrigingInterfaceGeodesic() { return &m_krigingGeodesic; }

        /// <summary>
        /// Sets whether bones should be used as an intermediate target.
        /// Note that at least bones or skin has to be chosen, otherwise there can't be any transformation
        /// </summary>
        /// <param name="flag">If set to <c>true</c>, bones will be used as intermediate targets.</param>
        void setUseIntermediateBones(bool flag) { this->m_intermUseBones = flag; m_skinAndBonesPointsSrc = nullptr; m_skinAndBonesTargetPoints = nullptr; }

        /// <summary>
        /// Sets whether skin should be used as an intermediate target.
        /// Note that at least bones or skin has to be chosen, otherwise there can't be any transformation
        /// </summary>
        /// <param name="flag">If set to <c>true</c>, skin will be used as intermediate targets</param>
        void setUseIntermediateSkin(bool flag) { this->m_intermUseSkin = flag; m_skinAndBonesPointsSrc = nullptr; m_skinAndBonesTargetPoints = nullptr; }
        
        /// <summary>
        /// Sets whether the domain decomposition should be used.
        /// </summary>
        /// <param name="flag">If set to <c>true</c>, krigign will be preformed per domain (if they are defined on the model). 
        /// If set to <c>false</c>, the whole model will be transformed at once.</param>
        /// <returns><c>True</c> in case this caused some changed, i.e. the set value is different than the value that was already there, <c>false</c> otherwise.</returns>
        bool setUseIntermediateDomains(bool flag);
        
        /// <summary>
        /// Gets whether domain decomposition is currently used for kriging (and also decimation and nugget computations).
        /// This does not check whether the domain metadata actually exist in the current model, so it can be set to <c>true</c> even when there are no domains.
        /// </summary>
        /// <returns>Flag if domain decomposition are currently being used.</returns>
        bool getUseIntermediateDomains() { return m_intermUseDomains; }


        /// <summary>
        /// Creates a scalar pointData vtkDoubleArray _PIPER_NUGGET_MAP for the provided skin mesh containing the computed nuggets.
        /// Nodes of the mesh that are not control nodes (and therefore have no nugget) will have the value 1 (otherwise nugget will be nonpositive).
        /// </summary>
        /// <param name="skinMesh">The skin mesh.</param>
        void createNuggetMapSkin(vtkSmartPointer<vtkPolyData> skinMesh);

        /// <summary>
        /// Creates a scalar pointData vtkDoubleArray _PIPER_NUGGET_MAP for all the bone points.
        /// Since all bones use all points of the HBM as their points array, the returned array matches that.
        /// </summary>
        /// <returns>Nugget map for all the points. Nodes that are not used as control point have a value of 1, other points are nonpositive.
        /// Returns NULL if nuggets are not computed.</returns>
        vtkSmartPointer<vtkDoubleArray> createNuggetMapBones();

        /// <summary>
        /// Creates a scalar vtkBitArray marking which points have been decimated and which have not.
        /// </summary>
        /// <returns>Array marking decimated points by IS_NOT_PRIMITIVES_SELECTED, kept points by IS_PRIMITIVE_SELECTED.
        /// If decimation has not been done yet returns NULL.</returns>
        vtkSmartPointer<vtkBitArray> createDecimationMapBones();
        void createDecimationMapSkin(vtkSmartPointer<vtkPolyData> skinMesh);

    private:

        hbm::HumanBodyModel *m_hbm;
        KrigingPiperInterface m_krigingGeodesic, m_krigingEuclidSkin, m_krigingEuclidBones;
        bool m_sourceValidGeodesic = false, m_sourceValidEuclidSkin = false, m_sourceValidEuclidBones = false; // flag notifying if source points of the model have changed or not since they were last used for kriging
        SurfaceDistance m_geodesicDistanceType;

        bool m_useDrift, m_interpolateDisplacement;
        bool m_intermUseBones = true, m_intermUseSkin = true, m_intermUseDomains = false;
        hbm::Nodes m_skinSource, m_bonesSource, m_skinTransformed, m_bonesTransformed; // contains the dones before and after transformation for skin and bones separately - valid only when the skin/boneTransValid flags are set to true
        bool m_skinTransValid = false, m_bonesTransValid = false, m_nuggetsValid = false, m_fixBones = false, m_lockSkinTarget = false, m_lockBonesTarget = false;
        // decimation parameters
        bool m_deciUseHomogenous = false, m_deciUseDisplacement = false, m_decimationValid = false;
        double m_deciDeviation = 0.15, m_deciRadius = 30;
        unsigned int m_deciGridSizeX = 40;

        vtkSmartPointer<vtkPoints> m_skinAndBonesTargetPoints = nullptr, m_skinAndBonesPointsSrc = nullptr; // vtk representation of concatenated m_skin/bonesSource/Transformed
        boost::container::vector<bool> mapRejected; // output of smart kriging decimation, for each point denotes which has been decimated, first are bones then skin nodes
        boost::container::vector<double> smartNuggets; // contains nuggets for smart kriging computed for the current control point set by computeNuggetsSmartKriging, first are bones then skin nodes
        bool m_useGeodesicSkin = false, m_useDefaultNugget = true;
        double m_nuggetBoneWeight = 0.5, m_nuggetRadius = 30,
            m_nuggetScaleSkin = 1, m_nuggetScaleBones = 1, m_geodesicPrecision = 50, m_splitBoxOverlap = 0;
        hbm::VCoord m_cpBonesSource, m_cpSkinSource, m_cpBonesTarget, m_cpSkinTarget;
        std::vector<double> m_bonesNugget, m_skinNugget; // nuggets associated with m_cpBones and m_cpSkin
        double m_UseBonesThreshold = 0.5, m_UseSkinThreshold = 0.5;
        // contains the skin mesh split into domains by hbmDomainDecomposition. If empty -> has not been done, if has only one mesh, it is the skin mesh (unchanged)
        std::list<HBMKrigingDomain> m_domains;
        HBMKrigingDomain m_mainDomain; // this domain represents the whole mesh
        std::function<void(std::string const&)> m_infoLog = [&](std::string const& msg){ std::cout << msg << std::endl; };
        std::function<void(std::string const&)> m_warningLog = [&](std::string const& msg){ std::cout << msg << std::endl; };
        std::map<vtkIdType, vtkIdType> vtkIdToCPIndex; // maps the vtk ids of the hbm's VTK mesh to the control points ID, i.e. index in the m_skinAndBonesPOints, map_rejected etc.
        std::map<hbm::Id, vtkIdType> inverseNid; // maps FE Ids of the whole mesh to vtk ids

        /// <summary>
        /// Creates the data structures concatenating skin and bone targets needed for decimating points and computing nuggets for the intermediate targets used for kriging.
        /// </summary>
        void preprocessIntermediates();

        /// <summary>
        /// Initializes the kriging deformer.
        /// </summary>
        /// <param name="isSkin">Flag to separate skin and bones kriging.</param>
        /// <param name="surface">The surface to use for computing non-euclidean distance between control points,
        /// i.e. "surface", "geodesic-like" distance between the points - how far they are from each other when marching over this surface.
        /// If the control points are not directly on this surface, surface points closest to these control points will be used instead
        /// as the source points, while the target remains unchanged. This may result in small violations of the targets, but should not be critical,
        /// unless the control points are far from the surface - but in such a case, the surface distance should not be used in the first place.
        /// To use euclidean distance, keep this at the default (nullptr) parameter.</param>
        void initDeformation(bool isSkin, vtkSmartPointer<vtkPolyData> surface = nullptr);
        
        /// <summary>
        /// Performs the domain decomposition on the current reference hbm model if it has not been done yet.
        /// The decomposition is based on the named metadata "****_decomposition" - the skin will be split by vertex rings defined by these metadata.
        /// </summary>
        void hbmDomainDecomposition();
        
        /// <summary>
        /// Performs the decimation of control points in a given domain based on relative displacement.
        /// The CPLocators for source of that domain MUST be built.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <seealso cref="setDecimationUseDisplacement"/>
        void decimateByDisplacement(HBMKrigingDomain &domain);
        
        /// <summary>
        /// Computes the nuggets for control points in the given domain based on deviation from local displacement.
        /// </summary>
        /// <param name="domain">The domain.</param>
        void computeNuggetsDispDeviation(HBMKrigingDomain &domain);
    };
}

#endif //_PIPER_INTERMEDIATE_KRIG_INTERFACE
