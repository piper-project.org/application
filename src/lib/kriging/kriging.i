// Copyright (C) 2017 CEESAR, FITT, INRIA, UCBL-Ifsttar
// This file is part of the PIPER Framework.
// Version: 1.0.0
// 
// The PIPER Framework is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 2 of the License, or (at your
// option) any later version.
// 
// The PIPER Framework is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details. You should have received a copy of the GNU General Public
// License along with the PIPER Framework.
// If not, see <http://www.gnu.org/licenses/>.
// 
// Contributors include Erwan Jolivet (CEESAR); Aditya Chhabra, Sachiv
// Paruchuri, Dhruv Kaushik, Sukhraj Singh, Kshitij Mishra, Anoop Chawla (FITT);
// Thomas Lemaire (INRIA); Tomas Janak, Thomas Dupeux, Matthieu Mear (UCBL-
// Ifsttar)
// 
// This work has received funding from the European Union Seventh Framework
// Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
// 
%module hbm

//%begin %{
//#ifdef _MSC_VER
//#define SWIG_PYTHON_INTERPRETER_NO_DEBUG
//#define _ALLOW_ITERATOR_DEBUG_LEVEL_MISMATCH
//#define _CRT_SECURE_INVALID_PARAMETER(expr)
//#endif
//%}

%include "std_string.i"
%include "std_list.i"
%include "std_vector.i"
%include "std_set.i"
%include "std_map.i"
%include "std_pair.i"
%include "std_vector.i"
%include "eigen.i"

%{
#include <vector>
#include "IntermediateTargetsInterface.h"
#include "hbm/types.h"
#include "hbm/HumanBodyModel.h"
#include "hbm/ControlPoint.h"
#include "hbm/Node.h"
%}

namespace piper {
	class IntermediateTargetsInterface
	{
	public:

		IntermediateTargetsInterface();
		IntermediateTargetsInterface(piper::hbm::HumanBodyModel* hbmref);

		void setRefHBM(piper::hbm::HumanBodyModel* hbmref);
		void setControlPoints(piper::hbm::InteractionControlPoint &cpsSource, piper::hbm::InteractionControlPoint &cpsTarget);

		void setUseGeodesicSkin(bool usegeo);
		void setUseSkinThreshold(double threshold);
		void setUseBonesThreshold(double threshold);
        void setDecimationRadius(double radius);
        void setNuggetRadius(double radius);
		void setDecimationDeviation(double deviation);
        void setNuggetScaleSkin(double scale);
        void setNuggetScaleBones(double scale);
		void setUseDefaultNugget(bool useDefault);
		void setGeoDistancePrecision(double precision);
		void setSplitBoxOverlap(double overlap);
		void setNuggetBoneWeight(double weight);
		void setFixBones(bool fix);
		void setUseIntermediateBones(bool flag);
        void setUseIntermediateSkin(bool flag);
        void setUseIntermediateDomains(bool flag);
	};
}
