/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "CtrlPtLoader.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

namespace piper {
    namespace kriging {

        CtrlPtLoader::CtrlPtLoader(const std::string &filename)
        {
            if (!loadData(filename))
                throw std::exception();
        }
        
        bool CtrlPtLoader::loadData(std::string const& filename)
        {
            _datavec.clear();

            std::ifstream myFile;

            try
            {
                myFile.open(filename);
                if (!myFile.is_open()){
                    std::cerr << " unable to open " << filename << std::endl;
                    return false;
                }

                double data;
                std::string myString;
                noOfItemsPerLine = 0;
                while (!myFile.eof())
                {
                    getline(myFile, myString);
                    std::stringstream elem(myString.c_str());
                    while (elem >> data){
                        _datavec.push_back(data);
                    }
                    noOfItemsPerLine++;
                }
                // used just to determine format of the file - might be wrong if the file has empty lines or other garbage
                noOfItemsPerLine = (int)(_datavec.size() / noOfItemsPerLine);

                myFile.close();
            }
            catch (std::exception e)
            {
                std::cerr << " unable to open " << filename << std::endl;
                _datavec.clear();
                return false;
            }
            return true;
        }

        int CtrlPtLoader::getItemsPerLine()
        {
            return noOfItemsPerLine;
        }

        std::vector<double>& CtrlPtLoader::getData(){
            return _datavec;
        }

        void CtrlPtLoader::getCoordinates(std::vector<hbm::Coord>& vcoord)
        {
            auto it = _datavec.begin();
            int i = 0;
            hbm::Coord tempCoord;
            int skip = noOfItemsPerLine - 3; // assumes the coordinates are the last three number on the line
            // skip the entries that are not coordinates, but make sure we don't go over the end of the vector
            for (int k = 0; k < skip && it != _datavec.end(); k++, it++);
            while (it != _datavec.end())
            {
                tempCoord[i] = *it;
                if (i == 2)
                {
                    vcoord.push_back(tempCoord);
                    i = 0;
                    // skip the entries that are not coordinates, but make sure we don't go over the end of the vector
                    for (int k = 0; k < skip && it != _datavec.end(); k++, it++) ;
                }
                else
                    ++i;
                ++it;
            }
        }

    }
}
