/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "krigingdeformation.h"
#include <vtkPointData.h>

#include <iostream>
#include <exception>


#ifdef _OPENMP
#include "omp.h"
#endif

#ifdef WIN32
#ifndef __func__
#define __func__ __FUNCTION__
#endif
#endif

using namespace Eigen;

KrigingDeformation::KrigingDeformation(std::shared_ptr< MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd> destCtrlPt, bool interpolateDisplacement, bool useDrift)
    :_srcCtrlPt(srcCtrlPt),_destCtrlPt(destCtrlPt), _solverState(SolverSystemState::systemCleared),_kernelType(KernelType::euclidian), _solverType(SolverType::partialPivLU),
    _systemMatEuclid(NULL), _systemMatGeodesic(NULL), _solutionMat(NULL), _srcSurface(NULL)
{
    _ctrlPtWeightArray=std::make_shared<ArrayXd>(ArrayXd::Zero(_srcCtrlPt->rows()));
    initCtrlPt();
    _useDrift = useDrift;
    if (useDrift)
        _interpolateDisplacement = false;
    else
        _interpolateDisplacement = interpolateDisplacement;
}

KrigingDeformation::KrigingDeformation(std::shared_ptr<MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt,
    std::shared_ptr< ArrayXd > ctrlPtWeight, bool interpolateDisplacement, bool useDrift)
    :_srcCtrlPt(srcCtrlPt),_destCtrlPt(destCtrlPt),_ctrlPtWeightArray(ctrlPtWeight),
    _solverState(SolverSystemState::systemCleared),_kernelType(KernelType::euclidian), _solverType(SolverType::partialPivLU),
    _systemMatEuclid(NULL), _systemMatGeodesic(NULL), _solutionMat(NULL), _srcSurface(NULL)
{
    initCtrlPt();
    _useDrift = useDrift;
    if (useDrift)
        _interpolateDisplacement = false;
    else
        _interpolateDisplacement = interpolateDisplacement;
}

KrigingDeformation::KrigingDeformation(std::shared_ptr<MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt,
        std::shared_ptr< ArrayXd > ctrlPtWeight, vtkSmartPointer<vtkPolyData> geodesicSurface, SurfaceDistance distanceType)
    :_srcCtrlPt(srcCtrlPt), _destCtrlPt(destCtrlPt), _ctrlPtWeightArray(ctrlPtWeight), 
    _solverState(SolverSystemState::systemCleared), _kernelType(KernelType::geodesic), _solverType(SolverType::partialPivLU),
    _systemMatEuclid(NULL), _systemMatGeodesic(NULL), _solutionMat(NULL), _srcSurface(geodesicSurface)
{
    initCtrlPt();
    _geoDist = vtkSmartPointer<vtkSurfaceDistance>::New();
    _geoDist->SetComputeDistance(distanceType);
    _geoDist->SetInputData(_srcSurface);
    _interpolateDisplacement = true;
    _useDrift = false;
}

KrigingDeformation::KrigingDeformation(std::shared_ptr<MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt, 
    vtkSmartPointer<vtkPolyData> geodesicSurface, SurfaceDistance distanceType)
    :_srcCtrlPt(srcCtrlPt), _destCtrlPt(destCtrlPt), _solverState(SolverSystemState::systemCleared), _kernelType(KernelType::geodesic), _solverType(SolverType::partialPivLU),
    _systemMatEuclid(NULL), _systemMatGeodesic(NULL), _solutionMat(NULL), _srcSurface(geodesicSurface)
{
    _ctrlPtWeightArray = std::make_shared<ArrayXd>(ArrayXd::Zero(_srcCtrlPt->rows()));
    initCtrlPt();
    _geoDist = vtkSmartPointer<vtkSurfaceDistance>::New();
    _geoDist->SetComputeDistance(distanceType);
    _geoDist->SetInputData(_srcSurface);
    _interpolateDisplacement = true;
    _useDrift = false;
}

void KrigingDeformation::SetGeodesicPrecision(double geoDistanceEigenPrecision)
{
    if (_geoDist && _geoDist->GetEigenPrecision() != geoDistanceEigenPrecision)
    {
        _geoDist->SetEigenPrecision(geoDistanceEigenPrecision);
        _lastGeoCorrelationTime = 0;
        _lastGeoDeformationTime = 0; // ensure the filter will get updated
    }
}

void KrigingDeformation::SetGeodesicType(SurfaceDistance geoDistanceType)
{
    if (_geoDist && _geoDist->GetComputeDistance() != geoDistanceType)
    {
        _geoDist->SetComputeDistance(geoDistanceType);
        _lastGeoCorrelationTime = 0;
        _lastGeoDeformationTime = 0; // ensure the filter will get updated
    }
}

bool KrigingDeformation::FillSystemMatrixDense(std::shared_ptr< MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt)
{
    _srcCtrlPt = srcCtrlPt ;
    _destCtrlPt = destCtrlPt ;
    
    initCtrlPt();

    return FillSystemMatrixDense();
}

bool KrigingDeformation::FillSystemMatrixDense(std::shared_ptr< MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt, vtkSmartPointer<vtkPolyData> geodesicSurface)
{
    _srcCtrlPt = srcCtrlPt;
    _destCtrlPt = destCtrlPt;

    if (_srcSurface != geodesicSurface)
    {
        _lastGeoCorrelationTime = 0;
        _lastGeoDeformationTime = 0;
        _srcSurface = geodesicSurface;
        _geoDist->SetInputData(_srcSurface);
    }

    initCtrlPt();
    return FillSystemMatrixDense();
}

bool KrigingDeformation::FillSystemMatrixDense(std::shared_ptr< MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt, std::shared_ptr< ArrayXd > ctrlPtWeight)
{
    _srcCtrlPt = srcCtrlPt;
    _destCtrlPt = destCtrlPt;
    _ctrlPtWeightArray = ctrlPtWeight;

    initCtrlPt();

    return FillSystemMatrixDense();
}

bool KrigingDeformation::FillSystemMatrixDense(std::shared_ptr< MatrixXd > srcCtrlPt, std::shared_ptr< MatrixXd > destCtrlPt,
    std::shared_ptr< ArrayXd > ctrlPtWeight, vtkSmartPointer<vtkPolyData> geodesicSurface)
{
    _srcCtrlPt = srcCtrlPt;
    _destCtrlPt = destCtrlPt;
    _ctrlPtWeightArray = ctrlPtWeight;
    if (_srcSurface != geodesicSurface)
    {
        _lastGeoCorrelationTime = 0;
        _lastGeoDeformationTime = 0;
        _srcSurface = geodesicSurface;
        _geoDist->SetInputData(_srcSurface);
    }

    initCtrlPt();
    return FillSystemMatrixDense();
}

bool KrigingDeformation::FillSystemMatrixDense()
{
    initSystem();
    if (!computeCorrelationMatrix())
        return false;
    if(!verifyCtrlPtAreDistinct())
        return false;
    if(!autoRemoveIdenticalCtrlPt())
        return false;
    fillSystemWithCtrlPt();
    return true;
}

void KrigingDeformation::initSystem()
{
    try{
        if (_useDrift)
            _systDestCtrlPt.resize(_nbCtrlPt+_nbHomogeneousDim,_nbDim);
        else
            _systDestCtrlPt.resize(_nbCtrlPt, _nbDim);
    }
    catch(std::bad_alloc e)
    {
        std::stringstream s;
        size_t requestedSize = _nbCtrlPt * _nbDim;
        if (_useDrift)
            requestedSize += _nbHomogeneousDim * _nbDim;
        requestedSize *= sizeof(double); // convert to how many bytes
        requestedSize /= (1024 * 1024); // convert to MB
        s << "Failed to allocate enough memory for control points vector in kriging, number of control points: " 
            << _nbCtrlPt << ", required memory at least: " << requestedSize << " MB.";
        throw std::runtime_error(s.str());
    }

    try{
        _solutionMat = std::make_shared<MatrixXd>();
        if (_useDrift)
            _solutionMat->resize(_nbCtrlPt+_nbHomogeneousDim,_nbDim); 
        else
            _solutionMat->resize(_nbCtrlPt, _nbDim);
    }
    catch(std::bad_alloc e)
    {
        std::stringstream s;
        size_t requestedSize = _nbCtrlPt * _nbDim;
        if (_useDrift)
            requestedSize += _nbHomogeneousDim * _nbDim;
        requestedSize *= sizeof(double); // convert to how many bytes
        requestedSize /= (1024 * 1024); // convert to MB
        s << "Failed to allocate enough memory for solution matrix, number of control points: "
            << _nbCtrlPt << ", required memory at least: " << requestedSize << " MB.";
        throw std::runtime_error(s.str());
    }

    try{
        _systemMatEuclid = std::make_shared<MatrixXd>();
        if (_useDrift)
            _systemMatEuclid->resize(_nbCtrlPt + _nbHomogeneousDim, _nbCtrlPt + _nbHomogeneousDim);
        else
            _systemMatEuclid->resize(_nbCtrlPt, _nbCtrlPt);
        // geodesic kernel needs the euclidean one in one step anyway, so create the euclidean one always,
        // geodesic matrix is created inside the vtkSurfaceDistance class, no need to allocate it here
    }
    catch(std::bad_alloc e)
    {
        std::stringstream s;
        size_t requestedSize = _nbCtrlPt * _nbCtrlPt; // we need two of these matrices
        if (_useDrift)
            requestedSize += (2 * _nbHomogeneousDim * _nbCtrlPt + _nbHomogeneousDim * _nbHomogeneousDim);
        if (_kernelType == KernelType::geodesic)
            requestedSize += _nbCtrlPt * _nbCtrlPt;
        requestedSize *= sizeof(double); // convert to how many bytes
        requestedSize /= (1024 * 1024); // convert to MB
        s << "Failed to allocate enough memory for kriging matrix, number of control points: " 
            << _nbCtrlPt << ", required memory at least: " << requestedSize << " MB.";
        throw std::runtime_error(s.str());
    }
    if (_interpolateDisplacement)
        _systDestCtrlPt.topRows(_nbCtrlPt) = *_destCtrlPt - *_srcCtrlPt;
    else
        _systDestCtrlPt.topRows(_nbCtrlPt) = *_destCtrlPt;

    if (_useDrift)
        _systDestCtrlPt.bottomRows(_nbHomogeneousDim) = MatrixXd::Zero(_nbHomogeneousDim,_nbDim);
    _solverState = SolverSystemState::systemCleared;
}

bool KrigingDeformation::initCtrlPt()
{
    if(_srcCtrlPt->rows() == _destCtrlPt->rows() && _srcCtrlPt->cols() == _destCtrlPt->cols() )
    {
        _nbCtrlPt = _srcCtrlPt->rows();
        _nbDim = _srcCtrlPt->cols();
        _nbHomogeneousDim = _nbDim + 1;
        _solverState = SolverSystemState::systemCleared;
        if(_ctrlPtWeightArray->size() != _nbCtrlPt)
            *_ctrlPtWeightArray = ArrayXd::Zero(_nbCtrlPt);

        return true;
    }
    else return false;
}

bool KrigingDeformation::verifyCtrlPtAreDistinct()
{    
    if (_kernelType != KernelType::euclidian) // if it's not euclidean kernel, euclidean matrix was not yet computed -> compute it
        computeEuclidianDistCorrelationMatrix();

    _identicalCtrlPtId.clear();

#ifdef _OPENMP
    std::vector<std::vector<unsigned int> > identicalCtrlPtIdChunks;
    identicalCtrlPtIdChunks.resize(omp_get_max_threads());
    #pragma omp parallel
    {
        #pragma omp for nowait schedule(dynamic)
        for(int i=0;i<_nbCtrlPt;i++){
            for(int j=i+1;j<_nbCtrlPt;j++){
                if((*_systemMatEuclid)(i,j)<= minDuplicationDist)
                    identicalCtrlPtIdChunks.at(omp_get_thread_num()).push_back(i);
            }
	    }
    }
    for(int i=0;i<identicalCtrlPtIdChunks.size();i++){
        if(identicalCtrlPtIdChunks.size()>0)
            _identicalCtrlPtId.insert(identicalCtrlPtIdChunks.at(i).begin(),identicalCtrlPtIdChunks.at(i).end());
    }
#else
     for(int i=0;i<_nbCtrlPt;i++){
        for(int j=i+1;j<_nbCtrlPt;j++){
            if((*_systemMat)(i,j)<=minDist)
                _identicalCtrlPtId.insert(i);
        }
	}
#endif
     return true;//_identicalCtrlPtId.empty();
}

bool KrigingDeformation::autoRemoveIdenticalCtrlPt()
{
    if (!_identicalCtrlPtId.empty())
    {
        if (!autoRemoveCtrlPtDuplicates){
            std::string msg("Error, the Source Control-Point set provided contains one or more duplication. Remove duplication or use autoRemoveCtrlPtDuplication option. "
                + std::string(__func__) + " in " + std::string(__FILE__));
            throw std::runtime_error(msg.c_str());
            return false;
        }
        // unmark the duplicate control points in the chosen_points array
        if (_kernelType == KernelType::geodesic)
        {    
            vtkSmartPointer<vtkBitArray> chosen = vtkBitArray::SafeDownCast(_srcSurface->GetPointData()->GetArray(_GEODESIC_CHOSENPOINTS));
            for (unsigned int dupl : _identicalCtrlPtId)
            {
                int chosenC = 0;
                for (int i = 0; i < chosen->GetNumberOfTuples(); i++)
                {
                    if (chosen->GetValue(i))
                    {
                        if (chosenC == dupl) // find the dupl-th control point in the chosen points array
                        {
                            chosen->SetValue(i, 0);
                            break;
                        }
                        chosenC++;
                    }
                }
            }
        }
        else
        {
            std::shared_ptr<MatrixXd> newCtrlPtSrc = std::make_shared<MatrixXd>();
            std::shared_ptr<MatrixXd> newCtrlPtDest = std::make_shared<MatrixXd>();
            std::shared_ptr<ArrayXd> newWeights = std::make_shared<ArrayXd>();
            std::shared_ptr<MatrixXd> newSystemMatEuclid = std::make_shared<MatrixXd>();

            size_t driftPartSize = _useDrift ? _nbHomogeneousDim : 0;

            try{
                newCtrlPtSrc->resize(_nbCtrlPt - _identicalCtrlPtId.size(), _nbDim);
                newCtrlPtDest->resize(_nbCtrlPt - _identicalCtrlPtId.size(), _nbDim);
                newWeights->resize(_nbCtrlPt - _identicalCtrlPtId.size());
                newSystemMatEuclid->resize(_nbCtrlPt - _identicalCtrlPtId.size() + driftPartSize, _nbCtrlPt - _identicalCtrlPtId.size() + driftPartSize);
            }
            catch (std::bad_alloc e)
            {
                std::string msg("Bad Alloc creating matrices in " + std::string(__func__) + " in " + std::string(__FILE__));
                throw std::runtime_error(msg.c_str());
            }

            unsigned int new_lastIt = 0, nbElem = 0, lastIt = 0;
            auto curIt = _identicalCtrlPtId.begin();
            std::vector<int> lastItVec, nbElemVec, new_lastItVec;
            for (; curIt != _identicalCtrlPtId.end(); ++curIt){
                nbElem = (*curIt) - lastIt;
                if (lastIt<_nbCtrlPt && nbElem>0){
                    newCtrlPtSrc->middleRows(new_lastIt, nbElem).noalias() = _srcCtrlPt->middleRows(lastIt, nbElem);
                    newCtrlPtDest->middleRows(new_lastIt, nbElem).noalias() = _destCtrlPt->middleRows(lastIt, nbElem);
                    newWeights->middleRows(new_lastIt, nbElem) = _ctrlPtWeightArray->middleRows(lastIt, nbElem);
                    if (_kernelType == KernelType::euclidian || _kernelType == KernelType::geodesic){
                        newSystemMatEuclid->block(new_lastIt, new_lastIt, nbElem, nbElem).noalias() = _systemMatEuclid->block(lastIt, lastIt, nbElem, nbElem);
                        for (int i = 0; i < lastItVec.size(); i++){
                            newSystemMatEuclid->block(new_lastItVec[i], new_lastIt, nbElemVec[i], nbElem).noalias() = _systemMatEuclid->block(lastItVec[i], lastIt, nbElemVec[i], nbElem);
                            newSystemMatEuclid->block(new_lastIt, new_lastItVec[i], nbElem, nbElemVec[i]).noalias() = _systemMatEuclid->block(lastIt, lastItVec[i], nbElem, nbElemVec[i]);
                        }
                    }
                    lastItVec.push_back(lastIt);
                    nbElemVec.push_back(nbElem);
                    new_lastItVec.push_back(new_lastIt);
                }

                new_lastIt += nbElem;
                lastIt = (*curIt) + 1;
            }

            if (lastIt < _nbCtrlPt){
                newCtrlPtSrc->bottomRows(_nbCtrlPt - lastIt).noalias() = _srcCtrlPt->bottomRows(_nbCtrlPt - lastIt);
                newCtrlPtDest->bottomRows(_nbCtrlPt - lastIt).noalias() = _destCtrlPt->bottomRows(_nbCtrlPt - lastIt);
                newWeights->bottomRows(_nbCtrlPt - lastIt) = _ctrlPtWeightArray->bottomRows(_nbCtrlPt - lastIt);
                if (_kernelType == KernelType::euclidian || _kernelType == KernelType::geodesic){
                    newSystemMatEuclid->bottomRightCorner(_nbCtrlPt - lastIt + driftPartSize, _nbCtrlPt - lastIt + driftPartSize).noalias()
                        = _systemMatEuclid->bottomRightCorner(_nbCtrlPt - lastIt + driftPartSize, _nbCtrlPt - lastIt + driftPartSize);
                    for (int i = 0; i < lastItVec.size(); i++){
                        newSystemMatEuclid->block(new_lastItVec[i], new_lastIt, nbElemVec[i], _nbCtrlPt - lastIt + driftPartSize).noalias()
                            = _systemMatEuclid->block(lastItVec[i], lastIt, nbElemVec[i], _nbCtrlPt - lastIt + driftPartSize);
                        newSystemMatEuclid->block(new_lastIt, new_lastItVec[i], _nbCtrlPt - lastIt + driftPartSize, nbElemVec[i]).noalias()
                            = _systemMatEuclid->block(lastIt, lastItVec[i], _nbCtrlPt - lastIt + driftPartSize, nbElemVec[i]);
                    }
                }
            }

            _nbCtrlPt -= _identicalCtrlPtId.size();

            _srcCtrlPt = newCtrlPtSrc;
            _destCtrlPt = newCtrlPtDest;
            _ctrlPtWeightArray = newWeights;


            _systDestCtrlPt.resize(_nbCtrlPt + driftPartSize, _nbDim);
            if (_useDrift)
                _systDestCtrlPt.bottomRows(driftPartSize) = MatrixXd::Zero(driftPartSize, _nbDim);

            if (_interpolateDisplacement)
                _systDestCtrlPt.topRows(_nbCtrlPt) = *_destCtrlPt - *_srcCtrlPt;
            else
                _systDestCtrlPt.topRows(_nbCtrlPt) = *_destCtrlPt;

            _systemMatEuclid = newSystemMatEuclid;
        }
    }
    return true;
}



bool KrigingDeformation::computeCorrelationMatrix(){
    switch(_kernelType)
    {
        case KernelType::euclidian : {
            computeEuclidianDistCorrelationMatrix();
            break;
        }
        case KernelType::geodesic : {
            return computeGeodesicDistCorrelationMatrix();
            break;
        }
    }
    return true;
}

    
void KrigingDeformation::computeEuclidianDistCorrelationMatrix(){ //TODOThomas : changer ce nom et extraire seulement le kernel de calcul de distance ? a voir si tous les calculs de distance rentrent dans le cadre d'un kernel s'appliquant sur les meme donnees
    double dist;
    MatrixXd coordTemp(1,_nbDim); //Beware to use row vector
/*     std::vector<double> distV;
    Matrix<double,Dynamic,Dynamic,RowMajor> coordTemp;//1,_nbDim); //Beware to use row vector
    try{
    #ifdef _OPENMP
    coordTemp.resize(omp_get_max_threads(),_nbDim);
    distV.resize(omp_get_max_threads());
    #else
    coordTemp.resize(1,_nbDim);
    distV.resize(1);
    #endif
        }
    catch(std::bad_alloc e)
    {
        // std::string msg="Bad Alloc creating _systDestCtrlPt in "+std::string(__FILE__)+" :: "; //TODOThomasD : rethrow exception avec le nom du fichier et de la fonction
        throw std::runtime_error("Bad Alloc preparing coordTemp in ");
    }
    */   
    #pragma omp parallel  private(coordTemp,dist) // shared(coordTemp,distV)
    #pragma omp for nowait schedule(dynamic)
    for(int i = 0; i < _nbCtrlPt; i++){
        coordTemp.resize(1,_nbDim); //Beware not to forget to resize in parallelized loop //Beware to use matrix as row vector and not vector which are column vector
        dist=0;/*
        int threadNum ;
        #ifdef _OPENMP
        threadNum=omp_get_thread_num();
        #else
        threadNum=0;
        #endif*/
        coordTemp.row(0) = (*_srcCtrlPt).row(i);
        for(int j = i + 1; j < _nbCtrlPt; j++){
            dist = ((*_srcCtrlPt).row(j) - coordTemp.row(0)).norm();
			(*_systemMatEuclid)(i,j)=dist;
			(*_systemMatEuclid)(j,i)=dist;
        }
	}
        
    _systemMatEuclid->topLeftCorner(_nbCtrlPt,_nbCtrlPt).diagonal() = *_ctrlPtWeightArray; 
}

bool KrigingDeformation::computeGeodesicDistCorrelationMatrix()
{
    if (_srcSurface == NULL)
        return false;
    // compute geodesic distance based on the provided surface mesh

    if (!_srcSurface->GetPointData()->HasArray(_GEODESIC_CHOSENPOINTS) && _srcSurface->GetNumberOfPoints() != _nbCtrlPt) // if no ctrl points are specified in the mesh, all must be control points
        return false;
    // compute the distance, but only if it hasn't been computed for the same mesh before
    if (_lastGeoCorrelationTime < _srcSurface->GetMTime())
    {
        _geoDist->SetMode(DistanceComputeSubset::EachChosenToEachChosen);
        _geoDist->Update();
        _lastGeoCorrelationTime = _srcSurface->GetMTime();
        _systemMatGeodesic = _geoDist->GetDistanceMatrix();
    }
    _systemMatGeodesic->diagonal() = *_ctrlPtWeightArray; // put nuggets
    return true;
}

void KrigingDeformation::fillSystemWithCtrlPt()
{
    if (_useDrift)
    {
        _systemMatEuclid->topRightCorner(_nbCtrlPt, _nbHomogeneousDim) << MatrixXd::Ones(_nbCtrlPt, 1), (*_srcCtrlPt);
        _systemMatEuclid->bottomLeftCorner(_nbHomogeneousDim, _nbCtrlPt) << MatrixXd::Ones(1, _nbCtrlPt), (*_srcCtrlPt).transpose();
        _systemMatEuclid->bottomRightCorner(_nbHomogeneousDim, _nbHomogeneousDim) << MatrixXd::Zero(_nbHomogeneousDim, _nbHomogeneousDim);
    }
    _solverState = SolverSystemState::initializedNotSolved;
}

bool KrigingDeformation::SolveSystemMatrixDense(){

    if(_solverState == SolverSystemState::initializedNotSolved)
    {
        std::shared_ptr<MatrixXd> systemMat = _systemMatEuclid;
        if (_kernelType == KernelType::geodesic)
            systemMat = _systemMatGeodesic;

        switch (_solverType){
            case SolverType::fullPivLU: {
                *_solutionMat = systemMat->fullPivLu().solve(_systDestCtrlPt);
                break; 
            }
            case SolverType::partialPivLU:
            default: {
                *_solutionMat = systemMat->partialPivLu().solve(_systDestCtrlPt);
                break;
            }

        }
        _solverState = SolverSystemState::systemSolved;
    }
    return _solverState == SolverSystemState::systemSolved;
}

bool KrigingDeformation::ApplyDeformationDense(std::shared_ptr<MatrixXd > srcMesh, std::shared_ptr< MatrixXd > destMesh){
    if(!srcMesh || !destMesh)
        return false;
    int nbRows = (int)srcMesh->rows();
    return ApplyDeformationDense(std::move(srcMesh),std::move(destMesh),0,0,nbRows);
}

bool KrigingDeformation::ApplyDeformationDense(std::shared_ptr<MatrixXd > srcMesh, std::shared_ptr< MatrixXd > destMesh, int srcOffset, int destOffset, int subsetSize){
    if(_solverState < SolverSystemState::systemSolved)
        return false;
    if(srcMesh->cols() != destMesh->cols() || srcMesh->cols() != _nbDim)
        return false;
    if( srcMesh->rows() < srcOffset+subsetSize || destMesh->rows() < destOffset + subsetSize)
        return false;

    size_t requestedSize = 0;
    size_t driftPartSize = _useDrift ? _nbHomogeneousDim : 0;
    try{
        MatrixXd matTest;
#ifdef _OPENMP
        matTest.resize(omp_get_max_threads(),_nbCtrlPt + driftPartSize);
        requestedSize = omp_get_max_threads() * (_nbCtrlPt + driftPartSize);
#else
        matTest.resize(1,_nbCtrlPt+driftPartSize);
        requestedSize = _nbCtrlPt + driftPartSize;
#endif
    }
    catch(std::bad_alloc e)
    {
        std::stringstream s;
        requestedSize *= sizeof(double); // convert to how many bytes
        requestedSize /= (1024 * 1024); // convert to MB
        s << "Failed to allocate enough memory for transformation matrix, number of control points: "
            << _nbCtrlPt << ", required memory at least: " << requestedSize << " MB.";
        throw std::runtime_error(s.str());
    }

    int srcOffsetI;
    if (_kernelType == KernelType::geodesic)
    {
        if (_lastGeoDeformationTime < _srcSurface->GetMTime())
        {
            _geoDist->SetMode(DistanceComputeSubset::EachChosenToAll);
            _geoDist->Update();
            _lastGeoDeformationTime = _srcSurface->GetMTime();
            _deformationGeodesicMat = _geoDist->GetDistanceMatrix();
        }
        
        // apply geodesic deformation matrix - the solution is only displacement -> add it to the source
        (*destMesh).bottomLeftCorner((destMesh->rows() - destOffset), destMesh->cols()) =  
            (*srcMesh).bottomLeftCorner((srcMesh->rows() - srcOffset), srcMesh->cols()) + *_deformationGeodesicMat * *_solutionMat;
    }
    else
    {
        MatrixXd matDistanceRow;
        #pragma omp parallel for shared(subsetSize,srcMesh,destMesh) private(matDistanceRow,srcOffsetI) 
        for (int i = 0; i < subsetSize; i++){
            srcOffsetI = i + srcOffset;
            matDistanceRow.resize(1, _nbCtrlPt + driftPartSize);
            if (_useDrift)
                matDistanceRow.topRightCorner(1, _nbHomogeneousDim) << 1.0, srcMesh->row(srcOffsetI);
            for (int j = 0; j < _nbCtrlPt; j++){
                matDistanceRow(0, j) = (_srcCtrlPt->row(j) - srcMesh->row(srcOffsetI)).norm(); //TODOThomas : factoriser eventuellement le calcul de distance avec le remplissage de la matrice selon le type de kernel MAIS ne SURTOUT pas faire de switch(kernel)
            }
            destMesh->row(i + destOffset) = matDistanceRow * *_solutionMat; //don't use noalias here !! row+noalias does not bring expected behavior!
            if (_interpolateDisplacement)
                destMesh->row(i + destOffset) = destMesh->row(i + destOffset) + srcMesh->row(srcOffsetI);
        }
    }
    return true;
}

bool KrigingDeformation::ApplyDeformationDense(std::shared_ptr<MatrixXd > meshIO){
    if(!meshIO)
        return false;
    return ApplyDeformationDense(std::move(meshIO),std::move(meshIO));
}


void KrigingDeformation::ClearSystemMatrix(){
    _systemMatEuclid->resize(0,0);
    if (_systemMatGeodesic)
        _systemMatGeodesic->resize(0, 0);
    _solverState=SolverSystemState::systemCleared;
}


void KrigingDeformation::SetKernelType(KernelType kernelType){
    _kernelType = kernelType;
    _solverState = SolverSystemState::systemCleared;
}

KrigingDeformation::KernelType KrigingDeformation::GetKernelType(){
    return _kernelType;
}

void KrigingDeformation::SetSolverType(SolverType solverType){
    _solverType=solverType;
}

KrigingDeformation::SolverType KrigingDeformation::GetSolverType(){
    return _solverType;
}

std::shared_ptr<const MatrixXd> KrigingDeformation::GetSystemMatrix(){
    if (_kernelType == KernelType::geodesic)
        return  _systemMatGeodesic;
    return _systemMatEuclid;
}

std::shared_ptr<const MatrixXd> KrigingDeformation::GetSolutionMatrix(){
    return _solutionMat;
}

void KrigingDeformation::setCtrlPtWeight(std::shared_ptr<ArrayXd> ctrlPtWeight)
{
    ArrayXd* newWeights=new ArrayXd();
    newWeights->resize(_nbCtrlPt);

    unsigned int new_lastIt=0, nbElem=0, lastIt=0;
    auto curIt=_identicalCtrlPtId.begin();
    for(;curIt!=_identicalCtrlPtId.end();++curIt){
        nbElem = (*curIt)-lastIt;
        if (lastIt < ctrlPtWeight->size() && nbElem>0){
            newWeights->middleRows(new_lastIt, nbElem) = ctrlPtWeight->middleRows(lastIt, nbElem);
        }
        new_lastIt+=nbElem;
        lastIt=(*curIt)+1;
    }

    if (lastIt < ctrlPtWeight->size()){
        newWeights->bottomRows(ctrlPtWeight->size() - lastIt) = ctrlPtWeight->bottomRows(ctrlPtWeight->size() - lastIt);
    }
    _ctrlPtWeightArray.reset(newWeights);
    delete newWeights;

    if (_solverState >= SolverSystemState::initializedNotSolved){
        _systemMatEuclid->topLeftCorner(_nbCtrlPt, _nbCtrlPt).diagonal() = *_ctrlPtWeightArray;
        if (_kernelType == KernelType::geodesic && _systemMatGeodesic)
            _systemMatGeodesic->topLeftCorner(_nbCtrlPt, _nbCtrlPt).diagonal() = *_ctrlPtWeightArray;
        _solverState = SolverSystemState::initializedNotSolved;
    }
}

std::shared_ptr<const ArrayXd> KrigingDeformation::getCtrlPtWeight(){
    return _ctrlPtWeightArray;
}
