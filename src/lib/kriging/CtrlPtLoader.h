/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef CTRL_PT_LOADER
#define CTRL_PT_LOADER


#include <string>

#include "hbm/types.h"

namespace piper {
    namespace kriging {
    
        /// <summary>
        /// Class for reading control point data from a proprietary text format.
        /// The format is expected to be 'x y z' coordinates per line, or 'id x y z',
        /// but it is not checked - data are read line by line and each element is stored as a single entity.
        /// </summary>
        class CtrlPtLoader
        {
            public:        

                CtrlPtLoader() {}

                /// <summary>
                /// Initializes a new instance of the <see cref="CtrlPtLoader"/> class and reads control points from a specified file.
                /// Can throw a std::exception in case the loading fails for some reason => not recommended to use, use no parameter constructor + loadData. 
                /// </summary>
                /// <param name="filename">Path to a file with control points.</param>
                CtrlPtLoader(std::string const& filename);
                
                /// <summary>
                /// Reads control points from a specified file. Any previously loaded data will be discarded.
                /// </summary>
                /// <param name="filename">Path to a file with control points.</param>
                /// <returns><c>True</c> in case file was read correctly, <c>false</c> in case of some failure.</returns>
                bool loadData(std::string const& filename);
                
                /// <summary>
                /// Gets the control points read by this CtrlPtLoader.
                /// If nothing has been read yet, it will be empty.
                /// </summary>
                /// <returns>A reference to a vector with loaded control points. To determine format of the data,
                /// use getItemsPerLine() - usually will be either 3 (for 'x y z' per line format), or 4 (for 'id x y z').</returns>
                std::vector<double>& getData();

                /// <summary>
                /// Extracts only the coordinates from the data and stores in the provided vector.
                /// !!! This assumes that the coordinates are always the last three numbers on each line - if that changes in the future,
                /// this method needs to be modified. !!!
                /// </summary>
                /// <param name="vcoord">Vector that will be filled with all coordinates read so far, in the order they were in the input file.
                /// This method does NOT clear the vector - all coordinates will be pushed back behind those that are already in the vector.</param>
                void getCoordinates(std::vector<hbm::Coord>& vcoord);
                
                /// <summary>
                /// Number of values that were on each line of the input file.
                /// CtrlPtLoader does not check validity of the file, specifically, if number of items per line
                /// differs in the file, this number will still just be equal to (number of data items) / (number of lines).
                /// This also means that it will be unreliable if there are empty lines in the middle of the file or end of the file.
                /// </summary>
                /// <returns>Values per line in the provided file. 0 if nothing has been read yet.</returns>
                int getItemsPerLine();

            private:
                    std::vector<double> _datavec;
                    int noOfItemsPerLine = 0;
        };

    }
}

#endif
