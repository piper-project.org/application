/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "KrigingPiperInterface.h"
#include "vtkPIPERFilters/vtkSurfaceDistance.h"

#define PIPER_KRIG_BOXES_DEBUGVIS 0 // visualization of how boxes are split and what CPs are inside, DO NOT push code with this enabled to the remote repository, only for debugging
#if (PIPER_KRIG_BOXES_DEBUGVIS)
#include <vtkOpenGLActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkGlyph3DMapper.h>
#include <vtkSphereSource.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkDataSetSurfaceFilter.h>
#include "hbm/VtkSelectionTools.h"
#endif

#define PIPER_KRIG_BOXES_EXPORT 0 // export split boxes to file as .obj, DO NOT push code with this enabled to the remote repository, only for debugging
#if (PIPER_KRIG_BOXES_EXPORT)
#define exportFolder "D:\\boxes\\"
#include <vtkSTLWriter.h>
#include <vtkTriangleFilter.h>
#endif

#ifdef _OPENMP
#include "omp.h"
#endif

#include <vtkPointData.h>
#include <vtkMath.h>

#include <fstream>

#ifdef WIN32
    #ifndef __func__
        #define __func__ __FUNCTION__
    #endif
#endif


// from http://www.cplusplus.com/forum/beginner/154004/
namespace util
{
    #if __cplusplus >= 201402L // C++14

        using std::make_unique ;

    #else // C++11

        template < typename T, typename... CONSTRUCTOR_ARGS >
        std::unique_ptr<T> make_unique( CONSTRUCTOR_ARGS&&... constructor_args )
        { return std::unique_ptr<T>( new T( std::forward<CONSTRUCTOR_ARGS>(constructor_args)... ) ); }

    #endif // __cplusplus == 201402L
}

using namespace piper::hbm;
using namespace Eigen;

namespace piper
{
#if PIPER_KRIG_BOXES_DEBUGVIS
    vtkSmartPointer<vtkOpenGLActor>  SelectionBox(vtkOBBNodePtr obb)
    {
        vtkSmartPointer<vtkPolyData> Box_ActorData = obb->GenerateAsPolydata();
        
        vtkSmartPointer<vtkOpenGLActor> actor = vtkSmartPointer<vtkOpenGLActor>::New();
        vtkSmartPointer<vtkPolyDataMapper>mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputDataObject(Box_ActorData);
        actor->SetMapper(mapper);
        actor->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
        actor->GetProperty()->SetOpacity(0.3);
        return actor;
    }

    vtkSmartPointer<vtkGlyph3DMapper> AllPoints(vtkSmartPointer<vtkDataSet> sourceData, double radius)
    {

        // initialize mapper for point highlighting
        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
        sphereSource->SetCenter(0, 0, 0);
        sphereSource->SetRadius(radius);
        vtkSmartPointer<vtkGlyph3DMapper> pointHighlight = vtkSmartPointer<vtkGlyph3DMapper>::New();
        pointHighlight->OrientOff();

        pointHighlight->SetSourceConnection(sphereSource->GetOutputPort());
        pointHighlight->SetInputData(sourceData);
        pointHighlight->SetScaleModeToNoDataScaling();

        return pointHighlight;
    }
#endif

    double const KrigingPiperInterface::krigingDuplPointLimit = 0.01;

    // for each chosen node in the geodesic mesh, this array contains the nid of the point the chosen node represents 
    // which does not necessarily have to be the same node - for control points that do not lie on the surface, the closest node on the surface represents them
#define _PIPER_NID_MAP_CLOSESTNODES "nids_of_CP_repreByClosestNodes"

    KrigingPiperInterface::KrigingPiperInterface() :krigingDeformer(nullptr)
    {
        srcCtrlPt = std::make_shared<MatrixXd>();//<double,Dynamic,Dynamic,ColMajor> >();
        destCtrlPt = std::make_shared<MatrixXd>();
        srcMesh = std::make_shared<MatrixXd>();
        destMesh = std::make_shared<MatrixXd>();
        ctrlPtWeight = std::make_shared<ArrayXd>();
        _surfaceGeodesicMesh = NULL;
    }

    KrigingPiperInterface::~KrigingPiperInterface()
    {
        srcCtrlPt->resize(0, 0);
        destCtrlPt->resize(0, 0);
        srcMesh->resize(0, 0);
        destMesh->resize(0, 0);
        ctrlPtWeight->resize(0);
    }

    void KrigingPiperInterface::testWithRandomValues(int nbCtrlPt, int nbDim)
    {
        srand((unsigned int)time(0));
        try{
            // std::cout <<std::endl<<"Random generation selected : " << nbCtrlPt << " ctrlPt, "<< nbNodes <<" node" <<std::endl; 
            srcCtrlPt->resize(nbCtrlPt, nbDim);
            destCtrlPt->resize(nbCtrlPt, nbDim);
            srcCtrlPt->noalias() = MatrixXd::Random(nbCtrlPt, nbDim);
            destCtrlPt->noalias() = MatrixXd::Random(nbCtrlPt, nbDim) * 10;
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Bad Alloc creating srcCtrlPt / destCtrlPt / srcMesh in function " + std::string(__func__) + " in " + std::string(__FILE__));
            throw std::runtime_error(msg.c_str());
        }
        krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, true, false);
        compute();
    }

    void KrigingPiperInterface::setControlPoints(VCoord const& piperSrcCtrlPt, VCoord const& piperTargetCtrlPt, double nuggetForAllPoints,
        bool interpDisplacement, bool useDrift)
    {
        if (nuggetForAllPoints != 0)
        {
            ctrlPtWeight->resize(piperSrcCtrlPt.size());
            for (int i = 0; i < piperSrcCtrlPt.size(); ++i)
                (*ctrlPtWeight)(i) = nuggetForAllPoints;
        }
        setControlPoints(piperSrcCtrlPt, piperTargetCtrlPt, std::vector<double>(), interpDisplacement, useDrift);
    }

    void KrigingPiperInterface::setControlPoints(VCoord const& piperSrcCtrlPt, VCoord const& piperTargetCtrlPt,
        double nuggetForAllPoints, vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        if (nuggetForAllPoints != 0)
        {
            ctrlPtWeight->resize(piperSrcCtrlPt.size());
            for (int i = 0; i < piperSrcCtrlPt.size(); ++i)
                (*ctrlPtWeight)(i) = nuggetForAllPoints;
        }
        setControlPoints(piperSrcCtrlPt, piperTargetCtrlPt, std::vector<double>(), surfaceMeshGeodesic, distanceType);
    }

    void KrigingPiperInterface::setControlPoints(VCoord const& piperSrcCtrlPt, VCoord const& piperTargetCtrlPt,
        std::vector<double> const& piperCtrlPtWeight,
        vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        try{
            if (piperSrcCtrlPt.size() != 0 && piperTargetCtrlPt.size() != 0)
            {
                std::vector<Coord> reorderedSrcCtrlPt;
                std::vector<Coord> reorderedTargetCtrlPt;
                std::vector<double> reorderedCtrlPtWeight;
                preprocessGeodesicMesh(surfaceMeshGeodesic, piperSrcCtrlPt, piperTargetCtrlPt, piperCtrlPtWeight,
                    &reorderedSrcCtrlPt, &reorderedTargetCtrlPt, &reorderedCtrlPtWeight);

                srcCtrlPt->resize(reorderedSrcCtrlPt.size(), 3);
                destCtrlPt->resize(reorderedTargetCtrlPt.size(), 3);
                ctrlPtWeight->resize(reorderedCtrlPtWeight.size());

#pragma omp parallel
                {
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedSrcCtrlPt.size(); ++i){
                        (*srcCtrlPt)(i, 0) = reorderedSrcCtrlPt[i][0];
                        (*srcCtrlPt)(i, 1) = reorderedSrcCtrlPt[i][1];
                        (*srcCtrlPt)(i, 2) = reorderedSrcCtrlPt[i][2];
                    }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedTargetCtrlPt.size(); ++i){
                        (*destCtrlPt)(i, 0) = reorderedTargetCtrlPt[i][0];
                        (*destCtrlPt)(i, 1) = reorderedTargetCtrlPt[i][1];
                        (*destCtrlPt)(i, 2) = reorderedTargetCtrlPt[i][2];
                    }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedCtrlPtWeight.size(); ++i){
                        (*ctrlPtWeight)(i) = reorderedCtrlPtWeight[i];
                    }
                }
                if (_surfaceGeodesicMesh)
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, _surfaceGeodesicMesh, distanceType);
                else
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, false, true); // use default when the mesh is null
            }
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Control points not set - allocation of memory failed");
            throw std::runtime_error(msg.c_str());
        }
    }


    void KrigingPiperInterface::setControlPoints(VCoord const& piperSrcCtrlPt, VCoord const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
        bool interpDisplacement, bool useDrift)
    {
        try{
            if (piperSrcCtrlPt.size() != 0 && piperTargetCtrlPt.size() != 0){
                srcCtrlPt->resize(piperSrcCtrlPt.size(), 3);
                destCtrlPt->resize(piperTargetCtrlPt.size(), 3);
                if (piperCtrlPtWeight.size() != 0)
                    ctrlPtWeight->resize(piperCtrlPtWeight.size());

#pragma omp parallel
            {
#pragma omp for schedule(dynamic)
                for (int i = 0; i < piperSrcCtrlPt.size(); ++i){
                    (*srcCtrlPt)(i, 0) = piperSrcCtrlPt[i](0);
                    (*srcCtrlPt)(i, 1) = piperSrcCtrlPt[i](1);
                    (*srcCtrlPt)(i, 2) = piperSrcCtrlPt[i](2);
                }
#pragma omp for schedule(dynamic)
                for (int i = 0; i < piperTargetCtrlPt.size(); ++i){
                    (*destCtrlPt)(i, 0) = piperTargetCtrlPt[i](0);
                    (*destCtrlPt)(i, 1) = piperTargetCtrlPt[i](1);
                    (*destCtrlPt)(i, 2) = piperTargetCtrlPt[i](2);
                }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < piperCtrlPtWeight.size(); ++i){
                        (*ctrlPtWeight)(i) = piperCtrlPtWeight[i];
                    }
                }
            }
            _surfaceGeodesicMesh = NULL;
            krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, interpDisplacement, useDrift);
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Control points not set - allocation of memory failed");
            throw std::runtime_error(msg.c_str());
        }

    }

    void KrigingPiperInterface::setControlPoints(piper::hbm::FEModel const& model, piper::hbm::VId const& piperSrcCtrlPt,
        std::vector<Coord> const& piperTargetCtrlPt, double nuggetForAllPoints, bool interpolateDisplacement, bool useDrift,
        vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        if (nuggetForAllPoints)
        {
            ctrlPtWeight->resize(piperSrcCtrlPt.size());
            for (int i = 0; i < piperSrcCtrlPt.size(); ++i)
                (*ctrlPtWeight)(i) = nuggetForAllPoints;
        }
        setControlPoints(model, piperSrcCtrlPt, piperTargetCtrlPt, std::vector<double>(), interpolateDisplacement, useDrift, surfaceMeshGeodesic, distanceType);
    }

    void KrigingPiperInterface::setControlPoints(piper::hbm::FEModel const& model, piper::hbm::VId const& piperSrcCtrlPt,
        std::vector<Coord> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight, bool interpolateDisplacement, bool useDrift,
        vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        try{
            size_t npts = piperSrcCtrlPt.size();
            if (npts != 0 && piperTargetCtrlPt.size() != 0 && npts == piperTargetCtrlPt.size())
            {
                piper::hbm::VId reorderedSrcCtrlPt;
                std::vector<Coord> reorderedTargetCtrlPt;
                std::vector<double> reorderedCtrlPtWeight;
                preprocessGeodesicMesh(model, surfaceMeshGeodesic, piperSrcCtrlPt, piperTargetCtrlPt, piperCtrlPtWeight,
                    &reorderedSrcCtrlPt, &reorderedTargetCtrlPt, &reorderedCtrlPtWeight);

                srcCtrlPt->resize(reorderedSrcCtrlPt.size(), 3);
                destCtrlPt->resize(reorderedTargetCtrlPt.size(), 3);
                if (reorderedCtrlPtWeight.size() != 0)
                    ctrlPtWeight->resize(reorderedCtrlPtWeight.size());

#ifdef _OPENMP 
                int max_threads = omp_get_max_threads();
#else
                int max_threads = 1;
#endif

                std::vector<std::pair<piper::hbm::VId::const_iterator, piper::hbm::VId::const_iterator> > chunkSrcCtrlPts;
                std::vector<size_t> chunkId;
                chunkSrcCtrlPts.reserve(max_threads);

                size_t chunk_size = reorderedSrcCtrlPt.size() / max_threads;

                auto cur_iter_srcCtrlPt = reorderedSrcCtrlPt.begin();
                size_t id = 0;
                for (int i = 0; i < max_threads - 1; ++i)
                {
                    auto last_iter_srcCtrlPt = cur_iter_srcCtrlPt;
                    std::advance(cur_iter_srcCtrlPt, chunk_size);
                    chunkSrcCtrlPts.push_back(std::make_pair(last_iter_srcCtrlPt, cur_iter_srcCtrlPt));
                    chunkId.push_back(id);
                    id += chunk_size;
                }
                chunkSrcCtrlPts.push_back(std::make_pair(cur_iter_srcCtrlPt, reorderedSrcCtrlPt.end()));
                chunkId.push_back(id);

#pragma omp parallel
                {
#pragma omp for schedule(dynamic) 
                    for (int k = 0; k < max_threads; ++k){
                        size_t i = chunkId[k];
                        for (auto it = chunkSrcCtrlPts[k].first; it != chunkSrcCtrlPts[k].second; ++i, ++it)
                        {
                            Node n = model.getNode(*it);
                            (*srcCtrlPt)(i, 0) = n.getCoordX();
                            (*srcCtrlPt)(i, 1) = n.getCoordY();
                            (*srcCtrlPt)(i, 2) = n.getCoordZ();
                        }
                    }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedTargetCtrlPt.size(); ++i){
                        (*destCtrlPt)(i, 0) = reorderedTargetCtrlPt[i][0];
                        (*destCtrlPt)(i, 1) = reorderedTargetCtrlPt[i][1];
                        (*destCtrlPt)(i, 2) = reorderedTargetCtrlPt[i][2];
                    }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedCtrlPtWeight.size(); ++i){
                        (*ctrlPtWeight)(i) = reorderedCtrlPtWeight[i];
                    }
                }
                if (_surfaceGeodesicMesh)
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, _surfaceGeodesicMesh, distanceType);
                else
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, interpolateDisplacement, useDrift);
            }
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Control points not set - allocation of memory failed");
            throw std::runtime_error(msg.c_str());
        }
    }

    void KrigingPiperInterface::setControlPoints(piper::hbm::FEModel const& model, piper::hbm::VId const& piperSrcCtrlPt,
        piper::hbm::VId const& piperTargetCtrlPt, double nuggetForAllPoints, bool interpolateDisplacement, bool useDrift,
        vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        if (nuggetForAllPoints)
        {
            ctrlPtWeight->resize(piperSrcCtrlPt.size());
            for (int i = 0; i < piperSrcCtrlPt.size(); ++i)
                (*ctrlPtWeight)(i) = nuggetForAllPoints;
        }
        setControlPoints(model, piperSrcCtrlPt, piperTargetCtrlPt, std::vector<double>(), interpolateDisplacement, useDrift, surfaceMeshGeodesic, distanceType);
    }

    void KrigingPiperInterface::setControlPoints(piper::hbm::FEModel const& model, piper::hbm::VId const& piperSrcCtrlPt,
        piper::hbm::VId const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight, bool interpolateDisplacement, bool useDrift,
        vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType)
    {
        try{
            size_t npts = piperSrcCtrlPt.size();
            if (npts != 0 && piperTargetCtrlPt.size() != 0 && npts == piperTargetCtrlPt.size())
            {
                piper::hbm::VId reorderedSrcCtrlPt;
                piper::hbm::VId reorderedTargetCtrlPt;
                std::vector<double> reorderedCtrlPtWeight;
                preprocessGeodesicMesh(model, surfaceMeshGeodesic, piperSrcCtrlPt, piperTargetCtrlPt, piperCtrlPtWeight,
                    &reorderedSrcCtrlPt, &reorderedTargetCtrlPt, &reorderedCtrlPtWeight);

                srcCtrlPt->resize(reorderedSrcCtrlPt.size(), 3);
                destCtrlPt->resize(reorderedTargetCtrlPt.size(), 3);

                if (reorderedCtrlPtWeight.size() != 0)
                    ctrlPtWeight->resize(reorderedCtrlPtWeight.size());

#ifdef _OPENMP 
                int max_threads = omp_get_max_threads();
#else
                int max_threads = 1;
#endif

                std::vector<std::pair<piper::hbm::VId::const_iterator, piper::hbm::VId::const_iterator> > chunkSrcCtrlPts;
                std::vector<std::pair<piper::hbm::VId::const_iterator, piper::hbm::VId::const_iterator> > chunkTargetCtrlPts;
                std::vector<size_t> chunkId;
                chunkSrcCtrlPts.reserve(max_threads);
                chunkTargetCtrlPts.reserve(max_threads);

                size_t chunk_size = reorderedSrcCtrlPt.size() / max_threads;

                auto cur_iter_srcCtrlPt = reorderedSrcCtrlPt.begin();
                auto cur_iter_targetCtrlPt = reorderedTargetCtrlPt.begin();
                size_t id = 0;
                for (int i = 0; i < max_threads - 1; ++i)
                {
                    auto last_iter_srcCtrlPt = cur_iter_srcCtrlPt;
                    auto last_iter_targetCtrlPt = cur_iter_targetCtrlPt;
                    std::advance(cur_iter_srcCtrlPt, chunk_size);
                    std::advance(cur_iter_targetCtrlPt, chunk_size);
                    chunkSrcCtrlPts.push_back(std::make_pair(last_iter_srcCtrlPt, cur_iter_srcCtrlPt));
                    chunkTargetCtrlPts.push_back(std::make_pair(last_iter_targetCtrlPt, cur_iter_targetCtrlPt));
                    chunkId.push_back(id);
                    id += chunk_size;
                }
                chunkSrcCtrlPts.push_back(std::make_pair(cur_iter_srcCtrlPt, reorderedSrcCtrlPt.end()));
                chunkTargetCtrlPts.push_back(std::make_pair(cur_iter_targetCtrlPt, reorderedTargetCtrlPt.end()));
                chunkId.push_back(id);


#pragma omp parallel shared(chunkSrcCtrlPts,chunkTargetCtrlPts)
                {
#pragma omp for schedule(dynamic) 
                    for (int k = 0; k < max_threads; ++k){
                        size_t i = chunkId[k];
                        for (auto it = chunkSrcCtrlPts[k].first; it != chunkSrcCtrlPts[k].second; ++i, ++it)
                        {
                            Node n = model.getNode(*it);
                            (*srcCtrlPt)(i, 0) = n.getCoordX();
                            (*srcCtrlPt)(i, 1) = n.getCoordY();
                            (*srcCtrlPt)(i, 2) = n.getCoordZ();
                        }
                    }
#pragma omp for schedule(dynamic) 
                    for (int k = 0; k < max_threads; ++k){
                        size_t i = chunkId[k];
                        for (auto it = chunkTargetCtrlPts[k].first; it != chunkTargetCtrlPts[k].second; ++i, ++it)
                        {
                            Node n = model.getNode(*it);
                            (*destCtrlPt)(i, 0) = n.getCoordX();
                            (*destCtrlPt)(i, 1) = n.getCoordY();
                            (*destCtrlPt)(i, 2) = n.getCoordZ();
                        }
                    }
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < reorderedCtrlPtWeight.size(); ++i){
                        (*ctrlPtWeight)(i) = reorderedCtrlPtWeight[i];
                    }
                }
                if (_surfaceGeodesicMesh)
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, _surfaceGeodesicMesh, distanceType);
                else
                    krigingDeformer = util::make_unique<KrigingDeformation>(srcCtrlPt, destCtrlPt, ctrlPtWeight, interpolateDisplacement, useDrift);
            }

        }
        catch (std::bad_alloc e)
        {
            std::string msg("Control points not set - allocation of memory failed");
            throw std::runtime_error(msg.c_str());
        }

    }


    void KrigingPiperInterface::preprocessGeodesicMesh(vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic,
        std::vector<Coord> const& piperSrcCtrlPt, std::vector<Coord> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
        std::vector<Coord> *reorderedSrcCtrlPt, std::vector<Coord> *reorderedTargetCtrlPt, std::vector<double> *reorderedCtrlPtWeight)
    {
        if (surfaceMeshGeodesic
            && ((size_t)surfaceMeshGeodesic->GetNumberOfPoints() >= piperSrcCtrlPt.size())) // necessarry, but not sufficient condition - but relatively easy to test and should get rid of simple errors like empty mesh specified etc.
        {
            // mark nodes chosen as control points
            vtkSmartPointer<vtkBitArray> chosenNodes = vtkSmartPointer<vtkBitArray>::New();
            chosenNodes->SetNumberOfValues(surfaceMeshGeodesic->GetNumberOfPoints());
            chosenNodes->SetName(_GEODESIC_CHOSENPOINTS);
            chosenNodes->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED); // first unmark all
            surfaceMeshGeodesic->GetPointData()->AddArray(chosenNodes);

            // for each control point, find the closest point on the mesh
            vtkSmartPointer<vtkKdTreePointLocator> ctrlPointsLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
            ctrlPointsLocator->SetDataSet(surfaceMeshGeodesic);
            ctrlPointsLocator->BuildLocator();

            srcCtrlMap.clear();
            double nodeCoord[3];
            vtkSmartPointer<vtkIdList> closest10Points = vtkSmartPointer<vtkIdList>::New();
            for (size_t i = 0; i < piperSrcCtrlPt.size(); i++) // mark control points
            {
                Coord curNode = piperSrcCtrlPt[i];
                nodeCoord[0] = curNode(0);
                nodeCoord[1] = curNode(1);
                nodeCoord[2] = curNode(2);
                vtkIdType vtkID = ctrlPointsLocator->FindClosestPoint(nodeCoord);
                if (chosenNodes->GetValue(vtkID) == IS_PRIMITIVE_SELECTED) // if this node was already chosen by other node, we have to do something to avoid having duplicated control points
                {
                    // try to find some other close-ish point, if none is found, discard this CP
                    closest10Points->Reset();
                    ctrlPointsLocator->FindClosestNPoints(10, nodeCoord, closest10Points); // find ten nearest points
                    for (vtkIdType j = 1; j < closest10Points->GetNumberOfIds(); j++)
                    {
                        vtkID = closest10Points->GetId(j);
                        if (chosenNodes->GetValue(vtkID) == IS_NOT_PRIMITIVE_SELECTED) // was not chosen yet, use it
                        {
                            chosenNodes->SetValue(vtkID, IS_PRIMITIVE_SELECTED);
                            srcCtrlMap[vtkID] = i;
                            break;
                        }
                    }
                }
                else
                {
                    chosenNodes->SetValue(vtkID, IS_PRIMITIVE_SELECTED);
                    srcCtrlMap[vtkID] = i;
                }
            }
            double temp[3], candidate[3], point[3];
            // store the mesh to later pass it in the compute function
            _surfaceGeodesicMesh = surfaceMeshGeodesic;
            _surfaceGeodesicMesh->Modified();

            for (vtkIdType i = 0; i < chosenNodes->GetNumberOfTuples(); i++)
            {
                if (chosenNodes->GetValue(i) == IS_PRIMITIVE_SELECTED)
                {
                    size_t id = srcCtrlMap[i];
                    point[0] = piperSrcCtrlPt[id](0);
                    point[1] = piperSrcCtrlPt[id](1);
                    point[2] = piperSrcCtrlPt[id](2);

                    // check it is not duplicated
                    for (vtkIdType j = i + 1; j < chosenNodes->GetNumberOfTuples(); j++)
                    {
                        if (chosenNodes->GetValue(j) == IS_PRIMITIVE_SELECTED)
                        {
                            size_t idCan = srcCtrlMap[j];
                            candidate[0] = piperSrcCtrlPt[idCan](0);
                            candidate[1] = piperSrcCtrlPt[idCan](1);
                            candidate[2] = piperSrcCtrlPt[idCan](2);
                            vtkMath::Subtract(point, candidate, temp);
                            if (vtkMath::Norm(temp) < krigingDuplPointLimit)
                            {
                                chosenNodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                                break;
                            }
                        }
                    }
                    if (chosenNodes->GetValue(i)) // if it is still marked, even after removing duplicities, push it to the result
                    {
                        (*reorderedSrcCtrlPt).push_back(piperSrcCtrlPt[id]);
                        (*reorderedTargetCtrlPt).push_back(piperTargetCtrlPt[id]);
                        if (piperCtrlPtWeight.size() == piperSrcCtrlPt.size())
                        {
                            (*reorderedCtrlPtWeight).push_back(piperCtrlPtWeight[id]);
                        }
                    }
                }
            }


            /*    ofstream source, target;
                source.open("D:\\source.txt");
                target.open("D:\\target.txt");
                for (Coord c : *reorderedSrcCtrlPt)
                source << c[0] << " " << c[1] << " " << c[2] << std::endl;
                for (Coord c : *reorderedTargetCtrlPt)
                target << c[0] << " " << c[1] << " " << c[2] << std::endl;
                source.close();
                target.close();*/
        }
        else
        {
            this->_surfaceGeodesicMesh = NULL; // make sure to turn off geodesic if the mesh was not specified
            (*reorderedSrcCtrlPt) = piperSrcCtrlPt;
            (*reorderedTargetCtrlPt) = piperTargetCtrlPt;
            (*reorderedCtrlPtWeight) = piperCtrlPtWeight;
        }
    }

    template <typename T>
    void KrigingPiperInterface::preprocessGeodesicMesh(piper::hbm::FEModel const& model, vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic,
        piper::hbm::VId const& piperSrcCtrlPt, std::vector<T> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
        piper::hbm::VId *reorderedSrcCtrlPt, std::vector<T> *reorderedTargetCtrlPt, std::vector<double> *reorderedCtrlPtWeight)
    {
        if (surfaceMeshGeodesic
            && ((size_t)surfaceMeshGeodesic->GetNumberOfPoints() >= piperSrcCtrlPt.size())) // necessarry, but not sufficient condition - but relatively easy to test and should get rid of simple errors like empty mesh specified etc.
        {
            // mark nodes chosen as control points
            vtkSmartPointer<vtkBitArray> chosenNodes = vtkSmartPointer<vtkBitArray>::New();
            chosenNodes->SetNumberOfValues(surfaceMeshGeodesic->GetNumberOfPoints());
            chosenNodes->SetName(_GEODESIC_CHOSENPOINTS);
            chosenNodes->FillComponent(0, IS_NOT_PRIMITIVE_SELECTED); // first unmark all
            surfaceMeshGeodesic->GetPointData()->AddArray(chosenNodes);

            // the chosen nodes do not have to be nodes of the mesh - for each chosen node, find the closest one on the mesh
            vtkSmartPointer<vtkKdTreePointLocator> ctrlPointsLocator = vtkSmartPointer<vtkKdTreePointLocator>::New();
            ctrlPointsLocator->SetDataSet(surfaceMeshGeodesic);
            ctrlPointsLocator->BuildLocator();
            // map of nid the chosen points represent, i.e. if the point is chosen, it is closest to some of the CP - the nid of the CP is marked here
            vtkSmartPointer<vtkIdTypeArray> origNodes = vtkSmartPointer<vtkIdTypeArray>::New();
            origNodes->SetNumberOfValues(surfaceMeshGeodesic->GetNumberOfPoints());
            origNodes->SetName(_PIPER_NID_MAP_CLOSESTNODES);
            surfaceMeshGeodesic->GetPointData()->AddArray(origNodes);
            
            srcCtrlMap.clear();
            double nodeCoord[3];
            vtkSmartPointer<vtkIdList> closest10Points = vtkSmartPointer<vtkIdList>::New();
            size_t counter = 0;
            for (Id id : piperSrcCtrlPt) // mark control points
            {
                Coord curNode = model.getNode(id).get();
                nodeCoord[0] = curNode(0);
                nodeCoord[1] = curNode(1);
                nodeCoord[2] = curNode(2);
                vtkIdType vtkID = ctrlPointsLocator->FindClosestPoint(nodeCoord);
                bool reg = true;
                if (chosenNodes->GetValue(vtkID) == IS_PRIMITIVE_SELECTED) // if this node was already chosen by other node, we have to do something to avoid having duplicated control points
                {
                    // try to find some other close-ish point, if none is found, discard this CP
                    closest10Points->Reset();
                    ctrlPointsLocator->FindClosestNPoints(10, nodeCoord, closest10Points); // find ten nearest points
                    reg = false;
                    for (vtkIdType j = 1; j < closest10Points->GetNumberOfIds(); j++)
                    {
                        vtkID = closest10Points->GetId(j);
                        if (chosenNodes->GetValue(vtkID) == IS_NOT_PRIMITIVE_SELECTED) // was not chosen yet, use it
                        {
                            chosenNodes->SetValue(vtkID, IS_PRIMITIVE_SELECTED);
                            origNodes->SetValue(vtkID, id);
                            reg = true;
                            break;
                        }
                    }
                }
                else
                {
                    chosenNodes->SetValue(vtkID, IS_PRIMITIVE_SELECTED);
                    origNodes->SetValue(vtkID, id);
                }
                if (reg)
                    srcCtrlMap[id] = counter;
                counter++; // move the counter even if the node was not registered - the srcCtrlMap must contain the index in the original vector, so unused indices must be skipped
            }
            // store the mesh to later pass it in the compute function
            _surfaceGeodesicMesh = surfaceMeshGeodesic;
            _surfaceGeodesicMesh->Modified();

            size_t npts = srcCtrlMap.size();
            reorderedSrcCtrlPt->resize(npts);
            reorderedTargetCtrlPt->resize(npts);
            int CPcount = 0;
            if (piperCtrlPtWeight.size() == piperSrcCtrlPt.size())
                reorderedCtrlPtWeight->resize(npts);

            for (vtkIdType i = 0; i < chosenNodes->GetNumberOfTuples(); i++)
            {
                if (chosenNodes->GetValue(i) == IS_PRIMITIVE_SELECTED)
                {
                    vtkIdType id = origNodes->GetValue(i);
                    (*reorderedSrcCtrlPt)[CPcount] = id; // piperSrcCtrlPt[srcCtrlMap[id]], which should equal to "id"
                    (*reorderedTargetCtrlPt)[CPcount++] = piperTargetCtrlPt[srcCtrlMap[id]];
                }
            }
            if (piperCtrlPtWeight.size() == npts)
            {
                CPcount = 0;
                for (vtkIdType i = 0; i < chosenNodes->GetNumberOfTuples(); i++)
                {
                    if (chosenNodes->GetValue(i))
                        (*reorderedCtrlPtWeight)[CPcount++] = piperCtrlPtWeight[srcCtrlMap[origNodes->GetValue(i)]];
                }
            }
        }
        else
        {
            _surfaceGeodesicMesh = NULL; // make sure to turn off geodesic if the mesh was not specified
            (*reorderedSrcCtrlPt) = piperSrcCtrlPt;
            (*reorderedTargetCtrlPt) = piperTargetCtrlPt;
            (*reorderedCtrlPtWeight) = piperCtrlPtWeight;
        }
    }

    void KrigingPiperInterface::setTargetControlPoints(hbm::VCoord& piperTargetCtrlPt)
    {
        hbm::VCoord reordered;
        if (_surfaceGeodesicMesh != NULL && !srcCtrlMap.empty())
        {
            reordered.resize(srcCtrlPt->rows());
            vtkSmartPointer<vtkBitArray> chosenNodes = vtkBitArray::SafeDownCast(_surfaceGeodesicMesh->GetPointData()->GetArray(_GEODESIC_CHOSENPOINTS));
            int CPcount = 0;
            for (vtkIdType i = 0; i < chosenNodes->GetNumberOfTuples(); i++)
            {
                if (chosenNodes->GetValue(i) == IS_PRIMITIVE_SELECTED)
                {
                    reordered[CPcount++] = piperTargetCtrlPt[srcCtrlMap[i]];
                }
            }
        }

        destCtrlPt->resize(reordered.empty() ? piperTargetCtrlPt.size() : reordered.size(), 3);
        if (reordered.empty())
        {
            for (int i = 0; i < piperTargetCtrlPt.size(); ++i)
            {
                (*destCtrlPt)(i, 0) = piperTargetCtrlPt[i][0];
                (*destCtrlPt)(i, 1) = piperTargetCtrlPt[i][1];
                (*destCtrlPt)(i, 2) = piperTargetCtrlPt[i][2];
            }
        }
        else
        {
            for (int i = 0; i < reordered.size(); ++i)
            {
                (*destCtrlPt)(i, 0) = reordered[i][0];
                (*destCtrlPt)(i, 1) = reordered[i][1];
                (*destCtrlPt)(i, 2) = reordered[i][2];
            }
        }
    }

    void KrigingPiperInterface::SetGeodesicPrecision(double geoDistanceEigenPrecision)
    {
        if (krigingDeformer)
            krigingDeformer->SetGeodesicPrecision(geoDistanceEigenPrecision);
    }

    void KrigingPiperInterface::SetGeodesicType(SurfaceDistance geoDistanceType)
    {
        if (krigingDeformer)
            krigingDeformer->SetGeodesicType(geoDistanceType);
    }

    void KrigingPiperInterface::setControlPointsWeight(std::vector<double> const& piperCtrlPtWeight)
    {
        try{
            if (piperCtrlPtWeight.size() != 0){
                ctrlPtWeight->resize(piperCtrlPtWeight.size());

#pragma omp parallel
                {
#pragma omp for schedule(dynamic)
                    for (int i = 0; i < piperCtrlPtWeight.size(); ++i){
                        (*ctrlPtWeight)(i) = piperCtrlPtWeight[i];
                    }
                }

            }

        }
        catch (std::bad_alloc e)
        {
            std::string msg("Nuggets not set - allocation of memory failed");
            throw std::runtime_error(msg.c_str());
        }

        if (krigingDeformer != nullptr)
        {
            krigingDeformer->setCtrlPtWeight(ctrlPtWeight);
        }

    }

    void KrigingPiperInterface::compute(bool autoRemoveCtrlPtDuplication, double minDuplicationDist)
    {
        if (srcCtrlPt->size() != destCtrlPt->size() || srcCtrlPt->size() == 0){
            std::string msg("Source and Target ControlPoints number do not match or are empty");
            throw std::runtime_error(msg.c_str());
        }
        try
        {
            krigingDeformer->SetAutoRemoveDupl(autoRemoveCtrlPtDuplication);
            krigingDeformer->SetMinDuplDistance(minDuplicationDist);
            if (ctrlPtWeight->size() == 0)
            {
                if (_surfaceGeodesicMesh)
                    krigingDeformer->FillSystemMatrixDense(srcCtrlPt, destCtrlPt, _surfaceGeodesicMesh);
                else
                    krigingDeformer->FillSystemMatrixDense(srcCtrlPt, destCtrlPt);
            }
            else
            {
                if (_surfaceGeodesicMesh)
                    krigingDeformer->FillSystemMatrixDense(srcCtrlPt, destCtrlPt, ctrlPtWeight, _surfaceGeodesicMesh);
                else
                    krigingDeformer->FillSystemMatrixDense(srcCtrlPt, destCtrlPt, ctrlPtWeight);
            }
            krigingDeformer->SolveSystemMatrixDense();
        }
        catch (std::exception e){
            throw e;
        }
    }

    void KrigingPiperInterface::reCompute()
    {
        try
        {
            if (!krigingDeformer->SolveSystemMatrixDense())
            {
                std::string msg("Run compute first - system not initialised.");
                throw std::runtime_error(msg.c_str());
            }
        }
        catch (std::exception e){
            throw e;
        }
    }

    void KrigingPiperInterface::clearSystemMatrix()
    {
        if (krigingDeformer != nullptr)
            krigingDeformer->ClearSystemMatrix();
    }

    void KrigingPiperInterface::extractFaceNeighborhood(vtkSmartPointer<vtkKdTreePointLocator> cpLocator,
        int paramNClosestPoints, double(&face)[4][3],
        vtkSmartPointer<vtkIdList> addCP)
    {
        vtkSmartPointer<vtkIdList> nearestNeighbors = vtkSmartPointer<vtkIdList>::New();
        double farthestPoint[3];
        vtkIdType i;
        // start at a corner as the boundary sample point
        if (cpLocator->GetDataSet()->GetNumberOfPoints() > paramNClosestPoints)
        {
            cpLocator->FindClosestNPoints(paramNClosestPoints, face[0], nearestNeighbors);
            for (i = 0; i < nearestNeighbors->GetNumberOfIds(); i++)
                addCP->InsertUniqueId(nearestNeighbors->GetId(i));

            // now process nodes to deform within the radius of the boundary sample point
            cpLocator->GetDataSet()->GetPoint(nearestNeighbors->GetId(nearestNeighbors->GetNumberOfIds() - 1), farthestPoint);
            double radius = sqrt(vtkMath::Distance2BetweenPoints(face[0], farthestPoint));

            // direction vectors - we will cover the face starting from corner 0 to the opposite one (index 2)
            double dir1[3], dir3[3]; // directions to face-points with indices 1 and 3
            vtkMath::Subtract(face[1], face[0], dir1);
            double dir1norm = vtkMath::Norm(dir1);
            if (dir1norm == 0)  return;
            vtkMath::Subtract(face[3], face[0], dir3);
            double dir3norm = vtkMath::Norm(dir3);
            if (dir3norm == 0)  return;
            double coveredSquare = radius; // the edge length of the square inside the sphere on the surface of the face - a safe "covered area" measure        
            // now process recursively the three remaining quadriliterals of this face when the covered square gets cutted off
            double subQuad[4][3];
            // make face[0] + dir1/2/3 = corner points of the three new subquads -> normalize, then multiply by coveredSquare distance
            vtkMath::MultiplyScalar(dir1, coveredSquare / dir1norm);
            vtkMath::MultiplyScalar(dir3, coveredSquare / dir3norm);
            if (dir1norm > coveredSquare) // we have not yet covered the border of the face along the direction of point 1
            {
                for (i = 0; i < 3; i++)
                {
                    subQuad[0][i] = face[0][i] + dir1[i];
                    subQuad[1][i] = face[1][i];
                    subQuad[2][i] = face[1][i] + dir3[i];
                    subQuad[3][i] = face[0][i] + dir1[i] + dir3[i];
                }
                extractFaceNeighborhood(cpLocator, paramNClosestPoints, subQuad, addCP);
            }
            if (dir3norm > coveredSquare) // we have not yet covered the border of the face along the direction of point 3
            {
                for (i = 0; i < 3; i++)
                {
                    subQuad[0][i] = face[0][i] + dir3[i];
                    subQuad[1][i] = face[0][i] + dir1[i] + dir3[i];
                    subQuad[2][i] = face[3][i] + dir1[i];
                    subQuad[3][i] = face[3][i];
                }
                extractFaceNeighborhood(cpLocator, paramNClosestPoints, subQuad, addCP);

                if (dir1norm > coveredSquare) // if both dir1 and dir3 were not covered yet, there is also the third quadrilateral (in the direction of point 2)
                {
                    for (i = 0; i < 3; i++)
                    {
                        subQuad[0][i] = face[0][i] + dir1[i] + dir3[i];
                        subQuad[1][i] = face[1][i] + dir3[i];
                        subQuad[2][i] = face[2][i];
                        subQuad[3][i] = face[3][i] + dir1[i];
                    }
                    extractFaceNeighborhood(cpLocator, paramNClosestPoints, subQuad, addCP);
                }
            }
        }
        else
            for (i = 0; i < cpLocator->GetDataSet()->GetNumberOfPoints(); i++)
                addCP->InsertNextId(i);
    }

    vtkSmartPointer<vtkPolyData> buildGrid(double *bounds, double step)
    {
        // create a regular grid over the whole bounding box to see the deformation on a regular grid
        vtkIdType gridX = (bounds[1] - bounds[0]) / step;
        vtkIdType gridY = (bounds[3] - bounds[2]) / step;
        vtkIdType gridZ = (bounds[5] - bounds[4]) / step;
        vtkSmartPointer<vtkPoints> gridPoints = vtkSmartPointer<vtkPoints>::New();
        for (vtkIdType i = 0; i <= gridX; i++)
        {
            double x = bounds[0] + i * step;
            for (vtkIdType j = 0; j <= gridY; j++)
            {
                double y = bounds[2] + j * step;
                for (vtkIdType k = 0; k <= gridZ; k++)
                {
                    gridPoints->InsertNextPoint(x, y, bounds[4] + k * step);
                }
            }
        }

        vtkSmartPointer<vtkPolyData> grid = vtkSmartPointer<vtkPolyData>::New();
        grid->Allocate();
        grid->SetPoints(gridPoints);
        vtkIdType line[2];
        for (vtkIdType i = 0; i <= gridX; i++)
        {
            vtkIdType offset = (gridZ + 1) * (gridY + 1) * i; // point index offset
            for (vtkIdType j = 0; j < gridY; j++)
            {
                for (vtkIdType k = 0; k < gridZ; k++)
                {
                    // add three lines from the current vertex - one in each axis direction
                    line[0] = offset + k;
                    line[1] = line[0] + 1;
                    grid->InsertNextCell(VTK_LINE, 2, line);
                    line[1] = line[0] + gridZ + 1;
                    grid->InsertNextCell(VTK_LINE, 2, line);
                    if (i < gridX)
                    {
                        line[1] = line[0] + (gridZ + 1) * (gridY + 1);
                        grid->InsertNextCell(VTK_LINE, 2, line);
                    }
                }
                // add the last line up (edge of the box)
                line[0] = offset + gridZ;
                line[1] = line[0] + gridZ + 1;
                grid->InsertNextCell(VTK_LINE, 2, line);
                if (i < gridX)
                {
                    line[1] = line[0] + (gridZ + 1) * (gridY + 1);
                    grid->InsertNextCell(VTK_LINE, 2, line);
                }
                offset += gridZ + 1;
            }
            // add the lines on the top
            for (vtkIdType k = 0; k < gridZ; k++)
            {
                line[0] = offset + k;
                line[1] = line[0] + 1;
                grid->InsertNextCell(VTK_LINE, 2, line);
                if (i < gridX)
                {
                    line[1] = line[0] + (gridZ + 1) * (gridY + 1);
                    grid->InsertNextCell(VTK_LINE, 2, line);
                }
            }
            if (i < gridX)
            {
                line[0] = offset + gridZ;
                line[1] = line[0] + (gridZ + 1) * (gridY + 1);
                grid->InsertNextCell(VTK_LINE, 2, line);
            }
        }
        return grid;
    }

    void KrigingPiperInterface::applyDeformationInABox(vtkUnstructuredGrid *deformedMesh, vtkSmartPointer<vtkPoints> referencePoints,
        VId &controlNodesCandidates, std::vector<bool> &fixedNodes, boost::container::vector<double> &ctrlPtWeight, boost::container::vector<vtkOBBNodePtr> &OBBNodes,
        unsigned int splitBoxThresholdCP, double splitBoxOverlap, bool interpolateDisplacement, bool useDrift, bool boxesBuiltOverSource,
        vtkUnstructuredGrid *refMesh)
    {
        std::sort(controlNodesCandidates.begin(), controlNodesCandidates.end());
        
        vtkSmartPointer<vtkSelectionTools> st = vtkSmartPointer<vtkSelectionTools>::New();
        vtkSmartPointer<vtkUnstructuredGrid> refPointsGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
        refPointsGrid->SetPoints(boxesBuiltOverSource ? referencePoints : deformedMesh->GetPoints());
        vtkSmartPointer<vtkBitArray> sel_nodes = vtkSelectionTools::ObtainSelectionArrayPoints(refPointsGrid, false);
        st->AddInputData(refPointsGrid);
        st->SetSelectionTargetType(SELECTION_TARGET::NODES);

        vtkSmartPointer<vtkBitArray> control_nodes = vtkSmartPointer<vtkBitArray>::New();
        control_nodes->SetNumberOfValues(sel_nodes->GetNumberOfTuples());

        // an array to contain the cummulative results of the kriging - each time a per-part kriging is invoked,
        // the results are cummulated (added) to this array. the timesModified then holds the count of how many times was a certain point
        // modified so that an average of the cummulated values can be made in the end
        vtkSmartPointer<vtkPoints> result = vtkSmartPointer<vtkPoints>::New();
        result->SetNumberOfPoints(deformedMesh->GetNumberOfPoints());
        vtkSmartPointer<vtkIntArray> timesModified = vtkSmartPointer<vtkIntArray>::New();
        timesModified->SetNumberOfComponents(1);
        timesModified->SetNumberOfValues(deformedMesh->GetNumberOfPoints());
        for (int i = 0; i < deformedMesh->GetNumberOfPoints(); i++)
        {
            // average will be made as result[i] / timesModified[i], but if there is no change - timesModified[i] == 0 - the original value will be used
            timesModified->SetValue(i, 0);
            result->SetPoint(i, 0, 0, 0);
        }

#if PIPER_KRIG_BOXES_DEBUGVIS
        vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
        vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
        renderWindow->AddRenderer(renderer);

        vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
        renderWindowInteractor->SetRenderWindow(renderWindow);

        double rootMidPoint[3];
        for (int i = 0; i < 3; i++)
            rootMidPoint[i] = OBBNodes[0]->Corner[i] + (OBBNodes[0]->Axes[0][i] + OBBNodes[0]->Axes[1][i] + OBBNodes[0]->Axes[2][i]) / 2;
#endif

        for (auto it = OBBNodes.begin(); it != OBBNodes.end(); it++) // for each OBB, do separate kriging
        {
#if PIPER_KRIGING_PROFILING
            time_t start = clock();
#endif
            // reset selection
            for (vtkIdType i = 0; i < sel_nodes->GetNumberOfTuples(); i++)
                sel_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);

            st->UseSelectByOrientedBox((*it)->Corner, (*it)->Axes); // perform selection by OBB
            st->Update();
            // copy the selection to control nodes, then remove all non-skin nodes from it
            control_nodes->CopyComponent(0, sel_nodes, 0);

            // the controlNodesCandidates list is sorted, use that to combine it with the selection array
            vtkIdType i = 0;
            for (auto iter = controlNodesCandidates.begin(); iter != controlNodesCandidates.end(); iter++, i++)
            {
                for (; i < *iter; i++)
                    control_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED); // deselect all nodes until the next candidate node
                // now i == *it, the index of the candidate node - leave it unchanged - if it is selected, keep it selected, if it is not, keep it that way
            }
            // deselect the remaining nodes after the last candidate node
            for (; i < control_nodes->GetNumberOfTuples(); i++)
                control_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);

     /*    
            // THIS IS NOT USED ANYMORE RIGHT NOW, ASSUMES NUGGETS ARE SET TO ALL NODES BEFORE THIS -> IF EVER THIS GETS RE-INTRODUCED, FILL THE NUGGETS
            vtkSmartPointer<vtkIdList> cellIds = vtkSmartPointer<vtkIdList>::New();
            // select nodes on the boundary of the box as control points
            // do it by checking if they have a neighbour outside the box
            for (i = 0; i < sel_nodes->GetNumberOfTuples(); i++)
            {
                if (sel_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED // if it is selected...
                    && control_nodes->GetValue(i) == IS_NOT_PRIMITIVE_SELECTED)// ...and is not a control node already, it is a candidate to be a control node
                {
                    deformedMesh->GetPointCells(i, cellIds);
                    for (vtkIdType j = 0; j < cellIds->GetNumberOfIds(); j++) // for each cell connected to the candidate node
                    {
                        vtkCell *cell = deformedMesh->GetCell(cellIds->GetId(j));
                        for (vtkIdType k = 0; k < cell->GetNumberOfPoints(); k++)
                        {
                            // if one of the points connected to node "i" is not a bone and is not part of the selection,
                            // it is outside the box -> "i" is on the boundary, use it as a control point
                            // all bones have been removed from the selection -> there should be no bony neighbour unless a node
                            // of a soft tissue shares a node with a bone - should not be happening...and if it is, it is probably not a big deal
                            // that we will get one few more control points
                            if (cell->GetPointId(k) != i &&
                                sel_nodes->GetValue(cell->GetPointId(k)) == IS_NOT_PRIMITIVE_SELECTED)
                            {
                                control_nodes->SetValue(i, IS_PRIMITIVE_SELECTED);
                                j = cellIds->GetNumberOfIds(); // break the outer loop
                                break; // break this loop
                            }
                        }
                    }
                }
            }*/

            // split box into smaller one if there are too many control points / other nodes inside of it
            // first "squeeze" the control points and selected points array, i.e. create an array that has only the indices of the selected nodes
            vtkSmartPointer<vtkIntArray> controlNodesSqueezed = vtkSmartPointer<vtkIntArray>::New();
            controlNodesSqueezed->SetNumberOfComponents(1);
            vtkSmartPointer<vtkIntArray> selNodesSqueezed = vtkSmartPointer<vtkIntArray>::New();
            selNodesSqueezed->SetNumberOfComponents(1);
            for (i = 0; i < control_nodes->GetNumberOfTuples(); i++)
            {
                if (control_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED) controlNodesSqueezed->InsertNextValue(i);
                if (sel_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED) selNodesSqueezed->InsertNextValue(i);
            }

            boost::container::vector < std::pair<vtkSmartPointer<vtkIntArray>, int>> nodesAndLimits;
            nodesAndLimits.push_back(std::pair<vtkSmartPointer<vtkIntArray>, int>(controlNodesSqueezed, splitBoxThresholdCP));
            nodesAndLimits.push_back(std::pair<vtkSmartPointer<vtkIntArray>, int>(selNodesSqueezed, std::numeric_limits<int>::max())); // allow virtually any number of nodes per box
            boost::container::vector<std::pair<vtkOBBNodePtr, boost::container::vector<vtkSmartPointer<vtkIntArray>>>> splittedBoxes =
                st->SplitOBBRecursivelyByPoints((*it), referencePoints, nodesAndLimits, splitBoxOverlap);

#if PIPER_KRIGING_PROFILING
            time_t end = clock();

            std::cout << "number of boxes after splitting: " << splittedBoxes.size() << std::endl;
            std::cout << "split time: " << ((end - start) / (double)CLOCKS_PER_SEC) << "s" << std::endl;

            time_t preprocTime = 0;
            time_t krigingTime = 0;
            krigDeformTime = 0;
            krigMatrixTime = 0;
            krigMatrixPreprocessTime = 0;
#endif

            // build point locators for finding relevant control points and regular points near boundaries
            std::map<vtkIdType , vtkSmartPointer<vtkKdTreePointLocator>> cpBoxLocators;
            double CP[3];
            i = 0;
            for (auto &n : splittedBoxes)
            {
                vtkSmartPointer<vtkPoints> pointsCP = vtkSmartPointer<vtkPoints>::New();
                pointsCP->SetNumberOfPoints(n.second[0]->GetNumberOfTuples());
                vtkSmartPointer<vtkIdTypeArray> origCPID = vtkSmartPointer<vtkIdTypeArray>::New();
                origCPID->SetNumberOfValues(n.second[0]->GetNumberOfTuples());
                origCPID->SetName("CPorig");
                for (vtkIdType j = 0; j < n.second[0]->GetNumberOfTuples(); j++)
                {
                    referencePoints->GetPoint(n.second[0]->GetValue(j), CP);
                    origCPID->SetValue(j, n.second[0]->GetValue(j));
                    pointsCP->SetPoint(j, CP);
                }
                vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                cpHolder->SetPoints(pointsCP);
                cpHolder->GetPointData()->AddArray(origCPID);

                n.first->ID = i++; // set the index in splittedBoxes for each box

                cpBoxLocators[n.first->ID] = vtkSmartPointer<vtkKdTreePointLocator>::New();
                cpBoxLocators[n.first->ID]->SetDataSet(cpHolder);
                if (cpHolder->GetNumberOfPoints() > 0)
                    cpBoxLocators[n.first->ID]->BuildLocator();
                    
            }
            
            for (auto iter = splittedBoxes.begin(); iter != splittedBoxes.end(); iter++)
            {
#if (PIPER_KRIG_BOXES_EXPORT)
                vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
                vtkSmartPointer<vtkTriangleFilter> triang = vtkSmartPointer<vtkTriangleFilter>::New();
                triang->SetInputData(iter->first->GenerateAsPolydata());
                triang->Update();
                writer->SetInputData(triang->GetOutput());
                std::stringstream path;
                path << exportFolder << iter->first->ID << ".stl";
                writer->SetFileName(path.str().c_str());
                writer->Update();
#endif

                // do not process "insignificant" boxes - but still keep them to be potentially used as neighbors
                if (iter->second[0]->GetNumberOfTuples() < 5)
                    continue;
#if PIPER_KRIGING_PROFILING
                start = clock();
#endif
                //first fill back the sel_nodes and control_nodes arrays - have to erase it and fill
                for (i = 0; i < sel_nodes->GetNumberOfTuples(); i++)
                {
                    sel_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                    control_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                }

#if PIPER_KRIG_BOXES_DEBUGVIS
                // reset visualization
                renderer = vtkSmartPointer<vtkRenderer>::New();
                renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
                renderWindow->AddRenderer(renderer);
                renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
                renderWindowInteractor->SetRenderWindow(renderWindow);

                vtkSmartPointer<vtkOpenGLActor> actor = SelectionBox(iter->first);
                actor->GetProperty()->SetOpacity(0.2);
                actor->GetProperty()->SetColor(1, 0, 0);
                actor->GetProperty()->EdgeVisibilityOn();
                renderer->AddActor(actor);

                if (iter->second[0])
                {
                    vtkSmartPointer<vtkPoints> pointsInCurBox = vtkSmartPointer<vtkPoints>::New();
                    for (vtkIdType j = 0; j < iter->second[0]->GetNumberOfTuples(); j++)
                    {
                        pointsInCurBox->InsertNextPoint(referencePoints->GetPoint(iter->second[0]->GetValue(j)));
                    }
                    vtkSmartPointer<vtkPolyData> curBoxPointsHolder = vtkSmartPointer<vtkPolyData>::New();
                    curBoxPointsHolder->SetPoints(pointsInCurBox);
                    vtkSmartPointer<vtkOpenGLActor> curBoxActor = vtkSmartPointer<vtkOpenGLActor>::New();
                    curBoxActor->SetMapper(AllPoints(curBoxPointsHolder, 0.5));
                    renderer->AddActor(curBoxActor);
                }

                vtkSmartPointer<vtkPoints> addedPoints = vtkSmartPointer<vtkPoints>::New();
#endif
                // add nodes and control points near boundary to this boxe's set of nodes/CPs to ensure smooth transformation between boxes
                double corners[4][3]; // coordinates of the four corners of the boundary face
                int N = 30; // parameter, how many nearest neighbors to search for
                for (int k = 0; k < 6; k++)
                {
                    std::set<vtkIdType> processedBoxes; // the following might add some nearby boxes twice, so avoid that by keeping track of what was added
                    iter->first->GetBoundaryFace(k, corners);
                    // collect all neighbours within a given axis
                    vtkSmartPointer<vtkPoints> cpNei = vtkSmartPointer<vtkPoints>::New();
                    vtkSmartPointer<vtkIdTypeArray> cpNeiIndexMap = vtkSmartPointer<vtkIdTypeArray>::New();
                    for (vtkOBBNodePtr n : iter->first->faceNeighbors[k])
                    {
                        // find the neighbor's axis on which the current box lies - the current box and the box opposite
                        // will not be considered as neighbors (we want only the boxes that share either a face or an edge with current box)
                        // note that while ignoring these might ignore some boxes that ARE sharing and edge with the current box
                        // (when the neighbor's face is bigger than the current face, so it has multiple neighbors on that face)
                        // but those will be processed as part of a different neighbor anyway as they are directly neighboring the current box along a different axis

                        // add the neighbors content
                        if (processedBoxes.find(n->ID) == processedBoxes.end() 
                            && cpBoxLocators[n->ID]->GetDataSet()->GetNumberOfPoints() > 0)
                        {
                            vtkSmartPointer<vtkIdTypeArray> cpOrig = vtkIdTypeArray::SafeDownCast(
                                cpBoxLocators[n->ID]->GetDataSet()->GetPointData()->GetArray("CPorig"));
                            if (cpBoxLocators[n->ID]->GetDataSet()->GetNumberOfPoints() < N)
                            {
                                for (i = 0; i < cpBoxLocators[n->ID]->GetDataSet()->GetNumberOfPoints(); i++)
                                {
                                    iter->second[0]->InsertNextValue(cpOrig->GetValue(i));
#if PIPER_KRIG_BOXES_DEBUGVIS
                                    addedPoints->InsertNextPoint(referencePoints->GetPoint(cpOrig->GetValue(i)));
#endif
                                }
                            }
                            else
                            {
                                vtkSmartPointer<vtkIdList> addCP = vtkSmartPointer<vtkIdList>::New();
                                extractFaceNeighborhood(cpBoxLocators[n->ID], N, corners, addCP);

                                // write the results
                                for (i = 0; i < addCP->GetNumberOfIds(); i++)
                                {
                                    iter->second[0]->InsertNextValue(cpOrig->GetValue(addCP->GetId(i)));
#if PIPER_KRIG_BOXES_DEBUGVIS
                                    addedPoints->InsertNextPoint(referencePoints->GetPoint(cpOrig->GetValue(addCP->GetId(i))));
#endif
                                }
                            }
                            processedBoxes.insert(n->ID);
                        }
                        // add content of the neighbor's neighbor so that we also cover edge-sharing neighbors
                        int curAxis = 0;
                        bool notFound = true;
                        for (int nK = 0; nK < 6 && notFound; nK++)
                        {
                            for (vtkOBBNodePtr nN : n->faceNeighbors[nK])
                            {
                                if (nN == iter->first)
                                {
                                    curAxis = nK;
                                    notFound = false;
                                    break;
                                }
                            }
                        }
#if PIPER_KRIG_BOXES_DEBUGVIS
                        vtkSmartPointer<vtkOpenGLActor> actor = SelectionBox(n);
                        actor->GetProperty()->SetOpacity(0.2);
                        actor->GetProperty()->SetColor(1, 1, 0);
                        actor->GetProperty()->EdgeVisibilityOn();
                        actor->GetProperty()->SetRepresentationToWireframe();
                        renderer->AddActor(actor);
#endif
                        // axes are in pairs of opposite directions of the same axis, we will block curAxis and curAxis + 1
                        // => make sure curAxis points at the first of the pair
                        curAxis -= curAxis % 2;
                        for (int nK = 0; nK < 6; nK++)
                        {
                            if (nK != curAxis && nK != curAxis + 1) // process only the neighbors on the other 2 axes
                            {
                                for (vtkOBBNodePtr nN : n->faceNeighbors[nK])
                                {
                                    if (processedBoxes.find(nN->ID) == processedBoxes.end() && 
                                        cpBoxLocators[nN->ID]->GetDataSet()->GetNumberOfPoints() > 0)
                                    {
#if PIPER_KRIG_BOXES_DEBUGVIS
                                        vtkSmartPointer<vtkOpenGLActor> actor = SelectionBox(nN);
                                        actor->GetProperty()->SetOpacity(0.1);
                                        actor->GetProperty()->SetColor(0, 1, 0);
                                        actor->GetProperty()->EdgeVisibilityOn();
                                        actor->GetProperty()->SetRepresentationToWireframe();
                                        renderer->AddActor(actor);
#endif

                                        vtkSmartPointer<vtkIdTypeArray> cpOrig = vtkIdTypeArray::SafeDownCast(
                                            cpBoxLocators[nN->ID]->GetDataSet()->GetPointData()->GetArray("CPorig"));
                                        if (cpBoxLocators[nN->ID]->GetDataSet()->GetNumberOfPoints() < N)
                                        {
                                            for (i = 0; i < cpBoxLocators[nN->ID]->GetDataSet()->GetNumberOfPoints(); i++)
                                            {
                                                iter->second[0]->InsertNextValue(cpOrig->GetValue(i));
#if PIPER_KRIG_BOXES_DEBUGVIS
                                                addedPoints->InsertNextPoint(referencePoints->GetPoint(cpOrig->GetValue(i)));
#endif
                                            }
                                        }
                                        else
                                        {
                                            vtkSmartPointer<vtkIdList> addCP = vtkSmartPointer<vtkIdList>::New();
                                            extractFaceNeighborhood(cpBoxLocators[nN->ID], N, corners, addCP);  

                                            // write the results
                                            for (i = 0; i < addCP->GetNumberOfIds(); i++)
                                            {
                                                iter->second[0]->InsertNextValue(cpOrig->GetValue(addCP->GetId(i)));
#if PIPER_KRIG_BOXES_DEBUGVIS
                                                addedPoints->InsertNextPoint(referencePoints->GetPoint(cpOrig->GetValue(addCP->GetId(i))));
#endif
                                            }
                                        }
                                        processedBoxes.insert(nN->ID);
                                    }
                                }
                            }
                        }
                    }
                }
#if PIPER_KRIG_BOXES_DEBUGVIS

                vtkSmartPointer<vtkPolyData> addedPointsHolder = vtkSmartPointer<vtkPolyData>::New();
                addedPointsHolder->SetPoints(addedPoints);
                vtkSmartPointer<vtkOpenGLActor> addedActor = vtkSmartPointer<vtkOpenGLActor>::New();
                addedActor->SetMapper(AllPoints(addedPointsHolder, 1));
                addedActor->GetProperty()->SetColor(0, 1, 0);
                renderer->AddActor(addedActor);
                renderWindow->Render();

                vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
                renderWindowInteractor->SetInteractorStyle(style);
                renderWindowInteractor->Start();
#endif
                
                for (vtkIdType ctrlIter = 0; ctrlIter < iter->second[0]->GetNumberOfTuples(); ctrlIter++)
                    control_nodes->SetValue(iter->second[0]->GetValue(ctrlIter), IS_PRIMITIVE_SELECTED);
                for (vtkIdType ctrlIter = 0; ctrlIter < iter->second[1]->GetNumberOfTuples(); ctrlIter++)
                    sel_nodes->SetValue(iter->second[1]->GetValue(ctrlIter), IS_PRIMITIVE_SELECTED);
                
                // add points to deform from neigboring boxes - for each face neighbor, consider it + all it's neighbors to get a fully enclosed area around current box
       /*         std::vector<vtkOBBNodePtr> neighbors;
                for (i = 0; i < 6; i++) // for each face neighbor of this boxes
                {
                    vtkOBBNodePtr curNei = iter->first->faceNeighbors[i];
                    if (curNei)
                    {
                        neighbors.push_back(curNei);
                        // the axes of each box are ordered by length -> they do not have to be aligned with the neighbor
                        // -> there is (probably) no clever way to determine which boxes have already been added to the collections -> compare it - there will be at most 9*6 boxes, so it doesn't cost anything
                        for (int j = 0; j < 6; j++)
                        {
                            if (curNei->faceNeighbors[j])
                            {
                                bool isAlreadyIn = false;
                                for (vtkOBBNodePtr &o : neighbors)
                                {
                                    if (o == curNei)
                                    {
                                        isAlreadyIn = true;
                                        break;
                                    }
                                }
                                if (!isAlreadyIn)
                                    neighbors.push_back(curNei);
                            }
                        }
                    }
                }
                int counter = 0;
                for (vtkOBBNodePtr neighbor : neighbors)
                {
                    for (auto &boxSet : splittedBoxes) // find this neighbor in the collection that has it's points as well
                    {
                        if (neighbor == boxSet.first)
                        {
                            // mark points of that box as selected
                            for (vtkIdType ctrlIter = 0; ctrlIter < boxSet.second[1]->GetNumberOfTuples(); ctrlIter++)
                            {
                                if (sel_nodes->GetValue(boxSet.second[1]->GetValue(ctrlIter)) == IS_NOT_PRIMITIVE_SELECTED)
                                    counter++;
                                sel_nodes->SetValue(boxSet.second[1]->GetValue(ctrlIter), IS_PRIMITIVE_SELECTED);

                            }
                        }
                    }
                }
                std::cout << "added " << counter << " extra points to krige" << std::endl;
                double boxCentroid[3];
                counter = 0;
                for (auto &boxSet : splittedBoxes) // add one control point for each other box
                {
                    for (i = 0; i < 3; i++) // centroid = corner + 0.5 * direction to opposite corner
                        boxCentroid[i] = boxSet.first->Corner[i]
                            + boxSet.first->Axes[0][i] * 0.5 + boxSet.first->Axes[1][i] * 0.5 + boxSet.first->Axes[2][i] * 0.5;
                    // find a point close enough to the center - we don't need to go through all of them
                    // let's do / 8 -> if the point is in the inner 25% of the box (1/8 eps in both direction from the actual centroid -> 1/4 of volume)
                    double eps0 = vtkMath::Norm(boxSet.first->Axes[0]) / 2; 
                    double eps1 = vtkMath::Norm(boxSet.first->Axes[1]) / 2;
                    double eps2 = vtkMath::Norm(boxSet.first->Axes[2]) / 2;

                    for (i = 0; i < boxSet.second[0]->GetNumberOfTuples(); i++) // go through the CP candidates
                    {
                        referencePoints->GetPoint(boxSet.second[0]->GetValue(i), CP);
                        if ((CP[0] > boxCentroid[0] - eps0) && (CP[0] < boxCentroid[0] + eps0)
                            && (CP[1] > boxCentroid[1] - eps1) && (CP[1] < boxCentroid[1] + eps1)
                            && (CP[2] > boxCentroid[2] - eps2) && (CP[2] < boxCentroid[2] + eps2)
                            )
                        {
                            if (control_nodes->GetValue(boxSet.second[0]->GetValue(i)) == IS_NOT_PRIMITIVE_SELECTED)
                                counter++;
                            control_nodes->SetValue(boxSet.second[0]->GetValue(i), IS_PRIMITIVE_SELECTED);
                        }
                    }
                }
                std::cout << "added " << counter << " extra control points" << std::endl;*/

                // add boundary control nodes to each split bounding box (based on connectivity - will not work with e.g. domain decomposed models)
         /*       for (i = 0; i < sel_nodes->GetNumberOfTuples(); i++)
                {
                    if (sel_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED // if it is selected...
                        && control_nodes->GetValue(i) == IS_NOT_PRIMITIVE_SELECTED)// ...and is not a control node already, it is a candidate to be a control node
                    {
                        deformedMesh->GetPointCells(i, cellIds);
                        for (vtkIdType j = 0; j < cellIds->GetNumberOfIds(); j++) // for each cell connected to the candidate node
                        {
                            vtkCell *cell = deformedMesh->GetCell(cellIds->GetId(j));
                            for (vtkIdType k = 0; k < cell->GetNumberOfPoints(); k++)
                            {
                                if (cell->GetPointId(k) != i &&
                                    sel_nodes->GetValue(cell->GetPointId(k)) == IS_NOT_PRIMITIVE_SELECTED)
                                {
#if PIPER_KRIGING_PROFILING
                                    if (control_nodes->GetValue(i) == IS_NOT_PRIMITIVE_SELECTED)
                                        addedBoundaryCtrlNds++;
#endif
                                    control_nodes->SetValue(i, IS_PRIMITIVE_SELECTED);
                                    j = cellIds->GetNumberOfIds(); // break the outer loop
                                    break; // break this loop
                                }
                            }
                        }
                    }
                }
                if (decimateControlPoints)
                {
                    // decimate control points
                    for (i = 0; i < control_nodes->GetNumberOfTuples(); i++)
                    {
                        if (control_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED)
                        {
                            deformedMesh->GetPointCells(i, cellIds);
                            for (vtkIdType j = 0; j < cellIds->GetNumberOfIds(); j++) // for each cell connected to the candidate node
                            {
                                vtkCell *cell = deformedMesh->GetCell(cellIds->GetId(j));
                                for (vtkIdType k = 0; k < cell->GetNumberOfPoints(); k++) // for each node of the cell, set it as not control point
                                {
                                    if (cell->GetPointId(k) != i)
                                        control_nodes->SetValue(cell->GetPointId(k), IS_NOT_PRIMITIVE_SELECTED);
                                }
                            }
                        }
                    }
                }*/
                // de-select fixed nodes
                for (i = 0; i < sel_nodes->GetNumberOfTuples(); i++)
                {
                    if (fixedNodes[i])
                        sel_nodes->SetValue(i, IS_NOT_PRIMITIVE_SELECTED);
                }
#if PIPER_KRIGING_PROFILING
                end = clock();
                preprocTime += end - start;
                start = clock();
#endif
#if PIPER_KRIG_BOXES_DEBUGVIS
                // reset visualization
                renderer = vtkSmartPointer<vtkRenderer>::New();
                renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
                renderWindow->AddRenderer(renderer);
                renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
                renderWindowInteractor->SetRenderWindow(renderWindow);

                actor = SelectionBox(iter->first);
                actor->GetProperty()->SetOpacity(0.1);
                renderer->AddActor(actor);
                vtkSmartPointer<vtkPolyData> poi = vtkSmartPointer<vtkPolyData>::New();
                vtkSmartPointer<vtkPoints> p = vtkSmartPointer<vtkPoints>::New();
                vtkSmartPointer<vtkPolyData> poiT = vtkSmartPointer<vtkPolyData>::New();
                vtkSmartPointer<vtkPoints> pT = vtkSmartPointer<vtkPoints>::New();
                vtkSmartPointer<vtkPolyData> links = vtkSmartPointer<vtkPolyData>::New();
                links->Allocate();
                vtkSmartPointer<vtkPoints> linkPoints = vtkSmartPointer<vtkPoints>::New();
                links->SetPoints(linkPoints);
                vtkIdType pts[2];
                for (i = 0; i < control_nodes->GetNumberOfTuples(); i++)
                {
                    if (control_nodes->GetValue(i) == IS_PRIMITIVE_SELECTED)
                    {
                        p->InsertNextPoint(referencePoints->GetPoint(i));
                        pT->InsertNextPoint(deformedMesh->GetPoint(i));
                        pts[0] = linkPoints->InsertNextPoint(referencePoints->GetPoint(i));
                        pts[1] = linkPoints->InsertNextPoint(deformedMesh->GetPoint(i));
                        links->InsertNextCell(VTK_LINE, 2, pts);
                    }
                }
                poi->SetPoints(p);
                poiT->SetPoints(pT);
                auto pointmapper = AllPoints(poi, 1);
                auto pointmapperT = AllPoints(poiT, 0.5);

                vtkSmartPointer<vtkOpenGLActor> pointactor = vtkSmartPointer<vtkOpenGLActor>::New();
                pointactor->SetMapper(pointmapper);
                pointactor->GetProperty()->SetDiffuseColor(actor->GetProperty()->GetDiffuseColor());
                pointactor->GetProperty()->SetOpacity(1);
          //      renderer->AddActor(pointactor);

                vtkSmartPointer<vtkOpenGLActor> pointactorT = vtkSmartPointer<vtkOpenGLActor>::New();
                pointactorT->SetMapper(pointmapperT);
                pointactorT->GetProperty()->SetDiffuseColor(actor->GetProperty()->GetDiffuseColor());
                pointactorT->GetProperty()->SetOpacity(1);
        //        renderer->AddActor(pointactorT);

                vtkSmartPointer<vtkOpenGLActor> linksactor = vtkSmartPointer<vtkOpenGLActor>::New();
                vtkSmartPointer<vtkPolyDataMapper> linksmaper = vtkSmartPointer<vtkPolyDataMapper>::New();
                linksmaper->SetInputData(links);
                linksactor->SetMapper(linksmaper);
                linksactor->GetProperty()->SetDiffuseColor(actor->GetProperty()->GetDiffuseColor());
                
      //          renderer->AddActor(linksactor);
#endif
                // now perform the kriging
                if (applyDeformationSelectedOnly(referencePoints, deformedMesh->GetPoints(), result,
                    sel_nodes, control_nodes, ctrlPtWeight, interpolateDisplacement, useDrift))
                {
                    // if it was performed (there were enough points/control nodes), increment the times the selected points were modified
                    for (vtkIdType ctrlIter = 0; ctrlIter < sel_nodes->GetNumberOfTuples(); ctrlIter++)
                    {
                        if (sel_nodes->GetValue(ctrlIter) == IS_PRIMITIVE_SELECTED)
                        {
                            timesModified->SetValue(ctrlIter, timesModified->GetValue(ctrlIter) + 1);
                        }
                    }
                }
#if PIPER_KRIGING_PROFILING
                end = clock();
                krigingTime += end - start;
                start = clock();
#endif
            }
#if PIPER_KRIGING_PROFILING
            std::cout << "Preprocessing: " << (preprocTime / (double)CLOCKS_PER_SEC) << "s" << std::endl
                << "Kriging matrix allocation: " << (krigMatrixPreprocessTime / (double)CLOCKS_PER_SEC) << "s" << std::endl
                << "Kriging matrix computation: " << (krigMatrixTime / (double)CLOCKS_PER_SEC) << "s" << std::endl
                << "Kriging deformation: " << (krigDeformTime / (double)CLOCKS_PER_SEC) << "s" << std::endl
                << "Kriging total: " << (krigingTime / (double)CLOCKS_PER_SEC) << "s" << std::endl;
#endif
        }

        // write the cummulative results
        for (vtkIdType i = 0; i < deformedMesh->GetNumberOfPoints(); i++)
        {
            int tMod = timesModified->GetValue(i);
            if (tMod > 0) // in case of non-zero timesModified, make an average of the new values, otherwise keep the original ones
            {
                double *point = result->GetPoint(i);
                deformedMesh->GetPoints()->SetPoint(i, point[0] / tMod, point[1] / tMod, point[2] / tMod);
            }
        }
#if PIPER_KRIG_BOXES_DEBUGVIS
        // blank half of the mesh
        vtkSmartPointer<vtkSelectionTools> blanker = vtkSmartPointer<vtkSelectionTools>::New();
        blanker->SetBlankSelected(true, true);
        blanker->SetSelectionTargetType(SELECTION_TARGET::ELEMENTS);
        double normal[3] = { 0, 1, 0 };
        blanker->UseSelectByPlane(rootMidPoint, normal);
        int prefixPoints = deformedMesh->GetNumberOfPoints() - refMesh->GetNumberOfPoints();
        for (vtkIdType i = 0; i < refMesh->GetNumberOfPoints(); i++)
        {
            refMesh->GetPoints()->SetPoint(i, deformedMesh->GetPoint(i + prefixPoints));
        }
        blanker->SetInputData(refMesh);
        blanker->Update();
        vtkSmartPointer<vtkDataSetSurfaceFilter> extractor = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
        extractor->SetPassThroughCellIds(true);
        extractor->SetPassThroughPointIds(true);
        extractor->UseStripsOff();
        extractor->SetInputDataObject(blanker->GetUnstructuredGridOutput());
        vtkSmartPointer<vtkPolyDataMapper>mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputConnection(extractor->GetOutputPort());
        vtkSmartPointer<vtkOpenGLActor> actor = vtkSmartPointer<vtkOpenGLActor>::New();
        actor->SetMapper(mapper);
        actor->GetProperty()->EdgeVisibilityOn();
        //actor->GetProperty()->SetOpacity(0.1);
        renderer->AddActor(actor);
        renderWindow->Render();

        vtkSmartPointer<vtkInteractorStyleTrackballCamera> style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

        renderWindowInteractor->SetInteractorStyle(style);

        // Begin mouse interaction
        renderWindowInteractor->Start();
#endif

    }

    bool KrigingPiperInterface::applyDeformationSelectedOnly(vtkSmartPointer<vtkPoints> originalMeshPoints,
        vtkSmartPointer<vtkPoints> targetMeshPoints, vtkSmartPointer<vtkPoints> resultCummulative, vtkSmartPointer<vtkBitArray> selectedIDs,
        vtkSmartPointer<vtkBitArray> controlIDs, boost::container::vector<double> ctrlPtWeight, bool interpolateDisplacement, bool useDrift)
    {
        std::vector<Coord> srcControlPoints;
        std::vector<Coord> targetControlPoints;
        std::vector<double> ctrlPtWeightFiltered; // a vector for keeping only weights relevant for the current control points
        VCoord nodes;
        // prepare data
        for (vtkIdType i = 0; i < selectedIDs->GetNumberOfTuples(); i++)
        {
            // if it is selected for kriging, add it to the srcNodes
            // if it is a control point, it will be added as well as a regular point and will be kriged - if nugget is used, it might get to other position than its current one
            if (selectedIDs->GetValue(i) == IS_PRIMITIVE_SELECTED)
            {
                double *point = originalMeshPoints->GetPoint(i);
                nodes.push_back(Coord(point[0], point[1], point[2]));
            }
            if (controlIDs->GetValue(i) == IS_PRIMITIVE_SELECTED) // if it is a control point, create coordinates for target and source
            {
                double *point = originalMeshPoints->GetPoint(i);
                srcControlPoints.push_back(Coord(point[0], point[1], point[2]));

                point = targetMeshPoints->GetPoint(i);
                targetControlPoints.push_back(Coord(point[0], point[1], point[2]));

                ctrlPtWeightFiltered.push_back(ctrlPtWeight[i]);
            }
        }

        if (srcControlPoints.size() < 5 || nodes.size() < 60)
            return false; // no point in processing if there is no usable data
#if PIPER_KRIGING_PROFILING

        std::cout << "number of control nodes: " << targetControlPoints.size()
            << " number of nodes: " << nodes.size() << std::endl;
        time_t start = clock();
#endif
        setControlPoints(srcControlPoints, targetControlPoints, ctrlPtWeightFiltered, interpolateDisplacement, useDrift);

#if PIPER_KRIGING_PROFILING
        time_t end = clock();
        krigMatrixPreprocessTime += end - start;
        start = clock();
#endif
        compute(true);

#if PIPER_KRIGING_PROFILING
        end = clock();
        krigMatrixTime += end - start;
        start = clock();
#endif
        applyDeformation(nodes, &nodes);
#if PIPER_KRIGING_PROFILING
        end = clock();
        krigDeformTime += end - start;
#endif
        // write output nodes
        VCoord::iterator c = nodes.begin();
        for (vtkIdType i = 0; i < selectedIDs->GetNumberOfTuples(); i++)
        {
            if (selectedIDs->GetValue(i) == IS_PRIMITIVE_SELECTED)
            {
                double *origPoint = resultCummulative->GetPoint(i);
                resultCummulative->SetPoint(i, (*c)(0) + origPoint[0], (*c)(1) + origPoint[1], (*c)(2) + origPoint[2]);
                c++;
            }
        }
        return true;
    }

    void KrigingPiperInterface::applyDeformation(Nodes& srcNodes, Nodes* outputNodes)
    {
        if (srcNodes.size() == 0 || krigingDeformer == nullptr)
            throw std::runtime_error("srcNodes matrix is empty or compute was not performed");

        try{
            srcMesh->resize(srcNodes.size(), 3);
            destMesh->resize(srcNodes.size(), 3);
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Failed to allocate memory for soure and target mesh nodes.");
            throw std::runtime_error(msg.c_str());
        }

#ifdef _OPENMP //parallel loop over std::map since VStudio 2012/2013 does not handle OPENMP3.0 and TASK construct 
        int max_threads = omp_get_max_threads();
        std::vector<std::tuple<Nodes::const_iterator, Nodes::const_iterator, int> > chunks;
        chunks.reserve(max_threads);
        int chunk_size = (int)(srcNodes.size() / max_threads); // chunk size cant be over the size of int anyway
        auto cur_iter = srcNodes.begin();
        for (int i = 0; i < max_threads - 1; ++i)
        {
            auto last_iter = cur_iter;
            std::advance(cur_iter, chunk_size);
            chunks.push_back(std::make_tuple(last_iter, cur_iter, i*chunk_size));
        }
        chunks.push_back(std::make_tuple(cur_iter, srcNodes.end(), (max_threads - 1)*chunk_size));

#pragma omp parallel shared(chunks)
        {
#pragma omp for
            for (int i = 0; i < max_threads; ++i){
                int j = std::get<2>(chunks[i]);
                for (auto it = std::get<0>(chunks[i]); it != std::get<1>(chunks[i]); ++it, ++j){
                    (*srcMesh)(j, 0) = (*it)->getCoordX();
                    (*srcMesh)(j, 1) = (*it)->getCoordY();
                    (*srcMesh)(j, 2) = (*it)->getCoordZ();
                }
            }
        }
#else
        int i=0;
        for(Nodes::const_iterator it = srcNodes.begin() ; it!= srcNodes.end() ; ++it,++i){
            (*srcMesh)(i,0) = (*it)->getCoordX();
            (*srcMesh)(i,1) = (*it)->getCoordY();
            (*srcMesh)(i,2) = (*it)->getCoordZ();
        }
#endif

        if (!krigingDeformer->ApplyDeformationDense(srcMesh, destMesh))
            throw std::runtime_error("One or more matrix is empty (srcCtrlPt, destCtrlPt, srcMesh) or compute was not performed");


#ifdef _OPENMP //parallel loop over std::map since VStudio 2012/2013 does not handle OPENMP3.0 and TASK construct 
        chunks.clear();
        chunks.reserve(max_threads);
        chunk_size = (int)(outputNodes->size() / max_threads);
        cur_iter = outputNodes->begin();
        for (int i = 0; i < max_threads - 1; ++i)
        {
            auto last_iter = cur_iter;
            std::advance(cur_iter, chunk_size);
            chunks.push_back(std::make_tuple(last_iter, cur_iter, i*chunk_size));
        }
        chunks.push_back(std::make_tuple(cur_iter, outputNodes->end(), (max_threads - 1)*chunk_size));

#pragma omp parallel shared(chunks)
        {
#pragma omp for
            for (int i = 0; i < max_threads; ++i){
                int j = std::get<2>(chunks[i]);
                for (auto it = std::get<0>(chunks[i]); it != std::get<1>(chunks[i]); ++it, ++j)
                    (*it)->setCoord((*destMesh)(j, 0), (*destMesh)(j, 1), (*destMesh)(j, 2));
            }
        }
#else
        i=0;
        for(Nodes::const_iterator it = outputNodes->begin() ; it!= outputNodes->end() ; ++it,++i){
            (*it)->setCoord((*destMesh)(i,0),(*destMesh)(i,1),(*destMesh)(i,2)) ;
        }
#endif

    }

    void KrigingPiperInterface::applyDeformation(hbm::VCoord& srcNodes, hbm::VCoord* outputNodes)
    {
        if (srcNodes.size() == 0 || krigingDeformer == nullptr)
            throw std::runtime_error("srcNodes matrix is empty or compute was not performed");

        try{
            srcMesh->resize(srcNodes.size(), 3);
            destMesh->resize(srcNodes.size(), 3);
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Failed to allocate memory for soure and target mesh nodes.");
            throw std::runtime_error(msg.c_str());
        }

#ifdef _OPENMP //parallel loop over std::map since VStudio 2012/2013 does not handle OPENMP3.0 and TASK construct 
        int max_threads = omp_get_max_threads();
        std::vector<std::tuple<hbm::VCoord::iterator, hbm::VCoord::iterator, int> > chunks;
        chunks.reserve(max_threads);
        int chunk_size = (int)(srcNodes.size() / max_threads); // chunk size cant be over the size of int anyway
        auto cur_iter = srcNodes.begin();
        for (int i = 0; i < max_threads - 1; ++i)
        {
            auto last_iter = cur_iter;
            std::advance(cur_iter, chunk_size);
            chunks.push_back(std::make_tuple(last_iter, cur_iter, i*chunk_size));
        }
        chunks.push_back(std::make_tuple(cur_iter, srcNodes.end(), (max_threads - 1)*chunk_size));

#pragma omp parallel shared(chunks)
        {
#pragma omp for
            for (int i = 0; i < max_threads; ++i){
                int j = std::get<2>(chunks[i]);
                for (auto it = std::get<0>(chunks[i]); it != std::get<1>(chunks[i]); ++it, ++j){
                    (*srcMesh)(j, 0) = (*it)(0);
                    (*srcMesh)(j, 1) = (*it)(1);
                    (*srcMesh)(j, 2) = (*it)(2);
                }
            }
        }
#else
        int i = 0;
        for (hbm::VCoord::iterator it = srcNodes.begin(); it != srcNodes.end(); ++it, ++i){
            (*srcMesh)(i, 0) = (*it)(0);
            (*srcMesh)(i, 1) = (*it)(1);
            (*srcMesh)(i, 2) = (*it)(2);
        }
#endif

        if (!krigingDeformer->ApplyDeformationDense(srcMesh, destMesh))
            throw std::runtime_error("One or more matrix is empty (srcCtrlPt, destCtrlPt, srcMesh) or compute was not performed");


#ifdef _OPENMP //parallel loop over std::map since VStudio 2012/2013 does not handle OPENMP3.0 and TASK construct 
        chunks.clear();
        chunks.reserve(max_threads);
        chunk_size = (int)(outputNodes->size() / max_threads);
        cur_iter = outputNodes->begin();
        for (int i = 0; i < max_threads - 1; ++i)
        {
            auto last_iter = cur_iter;
            std::advance(cur_iter, chunk_size);
            chunks.push_back(std::make_tuple(last_iter, cur_iter, i*chunk_size));
        }
        chunks.push_back(std::make_tuple(cur_iter, outputNodes->end(), (max_threads - 1)*chunk_size));

#pragma omp parallel shared(chunks)
        {
#pragma omp for
            for (int i = 0; i < max_threads; ++i){
                int j = std::get<2>(chunks[i]);
                for (auto it = std::get<0>(chunks[i]); it != std::get<1>(chunks[i]); ++it, ++j)
                {
                    (*it)(0) = (*destMesh)(j, 0);
                    (*it)(1) = (*destMesh)(j, 1);
                    (*it)(2) = (*destMesh)(j, 2);
                }
            }
        }
#else
        i = 0;
        for (hbm::VCoord::iterator it = outputNodes->begin(); it != outputNodes->end(); ++it, ++i){
            (*it)(0) = (*destMesh)(i, 0);
            (*it)(1) = (*destMesh)(i, 1);
            (*it)(2) = (*destMesh)(i, 2);
        }
#endif

    }

    double KrigingPiperInterface::getReprojectionRelativeError(){
        if (srcCtrlPt->size() != destCtrlPt->size() || srcCtrlPt->size() == 0 || krigingDeformer == nullptr)
            throw std::runtime_error("One or more matrix is empty (srcCtrlPt, destCtrlPt) or compute was not performed");

        std::shared_ptr<Eigen::MatrixXd> transformedSrcCtrlPt(std::make_shared<Eigen::MatrixXd>());
        try{
            transformedSrcCtrlPt->resize(srcCtrlPt->size() / 3, 3);
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Failed to allocate memory for transformed source control points.");
            throw std::runtime_error(msg.c_str());
        }

        krigingDeformer->ApplyDeformationDense(srcCtrlPt, transformedSrcCtrlPt);
        double relativeError = (*transformedSrcCtrlPt - *destCtrlPt).norm() / destCtrlPt->norm();
        double maxRelativeError = ((*transformedSrcCtrlPt - *destCtrlPt).rowwise().norm()).cwiseQuotient(destCtrlPt->rowwise().norm()).maxCoeff();
        return std::max(relativeError, maxRelativeError);
    }


    double KrigingPiperInterface::getRelativeError(Nodes& nodesToCompareWith){
        if (destMesh->size() == 0 || nodesToCompareWith.size() != destMesh->size() / 3)
            throw std::runtime_error("applyDeformation was not performed or meshes size do not match");
        Eigen::MatrixXd tmpMeshToCompareWith;
        try{
            tmpMeshToCompareWith.resize(destMesh->size() / 3, 3);
        }
        catch (std::bad_alloc e)
        {
            std::string msg("Failed to allocate memory for tmpMeshToCompareWith.");
            throw std::runtime_error(msg.c_str());
        }

#ifdef _OPENMP //parallel loop over std::map since VStudio 2012/2013 does not handle OPENMP3.0 and TASK construct 
        int max_threads = omp_get_max_threads();
        std::vector<std::tuple<Nodes::const_iterator, Nodes::const_iterator, int> > chunks;
        chunks.reserve(max_threads);
        int chunk_size = (int)(nodesToCompareWith.size() / max_threads); // chunk size cant be over the size of int anyway
        auto cur_iter = nodesToCompareWith.begin();
        for (int i = 0; i < max_threads - 1; ++i)
        {
            auto last_iter = cur_iter;
            std::advance(cur_iter, chunk_size);
            chunks.push_back(std::make_tuple(last_iter, cur_iter, i*chunk_size));
        }
        chunks.push_back(std::make_tuple(cur_iter, nodesToCompareWith.end(), (max_threads - 1)*chunk_size));

#pragma omp parallel shared(chunks)
        {
#pragma omp for
            for (int i = 0; i < max_threads; ++i){
                int j = std::get<2>(chunks[i]);
                for (auto it = std::get<0>(chunks[i]); it != std::get<1>(chunks[i]); ++it, ++j){
                    (tmpMeshToCompareWith)(j, 0) = (*it)->getCoordX();
                    (tmpMeshToCompareWith)(j, 1) = (*it)->getCoordY();
                    (tmpMeshToCompareWith)(j, 2) = (*it)->getCoordZ();
                }
            }
        }
#else
        int i=0;
        for(Nodes::const_iterator it = nodesToCompareWith.begin() ; it!= nodesToCompareWith.end() ; ++it,++i){
            (tmpMeshToCompareWith)(i,0) = (*it)->getCoordX();
            (tmpMeshToCompareWith)(i,1) = (*it)->getCoordY();
            (tmpMeshToCompareWith)(i,2) = (*it)->getCoordZ();
        }
#endif

        double relativeError = (*destMesh - tmpMeshToCompareWith).norm() / tmpMeshToCompareWith.norm();
        MatrixXf::Index maxRow, maxCol;
        double maxRelativeError = ((*destMesh - tmpMeshToCompareWith).rowwise().norm()).cwiseQuotient(tmpMeshToCompareWith.rowwise().norm()).maxCoeff(&maxRow, &maxCol);
        return std::max(relativeError, maxRelativeError);
    }
}

