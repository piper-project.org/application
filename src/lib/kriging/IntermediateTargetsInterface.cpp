/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#include "IntermediateTargetsInterface.h"

#include "anatomyDB/query.h"

#include <vtkPointData.h>
#include <vtkKdTreePointLocator.h>
#include <vtkMath.h>
#include <vtkSelectEnclosedPoints.h>

#define PIPER_DOMAIN_DECOMP_DEBUGVIS 0 // visualization of how boxes are split and what CPs are inside, DO NOT push code with this enabled to master, only for debugging
#if PIPER_DOMAIN_DECOMP_DEBUGVIS
#define PIPER_DOMAIN_DECOMP_DEBUGVISperdomain 0
#define PIPER_DOMAIN_DECOMP_DEBUGVISdomainOverview 1
#define PIPER_DOMAIN_DECOMP_DEBUGVISallPointsInside 0
#define PIPER_DOMAIN_DECOMP_DEBUGVIStargetMeshes 0
#include <vtkOpenGLActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkGlyph3DMapper.h>
#include <vtkSphereSource.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkDataSetSurfaceFilter.h>
#endif

using namespace piper::hbm;
using namespace Eigen;

namespace piper
{

    IntermediateTargetsInterface::IntermediateTargetsInterface(HumanBodyModel* hbmref) : m_hbm(hbmref)
    {

    }

    IntermediateTargetsInterface::~IntermediateTargetsInterface()
    {

    }

    void IntermediateTargetsInterface::setRefHBM(piper::hbm::HumanBodyModel* hbmref)
    {
        m_hbm = hbmref;
        inverseNid.clear();
        m_domains.clear();
        sourcesReset();
    }

    void IntermediateTargetsInterface::setMessageFunctions(std::function<void(std::string const&)> infoMessage, std::function<void(std::string const&)> warningMessage)
    {
        m_infoLog = infoMessage;
        m_warningLog = warningMessage;
    }

    void IntermediateTargetsInterface::resetKrigingInterface()
    {
        m_skinTransValid = false;
        m_bonesTransValid = false;
        m_decimationValid = false;
        m_nuggetsValid = false;
        if (!m_lockSkinTarget)
            m_skinTransformed.clear();
        if (!m_lockBonesTarget)
            m_bonesTransformed.clear();
        m_cpSkinTarget.clear();
        m_cpBonesTarget.clear();
    }

    void IntermediateTargetsInterface::sourcesReset()
    {
        m_skinAndBonesPointsSrc = NULL;
        m_sourceValidEuclidSkin = false;
        m_sourceValidEuclidBones = false;
        m_sourceValidGeodesic = false;
        m_bonesNugget.clear();
        m_skinNugget.clear();
        m_cpBonesSource.clear();
        m_cpSkinSource.clear();
        m_cpBonesTarget.clear();
        m_cpSkinTarget.clear();
        resetKrigingInterface();
    }

    bool IntermediateTargetsInterface::getSourcesAreLoaded()
    {
        return !(m_cpBonesSource.empty() && m_cpSkinSource.empty());
    }

    void IntermediateTargetsInterface::setControlPoints(InteractionControlPoint &cpsSource, InteractionControlPoint &cpsTarget)
    {
        sourcesReset();
        if (cpsSource.getNbControlPt() == cpsTarget.getNbControlPt())
        {
            VCoord coordsSource, coordsTarget;
            cpsSource.getCoordControlPoint(m_hbm->fem(), coordsSource);
            cpsTarget.getCoordControlPoint(m_hbm->fem(), coordsTarget);
            std::vector<double> &as_bones = cpsSource.getAssociationBones();
            std::vector<double> &as_skin = cpsSource.getAssociationSkin();
            std::vector<double> &nuggets = cpsSource.getWeight();
            double globalNugget = cpsSource.getGlobalWeight();
            for (size_t i = 0; i < coordsSource.size(); i++)
            {
                if (as_bones[i] >= m_UseBonesThreshold)
                {
                    m_cpBonesSource.push_back(coordsSource[i]);
                    m_cpBonesTarget.push_back(coordsTarget[i]);

                    if (nuggets.size() == coordsSource.size())
                        m_bonesNugget.push_back(nuggets[i]);
                    else if (!std::isnan(globalNugget))
                        m_bonesNugget.push_back(globalNugget);
                    else
                        m_bonesNugget.push_back(0);

                }
                if (as_skin[i] >= m_UseSkinThreshold)
                {
                    m_cpSkinSource.push_back(coordsSource[i]);
                    m_cpSkinTarget.push_back(coordsTarget[i]);

                    if (nuggets.size() == coordsSource.size())
                        m_skinNugget.push_back(nuggets[i]);
                    else if (!std::isnan(globalNugget))
                        m_skinNugget.push_back(globalNugget);
                    else
                        m_skinNugget.push_back(0);
                }
            }
        }
    }

    void IntermediateTargetsInterface::setTargetControlPoints(InteractionControlPoint &cpsTarget)
    {
        resetKrigingInterface();
        std::vector<double> &as_bones = cpsTarget.getAssociationBones();
        std::vector<double> &as_skin = cpsTarget.getAssociationSkin();
        VCoord coordsTarget;
        cpsTarget.getCoordControlPoint(m_hbm->fem(), coordsTarget);
        for (size_t i = 0; i < coordsTarget.size(); i++)
        {
            if (as_bones[i] >= m_UseBonesThreshold)
                m_cpBonesTarget.push_back(coordsTarget[i]);

            if (as_skin[i] >= m_UseSkinThreshold)
                m_cpSkinTarget.push_back(coordsTarget[i]);
        }
    }

    bool IntermediateTargetsInterface::setUseGeodesicSkin(bool usegeo)
    {
        if (usegeo != m_useGeodesicSkin)
        {
            m_useGeodesicSkin = usegeo;
            m_skinTransValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setGeodesicDistanceType(SurfaceDistance geoDistType)
    {
        if (m_geodesicDistanceType != geoDistType)
        {
            m_geodesicDistanceType = geoDistType;
            if (m_useGeodesicSkin)
                m_skinTransValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setDecimationUseDisplacement(bool value)
    {
        if (m_deciUseDisplacement != value)
        {
            m_deciUseDisplacement = value;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::getDecimationUseDisplacement()
    {
        return m_deciUseDisplacement;
    }

    bool IntermediateTargetsInterface::setDecimationUseHomogenous(bool value)
    {
        if (m_deciUseHomogenous != value)
        {
            m_deciUseHomogenous = value;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::getDecimationUseHomogenous()
    {
        return m_deciUseHomogenous;
    }

    bool IntermediateTargetsInterface::setDecimationGridSize(unsigned int sizeX)
    {
        if (sizeX != m_deciGridSizeX)
        {
            m_deciGridSizeX = sizeX;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    unsigned int IntermediateTargetsInterface::getDecimationGridSize()
    {
        return m_deciGridSizeX;
    }

    double IntermediateTargetsInterface::getDecimationRadius()
    {
        return m_deciRadius;
    }

    double IntermediateTargetsInterface::getDecimationDeviation()
    {
        return m_deciDeviation;
    }

    bool IntermediateTargetsInterface::setDecimationRadius(double radius)
    {
        if (radius != m_deciRadius)
        {
            m_deciRadius = radius;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }


    bool IntermediateTargetsInterface::setDecimationDeviation(double deviation)
    {
        if (deviation != m_deciDeviation)
        {
            m_deciDeviation = deviation;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }
    
    double IntermediateTargetsInterface::getNuggetBoneWeight()
    {
        return m_nuggetBoneWeight;
    }

    double IntermediateTargetsInterface::getNuggetRadius()
    {
        return m_nuggetRadius;
    }

    bool IntermediateTargetsInterface::setNuggetRadius(double radius)
    {
        if (radius != m_nuggetRadius)
        {
            m_nuggetRadius = radius;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setNuggetBoneWeight(double weight)
    {
        if (weight >= 0 && weight <= 1)
        {
            m_nuggetBoneWeight = weight;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setNuggetScaleSkin(double scale)
    {
        if (m_nuggetScaleSkin != scale)
        {
            m_nuggetScaleSkin = scale;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    double IntermediateTargetsInterface::getNuggetScaleSkin()
    {
        return m_nuggetScaleSkin;
    }

    bool IntermediateTargetsInterface::setNuggetScaleBones(double scale)
    {
        if (m_nuggetScaleBones != scale)
        {
            m_nuggetScaleBones = scale;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    double IntermediateTargetsInterface::getNuggetScaleBones()
    {
        return m_nuggetScaleBones;
    }

    void IntermediateTargetsInterface::setUseDefaultNugget(bool useDefault)
    {
        m_useDefaultNugget = useDefault;
    }

    bool IntermediateTargetsInterface::getUseDefaultNugget()
    {
        return m_useDefaultNugget;
    }

    bool IntermediateTargetsInterface::setInterpolateDisplacement(bool displacement)
    {
        if (displacement != m_interpolateDisplacement)
        {
            m_interpolateDisplacement = displacement;
            if (!m_useGeodesicSkin) // the displacement setting does not affect geodesic distance computation
                m_skinTransValid = false;
            m_bonesTransValid = false;
            m_sourceValidEuclidSkin = false;
            m_sourceValidEuclidBones = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setUseDrift(bool useDrift)
    {
        if (useDrift != m_useDrift)
        {
            m_useDrift = useDrift;
            if (!m_useGeodesicSkin) // the drift setting does not affect geodesic distance computation
                m_skinTransValid = false;
            m_bonesTransValid = false;
            m_sourceValidEuclidSkin = false;
            m_sourceValidEuclidBones = false;
            return true;
        }
        return false;
    }

    void IntermediateTargetsInterface::setSplitBoxOverlap(double overlap)
    {
        m_splitBoxOverlap = overlap;
    }

    bool IntermediateTargetsInterface::setGeoDistancePrecision(double precision)
    {
        if (precision != m_geodesicPrecision)
        {
            m_geodesicPrecision = precision;
            m_krigingGeodesic.SetGeodesicPrecision(precision);
            if (m_useGeodesicSkin)
                m_skinTransValid = false;
            return true;
        }
        return false;
    }

    double IntermediateTargetsInterface::getGeoDistancePrecision()
    {
        return m_geodesicPrecision;
    }

    double IntermediateTargetsInterface::getUseSkinThreshold()
    {
        return m_UseSkinThreshold;
    }

    double IntermediateTargetsInterface::getUseBonesThreshold()
    {
        return m_UseBonesThreshold;
    }

    bool IntermediateTargetsInterface::setUseBonesThreshold(double threshold)
    {
        if (m_UseBonesThreshold != threshold)
        {
            m_UseBonesThreshold = threshold;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setUseSkinThreshold(double threshold)
    {
        if (m_UseSkinThreshold != threshold)
        {
            m_UseSkinThreshold = threshold;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setUseIntermediateDomains(bool flag)
    {
        if (m_intermUseDomains != flag)
        {
            m_intermUseDomains = flag;
            m_nuggetsValid = false;
            m_decimationValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::setFixBones(bool fix)
    {
        if (fix != m_fixBones)
        {
            m_fixBones = fix;
            if (fix) // if bones are to be fixed, they must be used as intermediate targets, otherwise they would end up not being fixed
                m_intermUseBones = true;
            m_bonesTransValid = false;
            m_decimationValid = false;
            m_nuggetsValid = false;
            return true;
        }
        return false;
    }

    bool IntermediateTargetsInterface::getFixBones()
    {
        return m_fixBones;
    }

    void IntermediateTargetsInterface::scaleSkin()
    {
        if (!m_skinTransValid && m_cpSkinSource.size() > 0)
        {
            vtkSmartPointer<vtkPolyData> skinMesh = m_hbm->fem().getFEModelVTK()->getSkin(false, m_hbm->metadata().entities(), m_hbm->fem());
            initDeformation(true, m_useGeodesicSkin ? skinMesh : NULL);
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(skinMesh->GetPointData()->GetArray("nid"));
            if (m_skinAndBonesPointsSrc == NULL) // if its not null, m_skinSource must have already been created -> don't do it again
            {
                m_skinSource.clear();
                for (vtkIdType i = 0; i < nid->GetNumberOfTuples(); i++)
                {
                    NodePtr n = std::make_shared<Node>(); // make a copy of the node so that we can re-index it
                    NodePtr nOrig = m_hbm->fem().getNodes().getEntity(nid->GetValue(i));
                    n->setCoord(nOrig->getCoordX(), nOrig->getCoordY(), nOrig->getCoordZ());
                    n->setId(i); // set the index == vtk index so that the it is ensured that the order of the nodes is the same in the mesh as in the m_source array
                    // we could get the nids directly from HBM, but we need to construct the VTK representation of skin anyway, so it doesn't matter which one we use
                    m_skinSource.setEntity(n);
                }
            }
            if (!m_lockSkinTarget || m_skinTransformed.size() != m_skinSource.size())
            {
                m_skinTransformed.clear();
                m_skinTransformed.makeCopy(m_skinSource);
                m_infoLog("Scaling skin...");
                if (m_useGeodesicSkin)
                    m_krigingGeodesic.applyDeformation(m_skinSource, &m_skinTransformed);
                else
                    m_krigingEuclidSkin.applyDeformation(m_skinSource, &m_skinTransformed);

                m_skinAndBonesTargetPoints = NULL;
            }
            else
                m_infoLog("Re-using locked skin target");
            m_skinTransValid = true;
        }
    }

    vtkSmartPointer<vtkPolyData> IntermediateTargetsInterface::getScaledSkin()
    {
        vtkSmartPointer<vtkPolyData> skinMesh = m_hbm->fem().getFEModelVTK()->getSkin(false, m_hbm->metadata().entities(), m_hbm->fem());
        vtkSmartPointer<vtkPolyData> skinMeshScaled = vtkSmartPointer<vtkPolyData>::New();
        skinMeshScaled->DeepCopy(skinMesh);
        for (NodePtr n : m_skinTransformed)
            skinMeshScaled->GetPoints()->SetPoint(n->getId(), n->getCoordX(), n->getCoordY(), n->getCoordZ());
        return skinMeshScaled;
    }

    void IntermediateTargetsInterface::createNuggetMapSkin(vtkSmartPointer<vtkPolyData> skinMesh)
    {
        if ((vtkIdType)smartNuggets.size() >= skinMesh->GetNumberOfPoints() && m_intermUseSkin)
        {
            vtkSmartPointer<vtkDoubleArray> nuggetMap = vtkSmartPointer<vtkDoubleArray>::New();
            nuggetMap->SetNumberOfTuples(skinMesh->GetNumberOfPoints());
            nuggetMap->SetNumberOfComponents(1);
            nuggetMap->SetName(_PIPER_NUGGET_MAP);
            nuggetMap->FillComponent(0, 1);
            skinMesh->GetPointData()->AddArray(nuggetMap);
            size_t i = 0;
            vtkIdType boneOffset = m_intermUseBones ? m_bonesTransformed.size() : 0;
            for (NodePtr n : m_skinTransformed)
            {
                if (!mapRejected[i + boneOffset])
                    nuggetMap->SetValue(n->getId(), smartNuggets[i + boneOffset]); // skin nuggets are after bone nuggets -> offset by number of bone control points
                i++;
            }
        }
    }

    void IntermediateTargetsInterface::createDecimationMapSkin(vtkSmartPointer<vtkPolyData> skinMesh)
    {
        if (m_intermUseSkin && m_decimationValid)
        {
            vtkSmartPointer<vtkBitArray> deciMap = vtkSmartPointer<vtkBitArray>::New();
            deciMap->SetNumberOfTuples(skinMesh->GetNumberOfPoints());
            deciMap->SetNumberOfComponents(1);
            deciMap->SetName(_PIPER_DECIMATION_MAP);
            deciMap->FillComponent(0, IS_PRIMITIVE_SELECTED);
            skinMesh->GetPointData()->AddArray(deciMap);
            size_t i = 0;
            vtkIdType boneOffset = m_intermUseBones ? m_bonesTransformed.size() : 0;
            for (NodePtr n : m_skinTransformed)
            {
                if (mapRejected[i + boneOffset])// skin nodes are after bone -> offset by number of bone control points
                    deciMap->SetValue(n->getId(), IS_NOT_PRIMITIVE_SELECTED);
                i++;
            }
        }
    }

    vtkSmartPointer<vtkDoubleArray> IntermediateTargetsInterface::createNuggetMapBones()
    {
        if (m_intermUseBones && !smartNuggets.empty() && !m_useDefaultNugget)
        {
            vtkSmartPointer<vtkDoubleArray> nuggetMap = vtkSmartPointer<vtkDoubleArray>::New();
            nuggetMap->SetNumberOfTuples(m_hbm->fem().getFEModelVTK()->getVTKMesh()->GetNumberOfPoints());
            nuggetMap->SetNumberOfComponents(1);
            nuggetMap->SetName(_PIPER_NUGGET_MAP);
            nuggetMap->FillComponent(0, 1);
            size_t i = 0;
            for (NodePtr n : m_bonesTransformed)
            {
                if (!mapRejected[i])
                    nuggetMap->SetValue(inverseNid[n->getId()], smartNuggets[i]);
                i++;
            }
            return nuggetMap;
        }
        return NULL;
    }

    vtkSmartPointer<vtkBitArray> IntermediateTargetsInterface::createDecimationMapBones()
    {
        if (m_intermUseBones && m_decimationValid)
        {
            vtkSmartPointer<vtkBitArray> deciMap = vtkSmartPointer<vtkBitArray>::New();
            deciMap->SetNumberOfTuples(m_hbm->fem().getFEModelVTK()->getVTKMesh()->GetNumberOfPoints());
            deciMap->SetNumberOfComponents(1);
            deciMap->SetName(_PIPER_DECIMATION_MAP);
            deciMap->FillComponent(0, IS_PRIMITIVE_SELECTED);
            size_t i = 0;
            for (NodePtr n : m_bonesTransformed)
            {
                vtkIdType id = inverseNid[n->getId()];
                if (mapRejected[i])
                    deciMap->SetValue(id, IS_NOT_PRIMITIVE_SELECTED);
                i++;
            }
            return deciMap;
        }
        return NULL;
    }

    boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> IntermediateTargetsInterface::getScaledBones()
    {
        boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> ret;
        if (m_bonesTransValid)
        {
            boost::container::map<std::string, vtkSmartPointer<vtkUnstructuredGrid>> *entities = m_hbm->fem().getFEModelVTK()->getVTKEntityMesh();
            // use the same points array for all bones
            vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
            points->DeepCopy(m_hbm->fem().getFEModelVTK()->getVTKMesh()->GetPoints());
            for (NodePtr n : m_bonesTransformed)
                points->SetPoint(inverseNid[n->getId()], n->getCoordX(), n->getCoordY(), n->getCoordZ());
            for (auto &ent : *entities)
            {
                if (anatomydb::isBone(ent.first))
                {
                    vtkSmartPointer<vtkUnstructuredGrid> e = vtkSmartPointer<vtkUnstructuredGrid>::New();
                    e->ShallowCopy(ent.second);
                    e->SetPoints(points);
                    ret[ent.first] = e;
                }
            }
        }
        return ret;

    }

    void IntermediateTargetsInterface::scaleBones()
    {
        if (!m_bonesTransValid && m_cpBonesSource.size() > 0)
        {
            if (m_skinAndBonesPointsSrc == nullptr) // else it has already been done -> don't do it again
            {
                inverseNid.clear();
                vtkSmartPointer<vtkUnstructuredGrid> sourceModel = m_hbm->fem().getFEModelVTK()->getVTKMesh();
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(sourceModel->GetPointData()->GetArray("nid"));
                vtkIdType nPoints = sourceModel->GetNumberOfPoints();
                for (vtkIdType i = 0; i < nPoints; i++)
                    inverseNid[nid->GetValue(i)] = i;
                VId boneNodeIds = m_hbm->findBoneNodes(true, false, false);
                std::map<Id, bool> boneNodesMap;
                for (Id id : boneNodeIds)
                    boneNodesMap[id] = true;
                m_bonesSource.clear();
                Nodes n = m_hbm->fem().getNodes();
                for (NodePtr node : n)
                {
                    if (boneNodesMap[node->getId()])
                        m_bonesSource.setEntity(node);
                }
            }
            if (!m_lockBonesTarget || m_bonesTransformed.size() != m_bonesSource.size())
            {
                m_bonesTransformed.clear();
                m_bonesTransformed.makeCopy(m_bonesSource);
                if (!m_fixBones)
                {
                    initDeformation(false);
                    m_infoLog("Scaling bones...");
                    m_krigingEuclidBones.applyDeformation(m_bonesSource, &m_bonesTransformed);
                }
                m_skinAndBonesTargetPoints = NULL;
            }
            else
                m_infoLog("Re-using locked bones target");
            m_bonesTransValid = true;
        }
    }

    void IntermediateTargetsInterface::preprocessIntermediates()
    {
        if (m_skinAndBonesPointsSrc == NULL || m_skinAndBonesTargetPoints == NULL)
        {
            vtkSmartPointer<vtkUnstructuredGrid> sourceModel = m_hbm->fem().getFEModelVTK()->getVTKMesh();
            vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(sourceModel->GetPointData()->GetArray("nid")); // get nid of all the points now
            vtkSmartPointer<vtkPolyData> skinMesh = m_hbm->fem().getFEModelVTK()->getSkin(false, m_hbm->metadata().entities(), m_hbm->fem());
            vtkSmartPointer<vtkIdTypeArray> skinNid = vtkIdTypeArray::SafeDownCast(skinMesh->GetPointData()->GetArray("nid"));
            if (inverseNid.empty()) // it could have already been created in scaleBones
            {
                inverseNid.clear();
                vtkIdType nPoints = sourceModel->GetNumberOfPoints();
                for (vtkIdType i = 0; i < nPoints; i++)
                    inverseNid[nid->GetValue(i)] = i;
            }
            vtkIdType noOfCPs = m_bonesTransformed.size() + m_skinTransformed.size();
            if (m_intermUseSkin || m_intermUseBones)
            {
                if (m_intermUseSkin)
                    scaleSkin();
                else if (!m_lockSkinTarget)
                {
                    m_skinSource.clear();
                    m_skinTransformed.clear();
                }
                if (m_intermUseBones)
                    scaleBones();
                else if (!m_lockBonesTarget)
                {
                    m_bonesSource.clear();
                    m_bonesTransformed.clear();
                }
                noOfCPs = m_intermUseSkin ? m_skinSource.size() : 0;
                noOfCPs += m_intermUseBones ? m_bonesSource.size() : 0;
                if (m_skinAndBonesPointsSrc == NULL)
                {
                    // we have to create a vtk representation for the bone nodes
                    m_skinAndBonesPointsSrc = vtkSmartPointer<vtkPoints>::New();
                    m_skinAndBonesPointsSrc->SetNumberOfPoints(noOfCPs);

                    vtkIdType id = 0;
                    vtkIdToCPIndex.clear();
                    if (m_intermUseBones)
                    {
                        for (NodePtr n : m_bonesSource)
                        {
                            m_skinAndBonesPointsSrc->SetPoint(id, n->getCoordX(), n->getCoordY(), n->getCoordZ());
                            vtkIdToCPIndex[inverseNid[n->getId()]] = id++;
                        }
                    }
                    if (m_intermUseSkin)
                    {
                        for (NodePtr n : m_skinSource)
                        {
                            m_skinAndBonesPointsSrc->SetPoint(id, n->getCoordX(), n->getCoordY(), n->getCoordZ());
                            vtkIdToCPIndex[inverseNid[skinNid->GetValue(n->getId())]] = id++;
                        }
                    }

                    // build locators for the main domain - is used even if other domains are not
                    m_mainDomain.boundary = skinMesh;
                    m_mainDomain.pointsInDomain = sourceModel;
                    if (m_skinAndBonesPointsSrc->GetNumberOfPoints() > 0)
                    {
                        vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                        cpHolder->SetPoints(m_skinAndBonesPointsSrc);
                        m_mainDomain.CPLocatorSource = vtkSmartPointer<vtkKdTreePointLocator>::New();
                        m_mainDomain.CPLocatorSource->SetDataSet(cpHolder);
                        m_mainDomain.CPLocatorSource->BuildLocator();
                    }
                    m_domains.clear();// reset the domains
                }

                m_skinAndBonesTargetPoints = vtkSmartPointer<vtkPoints>::New();
                m_skinAndBonesTargetPoints->SetNumberOfPoints(noOfCPs);
                vtkIdType id = 0;
                // write down target positions to the target mesh
                if (m_intermUseBones)
                {
                    for (NodePtr n : m_bonesTransformed)
                        m_skinAndBonesTargetPoints->SetPoint(id++, n->getCoordX(), n->getCoordY(), n->getCoordZ());
                }
                if (m_intermUseSkin)
                {
                    for (NodePtr n : m_skinTransformed)
                        m_skinAndBonesTargetPoints->SetPoint(id++, n->getCoordX(), n->getCoordY(), n->getCoordZ());
                }
                if (m_skinAndBonesTargetPoints->GetNumberOfPoints() > 0)
                {
                    vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                    cpHolder->SetPoints(m_skinAndBonesTargetPoints);
                    m_mainDomain.CPLocatorTarget = vtkSmartPointer<vtkKdTreePointLocator>::New();
                    m_mainDomain.CPLocatorTarget->SetDataSet(cpHolder);
                    m_mainDomain.CPLocatorTarget->BuildLocator();
                }
            }

            mapRejected.clear();
            mapRejected.resize(noOfCPs);

            smartNuggets.clear();
            smartNuggets.resize(noOfCPs);
            std::fill(smartNuggets.begin(), smartNuggets.end(), 0);
        }
    }

    void IntermediateTargetsInterface::decimateByDisplacement(HBMKrigingDomain &domain)
    {
        double curPointSrc[3], curPointTarget[3], curPointDisplacement[3], avgDisplacement[3], tempTarget[3], tempSource[3], tempDisplacement[3];
        vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
        vtkSmartPointer<vtkPolyData> cpHolder = vtkPolyData::SafeDownCast(domain.CPLocatorSource->GetDataSet());
        vtkSmartPointer<vtkIdTypeArray> domToGlobalCP = vtkIdTypeArray::SafeDownCast(cpHolder->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
        vtkIdType *randomOrder = new vtkIdType[cpHolder->GetNumberOfPoints()];
        for (vtkIdType i = 0; i < cpHolder->GetNumberOfPoints(); i++)
            randomOrder[i] = i;
        std::random_shuffle(&randomOrder[0], &randomOrder[cpHolder->GetNumberOfPoints()]);
        for (vtkIdType k = 0; k < cpHolder->GetNumberOfPoints(); k++)
        {
            vtkIdType i = randomOrder[k];
            vtkIdType CPIndex = domToGlobalCP == NULL ? i : domToGlobalCP->GetValue(i);
            m_skinAndBonesPointsSrc->GetPoint(CPIndex, curPointSrc);
            m_skinAndBonesTargetPoints->GetPoint(CPIndex, curPointTarget);
            vtkMath::Subtract(curPointTarget, curPointSrc, curPointDisplacement);
            domain.CPLocatorSource->FindPointsWithinRadius(m_deciRadius, curPointSrc, neighbors);
            for (int axis = 0; axis < 3; axis++)
                avgDisplacement[axis] = 0;
            double distSum = 0;
            for (vtkIdType j = 0; j < neighbors->GetNumberOfIds(); j++)
            {
                vtkIdType neiCPIndex = domToGlobalCP == NULL ? neighbors->GetId(j) : domToGlobalCP->GetValue(neighbors->GetId(j));
                if (!mapRejected[neiCPIndex] && neiCPIndex != CPIndex)
                {
                    m_skinAndBonesPointsSrc->GetPoint(neiCPIndex, tempSource);
                    m_skinAndBonesTargetPoints->GetPoint(neiCPIndex, tempTarget);
                    double distWeight = vtkMath::Distance2BetweenPoints(tempSource, curPointSrc); // weigh by inverse distance
                    if (distWeight >= DBL_EPSILON)
                    {
                        distWeight = 1 / distWeight;
                        vtkMath::Subtract(tempTarget, tempSource, tempDisplacement);
                        distSum += distWeight;
                        vtkMath::MultiplyScalar(tempDisplacement, distWeight);
                        vtkMath::Add(avgDisplacement, tempDisplacement, avgDisplacement);
                    }
                }
            }
            if (distSum != 0)
            {
                vtkMath::MultiplyScalar(avgDisplacement, 1 / distSum); // average the weights
                double deviationFromAvg = sqrt(vtkMath::Distance2BetweenPoints(avgDisplacement, curPointDisplacement));
                if (deviationFromAvg <= m_deciDeviation)
                    mapRejected[CPIndex] = true;
            }
        }
        delete[]randomOrder;
    }

    void IntermediateTargetsInterface::decimateCtrlPoints()
    {
        if (!m_decimationValid)
        {
            preprocessIntermediates();
            if (m_intermUseDomains)
                hbmDomainDecomposition();

            std::fill(mapRejected.begin(), mapRejected.end(), false); // reset decimation - nothing is decimated
            if (m_deciUseDisplacement)
            {
                m_infoLog("Decimating control points based on relative displacement...");
                if (m_domains.size() < 2 || !m_intermUseDomains)
                    decimateByDisplacement(m_mainDomain);
                else
                {
                    for (HBMKrigingDomain &domain : m_domains)
                        decimateByDisplacement(domain);
                }
            }
            if (m_deciUseHomogenous)
            {
                m_infoLog("Decimating control points homogenously...");
                // compute bounds of the control points that are supposed to be used
                double bounds[6];
                bounds[0] = std::numeric_limits<double>::max();
                bounds[1] = std::numeric_limits<double>::min();
                bounds[2] = std::numeric_limits<double>::max();
                bounds[3] = std::numeric_limits<double>::min();
                bounds[4] = std::numeric_limits<double>::max();
                bounds[5] = std::numeric_limits<double>::min();
                size_t i = 0;
                size_t s = m_intermUseBones ? m_bonesTransformed.size() : 0;
                double curPoint[3];
                for (int v = 0; v < 2; v++) // one for skin, one for bones
                {
                    for (; i < s; i++)
                    {
                        if (!mapRejected[i])
                        {
                            m_skinAndBonesPointsSrc->GetPoint(i, curPoint);
                            if (curPoint[0] < bounds[0]) bounds[0] = curPoint[0];
                            if (curPoint[0] > bounds[1]) bounds[1] = curPoint[0];
                            if (curPoint[1] < bounds[2]) bounds[2] = curPoint[1];
                            if (curPoint[1] > bounds[3]) bounds[3] = curPoint[1];
                            if (curPoint[2] < bounds[4]) bounds[4] = curPoint[2];
                            if (curPoint[2] > bounds[5]) bounds[5] = curPoint[2];
                        }
                    }
                    s += m_intermUseSkin ? m_skinTransformed.size() : 0;
                }
                // set the size of the voxel by portion of width
                double xRange = bounds[1] - bounds[0];
                double yRange = bounds[3] - bounds[2];
                double voxelSize = xRange / m_deciGridSizeX;
                if (voxelSize > 0)
                {
                    double voxelSizeInv = 1 / voxelSize;
                    double voxelSizeHalf = voxelSize * 0.5;
                    int voxelsAlongY = (int)std::ceil(yRange * voxelSizeInv);
                    double prevPoint[3], center[3];
                    s = m_intermUseBones ? m_bonesTransformed.size() : 0;
                    i = 0;
                    for (int v = 0; v < 2; v++) // do the decimation separately for skin and for bones
                    {
                        // each voxel will contain only one node; this table will hash the IDs of those nodes as x + y * voxelsAlongX + z * voxelsAlongX * voxelsAlongY
                        std::map<long, vtkIdType> voxels;
                        for (; i < s; i++)
                        {
                            if (!mapRejected[i])
                            {
                                // compute which voxel this point belongs to
                                m_skinAndBonesPointsSrc->GetPoint(i, curPoint);
                                int x = (int)((curPoint[0] - bounds[0]) * voxelSizeInv);
                                int y = (int)((curPoint[1] - bounds[2]) * voxelSizeInv);
                                int z = (int)((curPoint[2] - bounds[4]) * voxelSizeInv);
                                long hash = x + y * m_deciGridSizeX + z * m_deciGridSizeX * voxelsAlongY;
                                auto prev = voxels.find(hash);
                                // other point has already been added to this voxel; keep the one that is closer to center
                                if (prev != voxels.end())
                                {
                                    m_skinAndBonesPointsSrc->GetPoint(prev->second, prevPoint);
                                    center[0] = x * voxelSize + voxelSizeHalf;
                                    center[1] = y * voxelSize + voxelSizeHalf;
                                    center[2] = z * voxelSize + voxelSizeHalf;
                                    // if curPoint is closer to center, keep it and decimate the previous point
                                    if (vtkMath::Distance2BetweenPoints(prevPoint, center) > vtkMath::Distance2BetweenPoints(curPoint, center)) 
                                    {
                                        voxels[hash] = i;
                                        mapRejected[prev->second] = true;
                                    }
                                    else
                                        mapRejected[i] = true; // otherwise decimate the curPoint
                                }
                                else
                                    voxels[hash] = i;
                            }
                        }
                        s += m_intermUseSkin ? m_skinTransformed.size() : 0;
                    }
                }
            }
            m_decimationValid = true;
            m_nuggetsValid = false;
        }
    }

    void IntermediateTargetsInterface::computeNuggetsDispDeviation(HBMKrigingDomain &domain)
    {
        vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
        double curPointSrc[3], curPointTarget[3], curPointDisplacement[3], avgDisplacementSkin[3], avgDisplacementBone[3], tempTarget[3], tempSource[3], tempDisplacement[3];
        vtkSmartPointer<vtkPolyData> cpHolder = vtkPolyData::SafeDownCast(domain.CPLocatorTarget->GetDataSet());
        vtkSmartPointer<vtkIdTypeArray> domToGlobalCP = vtkIdTypeArray::SafeDownCast(cpHolder->GetPointData()->GetArray(ORIGINAL_POINT_IDS));

        vtkIdType boneOffset = m_intermUseBones ? m_bonesTransformed.size() : 0;
        vtkIdType npts = cpHolder->GetNumberOfPoints();
        for (vtkIdType i = 0; i < npts; i++)
        {
            vtkIdType CPIndex = domToGlobalCP == NULL ? i : domToGlobalCP->GetValue(i);
            if (!mapRejected[CPIndex])
            {
                bool iIsBone = CPIndex < boneOffset;
                if (m_fixBones && iIsBone)
                    smartNuggets[CPIndex] = 0;
                else
                {
                    for (int axis = 0; axis < 3; axis++)
                    {
                        avgDisplacementBone[axis] = 0;
                        avgDisplacementSkin[axis] = 0;
                    }
                    double avgDistanceSkin = 0, avgDistanceBone = 0;
                    double distSumSkin = 0, distSumBone = 0;
                    m_skinAndBonesPointsSrc->GetPoint(CPIndex, curPointSrc);
                    m_skinAndBonesTargetPoints->GetPoint(CPIndex, curPointTarget);
                    domain.CPLocatorTarget->FindPointsWithinRadius(m_nuggetRadius, curPointSrc, neighbors);
                    vtkMath::Subtract(curPointTarget, curPointSrc, curPointDisplacement);
                    vtkIdType nIds = neighbors->GetNumberOfIds();
                    for (vtkIdType j = 0; j < nIds; j++)
                    {
                        vtkIdType neiCPIndex = domToGlobalCP == NULL ? neighbors->GetId(j) : domToGlobalCP->GetValue(neighbors->GetId(j));
                        if (mapRejected[neiCPIndex] || neiCPIndex == CPIndex)
                            continue;
                        m_skinAndBonesPointsSrc->GetPoint(neiCPIndex, tempSource);
                        m_skinAndBonesTargetPoints->GetPoint(neiCPIndex, tempTarget);
                        double distWeight = vtkMath::Distance2BetweenPoints(tempSource, curPointSrc); // weigh by inverse distance
                        if (distWeight < DBL_EPSILON)
                            continue;
                        bool isABone = neiCPIndex < boneOffset;
                        distWeight = 1 / distWeight;
                        vtkMath::Subtract(tempTarget, tempSource, tempDisplacement);
                        if (isABone)
                        {
                            distSumBone += distWeight;
                            avgDistanceBone += sqrt(vtkMath::Distance2BetweenPoints(tempDisplacement, curPointDisplacement)) * distWeight;
                        }
                        else
                        {
                            distSumSkin += distWeight;
                            avgDistanceSkin += sqrt(vtkMath::Distance2BetweenPoints(tempDisplacement, curPointDisplacement)) * distWeight;
                        }
                        vtkMath::MultiplyScalar(tempDisplacement, distWeight);
                        vtkMath::Add((isABone ? avgDisplacementBone : avgDisplacementSkin), tempDisplacement, (isABone ? avgDisplacementBone : avgDisplacementSkin));
                    }
                    double deviationFromAvgSkin = 0, deviationFromAvgBone = 0;
                    if (distSumSkin != 0)
                    {
                        vtkMath::MultiplyScalar(avgDisplacementSkin, 1 / distSumSkin); // average the weights
                        vtkMath::Add(avgDisplacementSkin, curPointSrc, avgDisplacementSkin); // avgDisplacement becomes avgTarget
                        deviationFromAvgSkin = sqrt(vtkMath::Distance2BetweenPoints(avgDisplacementSkin, curPointTarget));
                        avgDistanceSkin /= distSumSkin;
                    }
                    if (distSumBone != 0)
                    {
                        vtkMath::MultiplyScalar(avgDisplacementBone, 1 / distSumBone); // average the weights
                        vtkMath::Add(avgDisplacementBone, curPointSrc, avgDisplacementBone); // avgDisplacement becomes avgTarget
                        deviationFromAvgBone = sqrt(vtkMath::Distance2BetweenPoints(avgDisplacementBone, curPointTarget));
                        avgDistanceBone /= distSumBone;
                    }
                    smartNuggets[CPIndex] = (iIsBone ? -m_nuggetScaleBones : -m_nuggetScaleSkin)
                        * ((m_nuggetBoneWeight * deviationFromAvgBone) + ((1 - m_nuggetBoneWeight) * deviationFromAvgSkin));
                }
            }
        }
    }

    void IntermediateTargetsInterface::computeNuggetsInterm()
    {
        if (!m_nuggetsValid && !m_useDefaultNugget)
        {
            decimateCtrlPoints();
            m_infoLog("Computing nuggets based on deviation from local average of displacement...");

            if (m_domains.size() < 2 || !m_intermUseDomains)
                computeNuggetsDispDeviation(m_mainDomain);
            else
            {
                for (HBMKrigingDomain &domain : m_domains)
                    computeNuggetsDispDeviation(domain);
            }
            m_nuggetsValid = true;
        }
    }

    void IntermediateTargetsInterface::scaleHBMDenseWIntermediates(piper::hbm::Nodes* outputNodes, unsigned int boxSplitCP, double globalNugget)
    {
        decimateCtrlPoints();
        computeNuggetsInterm();
        bool useMainDomain = m_domains.size() < 2 || !m_intermUseDomains;
        // now use the obtained out-points as control points and deform everything
        vtkSmartPointer<vtkUnstructuredGrid> sourceModel = m_hbm->fem().getFEModelVTK()->getVTKMesh();
        vtkSmartPointer<vtkUnstructuredGrid> targetModel = vtkSmartPointer<vtkUnstructuredGrid>::New();
        targetModel->DeepCopy(sourceModel);
        vtkSmartPointer<vtkPolyData> skinMesh = m_hbm->fem().getFEModelVTK()->getSkin(false, m_hbm->metadata().entities(), m_hbm->fem());
        vtkSmartPointer<vtkIdTypeArray> skinNid = vtkIdTypeArray::SafeDownCast(skinMesh->GetPointData()->GetArray("nid"));
        std::map<Id, vtkIdType> inverseSkinNid; // maps vtkId on the skin mesh based on directly the FE nid
        vtkIdType nTup = skinNid->GetNumberOfTuples();
        for (vtkIdType i = 0; i < nTup; i++)
            inverseSkinNid[skinNid->GetValue(i)] = i;

        // write down target positions to the target mesh
        for (std::shared_ptr<Node> const& n : m_bonesTransformed)
            targetModel->GetPoints()->SetPoint(inverseNid[n->getId()], n->getCoordX(), n->getCoordY(), n->getCoordZ());
        for (std::shared_ptr<Node> const& n : m_skinTransformed)
            targetModel->GetPoints()->SetPoint(inverseNid[skinNid->GetValue(n->getId())], n->getCoordX(), n->getCoordY(), n->getCoordZ());
        
        std::vector<bool> fixedNodes;
        fixedNodes.resize(targetModel->GetNumberOfPoints());
        std::fill(fixedNodes.begin(), fixedNodes.end(), false); // don't fix anything
        // map the indices used by m_skinTransformed etc. to the vtk indices. also mark fixed nodes
        std::map<size_t, vtkIdType> indicesInOrigMesh;
        size_t boneOffset = m_intermUseBones ? m_bonesTransformed.size() : 0;
        for (size_t i = 0; i < boneOffset; i++) // it's a bone point, look up it's ID in the bonesTransformed/bonesSource vector
        {
            auto iter = m_bonesTransformed.begin();
            std::advance(iter, i);
            indicesInOrigMesh[i] = inverseNid[iter->get()->getId()];
            if (m_fixBones)
                fixedNodes[indicesInOrigMesh[i]] = true;
        }
        auto nSkin = mapRejected.size() - boneOffset;
        for (size_t i = 0; i < nSkin; i++)
        {
            auto iter = m_skinTransformed.begin();
            std::advance(iter, i);
            indicesInOrigMesh[i + boneOffset] = inverseNid[skinNid->GetValue(iter->get()->getId())];
        }
        std::stringstream s;
        VId controlNodeCandidates;
        boost::container::vector<double> ctrlPtWeight;
        vtkSmartPointer<vtkUnstructuredGrid> trgt;
        vtkSmartPointer<vtkPoints> referencePoints;
        vtkOBBNodePtr OBB = std::make_shared<vtkOBBNodeWNeighbors>();
        boost::container::vector<vtkOBBNodePtr> OBBNodes;
        OBBNodes.push_back(OBB);
        if (useMainDomain)
        {
            // build a bounding box of the entire geometry. The simplest solution would be to use minDouble/maxDouble as boundaries,
            // but let's not do this to avoid numerical instability. Instead build an axis-aligned bounding box using the bounds of the polydata
            double *bounds = sourceModel->GetBounds();
            for (int i = 0; i < 3; i++)
            {
                OBB->Corner[i] = bounds[i * 2];
                OBB->Axes[i][0] = i == 0 ? (bounds[1] - bounds[0]) : 0;
                OBB->Axes[i][1] = i == 1 ? (bounds[3] - bounds[2]) : 0;
                OBB->Axes[i][2] = i == 2 ? (bounds[5] - bounds[4]) : 0;
            }

            ctrlPtWeight.resize(targetModel->GetNumberOfPoints());
            std::fill(ctrlPtWeight.begin(), ctrlPtWeight.end(), globalNugget); // set the global nugget to all points as default
            for (size_t i = 0; i < mapRejected.size(); i++)
            {
                if (!mapRejected[i])
                {
                    controlNodeCandidates.push_back(indicesInOrigMesh[i]);
                    if (!m_useDefaultNugget)
                        ctrlPtWeight[indicesInOrigMesh[i]] = smartNuggets[i];
                }
            }
            trgt = targetModel;
            referencePoints = sourceModel->GetPoints();

            s << "Applying kriging based on the intermediate targets, skin nodes: " << m_skinTransformed.size() << ", bone nodes: " << m_bonesTransformed.size()
                << ", total control points after decimation: " << controlNodeCandidates.size();
            m_infoLog(s.str());

            m_krigingEuclidBones.applyDeformationInABox(trgt, referencePoints,
                controlNodeCandidates, fixedNodes, ctrlPtWeight, OBBNodes, boxSplitCP, m_splitBoxOverlap, m_interpolateDisplacement, m_useDrift, true, trgt);
        }
        else
        {
            for (auto &domain : m_domains)
            {
                // build a bounding box of the entire geometry. The simplest solution would be to use minDouble/maxDouble as boundaries,
                // but let's not do this to avoid numerical instability. Instead build an axis-aligned bounding box using the bounds of the polydata
                double *bounds = domain.boundary->GetBounds();
                for (int i = 0; i < 3; i++)
                {
                    OBB->Corner[i] = bounds[i * 2];
                    OBB->Axes[i][0] = i == 0 ? (bounds[1] - bounds[0]) : 0;
                    OBB->Axes[i][1] = i == 1 ? (bounds[3] - bounds[2]) : 0;
                    OBB->Axes[i][2] = i == 2 ? (bounds[5] - bounds[4]) : 0;
                }

                std::vector<bool> fixNodesDomain;
                ctrlPtWeight.resize(domain.pointsInDomain->GetNumberOfPoints());
                std::fill(ctrlPtWeight.begin(), ctrlPtWeight.end(), globalNugget); // set the global nugget to all points as default
                vtkSmartPointer<vtkIdTypeArray> domainToParent = vtkIdTypeArray::SafeDownCast(domain.pointsInDomain->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                fixNodesDomain.resize(domain.pointsInDomain->GetNumberOfPoints());
                // copy the source domain and update target positions
                trgt = vtkSmartPointer<vtkUnstructuredGrid>::New();
                vtkSmartPointer<vtkPoints> p = vtkSmartPointer<vtkPoints>::New();
                p->SetNumberOfPoints(domain.pointsInDomain->GetNumberOfPoints());
                referencePoints = vtkSmartPointer<vtkPoints>::New();
                referencePoints->SetNumberOfPoints(domain.pointsInDomain->GetNumberOfPoints());
                for (vtkIdType i = 0; i < domain.pointsInDomain->GetNumberOfPoints(); i++)
                {
                    vtkIdType vtkID = domainToParent->GetValue(i);
                    p->SetPoint(i, targetModel->GetPoint(vtkID));
                    referencePoints->SetPoint(i, sourceModel->GetPoint(vtkID));
                    fixNodesDomain[i] = fixedNodes[vtkID];
                    auto iter = vtkIdToCPIndex.find(vtkID); // this maps only control node candidates - check that the current point in domain is one
                    if (iter != vtkIdToCPIndex.end() && !mapRejected[iter->second])
                    {
                        controlNodeCandidates.push_back(i);
                        if (!m_useDefaultNugget)
                            ctrlPtWeight[i] = smartNuggets[iter->second];
                    }
                }
                trgt->SetPoints(p);
                s << "Applying kriging based on the intermediate targets to domain with " << controlNodeCandidates.size() << " control points (after decimation).";
                m_infoLog(s.str());
                s.clear();
                s.str("");

                m_krigingEuclidBones.applyDeformationInABox(trgt, referencePoints,
                    controlNodeCandidates, fixNodesDomain, ctrlPtWeight, OBBNodes, boxSplitCP, m_splitBoxOverlap, m_interpolateDisplacement, m_useDrift, true, trgt);
#if PIPER_DOMAIN_DECOMP_DEBUGVIS
#if PIPER_DOMAIN_DECOMP_DEBUGVIStargetMeshes
                vtkSmartPointer<vtkRenderer> renderer =
                    vtkSmartPointer<vtkRenderer>::New();
                vtkSmartPointer<vtkRenderWindow> renderWindow =
                    vtkSmartPointer<vtkRenderWindow>::New();
                renderWindow->AddRenderer(renderer);

                renderWindow->SetAlphaBitPlanes(1);
                renderWindow->SetMultiSamples(0);
                renderer->SetUseDepthPeeling(1);
                renderer->SetMaximumNumberOfPeels(10);
                renderer->SetOcclusionRatio(0.01);

                // An interactor
                vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
                    vtkSmartPointer<vtkRenderWindowInteractor>::New();
                renderWindowInteractor->SetRenderWindow(renderWindow);
                vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
                vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                actor1mapper->SetInputData(useMainDomain ? m_mainDomain.boundary : domain.boundary);
                actor1->SetMapper(actor1mapper);
                actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                actor1->GetProperty()->SetOpacity(0.5);
                renderer->AddActor(actor1);

                vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
                sphereSource->SetCenter(0, 0, 0);
                sphereSource->SetRadius(1);
                vtkSmartPointer<vtkGlyph3DMapper> pointHighlightTarget = vtkSmartPointer<vtkGlyph3DMapper>::New();
                pointHighlightTarget->OrientOff();
                pointHighlightTarget->SetSourceConnection(sphereSource->GetOutputPort());
                pointHighlightTarget->SetScaleModeToNoDataScaling();

                vtkSmartPointer<vtkPolyData> targetPoints = vtkSmartPointer<vtkPolyData>::New();
                vtkSmartPointer<vtkPoints> tP = vtkSmartPointer<vtkPoints>::New();
                for (Id &id : controlNodeCandidates)
                    tP->InsertNextPoint(trgt->GetPoints()->GetPoint(id));
                targetPoints->SetPoints(tP);
                pointHighlightTarget->SetInputData(targetPoints); // uncomment to see only target control points

                vtkSmartPointer<vtkGlyph3DMapper> pointHighlightAll = vtkSmartPointer<vtkGlyph3DMapper>::New();
                pointHighlightAll->OrientOff();
                pointHighlightAll->SetSourceConnection(sphereSource->GetOutputPort());
                pointHighlightAll->SetScaleModeToNoDataScaling();
                pointHighlightAll->SetInputData(trgt);

                vtkSmartPointer<vtkOpenGLActor> pointactor = vtkSmartPointer<vtkOpenGLActor>::New();
                pointactor->SetMapper(pointHighlightAll);
                pointactor->GetProperty()->SetDiffuseColor(1, 1, 1);
                pointactor->GetProperty()->SetOpacity(1);
                renderer->AddActor(pointactor);

                vtkSmartPointer<vtkOpenGLActor> pointactorTarget = vtkSmartPointer<vtkOpenGLActor>::New();
                pointactorTarget->SetMapper(pointHighlightTarget);
                pointactorTarget->GetProperty()->SetDiffuseColor(0, 0, 1);
                pointactorTarget->GetProperty()->SetOpacity(1);
                renderer->AddActor(pointactorTarget);

                renderWindow->Render();

                vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
                    vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

                renderWindowInteractor->SetInteractorStyle(style);

                // Begin mouse interaction
                renderWindowInteractor->Start();
#endif
#endif

                // update targetModel with the kriged coordinates - progressive kriging through domains
                for (vtkIdType i = 0; i < domain.pointsInDomain->GetNumberOfPoints(); i++)
                    targetModel->GetPoints()->SetPoint(domainToParent->GetValue(i), trgt->GetPoint(i));
                controlNodeCandidates.clear();
            }
        }
        Nodes src = m_hbm->fem().getNodes();
        outputNodes->clear();
        outputNodes->makeCopy(src);
        double point[3];
        for (std::shared_ptr<Node> n : *outputNodes)
        {
            targetModel->GetPoint(inverseNid[n->getId()], point);
            n->setCoord(point[0], point[1], point[2]);
        }
    }

    void IntermediateTargetsInterface::scaleSparseAllCPs(piper::hbm::Nodes* outputNodes, unsigned int boxSplitCP, vtkSmartPointer<vtkPoints> transCPs)
    {
        if (m_cpBonesSource.size() > 0 || m_cpSkinSource.size() > 0)
        {
            m_sourceValidEuclidBones = false; // ensure the control points will be set in initDeformation
            VCoord concatCPsSource, concatCPsTarget;
            boost::container::vector<double> concatNuggets;
            m_skinAndBonesPointsSrc = vtkSmartPointer<vtkPoints>::New();
            m_skinAndBonesPointsSrc->SetNumberOfPoints(m_cpBonesSource.size());
            m_skinAndBonesTargetPoints = nullptr; // to ensure the preprocess will be done
            m_bonesTransformed.getContainer().resize(m_cpBonesSource.size()); // set the size of this container - it is used to determine size of the mapRejected
            bool backupUseSkin = m_intermUseSkin;
            bool backupUseBones = m_intermUseBones;
            bool backupDomains = m_intermUseDomains;
            m_intermUseBones = false;
            m_intermUseSkin = false;
            m_decimationValid = false;
            m_intermUseDomains = false;
            preprocessIntermediates(); // just to build inverse nid and allocate mapRejected
            m_skinAndBonesTargetPoints = vtkSmartPointer<vtkPoints>::New();
            m_skinAndBonesTargetPoints->SetNumberOfPoints(m_cpBonesTarget.size());
            vtkIdType i = 0;
            for (Coord &c : m_cpBonesSource)
                m_skinAndBonesPointsSrc->SetPoint(i++, c(0), c(1), c(2));
            i = 0;
            for (Coord &c : m_cpBonesTarget)
                m_skinAndBonesTargetPoints->SetPoint(i++, c(0), c(1), c(2));

            if (m_deciUseDisplacement)
            {
                vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                cpHolder->SetPoints(m_skinAndBonesPointsSrc);
                m_mainDomain.CPLocatorSource = vtkSmartPointer<vtkKdTreePointLocator>::New();
                m_mainDomain.CPLocatorSource->SetDataSet(cpHolder);
                m_mainDomain.CPLocatorSource->BuildLocator();
            }
            m_intermUseBones = true; // a bit of a hack - the homogenous decimation code looks for this to decide whether the bone points should be used
            decimateCtrlPoints();
            m_intermUseDomains = backupDomains;
            vtkSmartPointer<vtkPoints> trgtPoints = vtkSmartPointer<vtkPoints>::New();
            vtkSmartPointer<vtkPoints> referencePoints = vtkSmartPointer<vtkPoints>::New();
            VId controlNodeCandidates;
            for (size_t i = 0; i < mapRejected.size(); i++)
            {
                // add the points to be transformed only if transformation of control points was requested or if it is not decimated
                vtkIdType id = 0;
                if (transCPs != nullptr || !mapRejected[i])
                {
                    Coord &t = m_cpBonesTarget[i];
                    Coord &s = m_cpBonesSource[i];
                    referencePoints->InsertNextPoint(s(0), s(1), s(2));
                    id = trgtPoints->InsertNextPoint(t(0), t(1), t(2));
                    concatNuggets.push_back(m_bonesNugget[i]);
                }
                if (!mapRejected[i])
                    controlNodeCandidates.push_back(id);
            }

            vtkSmartPointer<vtkUnstructuredGrid> sourceModel = m_hbm->fem().getFEModelVTK()->getVTKMesh();
            vtkIdType realPointsOffset = referencePoints->GetNumberOfPoints();
            for (vtkIdType i = 0; i < sourceModel->GetNumberOfPoints(); i++)
            {
                referencePoints->InsertNextPoint(sourceModel->GetPoint(i));
                trgtPoints->InsertNextPoint(sourceModel->GetPoint(i));
                concatNuggets.push_back(concatNuggets[0]);
            }

            Nodes src = m_hbm->fem().getNodes();
            outputNodes->clear();
            outputNodes->makeCopy(src);

            if (m_intermUseDomains)
                hbmDomainDecomposition();

            m_intermUseBones = backupUseBones;
            m_intermUseSkin = backupUseSkin;
            if (m_intermUseDomains && m_domains.size() > 1) // use domains
            {
                m_infoLog("Assigning control points to domains...");
                // assign control points to domains - these are not necessarrily on the mesh, so can be outside domains
                std::list<vtkSmartPointer<vtkKdTreePointLocator>> locators;
                for (HBMKrigingDomain &domain : m_domains)
                {
                    vtkSmartPointer<vtkKdTreePointLocator> locator = vtkSmartPointer<vtkKdTreePointLocator>::New();
                    locator->SetDataSet(domain.boundary);
                    locator->BuildLocator();
                    locators.push_back(locator);
                }
                std::vector<std::vector<Id>> domainCPs;
                for (HBMKrigingDomain &domain : m_domains)
                    domainCPs.push_back(std::vector<Id>());
                // find the closest domain for each control point
                for (vtkIdType i = 0; i < realPointsOffset; i++)
                {
                    if (transCPs != NULL || !mapRejected[i]) // if it is rejected or we are not transforming CPs, we don't need the point to be part of the transformation process
                    {
                        double distance = VTK_DOUBLE_MAX;
                        size_t closestDomain = 0;
                        std::list<vtkSmartPointer<vtkKdTreePointLocator>>::iterator locIter = locators.begin();
                        size_t domainIndex = 0;
                        double *point = referencePoints->GetPoint(i);
                        for (HBMKrigingDomain &domain : m_domains)
                        {
                            vtkIdType id = locIter->Get()->FindClosestPoint(point);
                            double canDist = vtkMath::Distance2BetweenPoints(domain.boundary->GetPoint(id), point);
                            if (canDist < distance)
                            {
                                distance = canDist;
                                closestDomain = domainIndex;
                            }
                            locIter++;
                            domainIndex++;
                        }
                        domainCPs[closestDomain].push_back(i);
                    }
                }
                // do the transformation
                size_t domainIndex = 0;
                vtkSmartPointer<vtkIdTypeArray> nid = vtkIdTypeArray::SafeDownCast(m_hbm->fem().getFEModelVTK()->getVTKMesh()->GetPointData()->GetArray("nid"));
                int *nUpdated = new int[outputNodes->size()];
                std::fill(nUpdated, nUpdated + (outputNodes->size()), 0);
                for (NodePtr n : *outputNodes)
                    n->setCoord(0, 0, 0);
                for (HBMKrigingDomain &domain : m_domains)
                {
                    vtkSmartPointer<vtkUnstructuredGrid> domTrgt = vtkSmartPointer<vtkUnstructuredGrid>::New();
                    vtkSmartPointer<vtkPoints> domTrgtPoints = vtkSmartPointer<vtkPoints>::New();
                    vtkSmartPointer<vtkPoints> domRefPoints = vtkSmartPointer<vtkPoints>::New();
                    boost::container::vector<double> domNuggets;
                    VId domCandidates;
                    Id cp = 0;
                    for (Id &c : domainCPs[domainIndex])
                    {
                        if (transCPs != NULL || !mapRejected[c])
                        {
                            domTrgtPoints->InsertNextPoint(trgtPoints->GetPoint(c));
                            domRefPoints->InsertNextPoint(referencePoints->GetPoint(c));
                            domNuggets.push_back(concatNuggets[c]);
                        }
                        if (!mapRejected[c])
                            domCandidates.push_back(cp++);
                    }
                    vtkIdType domRealPointsOffset = domRefPoints->GetNumberOfPoints();
                    vtkSmartPointer<vtkIdTypeArray> domainToParent = vtkIdTypeArray::SafeDownCast(domain.pointsInDomain->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                    for (vtkIdType i = 0; i < domain.pointsInDomain->GetNumberOfPoints(); i++)
                    {
                        vtkIdType idAtParent = domainToParent->GetValue(i);
                        domRefPoints->InsertNextPoint(sourceModel->GetPoint(idAtParent));
                        domTrgtPoints->InsertNextPoint(sourceModel->GetPoint(idAtParent)); // the points to deform have starting target same as source - they are not used as control points
                        domNuggets.push_back(concatNuggets[idAtParent + realPointsOffset]);
                    }
                    domTrgt->SetPoints(domTrgtPoints);
                    std::vector<bool> fixNodes;
                    fixNodes.resize(domRefPoints->GetNumberOfPoints());
                    std::fill(fixNodes.begin(), fixNodes.end(), false);
                    vtkOBBNodePtr OBB = std::make_shared<vtkOBBNodeWNeighbors>();
                    boost::container::vector<vtkOBBNodePtr> OBBNodes;
                    OBBNodes.push_back(OBB);
                    double *bounds = domRefPoints->GetBounds();
                    for (int i = 0; i < 3; i++)
                    {
                        OBB->Corner[i] = bounds[i * 2];
                        OBB->Axes[i][0] = i == 0 ? (bounds[1] - bounds[0]) : 0;
                        OBB->Axes[i][1] = i == 1 ? (bounds[3] - bounds[2]) : 0;
                        OBB->Axes[i][2] = i == 2 ? (bounds[5] - bounds[4]) : 0;
                    }

                    std::stringstream s;
                    s << "Applying kriging to domain with " << domCandidates.size() << " control points...";
                    m_infoLog(s.str());

                    m_krigingEuclidBones.applyDeformationInABox(domTrgt, domRefPoints,
                        domCandidates, fixNodes, concatNuggets, OBBNodes, boxSplitCP,
                        m_splitBoxOverlap, m_interpolateDisplacement, m_useDrift, true, domTrgt);
                    // write the results into the global target buffer 
                    if (transCPs)
                    {
                        // update the control points
                        for (vtkIdType i = 0; i < domRealPointsOffset; i++)
                            referencePoints->SetPoint(domainCPs[domainIndex][i], domTrgt->GetPoint(i));
                    }
                    // update the model nodes
                    for (vtkIdType i = 0; i < domain.pointsInDomain->GetNumberOfPoints(); i++)
                    {
                        vtkIdType id = nid->GetValue(domainToParent->GetValue(i));
                        NodePtr n = outputNodes->getEntity(id);
                        double *point = domTrgt->GetPoint(i + domRealPointsOffset);
                        n->setCoord(n->getCoordX() + point[0], n->getCoordY() + point[1], n->getCoordZ() + point[2]);
                        nUpdated[id]++;
                    }
                    domainIndex++;
                }
                for (Id i = 0; i < outputNodes->size(); i++)
                {
                    NodePtr n = outputNodes->getEntity(i);
                    n->setCoord(n->getCoordX() / nUpdated[i], n->getCoordY() / nUpdated[i], n->getCoordZ() / nUpdated[i]);
                }
                delete[] nUpdated;
            }
            else
            {
                std::vector<bool> fixNodes;
                fixNodes.resize(referencePoints->GetNumberOfPoints());
                std::fill(fixNodes.begin(), fixNodes.end(), false);
                vtkSmartPointer<vtkUnstructuredGrid> trgt = vtkSmartPointer<vtkUnstructuredGrid>::New();
                trgt->SetPoints(trgtPoints);

                vtkOBBNodePtr OBB = std::make_shared<vtkOBBNodeWNeighbors>();
                boost::container::vector<vtkOBBNodePtr> OBBNodes;
                OBBNodes.push_back(OBB);
                double *bounds = referencePoints->GetBounds();
                for (int i = 0; i < 3; i++)
                {
                    OBB->Corner[i] = bounds[i * 2];
                    OBB->Axes[i][0] = i == 0 ? (bounds[1] - bounds[0]) : 0;
                    OBB->Axes[i][1] = i == 1 ? (bounds[3] - bounds[2]) : 0;
                    OBB->Axes[i][2] = i == 2 ? (bounds[5] - bounds[4]) : 0;
                }
                std::stringstream s;
                s << "Applying direct kriging using  " << controlNodeCandidates.size() << " control points after decimation...";
                m_infoLog(s.str());
                m_krigingEuclidBones.applyDeformationInABox(trgt, referencePoints,
                    controlNodeCandidates, fixNodes, concatNuggets, OBBNodes, boxSplitCP, m_splitBoxOverlap, m_interpolateDisplacement, m_useDrift, true,
                    sourceModel);

                double point[3];
                if (transCPs)
                {
                    transCPs->SetNumberOfPoints(mapRejected.size());
                    for (vtkIdType i = 0; i < transCPs->GetNumberOfPoints(); i++)
                        transCPs->SetPoint(i, trgt->GetPoint(i));
                }
                // update the model nodes
                for (std::shared_ptr<Node> n : *outputNodes)
                {
                    trgt->GetPoint(inverseNid[n->getId()] + realPointsOffset, point);
                    n->setCoord(point[0], point[1], point[2]);
                }
            }

            m_bonesTransformed.clear();
            m_skinTransformed.clear();
            m_skinAndBonesPointsSrc = NULL;
            m_skinAndBonesTargetPoints = NULL;
        }
    }

    void IntermediateTargetsInterface::scaleSparseAllCPs(piper::hbm::Nodes& srcNodes, piper::hbm::Nodes* outputNodes)
    {
        m_sourceValidEuclidBones = false; // ensure the control points will be set in initDeformation
        VCoord concatCPsSource, concatCPsTarget;
        std::vector<double> concatNuggets;
        concatCPsSource.insert(concatCPsSource.begin(), m_cpBonesSource.begin(), m_cpBonesSource.end());
        concatCPsSource.insert(concatCPsSource.end(), m_cpSkinSource.begin(), m_cpSkinSource.end());
        concatCPsTarget.insert(concatCPsTarget.begin(), m_cpBonesTarget.begin(), m_cpBonesTarget.end());
        concatCPsTarget.insert(concatCPsTarget.end(), m_cpSkinTarget.begin(), m_cpSkinTarget.end());
        concatNuggets.insert(concatNuggets.begin(), m_bonesNugget.begin(), m_bonesNugget.end());
        concatNuggets.insert(concatNuggets.end(), m_skinNugget.begin(), m_skinNugget.end());
        m_krigingEuclidBones.setControlPoints(concatCPsSource, concatCPsTarget, concatNuggets, m_interpolateDisplacement, m_useDrift);
        m_krigingEuclidBones.compute(true, KrigingPiperInterface::krigingDuplPointLimit);
        m_krigingEuclidBones.applyDeformation(srcNodes, outputNodes);
    }

    void IntermediateTargetsInterface::initDeformation(bool isSkin, vtkSmartPointer<vtkPolyData> surface)
    {
        std::stringstream info;
        if (surface == NULL)
        {
            if (isSkin)
            {
                auto nCps = m_cpSkinSource.size();
                if (nCps > 0)
                {
                    if (m_sourceValidEuclidSkin)
                        m_krigingEuclidSkin.setTargetControlPoints(m_cpSkinTarget);
                    else
                    {
                        m_krigingEuclidSkin.setControlPoints(m_cpSkinSource, m_cpSkinTarget, m_skinNugget, m_interpolateDisplacement, m_useDrift);
                        m_sourceValidEuclidSkin = true;
                    }
                    info << "Computing skin deformation field (euclidean kernel), number of control points: " << nCps;
                    m_infoLog(info.str());
                    m_krigingEuclidSkin.compute(true, KrigingPiperInterface::krigingDuplPointLimit);
                }
                else
                    info << "Zero control points associated with skin, skin intermediate target will not be created.";
            }
            else
            {
                auto nCps = m_cpBonesSource.size();
                if (nCps > 0)
                {
                    if (m_sourceValidEuclidBones)
                        m_krigingEuclidBones.setTargetControlPoints(m_cpBonesTarget);
                    else
                    {
                        m_krigingEuclidBones.setControlPoints(m_cpBonesSource, m_cpBonesTarget, m_bonesNugget, m_interpolateDisplacement, m_useDrift);
                        m_sourceValidEuclidBones = true;
                    }
                    info << "Computing bone deformation field (euclidean kernel), number of control points: " << nCps;
                    m_infoLog(info.str());
                    m_krigingEuclidBones.compute(true, KrigingPiperInterface::krigingDuplPointLimit);
                }
                else
                    info << "Zero control points associated with bones, bone intermediate target will not be created.";
            }
        }
        else
        {
            auto nCps = m_cpSkinSource.size();
            if (nCps > 0)
            {
                if (m_sourceValidGeodesic)
                    m_krigingGeodesic.setTargetControlPoints(m_cpSkinTarget);
                else
                {
                    m_krigingGeodesic.setControlPoints(m_cpSkinSource, m_cpSkinTarget, m_skinNugget, surface, m_geodesicDistanceType);
                    m_sourceValidGeodesic = true;
                }
                info << "Computing skin deformation field (surface distance kernel), number of control points: " << nCps;
                m_infoLog(info.str());
                m_krigingGeodesic.SetGeodesicPrecision(m_geodesicPrecision);
                m_krigingGeodesic.SetGeodesicType(m_geodesicDistanceType);
                m_krigingGeodesic.compute(false); // the duplications are already checked-for in this version of setControlPoints (using the same DuplPointsLimit) - no need to check for them again
            }
            else
                info << "Zero control points associated with skin, skin intermediate target will not be created.";
        }
    }

    void IntermediateTargetsInterface::hbmDomainDecomposition()
    {
        if (m_domains.empty())
        {
            preprocessIntermediates();

            vtkSmartPointer<vtkPolyData> skinMesh = m_hbm->fem().getFEModelVTK()->getSkin(false, m_hbm->metadata().entities(), m_hbm->fem());
            vtkSmartPointer<vtkIdTypeArray> skinNid = vtkIdTypeArray::SafeDownCast(skinMesh->GetPointData()->GetArray("nid"));
            vtkSmartPointer<vtkUnstructuredGrid> sourceModel = m_hbm->fem().getFEModelVTK()->getVTKMesh();
            std::map<Id, vtkIdType> inverseSkinNid; // maps vtkId on the skin mesh based on directly the FE nid
            for (vtkIdType i = 0; i < skinNid->GetNumberOfTuples(); i++)
                inverseSkinNid[skinNid->GetValue(i)] = i;

            
            Metadata::GenericMetadataCont namedMeta = m_hbm->metadata().genericmetadatas();

            HBMKrigingDomain startDomain;
            startDomain.boundary = vtkSmartPointer<vtkPolyData>::New();
            startDomain.boundary->DeepCopy(skinMesh);
            startDomain.pointsInDomain = vtkSmartPointer<vtkUnstructuredGrid>::New();
            startDomain.pointsInDomain->DeepCopy(sourceModel);
            m_domains.push_back(startDomain);
            bool useOverlap = true; // could be a GUI parameter, let's see if it should be always on or not in testing
            for (auto &domain : namedMeta)
            {
                // if there is named metadata ****_decomposition, it should contain ring of vertices that divide one domain from another
                if (domain.first.length() >= GenericMetadata::domainDecompositionBoundary.length())
                {
                    if (0 == domain.first.compare(domain.first.length() - GenericMetadata::domainDecompositionBoundary.length(),
                        GenericMetadata::domainDecompositionBoundary.length(), GenericMetadata::domainDecompositionBoundary))
                    {
                        VId domainNodes = domain.second.getNodeIdVec(m_hbm->fem());
                        vtkSmartPointer<vtkIdList> ring = vtkSmartPointer<vtkIdList>::New();
                        ring->SetNumberOfIds(domainNodes.size());
                        vtkIdType i = 0;
                        for (Id &id : domainNodes)
                        {
                            auto inv = inverseSkinNid.find(id);
                            if (inv == inverseSkinNid.end())
                            {
                                std::stringstream w;
                                w << "Model decomposition \"" << domain.first << "\" is not valid and will be ignored: not all decomposing points are part of the skin.";
                                m_warningLog(w.str());
                                ring = NULL;
                                break;
                            }
                            ring->SetId(i++, inv->second);
                        }
                        if (ring == NULL)
                            continue;

                        // find the domain that contains the current ring - at least one of its points (the rings must not cross)
                        std::list<HBMKrigingDomain>::iterator splitDomain;
                        if (m_domains.size() > 1)
                        {
                            vtkIdType lookingFor = ring->GetId(0);
                            bool found = false;
                            for (splitDomain = m_domains.begin(); splitDomain != m_domains.end(); splitDomain++)
                            {
                                // lookingFor is a skinMesh index - look for it in the origPoints array
                                vtkSmartPointer<vtkIdTypeArray> domToSkinMapPoints = vtkIdTypeArray::SafeDownCast(splitDomain->boundary->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                                for (vtkIdType i = 0; i < domToSkinMapPoints->GetNumberOfTuples(); i++)
                                {
                                    if (domToSkinMapPoints->GetValue(i) == lookingFor)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (found)
                                    break;
                            }
                            if (!found)
                            {
                                std::stringstream w;
                                w << "Model decomposition \"" << domain.first << "\" is not valid and will be ignored. The specified points were not found on the skin.";
                                m_warningLog(w.str());
                                continue;
                            }
                            // now re-map the ring vertices to match IDs of the splitDomain
                            vtkSmartPointer<vtkIdTypeArray> domToSkinMapPoints = vtkIdTypeArray::SafeDownCast(splitDomain->boundary->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                            std::map < vtkIdType, vtkIdType> inv;
                            for (vtkIdType i = 0; i < domToSkinMapPoints->GetNumberOfTuples(); i++)
                                inv[domToSkinMapPoints->GetValue(i)] = i;
                            for (vtkIdType i = 0; i < ring->GetNumberOfIds(); i++)
                                ring->SetId(i, inv[ring->GetId(i)]);
                        }
                        else // it's the skin mesh
                            splitDomain = m_domains.begin();

                        std::pair<vtkSmartPointer<vtkPolyData>, vtkSmartPointer<vtkPolyData>> splitted = 
                            vtkSelectionTools::SplitMeshByVertexRing(splitDomain->boundary, ring, useOverlap);
                        if (splitted.first == NULL)
                        {
                            std::stringstream w;
                            w << "Model decomposition \"" << domain.first << "\" is not valid and will be ignored. The decomposition must be made of closed ring of manifold edges.";
                            m_warningLog(w.str());
                            continue;
                        }

                        // if the split domain was not the full skin mesh but some other domain we need to re-index originalPoint/Cell arrays to map to the skin mesh
                        // rather than the subdomain mesh to avoid reccursive mapping - all domains should map to the skin mesh
                        if (m_domains.size() > 1)
                        {
                            vtkSmartPointer<vtkIdTypeArray> domToSkinMapPoints = vtkIdTypeArray::SafeDownCast(splitDomain->boundary->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                            vtkSmartPointer<vtkIdTypeArray> domToSkinMapCells = vtkIdTypeArray::SafeDownCast(splitDomain->boundary->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                            vtkSmartPointer<vtkIdTypeArray> pointMap1 = vtkIdTypeArray::SafeDownCast(splitted.first->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                            vtkSmartPointer<vtkIdTypeArray> cellMap1 = vtkIdTypeArray::SafeDownCast(splitted.first->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                            vtkSmartPointer<vtkIdTypeArray> pointMap2 = vtkIdTypeArray::SafeDownCast(splitted.second->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                            vtkSmartPointer<vtkIdTypeArray> cellMap2 = vtkIdTypeArray::SafeDownCast(splitted.second->GetCellData()->GetArray(ORIGINAL_CELL_IDS));
                            for (vtkIdType i = 0; i < pointMap1->GetNumberOfTuples(); i++)
                            {
                                vtkIdType val = pointMap1->GetValue(i);
                                pointMap1->SetValue(i, val == -1 ? -1 : domToSkinMapPoints->GetValue(val));
                            }
                            for (vtkIdType i = 0; i < pointMap2->GetNumberOfTuples(); i++)
                            {
                                vtkIdType val = pointMap2->GetValue(i);
                                pointMap2->SetValue(i, val == -1 ? -1 : domToSkinMapPoints->GetValue(val));
                            }
                            for (vtkIdType i = 0; i < cellMap1->GetNumberOfTuples(); i++)
                            {
                                vtkIdType val = cellMap1->GetValue(i);
                                cellMap1->SetValue(i, val == -1 ? -1 : domToSkinMapCells->GetValue(val));
                            }
                            for (vtkIdType i = 0; i < cellMap2->GetNumberOfTuples(); i++)
                            {
                                vtkIdType val = cellMap2->GetValue(i);
                                cellMap2->SetValue(i, val == -1 ? -1 : domToSkinMapCells->GetValue(val));
                            }
                        }
                        m_domains.erase(splitDomain); // erase the parent domain - it has been split into two
                        HBMKrigingDomain a, b;
                        a.boundary = splitted.first;
                        b.boundary = splitted.second;
                        m_domains.push_back(a);
                        m_domains.push_back(b);
#if PIPER_DOMAIN_DECOMP_DEBUGVIS
#if PIPER_DOMAIN_DECOMP_DEBUGVISperdomain
                        vtkSmartPointer<vtkRenderer> renderer =
                            vtkSmartPointer<vtkRenderer>::New();
                        vtkSmartPointer<vtkRenderWindow> renderWindow =
                            vtkSmartPointer<vtkRenderWindow>::New();
                        renderWindow->AddRenderer(renderer);

                        renderWindow->SetAlphaBitPlanes(1);
                        renderWindow->SetMultiSamples(0);
                        renderer->SetUseDepthPeeling(1);
                        renderer->SetMaximumNumberOfPeels(10);
                        renderer->SetOcclusionRatio(0.01);

                        // An interactor
                        vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
                            vtkSmartPointer<vtkRenderWindowInteractor>::New();
                        renderWindowInteractor->SetRenderWindow(renderWindow);
                        vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
                        vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                        actor1mapper->SetInputData(splitted.first);
                        actor1->SetMapper(actor1mapper);
                        actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                        actor1->GetProperty()->SetOpacity(0.5);
                        actor1->GetProperty()->EdgeVisibilityOn();
                        renderer->AddActor(actor1);

                        vtkSmartPointer<vtkOpenGLActor> actor2 = vtkSmartPointer<vtkOpenGLActor>::New();
                        vtkSmartPointer<vtkPolyDataMapper> actor2mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                        actor2mapper->SetInputData(splitted.second);
                        actor2->SetMapper(actor2mapper);
                        actor2->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                        actor2->GetProperty()->SetOpacity(0.5);
                        actor2->GetProperty()->EdgeVisibilityOn();
                        renderer->AddActor(actor2);

                        renderWindow->Render();

                        vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
                            vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

                        renderWindowInteractor->SetInteractorStyle(style);

                        // Begin mouse interaction
                        renderWindowInteractor->Start();
#endif
#endif
                    }
                }
            }

#if PIPER_DOMAIN_DECOMP_DEBUGVIS
#if PIPER_DOMAIN_DECOMP_DEBUGVISdomainOverview
            vtkSmartPointer<vtkRenderer> renderer =
                vtkSmartPointer<vtkRenderer>::New();
            vtkSmartPointer<vtkRenderWindow> renderWindow =
                vtkSmartPointer<vtkRenderWindow>::New();
            renderWindow->AddRenderer(renderer);

            renderWindow->SetAlphaBitPlanes(1);
            renderWindow->SetMultiSamples(0);
            renderer->SetUseDepthPeeling(1);
            renderer->SetMaximumNumberOfPeels(10);
            renderer->SetOcclusionRatio(0.01);

            // An interactor
            vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
                vtkSmartPointer<vtkRenderWindowInteractor>::New();
            renderWindowInteractor->SetRenderWindow(renderWindow);

            for (auto &domain : m_domains)
            {
                vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
                vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                actor1mapper->SetInputData(domain.boundary);
                actor1->SetMapper(actor1mapper);
                actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                actor1->GetProperty()->SetOpacity(0.5);
                renderer->AddActor(actor1);
            }
            renderWindow->Render();

            vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
                vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

            renderWindowInteractor->SetInteractorStyle(style);

            // Begin mouse interaction
            renderWindowInteractor->Start();
#endif
#endif
            // now associate each point in the hbm's vtk mesh to one of the domains (unless there is only one).
            if (m_domains.size() > 1)
            {
                std::stringstream info;
                info << "Found decomposition metadata, model's surface was decomposed into " << m_domains.size() << " domains, assigning nodes to domains...";
                m_infoLog(info.str());
                // for more efficient selection of points inside, iteratively discard points that are already assigned to some domain
                vtkSmartPointer<vtkUnstructuredGrid> remainingPoints = sourceModel;
                vtkSmartPointer<vtkIdTypeArray> remPointsToSource = vtkSmartPointer<vtkIdTypeArray>::New(); // map points of the "remainingPoints" set to sourceModel
                remPointsToSource->SetNumberOfComponents(1);
                remPointsToSource->SetNumberOfValues(sourceModel->GetNumberOfPoints());
                for (vtkIdType i = 0; i < remPointsToSource->GetNumberOfTuples(); i++)
                    remPointsToSource->SetValue(i, i); // initialize to map to itself
                bool *isAssigned = new bool[sourceModel->GetNumberOfPoints()];
                int noAssigned = 0;
                std::fill(isAssigned, isAssigned + sourceModel->GetNumberOfPoints(), false);
                for (HBMKrigingDomain &domain : m_domains)
                {
                    vtkSmartPointer<vtkSelectEnclosedPoints> selector = vtkSmartPointer<vtkSelectEnclosedPoints>::New();
                    selector->SetSurfaceData(domain.boundary);
                    selector->SetInputData(remainingPoints);
                    selector->SetTolerance(0);
                    selector->Update();
                    vtkDataArray* insideArray = vtkDataArray::SafeDownCast(selector->GetOutput()->GetPointData()->GetArray("SelectedPoints"));
                    domain.pointsInDomain = vtkSmartPointer<vtkUnstructuredGrid>::New();
                    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
                    vtkSmartPointer<vtkIdTypeArray> origPoints = vtkSmartPointer<vtkIdTypeArray>::New();
                    origPoints->SetName(ORIGINAL_POINT_IDS);
                    origPoints->SetNumberOfComponents(1);
                    vtkSmartPointer<vtkIdTypeArray> domToSkinMapPoints = vtkIdTypeArray::SafeDownCast(domain.boundary->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                    std::map<vtkIdType, vtkIdType> sourceToRemP;
                    for (vtkIdType i = 0; i < remPointsToSource->GetNumberOfTuples(); i++)
                        sourceToRemP[remPointsToSource->GetValue(i)] = i;
                    // make sure all boudnary points are marked as well
                    for (vtkIdType i = 0; i < domain.boundary->GetNumberOfPoints(); i++)
                    {
                        vtkIdType val = domToSkinMapPoints->GetValue(i);
                        if (val >= 0) // ignore the hole-filling point that has -1
                            insideArray->SetComponent(sourceToRemP[inverseNid[skinNid->GetValue(val)]], 0, 1);
                    }
                    vtkSmartPointer<vtkIdTypeArray> remPointsToSourceTemp;
                    vtkSmartPointer<vtkPoints> remPointsArray;
                    if (!useOverlap) // if overlap is not used, we can save a bit of operations if we always discard points that have already been assigned to some domain
                    {
                        remPointsToSourceTemp = vtkSmartPointer<vtkIdTypeArray>::New(); // map points of the "remainingPoints" set to sourceModel
                        remPointsToSourceTemp->SetNumberOfComponents(1);
                        remPointsArray = vtkSmartPointer<vtkPoints>::New();
                    }
                    for (vtkIdType i = 0; i < insideArray->GetNumberOfTuples(); i++)
                    {
                        vtkIdType sourceID = remPointsToSource->GetValue(i);
                        if (insideArray->GetComponent(i, 0) == 1)
                        {
                            points->InsertNextPoint(sourceModel->GetPoint(sourceID));
                            origPoints->InsertNextValue(sourceID);
                            if (!isAssigned[sourceID])
                                noAssigned++;
                            isAssigned[sourceID] = true;
                        }
                        else if (!useOverlap)
                        {
                            remPointsArray->InsertNextPoint(sourceModel->GetPoint(sourceID));
                            remPointsToSourceTemp->InsertNextValue(sourceID);
                        }
                    }
                    if (!useOverlap)
                    {
                        remainingPoints = vtkSmartPointer<vtkUnstructuredGrid>::New();
                        remainingPoints->SetPoints(remPointsArray);
                        remPointsToSource = remPointsToSourceTemp;
                    }

                    domain.pointsInDomain->SetPoints(points);
                    domain.pointsInDomain->GetPointData()->AddArray(origPoints);
#if PIPER_DOMAIN_DECOMP_DEBUGVIS
#if PIPER_DOMAIN_DECOMP_DEBUGVISallPointsInside
                    vtkSmartPointer<vtkRenderer> renderer =
                        vtkSmartPointer<vtkRenderer>::New();
                    vtkSmartPointer<vtkRenderWindow> renderWindow =
                        vtkSmartPointer<vtkRenderWindow>::New();
                    renderWindow->AddRenderer(renderer);

                    renderWindow->SetAlphaBitPlanes(1);
                    renderWindow->SetMultiSamples(0);
                    renderer->SetUseDepthPeeling(1);
                    renderer->SetMaximumNumberOfPeels(10);
                    renderer->SetOcclusionRatio(0.01);

                    // An interactor
                    vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
                        vtkSmartPointer<vtkRenderWindowInteractor>::New();
                    renderWindowInteractor->SetRenderWindow(renderWindow);
                    vtkSmartPointer<vtkOpenGLActor> actor1 = vtkSmartPointer<vtkOpenGLActor>::New();
                    vtkSmartPointer<vtkPolyDataMapper> actor1mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                    vtkSmartPointer<vtkDataSetSurfaceFilter> extractor = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
                    extractor->UseStripsOff();
                    extractor->SetInputDataObject(sourceModel);
                    extractor->Update();

                    actor1mapper->SetInputData(extractor->GetOutput());
                    actor1->SetMapper(actor1mapper);
                    actor1->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                    actor1->GetProperty()->SetOpacity(0.5);
                    renderer->AddActor(actor1);

                    vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
                    sphereSource->SetCenter(0, 0, 0);
                    sphereSource->SetRadius(1);
                    vtkSmartPointer<vtkGlyph3DMapper> pointHighlight = vtkSmartPointer<vtkGlyph3DMapper>::New();
                    pointHighlight->OrientOff();

                    pointHighlight->SetSourceConnection(sphereSource->GetOutputPort());
                    pointHighlight->SetInputData(domain.pointsInDomain);
                    pointHighlight->SetScaleModeToNoDataScaling();

                    vtkSmartPointer<vtkOpenGLActor> pointactor = vtkSmartPointer<vtkOpenGLActor>::New();
                    pointactor->SetMapper(pointHighlight);
                    pointactor->GetProperty()->SetDiffuseColor(vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1), vtkMath::Random(0.4, 1));
                    pointactor->GetProperty()->SetOpacity(1);
                    renderer->AddActor(pointactor);

                    renderWindow->Render();

                    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
                        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New(); //like paraview

                    renderWindowInteractor->SetInteractorStyle(style);

                    // Begin mouse interaction
                    renderWindowInteractor->Start();
#endif
#endif
                }
                // assign points that are not inside any domain to the domain to which they are closest
                if (sourceModel->GetNumberOfPoints() - noAssigned > 0)
                {
                    std::list<vtkSmartPointer<vtkKdTreePointLocator>> locators;
                    for (HBMKrigingDomain &domain : m_domains)
                    {
                        vtkSmartPointer<vtkKdTreePointLocator> locator = vtkSmartPointer<vtkKdTreePointLocator>::New();
                        locator->SetDataSet(domain.boundary);
                        locator->BuildLocator();
                        locators.push_back(locator);
                    }
                    for (vtkIdType i = 0; i < sourceModel->GetNumberOfPoints(); i++)
                    {
                        if (!isAssigned[i])
                        {
                            double distance = VTK_DOUBLE_MAX;
                            HBMKrigingDomain &closestDomain = m_mainDomain;
                            std::list<vtkSmartPointer<vtkKdTreePointLocator>>::iterator locIter = locators.begin();
                            double *curPoint = sourceModel->GetPoint(i);
                            for (HBMKrigingDomain &domain : m_domains)
                            {
                                vtkIdType id = locIter->Get()->FindClosestPoint(curPoint);
                                double canDist = vtkMath::Distance2BetweenPoints(domain.boundary->GetPoint(id), curPoint);
                                if (canDist < distance)
                                {
                                    distance = canDist;
                                    closestDomain = domain;
                                }
                                locIter++;
                            }
                            closestDomain.pointsInDomain->GetPoints()->InsertNextPoint(curPoint);
                            vtkIdTypeArray::SafeDownCast(closestDomain.pointsInDomain->GetPointData()->GetArray(ORIGINAL_POINT_IDS))->InsertNextValue(i);
                        }
                    }
                }
                delete[] isAssigned;
            }
            else
                m_domains.begin()->pointsInDomain = sourceModel; // the whole model is only one domain -> assign all it's points to it

            // create control point locators for domains - used by both nugget computation and decimation
            if (!vtkIdToCPIndex.empty())
            {
                for (HBMKrigingDomain &domain : m_domains)
                {
                    if (m_domains.size() == 1)
                        domain.CPLocatorSource = m_mainDomain.CPLocatorSource;
                    else
                    {
                        vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                        // create a subset of points of the domain points that contain only points that are potential control points
                        vtkSmartPointer<vtkIdTypeArray> domToGlobalCP = vtkSmartPointer<vtkIdTypeArray>::New(); // map indices of the reduced domain pointset (that contain only CPs) directly to the global CP index
                        domToGlobalCP->SetNumberOfComponents(1);
                        vtkSmartPointer<vtkPoints> p = vtkSmartPointer<vtkPoints>::New();
                        vtkSmartPointer<vtkIdTypeArray> origPoints = vtkIdTypeArray::SafeDownCast(domain.pointsInDomain->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                        for (vtkIdType i = 0; i < domain.pointsInDomain->GetNumberOfPoints(); i++)
                        {
                            auto iter = vtkIdToCPIndex.find(origPoints->GetValue(i));
                            if (iter != vtkIdToCPIndex.end()) // this node is a control node (candidate)
                            {
                                p->InsertNextPoint(domain.pointsInDomain->GetPoint(i));
                                domToGlobalCP->InsertNextValue(iter->second);
                            }
                        }
                        cpHolder->SetPoints(p);
                        domToGlobalCP->SetName(ORIGINAL_POINT_IDS);
                        cpHolder->GetPointData()->AddArray(domToGlobalCP);

                        domain.CPLocatorSource = vtkSmartPointer<vtkKdTreePointLocator>::New();
                        domain.CPLocatorSource->SetDataSet(cpHolder);
                        domain.CPLocatorSource->BuildLocator();
                    }
                }
                // build locators on target for the domains
                for (HBMKrigingDomain &domain : m_domains)
                {
                    if (m_domains.size() == 1)
                        domain.CPLocatorTarget = m_mainDomain.CPLocatorTarget;
                    else
                    {
                        vtkSmartPointer<vtkPolyData> cpHolder = vtkSmartPointer<vtkPolyData>::New();
                        // create a subset of points of the domain points that contain only points that are potential control points
                        vtkSmartPointer<vtkPoints> p = vtkSmartPointer<vtkPoints>::New();
                        // utilize the fact that already have the locator for source - it's the same points, just fetch them from the target arrays
                        vtkSmartPointer<vtkIdTypeArray> domToGlobalCP = vtkIdTypeArray::SafeDownCast(domain.CPLocatorSource->GetDataSet()->GetPointData()->GetArray(ORIGINAL_POINT_IDS));
                        for (vtkIdType i = 0; i < domToGlobalCP->GetNumberOfTuples(); i++)
                            p->InsertNextPoint(m_skinAndBonesTargetPoints->GetPoint(domToGlobalCP->GetValue(i)));
                        cpHolder->SetPoints(p);
                        cpHolder->GetPointData()->AddArray(domToGlobalCP);

                        domain.CPLocatorTarget = vtkSmartPointer<vtkKdTreePointLocator>::New();
                        domain.CPLocatorTarget->SetDataSet(cpHolder);
                        domain.CPLocatorTarget->BuildLocator();
                    }
                }
            }
        }
    }
}

