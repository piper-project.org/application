/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef KRIGING_PIPER_INTERFACE
#define KRIGING_PIPER_INTERFACE

#include "krigingdeformation.h"
#include "hbm/VtkSelectionTools.h"

#include <limits>

#include <vtkKdTreePointLocator.h>

#include "hbm/HumanBodyModel.h"

#define PIPER_KRIGING_PROFILING 0 // macro to turn on (1) / off (0) computing and printing of computation times of kriging 

namespace piper
{

    class KrigingPiperInterface {
    public:

        KrigingPiperInterface();
        ~KrigingPiperInterface();

        void testWithRandomValues(int nbCtrlPt, int nbDim);

        void setControlPoints(hbm::VCoord const& piperSrcCtrlPt, hbm::VCoord const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            bool interpolateDisplacement, bool useDrift);

        void setControlPoints(hbm::VCoord const& piperSrcCtrlPt, hbm::VCoord const& piperTargetCtrlPt, double nuggetForAllPoints,
            bool interpolateDisplacement, bool useDrift);

        /// <summary>
        /// Sets the control points for kriging.
        /// </summary>
        /// <param name="model">The model containing the control points to krige.</param>
        /// <param name="piperSrcCtrlPt">IDs of the model's points to be used as source control points.</param>
        /// <param name="piperTargetCtrlPt">IDs of the model's points to be used as target control points. Size (and correspondence) must match the piperSrcCtrlPt size.</param>
        /// <param name="piperCtrlPtWeight">For each control point, nugget to assign to it. Size must match piperSrcCtrlPt size.</param>
        /// <param name="surfaceMeshGeodesic">If specified, geodesic distance of control points on this mesh will be used instead of euclidean distance.
        /// The control points must be points on this mesh. Furthermore, the mesh must contain "nid" pointData array
        /// (vtkIdTypeArray) which for each point of the surfaceMeshGeodesic contains the original piper ID in the FE model. 
        /// If this array is not present, euclidean distance will be used - i.e. this surface mesh will be ignored.</param>
        /// <param name="distanceType">The type of geodesic distance to compute.</param>
        void setControlPoints(hbm::FEModel const& model, hbm::VId const& piperSrcCtrlPt,
            hbm::VId const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            bool interpolateDisplacement, bool useDrift,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic = NULL, SurfaceDistance distanceType = SurfaceDistance::Biharmonic);

        void setControlPoints(hbm::FEModel const& model, hbm::VId const& piperSrcCtrlPt,
            std::vector<hbm::Coord> const& piperTargetCtrlPt, double nuggetForAllPoints,
            bool interpolateDisplacement, bool useDrift,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic = NULL, SurfaceDistance distanceType = SurfaceDistance::Biharmonic);

        void setControlPoints(hbm::FEModel const& model, hbm::VId const& piperSrcCtrlPt,
            std::vector<hbm::Coord> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            bool interpolateDisplacement, bool useDrift,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic = NULL, SurfaceDistance distanceType = SurfaceDistance::Biharmonic);

        void setControlPoints(hbm::VCoord const& piperSrcCtrlPt, hbm::VCoord const& piperTargetCtrlPt, double nuggetForAllPoints,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType);

        void setControlPoints(hbm::VCoord const& piperSrcCtrlPt, hbm::VCoord const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic, SurfaceDistance distanceType);

        /// <summary>
        /// Sets the control points for kriging.
        /// </summary>
        /// <param name="model">The model containing the control points to krige.</param>
        /// <param name="piperSrcCtrlPt">IDs of the model's points to be used as source control points.</param>
        /// <param name="piperTargetCtrlPt">IDs of the model's points to be used as target control points. Size (and correspondence) must match the piperSrcCtrlPt size.</param>
        /// <param name="nuggetForAllPoints">Nugget to assign to all control points.</param>
        /// <param name="surfaceMeshGeodesic">If specified, geodesic distance of control points on this mesh will be used instead of euclidean distance.
        /// The control points must be points on this mesh. Furthermore, the mesh must contain "nid" pointData array
        /// (vtkIdTypeArray) which for each point of the surfaceMeshGeodesic contains the original piper ID in the FE model.
        /// If this array is not present, euclidean distance will be used - i.e. this surface mesh will be ignored.</param>
        /// <param name="distanceType">The type of geodesic distance to compute.</param>
        void setControlPoints(hbm::FEModel const& model, hbm::VId const& piperSrcCtrlPt, hbm::VId const& piperTargetCtrlPt, double nuggetForAllPoints, 
            bool interpolateDisplacement, bool useDrift,
            vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic = NULL, SurfaceDistance distanceType = SurfaceDistance::Biharmonic);


        /// <summary>
        /// Sets the target control points for kriging. This assumes that setControlPoints was called before and source control points
        /// and all other parameters are still valid from that call - only the target points are updated.
        /// This assumes that for setControlPoints, methods with hbm::VCoord-type signature were used.
        /// </summary>
        /// <param name="piperTargetCtrlPt">Coordinates of target control points.</param>
        void setTargetControlPoints(hbm::VCoord& piperTargetCtrlPt);

        /// <summary>
        /// Sets the precision of geodesic distance computation. Only relevant if <c>surfaceMeshGeodesic</c> was specified when setting control points.
        /// </summary>
        /// <param name="geoDistanceEigenPrecision">The precision to use when computing geodesic distance using the vtkSurfaceDistance filter - this is it's
        /// <c>EigenPrecision</c> parameter.</param>
        void SetGeodesicPrecision(double geoDistanceEigenPrecision);

        /// <summary>
        /// Sets the type of geodesic distance. Only relevant if <c>surfaceMeshGeodesic</c> was specified when setting control points.
        /// </summary>
        /// <param name="geoDistanceType">The geodesic distance to use (set through vtkSurfaceDistance::ComputeDistance).</param>
        void SetGeodesicType(SurfaceDistance geoDistanceType);

        void setControlPointsWeight(std::vector<double> const& piperCtrlPtWeight);

        void compute(bool autoRemoveCtrlPtDuplication = false, double minDuplicationDist = 0.0);

        void reCompute();

        void clearSystemMatrix();

        void applyDeformation(hbm::Nodes& srcNodes, hbm::Nodes* outputNodes);
        void applyDeformation(hbm::VCoord& srcNodes, hbm::VCoord* outputNodes);

        /// <summary>
        /// Performs kriging in a box over the specified boxes. The result (i.e. new node positions) is written directly
        /// into the "deformedMesh" - make a copy of it if you want it intact.
        /// This method, unlike applyDeformation, does not use control points set by the setControlPoints - instead, you provide
        /// "candidates" for control points and subset of those points is then used for the kriging based on whether they belong to some box or not.
        /// </summary>
        /// <param name="deformedMesh">The mesh that is being deformed - target positions of control points is taken from the points of this mesh
        /// and upon completion, all points of this mesh are updated based on the transformation.</param>
        /// <param name="referencePoints">Source positions of the points of the mesh to use as a reference.</param>
        /// <param name="controlNodesCandidates">The control nodes candidates. This vector should contain IDs of only those nodes,
        /// which should be considered for control points - for example only nodes of bone surfaces if only those should be used as control points.
        /// Only the subset of these nodes that is also inside the given box will be used as the actual control points.</param>
        /// <param name="fixedNodes">Nodes that will not be krieged. Must have the size equal to the number of nodes in the mesh, have false for nodes
        /// that are to be krieged and true for nodes that should be fixed.</param>
        /// <param name="ctrlPtWeight">The nuggets for ALL the points in the mesh. Of course, only those that are used as control points will be used,
        /// but it is not know beforehand exactly what points will end up being used (it can change based on the chosen "spliting policy" of the boxes),
        /// so define them for all the points.</param>
        /// <param name="OBBNodes">A set of oriented bounding boxes inside which to do the transformation using kriging.</param>
        /// <param name="splitBoxThresholdCP">Maximum amount of control points per box (box will be split if it has more).</param>
        /// <param name="splitBoxOverlap">Percentage of the box that will be moved towards the other box when the box are split,
        /// making them overlap. This is done for both boxes, so e.g. overlap = 0.1 will cause box A to shift 10% of its size towards box B,
        /// but also box B towards box A, resulting in 20% of the volume being shared => overlap 0.5 will result in a 100% overlap.
        /// If the boxes are overlapping, resulting positiongs for points inside the overlap are averaged.</param>
        /// <param name="interpolateDisplacement">If set to <c>true</c>, displacement will be used as the quality to interpolate, otherwise position.</param>
        /// <param name="useDrift">If set to <c>true</c>, drift will be used during Kriging.</param>
        /// <param name="boxesBuiltOverSource">Set this based on what points - <c>deformedMesh->GetPoints()</c> or <c>referencePoints</c> you used
        /// for constructing the OBBNodes: if set to <c>true</c>, all selection querries will use referencePoints,
        /// i.e. assume that the OBBNodes were built based on those. Otherwise selection querries will be done on the deformedMesh's points.</param>
        /// <param name="refMesh">Used only for debugging - can safely remain NULL in release code.</param>
        void applyDeformationInABox(vtkUnstructuredGrid *deformedMesh, vtkSmartPointer<vtkPoints> referencePoints,
            hbm::VId &controlNodesCandidates, std::vector<bool> &fixedNodes, boost::container::vector<double> &ctrlPtWeight,
            boost::container::vector<vtkOBBNodePtr> &OBBNodes,
            unsigned int splitBoxThresholdCP, double splitBoxOverlap, bool interpolateDisplacement, bool useDrift, bool boxesBuiltOverSource,
            vtkUnstructuredGrid *refMesh = NULL);

        double getRelativeError(hbm::Nodes& nodesToCompareWith);

        double getReprojectionRelativeError();

        static const double krigingDuplPointLimit;

#if PIPER_KRIGING_PROFILING
        time_t krigMatrixTime;
        time_t krigMatrixPreprocessTime;
        time_t krigDeformTime;
#endif

    private:


        /// <summary>
        /// Checks if the mesh has necessarry data (original point IDs), then creates array marking which points of the mesh are control points and stores the mesh.
        /// The method also reorders the input arrays (source and target control points + weights) according to the order in which they are stored in the mesh.
        /// </summary>
        /// <param name="model">The FE mesh that has all the points in piperSrcCtrlPt.</param>
        /// <param name="surfaceMeshGeodesic">The input geodesic mesh.</param>
        /// <param name="piperSrcCtrlPt">The IDs of the source control points in the original FE mesh.</param>
        /// <param name="piperTargetCtrlPt">IDs of the model's points to be used as target control points. Size (and correspondence) must match the piperSrcCtrlPt size.</param>
        /// <param name="piperCtrlPtWeight">For each control point, nugget to assign to it. Size must match piperSrcCtrlPt size.</param>
        /// <param name="reorderedSrcCtrlPt">Upon finishing, this will contain the source control points in the order they are stored in the mesh.</param>
        /// <param name="reorderedTargetCtrlPt">Upon finishing, this will contain the target control points in the order they are stored in the mesh.</param>
        /// <param name="reorderedCtrlPtWeight">Upon finishing, this will contain the nuggets for control points in the order they are stored in the mesh.</param>
        template <typename T>
        void preprocessGeodesicMesh(hbm::FEModel const& model, vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic,
            hbm::VId const& piperSrcCtrlPt, std::vector<T> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            hbm::VId *reorderedSrcCtrlPt, std::vector<T> *reorderedTargetCtrlPt, std::vector<double> *reorderedCtrlPtWeight);

        void preprocessGeodesicMesh(vtkSmartPointer<vtkPolyData> surfaceMeshGeodesic,
            std::vector<hbm::Coord> const& piperSrcCtrlPt, std::vector<hbm::Coord> const& piperTargetCtrlPt, std::vector<double> const& piperCtrlPtWeight,
            std::vector<hbm::Coord> *reorderedSrcCtrlPt, std::vector<hbm::Coord> *reorderedTargetCtrlPt, std::vector<double> *reorderedCtrlPtWeight);



        /// <summary>
        /// Performs kriging on selected vertices of the targetMesh.
        /// The targetMeshPoints will be transformed by kriging.
        /// </summary>
        /// <param name="originalMeshPoints">The source positions of the points of the original mesh.</param>
        /// <param name="targetMeshPoints">The target positions for the points.</param>
        /// <param name="resultCummulative">Positions of points that are modified (all that are marked in selectedIDs) will be ADDED (accumulated) to this array.</param>
        /// <param name="selectedIDs">An array with value for each point
        /// denoting to which points kriging should be applied: 1 to select, 0 for those that should be left alone.</param>
        /// <param name="controlIDs">An array with value for each point
        /// denoting which points to use as control points: 1 for control points, 0 for all else.</param>
        /// <param name="ctrlPtWeight">The weights for all points in the mesh. 
        /// Weights relevant only for the subset that is selected will be extracted inside this method, so provide the full weight array.</param>
        /// <param name="interpolateDisplacement">If set to <c>true</c>, displacement will be used as the quality to interpolate, otherwise position.</param>
        /// <param name="useDrift">If set to <c>true</c>, drift will be used during Kriging.</param>
        /// <returns>True if kriging was performed, false if it was not (due to low amount of nodes / control points).</returns>
        bool applyDeformationSelectedOnly(vtkSmartPointer<vtkPoints> originalMeshPoints,
            vtkSmartPointer<vtkPoints> targetMeshPoints, vtkSmartPointer<vtkPoints> resultCummulative, vtkSmartPointer<vtkBitArray> selectedIDs,
            vtkSmartPointer<vtkBitArray> controlIDs, boost::container::vector<double> ctrlPtWeight, bool interpolateDisplacement, bool useDrift);

        
        /// <summary>
        /// Reccursive subrutine used by kriging in a box to extract both control points and regular points-to-deform on the boundary of two boxes.*
        /// It starts from one corner of the border-face of the two boxes and finds N closest neighbors control points. All points-to-deform
        /// in the sphere wrapping all those points (i.e. radius = the distance of the corner point and the farthest among the N neighbors) are added.
        /// Then the square carved by this sphere on the face (edge size = sqrt(radius)) is considered "covered", the remainder of the face is split
        /// into three quadrilaterals and processed reccursively in the same way until the whole original face is covered.
        /// </summary>
        /// <param name="cpLocator">Point locator of control points of the neighboring box.</param>
        /// <param name="paramNClosestPoints">How many closest points should be considered.</param>
        /// <param name="face">A [4][3] array with coordinates of the four corners of the quadrilateral face that is the border of the two boxes.</param>
        /// <param name="addCP">In the end will contain CPs that are meaningful for smooth interpolation between the boundaries of the two boxes. 
        /// Contains no duplicities. The list must be initialized outside this call (due to reccursive nature of this method, it can't be done inside)!</param>
        void extractFaceNeighborhood(vtkSmartPointer<vtkKdTreePointLocator> cpLocator,
            int paramNClosestPoints, double (&face)[4][3],
            vtkSmartPointer<vtkIdList> addCP);

        std::shared_ptr<Eigen::MatrixXd> srcCtrlPt;
        std::shared_ptr<Eigen::MatrixXd> destCtrlPt;
        std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight;
        std::shared_ptr<Eigen::MatrixXd> srcMesh;
        std::shared_ptr<Eigen::MatrixXd> destMesh;
        std::unique_ptr<KrigingDeformation> krigingDeformer;
        // geodesic kriging
        vtkSmartPointer<vtkPolyData> _surfaceGeodesicMesh; // used when geodesic distance is supposed to be used for the kriging
        std::map<vtkIdType, size_t> srcCtrlMap; // for i-th chosen point in the mesh returns its index in the piperCtrlPtSrc array
    };
}

#endif //KRIGING_PIPER_INTERFACE
