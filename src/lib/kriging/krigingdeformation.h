/*******************************************************************************
* Copyright (C) 2017 UCBL-Ifsttar                                              *
* This file is part of the PIPER Framework.                                    *
* Version: 1.0.0                                                               *
*                                                                              *
* The PIPER Framework is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by the     *
* Free Software Foundation, either version 2 of the License, or (at your       *
* option) any later version.                                                   *
*                                                                              *
* The PIPER Framework is distributed in the hope that it will be useful, but   *
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY   *
* or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License     *
* for more details. You should have received a copy of the GNU General Public  *
* License along with the PIPER Framework.                                      *
* If not, see <http://www.gnu.org/licenses/>.                                  *
*                                                                              *
* Contributors include Tomas Janak, Thomas Dupeux (UCBL-Ifsttar)               *
*                                                                              *
* This work has received funding from the European Union Seventh Framework     *
* Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).     *
*                                                                              *
*******************************************************************************/
#ifndef KRIGING_DEFORMATION
#define KRIGING_DEFORMATION

#include <Eigen/Dense>
#include <set>

#include <vtkPIPERFilters/vtkSurfaceDistance.h>

class KrigingDeformation {
public :
    enum class KernelType { euclidian, geodesic } ;
    enum class SolverType { partialPivLU , fullPivLU, sparseLU } ;
  //  enum class MatrixType { dense, sparse } ;

    KrigingDeformation(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt, bool interpolateDisplacement, bool useDrift);
    KrigingDeformation (std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt, 
        std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight, bool interpolateDisplacement, bool useDrift);
    
    /// <summary>
    /// Initializes a new instance of the <see cref="KrigingDeformation"/> class.
    /// </summary>
    /// <param name="srcCtrlPt">Coordinates of the source control points.</param>
    /// <param name="destCtrlPt">Coordinates of the target control points - size and order must match the source points.</param>
    /// <param name="ctrlPtWeight">The nuggets for control points - size and order must match teh source points.</param>
    /// <param name="geodesicSurface">The surface to use for computing the geodesic distance. The order of control points must match the order
    /// of the points in this mesh. If the _GEODESIC_CHOSENPOINTS pointData vtkBitArray is specified, only the marked points are taken into account -
    /// the order must still match after leaving out the unmarked points! If it is not specified, all points will be used. The number of (marked)
    /// points in the mesh must match the number of source control points.</param>
    /// <param name="distanceType">The type of geodesic distance to compute.</param>
    KrigingDeformation(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt,
        std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight, vtkSmartPointer<vtkPolyData> geodesicSurface, SurfaceDistance distanceType);

    /// <summary>
    /// Initializes a new instance of the <see cref="KrigingDeformation"/> class.
    /// </summary>
    /// <param name="srcCtrlPt">Coordinates of the source control points.</param>
    /// <param name="destCtrlPt">Coordinates of the target control points - size and order must match the source points.</param>
    /// <param name="geodesicSurface">The surface to use for computing the geodesic distance. The order of control points must match the order
    /// of the points in this mesh. If the _GEODESIC_CHOSENPOINTS pointData vtkBitArray is specified, only the marked points are taken into account -
    /// the order must still match after leaving out the unmarked points! If it is not specified, all points will be used. The number of (marked)
    /// points in the mesh must match the number of source control points.</param>
    /// <param name="distanceType">The type of geodesic distance to compute.</param>
    KrigingDeformation(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt,
        vtkSmartPointer<vtkPolyData> geodesicSurface, SurfaceDistance distanceType);
    //~KrigingDeformation()

    /// <summary>
    /// Sets the precision of geodesic distance computation. Only relevant if one of the constructors <c>accepting geodesicSurface</c> was used.
    /// </summary>
    /// <param name="geoDistanceEigenPrecision">The precision to use when computing geodesic distance using the vtkSurfaceDistance filter - this is it's
    /// <c>EigenPrecision</c> parameter.</param>
    void SetGeodesicPrecision(double geoDistanceEigenPrecision);

    /// <summary>
    /// Sets the type of geodesic distance. Only relevant if one of the constructors <c>accepting geodesicSurface</c> was used.
    /// </summary>
    /// <param name="geoDistanceType">The geodesic distance to use (set through vtkSurfaceDistance::ComputeDistance).</param>
    void SetGeodesicType(SurfaceDistance geoDistanceType);

    // resets the systems without having to call a new constructor
    bool FillSystemMatrixDense();
    bool FillSystemMatrixDense(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt);
    bool FillSystemMatrixDense(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt, std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight);
    bool FillSystemMatrixDense(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt, vtkSmartPointer<vtkPolyData> geodesicSurface);
    bool FillSystemMatrixDense(std::shared_ptr<Eigen::MatrixXd> srcCtrlPt, std::shared_ptr<Eigen::MatrixXd> destCtrlPt,
        std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight, vtkSmartPointer<vtkPolyData> geodesicSurface);

    bool SolveSystemMatrixDense();
    void ClearSystemMatrix();
    //FillSystemMatrixSparse()
    //FillSystemMatrixSparse(const Ref<const MatrixXd>& srcCtrlPt, const Ref<const MatrixXd>& destCtrlPt)

    bool ApplyDeformationDense(std::shared_ptr<Eigen::MatrixXd> srcMesh, std::shared_ptr<Eigen::MatrixXd> destMesh, int srcOffset, int destOffset, int subsetSize);
    bool ApplyDeformationDense(std::shared_ptr<Eigen::MatrixXd> srcMesh, std::shared_ptr<Eigen::MatrixXd> destMesh);
    bool ApplyDeformationDense(std::shared_ptr<Eigen::MatrixXd> meshIO);
    //ApplyDeformationSparse()

    //SetNugget()
    void SetKernelType(KernelType kernelType);
    KernelType GetKernelType();

    void SetSolverType(SolverType solverType);
    SolverType GetSolverType();

    void setCtrlPtWeight(std::shared_ptr<Eigen::ArrayXd> ctrlPtWeight);
    std::shared_ptr<const Eigen::ArrayXd> getCtrlPtWeight();

    void SetAutoRemoveDupl(bool autoremove) { this->autoRemoveCtrlPtDuplicates = autoremove; }
    void SetMinDuplDistance(double mindistance) { this->minDuplicationDist = mindistance; }

//    void SetMatrixType(MatrixType matrixType);
 //   MatrixType GetMatrixType();

    std::shared_ptr<const Eigen::MatrixXd> GetSystemMatrix();
    std::shared_ptr<const Eigen::MatrixXd> GetSolutionMatrix();
protected:
    bool initCtrlPt();
    bool verifyCtrlPtAreDistinct();
    bool autoRemoveIdenticalCtrlPt();
    void initSystem();
    bool computeCorrelationMatrix();    
    /// <summary>
    /// Computes the geodesic distance correlation matrix. 
    /// </summary>
    /// <returns></returns>
    bool computeGeodesicDistCorrelationMatrix();
    void fillSystemWithCtrlPt();

    void computeEuclidianDistCorrelationMatrix();

    //TODOThomasD : selon la methode de mise a jour des noeuds, le mesh est ou pas un matrixXd ou un node de hbm
    //TODOThomasD : reflechir egalement si les ctrlpt sont des matrixXd ou des node hbm (copie memoire ou pas vs question d'alignement)
    std::shared_ptr<Eigen::MatrixXd>  _destCtrlPt, _srcCtrlPt;
    std::shared_ptr<Eigen::MatrixXd>  _systemMatEuclid, _systemMatGeodesic, _solutionMat, _deformationGeodesicMat; //TODOThomasD : voir comment eviter d'avoir plusieurs copie memoire du meme jeu de points (eigen mat * 2 pour ctrlPt, hbm..)
    std::shared_ptr<Eigen::ArrayXd> _ctrlPtWeightArray;
    Eigen::MatrixXd _systDestCtrlPt;
    // SparseMatrix<double> systemMatSparse;
    // SparseLU<SparseMatrix<double> > solverSparseLU;

    std::set<unsigned int> _identicalCtrlPtId;
    //enum class kernel
    std::size_t _nbCtrlPt, _nbDim, _nbHomogeneousDim;
    bool autoRemoveCtrlPtDuplicates = false,
        _interpolateDisplacement = true, // if set to true, the interpolated quality of the points is displacement, if set to false, it is position
        _useDrift = false; // if set to true, the method is dual kriging, i.e. the transformation has the drift part, if set to false, it does not. 
                           // setting this to false sets interpolateDisplacement to true; using geodesic distance also makes this false and _interpolateDisplacement true
    double minDuplicationDist = 0.0;
    vtkSmartPointer<vtkSurfaceDistance> _geoDist; // filter used to compute the correlation and deformation matrices if geodesic distance is being used
    unsigned long _lastGeoCorrelationTime = 0, _lastGeoDeformationTime = 0; // the MTime of the _srcSurface when it was last used to compute the correlation matrix
    vtkSmartPointer<vtkPolyData> _srcSurface; // surface to compute the geodesic distance

    enum class SolverSystemState {
        initializedNotSolved,
        systemSolved,
        systemCleared
    } _solverState;

    KernelType _kernelType;
    SolverType _solverType;
};

#endif
