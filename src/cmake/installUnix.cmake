#
# cmake script to package the piper application for Linux
#

include(${CMAKE_SOURCE_DIR}/cmake/installCommon.cmake)

list(APPEND PIPER_QT_PLUGINS xcbglintegrations)

#OPTION(PIPER_INSTALL_SYSTEM_DEPENDENCIES "Copy to the installation directory system dependencies" OFF)
set(PIPER_INSTALL_SYSTEM_DEPENDENCIES true)

#
# add necessary install commands
#

# install xmllint
find_package(LibXml2)
message(STATUS "Found xmllint: ${LIBXML2_XMLLINT_EXECUTABLE}")
file(GLOB LIBXML2_LIBRARIES_FILES ${LIBXML2_LIBRARIES}*) # only a single lib
install(FILES ${LIBXML2_LIBRARIES_FILES} DESTINATION lib)
install(PROGRAMS ${LIBXML2_XMLLINT_EXECUTABLE} DESTINATION bin)
# but xmllint depends on libreadline which depends on...

# install QT along with the piper application
# libs
foreach(qt_lib_i ${PIPER_QT_LIBS})
    file(GLOB qt_lib_i_files ${PIPER_QT_INSTALL_PREFIX}/lib/${qt_lib_i}.so*)
    install(FILES ${qt_lib_i_files} DESTINATION lib)
endforeach()

# plugins
foreach(qt_plugin_i ${PIPER_QT_PLUGINS})
    install(DIRECTORY ${PIPER_QT_INSTALL_PREFIX}/plugins/${qt_plugin_i} DESTINATION bin)
endforeach()

# qml modules
foreach(qt_qml_module_i ${PIPER_QT_QML_MODULES})
    install(DIRECTORY ${PIPER_QT_INSTALL_PREFIX}/qml/${qt_qml_module_i} DESTINATION bin)
endforeach()

# vtk
find_package(VTK 7.0 REQUIRED)
include(${VTK_USE_FILE})
set(VTK_ROOT ${VTK_DIR}/../../..)
foreach(vtk_lib_i ${PIPER_VTK_LIBS})
    file(GLOB vtk_lib_i_files ${VTK_ROOT}/lib/${vtk_lib_i}*)
    install(FILES ${vtk_lib_i_files} DESTINATION lib)
endforeach()

# mesquite
find_package(Mesquite REQUIRED)
foreach(mesquite_lib_i ${PIPER_MESQUITE_LIBS})
    file(GLOB mesquite_lib_i_files ${Mesquite_LIBRARY_DIRS}/${mesquite_lib_i}*)
    install(FILES ${mesquite_lib_i_files} DESTINATION lib)
endforeach()

# sofa
install(DIRECTORY "${SOFA_ROOT}/lib" DESTINATION .)
install(DIRECTORY "${SOFA_ROOT}/etc" DESTINATION .)
#install(DIRECTORY "${SOFA_ROOT}/share/sofa/shaders" DESTINATION "share/sofa")


# install system dependencies along with the piper application
if(${PIPER_INSTALL_SYSTEM_DEPENDENCIES})
    # python
# python is a mess to package, skip it, anyway, it is so standard...
#    file(GLOB PIPER_PYTHON_INSTALL_FILES ${PYTHON_LIBRARIES}*)
#    install(FILES ${PIPER_PYTHON_INSTALL_FILES} DESTINATION lib)

    # numpy
    # used python should be the path
#    execute_process (COMMAND python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"
#                     OUTPUT_VARIABLE PYTHON_SITE_PACKAGE
#                     OUTPUT_STRIP_TRAILING_WHITESPACE )
#    message(STATUS "Found python site package ${PYTHON_SITE_PACKAGE}")
#    install(DIRECTORY ${PYTHON_SITE_PACKAGE}/numpy DESTINATION lib/python2.7/site-packages)

#    # numpy depends on BLAS and LAPACK
#    find_package(BLAS REQUIRED)
#    message("BLAS: ${BLAS_LIBRARIES}")

#    find_package(LAPACK REQUIRED)
#    message("LAPACK: ${LAPACK_LIBRARIES}")

    # boost
    find_package(Boost COMPONENTS system filesystem thread date_time chrono locale REQUIRED)
    foreach(boost_lib_i ${Boost_LIBRARIES})
        file(GLOB boost_lib_i_files ${boost_lib_i}*)
        install(FILES ${boost_lib_i_files} DESTINATION lib)
    endforeach()

    # extra libs
    # TODO this way we get all library versions, whereas only ne is used
    find_library(EXTRA_LIB_icuuc libicuuc.so)
    find_library(EXTRA_LIB_icui18n libicui18n.so)
    find_library(EXTRA_LIB_icudata libicudata.so)
    set(EXTRA_LIBS ${EXTRA_LIB_icuuc} ${EXTRA_LIB_icui18n} ${EXTRA_LIB_icudata})
    foreach(lib_i ${EXTRA_LIBS})
        file(GLOB lib_i_files ${lib_i}*)
        install(FILES ${lib_i_files} DESTINATION lib)
    endforeach()

endif()

#
# application packaging using cpack
#

set(CPACK_GENERATOR "STGZ")

#set(CPACK_GENERATOR "DEB")
#SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Piper team")

include(CPack)
