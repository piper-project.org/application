#
# common installation parameters
#

# list required Qt components
set(PIPER_QT_LIBS libicudata libicui18n libicuuc libQt5XcbQpa libQt5Core libQt5Gui libQt5Qml libQt5Quick libQt5Widgets libQt5Concurrent libQt5DBus libQt5Sql libQt5Network libQt5OpenGL)
set(PIPER_QT_PLUGINS platforms xcbglintegrations sqldrivers)
set(PIPER_QT_QML_MODULES Qt QtQuick "QtQuick.2" QtQml QtGraphicalEffects)

# list of required vtk libraries
set(PIPER_VTK_LIBS libvtkCommonDataModel libvtkCommonCore libvtkIOXML libvtkCommonMath libvtkCommonMisc libvtkCommonSystem libvtkCommonTransforms libvtksys libvtkIOXMLParser libvtkIOCore libvtkCommonExecutionModel libvtkexpat libvtkzlib libvtkFiltersCore libvtkFiltersExtraction libvtkFiltersGeneral libvtkFiltersSources libvtkFiltersVerdict libvtkverdict libvtkFiltersStatistics libvtkFiltersGeometry libvtkRenderingCore libvtkRenderingFreeType  libvtkfreetype libvtkftgl libvtkImagingHybrid libvtkInteractionStyle libvtkGUISupportQt libvtkRenderingVolume libvtkRenderingOpenGL libvtkRenderingVolumeOpenGL2 libvtkRenderingAnnotation libvtkCommonComputationalGeometry libvtkCommonColor libvtkIOImage libvtkImagingFourier libvtkglew libvtkalglib libvtkjpeg libvtkpng libvtktiff libvtkmetaio libvtkDICOMParser libvtkImagingCore)

# list of required mesquite libraries
set(PIPER_MESQUITE_LIBS libmesquite libmsqutil)

# list of required sofa libraries
# set(PIPER_SOFA_LIBS) # TODO

install(FILES ${PIPER_SHARE_DIR}/../README.md DESTINATION .)
install(FILES ${PIPER_SHARE_DIR}/doc/INSTALL.md DESTINATION .)
install(FILES ${PIPER_SHARE_DIR}/doc/ACKNOWLEDGMENT.md DESTINATION .)
install(FILES ${PIPER_SHARE_DIR}/../CHANGELOG.md DESTINATION .)
install(FILES ${PIPER_SHARE_DIR}/../LICENSE.md DESTINATION .)
install(DIRECTORY ${PIPER_SHARE_DIR}/example/scripting DESTINATION share/example)

#
# application packaging using cpack
#

SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "PIPER application")
SET(CPACK_PACKAGE_VENDOR "PIPER team")
SET(CPACK_PACKAGE_VERSION_MAJOR ${Piper_VERSION_MAJOR})
SET(CPACK_PACKAGE_VERSION_MINOR ${Piper_VERSION_MINOR})
SET(CPACK_PACKAGE_VERSION_PATCH ${Piper_VERSION_PATCH})
SET(CPACK_OUTPUT_FILE_PREFIX ${Piper_BINARY_DIR})