#
# common cmake option for piper project
#

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

# Set the output directories globally
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Enable the organisation in folders for generators that support
# it. (E.g. some versions of Visual Studio have 'solution folders')
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

# library headers
include_directories(${PIPER_SOURCE_DIR}/lib)

# C++11 for unix
if(NOT MSVC)
    add_definitions(-std=c++11) # TODO update to modern cmake
endif()

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

# activate parallel build by default for all projects for MSVC
IF(MSVC)
   SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP") 
ENDIF(MSVC)

# RPATH for unix, used at install time to set rpath of executable
if(UNIX)
    # use, i.e. don't skip the full RPATH for the build tree
    SET(CMAKE_SKIP_BUILD_RPATH  FALSE)

    # when building, don't use the install RPATH already
    # (but later on when installing)
    SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

    SET(CMAKE_INSTALL_RPATH "\$ORIGIN/../lib")

    # add the automatically determined parts of the RPATH
    # which point to directories outside the build tree to the install RPATH
    SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH FALSE)


#    # the RPATH to be used when installing, but only if it's not a system directory
#    LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
#    IF("${isSystemDir}" STREQUAL "-1")
#       SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
#    ENDIF("${isSystemDir}" STREQUAL "-1")
endif()

# testing
if(NOT TARGET gtest)
    add_subdirectory(${PIPER_SOURCE_DIR}/extlibs/gtest gtest)
endif()
include_directories(${PIPER_SOURCE_DIR}/extlibs/gtest/include)
enable_testing()
set(GTEST_OUTPUT "xml:${CMAKE_BINARY_DIR}/test-reports/")

find_package(PythonLibs REQUIRED)
find_package(SWIG 3 REQUIRED)
if(SWIG_FOUND AND PYTHONLIBS_FOUND)
    set(CMAKE_SWIG_FLAGS "-I${PIPER_SOURCE_DIR}/swig")
    message(STATUS "Add piper python package")
    if(MSVC_IDE)
        set(CMAKE_SWIG_OUTDIR ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/lib/python2.7/site-packages/piper)
    else(MSVC_IDE)
        set(CMAKE_SWIG_OUTDIR ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/python2.7/site-packages/piper)
    endif(MSVC_IDE)
    file(COPY ${PIPER_SOURCE_DIR}/__init__.py DESTINATION ${CMAKE_SWIG_OUTDIR})
    install(FILES ${PIPER_SOURCE_DIR}/__init__.py DESTINATION lib/python2.7/site-packages/piper)
endif()

if(MSVC)
    # find_program(SED_EXECUTABLE NAMES sed PATH "${PIPER_SOURCE_DIR}/../extsoft/sed-4.2.1-bin/bin/" DOC "Path to sed executable") # should work...
    set(SED_EXECUTABLE "${PIPER_SOURCE_DIR}/../extsoft/sed-4.2.1-bin/bin/sed.exe")
endif()

# trick to find Qt5 install prefix ## is it fine on windows ?
find_package(Qt5 COMPONENTS Core)
get_target_property(PIPER_QT_INSTALL_PREFIX Qt5::Core LOCATION)
get_filename_component(PIPER_QT_INSTALL_PREFIX ${PIPER_QT_INSTALL_PREFIX} DIRECTORY)
get_filename_component(PIPER_QT_INSTALL_PREFIX "${PIPER_QT_INSTALL_PREFIX}/.." ABSOLUTE)
message(STATUS "Found Qt5 install prefix: ${PIPER_QT_INSTALL_PREFIX}")
