#
# cmake script to package the piper application for Windows
#

include(${CMAKE_SOURCE_DIR}/cmake/installCommon.cmake)

# vtk
#find_package(VTK 6.2 REQUIRED)
#include(${VTK_USE_FILE})
#install(DIRECTORY "${VTK_DIR}/bin" DESTINATION .)
#install(DIRECTORY "${VTK_DIR}/lib" DESTINATION .)

# sofa
install(DIRECTORY "${SOFA_ROOT}/bin" DESTINATION .)
install(DIRECTORY "${SOFA_ROOT}/lib" DESTINATION .)
install(DIRECTORY "${SOFA_ROOT}/etc" DESTINATION .)

# examples
install(DIRECTORY "${CMAKE_SOURCE_DIR}/../share/example" DESTINATION "${CMAKE_INSTALL_PREFIX}/share/")

option(PIPER_BUNDLEINSTALL "Use bundle install to resolve dll dependencies when installing PIPER application" ON)

#copying required external dlls as side-by-side for the application in the install package
IF(PIPER_BUNDLEINSTALL)
    FILE(TO_CMAKE_PATH $ENV{SYSTEMROOT} systemRoot)
    #install msvc runtime libraries
    set(CMAKE_INSTALL_OPENMP_LIBRARIES ON)
    include(InstallRequiredSystemLibraries)
    #install MSVC12 runtime libraries when not compiling with MSVC12 in case specific lib are linked against these (for instance qt 5.4.0 x64 not compiled from source) 
    IF(NOT MSVC12 AND EXISTS "$ENV{SYSTEMROOT}/system32/msvcp120.dll")
        IF(CMAKE_CL_64 AND EXISTS ${systemRoot}/Sysnative/msvcp120.dll) #TODOThomasD : make sure CMAKE_CL_64 is unset when compiling 32 bit target otherwise check sthg like  if(CMAKE_SIZEOF_VOID_P EQUAL 8)
            INSTALL(PROGRAMS ${systemRoot}/Sysnative/msvcp120.dll ${systemRoot}/Sysnative/msvcr120.dll DESTINATION bin COMPONENT Runtime) #needed to avoid File System Redirector WOW64 to redirect to 32bit version of the dlls when run from 32bit cmake app
        ELSE()
            INSTALL(PROGRAMS ${systemRoot}/System32/msvcp120.dll ${systemRoot}/System32/msvcr120.dll DESTINATION bin COMPONENT Runtime)
        ENDIF()
    endif()
    
    SET(APPS "\${CMAKE_INSTALL_PREFIX}/bin/piper.exe")
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/qml/QtQuick.2" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/plugins/platforms" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/qml/QtQuick" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/qml/Qt" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/qml/QtGraphicalEffects" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/qml/QtQml" DESTINATION bin COMPONENT Runtime)
    INSTALL(DIRECTORY "${_qt5Core_install_prefix}/plugins/sqldrivers" DESTINATION bin COMPONENT Runtime)
#     INSTALL(DIRECTORY "${SOFA_ROOT}/share/sofa/shaders/orderIndependentTransparency" DESTINATION "${CMAKE_INSTALL_PREFIX}/share/sofa/shaders/" COMPONENT Runtime)
    INSTALL(DIRECTORY "${PYTHON_DIR}" DESTINATION ${CMAKE_INSTALL_PREFIX} COMPONENT Runtime)      
    if (INSTALL_OCTAVE)
        if (EXISTS ${OCTAVE_ROOT_DIR})
            INSTALL(DIRECTORY "${OCTAVE_ROOT_DIR}/bin" DESTINATION "${CMAKE_INSTALL_PREFIX}/Octave/" COMPONENT Runtime)
            INSTALL(DIRECTORY "${OCTAVE_ROOT_DIR}/lib/octave" DESTINATION "${CMAKE_INSTALL_PREFIX}/Octave/lib/" COMPONENT Runtime)
            INSTALL(DIRECTORY "${OCTAVE_ROOT_DIR}/share/octave" DESTINATION "${CMAKE_INSTALL_PREFIX}/Octave/share/" COMPONENT Runtime)
            INSTALL(PROGRAMS  "${CMAKE_SOURCE_DIR}/../script/octave_setPath/octavePathSetter.exe" DESTINATION "${CMAKE_INSTALL_PREFIX}/Octave/" COMPONENT Runtime)
            INSTALL(FILES  "${CMAKE_SOURCE_DIR}/../script/octave_setPath/template_octave_packages" DESTINATION "${CMAKE_INSTALL_PREFIX}/Octave/share/octave/" COMPONENT Runtime)
        else ()
            message(WARNING "OCTAVE_ROOT_DIR not found, Octave will not be installed!")
        endif()
    ENDIF()

    INSTALL(PROGRAMS "${CMAKE_SOURCE_DIR}/../script/RunPiper_windows.bat" DESTINATION ${CMAKE_INSTALL_PREFIX} COMPONENT Runtime RENAME "RunPiper.bat")
    
    IF(EXISTS ${systemRoot}/System32/python27.dll)
        INSTALL(PROGRAMS ${systemRoot}/System32/python27.dll DESTINATION bin COMPONENT Runtime)
    elseif(EXISTS ${systemRoot}/Sysnative/python27.dll)
        INSTALL(PROGRAMS ${systemRoot}/Sysnative/python27.dll DESTINATION bin COMPONENT Runtime) #needed if python64bit is installed for all users AND to avoid File System Redirector WOW64 to redirect to 32bit path windows/SysWOW64 version of the dlls when run from 32bit cmake app
    ENDIF()
    
    # directories to look for dependencies
   SET(DIRS  ${PYTHON_DIR} ${PYTHON_DIR}/DLLs ${SOFA_ROOT}/bin ${OCTAVE_ROOT_DIR}/bin ${QT_LIBRARY_DIRS} ${QT_BINARY_DIR} ${_qt5Core_install_prefix}/bin ${Boost_LIBRARY_DIRS} ${VTK_INSTALL_PREFIX}/bin ${Mesquite_BINARY_DIR})
   if(MSVC_IDE)
       list(APPEND DIRS ${Piper_BINARY_DIR}/bin/\${BUILD_TYPE})
   else()
       list(APPEND DIRS ${Piper_BINARY_DIR}/bin)
   endif()

    # looking for dependencies and installing files
    INSTALL(CODE "
        file(GLOB_RECURSE INSTALLED_PLUGINGS
        \"\${CMAKE_INSTALL_PREFIX}/bin/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
        include(BundleUtilities)
        fixup_bundle(\"${APPS}\" \"\${INSTALLED_PLUGINGS}\" \"${DIRS}\")
        " COMPONENT Runtime)
ENDIF()

#
# application packaging using cpack
#

set(CPACK_GENERATOR "ZIP")
include(CPack)