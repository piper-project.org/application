This folder contains the PIPER framework and application to position and
personalize human body models for impact, along with some data and 
documentation.

This work has received funding from the European Union Seventh Framework 
Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).

More info online at and http://piper-project.org

Obtaining the software
=======

Latest stable release version is: 1.1 (August 2020)
Download links for Windows can be found at http://piper-project.org/downloads
Source code can be found at https://gitlab.com/piper-project.org/application/
See the install page of the PIPER wiki for additional details: https://gitlab.com/piper-project.org/application/-/wikis/home

References to the old gitlab (hosted by Inria) may still be present in place.

License
=======

See the license file for details.
Open Source libraries used for the PIPER framework and application are 
under their own licenses. See Acknowledgement for the list.

Documentation
=============

* pdf version: share/doc/refman.pdf
* html version: share/doc/html/index.html
* html online (older version): http://piper-project.org/doc
* wiki and tutorials: https://gitlab.com/piper-project.org/application/-/wikis/home

Issues and discussion
=====================

Please post issues and items for discussion in the issue section: https://gitlab.com/piper-project.org/application/-/issues

If it is the first time you post, it would be helpful if you could briefly introduce yourself and give a few words about what you are trying to do.

Please note that during the migration from the gitlab at Inria and this repository, some names of users that originally posted the issues were lost.
