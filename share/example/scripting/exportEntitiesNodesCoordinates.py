# Export nodes coordinates from a model in history
# 
# arguments: 
#   /path/to/export/directory [history name]

import os
import os.path
import shutil
import exceptions

import piper.hbm
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not ( len(sys.argv) in {2,3} ) :
    raise RuntimeError("Invalid arguments")
else :
    exportDirectory = sys.argv[1]
    
    # setup model in history
    originalHistory = None
    if len(sys.argv) == 3:
        originalHistory = model.history().getCurrent()
        model.history().setCurrentModel(sys.argv[2])
        
    shutil.rmtree(exportDirectory, ignore_errors=True)
    os.makedirs(exportDirectory)
    metadata = model.metadata()
    fem = model.fem()
    piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();
    for (name,entity) in metadata.entities().iteritems() :
        ids = entity.getEntityNodesIds(fem)
        datFilePath = os.path.join(exportDirectory, name+".dat")
        piper.app.logInfo("Export {0} nodes from entity {1} to {2}".format(len(ids), name, datFilePath))
        with open(datFilePath, 'w') as datFile:
            datFile.write("# Entity {0}\n".format(name))
            datFile.write("# Id x y z\n")
            for nodeId in ids:
                coord = fem.getNode(nodeId).get().flatten()
                datFile.write("{0} {1} {2} {3}\n".format(piperIdToIdKey[nodeId].id, coord[0], coord[1], coord[2]))
    
    # restore model in history
    if not originalHistory is None:
        model.history().setCurrentModel(originalHistory)
