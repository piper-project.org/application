# Print information for a controller.
# Support landmark, joint and frame controllers
#
# arguments:
#  controller_name

# author: Thomas Lemaire date: 2017

import piper.app

if (2 != len(sys.argv)):
    raise RuntimeError("Invalid arguments")
else:
    controllerName = sys.argv[1]
    controller = None
    if controllerName in landmarkController.getControllerNames():
        controller = landmarkController
    elif controllerName in jointController.getControllerNames():
        controller = jointController
    elif controllerName in frameController.getControllerNames():
        controller = frameController
    if not controller is None:
        piper.app.logInfo("Controller: {0} - target: {1} - mask: {2} - position: {3}".format(controllerName, controller.getTarget(controllerName), controller.getMask(controllerName), controller.getCurrentPosition(controllerName)))
    else:
        piper.app.logInfo("Controller {0} does not exist".format(controllerName))