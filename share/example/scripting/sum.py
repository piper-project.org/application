# Compute the sum a + b
#
# arguments:
#  a b

import sys

# access PIPER application functions
import piper.app

def computeSum(a, b):
    piper.app.logInfo("a+b = {0}".format(a+b))

if (3 != len(sys.argv)):
    raise RuntimeError("Invalid arguments")
else :
    computeSum(float(sys.argv[1]), float(sys.argv[2]))

