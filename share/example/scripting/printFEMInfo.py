# Print information on a node, (keyword, original id)
#
# arguments:
#  nodeId

# author: Thomas Lemaire date: 2017

import piper.hbm
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not ( len(sys.argv) == 2 ) :
    raise RuntimeError("Invalid arguments")
else :
    nodeId = int(sys.argv[1])
    piperIdToIdKey = model.fem().computePiperIdToIdKeyMapNode();
    if not nodeId in piperIdToIdKey:
        piper.app.logInfo("Unknown node: {0}".format(nodeId))
    else:
        piper.app.logInfo("Node: {0} - FEM id: {1} - {2} - {3}".format(nodeId, piperIdToIdKey[nodeId][1].id, piperIdToIdKey[nodeId][0], piperIdToIdKey[nodeId][1].idstr))
