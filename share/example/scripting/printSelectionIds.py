# Print the currently selected nodes PIPER ids

import piper.hbm
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
else :
    modelVTK = model.fem().getFEModelVTK()
    nodeIds = modelVTK.getSelectedNodes()
    piper.app.logInfo("selected nodes ({0}): {1}".format(len(nodeIds), nodeIds))
