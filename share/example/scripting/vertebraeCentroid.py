# Compute the centroid of each vertebra and print it in the application log

# numpy is a standard package for matrix manipulation
import numpy

# access PIPER human body model functionnalities
import piper.hbm
# access anatomy database functionnalities
import piper.anatomyDB
# access PIPER application functions
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if (model.empty()):
    piper.app.logWarning("Model is empty")
else:
    # get metadata and fem from the current project
    metadata = piper.app.Context.instance().project().model().metadata()
    fem = piper.app.Context.instance().project().model().fem()
    for (name,entity) in metadata.entities().iteritems() :
        # check if this entity is a vertebrae
        if piper.anatomyDB.isEntityPartOf(name, "Vertebral_column", True):
            piper.app.logInfo("{0}: {1}".format(name, computeCentroid(entity, fem)))

# compute the centroid of an entity
def computeCentroid(entity, fem):
    c = numpy.zeros((3,1)) # initialize the centroid, notice the (3,1) shape for a vector
    envelopNodesId = entity.getEnvelopNodes(fem)
    for id in envelopNodesId :
         c += fem.getNode(id).get()
    return (c / len(envelopNodesId)).flatten()
