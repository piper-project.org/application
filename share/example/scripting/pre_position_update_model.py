# Add HBM current nodes position into the history.
# This script requires that "all nodes" update is activated
#
# arguments:
#  history_name_prefix

# author: Thomas Lemaire date: 2017

import piper.hbm
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not simulationController.isAllNodesActivated():
    piper.app.logWarning("\"all nodes\" update is not activated")
elif (2 != len(sys.argv)):
    raise RuntimeError("Invalid arguments")
else:
    historyPrefix = sys.argv[1]
    global historyPrefixCounter
    if not "historyPrefixCounter" in globals():
        # global variable to keep track of number, must be created only once
        historyPrefixCounter = dict()
    if not historyPrefix in historyPrefixCounter:
        historyPrefixCounter[historyPrefix] = 0
    historyName = "{0}_{1}".format(historyPrefix, historyPrefixCounter[historyPrefix])
    piper.app.Context.instance().addNewHistory(historyName)
    historyPrefixCounter[historyPrefix]+=1
    simulationController.updateModelNodes()
    piper.app.logInfo("Current HBM nodes position saved in {0}".format(historyName))
    
                                  
    