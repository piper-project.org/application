# to run the script piper --batch script_01.py

import piper.app
import piper.hbm

piper.app.logInfo("Starting example 01")

project = piper.app.Project("../data/model_01.ppj")

print "** Entities:"
for entity in project.model().metadata().entities().values():
    print entity.name(), "groupElement2D:", len(entity.get_groupElement2D()), "groupElement3D:", len(entity.get_groupElement3D())
    
print "** Model FEM:"
print "nodes:", project.model().fem().getNbNode()
print "elements1D:", project.model().fem().getNbElement1D()
print "elements2D:", project.model().fem().getNbElement2D()
print "elements3D:", project.model().fem().getNbElement3D()
