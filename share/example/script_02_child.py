# to run the script piper --batch script_01.py

import os.path
import math

import piper.app
import piper.hbm

project = piper.app.Project()
# your path to the child model
childModelPath = "/local/piper/model/LBMC_6YO_Ref_Model_v1.1"
project.importModel("Formatrules_LSDyna_Fixed.pfr", 
                    os.path.join(childModelPath, "LBMC6YO_description_LSDyna.pmr"), 
                    os.path.join(childModelPath, "LBMC.dyn"), 
                    piper.hbm.Length_mm);

project.moduleParameter().setInt("PhysPosi","autoStopNbIterationMax", 500)
project.moduleParameter().setFloat("PhysPosi", "autoStopVelocity", 1e-7)
project.moduleParameter().setFloat("PhysPosiDefo","voxelSize", 0.01)

target = piper.hbm.TargetList()
target.fixedBone.push_back(piper.hbm.FixedBoneTarget("Pelvic_skeleton"))
target.joint.push_back(piper.hbm.JointTarget("Left_hip_joint",{4:math.radians(45)}))

piper.app.physPosiDeform(project, target)

project.write("batch_test_script_02.ppj")
