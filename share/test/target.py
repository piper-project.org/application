import math

import piper.app
import piper.hbm
import piper.test

target = piper.hbm.TargetList()

target.add(piper.hbm.FixedBoneTarget("Pelvic_skeleton"))

leftASISTarget = piper.hbm.LandmarkTarget()
leftASISTarget.setName("Pelvis_position_1")
leftASISTarget.setUnit(piper.hbm.Length_m)
leftASISTarget.landmark = "Left_ASIS"
leftASISTarget.setValue({0:0.3, 1:1.2, 2:-0.25})
target.add(leftASISTarget)

piper.test.expect_eq(piper.hbm.Length_m, leftASISTarget.unit(), "Error during target creation")

hipTarget = piper.hbm.JointTarget()
hipTarget.setName("Hip_position")
hipTarget.setUnit(piper.hbm.Length_dm)
hipTarget.joint="Left_hip_joint"
hipTarget.setValue({4:math.radians(45)})
target.add(hipTarget)

piper.test.expect_eq(piper.hbm.Length_dm, hipTarget.unit(), "Error during target creation")

headTarget = piper.hbm.FrameToFrameTarget()
headTarget.setName("Head_position")
headTarget.frameSource = "W_Origin"
headTarget.frameTarget = "B_Skull"
headTarget.setUnit(piper.hbm.Length_mm)
headTarget.setValue({0:200})
target.add(headTarget)

piper.test.expect_eq(4, target.size(), "Error during target creation")