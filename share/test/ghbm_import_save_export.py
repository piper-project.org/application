import os.path

import piper.test
import piper.app
import piper.hbm

modelPath = os.getenv("PIPER_MODEL_PATH")

project = piper.app.Project()

# import GHBM
project.importModel(os.path.join(modelPath, "GHBM/lsdyna/GHBM_FixedFormat/GHBMC_M50-O_v4-1-1_2013-09-01/GHBMC_M50_v4-1-1_description_LSDyna.pmr"));

# save GHBM
ghbmProjectFile = os.path.join(piper.app.tempDirectoryPath(), "ghbm.ppj")
project.write(ghbmProjectFile)
piper.test.expect_true(os.path.exists(ghbmProjectFile), "GHBM project was not saved in {0}".format(ghbmProjectFile))

# export GHBM
ghbmExportDir = os.path.join(piper.app.tempDirectoryPath(), "ghbmExport")
project.exportModel(ghbmExportDir)
piper.test.expect_true(os.path.exists(ghbmExportDir+"/PIPER_GHBMC_M50-O_v4-1-1_20130901.dyn"), "GHBM model was not exported in {0}".format(ghbmExportDir))

#check metadata anthropometry
model = project.model()
age = model.metadata().anthropometry().age.value()
ageunit = piper.hbm.unitToStrAge(model.metadata().ageUnit())
piper.test.expect_true(ageunit, "year")
piper.test.expect_true(age, 28.0)
model.metadata().anthropometry().age.setValue(44)
piper.test.expect_true(age, 44.0)
weight = model.metadata().anthropometry().weight.value()
weightunit = piper.hbm.unitToStrMass(model.metadata().massUnit())
piper.test.expect_true(weightunit, "kg")
piper.test.expect_true(weight, 77.0)
height = model.metadata().anthropometry().height.value()
heightunit = piper.hbm.unitToStr(model.metadata().lengthUnit())
piper.test.expect_true(heightunit, "mm")
piper.test.expect_true(height, 1749)