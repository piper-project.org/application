import os.path
import math

import piper.app
import piper.hbm

project = piper.app.Project()
data_directory = piper.app.tempDirectoryPath()
# open Child
chilpProjectFile = os.path.join(piper.app.tempDirectoryPath(), "child.ppj")
project.read(chilpProjectFile)

# load Simplified Scalage Model
piper.app.importSimplifiedScalableModel(project, os.path.join(data_directory, "SNYDER_AnthropoModel.xml"))

# load target for scaling
target = piper.hbm.TargetList() 
target.read(os.path.join(data_directory, "target_child_snyder_MeanOnly.ptt"))

#apply target, using intermediate target in kriging process
piper.app.applyScalingTarget(project, target, True)
