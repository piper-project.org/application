import os.path
import math

import piper.app
import piper.hbm

project = piper.app.Project()

# save Child
chilpProjectFile = os.path.join(piper.app.tempDirectoryPath(), "child.ppj")
#chilpProjectFile = os.path.join("/local2/build/piper-release/child.ppj")
project.read(chilpProjectFile)

project.moduleParameter().setInt("PhysPosi","autoStopNbIterationMax", 100)
project.moduleParameter().setFloat("PhysPosi", "autoStopVelocity", 1e-7)
project.moduleParameter().setFloat("PhysPosi","voxelSize", 0.01)

target = piper.hbm.TargetList()

target.add(piper.hbm.FixedBoneTarget("Pelvic_skeleton"))

hipTarget = piper.hbm.JointTarget()
hipTarget.setName("Hip_position")
hipTarget.joint="Left_hip_joint"
hipTarget.setValue({4:math.radians(45)})
target.add(hipTarget)

headTarget = piper.hbm.FrameToFrameTarget()
headTarget.setName("Head_position")
headTarget.setUnit(piper.hbm.Length_mm)
headTarget.frameSource = "W_Origin"
headTarget.frameTarget = "B_Skull"
headTarget.setValue({0:200})
target.add(headTarget)

piper.app.physPosiDeform(project, target)
