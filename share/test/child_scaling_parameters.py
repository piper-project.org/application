import os.path
import math

import piper.app
import piper.hbm

project = piper.app.Project()

# open Child
chilpProjectFile = os.path.join(piper.app.tempDirectoryPath(), "child.ppj")
#chilpProjectFile = os.path.join("/local2/build/piper-release/child.ppj")
project.read(chilpProjectFile)

targetlist = piper.hbm.TargetList()

newtarget1 = piper.hbm.ScalingParameterTarget()
newtarget1.setName('skull_cortical_modulus')
newtarget1.setValue(2)   
targetlist.add(newtarget1)

newtarget2 = piper.hbm.ScalingParameterTarget()
newtarget2.setName('skull_suture_modulus')
newtarget2.setValue(3)   
targetlist.add(newtarget2)     

piper.app.applyTargetScalingParameter(project, targetlist)
