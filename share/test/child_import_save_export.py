import os.path

import piper.test
import piper.app
import piper.hbm

modelPath = os.getenv("PIPER_MODEL_PATH")

project = piper.app.Project()

# import Child
project.importModel(os.path.join(modelPath, "PIPER_ChildModel_6YO/Child_model.pmr"));

# save Child
chilpProjectFile = os.path.join(piper.app.tempDirectoryPath(), "child.ppj")
project.write(chilpProjectFile)
piper.test.expect_true(os.path.exists(chilpProjectFile), "Child project was not saved in {0}".format(chilpProjectFile))

# export Child
chilpExportDir = os.path.join(piper.app.tempDirectoryPath(), "childExport")
project.exportModel(chilpExportDir)
piper.test.expect_true(os.path.exists(chilpExportDir+"/Child_model.dyn"), "Child model was not exported in {0}".format(chilpExportDir))
