﻿import math
import os.path

import piper.app
import piper.hbm
import piper.KrigingDeformation

project = piper.app.Project()
projectFile = os.path.join("C:\\projects\\baselineModel.pmr")
project.importModel(projectFile) # or project.read(projectFile) to open a PIPER project file (.ppj)

targetFile = os.path.join("C:\\projects\\controlpoints\\myTargetControlPoints.txt") # full path to the target points
piper.app.loadTargetCPs(project, targetFile, "trg") # choose a unique name
sourceFile = os.path.join("C:\\projects\\controlpoints\\mySourceControlPoints.txt") # full path to the source points
piper.app.loadSourceCPs(project, sourceFile, "src") # choose a unique name

cpPairs = piper.app.mapStringString({'trg': 'src'}) # syntax for more pairs: {'trg' : 'src', 'trg2' : 'src2', 'trg3' : 'src3'}

 # CP parameters (nugget, as_bones, as_skin) must be set before calling project.collectControlPoints
project.model().metadata().interactionControlPoint("src").setGlobalAssociationSkin(1) # for example, this is how to change the as_skin parameter

sourceCPs = piper.hbm.InteractionControlPoint() # create an emtpy container for all control points
targetCPs = piper.hbm.InteractionControlPoint()
project.collectControlPoints(sourceCPs, targetCPs, cpPairs) # concatenates all pairs of control points to a single batch (need to call that even if you have only one pair)

kr = piper.KrigingDeformation.IntermediateTargetsInterface(project.model()) # creates an interface for kriging - this is the backend of all that you can see in the "Kriging Deformation" window

# set all kriging parameters as needed - nothing to set for "no intermediates" kriging

project.moduleParameter().setInt("Kriging", "splitThresholdCP", 10000) # this is how to set module parameters of Kriging - here the control points split threshold

kr.setControlPoints(sourceCPs, targetCPs) # set control points
piper.app.applyCPScalingTarget(project, kr, False) # this finaly calls the actual kriging. last argument True = with intermediates, False = without

# the model is now scaled with the first set of points, we can now set parameters for the next scaling with intermediate targets 
# if needed the model can be stored here (might be a good idea to have a look at it later), but is not needed
kr.setFixBones(True)
kr.setDecimationRadius(0)
kr.setUseIntermediateDomains(True)
kr.setUseIntermediateBones(True)
kr.setUseIntermediateSkin(True)

sourceCPs2 = piper.hbm.InteractionControlPoint()
targetCPs2 = piper.hbm.InteractionControlPoint()
cpPairs = piper.app.mapStringString({'myTargetPoints': 'mySrcPoints'}) # if the points are already loaded / are in the pmr, just use their name, no need to do load them from a file

project.collectControlPoints(sourceCPs, targetCPs, cpPairs)

kr.setControlPoints(sourceCPs, targetCPs) # set control points
piper.app.applyCPScalingTarget(project, kr, True) # last argument True = with intermediates, False = without

# export resulting model
resultDir = os.path.join("C:\\projects\\outputDirectory")
project.exportModel(resultDir) # or project.write(destination) to save it as a piper project (.ppj)

