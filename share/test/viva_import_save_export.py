import os.path

import piper.test
import piper.app
import piper.hbm

modelPath = os.getenv("PIPER_MODEL_PATH")

project = piper.app.Project()

# import ViVa
project.importModel(os.path.join(modelPath, "VIVA_OpenHBM_F50_20160912/VIVA_description_LSDyna.pmr"));

# save ViVa
vivaProjectFile = os.path.join(piper.app.tempDirectoryPath(), "viva.ppj")
project.write(vivaProjectFile)
piper.test.expect_true(os.path.exists(vivaProjectFile), "ViVa project was not saved in {0}".format(vivaProjectFile))

# export ViVa
vivaExportDir = os.path.join(piper.app.tempDirectoryPath(), "vivaExport")
project.exportModel(vivaExportDir)
piper.test.expect_true(os.path.exists(vivaExportDir+"/run_VIVA_OpenHBM_F50_20160912.key"), "ViVa model was not exported in {0}".format(vivaExportDir))
