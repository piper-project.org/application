Acknowledgements     {#pageAcknowledgements}
================

The PIPER application makes use of the following external libraries and tools :

- [boost](http://www.boost.org/)
- [python](https://www.python.org/)
- [numpy](http://www.numpy.org/)
- [qt](https://www.qt.io/)
- [SOFA](https://www.sofa-framework.org/)
- [VTK](http://www.vtk.org/)
- [eigen](http://eigen.tuxfamily.org)
- [Spectra](https://spectralib.org/)
- [mesquite](https://trilinos.org/packages/mesquite/)
- [tetgen](http://tetgen.org/)
- [TinySpline](https://github.com/retuxx/tinyspline)
- [TinyXML](http://www.grinninglizard.com/tinyxml2)
- [SWIG](http://swig.org/)
- [SQLite](http://sqlite.org/)
- [xmllint](http://xmlsoft.org/)
- [Google Test](https://github.com/google/googletest)
- [Doxygen](http://www.doxygen.org)
- [LaTeX](http://www.latex-project.org/)
