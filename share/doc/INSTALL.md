Install     {#pageInstall}
=======

Windows
-------

The provided software archive contains all the necessary dependencies.
It is a windows 64 version (tested on windows 7 and 10). 
It can be started using “Runpiper.bat”. 
No installation or administrative rights are needed.

Linux
-----

On linux we rely on the distribution package management system. To run the PIPER application, the required packages are:

    python2.7, python-numpy, libxml2-utils, freeglut3

Some functionnalities rely on octave scripts and a few octave toolboxes, to be able to use them, install the additionnal packages:

    octave, liboctave-dev, unzip

And then install the octave packages, start octave and proceed with:

    $ octave-cli
    octave:1> pkg install -forge io
    octave:2> pkg install -forge statistics
    octave:3> pkg list
    Package Name  | Version | Installation directory
    --------------+---------+-----------------------
              io  |   2.4.2 | /home/thomas/octave/io-2.4.2
      statistics  |   1.2.4 | /home/thomas/octave/statistics-1.2.4
