/*! \page workflowScriptPartialExportToObj Export Partial parts of the mesh to Obj format a re-import it to Piper

\tableofcontents

This workflow come after a  position modification or a personalization of the model within piper that went wrong. If one of the points are not in correct location, or if you want to smooth a part of the model after a transformation, this workflow will show you how to edit a part of the Piper mesh with your favorite 3D engine software. Note that you can use the scritps 'exportSelectedGeometryToDyna' & 'updateNodesfromDyna' the same way as this workflow to work with the LsDyna file format.

\image html exportToObjWorkflow1.png "Example of disorted model"
\image latex exportToObjWorkflow1.png "Example of disorted model" width=0.8\textwidth

\section workflow_exportToObjFile Export to Obj file

  -# Open the picking window from any module. Make sure that the option "visualize full model" is unchecked in the Entity Display window. Export supports only selection on model (non blanked) entities. 
  -# Click on "Box element Picker"
  -# Select the part of the mesh you want to export with the box picking tool.

\image html exportToObjWorkflow2.png "Select the disorted parts"
\image latex exportToObjWorkflow2.png "Select the disorted parts" width=0.8\textwidth

  -# Click on "Scripting" tab from the upper menu and select "run script"
  -# Select the script "ExportSelectedGeometryToObj" located in: "piper/share/example/scripting/" folder.
  -# As arguments, put the path of the output .obj file you want to generate, and click on "Ok"

\image html ExportFromPiperToObj.png "Example of how to use the script menu in Piper"
\image latex ExportFromPiperToObj.png "Example of how to use the script menu in Piper" width=0.8\textwidth

\section workflow_EditObj Edit the obj file

Now you can edit the obj file within a 3rd party 3d software. The tests for this workflow have been done with Meshlab that you can find: <a href="http://www.meshlab.net">here</a> 

  -# Open Meshlab and import the obj file you just extracted from piper.
  -# Do a modification on the mesh, like applying a smoothing algorithm.

\image html exportToObjWorkflow5.png "Smoothing performed by meshlab on disorted part"
\image latex exportToObjWorkflow5.png "Example of how to use the script menu in Piper" width=0.8\textwidth

  -# When it's done, export the model as an .obj and overwrite the one you exported previously from PIPER application.

\section workflow_ImportBackObj Import the obj back to Piper

  -# Go back to the Piper application.
  -# Click on the "Scripting" tab again.
  -# Select the script "UpdateNodesFromObj" within the same script folder.
  -# Give the path for your obj file and a name to be stored in the model history as an argument. example "c:/path/to/obj/file.obj modelWithSmoothedHips"
  -# Click on "Ok"

The selected part of the mesh that you extracted and modified in an other 3d application is now updated accordingly to the modifications you've done on it.

\image html exportToObjWorkflow4.png "Smoothing part reimported on Piper"
\image latex exportToObjWorkflow4.png "Smoothing part reimported on Piper" width=0.8\textwidth

If you wish you can apply a 3D mesh smoothing algorithm on the model currently displayed in the Piper application. To do that you can read the folowing workflow: \ref workflowPositioningSmoothing

*/