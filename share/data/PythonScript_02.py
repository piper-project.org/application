import piper.hbm
import piper.test

targetList = piper.hbm.TargetList()
piper.test.expect_true(0 == targetList.size())
targetList.read("target_01.ptt")
piper.test.expect_true(0 < targetList.size())
