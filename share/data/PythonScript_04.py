import sys

import piper.test

piper.test.expect_eq(3, len(sys.argv))
piper.test.expect_eq(3, float(sys.argv[1])+float(sys.argv[2]))
