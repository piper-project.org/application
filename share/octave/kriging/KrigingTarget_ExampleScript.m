arg_list = argv();

% close all
% clear all
% clc
% 

ControlPointSetName = arg_list{1}; 
InputPointFile = arg_list{2}; 
OutputPointFile = arg_list{3}; 


%% Uncomment for GUI checking
%ControlPointSetName = 'tutu' 
%currentdir = pwd; % Because Octave has bugs concerning relative paths...
%InputPointFile = [currentdir '\KrigingTarget_ControlPointData.txt'];
%OutputPointFile = [currentdir '\KrigingTarget_ControlPointData_output.txt'];

pkg load io

InputCP = dlmread(InputPointFile, " ");
ScaleFactor = 0.5;

OutputPoint = InputCP;
OutputPoint(:,2:4) = OutputPoint(:,2:4)*ScaleFactor(1)

dlmwrite(OutputPointFile, OutputPoint, " ");