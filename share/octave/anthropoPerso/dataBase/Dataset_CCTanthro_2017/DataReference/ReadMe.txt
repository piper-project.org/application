The directory "DataReference/CCTanthro_OriginalData" contains the CCT anthropometric data as obtained from Ceesar.

These data are released under the license CC BY v4.0, license text is in data directory.

Data has been copied from the excel file into csv files, with 
1) the "cause of DC" field/column being deleted 
2) the values of the "Sexe" field/column being changed in numeric values as follows: M => 1; F => 2.

