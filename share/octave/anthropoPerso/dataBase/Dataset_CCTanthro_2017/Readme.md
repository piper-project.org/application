
This directory contains data from the CCTAnthro database released under Creative Commons CC-BY-4.0 by CEESAR. 
CCTAnthro is a database including anthropometric dimensions from older subjects used as PMHS in previous testing.

The original data is provided in the DataReference folder.
The data formatted to be usable in PIPER is in the current folder.
See the license files.
