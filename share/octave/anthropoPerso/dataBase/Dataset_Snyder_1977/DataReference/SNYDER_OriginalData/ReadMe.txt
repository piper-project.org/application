The directory "DataReference/SNYDER_OriginalData" contains the SNYDER data as downloaded from Matthew Reed's Downloads website.

More details about this can be found at Matthew Reed's website http://mreed.umtri.umich.edu/ 
or by contacting him directly at mreed@umich.edu.

Reference for the SNYDER data is 

  C.L. Owings, D.B. Chaffin, R.G. Snyder, R.H. Norcutt, 1975, "Strength Characteristics of 
  U.S. Children for Product Safety Design", Final Report, Contract FDA-73-32 
  that can be found at http://http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Owings_1975.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Owings_1975.pdf.

  R.G. Snyder, M.L. Spencer, C.L. Owings, L.W. Schneider, 1975, "Physical Characteristics 
  of Children as Related to Death and Injury for Consumer Product Safety Design", Final Report
  May 31 1975, UM-HSRI-BI-75-5, Contract FDA-72-70
  that can be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Owings_1975.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Snyder_1975_Child.pdf.

  R.G. Snyder, L.W. Schneider, C.L. Owings, H.M. Reynolds, D.H. Golomb, M.A. Schork, 1977,
  "Anthropometry of Infants, Children, and Youths to Age 18 for Product Safety Design", 
  Final Report May 31, 197, UM-HSRI-77-17, Contract CPSC-C-75-0068
  that can be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Snyder_1977_Child.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Snyder_1977_Child.pdf.

=====================================================

Copy of the data has also been made (*.xlsx and *.csv files were generated and saved from the original *.txt file)


Notes:
-------
1) The use of a "ID" field is such that Excel identifies the csv file as a SYLK file.  

This field has therefore been renamed "Subject_Number" in the snyder.csv and other files that are present in the directory containing the current one.

2) The age in years field, "Age_in_Years", appears to actually be in milliyears, i.e. a value 4219 appears to mean 4 years and 219/1000.

This field has therefore been renamed "Age_in_MilliYears" in the snyder.csv and other files that are present in the directory containing the current one.

3) Many fields have missing values that shouldn't be confused with zero values.

4) Gender==1 for male, Gender=2 for female, based on the relationsships between age and stature for both genders.

5) the values of weight ("Weight") are in hg (1 kg = 10 hg).

6) Height (Stature) is in mm.

7) "BMI" (body mass index = (mass in kg) / (stature in m)^2 ) has been added to the data in the files snyder_withBMI....  BMI unit is kg/m^2

8) the variable "_Face_Height" has been renamed "Face_Height".

9) "Age_in_Years" (age in years) has been added to the data in the files snyder_withBMI....  Unit is y for 'year'.

--------------

C.Lecomte, University of Southampton, April 2017, EU FP7 PIPER project.  

===============================================================================