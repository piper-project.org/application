The directory "DataReference/SNYDER_OriginalData" contains the SNYDER data as downloaded from Matthew Reed's Downloads directory.

Copy of the data has also been made (*.xlsx and *.csv files were generated and saved from the original *.txt file)


Notes:
-------
1) The use of a "ID" field is such that Excel identifies the csv file as a SYLK file.  

This field has therefore been renamed "Subject_Number" in the snyder.csv and other files that are present in the directory containing the current one.

2) The age in years field, "Age_in_Years", appears to actually be in milliyears, i.e. a value 4219 appears to mean 4 years and 219/1000.

This field has therefore been renamed "Age_in_MilliYears" in the snyder.csv and other files that are present in the directory containing the current one.

3) Many fields have missing values that shouldn'e be confused with zero values.

4) Gender==1 for male, Gender=2 for female, based on the relationsships between age and stature for both genders.

5) the values of weight ("Weight") are in hg (1 kg = 10 hg).

6) Height (Stature) is in mm.

7) "BMI" (body mass index = (mass in kg) / (stature in m)^2 ) has been added to the data in the files snyder_withBMI....  BMI unit is kg/m^2

8) the variable "_Face_Height" has been renamed "Face_Height".

9) "Age_in_Years" (age in years) has been added to the data in the files snyder_withBMI....  Unit is y for 'year'.

