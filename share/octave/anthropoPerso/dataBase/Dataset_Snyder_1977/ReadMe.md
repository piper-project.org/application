This directory contains the "Snyder" data as selected and shared by UMTRI.

More details about this can be found at Matthew Reed's website http://mreed.umtri.umich.edu/ 
or by contacting him directly at mreed@umich.edu.

Reference for the SNYDER data is 

  C.L. Owings, D.B. Chaffin, R.G. Snyder, R.H. Norcutt, 1975, "Strength Characteristics of 
  U.S. Children for Product Safety Design", Final Report, Contract FDA-73-32 
  that can be found at http://http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Owings_1975.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Owings_1975.pdf.

  R.G. Snyder, M.L. Spencer, C.L. Owings, L.W. Schneider, 1975, "Physical Characteristics 
  of Children as Related to Death and Injury for Consumer Product Safety Design", Final Report
  May 31 1975, UM-HSRI-BI-75-5, Contract FDA-72-70
  that can be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Owings_1975.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Snyder_1975_Child.pdf.

  R.G. Snyder, L.W. Schneider, C.L. Owings, H.M. Reynolds, D.H. Golomb, M.A. Schork, 1977,
  "Anthropometry of Infants, Children, and Youths to Age 18 for Product Safety Design", 
  Final Report May 31, 197, UM-HSRI-77-17, Contract CPSC-C-75-0068
  that can be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/child/Snyder_1977_Child.pdf
  or in the DataReference directory of the current dataset 
     DataReference/SNYDER_OriginalData/Snyder_1977_Child.pdf.


=====================================================

C.Lecomte, University of Southampton, April 2017, EU FP7 PIPER project.  