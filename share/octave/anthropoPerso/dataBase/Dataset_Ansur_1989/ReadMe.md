This directory contains the ANSUR data as selected and shared by UMTRI.

More details about this can be found at Matthew Reed's website http://mreed.umtri.umich.edu/ 
or by contacting him directly at mreed@umich.edu.

Reference for the ANSUR data is 

ANSUR final report:
  Gordon, C.C., Bradtmiller, B., Clausen, C.E., Churchill, T., McConville, J.T., Tebbetts, I., Walker, R.A., 1989.
  1987�1988 Anthropometric survey of US Army personnel. Methods & summary statistics.
  Natick/TR-89-044. US Army Natick Research Development and Engineering Center, Natick,
  MA. Available from the National Technical Information Service website, http://www.ntis.gov.
  Copy can also be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/ansur/Gordon_1989.pdf 
  or in the DataReference directory of the current dataset 
     DataReference/ANSUR_OriginalData/Gordon 1989 Report ADA225094 - ANSUR 1988 Anthropometric survey of U.S. army personnel.pdf.

The measurement definitions:
  ANTHROPOMETRIC DATA ANALYSIS SETS MANUAL, PREPARED BY: Human Systems Information Analysis Center, AFRL/HEC/HSIAC,
  Bldg. 196, 2261 Monahan Way, Wright-Patterson AFB OH 45433-7022
  that can be found at http://mreed.umtri.umich.edu/mreed/downloads/anthro/ansur/ADAS-Dimension_Definitions.pdf
  or in the DataReference directory of the current dataset 
     DataReference/ANSUR_OriginalData/AUTHOR YEAR ADAS-Dimension_Definitions.pdf.

The Data codes that can be found at 
  http://mreed.umtri.umich.edu/mreed/downloads/anthro/ansur/ANSUR_88_Codes.pdf
  or in the DataReference directory of the current dataset 
     DataReference/ANSUR_OriginalData/AUTHOR YEAR ANSUR_88_Codes.pdf.
 
=====================================================
The original UMTRI data is saved in csv format in the two files:
ansur_men.csv
ansur_women.csv

This is concatenated in the single file:
ansur.csv

BMI (Body Mass Index) has been added to the original data and stored in the following corresponding files: 
ansur_withBMI_men.csv               (ansur_men.csv with added BMI column)
ansur_withBMI_women.csv             (ansur_women.csv with added BMI column)
ansur_withBMI.csv                   (ansur.csv with added BMI column)

The data in these six files is also stored in Matlab *.mat files that are readable from Octave.

C.Lecomte, University of Southampton, April 2017, EU FP7 PIPER project.  