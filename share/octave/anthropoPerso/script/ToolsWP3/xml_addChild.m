% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [root,indChild]=xml_addChild(root,childName,isList)
%
% [root,indChild]=xml_addChild(root,childName,isList);
%
% add a child node to a xml structure
% 
% Input: root, root node
%        childName, name of the child node to add
%        isList, tag to indicate if a list of children node with the
%                same name is allowed.  Otherwise, a single child is 
%                with a given name is allowed and pre-existing such child
%                is overwritten.  [default: isList=0]
%
if nargin<3
    isList=0;
end
%
if isfield(root,'child')
    indChild=[];
    if ~isList
        % look for preexisting child with the specified name (only if
        %      if there is no list)
        for j=1:length(root.child)
            if strcmp(root.child{j}.name,childName)
                indChild=j;
            end
        end
    end
    %
    if isempty(indChild)
        indChild=length(root.child)+1;
        root.child{indChild}=struct;
        root.child{indChild}.name=childName;
    end
else
    root.child=cell(1,0);
    root.child{1}=struct;
    root.child{1}.name=childName;
    indChild=1;
end
