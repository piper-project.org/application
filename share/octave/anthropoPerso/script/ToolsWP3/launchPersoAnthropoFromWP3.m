% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
%function launchPersoAnthropoFromWP3(inputFile,CodeDir)
%
% % Please uncomment the lines below and comment the first line of this
% %       file if the script is called from the PIPER application
arg_list = argv();
inputFile=arg_list{1};
CodeDir=arg_list{2};
%
%
if nargin>=2
    addpath([CodeDir,filesep,'ToolsParam'])
    addpath([CodeDir,filesep,'ToolsDatabase'])
    addpath([CodeDir,filesep,'ToolsWP3'])
end
%
% ==============================================================================
% 1) Load the input and parameter data
% -------------------------------------
if nargin>1
    Input=loadAnthropoInputCSV(inputFile,CodeDir);
else
    Input=loadAnthropoInputCSV(inputFile);
end
%
% ==============================================================================
% 2) Assign the paths and parameters
% -----------------------------------
if ~isfield(Input,'CodeDir')
    error('Please indicate the location of the code')
end
%
if ~isfield(Input,'DatabaseDir')
    error('Please indicate the location of the statistical database')
end
%
addpath([Input.CodeDir,filesep,'ToolsParam']);
%
initParamPerso('codeDir',Input.CodeDir);
initParamPerso('databaseDir',Input.DatabaseDir);
initParamPerso('ANSURdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_Ansur_1989']);
initParamPerso('SNYDERdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_Snyder_1977']);
initParamPerso('CCTANTHROdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_CCTanthro_2017']);
%
if isfield(Input,'RegressionDir')
    initParamPerso('RegressionDir',Input.RegressionDir);
elseif isfield(Input,'ResultsDir')
    initParamPerso('RegressionDir',Input.ResultsDir);
else
    initParamPerso('RegressionDir','.');
end
%
if isfield(Input,'SampleDir')
    initParamPerso('SampleDir',Input.SampleDir);
elseif isfield(Input,'ResultsDir')
    initParamPerso('SampleDir',Input.ResultsDir);
else
    initParamPerso('SampleDir','.');
end
%
addpathPerso;
%
% - non directory parameters...
if ~isfield(Input,'useLandmarks')
    % default value
    Input.useLandmarks=0;
end
%
initParamPerso('useLandmarks',Input.useLandmarks);
%
%  
%  SSM and optimisation parameters...
% =====================================
%  These should ideally be passed as parameters in the Input file
%     (otherwise, default values are used)
if ~isfield(Input,'SSM_maxIter')
    % default value
    Input.SSM_maxIter=20;
end
%
initParamPerso('SSM_maxIter',Input.SSM_maxIter);
%
if ~isfield(Input,'SSM_toler')
    % default value
    Input.SSM_toler=1e-6;
end
%
initParamPerso('SSM_toler',Input.SSM_toler);
%
if ~isfield(Input,'SSM_scaleCirc')
    % default value
    Input.SSM_scaleCirc=1;
end
%
initParamPerso('SSM_scaleCirc',Input.SSM_scaleCirc);
%
if ~isfield(Input,'SSM_scaleXP')
    % default value
    Input.SSM_scaleXP=1/1000;
end
%
initParamPerso('SSM_scaleXP',Input.SSM_scaleXP);
%
% ==============================================================================
% 3) Evaluate or load the regression
% -----------------------------------
%   (The regression is chosen to be evaluated if predictors are given;
%    It is otherwise chosen to be loaded if RegressionMatFilename is
%                 specified)
if isfield(Input,'Predictor')
    if ~isfield(Input,'MeasurementOfInterest')
        error('Unspecified measurements of interest for the regression')
    end
    % check that predictors are not also present as MeasurementOfInterest
    for j=1:length(Input.Predictor)
        predictor=Input.Predictor{j};
        Input.MeasurementOfInterest=...
              Input.MeasurementOfInterest(...
                      ~strcmp(Input.MeasurementOfInterest,predictor));
    end    
    %
    % Requested predictors - Measurements of interest
    requestX=Input.Predictor;
    requestY=Input.MeasurementOfInterest;
    % ... and information about their internal flag (if they should be exported
    %                      as target or not) 
    % 
    % INTERNAL FLAGS are handled this way:
    % --------------------------------------
    %    They are stored as an array "internalU" related to the measurements
    %    "requestU".  Those are stored in the generated regression as
    %    regression.requestU and regression.internalU...
    %    Any flag InternalU(j)=1 means that the measurement variable 
    %    requestU{j} should not be exported in the target files...
    %    By default, every measurement is exported in the target file.
    %    Internal usage is thus:
    %        - the values of requestU and internalU are read below
    %        - they are only saved when the regression is 
    %           generated;
    %        - they are only used when the target file is written
    %    
    if isfield(Input,'Predictor_InternalFlag')
        internalX=Input.Predictor_InternalFlag;
    end
    if isfield(Input,'MOI_InternalFlag')
        internalY=Input.MOI_InternalFlag;
    end
    %
    requestU=[requestX,requestY];
    internalU=[internalX,internalY];
    %
    if ~isfield(Input,'Population')
        error('Unspecified population for generating the regression')
    end
    %
    targetBin=cell(Input.Population.NBins,1);
    for j=1:length(targetBin)
        targetBin{j}.Ncriteria=Input.Population.Bin{j}.NCriteria;
        for k=1:targetBin{j}.Ncriteria
            targetBin{j}.criteria{k}= Input.Population.Bin{j}.criteriaName{k};
            targetBin{j}.value{k}= Input.Population.Bin{j}.criteriaValue{k};
        end
        targetBin{j}.percentage=Input.Population.Bin{j}.percentage;
    end
    %
    sourceBin=targetBin;
    %   sourceBin=rmfield(sourceBin{:},'percentage');
    %
    requestC=sourceBin{1}.criteria(:);
    for j=2:length(sourceBin)
        requestC(end+1:end+length(sourceBin{j}.criteria(:)))=...
            sourceBin{j}.criteria(:);
    end
    requestC=unique(requestC);
    %
    % 3.1.1 Evaluate the regression
    % ------------------------------
    databaseDir=paramPerso('databaseDir');
    labelPRRun='PRRun';
    saveResults=0;
    RegressionDir=paramPerso('RegressionDir');
    useLandmarks=paramPerso('useLandmarks');
    regression=persoAnthropo_WP3(...
        databaseDir,Input.AnthropometricDataset,...
        sourceBin,targetBin,requestX,requestY,requestC,...
        labelPRRun,saveResults,RegressionDir,useLandmarks);
    %
    % -> Internal flags...
    regression.requestU=requestU;
    regression.internalU=internalU;    
    %
    % 3.1.2 Save the regression
    % --------------------------
    if isfield(Input,'RegressionMatFilename')
        RegressionDir=paramPerso('RegressionDir');
        save(strcat(RegressionDir,filesep,Input.RegressionMatFilename),'regression','-mat');
    end
    %
    if isfield(Input,'PopulationMatFilename')
        RegressionDir=paramPerso('RegressionDir');
        save(strcat(RegressionDir,filesep,Input.PopulationMatFilename),'regression','-mat');
    end
    %
elseif isfield(Input,'RegressionMatFilename')
    % 3.2 Load the regression
    % ------------------------
    RegressionDir=paramPerso('RegressionDir');
    load(strcat(RegressionDir,filesep,Input.RegressionMatFilename),'regression','-mat');
end
%
% ==============================================================================
% 4) Sample subjects measurements from the regression
% ----------------------------------------------------
%   (the regression may have been generated in a previous run)
%
% 4.1 reference file information for exporting
% --------------------------------------------- 
if strcmp(regression.originalDatabase,'SNYDER')
            referenceFilename=[paramPerso('SNYDERdir'),filesep,...
                'Reference_snyder_withBMI.csv'];
            referenceHeading='Reference Code, SNYDER77';
elseif strcmp(regression.originalDatabase,'ANSUR')
            referenceFilename=[paramPerso('ANSURdir'),filesep,...
                'Reference_ansur_withBMI.csv'];
            referenceHeading='Reference Code, ANSUR88';
elseif strcmp(regression.originalDatabase,'CCTANTHRO')
            referenceFilename=[paramPerso('CCTANTHROdir'),filesep,...
                'Reference_cctanthro_withBMI.csv'];
            referenceHeading='Reference Code, CCTANTHRO17';
else
    error(['Currently unsupported target export for database ',...
                                     regression.originalDatabase]);
end
%
% 4.2 sort out different types of sampling
% ----------------------------------------- 
if isfield(Input,'SampleOutputType')
    if ~exist('regression','var')
        error('A regression to sample from is not defined')
    end
    %
    SampleDir=paramPerso('SampleDir');
    %
    if ~isfield(Input,'SampleInputType')
        error('Please define the regression sampling input type')
    end
    %
    % 4.2.1 First organise the input
    % -------------------------------
    fixedPredictor=0;
    sampledPredictor=0;
    if strcmp(Input.SampleInputType,'FixedPredictor')
        % use prescribed values of predictors
        fixedPredictor=1;
        if ~isfield(Input,'FixedPredictor')
            error('Fixed predictor values are not prescribed')
        end
        %
        XNames=Input.FixedPredictor.Name;
        Xs=zeros(size(Input.FixedPredictor.Value));
        for j=1:length(Input.FixedPredictor.Value)
            Xs(j)=str2num(Input.FixedPredictor.Value{j});
        end
        %
    elseif strcmp(Input.SampleInputType,'SampledPredictor')
        sampledPredictor=1;
        if (~isfield(Input,'SampleInputReferencePopulationMatFilename'))...
           && (~isfield(Input,'SamplePopulationMatFilename'))            
            warning('A different reference population is not prescribed (using current regression population)')
            refRegression=regression;
        else
            % recuperate reference population from a previous analysis
            currentRegression=regression;
            RegressionDir=paramPerso('RegressionDir');
            if isfield(Input,'SamplePopulationMatFilename')
                load(strcat(RegressionDir,filesep,Input.SamplePopulationMatFilename),'regression');
            else
                load(strcat(RegressionDir,filesep,Input.SampleInputReferencePopulationMatFilename),'regression');
            end
            refRegression=regression;
            regression=currentRegression;
            clear currentRegression
        end
        if ~isfield(Input,'SampleInputPDFType')
            warning('SampleInputPDFType is not prescribed (normal distribution is assumed)')
            Input.SampleInputPDFType='Normal';
        end
        if ~strcmp(Input.SampleInputPDFType,'Normal')
            error('Unsupported sample input PDF type')
        end
        if ~isfield(Input,'NbInputSamples')
            warning('The number of input samples is unspecified (One sample will be taken).')
            Input.NbInputSamples='1';
        end
        %
        XNames=refRegression.XUheadings;
        Xref=refRegression.XU;
        %
    end
%   ---------------------------------------
    % The test below is no more relevant since regressions with slave 
    %      regressions are now supported and the predictors names, XNames,
    %      are passed explicitly and checked during sampling...
    %
%%    % check that the names of predictors correspond to those
%%    %    of the regression...
%%    for j=1:length(regression.XUheadings)
%%        if ~strcmp(regression.XUheadings{j},XNames{j})
%%            error('Inconsistent predictor choices')
%%        end
%%    end
%   ---------------------------------------
    Xsheadings=XNames;
%   %
    % 4.2.2 Then, organise the output
    % --------------------------------
    meanOnly=0;
    sampledOutput=0;
    if find(strcmp(Input.SampleOutputType,'MeanOnly'))
        % evaluate the mean predicted value...
        meanOnly=1;
    end
    %
    if meanOnly
        if ~isfield(Input,'SampleMeanOnlyXMLFilename')
            warning('SampleMeanOnlyXMLFilename is not prescribed (normal distribution is assumed)')
        end
        SampleMeanOnlyXMLFilename=Input.SampleMeanOnlyXMLFilename;
    end
    %
    if find(strcmp(Input.SampleOutputType,'SampledOutput'))
        % evaluate a sampled predicted value (with regression error)...
        sampledOutput=1;
        if ~isfield(Input,'SampleOutputPDFType')
            warning('SampleOutputPDFType is not prescribed (normal distribution is assumed)')
            Input.SampleOutputPDFType='Normal';
        end
        if ~strcmp(Input.SampleOutputPDFType,'Normal')
            error('Unsupported sample output PDF type')
        end
        if ~isfield(Input,'NbOutputSamples')
            warning('The number of output samples is unspecified (One sample will be taken).')
            Input.NbOutputSamples='1';
        end
    end
    %
    if sampledOutput
        if ~isfield(Input,'SampleSampledOutputXMLFilename')
            error('SampleSampledOutputXMLFilename is not prescribed')
        end
        SampleSampledOutputXMLFilename=Input.SampleSampledOutputXMLFilename;
    end
    %
    if ~isfield(regression,'requestU')
       requestU=cell(1,0);
    else
       requestU=regression.requestU;
    end
    if ~isfield(regression,'internalU')
       internalU=zeros(size(requestU));   
    else
       internalU=regression.internalU;
    end
    %
    % 4.3 Generate samples and save them in target files
    % ---------------------------------------------------
    if fixedPredictor
        %
        if meanOnly
            [Ys,Ysheadings]=sampleRegression_Slaves(regression,Xs,...
                Xsheadings,meanOnly);
            isCoord=zeros(size(Ysheadings));
            for j=1:length(Ysheadings)
                if strcmp(Ysheadings{j}(end-1:end),'_x')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_y')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_z')
                   isCoord(j)=1;
                end
            end                
            if size(Ys,1)>1
               negSamples=find(min(Ys(:,~isCoord),[],2)<0);
            else
               negSamples=find(Ys(~isCoord)<0);
            end
% %             while ~isempty(negSamples)
            if ~isempty(negSamples)
                warning('Some mean predicted values are negative.  Please check predictor values.')
% %                 [Ys(negSamples,:)]=sampleRegression_Slaves(regression,...
% %                     Xs(negSamples,:),Xsheadings,meanOnly);
% %                 negSamples=find(min(Ys,[],2)<0);
            end
            % Predictors and samples regrouped in a single set of
            %     measurements
            U=[Xs,Ys];
            Uheadings=[XNames,Ysheadings];
            
            writeXML_targetsAnthro([SampleDir,filesep,...
                SampleMeanOnlyXMLFilename],U,Uheadings,...
                regression.originalDatabase,...
                referenceFilename,referenceHeading,...
                requestU,internalU);
            %
        end
        %
        if sampledOutput
            %
            NbOutputSamples=str2double(Input.NbOutputSamples);
            %
            inputXs=ones(NbOutputSamples,1)*Xs;
            [Ys,Ysheadings]=sampleRegression_Slaves(regression,...
                inputXs,Xsheadings,0); 
            isCoord=zeros(size(Ysheadings));
            for j=1:length(Ysheadings)
                if strcmp(Ysheadings{j}(end-1:end),'_x')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_y')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_z')
                   isCoord(j)=1;
                end
            end                            
            % 
            negSamples=find(min(Ys(:,~isCoord),[],2)<0);
            warningIssued=0;
            while ~isempty(negSamples)
                if ~warningIssued
                    warning('Some negative sampled predicted values were resampled.  Please check predictor values.')
                    warningIssued=1;
                end
                %
                [Ys(negSamples,:)]=...
                    sampleRegression_Slaves(regression,...
                    inputXs(negSamples,:),Xsheadings,0);
                negSamples=find(min(Ys(:,~isCoord),[],2)<0);
            end
            % Predictors and samples regrouped in a single set of
            %     measurements
            U=[ones(NbOutputSamples,1)*Xs,Ys];
            Uheadings=[XNames,Ysheadings];
            %
            if NbOutputSamples>1
                for j=1:NbOutputSamples
                    SampleFilenameNbOj=updateNameOutput(...
                        SampleSampledOutputXMLFilename,j);
                    %
                    writeXML_targetsAnthro([SampleDir,filesep,...
                      SampleFilenameNbOj],U(j,:),Uheadings,...
                      regression.originalDatabase,...
                      referenceFilename,referenceHeading,...
                      requestU,internalU);
                    
                end
            else
                writeXML_targetsAnthro([SampleDir,filesep,...
                    SampleSampledOutputXMLFilename],U,Uheadings,...
                    regression.originalDatabase,...
                    referenceFilename,referenceHeading,...
                    requestU,internalU);
            end
            %
        end
        %
    elseif sampledPredictor
        %
        if meanOnly
            NbInputSamples=str2double(Input.NbInputSamples);
            [Ys,Ysheadings,Xs]=sampleRegression_XrefNormal_Slaves(...
                regression,Xref,Xsheadings,NbInputSamples,meanOnly);
            isCoord=zeros(size(Ysheadings));
            for j=1:length(Ysheadings)
                if strcmp(Ysheadings{j}(end-1:end),'_x')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_y')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_z')
                   isCoord(j)=1;
                end
            end                
            negSamples=find(min([Ys(:,~isCoord),Xs],[],2)<0);
            warningIssued=0;
            while ~isempty(negSamples)
                if ~warningIssued
                    warning('Some negative sampled predicted values were resampled.  Please check predictor values.')
                    warningIssued=1;
                end
                [Ys(negSamples,:),~,Xs(negSamples,:)]=...
                sampleRegression_XrefNormal_Slaves(...
                regression,Xref,Xsheadings,length(negSamples),meanOnly);
                negSamples=find(min([Ys(:,~isCoord),Xs],[],2)<0);
            end
            % Predictors and samples regrouped in a single set of
            %     measurements
            U=[Xs,Ys];
            Uheadings=[XNames,Ysheadings];
            if NbInputSamples>1
                for j=1:NbInputSamples
                    SampleFilenameNbIj=updateNameInput(...
                        SampleMeanOnlyXMLFilename,j);
                    writeXML_targetsAnthro([SampleDir,filesep,...
                      SampleFilenameNbIj],U(j,:),Uheadings,...
                      regression.originalDatabase,...
                      referenceFilename,referenceHeading,...
                      requestU,internalU);
                end                    %
                
            else
                writeXML_targetsAnthro([SampleDir,filesep,...
                    SampleMeanOnlyXMLFilename],U,Uheadings,...
                    regression.originalDatabase,...
                    referenceFilename,referenceHeading,...
                    requestU,internalU);
            end
            %
        end
        %
        if sampledOutput
            %
            NbInputSamples=str2double(Input.NbInputSamples);
            NbOutputSamples=str2double(Input.NbOutputSamples);
            %
            Nsamples=NbInputSamples*NbOutputSamples;
            [Ys,Ysheadings,Xs]=sampleRegression_XrefNormal_Slaves(...
                regression,Xref,Xsheadings,Nsamples,0);
            isCoord=zeros(size(Ysheadings));
            for j=1:length(Ysheadings)
                if strcmp(Ysheadings{j}(end-1:end),'_x')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_y')
                   isCoord(j)=1;
                elseif strcmp(Ysheadings{j}(end-1:end),'_z')
                   isCoord(j)=1;
                end
            end                
            negSamples=find(min([Ys(:,~isCoord),Xs],[],2)<0);
            warningIssued=0;
            while ~isempty(negSamples)
                if ~warningIssued
                    warning('Some negative sampled predicted values were resampled.  Please check predictor values.')
                    warningIssued=1;
                end
               [Ys(negSamples,:),~,Xs(negSamples,:)]=...
                     sampleRegression_XrefNormal_Slaves(...
                regression,Xref,Xsheadings,length(negSamples),0);
                negSamples=find(min([Ys(:,~isCoord),Xs],[],2)<0);
            end            
            % Predictors and samples regrouped in a single set of
            %     measurements
            U=[Xs,Ys];
            Uheadings=[XNames,Ysheadings];
            if Nsamples>1
                for j=1:Nsamples
                    SampleFilenameNbIOj=updateNameInputOutput(...
                        SampleSampledOutputXMLFilename,j);
                    writeXML_targetsAnthro([SampleDir,filesep,...
                      SampleFilenameNbIOj],U(j,:),Uheadings,...
                      regression.originalDatabase,...
                      referenceFilename,referenceHeading,...
                      requestU,internalU);
                end
            else
                writeXML_targetsAnthro([SampleDir,filesep,...
                    SampleSampledOutputXMLFilename],U,Uheadings,...
                    regression.originalDatabase,...
                    referenceFilename,referenceHeading,...
                    requestU,internalU);
            end
        end
        %
    end
    %
end
%
