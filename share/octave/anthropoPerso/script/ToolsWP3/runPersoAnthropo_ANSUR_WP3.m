% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [YsMale,YsMaleAverageOnly,YsFemale,YsFemaleAverageOnly]=...
                       runPersoAnthropo_ANSUR_WP3(resultsDir)
%
% [YsMale,YsMaleAverageOnly,YsFemale,YsFemaleAverageOnly]=...
%                       runPersoAnthropo_ANSUR_WP3(resultsDir);
%
% Prototype call to persoAnthropo_ANSUR_WP3 plus interaction with WP3.
%
% Anthropometric measurement samples are taken from the regression models 
%      and exported to WP3 (in the form of xml files). 
%     
% Six samples (of combination of population and predictors) are taken:
%    - POP: male;   PRED: Stature 180 cm, BMI 30
%    - POP: male;   PRED: Stature 160 cm, BMI 30
%    - POP: male;   PRED: Stature 160 cm, BMI 25
%    - POP: female; PRED: Stature 180 cm, BMI 30
%    - POP: female; PRED: Stature 160 cm, BMI 30
%    - POP: female; PRED: Stature 160 cm, BMI 25
%
% 15 measurements are considered (note that BMI, weight and stature are 
%      not independent): 'WEIGTH','NECK_CIRC-OVER_LARYNX','CHEST_BRTH',
%             'CHEST_DEPTH','WAIST_BRTH_OMPHALION','WAIST_DEPTH-OMPHALION',
%             'BUTT_DEPTH','ELBOW_CIRC-EXTENDED','WRIST_CIRC-STYLION',
%             'THIGH_CIRC-PROXIMAL','THIGH_CIRC-DISTAL','CALF_CIRC',
%             'ANKLE_CIRC','BIDELTOID_BRTH','HIP_BRTH_SITTING'
%  
% samples are taken with two options: 
%      neglecting or considering the error in the regression model
%      (if considered, the regression error is modelled as Gaussian with 
%       standard deviation values estimated from the data)
%
% The following results are stored in the directory indicated in the variable 
%     resultsDir:  
%    - the evaluated regressions are saved as mat files for both male
%         and female population
%    - the 12 samples (male/female, three sets of predictor values, with/without
%         regression error) are each saved in an xml file. 
%
% % ------------
% % EXAMPLE
%
%   piperCodesDir='Piper_Codes';    % update this directory location
%   cd piperCodesDir;
%   start
%
%   resultsDir=paramPerso('resultsDir'); 
%
%   [YsMale,YsMaleAverageOnly,YsFemale,YsFemaleAverageOnly]=...
%                       runPersoAnthropo_ANSUR_WP3(resultsDir);% 
%
% % ------------
% C.Lecomte
% April-Aug.2016
% University of Southampton
% work funded by the EU FP7 PIPER project
%
if nargin<1
    [resultsDir,errorCode,~]=paramPerso('resultsDir');
    if errorCode
        resultsDir='.';
        initParamPerso('resultsDir',resultsDir);
    end
end
% A - generate the regressions
% ----------------------------
% predictors
requestX={'STATURE','BMI'};
% measurements
requestY={'WEIGHT',...
          'NECK_CIRC-OVER_LARYNX',...
          'CHEST_BRTH',...
          'CHEST_DEPTH',...
          'WAIST_BRTH_OMPHALION',...
          'WAIST_DEPTH-OMPHALION',...
          'BUTT_DEPTH',...
          'ELBOW_CIRC-EXTENDED',...
          'WRIST_CIRC-STYLION',...
          'THIGH_CIRC-PROXIMAL',...
          'THIGH_CIRC-DISTAL',...
          'CALF_CIRC',...
          'ANKLE_CIRC',...
          'BIDELTOID_BRTH',...
          'HIP_BRTH_SITTING'};
% population criteria
requestC={'GENDER'};
%
%
%
ANSURdir=paramPerso('ANSURdir'); 
%
% Only one criteria (gender) and one bin (either male or female) per regression model
%
% first regression (male)
sourceBinMale=cell(1,1);
sourceBinMale{1}.Ncriteria=1;
sourceBinMale{1}.criteria{1}='GENDER';
sourceBinMale{1}.value{1}='1';
%
targetBinMale=cell(1,1);
targetBinMale{1}.Ncriteria=1;
targetBinMale{1}.criteria{1}='Gender';
targetBinMale{1}.value{1}='Male';
targetBinMale{1}.percentage=100;
%
% second regression (female)
sourceBinFemale=cell(1,1);
sourceBinFemale{1}.Ncriteria=1;
sourceBinFemale{1}.criteria{1}='GENDER';
sourceBinFemale{1}.value{1}='2';
%
targetBinFemale=cell(1,1);
targetBinFemale{1}.Ncriteria=1;
targetBinFemale{1}.criteria{1}='Gender';
targetBinFemale{1}.value{1}='Female';
targetBinFemale{1}.percentage=100;
%
% Run the two regression analysis to build the regression models
%
saveResults=1;
%
labelPRRun='ANSUR_WP2WP3_male';
regressionMale=persoAnthropo_ANSUR_WP3(ANSURdir,...
                 sourceBinMale,targetBinMale,requestX,requestY,requestC,...
                 labelPRRun,saveResults,resultsDir);
%
labelPRRun='ANSUR_WP2WP3_female';
regressionFemale=persoAnthropo_ANSUR_WP3(ANSURdir,...
                 sourceBinFemale,targetBinFemale,requestX,requestY,requestC,...
                 labelPRRun,saveResults,resultsDir);
%
% B - Sample from the regression models
% -------------------------------------
%   (12 samples altogether: 2 populations;3 sets of predictors; 
%                           regression with/without error)
Xs=[1800,30;...
    1600,30;
    1600,25];
Xsheadings=regressionMale.XUheadings;
%
averageOnly=1;
%
[YsMale]=sampleRegression(regressionMale,Xs,Xsheadings);
[YsMaleAverageOnly]=sampleRegression(regressionMale,Xs,Xsheadings,...
                                                         averageOnly);
%
Xsheadings=regressionFemale.XUheadings;
[YsFemale]=sampleRegression(regressionFemale,Xs,Xsheadings);
[YsFemaleAverageOnly]=sampleRegression(regressionFemale,Xs,Xsheadings,...
                                                         averageOnly);
%
% C - Export the sample to WP3
% ----------------------------
% congregate the samples
%
YsAll=[YsMale;YsMaleAverageOnly;YsFemale;YsFemaleAverageOnly];
XsAll=[Xs;Xs;Xs;Xs];
%
nameAll={'male_Case1_predErr';'male_Case2_predErr';'male_Case3_predErr';...
  'male_Case1_predMean';'male_Case2_predMean';'male_Case3_predMean';...
  'female_Case1_predErr';'female_Case2_predErr';'female_Case3_predErr';...
  'female_Case1_predMean';'female_Case2_predMean';'female_Case3_predMean'};
%
xuheadings=regressionMale.XUheadings;
yuheadings=regressionMale.YUheadings;
uheadings=[xuheadings,yuheadings];
%
referenceFilename=[paramPerso('ANSURdir'), filesep,...
                          'Reference_ansur_withBMI.csv'];
referenceHeading='Reference Code, ANSUR88';
%
for j=1:length(nameAll)
    name=nameAll{j};
    xs=XsAll(j,:);
    ys=YsAll(j,:);
    u=[xs,ys];
    %
    % create and save a (xml) .ppt file for this (predicted) subject
    %
    writeXML_targetsAnthro([resultsDir,filesep,name,'.ptt'],u,uheadings,...
                referenceFilename,referenceHeading);
end
