% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
%
% Contributors include Christophe Lecomte (University of Southampton)
%
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
function docNode=writeXML_targetsAnthro(targetfile,u,uheadings,...
                                    originalDatabase,...
                                    referenceFilename,referenceHeading,...
                                    requestU,internalU)
%
% generate a piper target file of anthropometric measurements in XML format 
%
% Input: targetfile, name of the XML target file
%        u, values of anthropometric measurements
%        uheadings, names of the u variables (in an array of cell format)
%        originalDatabase, name of the original database from which the 
%              data has been extracted.  Currently supported are 'ANSUR'
%              and 'SNYDER'.
%        referenceFilename, name of the (CSV) reference file that contains
%              the description of the anthropometric measurement variables.
%        referenceHeading, name of the column containing the reference
%              codes in the reference file.
%        requestU, list of measurement reference codes used for internal flag 
%              handling (see "internalU" below).
%        internalU, internal flags are stored as an array "internalU" related 
%              to the measurements in requestU.  Any flag InternalU(j)=1 means 
%              that the measurement variable requestU{j} should not be exported
%              in the target files...  By default, every measurement is exported
%              in the target file.

% Output: the content of the target file follows the following structure:
%         <?xml version="1.0" encoding="UTF-8"?>
%         <piper-target version="0.5">
%            <targetList>
%               <target type="AnthropometricSectionDimensions">
%                  <name>ANSUR_CODE_NAME</name>
%                  <units length="m" />
%                  <value>300</value>
%                  <posture type="sitting" />
%            </targetList>
%         </piper-target>
%
% ------------
% % Example
% uheadings={'STATURE', 'BMI', 'WEIGHT', 'CHEST_BRTH', 'CHEST_DEPTH'};
% u=[  1800.0, 30.0, 954.4, 350.8, 293.9];
% originalDatabase='ANSUR';
% referenceFilename=[paramPerso('ANSURdir'), filesep,...
%                           'Reference_ansur_withBMI.csv'];
% referenceHeading='Reference Code, ANSUR88';
% targetFile='target.xml';
% writeXML_targetsAnthro(targetfile,u,uheadings,...
%               originalDatabase,...
%               referenceFilename,referenceHeading);
% ------------
% C.Lecomte
% New version, Aug.2016
% University of Southampton
% Work funded by the EU FP7 PIPER project
%
% update, April 2017, CL
% support for "internal" flags (requestU,internalU) 
%
if nargin<7
   requestU=cell(1,0);
end
%
if nargin<8
   internalU=zeros(size(requestU));
end
% % NEW VERSION (SUPPORTED BY OCTAVE) BELOW
%[~,~, M]=myCSVread();
[~,~, M]=myCSVread(referenceFilename);
%
colREF=find(strcmp(M(1,:),referenceHeading));
colUNIT=find(strcmp(M(1,:),'Unit'));
colPOSTURE=find(strcmp(M(1,:),'Posture'));
%
% --- OUTPUT ---
%
% First filter and sort out the special types of data:
%    1) circumferences are also saved as targets
%    2) landmarks are saved as coordx, coordy, coordz
%
[u,uheadings]=filterOutAnthroData(targetfile,u,uheadings);
%
% % docNode = com.mathworks.xml.XMLUtils.createDocument('piper-target'); % creer l'element principal du document
docNode=xml_createDoc('piper-target');
%
% % toc = docNode.getDocumentElement; % get element principal du document
% % toc.setAttribute('version','0.5'); % ajout d'un attribut version et sa valeur
docNode=xml_assignAttribute(docNode,'version','"0.5"'); % ajout d'un attribut version et sa valeur
%
% add target
% % tgtList_node = docNode.createElement('targetList');
[docNode,indList]=xml_addChild(docNode,'targetList');
tgtList_node=docNode.child{indList};
%
for j=1:length(u)
    RefCode=uheadings{j};
    indRefU=find(strcmp(requestU,RefCode));
    internal=0;
    if ~isempty(indRefU)
       indRefU=indRefU(1);
       internal=internalU(indRefU);
    end
    if ~internal
       valueTarget=u(j);
       unitTarget=M(strcmp(M(:,colREF),RefCode),colUNIT);
       unitTarget=unitTarget{1};
       postureTarget=M(strcmp(M(:,colREF),RefCode),colPOSTURE);
       postureTarget=postureTarget{1};
       %
       [tgtList_node]=add_tgt_node(RefCode,valueTarget,unitTarget,...
                   postureTarget,tgtList_node,originalDatabase);
    end
end
%
docNode.child{indList}=tgtList_node;
%
%ecris le fichier
xml_write(targetfile,docNode);
%
end
%
%
%
function [tgtList_node]=add_tgt_node(RefCode,valueTarget,unitTarget,...
                    postureTarget,tgtList_node,originalDatabase)
%
isList=1;
[tgtList_node,indTarget] = xml_addChild(tgtList_node,'target',isList);
tgt_node=tgtList_node.child{indTarget};
%
% Exception of target types for stature, weight, BMI, and age
if (strcmp(RefCode,'STATURE') && strcmp(originalDatabase,'ANSUR')) || ...
   (strcmp(RefCode,'Stature') && strcmp(originalDatabase,'SNYDER')) || ...
   (strcmp(RefCode,'Stature') && strcmp(originalDatabase,'CCTANTHRO'))
    tgt_node=xml_assignAttribute(tgt_node,'type','"Height"');
elseif strcmp(RefCode,'Sitting_Height')&&strcmp(originalDatabase,'SNYDER')
    tgt_node=xml_assignAttribute(tgt_node,'type','"Height"');
elseif strcmp(RefCode,'Eye_Height_Sitting')&&...
        strcmp(originalDatabase,'SNYDER')
    tgt_node=xml_assignAttribute(tgt_node,'type','"Height"');
elseif (strcmp(RefCode,'WEIGHT')&& strcmp(originalDatabase,'ANSUR')) || ...
       (strcmp(RefCode,'Weight')&& strcmp(originalDatabase,'SNYDER'))|| ...
       (strcmp(RefCode,'Weight')&& strcmp(originalDatabase,'CCTANTHRO'))
    tgt_node=xml_assignAttribute(tgt_node,'type','"Weight"');
elseif (strcmp(RefCode,'BMI')&& strcmp(originalDatabase,'ANSUR')) || ...
       (strcmp(RefCode,'BMI')&& strcmp(originalDatabase,'SNYDER'))|| ...
       (strcmp(RefCode,'BMI')&& strcmp(originalDatabase,'CCTANTHRO'))
    tgt_node=xml_assignAttribute(tgt_node,'type','"BMI"');
elseif (strcmp(RefCode,'AGE-ANSUR88')&&strcmp(originalDatabase,'ANSUR'))...
    || (strcmp(RefCode,'Age_in_Years')&&strcmp(originalDatabase,'SNYDER'))...
    || (strcmp(RefCode,'Age')&&strcmp(originalDatabase,'CCTANTHRO'))
    tgt_node=xml_assignAttribute(tgt_node,'type','"Age"');
else
% Default anthropometric type...
% Note that there was a change in the generic measurement type from
%    AnthropometricSectionDimensions to AnthropometricDimension
% tgt_node=xml_assignAttribute(tgt_node,'type','"AnthropometricSectionDimensions"');
    tgt_node=xml_assignAttribute(tgt_node,'type',...
                          '"AnthropometricDimension"');
end
%
tgt_node=xml_assignChild(tgt_node,'name',RefCode);
%
[tgt_node,indUnits]=xml_addChild(tgt_node,'units');
tgtunits_node=tgt_node.child{indUnits};
unitType=getUnitType(unitTarget);
tgtunits_node=xml_assignAttribute(tgtunits_node,unitType,['"',unitTarget,'"']);
tgt_node.child{indUnits}=tgtunits_node;
%
tgt_node=xml_assignChild(tgt_node,'value',num2str(valueTarget));
%
[tgt_node,indPosture]=xml_addChild(tgt_node,'posture');
tgtposture_node=tgt_node.child{indPosture};
tgtposture_node=xml_assignAttribute(tgtposture_node,'type',['"',postureTarget,'"']);
tgt_node.child{indPosture}=tgtposture_node;
%
tgtList_node.child{indTarget}=tgt_node;
%
end
%
%
%
function unitType=getUnitType(unit)
%
% This could be expanded to support more situations if needed
if strfind(unit,'kg/m^2')
    unitType='areaDensity';
elseif strfind(unit,'g')
    unitType='mass';
elseif strfind(unit,'y')
    unitType='age';
elseif strfind(unit,'l')
    error('unsupported volume unit');
elseif strfind(unit,'s')
    error('unsupported time unit');
elseif strfind(unit,'m')
    unitType='length';
end
%
end
%
%
%
function [u,uheadings]=filterOutAnthroData(targetfile,u,uheadings)
%
nbfound=0;
j=1;
circheadings=cell(1,0);
circvalues=zeros(1,0);
while j<=length(uheadings)
    if strcmp(uheadings{j}(max(1,end-4):end),'_circ')
        nbfound=nbfound+1;
        % store circumference info
        circheadings{nbfound}=uheadings{j};
        circvalues(nbfound)=u(j);
        % update anthropo tables
        u=u(:,[1:j-1,j+1:end]);
        uheadings=uheadings([1:j-1,j+1:end]);
    else
        j=j+1;
    end
end
%
% Write sampled circumferences 
% =============================
% circvalues, circheadings
if ~isempty(circvalues)
circumfFile=strrep(targetfile,'.ptt','_circumf.csv');
Aheadings{1,1}='bone';
Aheadings{1,2}='_circumf(mm)';
%
Adata=cell(length(circheadings),2);
for j=1:length(circheadings)
    Adata(j,1)=strrep(circheadings{j},'_circ','');
end
for j=1:length(circvalues)
    Adata(j,2)={num2str(circvalues(j))};
end
%
separator=',';
alphaBnd='"';
myCSVwrite(circumfFile,Adata,Aheadings,separator,alphaBnd);
%
end
%
%
%
nbfound=0;
j=1;
k=0;
XPcoordheadings=cell(1,0);
XPcoordvalues=zeros(1,0);
while j<=length(uheadings)
    if strcmp(uheadings{j}(max(1,end-1):end),'_x') || ...
       strcmp(uheadings{j}(max(1,end-1):end),'_y') || ...
       strcmp(uheadings{j}(max(1,end-1):end),'_z')
       %
        nbfound=nbfound+1;
        % store circumference info
        XPcoordheadings{nbfound}=uheadings{j};
        XPcoordvalues(nbfound)=u(j);
        if strcmp(uheadings{j}(max(1,end-1):end),'_x')
            k=k+1;
            indXcoord(k)=nbfound;
        end
        % update anthropo tables
        u=u(:,[1:j-1,j+1:end]);
        uheadings=uheadings([1:j-1,j+1:end]);
        %
    else
        j=j+1;
    end
end
%
% Write landmark coordinates
% =============================
%
if ~isempty(XPcoordvalues)
landmarksFile=strrep(targetfile,'.ptt','_landmarks.csv');
Aheadings{1,1}='name';
Aheadings{1,2}='bone';
Aheadings{1,3}='x(mm)';
Aheadings{1,4}='y(mm)';
Aheadings{1,5}='z(mm)';
%
Adata=cell(length(indXcoord),2);
%
% body parts are retrieved from parameters
dataBP=paramPerso('dataBP');
landmarkCodesXRows=paramPerso('landmarkCodesXRows');
%
for k=1:length(indXcoord)
    landmarkName=strrep(XPcoordheadings{indXcoord(k)},'_x','');
    Adata(k,1)=landmarkName;
    Adata(k,3)=XPcoordvalues(indXcoord(k));
    j=indXcoord(k)+1;
    if ~strcmp(XPcoordheadings{j},strrep(XPcoordheadings{indXcoord(k)},'_x','_y'))
       error('Please verify the presence of y coordinates')
    end
    Adata(k,4)=XPcoordvalues(j);
    j=indXcoord(k)+2;
    if ~strcmp(XPcoordheadings{j},strrep(XPcoordheadings{indXcoord(k)},'_x','_z'))
       error('Please verify the presence of z coordinates')
    end
    Adata(k,5)=XPcoordvalues(j);
    %
    p=find(strcmp(landmarkCodesXRows,landmarkName));  
    Adata(k,2)=dataBP{p};
    %
end
%
separator=',';
alphaBnd='"';
myCSVwrite(landmarksFile,Adata,Aheadings,separator,alphaBnd);
%
end
%
end
%
%
%