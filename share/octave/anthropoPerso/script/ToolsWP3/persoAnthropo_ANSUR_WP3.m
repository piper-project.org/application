% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function regression=persoAnthropo_ANSUR_WP3(ANSURdir,...
                 sourceBin,targetBin,requestX,requestY,requestC,...
                 labelPRRun,saveResults,resultsDir)
%
% function regression=persoAnthropo_ANSUR_WP3;
%          regression=persoAnthropo_ANSUR_WP3(ANSURdir,...
%                     sourceBin,targetBin,...
%                     requestX,requestY,requestC,...
%                     labelPRRun,saveResults,resultsDir);
%
% Application of Parkinson & Reed's approach (script "ParkinsonReed.m")
%      for personalisation of anthropometric measurements
%      using **ANSUR** data to personalise the WP3 existing FE model.
%
% The anthropometric measurements are generated in terms of a few 
%      predictors/descriptors, based on a multiple regression model.
%
% The regression is built from a virtual population, whose composition 
%      is defined by the user, in terms of a number of 
%      bins and their percentage in the whole population.  The bins are
%      defined by values of some chosen criteria.
%
% The population may for example be divided in four bins (subgroups):
%    Bin 1: Female, White  
%    Bin 2: Female, Black
%    Bin 3: Female, Hispanic  
%    Bin 4: Female, Other
% The composition of the bins is passed in the form of 
% a) a cell of structured data, sourceBin or targetBin, so that 
%        - sourceBin{j} corresponds to the j-th bin
%        - sourceBin{j}.Ncriteria inidcates the number of criteria defining
%                                 the bin
%        - sourceBin{j}.criteria{k} is the name of the k-th criteria for 
%                                 the j-th bin
%        - sourceBin{j}.value{k} is the value of the k-th criteria for 
%                                 the j-th bin
%    sourceBin contains the criteria names and values in terms of the 
%    ANSUR dataset.  targetBin contains those names in a user format that
%    can be freely chosen.
%    For example gender value "1" in ANSUR could be renamed "male". 
% b) the population percentages, being the percentage of the j-th bin 
%      in the population.  This is stored as a field of the targetBin
%      structure: targetBin{j}.percentage.
%
%  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%
% Output: regression, regression between the predictor data specified
%           by requestX and the measurements of interest specified by 
%           requestY.  This is a structure that contains the following 
%           fields:
%                regression.label, label of the run, see input labelPRRun
%                regression.approach, version of the algorithm used
%                regression.originalDatabase, label of the anthropometric 
%                          database used 
%                regression.XUheadings, headings of the predictor data
%                regression.YUheadings, headings of the measurements of 
%                          interest
%                regression.CUheadings, headings of the criteria defining
%                          the populations
%                regression.CUheadingsTarget, same as CUheadings except
%                          that these are user defined labels instead 
%                regression.bin, description of the target bins
%                regression.binSource, dwescription of the bins, expressed
%                          with source headings,
%            regression.Q, regression.P, regression.YU_mean,
%            regression.B, regression.E, regression.psig, regression.sigQ,
%            regression.XU: regression parameters (see documentation of
%            the ParkinsonReed script 'help ParkinsonReed'.  Note that 
%            XU corresponds to the upsampled virtual population.)
%
% Inputs: - ANSURdir, location of the dataset of the ANSUR data.
%         - sourceBin, description of the source population bins.   
%              sourceBin{j} corresponds to the j-th bin; 
%              sourceBin{j}.Ncriteria is the number of criteria defining 
%              the bin; sourceBin{j}.criteria{k} is the name of 
%              the k-th criteria; sourceBin{j}.value{k} is the value of 
%              the k-th criteria.  The number and criteria fields must
%              be identical over all the bins.
%         - targetBin, description of the target population bins.   
%              This has the same format as sourceBin plus a "percentage"
%              field.  
%              targetBin{j} corresponds to the j-th bin; 
%              targetBin{j}.Ncriteria is the number of criteria defining 
%              the bin; targetBin{j}.criteria{k} is the name of 
%              the k-th criteria; targetBin{j}.value{k} is the value of 
%              the k-th criteria; targetBin{k}.percentage is the percentage
%              of the bin in the population. 
%         - requestX, requestY, requestC, column numbers or  label of data 
%              (cell of strings of characters) of the (ANSUR) data  
%              respectively, as predictors, measurements of interest, and 
%              criteria.  
%              RequestC must include the criteria used to define
%              the population.  
%         - labelPRRun, a (user defined) label for the analysis run.
%         - saveResults, tag that indicates if the evaluated regression is 
%              saved as a mat file named ['regression_',labelPRRun,'.mat']
%         - resultsDir, directory in which the mat file containing the 
%              regression information is saved. 
%
% Version 1.0 (based on "runParkinsonReed")
%
% Christophe Lecomte
% University of Southampton
% April 2016
%
% =======================================
% % Example
% % ========
% % a) allocation of path and parameters and ANSUR directory
% % ---------------------------------------------------------
%   piperCodesDir='Piper_Codes';    % update this directory location
%   cd piperCodesDir;
%   start
%
%   ANSURdir=paramPerso('ANSURdir'); 
%
% % b) target bin description (and equivalent source bin description)
% % --------------------------
%  popBinPercentage(1)=40;
%  popBinPercentage(2)=30;
%  popBinPercentage(3)=10;
%  popBinPercentage(4)=20;
%
% targetBin{1}.Ncriteria=2;
% targetBin{1}.criteria{1}='Gender';
% targetBin{1}.value{1}=   'Female';
% targetBin{1}.criteria{2}='Race';
% targetBin{1}.value{2}={  'White'};
% targetBin{1}.percentage=popBinPercentage(1);
% %
% targetBin{2}.Ncriteria=2;
% targetBin{2}.criteria{1}='Gender';
% targetBin{2}.value{1}=   'Female';
% targetBin{2}.criteria{2}='Race';
% targetBin{2}.value{2}={  'Black'};
% targetBin{2}.percentage=popBinPercentage(2);
% %
% targetBin{3}.Ncriteria=2;
% targetBin{3}.criteria{1}='Gender';
% targetBin{3}.value{1}=   'Female';
% targetBin{3}.criteria{2}='Race';
% targetBin{3}.value{2}={  'Hispanic'};
% targetBin{3}.percentage=popBinPercentage(3);
% %
% targetBin{4}.Ncriteria=2;
% targetBin{4}.criteria{1}='Gender';
% targetBin{4}.value{1}=   'Female';
% targetBin{4}.criteria{2}='Race';
% targetBin{4}.value{2}={  'Other'};
% targetBin{4}.percentage=popBinPercentage(4);
% %%
% %
% sourceBin{1}.Ncriteria=2;
% sourceBin{1}.criteria{1}='GENDER';
% sourceBin{1}.value{1}=   '2';
% sourceBin{1}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{1}.value{2}={  '1'};
% %
% sourceBin{2}.Ncriteria=2;
% sourceBin{2}.criteria{1}='GENDER';
% sourceBin{2}.value{1}=   '2';
% sourceBin{2}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{2}.value{2}={  '2'};
% %
% sourceBin{3}.Ncriteria=2;
% sourceBin{3}.criteria{1}='GENDER';
% sourceBin{3}.value{1}=   '2';
% sourceBin{3}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{3}.value{2}={  '3'};
% %
% sourceBin{4}.Ncriteria=2;
% sourceBin{4}.criteria{1}='GENDER';
% sourceBin{4}.value{1}=   '2';
% sourceBin{4}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{4}.value{2}={  '4',...
%     '5',...
%     '6',...
%     '7',...
%     '8',...
%     '9',...
%     '**',...
%     '***',...
%     '9999' };
%
% % c) label for the regression and storage info
% % --------------------------------------------
% labelPRRun='demoPersoAnthroWP3';
% saveResults=1;
% resultsDir='Results';
%
% 
% % d) requested predictors and measurements
% % -----------------------------------------
% % 100: STATURE (1 for subject number + 99 for ANSUR code),
% % 124: WEIGHT (1 for subject number + 123 for ANSUR code),
% requestX=[100,124];
% %
% % following numbers x are all 1 for subject number + (x-1) for ANSUR code
% % -----------------------------------------------------------------------
% %  (please check numbers - higher numbers are the same as the ANSUR code)
% %
% % 81: 'NECK_CIRC-OVER_LARYNX' (1 for subject number + 80 for ANSUR code)
% % 3: 'ACROMION_HT' (1 for subject number + 2 for ANSUR code)
% % 5: 'ACR-RADL_LNTH' (etc...)  
% % 11: 'BIACROMIAL_BRTH'
% % 13: 'BIDELTOID_BRTH' (equivalent to "Shoulder breadth" in CAESAR data?)
% % 27: 'BUTT_KNEE_LNTH'
% % 36: 'CHEST_CIRC-BELOW_BUST_'
% % 61: 'HEAD_BRTH'
% % 63: 'HEAD_LNTH'
% % 67: 'HIP_BRTH_SITTING'
% % 74: 'KNEE_HT_-_SITTING'
% % 94: 'SITTING_HT'
% % 104: 'THIGH_CIRC-PROXIMAL' (alias THIGH_CIRC)
% % 107: 'THUMB-TIP_REACH'
% %
% requestY=[4, 3, 5, 11, 13, 27, 36, 61, 63, 67, 74, 94, 104, 107];
% %
% requestC={'GENDER','RACE_OF_SUBJECT-ANSUR88'};
% %
% % INFO ON CRITERIA FOR ANSUR DATA...
% %
% % 1262 - 1264 - RACE
% % RACE: RACIAL GROUPS WERE ASSIGNED THE FOLLOWING NUMERICAL CODES:
% % ======
% % 1 - WHITE / 
% % 2 - BLACK / 
% % 3 - HISPANIC / 
% % 4 - ASIAN / PACIFIC ISLANDER / 
% % 5 - AMERICAN INDIAN /
% % 6 - CARRIBEAN, INCLUDING: BERMUDAN, HAITIAN, JAMAICAN, AND WEST INDIAN / 
% % 7 - EAST INDIAN, INCLUDING: ASIAN INDIAN AND EAST INDIAN / 
% % 8- ARAB, INCLUDING: ARABIAN, EGYPTIAN, IRANIAN, AND IRAQIAN.
% % 9999- A CODE OF (9999) INDICATES N/A, MISSING OR ILLEGIBLE ENTRIES.
% % A ONE DIGIT CODE INDICATES A SINGLE RACE, E.G., (1) CORRESPONDS TO WHITE. 
% % A TWO TO FOUR DIGIT CODE INDICATES SUBJECT OF MIXED PARENTAGE, E.G., 
% %    (127) CORRESPONDS TO WHITE/BLACK/EAST INDIAN. IN ADDITION, THE ORDER  
% %    OF THE DIGITS CORRESPONDS DIRECTLY TO THE ORDER IN WHICH THE SUBJECT 
% %    INDICATED HIS OR HER MIXED PARENTAGE ON THE BIOGRAPHICAL DATA FORMS.
%
%  regression=persoAnthropo_ANSUR_WP3(ANSURdir,...
%                     sourceBin,targetBin,...
%                     requestX,requestY,requestC,...
%                     labelPRRun,saveResults,resultsDir);
% % ---------------------
% Ref [1] 1988 ANTHROPOMETRIC SURVEY U.S. ARMY - FEMALE WORKING DATA BASE
%
% See also: parkinsonReed, populationUpsample, multipleRegression,
%           loadANSURdata, XYCdataANSUR
%
%
if nargin<1
%   Seeks the address of the ANSUR directory among the PIPER parameters
   ANSURdir=paramPerso('ANSURdir');
end
%
if nargin<7
    % Label to the run for the identification of analysis and output...
    labelPRRun='PR_ANSUR';
end
if nargin<8
    saveResults=0;
end
if nargin<9
    [resultsDir,errorCode,~]=paramPerso('resultsDir');
    if errorCode
        resultsDir='.';
        initParamPerso('resultsDir',resultsDir);
    end
end
% 1) load source data
% --------------------
% load ANSUR data
loadMat=1;
[ANSURdata,ANSURheading]=loadANSURdata(ANSURdir,loadMat);
% format requested data in X, Y, C arrays (predictor, detailed
%                                              measurements, criteria)
%
iscell(ANSURdata)
% 2) extract the useful data for the chosen regression analysis
% --------------------------------------------------------------
% (1. select and extract requested data; 2. put it in "X,Y,C" format ,i.e.
%    in the format of a list of predictors, measurements and criteria.)
[XU,XUheadings,YU,YUheadings,CU,CUheadings]=...
    XYCdataANTHROPO(ANSURdata,ANSURheading,...
    requestX,requestY,requestC);
%
% 3) Apply the Parkinson-Reed approach
% -------------------------------------
% (1. generates a virtual population; 2. build a multiple regression model)
psig=0.05;      % threshold for significance
%
[XU_vrt,~,~,...
    Q,P,YU_mean,B,E,sigQ]=...
    ParkinsonReed(XU,XUheadings,YU,CU,CUheadings,...
    targetBin,sourceBin,psig);
%
% 4) save PR regression results
% ------------------------------
% This stores information about the resulting regression...
% In future versions:
%     The names/label/codes for approaches should be formalised
%     The names/label/codes for this and other databases should be formalised
%     Names of headings should be formalised as "criteria"
regression.label=labelPRRun;
regression.approach='PR_v1.1';
regression.originalDatabase='ANSUR';
regression.XUheadings=XUheadings;
regression.YUheadings=YUheadings;
regression.CUheadings=CUheadings;
% regression.CUheadingsTarget=CUheadingsTarget;
regression.bin=targetBin;
regression.binSource=sourceBin;
regression.Q=Q;
regression.P=P;
regression.YU_mean=YU_mean;
regression.B=B;
regression.E=E;
regression.psig=psig;
regression.sigQ=sigQ;
regression.XU=XU_vrt;
%
if saveResults
    if ~exist(resultsDir,'dir')
        mkdir(resultsDir);
    end
    save(strcat(resultsDir,filesep,'regression_',labelPRRun,'.mat'),'regression','-mat');
end
%
