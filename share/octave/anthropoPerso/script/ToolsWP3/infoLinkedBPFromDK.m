function linkedBP=infoLinkedBPFromDK(modelBP,DKmodelDir)
%
% ======================================
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ======================================
%
% function linkedBP=infoLinkedBPFromDK(modelBP,DKmodelDir);
%
% Gets information from the kinematic model (hard coded to be the
%        "PIPER" kinematic model at this stage) about the data segments
%
% The body parts in the modelBP cell array are reorganised as linked body parts,
%        in linkedBP.  Each component of linkedBP corresponds to a segment of 
%        the PIPER articulated model that can be found within the directory 
%        DKmodelDir.
%
% % Input: DKmodelDir, address of the forward kinematic model.
%        modelBP, list of (unique) names of body parts.  This is in 
%                  the format of a cell array, e.g.  modelBP{1}='Left_femur'.
% Output: linkedBP, list of body parts, organised by "linked groups".  These 
%                  groups are single entities or 'sub-configurations' used in 
%                  alignment and Procrustres analysis, or 'segments' used
%                  in direct kinematics (DK).  This is a cell array of 
%                  cell arrays, e.g. linkedBP{1}={'Left_femur'} or 
%                  linkedBP{1}={'Left_femur','right_femur'}.  In the present 
%                  case, each linked group corresponds to a segment of the 
%                  direct kinematic model.
%
% C.Lecomte, 2017
% University of Southampton
%

% Needs access to DK (direct kinematic) tools and data
if nargin<2
    DKmodelDir=paramPerso('DKmodelDir');
end
%--read the kinematic model
load('-text',[DKmodelDir,filesep,'Piper.Model.txt'],'model');
%
% This is the list of joints of the kinematic model
ListOfJoints = fieldnames(model.Kinematic);
% To each "joint" corresponds a body segment, of which all BPs are linked 
%                                                                   together
linkedBP=cell(size(ListOfJoints));
SegModelBP=zeros(size(modelBP));   % indicates the segment of a Body part
%
% handling of names
[AequivBP,AequivBP_heading]=...
         myCSVread([paramPerso('databaseDir'),filesep,'DataReference',...
                            filesep,'equivalenceBodyParts.csv']);
col_anatDB=find(strcmp(AequivBP_heading,'AnatomyDB_Entity'));
col_DKbone=find(strcmp(AequivBP_heading,'DirectKinematic_Bone'));
%
for j=1:length(ListOfJoints)
    Joint=ListOfJoints{j};
    for i=1:length(modelBP)
        % handling of names
        BP_anatDB=modelBP{i};
        indBP=find(strcmp(AequivBP(:,col_anatDB),BP_anatDB));
        BP_DKbone=AequivBP(indBP,col_DKbone);
        %
        if ~isempty(find(strcmpi(model.Kinematic.(Joint).Bones,BP_DKbone)))
            SegModelBP(i)=j;
            if isempty(linkedBP{j})
                linkedBP{j}={modelBP{i}};
            else
                linkedBP{j}=vertcat(linkedBP{j},modelBP(i));
            end
        end
    end
end
%
% Remove empty linked body parts
j=1;
while j<=length(linkedBP)
    if isempty(linkedBP{j})
        linkedBP=vertcat(linkedBP(1:j-1),linkedBP(j+1:end));
        SegModelBP(SegModelBP>j)=SegModelBP(SegModelBP>j)-1;
    else
        j=j+1;
    end
end
%
% Add bones that are not in a DK segment
%
numberSegments=length(linkedBP);
for j=1:length(modelBP)
    if ~SegModelBP
        linkedBP=vertcat(linkedBP,modelBP(j));
        SegModelBP(j)=length(linkedBP);
    end
end
%
%
%