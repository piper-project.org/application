function catText=catKron(preText,sufText)
%
% ==============================================================================
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ==============================================================================
% Concatenate text in "Kronecker" fashion, i.e. each of the prefix arguments
%     is concatenated in turn with all the suffix arguments.
%
% Input: - preText, prefix text stored in a cell array. 
%        - sufText, suffix text stored in a cell array.
% Output: - catText, concatenated text stored in a cell array.
%
% E.g.  catKron({'apexPos','nosePos'},{'_x','_y','_z'})
%     returns    {'apexPos_x';'apexPos_y';'apexPos_z';'nosePos_x';...
%                                 'nosePos_y';'nosePos_z'}
%
if ~iscell(preText)
   error('catKron input should be cell arrays')
end
if ~iscell(sufText)
   error('catKron input should be cell arrays')
end
if min(size(preText))~=1
   error('catKron does not support multiple dimension cell arrays')
end
if min(size(sufText))~=1
   error('catKron does not support multiple dimension cell arrays')
end
catText=cell(length(preText)*length(sufText),1);
%
nElem=0;
for j=1:length(preText)
    for k=1:length(sufText)
        nElem=nElem+1;
        catText{nElem}=[preText{j},sufText{k}];
    end
end
% returns a row/col cell array depending of the form of the preText input 
if size(preText,1)==1
   catText=catText';
end
%
%