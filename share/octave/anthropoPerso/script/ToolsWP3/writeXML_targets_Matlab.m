% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function writeXML_targets_Matlab(targetfile,xs,xuheadings,ys,yuheadings,ANSURdir)
%
% generate a piper target file
%
% <?xml version="1.0" encoding="UTF-8"?>
% <piper-target version="0.5">
% <targetList>
%     <target type="AnthropometricSectionDimensions">
%         <name>ANSUR_CODE_NAME</name>
%         <units length="m" />
%         <value>300</value>
%         <posture type=�sitting� />
% </targetList>
% </piper-target>
%

% read info from ANSUR dataset
%
% % OLD VERSION BELOW
% % isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
% % if isOctave
% %    error('xlsread is not supported in Octave')
% % end
% % [~,~, M]=xlsread([ANSURdir, filesep,'Reference_ansur_withBMI.xlsx']);
% % NEW VERSION (SUPPORTED BY OCTAVE) BELOW
[~,~, M]=myCSVread([ANSURdir, filesep,'Reference_ansur_withBMI.csv']);
%
colREF=find(strcmp(M(1,:),'Reference Code, ANSUR88'));
colUNIT=find(strcmp(M(1,:),'Unit'));
colPOSTURE=find(strcmp(M(1,:),'Posture'));
%
% --- OUTPUT ---
%
docNode = com.mathworks.xml.XMLUtils.createDocument('piper-target'); % creer l'element principal du document
toc = docNode.getDocumentElement; % get element principal du document
toc.setAttribute('version','0.5'); % ajout d'un attribut version et sa valeur
%
% add target
tgtList_node = docNode.createElement('targetList');
%
for j=1:length(xs)
    RefCodeANSUR=xuheadings{j};
    valueTarget=xs(j);
    iInfo=find(strcmp(M(:,colREF),RefCodeANSUR));
    %
    unitTarget=M(iInfo,colUNIT);
    postureTarget=M(iInfo,colPOSTURE);
    %
    [tgtList_node,docNode]=add_tgt_node(RefCodeANSUR,...
        valueTarget,unitTarget,postureTarget,tgtList_node,docNode);
end
%
%
for j=1:length(ys)
    RefCodeANSUR=yuheadings{j};
    valueTarget=ys(j);
    iInfo=find(strcmp(M(:,colREF),RefCodeANSUR));
    %
    unitTarget=M(iInfo,colUNIT);
    postureTarget=M(iInfo,colPOSTURE);
    %
    [tgtList_node,docNode]=add_tgt_node(RefCodeANSUR,valueTarget,...
        unitTarget,postureTarget,tgtList_node,docNode);
end
%
%
toc.appendChild(tgtList_node);
%ecris le fichier
xmlwrite(targetfile,docNode);

end
%
%
%
function [tgtList_node,docNode]=add_tgt_node(RefCodeANSUR,valueTarget,unitTarget,postureTarget,tgtList_node,docNode)

tgt_node = docNode.createElement('target');
tgt_node.setAttribute('type','AnthropometricDimension');
tgtname_node = docNode.createElement('name');
tgtname_node.appendChild(docNode.createTextNode(RefCodeANSUR));
tgtunits_node = docNode.createElement('units');
tgtunits_node.appendChild(docNode.createTextNode(unitTarget));
tgtvalue_node = docNode.createElement('value');
tgtvalue_node.appendChild(docNode.createTextNode(num2str(valueTarget)));
tgtposture_node = docNode.createElement('posture');
tgtposture_node.setAttribute('type',postureTarget);
%
tgt_node.appendChild(tgtname_node);
tgt_node.appendChild(tgtunits_node);
tgt_node.appendChild(tgtvalue_node);
tgt_node.appendChild(tgtposture_node);
%
tgtList_node.appendChild(tgt_node);

end
