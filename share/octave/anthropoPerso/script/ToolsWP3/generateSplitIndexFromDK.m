function [splitIndex,splitIndexGroup,linkedBP]=...
                        generateSplitIndexFromDK(dataBP,modelBP,DKmodelDir)
%
% ==============================================================================
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ==============================================================================
%
% function [splitIndex,splitIndexGroup,linkedBP]=...
%                        generateSplitIndexFromDK(dataBP,modelBP,DKmodelDir);
%
% Input: dataBP, list of names of body parts (to which indiviadual elements, 
%                  such as nodes or landmarks are attached).  This is in 
%                  the format of a cell array, e.g.  dataBP{1}='Left_femur'.
%        modelBP, list of (unique) names of body parts.  This is in 
%                  the format of a cell array, e.g.  modelBP{1}='Left_femur'.
% Output: splitIndex, index indicating which body part number in modelBP, 
%                  an element from dataBP belongs to.  splitIndex(k)=j indicates
%                  that the k-th element of dataBP, dataBP{k}, belongs to the 
%                  j-th considered body part (modelBP{j}).
%         linkedBP, list of body parts, organised by "linked groups".  These 
%                  groups are single entities or 'sub-configurations' used in 
%                  alignment and Procrustres analysis, or 'segments' used
%                  in direct kinematics (DK).  This is a cell array of 
%                  cell arrays, e.g. linkedBP{1}={'Left_femur'} or 
%                  linkedBP{1}={'Left_femur','right_femur'} .
%         splitIndexGroup, index indicating which linkedBP group, an element 
%                  from dataBP belongs to.  splitIndexGroup(k)=j indicates
%                  that the k-th element of dataBP, dataBP{k}, belongs to the 
%                  j-th group (linkedBP{j}).
%
% Note that the names in linkedBP, dataBP, and modelBP are consistent, in the
%         sense that sub body parts as "individual ribs" being parts of the 
%         "rib cage" are not supported.  (See commented lines if needed...)
%
% Christophe Lecomte
% University of Southampton
% Work funded by the EU FP7 PIPER project
% 2017
%

% Needs access to DK (direct kinematic) tools and data
if nargin<3
    DKmodelDir=paramPerso('DKmodelDir');
end
%
linkedBP=infoLinkedBPFromDK(modelBP,DKmodelDir);
%
splitIndex=zeros(length(dataBP),1);
%
for j=1:length(dataBP)
    BP=dataBP{j};
    ind=find(strcmp(modelBP,BP));
    splitIndex(j)=ind;
end
%
%
%
linkedGroupBP=zeros(length(modelBP),1);
for j=1:length(linkedBP)
    % current group of linked BP's
    linkedBP_current=linkedBP{j};
    if ischar(linkedBP_current)
        warning('String LinkedBP component has been changed into a cell')
        linkedBP_current={linkedBP_current};
    end
    %   
    
    % =====>  The code below could be uncommented/updated to support the 
    %         handling of parts of body parts (ribs part of rib cage, etc.)
    %
%%%    for k=1:length(linkedBP_current)
%%%        % take each element BP of the group and extract all BP's that are
%%%        %       part of it...
%%%        listBP=allSubBP(linkedBP_current{k});  % 
%%%        % then check if these correspond to an actual desired BP (modelBP)
%%%        for i=1:length(modelBP)
%%%            if sum(strcmp(modelBP{i},listBP))
%%%                if linkedGroupBP(i)~=0 && linkedGroupBP(i)~=j 
%%%                    error('A body part is in several linked BPs')
%%%                else
%%%                    linkedGroupBP(i)=j;
%%%                end
%%%            end
%%%        end
%%%        %        
%%%    end
    % =====>  The alternative is below...
    for k=1:length(linkedBP_current)
        BP=linkedBP_current{k};
        %
        for i=1:length(modelBP)
            if strcmp(modelBP{i},BP)
                if linkedGroupBP(i)~=0 && linkedGroupBP(i)~=j 
                    error('A body part is in several linked BPs')
                else
                    linkedGroupBP(i)=j;
                end
            end
        end
        %
    end 
    %
end
%
indexBPGroup=1:length(modelBP);
% Split indices are regrouped if the BP's are parts of subconfigurations,
%        i.e. sets of body parts that are treated as a single entity 
%        during alignment.
for i=1:length(modelBP)
    if linkedGroupBP(i)
        indexBPGroup(i)=length(modelBP)+linkedGroupBP(i);
    end
end
[~,~,I]=myUniqueRowsStable(indexBPGroup(:));
%
splitIndexGroup=I(splitIndex);
%
%