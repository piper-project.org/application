% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [errorCode]=checkInput(Input)
%
% Check on supported values of inputs the anthropometric
%      tools
%
if isfield(Input,'AnthropometricDataset')
    if ~strcmp(Input.AnthropometricDataset,'ANSUR') &&...
            ~strcmp(Input.AnthropometricDataset,'SNYDER') &&...
                  ~strcmp(Input.AnthropometricDataset,'CCTANTHRO')
        error('Only the ANSUR, SNYDER or CCTANTHRO datasets are currently supported')
    end
    %    
end
%
if isfield(Input,'SampleOutputType')
    for j=1:length(Input.SampleOutputType)
        if ~strcmp(Input.SampleOutputType{j},'MeanOnly')...
                && ~strcmp(Input.SampleOutputType{j},'SampledOutput')
            error('Check SampleOutputType value')
        end
    end
end
%
if isfield(Input,'SampleInputType')
    for j=1:length(Input.SampleInputType)
        if ~strcmp(Input.SampleInputType{j},'FixedPredictor')...
                && ~strcmp(Input.SampleInputType{j},'SampledPredictor')
            error('Check SampleInputType value')
        end
    end
end
%
if isfield(Input,'SampleInputPDFType')
    if ~strcmp(Input.SampleInputPDFType,'Normal')
        error('Check supported value of SampleInputPDFType')
    end
end
%
if isfield(Input,'SampleOutputPDFType')
    if ~strcmp(Input.SampleOutputPDFType,'Normal')
        error('Check supported value of SampleOutputPDFType')
    end
end
%
if isfield(Input,'Population')
    if ~isfield(Input.Population,'NBins') || ~isfield(Input.Population,'Bin')
        error('The population descriptors are not complete')
    end
    %
    if Input.Population.NBins ~= length(Input.Population.Bin)
        error('The bins are not all described')
    end
    %
end
%
if isfield(Input,'useLandmarks')
    if strcmp(Input.useLandmarks,'True')||strcmp(Input.useLandmarks,'true')...
      || strcmp(Input.useLandmarks,'TRUE')||strcmp(Input.useLandmarks,'YES')...
      || strcmp(Input.useLandmarks,'Yes')||strcmp(Input.useLandmarks,'yes')...
      || strcmp(Input.useLandmarks,'T')||strcmp(Input.useLandmarks,'t')...
      || strcmp(Input.useLandmarks,'Y')||strcmp(Input.useLandmarks,'y')...
                   || strcmp(Input.useLandmarks,'1')||strcmp(Input.useLandmarks,'y')
        Input.useLandmarks=1;
    elseif strcmp(Input.useLandmarks,'False')||...
         strcmp(Input.useLandmarks,'false')...
      || strcmp(Input.useLandmarks,'FALSE')||strcmp(Input.useLandmarks,'NO')...
      || strcmp(Input.useLandmarks,'No')||strcmp(Input.useLandmarks,'no')...
      || strcmp(Input.useLandmarks,'F')||strcmp(Input.useLandmarks,'f')...
      || strcmp(Input.useLandmarks,'N')||strcmp(Input.useLandmarks,'n')...
                   || strcmp(Input.useLandmarks,'0')
        Input.useLandmarks=0;
    else  
        error('Check supported value of parameter useLandmarks')
    end
end
%
%
%