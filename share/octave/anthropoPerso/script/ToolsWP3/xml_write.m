% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function xml_write(filename,docNode,XMLversion,nSpaceTab)
%
if nargin<4
    nSpaceTab=2;
end
TABL='';
for j=1:nSpaceTab
    TABL=[TABL,' '];
end
%
if nargin<3
    XMLversion='1.0';
end
%
%
if ~strcmp(XMLversion,'1.0')
    error('Please check the desired XML version')
end
%
fid=fopen(filename,'w');
%
STRheading='<?xml version="1.0" encoding="UTF-8"?>\n';
%
isListMember=0;
level=0;
STRxml=xml_text_child(docNode,isListMember,level,TABL);
%
fprintf(fid,[STRheading,STRxml]);
%
fclose(fid);
%
end
%
%
%
function STR=xml_text_child(childNode,isListMember,level,TABL)
%
TAB='';
for j=1:level
    TAB=[TAB,TABL];
end
%
startTag=[TAB,'<',childNode.name];
if isfield(childNode,'attribute')
    for j=1:length(childNode.attribute)
        startTag=[startTag,' ',childNode.attribute{j}.name,'=',...
            childNode.attribute{j}.value];
    end
end
%
if isListMember
    % list node: start tag on a line, followed by children nodes
    startTag=[startTag,'>\n'];
    %
    if isListNode(childNode)
        error('Nesting of lists is not supported')
    else
        isListMemberChild=0;
    end
    %
    coreSTR='';
    for j=1:length(childNode.child)
        childSTR=xml_text_child(childNode.child{j},isListMemberChild,...
            level+1,TABL);
        coreSTR=[coreSTR,childSTR]; %#ok<*AGROW>
    end
    % NOTE BELOW: this appears to be different to be different from the
    %     proposed format but this seems necessary when entering the 
    %     xml file in word (one may assume that this is a necessary 
    %     syntax format)
% %    endTag='';
    endTag=[TAB,'</',childNode.name,'>\n'];
%
    %
else
    if ~isfield(childNode,'child') && ~isfield(childNode,'value')
        % node with no value or child: name and attributes on a line
        startTag=[startTag,'/>\n'];
        endTag='';
        coreSTR='';
    elseif isfield(childNode,'value')
        % node with value only: start tage, value, end tag on a line
        startTag=[startTag,'>'];
        endTag=['</',childNode.name,'>\n'];
        coreSTR=[childNode.value];
    else
        % node with children: start tag on a line, new lines for values
        %    or children and end tag on a line.
        startTag=[startTag,'>\n'];
        endTag=[TAB,'</',childNode.name,'>\n'];
        if isfield(childNode,'value')
            coreSTR=[childNode.value,'\n'];
        else
            coreSTR='';
        end
        %
        if isListNode(childNode)
            isListMemberChild=1;
        else
            isListMemberChild=0;
        end
        for j=1:length(childNode.child)
            childSTR=xml_text_child(childNode.child{j},isListMemberChild,...
                level+1,TABL);
            coreSTR=[coreSTR,childSTR];
        end
        %
    end
    %
end
%
STR=[startTag,coreSTR,endTag];
%
end
%
%
%
function isList=isListNode(node)
%
isList=0;
if strcmp(node.name,'targetList')
    isList=1;
end
%
end
%
%
%
