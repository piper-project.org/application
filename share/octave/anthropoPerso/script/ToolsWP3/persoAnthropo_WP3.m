% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function regression=persoAnthropo_WP3(databaseDir,Ldatasets,...
                 sourceBin,targetBin,requestX,requestY,requestC,...
                 labelRegressionRun,saveResults,resultsDir,useLandmarks,...
                 maxIter,toler)
%
% function regression=persoAnthropo_WP3;
%          regression=persoAnthropo_WP3(databaseDir,Ldatasets,...
%                     sourceBin,targetBin,...
%                     requestX,requestY,requestC,...
%                     labelRegressionRun,saveResults,resultsDir,useLandmarks,...
%                     maxIter,toler);
%
% Generate a PIPER regression, based on anthropometric data, using 
%      one dataset, specified in Ldatasets, to personalise 
%      the WP3 existing FE model... A single dataset at a time is currently
%      supported.
%
% The anthropometric measurements are generated in terms of a few 
%      predictors/descriptors.
%
% Landmarks and associated measurements can also be considered, if useLandmarks.
% Dependening on that, one or another approach is used to generate the 
% regression.
% 
% => If NOT useLandmarks...
% Parkinson & Reed's approach (script "ParkinsonReed.m") is applied, and
%     the regression is based on a multiple regression model, for each of the
%     principal components of the output, i.e. measurement of interest, data.
%
% => If useLandmarks...
% A PCA is first evaluated for the whole data, input and output.  A constrained
%     optimisation approach is then used to match the desired values of the 
%     predictors, only for a few main PC modes.  (This is currently only
%     supported for the older PMHS subjects dataset, CCTANTHRO.) 
%
% The regression is built from a virtual population, whose composition 
%      is defined by the user, in terms of a number of 
%      bins and their percentage in the whole population.  The bins are
%      defined by values of some chosen criteria.
%
% The population may for example be divided in four bins (subgroups):
%    Bin 1: Female, White  
%    Bin 2: Female, Black
%    Bin 3: Female, Hispanic  
%    Bin 4: Female, Other
% The composition of the bins is passed in the form of 
% a) a cell of structured data, sourceBin or targetBin, so that 
%        - sourceBin{j} corresponds to the j-th bin
%        - sourceBin{j}.Ncriteria inidcates the number of criteria defining
%                                 the bin
%        - sourceBin{j}.criteria{k} is the name of the k-th criteria for 
%                                 the j-th bin
%        - sourceBin{j}.value{k} is the value of the k-th criteria for 
%                                 the j-th bin
%    sourceBin contains the criteria names and values in terms of the 
%    chosen dataset.
%    TargetBin contains those names in a user format that can be freely 
%    chosen. For example gender value "1" in ANSUR could be renamed "male". 
% b) the population percentages, being the percentage of the j-th bin 
%      in the population.  This is stored as a field of the targetBin
%      structure: targetBin{j}.percentage.
%
%  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
%
% Output: regression, regression between the predictor data specified
%           by requestX and the measurements of interest specified by 
%           requestY.  This is a structure that contains the following 
%           fields:
%                regression.label, label of the run, see input labelRegressionRun
%                regression.approach, version of the algorithm used
%                regression.originalDatabase, label of the anthropometric 
%                          database used 
%                regression.XUheadings, headings of the predictor data
%                regression.YUheadings, headings of the measurements of 
%                          interest
%                regression.CUheadings, headings of the criteria defining
%                          the populations
%                regression.CUheadingsTarget, same as CUheadings except
%                          that these are user defined labels instead 
%                regression.bin, description of the target bins
%                regression.binSource, dwescription of the bins, expressed
%                          with source headings,
%            regression.Q, regression.P, regression.YU_mean,
%            regression.B, regression.E, regression.psig, regression.sigQ,
%            regression.XU: regression parameters (see documentation of
%            the ParkinsonReed script 'help ParkinsonReed'.  Note that 
%            XU corresponds to the upsampled virtual population.)
%
% Inputs: - databaseDir, location of the dataset of the ANSUR data.
%         - Ldatasets, list of anthropometric datasets from which the
%              regressions are constructed.  This is in the format of 
%              a cell of strings (or just a single string if a single 
%              dataset is used).  For example, Ldataset{1}='ANSUR'.
%         - sourceBin, description of the source population bins.   
%              sourceBin{j} corresponds to the j-th bin; 
%              sourceBin{j}.Ncriteria is the number of criteria defining 
%              the bin; sourceBin{j}.criteria{k} is the name of 
%              the k-th criteria; sourceBin{j}.value{k} is the value of 
%              the k-th criteria.  The number and criteria fields must
%              be identical over all the bins.
%         - targetBin, description of the target population bins.   
%              This has the same format as sourceBin plus a "percentage"
%              field.  
%              targetBin{j} corresponds to the j-th bin; 
%              targetBin{j}.Ncriteria is the number of criteria defining 
%              the bin; targetBin{j}.criteria{k} is the name of 
%              the k-th criteria; targetBin{j}.value{k} is the value of 
%              the k-th criteria; targetBin{k}.percentage is the percentage
%              of the bin in the population. 
%         - requestX, requestY, requestC, column numbers or  label of data 
%              (cell of strings of characters) of the (ANSUR) data  
%              respectively, as predictors, measurements of interest, and 
%              criteria.  
%              RequestC must include the criteria used to define
%              the population.  
%         - labelRegressionRun, a (user defined) label for the analysis run.
%         - saveResults, tag that indicates if the evaluated regression is 
%              saved as a mat file named ['regression_',labelRegressionRun,'.mat']
%         - resultsDir, directory in which the mat file containing the 
%              regression information is saved. 
%         - useLandmarks, flag that indicates if bone landmarks are to be
%              used in conjunction with anthropometric measurements.  This is
%              only supported for the CCTanthro dataset of older subjects.
%              [default=No]
%         - maxIter, maximum number of iterations; [default=20]
%         - toler, relative difference between the Procrustes sums of
%               squares during iterations, used as tolerance criteria
%               to stop iterations, both during the rotations -step 2-
%               and scaling -step3-; [default=1e-6]
%
% Version 1.0 (based on "runParkinsonReed")
%
% Christophe Lecomte
% University of Southampton
% April 2016
%
% =======================================
% % Example
% % ========
% % a) allocation of path and parameters and ANSUR directory
% % ---------------------------------------------------------
%   piperCodesDir='Piper_Codes';    % update this directory location
%   cd piperCodesDir;
%   start
%
%   databaseDir=paramPerso('databaseDir'); 
%
% % b) target bin description (and equivalent source bin description)
% % --------------------------
%  popBinPercentage(1)=40;
%  popBinPercentage(2)=30;
%  popBinPercentage(3)=10;
%  popBinPercentage(4)=20;
%
% targetBin{1}.Ncriteria=2;
% targetBin{1}.criteria{1}='Gender';
% targetBin{1}.value{1}=   'Female';
% targetBin{1}.criteria{2}='Race';
% targetBin{1}.value{2}={  'White'};
% targetBin{1}.percentage=popBinPercentage(1);
% %
% targetBin{2}.Ncriteria=2;
% targetBin{2}.criteria{1}='Gender';
% targetBin{2}.value{1}=   'Female';
% targetBin{2}.criteria{2}='Race';
% targetBin{2}.value{2}={  'Black'};
% targetBin{2}.percentage=popBinPercentage(2);
% %
% targetBin{3}.Ncriteria=2;
% targetBin{3}.criteria{1}='Gender';
% targetBin{3}.value{1}=   'Female';
% targetBin{3}.criteria{2}='Race';
% targetBin{3}.value{2}={  'Hispanic'};
% targetBin{3}.percentage=popBinPercentage(3);
% %
% targetBin{4}.Ncriteria=2;
% targetBin{4}.criteria{1}='Gender';
% targetBin{4}.value{1}=   'Female';
% targetBin{4}.criteria{2}='Race';
% targetBin{4}.value{2}={  'Other'};
% targetBin{4}.percentage=popBinPercentage(4);
% %%
% %
% sourceBin{1}.Ncriteria=2;
% sourceBin{1}.criteria{1}='GENDER';
% sourceBin{1}.value{1}=   '2';
% sourceBin{1}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{1}.value{2}={  '1'};
% %
% sourceBin{2}.Ncriteria=2;
% sourceBin{2}.criteria{1}='GENDER';
% sourceBin{2}.value{1}=   '2';
% sourceBin{2}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{2}.value{2}={  '2'};
% %
% sourceBin{3}.Ncriteria=2;
% sourceBin{3}.criteria{1}='GENDER';
% sourceBin{3}.value{1}=   '2';
% sourceBin{3}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{3}.value{2}={  '3'};
% %
% sourceBin{4}.Ncriteria=2;
% sourceBin{4}.criteria{1}='GENDER';
% sourceBin{4}.value{1}=   '2';
% sourceBin{4}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
% sourceBin{4}.value{2}={  '4',...
%     '5',...
%     '6',...
%     '7',...
%     '8',...
%     '9',...
%     '**',...
%     '***',...
%     '9999' };
%
% % c) label for the regression and storage info
% % --------------------------------------------
% labelRegressionRun='demoPersoAnthroWP3';
% saveResults=1;
% resultsDir='Results';
%
% 
% % d) requested predictors and measurements
% % -----------------------------------------
% % 100: STATURE (1 for subject number + 99 for ANSUR code),
% % 124: WEIGHT (1 for subject number + 123 for ANSUR code),
% requestX=[100,124];
% %
% % following numbers x are all 1 for subject number + (x-1) for ANSUR code
% % -----------------------------------------------------------------------
% %  (please check numbers - higher numbers are the same as the ANSUR code)
% %
% % 81: 'NECK_CIRC-OVER_LARYNX' (1 for subject number + 80 for ANSUR code)
% % 3: 'ACROMION_HT' (1 for subject number + 2 for ANSUR code)
% % 5: 'ACR-RADL_LNTH' (etc...)  
% % 11: 'BIACROMIAL_BRTH'
% % 13: 'BIDELTOID_BRTH' (equivalent to "Shoulder breadth" in CAESAR data?)
% % 27: 'BUTT_KNEE_LNTH'
% % 36: 'CHEST_CIRC-BELOW_BUST_'
% % 61: 'HEAD_BRTH'
% % 63: 'HEAD_LNTH'
% % 67: 'HIP_BRTH_SITTING'
% % 74: 'KNEE_HT_-_SITTING'
% % 94: 'SITTING_HT'
% % 104: 'THIGH_CIRC-PROXIMAL' (alias THIGH_CIRC)
% % 107: 'THUMB-TIP_REACH'
% %
% requestY=[4, 3, 5, 11, 13, 27, 36, 61, 63, 67, 74, 94, 104, 107];
% %
% requestC={'GENDER','RACE_OF_SUBJECT-ANSUR88'};
% %
% % INFO ON CRITERIA FOR ANSUR DATA...
% %
% % 1262 - 1264 - RACE
% % RACE: RACIAL GROUPS WERE ASSIGNED THE FOLLOWING NUMERICAL CODES:
% % ======
% % 1 - WHITE / 
% % 2 - BLACK / 
% % 3 - HISPANIC / 
% % 4 - ASIAN / PACIFIC ISLANDER / 
% % 5 - AMERICAN INDIAN /
% % 6 - CARRIBEAN, INCLUDING: BERMUDAN, HAITIAN, JAMAICAN, AND WEST INDIAN / 
% % 7 - EAST INDIAN, INCLUDING: ASIAN INDIAN AND EAST INDIAN / 
% % 8- ARAB, INCLUDING: ARABIAN, EGYPTIAN, IRANIAN, AND IRAQIAN.
% % 9999- A CODE OF (9999) INDICATES N/A, MISSING OR ILLEGIBLE ENTRIES.
% % A ONE DIGIT CODE INDICATES A SINGLE RACE, E.G., (1) CORRESPONDS TO WHITE. 
% % A TWO TO FOUR DIGIT CODE INDICATES SUBJECT OF MIXED PARENTAGE, E.G., 
% %    (127) CORRESPONDS TO WHITE/BLACK/EAST INDIAN. IN ADDITION, THE ORDER  
% %    OF THE DIGITS CORRESPONDS DIRECTLY TO THE ORDER IN WHICH THE SUBJECT 
% %    INDICATED HIS OR HER MIXED PARENTAGE ON THE BIOGRAPHICAL DATA FORMS.
%
%  Ldatasets={'ANSUR'};
%  regression=persoAnthropo_WP3(databaseDir,Ldatasets,...
%                     sourceBin,targetBin,...
%                     requestX,requestY,requestC,...
%                     labelRegressionRun,saveResults,resultsDir);
% % ---------------------
% Ref [1] 1988 ANTHROPOMETRIC SURVEY U.S. ARMY - FEMALE WORKING DATA BASE
%
% See also: parkinsonReed, populationUpsample, multipleRegression,
%           loadANSURdata, XYCdataANSUR
%
%
if nargin<1
%   Seeks the address of the ANSUR directory among the PIPER parameters
   databaseDir=paramPerso('databaseDir');
end
%
if nargin<2
%   Seeks the address of the ANSUR directory among the PIPER parameters
   Ldatasets={'ANSUR'};
end
%
if ~iscell(Ldatasets)
    Ldatasets={Ldatasets};
end
%
if nargin<8
    % Label to the run for the identification of analysis and output...
    if ~useLandmarks
        labelRegressionRun='PRRun';
    else
        labelRegressionRun='OptiRun_withLandmarks';
    end
end
if nargin<9
    saveResults=0;
end
if nargin<10
    [resultsDir,errorCode,~]=paramPerso('resultsDir');
    if errorCode
        resultsDir='.';
        initParamPerso('resultsDir',resultsDir);
    end
end
if nargin<11
    [useLandmarks,errorCode,~]=paramPerso('useLandmarks');
    if errorCode
        useLandmarks='0';
        initParamPerso('useLandmarks',useLandmarks);
    end
end
%
if ~useLandmarks
    regressionApproach='multiLinear';
else
    regressionApproach='constrainedOptimisation';
end
%
if nargin<13
    toler=paramPerso('SSM_toler');
    if isempty(toler)
        toler=1e-6;
    end
end
%
if nargin<12
    maxIter=paramPerso('SSM_maxIter');
    if isempty(maxIter)
        maxIter=20;
    end
end
%
% 1) load source data
% --------------------
% load anthropometric data
loadMat=1;
useANSUR=find(strcmp(Ldatasets,'ANSUR'));
useSNYDER=sign(find(strcmp(Ldatasets,'SNYDER')));
useCCTANTHRO=sign(find(strcmp(Ldatasets,'CCTANTHRO')));
%
if useANSUR
   ANSURdir=paramPerso('ANSURdir');
   [ANSURdata,ANSURheading]=loadANSURdata(ANSURdir,loadMat);
end
%
if useSNYDER
   SNYDERdir=paramPerso('SNYDERdir');
   [SNYDERdata,SNYDERheading]=loadSNYDERdata(SNYDERdir,loadMat);
end
%
if useCCTANTHRO
   CCTANTHROdir=paramPerso('CCTANTHROdir');
   [CCTANTHROdata,CCTANTHROheading]=loadCCTANTHROdata(CCTANTHROdir,...
                                                               loadMat);
end
%
if sum([useANSUR,useSNYDER,useCCTANTHRO])>1
    error('The use of multiple anthropometric datasets is not yet supported')
else
    if useANSUR
        Adata=ANSURdata;
        Aheading=ANSURheading;
    end
    %
    if useSNYDER
        Adata=SNYDERdata;
        Aheading=SNYDERheading;
    end
    %
    if useCCTANTHRO
        Adata=CCTANTHROdata;
        Aheading=CCTANTHROheading;
    end
    %
end
% format requested data in X, Y, C arrays (predictor, detailed
%                                              measurements, criteria)
%
% 2) extract the useful data for the chosen regression analysis
% --------------------------------------------------------------
% (1. select and extract requested data; 2. put it in "X,Y,C" format ,i.e.
%    in the format of a list of predictors, measurements and criteria.)
if useLandmarks
    % make sure that the subject numbers/ID are kept for matching the 
    %                          landmark info
    if useCCTANTHRO
        requestC=myUniqueRowsStable(vertcat(requestC(:),{'Subject Number'}));
    else
        error('Unsupported dataset for additional landmarks')
    end
end
[XU,XUheadings,YU,YUheadings,CU,CUheadings]=...
    XYCdataANTHROPO(Adata,Aheading,...
    requestX,requestY,requestC);
%
% NOTE that some values may not be numeric at this stage (i.e. they are
%          unavailable).  Only subjects that have a full useful set of
%          data are preserved.  Here a "full useful" set of data refers
%          to regressions evaluated by groups of measurement of interest:
%          group 1 is the master group.  
%             Regression is built between the predictors (XU) and its 
%             measurements, YU(:,"group 1").  All subjects kept must have a 
%             full set of XU and and YU(:,"group 1") values.
%          group 0 is unused.
% 
%          groups 2, 3, etc... are slave groups.
%             They have regressions built between the 
%             values of "group 1" and theirs, say "group 2".
%             Therefore, for each of the slave groups, all subjects must
%             have their corresponding values non-zero.
%
% Kept subjects are evaluated and selected after the identification of 
%    groups below...
%
if useLandmarks
    if useCCTANTHRO
        CCTLANDMARKSdir=paramPerso('CCTLANDMARKSdir');
        CCTCIRCUMFdir=paramPerso('CCTCIRCUMFdir');
        [X,dataBP,landmarkCodesXRows,subjectSRNXCols,...
        A_circ,dataBP_circ]=loadCCTANTHROlandmarks(CCTLANDMARKSdir,...
                                                     CCTCIRCUMFdir);
    else
        error('The use of landmarks is only supported with CCTANTHRO')
    end
end
%          
% 3) Apply the regression approach   
% ---------------------------------
% Parkinson-Reed approach
% -----------------------
% (1. generates a virtual population; 2. build a multiple regression model)
% (New steps to work with a master set of measures of interest, together 
%     with one or more slave sets of measures of interest.)
% groupY contains a list of group codes for the measures of interest, YU
%     groupY.referenceCode contains the reference codes (that should match
%                                   values in YUheadings)
%     groupY.groupNo contains the group numbers (1 is the master group
%                 for which regression on predictors, XU, are evaluated)
%
if useANSUR
    [Adata,Aheading]=loadANSURreference(ANSURdir);
    colReference=find(strcmp(Aheading,'Reference Code, ANSUR88'));
    groupY.dataset='ANSUR';
elseif useSNYDER
    [Adata,Aheading]=loadSNYDERreference(SNYDERdir); 
    colReference=find(strcmp(Aheading,'Reference Code, SNYDER77'));
    groupY.dataset='SNYDER';
elseif useCCTANTHRO
    [Adata,Aheading]=loadCCTANTHROreference(CCTANTHROdir); 
    colReference=find(strcmp(Aheading,'Reference Code, CCTANTHRO17'));
    groupY.dataset='CCTANTHRO';
end
groupY.referenceCode=Adata(:,colReference);
colGroup=find(strcmp(Aheading,'Group'));
if isempty(colGroup)
    groupY.groupNo=ones(size(groupY.referenceCode));
else
    groupY.groupNo=Adata(:,colGroup);
end
if iscell(groupY.groupNo)
  groupNo=zeros(size(groupY.groupNo));
  for j=1:length(groupNo)
    groupNo(j)=str2num(groupY.groupNo{j});
  end
  groupY.groupNo=groupNo;
end
%    
% Generate the regression for the master group and remove subjects with values 
%       (for this group of measurements) that are not numeric, etc. 
%
%
iGroup=1;       % master group
referenceGroup=groupY.referenceCode(groupY.groupNo==iGroup);
%
iKeptXfields=zeros(size(XUheadings));
for i=1:length(XUheadings)
  ifound= find(strcmp(referenceGroup,XUheadings{i}));
  if ~isempty(ifound)
      iKeptXfields(i)=1;
  end
end
iKeptXfields_Master=find(iKeptXfields);
%
jKeptYfields=zeros(size(YUheadings));
for j=1:length(YUheadings)
  jfound= find(strcmp(referenceGroup,YUheadings{j}));
  if ~isempty(jfound)
      jKeptYfields(j)=1;
  end
end
jKeptYfields_Master=find(jKeptYfields);
%
if ~useLandmarks
    % ParkinsonReed
    psig=0.05;      % threshold for significance
    [regression,~,~,~,iKeptSubjects]=...
                  ParkinsonReed_Wrapper(XU,XUheadings,...
                  YU(:,jKeptYfields_Master),YUheadings(jKeptYfields_Master),...
                  CU,CUheadings,targetBin,sourceBin,psig,labelRegressionRun,...
                  Ldatasets);
else
    % Adding landmark info and evaluating PC modes (for further PC score eval.)
    if useCCTANTHRO
        colSubjectID=find(strcmp(CUheadings,'Subject Number'));
        if ~isempty(colSubjectID)
            subjectSRNURows=CU(:,colSubjectID);
            for j=1:length(subjectSRNURows)
                subjectSRNURows{j}=num2str(subjectSRNURows{j});
                while (length(subjectSRNURows{j})<3)
                    subjectSRNURows{j}=strcat('0',subjectSRNURows{j});
                end
                subjectSRNURows{j}=strcat('LTE',subjectSRNURows{j});
            end    
        else
            error('The subject ID is not identified in the ANTHROPO dataset')
        end
    else
        error('Landmark anthropometric is only supported with CCTANTHRO')
    end
    % %     [regression,XU_out,YU_out,CU_out,iKeptSubjects]=...
    [regression,~,~,~,iKeptSubjects]=...
                  RegressionOpti_Wrapper(XU,XUheadings,...
                  YU(:,jKeptYfields_Master),YUheadings(jKeptYfields_Master),...
                  CU,CUheadings,subjectSRNURows,targetBin,sourceBin,...
                  labelRegressionRun,Ldatasets,...                  
                  X,dataBP,landmarkCodesXRows,subjectSRNXCols,...
                  A_circ,dataBP_circ,...
                  maxIter,toler);
    %              
end
%
% Loop on slaves...
%
iSlave=0;
for iGroupMinus1=1:max(groupY.groupNo)-1
  iGroup=iGroupMinus1+1;       % master group
  referenceGroup=groupY.referenceCode(groupY.groupNo==iGroup);
  %
  iKeptXfields=zeros(size(XUheadings));
  for i=1:length(XUheadings)
    ifound= find(strcmp(referenceGroup,XUheadings{i}));
    if ~isempty(ifound)
        iKeptXfields(i)=1;
    end
  end
  iKeptXfields_Slave=find(iKeptXfields);
  %
  jKeptYfields=zeros(size(YUheadings));
  for j=1:length(YUheadings)
    jfound= find(strcmp(referenceGroup,YUheadings{j}));
    if ~isempty(jfound)
        jKeptYfields(j)=1;
    end
  end
  jKeptYfields_Slave=find(jKeptYfields);
  %
  if ~isempty(jKeptYfields_Slave)
    iSlave=iSlave+1;
    XU_Slave=[XU(:,iKeptXfields_Master),XU(:,iKeptXfields_Slave),...
                                          YU(:,jKeptYfields_Master)];
    XUheadings_Slave=[XUheadings(iKeptXfields_Master),...
                                          XUheadings(iKeptXfields_Slave),...
                                          YUheadings(jKeptYfields_Master)];
    %
    disp(['DEBUG/TEST: iGroup:',num2str(iGroup)])
    if ~useLandmarks
       [regression_Slave,~,~,~,iKeptSubjects]=...
                   ParkinsonReed_Wrapper(XU_Slave,XUheadings_Slave,...
                   YU(:,jKeptYfields_Slave),YUheadings(jKeptYfields_Slave),...
                   CU,CUheadings,targetBin,sourceBin,psig,labelRegressionRun,...
                   Ldatasets);
    %
    else
    % %     [regression,XU_out,YU_out,CU_out,iKeptSubjects]=...
       [regression,~,~,~,iKeptSubjects]=...
                  RegressionOpti_Wrapper(XU_Slave,XUheadings_Slave,...
                  YU(:,jKeptYfields_Slave),YUheadings(jKeptYfields_Slave),...
                  CU,CUheadings,subjectSRNURows,targetBin,sourceBin,...
                  labelRegressionRun,Ldatasets,...                  
                  X,dataBP,landmarkCodesXRows,subjectSRNXCols,...
                  A_circ,dataBP_circ,...
                  maxIter,toler);
    %              
    end
    regression.Slave{iSlave}.group=iGroup;
    regression.Slave{iSlave}.regression=regression_Slave;
  end
end
%
if saveResults
    if ~exist(resultsDir,'dir')
        mkdir(resultsDir);
    end
    save(strcat(resultsDir,filesep,'regression_',labelRegressionRun,'.mat'),'regression','-mat');
end
%
%
%
