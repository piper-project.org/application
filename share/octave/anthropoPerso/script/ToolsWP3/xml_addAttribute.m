% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [root,indAttribute]=xml_addAttribute(root,attributeName)
%
%
%
if isfield(root,'attribute')
    indAttribute=[];
    for j=1:length(root.attribute)
        if strcmp(root.attribute{j}.name,attributeName)
            indAttribute=j;
        end
    end
    %
    if isempty(indAttribute)
        root.attribute{end+1}=struct;
        root.attribute{end+1}.name=attributeName;
    end
else
    root.attribute=cell(1,0);
    root.attribute{1}=struct;
    root.attribute{1}.name=attributeName;
    indAttribute=1;
end
