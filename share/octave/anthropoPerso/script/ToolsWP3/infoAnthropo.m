% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [fileName,refInfo]=infoAnthropo(dataset,DatabaseDir,CodeDir)
%
% function fileName=infoAnthropo(dataset,DatabaseDir);
%          [fileName,refInfo]=infoAnthropo(dataset,DatabaseDir,CodeDir);
%
% Returns the adddress of a file that contains information about the
%    SNYDER dataset.
%
% Input: - dataset, name of the dataset.  Supported values: 'ANSUR', 'SNYDER'
%        - DatabaseDir, adddress of the statistical database
%        - CodeDir, address of the statistical tools that include the
%                       anthropometric tools
% Output: - fileName, address of the reference file that contains the
%              information about the fields of the SNYDER dataset.
%              Data is stored in CSV (ascii) format.  The first rows
%              contains the headings that include
%                "Reference Code, SNYDER88": codes of measurements
%                "Unit" as mm, hg, etc.
%                "Posture" as standingSNYDER88 or sittingSNYDER88
%                "Description": a description of the measurements
%                "Support WP3": a flag that indicates if the measurement
%                             is supported as a WP3 target
%         - refInfo, information from the 'fileName' reference file saved 
%                in a structure format.
%
% %
% % Please uncomment the lines below and comment the first line of this 
% %       file if the script is called from the PIPER application
% arg_list = argv();
% dataset=arg_list{1};
% DatabaseDir=arg_list{2};
% CodeDir=arg_list{3};
%
if strcmp(dataset,'ANSUR')
    fileName=[DatabaseDir,filesep,'Dataset_Ansur_1989',filesep,...
        'Reference_ansur_withBMI.csv'];
elseif strcmp(dataset,'SNYDER')
    fileName=[DatabaseDir,filesep,'Dataset_Snyder_1977',filesep,...
        'Reference_snyder_withBMI.csv'];
else
    error('The dataset is not supported')
end
%
if nargout>=2
    % This is to access myCSVread.m
    if nargin>=3
        addpath([CodeDir,filesep,'ToolsDatabase'])
    end
    %
    nHeadRows=1;
    [Adata,Aheading]=myCSVread(fileName,nHeadRows);
    %
    Afields=strrep(Aheading,' ','');
    Afields=strrep(Afields,',','_');
    %
    refInfo=struct;
    %
    for j=1:length(Afields)
        refInfo.(Afields{j})=Adata(:,j);
    end
    %
end
%
%
