% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function []=myCSVwrite(fileName,Adata,Aheading,separator,alphaBnd)
%
% function []=myCSVwrite(fileName,Adata,Aheading,separator,alphaBnd);
%
% Input: fileName, name of the input file (string of characters).
%        Adata, data to be written in the output file, in the format of a
%           cell array of strings.
%           Adata(4,7) contains the 7h item of information stored in
%           the 4th row of data (4+nHeadRows)th row of the output file.
%        Aheading, items in the first rows of the input file,
%           in the format of cell array of strings.
%           Aheading(2,5) contains the 5th item on the second heading row.
%        separator, separator used to separate items of a row. [Def.=',']
%        alphaBnd, delimitor of chain of characters if any. [Default='"'].
%
% Christophe Lecomte, Sep.2016
% Work funded by the EU FP7 PIPER project
% University of Southampton
%
%
if nargin<4
    separator=',';
end
%
if nargin<5
    alphaBnd='"';
end
%
fid=fopen(fileName, 'w');
%
for j=1:size(Aheading,1)
    newElement=Aheading{j,1};
    if strfind(newElement,separator)
        newElement=[alphaBnd,newElement,alphaBnd];
    end
    tline=newElement;    
    for k=2:size(Aheading,2)
        newElement=Aheading{j,k};
        if strfind(newElement,separator)
            newElement=[alphaBnd,newElement,alphaBnd];
        end
        tline=[tline,separator,newElement];
    end
    tline=[tline];
    fprintf(fid,'%s\n',tline);
end
%
for j=1:size(Adata,1)
    data=Adata{j,1};
    if isnumeric(data)
        data=num2str(data);
    elseif strfind(data,separator)
        data=[alphaBnd,data,alphaBnd];
    end
    tline=data;
    for k=2:size(Adata,2)
        data=Adata{j,k};
        if isnumeric(data)
            data=num2str(data);
        elseif strfind(data,separator)
            data=[alphaBnd,data,alphaBnd];
        end
        tline=[tline,separator,data]; %#ok<*AGROW>
    end
    fprintf(fid,'%s\n',tline);
end
%
fclose(fid);
%
%
%
