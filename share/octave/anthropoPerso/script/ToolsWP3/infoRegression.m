% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
%function infoRegression(filenameRegression,filenameInfo)
%
% function infoRegression(filenameRegression,filenameInfo);
%
% Exports regression information 
%
% filenameRegression is the name of the mat file containing the regression
%                information
% filenameInfo is the name of a CSV file in which information will be
%                written
%
% % Please uncomment the lines below and comment the first line of this
% %       file if the script is called from the PIPER application
 arg_list = argv();
 filenameRegression=arg_list{1};
 filenameInfo=arg_list{2};
%
% C.Lecomte
% University of SOuthampton
% October 2016
% Work funded by the EU FP7 PIPER project 
%
%
load(filenameRegression)
%
XUheadings=regression.XUheadings;
YUheadings=regression.YUheadings;
%
nX=length(XUheadings);
nY=length(YUheadings);
%
A=cell(nX+nY,3);
%
A(:,1)={'RegressionInformation'}
A(1:nX,2)={'Predictor'};
A(nX+1:nX+nY,2)={'MeasurementOfInterest'};
%
for j=1:nX
    A(j,3)=XUheadings(j);
end
%
for j=1:nY
    A(j+nX,3)=YUheadings(j);
end
%
myCSVwrite(filenameInfo,A,{});
%