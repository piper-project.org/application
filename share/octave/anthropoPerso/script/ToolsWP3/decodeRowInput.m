function Input=decodeRowInput(rowInput,Input)
%
% ============================================================================== 
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% ============================================================================== 
%
continueCheck=1;
while continueCheck
    if length(rowInput)==0
        continueCheck=0;
    else
        if isempty(rowInput{end})
            rowInput=rowInput(1:end-1);
        else
            continueCheck=0;
        end
    end
end
%
if length(rowInput)>=1
    %%
    if strcmp(rowInput{1},'Parameter')
        %
        if length(rowInput)~=3
            error('Check Parameter row of input file')
        end
        %
        if strcmp(rowInput{2},'CodeDir')
            Input.CodeDir=rowInput{3};
        elseif strcmp(rowInput{2},'DatabaseDir')
            Input.DatabaseDir=rowInput{3};
        elseif strcmp(rowInput{2},'RegressionDir')
            Input.RegressionDir=rowInput{3};
        elseif strcmp(rowInput{2},'SampleDir')
            Input.SampleDir=rowInput{3};
        elseif strcmp(rowInput{2},'ResultsDir')
            Input.ResultsDir=rowInput{3};
        elseif strcmp(rowInput{2},'useLandmarks')
            Input.useLandmarks=rowInput{3};
        elseif strcmp(rowInput{2},'SSM_maxIter')
            Input.SSM_maxIter=rowInput{3};
        elseif strcmp(rowInput{2},'SSM_toler')
            Input.SSM_toler=rowInput{3};
        elseif strcmp(rowInput{2},'SSM_scaleCirc')
            Input.SSM_scaleCirc=rowInput{3};
        elseif strcmp(rowInput{2},'SSM_scaleXP')
            Input.SSM_scaleXP=rowInput{3};
        end
        %
        %%
    elseif strcmp(rowInput{1},'RegressionInformation')
        %
        if length(rowInput)<2
            error('Check RegressionInformation row of input file')
        end
        %
        if strcmp(rowInput{2},'AnthropometricDataset')
            if length(rowInput)~=3
                error('Check AnthropometricDataset row of input file')
            end
            Input.AnthropometricDataset=rowInput{3};
            %
        elseif strcmp(rowInput{2},'RegressionMatFilename')
            if length(rowInput)~=3
                error('Check RegressionMatFilename row of input file')
            end
            Input.RegressionMatFilename=rowInput{3};
            %
        elseif strcmp(rowInput{2},'PopulationMatFilename')
            if length(rowInput)~=3
                error('Check PopulationMatFilename row of input file')
            end
            Input.PopulationMatFilename=rowInput{3};
            %
        elseif strcmp(rowInput{2},'Predictor')
            if length(rowInput)~=3
                if length(rowInput)~=4
                   error('Check Predictor row of input file')
                elseif ~strcmp(rowInput{4},'Internal')
                   error('Check 4th item in Predictor row of input file')
                else
                   flagInternal=1;
                end
            else
               flagInternal=0;
            end
            if isfield(Input,'Predictor')
                Input.Predictor{end+1}=rowInput{3};
                Input.Predictor_InternalFlag(end+1)=flagInternal;
            else
                Input.Predictor{1}=rowInput{3};
                Input.Predictor_InternalFlag(1)=flagInternal;
            end
            %
        elseif strcmp(rowInput{2},'MeasurementOfInterest')
            if length(rowInput)~=3
                if length(rowInput)~=4
                   error('Check MeasurementOfInterest row of input file')
                elseif ~strcmp(rowInput{4},'Internal')
                   error('Check 4th item in MeasurementOfInterest row of input file')
                else
                   flagInternal=1;
                end
            else
               flagInternal=0;
            end
            if isfield(Input,'MeasurementOfInterest')
                Input.MeasurementOfInterest{end+1}=rowInput{3};
                Input.MOI_InternalFlag(end+1)=flagInternal;
            else
                Input.MeasurementOfInterest{1}=rowInput{3};
                Input.MOI_InternalFlag(1)=flagInternal;
            end
            %
            %
        elseif strcmp(rowInput{2},'PopulationDescriptor')
            if length(rowInput)<3
                error('Check MeasurementOfInterest row of input file')
            end
            if strcmp(rowInput{3},'NumberOfBins')
                if length(rowInput)~=4
                    error('Check NumberOfBins row of input file')
                end
                Input.Population.NBins=str2num(rowInput{4});
            elseif strcmp(rowInput{3},'Bin')
                if length(rowInput)<8
                    error('Check Bin row of input file')
                end
                BinNo=str2num(rowInput{4});
                if ~strcmp(rowInput{5},'percentage')
                    error('Check precentage row of input file')
                else
                    percentage=str2num(rowInput{6});
                end
                Input.Population.Bin{BinNo}.percentage=percentage;
                %
                if ~strcmp(rowInput{7},'NCriteria')
                    error('Check NCriteria row of input file')
                else
                    NCriteria=str2num(rowInput{8});
                end
                Input.Population.Bin{BinNo}.NCriteria=NCriteria;
                %
                if length(rowInput)~=(8+NCriteria*4)
                    error('Check length of Bin row of input file')
                end
                %
                for j=1:NCriteria
                    if ~strcmp(rowInput{9+(j-1)*4},'criteria')
                        error('Check Bin (criteria) row of input file')
                    else
                        Input.Population.Bin{BinNo}.criteriaName{j}=...
                            rowInput{10+(j-1)*4};
                    end
                    if ~strcmp(rowInput{11+(j-1)*4},'value')
                        error('Check Bin (value) row of input file')
                    else
                        Input.Population.Bin{BinNo}.criteriaValue{j}=...
                            rowInput{12+(j-1)*4};
                    end
                end
            end
        end
    elseif strcmp(rowInput{1},'SampleInformation')
        if length(rowInput)<2
            error('Check SampleInformation row of input file')
        end
        %
        if strcmp(rowInput{2},'SampleOutputType')
            if length(rowInput)~=3
                error('Check SampleOutputType row of input file')
            end
            listSampleOutputType={rowInput{3}};
            j=findstr(listSampleOutputType{1},',');
            nlist=1;
            while ~isempty(j)
                j=j(1);
                listSampleOutputType{nlist+1}=...
                    listSampleOutputType{nlist}(j+1:end);
                listSampleOutputType{nlist}=...
                    listSampleOutputType{nlist}(1:j-1);
                j=findstr(listSampleOutputType{nlist+1},',');
            end
            %
            if ~isfield(Input,'SampleOutputType')
                Input.SampleOutputType=listSampleOutputType;
            else
                for j=1:length(listSampleOutputType)
                    k=find(strcmp(Input.SampleOutputType,...
                        listSampleOutputType{j}));
                    if isempty(k)
                        Input.SampleOutputType{end+1}=...
                            listSampleOutputType{j};
                    end
                end
            end
            %
            %
        elseif strcmp(rowInput{2},'SampleMeanOnlyXMLFilename')
            if length(rowInput)~=3
                error('Check SampleMeanOnlyXMLFilename row of input file')
            end
            Input.SampleMeanOnlyXMLFilename=rowInput{3};
            %
        elseif strcmp(rowInput{2},'SampleSampledOutputXMLFilename')
            if length(rowInput)~=3
                error('Check SampleSampledOutputXMLFilename row of input file')
            end
            Input.SampleSampledOutputXMLFilename=rowInput{3};
            %
            %
        elseif strcmp(rowInput{2},'SampleInputType')
            if length(rowInput)~=3
                error('Check SampleInputType row of input file')
            end
            listSampleInputType={rowInput{3}};
            %
            Input.SampleInputType=listSampleInputType;
            %
            %
        elseif strcmp(rowInput{2},'SampleInputValues')
            j=1;            
            while length(rowInput)>j*2
                if length(rowInput)==j*2+1
                    error('Check SampleInputValue row')
                end
                Input.FixedPredictor.Name{j}=rowInput{j*2+1};
                Input.FixedPredictor.Value{j}=rowInput{j*2+2};
                %
                j=j+1;
            end   
        elseif strcmp(rowInput{2},'SampleOutputPDFType')
            if length(rowInput)~=3
                error('Check SampleOutputPDFType row of input file')
            end
            Input.SampleOutputPDFType=rowInput{3};
            %
        elseif strcmp(rowInput{2},'SampleInputPDFType')
            if length(rowInput)~=3
                error('Check SampleInputPDFType row of input file')
            end
            Input.SampleInputPDFType=rowInput{3};
            %
        elseif strcmp(rowInput{2},'SampleInputReferencePopulationMatFilename')
            if length(rowInput)~=3
                error('Check SampleInputReferencePopulationMatFilename row of input file')
            end
            Input.SampleInputReferencePopulationMatFilename=rowInput{3};
            %
        elseif strcmp(rowInput{2},'PopulationMatFilename')
            if length(rowInput)~=3
                error('Check PopulationMatFilename row of input file')
            end
            Input.SamplePopulationMatFilename=rowInput{3};
            %
        elseif strcmp(rowInput{2},'NbInputSamples')
            if length(rowInput)~=3
                error('Check NbInputSamples row of input file')
            end
            Input.NbInputSamples=rowInput{3};
            %
        elseif strcmp(rowInput{2},'NbOutputSamples')
            if length(rowInput)~=3
                error('Check NbOutputSamples row of input file')
            end
            Input.NbOutputSamples=rowInput{3};
            %
            %
        end
        %
    end
    %
end
%
