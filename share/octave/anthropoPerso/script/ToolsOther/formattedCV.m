% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function listCV=formattedCV(listCV)
%
% Formats a list of Criteria Values (CV) so that:
%      - the returned list is a cell list;
%      - duplicates are suppressed from the list;
%      - any string with commas is splitted into individual strings at the
%         level of the commas.
%
% See Also: formattedBP
%
% Christophe Lecomte
% Jan. 2016
% University of Southampton
% EU FP7 project PIPER
%
if ischar(listCV)
    listCV={listCV};
end
%
[listCV,order,~]=unique(listCV);
[~,ind]=sort(order);
listCV=listCV(ind);
%
j=0;
while j<length(listCV)
    CV=listCV{j+1};
    %
    icomma=strfind(CV,',');
    if ~isempty(icomma)
        realCV=CV(1:icomma(1)-1);
        remainingCV=CV(icomma(1)+1:end);
        listCVbefore=listCV(1:j);
        listCVafter=listCV(j+2:end);
        %
        [listCV,order,~]=unique([listCVbefore(:).',...
            {realCV},listCVafter(:).',{remainingCV}]);
        [~,ind]=sort(order);
        listCV=listCV(ind);
    else
        j=j+1;
    end
end
%
end
%
% =================
%
function listCV=addCV(listCV,varargin)
%

% The three (active) lines of code below achieve the same as the 
%     commented line in Matlab.  The 'stable' option is however not 
%     supported in Octave.
% listCV=unique([listCV(:).',varargin(:).'],'stable');
% =>
    [listCV,order,~]=unique([listCV(:).',varargin(:).']);
    [~,ind]=sort(order);
    listCV=listCV(ind);
%
end
%
% =================
