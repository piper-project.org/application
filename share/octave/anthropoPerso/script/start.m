function [errorCode,errorMessage]=start(piperRoot)
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ========================================================================
%
% function [errorCode,errorMessage]=start(piperRoot);
%
% Start the personalisation PIPER code by initialising the (global)
%     parameters and updating the octave/matlab path.
%
% Please update this script or create a similar one to assign your own 
%     parameters.
%
% Input: piperRoot, address of the PIPER codes. 
% Output: errorCode, return error code.  If errorCode~=0, a problem occured
%            and an error message is returned in "errorMessage".
%         errorMessage, error message returned if an error occured.
%
% Example:
% ========
% [~,hostname]= system('hostname');
% %
% if strfind(hostname,'UOS')
%     piperRoot=['C:',filesep,'local',filesep,'LecomtePiper',filesep,...
%                                                     'Piper_CodesOPEN'];
% elseif strfind(hostname,'Darios')
%     piperRoot=['C:',filesep,'Data',filesep,'Clocal',filesep,...
%                                                      'LecomtePiper'];
% elseif strfind(hostname,'DESKTOP-MOJG0P2')
%     piperRoot=['E:',filesep,'Data',filesep,'Clocal',filesep,...
%                                                      'LecomtePiper'];
% elseif strfind(hostname,'DESKTOP-43EQ0O2')
%     piperRoot=['C:',filesep,'Data',filesep,'Clocal',filesep,...
%                                                      'LecomtePiper'];
% else
%     error('Unsupported host for this script')
% end
% %
% start(piperRoot)
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of Southampton
%
%
% There are two steps:
% step 1: assign the values of the parameters (see paramPerso for a full 
%                                                                  list)
% step2: add the path, using the "addpathPerso" script
%
chdir('ToolsParam');
% Step 1 (update this part to assign your own parameter values):
%
initParamPerso_Xtof(piperRoot);
% Step 2
[errorCode,errorMessage]=addpathPerso();
if errorCode
    error(errorMessage);
end
%
cd ..
%