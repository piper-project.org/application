% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
%
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
%
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
%
% Contributors include Christophe Lecomte (University of Southampton)
%
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
function [Ys,Ysheadings]=sampleRegression_Slaves(regression,Xs,...
    Xsheadings,varargin)
%
%  function [Ys,Ysheadings]=sampleRegression_Slaves(regression,Xs,...
%                         Xsheadings,averageOnly);
%
%  sample a regression as in sampleRegression
%  then do the same for each of the slave regressions (regression.Slave{j}'s)
%  using only the predictors available
%
%  See sampleRegression, persoAnthropo_WP3, ParkinsonReed_Wrapper
%
%  Christophe Lecomte
%  University of Southampton
%  January 2017
%  Work funded by the EU FP7 PIPER project
%

% Keep track of all predictor variables to make sure that there is a one to one
%      correspondance between those in the regression and those in Xs
treatedXs=zeros(size(Xsheadings));
%
% First, deal with the master regression
XUheadings_Master=regression.XUheadings;
Xs_Master=zeros(size(Xs,1),length(XUheadings_Master));
for j=1:length(XUheadings_Master)
    index_Xs=find(strcmp(Xsheadings,XUheadings_Master{j}));
    if isempty(index_Xs)
        error('Not all predictor values are specified when sampling the regression')
    end
    treatedXs(index_Xs)=1;
    Xs_Master(:,j)=Xs(:,index_Xs);
    %
end
%
regression_MasterOnly=regression;
if isfield(regression_MasterOnly,'Slave')
    regression_MasterOnly=rmfield(regression_MasterOnly,'Slave');
end
if strfind(regression.approach,'PR_v')==1
    %   "ParkinsonReed" approach (first PCA, then multiple regressions)
    [Ys_Master,Ysheadings_Master,Qs,Es]=...
        sampleRegression(regression_MasterOnly,...
        Xs_Master,XUheadings_Master,varargin{:});
elseif  strfind(regression.approach,'RegOpti_v')==1
    %   "Regression by optimisation" approach
    [PCdeficiency,errorCode]=paramPerso('PCdeficiency');
    if errorCode
        PCdeficiency=0;
        initParamPerso('PCdeficiency',PCdeficiency);
    end
    [Ys_Master,Ysheadings_Master,...
        Xs_actual_Master]=...
        sampleRegressionOpti(regression_MasterOnly,Xs_Master,...
        XUheadings_Master,PCdeficiency,varargin{:});
else
    error('Unsupported regression type.')
end
%
Ys=Ys_Master;
Ysheadings=Ysheadings_Master;
%
if isfield(regression,'Slave')
    % Then, loop through all slave regressions.
    for iSlave=1:length(regression.Slave)
        regression_Slave=regression.Slave{iSlave}.regression;
        %
        XUheadings_Slave=regression_Slave.XUheadings;
        Xs_Slave=zeros(size(Xs,1),length(XUheadings_Slave));
        for j=1:length(XUheadings_Slave)
            index_Xs=find(strcmp(Xsheadings,XUheadings_Slave{j}));
            if isempty(index_Xs)
                index_Ys_Master=find(strcmp(Ysheadings_Master,XUheadings_Slave{j}));
                if isempty(index_Ys_Master)
                    error('Not all predictor values are specified when sampling the regression')
                else
                    Xs_Slave(:,j)=Ys_Master(:,index_Ys_Master);
                end
            else
                treatedXs(index_Xs)=1;
                Xs_Slave(:,j)=Xs(:,index_Xs);
            end
            %
            [Ys_Slave,Ysheadings_Slave]=...
                sampleRegression(regression_Slave,...
                Xs_Slave,XUheadings_Slave,varargin{:});
        end
        %
        Ys=[Ys,Ys_Slave];
        Ysheadings={Ysheadings{:},Ysheadings_Slave{:}};
    end
end
%
if sum(~treatedXs)
    warning('Some specified predictor values are not used in the regression')
end
%
%
