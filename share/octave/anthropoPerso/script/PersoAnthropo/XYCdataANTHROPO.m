% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [XU,XUheadings,YU,YUheadings,CU,CUheadings]=...
    XYCdataANTHROPO(ANTHROPOdata,ANTHROPOheading,...
    requestX,requestY,requestC)
% XYCdataANTHROPO - Change the anthropometric data, ANTHROPO, into X Y C 
%     format, i.e. predictor, detailed data, and criteria for regression
%     analysis and virtual population construction (Parkinson Reed method).
%
% [XU,XUheadings,YU,YUheadings,CU,CUheadings]=...
%                XYCdataANTHROPO(ANTHROPOdata,ANTHROPOheading,...
%                requestX,requestY,requestC);
%
% Input: - ANTHROPOdata, actual array of data, one row per subject, one 
%          column per data type.
%        - ANTHROPOheading, label (data type description) of each column.
%        - requestX, column numbers of data requested as predictors.
%               Alternatively, label of data requested (cell of strings of 
%               characters).
%        - requestY, column numbers of data requested as detailed measurements.
%               Alternatively, label of data requested (cell of strings of 
%               characters).
%        - requestC, column numbers of data requested as population criteria.
%               Alternatively, label of data requested (cell of strings of 
%               characters).
% Output: - XU, predictor values, one row per subject, one column per data
%                    type.
%         - XUheadings, label (data type description) of each column of XU.
%         - YU, detailed measurements, one row per subject, one column per
%                    data type.
%         - YUheadings, label (data type description) of each column of YU.
%         - CU, criteria, one row per subject, one column per
%                    data type.
%         - CUheadings, label (data type description) of each column of CU.
%
% Christophe Lecomte, 2016
% University of Southampton
% Work funded by the FP7 PIPER project
%
%
for j=1:length(requestX)
    if ~isnumeric(requestX(j))
        
    end
end
NU=size(ANTHROPOdata,1);
if iscell(requestX)
    XU=zeros(NU,length(requestX));
    XUheadings=cell(1,length(requestX));
    foundLabel=zeros(1,length(requestX));
    for iX=1:length(requestX)
        Label=requestX{iX};
        for j=1:length(ANTHROPOheading)
            if strcmp(Label,ANTHROPOheading{j})
                XUheadings{iX}=Label;
                for k=1:size(ANTHROPOdata,1)
                    XU(k,iX)=ANTHROPOdata(k,j);
                end
                foundLabel(iX)=j;
            end
        end
        if ~foundLabel(iX)
            error(['Unsupported label ''',Label,'''.  Please check.'])
        end
    end
else
    XU=ANTHROPOdata(1:NU,requestX);   % predictors,  ANTHROPO data
    XUheadings=ANTHROPOheading(:,requestX);
end
%
%
%
if iscell(requestY)
    YU=zeros(NU,length(requestY));
    YUheadings=cell(1,length(requestY));
    foundLabel=zeros(1,length(requestY));
    for iY=1:length(requestY)
        Label=requestY{iY};
        for j=1:length(ANTHROPOheading)
            if strcmp(Label,ANTHROPOheading{j})
                YUheadings{iY}=Label;
                for k=1:size(ANTHROPOdata,1)
                    YU(k,iY)=ANTHROPOdata(k,j);
                end
                foundLabel(iY)=j;
            end
        end
        if ~foundLabel(iY)
            error(['Unsupported label ''',Label,'''.  Please check.'])
        end
    end
else
    YU=ANTHROPOdata(1:NU,requestY);  % detailed anthropometric measures, ANTHROPO
    YUheadings=ANTHROPOheading(:,requestY);
end
%
%
%
if iscell(requestC)
    CU=cell(NU,length(requestC));
    CUheadings=cell(1,length(requestC));
    foundCrit=zeros(1,length(requestC));
    for iC=1:length(requestC)
        Crit=requestC{iC};
        for j=1:length(ANTHROPOheading)
            if strcmp(Crit,ANTHROPOheading{j})
                CUheadings{iC}=Crit;
                for k=1:size(ANTHROPOdata,1)
                    CU{k,iC}=num2str(ANTHROPOdata(k,j));
                end
                foundCrit(iC)=j;
            end
        end
        if ~foundCrit(iC)
            error(['Unsupported criteria ''',Crit,'''.  Please check.'])
        end
    end
else
    CU=ANTHROPOdata(1:NU,requestC);  % detailed anthropometric measures, ANTHROPO
    CUheadings=ANTHROPOheading(:,requestC);    
end
%
%
%
