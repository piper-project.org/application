% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function regression=runParkinsonReed_ANSUR(ANSURdir,popBinPercentage,saveResults,resultsDir)
%
% function regression=runParkinsonReed_ANSUR;
%          regression=runParkinsonReed_ANSUR(ANSURdir,popBinPercentage,...
%                                  saveResults,resultsDir);
%
% Example of application Parkinson & Reed's approach (script "ParkinsonReed.m")
% using **ANSUR** data
%
% This demonstrates the generation of regressions of anthropometric 
%     measurements in terms of a few predictors/descriptors.
%
% The population is divided in four bins (subgroups) in this example:
%    Bin 1: Female, White  
%    Bin 2: Female, Black
%    Bin 3: Female, Hispanic
%    Bin 4: Female, Other
%  (this is hardcoded in the script as an example of call to parkinsonReed)
%
% Percentage of these bins in the virtual population can be chosen as
%    calling argument to the script.
%
% Similarly, the lists of predictors and measurements of interest are
%    hard-coded in the script (as an example of call to parkinsonReed) but 
%    can also easily be changed.
%
% Version 1.0 (based on "runParkinsonReed")
%
% Christophe Lecomte
% University of Southampton
% Jan 2016
%
% ----------------------
% Example
% --------
%   piperCodesDir='Piper_Codes';    % update this directory location
%   cd piperCodesDir;
%   start
%
%   ANSURdir=paramPerso('ANSURdir'); 
%
%     popBinPercentage(1)=40;
%     popBinPercentage(2)=30;
%     popBinPercentage(3)=10;
%     popBinPercentage(4)=20;
% 
%   regression=runParkinsonReed_ANSUR(ANSURdir,popBinPercentage);
%
% ---------------------
% Ref [1] 1988 ANTHROPOMETRIC SURVEY U.S. ARMY - FEMALE WORKING DATA BASE
%
% See also: parkinsonReed, populationUpsample, multipleRegression
%
%
if nargin<1
%   Seeks the address of the ANSUR directory among the PIPER parameters
   ANSURdir=paramPerso('ANSURdir');
end
%
if nargin<2
    popBinPercentage(1)=51.6;
    popBinPercentage(2)=41.8;
    popBinPercentage(3)=2.6;
    popBinPercentage(4)=4.0;
end
if nargin<3
    saveResults=0;
end
if nargin<4
    resultsDir='Results';
end
% 1) load source data
% --------------------
% load ANSUR data
firstRun=0;
[ANSURdata,ANSURheading]=loadANSURdata(ANSURdir,firstRun);
% format requested data in X, Y, C arrays (predictor, detailed
%                                              measurements, criteria)
%
% Gives a label to the run for the saving of data...
labelPRRun='PR_ANSUR';
%
% 100: STATURE (1 for subject number + 99 for ANSUR code),
% 167: BMI (1 for subject number + 166 for *derived* ANSUR code),
requestX=[100,167];
%
% following numbers are all 1 for subject number + 3 for ANSUR code:
% ------------------------------------------------------------------
% 4: 'ACR_HT-SIT' (1 for subject number + 3 for ANSUR code)
% 3: 'ACROMION_HT' (1 for subject number + 2 for ANSUR code)
% 5: 'ACR-RADL_LNTH' (etc...)  
% 11: 'BIACROMIAL_BRTH'
% 13: 'BIDELTOID_BRTH' (equivalent to "Shoulder breadth" in CAESAR data?)
% 27: 'BUTT_KNEE_LNTH'
% 36: 'CHEST_CIRC-BELOW_BUST_'
% 61: HEAD_BRTH
% 63: HEAD_LNTH
% 67: HIP_BRTH_SITTING
% 74: KNEE_HT_-_SITTING
% 94: SITTING_HT
% 104: THIGH_CIRC-PROXIMAL (alias THIGH_CIRC)
% 107: THUMB-TIP_REACH
%
requestY=[4, 3, 5, 11, 13, 27, 36, 61, 63, 67, 74, 94, 104, 107];
requestC={'GENDER','RACE_OF_SUBJECT-ANSUR88'};
[XU,XUheadings,YU,YUheadings,CU,CUheadings]=...
    XYCdataANTHROPO(ANSURdata,ANSURheading,...
    requestX,requestY,requestC);
%
% INFO ON CRITERIA FOR ANSUR DATA...
%
%
% 1262 - 1264 - RACE
% RACE: RACIAL GROUPS WERE ASSIGNED THE FOLLOWING NUMERICAL CODES:
% ======
% 1 - WHITE / 
% 2 - BLACK / 
% 3 - HISPANIC / 
% 4 - ASIAN / PACIFIC ISLANDER / 
% 5 - AMERICAN INDIAN /
% 6 - CARRIBEAN, INCLUDING: BERMUDAN, HAITIAN, JAMAICAN, AND WEST INDIAN / 
% 7 - EAST INDIAN, INCLUDING: ASIAN INDIAN AND EAST INDIAN / 
% 8- ARAB, INCLUDING: ARABIAN, EGYPTIAN, IRANIAN, AND IRAQIAN.
% 9999- A CODE OF (9999) INDICATES N/A, MISSING OR ILLEGIBLE ENTRIES.
% A ONE DIGIT CODE INDICATES A SINGLE RACE, E.G., (1) CORRESPONDS TO WHITE. 
% A TWO TO FOUR DIGIT CODE INDICATES SUBJECT OF MIXED PARENTAGE, E.G., 
%    (127) CORRESPONDS TO WHITE/BLACK/EAST INDIAN. IN ADDITION, THE ORDER  
%    OF THE DIGITS CORRESPONDS DIRECTLY TO THE ORDER IN WHICH THE SUBJECT 
%    INDICATED HIS OR HER MIXED PARENTAGE ON THE BIOGRAPHICAL DATA FORMS.
%
%
%
%
%
% 2) define target bins
% ----------------------
Nbins=4;
%
CUheadingsTarget={'Gender','Race'};
targetBin{1}.Ncriteria=2;
targetBin{1}.criteria{1}='Gender';
targetBin{1}.value{1}=   'Female';
targetBin{1}.criteria{2}='Race';
targetBin{1}.value{2}={  'White'};
targetBin{1}.percentage=popBinPercentage(1);
%
targetBin{2}.Ncriteria=2;
targetBin{2}.criteria{1}='Gender';
targetBin{2}.value{1}=   'Female';
targetBin{2}.criteria{2}='Race';
targetBin{2}.value{2}={  'Black'};
targetBin{2}.percentage=popBinPercentage(2);
%
targetBin{3}.Ncriteria=2;
targetBin{3}.criteria{1}='Gender';
targetBin{3}.value{1}=   'Female';
targetBin{3}.criteria{2}='Race';
targetBin{3}.value{2}={  'Hispanic'};
targetBin{3}.percentage=popBinPercentage(3);
%
targetBin{4}.Ncriteria=2;
targetBin{4}.criteria{1}='Gender';
targetBin{4}.value{1}=   'Female';
targetBin{4}.criteria{2}='Race';
targetBin{4}.value{2}={  'Other'};
targetBin{4}.percentage=popBinPercentage(4);
%%
%
sourceBin{1}.Ncriteria=2;
sourceBin{1}.criteria{1}='GENDER';
sourceBin{1}.value{1}=   '2';
sourceBin{1}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
sourceBin{1}.value{2}={  '1'};
%
sourceBin{2}.Ncriteria=2;
sourceBin{2}.criteria{1}='GENDER';
sourceBin{2}.value{1}=   '2';
sourceBin{2}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
sourceBin{2}.value{2}={  '2'};
%
sourceBin{3}.Ncriteria=2;
sourceBin{3}.criteria{1}='GENDER';
sourceBin{3}.value{1}=   '2';
sourceBin{3}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
sourceBin{3}.value{2}={  '3'};
%
sourceBin{4}.Ncriteria=2;
sourceBin{4}.criteria{1}='GENDER';
sourceBin{4}.value{1}=   '2';
sourceBin{4}.criteria{2}='RACE_OF_SUBJECT-ANSUR88';
sourceBin{4}.value{2}={  '4',...
    '5',...
    '6',...
    '7',...
    '8',...
    '9',...
    '**',...
    '***',...
    '9999' };
%
% 3) Apply the Parkinson-Reed approach
% -------------------------------------
%
psig=0.05;      % threshold for significance
%
[XU_vrt,~,~,...
    Q,P,YU_mean,B,E,sigQ]=...
    ParkinsonReed(XU,XUheadings,YU,CU,CUheadings,...
    targetBin,sourceBin,psig);
%
% Furthermore evaluate (and later store in "regression") the mean
%     and standard deviation of the predictor variables.
% % XU_mu=mean(XU_vrt,1);    % based on the virtual population
% % XU_vrt_centred=XU_vrt-repmat(XU_mu,size(XU_vrt,1),1);
% % %
% % % uncorrelate the centred data
% % Cxx=XU_vrt_centred.'*XU_vrt_centred;
% % [Vxx,Dxx]=eig(Cxx)
% %
% %
% %
% % SXX_up=sum(sum(XU_vrt_centred.*XU_vrt));
% % %
%
% 4) save PR regression results
% ------------------------------
% This stores information about the resulting regression...
% In future versions:
%     The names/label/codes for approaches should be formalised
%     The names/label/codes for this and other databases should be formalised
%     Names of headings should be formalised as "criteria"
regression.label=labelPRRun;
regression.approach='PR_v1.1';
regression.originalDatabase='ANSUR_women';
regression.XUheadings=XUheadings;
regression.YUheadings=YUheadings;
regression.CUheadings=CUheadings;
regression.bin=targetBin;
regression.binSource=sourceBin;
regression.Q=Q;
regression.P=P;
regression.YU_mean=YU_mean;
regression.B=B;
regression.E=E;
regression.psig=psig;
regression.sigQ=sigQ;
regression.XU=XU_vrt;
%
if saveResults
    save(strcat(resultsDir,filesep,'regression_',labelPRRun,'.mat'),'regression','-mat');
end
%
% comparison with simple linear regression:
% Backup_simpleRegressionOption_PReed
%
