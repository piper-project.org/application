% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function saveRegression(regression,fileName)
%
% save regression information in ascii file fileName
%
% saveRegression(regression,fileName)
%
[labelPRRun,approach,originalDatabase,XUheadings,YUheadings,...
    CUheadings,bin,sourceBin,Q,P,YU_mean,B,E,psig,sigQ,XU]=...
    dataRegression(regression);
%
Lvar={'labelPRRun','approach','originalDatabase','XUheadings',...
    'YUheadings','CUheadings','bin','sourceBin',...
    'Q','P','YU_mean','B','E','psig','sigQ','XU'};
%
fid=fopen(fileName,'w');
for j=1:length(Lvar)
    fid=fprintfRegression(fid,Lvar{j},eval(Lvar{j}),fileName);
    %
end
%
fclose(fid);
%
% -----------------------------------------------------------------------
%
function fid=fprintfRegression(fid,varName,varVal,fileName)
%
if ischar(varVal)
    fprintf(fid,'%s\n',strcat('VariableName:',varName));
    fprintf(fid,'%s %d\n','Size:',length(varVal));
    fprintf(fid,'%s %s\n','Type:','char');
    fprintf(fid,'%s\n',varVal);
end
%
if iscell(varVal)
    if ischar(varVal{1})
        fprintf(fid,'%s\n',strcat('VariableName:',varName));
        fprintf(fid,'%s %d\n','Size:',length(varVal));
        fprintf(fid,'%s %s\n','Type:','cellOfStrings');
        fprintf(fid,'%s %d\n','NumberCells:',length(varVal));
        for j=1:length(varVal)
            if ~ischar(varVal{j})
                error([varName,'- should be a cell variable of strings.'])
            end
            fprintf(fid,'%s %d \n','Size:',length(varVal{j}));
            fprintf(fid,'%s %s\n','Type:','char');
            fprintf(fid,'%s\n',varVal{j});
        end
    elseif isstruct(varVal{1})
        disp('Information about population bins is not saved...')
    end
end
%
if isnumeric(varVal)
    fprintf(fid,'%s\n',strcat('VariableName:',varName));
    fprintf(fid,'%s %d %d\n','Size:',size(varVal));
    fprintf(fid,'%s %s\n','Type:','numeric');
    % % %        fprintf(fid,'%s\n',varVal);
    fclose(fid);
    save(fileName,'varVal','-ascii','-append');
    fid=fopen(fileName,'a+');
    %
end
%
%
%
