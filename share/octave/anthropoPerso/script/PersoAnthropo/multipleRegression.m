% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Q,P,bar_YU,B,E,sigQ,XUheadings]=...
    multipleRegression(XU,XUheadings,YU,psig)
%
% [Q,P,bar_YU,B,E,sigQ,XUheadings]=...
%                     multipleRegression(XU,XUheadings,YU,psig);
%
% Input:  - XU, predictor values, one row per subject, one column per data
%                    type.
%         - XUheadings, label (data type description) of each column of XU.
%         - YU, detailed measurements, one row per subject, one column per
%                    data type.
%         - psig, p-value threshold to evaluate the significance of the
%                    dependency of the columns of Q to predictors X.
% - Output: - Q, orthogonal matrix (Q'*Q= identity matrix I) of principal 
%                 components such that 
%                  YU = Q P + ones(size(YU,1)) * bar_YU
%           - P, matrix of loadings
%           - bar_YU, bar_YU = mean(YU)
%           - B, scoring matrix such that 
%                  Q = XU B(:,1:end-1) + B(:,end) + E
%           - E, is the matrix of errors (on Q) in the above expression
%                 the matrix of error on the detailed measurements is 
%                 therefore E * P.  Information about these errors
%                 may allow to reconstruct sample including variance
%                 (see Reference, for the "first z components", as well
%                 as the discussion for the other principal components
%                 that are not significantly dependent on the regression.)
%           - sigQ, table of indices of principal component columns 
%                 exhibiting significant regression as a function of the 
%                 predictors p-value < psig (note that this may differ from 
%                 the Parkinson Reed approach to select the *first* z 
%                 principal components as significant).
%           - XUheadings, label (data type description) of each column of XU.
%

isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave
    if ~existPackage('statistics')
       pkg install -forge statistics     % installation
    end
    pkg load statistics     % loading statistics package
end
%
% 1) centre the data
% ===================
U=[XU,YU];                   
NU=size(U,1);
%
bar_YU=mean(YU);                 % mean anthropometric data
hat_YU=YU-ones(NU,1)*bar_YU;     % centered anthropometric data
%
% 2) PCA
% =======
% NOTE:
% covariance matrix:   C = ( hat_YU * hat_YU.' )
%    svd of hat_YU = u *s * v.'
%    =>  C = ( hat_YU * hat_YU.' ) = ( u *s^2 * u.' )
%    C u = u s^2  -> u contains the eigenvectors of C, s^2 the eigenvalues
%
% Regression
%
% a. decompose the (centered) shape of each subject into modes
%
[u,s,v]=svd(hat_YU,0);
Q=u;
P=s*v';
%
% b. evaluate the coefficients of a linear regression for each mode
%                    and each predictor
% ref: Weisberg, 2005 Applied Linear Regression, chapter 2
%
Qbar=zeros(1,size(Q,2));
SYY=zeros(1,size(Q,2));
for j=1:size(Q,2)
    %  Qbar is already known to be zero...
    Qbar(j)=mean(Q(:,j));% sample average of predictor Q(:,j)
    SYY(j)=sum((Q(:,j)-Qbar(j)).*Q(:,j)); % sum of squares for the y's
    % SDy2(j)=SYY(j)/(n-1);    % sample variance of the y's
    % SDy(j)=sqrt(SDy2(j));    % sample standard deviation of the y's
end
%
p=size(XU,2);
XU=[XU,ones(NU,1)];
pprime=p+1;
XUheadings{end+1}='Intercept';
%
% Multiple regression (several predictors/one principal component)
%
X=XU;
n=size(X,1);
B=(X.'*X)\(X.'*Q);
% gammahat_mult=B*P;
%
E=zeros(size(Q));
SSreg_mult=zeros(size(Q,2),1);
RSS_mult=zeros(size(Q,2),1);
MSreg_mult=zeros(size(Q,2),1);
sigma2hat_mult=zeros(size(Q,2),1);
F_mult=zeros(size(Q,2),1);
pvaluE=zeros(size(Q,2),1);
%
for j=1:size(Q,2)
    ej=(Q(:,j)-X*B(:,j));
    E(:,j)=ej;
    RSS_mult(j)=ej.'*ej;
    %   SYY(j);
    SSreg_mult(j)=SYY(j)-RSS_mult(j);
    %
    MSreg_mult(j)=SSreg_mult(j)/p;  % p degrees of freedom
    sigma2hat_mult(j)=RSS_mult(j)/(n-pprime);  % (n-p') df
    %
    F_mult(j)=MSreg_mult(j)/sigma2hat_mult(j);
    pvaluE(j)=1-cdf('F',F_mult(j),p,n-pprime);
end
%
% DOES NOT RETURN THE INTERCEPT COLUMN OF ones...
% % XU=XU(:,1:end-1);
%
% find principal components that depend significantly on the
%                    predictors and preserve them in the model
% (note that these are not in general the z *first* components)
sigQ=find(pvaluE<psig);
% % keepvaluE=pvaluE(sigQ);
% % %
% % % update the tables
% % Qr=Q(:,sigQ);
% % betahatr_mult=B(:,sigQ);
% % Pr=P(sigQ,:);
% % Er_mult=E(:,sigQ);
%
