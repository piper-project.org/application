% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [regression,XU,YU,CU,iKeptSubjects]=...
                     ParkinsonReed_Wrapper(XU,XUheadings,YU,YUheadings,...
                     CU,CUheadings,targetBin,sourceBin,psig,labelPRRun,...
                     originalDatabase)
%
% Wrapper to call the script ParkinsonReed
%
% Two things are done:
% 1) subjects for which data is missing (NaN values, negative measurements, etc)
%    are removed.  The updated arrays are output, as well as an array,
%    iKeptSubjects, that indicates the kept subjects. 
% 2) the results and input are stored in a structure format, regression, also
%    output of the call.
%
% Christophe Lecomte
% University of Southampton
% January 2017
% Work funded in the context of the EU FP7 PIPER project
%

% 0 - Initialise the list of kept subjects
iKeptSubjects=ones(size(YU,1),1);
%
% 1 - Remove subjects with gaps or negative value in predictor values
%
iKeptSubjects(find(sum(isnan(XU),2)))=0;
iKeptSubjects(find(min(XU,[],2)<0))=0;
%
% 2 - Remove subjects with gaps in characteristic values (used in population
%                                                                descriptors)
%
iKeptSubjects(find(sum(strcmp(CU,'NaN'),2)))=0;
%
% 3 - Remove subjects with gaps or negative value in measurements
iKeptSubjects(find(sum(isnan(YU),2)))=0;
iKeptSubjects(find(min(YU,[],2)<0))=0;
%
% 4 - extract indices (from the tables of 0's and 1's)
iKeptSubjects=find(iKeptSubjects);
% ---
%
XU=XU(iKeptSubjects,:);
YU=YU(iKeptSubjects,:);
CU=CU(iKeptSubjects,:);
%
%
[XU_vrt,~,~,...
    Q,P,YU_mean,B,E,sigQ]=...
    ParkinsonReed(XU,XUheadings,YU,CU,CUheadings,...
    targetBin,sourceBin,psig);
%
% 4) save PR regression results
% ------------------------------
% This stores information about the resulting regression...
% In future versions:
%     The names/label/codes for approaches should be formalised
%     The names/label/codes for this and other databases should be formalised
%     Names of headings should be formalised as "criteria"
regression.label=labelPRRun;
regression.approach='PR_v1.1';
regression.originalDatabase=cellStrcat(originalDatabase);
regression.XUheadings=XUheadings;
regression.YUheadings=YUheadings;
regression.CUheadings=CUheadings;
% regression.CUheadingsTarget=CUheadingsTarget;
regression.bin=targetBin;
regression.binSource=sourceBin;
regression.Q=Q;
regression.P=P;
regression.YU_mean=YU_mean;
regression.B=B;
regression.E=E;
regression.psig=psig;
regression.sigQ=sigQ;
regression.XU=XU_vrt;
regression.XU_beforeUpsampling=XU;
%
end
%
%
%
function combinedStr=cellStrcat(CS)
%
%
combinedStr=CS{1};
%
for j=2:length(CS)
    combinedStr=[combinedStr,',',CS{j}];
end
%
end
