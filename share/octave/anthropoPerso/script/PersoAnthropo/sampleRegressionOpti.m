% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Ys,Ysheadings,Xs_actual,Alpha,Beta]=...
                    sampleRegressionOpti(regression,Xs,...
                                         Xsheadings,PCdeficiency,averageOnly)
%
% function [Ys,Ysheadings]=sampleRegressionOpti(regression,Xs,Xsheadings,...
%                                                           PCdeficiency);
%
% function [Ys,Ysheadings,Xs_actual,Alpha,Beta]=...
%               sampleRegressionOpti(regression,Xs,Xsheadings,...
%                                               PCdeficiency,averageOnly);
%
% Sampling of measurements and generation of the regression parameters
%                                        for some values of predictors.
%
% Input: regression, is a structure containing the regression information,
%       Xs, values of the desired predictors
%       Xsheadings, names of the desired predictor variables
%       PCdeficiency, deficiency number of principal components (PCs) used, in 
%                 relation to the number of predictors.  If there are less
%                 PCs than the number predictors minus this deficiency number,
%                 all PCs are used.
%       averageOnly, indicates if regression error is considered (averageOnly=0)
%                 or not (averageOnly=1).  Current version only supports 
%                 averageOnly=1.
% Output: Ys, samples of the measurements of interest
%         Ysheadings, labels of the measurements of interest
%         Xs_actual, is the actual value of the predictors as estimated from 
%                 a regression using less modes than the number of predictors,
%                 i.e. when deficiency>0 (this is a least square approach).
%                 The variables are ordered in the same way as the Xs (i.e.
%                 as described in Xsheadings).
%         Alpha, Beta, parameters of the regression XYs= Alpha + Xs Beta, where
%                 the outputs (XYs) are described by the headings stored
%                 in regression.XYUheadings.
%

%
% --------------------------------
% --------------------------------
% Christophe Lecomte
% University of Southampton
% work funded by the PIPER project
% 2017
%
if nargin<4
    averageOnly=1;
end
%
if ~averageOnly
    warning(['The current version of sampleRegressionOpti does not yet',...
        ' support regression error'])
end
%
if isfield(regression,'Slave')
  warning(['Slave regressions are not sampled.  Rather consider using ',...
              'sampleRegressionOpti_Slaves'])
end
%
treatedXs=zeros(length(Xsheadings),1);
XYUheadings=regression.XYUheadings;
index_Xs=zeros(size(Xsheadings));
for j=1:length(Xsheadings)
  indFound=find(strcmp(XYUheadings,Xsheadings{j}));
  if ~isempty(indFound)
      index_Xs(j)=indFound(1);
      treatedXs(j)=1;
  end
  %
end
if sum(~treatedXs)
   warning('Some specified predictor values are unused in the regression')
end
%
P = regression.P;
%
nKeptP=min(sum(treatedXs) - PCdeficiency,size(P,2));  % number of kept P columns
%                 components of P associated with the predictors
Px=diag(regression.scaleInv(index_Xs(treatedXs==1)))*...
                          P(index_Xs(treatedXs==1),1:nKeptP);  
%
% Regression is built to the variance compared to the mean, so the mean is
%         first removed from the predictor values
% For a single input vector:
% c=Px\(Xs(:)-regression.XYU_mean(index_Xs(treatedXs==1))(:));
pinvPx=(Px'*Px)\Px';
c= (Xs-ones(size(Xs,1),1)*regression.XYU_mean(index_Xs(treatedXs==1)))*(pinvPx.');
%
Zs=c*P(:,1:nKeptP).'*diag(regression.scaleInv);
XYs=ones(size(Xs,1),1)*regression.XYU_mean*diag(regression.scaleInv)+Zs;
%
% Equivalent expressions:
% ========================
% XYs=regression.XYU_mean(:)+diag(regression.scaleInv)*P(:,1:nKeptP)*...
%            pinvPx*(Xs(:)-regression.XYU_mean(index_Xs(treatedXs==1))(:))
%   =regression.XYU_mean(:)+...
%      diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*Xs(:)...
%    -diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*...
%                  regression.XYU_mean(index_Xs(treatedXs==1))(:)
%
% XYs_actual=(regression.XYU_mean(:)-...
%               diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*...
%               regression.XYU_mean(index_Xs(treatedXs==1))(:)...
%              +diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*Xs(:)).'
%  
% XYs_actual=Alpha...
%              +Xs*Beta
% 
% Alpha = (regression.XYU_mean(:)-...
%               diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*...
%               regression.XYU_mean(index_Xs(treatedXs==1))(:)).';
% Beta = (diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx).';
%
%
Xs_actual=XYs(:,index_Xs(treatedXs==1));
%
Ys=XYs(:,setdiff(1:length(XYs),index_Xs(treatedXs==1)));
Ysheadings=XYUheadings(setdiff(1:length(XYs),index_Xs(treatedXs==1)));
%
%

Alpha = ones(size(Xs,1),1)*regression.XYU_mean*diag(regression.scaleInv)-...
              (diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx*...
              regression.XYU_mean(index_Xs(treatedXs==1))(:)).';
Beta = (diag(regression.scaleInv)*P(:,1:nKeptP)*pinvPx).';
%
%