% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [XU_vrt,YU_vrt,CU_vrt,tableDupli]=...
    populationUpsample(XU,YU,CU,CUheadings,sourceBin,targetBin)
%
% function [XU_vrt,YU_vrt,CU_vrt,tableDupli]=...
%        populationUpsample(XU,YU,CU,CUheadings,sourceBin,targetBin);
%
% Generate a virtual population through (population bin) upsampling
%
% * A POPULATION is described by BINS, i.e. sub-sets of population
%                                        with given characteristics.
% * A TARGET virtual population is built from a SOURCE population.
% * The PERCENTAGE of bins within the whole population varies from the
%                     source to target populations.
%
% Input: - source population descriptors
%               -> XU,YU,CU: list of values of respectively, predictors,
%                   measurements of interest, criteria/characteristics of
%                   the population members.
%                   Each column corresponds to one of these values (a field
%                   of values), each row corresponds to one subject.
%               -> CUheadings: names of the fields corresponding to the
%                   columns of the CU (the characteristics of the
%                   population).
%         - targetBin, description of the desired bins of a target population
%                   This is a cell of structures, so that the bin j is
%                   targetBin{j}, the number of criteria values decribing
%                   it is targetBin{j}.Ncriteria, and each corresponding
%                   criteria is defined by a criteria label and value.
%                   For, criteria k of bin j, those are
%                      targetBin{j}.criteria{k}
%                      targetBin{j}.value{k}.
%                   A percentage of each bin in the total population is also
%                   given, say targetBin{j}.percentage for bin j.
%         - sourceBin, description of the desired bins of the target
%                   population as represented in the source population.
%                   The format is similar as that of the targetBin
%                   input, except that criteria may have different names
%                   in the source population and also that a single desired
%                   criteria value in the target population may correspond
%                   to several values in the source population.  For
%                   example (target) "hispanic" could be either
%                   "hispanic/mexican" or "hispanic/spanish".  In this
%                   case, the several values are separated by commas.
%                   Percentages are not specified for the source bins (as
%                   they can be evaluated).
% Output: - XU_vrt,YU_vrt,CU_vrt: list of values of respectively, predictors,
%                   measurements of interest, criteria/characteristics of
%                   the generated virtual population members.
%                   Each column corresponds to one of these values (a field
%                   of values), each row corresponds to one (possibly 
%                   duplicated) subject.
%         - tableDupli, list of the location of duplicated subjects in the new
%                   virtual population. 
%
% Christophe Lecomte, 2015
% University of Southampton
% Work funded by the FP7 PIPER project
% Isolated from ParkinsonReed.m, Version 1.1, January 2016.
% Updated the comparison of items to include wild characters 
%

% Neglect the bins with zero percentage...
%
percentageBin=cellfun(@(x) x.percentage,targetBin,'UniformOutput',true);
active=find(percentageBin>0);
sourceBin=sourceBin(active);
targetBin=targetBin(active);
%
% 1) BIN SORTING AND UPSAMPLING
% ==============================
% This is input information (for the target population)...
%
% 1.a) sort the bins of the source population
%
disp(['DEBUG/TEST: number of subjects before bin selection:',num2str(size(XU,1))])
% first, check that all bins have identical criteria
Ncrit=sourceBin{1}.Ncriteria;
colCritU=zeros(Ncrit,1);
for j=1:Ncrit
    colCritU(j)=find(strcmp(sourceBin{1}.criteria{j},CUheadings));
end
%
Nbins=length(sourceBin);
%
for k=1:Nbins
    if Ncrit~=sourceBin{k}.Ncriteria
        error('Inconsistent number of bin criteria...')
    end
    for j=1:Ncrit
        if colCritU(j)~=find(strcmp(sourceBin{k}.criteria{j},CUheadings))
            error('Some source bins use inconsistent category criteria')
        end
    end
end
%
% then go through all items and check if they fit in a bin, and which one
NU=size(YU,1);
binU=zeros(NU,1);  % indicates which bin an item is in
valueBinU=cell(1,2);
%
for j=1:NU
% for j=NU:NU
    % Values of the criteria defining the bins for this particular item
    for k=1:Ncrit
        valueBinU(k)=CU(j,colCritU(k));
    end
    for i=1:Nbins
        isInBin=1;
        for k=1:Ncrit
%             if ~sum(strcmp(valueBinU(k),sourceBin{i}.value{k}))
            testIsInBin=isInBin_specChar(valueBinU{k},sourceBin{i}.value{k});
            if ~sum(testIsInBin)
                isInBin=0;
            elseif sum(testIsInBin)>1
                warning(['Several matching bin values for bin\#',...
                    num2str(i),' and value ',valueBinU(k)])
            end            
        end
        if isInBin
            if binU(j)~=0
                error(['Some bins overlap.  Check item ',num2str(j),...
                    ' for bins ',num2str(i),' and ',...
                    num2str(binU(j)),'.'])
            end
            binU(j)=i;
        end
    end
end
%
% 1.b) upsample the bins to the target population characteristics
%
% desired percentages
percentage=zeros(Nbins,1);
for i=1:Nbins
    percentage(i)=targetBin{i}.percentage;
end
if abs(sum(percentage)-100)>1e-1
    warning('The sum of percentages of bins differs from 100% by more than 0.1%')
end
%
% percentages in source data
Samples=cell(Nbins,1);
NBinSamplesSource=zeros(Nbins,1);
for i=1:Nbins
    Samples{i}=find(binU==i);
    NBinSamplesSource(i)=length(Samples{i});
end

disp(['DEBUG/TEST:   size of bins before upsampling:',num2str(NBinSamplesSource(:).')])


NSource=sum(NBinSamplesSource);
if NSource==0
    error('The population bins are empty.  Please check their criteria.')
end
percentageSource=NBinSamplesSource/NSource;
%
% The number of samples in each bin of the target population must be
%     larger that in the equivalent bin of the source population:
% NTarget * percentage >= NSource * percentageSource
% NTarget >= NSource * (percentageSource./percentage)
% therefore, the minimum number of samples is
NTarget_min = NSource * max(percentageSource./percentage);
% and each bin contains a minimum number
% NBinSamplesTarget_min = NTarget_min * percentage;
% This is rounded...
NBinSamplesTarget=round(NTarget_min*percentage);
%

disp(['DEBUG/TEST:   size of bins after upsampling:',num2str(NBinSamplesTarget(:).')])


% Note that using this strategy to assign the number of samples in each bin
%    of the target population on the CAESAR women data (removing the subjects with
%    incomplete data), gives 3350 upsampled subjects.  This differs from
%    the number (1872) reported in the original paper (Parkinson & Reed, 2010)
%
% fill the upsampled arrays
%
NU_vrt=sum(NBinSamplesTarget);
%
XU_vrt=zeros(NU_vrt,size(XU,2));
YU_vrt=zeros(NU_vrt,size(YU,2));
CU_vrt= cell(NU_vrt,size(CU,2));
%
tableDupli=zeros(NU_vrt,1);
%
ind=0;
for i=1:Nbins
    XUi=XU(binU==i,:);
    YUi=YU(binU==i,:);
    CUi=CU(binU==i,:);
    NUi=length(XUi);
    indInU=find(binU==i);
    %
    XU_vrt(ind+(1:NBinSamplesSource(i)),:)=XUi;
    YU_vrt(ind+(1:NBinSamplesSource(i)),:)=YUi;
    CU_vrt(ind+(1:NBinSamplesSource(i)),:)=CUi;
    tableDupli(ind+(1:NBinSamplesSource(i)))=indInU;
    %
    ind=ind+NUi;
    % add missing subjects one by one (upsampling)
    for j=1:(NBinSamplesTarget(i)-NBinSamplesSource(i));
        % 
        if NUi==0
            error(['The ',num2str(i),...
                      '-th bin is empty.  Upsampling is impossible.']);
        end
        %
        sampleUi=ceil(rand*NUi);
        XU_vrt(ind+1,:)=XUi(sampleUi,:);
        YU_vrt(ind+1,:)=YUi(sampleUi,:);
        CU_vrt(ind+1,:)=CUi(sampleUi,:);
        tableDupli(ind+1)=indInU(sampleUi);
        %        
        ind=ind+1;
    end
end
%
disp(['DEBUG/TEST: number of subjects after upsampling:',num2str(size(XU_vrt,1))])
%
end
%
%
%
function foundCV=isInBin_specChar(CV,listCV)
%
% Indicates if a Criteria Value is found in a list of such values
%
% Special characters are considered:
%   - a star ("*") in the listCV list indicates that any character of CV
%             is considered a match.
%
% Input: CV, value of a Criteria Value;
%        listCV, cell list of Criteria Values.  Individual items that
%                include commas are separated at the level of commas;
% Output: foundCV, array of flags that indicates if CV is in listCV
%                 (if foundCV(k)=1, CV is in listCV{k})
%
% Christophe Lecomte
% January 2016
%

% remove duplicates and distinguish items separated by commas
CV=formattedCV(CV);
if length(CV)~=1
    error('More than one criteria value is not supported')
end
CV=CV{1};  % extract string of characters (from the cell of length 1)
%
if ischar(listCV)
    listCV={listCV};
end
%
foundCV=zeros(size(listCV));
for j=1:length(listCV)
    listCVj=formattedCV(listCV{j});
    %
    % Deal with intervals (keep them in a single CV item)
    k=1;
    while k<=length(listCVj)
        listCVItem=listCVj{k};
        if strcmp(listCVItem(1),'[')
            if ~strcmp(listCVItem(end),']')
                %
                if length(listCVj)<k+1
                    error('please check interval of criteria value')
                else
                    listCVNext=listCVj{k+1};
                    %
                    if ~strcmp(listCVNext(end),']')
                        error('please check interval of criteria value')
                    else
                        listCVj{k}=[listCVj{k},',',listCVj{k+1}];
                        listCVj={listCVj{1:k},listCVj{k+2:end}};
                    end
                    %
                end
            end    
                %
        end
        k=k+1;
    end
    %
    % Check if CV is in any of the listCVj items
    % 
    for k=1:length(listCVj)
        listCVItem=listCVj{k};
        %
        % a. Particular case of intervals
        %
        if strcmp(listCVItem(1),'[')
            icomma=strfind(listCVItem,',');
            if length(icomma)~=1
                error('check interval definition (absence of comma)')
            end
            %
            lowerBound=str2num(listCVItem(2:icomma-1));
            upperBound=str2num(listCVItem(icomma+1:end-1));
            %
            if str2num(CV)>=lowerBound && str2num(CV)<=upperBound
                foundCV(j)=1;
            end
            %
        else
            % b. all other cases, including with special "*" character
            
            % check each one of the pieces of the CV and listCVItem that are
            %     separated by stars (in listCVItem)
            iStar=strfind(listCVItem,'*');
            %
            foundItemjk=1;
            remainingA=CV;
            remainingB=listCVItem;
            if length(remainingA) ~= length(remainingB)
                foundItemjk=0;
            else
                % go through all bits before a star
                for s=1:length(iStar)
                    bitA=remainingA(1:iStar(s)-1);
                    bitB=remainingB(1:iStar(s)-1);
                    remainingA=remainingA(iStar(s)+1:end);
                    remainingB=remainingB(iStar(s)+1:end);
                    % updates location so that they are provided with respect to
                    %              the remaining (remainingA & remainingB) of the
                    %              compared items (CV and CVitem).
                    iStar=iStar-iStar(s);
                    %
                    if ~strcmp(bitA,bitB)
                        foundItemjk=0;
                    end
                    %
                end
            end
            %
            bitA=remainingA;
            bitB=remainingB;
            %
            if ~strcmp(bitA,bitB)
                foundItemjk=0;
            end
            %
            if foundItemjk
                foundCV(j)=1;
            end
            %
        end
        %
    end
    %
end
%
end
%
%
%
