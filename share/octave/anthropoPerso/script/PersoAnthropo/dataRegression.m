% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [labelPRRun,approach,originalDatabase,XUheadings,YUheadings,...
       CUheadings,bin,sourceBin,Q,P,YU_mean,B,E,psig,sigQ,XU]=...
                                               dataRegression(regression)
%
% See also: multipleRegression, saveRegression
%
labelPRRun=regression.label;
approach=regression.approach;
originalDatabase=regression.originalDatabase;
XUheadings=regression.XUheadings;
YUheadings=regression.YUheadings;
CUheadings=regression.CUheadings;
bin=regression.bin;
sourceBin=regression.binSource;
Q=regression.Q;
P=regression.P;
YU_mean=regression.YU_mean;
B=regression.B;
E=regression.E;
psig=regression.psig;
sigQ=regression.sigQ;
XU=regression.XU;
%
%
%
