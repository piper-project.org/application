function     [regression,XU_vrt,YU_vrt,CU_vrt,iKeptSubjects]=...
                  RegressionOpti_Wrapper(XU,XUheadings,...
                  YU,YUheadings,CU,CUheadings,...
                  subjectSRNURows,targetBin,sourceBin,...
                  labelRegressionRun,originalDatabase,...
                  X,dataBP,landmarkCodesXRows,subjectSRNXCols,...
                  A_circ,dataBP_circ,...
                  maxIter,toler,scaleXP,scaleCirc,tolerVar)
%
%
% ==============================================================================
%
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ==============================================================================
%
% Two things are done:
% 1) subjects for which data is missing (NaN values, negative measurements, etc)
%    are removed.  The updated arrays are output, as well as an array,
%    iKeptSubjects, that indicates the kept subjects.  
%    This is based on the (external) anthropometric data only.
% 2) (IF landmarks are provided,) only subjects for which landmarks are 
%                                                       available are used
% 3) virtual populations are generated
% 4) body parts are aligned group by group
% 5) a PC analysis is run on the data consisting of
%       a) anthropo data
%       (IF landmarks are provided, b) aligned landmark coordinates
%                                   c) bone circumferences        )
% 6) the results and input are stored in a structure format, regression, also
%    output of the call.  The actual values of the regression are evaluated
%    implicitly by optimisation at the time of sampling. 
%
% Input:  - XU, predictor values, one row per subject, one column per data
%                    type.
%         - XUheadings, label (data type description) of each column of XU.
%         - YU, detailed measurements, one row per subject, one column per
%                    data type.
%         - CU, criteria, one row per subject, one column per
%                    data type.
%         - CUheadings, label (data type description) of each column of CU.
%         - subjectSRNUCols, identifier of the subjects of XU, YU and CU.  
%           It is a cell array so that subjectSRNXCols{j} contains the ID of the
%           j-th subject.
%         - targetBin, description of the desired bins of a target population
%                   This is a cell of structures, so that the bin j is
%                   targetBin{j}, the number of criteria values decribing
%                   it is targetBin{j}.Ncriteria, and each corresponding
%                   criteria is defined by a criteria label and value.
%                   For, criteria k of bin j, those are
%                      targetBin{j}.criteria{k}
%                      targetBin{j}.value{k}.
%                   A percentage of each bin in the total population is also
%                   given, say targetBin{j}.percentage for bin j.
%         - sourceBin, description of the desired bins of the target
%                   population as represented in the source population.
%                   The format is similar as that of the targetBin
%                   input, except that criteria may have different names
%                   in the source population and also that a single desired
%                   criteria value in the target population may correspond
%                   to several values in the source population.  For
%                   example (target) "hispanic" could be either
%                   "hispanic/mexican" or "hispanic/spanish".  In this
%                   case, the several values are separated by commas.
%                   Percentages are not specified for the source bins (as
%                   they can be evaluated).
%         - maxIter, maximum number of iterations; [default=20]
%         - toler, relative difference between the Procrustes sums of
%               squares during iterations, used as tolerance criteria
%               to stop iterations, both during the rotations -step 2-
%               and scaling -step3-; [default=1e-6]
%         - X, configurations, i.e. sets of landmark coordinates. This is
%           a three dimensional array.  X(i,j,k) corresponds to the j-th
%           coordinate of the i-th landmark of the k-th subject.
%         - dataBP, name of body part associated to the landmarks.  It is 
%           a cell array, so that dataBP{j} contains the name of the body
%           part associated with the j-th landmark.
%         - landmarkCodesXRows, name of the landmarks.  It is a cell array
%           so that landmarkCodesXRows{j} contains the name of the
%           j-th landmark.
%         - subjectSRNXCols, identifier of the subjects of X.  It is a cell 
%           array so that subjectSRNXCols{j} contains the ID of the
%           j-th subject.
%         - A_circ, Anthropometric circumference. A_circ(i,j) is the 
%           circumference corresponding to modelBP{j} of the i-th 
%           configuration (i.e. i-th subject, subjectSRNXCols{i});
%         - dataBP_circ, actual list of body parts (as provided in the
%               "bone" column of the data files) for which a circumference is
%               returned. This is a cell array of strings.
%  - Output: regression, structure containing regression information, such that
%            regression.label, is a label for the regression
%            regression.approach, is a descriptor of the approach used for 
%                                             generating the regression
%            regression.originalDatabase, name of the original database from 
%                         data originated
%            regression.XYUheadings
%            regression.CUheadings
%%                regression.CUheadingsTarget
%            regression.bin
%            regression.binSource
%            regression.P
%            regression.Sigma_hat
%            regression.XYU_mean
%            regression.scaleXP
%            regression.scaleCirc
%            regression.scaleInv
%            regression.XU
%            regression.YU
%            regression.CU
%            regression.XU_beforeUpsampling
%            regression.XUheadings
%            regression.YUheadings
%
% Christophe Lecomte
% University of Southampton
% January 2017
% Work funded in the context of the EU FP7 PIPER project
%
if nargin>11
    useLandmarks=1;
else
    useLandmarks=0;    
end
%
if nargin<19
    toler=paramPerso('SSM_toler');
    if isempty(toler)
        toler=1e-6;
    end
end
%
if nargin<18
    maxIter=paramPerso('SSM_maxIter');
    if isempty(maxIter)
        maxIter=20;
    end
end
%
%
if nargin<20
    scaleXP=paramPerso('SSM_scaleXP');
    if isempty(scaleXP)
        scaleXP=1;
    end
end
%
if nargin<21
    scaleCirc=paramPerso('SSM_scaleCirc');
    if isempty(scaleCirc)
        scaleCirc=1;
    end
end
%
if nargin<22
    tolerVar=paramPerso('SSM_tolerVar');
    if isempty(tolerVar)
        tolerVar=1e-10;
    end
end
% 1 PREPARE DATA
% ---------------
% 1.0 - Initialise the list of kept subjects
iKeptSubjects=ones(size(YU,1),1);
%
% 1.1 - Remove subjects with gaps or negative value in predictor values
%
iKeptSubjects(find(sum(isnan(XU),2)))=0;
iKeptSubjects(find(min(XU,[],2)<0))=0;
%
% 1.2 - Remove subjects with gaps in characteristic values (used in population
%                                                                descriptors)
%
iKeptSubjects(find(sum(strcmp(CU,'NaN'),2)))=0;
%
% 1.3 - Remove subjects with gaps or negative value in measurements
iKeptSubjects(find(sum(isnan(YU),2)))=0;
iKeptSubjects(find(min(YU,[],2)<0))=0;
%
% 1.4 - only keep subjects that have landmark measurements (only if landmarks 
%                                                              are used)
if useLandmarks
    indSRNXofSRNU=zeros(size(subjectSRNURows));
    % retrieve index of SRNX (landmarks data) within the SRNU data (anthropo) 
    for j=1:length(subjectSRNXCols)
        ind=find(strcmp(subjectSRNURows,subjectSRNXCols{j}));
        if ~isempty(ind)
            indSRNXofSRNU(ind)=j;
        end
    end
    unidentifiedAnthro=...
         setdiff(1:length(subjectSRNXCols),myUniqueRowsSorted(indSRNXofSRNU));
    if ~isempty(unidentifiedAnthro)
        warningText='There is no anthropometric data for the following subjects';
        for j=1:length(unidentifiedAnthro)
            warningText=[warningText,', ',subjectSRNXCols{unidentifiedAnthro(j)}];
        end
        warning(warningText)
    end 
    iKeptSubjects(indSRNXofSRNU==0)=0;
end
%
% 1.5 - extract indices (from the tables of 0's and 1's)
iKeptSubjects=find(iKeptSubjects);
% ---
XU=XU(iKeptSubjects,:);
YU=YU(iKeptSubjects,:);
CU=CU(iKeptSubjects,:);
subjectSRNURows=subjectSRNURows(iKeptSubjects,:);
%
%
if useLandmarks
    % Table of equivalence of SRN for landmark configurations and circumferences
    %     is first kept up to date
    indSRNXofSRNU=indSRNXofSRNU(iKeptSubjects,:);
    %
    % Then, landmark data is updated so that the subjects are in the same
    %     order as in the anthropometric data tables
    X=X(:,:,indSRNXofSRNU);
    A_circ=A_circ(indSRNXofSRNU,:);
    indSRNXofSRNU=1:length(indSRNXofSRNU);
    %
end
%
% information about body parts associated to landmarks is preserved:
initParamPerso('dataBP',dataBP);
initParamPerso('landmarkCodesXRows',landmarkCodesXRows);
%
%
% At this stage, the data corresponding to the chosen population is available.
%     Subjects are ordered (same order for anthropo and landmark data)
% Next steps will be 
%         a. population upsampling, 
%         b. adding landmarks and circumference data
%         c. extracting PC modes  
%
% 2 GENERATE THE REGRESSION INFO 
% -------------------------------
% 2.a generate virtual population through (population bin) upsampling 
[XU_vrt,YU_vrt,CU_vrt,tableDupli]=...
    populationUpsample(XU,YU,CU,CUheadings,sourceBin,targetBin);
% 2.b add landmark and circumference data to the anthropometric data
%             (if useLandmarks only) 
scaleInv=[ones(size(YU_vrt,2),1)];
if useLandmarks
%     2.b1  Alignment (considering the forward kinematic "DK" model)
    [modelBP,~,~]=myUniqueRowsStable(dataBP);
    [~,splitIndexGroup,~]=generateSplitIndexFromDK(dataBP,modelBP);
%         - splitIndex, index indicating which body part a node belongs to.
%               splitIndex(k)=j indicates that the k-th node belongs to the
%               j-th considered body part (modelBP{j})
%         - splitIndexGroup, is similar to splitIndex but indicates to which 
%               *group* of body parts a node belongs
%         - linkedBP, is a description of the groups of linked body parts. 
%
    alignOnly=1;
    partial=1;
    verbose=0;
    centered=0;
    [Xmu,~,~,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec]=...
    shapeModel(X,splitIndexGroup,maxIter,toler,[],centered,verbose,partial,...
                                                         alignOnly);
% After Procrustres alignment of the bones, linked group by linked group,
%     Xmu is the mean, among the subjects, of the aligned groups of bones.
%     XP are the aligned bones (aligned group by group)
                                                        
    XP_RowHeading=landmarkCodesXRows;
%
% 2.b2 formally adding the (aligned) configuration components to the 
%           tables of anthropometric measurements.
%
%  XP is an array with three components, we store it in an array of 
%    dimension two (one column vector per subject):
    [k,m,nSubjectsX]=size(XP);
    xp_transpose=reshape(XP,k*m,nSubjectsX);
    xp_transpose=xp_transpose.';
% The coordinate headings have a '_x', '_y', '_z' suffix
    xp_transpose_headings=catKron(XP_RowHeading,{'_x','_y','_z'});
    xp_subjectsID=subjectSRNXCols;   % subject ID for each row of xp_transpose
%
% Note the use of scaling...
    YU_vrt=[YU_vrt,scaleXP*xp_transpose(tableDupli,:)];
    YUheadings=vertcat(YUheadings(:),xp_transpose_headings);
    % scaleInv contains the inverse of scaling used so that the actual
    %      physical variables can be retrieved.
    scaleInv=[scaleInv;ones(size(xp_transpose,2),1)/scaleXP];
%
% Note the use of scaling...
    YU_vrt=[YU_vrt,scaleCirc*A_circ(tableDupli,:)];
    YUheadings=vertcat(YUheadings,strcat(dataBP_circ,'_circ'));
    % scaleInv contains the inverse of scaling used so that the actual
    %      physical variables can be retrieved.
    scaleInv=[scaleInv;ones(size(A_circ,2),1)/scaleCirc];
%
end
%
% 2.c Evaluating the PC modes (of the combined predictors and measurements of 
%                                                   interest)
%
XYU_vrt=[XU_vrt,YU_vrt];
XYUheadings=vertcat(XUheadings(:),YUheadings(:)).';
% scaleInv contains the inverse of scaling used so that the actual
%      physical variables can be retrieved.
scaleInv=[ones(size(XU_vrt,2),1);scaleInv];
%
% matrix of covariance...
% ========================
% => noting Z=XYU_vrt - mean_overSubjects(XYU_vrt), one has that the 
% => covariance matrix C = Z^T  Z
% => its eigenvectors and eigenvalues are the modes of variance
% => one does not need to evaluate C and its modal decomposition explicitly
% => instead, use SVD:  Z= Q D P^T, where Q^T Q = I, P^T P=I, and D is diagonal
% => then, C= P D^2 P^T
% => eigenvalues of C = diagonal terms of D^2
% => eigenvectors of C = columns of P, since C * P = P * D^2  (remember P^T P=I)
% => eigenvectors of C are the "Principal Components" or "PC" of the data
% => if each "PC" eigen pair had a Gaussian distribution, standard deviations
%        "Sigma_hat" of their magnitude in the data could be estimated as 
%                                               diag(S)/sqrt(n-1)
% =>                         where n is the number of subjects
%    
XYU_mean=mean(XYU_vrt);
Z=XYU_vrt-ones(size(XYU_vrt,1),1)*XYU_mean;
% C= P D^2 P^T
[Q,D,P]=svd(Z,0);
% one only keeps the "significant" variance modes (up to chosen tolerance)
kept=find(diag(D)>tolerVar*D(1,1));
Q=Q(:,kept);
D=D(kept,kept);
P=P(:,kept);
%
n=size(Z,1);
Sigma_hat=diag(D)/sqrt(n-1);
%
% Principal component info is available, at this stage...
%
%  => Sigma_hat: estimated standard deviation for each PC mode
%  => P: columns of principal components (ortho-normalised)
%
% Subjects are the sum of the mean and a weighted sum of the PC modes:
%     XYU_vrt(j,:) = XYU_mean + (diag(scaleInv) * P * c)';
%     where the c vectors have estimated standard deviation Sigma_hat.
%     scaleInv contains the scaling factor necessary to work with actual
%          physical values of landmark coordinates and circumferences instead of
%          their scaled versions
% Contents of subject variables are described in XYUheadings.
%   if useLandmarks, associated data is also stored such that
%   landmarks' coordinates are described by their names suffixed by "_x"(or y/z)
%   body parts' circumferences  are described by their names suffixed by "_circ"
% 

%
% 3) SAVE REGRESSION INFO
% ------------------------
% This stores information about the resulting regression...
% In future versions:
%     The names/label/codes for approaches should be formalised
%     The names/label/codes for this and other databases should be formalised
%     Names of headings should be formalised as "criteria"
regression.label=labelRegressionRun;
regression.approach='RegOpti_v1.0';
regression.originalDatabase=cellStrcat(originalDatabase);
regression.XYUheadings=XYUheadings;
regression.CUheadings=CUheadings;
% regression.CUheadingsTarget=CUheadingsTarget;
regression.bin=targetBin;
regression.binSource=sourceBin;
regression.P=P;
regression.Sigma_hat=Sigma_hat;
regression.XYU_mean=XYU_mean;
regression.scaleXP=scaleXP;
regression.scaleCirc=scaleCirc;
regression.scaleInv=scaleInv;
%
regression.XU=XU_vrt;
regression.YU=YU_vrt;
regression.CU=CU_vrt;
%
regression.XU_beforeUpsampling=XU;
regression.XUheadings=XUheadings;
regression.YUheadings=YUheadings;
%
end
%
%
%
function combinedStr=cellStrcat(CS)
%
%
combinedStr=CS{1};
%
for j=2:length(CS)
    combinedStr=[combinedStr,',',CS{j}];
end
%
end
