% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Ys,Ysheadings,Qs,Es]=sampleRegression(regression,Xs,...
                                         Xsheadings,averageOnly,mixNonSig)
%
% function [Ys,Ysheadings]=sampleRegression(regression,Xs,Xsheadings);
% function [Ys,Ysheadings,Qs,Es]=sampleRegression(regression,Xs,...
%                                                            Xsheadings);
%
% function [Ys,Ysheadings,Qs,Es]=sampleRegression(regression,Xs,...
%                                                Xsheadings,averageOnly);
%
% function [Ys,Ysheadings,Qs,Es]=sampleRegression(regression,Xs,...
%                                      Xsheadings,averageOnly,mixNonSig);
%
% Sampling of measurements for some values of predictors, following
%     regression information.  Parkinson&Reed (PR) approach [2010].
%
% Input: regression, is a structure containing the regression information,
%                    described by the following expressions
%                       Y = Q P + ones(size(X,1)) * YU_mean     (1)
%                       Q = X B(:,1:end-1) + B(:,end)+ E        (2)
%                    where:
%                       Q = regression.Q
%                       P = regression.P
%                       YU_mean = regression.YU_mean
%                       E = regression.E
%                       B = regression.B
%                    This is based on actual (X, Y) data.
%                    The indices of principal component columns of Q that
%                    depend significantly on the predictor X up to threshold
%                    p-values < psig are stored in sigQ:
%                       sigQ = regqression.sigQ                  (3)
%                       psig = regression.psig                  (4)
%                (note that B(:,end) contains the regression intercept)
%        Xs, values of the predictors for the desired samples
%                    Ys = Qs P + ones(size(X,1),1) * YU_mean
%            where Qs depends on Xs as follows:
%                    Qs = Xs B(:,1:end-1) + B(:,end) + Es
%            The errors Es are generally and by default sampled from
%            normal variables with variance equal to the residual mean square
%            (the square root of the residual sum of squares of the actual
%             (measured) errors E divided by the number of degrees of
%             freedom).
%            The (Parkinson-Reed) alternative is defined as described
%            below, when mixNonSig is active.
%        averageOnly, optional flag that indicates if the error Es are
%            forced to be zero or not.  [default:averageOnly=0]
%        mixNonSig, optional flag that indicates if the columns of Qs
%            corresponding to the principal components of Q that do not
%            depend significantly on the predictors are sampled differently
%            than as described above (see description of input Xs).
%            The alternative is the one used in the Parkinson-Reed paper:
%            for the columns that are not flagged in sigQ, the columns
%            of Qs are sampled randomly from those of Q.
%            (Note that this is only possible if the dimensions of Q and
%             Qs are equal, i.e. if the number size(Xs,1) of samples is
%             equal to the number of rows in Q. Otherwise, the default
%             approach is used.)
%
% --------------------------------
% Example
% -------
%   % See doc of runParkinsonReed_ANSUR
%   regression=runParkinsonReed_ANSUR(ANSURdir,popBinPercentage);
%
%   % double check that predictors are stature and BMI
%   if length(regression.XUheadings)==2
%      if strcmp(regression.XUheadings{1},'STATURE') && ...
%         strcmp(regression.XUheadings{2},'BMI')
%         Xs=[1600,30;1500,27];
%         Xsheadings={'STATURE','BMI'};
%         [Ys,Ysheadings]=sampleRegression(regression,Xs,Xsheadings);
%      end
%   end
%
% --------------------------------
% Christophe Lecomte
% University of Southampton
% work funded by the PIPER project
% Summer/Fall 2015
%
if nargin<4
    averageOnly=0;
end
if nargin<5
    mixNonSig=0;
end
if isfield(regression,'Slave')
  warning(['Slave regressions are not sampled.  Rather consider using ',...
              'sampleRegression_Slaves'])
end
%
treatedXs=zeros(size(Xsheadings));
XUheadings=regression.XUheadings;
for j=1:length(XUheadings)
  index_Xs=find(strcmp(Xsheadings,XUheadings{j}));
  if isempty(index_Xs)
    error('Not all predictor values are specified when sampling the regression')
  end
  treatedXs(index_Xs)=1;
  %
end
if sum(~treatedXs)
  warning('Some specified predictor values are unused in the regression')
end
%
%
%
B = regression.B;
P = regression.P;
Q = regression.Q;
sigQ = regression.sigQ;
YU_mean = regression.YU_mean;
E = regression.E;
YUheadings = regression.YUheadings;
Ysheadings=YUheadings;
%
sigma=zeros(size(E,2));
Es=zeros(size(Xs,1),size(E,2));
Qs=zeros(size(Xs,1),size(E,2));
%
% Treatment of "significantly" predictor dependent components
%
for k=1:length(sigQ)
    j=sigQ(k);
    % estimate the variance by the residual mean square
    % degrees of freedom (df) = number of cases - number of parameters in
    %                           the mean function.
    df=size(E,1)-size(B,1);
    sigma(j)=sqrt((sum(E(:,j).^2))/df);
    %
    if ~averageOnly
        % otherwise, Es=0.
        Es(:,j) = randn(size(Xs,1),1) * sigma(j);
    end
    Qs(:,j) = [Xs,ones(size(Xs,1),1)] * B(:,j) + Es(:,j);
end
%
% Treatment of NON-"significantly" predictor dependent components
%
nonQ=setdiff(1:size(Q,2),sigQ);
if mixNonSig && size(Xs,1)==size(Q,1)
    Qs(:,nonQ) = Q(:,nonQ( ceil(length(nonQ)*rand(length(nonQ),1)) ));
    if ~averageOnly
        % otherwise, Es=0.
        Es(:,nonQ) = Qs(:,nonQ)-[Xs,ones(size(Xs,1),1)]*B(:,nonQ);
    end
else
    for k=1:length(nonQ)
        j=nonQ(k);
        % estimate the variance by the residual mean square
        % degrees of freedom (df) = number of cases - number of parameters in
        %                           the mean function.
        df=size(E,1)-size(B,1);
        sigma(j)=sqrt((sum(E(:,j).^2))/df);
        %
        if ~averageOnly
            % otherwise, Es=0.
            Es(:,j) = randn(size(Xs,1),1) * sigma(j);
        end
        Qs(:,j) = [Xs,ones(size(Xs,1),1)] * B(:,j) + Es(:,j);
    end
end
%
Ys = Qs * P + ones(size(Xs,1),1) * YU_mean;
%
