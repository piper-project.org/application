% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% ParkinsonReed - Implementation of the approach proposed by Parkinson and Reed
%    to generate virtual user populations, and reproduction of reported
%    results for the same input data
%
%    [XU_up,YU_up,CU_up,...
%    Q,P,YU_mean,B,E,sigQ,XUheadings_updated]=...
%    ParkinsonReed(XU,XUheadings,YU,CU,CUheadings,...
%    targetBin,sourceBin,psig);
%
% * A POPULATION is described by BINS, i.e. sub-sets of population 
%                                        with given characteristics.
% * A TARGET virtual population is built from a SOURCE population.
% * The PERCENTAGE of bins within the whole population varies from the
%                      source to target populations.
% 
% Input:  - XU, predictor values, one row per subject, one column per data
%                    type.
%         - XUheadings, label (data type description) of each column of XU.
%         - YU, detailed measurements, one row per subject, one column per
%                    data type.
%         - CU, criteria, one row per subject, one column per
%                    data type.
%         - CUheadings, label (data type description) of each column of CU.
%         - targetBin, description of the desired bins of a target population
%                   This is a cell of structures, so that the bin j is
%                   targetBin{j}, the number of criteria values decribing
%                   it is targetBin{j}.Ncriteria, and each corresponding
%                   criteria is defined by a criteria label and value.
%                   For, criteria k of bin j, those are
%                      targetBin{j}.criteria{k}
%                      targetBin{j}.value{k}.
%                   A percentage of each bin in the total population is also
%                   given, say targetBin{j}.percentage for bin j.
%         - sourceBin, description of the desired bins of the target
%                   population as represented in the source population.
%                   The format is similar as that of the targetBin
%                   input, except that criteria may have different names
%                   in the source population and also that a single desired
%                   criteria value in the target population may correspond
%                   to several values in the source population.  For
%                   example (target) "hispanic" could be either
%                   "hispanic/mexican" or "hispanic/spanish".  In this
%                   case, the several values are separated by commas.
%                   Percentages are not specified for the source bins (as
%                   they can be evaluated).
%           - psig, p-value threshold to evaluate the significance of the
%                   dependency of the columns of Q to predictors X.
% - Output: - XU_up, upsampled predictor values, one row per subject, one
%                 column per data type.
%           - YU_up, upsampled detailed measurements, one row per subject,
%                 one column per data type.
%           - CU_up, upsampled criteria, one row per subject, one column per
%                 data type.
%           - Q, orthogonal matrix (Q'*Q= identity matrix I) of principal 
%                 components such that 
%                  YU_up = Q P + ones(size(YU_up,1)) * YU_mean
%           - P, matrix of loadings
%           - YU_mean, YU_mean = mean(YU_up)
%           - B, scoring matrix such that 
%                  Q = XU_up B(:,1:end-1) + B(:,end) + E
%           - E, is the matrix of errors (on Q) in the above expression
%                   the matrix of error on the detailed measurements is 
%                   therefore E * P.  Information about these errors
%                   may allow to reconstruct sample including variance
%                   (see Reference, for the "first z components", as well
%                   as the discussion for the other principal components
%                   that are not significantly dependent on the regression.)
%           - sigQ, table of indices of principal component columns exhibiting 
%                 significant regression as a function of the predictors
%                 p-value < psig (note that this may differ from the 
%                 Parkinson Reed approach to select the *first* z principal 
%                 components as significant).
%           - XUheadings_updated, updated XUheadings (label of each
%                 column of XU), in such a way that it includes the 
%                 intercept for the last column.
%
% Christophe Lecomte, 2015
% University of Southampton
% Work funded by the FP7 PIPER project
% Version 1.1
%
function [XU_vrt,YU_vrt,CU_vrt,...
    Q,P,YU_mean,B,E,sigQ,XUheadings_updated]=...
    ParkinsonReed(XU,XUheadings,YU,CU,CUheadings,...
    targetBin,sourceBin,psig)
%
% ------------------------------
% Generate a virtual population through (population bin) upsampling
[XU_vrt,YU_vrt,CU_vrt]=...
    populationUpsample(XU,YU,CU,CUheadings,sourceBin,targetBin);
%
% ------------------------------
% Generate the descriptive model, using the virtual population
[Q,P,YU_mean,B,E,sigQ,XUheadings_updated]=...
    multipleRegression(XU_vrt,XUheadings,YU_vrt,psig);
%
% ------------------------------
