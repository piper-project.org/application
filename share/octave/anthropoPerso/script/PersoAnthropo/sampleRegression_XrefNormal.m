% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Ys,Ysheadings,Xs]=sampleRegression_XrefNormal(...
               regression,Xref,Xsheadings,Nsamples,averageOnly)
%
% function [Ys,Ysheadings]=sampleRegression_XrefNormal(regression,Xref,...
%                                   Xsheadings,Nsamples);
% function [Ys,Ysheadings,Xs]=sampleRegression_XrefNormal(...
%                   regression,Xref,Xsheadings,Nsamples);
%
% function [Ys,Ysheadings,Xs]=sampleRegression_XrefNormal(...
%              regression,Xref,Xsheadings,Nsamples,averageOnly);
%
% Sampling of measurements for sampled random values of predictors,
%   following regression information.
%
% The values of the sampled values of the predictors, Xs, are evaluated,
%   based on a normal model, whose parameters are themselves evaluated
%   based on a reference set of sampled predictor values, Xref.
%
%      Xref => normal model => samples Xs from the normal model
%
% Input: - regression, is a structure containing the regression information.
%             ( See doc of sampleRegression for details
%                  The desired samples are
%                     Ys = Qs P + ones(size(X,1),1) * YU_mean
%                  where Qs depends on predictor samples Xs as follows:
%                     Qs = Xs B(:,1:end-1) + B(:,end) + Es
%                  Es are the errors... )
%        - Xref, reference sampled values of the predictors.
%                This is the source of the normal model from which
%                the actual samples Xs are drawn.
%
%                This is done as follows:
%               ==========================
%            1) the mean Xref_mean=mean(Xref,1) is evaluated
%            2) centred data is evaluated:
%                   Xref_centred=Xref-repmat(Xref_mean,size(Xref,1),1);
%            3) modes of the variance of centred data is computed:
%                   Sigma_xx=(Xref_centred.'*Xref_centred)/(size(Xref,1)-1);
%                  [Vxx,Dxx]=eig(Sigma_xx);
%            4) The j-th columns of XV = Xref_centred Vxx are estimated as
%                 Gaussian variables with mean Square root = Dxx(j,j)^(1/2)
%            5) One samples them:
%                  XV_centred=randn(Nsamples,size(Xref_mean,2))*Dxx.^(1/2);
%            6) Equivalent sampled centered vectors are
%                  Xs_centred=XV_centred*Vxx.';
%            7) equivalent samples are thus
%                  Xs=Xs_centred+repmat(Xref_mean,Nsamples,1);
%
%        - Xsheadings, labels of the columns of the Xref predictor array.
%
%        - averageOnly, optional flag that indicates if the error Es are
%            forced to be zero or not.  [default:averageOnly=0]
%
% --------------------------------
% Example
% -------
%   % See doc of runParkinsonReed_ANSUR
%   regression=runParkinsonReed_ANSUR(ANSURdir,popBinPercentage);
%
%   % double check that predictors are stature and BMI
%   if length(regression.XUheadings)==2
%      if strcmp(regression.XUheadings{1},'STATURE') && ...
%         strcmp(regression.XUheadings{2},'BMI')
%         Nsamples=100;
%         Xsheadings={'STATURE','BMI'};
%         [Ys,Ysheadings]=sampleRegression_XrefNormal(regression,...
%                   regression.XU,Xsheadings,Nsamples);
%      end
%   end
%
% --------------------------------
% Christophe Lecomte
% University of Southampton
% work funded by the PIPER project
% Summer/Fall 2015
%
% See also: sampleRegression
%
if nargin<5
    averageOnly=0;
end
%
%
Xref_mean=mean(Xref,1);    % based on the reference population
Xref_centred=Xref-repmat(Xref_mean,size(Xref,1),1);
%
% uncorrelate the centred data
Sigma_xx=(Xref_centred.'*Xref_centred)/(size(Xref,1)-1);
[Vxx,Dxx]=eig(Sigma_xx);
% One has...
% Sigma_xx Vxx = Vxx Dxx
% Vxx^T Sigma_xx Vxx = Dxx
% Vxx^T Xref_centred.'*Xref_centred  Vxx/(size(Xref,1)-1) = Dxx
% Therefore, columns of XV = Xref_centred Vxx are estimated as
%      Gaussian variables with mean Square root = Dxx^(1/2)
%
% One samples them:
XV_centred=randn(Nsamples,size(Xref_mean,2))*Dxx.^(1/2);
% Equivalent sampled centered vectors are
Xs_centred=XV_centred*Vxx.';
% equivalent samples are thus
Xs=Xs_centred+repmat(Xref_mean,Nsamples,1);
%
%
%
if strfind(regression.approach,'PR_v')==1
%   "ParkinsonReed" approach (first PCA, then multiple regressions)
     [Ys,Ysheadings]=sampleRegression(regression,Xs,Xsheadings,...
                                        averageOnly);
elseif  strfind(regression.approach,'RegOpti_v')==1
%   "Regression by optimisation" approach
      [PCdeficiency,errorCode]=paramPerso('PCdeficiency');
      if errorCode
          PCdeficiency=0;
          initParamPerso('PCdeficiency',PCdeficiency);
      end
      [Ys,Ysheadings]=...
                  sampleRegressionOpti(regression,Xs,...
                           Xsheadings,PCdeficiency,averageOnly);
else
    error('Unsupported regression type.')
end 
%
%
%
