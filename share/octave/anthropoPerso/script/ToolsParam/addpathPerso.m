% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [errorCode,errorMessage]=addpathPerso()
%
% function [errorCode,errorMessage]=addpathPerso();
%
% Add octave/matlab path for the personalisation PIPER code
%
% Output: errorCode, return error code.  If errorCode~=0, a problem occured
%            and an error message is returned in "errorMessage".
%         errorMessage, error message returned if an error occured.
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of Southampton
%
%
[codeDir,errorCode,errorMessage]=paramPerso('codeDir');
%
if errorCode
    error(errorMessage)
end
%
addpath(codeDir);
addpath([codeDir,filesep,'PersoAnthropo']);
addpath([codeDir,filesep,'PersoArticulation']);
addpath([codeDir,filesep,'PersoSSM']);
addpath([codeDir,filesep,'ToolsParam']);
addpath([codeDir,filesep,'ToolsDatabase']);
addpath([codeDir,filesep,'ToolsWP3']);
addpath([codeDir,filesep,'ToolsOther']);
%
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'scans']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'scans',filesep,'CCTc649']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'scans',filesep,'CCTc673']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'ReferencePostures']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'output']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'loadsave']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb',filesep,'kinematic']);
% addpath([codeDir,filesep,'yyyDevelopments',filesep,...
%     'ToolsRightLowerLimb']);
% addpath([codeDir,filesep,'yyyDevelopments']);
%
%
%
