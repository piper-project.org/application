% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function []=initParamPerso_Xtof(piperRoot)
%
% function initParamPerso_Xtof(piperRoot);
%
% Example of initialisation of personalisation tools of the PIPER project
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of SOuthampton
%
% See also: start, paramPerso, initParamPerso_Xtof
%
initParamPerso('databaseDir',...
    [piperRoot,filesep,'anthropoPerso',filesep,'dataBase']);
initParamPerso('codeDir',...
    [piperRoot,filesep,'anthropoPerso',filesep,'script']);
initParamPerso('resultsDir',...
    [piperRoot,filesep,'anthropoPerso',filesep,'samples']);
%
initParamPerso('DKdir',...
    [piperRoot,filesep,'ForwardKinematicsProgram']);
initParamPerso('DKmodelDir',...
    [paramPerso('DKdir'),filesep,'Input',filesep,'ModelStructure']);    
%
initParamPerso('ANSURdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_Ansur_1989']);
initParamPerso('SNYDERdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_Snyder_1977']);
initParamPerso('CCTANTHROdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_CCTanthro_2017']);
initParamPerso('CCTLANDMARKSdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_CCTlandmarks_2017']);
initParamPerso('CCTCIRCUMFdir',...
    [paramPerso('databaseDir'),filesep,'Dataset_CCTlandmarks_2017']);
%
