% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [code,message]=initParamPerso(variable,value)
%
% function initParamPerso(variable,value);
%
% Initialises the values of parameters of the personalisation PIPER code
%
% Input: The global parameter "variable" is assigned the value "value"
%
% The list of parameters expected can be found by list=initParamPerso()
%
% Example:
%   initParamPerso('databaseDir', ['C:',filesep,'local',filesep,...
%                                    'Piper_Database']);
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of SOuthampton
%
% See also: paramPerso, initParamPerso_Xtof, listParamPerso
%
global Lvariable Lvalue
%
code=0;          % return code
message='';      % return message
%
if nargin==0
    list={'databaseDir','root directory of PIPER statistical database';...
        'codeDir','root directory of PIPER perosnalisation codes';...
        'ANSURdir','root directory of PIPER ANSUR anthropometric dataset'};
    for j=1:size(list,1)
        disp([list{j,1},':',list{j,2}]);
    end
    % returns list as first output
    code=list;
elseif nargin==2
    k=0;
    for j=1:length(Lvariable)
        if strcmp(Lvariable{j},variable)
            k=j;
        end
    end
    %
    if ~k
        k=length(Lvariable)+1;
        eval(['Lvariable{k}=''',variable,''';']);
    end
    %
    Lvalue{k}=value;
    %
end
%
