% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [value,errorCode,errorMessage]=paramPerso(variable)
%
% function [value,errorCode,errorMessage]=paramPerso(variable);
%
% Returns values of parameters of the personalisation PIPER code
%
% Input: variable, a global variable that contains the parameter value
% Output: value, the desired parameter value.
%         errorCode, return error code.  If errorCode=0, the desired value exists
%               and has been returned.  Otherwise, a problem occured and
%               an error message is returned in "errorMessage".
%         errorMessage, error message returned if an error occured (if errorCode~=0).
%
% The current list of parameters can be found by a call to
%    [Lvariable,Lvalue]=paramPerso();
%
%    output: Lvariable, cell array of names of variables
%            Lvalue, cell array of corresponding values
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of Southampton
%
global Lvariable Lvalue
%
value='';             % output
errorCode=0;          % returned error code
errorMessage='';      % returned error message
%
if nargin==0
    value=Lvariable;
    errorCode=Lvalue;
else
    k=0;
    for j=1:length(Lvariable)
        if strcmp(Lvariable{j},variable)
            k=j;
        end
    end
    %
    if ~k
        errorCode=1;
        errorMessage=['The variable ',variable,' is not a supported parameter.'];
    elseif isempty(Lvalue{k})
        errorCode=2;
        errorMessage=['Please initialise ',variable,' using initParamPerso.'];
    else
        value=Lvalue{k};
    end
end
%
%
