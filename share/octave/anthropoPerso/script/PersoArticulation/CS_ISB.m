% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function varargout=CS_ISB(BP,varargin)
%
% Returns information about Coordinate Systems (CS) adapted from the
%    International Society of Biomechanics (ISB) recommendations
%    defined in the articles [1,2].
%
% Note that the definitions for the spine are somewhat different/adapted
%    from the ISB recommendations: the centres of vertebrae are defined
%    with respect to top and bottom plates of each vertebra body.
%
% Step 0 (script info):
% ======================
% The list of supported body parts and script version ID is returned by
%     a call without argument:
% [supportedBP,versionID]=CS_ISB();
%
% Otherwise, this code works in two steps:
%
% Step 1:
% ========
% [listLandmarks,listSurfacesSTLFilenames]=CS_ISB(BP);
%    returns a list of landmark names and STL surface filenames
%    (that are used to generate the coordinate system of a body part)
% input: - BP, body part name in PIPER WP2 database format (string of char)
% output: - listLandmarks, list of landmark names (cell array of strings
%                              of characters)
%         - listSurfacesSTLFilenames, list of STL surface filenames
%
% Step 2:
% ========
% [CS,O]=CS_ISB(BP,landmarks_1,landmark_2,...,surface_1,...);
% [CS,OR,OL]=CS_ISB(BP,landmarks_1,landmark_2,...,surface_1,...);
%    returns the coordinate system(s) direction and origin, from the
%    landmarks and STL surfaces identified in the Step 1 call.
% input: - BP, body part name in PIPER WP2 database format (string of char)
%        - landmarks_1, landmarks_2, ...: cartesian coordinates of the 
%             landmarks as indicated in the  list of landmark names,
%             listLandmarks.
%        - surface_1, surface_1, ...: names of STL files as indicated in 
%             the list of STL surface filenames, listSurfacesSTLFilenames.
% output: - CS, coordinate system, CS(:,i) is the i-th direction
%         - O, (single) origin of a body part
%         - OR, (right) origin of a body part
%         - OL, (left) origin of a body part
%
%
% Supported body parts:
% - 'Body_Pelvis'
% - 'Body_Femur_R'
% - 'Body_Femur_L'
% - 'Body_Tibia_R'
% - 'Body_Tibia_L'
% - 'Body_Scapula_R'
% - 'Body_Scapula_L'
% - 'Body_Humerus_R'
% - 'Body_Humerus_L'
% - 'Body_Radius_R'
% - 'Body_Radius_L'
% => Spine:
% -    'Body_Cervical_C1'
% -    'Body_Cervical_C2'
% -    'Body_Cervical_C3'
% -    'Body_Cervical_C4'
% -    'Body_Cervical_C5'
% -    'Body_Cervical_C6'
% -    'Body_Cervical_C7'
% -    'Body_Thoracic_T01'
% -    'Body_Thoracic_T02'
% -    'Body_Thoracic_T03'
% -    'Body_Thoracic_T04'
% -    'Body_Thoracic_T05'
% -    'Body_Thoracic_T06'
% -    'Body_Thoracic_T07'
% -    'Body_Thoracic_T08'
% -    'Body_Thoracic_T09'
% -    'Body_Thoracic_T10'
% -    'Body_Thoracic_T11'
% -    'Body_Thoracic_T12'
% -    'Body_Lumbar_L1'
% -    'Body_Lumbar_L2'
% -    'Body_Lumbar_L3'
% -    'Body_Lumbar_L4'
% -    'Body_Lumbar_L5'
% -    'Body_Sacrum'
%
% ========================================================================
% Example:
% --------
% [listLandmarks,listSurfacesSTLFilenames]=CS_ISB('Body_Tibia_R')
%
% MMR=[-42.6249	110.767	-1659.6];
% LMR=[15.721	95.6747	-1671.23];
% MCR=[13.2416	104.623	-1335.45];
% LCR=[76.311	121.522	-1341.41];
%
% [CS,O]=CS_ISB('Body_Tibia_R',MMR,LMR,MCR,LCR);
%
% ===============================================================
% Christophe Lecomte
% University of Southampton
% Nov.2015 - Feb.2016
% Work funded by the EU FP7 project PIPER
%
% (Early prototype of positioning tools have been developed by Pierre
%  Mailliez during an undergraduate internship under supervision by
%  C.Lecomte and Atul Bhaskar, in Spring 2015 at the University of
%  Southampton.)
%
% Ref.
% [1] Wu, Siegler, Allard, Kirtley, Leardini, Rosenbaum,
%     Whittle, D'Lima, Cristofolini, Witte, Schmid, Stokes,
%     "ISB recommendation on definitions of joint coordinate system of
%     various joints for the reporting of human joint motion - part I:
%     ankle, hip, and spine", Journal of Biomechanics 35 (2002) 543-548
% [2] Wu, van der Helm, Veeger, Makhsouse, Van Roy, Anglin, Nagels,
%     Karduna, McQuade, Wang, Werner, Buchholz, "ISB recommendation on
%     definitions of joint coordinate systems of various joints for
%     the reporting of human joint motion - Part II: shoulder, elbow,
%     wrist and hand", Journal of Biomechanics 38 (2005) 981-992
%

%
LSpineBones={'Body_Cervical_C1','Body_Cervical_C2','Body_Cervical_C3',...
    'Body_Cervical_C4','Body_Cervical_C5','Body_Cervical_C6',...
    'Body_Cervical_C7','Body_Thoracic_T01','Body_Thoracic_T02',...
    'Body_Thoracic_T03','Body_Thoracic_T04','Body_Thoracic_T05',...
    'Body_Thoracic_T06','Body_Thoracic_T07','Body_Thoracic_T08',...
    'Body_Thoracic_T09','Body_Thoracic_T10','Body_Thoracic_T11',...
    'Body_Thoracic_T12','Body_Lumbar_L1','Body_Lumbar_L2',...
    'Body_Lumbar_L3','Body_Lumbar_L4','Body_Lumbar_L5','Body_Sacrum'};
%
if nargin==0
    supportedBP={'Body_Pelvis','Body_Femur_R','Body_Femur_L',...
        'Body_Tibia_R','Body_Tibia_L','Body_Scapula_R','Body_Scapula_L',...
        'Body_Humerus_R','Body_Humerus_L','Body_Radius_R',...
        'Body_Radius_L',LSpineBones{:}};
    versionID='PIPER_CSISB_v1.0';
    varargout{1}=supportedBP;
    varargout{2}=versionID;
else
    %
    iSpine=find(strcmp(LSpineBones,BP));
    %
    if nargin==1
        infoOnly=1;
    else
        infoOnly=0;
    end
    %
    if strcmp(BP,'Body_Pelvis')
        % [1,section 4.3] "Pelvic coordinate system-XYZ (Fig.3)"
        % Pelvis
        if infoOnly
            varargout{1}={'LMK_RASIS','LMK_LASIS','LMK_RPSIS','LMK_LPSIS'};
            varargout{2}={'rotule_femur_R.stl','rotule_pelvis_R.stl',...
                'rotule_femur_L.stl','rotule_pelvis_L.stl'};
        else
            RASIS=varargin{1};
            LASIS=varargin{2};
            RPSIS=varargin{3};
            LPSIS=varargin{4};
            fileRotuleFemurR=varargin{5};
            fileRotulePelvisR=varargin{6};
            fileRotuleFemurL=varargin{6};
            fileRotulePelvisL=varargin{7};
            %
            % [1,section 4.3] "Pelvic coordinate system-XYZ (Fig.3)"
            axuPelvis=zeros(3);
            %
            axuPelvis(:,3)=RASIS-LASIS;
            axuPelvis(:,3)=axuPelvis(:,3)/norm(axuPelvis(:,3));
            %
            axuPelvis(:,2)=cross((LPSIS+RPSIS)/2-LASIS,axuPelvis(:,3));
            axuPelvis(:,2)=axuPelvis(:,2)/norm(axuPelvis(:,2));
            %
            axuPelvis(:,1)=cross(axuPelvis(:,2),axuPelvis(:,3));
            axuPelvis(:,1)=axuPelvis(:,1)/norm(axuPelvis(:,1));
            %
            [centre_hipR] = sphere_fitting(2,0,fileRotuleFemurR,fileRotulePelvisR);
            [centre_hipL] = sphere_fitting(2,0,fileRotuleFemurL,fileRotulePelvisL);
            %
            varargout{1}=axuPelvis;
            varargout{2}=centre_hipR;
            varargout{3}=centre_hipL;
            %
        end
        %
    elseif strcmp(BP,'Body_Femur_R')
        % [1,section 4.4] "Femoral coordinate system-xyz (Fig. 3)"
        %FEMUR :
        if infoOnly
            varargout{1}={'FEM_R','FEL_R'};
            varargout{2}={'rotule_femur_R.stl','rotule_pelvis_R.stl'};
        else
            FEMR=varargin{1};
            FELR=varargin{2};
            fileRotuleFemurR=varargin{3};
            fileRotulePelvisR=varargin{4};
            %
            [centre_hipR]=sphere_fitting(2,0,fileRotuleFemurR,fileRotulePelvisR);
            %
            axuFemurR=zeros(3);
            %
            %Definition of Y
            axuFemurR(:,2)=centre_hipR-(FEMR+FELR)/2;
            axuFemurR(:,2)=axuFemurR(:,2)/norm(axuFemurR(:,2));
            %Definition of X
            axuFemurR(:,1)= cross(axuFemurR(:,2),FELR-FEMR);
            %Definition of Z
            axuFemurR(:,3)=cross(axuFemurR(:,1),axuFemurR(:,2));
            axuFemurR(:,3)=axuFemurR(:,3)/norm(axuFemurR(:,3));
            %
            varargout{1}=axuFemurR;
            varargout{2}=centre_hipR;
            %
        end
        %
    elseif strcmp(BP,'Body_Femur_L')
        % [1,section 4.4] "Femoral coordinate system-xyz (Fig. 3)"
        %FEMUR :
        if infoOnly
            varargout{1}={'FEM_L','FEL_L'};
            varargout{2}={'rotule_femur_L.stl','rotule_pelvis_L.stl'};
        else
            FEML=varargin{1};
            FELL=varargin{2};
            fileRotuleFemurL=varargin{3};
            fileRotulePelvisL=varargin{4};
            %
            [centre_hipL]=sphere_fitting(2,0,fileRotuleFemurL,fileRotulePelvisL);
            %
            axuFemurL=zeros(3);
            %
            %Definition of Y
            axuFemurL(:,2)=centre_hipL-(FEML+FELL)/2;
            axuFemurL(:,2)=axuFemurL(:,2)/norm(axuFemurL(:,2));
            %Definition of X
            axuFemurL(:,1)= cross(axuFemurL(:,2),FEML-FELL);
            axuFemurL(:,1)= axuFemurL(:,1)/norm(axuFemurL(:,1));
            %Definition of Z
            axuFemurL(:,3)=cross(axuFemurL(:,1),axuFemurL(:,2));
            axuFemurL(:,3)=axuFemurL(:,3)/norm(axuFemurL(:,3));
            %
            varargout{1}=axuFemurL;
            varargout{2}=centre_hipL;
            %
        end
        %
    elseif strcmp(BP,'Body_Tibia_R')
        % [1,section 3.3] "Tibia/fibula coordinate system-XYZ (Fig. 1)"
        % TIBIA :
        % -------
        if infoOnly
            varargout{1}={'MM_R','LM_R','MC_R','LC_R'};
            varargout{2}={};
        else
            axuTibiaR=zeros(3);
            % Definition of Z
            MMR=varargin{1};
            LMR=varargin{2};
            axuTibiaR(:,3)=LMR-MMR;
            axuTibiaR(:,3)=axuTibiaR(:,3)/norm(axuTibiaR(:,3));
            %
            IMR=(MMR+LMR)/2;
            %
            % Definition of X
            [MCR]=varargin{3};
            [LCR]=varargin{4};
            [ICR]=(MCR+LCR)/2;
            %
            axuTibiaR(:,1)=cross(LMR-ICR,MMR-ICR);
            axuTibiaR(:,1)=axuTibiaR(:,1)/norm(axuTibiaR(:,1));
            %
            % Definition of Y
            axuTibiaR(:,2)=cross(axuTibiaR(:,3),axuTibiaR(:,1));
            axuTibiaR(:,2)=axuTibiaR(:,2)/norm(axuTibiaR(:,2));
            %
            varargout{1}=axuTibiaR;
            varargout{2}=IMR;
            %
        end
        %
    elseif strcmp(BP,'Body_Tibia_L')
        % [1,section 3.3] "Tibia/fibula coordinate system-XYZ (Fig. 1)"
        % TIBIA :
        % -------
        if infoOnly
            varargout{1}={'MM_L','LM_L','MC_L','LC_L'};
            varargout{2}={};
        else
            axuTibiaL=zeros(3);
            % Definition of Z
            MML=varargin{1};
            LML=varargin{2};
            axuTibiaL(:,3)=MML-LML;
            axuTibiaL(:,3)=axuTibiaL(:,3)/norm(axuTibiaL(:,3));
            %
            IML=(MML+LML)/2;
            %
            % Definition of X
            [MCL]=varargin{3};
            [LCL]=varargin{4};
            [ICL]=(MCL+LCL)/2;
            %
            axuTibiaL(:,1)=cross(MML-ICL,LML-ICL);
            axuTibiaL(:,1)=axuTibiaL(:,1)/norm(axuTibiaL(:,1));
            % Definition of Y
            axuTibiaL(:,2)=cross(axuTibiaL(:,3),axuTibiaL(:,1));
            axuTibiaL(:,2)=axuTibiaL(:,2)/norm(axuTibiaL(:,2));
            %
            varargout{1}=axuTibiaL;
            varargout{2}=IML;
            %
        end
        %
    elseif strcmp(BP,'Body_Scapula_R')
        % [2,section 2.3.3] "Scapula coordinate system-XsYsZs (Fig. 1 & 4)"
        % SCAPULA :
        if infoOnly
            varargout{1}={'AA_R','TS_R','AI_R'};
            varargout{2}={};
        else
            axuScapulaR=zeros(3);
            % Definition of Z
            AAR=varargin{1};
            TSR=varargin{2};
            axuScapulaR(:,3)=AAR-TSR;
            axuScapulaR(:,3)=axuScapulaR(:,3)/norm(axuScapulaR(:,3));
            % Definition of X
            AIR=varargin{3};
            axuScapulaR(:,1)=cross(axuScapulaR(:,3),AIR-TSR);
            axuScapulaR(:,1)=axuScapulaR(:,1)/norm(axuScapulaR(:,1));
            % Definition of Y
            axuScapulaR(:,2)=cross(axuScapulaR(:,3),axuScapulaR(:,1));
            axuScapulaR(:,2)=axuScapulaR(:,2)/norm(axuScapulaR(:,2));
            %
            varargout{1}=axuScapulaR;
            varargout{2}=AAR;
            %
        end
        %
    elseif strcmp(BP,'Body_Scapula_L')
        % [2,section 2.3.3] "Scapula coordinate system-XsYsZs (Fig. 1 & 4)"
        % SCAPULA :
        if infoOnly
            varargout{1}={'AA_L','TS_L','AI_L'};
            varargout{2}={};
        else
            axuScapulaL=zeros(3);
            % Definition of Z
            AAL=varargin{1};
            TSL=varargin{2};
            axuScapulaL(:,3)=TSL-AAL;
            axuScapulaL(:,3)=axuScapulaL(:,3)/norm(axuScapulaL(:,3));
            % Definition of X
            AIL=varargin{3};
            axuScapulaL(:,1)=cross(axuScapulaL(:,3),AIL-TSL);
            axuScapulaL(:,1)=axuScapulaL(:,1)/norm(axuScapulaL(:,1));
            % Definition of Y
            axuScapulaL(:,2)=cross(axuScapulaL(:,3),axuScapulaL(:,1));
            axuScapulaL(:,2)=axuScapulaL(:,2)/norm(axuScapulaL(:,2));
            %
            varargout{1}=axuScapulaL;
            varargout{2}=AAL;
            %
        end
        %
    elseif strcmp(BP,'Body_Humerus_R')
        % [2, section 2.3.4] "Humerus (1st option) coordinate system-
        %              Xh1Yh1Zh1 (see 1 and 5; see also notes 1 and 2)"
        % HUMERUS (First option of WU2005 - second option is not really possible
        %         with the body in arbitrary posture):
        if infoOnly
            varargout{1}={'EL_R','EM_R'};
            varargout{2}={'rotule_humerus_R.stl'};
        else
            ELR=varargin{1};
            EMR=varargin{2};
            fileRotuleHumerusR=varargin{3};
            %
            [centre_humerusR] = sphere_fitting(3,0,fileRotuleHumerusR);
            GHR=centre_humerusR;
            [centre_elbowR]=(EMR+ELR)/2;
            %
            axuHumerusR=zeros(3);
            %Definition of Y
            axuHumerusR(:,2)=GHR-centre_elbowR;
            axuHumerusR(:,2)=axuHumerusR(:,2)/norm(axuHumerusR(:,2));
            %
            %Definition of X
            dER= ELR-EMR;
            axuHumerusR(:,1)=cross(axuHumerusR(:,2),dER);
            axuHumerusR(:,1)=axuHumerusR(:,1)/norm(axuHumerusR(:,1));
            %
            %Definition of Z
            axuHumerusR(:,3)= cross(axuHumerusR(:,1),axuHumerusR(:,2));
            axuHumerusR(:,3)= axuHumerusR(:,3)/norm(axuHumerusR(:,3));
            %
            varargout{1}=axuHumerusR;
            varargout{2}=GHR;
            %
        end
        %
    elseif strcmp(BP,'Body_Humerus_L')
        % [2, section 2.3.4] "Humerus (1st option) coordinate system-
        %              Xh1Yh1Zh1 (see 1 and 5; see also notes 1 and 2)"
        % HUMERUS (First option of WU2005 - second option is not really possible
        %         with the body in arbitrary posture):
        if infoOnly
            varargout{1}={'EL_L','EM_L'};
            varargout{2}={'rotule_humerus_L.stl'};
        else
            ELL=varargin{1};
            EML=varargin{2};
            fileRotuleHumerusL=varargin{3};
            %
            [centre_humerusL] = sphere_fitting(3,0,fileRotuleHumerusL);
            GHL=centre_humerusL;
            [centre_elbowL]=(EML+ELL)/2;
            %
            axuHumerusL=zeros(3);
            %Definition of Y
            axuHumerusL(:,2)=GHL-centre_elbowL;
            axuHumerusL(:,2)=axuHumerusL(:,2)/norm(axuHumerusL(:,2));
            %
            %Definition of X
            dEL= EML-ELL;
            axuHumerusL(:,1)=cross(axuHumerusL(:,2),dEL);
            axuHumerusL(:,1)=axuHumerusL(:,1)/norm(axuHumerusL(:,1));
            %
            %Definition of Z
            axuHumerusL(:,3)= cross(axuHumerusL(:,1),axuHumerusL(:,2));
            axuHumerusL(:,3)= axuHumerusL(:,3)/norm(axuHumerusL(:,3));
            %
            varargout{1}=axuHumerusL;
            varargout{2}=GHL;
            %
        end
        %
    elseif strcmp(BP,'Body_Radius_R')
        % [2, section 3.3.4. Radius coordinate system-XrYrZr (defined with
        % forearm in the neutral position and elbow flexed 90 in the
        % sagittal plane)
        % RADIUS :
        if infoOnly
            varargout{1}={'RS_R','US_R','EL_R'};
            varargout{2}={};
        else
            RSR=varargin{1};
            USR=varargin{2};
            ELR=varargin{3};
            %
            axuRadiusR=zeros(3);
            % Definition of Y
            axuRadiusR(:,2)=ELR-RSR;
            axuRadiusR(:,2)=axuRadiusR(:,2)/norm(axuRadiusR(:,2));
            % Definition of X
            axuRadiusR(:,1)=cross(USR-RSR,axuRadiusR(:,2));
            axuRadiusR(:,1)=axuRadiusR(:,1)/norm(axuRadiusR(:,1));
            % Definition of Z
            axuRadiusR(:,3)=cross(axuRadiusR(:,1),axuRadiusR(:,2));
            axuRadiusR(:,3)=axuRadiusR(:,3)/norm(axuRadiusR(:,3));
            %
            varargout{1}=axuRadiusR;
            varargout{2}=RSR;
            %
        end
        %
    elseif strcmp(BP,'Body_Radius_L')
        % [2, section 3.3.4. Radius coordinate system-XrYrZr (defined with
        % forearm in the neutral position and elbow flexed 90 in the
        % sagittal plane)
        % RADIUS :
        if infoOnly
            varargout{1}={'RS_L','US_L','EL_L'};
            varargout{2}={};
        else
            RSL=varargin{1};
            USL=varargin{2};
            ELL=varargin{3};
            %
            axuRadiusL=zeros(3);
            % Definition of Y
            axuRadiusL(:,2)=ELL-RSL;
            axuRadiusL(:,2)=axuRadiusL(:,2)/norm(axuRadiusL(:,2));
            % Definition of X
            axuRadiusL(:,1)=cross(RSL-USL,axuRadiusL(:,2));
            axuRadiusL(:,1)=axuRadiusL(:,1)/norm(axuRadiusL(:,1));
            % Definition of Z
            axuRadiusL(:,3)=cross(axuRadiusL(:,1),axuRadiusL(:,2));
            axuRadiusL(:,3)=axuRadiusL(:,3)/norm(axuRadiusL(:,3));
            %
            varargout{1}=axuRadiusL;
            varargout{2}=RSL;
            %
        end
        %
    elseif iSpine
        % spine bone/vertebra index, iSpine, not zero...
        %
        % could/should rename VS and DS into VX and DX (X for Coccyx)
        % could/should rename 'endplate_sacrum.stl' into 'endplate_coccyx.stl'
        LLMKV={'VC1','VC2','VC3','VC4',...
            'VC5','VC6','VC7',...
            'VT1','VT2','VT3','VT4',...
            'VT5','VT6','VT7','VT8',...
            'VT9','VT10','VT11','VT12',...
            'VL1','VL2','VL3','VL4',...
            'VL5','VS'};
        %
        LLMKD={'DC1','DC2','DC3','DC4',...
            'DC5','DC6','DC7',...
            'DT1','DT2','DT3','DT4',...
            'DT5','DT6','DT7','DT8',...
            'DT9','DT10','DT11','DT12',...
            'DL1','DL2','DL3','DL4',...
            'DL5','DS'};
        %
        LPlate_up={'',...
            'endplate_up_C2.stl',...
            'endplate_up_C3.stl',...
            'endplate_up_C4.stl',...
            'endplate_up_C5.stl',...
            'endplate_up_C6.stl',...
            'endplate_up_C7.stl',...
            'endplate_up_T01.stl',...
            'endplate_up_T02.stl',...
            'endplate_up_T03.stl',...
            'endplate_up_T04.stl',...
            'endplate_up_T05.stl',...
            'endplate_up_T06.stl',...
            'endplate_up_T07.stl',...
            'endplate_up_T08.stl',...
            'endplate_up_T09.stl',...
            'endplate_up_T10.stl',...
            'endplate_up_T11.stl',...
            'endplate_up_T12.stl',...
            'endplate_up_L1.stl',...
            'endplate_up_L2.stl',...
            'endplate_up_L3.stl',...
            'endplate_up_L4.stl',...
            'endplate_up_L5.stl',...
            'endplate_sacrum.stl'};
        %
        LPlate_down={'endplate_down_C1.stl',...
            'endplate_down_C2.stl',...
            'endplate_down_C3.stl',...
            'endplate_down_C4.stl',...
            'endplate_down_C5.stl',...
            'endplate_down_C6.stl',...
            'endplate_down_C7.stl',...
            'endplate_down_T01.stl',...
            'endplate_down_T02.stl',...
            'endplate_down_T03.stl',...
            'endplate_down_T04.stl',...
            'endplate_down_T05.stl',...
            'endplate_down_T06.stl',...
            'endplate_down_T07.stl',...
            'endplate_down_T08.stl',...
            'endplate_down_T09.stl',...
            'endplate_down_T10.stl',...
            'endplate_down_T11.stl',...
            'endplate_down_T12.stl',...
            'endplate_down_L1.stl',...
            'endplate_down_L2.stl',...
            'endplate_down_L3.stl',...
            'endplate_down_L4.stl',...
            'endplate_down_L5.stl',...
            ''};
        %
        if infoOnly
            iC1=find(strcmp('VC1',LLMKV));
            iL5=find(strcmp('VL5',LLMKV));
            %
            varargout{1}={LLMKV{iSpine},LLMKD{iSpine},LLMKV{iC1},LLMKV{iL5}};
            varargout{2}={LPlate_up{iSpine},LPlate_down{iSpine}};
        else
            Vcurr=varargin{1};
            Dcurr=varargin{2};
            VC1=varargin{3};
            VL5=varargin{4};
            filePlateUp=varargin{5};
            filePlateDown=varargin{6};
            %
            if isempty(filePlateDown)
                % Coccyx
                [normal2,~,~,centre]=plane_fittingXtof(0,filePlateUp);
            elseif isempty(filePlateUp)
                % Cervical C1
                [normal2,~,~,centre]=plane_fittingXtof(0,filePlateDown);
            else
                % All other vertebrae (C2-C7, T01-T12, L1-L5)
                [normal2,~,~,centre]=plane_fittingXtof(0,filePlateUp,filePlateDown);
            end
            % normalv is pointing upwards (L5 to C1) and is used to reorient
            %         the vertebra's vertical normal, normal2, if necessary...
            normalv=(VC1-VL5);
            normalv=normalv/norm(normalv);
            %
            if (normalv(:).'*normal2(:))<0
%                    warning(['Reoriented (upwards) normal 2 of item ',num2str(item)])
                normal2=-normal2;
            end
            % Symmetry plane defined by vertical and by ventral and dorsal
            %                    landmarks
            normal1=cross(normal2,Vcurr-Dcurr);
            normal1=normal1/norm(normal1);
            %
            normal3=cross(normal1,normal2);
            %
            axuVertebra=[normal1(:),normal2(:),normal3(:)];
            %
            varargout{1}=axuVertebra;
            varargout{2}=centre;
            %
        end
        %
    end
    %
end
%
%
