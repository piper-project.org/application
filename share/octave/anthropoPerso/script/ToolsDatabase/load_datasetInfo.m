% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% load_datasetInfo - Reads information from a Piper dataset
%
% [dataset]=load_datasetInfo(inventoryFullName);
%
% Output: * each element of cell array dataset is an item of the dataset;
% ======= * each item dataset{j} is a structure whose fields are those of
%              the database, as dataset{j}.B3_Age for the "B3" column
%              that contains the "Age" of the subject corresponding to the
%              j-th dataset item (special characters are replaced by "_");
% Input: * inventoryName: name of the dataset inventory file.
% ======         The path of this name is used as the dataset directory
%                (files are read and written there).
%                The extension/type of the file should be csv, xls or xlsx.
%
% Options:
% =======
%     [datasetInfo]=load_datasetInfo(inventoryName,verbose,...
%                                      savemat,loadmat);
%
% Example:
% ========
%     cd Dataset_CEESAR_CTscan_LTE616_ceesar
%     inventoryName='Dataset_CEESAR_CTscan_LTE616_ceesar_v1.1.xls';
%     [CCTc616]=load_datasetInfo(inventoryName,1,1,1);
%
% Christophe Lecomte
% University of Southampton
% From load_CCTs670August 2014 - v1.0
% Nov.2015 - extracted from load_dataset v1.1
% 
%
% Work funded by EU Project Piper
%
% Reference: Piper project, files 'Piper_WP2_DataTemplate' and
%            'Dataset_Template'
%

% version 1.1: added special characters that are replaced in dataset field names
%              renamed "datasetDSN" into "dataset"
%
function [dataset]=load_datasetInfo(inventoryFullName,verbose,savemat,loadmat)
%
if nargin<2
    verbose=0;
end
if nargin<3
    savemat=0;
end
if nargin<4
    loadmat=0;
end
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave
  %  loadmat=1;
    if ~existPackage('io')
       pkg install -forge io     % installation
    end
    pkg load io     % loading statistics package
    %
end
%
[inventoryDir,~,~] = fileparts(inventoryFullName);
if isempty(inventoryDir)
    inventoryDir='.';
end
%
% A - import raw database information
% ------------------------------------
versionLoc=strfind(inventoryFullName,'_v');
suffixLoc=strfind(inventoryFullName,'.xls');
if isempty(suffixLoc)
    suffixLoc=strfind(inventoryFullName,'.csv');
end 
if ~isempty(versionLoc) && ~isempty(suffixLoc)
    version=inventoryFullName(versionLoc+2:suffixLoc-1);
    %
    disp(['loading VERSION ',version,' of the dataset information'])
end
%
if loadmat
    warning(strcat('Loading directly mat file datasetInfo_v',version,'.mat'))
    load('-mat',strcat(inventoryDir,filesep,'datasetInfo_v',version,'.mat'))
    %
else
%    [~,~,raw_dataset]=xlsread(inventoryFullName);
    inventoryFullName=strrep(inventoryFullName,'xlsx','csv');
    inventoryFullName=strrep(inventoryFullName,'xls','csv');
    [~,~,raw_dataset]=myCSVread(inventoryFullName);    
    %
    % The two first rows contain the column headings
    nItem=size(raw_dataset,1)-2;
    %
    % nItem=75; % (temporary) forced value while building the final database
    %
    % modify column labels to use as structure field names
    nField=size(raw_dataset,2);
    LField=cell(nField,1);
    for iField=1:nField
        LField{iField}=strcat(raw_dataset(1,iField),'_',raw_dataset(2,iField));
        LField{iField}=strrep(LField{iField},' ','_');
        LField{iField}=strrep(LField{iField},'/','_');
        LField{iField}=strrep(LField{iField},',','_');
        LField{iField}=strrep(LField{iField},':','_');
        LField{iField}=strrep(LField{iField},'(','_');
        LField{iField}=strrep(LField{iField},')','_');
    end
    %
    % B - read the database information
    % ----------------------------------
    % b1.initialise the dataset structure
    dataset=cell(nItem,1);
    % b2.load the database information
    for iItem=1:nItem
        %
        for iField=1:nField
            fieldName=LField{iField};         % note that fieldName is a cell
            dataset{iItem}.(fieldName{1})=raw_dataset(iItem+2,iField);
        end
    end
    %
    if verbose
        DSN=dataset{1}.A2_DSN;
        DSN=DSN{1};
        disp(['The inventory has been loaded - dataset DSN:',DSN])
    end
    %
    %
    if savemat
        if verbose
%             DSN=dataset{iItem}.A2_DSN;
%             DSN=DSN{1};
            disp(['Saving loaded dataset in mat file:',...
                'datasetInfo_v',version,'.mat...']);
        end
        %    save(strcat(DSN,'.mat'),'-mat','dataset')
        save(strcat(inventoryDir,filesep,'datasetInfo_v',version,'.mat'),'-mat','dataset')
    end
    %
end
%
