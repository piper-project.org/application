% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [X,modelBP,splitIndex,...
    RMeshCoordXRows,RMeshElem,subjectSRNXCols,dataDRNXCols]=...
    loadConfigRMeshBP(desiredBP,...
    LDatasetName,databaseDir,loadmat,savemat,verbose)
%
% [X,modelBP,splitIndex,...
%     RMeshCoordXRows,RMeshElem,subjectSRNXCols,dataDRNXCols]=...
%     loadConfigRMeshBP(desiredBP,...
%     LDatasetName,databaseDir,loadmat,savemat,verbose)
%
% load configurations (i.e. sets of nodes' coordinates) from PIPER
%      database's surface meshes.
%
%      The surfaces are chosen based on the body parte they are
%      attached to.
%
% Input: - desiredBP, list of desired body parts (as indicated in the
%               meshes dataset inventory file or in the sets of body
%               parts, as 'Body_LowerLimb', defined in the "allSubBP.m"
%               script), in the format of a cell array of strings.
%        - LDatasetName, (cell array) list of names of datasets of interest.
%        - databaseDir, location of the PIPER database (root directory);
%        - loadmat: flag that indicates if data is loaded directly from
%               a "*.mat" file (as opposed to being read from a *.CSV,
%               *.STL, or *.XLS file).  [default=0]
%        - savemat: flag that indicates if data is saved in
%               a "*.mat" file after having been read from inventory,
%               STL or data reference files.  [default=0]
%               For example, the arrays of nodes and elements read from
%               a particulat *.STL file are saved in two files with
%               names that have "_nodes.mat" and "_elems.mat"
%               extensions.
%        - verbose: flag that indicates if information is
%                   displayed during loading.
% Output: - X: configurations where X(i,j,k) is the j-th coordinate
%                   of the i-th node of the k-th configuration;
%         - modelBP, actual list of body parts (as indicated in the
%               meshes dataset inventory file) for which a mesh is
%               returned. This is a cell array of strings.
%               These actual body parts are included in the list of
%               desiredBP (for example, the actual body part,
%               'Body_Femur_R', is included in the desired body part
%               'Body_LowerLimb');
%         - splitIndex: contains indices indicating which body part
%               (splitIndex(k)=j indicates that k-th conf is part of
%                modelBP{j}) configurations are attached to.
%         - RMeshCoordXRows, reference mesh(es) associated to the
%               configurations.  (Note that in the current version, it
%               is the first configuration...)
%         - RMeshElem, indices of the nodes of the elements associated to
%               the various body parts.  RMeshElem{k} contains the
%               node numbers of the k-th body part.  (i-th node number is
%               stored in the i-th row of X.)
%         - subjectSRNXCols: contains the Subject Reference Codes
%               (SRN) associated with the columns of the configurations,
%               i.e. subjectSRNXCols{k} is the SRN of the subject on
%               which all nodes X(i,:,k) where measured.
%         - dataDRNXCols: contains the Data Reference Codes
%               (DRN) associated with the columns of the configurations,
%               i.e. dataDRNXCols{k} is the DRN of the data item
%               corresponding to all nodes X(i,:,k).
%
% ---------------------
%    % Example (Please check and enter your parameters...):
%    %
%    piperCodesDir='Piper_Codes';    % update this directory location
%    cd piperCodesDir;
%    start
%    %
%    databaseDir=paramPerso('databaseDir');
%    %
%    desiredBP=cell(1,2);
%    desiredBP{1}={'Body_Tibia_R'};
%    desiredBP{2}={'Body_Pelvis'};
%    %
%    LDatasetName={'CCTc'};
%    %
%    loadmat=0;
%    savemat=0;
%    verbose=1;
%    %
% [X,modelBP,splitIndex,...
%     RMeshCoordXRows,RMeshElem,subjectSRNXCols,dataDRNXCols]=...
%     loadConfigRMeshBP(desiredBP,...
%     LDatasetName,databaseDir,loadmat,savemat,verbose)
%    %
% % --------------
%
% C.Lecomte
% University of Southampton, Nov.2015
% Work funded by the EU FP7 PIPER project
%
% See also: shapeModel, demoSSMRMesh, sampleSSM, plotSSM, allSubBP
%
dataType='T001_SurfaceMesh';
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
%
% 0. PARAMETERS
% ==============
if nargin<6
    verbose=0;
end
if nargin<5
    savemat=0;
end
if nargin<4
    loadmat=0;
end
if nargin<3
%   Seeks the address of the PIPER database directory among the PIPER parameters
   databaseDir=paramPerso('databaseDir');
end
%
if length(LDatasetName)==1
    if strcmp(LDatasetName,'CCTc') || strcmp(LDatasetName,'CCTc_male') || ...
            strcmp(LDatasetName,'CCTc_female')
        % data series...
        [LDatasetName]=listDatasetName(LDatasetName);
        % INFO ON THIS DATASET IS NOT CORRECT (Discussed this with Baptiste):
        %                    'Dataset_CEESAR_CTscan_CCTc662_ceesar',...
    end
end
%
% LOOP on datasets
% =================
subjectSRNXCols=cell(length(LDatasetName),1);
dataDRNXCols=cell(length(LDatasetName),1);
RMeshElem=cell(length(desiredBP),1);
for iDataset=1:length(LDatasetName)
    %
    datasetName=LDatasetName{iDataset};
    % 1. LOAD THE RAW DATA
    % =====================
    % % %     % a) allocate the database address and tools
    % % %     addpath([databaseDir,filesep,'Tools']);
    % b) load the dataset data:
    inventoryName=findInventoryName(databaseDir,datasetName);
    fullInventoryName=[databaseDir,filesep,inventoryName];
    %
    if isOctave
        lengthName=length(fullInventoryName);
        if strfind(fullInventoryName,'.xls')==lengthName-3
            ftest=strrep(fullInventoryName,'.xls','.xlsx');
            if exist(ftest,'file')
                fullInventoryName=ftest;
                warning('Read data from ''xlsx'' instead of ''xls'' file:')
                warning([' ',fullInventoryName])
            else
                warning('Please check that octave reads the ''xls'' file:')
                warning([' ',fullInventoryName])
            end
            % %  % reading of CSV in octave is now supported (using myCSVread.m)
            % %         elseif strfind(fullInventoryName,'.csv')==lengthName-3
            % %             ftest=strrep(fullInventoryName,'.csv','.xlsx');
            % %             if exist(ftest,'file')
            % %                 fullInventoryName=ftest;
            % %                 warning('Read data from ''xlsx'' instead of ''csv'' file:')
            % %                 warning([' ',fullInventoryName])
            % %             else
            % %                 warning('Please check that octave reads the ''csv'' file:')
            % %                 warning([' ',fullInventoryName])
            % %             end
        end
    end
    %
    % desiredBP: requested BP to be considered
    % datasetBP: list of BP available in dataset
    % modelBP: returned BP in the configurations
    %
    % desiredBP=='All'  =>  return modelBP=datasetBP
    %               otherwise, return the intersection of datasetBP
    %                          and all subsets of desiredBP
    if iDataset==1
        takeAllBP=0;
        [datasetBP,dataset]=listBodyPart(fullInventoryName);
        if ischar(desiredBP)
            if strcmp(desiredBP,'All') || strcmp(desiredBP,'all')
                takeAllBP=1;
            else
                desiredBP={desiredBP};
            end
        end
        %
        if takeAllBP
            desiredBP=datasetBP;
        end
        %
    else
        [dataset]=load_datasetInfo(fullInventoryName,...
            verbose,savemat,loadmat);
    end
    %
    % 2. TREAT DATA DEPENDING ON ITS TYPE (only T001_SurfaceMesh allowed here...)
    % ====================================
    if strcmp(dataType,'T001_SurfaceMesh')
        % c) select meshes of interest
        %
        BP=cell(length(dataset),1);
        for j=1:length(dataset)
            % only look at mesh info...
            if strcmp(dataset{j}.D3_Type_of_information,'T001_SurfaceMesh')
                BP(j)=dataset{j}.D4_Body_parts_covered;
            end
        end
        %
        if iDataset==1
            % Build the list of model Body Parts on the first dataset...
            %      indBP
            modelBP={};
            indBP=zeros(length(modelBP),1);
            nModelBP=0;
            for j=1:length(BP)
                for ind=1:length(desiredBP)
                    if includesBP(desiredBP{ind},BP{j})
                        % Found body part of interest...
                        % Check if it was already found:
                        foundBP=strcmp(modelBP,BP{j});
                        if sum(foundBP)
                            warning(['Check the multiple presence of body part:',...
                                BP{j},' in dataset ',datasetName,...
                                '. Last occurence is used.']);
                            %
                            iBP=find(foundBP,1,'first');
                        else
                            nModelBP=nModelBP+1;
                            iBP=nModelBP;
                        end
                        modelBP{iBP}=BP{j};
                        %
                        indBP(iBP)=j;
                        if nModelBP==1
                            SRNdataset=dataset{j}.B1_SRN;
                        else
                            if ~strcmp(SRNdataset,dataset{j}.B1_SRN)
                                error('One and only one subject per dataset is currently supported by this script')
                            end
                        end
                        %
                    end
                    %
                end
            end
        else
            %
            for k=1:length(modelBP)
                foundBP=strcmp(modelBP{k},BP);
                if sum(foundBP)>1
                    warning(['Check the multiple presence of body part:',...
                        modelBP{k},' in dataset ',datasetName,...
                        '. Last occurence is used.']);
                end
                if sum(foundBP)<1
                    error(['Check the absence of body part:',...
                        modelBP{k},' in dataset ',datasetName]);
                end
                indBP(k)=find(foundBP,1,'last');
                % check that there is one and only one subject in this dataset
                %      for these body parts
                if k==1
                    SRNdataset=dataset{indBP(k)}.B1_SRN;
                else
                    if ~strcmp(SRNdataset,dataset{indBP(k)}.B1_SRN)
                        error('One and only one subject per dataset is currently supported by this script')
                    end
                end
                %
            end
            %
        end
        % Initialise data size from first encountered dataset
        inventoryDir=[databaseDir,filesep,datasetName];
        sizeNodesBP=zeros(length(modelBP),2,length(LDatasetName));
        for k=1:length(modelBP)
            iItem=indBP(k);
            dataset=load_dataItem(inventoryDir,dataset,iItem,verbose,...
                savemat,loadmat);
            sizeNodesBP(k,:,iDataset)=size(dataset{iItem}.nodes);
        end
        if iDataset==1
            nNodes=sum(sizeNodesBP(:,1,iDataset));
            nDim=sizeNodesBP(1,2,iDataset);
            X=zeros(nNodes,nDim,length(LDatasetName));
            splitIndex=zeros(nNodes,1);
            for k=1:length(modelBP)
                iItem=indBP(k);
                % Note the shift of node numbers as the configurations
                %                   may include several body parts...
                RMeshElem{k}=dataset{iItem}.elems+...
                    sum(sizeNodesBP(1:(k-1),1,iDataset));
            end
        else
            if nNodes~=sum(sizeNodesBP(:,1,iDataset))
                error('inconsistent number of nodes')
            end
            if nDim~=sizeNodesBP(:,2,iDataset);
                error('inconsistent number of dimensions')
            end
        end
        % INPUT FOR STATISTICAL SHAPE MODEL ANALYSIS
        for k=1:length(modelBP)
            iItem=indBP(k);
            indNodesSSM=sum(sizeNodesBP(1:(k-1),1,iDataset))+1:...
                sum(sizeNodesBP(1:k,1,iDataset));
            X(indNodesSSM,:,iDataset)=dataset{iItem}.nodes;
            splitIndex(indNodesSSM)=k;
            dataDRNXCols{iDataset}{end+1}=dataset{iItem}.D1_DRN{:};
        end
        %
        subjectSRNXCols(iDataset)=SRNdataset;
        %
    end
    %
end
%
warning('Returned reference nodes correspond to the first configuration')
% ... as the VOLMO reference model is not available.
RMeshCoordXRows=X(:,:,1);
%
%
%