% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% load_dataset - Import a Piper dataset
%
% [dataset]=load_dataset(inventoryName);
%
% Output: * each element of cell array dataset is an item of the dataset;
% ======= * each item dataset{j} is a structure whose fields are those of
%              the database, as dataset{j}.B3_Age for the "B3" column
%              that contains the "Age" of the subject corresponding to the
%              j-th dataset item (special characters are replaced by "_");
%         * additional fields contain the (unique) nodes and triangular
%              elements of the surface mesh:
%            -> dataset{j}.nodes is an array that contains the
%                  cartesian coordinates of the nodes (3 values, on
%                  row i for the x, y, z coordinates of the i-th node)
%            -> dataset{j}.elems is an array that contains the
%                  node numbers of the triangular surface elements
%                 (3 values, on row i for the nodes of the i-th element)
% Input: * inventoryName: name of the dataset inventory file.
% ======         The path of this name is used as the dataset directory
%                (files are read and written there).
%                The extension/type of the file should be csv, xls or xlsx.
%
% Options:
% =======
%     [dataset]=load_dataset(inventoryName,checkNormals,verbose,...
%                                                   savemat,loadmat);
%
%     checkNormals=1; % checks that normals all point outwards the mesh;
%     verbose=1; % activate the display of comments during data loading;
%     savemat=1; % save the output as a mat file named
%                % 'dataset_v',version,'.mat' where "version" is found
%                % from the name of the inventory file (inventoryName),
%                % specifically between "_v" and the extension as ".csv".
%                %
%             % where "dataset" is substituted by the variable content
%             %  (the mat file is loadable in octave - a free open source
%             % alternative to matlab.)
%     loadmat=1; % read data directly from the 'dataset.mat' mat file,
%                % rather than from the other data files.
%
% Example:
% ========
%     cd Dataset_CEESAR_CTscan_CCTc616_ceesar
%     inventoryName='Dataset_CEESAR_CTscan_CCTc616_ceesar_v1.1.xls';
%     [CCTc616]=load_dataset(inventoryName,1,1,1);
%
% Christophe Lecomte
% University of Southampton
% From load_CCTs670August 2014 - v1.0
% Aug.2015
% v1.1
%
% Work funded by EU Project Piper
%
% Reference: Piper project, files 'Piper_WP2_DataTemplate' and
%            'Dataset_Template'
%

% version 1.1: added special characters that are replaced in dataset field names
%              renamed "datasetDSN" into "dataset"
%
function [dataset]=load_dataset(inventoryFullName,checkNormals,verbose,savemat,loadmat)
%
if nargin<2
    checkNormals=0;
end
if nargin<3
    verbose=0;
end
if nargin<4
    savemat=0;
end
if nargin<5
    loadmat=0;
end
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
if isOctave
    loadmat=1;
    if ~existPackage('statistics')
        pkg install -forge statistics     % installation
    end
    pkg load statistics     % loading statistics package
end
%
[inventoryDir,~,~] = fileparts(inventoryFullName);
if isempty(inventoryDir)
    inventoryDir='.';
end
%
% A - import raw database information
% ------------------------------------
if loadmat
    warning(strcat('Loading directly mat file dataset_v',version,'.mat'))
    load('-mat',strcat(inventoryDir,filesep,'dataset_v',version,'.mat'))
    %
else
    [dataset]=load_datasetInfo(inventoryFullName,verbose,savemat,loadmat);
    nItem=length(dataset);
    % b3.load the meshes from files
    for iItem=1:nItem
        %
        dataset=load_dataItem(inventoryDir,dataset,iItem,verbose,savemat,loadmat);
% % %         if strcmp(dataset{iItem}.D6_Format_of_information,'F001_MeshSurfSTL')
% % %             % actual (mesh) data is stored in a file whose name is in column "D9"
% % %             fileName=dataset{iItem}.D9_Location_of_data;
% % %             fileName=strtrim(strrep(fileName,'file:','')); % remove prefix "file:"
% % %             %
% % %             dataFormat=dataset{iItem}.D7_Format_of_data{1};
% % %             if strfind(dataFormat,'Binary')
% % %                 if exist([inventoryDir,filesep,fileName{1}], 'file')
% % %                     if verbose
% % %                         disp(strcat('Importing data from file:',fileName{1},'...'));
% % %                     end
% % %                     [v,f]=stlread([inventoryDir,filesep,fileName{1}]);    % note that fileName is a cell
% % %                 else
% % %                     warning(strcat('inexisting binary file',fileName{1}))
% % %                     fileNameAsc=strrep([inventoryDir,filesep,fileName{1}],'bin','asc');
% % %                     if exist(fileNameAsc, 'file')
% % %                         if verbose
% % %                             disp(strcat('Importing data from file:',fileNameAsc,'...'));
% % %                         end
% % %                         [v,f,~]=import_stl_fastCL([inventoryDir,filesep,fileNameAsc],1);
% % %                     else
% % %                         disp(strcat('WARNING - INEXISTING file:',fileNameAsc,'...'));
% % %                     end
% % %                 end
% % %             elseif strfind(dataFormat,'ascii')
% % %                 if verbose
% % %                     disp(strcat('Importing data from file:',fileName{1},'...'));
% % %                 end
% % %                 [v,f,~]=import_stl_fastCL([inventoryDir,filesep,fileName{1}],1);
% % %             end
% % %             %
% % %             [dataset{iItem}.nodes,~,IC]=unique(v,'rows','stable');
% % %             %    fv.vertices=dataset{iItem}.nodes(IC),
% % %             %    i.e. IC(i) gives new node number of initial node number, i
% % %             dataset{iItem}.elems=IC(unique(f,'rows'));
% % %             % Note the "renumbering" in the previous line: unique(fv.faces,'rows')
% % %             % contains the initial (non-unique) node numbers...
% % %             %
% % %         elseif strcmp(dataset{iItem}.D6_Format_of_information,'F004_NoCart3D, unit=mm')
% % %             % b4.load the description of dataset landmarks
% % %             fieldFileLandmarkInfo='D5_List_of_information';
% % %             fileLandmarkInfo=dataset{iItem}.(fieldFileLandmarkInfo);
% % %             fileLandmarkInfo=strrep(fileLandmarkInfo{1},'file:','');
% % %             % note that the data is read from the inventory directory
% % %             [~,~,rawListLandmarks]=xlsread([inventoryDir,filesep,fileLandmarkInfo]);
% % %             if ~strcmp(rawListLandmarks{1,1},'bin number in dataset')
% % %                 error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% % %             end
% % %             if ~strcmp(rawListLandmarks{1,2},'reference code')
% % %                 error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% % %             end
% % %             if ~strcmp(rawListLandmarks{1,3},'version')
% % %                 error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% % %             end
% % %             for j=2:size(rawListLandmarks,1)
% % %                 listLMKBinNumber(j-1)=rawListLandmarks{j,1};
% % %                 listLMKRefCode{j-1}=rawListLandmarks{j,2};
% % %                 listLMKVersion{j-1}=rawListLandmarks{j,3};
% % %             end
% % %             % b5.load thelandmarks from files
% % %             
% % %             fileName=dataset{iItem}.D9_Location_of_data;
% % %             fileName=strrep(fileName,'file:',''); % remove prefix "file:"
% % %             %
% % %             fieldFormatInfo='D6_Format_of_information';
% % %             fieldFormatData='D7_Format_of_data';
% % %             %
% % %             if ~strcmp(dataset{iItem}.(fieldFormatInfo),'F004_NoCart3D, unit=mm')
% % %                 error(['Check format of information for item ',num2str(iItem),' in dataset dataset']);
% % %             end
% % %             if ~strcmp(dataset{iItem}.(fieldFormatData),'ASCII, csv')
% % %                 error(['Check format of data for item ',num2str(iItem),' in dataset dataset']);
% % %             end
% % %             %
% % %             if exist([inventoryDir,filesep,fileName{1}], 'file')
% % %                 if verbose
% % %                     disp(strcat('Importing data from file:',fileName{1},'...'));
% % %                 end
% % %                 M=csvread([inventoryDir,filesep,fileName{1}]);    % note that fileName is a cell
% % %             else
% % %                 disp(strcat('WARNING - INEXISTING file:',fileName{1},'...'));
% % %             end
% % %             %
% % %             dataset{iItem}.landmarks=M(:,2:4);
% % %             dataset{iItem}.unit='mm';
% % %             dataset{iItem}.lmkBinNumber=M(:,1);
% % %             indLmkBinNumber=zeros(size(dataset{iItem}.lmkBinNumber));
% % %             for k=1:length(indLmkBinNumber)
% % %                 indLmkBinNumber(k)=find(listLMKBinNumber==dataset{iItem}.lmkBinNumber(k));
% % %             end
% % %             dataset{iItem}.lmkReference=listLMKRefCode(indLmkBinNumber).';
% % %             dataset{iItem}.lmkRefVersion=listLMKVersion(indLmkBinNumber).';
% % %         else
% % %             warning(['Format of information (',dataset{iItem}.D6_Format_of_information,...
% % %                 ', is not recognised for item:',num2str(iItem)])
% % %         end
    end
    %
    % C - check meshes (optional)
    % ----------------------------
    if checkNormals
        % c1.check normals
        %    The normals are oriented in a right-hand rule order: If the nodes of
        %    the triangle are numbered in order 1,2,3, the normal points in the
        %    direction of the cross-roduct of the oriented edges v_12 going from
        %    vertices 1 to 2 and v_23 going from vertices 2 to 3:
        %    n=(v_12 x v_23)/(| v_12 x v_23 |)
        %    In other words, if the triangle is in the plane as drawn,
        %         1 ------------ 2
        %           \           /
        %             \       /
        %               \   /
        %                 3
        %    then the normal points inside the page (further away from the reader).
        %    The STL surface mesh is assumed to be "regular", i.e. a) none of the
        %    triangles is degenerated with colinear edges, b) the mesh encloses
        %    a closed volume, c) there is no contact between different points on
        %    the surface mesh, or in other words, no two different triangles
        %    cross or touch in the 3d space except for topologically adjacent ones
        %    (except if they share one or two nodes in which case, contact only
        %    occurs at the corresponding shared node or edge).
        %
        %    Verification below is only that the orientation of normals is
        %    consistent: all normals point inside or outside the enclosed volume.
        %    The test proceeds by checking that every single edge appears
        %    twice (the two nodes appear in opposite circular order in the
        %    two adjacent triangles that include it).
        %
        for iItem=1:nItem
            if strcmp(dataset{iItem}.D6_Format_of_information,'F001_MeshSurfSTL')
                DRN=dataset{iItem}.D1_DRN{1};
                % checking that meshes are not too large for the normals test
                %     (this could be based on the number of nodes/elements.)
                runTest=1;
                if strcmp(DRN,'CCTs635_BP251_Skull')
                    warning('data item CCTs635_BP251_Skull is not checked.')
                    runTest=0;
                end
                if strcmp(DRN,'CCTs635_BP301_Skin')
                    warning(['data item CCTs635_BP301_Skin is not checked as ',...
                        'its mesh is too large.'])
                    runTest=0;
                end
                if strcmp(DRN,'CCTs635_BP901_Skeleton')
                    warning(['data item CCTs635_BP901_Skeleton is not checked as ',...
                        'its mesh is too large.'])
                    runTest=0;
                end
                %
                if runTest
                    if verbose
                        disp(strcat('Checking normals of mesh for data item:',...
                            DRN,'...'));
                    end
                    LElems=dataset{iItem}.elems;   % number nodes in sequential order
                    NElems=size(LElems,1);
                    NNodes=size(dataset{iItem}.nodes,1);
                    NNeigh=NElems*3/2;
                    LNeigh=zeros(NNeigh,2); % two neighbour elements for each edge
                    LEdges=spalloc(NNodes,NNodes,NNeigh); % upper triangular array of edge #s
                    %
                    newNeigh=1;
                    for iElem=1:NElems
                        for iEdge=1:3
                            N1=LElems(iElem,iEdge);
                            N2=LElems(iElem,mod(iEdge,3)+1);
                            %
                            if N2>N1
                                signE=1;
                            else
                                signE=-1;
                            end
                            if LEdges(min(N1,N2),max(N2,N1))==0
                                LEdges(min(N1,N2),max(N2,N1))=newNeigh;
                                LNeigh(newNeigh,1)=signE*iElem;
                                newNeigh=newNeigh+1;
                            else
                                if LNeigh(LEdges(min(N1,N2),max(N2,N1)),2)~=0
                                    error(['multiple facet connectivity, ITEM #',num2str(iItem)])
                                else
                                    LNeigh(LEdges(min(N1,N2),max(N2,N1)),2)=signE*iElem;
                                end
                            end
                        end
                    end
                    % Check that each edge connects 2 elements with correct orientation
                    check=sign(LNeigh(:,1)).*sign(LNeigh(:,2));
                    if ~isempty(check(check>0))
                        error('inconsistent normal orientation')
                    end
                    % Check if normals point inwards or outwards
                    % ====
                    %     pick the highest node, then the highest edge
                    LNodes=dataset{iItem}.nodes;
                    iHigher=find(LNodes(:,3)==max(LNodes(:,3)));  % node with max z value
                    iHigher=iHigher(1);
                    HCoord=LNodes(iHigher,:);    % COORDinates of Higher point
                    [Helem,Hnode]=find(LElems==iHigher);  % Higher elements
                    % look for edge passing through highest node that has smallest slope
                    LHelem=LElems(Helem,:);   % nodes of elements connected to highest node
                    other=ones(size(Helem,1),2); % other nodes...
                    %
                    maxSlope=-1;
                    maxi=1;
                    maxj=1;
                    for i=1:size(other,1)
                        other(i,1:2)=LHelem(i,setdiff(1:3,Hnode(i)));
                        % unit vector of the edges connected to the highest node
                        for j=1:2
                            % COORDinates of Other points
                            OCoord=LNodes(other(i,j),:);
                            Vector=OCoord-HCoord;  % non-normalised vector
                            UnitV=Vector/norm(Vector);
                            %
                            if UnitV(:,3)>=maxSlope
                                maxi=i;
                                maxj=j;
                                maxSlope=UnitV(:,3);
                                maxV=UnitV;  % preserve direction of higher edge
                            end
                        end
                    end
                    %
                    i2Higher=other(maxi,maxj); % index of node corresponding to higher edge
                    i2Higher=i2Higher(1);
                    %
                    % Treat the two elements connected to the highest edge and find the
                    %     highest one
                    [ind,H2node]=find(LHelem==i2Higher);
                    H2elem=Helem(ind);
                    Hnode=Hnode(ind);  % reduced set of elements (H2elem)
                    if length(ind)~=2
                        error('wrong connectivity') % just in case...
                    end
                    %
                    N1=LElems(H2elem(1),setdiff(1:3,[Hnode(1),H2node(1)]));
                    N2=LElems(H2elem(2),setdiff(1:3,[Hnode(2),H2node(2)]));
                    %
                    % Check which of the two elements is higher.
                    %    Upper orientation of the normal of this
                    %    element indicates that all normals point outwards.
                    Vec1=LNodes(N1,:)-HCoord;
                    Vec2=LNodes(N2,:)-HCoord;
                    % Look at slope of the two elements compared to a horizontal line in
                    %    the vertical plane containing the highest edge.
                    % horizontal vector (in the vertical plane of the highest edge)
                    Vhoriz=maxV;Vhoriz(3)=0;
                    Vhoriz=Vhoriz/norm(Vhoriz);  % unit horizontal vector
                    % take a vector in each element, remove its component in the
                    %     horizontal direction.  The angle between the remaining vectors
                    %     and the vertical gives indication of the slopes of the
                    %     elements compared to the horizontal vector.  The larger this
                    %     angle, the higher the element.
                    Vec1=Vec1-Vhoriz*(Vhoriz(:)'*Vec1(:));
                    Vec1=Vec1/norm(Vec1);
                    Vec2=Vec2-Vhoriz*(Vhoriz(:)'*Vec2(:));
                    Vec2=Vec2/norm(Vec2);
                    %
                    if Vec1(:,3) > Vec2(:,3)
                        i3Higher=N1;
                        H3elem=H2elem(1);
                    else
                        i3Higher=N2;
                        H3elem=H2elem(2);
                    end
                    % H3elem is the "highest element" (in the neighborhood of the highest
                    %                                                       point)
                    % the vertical component of its normal indicates if normal points
                    %     outwards or inwards the surface mesh...
                    v12=LNodes(LElems(H3elem,2),:)-LNodes(LElems(H3elem,1),:);
                    v23=LNodes(LElems(H3elem,3),:)-LNodes(LElems(H3elem,2),:);
                    % cross product of the two vectors (v12 x v23)
                    normal=cross(v12,v23);      % not normalised to unit length!
                    %
                    if normal(3)<0
                        error(strcat('normal of item ',num2str(iItem),...
                            ' does not point outwards'));
                    end
                    %
                end
            end
        end
        % ====
    end
    %
    if savemat
        if verbose
            %             DSN=dataset{iItem}.A2_DSN;
            %             DSN=DSN{1};
            disp(['Saving loaded dataset in mat file:',...
                'dataset_v',version,'.mat...']);
        end
        %    save(strcat(DSN,'.mat'),'-mat','dataset')
        save(strcat(inventoryDir,filesep,'dataset_v',version,'.mat'),'-mat','dataset')
    end
    %
end
%
