% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function listBP=formattedBP(listBP)
%
% Formats a list of Body Parts (BP) so that:
%      - the returned list is a cell list;
%      - duplicates are suppressed from the list;
%      - any string with commas is splitted into individual strings at the
%         level of the commas.
%
% See Also: includesBP,allSubBP, formattedCV
%
% Christophe Lecomte
% Jan. 2016
% University of Southampton
% EU FP7 project PIPER
%
if ischar(listBP)
    listBP={listBP};
end
%
[listBP,order,~]=unique(listBP);
[~,ind]=sort(order);
listBP=listBP(ind);
%
j=0;
while j<length(listBP)
    BP=listBP{j+1};
    %
    icomma=strfind(BP,',');
    if ~isempty(icomma)
        realBP=BP(1:icomma(1)-1);
        remainingBP=BP(icomma(1)+1:end);
        listBPbefore=listBP(1:j);
        listBPafter=listBP(j+2:end);
        %
        [listBP,order,~]=unique([listBPbefore(:).',...
            {realBP},listBPafter(:).',{remainingBP}]);
        [~,ind]=sort(order);
        listBP=listBP(ind);
    else
        j=j+1;
    end
end
%
end
%
% =================
%
function listBP=addBP(listBP,varargin)
%

% The three (active) lines of code below achieve the same as the 
%     commented line in Matlab.  The 'stable' option is however not 
%     supported in Octave.
% listBP=unique([listBP(:).',varargin(:).'],'stable');
% =>
    [listBP,order,~]=unique([listBP(:).',varargin(:).']);
    [~,ind]=sort(order);
    listBP=listBP(ind);
%
end
%
% =================
