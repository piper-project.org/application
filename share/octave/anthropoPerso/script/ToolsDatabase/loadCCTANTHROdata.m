function [Adata,Aheading]=loadCCTANTHROdata(CCTANTHROdir,loadMat,withBMI)
% loadCCTANTHROdata - Load the CCTANTHRO data.
%
% =========================================================================
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% =========================================================================
%
% [Adata,Aheading]=loadCCTANTHROdata(CCTANTHROdir);
% [Adata,Aheading]=loadCCTANTHROdata(CCTANTHROdir,loadMat);
%
% Load the CCTANTHRO data.
%
% Input: - CCTANTHROdir, location of the CCTANTHRO directory.
%        - loadMat, optional argument.  If loadMat, the data is loaded from 
%           a binary (Matlab) file that has the same name as the ASCII 
%           CSV file, except for the extension '.mat' instead of '.csv'.
% Output: - CCTANTHROdata, actual array of data, one row per subject, one column
%           per data type.
%         - CCTANTHROheading, label (data type description) of each column.
%
% The data has been furnished by CEESAR in the context of the PIPER project
% 
% Information about the measurements and the codes used in the dataset 
%    can be found within the CCTANTHRO reports.
% 
% Christophe Lecomte, 2017
% University of Southampton
% Work funded by the FP7 PIPER project
%
%

if nargin<1
%   Seeks the address of the CCTANTHRO directory among the PIPER parameters
   CCTANTHROdir=paramPerso('CCTANTHROdir');
end
%
strrep(CCTANTHROdir,'/',filesep);
strrep(CCTANTHROdir,'\',filesep);
%
if nargin<2
    loadMat=0;
end
% forces loadMat to zero (this could be updated if needed and
%                         if the *.mat is made available)
% loadMat = 0
%
if nargin<3
    withBMI=1;
end
%
% Note that the name (and location) of the  
if withBMI
    filenameCSV='cctanthro_withBMI.csv';
else
    filenameCSV='cctanthro.csv';
end
%
if loadMat
    %
    filenameMAT=regexprep(filenameCSV,'.csv$','.mat');
    load([CCTANTHROdir,filesep,filenameMAT]);
%
else
    nHeadRows=1;     % number of heading rows
    separator=',';   % separator between items
    alphaBnd='"';    % delimitor of string values
    isnumData=1;     % data is numeric
    [Adata,Aheading]=myCSVread([CCTANTHROdir,filesep,filenameCSV],...
                    nHeadRows,separator,alphaBnd,isnumData);
    %    
    filenameMAT=regexprep(filenameCSV,'.csv$','.mat');
    save('-mat-binary',[CCTANTHROdir,filesep,filenameMAT],'Adata','Aheading');
    %   
end
