function [X,modelBP,landmarkCodesXRows,subjectSRNXCols]=...
    loadCCTLANDMARKSdata(CCTLANDMARKSdir,loadMat)
% loadCCTLANDMARKSdata - Load the CCTLANDMARKS data (configuration).
%
% =========================================================================
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
%
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
%
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
%
% Contributors include Christophe Lecomte (University of Southampton)
%
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% =========================================================================
%
% [X,modelBP,landmarkCodesXRows,subjectSRNXCols]=...
%                              loadCCTLANDMARKSdata(CCTLANDMARKSdir);
% [X,modelBP,landmarkCodesXRows,subjectSRNXCols]=...
%                              loadCCTLANDMARKSdata(CCTLANDMARKSdir,loadMat);
%
% Input: - CCTLANDMARKSdir, location of the CCTLANDMARKS directory.
%        - loadMat, optional argument.  If loadMat, the data is loaded from
%           a binary (Matlab) file that has the same name as the ASCII
%           CSV file, except for the extension '.mat' instead of '.csv'.
% Output: - X: configurations where X(i,j,k) is the j-th coordinate
%                   of the i-th landmark of the k-th configuration;
%         - modelBP, actual list of body parts (as provided in the
%               "bone" column of the data files) for which a landmark is
%               returned. This is a cell array of strings.
%         - landmarkCodesXRows: contains the landmark Codes (as provided in the
%               "bone" column of the data files) associated with the rows of
%               configurations X(i,j,k).
%         - subjectSRNXCols: contains the Subject Numbers associated
%               with the columns of the configurations,
%               i.e. subjectSRNXCols{k} is the SRN of the subject on
%               which all landmarks X(i,:,k) where measured.
%
% These landmarks have been measured by UCBL and CEESAR, on CT scans provided
%            by CEESAR in the context of the PIPER project
%
% Christophe Lecomte, 2017
% University of Southampton
% Work funded by the FP7 PIPER project
%
%
if nargin<1
    %   Seeks the address of the CCTLANDMARKSdir directory among the PIPER parameters
    CCTLANDMARKSdir=paramPerso('CCTLANDMARKSdir');
end
%
strrep(CCTLANDMARKSdir,'/',filesep);
strrep(CCTLANDMARKSdir,'\',filesep);
%
if nargin<2
    loadMat=0;
end
%
% get all the subject SRNs
%
list=dir(CCTLANDMARKSdir);
%
nSubjects=0;
subjectSRNXCols=cell(0);
for j=1:length(list)
    currentName=list(j).name;
    ind=findstr(currentName,'_');
    if ~isempty(ind)
        ind=ind(1);
        prefix=currentName(1:ind-1);
        suffix=currentName(ind+1:end);
        %
        if strcmp(suffix,'lm_for_stats_landmarks.csv')
            % additional subject data
            nSubjects=nSubjects+1;
            subjectSRNXCols{nSubjects}=['LTE',prefix];
            %
            filenameCSV=currentName;
            if loadMat
                %
                filenameMAT=regexprep(filenameCSV,'.csv$','.mat');
                load([CCTLANDMARKSdir,filesep,filenameMAT]);
                %
            else
                nHeadRows=1;     % number of heading rows
                separator=',';   % separator between items
                alphaBnd='"';    % delimitor of string values
                isnumData=0;     % data is numeric
                [Adata,Aheading]=myCSVread([CCTLANDMARKSdir,filesep,...
                    filenameCSV],nHeadRows,separator,alphaBnd,isnumData);
                %
                %%%               filenameMAT=regexprep(filenameCSV,'.csv$','.mat');
                %%%               save('-mat-binary',[CCTLANDMARKSdir,filesep,filenameMAT],...
                %%%                                        'Adata','Aheading');
                %%               %
            end
            %
            colLmkName=0;
            colBodyPart=0;
            colxcoord=0;
            colycoord=0;
            colzcoord=0;
            %
            for j=1:length(Aheading)
                if strcmp(Aheading{j},'name');
                    colLmkName=j;
                elseif strcmp(Aheading{j},'bone');
                    colBodyPart=j;
                elseif strcmp(Aheading{j},'x(mm)');
                    colx=j;
                elseif strcmp(Aheading{j},'y(mm)');
                    coly=j;
                elseif strcmp(Aheading{j},'z(mm)');
                    colz=j;
                end
            end
            %
            if nSubjects==1
                landmarkCodesXRows=Adata(:,colLmkName);
                modelBP=Adata(:,colBodyPart);
            else
                for k=1:length(landmarkCodesXRows)
                    if ~strcmp(landmarkCodesXRows{k},Adata(k,colLmkName))
                        % note that different orders of landmark names is not
                        % supported...
                        error('Inconsistent list of landmarks.')
                    end
                    if ~strcmp(modelBP{k},Adata(k,colBodyPart))
                        % note that different orders of body part names is not
                        % supported...
                        error('Inconsistent list of body parts.')
                    end
                end
            end
            %
            for k=1:size(Adata,1)
                X(k,1,nSubjects)=str2num(Adata{k,colx});
                X(k,2,nSubjects)=str2num(Adata{k,coly});
                X(k,3,nSubjects)=str2num(Adata{k,colz});
            end
            %
        end
        %
    end
    %
end
%
%