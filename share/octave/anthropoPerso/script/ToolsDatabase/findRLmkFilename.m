% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function filename=findRLmkFilename(databaseDir)
%
% filename=findRLmkFilename(databaseDir);
%
% Retrieve the name of the landmark reference file (with relative path).
%
% Input: databaseDir, database diretory (with relative path)
%
% C.Lecomte, Oct.2015
% EU FP7 PIPER project
%
if nargin<1
    databaseDir=paramPerso('databaseDir');
end
%
referenceDir=[databaseDir,filesep,'DataReference'];
%
D=dir(referenceDir);
%
found=0;
for j=1:length(D)
    filenameTest=D(j).name;
    if strfind(filenameTest,'Piper_WP2_Data_Reference_Landmarks')==1
        if strfind(filenameTest,'.csv')
            if found
               warning('There appears to be two landmark reference files...')
            else
                found=1;
                filename=filenameTest;
            end
        end
    end
end
%
%
%
