% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Adata,Aheading]=loadANSURreference(ANSURdir,withBMI)
% loadANSURreference - Load the ANSUR reference file.
%
% [Adata,Aheading]=loadANSURreference(ANSURdir);
% [Adata,Aheading]=loadANSURreference(ANSURdir,loadMat);
%
% Input: - ANSURdir, location of the ANSUR directory.
% Output: - ANSURdata, actual array of data, one row per subject, one column
%           per data type.
%         - ANSURheading, label (data type description) of each column.
%
% The data has been "extracted the data from public files
%    available from the DTIC, which are in a somewhat cumbersome format" 
%    by Matthew Reed from The University of Michigan.
%    (see http://mreed.umtri.umich.edu/mreed/downloads.html#amvo)
% 
% Information about the measurements and the codes used in the dataset 
%    can be found within the ANSUR reports.
% 
% Christophe Lecomte, 2015
% University of Southampton
% Work funded by the FP7 PIPER project
%
% Ref: Gordon, C.C., Bradtmiller, B., Clausen, C.E., Churchill, T., 
%         McConville, J.T., Tebbetts, I., Walker, R.A., 1989. 
%         1987-1988 Anthropometric survey of US Army personnel. 
%         Methods & summary statistics. Natick/TR-89-044. 
%         US Army Natick Research Development and Engineering Center, 
%         Natick, MA. Available from the National Technical Information 
%         Service website, http://www.ntis.gov. ("ANSUR data")
%

if nargin<1
%   Seeks the address of the ANSUR directory among the PIPER parameters
   ANSURdir=paramPerso('ANSURdir');
end
%
strrep(ANSURdir,'/',filesep);
strrep(ANSURdir,'\',filesep);
%
if nargin<2
    withBMI=1;
end
%
filenameCSV='Reference_ansur_withBMI.csv';
%
nHeadRows=1;     % number of heading rows
separator=',';   % separator between items
alphaBnd='"';    % delimitor of string values
isnumData=0;     % data is numeric
[Adata,Aheading]=myCSVread([ANSURdir,filesep,filenameCSV],...
    nHeadRows,separator,alphaBnd,isnumData);
%
