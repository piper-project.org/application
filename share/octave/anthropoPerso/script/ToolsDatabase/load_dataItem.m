% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function dataset=load_dataItem(inventoryDir,dataset,iItem,verbose,savemat,loadmat)
% load only dataset item iItem...
% see documentation of load_dataset:
%     help load_dataset
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
warningLoadmat=0;
%
if strcmp(dataset{iItem}.D6_Format_of_information,'F001_MeshSurfSTL')
    % actual (mesh) data is stored in a file whose name is in column "D9"
    fileName=dataset{iItem}.D9_Location_of_data;
    fileName=strtrim(strrep(fileName,'file:','')); % remove prefix "file:"
    %
    if isOctave
        if ~loadmat
            if ~warningLoadmat
                % Only warn once...
                warning('Forced loading of data from mat files...')
                warningLoadmat=1;
                loadmat=1;
            end
        end
    end
    %
    if loadmat
        load([inventoryDir,filesep,fileName{1},...
            '_nodes.mat'],'-mat');
        dataset{iItem}.nodes=nodes;
        load([inventoryDir,filesep,fileName{1},...
            '_elems.mat'],'-mat');
        dataset{iItem}.elems=elems;
    else
        dataFormat=dataset{iItem}.D7_Format_of_data{1};
        if strfind(dataFormat,'Binary')
            if exist([inventoryDir,filesep,fileName{1}], 'file')
                if verbose
                    disp(strcat('Importing data from file:',fileName{1},'...'));
                end
                [v,f]=stlread([inventoryDir,filesep,fileName{1}]);    % note that fileName is a cell
            else
                warning(strcat('inexisting binary file',fileName{1}))
                fileNameAsc=strrep([inventoryDir,filesep,fileName{1}],'bin','asc');
                if exist(fileNameAsc, 'file')
                    if verbose
                        disp(strcat('Importing data from file:',fileNameAsc,'...'));
                    end
                    [v,f,~]=import_stl_fastCL([inventoryDir,filesep,fileNameAsc],1);
                else
                    disp(strcat('WARNING - INEXISTING file:',fileNameAsc,'...'));
                end
            end
        elseif strfind(dataFormat,'ascii')
            if verbose
                disp(strcat('Importing data from file:',fileName{1},'...'));
            end
            [v,f,~]=import_stl_fastCL([inventoryDir,filesep,fileName{1}],1);
        end
        %
        [dataset{iItem}.nodes,~,IC]=unique(v,'rows','stable');
        %    fv.vertices=dataset{iItem}.nodes(IC),
        %    i.e. IC(i) gives new node number of initial node number, i
        dataset{iItem}.elems=IC(unique(f,'rows'));
        % Note the "renumbering" in the previous line: unique(fv.faces,'rows')
        % contains the initial (non-unique) node numbers...
        %
        if savemat
            nodes=dataset{iItem}.nodes;
            save([inventoryDir,filesep,fileName{1},...
                '_nodes.mat'],'-mat','nodes');
            elems=dataset{iItem}.elems;
            save([inventoryDir,filesep,fileName{1},...
                '_elems.mat'],'-mat','elems');
        end
    end
elseif strcmp(dataset{iItem}.D6_Format_of_information,'F004_NoCart3D, unit=mm')
    % b4.load the description of dataset landmarks
    fieldFileLandmarkInfo='D5_List_of_information';
    fileLandmarkInfo=dataset{iItem}.(fieldFileLandmarkInfo);
    fileLandmarkInfo=strrep(fileLandmarkInfo{1},'file:','');    
    % note that the data is read from the inventory directory
    % ================
    % [~,~,rawListLandmarks]=xlsread([inventoryDir,filesep,fileLandmarkInfo]);
    % SHOULD CHANGE THE RULES SO THAT ALL INFO FILES ARE IN CSV 
    %      FORMAT WITH *.CSV EXTENSION...
    fileLandmarkInfo=strrep(fileLandmarkInfo,'.xlsx','.csv');
    fileLandmarkInfo=strrep(fileLandmarkInfo,'.xls','.csv');
    [~,~,rawListLandmarks]=myCSVread([inventoryDir,filesep,fileLandmarkInfo]);
    % ================
    if ~strcmp(rawListLandmarks{1,1},'bin number in dataset')        
        error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
    end
    if ~strcmp(rawListLandmarks{1,2},'reference code')
        error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
    end
    if ~strcmp(rawListLandmarks{1,3},'version')
        error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
    end
    for j=2:size(rawListLandmarks,1)
        listLMKBinNumber(j-1)=str2num(rawListLandmarks{j,1});
        listLMKRefCode{j-1}=rawListLandmarks{j,2};
        listLMKVersion{j-1}=rawListLandmarks{j,3};
    end
    % b5.load thelandmarks from files
    
    fileName=dataset{iItem}.D9_Location_of_data;
    fileName=strrep(fileName,'file:',''); % remove prefix "file:"
    %
    fieldFormatInfo='D6_Format_of_information';
    fieldFormatData='D7_Format_of_data';
    %
    if ~strcmp(dataset{iItem}.(fieldFormatInfo),'F004_NoCart3D, unit=mm')
        error(['Check format of information for item ',num2str(iItem),' in dataset dataset']);
    end
    if ~strcmp(dataset{iItem}.(fieldFormatData),'ASCII, csv')
        error(['Check format of data for item ',num2str(iItem),' in dataset dataset']);
    end
    %
    if exist([inventoryDir,filesep,fileName{1}], 'file')
        if verbose
            disp(strcat('Importing data from file:',fileName{1},'...'));
        end
        M=csvread([inventoryDir,filesep,fileName{1}]);    % note that fileName is a cell
    else
        disp(strcat('WARNING - INEXISTING file:',fileName{1},'...'));
    end
    %
    dataset{iItem}.landmarks=M(:,2:4);
    dataset{iItem}.unit='mm';
    dataset{iItem}.lmkBinNumber=M(:,1);
    indLmkBinNumber=zeros(size(dataset{iItem}.lmkBinNumber));
    for k=1:length(indLmkBinNumber)
        indLmkBinNumber(k)=find(listLMKBinNumber==dataset{iItem}.lmkBinNumber(k));
    end
    dataset{iItem}.lmkReference=listLMKRefCode(indLmkBinNumber).';
    dataset{iItem}.lmkRefVersion=listLMKVersion(indLmkBinNumber).';
else
    warning(['Format of information (',dataset{iItem}.D6_Format_of_information,...
        ', is not recognised for item:',num2str(iItem)])
end
