% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [lmkBinNumber, lmkRefCode, lmkVersion]   = ...
                     load_listOfLandmarks(fileLandmarkInfo)
% load_listOfLandmarks - Loads a list of landmarks from a dataset
%
% function [lmkBinNumber,lmkRefCode,lmkVersion]=load_listOfLandmarks(fileName);
%
% input: fileName, address of the file containing the information
% ======
% output: info about the j-th landmark is defined by
% =======   . lmkBinNumber(j) -> bin number in dataset
%           . lmkRefCode{j}   -> reference code in database
%           . lmkVersion{j}   -> version of the reference code
%
% C.Lecomte
% University of Southampton
% Aug.2015
% Code written in the context of work funded by the EU FP7 Piper project
%

% =================
% SHOULD CHANGE THE RULES SO THAT ALL INFO FILES ARE IN CSV 
%      FORMAT WITH *.CSV EXTENSION...
% [~,~,rawListLandmarks]=xlsread(fileLandmarkInfo);
fileLandmarkInfo=strrep(fileLandmarkInfo,'.xlsx','.csv');
fileLandmarkInfo=strrep(fileLandmarkInfo,'.xls','.csv');
[~,~,rawListLandmarks]=myCSVread(fileLandmarkInfo);
% =================
if ~strcmp(rawListLandmarks{1,1},'bin number in dataset')
    error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
end
if ~strcmp(rawListLandmarks{1,2},'reference code')
    error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
end
if ~strcmp(rawListLandmarks{1,3},'version')
    error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
end
%
lmkBinNumber=zeros(size(rawListLandmarks,1)-1,1);
lmkRefCode=cell(size(rawListLandmarks,1)-1,1);
lmkVersion=cell(size(rawListLandmarks,1)-1,1);
for j=2:size(rawListLandmarks,1)
    lmkBinNumber(j-1)=str2num(rawListLandmarks{j,1});
    lmkRefCode{j-1}=rawListLandmarks{j,2};
    lmkVersion{j-1}=rawListLandmarks{j,3};
end
%
