% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [REFdata] = load_REFdata(fileName,inputField,inputValue,request)
% load_REFdata - Extract part of the data from a reference file.
%
% function [REFdata]=load_REFdata(fileName,inputField,inputValue,request);
%
% Input: fileName, full address and name of the reference file containing
% ======           the info.
%        inputField, column heading (in string format) that contain pointers
%                    to the desired rows.  For example,
%                    inputField='Refence Code'.
%        inputValue, value of the inputField corresponding to a desired row
%                    Format is that of strings stored in a cell array,
%                    as, for example, inputValue{1}='LMK_LPSIS'.
%        request, column headings corresponding to columns requested from
%                 reference file.  Format is that of strings stored in
%                 a cell array, as, for example, request{1}='Type'.
% Output: lmkInfo, requested information, in the format of a cell array.
% =======
%
% Example:
% ========
%   fileName=['DataReference',filesep,'Piper_WP2_Data_Reference_Landmarks_Rev1.1.xlsx'];
%   inputField='Reference Code';
%   inputValue={'LMK_RASIS','LMK_RPSIS','LMK_RHJCPEL'};
%   request={'Type','Version','Body Part'}
%   [REFdata]=load_REFdata(fileName,inputField,inputValue,request);
%
%
%   >> REFdata
%      REFdata =
%         'palpable'      'v1.0'  'Body_Pelvis'
%         'palpable'      'v1.0'  'Body_Pelvis'
%         'joint center'  'v1.0'  'Body_Pelvis'
%
% C.Lecomte, Uni. of Southampton, 2015, for work funded by the EU FP7 project Piper
%
% version 1.0
%

% Notes:
%    - This script is not optimised
%    - One could use XML format for storing and handling the data.
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
%
% if nargin<5
%     savemat=0;
% end
%
%
% ======================
    [~,~,rawREFdata]=myCSVread(fileName);
% % % % if isOctave
% % % %     warning(strcat('Loading mat file:',strrep(fileName,'csv','mat - ')))
% % % %     load('-mat',strrep(fileName,'csv','mat'))
% % % %     %
% % % % else
% % % %     load('-mat',strrep(fileName,'csv','mat'))
% % % % % %      [~,~,rawREFdata]=xlsread(fileName);
% % % % 
% % % % 
% % % % 
% % % % %     if savemat
% % % % %         save('-mat',strrep(fileName,'csv','mat'),'rawREFdata')
% % % % %     end
% % % % end
% ======================
%
Ncols=size(rawREFdata,2);
Nreqs=length(request);
%
headingsREF=cell(1,size(rawREFdata,2));
colInput=0;
colRequest=zeros(length(request),1);
for k=1:Ncols
    headingsREF{k}=rawREFdata{1,k};
    if strcmp(headingsREF{k},inputField)
        colInput=k;
    end
    for j=1:Nreqs
        if ~colRequest(j)
            if strcmp(headingsREF{k},request{j})
                colRequest(j)=k;
            end
        end
    end
end
%
if colInput==0
    error(['Please check the format of the data reference file.',...
        '  No ''',inputField,''' field was found.'])
end
for j=1:Nreqs
    if ~colRequest(j)
        error(['Please check the format of the data reference file.',...
            '  No requested ''',request{j},''' field was found.'])
    end
end
%
Nrows=size(rawREFdata,1)-1;
rowRequest=zeros(length(inputValue),1);
%
for k=1:Nrows
    for j=1:length(inputValue)
        if ~rowRequest(j)
            if strcmp(inputValue{j},rawREFdata{k+1,colInput})
                % row in the "raw" rawREFdata array
                rowRequest(j)=k+1;
            end
        end
    end
end
for j=1:length(inputValue)
    if ~rowRequest(j)
        error(['Please check the content of the data reference file.',...
            'No requested ''',inputValue{j},''' input value was found.'])
    end
end
%
REFdata=rawREFdata(rowRequest,colRequest);
%
