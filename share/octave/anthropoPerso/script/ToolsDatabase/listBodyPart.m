% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [listBP,dataset]=listBodyPart(inventoryFullName)
%
% Returns the list of body parts found in a PIPER dataset 
%
% Input: inventoryFullName, full name, including directory path, of the 
%                         dataset inventory file.
% Output: listBP, cell array of all body parts reported in the inventory 
%                         file.
%
% C.Lecomte, 2015
% work funded by the EU FP7 Piper project.
%
[dataset]=load_datasetInfo(inventoryFullName);
%
listBP=cell(length(dataset),1);
for j=1:length(dataset)
    listBP(j)=dataset{j}.D4_Body_parts_covered;
end
[listBP,order,~]=unique(listBP);
[~,ind]=sort(order);
listBP=listBP(ind);
%
%
%
% [listBP,dataset]=listBodyPart(inventoryFullName,verbose,savemat,loadmat)
%
% if nargin<2
%     verbose=0;
% end
% if nargin<3
%     savemat=0;
% end
% if nargin<4
%     loadmat=0;
% end
% %
