% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function inventoryName=findInventoryName(databaseDir,datasetName)
%
% inventoryName=findInventoryName(databaseDir,datasetName);
%
% Retrieve the inventory name (with relative path) of a dataset.
%
% C.Lecomte, Oct.2015
% EU FP7 PIPER project
%
fid=fopen([databaseDir,filesep,'listDatasetsFiles.txt']);
endFile=0;
found=0;
while ~endFile
    tline = fgetl(fid);
    if ~ischar(tline), 
        endFile=1;
    else
        if strfind(tline,datasetName)
            if found
                warning('The same datasetName was found twice in the database')
            else
                found=1;
                inventoryName=tline;
            end
        end
    end
end
fclose(fid);
%
%
%
