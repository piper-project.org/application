% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [foundBP,warningText]=includesBP(desiredBP,actualBP)
%
% [foundBP,warningText]=includesBP(desiredBP,actualBP)
%
% Check if actual body parts (actualBP) can be found in a list of
%                desired body parts (desiredBP) or a sub part of it.
%
% Input: desiredBP, the list of desired body parts.
%        actualBP, the list of actual body parts.
%          Both lists can be either a cell array of chains of characters
%              or a single chain of characters.  If a chain of characters
%              contains commas, the commas are considered separators of
%              individual body part names.
%              E.g. the two following assignments define the same
%                   pair of body parts:
%                      desiredBP={'Body_Hip_R','Body_Femur_R'};
%                      desiredBP='Body_Hip_R,Body_Femur_R';
% Output: foundBP, boolean flag that indicates some of the actual body
%              parts are included within the set of all the
%              "sub body parts" of the desired body parts. See allSubBP.m
%              for a description of the sub body parts.
%              (note that if no actual body parts are found in the desired
%               list of body parts BUT *sub* parts of the actual body
%               parts are also sub body parts of the desired body parts, a
%               warning is returned in "warningText".)
%         warningText, chain of characters that contains warning message
%              that (if it is the case) no actual body parts are found in 
%              the desired list of body parts BUT *sub* parts of the actual
%              body parts are also sub body parts of the desired body parts
%
% Christophe Lecomte
% January 2016
% Work funded by the EU FP7 Piper project
%
allSubBPOfDesiredBP=allSubBP(desiredBP);
formattedActualBP=formattedBP(actualBP);
%
if ~isempty(intersect(formattedActualBP,allSubBPOfDesiredBP))
    foundBP=1;
else
    foundBP=0;
end
%
warningText={};
if ~foundBP
   allSubBPOfActualBP=allSubBP(actualBP);
   if ~isempty(intersect(allSubBPOfActualBP,allSubBPOfDesiredBP))
       warningText='Some of the desired and actual body parts overlap without inclusion';
   end
end
%
% ================
