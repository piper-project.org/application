% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Adata,Aheading]=loadSNYDERdata(SNYDERdir,loadMat,withBMI)
% loadSNYDERdata - Load the SNYDER data.
%
% [Adata,Aheading]=loadSNYDERdata(SNYDERdir);
% [Adata,Aheading]=loadSNYDERdata(SNYDERdir,loadMat);
%
% Input: - SNYDERdir, location of the SNYDER directory.
%        - loadMat, optional argument.  If loadMat, the data is loaded from 
%           a binary (Matlab) file that has the same name as the ASCII 
%           CSV file, except for the extension '.mat' instead of '.csv'.
% Output: - SNYDERdata, actual array of data, one row per subject, one column
%           per data type.
%         - SNYDERheading, label (data type description) of each column.
%
% The data has been xxx
%    (see http://xxx)
% 
% Information about the measurements and the codes used in the dataset 
%    can be found within xxx
% 
% Christophe Lecomte, 2016
% University of Southampton
% Work funded by the FP7 PIPER project
%
% Ref: xxx
%

if nargin<1
%   Seeks the address of the SNYDER directory among the PIPER parameters
   SNYDERdir=paramPerso('SNYDERdir');
end
%
strrep(SNYDERdir,'/',filesep);
strrep(SNYDERdir,'\',filesep);
%
if nargin<2
    loadMat=0;
end
%
if nargin<3
    withBMI=1;
end
%
% Note that the name (and location) of the  
if withBMI
    filenameCSV='snyder_withBMI.csv';
else
    filenameCSV='snyder.csv';
end
%
if loadMat
    %
    filenameMAT=regexprep(filenameCSV,'.csv$','.mat');
    load([SNYDERdir,filesep,filenameMAT]);
%
else
    nHeadRows=1;     % number of heading rows
    separator=',';   % separator between items
    alphaBnd='"';    % delimitor of string values
    isnumData=1;     % data is numeric
    [Adata,Aheading]=myCSVread([SNYDERdir,filesep,filenameCSV],...
                    nHeadRows,separator,alphaBnd,isnumData);
end
%
