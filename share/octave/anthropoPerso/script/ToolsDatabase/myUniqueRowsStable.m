function [p,i,j]=myUniqueRowsStable(v)
%
% =========================================================================
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
%
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
%
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
%
% Contributors include Christophe Lecomte (University of Southampton)
%
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% =========================================================================
%
% function [p,i,j]=myUniqueRowsStable(v);
%
% Sorts a two-dimensional array so that all rows are unique and the
%     order of the rows is preserved.
%
% This is script is supported both in Octave and Matlab and is equivalent 
%     to the matlab command [p,i,j]=unique(v,'rows','stable')
%
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
%
if isOctave
    [p,i,j]=unique(v,'rows','first');
else
    % [p,i,j]=unique(v,'rows','stable'); %now v=p(j) p=v(i);
    [p,i,j]=unique(v,'rows');
end
%
[~,ind]=sort(i);
p=p(ind,:);  % "PSTAR"
i=i(ind);    % "ISTAR"
[~,iind]=sort(ind);   % invert the ind table to preserve the relationship
%                       between (the new) p and v...  (i.e. p(j)=v)
j=iind(j);   % jstar
%
%
%