% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [X,modelBP,splitIndex,landmarkCodesXRows,subjectSRNXCols,...
    dataDRNXCols]=loadConfigLMKTypeBP(consideredTypes,desiredBP,...
    LDatasetName,databaseDir,verbose)
%
% [X,modelBP,splitIndex,landmarkCodesXRows,subjectSRNXCols,...
%          dataDRNXCols]=loadConfigLMKTypeBP(consideredTypes,desiredBP,...
%                                       LDatasetName,databaseDir,verbose);
%
% Load (landmark) configurations from the WP2 PIPER database.
%
%      Landmarks are chosen based on their type and the body part they are
%      attached to.  The only subjects that are considered are those for
%      which a full set of landmarks is available.
%
% Notes: restrictions of the currect version:
% =======   - supports a single dataset (as KLL dataset. The code could
%                  could be adapted to extract landmarks from different
%                  datasets, as those in the CCTc series.);
%           - all landmarks for a given subject are stored in a single
%                  data item (DRN).  This is consistent with the
%                  current status of the PIPER database.
%
% Input: - consideredTypes, list of considered landmark types, in the 
%               format of a cell array of strings.  The types must be 
%               consistent with those used in the dataset;
%        - desiredBP, list of desired body parts (as indicated in the 
%               landmark dataset inventory file or in the sets of body
%               parts, as 'Body_LowerLimb', defined in the "allSubBP.m" 
%               script), in the format of a cell array of strings.  
%        - LDatasetName, (cell array) list of names of datasets of interest.
%        - databaseDir, location of the PIPER database (root directory);
%        - verbose: flag that indicates if information is 
%                   displayed during loading. 
% Output: - X: configurations where X(i,j,k) is the j-th coordinate
%                   of the i-th landmark of the k-th configuration;
%         - modelBP, actual list of body parts (as indicated in the 
%               landmark dataset inventory file) for which a landmark is 
%               returned. This is a cell array of strings.  
%               These actual body parts are included in the list of 
%               desiredBP (for example, the actual body part, 
%               'Body_Femur_R', is included in the desired body part 
%               'Body_LowerLimb');
%         - splitIndex: contains indices indicating which body part 
%               (splitIndex(k)=j indicates that k-th conf is part of
%                modelBP{j}) configurations are attached to.
%         - landmarkCodesXRows: contains the landmark Codes (consistent
%               with the PIPER database) associated with the rows of
%               configurations X(i,j,k).
%         - subjectSRNXCols: contains the Subject Reference Codes
%               (SRN) associated with the columns of the configurations,
%               i.e. subjectSRNXCols{k} is the SRN of the subject on
%               which all landmarks X(i,:,k) where measured.
%         - dataDRNXCols: contains the Data Reference Codes
%               (DRN) associated with the columns of the configurations,
%               i.e. dataDRNXCols{k} is the DRN of the data item 
%               corresponding to all landmarks X(i,:,k).
%
% ---------------------
%    % Example (Please check and enter your parameters...):
%    %
%    piperCodesDir='Piper_Codes';    % update this directory location
%    cd piperCodesDir;
%    start
%    %
%    databaseDir=paramPerso('databaseDir'); 
%    %
%    consideredTypes=cell(3,1);
%    consideredTypes{1}='anthropometric';
%    consideredTypes{2}='joint center';
%    consideredTypes{3}='palpable';
%    %
%    desiredBP=cell(1,1);
%    desiredBP{1}='Body_LowerLimb_R';
%    %
%    LDatasetName={'KeppleEtAl_LLimb'};
%    %
%    verbose=1;
%    %
%    [X,modelBP,splitIndex,landmarkCodesXRows,subjectSRNXCols,...
%          dataDRNXCols]=loadConfigLMKTypeBP(consideredTypes,desiredBP,...
%                                       LDatasetName,databaseDir,verbose);
%    %
% ----------------
%
% Christophe Lecomte
% work funded by the EU FP7 PIPER project
% University of Southampton
% 2015
%
% See also: shapeModel, demoSSMLMK, sampleSSM, plotSSM, allSubBP
%
dataType='landmark';
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
% if isOctave
%     if ~existPackage('io')
%         pkg install -forge io     % installation
%     end
%     pkg load io     % loading statistics package
% end
%
% 0. PARAMETERS
% ==============
if nargin<5
    verbose=0;
end
if nargin<4
%   Seeks the address of the PIPER database directory among the PIPER parameters
   databaseDir=paramPerso('databaseDir');
end
%
% LOOP on datasets
% =================
if length(LDatasetName)>1
    error('Multiple datasets option is not yet supported...')
end
%
iDataset=1;
datasetName=LDatasetName{iDataset};
% 1. LOAD THE RAW DATA
% =====================
% % % % a) allocate the database address and tools
% % % addpath([databaseDir,filesep,'Tools']);
% b) load the dataset data (info first):
inventoryName=findInventoryName(databaseDir,datasetName);
fullInventoryName=[databaseDir,filesep,inventoryName];
%
if isOctave
    lengthName=length(fullInventoryName);
    if strfind(fullInventoryName,'.xls')==lengthName-3
        ftest=strrep(fullInventoryName,'.xls','.xlsx');
        if exist(ftest,'file')
            fullInventoryName=ftest;
            warning('Read data from ''xlsx'' instead of ''xls'' file:')
            warning([' ',fullInventoryName])
        else
            warning('Please check that octave reads the ''xls'' file:')
            warning([' ',fullInventoryName])
        end
%         % reading of CSV in octave is now supported (using myCSVread.m)
% %     elseif strfind(fullInventoryName,'.csv')==lengthName-3
% %         ftest=strrep(fullInventoryName,'.csv','.xlsx');
% %         if exist(ftest,'file')
% %             fullInventoryName=ftest;
% %             warning('Read data from ''xlsx'' instead of ''csv'' file:')
% %             warning([' ',fullInventoryName])
% %         else
% %             warning('Please check that octave reads the ''csv'' file:')
% %             warning([' ',fullInventoryName])
% %         end
    end
end
%
takeAllBP=0;
[datasetBP,dataset]=listBodyPart(fullInventoryName);
if ischar(desiredBP)
    if strcmp(desiredBP,'All') || strcmp(desiredBP,'all')
        takeAllBP=1;
    else
        desiredBP={desiredBP};
    end
end
%
if takeAllBP
    desiredBP=datasetBP;
end
%
%
% 2. TREAT DATA DEPENDING ON ITS TYPE (only landmark allowed here...)
% ====================================
if strcmp(dataType,'landmark')
    % c) select landmarks of interest
    % first check if all landmark items of the current dataset
    % share the same file describing the landmark information.
    LFileLandmarkInfo=cell(length(dataset),1);
    %
    for j=1:length(dataset)
        % only look at landmark info...
        if strcmp(dataset{j}.D3_Type_of_information,'T002_Landmark')
            LFileLandmarkInfo{j}=dataset{j}.D5_List_of_information{1};
            LFileLandmarkInfo{j}=strrep(LFileLandmarkInfo{j},'file:','');
        end
    end
    %
    [LFileLandmarkInfo,order,~]=unique(LFileLandmarkInfo);
    [~,ind]=sort(order);
    LFileLandmarkInfo=LFileLandmarkInfo(ind);
    %
    if length(LFileLandmarkInfo)>1
        error('Multiple ''fileLandmarkInfo'' files option is not yet supported.')
    end
    %
    % d) extract reference codes for landmarks (in this dataset)
    fileLandmarkInfo=LFileLandmarkInfo{1};
    %
    inventoryDir=[databaseDir,filesep,'Dataset_',datasetName];
    fullfileLandmarkInfo=[inventoryDir,filesep,fileLandmarkInfo];
    %
    [lmkBinNumber, lmkRefCode, lmkVersion]= ...
        load_listOfLandmarks(fullfileLandmarkInfo);
    %
% %         [~,~,rawListLandmarks]=xlsread(filename);
% %         iBin=1;
% %         iRef=2;
% %         iVer=3;
% %         if ~strcmp(rawListLandmarks{1,iBin},'bin number in dataset')
% %             error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% %         end
% %         if ~strcmp(rawListLandmarks{1,iRef},'reference code')
% %             error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% %         end
% %         if ~strcmp(rawListLandmarks{1,iVer},'version')
% %             error(['check first column of landmark descriptions - in ',fileLandmarkInfo])
% %         end
% %         lmkBinNumber=zeros(size(rawListLandmarks,1)-1,1);
% %         lmkRefCode=cell(size(rawListLandmarks,1)-1,1);
% %         lmkVersion=cell(size(rawListLandmarks,1)-1,1);
% %         for j=2:size(rawListLandmarks,1)
% %             lmkBinNumber(j-1)=rawListLandmarks{j,iBin};
% %             lmkRefCode{j-1}=rawListLandmarks{j,iRef};
% %             lmkVersion{j-1}=rawListLandmarks{j,iVer};
% %         end
    %
    % d) obtain further information from the database level
    %    (For each reference code: one extracts the type and
    %     body part associated with the k landmarks of the dataset.
    %     Data is stored in "REFdata", one row per landmark in the
    %     dataset - in the same order.)
    filename=findRLmkFilename(databaseDir);
    fullFilename=[databaseDir,filesep,'DataReference',filesep,filename];
    inputField='Reference Code';
    inputValue=lmkRefCode;
    request={'Type','Version','Body Part'};
    [REFdata]=load_REFdata(fullFilename,inputField,inputValue,request);
    % ----
    % Note that four arrays give information about the landmarks
    %   contained in the dataset:
    %  lmkBinNumber,lmkRefCode,lmkVersion,REFdata
    % ----
    % check versions of landmark reference codes
    versionOK=zeros(length(lmkRefCode),1);
    for j=1:length(lmkRefCode)
        if strcmp(lmkVersion{j},REFdata{j,2})
            versionOK(j)=1;
        end
    end
    if sum(~versionOK)
        error('Please check versions of the landmark reference codes.')
    end
    %
    % e) filter landmarks data of interest based on landmark type and
    %                    body part of interest.
    if verbose
        disp('The dataset contains the following types of landmarks:')
        disp(unique(REFdata(:,1)))
        %
        disp('In this analysis, only the following are considered:')
        disp(consideredTypes)
        %
        % considered body parts are limited to
        disp('The dataset relates to the following body parts:')
        disp(unique(REFdata(:,3)))
        %
    end
    %
    % f) Only work with subjects for whom a full set of considered landmarks
    %          is provided.
    %
    % ==> OPERATIONS (Flags landmarks of interest based on selection - body parts/landmark types, here)
    %
    % go through all landmarks and flag if they are considered or not
    %     (Note that the list of landmarks "consideredLMK_flag" has the
    %       same argument as the first argument of the four arrays
    %       lmkBinNumber,lmkRefCode,lmkVersion,REFdata.)
    consideredLMK_flag=zeros(size(REFdata,1),1);
    modelBP={};
    for j=1:size(REFdata,1)
        foundType=strfind(consideredTypes,REFdata{j,1});
        if sum(cell2mat(foundType))
            % Type is fine...
            % see if the body part is of interest
            for ind=1:length(desiredBP)
                % REFdata{j,3} is the body part extracted from the
                %                          reference landmark file
                % desiredBP{ind} is the ind-th desired body part.
                %
                if includesBP(desiredBP{ind},REFdata{j,3})
                    consideredLMK_flag(j)=1;
                    modelBP{length(modelBP)+1}=REFdata{j,3};
                end                
            end
        end
    end
    modelBP=unique(modelBP(:));
    %
    % consideredLMK therefore contains pointers to the "rows" of the 4
    %     arrays lmkBinNumber,lmkRefCode,lmkVersion,REFdata
    %     that correspond to landmarks of interest...
    consideredLMK=find(consideredLMK_flag==1);
    if verbose
        disp('In this analysis, only the following body parts are considered:')
        disp(modelBP)
        disp(['This leaves ',num2str(length(consideredLMK)),' landmarks of interest'])
        disp(['    (out of a potential of ',num2str(length(lmkBinNumber)),...
            ' landmarks in the whole dataset).'])
    end
    %
    %
    % ==> OPERATIONS (Deal with duplicated landmarks)
    %
    % The consideredLMK list is sorted and reduced in such a way that duplicate
    %     landmarks appear only once (as their mean of duplicated pairs).
    %     new order is
    %          - unique landmarks
    %          - original landmarks (for duplicated pairs)
    %          - duplicate landmarks (of the original ones)
    % (Note that "duplicates" are expected to be removed from the
    %     landmark datasets and taken care of in an alternative
    %     manner - allowing multiple landmark types.  Need check
    %     first that this is OK, so this test in the mean time.)
    %
    duplLoc=strfind(lmkRefCode(consideredLMK(:)),'duplicate');
    emptyInd=cellfun('isempty',duplLoc);
    duplLoc(emptyInd)={0};
    duplLoc=cell2mat(duplLoc);
    duplLoc=duplLoc(:).';
    %
    % ==> OPERATIONS (Sort landmarks into unique, duplicate, pairs)
    duplInd=find(duplLoc);
    origInd=zeros(size(duplInd));
    for j=1:length(duplInd)
        % goes through the duplicated landmarks and merge them into one.
        %    (the "duplicate" landmarks are neglected and the corresponding
        %      "original" landmarks are substituted by the pairs' average.)
        duplicatedLMK=lmkRefCode(consideredLMK(duplInd(j)));
        duplicatedLMK=strrep(duplicatedLMK{1},'duplicate','');
        duplicatedLMK=strtrim(duplicatedLMK);
        %
        indPair=find(cellfun(@(x) ~isempty(x),strfind(lmkRefCode(consideredLMK).',duplicatedLMK)));
        if length(indPair)~=2
            error('Check the list of duplicates. Some do not come in pairs.')
        end
        origInd(j)=setdiff(indPair,duplInd(j));
        %
    end
    uniqInd=setdiff(1:length(consideredLMK),origInd);
    uniqInd=setdiff(uniqInd,duplInd);
    %
    if verbose
        disp([num2str(length(duplInd)),' pairs of landmarks are ',...
            'duplicate of each other.  Their mean (only) is considered,'])
        disp(['so that statistics are established from ',...
            num2str(length(uniqInd)+length(origInd)),' landmarks.'])
    end
    %
    consideredLMK=consideredLMK([uniqInd,origInd,duplInd]);
    % (Note: the operation above has no effect on the arrays
    %       lmkBinNumber,lmkRefCode,lmkVersion,REFdata)
    %
    % Only consider the subjects for which a full set of desired landmark
    %    measurements are available.
    nLMK=length(consideredLMK);
    %
    % ==> OPERATIONS (Flags items of interest based on a selection of desired==required landmarks)
    %
    consideredITEM=zeros(length(dataset),1);
    consideredLMKind=zeros(length(dataset),length(consideredLMK));
    nITEMS=0;
    for iItem=1:length(dataset)
        % loading of actual landmark data (landmark coordinates)
        dataset=load_dataItem(inventoryDir,dataset,iItem,verbose);
        %
        % check the landmarks availabe for the current item with the
        %     desired (considered) landmarks
        [Lia,Locb] = ismember(lmkBinNumber(consideredLMK),dataset{iItem}.lmkBinNumber);
        %
        if (length(find(Lia==1))==nLMK) && (nLMK>0)
            % full set of desired landmark measurements for this item
            nITEMS=nITEMS+1;
            consideredITEM(nITEMS)=iItem;
            consideredLMKind(nITEMS,:)=Locb;
        end
    end
    consideredITEM=consideredITEM(1:nITEMS);
    consideredLMKind=consideredLMKind(1:nITEMS,:);
    %
    % e) load the actual landmark data (updating the duplicated pairs)
    %
    % ==> OPERATIONS (Load/sort landmarks of interest, for selected landmarks/subjects)
    % ==> OPERATIONS (Merge duplicated landmarks of interest into a single mean per pair)
    nLMK_uniq=length(uniqInd);
    nLMK_orig=length(origInd);
    nLMK_dupl=length(duplInd);
    %
    LMKmeas=zeros(nLMK_uniq+nLMK_orig,3,nITEMS);
    % % % LMKdata_withDupl=zeros(nITEMS,nLMK,3);
    for j=1:nITEMS
        iItem=consideredITEM(j);
        LMKmeas(1:nLMK_uniq+nLMK_orig,1:3,j)=...
            [dataset{iItem}.landmarks(consideredLMKind(j,1:nLMK_uniq),1:3);...
            (dataset{iItem}.landmarks(consideredLMKind(j,...
            (nLMK_uniq+1):(nLMK_uniq+nLMK_orig)),1:3)+...
            dataset{iItem}.landmarks(consideredLMKind(j,...
            (nLMK_uniq+nLMK_orig+1):(nLMK_uniq+nLMK_orig+nLMK_dupl)),1:3))/2];
        %
        % % % LMKdata_withDupl(iItem,1:nLMK,1:3)=...
        % % %     KLL{j}.landmarks(consideredLMKind(iItem,:),1:3);...
        % % % %
    end
    %
    % truncate the list of considered landmarks to only point to
    %     unique landmarks and one mean per pair of duplicates.
    consideredLMK=consideredLMK(1:(nLMK_uniq+nLMK_orig));
    %
    % f) sort landmark data body part by body part for alignment
    %
    splitIndex=zeros(length(consideredLMK),1);
    indBP=cell(size(modelBP));
    for j=1:length(modelBP)
        indBP{j}=find(strcmp(REFdata(consideredLMK,strcmp(request,'Body Part')),modelBP{j}));
        splitIndex(indBP{j})=j;
    end
    %
    % INPUT FOR STATISTICAL SHAPE MODEL ANALYSIS
    %
    X=LMKmeas;  clear LMKmeas;
    landmarkCodesXRows=lmkRefCode(consideredLMK);
    subjectSRNXCols=cell(length(consideredITEM),1);
    dataDRNXCols=cell(length(consideredITEM),1);
    for j=1:length(consideredITEM)
        subjectSRNXCols{j}=dataset{consideredITEM(j)}.B1_SRN{1};
        dataDRNXCols{j}=dataset{consideredITEM(j)}.D1_DRN{1};
    end
    %
end
%
%
%
