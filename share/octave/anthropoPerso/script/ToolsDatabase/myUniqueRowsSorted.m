function [C,IA,IC] = myUniqueRowsSorted(A)
%
% =========================================================================
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
%
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
%
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
%
% Contributors include Christophe Lecomte (University of Southampton)
%
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% =========================================================================
%
% function [C,I,IC]=myUniqueRowsSorted(A);
%
% Sorts a two-dimensional array so that all rows are unique and the
%     rows are sorted on increasing values of the first column, then
%     second, third,... columns.
%
% This is script is supported both in Octave and Matlab and is equivalent 
%     to the matlab command [C,IA,IC]=unique(v,'rows'), so that A=C(IC,:)
%     and C=A(IA,:).
%
% See also: unique
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
%
if isOctave
    [C,IA,IC]=unique(A,'rows','first');
else
    [C,IA,IC]=unique(A,'rows');
end
%
%