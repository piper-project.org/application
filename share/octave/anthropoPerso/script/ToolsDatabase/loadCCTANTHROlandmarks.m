function [X,dataBP,landmarkCodesXRows,subjectSRNXCols,...
    A_circ,dataBP_circ]=loadCCTANTHROlandmarks(CCTLANDMARKSdir,...
                                             CCTCIRCUMFdir,loadMat)
% loadCCTANTHROlandmarks - Load the landmark data (that is additional to 
%                                                         CCTANTHRO data.)
%
% =========================================================================
% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
% =========================================================================
%
% [X,dataBP,landmarkCodesXRows,subjectSRNXCols,A_circ,dataBP_circ]=...
%                  loadCCTANTHROlandmarks(CCTLANDMARKSdir,CCTCIRCUMFdir);
% [X,dataBP,landmarkCodesXRows,subjectSRNXCols,A_circ,dataBP_circ]=
%          loadCCTANTHROlandmarks(CCTLANDMARKSdir,CCTCIRCUMFdir,loadMat);
%
% Load the CCTANTHRO landmark data.
%
% Input: - CCTLANDMARKdir, location of the CCTLANDMARK directory.
%        - CCTCIRCUMFdir, location of the CCTCIRCUMF directory.
%        - loadMat, optional argument.  If loadMat, the data is loaded from 
%           a binary (Matlab) file that has the same name as the ASCII 
%           CSV file, except for the extension '.mat' instead of '.csv'.
% Output: - X, configurations, i.e. sets of landmark coordinates. This is
%           a three dimensional array.  X(i,j,k) corresponds to the j-th
%           coordinate of the i-th landmark of the k-th subject.
%         - dataBP, name of body part associated to the landmarks.  It is 
%           a cell array, so that dataBP{j} contains the name of the body
%           part associated with the j-th landmark.
%         - landmarkCodesXRows, name of the landmarks.  It is a cell array
%           so that landmarkCodesXRows{j} contains the name of the
%           j-th landmark.
%         - subjectSRNXCols, idetifier of the subjects.  It is a cell array
%           so that subjectSRNXCols{j} contains the ID of the
%           j-th subject.
%         - A_circ, Anthropometric circumference. A_circ(i,j) is the 
%           circumference corresponding to modelBP{j} of the i-th 
%           configuration (i.e. i-th subject, subjectSRNXCols{i});
%         - dataBP, actual list of body parts (as provided in the
%               "bone" column of the data files) for which a circumference is
%               returned. This is a cell array of strings.

%
% The data has been furnished by CEESAR in the context of the PIPER project
% 
% Information about the measurements and the codes used in the dataset 
%    can be found within the CCTANTHRO reports.
% 
% Christophe Lecomte, 2017
% University of Southampton
% Work funded by the FP7 PIPER project
%
%

if nargin<1
%   Seeks the address of the CCTLANDMARK directory among the PIPER parameters
   CCTLANDMARKSdir=paramPerso('CCTLANDMARKSdir');
end
%
strrep(CCTLANDMARKSdir,'/',filesep);
strrep(CCTLANDMARKSdir,'\',filesep);
%
if nargin<2
%   Seeks the address of the CCTLANDMARK directory among the PIPER parameters
   CCTCIRCUMFdir=paramPerso('CCTCIRCUMFdir');
end
%
strrep(CCTCIRCUMFdir,'/',filesep);
strrep(CCTCIRCUMFdir,'\',filesep);
%
if nargin<3
    loadMat=0;
end
% forces loadMat to zero (this could be updated if needed and
%                         if the *.mat is made available)
% loadMat = 0
%
[X,dataBP,landmarkCodesXRows,subjectSRNXCols]=...
                                loadCCTLANDMARKSdata(CCTLANDMARKSdir,loadMat);
%
[A_circ,dataBP_circ,subjectSRNXCols_circ]=loadCCTCIRCUMFdata(CCTCIRCUMFdir,loadMat);
% check consistency of subjectSRNXCols...
for j=1:length(subjectSRNXCols)
    if ~strcmp(subjectSRNXCols_circ{j},subjectSRNXCols{j})
        error('inconsistent subjects for landmarks and circumference data')
    end
end
%
