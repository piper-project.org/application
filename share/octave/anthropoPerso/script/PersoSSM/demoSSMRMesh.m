% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Xmu,P,Sigma_hat,...
    modelBP,splitIndex,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C,...
    RMeshCoordXRows,RMeshElem,subjectSRNXCols,dataDRNXCols,X]=...
    demoSSMRMesh(LDatasetName,consideredRMesh,consideredBP,...
    linkedBP,databaseDir,...
    maxIter,toler,tolerVar,centered,verbose,partial,loadmat,savemat)
%
% [Xmu,P,Sigma_hat,modelBP,splitIndex,...
%     XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C,...
%     RMeshCoordXRows,RMeshElem,subjectSRNXCols,dataDRNXCols]=...
%     demoSSMRMesh(datasetName,consideredRMesh,consideredBP,...
%     linkedBP,databaseDir,...
%     maxIter,toler,tolerVar,centered,verbose,partial,loadmat,savemat)
%
% Demonstration of the generation of a statistical shape model on 
%      T001_SurfaceMesh information already put in correspondence.
%
%      Meshes are chosen based on the reference mesh they have been put in
%      correspondence with and the body parts they are attached to.
%      The only subjects that are considered are those for
%      which a full set of surface meshes is available.
%
% Some body parts can be grouped together in alignment, in the sense that
%      posture is included in the aligned groups of shapes.  The linked
%      body parts (i.e. those that should be aligned as a single entity)
%      are described in the lists contained in the variable linkedBP.
%
% Input: - datasetName, name of the datasets of interest.
%               For now, in the current version of the code, 
%                  datasetName=='CCTc' indicates the whole series of 
%                  "CCTc" datasets;
%        - consideredRMesh, reference code of the mesh to wich the 
%               configuration meshes are desired to have been put in 
%               correspondence with.  
%               For now, in the current version of the code, 
%                  consideredRMesh=='CCTc' indicates the VOLMO segmented
%                  model that served as implicit correspondence for 
%                  the segnentation of the "CCTc" datasets;
%        - consideredBP, of considered body parts, in the format of a cell
%               array of strings.  The body part must be described by 
%               reference codes consistent with the PIPER database 
%               conventions.  
%               General body regions or body series are also supported.
%               These are then stored as strings:
%               consideredBP='All' returns all body parts in the dataset;
%        - linkedBP, list of groups of body parts that are treated as a 
%               single entity during alignment.  If the list is empty,
%               all body parts are aligned individually.  
%               Each linkedBP{j}, for any j
%                     contains a list of body parts that are treated
%                     as a single entity.
%               As for consideredBP, the body parts are described by 
%               reference codes consistent with the PIPER database 
%               conventions.
%        - databaseDir, location of the PIPER database (root directory);
%        - maxIter: maximum number of iterations; [default=20]
%        - toler: relative difference between the Procrustes sums of
%               squares during iterations, used as tolerance criteria
%               to stop iterations, both during the rotations -step 2-
%               and scaling -step3-; [default=1e-6]
%        - tolerVar: relative tolerance in the extraction of the variance
%               principal components or modes. Only the terms with 
%               Sigma_hat larger than Sigma_hat(1) * tolerVar are 
%               preserved; [default=1e-13]
%        - centered: flag that indicates if the configurations have already
%               been centered (average of coordinates at the origin);
%                                    [default=0]
%        - verbose: flag that indicates if information such as values of 
%               the relative change in the residual sum of squares is 
%               displayed.         [default=0]
%        - partial: flag to choose if a *partial* Procrustes analysis is
%               desired as opposed to a *full* Procrustes analysis,
%               i.e. an alignment that operates without or with
%               scaling (partial=1 corresponds to *partial* Prorustes
%               and no scaling).   [default=1]
%        - loadmat: flag that indicates if data is loaded directly from
%               a "*.mat" file (as opposed to being read from a *.CSV,
%               *.STL, or *.XLS file).  [default=0]
%        - savemat: flag that indicates if data is saved in
%               a "*.mat" file after having been read from inventory, 
%               STL or data reference files.  [default=0]
%               For example, the arrays of nodes and elements read from
%               a particulat *.STL file are saved in two files with 
%               names that have "_nodes.mat" and "_elems.mat" 
%               extensions.
% Output: - Xmu: arithmetic mean of the Procrustes fits (it is centered
%               and has the same shape as the Procrustes mean. [1,pg89])
%               Xmu(i,j) is the j-th coordinate of the i-th node of the 
%               T001_SurfaceMesh mesh of interest.
%               (alignment may be per sub-configuration, i.e. body part per
%               body part, if requested through the values of splitIndex);
%         - P: principal components or modes of the covariance matrix of 
%               aligned configurations compared to Xmu.  Note that P is
%               an orthogonal matrix, so that P.'*P=I, the identity matrix.
%               P(i,j,k) is the j-th coordinate of the i-th node of the 
%               T001_SurfaceMesh mesh of interest and for the k-th mode;
%         - Sigma_hat: vector containing estimates of the standard deviation 
%               magnitude of these components. (If the eigenvalues of the 
%               covariance matrix of the aligned configurations compared to 
%               the mean configuration, Xmu, are stored in a vector Lambda,
%               then, Sigma_hat(j)=sqrt(Lambda(j)/(n-1)), where n is the 
%               number of configurations.)
%        - modelBP, list of body parts considered in the model, in the 
%               format of a cell array of strings.  The body part is 
%               described by reference codes consistent with the PIPER 
%               database conventions.  This is equal to consideredBp if
%               this input list is explicit.
%         - splitIndex, index indicating which body part a node belongs to.
%               splitIndex(k)=j indicates that the k-th node belongs to the
%               j-th considered body part (output modelBP{j}).
%         - XP: procrustes fits (centred at zero), so that 
%               XP(splitIndex==s,:,k) = ...
%                    beta(k ,s)* X(splitIndex==s,:,k)* Gamma(:,:,k ,s) + 
%                              repmat(gammaShift(:,k ,s),size(X,1),1)
%              where s refers to the sub-configuration index
%              (in "short":  XP_k^(s) = beta_k^(s)  X_k^(s)  Gamma_k^(s) + 
%                      1_n gammaShift_k^(s)  where 1_n is a vector of ones)
%               Reminder that if (partial), there is no scaling, so that
%               "XP_k^(s) = X_k^(s)  Gamma_k^(s) + 1_n gammaShift_k^(s)"...
%         - gammaShift: set of translation vectors such that all 
%                 shifted configurations are centred at the origin.  
%                 gammaShift(:,k ,s) corresponds to the k-th configuration
%                 for the s-th sub-configuration component.
%         - Gamma: set of rotation matrices. Gamma(:,:,k ,s) corresponds to
%                 the rotation matrix for the k-th configuration
%                 for the s-th sub-configuration component. 
%         - beta: vector of scaling coefficients beta(k ,s) of the k-th
%                 configuration for the s-th sub-configuration component. 
%                 Note that if (partial), then all of these coefficients 
%                 are equal to 1.
%         - converged: converged(s) is a flag of the GPA iterative 
%                 algorithm that indicates if it converged up to tolerance 
%                 toler within the maximum, maxIter, number of steps, when
%                 it was applied to the s-th sub-configuration component.
%         - iter: iter(s) is an output of the GPA iterative algorithm that
%                 indicates the total number of rotation steps used, when
%                 it was applied to the s-th sub-configuration component.
%         - G: iter(s) is an output of the GPA iterative algorithm that
%                 indicates the final sum of squares between the rotated 
%                 (possibly scaled) Procrustes fits and their mean, when
%                 it was applied to the s-th sub-configuration component.
%         - Gprec: Gprec(s) is an output of the GPA iterative algorithm
%                 that indicates the value of the similar sum of squares  
%                 at the penultimate step, when it was applied to the 
%                 s-th sub-configuration component.
%         - C: transformation matrix that goes from the Procrustes fits
%                 (variance from the mean Xmu) to the variance principal 
%                 components or modes.  If XP is the array of fits, one has
%                   1)  for i=1:n, DX(:,:,i)=XP(:,:,i)-Xmu; end
%                   2)  dX=reshape(DX,k*m,n);
%                   3)  p=dX * C
%                   4)  P=reshape(p,k,m,size(C,2));
%                  where k,m,n are respectively the numbers of T001_SurfaceMeshs,
%                  dimensions, and configurations.
%         - RMeshCoordXRows, coordinates of the nodes (of the reference
%                  correspondence mesh) corresponding to the rows of the 
%                  X, P, etc. arrays.
%                  (These are stored such that RMeshCoordXRows(j,i)  
%                  contains the i-th coordinate of the reference mesh node 
%                  corresponding to the j-th row.)
%         - RMeshElem, vertices of the elements of the reference
%                  correspondence mesh.  These are stored body part by 
%                  body part, with RMeshElem{j} corresponding to the 
%                  vertices of the j-th body part as indicated by  
%                  splitIndex(k)=j for the node k (the nodes are sorted
%                  in the same order).
%         - subjectSRNXCols, subjects SRN corresponding to the columns of
%                  the X, P, etc. arrays. These are stored in the format 
%                  of a cell of strings (subjectSRNXCols{j} contains the  
%                  SRN corresponding to the j-th column).
%         - dataDRNXCols, data item DRN  corresponding to the columns of
%                  the X, P, etc. arrays. These are stored in the format 
%                  of a cell of cells of strings (dataDRNXCols{i}{j} 
%                  contains the DRN corresponding to the j-th splitIndex 
%                  value of the i-th column, i.e. all the coordinates
%                  X(k,:,i) come from the data item with DRN 
%                  dataDRNXCols{i}{j} if splitIndex(k)=j).
%
%  % --------------
%  % Example (Please check and enter your own parameters, notably the 
%  %          location of the PIPER database root directory...):
%
%    piperCodesDir='Piper_Codes';    % update this directory location
%    cd piperCodesDir;
%    start
%    %
%    databaseDir=paramPerso('databaseDir'); 
%    %
%    datasetSeries='CCTc';
%    consideredRMesh='CCTc';
%    %
%    consideredBP=cell(3,1);
%    consideredBP{1}='Body_Pelvis';
%    consideredBP{2}='Body_Femur_L';
%    consideredBP{3}='Body_Femur_R';
%    %
% %    consideredBP='All';
%    %
%    % linkedBP={{'Body_Femur_L','Body_Femur_R'}};
%    linkedBP={{'Body_Femur_L'}};
%    %
%    maxIter=20;
%    toler=1e-6;
%    tolerVar=1e-10;
%    centered=0;
%    verbose=0;
%    partial=1;
%    %
%    [Xmu,P,Sigma_hat,modelBP,splitIndex,XP,gammaShift,Gamma,beta,...
%    converged,iter,G,Gprec,C,RMeshCoordXRows,RMeshElem,subjectSRNXCols,...
%    dataDRNXCols]=...
%          demoSSMRMesh(datasetSeries,consideredRMesh,consideredBP,...
%             linkedBP,databaseDir,maxIter,toler,tolerVar,centered,verbose,partial);
%
%    % Sample from the statistical model:
%    Xs=sampleSSM(Xmu,P,Sigma_hat,50);
%
%    for k=1:length(consideredBP)
%      % (limit oneself to the nodes associated to the k-th consideredBP
%      %     mesh => only the nodes associated to this element are 
%      %     considered.  Therefore, the node numbering changes in the
%      %     elements' descriptions.
%        RMeshElemk=RMeshElem{k};
%        L=1:length(splitIndex);
%        L=L(find(splitIndex==k));
%        for j=1:length(L)
%            LL(L(j))=j;
%        end
%        for j=1:size(RMeshElemk,2)
%            for i=1:size(RMeshElemk,1)
%                RMeshElemk(i,j)=LL(RMeshElemk(i,j));
%            end
%        end
%        %
%        bone=struct('vertices',Xmu(splitIndex==k,:),'faces',RMeshElemk);
%        figure_patch(bone,[0.7 0.7 0.6],0.8)
%    end
% % --------------
%
% C.Lecomte
% University of Southampton, Nov.2015
% Work funded by the EU FP7 PIPER project
%
% See also: shapeModel, demoSSMLMK, sampleSSM, plotSSM
%
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;
%
% 0. PARAMETERS (load/save the dataset in binary format):
% ==============
% Name of dataset of interest
if nargin<1
    LDatasetName={'CCTc'};
end
if ~iscell(LDatasetName)
    LDatasetName={LDatasetName};
end
% e) type of data
dataType='T001_SurfaceMesh';
% f) types of T001_SurfaceMeshs considered
if nargin<2
    consideredRMesh='CCTc';
end
% g) types of body parts considered
if nargin<3
    consideredBP=cell(3,1);
    consideredBP{1}='Body_Pelvis';
    consideredBP{2}='Body_Femur_L';
    consideredBP{3}='Body_Femur_R';
end
if nargin<4
    linkedBP=cell(0,1);
end
% Database location (root of the database)
if nargin<5
%   Seeks the address of the PIPER database directory among the PIPER parameters
   databaseDir=paramPerso('databaseDir');
end
%
% Statistical shape analysis/GPA parameters
if nargin<6
    maxIter=20;
end
if nargin<7
    toler=1e-6;
end
if nargin<8
    tolerVar=1e-13;
end
if nargin<9
    centered=0;
end
if nargin<10
    verbose=0;
end
if nargin<11
    partial=1;
end
%
% Indicates if the T001_SurfaceMesh info is loaded from a binary mat file
if nargin<12
    loadmat=0;
end
% Indicates if the T001_SurfaceMesh info is saved in a binary mat file
if nargin<13
    savemat=1;
end
%
% 1. LOAD THE (MESHES) CONFIGURATIONS
% ======================================
% % addpath([databaseDir,filesep,'Tools']);
%
if isOctave
    if ~existPackage('io')
       pkg install -forge io     % installation
    end
    pkg load io     % loading io package
end
%
if strcmp(consideredRMesh,'CCTc')
    warning('Note that the reference model of the CCTc dataset series is only implicitly known so far...')
else
    error('Only the (implicit) ''CCTc'' reference model is currently supported...')
end
%
if strcmp(LDatasetName,'CCTc') || strcmp(LDatasetName,'CCTc_male') || ...
        strcmp(LDatasetName,'CCTc_female') 
    % data series...    
    [LDatasetName]=listDatasetName(LDatasetName);
% INFO ON THIS DATASET IS NOT CORRECT (Discussed this with Baptiste):
%                    'Dataset_CEESAR_CTscan_CCTc662_ceesar',...
end
%
[X,modelBP,splitIndex,RMeshCoordXRows,RMeshElem,...
    subjectSRNXCols,dataDRNXCols]=...
    loadConfigRMeshBP(consideredBP,...
    LDatasetName,databaseDir,loadmat,savemat,verbose);
%
% 2. GENERATE THE STATISTICAL MODEL
% ==================================
if verbose
    disp('Generating the statistical model...')
end
%
if isOctave
    if ~existPackage('statistics')
       pkg install -forge statistics     % installation
    end
    pkg load statistics     % loading statistics package
end
%
linkedGroupBP=zeros(length(modelBP),1);
for j=1:length(linkedBP)
    % current group of linked BP's
    linkedBP_current=linkedBP{j};
    if ischar(linkedBP_current)
        warning('String LinkedBP component has been changed into a cell')
        linkedBP_current={linkedBP_current};
    end
    %   
    for k=1:length(linkedBP_current)
        % take each element BP of the group and extract all BP's that are
        %       part of it...
        listBP=allSubBP(linkedBP_current{k});  % 
        % then check if these correspond to an actual desired BP (modelBP)
        for i=1:length(modelBP)
            if sum(strcmp(modelBP{i},listBP))
                if linkedGroupBP(i)~=0 && linkedGroupBP(i)~=j 
                    error('A body part is in several linked BPs')
                else
                    linkedGroupBP(i)=j;
                end
            end
        end
        %        
    end
end
%
indexBPGroup=1:length(modelBP);
% Split indices are regrouped if the BP's are parts of subconfigurations,
%        i.e. sets of body parts that are treated as a single entity 
%        during alignment.
for i=1:length(modelBP)
    if linkedGroupBP(i)
        indexBPGroup(i)=length(modelBP)+linkedGroupBP(i);
    end
end
[~,~,I]=unique(indexBPGroup);
%
splitIndexGroup=I(splitIndex);
%
[Xmu,P,Sigma_hat,...
    XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C]=...
    shapeModel(X,splitIndexGroup,maxIter,toler,tolerVar,...
    centered,verbose,partial);
% ------
% output gammaShift, Gamma, and beta for the modelBP distribution (rather
%       than for the linked groups of BP)
gammaShiftModel=zeros(size(gammaShift,1),size(gammaShift,2),length(modelBP));
GammaModel=zeros(size(Gamma,1),size(Gamma,2),size(Gamma,3),length(modelBP));
betaModel=zeros(size(beta,1),length(modelBP));
for j=1:length(modelBP)
    gammaShiftModel(:,:,j)=gammaShift(:,:,I(j));
    GammaModel(:,:,:,j)=Gamma(:,:,:,I(j));
    betaModel(:,j)=beta(:,I(j));    
end
%
gammaShift=gammaShiftModel;
Gamma=GammaModel;
beta=betaModel;
%
% ------
if verbose
    disp('Results are Xmu for the mean shape, P for the variance modes,')
    disp('Sigma_hat for the standard deviation - i.e. square root of')
    disp('the variance magnitude - , etc...  ')
    disp('See ''help shapeModel'' for more information.')
    %
    if strcmp(dataType,'T001_SurfaceMesh')
        disp('---')
        disp(['Rows correspond to the nodes of the reference mesh '...
             'with coordinates given in RMeshCoordXRows.'])
        disp('Columns correspond to the subjects with SRN in subjectSRNXCols.')
    end
end
%
