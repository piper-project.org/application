% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Xs=sampleSSM(Xmu,P,Sigma,n)
%
% function Xs=sampleSSM(Xmu,P,Sigma,n)
%
% Generate samples from a (Gaussian) statistical shape model where
%      Xmu is the mean shape, P contains the modes of variance and the
%      magnitude of these modes are described by the standard deviation
%      equal to sigma(j)=sqrt(Lambda(j)) for the j-th mode.
%
% Input: - Xmu: mean of the Statistical shape model.
%            Xmu(i,j) is the j-th coordinate of the i-th landmark;
%         - P: principal components or modes of variance compared to Xmu.
%              P(i,j,k) is the j-th coordinate of the i-th landmark of the
%              k-th mode;
%         - Sigma: Standard deviation magnitude of the components: 
%              sigma_hat(j)=sqrt(Lambda(j));
%         - n: number of desired samples.
% Output: Xs, generated samples of the shapes, based on a Gaussian model
%             for the mode magnitudes:
%             Xs = Xmu + sum_j P(:,:,j) * randn * Sigma(j).
%
% C.Lecomte
% University of Southampton, Oct.2015
% Work funded by the EU FP7 PIPER project
%
% See also: shapeModel, demoSSMRMesh, demoSSMLMK, plotSSM
%

k=size(P,3);
Xs=zeros([size(Xmu),n]);
for i=1:n
    Xs(:,:,i) = Xmu;
    for j=1:k
        Xs(:,:,i) = Xs(:,:,i) + P(:,:,j) * randn * Sigma(j);
    end
end
%
%
%
