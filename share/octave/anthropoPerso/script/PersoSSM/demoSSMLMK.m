% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Xmu,P,Sigma_hat,modelBP,splitIndex,...
    XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C,...
    landmarkCodesXRows,subjectSRNXCols,dataDRNXCols]=...
    demoSSMLMK(LDatasetName,consideredTypes,consideredBP,databaseDir,...
    maxIter,toler,tolerVar,centered,verbose,partial)
%
% [Xmu,P,Sigma_hat,modelBP,splitIndex,...
%     XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C,...
%     landmarkCodesXRows,subjectSRNXCols,dataDRNXCols]=...
%     demoSSMLMK(datasetName,consideredTypes,consideredBP,databaseDir,...
%     maxIter,toler,tolerVar,centered,verbose,partial)
%
% Demonstration of the generation of a statistical shape model (SSM) of 
%      landmark information.
%
%      Landmarks are chosen based on their type and the body part they are
%      attached to.  The only subjects that are considered are those for
%      which a full set of landmarks is available.
%
% Input: - datasetName, name of the dataset of interest.
%        - consideredTypes, list of considered landmark types, in the 
%               format of a cell array of strings.  The types must be 
%               consistent with those used in the dataset;
%        - consideredBP, of considered body parts, in the 
%               format of a cell array of strings.  The body part
%               must be described by reference codes consistent with the 
%               PIPER database conventions;
%        - databaseDir, location of the PIPER database (root directory);
%        - maxIter: maximum number of iterations; [default=20]
%        - toler: relative difference between the Procrustes sums of
%                 squares during iterations, used as tolerance criteria
%                 to stop iterations, both during the rotations -step 2-
%                 and scaling -step3-; [default=1e-6]
%        - tolerVar: relative tolerance in the extraction of the variance
%                 principal components or modes. Only the terms with 
%                 Sigma_hat larger than Sigma_hat(1) * tolerVar are 
%                 preserved; [default=1e-13]
%        - centered: flag that indicates if the configurations have already
%                   been centered (average of coordinates at the origin);
%                                      [default=0]
%        - verbose: flag that indicates if information such as values of 
%                   the relative change in the residual sum of squares is 
%                   displayed.         [default=0]
%        - partial: flag to choose if a *partial* Procrustes analysis is
%                   desired as opposed to a *full* Procrustes analysis,
%                   i.e. an alignment that operates without or with
%                   scaling (partial=1 corresponds to *partial* Prorustes
%                   and no scaling).   [default=1]
% Output: - Xmu: arithmetic mean of the Procrustes fits (it is centered
%              and has the same shape as the Procrustes mean. [1,pg89])
%              Xmu(i,j) is the j-th coordinate of the i-th landmark.
%              (alignment may be per sub-configuration if requested through
%              the values of splitIndex);
%         - P: principal components or modes of the covariance matrix of 
%              aligned configurations compared to Xmu.  Note that P is
%              an orthogonal matrix, so that P.'*P=I, the identity matrix.
%              P(i,j,k) is the j-th coordinate of the i-th landmark of the
%              k-th mode;
%         - Sigma_hat: vector containing estimates of the standard deviation 
%              magnitude of these components. (If the eigenvalues of the 
%              covariance matrix of the aligned configurations compared to 
%              the mean configuration, Xmu, are stored in a vector Lambda,
%              then, Sigma_hat(j)=sqrt(Lambda(j)/(n-1)), where n is the 
%              number of configurations.)
%         - modelBP, list of body parts considered in the model, in the 
%              format of a cell array of strings.  The body part is 
%              described by reference codes consistent with the PIPER 
%              database conventions.  This is equal to consideredBp if
%              this input list is explicit.
%         - splitIndex, index indicating which body part a landmark belongs
%              to.  splitIndex(k)=j indicates that the k-th node belongs 
%              to the j-th considered body part (output modelBP{j}).
%         - XP: procrustes fits (centred at zero), so that 
%              XP(splitIndex==s,:,k) = ...
%                    beta(k ,s)* X(splitIndex==s,:,k)* Gamma(:,:,k ,s) + 
%                              repmat(gammaShift(:,k ,s),size(X,1),1)
%              where s refers to the sub-configuration index
%              (in "short":  XP_k^(s) = beta_k^(s)  X_k^(s)  Gamma_k^(s) + 
%                     1_n gammaShift_k^(s)  where 1_n is a vector of ones)
%              Reminder that if (partial), there is no scaling, so that
%              "XP_k^(s) = X_k^(s)  Gamma_k^(s) + 1_n gammaShift_k^(s)"...
%         - gammaShift: set of translation vectors such that all 
%              shifted configurations are centred at the origin.  
%              gammaShift(:,k ,s) corresponds to the k-th configuration
%              for the s-th sub-configuration component.
%         - Gamma: set of rotation matrices. Gamma(:,:,k ,s) corresponds to
%              the rotation matrix for the k-th configuration
%              for the s-th sub-configuration component. 
%         - beta: vector of scaling coefficients beta(k ,s) of the k-th
%              configuration for the s-th sub-configuration component. 
%              Note that if (partial), then all of these coefficients 
%              are equal to 1.
%         - converged: converged(s) is a flag of the GPA iterative 
%              algorithm that indicates if it converged up to tolerance 
%              toler within the maximum, maxIter, number of steps, when
%              it was applied to the s-th sub-configuration component.
%         - iter: iter(s) is an output of the GPA iterative algorithm that
%              indicates the total number of rotation steps used, when
%              it was applied to the s-th sub-configuration component.
%         - G: iter(s) is an output of the GPA iterative algorithm that
%              indicates the final sum of squares between the rotated 
%              (possibly scaled) Procrustes fits and their mean, when
%              it was applied to the s-th sub-configuration component.
%         - Gprec: Gprec(s) is an output of the GPA iterative algorithm
%              that indicates the value of the similar sum of squares  
%              at the penultimate step, when it was applied to the 
%              s-th sub-configuration component.
%         - C: transformation matrix that goes from the Procrustes fits
%              (variance from the mean Xmu) to the variance principal 
%              components or modes.  If XP is the array of fits, one has
%                1)  for i=1:n, DX(:,:,i)=XP(:,:,i)-Xmu; end
%                2)  dX=reshape(DX,k*m,n);
%                3)  p=dX * C
%                4)  P=reshape(p,k,m,size(C,2));
%              where k,m,n are respectively the numbers of landmarks,
%              dimensions, and configurations.
%         - landmarkCodesXRows, reference codes of landmarks corresponding 
%              to the rows of the X, P, etc. arrays.
%              These are stored in the format of a cell of strings 
%              (landmarkCodesXRows{j} contains the  
%                   reference code corresponding to the j-th column.)
%         - subjectSRNXCols, subjects SRN corresponding to the columns of
%              the X, P, etc. arrays. These are stored in the format 
%              of a cell of strings (subjectSRNXCols{j} contains the  
%              SRN corresponding to the j-th column.)
%         - dataDRNXCols, data item DRN  corresponding to the columns of
%              the X, P, etc. arrays. These are stored in the format 
%              of a cell of cells of strings (dataDRNXCols{i} 
%              contains the DRN corresponding to the i-th column, i.e. 
%              all the coordinates X(k,:,i) come from the data item with  
%              DRN dataDRNXCols{i}.  (Note that there is no dependency
%              on splitIndex on the current version...)
%
%  % --------------
%  % Example (Please check and enter your parameters...):
%
%    piperCodesDir='Piper_Codes';    % update this directory location
%    cd piperCodesDir;
%    start
%    %
%    databaseDir=paramPerso('databaseDir'); 
%    %
%    consideredTypes=cell(3,1);
%    consideredTypes{1}='anthropometric';
%    consideredTypes{2}='joint center';
%    consideredTypes{3}='palpable';
%    %
%    consideredBP=cell(3,1);
%    consideredBP{1}='Body_Pelvis';
%    consideredBP{2}='Body_Femur_L';
%    consideredBP{3}='Body_Femur_R';
%    %
%    maxIter=20;
%    toler=1e-6;
%    tolerVar=1e-10;
%    centered=0;
%    verbose=0;
%    partial=1;
%    %
%    [Xmu,P,Sigma_hat,modelBP,splitIndex,...
%            XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C,...
%            landmarkCodesXRows,subjectSRNXCols,dataDRNXCols]=...
%          demoSSMLMK('KeppleEtAl_LLimb',consideredTypes,consideredBP,...
%             databaseDir,maxIter,toler,tolerVar,centered,verbose,partial);
%
%    % Extraction of information about the landmarks:
%    filename=findRLmkFilename(databaseDir);
%    fullFilename=[databaseDir,filesep,'DataReference',filesep,filename];
%    inputField='Reference Code';
%    inputValue=landmarkCodesXRows;
%    request={'Reference Code','Description','Type','Body Part'};
%    [LandmarkDetail]=load_REFdata(fullFilename,inputField,inputValue,request);
%
%    % Sample from the statistical model:
%    Xs=sampleSSM(Xmu,P,Sigma_hat,50);
%
%    % Identify the coordinate system of the mean pelvis shape and rotate
%    %      the mean shape and variance modes with respect to this 
%    %      CS.
%    listLandmarks=CS_ISB('Body_Pelvis');
%    for j=1:length(listLandmarks)
%        indLandmarks(j)=find(strcmp(landmarkCodesXRows,listLandmarks{j}));
%        landmarks{j}=Xmu(indLandmarks(j),:);
%    end
%    CS=CS_ISB('Body_Pelvis',landmarks{:});
%    R=CS(:,[3,1,2]).';
%    [Xmu_R,P_R]=rotateSSM(Xmu,P,R);
%    %
% % --------------
%
% C.Lecomte
% University of Southampton, Oct.2015
% Work funded by the EU FP7 PIPER project
%
% See also: shapeModel, demoSSMRMesh, sampleSSM, plotSSM
%

%
% 0. PARAMETERS (load/save the dataset in binary format):
% ==============
% Name of dataset of interest
if nargin<1
    LDatasetName={'KeppleEtAl_LLimb'};
end
if ~iscell(LDatasetName)
    LDatasetName={LDatasetName};
end
% e) type of data
dataType='landmark';
% f) types of landmarks considered
if nargin<2
    consideredTypes=cell(3,1);
    % consideredTypes=cell(7,1);
    consideredTypes{1}='anthropometric';
    consideredTypes{2}='joint center';
    consideredTypes{3}='palpable';
    % consideredTypes{4}='ligament';
    % consideredTypes{5}='muscle connector';
    % consideredTypes{6}='muscle insertion';
    % consideredTypes{7}='muscle origin';
end
% g) types of body parts considered
if nargin<3
    consideredBP=cell(3,1);
    consideredBP{1}='Body_Pelvis';
    consideredBP{2}='Body_Femur_L';
    consideredBP{3}='Body_Femur_R';
end
% Database location (root of the database)
if nargin<4
%   Seeks the address of the PIPER database directory among the PIPER parameters
   databaseDir=paramPerso('databaseDir');
end
%
% Statistical shape analysis/GPA parameters
if nargin<5
    maxIter=20;
end
if nargin<6
    toler=1e-6;
end
if nargin<7
    tolerVar=1e-13;
end
if nargin<8
    centered=0;
end
if nargin<9
    verbose=0;
end
if nargin<10
    partial=1;
end
%
% 1. LOAD THE (LANDMARK) CONFIGURATIONS
% ======================================
% % addpath([databaseDir,filesep,'Tools']);
%
[X,modelBP,splitIndex,landmarkCodesXRows,subjectSRNXCols,dataDRNXCols]=...
    loadConfigLMKTypeBP(consideredTypes,consideredBP,...
    LDatasetName,databaseDir,verbose);
%
% 2. GENERATE THE STATISTICAL MODEL
% ==================================
if verbose
    disp('Generating the statistical model...')
end
%
[Xmu,P,Sigma_hat,...
    XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C]=...
    shapeModel(X,splitIndex,maxIter,toler,tolerVar,centered,verbose,partial);
%
if verbose
    disp('Results are Xmu for the mean shape, P for the variance modes,')
    disp('Sigma_hat for the modal magnitude of these modes, etc...  ')
    disp('See ''help shapeModel'' for more information.')
    %
    if strcmp(dataType,'landmark')
        disp('---')
        disp('Rows correspond to the landmarks with reference codes in landmarkCodesXRows.')
        disp('Columns correspond to the subjects with SRN in subjectSRNXCols.')
    end
end
%
% ---
%
