% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Z1,b,R,t,OSS,rho,Z1c,X2c]=OPA(X1,X2,centered)
% OPA - Full Ordinary Procrustes Analysis
%
% [Z1,b,R,t,OSS,rho,Z1c,X2c]=OPA(X1,X2,centered)
% [Z1,b,R,t,OSS,rho,Z1c,X2c]=OPA(X1,X2)
%
% Full Ordinary Procrustes Analysis of a configuration X1 onto a
%      configuration X2 (X1 and X2 are not centered by default).
%      The full Procrustes distance is minimised, i.e. the minimum 
%      squared Euclidian distance is obtained through full transformation 
%      (translation, rotation, scaling) of X1, :
%
%         D_OPA = norm2( X2 - b X1 R  - repmat(t,size(X1,1),1)
%
%      where norm2( X ) = sqrt( trace( X.' * X ) )
%
% Input: - X1, X2 configurations of size k x m (k landmarks, m dimensions).
%        - centered = 1 if X1 and X2 are already centered.  [Default:0]
% Output: - Z1 is the modified (shift+rotation+scaling) X1 configuration
%           that minimises the full Procrustes distance to X2,
%              Z1=  b X1 R  + repmat(t,size(X1,1),1).
%         - b, R, t are the scaling, rotation matrix and translation vector. 
%         - OSS is the ordinary (Procrustes) sum of squares, i.e. the 
%              minimum value of D_OPA^2.
%         - rho is the Procrustes distance between X2 and Z1, i.e. the
%              closest great circle distance between the two corresponding 
%              pre-shapes (centered and normalised). See [1], Eq.(4.22).
%         - Z1c is the centered modified (shift+rotation+scaling+)
%           configuration (that minimises the full Procrustes distance to 
%           the centered X2 configuration).
%         - X2c is the the *centered X2* configuration.
%
% --------------
% Example
%
% X1=[1,2;2.3,2.5;1.7,4;1.2,-1];
% X2=[1.13,2.1;2.2,2.15;1.73,4.7;1.25,-2];
%
% [Z1,b,R,t,OSS,rho,Z1c,X2c]=OPA(X1,X2);
%    
% --------------
%
% Christophe Lecomte,
% University of Southampton,
% Oct.2014
% Work carried in the context of the EU Piper project
%
% Ref.: [1] Dryden and Mardia, Statistical shape analysis, 1998, page 84
%

% Aug.2015: use a single shift instead of two (origin to and from the 
%               centers of the two configurations).  Added test for 
%               reflection case and change of lambda_m when necessary.
%
if nargin<3
    centered=0;
end
%
if ~centered
    k=size(X1,1);
    m1= mean(X1,1);
    X1c= X1 - repmat(m1, k, 1);
    m2= mean(X2,1);
    X2c= X2 - repmat(m2, k, 1);
else
    m1=zeros(size(X1));
    X1c=X1;
    m2=zeros(size(X1));
    X2c=X2;
end
%
normX1c=sqrt(trace(X1c.'*X1c));
normX2c=sqrt(trace(X2c.'*X2c));
%
if (normX1c==0 || normX2c==0)
    u=zeros(size(X2c,1),1);
    v=zeros(size(X1c,1),1);
    lambda=0;
else
[u,lambda,v]=svd(X2c.'*X1c/normX1c/normX2c);  % See [1], Eq.(5.4)
end
if det(X2c.'*X1c)<0
    % a reflection (if allowed, which is not the case, here) would 
    %             provide a better match
    u(:,end)=-u(:,end);
    lambda(end,end)=-lambda(end,end);
end
%
R=v*u.';                                % See [1], Eq.(5.3)
beta=trace(lambda);
b=beta*normX2c/normX1c;
% also: b=trace(X2c.'*X1c*R)/trace(X1c.'*X1c)
%
Z1c=b*X1c*R;
if ~centered    
    Z1=Z1c+repmat(m2, k, 1);
    t=m2-b*m1*R;
else
    Z1=Z1c;
    t=zeros(1,size(X1,2));
end
%
OSS=normX2c^2*(1-beta^2);
rho=acos(beta);                       % See [1], Eq.(5.7)
%
