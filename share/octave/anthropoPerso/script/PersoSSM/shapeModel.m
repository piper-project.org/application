% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Xmu,P,Sigma_hat,...
   XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C]=...
   shapeModel(X,splitIndex,maxIter,toler,tolerVar,centered,verbose,partial,...
              alignOnly)
%
% [Xmu,P,Sigma_hat]=...
% shapeModel(X,splitIndex,maxIter,toler,tolerVar,centered,verbose,partial);
%
% [Xmu,P,Sigma_hat,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec,C]=...
% shapeModel(X,splitIndex,maxIter,toler,tolerVar,centered,verbose,partial);
%
% alignOnly=1;
% [Xmu,~,~,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec]=...
% shapeModel(X,splitIndex,maxIter,toler,tolerVar,centered,verbose,partial,...
%                                                         alignOnly);
%
% Generate a statistical shape model (SSM) of configurations, with 
%          possible differentiated alignment for sub-configurations (i.e
%          sub parts) of the configurations.
%
% Input: - X: initial configurations X(i,j,k) is the j-th coordinate
%                   of the i-th landmark of the k-th configuration;
%        - splitIndex: contains indices indicating sub-configuration
%                   component numbers in the case where different
%                   components (sets of landmarks) of the configurations
%                   are aligned (by Procrustes analysis) independently of
%                   each other.  In practice, this may for example be the
%                   case if the landmarks correspond to different bones of
%                   an articulated skeleton.   [by default, all landmarks
%                                      are treated as a single component.]
%        - maxIter: maximum number of iterations; [default=20]
%        - toler: relative difference between the Procrustes sums of
%                 squares during iterations, used as tolerance criteria
%                 to stop iterations, both during the rotations -step 2-
%                 and scaling -step3-; [default=1e-6]
%        - tolerVar: relative tolerance in the extraction of the variance
%                 principal components or modes. Only the terms with
%                 Sigma_hat larger than Sigma_hat(1) * tolerVar are
%                 preserved; [default=1e-13]
%        - centered: flag that indicates if the configurations have already
%                   been centered (average of coordinates at the origin);
%                                      [default=0]
%        - verbose: flag that indicates if values of the relative change in
%                   the residual sum of squares is displayed during the
%                   iterations.        [default=0]
%        - partial: flag to choose if a *partial* Procrustes analysis is
%                   desired as opposed to a *full* Procrustes analysis,
%                   i.e. an alignment that operates without or with
%                   scaling (partial=1 corresponds to *partial* Prorustes
%                   and no scaling).   [default=0]
%        - alignOnly: flag to choose if the analysis is limited to alignment
%                   only, i.e. if the variance modes and magnitudes are 
%                   evaluated or not.  If alignOnly, no P, Sigma_hat and C
%                   is evaluated.      [default=0]
% Output: - Xmu: arithmetic mean of the Procrustes fits (it is centered
%            and has the same shape as the Procrustes mean. [1,pg89])
%            Xmu(i,j) is the j-th coordinate of the i-th landmark.
%            (alignment may be per sub-configuration if requested through
%             the values of splitIndex);
%         - P: principal components or modes of the covariance matrix of
%              aligned configurations compared to Xmu.  Note that P is
%              an orthogonal matrix, so that P.'*P=I, the identity matrix.
%              P(i,j,k) is the j-th coordinate of the i-th landmark of the
%              k-th mode;
%         - Sigma_hat: vector containing estimates of the standard deviation
%              magnitude of these components. (If the eigenvalues of the
%              covariance matrix of the aligned configurations compared to
%              the mean configuration, Xmu, are stored in a vector Lambda,
%              then, Sigma_hat(j)=sqrt(Lambda(j)/(n-1)), where n is the
%              number of configurations.)
%         - XP: procrustes fits (centred at zero), so that
%                XP(splitIndex==s,:,k) = ...
%                     beta(k ,s)* X(splitIndex==s,:,k)* Gamma(:,:,k ,s) +
%                               repmat(gammaShift(:,k ,s),size(X,1),1)
%               where s refers to the sub-configuration index
%               (in "short":  XP_k^(s) = beta_k^(s)  X_k^(s)  Gamma_k^(s) +
%                      1_n gammaShift_k^(s)  where 1_n is a vector of ones)
%               Reminder that if (partial), there is no scaling, so that
%               "XP_k^(s) = X_k^(s)  Gamma_k^(s) + 1_n gammaShift_k^(s)"...
%         - gammaShift: set of translation vectors such that all
%                 shifted configurations are centred at the origin.
%                 gammaShift(:,k ,s) corresponds to the k-th configuration
%                 for the s-th sub-configuration component.
%         - Gamma: set of rotation matrices. Gamma(:,:,k ,s) corresponds to
%                 the rotation matrix for the k-th configuration
%                 for the s-th sub-configuration component.
%         - beta: vector of scaling coefficients beta(k ,s) of the k-th
%                 configuration for the s-th sub-configuration component.
%                 Note that if (partial), then all of these coefficients
%                 are equal to 1.
%         - converged: converged(s) is a flag of the GPA iterative
%                 algorithm that indicates if it converged up to tolerance
%                 toler within the maximum, maxIter, number of steps, when
%                 it was applied to the s-th sub-configuration component.
%         - iter: iter(s) is an output of the GPA iterative algorithm that
%                 indicates the total number of rotation steps used, when
%                 it was applied to the s-th sub-configuration component.
%         - G: iter(s) is an output of the GPA iterative algorithm that
%                 indicates the final sum of squares between the rotated
%                 (possibly scaled) Procrustes fits and their mean, when
%                 it was applied to the s-th sub-configuration component.
%         - Gprec: Gprec(s) is an output of the GPA iterative algorithm
%                 that indicates the value of the similar sum of squares
%                 at the penultimate step, when it was applied to the
%                 s-th sub-configuration component.
%         - C: transformation matrix that goes from the Procrustes fits
%                 (variance from the mean Xmu) to the variance principal
%                 components or modes.  If XP is the array of fits, one has
%                   1)  for i=1:n, DX(:,:,i)=XP(:,:,i)-Xmu; end
%                   2)  dX=reshape(DX,k*m,n);
%                   3)  p=dX * C
%                   4)  P=reshape(p,k,m,size(C,2));
%                  where k,m,n are respectively the numbers of landmarks,
%                  dimensions, and configurations.
%
% Christophe Lecomte
% University of SOuthampton
% Work funded by the EU FP7 PIPER project
% Oct.2015
%
if nargin<2
    splitIndex=[];
end
if nargin<3
    maxIter=20;
end
if nargin<4
    toler=1e-6;
end
if nargin<5
    tolerVar=1e-13;
end
if nargin<6
    centered=0;
end
if nargin<7
    verbose=0;
end
if nargin<8
    partial=0;
end
if nargin<9
    alignOnly=0;
end
%
if isempty(splitIndex)
    splitIndex=ones(size(X,1),1);
end
[LSplit,order,~]=unique(sort(splitIndex));
[~,ind]=sort(order);
LSplit=LSplit(ind);
%
if ~isempty(LSplit)
    nSplit=LSplit(end);
else
    nSplit=0;
end
if (length(LSplit)~=nSplit) && (LSplit(1) ~= 1)
    error('Please check the decomposition of configurations into components')
end
%
if verbose
    if partial
        disp('Evaluating the mean partial Procrustes shape (i.e. with no scaling).')
    else
        disp('Evaluating the mean full Procrustes shape (i.e. with rotation and scaling).')
    end
end
% Evaluate the (Procrustes) mean of the configurations
% [Xmu,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec]=...
% a. Initialise tables
[k,m,n]=size(X);
% k: (total) number of landmarks;
% m: number of dimensions;
% n: number of configurations
%
Xmu=zeros(k,m);
XP=zeros(k,m,n);
gammaShift=zeros(m,n,nSplit);
Gamma=zeros(m,m,n,nSplit);
beta=zeros(n,nSplit);
converged=zeros(nSplit,1);
iter=zeros(nSplit,1);
G=zeros(nSplit,1);
Gprec=zeros(nSplit,1);
if n==0
    P=zeros(k,m,0);
    Sigma_hat=zeros(1,0);
    C=zeros(0,0);
else
    % b. Loop on each of the sub-configurations (e.g. body parts)
    for j=1:nSplit
%         disp(['Split index=',num2str(j)]);
        ind=find(splitIndex==j);
        [Xmu(ind,:),XP(ind,:,:),...
            gammaShift(:,:,j),Gamma(:,:,:,j),beta(:,j),...
            converged(j),iter(j),G(j),Gprec(j)]=...
            GPA(X(ind,:,:),maxIter,toler,centered,verbose,partial);
    end
    %
    if ~alignOnly
        DX=zeros(size(XP));
        for i=1:n
            DX(:,:,i)=XP(:,:,i)-Xmu;
        end
        %
        dX=reshape(DX,k*m,n);
        %
        % Matrix of covariance... (could be expressed as)
        % S=dX*dX.';
        %
        % its eigenvalues are the columns of P such that
        %    S * P = P *diag(Lambda)
        % where Lambda is diagonal.
        %
        % One evaluates this modal decomposition through SVD:
        %   dX = U * s * V.'   where U.' * U = I; V.'*V=I; s is diagonal.
        %   1) the eigenvectors of S are the singular vectors columns of U as
        %   S * U = (U * s * V.') * (V * s * U.') * U = U s^2.
        %   2) the eigenvalues of S are therefore given by Lambda = diag(s).^2.
        %
        [P,s,V]=svd(dX,0);
        kept=find(diag(s)>tolerVar*s(1,1));
        P=P(:,kept);
        s=s(kept,kept);
        V=V(:,kept);
        % Lambda=diag(s(1:size(P,2),1:size(P,2))).^2;
        % "for all j", Sigma_hat(j)=sqrt(Lambda(j)/(n-1));
        Sigma_hat=diag(s(1:size(P,2),1:size(P,2)))/sqrt(n-1);
        P=reshape(P,k,m,size(P,2));
        %
        C=V*diag(diag(s).^(-1));
        % ------
    end
    %
end
%
%
