% Copyright (C) 2017 Christophe Lecomte (University of Southampton)
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Xmu,XProt,gammaShift,Gamma,beta,converged,iter,G,Gprec]=...
    GPA(X,maxIter,toler,centered,verbose,partial) 
% GPA - Full or partial Generalized Procrustes Analysis
%
% [Xmu,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec]=...
%                        GPA(X,maxIter,toler,centered,verbose,partial)
%
% Estimate the full Procrustes mean shape Xmu and fits of configurations XP
%
% Input: - X: initial configurations X(i,j,k) is the j-th coordinate
%                   of the i-th landmark of the k-th configuration;
%        - maxIter: maximum number of iterations;    [default=20]
%        - toler: relative difference between the Procrustes sums of
%                 squares during iterations, used as tolerance criteria
%                 to stop iterations, both during the rotations -step 2-
%                 and scaling -step3-; [default=1e-6]
%        - centered: flag that indicates if the configurations have already
%                   been centered (average of coordinates at the origin);
%                                      [default=0]
%        - verbose: flag that indicates if values of the relative change in
%                   the residual sum of squares is displayed during the
%                   iterations.        [default=0]
%        - partial: flag to choose if a *partial* Procrustes analysis is
%                   desired as opposed to a *full* Procrustes analysis,
%                   i.e. an alignment that operates without or with
%                   scaling (partial=1 corresponds to *partial* Procrustes
%                   and no scaling).   [default=0]
% Output: - Xmu: arithmetic mean of the Procrustes fits (it is centered and
%                has the same shape as the full Procrustes mean. [1,pg89]).
%                Xmu(i,j) is the j-th coordinate of the i-th landmark.
%         - XP: procrustes fits (centred at zero), so that 
%                XP(:,:,k) = beta(k)* X(:,:,k)* Gamma(:,:,k) + 
%                               repmat(gammaShift(:,k),size(X,1),1)
%               (in "short":  XP_k = beta_k  X_k  Gamma_k + 
%                          1_n gammaShift_k  where 1_n is a vector of ones)
%               Reminder that if (partial), there is no scaling, so that
%                      "XP_k = X_k  Gamma_k + 1_n gammaShift_k"...
%         - gammaShift: set of translation vectors such that all 
%                 shifted configurations are centred at the origin.  
%                 gammaShift(:,k) corresponds to the k-th configuration.
%         - Gamma: set of rotation matrices. Gamma(:,:,k) corresponds to
%                 the rotation matrix for the k-th configuration. 
%         - beta: vector of scaling coefficients. Note that if (partial),
%                 then all of these coefficients are equal to 1.
%         - converged: indicate of the GPA iterative algorithm converged
%                 up tp tolerance toler within the maximum, maxIter, number 
%                 of steps.
%         - iter: indicate the total number of rotation steps used.
%         - G: final sum of squares between the rotated (possibly scaled)
%                 Procrustes fits and their mean.
%         - Gprec: value of the similar sum of squares at the penultimate 
%                 step.
%
% % -------------
% % Example
%
% X=zeros(4,2,3);
%
% X(:,:,1)=[2,2;3.3,2.5;2.7,4;2.2,-1];
% X(:,:,2)=[2.13,2.1;3.2,2.15;2.73,4.7;2.25,-2];
% X(:,:,3)=[2.08,2.17;3.27,2.35;2.71,4.2;2.23,-1.2];
%
% [Xmu,XP,gammaShift,Gamma,beta,converged,iter,G,Gprec]=GPA(X);
%
% figure
% plot(X(:,1,1),X(:,2,1),'x')
% hold on
% plot(X(:,1,2),X(:,2,2),'sg')
% plot(X(:,1,3),X(:,2,3),'or')
%
% plot(Xmu(:,1),Xmu(:,2),'*b')
% plot(XP(:,1,1),XP(:,2,1),'x')
% plot(XP(:,1,2),XP(:,2,2),'sg')
% plot(XP(:,1,3),XP(:,2,3),'or')
% 
% % -------------
%   
% Christophe Lecomte
% University of Southampton,
% Oct.2014 (first version)
% Work carried in the context of the EU Piper project
%
% References:
% -----------
%   [1] Dryden and Mardia, Statistical shape analysis, 1998, page 90
%   [2] Goodall, Procrustes Methods in the Statistical Analysis of Shapes,
%                Journal of the Royal Statistical Society. Series B,
%                Vol.53, No.2 (1991), page 306 of 285-339.
%   [3] Ten Berge, Orthogonal Procrustes rotation for two or more matrices,
%                Psychometrika-vol.42, No.2, June 1977, pages 274-275 of
%                267-276.
%

% Aug.2015: use the new version of the OPA script (instead of the previous
%              version named fullOPA).
% Oct. 2015: renamed the script GPA (instead of fullGPA)
%            Split the alignment in rotations and scaling, instead of
%                  working with both rotation and scaling in the
%                  OPA steps.
%
if nargin<2
    maxIter=20;
end
if nargin<3
    toler=1e-6;
end
if nargin<4
    centered=0;
end
if nargin<5
    verbose=0;
end
if nargin<6
    partial=0;
end
[k,m,n]=size(X);
% k: number of landmarks;
% m: number of dimensions;
% n: number of configurations
%
XProt=X;
% STEP 1: Centre the configurations
% ----------------------------------
gammaShift=zeros(m,n);
if ~centered
    for i=1:n
        gammaShift(1:m,i)=mean(XProt(:,:,i),1);
        XProt(:,:,i)=XProt(:,:,i)-repmat(gammaShift(:,i).',k,1);
    end
end
%
% STEP 2: Rotate the (centred) configurations...
% -----------------------------------------------
%  - each configuration in turn,
%  - with regards to the average of the other configurations,
%  - using ordinary Procrustes analysis (OPA),
%  - until the Procrustes sum of squares is not reduced,
%  - (only with rotations allowed).
%
converged=0;
iterScal=0;   % iteration counter (for the external/scaling loop)
iter=0;       % iteration counter
%
Gamma=zeros(m,m,n);   % rotations are being kept tracked of
for i=1:n
    Gamma(:,:,i)=eye(m);
end
beta=ones(n,1);
%
while ~converged && iter<maxIter        % loop on rotation and scaling
    %
    iterScal=iterScal+1;
    %
    convergedRot=0;
    while ~convergedRot && iter<maxIter     % loop on rotation
        iter=iter+1;   % current iteration is k-th
        % step 2.a - one run of rotations
        % --------
        for i=1:n
            % Consider i-th configuration...
            % mean of all other configurations (non-scale d - see [1,step2,p.90]
            Xhati=mean(XProt(:,:,setdiff(1:n,i)),3);
            if verbose
                if find(eig(XProt(:,:,i).'*Xhati)<0)
                    disp(['Negative eigenvalue; i=',num2str(i)])
                end
            end
            % OPA of i-th configuration on Xhat (without reflection!)
            [~,~,Gamma_i]=OPA(XProt(:,:,i),Xhati,1);
            Gamma(:,:,i)=Gamma(:,:,i)*Gamma_i;
            % update the according rotation of i-th configuration
            XProt(:,:,i)=XProt(:,:,i)*Gamma_i;
            % NOTE %   % rotation AND scale of i-th configuration would be
            % ==== %     XP(:,:,i)=beta_i*XProt(:,:,i)*Gamma_i;
        end
        %
        % step 2.b - test convergence
        % --------
        % evaluate the Procrustes sum of squares to test convergence
        %
        Xmu=mean(XProt,3);              % global average (of rotated shapes)
        %
        G=0;
        for i=1:n
            G=G+sum(sum((XProt(:,:,i)-Xmu).^2));     % Sum of ordinary sums of squares [1, Eq.(5.6)]
        end
        %
        if iter==1
            Gprec=G;
            if verbose
                disp(strcat('  First rotation...'))
                disp(strcat('      G=',num2str(G)))
            end
        else
            if verbose
                disp(strcat('  Relative change in G:',num2str(abs(G-Gprec)/abs(G))))
                disp(strcat('      G=',num2str(G)))
            end
            if (abs(G-Gprec)/abs(G)) < toler
                convergedRot=1;
            end
            Gprec=G;
        end
        %
    end
    %
    % Step 3: Scale the (centred) configurations
    % -------------------------------------------
    % Following [1] & [2], See [3] for details
    % covariance matrix (for *configurations*)
    if partial  % partial Procrustes analysis => no scaling is considered
        converged=convergedRot;
    else
        Y=zeros(n,n);
        for i=1:n
            for j=1:n
                Y(i,j)=sum(sum(XProt(:,:,i).*XProt(:,:,j)));
            end
        end
        sumyd=trace(Y);
        % matrix of coefficients of congruence (Phi)
        yd=diag(Y);
        yd_iroot=yd.^(-1/2);            % vector of diagonal coefficients
        Yd_iroot=sparse(1:n,1:n,yd_iroot);    % diagonal matrix
        % correlation matrix
        Phi=Yd_iroot*Y*Yd_iroot;
        %
        % evaluation of the eigenvector p1 corresponding to the largest
        % eigenvalue
        [P,~]=eig(Phi);
        p1=P(:,1);
        %
        beta_iter=((sumyd./yd).^(1/2)).*p1;
        % Actual scaling
        for i=1:n
            XProt(:,:,i)=XProt(:,:,i)*beta_iter(i);
        end
        % store total scaling (over all iterations)
        beta=beta.*beta_iter;
        %
        % Step 4: test convergence
        % -------------------------
        Xmu=mean(XProt,3);    % global average (of scaled shapes)
        %
        G=0;
        for i=1:n
            G=G+sum(sum((XProt(:,:,i)-Xmu).^2));     % Sum of ordinary sums of squares [1, Eq.(5.6)]
        end
        %
        if iterScal==1
            GprecScal=G;
            if verbose
                disp(strcat('First scaling...'))
                disp(strcat('      G=',num2str(G)))
            end
        else
            if verbose
                disp(strcat('Relative change in G - scaling to scaling:',num2str(abs(G-GprecScal)/abs(G))))
                disp(strcat('      G=',num2str(G)))
            end
            if (abs(G-GprecScal)/abs(G)) < toler
                converged=1;
            end
            GprecScal=G;
        end
        Gprec=G;
    end
    %
end
%
% correct the value of gammaShift (to satisfy the transformation equation)
%
for j=1:size(XProt,3)
    gammaShift(:,j) = mean(XProt(:,:,j) - beta(j)* X(:,:,j)* Gamma(:,:,j));
end
%
% That's it -
%
