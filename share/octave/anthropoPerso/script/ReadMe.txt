This directory contains personalisation tools developed during the 
EU FP7 PIPER project.  They are supported in Octave and Matlab.

General documentation for the algorithms and code is provided in the
directory documentation.  Further documentation is provided within
the octave/matlab scripts.

A user can start using the tools by launching the script 'start'
in the current directory within octave or matlab.  This assigns
values of some global parameters (location of datasets, etc.) 
and define the scripts paths.  Some update to the start script
may be necessary to define the desired parameters.

Demonstration of anthropometric tools can be found in
  - PersoAnthropo/runParkinsonReed_ANSUR.m
  - ToolsWP3/runPersoAnthropo_ANSUR_WP3.m

Demonstration of statistical shape model tools can be found in
  - PersoSSM/demoSSMLMK.m
  - PersoSSM/demoSSMRMesh.m

------------------------------
Christophe Lecomte
University of Southampton
Aug.2016
work funded by the EU FP7 PIPER project.
