arg_list = argv();

xmlFile = arg_list{1}; 
outputfile = arg_list{2}; 
jar1 = arg_list{3}; 
jar2 = arg_list{4};

% add java path for octave xml package
javaaddpath(jar1);
javaaddpath(jar2);

% open outputfile
fid = fopen (outputfile,"w+");

% open xml doc
parser = javaObject('org.apache.xerces.parsers.DOMParser');
parser.parse(xmlFile); 
xDoc = parser.getDocument;
 
pipertarget = xDoc.getElementsByTagName('piper-target').item(0);
targetlist = pipertarget.getElementsByTagName('targetList').item(0);
target = targetlist.getElementsByTagName('target');
% get target
for i=0:target.getLength()-1
  curTarget = target.item(i);
  att = curTarget.getAttribute('type');
  fputs(fid, att);
  fputs(fid, " ");
  name = curTarget.getElementsByTagName('name').item(0).getFirstChild.getTextContent;
  fputs(fid, name);
  fputs(fid, " ");
  value = curTarget.getElementsByTagName('value').item(0).getFirstChild.getTextContent;
  fputs(fid, value);
  fputs(fid, " ");
end
  fputs(fid, "\n");

fclose(fid);