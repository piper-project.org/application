% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Q=BCS_Vertebra(L52C1,VV,DV,endPlateUp,endPlateDown,lmNames,ALs)
%BCS_Vertebra.m defines the body coordinate system of a vertebra.
%
%For all verterae except for C1 and Sacrum which have only one plateau up
%or down, the BCS is defined accoording to ISB recommendation
%   Y: vertical pointing upwards
%   X: anteriorly
%   Z: laterally pointing to the right
%   Origine: center of the vertebral body
%
%Inputs:
%   L52C1: L5 to C1 direction
%   VV: ventral landmark of the vertebra
%   DV: dorsal landmark of the vertebra
%   endPlateUp.normal: normal direction of the up endplate in the same direction
%                as the L5 to C1 direction
%   endPlateUp.center: center of the up endplate
%   endPlateDown.normal: normal direction of the down endplate in the same direction
%                  as the L5 to C1 direction 
%   endPlateDown.center: center of the down endplate
%
%Outputs
%   Q=[x' y' z'], attitude matrix
%   Orig: Origine
%
%   Authors: Xuguang Wang (UCBL/IFSTTAR), Guillaume Pacquaut (UCBL),
%   Christophe Lecomte (SOTON)
%   Creation: 2016-12-20
%

if ~isempty(VV) & ~isempty(DV)
%     if isempty(endPlateUp) % C1
%         y=endPlateDown;
%         y = endPlateUp - endPlateDown; y=y/norm(y);
%         Orig=endPlateDown;
%     else
%         Orig=(endPlateUp+endPlateDown)/2;
        Orig=endPlateDown;
        y = endPlateUp - endPlateDown; y=y/norm(y);
        %--Symmetry plane defined by vertical and by ventral and dorsal landmarks
        vx=VV-DV;
        z=cross(vx,y); z=z/norm(z);
        x=cross(y,z);  x=x/norm(x);
        Q=[x',y',z'];
%     end
        
    else
        endPlateDown_center=xyzLM(ALs,lmNames{3});
    endPlateUp_center=xyzLM(ALs,lmNames{4});
    Left_point_sup_plate=xyzLM(ALs,lmNames{5});
    Left_point_inf_plate=xyzLM(ALs,lmNames{6});
    Right_point_sup_plate=xyzLM(ALs,lmNames{7});
    Right_point_inf_plate=xyzLM(ALs,lmNames{8});
    Anterior_point_sup_plate=xyzLM(ALs,lmNames{9});
    Anterior_point_inf_plate=xyzLM(ALs,lmNames{10});
    Posterior_point_sup_plate=xyzLM(ALs,lmNames{11});
    Posterior_point_inf_plate=xyzLM(ALs,lmNames{12});

    if isempty(endPlateDown) %Sacrum
        Y=[Left_point_sup_plate;Right_point_sup_plate ; Anterior_point_sup_plate; Posterior_point_sup_plate];
        [endPlateUp_normal,V,p] = affine_fit(Y);
        y=endPlateUp_normal; 
        if dot(y,L52C1)<0 y=-y; end
        Orig=endPlateUp_center;
    elseif isempty(endPlateUp) % C1
        Y=[Left_point_inf_plate;Right_point_inf_plate ; Anterior_point_inf_plate; Posterior_point_inf_plate];
        [endPlateDown_normal,V,p] = affine_fit(Y);
        y=endPlateDown_normal;
        if dot(y,L52C1)<0 y=-y; end
        Orig=endPlateDown_center;
    else % All other vertebrae (C2-C7, T01-T12, L1-L5)
        Y1=[Left_point_sup_plate;Right_point_sup_plate ; Anterior_point_sup_plate; Posterior_point_sup_plate];
        [endPlateUp_normal,V,p] = affine_fit(Y1);
        Y2=[Left_point_inf_plate;Right_point_inf_plate ; Anterior_point_inf_plate; Posterior_point_inf_plate];
        [endPlateDown_normal,V,p] = affine_fit(Y2);

        y1=endPlateUp_normal; if dot(y1,L52C1)<0 y1=-y1; end
        y2=endPlateDown_normal; if dot(y2,L52C1)<0 y2=-y2; end
        y=(y1+y2)/2; y=y/norm(y);
        Orig=(endPlateUp_center+endPlateDown_center)/2;
    end 

    Z=[Anterior_point_sup_plate; Anterior_point_inf_plate; Posterior_point_sup_plate; Posterior_point_inf_plate];
    [ztemp,V,p] = affine_fit(Z);
    ztemp=ztemp/norm(ztemp);
    x=cross(y,ztemp); x=x/norm(x);
    z=cross(x,y); z=z/norm(z);
    Q=[x y z];
end
    

%visu_axe(Q,Orig,200,'r');

end