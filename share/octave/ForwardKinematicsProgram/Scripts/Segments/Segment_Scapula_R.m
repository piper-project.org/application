% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Scapula_R(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
AA_R=xyzLM(ALs,lmNames{1}); 
TS_R=xyzLM(ALs,lmNames{2}); 
AI_R=xyzLM(ALs,lmNames{3});
Center_Humerus_R=xyzLM(ALs,lmNames{8});
AC_R = xyzLM(ALs,lmNames{9});

axuScapulaR=zeros(3);

% Definition of Z
axuScapulaR(:,3)=(AA_R-TS_R);
axuScapulaR(:,3)=axuScapulaR(:,3)/norm(axuScapulaR(:,3));
%
% Definition of X
axuScapulaR(:,1)=cross((AI_R-AA_R),(AI_R-TS_R));
axuScapulaR(:,1)=axuScapulaR(:,1)/norm(axuScapulaR(:,1));
% Definition of Y
axuScapulaR(:,2)=cross(axuScapulaR(:,3),axuScapulaR(:,1));
axuScapulaR(:,2)=axuScapulaR(:,2)/norm(axuScapulaR(:,2));

Q=axuScapulaR;
% Orig=AA_L;
Orig=AC_R;

%---local coordinates of joint centers
v1=AC_R-Orig; AC_R=v1*Q;
%v2=Center_Humerus_R-Orig; Center_Humerus_R=v2*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.PointProximal='Acromioclavicular_R';
% Segment.JointCenterDistal=[Center_Humerus_L, 1];
% Segment.PointDistal='Shoulder_L';
Segment.JointCenterDistal=[Orig, 1];
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end