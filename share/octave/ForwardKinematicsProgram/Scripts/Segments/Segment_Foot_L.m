% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Foot_L(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
MML=xyzLM(ALs,lmNames{1}); 
LML=xyzLM(ALs,lmNames{2}); 
MCL=xyzLM(ALs,lmNames{3}); 
LCL=xyzLM(ALs,lmNames{4}); 

axuTibiaL=zeros(3);
% Definition of Z
axuTibiaL(:,3)= (LML-MML);
axuTibiaL(:,3)=axuTibiaL(:,3)/norm(axuTibiaL(:,3));
%
IML=(MML+LML)/2;
Center_Ankle_L=IML;

%
% Definition of X
ICL=(MCL+LCL)/2;
%
axuTibiaL(:,1)=-cross(LML-ICL,MML-ICL);
axuTibiaL(:,1)=axuTibiaL(:,1)/norm(axuTibiaL(:,1));
%
% Definition of Y
axuTibiaL(:,2)=cross(axuTibiaL(:,3),axuTibiaL(:,1));
axuTibiaL(:,2)=axuTibiaL(:,2)/norm(axuTibiaL(:,2));

Q=axuTibiaL;
%Orig=ICR;
Orig=Center_Ankle_L;

%---local coordinates of joint centers
v1=ICL-Orig; ICL=v1*Q;
v1=IML-Orig; IML=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[IML, 1];
Segment.PointProximal='Ankle_L';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end