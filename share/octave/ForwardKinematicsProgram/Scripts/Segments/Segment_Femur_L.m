% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Femur_L(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
FEML=xyzLM(ALs,lmNames{1}); 
FELL=xyzLM(ALs,lmNames{2}); 
Center_Hip_L=xyzLM(ALs,lmNames{3}); 
% Center_Patella_L = xyzLM(ALs,lmNames{4}); %Posterior
Center_Patella_L = xyzLM(ALs,lmNames{5}); %Anterior
axuFemurL=zeros(3);
%
Center_Knee_L=(FEML+FELL)/2;
%Definition of Y
axuFemurL(:,2)=Center_Hip_L-(FEML+FELL)/2;
axuFemurL(:,2)=axuFemurL(:,2)/norm(axuFemurL(:,2));
%Definition of X
axuFemurL(:,1)= cross(axuFemurL(:,2),(FEML-FELL)');
axuFemurL(:,1)= axuFemurL(:,1)/norm(axuFemurL(:,1));
%Definition of Z
axuFemurL(:,3)=cross(axuFemurL(:,1),axuFemurL(:,2));
axuFemurL(:,3)=axuFemurL(:,3)/norm(axuFemurL(:,3));

Orig=Center_Hip_L;
Q=axuFemurL;

%---local coordinates of joint centers
v1=Center_Hip_L-Orig; Center_Hip_L=v1*Q;
v1=Center_Knee_L-Orig; Center_Knee_L=v1*Q;
v1=Center_Patella_L-Orig; Center_Patella_L=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_Knee_L, 1];
Segment.JointCenterDistal2=[Center_Patella_L, 1];
Segment.PointProximal='Hip_L';
Segment.PointDistal='TibioFemoral_L';
Segment.PointDistal2='PatelloFemoral_L';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end