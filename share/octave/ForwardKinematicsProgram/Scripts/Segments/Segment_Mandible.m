% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Mandible(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
Right_jaw_angle=xyzLM(ALs,lmNames{1}); 
Left_jaw_angle=xyzLM(ALs,lmNames{2}); 
Inferior_crest_of_the_jaw=xyzLM(ALs,lmNames{3}); 
Incisive=xyzLM(ALs,lmNames{4});

Orig= 0.5*(Right_jaw_angle+Left_jaw_angle);
%
axuRadiusL=zeros(3);
% Definition of Z
axuRadiusL(:,3)=Right_jaw_angle-Left_jaw_angle;
axuRadiusL(:,3)=axuRadiusL(:,3)/norm(axuRadiusL(:,3));
% Definition of X
axuRadiusL(:,1)= cross(Incisive - Right_jaw_angle,Incisive - Left_jaw_angle);
axuRadiusL(:,1)=axuRadiusL(:,1)/norm(axuRadiusL(:,1));
% Definition of Y
axuRadiusL(:,2)=cross(axuRadiusL(:,3),axuRadiusL(:,1));
axuRadiusL(:,2)=axuRadiusL(:,2)/norm(axuRadiusL(:,2));
%
Q=axuRadiusL;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Orig, 1];
Segment.PointProximal='Mandible';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end