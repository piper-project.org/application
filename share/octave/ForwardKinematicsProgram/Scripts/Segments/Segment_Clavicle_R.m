% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Clavicle_R(joint,ListofALs,model, ALs)
%
lmNames=model.Kinematic.(joint).ALs;
ACR=xyzLM(ALs,lmNames{1}); 
SCR=xyzLM(ALs,lmNames{2}); 
T8=xyzLM(ALs,lmNames{3});
C7=xyzLM(ALs,lmNames{4});
Center_Humerus_R=xyzLM(ALs,lmNames{5});

axuClavicleR=zeros(3);
% Definition of Z
axuClavicleR(:,3)=ACR-SCR;
axuClavicleR(:,3)=axuClavicleR(:,3)/norm(axuClavicleR(:,3));
% Definition of X
axuClavicleR(:,1)=cross(axuClavicleR(:,3),(C7-T8)');
axuClavicleR(:,1)=axuClavicleR(:,1)/norm(axuClavicleR(:,1));

% Definition of Y
axuClavicleR(:,2)=cross(axuClavicleR(:,3),axuClavicleR(:,1));
axuClavicleR(:,2)=axuClavicleR(:,2)/norm(axuClavicleR(:,2));

%
Q=axuClavicleR;
Orig=SCR;

%---local coordinates of joint centers
v1=Center_Humerus_R-Orig; Center_Humerus_R=v1*Q;
v1=ACR-Orig; ACR=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_Humerus_R, 1];
Segment.PointProximal='ClavicleJ_R';
Segment.PointDistal='Shoulder_R';
Segment.JointCenterDistal2=[ACR, 1];
Segment.PointDistal2='Acromioclavicular_R';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end