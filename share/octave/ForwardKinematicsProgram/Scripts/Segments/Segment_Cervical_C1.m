% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Cervical_C1(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
VV=xyzLM(ALs,lmNames{1});
DV=xyzLM(ALs,lmNames{2});
endPlateDown=xyzLM(ALs,lmNames{3});
Center_atlanto_occipital_joint=xyzLM(ALs,lmNames{4}); 
center_endplate_up_C2=xyzLM(ALs,lmNames{5});

y=Center_atlanto_occipital_joint-endPlateDown; y=y/norm(y);

Orig=center_endplate_up_C2;

vx=VV-DV;
z=cross(vx,y); z=z/norm(z);
x=cross(y,z);
Q=[x',y',z'];

% v1=DC1-Orig; DC1=v1*Q;
v1=Center_atlanto_occipital_joint-Orig; Center_atlanto_occipital_joint=v1*Q;

Segment.JointCenterProximal=[Orig 1];
Segment.JointCenterDistal=[Center_atlanto_occipital_joint, 1];
Segment.PointProximal='C1';
Segment.PointDistal='Skull';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end