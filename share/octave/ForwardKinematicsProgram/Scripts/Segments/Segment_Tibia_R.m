% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Tibia_R(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
MMR=xyzLM(ALs,lmNames{1}); 
LMR=xyzLM(ALs,lmNames{2}); 
MCR=xyzLM(ALs,lmNames{3}); 
LCR=xyzLM(ALs,lmNames{4}); 
FEMR=xyzLM(ALs,lmNames{5}); 
FELR=xyzLM(ALs,lmNames{6}); 

Center_Knee_R=(FEMR+FELR)/2;

axuTibiaR=zeros(3);
% Definition of Z
axuTibiaR(:,3)=LMR-MMR;
axuTibiaR(:,3)=axuTibiaR(:,3)/norm(axuTibiaR(:,3));
%
IMR=(MMR+LMR)/2;
%
% Definition of X
ICR=(MCR+LCR)/2;
%
axuTibiaR(:,1)=cross(LMR-ICR,MMR-ICR);
axuTibiaR(:,1)=axuTibiaR(:,1)/norm(axuTibiaR(:,1));
%
% Definition of Y
axuTibiaR(:,2)=cross(axuTibiaR(:,3),axuTibiaR(:,1));
axuTibiaR(:,2)=axuTibiaR(:,2)/norm(axuTibiaR(:,2));

Q=axuTibiaR;
%Orig=ICR;
Orig=Center_Knee_R;

%---local coordinates of joint centers
v1=ICR-Orig; ICR=v1*Q;
v1=IMR-Orig; IMR=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[IMR, 1];
Segment.PointProximal='TibioFemoral_R';
Segment.PointDistal='Ankle_R';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end