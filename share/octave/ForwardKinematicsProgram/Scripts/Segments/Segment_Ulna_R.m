% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Ulna_R(joint,ListofALs,model, ALs)
%
lmNames=model.Kinematic.(joint).ALs;
USR=xyzLM(ALs,lmNames{1}); 
RSR=xyzLM(ALs,lmNames{2}); 
ELR=xyzLM(ALs,lmNames{3}); 
EMR=xyzLM(ALs,lmNames{4});
Center_elbowR=(ELR+EMR)/2; 

axuRadiusR=zeros(3);
% Definition of Y
axuRadiusR(:,2)=ELR-RSR;
axuRadiusR(:,2)=axuRadiusR(:,2)/norm(axuRadiusR(:,2));
% Definition of X
axuRadiusR(:,1)=cross(USR-RSR,axuRadiusR(:,2)');
axuRadiusR(:,1)=axuRadiusR(:,1)/norm(axuRadiusR(:,1));
% Definition of Z
axuRadiusR(:,3)=cross(axuRadiusR(:,1),axuRadiusR(:,2));
axuRadiusR(:,3)=axuRadiusR(:,3)/norm(axuRadiusR(:,3));
%
%
Q=axuRadiusR;
Orig=Center_elbowR;

%---local coordinates of joint centers
v1=RSR-Orig; RSR=v1*Q;
v1=USR-Orig; USR=v1*Q;
        
Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Orig, 1];
Segment.PointProximal='Elbow_R';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];


end