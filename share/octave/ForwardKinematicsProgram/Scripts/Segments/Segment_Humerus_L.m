% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Humerus_L(joint,ListofALs,model, ALs)
%
lmNames=model.Kinematic.(joint).ALs;
EML=xyzLM(ALs,lmNames{1});
ELL=xyzLM(ALs,lmNames{2});
Center_Humerus_L=xyzLM(ALs,lmNames{3});

GHL=Center_Humerus_L;
Center_elbowL=(EML+ELL)/2;
%
axuHumerusL=zeros(3);
%Definition of Y
axuHumerusL(:,2)=(GHL-Center_elbowL);
axuHumerusL(:,2)=axuHumerusL(:,2)/norm(axuHumerusL(:,2));
%
%Definition of X
dEL= EML-ELL;
axuHumerusL(:,1)=-cross(axuHumerusL(:,2),dEL');
axuHumerusL(:,1)=axuHumerusL(:,1)/norm(axuHumerusL(:,1));
%
%Definition of Z
axuHumerusL(:,3)= cross(axuHumerusL(:,1),axuHumerusL(:,2));
axuHumerusL(:,3)= axuHumerusL(:,3)/norm(axuHumerusL(:,3));
%
Q=axuHumerusL;
Orig=Center_Humerus_L;

%---local coordinates of joint centers
v1=Center_Humerus_L-Orig; Center_Humerus_L=v1*Q;
v1=Center_elbowL-Orig; Center_elbowL=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_elbowL, 1];
Segment.PointProximal='Shoulder_L';
Segment.PointDistal='Elbow_L';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end