% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Patella_L(joint,jointS, model, ALs)

lmNames=model.Kinematic.(joint).ALs;

Ant_C=xyzLM(ALs,lmNames{9}); 
Lat=xyzLM(ALs,lmNames{6}); 
Med=xyzLM(ALs,lmNames{7});
Ant_Pr=xyzLM(ALs,lmNames{end});
Ant_Dist=xyzLM(ALs,lmNames{5});

axuPatellaL=zeros(3);

% Definition of Z
axuPatellaL(:,3)= -(Lat - Med);
axuPatellaL(:,3)=axuPatellaL(:,3)/norm(axuPatellaL(:,3));
%
% Definition of X
%
axuPatellaL(:,1)=cross(Ant_Pr-Ant_Dist,(Lat - Med));
axuPatellaL(:,1)=axuPatellaL(:,1)/norm(axuPatellaL(:,1));
%
% Definition of Y
axuPatellaL(:,2)=cross(axuPatellaL(:,3),axuPatellaL(:,1));
axuPatellaL(:,2)=axuPatellaL(:,2)/norm(axuPatellaL(:,2));

Q=axuPatellaL;
Orig=Ant_C;
%Orig=ICR;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Orig, 1];
Segment.PointProximal='PatelloFemoral_L';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end