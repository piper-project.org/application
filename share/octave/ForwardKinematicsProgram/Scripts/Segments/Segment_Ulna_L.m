% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Ulna_L(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
USL=xyzLM(ALs,lmNames{1}); 
RSL=xyzLM(ALs,lmNames{2}); 
ELL=xyzLM(ALs,lmNames{3}); 
EML=xyzLM(ALs,lmNames{4});

Center_elbowL=(ELL+EML)/2; 
%
axuRadiusL=zeros(3);
% Definition of Y
axuRadiusL(:,2)=ELL-RSL;
axuRadiusL(:,2)=axuRadiusL(:,2)/norm(axuRadiusL(:,2));
% Definition of X
axuRadiusL(:,1)=cross(RSL-USL,axuRadiusL(:,2)');
axuRadiusL(:,1)=axuRadiusL(:,1)/norm(axuRadiusL(:,1));
% Definition of Z
axuRadiusL(:,3)=cross(axuRadiusL(:,1),axuRadiusL(:,2));
axuRadiusL(:,3)=axuRadiusL(:,3)/norm(axuRadiusL(:,3));
%
Q=axuRadiusL;
Orig=Center_elbowL;

v1=RSL-Orig; RSL=v1*Q;
v1=USL-Orig; USL=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Orig, 1];
Segment.PointProximal='Elbow_L';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end