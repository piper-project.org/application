% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_RibCage(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
SCL=xyzLM(ALs,lmNames{1}); 
SCR=xyzLM(ALs,lmNames{2}); 
T4=xyzLM(ALs,lmNames{3});
T10=xyzLM(ALs,lmNames{4});

axuSternum=zeros(3);

% Definition of Z
axuSternum(:,3)=(SCL-SCR);
axuSternum(:,3)=axuSternum(:,3)/norm(axuSternum(:,3));
%
% Definition of X
axuSternum(:,1)=cross((T4-T10),(SCL-SCR));
axuSternum(:,1)=axuSternum(:,1)/norm(axuSternum(:,1));
% Definition of Y
axuSternum(:,2)=cross(axuSternum(:,3),axuSternum(:,1));
axuSternum(:,2)=axuSternum(:,2)/norm(axuSternum(:,2));

Q=axuSternum;
Orig= T4;

%---local coordinates of joint centers
v1=SCL-Orig; SCL=v1*Q;
v1=SCR-Orig; SCR=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.PointProximal='RibCage';
Segment.JointCenterDistal=[Orig, 1];
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end