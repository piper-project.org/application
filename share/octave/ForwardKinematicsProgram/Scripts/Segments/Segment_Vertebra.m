% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment = Segment_Vertebra(L52C1,joint,jointP,jointS, model, ALs, ListofALs)
%Segment_Vertebra defines
%   - the BCS of the vertebra
%   - proximal and distal points in the BCS
%   - bone mesh points in the BCS
%
%Input:
%   joint: current joint name
%   jointP: joint Précédent
%   jointS: joint Suivant
%   model: kinematic model
%   ALs: all anatomical landmarks
%
%  04/01/2017
%  Xuguang WANG (IFSTTAR/UCBL)
%

body=model.Kinematic.(joint).BodyName;
lmNames=model.Kinematic.(joint).ALs;
lmNamesP=model.Kinematic.(jointP).ALs; 

if ~isempty(lmNamesP{4})
    tmp=xyzLM(ALs,lmNamesP{4});
    if isstruct(tmp)
        center_endplateP_up=tmp.center;
    else
        center_endplateP_up=tmp;
    end
end

if ~isempty(jointS)
    lmNamesS=model.Kinematic.(jointS).ALs;
    tmp=xyzLM(ALs,lmNamesS{3});
    if isstruct(tmp)
        center_endplateS_down=tmp.center;
    else
        center_endplateS_down=tmp;
    end
    endPlateUp=xyzLM(ALs,lmNames{4});
    joint_center_d=(endPlateUp+center_endplateS_down)/2; 
else
    endPlateUp='';
    joint_center_d='';
end


VV=xyzLM(ALs,lmNames{1});
DV=xyzLM(ALs,lmNames{2});
endPlateDown=xyzLM(ALs,lmNames{3});
Q=BCS_Vertebra(L52C1,VV,DV,endPlateUp,endPlateDown,lmNames,ALs);

Orig=(endPlateDown+center_endplateP_up)/2;

if ~isempty(jointS)
    %--local coordinates
    v1=joint_center_d-Orig; joint_center_d=v1*Q;
end

if strcmpi(joint,'T01')
    SCR=xyzLM(ALs,lmNames{(end-1)});
    SCL=xyzLM(ALs,lmNames{end});
      v1=SCL-Orig; SCL=v1*Q;
      v1=SCR-Orig; SCR=v1*Q;
    Segment.JointCenterProximal=[Orig, 1];
    Segment.JointCenterDistal=[joint_center_d, 1];
    Segment.JointCenterDistal2=[SCL, 1];
    Segment.JointCenterDistal3=[SCR, 1];
    Segment.PointProximal=joint;
    Segment.PointDistal=jointS;
    Segment.PointDistal2='ClavicleJ_L';
    Segment.PointDistal3='ClavicleJ_R';
    Segment.Toisb=[Q(:,1) Q(:,2) Q(:,3) Orig';
        0  0  0    1];
elseif strcmpi(joint,'T04')
    VT04=xyzLM(ALs,lmNames{(1)});
    VT10=xyzLM(ALs,lmNames{end});
    v1=VT04-Orig; VT04=v1*Q;
    v1=VT10-Orig; VT10=v1*Q;
    Segment.JointCenterProximal=[Orig, 1];
    Segment.JointCenterDistal=[joint_center_d, 1];
    Segment.JointCenterDistal2=[VT04, 1];
    Segment.PointProximal=joint;
    Segment.PointDistal=jointS;
    Segment.PointDistal2='RibCage';
    Segment.Toisb=[Q(:,1) Q(:,2) Q(:,3) Orig';
        0  0  0    1];
elseif strcmpi(joint,'C2')
    center_endplate_up_C2=xyzLM(ALs,lmNames{end});
    v1 = center_endplate_up_C2-Orig; center_endplate_up_C2=v1*Q;
    Segment.JointCenterProximal=[Orig, 1];
    Segment.JointCenterDistal=[center_endplate_up_C2, 1];
    Segment.PointProximal=joint;
    Segment.PointDistal=jointS;
    Segment.Toisb=[Q(:,1) Q(:,2) Q(:,3) Orig';
        0  0  0    1];
else
    Segment.JointCenterProximal=[Orig, 1];
    Segment.JointCenterDistal=[joint_center_d, 1];
    Segment.PointProximal=joint;
    Segment.PointDistal=jointS;
    Segment.Toisb=[Q(:,1) Q(:,2) Q(:,3) Orig';
        0  0  0    1];
end

end