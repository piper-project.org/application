% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Humerus_R(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
EMR=xyzLM(ALs,lmNames{1});
ELR=xyzLM(ALs,lmNames{2});
Center_Humerus_R=xyzLM(ALs,lmNames{3});

GHR=Center_Humerus_R;
Center_elbowR=(EMR+ELR)/2;
%
axuHumerusR=zeros(3);
%Definition of Y
axuHumerusR(:,2)=GHR-Center_elbowR;
axuHumerusR(:,2)=axuHumerusR(:,2)/norm(axuHumerusR(:,2));
%
%Definition of X
dER= ELR-EMR;
axuHumerusR(:,1)=cross(axuHumerusR(:,2),dER');
axuHumerusR(:,1)=axuHumerusR(:,1)/norm(axuHumerusR(:,1));
%
%Definition of Z
axuHumerusR(:,3)= cross(axuHumerusR(:,1),axuHumerusR(:,2));
axuHumerusR(:,3)= axuHumerusR(:,3)/norm(axuHumerusR(:,3));
%
Q=axuHumerusR;
Orig=Center_Humerus_R;

%---local coordinates of joint centers
v1=Center_Humerus_R-Orig; Center_Humerus_R=v1*Q;
v1=Center_elbowR-Orig; Center_elbowR=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_elbowR, 1];
Segment.PointProximal='Shoulder_R';
Segment.PointDistal='Elbow_R';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end