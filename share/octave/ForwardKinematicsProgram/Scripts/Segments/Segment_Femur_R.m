% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Femur_R(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
FEMR=xyzLM(ALs,lmNames{1}); 
FELR=xyzLM(ALs,lmNames{2}); 
Center_Hip_R=xyzLM(ALs,lmNames{3}); 
Center_Patella_R = xyzLM(ALs,lmNames{5}); 

axuFemurR=zeros(3);
Center_Knee_R=(FEMR+FELR)/2;
%
%Definition of Y
axuFemurR(:,2)=Center_Hip_R-Center_Knee_R;
axuFemurR(:,2)=axuFemurR(:,2)/norm(axuFemurR(:,2));
%Definition of X
axuFemurR(:,1)= cross(axuFemurR(:,2),(FELR-FEMR)');
axuFemurR(:,1)= axuFemurR(:,1)/norm(axuFemurR(:,1));
%Definition of Z
axuFemurR(:,3)=cross(axuFemurR(:,1),axuFemurR(:,2));
axuFemurR(:,3)=axuFemurR(:,3)/norm(axuFemurR(:,3));

Orig=Center_Hip_R;
Q=axuFemurR;

%---local coordinates of joint centers
v1=Center_Hip_R-Orig; Center_Hip_R=v1*Q;
v1=Center_Knee_R-Orig; Center_Knee_R=v1*Q;
v1=Center_Patella_R-Orig; Center_Patella_R=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_Knee_R, 1];
Segment.JointCenterDistal2=[Center_Patella_R, 1];%
Segment.PointProximal='Hip_R';
Segment.PointDistal='TibioFemoral_R';
Segment.PointDistal2='PatelloFemoral_R';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end