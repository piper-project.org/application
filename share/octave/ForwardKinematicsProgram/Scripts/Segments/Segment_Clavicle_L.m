% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Clavicle_L(joint,ListofALs,model, ALs)
%
lmNames=model.Kinematic.(joint).ALs;
ACL=xyzLM(ALs,lmNames{1}); 
SCL=xyzLM(ALs,lmNames{2}); 
T8=xyzLM(ALs,lmNames{3});
C7=xyzLM(ALs,lmNames{4});
Center_Humerus_L=xyzLM(ALs,lmNames{5});

axuClavicleL=zeros(3);

% Definition of Z
axuClavicleL(:,3)=-ACL+SCL;
axuClavicleL(:,3)=axuClavicleL(:,3)/norm(axuClavicleL(:,3));
%
% Definition of X
axuClavicleL(:,1)=cross(axuClavicleL(:,3),(C7-T8)');
axuClavicleL(:,1)=axuClavicleL(:,1)/norm(axuClavicleL(:,1));
% Definition of Y
axuClavicleL(:,2)=cross(axuClavicleL(:,3),axuClavicleL(:,1));
axuClavicleL(:,2)=axuClavicleL(:,2)/norm(axuClavicleL(:,2));

Q=axuClavicleL;
Orig=SCL;
%---local coordinates of joint centers
v1=ACL-Orig; ACL=v1*Q;
v1=SCL-Orig; SCL=v1*Q;
v2=Center_Humerus_L-Orig; Center_Humerus_L=v2*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.PointProximal='ClavicleJ_L';
Segment.JointCenterDistal=[Center_Humerus_L, 1];
Segment.PointDistal='Shoulder_L';
Segment.JointCenterDistal2=[ACL, 1];
Segment.PointDistal2='Acromioclavicular_L';
Segment.JointCenterDistal3=[SCL, 1];
Segment.PointDistal3='Sternoclavicular';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end