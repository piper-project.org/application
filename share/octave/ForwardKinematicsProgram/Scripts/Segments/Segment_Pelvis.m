% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment = Segment_Pelvis(joint,jointS, model, ALs)
%Segment_Pelvis defines
%   - the BCS of the pelvis
%   - proximal and distal points in the BCS
%
body=model.Kinematic.(joint).BodyName;
lmNames=model.Kinematic.(joint).ALs;
lmNamesS=model.Kinematic.(jointS).ALs;
tmp=xyzLM(ALs,lmNamesS{3});
if isstruct(tmp)
    center_endplate_down_L5=tmp.center;
else
    center_endplate_down_L5=tmp;
end

Center_Hip_R=xyzLM(ALs,lmNames{1});
Center_Hip_L=xyzLM(ALs,lmNames{2});
Center_Vertebra_L5=xyzLM(ALs,lmNames{3});

Q = BCS_Pelvis(Center_Hip_R, Center_Hip_L,Center_Vertebra_L5);
Orig=(Center_Hip_R+Center_Hip_L)/2;
%-- local coordinates of other points
v1=Center_Hip_L-Orig; Center_Hip_L=v1*Q;
v1=Center_Hip_R-Orig; Center_Hip_R=v1*Q;
v1=Center_Vertebra_L5-Orig; Center_Vertebra_L5=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Center_Hip_R, 1];
Segment.JointCenterDistal2=[Center_Hip_L, 1];
Segment.JointCenterDistal3=[Center_Vertebra_L5, 1];
Segment.PointProximal=joint;
Segment.PointDistal='Hip_R';
Segment.PointDistal2='Hip_L';
Segment.PointDistal3=jointS;
Segment.Toisb=[Q(:,1) Q(:,2) Q(:,3) Orig';
    0  0  0    1];

end