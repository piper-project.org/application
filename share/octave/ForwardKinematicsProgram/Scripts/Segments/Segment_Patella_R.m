% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Patella_R(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
% Post_C=xyzLM(ALs,lmNames{4}); 
% Lat=xyzLM(ALs,lmNames{6}); 
% Med=xyzLM(ALs,lmNames{7});
% Ant_Pr=xyzLM(ALs,lmNames{end});

Ant_C=xyzLM(ALs,lmNames{9}); 
Lat=xyzLM(ALs,lmNames{6}); 
Med=xyzLM(ALs,lmNames{7});
Ant_Pr=xyzLM(ALs,lmNames{end});
Ant_Dist=xyzLM(ALs,lmNames{5});

axuPatellaR=zeros(3);
% % Definition of Z
% axuPatellaL(:,3)= -(Lat - Med);
% axuPatellaL(:,3)=axuPatellaL(:,3)/norm(axuPatellaL(:,3));
% %
% % Definition of X
% %
% axuPatellaL(:,1)=cross(Ant_Pr-Ant_C,Med-Ant_C);
% axuPatellaL(:,1)=axuPatellaL(:,1)/norm(axuPatellaL(:,1));
% %
% % Definition of Y
% axuPatellaL(:,2)=cross(axuPatellaL(:,3),axuPatellaL(:,1));
% axuPatellaL(:,2)=axuPatellaL(:,2)/norm(axuPatellaL(:,2));

% Definition of Z
axuPatellaR(:,3)= -(Lat - Med);
axuPatellaR(:,3)=axuPatellaR(:,3)/norm(axuPatellaR(:,3));
%
% Definition of X
%
axuPatellaR(:,1)=cross(Ant_Pr-Ant_Dist,(Lat - Med));
axuPatellaR(:,1)=axuPatellaR(:,1)/norm(axuPatellaR(:,1));
%
% Definition of Y
axuPatellaR(:,2)=cross(axuPatellaR(:,3),axuPatellaR(:,1));
axuPatellaR(:,2)=axuPatellaR(:,2)/norm(axuPatellaR(:,2));

Q=axuPatellaR;
Orig=Ant_C;
%Orig=ICR;

%---local coordinates of joint centers
% v1=Center_Patella_R-Orig; Center_Patella_R=v1*Q;
% v1=Fem_R-Orig; Fem_R=v1*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.JointCenterDistal=[Orig, 1];
Segment.PointProximal='PatelloFemoral_R';
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end