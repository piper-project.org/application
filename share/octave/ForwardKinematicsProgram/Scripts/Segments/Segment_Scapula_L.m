% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function Segment=Segment_Scapula_L(joint,ListofALs,model, ALs)

lmNames=model.Kinematic.(joint).ALs;
AA_L=xyzLM(ALs,lmNames{1}); 
TS_L=xyzLM(ALs,lmNames{2}); 
AI_L=xyzLM(ALs,lmNames{3});
Center_Humerus_L=xyzLM(ALs,lmNames{8});
AC_L = xyzLM(ALs,lmNames{9});

axuScapulaL=zeros(3);

% Definition of Z
axuScapulaL(:,3)=(AA_L-TS_L);
axuScapulaL(:,3)=axuScapulaL(:,3)/norm(axuScapulaL(:,3));
%
% Definition of X
axuScapulaL(:,1)=cross((AI_L-AA_L),(AI_L-TS_L));
axuScapulaL(:,1)=axuScapulaL(:,1)/norm(axuScapulaL(:,1));
% Definition of Y
axuScapulaL(:,2)=cross(axuScapulaL(:,3),axuScapulaL(:,1));
axuScapulaL(:,2)=axuScapulaL(:,2)/norm(axuScapulaL(:,2));

Q=axuScapulaL;
Orig=AC_L;

%---local coordinates of joint centers
v1=AC_L-Orig; AC_L=v1*Q;
v2=Center_Humerus_L-Orig; Center_Humerus_L=v2*Q;

Segment.JointCenterProximal=[Orig, 1];
Segment.PointProximal='Acromioclavicular_L';
% Segment.JointCenterDistal=[Center_Humerus_L, 1];
% Segment.PointDistal='Shoulder_L';
Segment.JointCenterDistal=[Orig, 1];
Segment.PointDistal='';
Segment.Toisb=[Q Orig'; 0,0,0,1];

end