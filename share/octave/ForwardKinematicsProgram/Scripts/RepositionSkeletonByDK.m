% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function MainLMsDrivingPosture = RepositionSkeletonByDK(sbj)
% RepositionSkeletonByDK drives the kinematic model to a target posture using defined joint angles
%Input:
% - Piper articumated skeleton model in txt (user-defined)
% - Joint angles of the target posture (*.csv)
% Output:
% - Anatomical landmarks coordinates in local and global coordinate systems
%   of the target posture (sbj.ScannedPosture.LMs.csv)

Subject = sbj;
%-- define path
Scripts = [cd('..'), '\'];
Path_Main = [pwd(),'\'];
cd(Scripts)
Path_Out=[Path_Main 'Output\'];
Path_Model=[Path_Main 'Input\ModelStructure\'];

%--add path
addpath([Scripts 'Segments\']);
addpath([Scripts 'LCS\']);
addpath([Scripts 'Tools\']);

% Steps 1 & 2: read the kinematic model and body structure
%--read the kinematic model
load('-text',[Path_Model 'PiperModel.txt'],'model')
Right_Side = 0;

ListOfJoints = fieldnames(model.Kinematic);
%--load the dimensions of the articulated model
file=[Path_Out Subject '\' Subject '.Dimensions.mat'];
load(file,'dim');

file=[Path_Out Subject '\' Subject '.Body.mat'];
load(file,'ske');

%--read a new posture
file=[Path_Model 'DrivingPosture.JointAngles.csv'];
[tempPos,Aheading,A]=myCSVread(file,1,',');
[temp,Aheading,A]=myCSVread(file,3,',');

angles.(tempPos{1,1}).x = str2num(tempPos{1,2});
angles.(tempPos{1,1}).y = str2num(tempPos{1,3});
angles.(tempPos{1,1}).z = str2num(tempPos{1,4});

for i = 1:size(temp,1)
    angles.(temp{i,1}).rx = str2num(temp{i,2});
    angles.(temp{i,1}).ry = str2num(temp{i,3});
    angles.(temp{i,1}).rz = str2num(temp{i,4});
end

ToG=Positioning(model,dim,angles);

file=[Path_Out Subject '\' Subject '.DrivingPosture.LMs.csv'];
LMs_Final = LandMarksPosition(dim,ske,ToG,file);
copyfile(file,[Path_Out,'\LMs']);