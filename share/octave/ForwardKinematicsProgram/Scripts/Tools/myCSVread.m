% Copyright (C) 2017 University of Southampton
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Christophe Lecomte (University of Southampton)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function [Adata,Aheading,A]=myCSVread(fileName,nHeadRows,separator,alphaBnd,isnumData)
%
% [Adata,Aheading,A]=myCSVread(fileName,nHeadRows,separator);
% [Adata,Aheading,A]=myCSVread(fileName,nHeadRows,separator,alphaBnd,isnumData);
%
% Reads a comma separated (CSV) file whose first nHeadRows rows are made
%    of header titles.  Data can be numeric or alphanumeric.
%
% Output: Adata, data stored in the input file, in the format of a cell
%           array of strings.
%           Adata(4,7) contains the 7h item of information stored in
%           the 4th row of data (4+nHeadRows)th row of the input file.
%         Aheading, items in the nHeadRows first rows of the input file,
%           in the format of cell array of strings.
%           Aheading(2,5) contains the 5th item on the second heading row.
%         A, is a concatenated cell array made of Aheading and Adata.
%           A=[Aheading;Adata];
% Input: fileName, name of the input file (string of characters).
%        nHeadRows, number of rows containing the data headers. [Default=1]
%        separator, separator used to separated items of a row. [Def.='\,']
%        alphaBnd, delimitor of chain of characters if any. [Default='"']
%        isNumData, flag to indicate if the data is numeric, in which case,
%           Adata is returned as an array of numerical values. [Def.=0]
%
% Christophe Lecomte, Aug.2016
% Work funded by the EU FP7 PIPER project
% University of Southampton
%

%
if nargin<5
    isnumData=0;
end
%
if nargin<4
    alphaBnd='"';
end
if isempty(alphaBnd)
    alphaBnd='"';
end
%
if nargin<3
    separator='\,';
end
if isempty(separator)
    separator='\,';
end
%
if nargin<2
    nHeadRows=1;
end
if isempty(nHeadRows)
    nHeadRows=1;
end
%
fid=fopen(fileName, 'r');
%
A=cell(1,0);
for j=1:nHeadRows
    tline = fgetl(fid);
    row=regexp(tline,separator,'split');
    row=checkAlpha(row,alphaBnd,separator);
    A(j,1:length(row))=row;
end
%
j=nHeadRows;
while(~feof(fid))
    j=j+1;
    tline = fgetl(fid);
    row=regexp(tline,separator,'split');
    %    if ischar(row{1})
    row=checkAlpha(row,alphaBnd,separator);
    %    end
    A(j,1:length(row))=row;
end
for j=1:size(A,1)
    for k=1:size(A,2)
        if isnumeric(A{j,k})
            A{j,k}=num2str(A{j,k});
        end
    end
end
%
fclose(fid);
%
% suppress the double quotes
A=regexprep(A,'^"|"$','');
%
Aheading=A(1:nHeadRows,:);
Adata=A(nHeadRows+1:end,:);
%
if isnumData
    Adata=cell2mat(cellfun(@str2double,Adata,'UniformOutput',0));
end
%
end
%
% ===================================================================
%
function row=checkAlpha(row,alphaBnd,separator)
%
separator=strrep(separator,'\','');
%
j=1;
while j<length(row)
    elem=row{j};
    if length(elem)>0
        if strcmp(elem(1),alphaBnd)
            distAlphaBnd=diff(findstr(elem,alphaBnd));
            countBeg=1;
            if length(distAlphaBnd)>0
                testChar=1;
            else
                testChar=0;
            end
            while testChar
                if distAlphaBnd(countBeg)==1  % count the number of
                    %  sequential bound characters.  This is to support
                    %  the case where double quotes, ", are included in
                    %  the tring of characters.
                    countBeg=countBeg+1;
                    if countBeg>length(distAlphaBnd)
                        testChar=0;  % exhausted the string of chars
                    end
                else
                    testChar=0;  % completed the count of consecutive chars
                end
            end
            if mod(countBeg,2)  % only check if the number of consecutive
                % bound characters (such as ") is odd.
                % ( This is because
                % actual double quotes are duplicated in the string of
                % characters, contrary to the double quotes added at the
                % beginning and end of the strings to indicates that
                % commas are part of them. )
                continueCheckEnd=1;
                while continueCheckEnd
                    % concatenate with the next element until the value is alphanumeric
                    %      (until the last element delimitates an alphanumeric value)
                    countEnd=length(elem);
                    if strcmp(elem(end),alphaBnd)
                        while strcmp(elem(countEnd-1),alphaBnd)
                            countEnd=countEnd-1;
                        end
                        if ~mod(length(elem)-countEnd,2)
                            % if there is an odd number of consecutive
                            %    characters at the end of the string, stop
                            %    merging...
                            continueCheckEnd=0;
                        end
                    end
                    %
                    if continueCheckEnd
                        % prepare for next step by concatenating
                        elem=[elem,separator,row{j+1}];
                        % shorten the list after concatenation
                        row(j+1:end-1)=row(j+2:end);
                        row=row(1:end-1);
                    end
                    %
                end
                row{j}=elem(2:end-1);
            end
        end
    end
    j=j+1;
end
%
end
%
% ===================================================================
%