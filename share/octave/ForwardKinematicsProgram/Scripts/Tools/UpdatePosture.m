% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function angles=UpdatePosture(CurrentListOfSegments,angles,Skeleton,Q0)
%UpdatePosture.m update posture from local coordinate systems
%

r2d=180/pi;
for iSeg=1:length(CurrentListOfSegments)
    SegName=CurrentListOfSegments{iSeg};
    JointName=Skeleton.(SegName).PointProximal;
    Q=Skeleton.(SegName).Toisb(1:3,1:3); t=Skeleton.(SegName).Toisb(1:3,4);

    if strcmp(JointName,'ROOT')
        angles.ROOT.x=t(1);angles.ROOT.y=t(2);angles.ROOT.z=t(3);
    end
    R=Q0'*Q;ang=weuler(R)*r2d;
    angles.(JointName).rx=ang(3);angles.(JointName).ry=ang(2);angles.(JointName).rz=ang(1);
    Q0=Q;
end 
end