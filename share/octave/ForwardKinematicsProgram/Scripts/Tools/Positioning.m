% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function pos = Positioning(model,dim,angles)

kinematic=model.Kinematic;
joints=fieldnames(kinematic);
num_joint=length(joints);

for i_joint=1:num_joint
    pos.(joints{i_joint})=zeros(4,4);
end

for i_joint=1:num_joint
    if isempty(kinematic.(joints{i_joint}).ref_joint)
        j_pr(:,:)=eye(3);
    else
        j_pr=pos.(kinematic.(joints{i_joint}).ref_joint)(1:3,1:3);
    end
    anglestmp=angles.(joints{i_joint});
    trm=eye(3);
    for i_rot = 1:3
        if i_rot == 1
            tmp=rz(pi/180*anglestmp.rz);
        end
        if i_rot == 2
            tmp=ry(pi/180*anglestmp.ry);
        end
        if i_rot == 3
            tmp=rx(pi/180*anglestmp.rx);
        end        
        trm=trm*tmp;
        pos.(joints{i_joint})(1:3,1:3)=j_pr(1:3,1:3)*trm;
        if isempty(kinematic.(joints{i_joint}).ref_joint)
            tmp=angles.(joints{i_joint});
            trj=[tmp.x tmp.y tmp.z];
        else
            trj=pos.(kinematic.(joints{i_joint}).ref_joint)(1:3,4)'+...
                (j_pr(1:3,1:3)*dim.(joints{i_joint})')';
        end
        reptmp=pos.(joints{i_joint});
        pos.(joints{i_joint})(1:4,1:4)=[reptmp(1:3,1:3) trj' ;[0 0 0 1]];
    end
end 