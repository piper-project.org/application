% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function LM=xyzLM(LMs,Name)
% xyzLM.m coordinates of a LM

ListofLMs={LMs.NAME};
i=find(strcmpi(ListofLMs,Name));

if ~isempty(i)
    if length(LMs(i).NAME)>14&&strcmpi(LMs(i).NAME(1:15),'center_endplate')
        LM.center=[LMs(i).X LMs(i).Y LMs(i).Z];
        LM.normal=[LMs(i).nX LMs(i).nY LMs(i).nZ];
    else
        LM=[LMs(i).X LMs(i).Y LMs(i).Z];
    end
else
    LM=[];
end

end