% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function LMs=LandMarksPosition(dim,ske,ToG,file)
%LandMarksPosition.m calculate the position of the landmarks
%   
%   Xuguang WANG, IFSTTAR/UCBL
%

%list of landmarks
ListLMs={dim.Marker.Name};
formatspec = '%s,%s,%f,%f,%f,%f,%f,%f,\r\n';
fileID = fopen(file,'w');
fprintf (fileID, 'Landmark,Segment,LCS_x,LCS_y,LCS_z,GCS_x,GCS_y,GCS_z, \r\n');
for i=1:length(ListLMs)
    SegName=dim.Marker(i).ShortBodyName;
    JointName=ske.(SegName).PointProximal;
    To=ToG.(JointName);Q=To(1:3,1:3);t=To(1:3,4);
    v=dim.Marker(i).LocalPosition;
    v1=v*Q'+t'; 
    LMs(i).Name=ListLMs{i};
    LMs(i).BodyName=dim.Marker(i).BodyName;
    LMs(i).LCS_x=v(1);LMs(i).LCS_y=v(2);LMs(i).LCS_z=v(3);
    LMs(i).GCS_x=v1(1);LMs(i).GCS_y=v1(2);LMs(i).GCS_z=v1(3);
    temp = {LMs(i).Name,  LMs(i).BodyName, LMs(i).LCS_x, LMs(i).LCS_y, LMs(i).LCS_z, ...
        LMs(i).GCS_x, LMs(i).GCS_y, LMs(i).GCS_z};
    fprintf (fileID, formatspec, temp{:});
end
fclose('all');    
end