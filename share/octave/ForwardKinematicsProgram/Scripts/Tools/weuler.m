% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function res=weuler(r,angref)

%WEULER.m detremine les trois angles Euler a partir de la matrice de rotation.
%       deduit les angles d'orientation psi, teta et phi du referentiel
%       chainon iref a partir de la matrice des cosinus directeurs       
%       on suppose que les rotations s'effectuent dans l'ordre 
%       psi (autour de z), teta (y) puis phi (x) quand on passe du
%       referentiel general au referentiel chainon
%
%	angref(3): les trois angles referentiels psi,teta,phi
%
%	[psi,teta,phi]=weuler(r,angref)
%

if nargin==1
    angref=[0 0 0];
end


epsi=1.e-6; 

%---valeurs possibles pour teta
st2=-r(3,1);
if abs(st2)<epsi 
	st2=0;
	teta2(1)=0;
	teta2(2)=pi;
elseif abs(abs(st2)-1)<epsi
	teta2(1)=pi/2;
	teta2(2)=-pi/2;
else
	teta2(1)=asin(st2);
	q=abs(st2);
	aa=st2/q;
	teta2(2)=pi*aa-teta2(1);
end

%---on calcule les solutions qui en decoulent
for i=1:2
	ct2=cos(teta2(i));
	if abs(ct2)<epsi
		phi2(i)=0;
		st1=-r(1,2);
           	ct1=r(2,2);
                if abs(st1)<epsi
			psi2(i)=(1-ct1)*pi/2;
		elseif abs(ct1)<epsi 
			psi2(i)=st1*pi/2;
           	else
                	psi2(i)=atan2(st1,ct1);
           	end
      	else
	% ------------       pour psi
         	st1= r(2,1)/ct2;
         	ct1=r(1,1)/ct2;
                if abs(st1)<=epsi 
			psi2(i)=(1-ct1)*pi/2;
                elseif abs(ct1)<=epsi 
			psi2(i)=st1*pi/2;
         	else
                	psi2(i)=atan2(st1,ct1);
         	end
	%------------        et pour phi
           	st3=r(3,2)/ct2;
           	ct3=r(3,3)/ct2;
                if abs(st3)<epsi
			phi2(i)=(1-ct3)*pi/2;
		elseif abs(ct3)<epsi 
			phi2(i)=st3*pi/2;
           	else
                	phi2(i)=atan2(st3,ct3);
           	end
      end
end

%----------------------on retient la solution du moindre
%                         deplacement angulaire global
for i=1:2
   	%som(i)=abs(phi2(i))+abs(teta2(i))+abs(psi2(i));
	som(i)=abs(phi2(i)-angref(3))+abs(teta2(i)-angref(2))+...
		abs(psi2(i)-angref(1));
end
isol=1;
if som(2)<som(1)
	isol=2;
end
phi=phi2(isol);
teta=teta2(isol);
psi=psi2(isol);
res=[psi, teta, phi];
