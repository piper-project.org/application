% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function MainLMsScannedPosture = CreatePersonalizedPiperModel(sbj)
%CreatePersonalizedPiperModel.m creates a personalized Piper model
%
% 1) Load the skeketon in a global coordinate system
% 2) Define the local coordinate systems (LCS) of each body segment accoording
%    to ISB
%    2.1) Calculate the local coordinates of the segment points
%    2.2) Save the dimensions of the articulated model (dim)
% 3) Calculate the angles along the joint coordinate system (JCS) according to ISB
%    3.1) Save the angles in *.csv
% 4) Position the skelon in the scan posture
%    4.1) Save the anatomical landmarks (.csv) in local and global coordinate systems
%    for the scan posture
% Input:
% - Piper articumated skeleton model in txt (user-defined)
% - Anatomical landmarks in .csv format
% Output:
% - Joints center coordinates (sbj.JointCenters.csv)
% - Angles of the scan posture (sbj.ScannedPosture.JointAngles.csv)
% - Anatomical landmarks coordinates in local and global coordinate systems
%   of the scan posture (sbj.ScannedPosture.LMs.csv)
%
%   Authors: Xuguang Wang (UCBL/IFSTTAR), Guillaume Pacquaut (UCBL)
%   Creation: 2016-12-19
%
Subject = sbj;
%-- define path
Scripts = [cd('..'), '\'];
Path_Main = [pwd(),'\'];
cd(Scripts)
Path_Out=[Path_Main 'Output\'];
Path_Model=[Path_Main 'Input\ModelStructure\'];
Path_In=[Path_Main 'Input\DataLMs\'];

%--add path
addpath([Scripts 'Segments\']);
addpath([Scripts 'LCS\']);
addpath([Scripts 'Tools\']);

%--read the kinematic model
load('-text',[Path_Model 'PiperModel.txt'],'model')
Right_Side = 1;

%---define the body structure
ListOfJoints = fieldnames(model.Kinematic); %short joint name

%--read landmarks
file = [Path_In, Subject,'_LMsSym.csv'];
[temp,Aheading,A]=myCSVread(file,1,',');
for i =1:length(temp)
  tmp{i,3} = str2num(temp{i,3});
  tmp{i,4} = str2num(temp{i,4});
  tmp{i,5} = str2num(temp{i,5});
end
ALs=struct('NAME', temp(:,1),  'BONE', temp(:,2), 'X', tmp(:,3),...
   'Y', tmp(:,4),'Z', tmp(:,5));

ListofALs={ALs.NAME};
BoneALs={ALs.BONE};

%--associate ALs to the corresponding body segment
SegALs=cell(length(BoneALs),1);
for i=1:length(BoneALs)
    kompt=0;
    while isempty(SegALs{i})&&kompt<length(ListOfJoints)
        kompt=kompt+1;
        Joint=(ListOfJoints{kompt});
        Body=model.Kinematic.(Joint).BodyName;
        if ~isempty(find(strcmpi(model.Kinematic.(Joint).ShortBodyName,BoneALs{i})))
            SegALs{i}=Body;
        end
    end
end


%--Step 1
for i=1:length(ListOfJoints)
    tmp=model.Kinematic.(ListOfJoints{i}).Bones;
    if ~iscell(tmp)
        if strcmpi(model.name,'ghbm')
            ListOfBones={model.Kinematic.(ListOfJoints{i}).Bones};
        else
            ListOfBones={model.Kinematic.(ListOfJoints{i}).BodyName};
        end
    else
        ListOfBones=tmp;
    end;
    for j=1:length(ListOfBones)
        if length(ListOfBones)>1
            BoneCode=model.Kinematic.(ListOfJoints{i}).Codes{j};
        else
            BoneCode=model.Kinematic.(ListOfJoints{i}).Codes;
        end
    end
end
Q0=eye(3); Orig=[0 0 0];

ListOfVertebra={'Lumbar_L5','Lumbar_L4','Lumbar_L3','Lumbar_L2','Lumbar_L1',...
    'Thoracic_T12','Thoracic_T11',...
    'Thoracic_T10','Thoracic_T09','Thoracic_T08','Thoracic_T07','Thoracic_T06','Thoracic_T05',...
    'Thoracic_T04','Thoracic_T03','Thoracic_T02','Thoracic_T01','Cervical_C7',...
    'Cervical_C6','Cervical_C5','Cervical_C4','Cervical_C3','Cervical_C2'};

%Step 2: calculate the local coordinates of the mesh points for each segment
%disp('Calculate LCS...');
VL5=xyzLM(ALs,model.Kinematic.L5.ALs{1});
VC1=xyzLM(ALs,model.Kinematic.C1.ALs{1});
L52C1=VC1-VL5; L52C1=L52C1/norm(L52C1);

for i=1:length(ListOfJoints)
    Joint=ListOfJoints{i};% disp(Joint); %current joint
    Body=model.Kinematic.(Joint).BodyName; %disp(Body);
    
    if i<length(ListOfJoints)
        JointS=ListOfJoints{i+1};
    else
        JointS='';
    end
    if strcmpi(Body,'pelvis')
        
        ske.(Body)=Segment_Pelvis(Joint,JointS,model,ALs);
    elseif ismember(Body,ListOfVertebra);
        JointP= model.Kinematic.(Joint).ref_joint;
        
        ske.(Body)=Segment_Vertebra(L52C1,Joint,JointP,JointS,model,ALs,ListofALs);
        BodyP=model.Kinematic.(JointP).BodyName;
        if i==2 % for the 2nd joint from the root (L5)
            dim.(Joint)=ske.(BodyP).JointCenterDistal3(1:3); %only for the spine
        else
            dim.(Joint)=ske.(BodyP).JointCenterDistal(1:3);
        end
    else
        JointP= model.Kinematic.(Joint).ref_joint;
        BodyP=model.Kinematic.(JointP).BodyName;
        
        ske.(Body)=feval(['Segment_' Body],Joint,ListofALs,model,ALs);
        if strcmpi(Joint,'Hip_L')
            dim.(Joint)=ske.(BodyP).JointCenterDistal2(1:3);
        elseif strcmpi(Joint,'ClavicleJ_L')|| strcmpi(Joint,'Acromioclavicular_L') ...
                || strcmpi(Joint,'PatelloFemoral_L') || strcmpi(Joint,'RibCage')...
                || strcmpi(Joint,'PatelloFemoral_R') || strcmpi(Joint,'Acromioclavicular_R')
            dim.(Joint)=ske.(BodyP).JointCenterDistal2(1:3);
        elseif strcmpi(Joint,'ClavicleJ_R') || strcmpi(Joint,'Sternoclavicular')
            dim.(Joint)=ske.(BodyP).JointCenterDistal3(1:3);
        else
            dim.(Joint)=ske.(BodyP).JointCenterDistal(1:3);
        end
    end
end
if exist([Path_Out, Subject],'dir')==0;
    mkdir([Path_Out, Subject]);
end
if exist([Path_Out, '\LMs'],'dir')==0;
    mkdir([Path_Out, '\LMs']);
end
%---ALs used for postural driving by IK
k=0;
for i=1:length(ListofALs)
    if ~isempty(SegALs{i})
        k=k+1;
        dim.Marker(k).Name=(ListofALs{i});
        dim.Marker(k).ShortBodyName=SegALs{i};
        dim.Marker(k).BodyName=BoneALs{i};
        dim.Marker(k).Type='user';
        %--local coordinates
        To=ske.(SegALs{i}).Toisb; Q=To(1:3,1:3); Orig=To(1:3,4);
        v1=[ALs(i).X ALs(i).Y ALs(i).Z];
        v1=v1-Orig'; LP=v1*Q;
        dim.Marker(k).LocalPosition=LP;
    end
end
%--save defaut dimension
k=0;
for ijt=1:length(ListOfJoints)
    if strcmp((ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).PointDistal), '')==0;
        if isfield( ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName),'PointDistal3')==1
            k=k+1;
            JointCenters(k).Name=['Center_',ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).PointDistal3];
            JointCenters(k).ShortBodyName=model.Kinematic.(ListOfJoints{ijt}).ShortBodyName;
            JointCenters(k).BodyName=model.Kinematic.(ListOfJoints{ijt}).BodyName;
            JointCenters(k).Type='user';
            JointCenters(k).LocalPosition=ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).JointCenterDistal3(1:3);   %!check here
        end
        if isfield( ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName),'PointDistal2')==1
            k=k+1;
            JointCenters(k).Name=['Center_',ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).PointDistal2];
            JointCenters(k).ShortBodyName=model.Kinematic.(ListOfJoints{ijt}).ShortBodyName;
            JointCenters(k).BodyName=model.Kinematic.(ListOfJoints{ijt}).BodyName;
            JointCenters(k).Type='user';
            JointCenters(k).LocalPosition=ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).JointCenterDistal2(1:3);   %!check here
        end
        k=k+1;
        JointCenters(k).Name=['Center_',ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).PointDistal];
        JointCenters(k).ShortBodyName=model.Kinematic.(ListOfJoints{ijt}).ShortBodyName;
        JointCenters(k).BodyName=model.Kinematic.(ListOfJoints{ijt}).BodyName;
        JointCenters(k).Type='user';
        %--local coordinates
        JointCenters(k).LocalPosition=ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).JointCenterDistal(1:3);   %!check here
    end
    k=k+1;
    %Proximal
    JointCenters(k).Name=['Center_',ske.(model.Kinematic.(ListOfJoints{ijt}).BodyName).PointProximal];
    JointCenters(k).ShortBodyName=model.Kinematic.(ListOfJoints{ijt}).ShortBodyName;
    JointCenters(k).BodyName=model.Kinematic.(ListOfJoints{ijt}).BodyName;
    JointCenters(k).Type='user';
    %--local coordinates
    JointCenters(k).LocalPosition=[0,0,0];   %!check here
end
file=[Path_Out Subject '\' Subject '.JointCenters.csv'];
formatspec = '%s,%s,%f,%f,%f,\r\n';
fileID = fopen(file,'w');
fprintf (fileID, 'JointCenter,Segment,LCS_x,LCS_y,LCS_z, \r\n');
for i=1:length(JointCenters)
    temp = {JointCenters(i).Name,  JointCenters(i).ShortBodyName, ...
        JointCenters(i).LocalPosition(1), JointCenters(i).LocalPosition(2), ...
        JointCenters(i).LocalPosition(3)};
    fprintf (fileID, formatspec, temp{:});
end
fclose('all'); 

file=[Path_Out Subject '\' Subject '.Dimensions.mat'];
save(file,'dim','-v6');

file=[Path_Out Subject '\' Subject '.Body.mat'];
save(file,'ske','-v6');

%--read the zero posture
file=[Path_Model 'ZeroJointAngles.txt'];
formatspec = '%s %f %f %f ';
fid = fopen(file);
tempPos = textscan(fid, formatspec, 1, 'Delimiter',' ','HeaderLines',1);
temp = textscan(fid, formatspec, 'Delimiter',' ','HeaderLines',1);
angles.(tempPos{1,1}{1}).x = tempPos{1,2}(1);
angles.(tempPos{1,1}{1}).y = tempPos{1,3}(1);
angles.(tempPos{1,1}{1}).z = tempPos{1,4}(1);
for i = 1:length(temp{1})
    angles.(temp{1,1}{i}).rx = temp{1,2}(i);
    angles.(temp{1,1}{i}).ry = temp{1,3}(i);
    angles.(temp{1,1}{i}).rz = temp{1,4}(i);
end
fclose(fid);
r2d=180/pi;

% %Step 3: calculate the joint angles of the scan posture
ListOfSegments=fieldnames(ske);
% %--right lower limb
if Right_Side == 1
    ListOfSegments={'Pelvis','Femur_R','Tibia_R'};
    angles=UpdatePosture(ListOfSegments,angles,ske,Q0);
    angles=UpdatePosture({'Patella_R'},angles,ske,ske.Femur_R.Toisb(1:3,1:3));
end
% % %--left lower limb
ListOfSegments={'Pelvis','Femur_L','Tibia_L'};
angles=UpdatePosture(ListOfSegments,angles,ske,Q0);
angles=UpdatePosture({'Patella_L'},angles,ske,ske.Femur_L.Toisb(1:3,1:3));

ListOfSegments={'Pelvis','Lumbar_L5','Lumbar_L4','Lumbar_L3','Lumbar_L2','Lumbar_L1',...
    'Thoracic_T12','Thoracic_T11',...
    'Thoracic_T10','Thoracic_T09','Thoracic_T08','Thoracic_T07','Thoracic_T06','Thoracic_T05',...
    'Thoracic_T04','Thoracic_T03','Thoracic_T02','Thoracic_T01','Cervical_C7',...
    'Cervical_C6','Cervical_C5','Cervical_C4','Cervical_C3','Cervical_C2'};

angles=UpdatePosture(ListOfSegments,angles,ske,Q0);

Q0=ske.Cervical_C2.Toisb(1:3,1:3);
ListOfSegments={'Cervical_C1','Skull','Mandible'};
angles=UpdatePosture(ListOfSegments,angles,ske,Q0);

if Right_Side == 1
    % % %--right upper limb
    Q0=ske.Thoracic_T01.Toisb(1:3,1:3);
    ListOfSegments={'Clavicle_R','Humerus_R','Ulna_R'};
    angles=UpdatePosture(ListOfSegments,angles,ske,Q0);
    angles=UpdatePosture({'Scapula_R'},angles,ske,ske.Clavicle_R.Toisb(1:3,1:3));
end
% % %--left upper limb
Q0=ske.Thoracic_T01.Toisb(1:3,1:3);
ListOfSegments={'Clavicle_L','Humerus_L','Ulna_L'};
angles=UpdatePosture(ListOfSegments,angles,ske,Q0);

angles=UpdatePosture({'Scapula_L'},angles,ske,ske.Clavicle_L.Toisb(1:3,1:3));
angles=UpdatePosture({'Sternum'},angles,ske,ske.Clavicle_L.Toisb(1:3,1:3));
angles=UpdatePosture({'RibCage'},angles,ske,ske.Thoracic_T04.Toisb(1:3,1:3));

%--save scan posture angles
file=[Path_Out Subject '\' Subject '.ScannedPosture.JointAngles.csv'];
formatspec = '%s,%f,%f,%f,\r\n';
fid = fopen(file, 'w');
fprintf(fid, 'Segment, x, y, z, \r\n');
temp = {ListOfJoints{1}, angles.(ListOfJoints{1}).x, angles.(ListOfJoints{1}).y, angles.(ListOfJoints{1}).z};
fprintf(fid, formatspec, temp{:});
fprintf(fid, 'Segment, rx, ry, rz, \r\n');
for i = 1:length(ListOfJoints)
    temp = {ListOfJoints{i}, angles.(ListOfJoints{i}).rx, angles.(ListOfJoints{i}).ry, angles.(ListOfJoints{i}).rz};
    fprintf (fid, formatspec, temp{:});
end
fclose(fid);

%Step 4: position the skeleton model in the scan posture
%--calculate 4*4 passage matrice for the present model, which is used to
%put all local coordinates of skeleton into global coordinate system

ToG=Positioning(model,dim,angles);

file=[Path_Out Subject '\' Subject '.ScannedPosture.LMs.csv'];
LMs_Final = LandMarksPosition(dim,ske,ToG,file);
copyfile(file,[Path_Out,'\LMs']);
