% Copyright (C) 2017 UCBL-Ifsttar
% This file is part of the PIPER Framework.
% Version: 1.0.0
% 
% The PIPER Framework is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 2 of the License, or (at
% your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details. You should have received a copy
% of the GNU General Public License along with the PIPER Framework.
% If not, see <http://www.gnu.org/licenses/>.
% 
% Contributors include Xuguang Wang, Ilias Theodorakos, Guillaume Pacquaut
% (UCBL-Ifsttar)
% 
% This work has received funding from the European Union Seventh Framework
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
% 
function FormatFiles = SymLMs(sbj)
Subject =  sbj;

%-- define path
Scripts = [cd('..'), '\'];
Path_Main = [pwd(),'\'];
cd(Scripts)
Path_In=[Path_Main 'Input\DataLMs\'];
addpath([Scripts 'Tools\']);

%--read landmarks
file = [Path_In, Subject,'_lm.csv'];
[temp,Aheading,A]=myCSVread(file,1,',');
   
for i = 1: size(temp,1)
    tmp = strsplit(temp{i,2},'_');
    if any(ismember(tmp, 'RibCage')) == 1
        temp{i,2} = 'Left_RibCage';
    elseif any(ismember(tmp, 'fibula')) == 1
        temp{i,2} = strrep(temp{i,2}, 'fibula', 'tibia');
    elseif any(ismember(tmp, 'radius')) == 1
        temp{i,2} = strrep(temp{i,2},'radius', 'ulna');
    elseif any(ismember(tmp, 'hip')) == 1 || any(ismember(tmp, 'Sacrum')) == 1
        temp{i,2} = 'Pelvic_skeleton';
    end
end
index = 0;
for i = 1: size(temp,1)
    tmp = strsplit(temp{i,2},'_');
    if any(ismember(tmp, 'Left')) == 1
        index = index +1;
        Right_Side{index,1} = strrep(temp{i,1}, 'left', 'right');
        Right_Side{index,1} = strrep(Right_Side{index,1}, 'Left', 'Right');
        Right_Side{index,1} = strrep(Right_Side{index,1}, '_L', '_R');
        Right_Side{index,1} = strrep(Right_Side{index,1}, 'PatellaL_', 'PatellaR_');
        Right_Side{index,2} = strrep(temp{i,2}, 'Left', 'Right');
        Right_Side{index,3} = num2str(-str2num(temp{i,3}));
        Right_Side{index,4} = num2str(str2num(temp{i,4}));
        Right_Side{index,5} = num2str(str2num(temp{i,5}));
    end
end

% for ii = 1:size(temp,1)
%     hold on
%     plot3(str2num(temp{ii,3}),str2num(temp{ii,4}),str2num(temp{ii,5}),'r+')
%     X(ii) = str2num(temp{ii,3});
%     Y(ii) = str2num(temp{ii,4});
%     Z(ii) = str2num(temp{ii,5});
% end
% scatter3(X, Y, Z,'r')

temp = temp (:,1:5);
temp = [temp; Right_Side];

for i = 1: size(temp,1)
    tmp = strsplit(temp{i,2},'_');
    if any(ismember(tmp, 'RibCage')) == 1
        temp{i,2} = 'RibCage';
    end
end

file = strrep(file,'_lm','_LMsSym');
formatspec = '%s,%s,%f,%f,%f\r\n';
fid = fopen(file,'w');
fprintf(fid, 'NAME,BONE,X,Y,Z\r\n');
for i = 1:size(temp,1)
    writedata = {temp{i,1}, temp{i,2}, str2num(temp{i,3}), ...
        str2num(temp{i,4}), str2num(temp{i,5})};
    fprintf(fid, formatspec, writedata{:});
end
fclose('all');