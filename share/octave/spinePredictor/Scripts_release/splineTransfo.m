function Qtransf = splineTransfo(sL5,sC7,sC1,tL5,tC7,tC1,htl,hc,l,thetac,weight,posture_ref, posture_ini,posture_target,TransformationParam)
% Define a shape (based on the posture_ini and posture_target) for the
% whole spine from a reference posture. It transforms the reference spline
% (spine in the reference posture) in a spline into the initial posture, then
% it transforms this spline into a spline in target posture.
% sL5, sC7, sC1, tL5, tC7 and tC1 are the parameters of the original standing
% spline. htl is the distance between L5 and C7. hc is the distance between C7 and C1.
% thetac is the angle between L5-C7 and C7-C1. l is the length of the
% original spine.
% weight = completion of the transformation between the initial posture and
% the final posture (0 means no transformation. 1 means total
% transformation).
% posture_ref = ID of the reference posture (found in the transformationParameters.xlsx file).
% posture_ini = ID of the initial posture (found in the transformationParameters.xlsx file).
% posture_target = ID of the initial posture (found in the transformationParameters.xlsx file).
% TransformationParam = cell array of the transformation parameters
% obtained from the transformationParameters.xlsx file.
% The dh, dtheta, ds* and dt* come from analysis on nine healthy volunteers.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

N = 1000;

%% Transformation from the reference posture to initial posture
dsL5 = TransformationParam(1,posture_ini) - TransformationParam(1,posture_ref);
dtL5 = (TransformationParam(2,posture_ini) - TransformationParam(2,posture_ref))/TransformationParam(2,posture_ref); % relative to the original L5 tension
dsC7 = TransformationParam(3,posture_ini) - TransformationParam(3,posture_ref);
dtC7 = (TransformationParam(4,posture_ini) - TransformationParam(4,posture_ref))/TransformationParam(4,posture_ref); % relative to the original C7 tension
dsC1 = TransformationParam(5,posture_ini) - TransformationParam(5,posture_ref);
dtC1 = (TransformationParam(6,posture_ini) - TransformationParam(6,posture_ref))/TransformationParam(6,posture_ref); % relative to the original C1 tension
dhtl = (TransformationParam(7,posture_ini) - TransformationParam(7,posture_ref))/TransformationParam(7,posture_ref); % relative to the height
dhc = (TransformationParam(8,posture_ini) - TransformationParam(8,posture_ref))/TransformationParam(8,posture_ref); % relative to the height
dthetac = TransformationParam(9,posture_ini) - TransformationParam(9,posture_ref);
dtheta = (TransformationParam(10,posture_ini) - TransformationParam(10,posture_ref))*pi/180;


%% Transformation from initial posture to target posture
dsL5 = dsL5 + (TransformationParam(1,posture_target) - TransformationParam(1,posture_ini))*weight;
dtL5 = dtL5 + (TransformationParam(2,posture_target) - TransformationParam(2,posture_ini))/TransformationParam(2,posture_ini)*weight; % relative to the original L5 tension
dsC7 = dsC7 + (TransformationParam(3,posture_target) - TransformationParam(3,posture_ini))*weight;
dtC7 = dtC7 + (TransformationParam(4,posture_target) - TransformationParam(4,posture_ini))/TransformationParam(4,posture_ini)*weight; % relative to the original C7 tension
dsC1 = dsC1 + (TransformationParam(5,posture_target) - TransformationParam(5,posture_ini))*weight;
dtC1 = dtC1 + (TransformationParam(6,posture_target) - TransformationParam(6,posture_ini))/TransformationParam(6,posture_ini)*weight; % relative to the original C1 tension
dhtl = dhtl + (TransformationParam(7,posture_target) - TransformationParam(7,posture_ini))/TransformationParam(7,posture_ini)*weight; % relative to the height
dhc = dhc + (TransformationParam(8,posture_target) - TransformationParam(8,posture_ini))/TransformationParam(8,posture_ini)*weight; % relative to the height
dthetac = dthetac + (TransformationParam(9,posture_target) - TransformationParam(9,posture_ini))*weight;
dtheta = dtheta + (TransformationParam(10,posture_target) - TransformationParam(10,posture_ini))*pi/180*weight;

dh = 1; % Correction for the new spline to be the same length as the original one.
ltarget = 0;
n = 0; % Number of loop.
cond1 = 0;
cond2 = 0;
delta = 0.5;
while abs(ltarget-l)/l > 0.0001 % 0.01% error tolerated on the spline's length.
    n = n+1;
    Xtl = {{[0,0] sL5+dsL5 tL5*(1+dtL5)},{[(htl+dhtl*htl)*dh 0] sC7+dsC7 tC7*(1+dtC7)}}; % Spline parameters for the thoraco-lumbar part.
    Xc = {{[(htl+dhtl*htl)*dh 0] sC7+dsC7 tC7*(1+dtC7)},{[(htl+dhtl*htl+hc*(1+dhc)*cos((thetac+dthetac)*pi/180))*dh (hc*(1+dhc)*sin((thetac+dthetac)*pi/180))*dh] sC1+dsC1 tC1*(1+dtC1)}}; % Spline parameters for the cervical part.
    Qtl = hobbysplines(Xtl,N,'cycle',false); % Transformation without angle between original and transformed sacrum.
    Qc = hobbysplines(Xc,N,'cycle',false);
    ltarget = lengthcurve([Qtl;Qc(2:end,:)]);
    if ltarget > l
        cond1 = 1;
        if cond2 == 1
            delta = delta/2;
            cond2 = 0;
        end
        dh = dh-delta;
    else
        cond2 = 1;
        if cond1 == 1
            delta = delta/2;
            cond1 = 0;
        end
        dh = dh+delta;
        
    end
end
%% Solution 1
% Qtransf = rot3d([Qtl;Qc(2:end,:)],[0,0,0]); 

%% Solution 2 (v.1.8.6)
Qtransf = rot3d([Qtl;Qc(2:end,:)],[0,0,dtheta]); % Transformation with variation of angle to give a correct angle for the pelvis.

end