function pointRot = rotvect(point, vect, angle, center)
% Returns a point rotated around the axis defined by 'vect' by the angle
% 'angle' in rad. 'center' is optional it is the center of rotation 
% (default is [0 0 0]).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

    if ~exist('center','var')
        center = [0 0 0];
    end
    
    for i=1:size(point,1)
        point(i,:) = point(i,:) - center;
    end

    R = [vect(1)^2*(1-cos(angle))+cos(angle) vect(1)*vect(2)*(1-cos(angle))-vect(3)*sin(angle) vect(1)*vect(3)*(1-cos(angle))+vect(2)*sin(angle); ...
        vect(1)*vect(2)*(1-cos(angle))+vect(3)*sin(angle) vect(2)^2*(1-cos(angle))+cos(angle) vect(2)*vect(3)*(1-cos(angle))-vect(1)*sin(angle); ...
        vect(1)*vect(3)*(1-cos(angle))-vect(2)*sin(angle) vect(2)*vect(3)*(1-cos(angle))+vect(1)*sin(angle) vect(3)^2*(1-cos(angle))+cos(angle)];
    
    pointRot = (R*point')';
    for i=1:size(point,1)
        pointRot(i,:) = pointRot(i,:) + center;
    end

end