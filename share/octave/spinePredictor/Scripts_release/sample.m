function Qtransf = sample(Q,x)
% Resample a list of points Q according to the x coordinates.
% CAREFUL : Q and x must have the same length, and x(1) must be the same as
% Q(1,1).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    
    Qtransf = Q;
    j = 1;
    for i=2:size(Q,1)
        a = (Q(i,2)-Q(i-1,2))/(Q(i,1)-Q(i-1,1));
        b = Q(i,2) - (a*Q(i,1));
        while j <= size(x,1) && Q(i,1) >= x(j)
            Qtransf(j,1) = x(j);
            Qtransf(j,2) = a*x(j) + b;
            j = j+1;
        end
    end

end