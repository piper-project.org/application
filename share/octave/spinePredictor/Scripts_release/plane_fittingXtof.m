function [normal,surface1_centre, surface2_centre, centre] = plane_fittingXtof( draw,mesh1, mesh2)
% SPHERE FITTING: Compute the center and the radius of the mesh
%
% INPUT ARGUMENTS:
% mesh1 and mesh2 the name of the stl file used
%
% draw=1 : Draw the result
%
% Default : mode=2,draw=0
%
% Example : plane_fittingXtof( 'rotule1.stl','rotule2.stl',1,1 )

% Copyright (C) 2017 U Southampton
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Code by Pierre Mailliez, SOTON, Summer 2015
% Corrections/modifications by Christophe Lecomte (CL), SOTON, Summer 2015.
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

if nargin==2
    meshes=0;
else
    meshes=1;
end

% MESH 1
% Import vertice from stl file
vertice=stlread(mesh1);
[vertice2,~,~]=unique(vertice,'rows');
%
%
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% CL: I replaced the code below with a call to "planeCloud" that evaluates
%     the plane with minimum sum of squares of distances to the points
%
% n_vertice2=length(vertice2);
% x=vertice2(:,1);
% y=vertice2(:,2);
% z=vertice2(:,3);
%
% % Calculation of the parameters
% M=zeros(3);
% for k=1:n_vertice2
%     v=[x(k) y(k) 1];
%     M=M+v'*v;
% end
%
% B=zeros(1,3);
% for k=1:n_vertice2
%     B=B+[x(k)*z(k) y(k)*z(k) z(k)];
% end
% parameter1=M\B';
%
% % Centre of gravity
% surface1_centre=[sum(vertice2(:,1)) sum(vertice2(:,2)) sum(vertice2(:,3))]/n_vertice2;
% % CL: I added this definition (the two lines below) of the normal
% %     This is the "old version" still...
% surface1_normal=[parameter1(1) parameter1(2) -1];
% surface1_normal=surface1_normal/norm(surface1_normal);
% CL: end of commented code...
% % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
[surface1_centre,surface1_normal]=planeCloud(vertice2);
% % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if meshes==0
    % CL : I commented the two following lines and sbstituted the
    %      (third) following one (see modif. above to define the normal
    %      to surface 1, surface1_normal).
    % normal=[parameter1(1) parameter1(2) -1];
    % normal=normal/norm(normal);
    normal=surface1_normal;
    surface2_centre=surface1_centre;
    centre=surface1_centre;
end

% % % CL: I commented the (drawing) code below (as I commented the definition
% % %     of parameter1, etc.)
% % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% if draw==1
%     figure
%     %Draw points
%     plot3(x,y,z,'.b')
%     hold on
%     plot3(surface1_centre(1),surface1_centre(2),surface1_centre(3),'*r')
%     hold on
%     % Draw plane 1
%     xLim = [min(vertice2(:,1)) max(vertice2(:,1))];
%     yLim = [min(vertice2(:,2)) max(vertice2(:,2))];
%     [X,Y] = meshgrid(xLim,yLim);
%     Z = (parameter1(1) * X + parameter1(2) * Y + parameter1(3));
%     reOrder = [1 2  4 3];
%     patch(X(reOrder),Y(reOrder),Z(reOrder),'b');
%     grid on;
%     alpha(0.3);
%     hold on
% end
% % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


if meshes==1
    % MESH 2
    % Import vertice from stl file
    vertice=stlread(mesh2);
    [vertice2,~,~]=unique(vertice,'rows');
    %
    % % % CL: I replaced the code below with a call to "planeCloud" that evaluates
    % % %     the plane with minimum sum of squares of distances to the points
    % % %   ALSO: The normal is the mean of the two normals
    % % %    The centre is the mean of the two centres
    % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %
%     n_vertice2=length(vertice2);
%     x=vertice2(:,1);
%     y=vertice2(:,2);
%     z=vertice2(:,3);
%     
%     % Calculation of the parameters
%     M=zeros(3);
%     for k=1:n_vertice2
%         v=[x(k) y(k) 1];
%         M=M+v'*v;
%     end
%     
%     B=zeros(1,3);
%     for k=1:n_vertice2
%         B=B+[x(k)*z(k) y(k)*z(k) z(k)];
%     end
%     parameter2=M\B';
%     
%     % Centre of gravity
%     surface2_centre=[sum(vertice2(:,1)) sum(vertice2(:,2)) sum(vertice2(:,3))]/n_vertice2;
%     % CL: I commented and replaced the definition of the median plane...
%     % Median Plane
%     parameter3=[parameter1(1),parameter1(2),-1,parameter1(3)]/sqrt(parameter1(1)^2+parameter1(2)^2+1);
%     parameter3=parameter3+([parameter2(1),parameter2(2),-1,parameter2(3)]/sqrt(parameter2(1)^2+parameter2(2)^2+1));
%     normal=[parameter3(1) parameter3(2) parameter3(3)];
%     normal=normal/norm(normal);
%     
%     % Centre
%     centre=(surface1_centre+surface2_centre)/2;
    % CL: end of commented code...
    % % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [surface2_centre,surface2_normal]=planeCloud(vertice2);
    % assures that first and second normals point more or less in the 
    %        same direction
    if (surface1_normal(:).'*surface2_normal(:))>0       
        normal=surface1_normal+surface2_normal;
    else
        normal=surface1_normal-surface2_normal;
    end
    normal=normal/norm(normal);
    centre=(surface1_centre+surface2_centre)/2;    
    % % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    %
end

% % % CL: I commented the (drawing) code below (as I commented the definition
% % %     of parameter1, etc.)
% % ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% % % Draw
% % if draw==1
% %     if meshes==1
% %         %         % Draw points
% %         %         plot3(x,y,z,'.b')
% %         %         hold on
% %         %         plot3(surface2_centre(1),surface2_centre(2),surface2_centre(3),'*r')
% %         %         hold on
% %         % Draw centre
% %         plot3(surface2_centre(1),surface2_centre(2),surface2_centre(3),'*r')
% %         hold on
% %         % Draw plane 2
% %         Z = (parameter2(1) * X + parameter2(2) * Y + parameter2(3));
% %         reOrder = [1 2  4 3];
% %         patch(X(reOrder),Y(reOrder),Z(reOrder),'b');
% %         grid on;
% %         alpha(0.3);
% %         hold on
% %         % Draw median plane
% %         Z = (parameter3(1) * X + parameter3(2) * Y + parameter3(4))/-parameter3(3);
% %         reOrder = [1 2  4 3];
% %         patch(X(reOrder),Y(reOrder),Z(reOrder),'g');
% %         grid on;
% %         alpha(0.3);
% %         hold on
% %     end
% % end

end

