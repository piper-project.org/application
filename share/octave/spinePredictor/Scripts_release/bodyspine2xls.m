function bodyspine2xls(bodyspine, outdir, filename, spineOrder)
% Writes the xml master file of the spine based on the bodyspine.
% outdir = string containing the output directory path.
% filename = string containing the name of the xls file.
% spineOrder = optional, cell array of strings contains the order of the 
% main part in the bodyspine (default is {'Sacrum'; 'L5'; 'L4'; ... ; 'C1';
% 'Skull'}).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%  
    fprintf('Writing the master file...\n');

    path = [outdir filename];
    
    if ~exist('spineOrder','var')
       spineOrder = {'Sacrum'; 'L5'; 'L4'; 'L3'; 'L2'; 'L1'; 'T12'; 'T11'; 'T10'; ...
        'T09'; 'T08'; 'T07'; 'T06'; 'T05'; 'T04'; 'T03'; 'T02'; 'T01'; 'C7'; ...
        'C6'; 'C5'; 'C4'; 'C3'; 'C2'; 'C1'; 'Skull'}; % Default order of the spine's parts.
    end
    
    A(1,:) = {'part' 'name' 'file' 'path' 'extra' 'landmark'};
    
    for i=1:size(bodyspine,1)
        A{i+1,1} = bodyspine(i).part;
        A{i+1,2} = spineOrder{i};
        A{i+1,3} = bodyspine(i).name;
        A{i+1,4} = bodyspine(i).path;
        if ~isempty(bodyspine(i).landmarks)
            A{i+1,6} = [outdir bodyspine(i).part '\landmarks\' bodyspine(i).name '_landmarks.xlsx'];
        else
            A{i+1,6} = [];
        end
        extra = [];
        for j=1:size(bodyspine(i).extra,1)
            if j == 1
                extra = [bodyspine(i).extra(j).name ','];
            else
                extra = [extra bodyspine(i).extra(j).name ','];
            end
        end
        extra = extra(1:end-1);
        A{i+1,5} = extra;
    end
    
    p = size(bodyspine,1)+1;
    for ii=1:size(bodyspine,1)
        for jj=1:size(bodyspine(ii).extra)
            p = p+1;
            A{p,1} = bodyspine(ii).extra(jj).part;
            A{p,2} = [];
            A{p,3} = bodyspine(ii).extra(jj).name;
            A{p,4} = bodyspine(ii).extra(jj).path;
            if ~isempty(bodyspine(ii).extra(jj).landmarks)
                A{p,6} = [outdir bodyspine(ii).extra(jj).part '\landmarks\' bodyspine(ii).extra(jj).name '_landmarks.xlsx'];
            else
                A{p,6} = [];
            end
            extra = [];
            for j=1:size(bodyspine(ii).extra(jj).extra,1)
                if j==1
                    extra = [bodyspine(ii).extra(jj).extra(j).name ','];
                else
                    extra = [extra bodyspine(ii).extra(jj).extra(j).name ','];
                end
            end
            extra = extra(1:end-1);
            A{p,5} = extra;
        end
    end
    
    for ii=1:size(bodyspine,1)
        for jj=1:size(bodyspine(ii).extra)
            for kk=1:size(bodyspine(ii).extra(jj).extra)
                p = p+1;
                A{p,1} = bodyspine(ii).extra(jj).extra(kk).part;
                A{p,2} = [];
                A{p,3} = bodyspine(ii).extra(jj).extra(kk).name;
                A{p,4} = bodyspine(ii).extra(jj).extra(kk).path;
                if ~isempty(bodyspine(ii).extra(jj).extra(kk).landmarks)
                    A{p,6} = [outdir bodyspine(ii).extra(jj).extra(kk).part '\landmarks\' bodyspine(ii).extra(jj).extra(kk).name '_landmarks.xlsx'];
                else
                    A{p,6} = [];
                end
                extra = [];
                A{p,5} = extra;
            end
        end
    end
    
    if exist('OCTAVE_VERSION', 'builtin')
        currentdir = pwd;
        cd(outdir);
    end
    
    xlswrite(path,A);
    
    if exist('OCTAVE_VERSION', 'builtin')
        cd(currentdir);
    end
    
    fprintf('Done.\n');

end