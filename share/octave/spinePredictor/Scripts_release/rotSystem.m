function systemr = rotSystem(system,R)
% Rotate each local Coordinate System (CS) contained in the cell array
% 'system' with the rotation matrix R.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

    N = size(system,1);
    systemr = cell(N,1);
    
    for i=1:N
        systemr{i} = zeros(4,3);
        for j=1:4
            systemr{i}(j,:) = (R*system{i}(j,:)')';
            if j ~= 1
                systemr{i}(j,:) = systemr{i}(j,:)/norm(systemr{i}(j,:));
        end
    end

end