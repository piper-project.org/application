function Qtransf = sampleDist(Q,dist)
% Resample a list of points Q with equal distance between each points of
% the curve according to the distance dist.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    
    Qtransf(1,:) = Q(1,:);
    i = 1;
    j = 1;
    n = 2;
    X1 = [0 0 0];
    X2 = [0 0 0];
    while i < size(Q,1)
        dist1 = 0;
        while j < size(Q,1) && dist1 < dist
            j = j+1;
            dist1 = dist1 + norm(Q(j-1,:)-Q(j,:));
        end
        Qtransf(n,:) = Q(j,:);
        n = n+1;
        i = j;
    end

end