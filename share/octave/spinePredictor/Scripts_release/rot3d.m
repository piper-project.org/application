function Xrot = rot3d(X,Theta)
% Rotate the list of point X by the angles in Theta =
% [thetax,thetay,thetaz]. Rotation order = Rz->Rx->Ry.
% (rotation around the x,y and z absolute Coordinate System (CS) axis).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    thetax = Theta(1);
    thetay = Theta(2);
    thetaz = Theta(3);
    Rx = [1 0 0; 0 cos(thetax) -sin(thetax); 0 sin(thetax) cos(thetax)];
    Ry = [cos(thetay) 0 sin(thetay); 0 1 0; -sin(thetay) 0 cos(thetay)];
    Rz = [cos(thetaz) -sin(thetaz) 0; sin(thetaz) cos(thetaz) 0; 0 0 1];
    
    Xrot = X';
    X1 = Rz*Xrot;
    X2 = Rx*X1;
    Xrot = (Ry*X2)';
end