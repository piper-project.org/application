function [systemS,inflexpoint] = spline2vert(VertebraeCenters,Qgtransf,orientation,smooth,inflexpoint)
% Returns the spline (Qgtransf)'s cell array of local Coordinate Systems (CS) 
% based on the distance between each vertebrae of the original spine.
% 'orientation' = optional, 0 means the CS's x-axis are in the same plane
% as the one defined by x and y of the first system (L5), meaning there is
% no rotation of the vertebrae along the y axis, only lateral flexion
% permitted. 1 means these rotation are permitted and defined by the
% curvature of the spline, results may not be realistic.
% 'smooth' = optional, number of smoothing loop wanted. Used to smooth the
% discontinuities from the orientation mode 1. Useless if orientation mode
% is 0.
% inflexpoint (OPTIONAL) is the vertebrae numbers where the inflexion points
% are located(needed if lateral flexion).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

    fprintf('Creating the local Coordinate Systems on the spline...\n');

    if ~exist('orientation','var')
        orientation = 0; % Axial rotation not permitted (Default).
    end
    
    if ~exist('smooth','var')
        smooth = 0; % No smoothing (Default).
    end
    
    Nvert = size(VertebraeCenters,1); % Number of vertebrae.
    systemS = cell(Nvert,1);
    for i=1:Nvert
        systemS{i} = zeros(4,3);
    end
    distJoint = zeros(size(VertebraeCenters,1)-1,1); % Distance between each vertebra's center.
    for i=1:size(VertebraeCenters,1)-1
        distJoint(i) = sqrt((VertebraeCenters(i+1,1)-VertebraeCenters(i,1))^2 + (VertebraeCenters(i+1,2)-VertebraeCenters(i,2))^2 + (VertebraeCenters(i+1,3)-VertebraeCenters(i,3))^2); % Distance between each joint.
    end
    
    %% CS creation (original Frenet's coordinate systems)
    systemS{1}(1,:) = Qgtransf(1,:);
    systemS{1}(3,:) = (Qgtransf(2,:)-Qgtransf(1,:))/norm(Qgtransf(2,:)-Qgtransf(1,:));
    systemS{1}(4,:) = cross(Qgtransf(3,:)-Qgtransf(1,:),Qgtransf(2,:)-Qgtransf(1,:))/norm(cross(Qgtransf(3,:)-Qgtransf(1,:),Qgtransf(2,:)-Qgtransf(1,:)));
    systemS{1}(2,:) = cross(systemS{1}(3,:),systemS{1}(4,:))/norm(cross(systemS{1}(3,:),systemS{1}(4,:)));
    
    n = 2; % Number of the vertebra to be placed.
    Xvert = zeros(size(VertebraeCenters));
    Xvert(1,:) = Qgtransf(1,:);
    tooShort = true; % if the spline is too short compared to the length of the spine.
    for i=1:size(Qgtransf,1)-2
        dist = sqrt((Qgtransf(i,1)-Xvert(n-1,1))^2 + (Qgtransf(i,2)-Xvert(n-1,2))^2 + (Qgtransf(i,3)-Xvert(n-1,3))^2);
        if dist >= distJoint(n-1) && n <= Nvert
            Xvert(n,:) = Qgtransf(i,:);
            systemS{n}(1,:) = Xvert(n,:);
            systemS{n}(3,:) = (Qgtransf(i+1,:)-Qgtransf(i,:))/norm(Qgtransf(i+1,:)-Qgtransf(i,:));
            systemS{n}(4,:) = cross(Qgtransf(i+2,:)-Qgtransf(i,:),Qgtransf(i+1,:)-Qgtransf(i,:))/norm(cross(Qgtransf(i+2,:)-Qgtransf(i,:),Qgtransf(i+1,:)-Qgtransf(i,:)));
            systemS{n}(2,:) = cross(systemS{n}(3,:),systemS{n}(4,:))/norm(cross(systemS{n}(3,:),systemS{n}(4,:)));
            n = n+1;
        end
        if n > Nvert
            tooShort = false;
            break;
        end
    end
    if tooShort % if the spline is not long enough to place the last vertebra
        v = Qgtransf(end,:) - Xvert(n-1,:); % Vector formed by the last position of vertebra found and the last point of the spline.
        for i=n:Nvert
            alpha = distJoint(i-1)/sqrt(v(1)^2 + v(2)^2 + v(3)^2); % Coef multiplying the vector v in order to have the right length.
            Xvert(i,:) = Xvert(i-1,:) + alpha*v;
            systemS{i} = systemS{i-1};
            systemS{i}(1,:) = Xvert(i,:);
        end
    end
    
    %% Calculation of the inflexion points (The spine must be in the sagittal plane)
    if ~exist('inflexpoint','var')
        nbcurve = 0;
        n = 0;
        for i=1:size(systemS,1)-1
            if dot(systemS{i}(4,:),systemS{i+1}(4,:)) < 0
                n = n+1;
                nbcurve = nbcurve+1;
                inflexpoint(n,:) = i;
            end
        end
    else
        nbcurve = size(inflexpoint,1);
    end
    
    %% Reorientation of the vertebrae's CS based on the number of sagittal curvature
    if nbcurve == 2
        for i=[1:inflexpoint(1), inflexpoint(2)+1:size(systemS,1)]
            systemS{i}(2,:) = -systemS{i}(2,:);
            systemS{i}(4,:) = -systemS{i}(4,:);
        end
    elseif nbcurve == 1
        fprintf('Solution is expected to be in forward flexion. Check if the vertebrae orientation is ok.\n');
        for i=inflexpoint(1)+1:size(systemS,1)
            systemS{i}(2,:) = -systemS{i}(2,:);
            systemS{i}(4,:) = -systemS{i}(4,:);
        end
    elseif nbcurve == 0
        fprintf('Solution is expected to be in full forward flexion. Check if the vertebrae orientation is ok.\n');
    else
        for i=[1:inflexpoint(1), inflexpoint(2)+1:size(systemS,1)]
            systemS{i}(2,:) = -systemS{i}(2,:);
            systemS{i}(4,:) = -systemS{i}(4,:);
        end
        fprintf('Too much curvatures. Check if the vertebrae orientation is ok.\n');
    end
    
    %% Removal of the Frenet's induced axial rotation
    if orientation == 0
        for i=2:size(systemS,1)
            systemS{i}(2,:) = systemS{1}(2,:);
            systemS{i}(4,:) = cross(systemS{i}(2,:),systemS{i}(3,:))/norm(cross(systemS{i}(2,:),systemS{i}(3,:)));
            systemS{i}(2,:) = cross(systemS{i}(3,:),systemS{i}(4,:))/norm(cross(systemS{i}(3,:),systemS{i}(4,:)));
        end
    end
    
    %% Smoothing
    for j=1:smooth
        systsmooth = systemS{1};
        for i=2:size(systemS,1)-1
            systsmooth = (systemS{i-1}+systemS{i+1})/2;
            systemS{i} = (systsmooth + systemS{i})/2;
        end
    end
    
    fprintf('Done.\n');
end