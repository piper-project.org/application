function system = body2system(bodyspine)
% Returns a cell array of the spine's vertebrae Coordinate System (CS) based on
% a bodyspine. The CS are constructed with bodyspine's landmarks.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%  
    system = cell(size(bodyspine,1)-2,1);
    for i=1:size(bodyspine,1)-4
        lm = zeros(10,3)*NaN;
        system{i} = zeros(4,3);
        for j=1:size(bodyspine(i+1).landmarks,1)
            if size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperRightEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperRightEnd',2)+1:end),'UpperRightEnd')
                lm(1,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperFrontEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperFrontEnd',2)+1:end),'UpperFrontEnd')
                lm(2,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperLeftEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperLeftEnd',2)+1:end),'UpperLeftEnd')
                lm(3,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperRearEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperRearEnd',2)+1:end),'UpperRearEnd')
                lm(4,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerRightEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerRightEnd',2)+1:end),'LowerRightEnd')
                lm(5,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerFrontEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerFrontEnd',2)+1:end),'LowerFrontEnd')
                lm(6,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerLeftEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerLeftEnd',2)+1:end),'LowerLeftEnd')
                lm(7,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerRearEnd',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerRearEnd',2)+1:end),'LowerRearEnd')
                lm(8,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) <= size('VT01',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(1),'V')
                lm(9,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            elseif size(bodyspine(i+1).landmarks{j,1},2) <= size('DT01',2) && ...
                    strcmp(bodyspine(i+1).landmarks{j,1}(1),'D')
                lm(10,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
            end
        end
        centerup = [sum(lm(1:4,1)) sum(lm(1:4,2)) sum(lm(1:4,3))]/4;
        centerdown = [sum(lm(5:8,1)) sum(lm(5:8,2)) sum(lm(5:8,3))]/4;
        centerfront = [lm(2,1)+lm(6,1) lm(2,2)+lm(6,2) lm(2,3)+lm(6,3)]/2;
        system{i}(1,:) = (centerup+centerdown)/2;
        system{i}(3,:) = (centerup-centerdown)/norm(centerup-centerdown);
        if ~isnan(lm(9,1)) && ~isnan(lm(10,1))
            system{i}(2,:) = (lm(9,:)-lm(10,:))/norm(lm(9,:)-lm(10,:));
        else
            system{i}(2,:) = (centerfront-system{i}(1,:))/norm(centerfront-system{i}(1,:));
        end
        system{i}(4,:) = cross(system{i}(2,:),system{i}(3,:))/norm(cross(system{i}(2,:),system{i}(3,:)));
        system{i}(2,:) = cross(system{i}(3,:),system{i}(4,:))/norm(cross(system{i}(3,:),system{i}(4,:)));
    end
    
    i = i+1; % C2
    lm = zeros(9,3)*NaN;
    system{i} = zeros(4,3);
    for j=1:size(bodyspine(i+1).landmarks,1)
        if size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperLatFacet_R',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperLatFacet_R',2)+1:end),'UpperLatFacet_R')
            lm(1,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('AntArch',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('AntArch',2)+1:end),'AntArch')
            lm(2,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('UpperLatFacet_L',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('UpperLatFacet_L',2)+1:end),'UpperLatFacet_L')
            lm(3,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerRightEnd',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerRightEnd',2)+1:end),'LowerRightEnd')
            lm(4,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerFrontEnd',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerFrontEnd',2)+1:end),'LowerFrontEnd')
            lm(5,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerLeftEnd',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerLeftEnd',2)+1:end),'LowerLeftEnd')
            lm(6,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerRearEnd',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerRearEnd',2)+1:end),'LowerRearEnd')
            lm(7,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('DC2',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('DC2',2)+1:end),'DC2')
            lm(8,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('VC2',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('VC2',2)+1:end),'VC2')
            lm(9,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        end
    end
    centerup = [lm(1,1)+lm(3,1) lm(1,2)+lm(3,2) lm(1,3)+lm(3,3)]/2;
    centerdown = [sum(lm(4:7,1)) sum(lm(4:7,2)) sum(lm(4:7,3))]/4;
    centerfront = [lm(2,1)+lm(5,1) lm(2,2)+lm(5,2) lm(2,3)+lm(5,3)]/2;
    system{i}(1,:) = (centerup+centerdown)/2;
    system{i}(3,:) = (centerup-centerdown)/norm(centerup-centerdown);
    if ~isnan(lm(9,1)) && ~isnan(lm(8,1))
        system{i}(2,:) = (lm(9,:)-lm(8,:))/norm(lm(9,:)-lm(8,:));
    else
        system{i}(2,:) = (centerfront-system{i}(1,:))/norm(centerfront-system{i}(1,:));
    end
    system{i}(4,:) = cross(system{i}(2,:),system{i}(3,:))/norm(cross(system{i}(2,:),system{i}(3,:)));
    system{i}(2,:) = cross(system{i}(3,:),system{i}(4,:))/norm(cross(system{i}(3,:),system{i}(4,:)));
    
    i = i+1; % C1
    lm = zeros(6,3)*NaN;
    system{i} = zeros(4,3);
    for j=1:size(bodyspine(i+1).landmarks,1)
        if size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerLatFacet_R',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerLatFacet_R',2)+1:end),'LowerLatFacet_R')
            lm(1,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('AntArch',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('AntArch',2)+1:end),'AntArch')
            lm(2,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('LowerLatFacet_L',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('LowerLatFacet_L',2)+1:end),'LowerLatFacet_L')
            lm(3,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('PostArch',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('PostArch',2)+1:end),'PostArch')
            lm(4,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('VC1',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('VC1',2)+1:end),'VC1')
            lm(5,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        elseif size(bodyspine(i+1).landmarks{j,1},2) >= size('DC1',2) && ...
                strcmp(bodyspine(i+1).landmarks{j,1}(end-size('DC1',2)+1:end),'DC1')
            lm(6,:) = [bodyspine(i+1).landmarks{j,2} bodyspine(i+1).landmarks{j,3} bodyspine(i+1).landmarks{j,4}];
        end
    end
    center = [lm(1,1)+lm(3,1) lm(1,2)+lm(3,2) lm(1,3)+lm(3,3)]/2;
    system{i}(1,:) = center;
    if ~isnan(lm(6,1)) && ~isnan(lm(5,1))
        system{i}(2,:) = (lm(5,:)-lm(6,:))/norm(lm(5,:)-lm(6,:));
    else
        system{i}(2,:) = (lm(2,:)-lm(4,:))/norm(lm(2,:)-lm(4,:));
    end
    system{i}(4,:) = (lm(1,:)-lm(3,:))/norm(lm(1,:)-lm(3,:));
    system{i}(3,:) = cross(system{i}(4,:),system{i}(2,:))/norm(cross(system{i}(4,:),system{i}(2,:)));
    system{i}(4,:) = cross(system{i}(2,:),system{i}(3,:))/norm(cross(system{i}(2,:),system{i}(3,:)));
end