function [system, systemSacr, systemC0] = csfile2system(systempath)
% Returns the local Coordinate Systems contained in the systempath file.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
fprintf('Creating the spine''s local Coordinate Systems...\n');

vert = {'L5'; 'L4'; 'L3'; 'L2'; 'L1'; 'T12'; 'T11'; 'T10'; ...
    'T9'; 'T8'; 'T7'; 'T6'; 'T5'; 'T4'; 'T3'; 'T2'; 'T1'; 'C7'; ...
    'C6'; 'C5'; 'C4'; 'C3'; 'C2'; 'C1'}; % Vertebrae order and name.

system = cell(size(vert,1),1);
for i=1:size(system,1)
    system{i} = zeros(4,3);
end

A = openfile(systempath,' ');

for i=1:size(vert,1)
    for j=1:size(A,1)
        if strcmp(vert{i},A{j}{1})
            system{i} = [str2double(A{j}{2}), str2double(A{j}{3}), str2double(A{j}{4}); ...
                str2double(A{j}{5}), str2double(A{j}{6}), str2double(A{j}{7}); ...
                str2double(A{j}{8}), str2double(A{j}{9}), str2double(A{j}{10}); ...
                str2double(A{j}{11}), str2double(A{j}{12}), str2double(A{j}{13})];
        end
    end
end

%% Sacrum system (currently processed separately)

systemSacr = cell(1,1);
% systemSacr{1} = zeros(4,3);

for j=1:size(A,1)
    if strcmp('sacrum',A{j}{1}) || strcmp('Sacrum',A{j}{1})
        systemSacr{1} = [str2double(A{j}{2}), str2double(A{j}{3}), str2double(A{j}{4}); ...
            str2double(A{j}{5}), str2double(A{j}{6}), str2double(A{j}{7}); ...
            str2double(A{j}{8}), str2double(A{j}{9}), str2double(A{j}{10}); ...
            str2double(A{j}{11}), str2double(A{j}{12}), str2double(A{j}{13})];
    end
end

flagSacr = not(isempty(systemSacr{1})); % not used currently

%% C0 system (currently processed separately)
systemC0 = cell(1,1);
% systemC0{1} = zeros(4,3);

for j=1:size(A,1)
    if strcmp('C0',A{j}{1})
        systemC0{1} = [str2double(A{j}{2}), str2double(A{j}{3}), str2double(A{j}{4}); ...
            str2double(A{j}{5}), str2double(A{j}{6}), str2double(A{j}{7}); ...
            str2double(A{j}{8}), str2double(A{j}{9}), str2double(A{j}{10}); ...
            str2double(A{j}{11}), str2double(A{j}{12}), str2double(A{j}{13})];
    end
end

flagC0 = not(isempty(systemC0{1})); % not used currently

fprintf('Done.\n');
end