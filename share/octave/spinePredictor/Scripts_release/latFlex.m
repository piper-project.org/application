function systemlatflex = latFlex(system, thoracolumbarlatflex, cervicallatflex)
% Returns a system from an initial system where each vertebra has rotated 
% along there y axis (ISB) in order to give a total rotation of the 
% thoraco-lumbar part (L5-T1) of 'thoracolumbarRot' and a total rotation of
% the cervical part (T1-C1) of 'cervicalRot'.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    nT1 = 17;
    maxROM = [9.30000000000000;8.70000000000000;6.70000000000000;10.5000000000000; ...
        11.2000000000000;8.60000000000000;5.70000000000000; ... % Values of literature (from C1/C2 to C7/T1).
        4.92857142900000;6;4.92857142900000;6.07142857100000;5.92857142900000; ...
        5.92857142900000;6;6;5.92857142900000;7;9;7.85714285700000;6;6; ...
        7.85714285700000;6;3]; % Values of literature (from T1/T2 to L5/S1).
    maxROM = maxROM/2; % max rotation to the left or max rotatio to the right.
    
    cervCouplingRot = [4.31182795700000;0.745977011000000;0.640298507000000; ...
        0.562857143000000;0.491071429000000;0.374418605000000;0.328070175000000]; % Axial rotation coupling from C1/C2 to C7/T1.
    
    currentLatFlexAngles = zeros(size(system,1),1);
    currentLatFlexAngles(1) = 0;
    for i=2:size(system,1)
        currentLatFlexAngles(i) = atan2(dot(system{i-1}(4,:),system{i}(3,:)),dot(system{i-1}(3,:),system{i}(3,:)))*180/pi;
    end
    
    maxROM1 = maxROM;
    cervCouplingRot1 = cervCouplingRot;
    for i=1:size(maxROM,1)
        maxROM1(i) = maxROM(end-i+1);
    end
    for i=1:size(cervCouplingRot,1)
        cervCouplingRot1(i) = cervCouplingRot(end-i+1);
    end
    maxROM = maxROM1;
    cervCouplingRot = cervCouplingRot1;
    
    if ~isempty(thoracolumbarlatflex)
        if thoracolumbarlatflex > sum(maxROM(1:nT1))
            fprintf('Thoracolumbar rotation exceeds physiological limits.\nIt has been reduced to the maximum ROM.\n');
            thoracolumbarlatflex = sum(maxROM(1:nT1));
        end

        coeftl = (thoracolumbarlatflex-sum(currentLatFlexAngles(1:nT1)))/sum(maxROM(1:nT1));
        for i=1:nT1
            for j=i:size(system,1)
                angle = coeftl*maxROM(i)*pi/180;
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(2,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(2,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(2,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(2,:),angle);
            end
        end
    end
    
    if ~isempty(cervicallatflex)
        if cervicallatflex > sum(maxROM(nT1+1:end))
            fprintf('Cervical rotation exceeds physiological limits.\nIt has been reduced to the maximum ROM.\n');
            cervicallatflex = sum(maxROM(nT1+1:end));
        end
        
        coefc = (cervicallatflex-sum(currentLatFlexAngles(nT1+1:end)))/sum(maxROM(nT1+1:end));
        for i=nT1+1:size(system,1)
            for j=i:size(system,1)
                angle = coefc*maxROM(i)*pi/180;
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(2,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(2,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(2,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(2,:),angle);
                % Lateral flexion coupling
                angle = -angle*cervCouplingRot(i-nT1);
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(3,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(3,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(3,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(3,:),angle);

            end
        end
    end
    
    systemlatflex = system;
end