function k = openfile(filename,delimiter,startrow,endrow,startcol)
% Open a file and extract a cell array from the data in it delimited by 
% delimiter (startrow, endrow and startcol not used yet).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    if nargin < 3
        startrow = 1;
        startcol = 1;
        endrow = 0;
    end
    if nargin < 4
        startcol = 1;
        endrow = 0;
    end
    if nargin < 5
        startcol = 1;
    end

    fileID = fopen(filename,'r');
    j = 0;
    k = {};
    
    while 1
        t = fgetl(fileID);
        if(~ischar(t)),break,end
        j = j + 1;
        k{j,1} = strsplit(t,delimiter);
    end
    
    fclose(fileID);
    
end