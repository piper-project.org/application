function systemwrite(path,system,sacrCS,C0CS)
% Writes a .txt file of a cell array of vertebrae CS ('system') in 'path'.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
fprintf('Writing the CS file...\n');

A = cell(1+24,1);

if isempty(sacrCS{1}) == true
    if  isempty(C0CS{1}) == false
        A = cell(1+24+1,1);
    end
else
    if  isempty(C0CS{1}) == false
        A = cell(1+24+2,1);
    else
        A = cell(1+24+1,1);
    end
end

vert = {'L5'; 'L4'; 'L3'; 'L2'; 'L1'; 'T12'; 'T11'; 'T10'; ...
    'T9'; 'T8'; 'T7'; 'T6'; 'T5'; 'T4'; 'T3'; 'T2'; 'T1'; 'C7'; ...
    'C6'; 'C5'; 'C4'; 'C3'; 'C2'; 'C1'}; % Vertebrae's order.

A{1} = '# Coordinate system [Origin; x-axis; y-axis; z-axis]';

n = 1;

% Sacrum
if  isempty(sacrCS{1}) == false 
    A{n+1} = 'Sacrum';
    A{n+1} = [A{n+1} ' ' num2str(sacrCS{1}(1,1)) ' ' num2str(sacrCS{1}(1,2)) ' ' num2str(sacrCS{1}(1,3))];
    A{n+1} = [A{n+1} ' ' num2str(sacrCS{1}(2,1)) ' ' num2str(sacrCS{1}(2,2)) ' ' num2str(sacrCS{1}(2,3))];
    A{n+1} = [A{n+1} ' ' num2str(sacrCS{1}(3,1)) ' ' num2str(sacrCS{1}(3,2)) ' ' num2str(sacrCS{1}(3,3))];
    A{n+1} = [A{n+1} ' ' num2str(sacrCS{1}(4,1)) ' ' num2str(sacrCS{1}(4,2)) ' ' num2str(sacrCS{1}(4,3))];
    n = n+1;
end

% Other vertebrae
for i=1:size(system,1)
    A{n+1} = vert{i};
    A{n+1} = [A{n+1} ' ' num2str(system{i}(1,1)) ' ' num2str(system{i}(1,2)) ' ' num2str(system{i}(1,3))];
    A{n+1} = [A{n+1} ' ' num2str(system{i}(2,1)) ' ' num2str(system{i}(2,2)) ' ' num2str(system{i}(2,3))];
    A{n+1} = [A{n+1} ' ' num2str(system{i}(3,1)) ' ' num2str(system{i}(3,2)) ' ' num2str(system{i}(3,3))];
    A{n+1} = [A{n+1} ' ' num2str(system{i}(4,1)) ' ' num2str(system{i}(4,2)) ' ' num2str(system{i}(4,3))];
    n = n+1;
end

% C0
if  isempty(C0CS{1}) == false
    A{n+1} = 'C0';
    A{n+1} = [A{n+1} ' ' num2str(C0CS{1}(1,1)) ' ' num2str(C0CS{1}(1,2)) ' ' num2str(C0CS{1}(1,3))];
    A{n+1} = [A{n+1} ' ' num2str(C0CS{1}(2,1)) ' ' num2str(C0CS{1}(2,2)) ' ' num2str(C0CS{1}(2,3))];
    A{n+1} = [A{n+1} ' ' num2str(C0CS{1}(3,1)) ' ' num2str(C0CS{1}(3,2)) ' ' num2str(C0CS{1}(3,3))];
    A{n+1} = [A{n+1} ' ' num2str(C0CS{1}(4,1)) ' ' num2str(C0CS{1}(4,2)) ' ' num2str(C0CS{1}(4,3))];
end
writefile(path,A);

fprintf('Done.\n');
end