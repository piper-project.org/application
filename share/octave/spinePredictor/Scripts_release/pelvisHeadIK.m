function systemsave = pelvisHeadIK(systemOrig, sightVector, mode)
% Returns a system from an initial system where each vertebra has rotated 
% in order to make the sight vector and the x local vector of C1 coplanar.
% mode = 1 : only cervicals are rotated.
%        2 (default) : every vertebra is rotated.
%        3 : every vertebra is rotated but the precision is better
%        (slower).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    fprintf('Calculating the best rotations for the head orientation...\n');

    if ~exist('mode','var')
        mode = 1;
    end
    sightVector = sightVector/norm(sightVector);

    %% max angle in axial rotation and lateral flexion (theoretical non coupled angles)
    systemC0orig = systemOrig{end};
    
    vectAngleRot = [0 0 0];
    vectAngleRot(1) = dot(sightVector, systemC0orig(2,:));
    vectAngleRot(3) = dot(sightVector, systemC0orig(4,:));
    angleRot = atan2(vectAngleRot(1),vectAngleRot(3))-pi/2;
    if(angleRot < -pi)
        angleRot = angleRot+2*pi;
    elseif(angleRot > pi)
        angleRot = angleRot - 2*pi;
    end
    
    
    vectAngleLatFlex = [0 0 0];
    vectAngleLatFlex(2) = dot(sightVector, systemC0orig(3,:));
    vectAngleLatFlex(3) = dot(sightVector, systemC0orig(4,:));
    angleLatFlex = atan2(vectAngleLatFlex(3),vectAngleLatFlex(2));
    
    
    %% Calculation of a right configuration
    delta = linspace(0,1,10);
    if mode > 2
        delta = linspace(0,1,50);
    end
    errprec = 1000000;
    for i=1:size(delta,2)
        angleRot1 = angleRot*delta(i);

        systemrot = axialRot(systemOrig,[],angleRot1*180/pi);

%         vectAngleRot = [0 0 0];
%         vectAngleRot(1) = dot(systemrot{end}(2,:), systemC0orig(2,:));
%         vectAngleRot(3) = dot(systemrot{end}(2,:), systemC0orig(4,:));
%         angleRot1 = atan2(vectAngleRot(1),vectAngleRot(3))-pi/2;
%         if abs((angleRot1 - angleRot)/angleRot) < errprec
%             errprec = abs((angleRot1 - angleRot)/angleRot);
%             angleRotSave = angleRot*deltarot(i)-pi/2;
%             systemrotsave = systemrot;
%         end
        for j=1:size(delta,2)
            angleLatFlex1 = angleLatFlex*delta(j);

            systemlatflex = latFlex(systemrot,[],angleLatFlex1*180/pi);

%             vectAngleLatFlex = [0 0 0];
%             vectAngleLatFlex(2) = dot(systemlatflex{end}(2,:), systemC0orig(3,:));
%             vectAngleLatFlex(3) = dot(systemlatflex{end}(2,:), systemC0orig(4,:));
%             angleLatFlex1 = atan2(vectAngleLatFlex(3),vectAngleLatFlex(2));
            err = norm(cross(systemlatflex{end}(2,:),sightVector));
            if sign(dot(systemlatflex{end}(2,:),sightVector)) == -1
                err = 1 + 1-err;
            end
            if err < errprec
                errprec = err;
                angleLatFlexSave = angleLatFlex*delta(j); %%%%%%%%%% deltarot -> deltalatflex
                systemsave = systemlatflex;
            end
        end
    end
    
    %% Thoraco-lumbar rotations
    if mode > 1
        systemOrig = systemsave;
        deltarot = linspace(-25,25,10);
        deltalf = linspace(-30,30,10);
        if mode > 2
            deltarot = linspace(-40,40,20);
            deltalf = linspace(-52,52,20);
        end
        for i=1:size(deltarot,2)

            systemrot = axialRot(systemOrig,deltarot(i),[]);

    %         vectAngleRot = [0 0 0];
    %         vectAngleRot(1) = dot(systemrot{end}(2,:), systemC0orig(2,:));
    %         vectAngleRot(3) = dot(systemrot{end}(2,:), systemC0orig(4,:));
    %         angleRot1 = atan2(vectAngleRot(1),vectAngleRot(3))-pi/2;
    %         if abs((angleRot1 - angleRot)/angleRot) < errprec
    %             errprec = abs((angleRot1 - angleRot)/angleRot);
    %             angleRotSave = angleRot*deltarot(i)-pi/2;
    %             systemrotsave = systemrot;
    %         end
            for j=1:size(deltalf,2)
                systemlatflex = latFlex(systemrot,deltalf(j),[]);

    %             vectAngleLatFlex = [0 0 0];
    %             vectAngleLatFlex(2) = dot(systemlatflex{end}(2,:), systemC0orig(3,:));
    %             vectAngleLatFlex(3) = dot(systemlatflex{end}(2,:), systemC0orig(4,:));
    %             angleLatFlex1 = atan2(vectAngleLatFlex(3),vectAngleLatFlex(2));
                err = norm(cross(systemlatflex{end}(2,:),sightVector));
                if sign(dot(systemlatflex{end}(2,:),sightVector)) == -1
                    err = 1 + 1-err;
                end
                if err < errprec
                    errprec = err;
                    angleLatFlexSave = angleLatFlex*deltalf(j); %%%%%%%%%% deltarot -> deltalatflex
                    systemsave = systemlatflex;
                end
            end
        end
    end
    
    fprintf('Done.\n');
    
    
% figure()
% hold on
% systemPlot(systemrotsave,20)
% plot3([systemrotsave{end}(1,1) (sightVector(1)*20+systemrotsave{end}(1,1))],[systemrotsave{end}(1,2) (sightVector(2)*20+systemrotsave{end}(1,2))],[systemrotsave{end}(1,3) (sightVector(3)*20+systemrotsave{end}(1,3))],'-+')
% view(3)
% axis equal
    
end