function systemPlot(system,alpha)
% Plot a system=[{O}{X}{Y}{Z}] with a scaling factor alpha.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    c = ['-r';'-g';'-b'];
    for i=1:size(system,1)
        for j=2:4
            X = [system{i}(1,1) system{i}(1,1)+alpha*system{i}(j,1)];
            Y = [system{i}(1,2) system{i}(1,2)+alpha*system{i}(j,2)];
            Z = [system{i}(1,3) system{i}(1,3)+alpha*system{i}(j,3)];
            plot3(X,Y,Z,c(j-1,:),'LineWidth',2);
        end
    end
end