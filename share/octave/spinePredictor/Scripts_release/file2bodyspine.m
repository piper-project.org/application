function bodyspine = file2bodyspine(initialfilepath,spineOrder)
% Returns a bodyspine from the master file 'initialfilepath'.
% spineOrder = optional, cell array of strings containing the order of the 
% main part in the bodyspine (default is {'Sacrum'; 'L5'; 'L4'; ... ; 'C1';
% 'Skull'}).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    if nargin < 2
       spineOrder = {'Sacrum'; 'L5'; 'L4'; 'L3'; 'L2'; 'L1'; 'T12'; 'T11'; 'T10'; ...
        'T09'; 'T08'; 'T07'; 'T06'; 'T05'; 'T04'; 'T03'; 'T02'; 'T01'; 'C7'; ...
        'C6'; 'C5'; 'C4'; 'C3'; 'C2'; 'C1'; 'Skull'}; % Default spine order.
    end
    
    
    %% Reading of the master file
    fprintf('Reading the master file...\n');
    [~,~,initialspine] = xlsread(initialfilepath);
    fprintf('Done.\n');
    
    
    %% Creation of the bodyspine structure array
    fprintf('Creating the bodyspine structure array...\n');
    for i=1:size(initialspine,1)
        for j=1:size(initialspine,2)
            if isnan(initialspine{i,j})
                initialspine{i,j} = [];
            end
        end
    end
    
    % Prototype of a body structure.
    body.name = [];
    body.path = [];
    body.extra = [];
    body.vertices = [];
    body.faces = [];
    body.landmarks = [];
    body.part = [];
    
    bodyspine(size(spineOrder,1),1) = body;

    for i=1:size(spineOrder,1)
        for j=1:size(initialspine,1)
            if strcmp(spineOrder(i),initialspine{j,2})
                bodyspine(i).name = initialspine{j,3};
                bodyspine(i).path = initialspine{j,4};
                bodyspine(i).landmarks = pp2landmarks(initialspine{j,6});
                bodyspine(i).part = initialspine{j,1};
                if isempty(initialspine{j,5}), continue; end
                extra = strsplit(initialspine{j,5},',')';
                if size(extra,1) > 1
                    bodyspine(i).extra(size(extra,1),:) = body;
                    for jj=1:size(extra,1)
                        for kk=1:size(initialspine,1)
                            if strcmp(extra(jj),initialspine(kk,3))
                                bodyspine(i).extra(jj).name = initialspine{kk,3};
                                bodyspine(i).extra(jj).path = initialspine{kk,4};
                                bodyspine(i).extra(jj).landmarks = pp2landmarks(initialspine{kk,6});
                                bodyspine(i).extra(jj).part = initialspine{kk,1};
                                if isempty(initialspine{kk,5}), continue; end
                                extra2 = strsplit(initialspine{kk,5},',')';
                                if size(extra2,1) > 1
                                    bodyspine(i).extra(jj).extra(size(extra2,1),:) = body;
                                    for jjj=1:size(extra2,1)
                                        for kkk=1:size(initialspine,1)
                                            if strcmp(extra2(jjj),initialspine(kkk,3))
                                                bodyspine(i).extra(jj).extra(jjj).name = initialspine{kkk,3};
                                                bodyspine(i).extra(jj).extra(jjj).path = initialspine{kkk,4};
                                                bodyspine(i).extra(jj).extra(jjj).landmarks = pp2landmarks(initialspine{kkk,6});
                                                bodyspine(i).extra(jj).extra(jjj).part = initialspine{kkk,1};
                                            end
                                        end
                                    end
                                elseif size(extra2,1) == 1
                                    bodyspine(i).extra(jj).extra = body;
                                    for jjj=1:size(extra2,1)
                                        for kkk=1:size(initialspine,1)
                                            if strcmp(extra2(jjj),initialspine(kkk,3))
                                                bodyspine(i).extra(jj).extra(jjj).name = initialspine{kkk,3};
                                                bodyspine(i).extra(jj).extra(jjj).path = initialspine{kkk,4};
                                                bodyspine(i).extra(jj).extra(jjj).landmarks = pp2landmarks(initialspine{kkk,6});
                                                bodyspine(i).extra(jj).extra(jjj).part = initialspine{kkk,1};
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                elseif size(extra,1) == 1
                    bodyspine(i).extra = body;
                    for jj=1:size(extra,1)
                        for kk=1:size(initialspine,1)
                            if strcmp(extra(jj),initialspine(kk,3))
                                bodyspine(i).extra(jj).name = initialspine{kk,3};
                                bodyspine(i).extra(jj).path = initialspine{kk,4};
                                bodyspine(i).extra(jj).landmarks = pp2landmarks(initialspine{kk,6});
                                bodyspine(i).extra(jj).part = initialspine{kk,1};
                                if isempty(initialspine{kk,5}), continue; end
                                extra2 = strsplit(initialspine{kk,5},',')';
                                if size(extra2,1) > 1
                                    bodyspine(i).extra(jj).extra(size(extra2,1),:) = body;
                                    for jjj=1:size(extra2,1)
                                        for kkk=1:size(initialspine,1)
                                            if strcmp(extra2(jjj),initialspine(kkk,3))
                                                bodyspine(i).extra(jj).extra(jjj).name = initialspine{kkk,3};
                                                bodyspine(i).extra(jj).extra(jjj).path = initialspine{kkk,4};
                                                bodyspine(i).extra(jj).extra(jjj).landmarks = pp2landmarks(initialspine{kkk,6});
                                                bodyspine(i).extra(jj).extra(jjj).part = initialspine{kkk,1};
                                            end
                                        end
                                    end
                                elseif size(extra2,1) == 1
                                    bodyspine(i).extra(jj).extra = body;
                                    for jjj=1:size(extra2,1)
                                        for kkk=1:size(initialspine,1)
                                            if strcmp(extra2(jjj),initialspine(kkk,3))
                                                bodyspine(i).extra(jj).extra(jjj).name = initialspine{kkk,3};
                                                bodyspine(i).extra(jj).extra(jjj).path = initialspine{kkk,4};
                                                bodyspine(i).extra(jj).extra(jjj).landmarks = pp2landmarks(initialspine{kkk,6});
                                                bodyspine(i).extra(jj).extra(jjj).part = initialspine{kkk,1};
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    
    for i=1:size(bodyspine,1)
        if exist(bodyspine(i,:).path,'file')
            [bodyspine(i,:).vertices, bodyspine(i,:).faces, ] = stlread(bodyspine(i,:).path);
        else
            bodyspine(i,:).vertices = [];
            bodyspine(i,:).faces = [];
        end
        for j=1:size(bodyspine(i,:).extra,1)
            if exist(bodyspine(i,:).extra(j,:).path,'file')
                [bodyspine(i,:).extra(j,:).vertices, bodyspine(i,:).extra(j,:).faces, ] = stlread(bodyspine(i,:).extra(j,:).path);
            else
                bodyspine(i,:).extra(j,:).vertices = [];
                bodyspine(i,:).extra(j,:).faces = [];
            end
            for k=1:size(bodyspine(i,:).extra(j,:).extra,1)
                if exist(bodyspine(i,:).extra(j,:).extra(k,:).path,'file')
                    [bodyspine(i,:).extra(j,:).extra(k,:).vertices, bodyspine(i,:).extra(j,:).extra(k,:).faces, ] = stlread(bodyspine(i,:).extra(j,:).extra(k,:).path);
                else
                    bodyspine(i,:).extra(j,:).extra(k,:).vertices = [];
                    bodyspine(i,:).extra(j,:).extra(k,:).faces = [];
                end
            end
        end
    end
    fprintf('Done.\n');

end