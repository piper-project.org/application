function systemrot = axialRot(system, thoracolumbarRot, cervicalrot)
% Returns a system from an initial system where each vertebra has rotated 
% along their y axis (ISB) in order to give a total rotation of the 
% thoraco-lumbar part (L5-T1) of 'thoracolumbarRot' and a total rotation of
% the cervical part (T1-C1) of 'cervicalRot'.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    nT1 = 17;
    maxROM = [71;9.50000000000000;10.8000000000000;12.3000000000000;9; ...
        5.60000000000000;5.70000000000000; ... % Values of literature (from C1/C2 to C7/T1).
        9;7.85714285700000; 7.85714285700000;7.85714285700000;8; ...
        6.85714285700000;6.85714285700000;6;4;2.07142857100000;2.07142857100000; ...
        2.07142857100000;2.07142857100000;2.07142857100000;2;2;1]; % Values of literature (from T1/T2 to L5/S1).
    maxROM = maxROM/2; % max rotation to the left or max rotatio to the right.
    
    cervCouplingLatflex = [0.0800000000000000;0.680000000000000;0.550000000000000; ...
        0.540000000000000;0.720000000000000;0.680000000000000;0.600000000000000]; % Lateral flexion coupling from C1/C2 to C7/T1.
    
    currentRotAngles = zeros(size(system,1),1);
    currentRotAngles(1) = 0;
    for i=2:size(system,1)
        currentRotAngles(i) = atan2(dot(system{i-1}(2,:),system{i}(4,:)),dot(system{i-1}(4,:),system{i}(4,:)))*180/pi;
    end
    
    maxROM1 = maxROM;
    cervCouplingLatflex1 = cervCouplingLatflex;
    for i=1:size(maxROM,1)
        maxROM1(i) = maxROM(end-i+1);
    end
    for i=1:size(cervCouplingLatflex,1)
        cervCouplingLatflex1(i) = cervCouplingLatflex(end-i+1);
    end
    cervCouplingLatflex = cervCouplingLatflex1;
    maxROM = maxROM1;
    
    
    if ~isempty(thoracolumbarRot)
        if thoracolumbarRot > sum(maxROM(1:nT1))
            fprintf('Thoracolumbar rotation exceeds physiological limits.\nIt has been reduced to the maximum ROM.\n');
            thoracolumbarRot = sum(maxROM(1:nT1));
        end

        coeftl = (thoracolumbarRot-sum(currentRotAngles(1:nT1)))/sum(maxROM(1:nT1));
        for i=1:nT1
            for j=i:size(system,1)
                angle = coeftl*maxROM(i)*pi/180;
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(3,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(3,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(3,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(3,:),angle);
            end
        end
    end
    
    
    if ~isempty(cervicalrot)
        if cervicalrot > sum(maxROM(nT1+1:end))
            fprintf('Cervical rotation exceeds physiological limits.\nIt has been reduced to the maximum ROM.\n');
            cervicalrot = sum(maxROM(nT1+1:end));
        end

        coefc = (cervicalrot-sum(currentRotAngles(nT1+1:end)))/sum(maxROM(nT1+1:end));
        for i=nT1+1:size(system,1)
            for j=i:size(system,1)
                angle = coefc*maxROM(i)*pi/180;
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(3,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(3,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(3,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(3,:),angle);
                % Lateral flexion coupling
                angle = -angle*cervCouplingLatflex(i-nT1);
                system{j}(1,:) = rotvect(system{j}(1,:),system{i}(2,:),angle,system{i}(1,:));
                system{j}(2,:) = rotvect(system{j}(2,:),system{i}(2,:),angle);
                system{j}(3,:) = rotvect(system{j}(3,:),system{i}(2,:),angle);
                system{j}(4,:) = rotvect(system{j}(4,:),system{i}(2,:),angle);

            end
        end
    end
    
    systemrot = system;
end