function [Qg, sL5save, tL5save, sC7save, tC7save, sC1save, tC1save, htl, hc, thetac, R, thetaz, errsave] = fitSpline(system1, nbpoints, err, nC7)
% From a cell of local systems, returns the list of points of the spline
% Q and the spline parameters (angle and tension for both extremeties and C7).
% nbpoints is the number of points composing the spline Q (best to choose a
% high value). err is the maximum error wanted on the fit (relative error
% to highest point on the curve). This is not always respected as the
% algorithm might have difficulties to find a spline that fit X perfectly,
% but choosing a low value guarantee that the algorithm will find the best
% fitting spline.
% htl is the distance between L5 and C7, hc is the distance between C7 and C1,
% thetac is the angle between L5/C7 and C7/C1.
% R is the rotation matrix used to rotate the spine along x axis facing up. 
% thetaz is the angle used to rotate the spine in order to make the L5 and 
% C7 both on x axis. errsave is the actual max error of the fit.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org

    %% NB working with Octave
    % Octave is way slower than Matlab concerning the optimization loop, so
    % the optimization is a bit less precise with Octave to conserve a
    % quick execution of the script.
    if exist('OCTAVE_VERSION', 'builtin')
        range = linspace(-10,10,3);
    else
        range = -10:10;
    end
    
    %% Location of C7 in the cell array of CS.
    if nargin < 4
        nC7 = 18; % C7 is by default the 18th vertebra  in the cell array of CS (beggining from L5).
    end
    
    fprintf('Spline fitting...\n')
    
    
    %% Extracting the vertebrae's center from the cell array of CS.
    systemtl = system1(1:nC7);
    [~,R,thetaz] = rotSpine3d(systemtl); % Rotation of the spine to have a normalized placement to calculate the spline parameters.
    orig = system1{1}(1,:);
    systemt = transSystem(system1,-orig);
    systemr = rotSystem(systemt,R);
    system = rotSystemAngle(systemr,[0 0 thetaz]);
    xgr = zeros(size(system,1),1);
    ygr = zeros(size(system,1),1);
    zgr = zeros(size(system,1),1);
    for i=1:size(system,1)
        xgr(i) = system{i}(1,1);
        ygr(i) = system{i}(1,2);
        zgr(i) = system{i}(1,3);
    end
    htl = xgr(nC7);
    N = nbpoints;
    
    %% Optimization of the spline on thoraco-lumbar part.
    nbloop = 0; % Number of optimisation loops done.
    firstloop2 = true; % Identifier for the first loop on ii and jj.
    for ii=range % best sL5 search
        correcSL5 = ii;
        for jj=range % best sC7 search
            correcSC7 = jj;
            % Spline creation, based on L5 and C7
            sL5 = atan((ygr(2)-ygr(1))/(xgr(2)-xgr(1)))*180/pi+correcSL5; % Angle at origin
            tL5 = 1; % Tension at origin
            sC7 = atan((ygr(nC7+1)-ygr(nC7-1))/(xgr(nC7+1)-xgr(nC7-1)))*180/pi+correcSC7; % Angle at the end of the curve.
            tC7 = 1; % Tension at the end of the curve.
            X = {{[xgr(1) ygr(1)] sL5 tL5}, {[xgr(nC7) ygr(nC7)] sC7 tC7}}; % Coordinates and parameters for the spline.

            Q = hobbysplines(X,N,'cycle',false);
            errmax = err+0.0001; % Max error on the spline.
            errprec = 0; % Precedent loop max error.
            firstloop = true; % Identifier on the first optimisation loop.
            delta = -0.1; % Tension step.
            breakcond = 0; % Condition to avoid infinite loop.
            breakcond2 = false; % Condition to avoid infinite loop.
            while errmax > err
                errmax = 0;
                nbloop = nbloop + 1;
                iii = 1;
                for j=1:nC7
                    for i=iii:size(Q,1)-1
                        if Q(i,1) <= xgr(j) && Q(i+1,1) > xgr(j) % If xgr(j) between xQi and xQi+1
                            iii = i;
                            if abs(Q(i,2)-ygr(j))/max(abs(ygr)) > errmax
                                errmax = abs(Q(i,2)-ygr(j))/max(abs(ygr));
                                break;
                            end
                        end
                    end
                end
                if errmax > err
                    if firstloop
                        tL5 = tL5+delta;
                        tC7 = tC7+delta;
                        X = {{[xgr(1) ygr(1)] sL5 tL5}, {[xgr(nC7) ygr(nC7)] sC7 tC7}};
                        Q = hobbysplines(X,N,'cycle',false);
                        if norm(Q(end,:)-[xgr(nC7),ygr(nC7),0]) > norm([xgr(nC7-1),ygr(nC7-1),0]-[xgr(nC7),ygr(nC7),0])
                            Q(end,:) = [xgr(nC7),ygr(nC7),0];
                        end
                    end
                    if ~firstloop
                        if errprec > errmax
                            breakcond = 0;
                            tL5 = tL5+delta;
                            tC7 = tC7+delta;
                            X = {{[xgr(1) ygr(1)] sL5 tL5}, {[xgr(nC7) ygr(nC7)] sC7 tC7}};
                            Q = hobbysplines(X,N,'cycle',false);
                            if norm(Q(end,:)-[xgr(nC7),ygr(nC7),0]) > norm([xgr(nC7-1),ygr(nC7-1),0]-[xgr(nC7),ygr(nC7),0])
                                Q(end,:) = [xgr(nC7),ygr(nC7),0];
                            end
                        else
                            breakcond = breakcond + 1;
                            tL5 = tL5-delta;
                            tC7 = tC7-delta;
                            delta = 0.5*delta;
                            tL5 = tL5+delta;
                            tC7 = tC7+delta;
                            X = {{[xgr(1) ygr(1)] sL5 tL5}, {[xgr(nC7) ygr(nC7)] sC7 tC7}};
                            Q = hobbysplines(X,N,'cycle',false);
                            if norm(Q(end,:)-[xgr(nC7),ygr(nC7),0]) > norm([xgr(nC7-1),ygr(nC7-1),0]-[xgr(nC7),ygr(nC7),0])
                                Q(end,:) = [xgr(nC7),ygr(nC7),0];
                            end
                        end
                    end
                end
                if breakcond >= 5; % if delta reduced more than 5 consecutive times (because error didn't reduced)
                    tL5 = tL5-delta;
                    tC7 = tC7-delta;
                    X = {{[xgr(1) ygr(1)] sL5 tL5}, {[xgr(nC7) ygr(nC7)] sC7 tC7}};
                    Q = hobbysplines(X,N,'cycle',false);
                    if norm(Q(end,:)-[xgr(nC7),ygr(nC7),0]) > norm([xgr(nC7-1),ygr(nC7-1),0]-[xgr(nC7),ygr(nC7),0])
                        Q(end,:) = [xgr(nC7),ygr(nC7),0];
                    end
                    delta = -delta;
                    if breakcond2 % If after changing sign of delta, still no improvement.
                        errmax = errprec;
%                         fprintf('\nCant find proper parameters for asked max error\n');
                        break
                    end
                    breakcond2 = true;
                end
                if errprec > errmax && ~firstloop
                    errprec = errmax;
                end
                if firstloop
                    errprec = errmax;
                    firstloop = false;
                end
            end
            if firstloop2
                errsave = errmax;
                Qsave = Q;
                sL5save = sL5;
                tL5save = 1;
                sC7save = sC7;
                tC7save = tC7;
                firstloop2 = false;
            else
                if errsave > errmax
                    errsave = errmax;
                    Qsave = Q;
                    sL5save = sL5;
                    tL5save = 1;
                    sC7save = sC7;
                    tC7save = tC7;
                end
            end
        end
    end
    Qtl = Qsave;
    
    
    %% Optimization of the spline on cervical part.
    firstloop = true;
    for ii=range % best sC1 search
        correcSC1 = ii;
        for jj=0:0 % best tC1 search (not used anymore)
            correcTC1 = jj*0.05;
            % Spline creation, based on C7 and C1
            sC1 = atan((ygr(end)-ygr(end-1))/(xgr(end)-xgr(end-1)))*180/pi+correcSC1; % Angle at the end of the curve.
            tC1 = 1-correcTC1; % Tension at the end of the curve.
            X = {{[xgr(nC7) ygr(nC7)] sC7save 1}, {[xgr(end) ygr(end)] sC1 tC1}}; % Coordinates and parameters for the spline.
            Q = hobbysplines(X,N,'cycle',false);
            nbloop = nbloop+1;
            % Research of max error
            errmax = 0;
            iii = 1;
            for j=nC7:size(xgr,1)
                for i=iii:size(Q,1)-1
                    if Q(i,1) <= xgr(j) && Q(i+1,1) > xgr(j) % If xgr(j) between xQi and xQi+1
                        iii = i;
                        if abs(xgr(j)-Q(i,1)) < abs(xgr(j)-Q(i+1,1)) % If xQi nearer to xgr than xQi+1
                            if abs(Q(i,2)-ygr(j))/max(abs(ygr)) > errmax
                                errmax = abs(Q(i,2)-ygr(j))/max(abs(ygr));
                                break;
                            end
                        end
                        if abs(xgr(j)-Q(i,1)) > abs(xgr(j)-Q(i+1,1)) % If xQi+1 nearer to xgr than xQi
                            if abs(Q(i+1,2)-ygr(j))/max(abs(ygr)) > errmax
                                errmax = abs(Q(i+1,2)-ygr(j))/max(abs(ygr));
                                break;
                            end
                        end
                    end
                end
            end
            if firstloop
                errprec = errmax;
                firstloop = false;
            end
            if errmax <= errprec
                errsavec = errmax;
                errprec = errmax;
                Qsave = Q;
                sC1save = sC1;
                tC1save = tC1;
            end
        end
    end
    Qc = Qsave;
    thetac = atan2(Qc(end,2)-Qc(1,2),Qc(end,1)-Qc(1,1))*180/pi; % Angle of the cervical part.
    hc = norm(Qc(end,:)-Qc(1,:));
    
    if errsavec > errsave
        errsave = errsavec;
    end
    
    
    %% Placing the spline on the original spine position
    Qsave = [Qtl;Qc(2:end,:)];
    Qg1 = invRot3d(Qsave,[0,0,thetaz]); % Rotation of the spline.
    Qg = zeros(size(Qg1));
    for i=1:size(Qg1,1)
        Qg(i,:) = (R\Qg1(i,:)')'; % Rotation of the spline to its original position
    end
    for i=1:size(Qg,1) % Translation
        Qg(i,1) = Qg(i,1) + orig(1);
        Qg(i,2) = Qg(i,2) + orig(2);
        Qg(i,3) = Qg(i,3) + orig(3);
    end
    
    fprintf('Done.\n');
    fprintf('\n%d loops\nSlope L5 = %f\nTension L5 = %f\nSlope C7 = %f\nTension C7 = %f\nSlope C1 = %f\nTension C1 = %f\nhtl = %f\nhc = %f\nthetac = %f\nMax error = %f\n\n',nbloop,sL5save,tL5save,sC7save,tC7save,sC1save,tC1save,htl,hc,thetac,errsave);
end



