% Copyright (C) 2017 UCBL-IFSTTAR, INRIA
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Thomas Lemaire, INRIA
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
arg_list = argv();

% close all
% clear all
% clc

%% For debugging purposes
% arg_list = {'../Datas/CS.dat' '../tmp/gravity.dat' '1' '5' '0.5' '0' '0' '../TransformedSpine/' '1'}; % Test using Matlab
% octave-cli spinePredictor.m ../Datas/CS.dat ../tmp/gravity.dat 3 5 0.5 0 0 ../TransformedSpine/ 1 % Test using octave client

systemPath = arg_list{1}; % a path to a default file should be set in sofa
gravVectorPath = arg_list{2}; % a path to a default file should be set in sofa
srcposture = str2double(arg_list{3}); % a default should be set in sofa, e.g. 1 
trgtposture = str2double(arg_list{4}); % a default should be set in sofa, e.g. 3
alpha = str2double(arg_list{5}); % a default should be set in sofa, e.g. 0
latFlexTL = str2double(arg_list{6}); % default should be set to 0 in sofa. Max is 27.7 deg / -27.7 deg (Angle of thoraco-lumbar segment)
latFlexC = str2double(arg_list{7}); % default should be set to 0 in sofa. Max is 7.4 deg / -7.4 deg (Angle of cervical segment)
outdirusr = arg_list{8}; % a default should be set in sofa
alignChoice = str2double(arg_list{9}); % default should be set to 0 in sofa

% pkg install -forge io
pkg load io

%% Uncomment for GUI checking/debugging
% close all
% clear all
% clc
% srcposture = 6;     
% trgtposture = 5;    
% alpha = 0.1;
% latFlexTL = 13; % Max is 27.7 deg / -27.7 deg (Angle of thoraco-lumbar segment)
% latFlexC = 3.5; % Max is 7.4 deg / -7.4 deg (Angle of cervical segment)
% alignChoice = 1;
% currentdir = pwd; % Because Octave has bugs concerning relative paths...
% systemPath = [currentdir '\..\ReferenceSpine\CS.dat'];
% gravVectorPath = [currentdir '\..\tmp\gravity.dat']; % a path to a default file should be set in sofa
% outdirusr = [currentdir '\..\TransformedSpine\']; % Output directory path for the transformed/positioned spine.
% gravVector = [0 0 -1];

%%
reliabilityFile = [outdirusr 'sp_reliability.dat']; 
[systemref, splineref, splineParameters, systemsacr, systemc0] = initialPositioning_light_wp3(systemPath);

%% Use if accounting for lateral flexion
spineTransformationcs_wp3(srcposture, trgtposture, alpha, systemref, systemsacr, systemc0, splineref, splineParameters, outdirusr, gravVectorPath, alignChoice, latFlexTL, latFlexC);
%% Use if not accounting for lateral flexion
% spineTransformationcs_wp3(srcposture, trgtposture, alpha, systemref, systemsacr, systemc0, splineref, splineParameters, outdirusr, gravVectorPath, alignChoice);
%% Flag for visual estimation of the sp_reliability of the prediction
if exist('latFlexTL','var') || exist('latFlexC','var')
    if abs(latFlexTL) <= 0.1
        if abs(latFlexC) <= 0.1 % Case of the spine in the sagittal plane
            if alpha >= 0
                if alpha <= 1
                    sp_reliability = 1; % green
                elseif alpha <= 1.1
                    sp_reliability = 2;% orange
                else
                    sp_reliability = 3; % red
                end
            elseif alpha >= -0.1
                sp_reliability = 2;
            else
                sp_reliability = 3;
            end
        elseif abs(latFlexC) <= 7.4 % Case of the cervical spine OOP but still within ROM
            sp_reliability = 2;
        else
            sp_reliability = 3;
        end    
    elseif abs(latFlexTL) <= 27.7 % Case of the TL spine OOP but still within ROM
        sp_reliability = 2;
    else
        sp_reliability = 3;
    end  
else
    if alpha >= 0
        if alpha <= 1
            sp_reliability = 1;
        elseif alpha <= 1.1
            sp_reliability = 2;
        else
            sp_reliability = 3;
        end
    elseif alpha >= -0.1
        sp_reliability = 2;
    else
        sp_reliability = 3;
    end
end
fileID = fopen(reliabilityFile,'w');
fprintf(fileID,'%i ',sp_reliability);
fclose(fileID);