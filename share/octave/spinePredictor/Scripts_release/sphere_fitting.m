function [ centre ] = sphere_fitting( mode,draw,mesh1,mesh2 )
% SPHERE FITTING: Compute the center and the radius of the mesh 
% 
% INPUT ARGUMENTS:
% mesh1 and mesh2 the name of the stl file used
% mode:
%   - 1 : Apply shpere fitting with one radius and one center per mesh
% 	- 2 : Apply sphere fitting with one radius per mesh and one center for
%         the two meshes
%   - 3 : Sphere fitting for only one mesh
% 
% draw=1 : Draw the result
%
% Default : mode=2,draw=0 
%
% Example : sphere_fitting( 'rotule1.stl','rotule2.stl',1,1 )

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

if mode==1
    
    % Sphere fitting with two differents center and radius
    % Import vertice from stl file

    vertice=stlread(mesh1);
    n_vertice=length(vertice);
    x=vertice(:,1);
    y=vertice(:,2);
    z=vertice(:,3);

    % Calculation of the parameters
    M=zeros(4);

    for k=1:n_vertice
        v=[1 x(k) y(k) z(k)];
        M=M+v'*v;
    end

    B=zeros(4,1);
    for k=1:n_vertice
        p(k)=x(k)^2+y(k)^2+z(k)^2;
        B=B+[p(k);p(k)*x(k);p(k)*y(k);p(k)*z(k)];
    end

    centre=zeros(1,3);
    A=M\-B;
    centre(1)=-A(2)/2;
    centre(2)=-A(3)/2;
    centre(3)=-A(4)/2;
    R=sqrt(centre(1)^2+centre(2)^2+centre(3)^2-A(1));

    if draw==1
        figure
        %Draw points
        plot3(x,y,z,'.b')
        hold on

        %Draw sphere calculated
        [Sx,Sy,Sz] = sphere(20);
        surf(R*Sx+centre(1),R*Sy+centre(2),R*Sz+centre(3),'faceAlpha',0.3,'Facecolor','c')
        hold on
        plot3(centre(1),centre(2),centre(3),'*b')
        hold on
    end

    % Import vertice from stl file
    vertice=stlread(mesh2);
    n_vertice=length(vertice);
    x=vertice(:,1);
    y=vertice(:,2);
    z=vertice(:,3);

    % Calculation of the parameters
    M=zeros(4);

    for k=1:n_vertice
        v=[1 x(k) y(k) z(k)];
        M=M+v'*v;
    end

    B=zeros(4,1);
    for k=1:n_vertice
        p(k)=x(k)^2+y(k)^2+z(k)^2;
        B=B+[p(k);p(k)*x(k);p(k)*y(k);p(k)*z(k)];
    end

    centre=zeros(1,3);
    A=M\-B;
    centre(1)=-A(2)/2;
    centre(2)=-A(3)/2;
    centre(3)=-A(4)/2;
    R=sqrt(centre(1)^2+centre(2)^2+centre(3)^2-A(1));
    
    if draw==1
        %Draw points
        plot3(x,y,z,'.r')
        hold on

        %Draw sphere calculated
        [Sx,Sy,Sz] = sphere(20);
        surf(R*Sx+centre(1),R*Sy+centre(2),R*Sz+centre(3),'faceAlpha',0.3,'Facecolor','c')
        hold on
        plot3(centre(1),centre(2),centre(3),'*r')
    end
    
%%  Mode 2 
elseif mode==2
    %Sphere fitting with two different radius and one center

    % Import vertice from stl file
    vertice1=stlread(mesh1);
    vertice2=stlread(mesh2);
    n_vertice=min(length(vertice1),length(vertice2));

    x1=vertice1(:,1);
    y1=vertice1(:,2);
    z1=vertice1(:,3);

    x2=vertice2(:,1);
    y2=vertice2(:,2);
    z2=vertice2(:,3);

    % Calculation of the parameters
    M=zeros(5);

    for k=1:n_vertice
        v1=[1 x1(k) y1(k) z1(k) 0];
        v2=[0 x2(k) y2(k) z2(k) 1];
        M=M+v1'*v1+v2'*v2;
    end

    B=zeros(5,1);
    for k=1:n_vertice
        p1(k)=x1(k)^2+y1(k)^2+z1(k)^2;
        p2(k)=x2(k)^2+y2(k)^2+z2(k)^2;
        B=B+[p1(k);p1(k)*x1(k);p1(k)*y1(k);p1(k)*z1(k);0]+[0;p2(k)*x2(k);p2(k)*y2(k);p2(k)*z2(k);p2(k)];
    end


    A=inv(M)*-B;

    centre=zeros(1,3);
    centre(1)=-A(2)/2;
    centre(2)=-A(3)/2;
    centre(3)=-A(4)/2;

    radius1=sqrt(centre(1)^2+centre(2)^2+centre(3)^2-A(1));
    radius2=sqrt(centre(1)^2+centre(2)^2+centre(3)^2-A(5));
    
    if draw==1
        %Draw points
        figure
        plot3(x1,y1,z1,'.b')
        hold on
        plot3(x2,y2,z2,'.r')

        %Draw sphere calculated
        [Sx,Sy,Sz] = sphere(20);
        surf(radius1*Sx+centre(1),radius1*Sy+centre(2),radius1*Sz+centre(3),'faceAlpha',0.3,'Facecolor','c')
        hold on
        surf(radius2*Sx+centre(1),radius2*Sy+centre(2),radius2*Sz+centre(3),'faceAlpha',0.3,'Facecolor','m')
        hold on
        plot3(centre(1),centre(2),centre(3),'*b')
    end
 
%% Mode 3
elseif mode==3
    % Sphere fitting with one mesh
    % Import vertice from stl file
    
    vertice=stlread(mesh1);
    n_vertice=length(vertice);
    x=vertice(:,1);
    y=vertice(:,2);
    z=vertice(:,3);

    % Calculation of the parameters
    M=zeros(4);

    for k=1:n_vertice
        v=[1 x(k) y(k) z(k)];
        M=M+v'*v;
    end

    B=zeros(4,1);
    for k=1:n_vertice
        p(k)=x(k)^2+y(k)^2+z(k)^2;
        B=B+[p(k);p(k)*x(k);p(k)*y(k);p(k)*z(k)];
    end

    centre=zeros(1,3);
    A=M\-B;
    centre(1)=-A(2)/2;
    centre(2)=-A(3)/2;
    centre(3)=-A(4)/2;
    R=sqrt(centre(1)^2+centre(2)^2+centre(3)^2-A(1));

    if draw==1
        figure
        %Draw points
        plot3(x,y,z,'.b')
        hold on

        %Draw sphere calculated
        [Sx,Sy,Sz] = sphere(20);
        surf(R*Sx+centre(1),R*Sy+centre(2),R*Sz+centre(3),'faceAlpha',0.3,'Facecolor','c')
        hold on
        plot3(centre(1),centre(2),centre(3),'*r')
        hold on
    end
end

end

