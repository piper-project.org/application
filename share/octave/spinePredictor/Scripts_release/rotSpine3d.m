function [systemRot,R,thetaz] = rotSpine3d(locSystem)
% Returns the cell of local Coordinate System (CS) so that the spine is 
% along the x-axis (absolute Coordinate System) facing up (rotation with R 
% matrix) and so that the L5 and C7 are both on the x-axis (thetaz 
% rotation).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    
    orig = locSystem{1}(1,:); % L5 location.
    for i=1:size(locSystem,1)
        locSystem{i}(1,:) = locSystem{i}(1,:) - orig; % Translation so that L5 is at the origin.
    end
	R = [0 1 0; 1 0 0; 0 0 -1]'*locSystem{1}(2:end,:); % Rotation that makes the first local CS as Yloc=X, Zloc=-Z and Xloc=Y.
    
    locSystem1 = cell(size(locSystem));
    for i=1:size(locSystem,1)
        locSystem1{i} = zeros(size(locSystem{i}));
        for j=1:size(locSystem{i},1)
            locSystem1{i}(j,:) = (R*locSystem{i}(j,:)')'; % First rotation.
        end
    end
    
    V = locSystem1{end}(1,:);
    thetaz = atan2(-V(2),V(1)); % Rotation angle along z in order to place the final point of the spine on the x-z plane
    
    systemRot = cell(size(locSystem));
    for i=1:size(locSystem,1)
        for j=1:size(locSystem{1},1)
            systemRot{i}(j,:) = rot3d(locSystem1{i}(j,:),[0,0,thetaz]); % Second rotation.
        end
    end
    
end



