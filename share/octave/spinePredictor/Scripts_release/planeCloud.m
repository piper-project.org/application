function [centre,normal,d2]=planeCloud(p)
%
% Fits a plane through a cloud of points, with a least sum of square  
%     of distances between the points and the plane.
%
% [centre,normal,d2]=planeCloud(p)
%
% input: p, coordinates of points, 
%              p(i,1), x coordinate of point i
%              p(i,2), y coordinate of point i
%              p(i,3), z coordinate of point i.
% output: centre, one of the points of the plane (chosen as the centre 
%                    of gravity of the points).
%         normal, normal to the plane.
%         d2, sum of squares of distances.
%
% Copyright (C) 2017 U Southampton
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Code by Christophe Lecomte (CL), SOTON, Summer 2015.
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
centre=mean(p,1);
%
s=p-repmat(centre,size(p,1),1);
%
M=s.'*s;
[N,D]=eig(M);
%
d2=D(1,1);
normal=N(:,1);
%
