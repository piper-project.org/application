function [systemTarget,splineRef1, splineParameters, systemTSacr, systemTC0]=initialPositioning_light_wp3(systemPath)
% Performs a positioning of spine BCS in the reference posture (default = standing erect)

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
%% Path to the main files
currentdir = pwd;
if ~exist('systemPath','var')
    systemPath = [currentdir '/../Datas/CS.dat']; % Default spine coordinate system path.
end

%% Reference spline parameters
N = 1000; % Number of points in each spline (thoraco-lumbar and cervical).
sL5 = 15;
tL5 = 1;
sC7 = 29;
tC7 = 0.75;
sC1 = 0;
tC1 = 1;
htl = 4.46E+02;
hc = 97.59;
thetac = 15;

%% N.B. Any reference posture may be chosen and these data can be extracted from the transformation parameters
% TransformationParamPath = [currentdir '/../Datas/TransformationParameters.xlsx']; % Path to the transformation parameters xls file.
% TransformationParam = readTransfoParamxls(TransformationParamPath); % All the parameters needed for the spline transformation from one posture to another.

%% Creation of the bodyspine and the coordinate system associated
[systemVert, systemVSacr, systemVC0] = csfile2system(systemPath);
l = lengthcurve(systemOrig(systemVert)); % Length of the spine.

%% Check plots n�1 (Input CS)
% figure('Name','Input system and reference spline')
% hold on
% systemPlot(systemVert,20)
% view(3)
% axis equal
% systemPlot(systemVSacr,20) % Comment if not present
% systemPlot(systemVC0,20) % Comment if not present

%% Create a reference spline that has the same length as the input spine segment
fprintf('Creating the reference spline...\n');
dh = 1; % Correction for the new spline to be the same length as the original spine.
ltarget = 0;
n = 0; % Number of loops.
delta = 0.5;
cond1 = 0;
cond2 = 0;
while abs(ltarget-l)/l > 0.0001 % 0.01% error tolerated on the spline's length.
    n = n+1;
    Xtl = {{[0,0] sL5 tL5},{[htl*dh 0] sC7 tC7}}; % Spline parameters for the thoraco-lumbar part.
    Xc = {{[htl*dh 0] sC7 tC7},{[(htl+hc*cos(thetac*pi/180))*dh (hc*sin(thetac*pi/180))*dh] sC1 tC1}}; % Spline parameters for the cervical part.
    Qtl = hobbysplines(Xtl,N,'cycle',false); % Thoraco-lumbar spline.
    Qc = hobbysplines(Xc,N,'cycle',false); % Cervical spline.
    ltarget = lengthcurve([Qtl;Qc(2:end,:)]);
    if ltarget > l
        cond1 = 1;
        if cond2 == 1
            delta = delta/2;
            cond2 = 0;
        end
        dh = dh-delta;
    else
        cond2 = 1;
        if cond1 == 1
            delta = delta/2;
            cond1 = 0;
        end
        dh = dh+delta;    
    end
end
splineRef = [Qtl;Qc(2:end,:)]; % Concatenation of Thoraco-lumbar and cervical splines
fprintf('Done.\n');

%% Check plots n�1bis (Reference splines in local L5C7 CS)
% plot3(Qtl(:,1),Qtl(:,2),Qtl(:,3),'LineWidth',2)
% plot3(Qc(:,1),Qc(:,2),Qc(:,3),'LineWidth',2)

%% Creation of the transformed spine's CS back in its initial position in the Global CS
[systemTarget,] = spline2vert(systemOrig(systemVert),splineRef,0,0);
R = rotMatrix(systemTarget{1},systemVert{1}); % Rotation between CSs in the L5-C7 X-Y CS where the spline was created, and the initial CS (Global)
systemTarget = rotSystem(systemTarget,R);
systemTarget = transSystem(systemTarget,systemVert{1}(1,:));
splineRef1 = translation((R*splineRef')',systemVert{1}(1,:));
splineParameters = {sL5, tL5, sC7, tC7, sC1, tC1, htl, hc, thetac, R}; % Spline parameters are updated. They are now the same as the Reference Spline parameters

%% Specific case of C0 and Sacrum/Hip bones (if any)
% C0 is rigidly linked to C1. C0 rotations and constraints (such as e.g.
% keeping eyesight horizontal) should not be processed here but by either
% direct or inverse kinematics modules
if  isempty(systemVC0{1}) == false
    RC1_ini2trgt = rotMatrix(systemVert{end},systemTarget{end});
    vectC12C0_ini = systemVC0{1}(1,:) - systemVert{end}(1,:);
    systemTC0 = rotSystem(systemVC0,RC1_ini2trgt);
    systemTC0{1}(1,:) = systemTarget{end}(1,:) + (RC1_ini2trgt*vectC12C0_ini')' ; 
else systemTC0 = systemVC0;
end
systemTSacr = systemVSacr; % assumes that there always is a CS for the Sacrum

%% Check plots n�2 vertebral CS and spline before and after transformation
% figure('Name','Input system and traget after initial positioning in reference posture')
% hold on
% systemPlot(systemVert,20)
% view(3)
% axis equal
% systemPlot(systemTarget,20)
% plot3(splineRef1(:,1),splineRef1(:,2),splineRef1(:,3),'LineWidth',2)
% systemPlot(systemTC0,20); % Comment if not present
% systemPlot(systemTSacr,20); % Comment if not present

end