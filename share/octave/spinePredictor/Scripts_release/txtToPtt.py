# Copyright (C) 2017 UCBL-IFSTTAR
# 
# This file is part of the PIPER Framework
#
# The PIPER Framework is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# The PIPER Framework is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 
# Contributors: 
# Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
# Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
# 
# This work has received funding from the European Union Seventh Framework 
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
#
# Contact:
# www.piper-project.org

import sys
import math
import numpy as np
import xml.etree.ElementTree as ET

print "Processing",sys.argv[1],"..."

#vertebraName = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10", "T11", "T12", "L1", "L2", "L3", "L4", "L5"]
dofName = ["x", "y", "z", "rx", "ry", "rz"]

rootXml = ET.Element("piper-target")
rootXml.set("version", "0.6")
targetListXml = ET.SubElement(rootXml, "targetList")


with open(sys.argv[1], 'r') as txtFile:
    for lineStr in txtFile:
        if "#" == lineStr[0].strip():
            continue
        line = lineStr.strip().split()
        name = line[0]
        print "Vertebra",name
        origin = np.array(map(float,line[1:4]))
        
        rotMat = np.array(map(float,line[4:13])).reshape(3,3)
        
        theta = math.acos( 0.5*(np.trace(rotMat)-1.) )
        rotVec = np.array([
            (rotMat[2,1]-rotMat[1,2]) / (2*math.sin(theta)) ,
            (rotMat[0,2]-rotMat[2,0]) / (2*math.sin(theta)) ,
            (rotMat[1,0]-rotMat[0,1]) / (2*math.sin(theta)) ,
            ])
        rotVec *= theta
        print origin, rotMat, theta, rotVec
        targetXml = ET.SubElement(targetListXml, "target")
        targetXml.set("type","FrameToFrame")
        nameXml = ET.SubElement(targetXml, "name")
        nameXml.text = "W_Origin_to_"+name+"_frame"
        unitsXml = ET.SubElement(targetXml, "units")
        unitsXml.set("length","mm")
        
        for i in range(0,3):
            dofXml = ET.SubElement(targetXml, "value")
            dofXml.set("dof",dofName[i])
            dofXml.text = str(origin[i])
        for i in range(3,6):
            dofXml = ET.SubElement(targetXml, "value")
            dofXml.set("dof",dofName[i])
            dofXml.text = str(rotVec[i-3])
        
        frameSourceXml = ET.SubElement(targetXml, "frameSource")
        frameSourceXml.text = "W_Origin"
        frameTargetXml = ET.SubElement(targetXml, "frameTarget")
        frameTargetXml.text = name+"_frame"
        
        
ET.ElementTree(rootXml).write("output.ptt")

