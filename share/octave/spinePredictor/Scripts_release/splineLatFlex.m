function QlatFlex = splineLatFlex(Q,latFlexTL,latFlexC)
% Returns a spline representing the initial spline Q (spine in a sagittal
% posture) with lateral flexion defined by latFlexTL (lateral angle of the
% thoraco-lumbar part of the spine, L5-C7) and latFlexC (lateral angle of 
% the cervical part of the spine, C7-C1).

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

    fprintf('Applying lateral flexion...\n');
    
    %% Original spline's parameters
    N = size(Q(1:ceil(end/2),:),1); % Number of points composing the thoraco-lumbar part.
    ltl = lengthcurve(Q(1:ceil(end/2),:)); % Length of the thoraco-lumbar part.
    lc = lengthcurve(Q(ceil(end/2)+1:end,:)); % Length of the cervical part.
    
    %% Rotating the spline to make L5-C7 the x-axis
    theta = atan2(Q(ceil(end/2),2)-Q(1,2),Q(ceil(end/2),1)-Q(1,1));
    Q = rot3d(Q,[0 0 -theta]);
    
    %% Parameters for the frontal plane spline (defining the lateral bending)
    thetaLatTLmax = 27.7443; % Maximum angle of the thoraco-lumbar part.
    thetaLatCmax = 7.4339; % Maximum angle of the cervical part.
    sC7max = 52.7199; % Maximum C7 slope corresponding to thetaLatTLmax.
    sC1max = 24.0164; % Maximum C1 slope corresponding to thetaLatCmax.
    
    sL5 = 0;
    sC7 = sC7max*latFlexTL/thetaLatTLmax;
    sC1 = sC1max*latFlexC/thetaLatCmax;
    tL5 = 1;
    tC7 = 1;
    tC1 = 1;
    htl = Q(ceil(end/2),1);
    hc = Q(end,1) - htl;
    
    %% Thoracolumbar latreral flexion
    dh = 1; % Correction for the new spline to be the same length as the original one.
    ltarget = 0;
    n = 0; % Number of loop.
    Xtl = {{[0,0] sL5 tL5},{[htl htl*sin(latFlexTL*pi/180)] sC7 tC7}}; % Spline parameters for the thoraco-lumbar part.
    Qtl = hobbysplines(Xtl,N,'cycle',false); % Transformation without angle between original and transformed sacrum.
    Qtl = sample(Qtl,Q(1:ceil(end/2),1));
    
    
    %% Cervical lateral flexion
    Xc = {{[0,0] 0 1},{[hc hc*sin(latFlexC*pi/180)] sC1 tC1}}; % Spline parameters for the thoraco-lumbar part.
    Qc = hobbysplines(Xc,N,'cycle',false); % Transformation without angle between original and transformed sacrum.
    thetatl = atan2(Qtl(end,2)-Qtl(end-1,2),Qtl(end,1)-Qtl(end-1,1)); % Thoraco-lumbar angle
    Qorig = Q(ceil(end/2)+1:end,:); % Qorig is the cervical part of the original spline.
    Qorig = translation(Qorig,-Qorig(1,:));
    Qc = sample(Qc(2:end,:),Qorig(:,1));
    Qc = [Qorig(:,1),Qorig(:,2),Qc(:,2)];
    vect = cross(Q(ceil(end/2),:)-Q(ceil(end/2)-1,:),[0 0 1]);
    vect = vect/norm(vect);
    Qc = rotvect(Qc,vect,thetatl,Qorig(1,:));
    Qc = translation(Qc,Qtl(end,:));
    
    
    %% Verifying the length of the spine
    QlatFlexTL1 = [Q(1:ceil(end/2),1),Q(1:ceil(end/2),2),Qtl(:,2)];
    dh = 1; % Correction for the new spline to be the same length as the original one.
    ltarget = 0;
    n = 0; % Number of loop.
    delta = 0.5;
    cond1 = 0;
    cond2 = 0;
    while abs(ltarget-ltl)/ltl > 0.0001 % 0.01% error tolerated on the spline's length.
        n = n+1;
        QlatFlexTL = QlatFlexTL1.*dh;
        ltarget = lengthcurve(QlatFlexTL);
        if ltarget > ltl
            cond1 = 1;
            if cond2 == 1
                delta = delta/2;
                cond2 = 0;
            end
            dh = dh-delta;
        else
            cond2 = 1;
            if cond1 == 1
                delta = delta/2;
                cond1 = 0;
            end
            dh = dh+delta;
        end
    end
    
    QlatFlexC1 = Qc;
    dh = 1; % Correction for the new spline to be the same length as the original one.
    ltarget = 0;
    n = 0; % Number of loop.
    delta = 0.5;
    cond1 = 0;
    cond2 = 0;
    while abs(ltarget-lc)/lc > 0.0001 % 0.01% error tolerated on the spline's length.
        n = n+1;
        QlatFlexC = translation(QlatFlexC1,-QlatFlexC1(1,:));
        QlatFlexC = QlatFlexC.*dh;
        QlatFlexC = translation(QlatFlexC,QlatFlexTL(end,:));
        ltarget = lengthcurve(QlatFlexC);
        if ltarget > lc
            cond1 = 1;
            if cond2 == 1
                delta = delta/2;
                cond2 = 0;
            end
            dh = dh-delta;
        else
            cond2 = 1;
            if cond1 == 1
                delta = delta/2;
                cond1 = 0;
            end
            dh = dh+delta;
        end
    end
    
    
    %% Assembling the cervical and thoraco-lumbar part
    QlatFlex = [QlatFlexTL; QlatFlexC];
    QlatFlex(2,:) = Q(2,:); % For first local CS correction.
    QlatFlex(3,:) = Q(3,:); % For first local CS correction.
    QlatFlex = rot3d(QlatFlex,[0 0 theta]);
    
    fprintf('Done.\n');
    
end