function bodyspine = bodyspinewrite(outdir,bodyspine)
% Writes the vertebrae stl files, the paints and the landmarks of the spine
% from bodyspine in the directory 'outdir' and returns a bodyspine with
% the right paths to create the master file.
% The hierarchy of the file written are the same of the name of the part in
% the xml master file. For example if the name of the part is 'spine' then
% the part will be written in 'outdir\spine\' and if the name of the part
% is spine\paint, the part will be written in 'outdir\spine\paint\'. Their
% landmark's file associated is written in the subdirectory 'landmarks'.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%
    fprintf('Writing the stl and landmarks files...\n');
    for i=1:size(bodyspine,1)
        if ~exist([outdir bodyspine(i).part],'dir')
            mkdir([outdir bodyspine(i).part]);
        end
        if ~isempty(bodyspine(i).vertices)
            stlwrite([outdir bodyspine(i).part '\' bodyspine(i).name '.bin.stl'], bodyspine(i).faces, bodyspine(i).vertices);
            bodyspine(i).path = [outdir bodyspine(i).part '\' bodyspine(i).name '.bin.stl'];
        end
        if ~isempty(bodyspine(i).landmarks)
            if ~exist([outdir bodyspine(i).part '\landmarks\'],'dir')
                mkdir([outdir bodyspine(i).part '\landmarks\']);
            end
            if exist('OCTAVE_VERSION', 'builtin')
                currentdir = pwd;
                cd([outdir bodyspine(i).part '\landmarks\']);
            end
            xlswrite([outdir bodyspine(i).part '\landmarks\' bodyspine(i).name '_landmarks.xlsx'], bodyspine(i).landmarks(1:end,:));
            if exist('OCTAVE_VERSION', 'builtin')
                cd(currentdir);
            end
        end
        if ~isempty(bodyspine(i).extra)
        for j=1:size(bodyspine(i).extra,1)
            if ~exist([outdir bodyspine(i).extra(j).part],'dir')
                mkdir([outdir bodyspine(i).extra(j).part]);
            end
            if ~isempty(bodyspine(i).extra(j).vertices)
                stlwrite([outdir bodyspine(i).extra(j).part '\' bodyspine(i).extra(j).name '.stl'], bodyspine(i).extra(j).faces, bodyspine(i).extra(j).vertices);
                bodyspine(i).extra(j).path = [outdir bodyspine(i).extra(j).part '\' bodyspine(i).extra(j).name '.stl'];
            end
            if ~isempty(bodyspine(i).extra(j).landmarks)
                if ~exist([outdir bodyspine(i).extra(j).part '\landmarks\'],'dir')
                    mkdir([outdir bodyspine(i).extra(j).part '\landmarks\']);
                end
                if exist('OCTAVE_VERSION', 'builtin')
                    currentdir = pwd;
                    cd([outdir bodyspine(i).extra(j).part '\landmarks\']);
                end
                xlswrite([outdir bodyspine(i).extra(j).part '\landmarks\' bodyspine(i).extra(j).name '_landmarks.xlsx'], bodyspine(i).extra(j).landmarks(1:end,:));
                if exist('OCTAVE_VERSION', 'builtin')
                    cd(currentdir);
                end
            end
            if ~isempty(bodyspine(i).extra(j).extra)
            for k=1:size(bodyspine(i).extra(j).extra,1)
                if ~exist([outdir bodyspine(i).extra(j).extra(k).part '\'],'dir')
                    mkdir([outdir bodyspine(i).extra(j).extra(k).part '\']);
                end
                if ~isempty(bodyspine(i).extra(j).extra(k).vertices)
                    stlwrite([outdir bodyspine(i).extra(j).extra(k).part '\' bodyspine(i).extra(j).extra(k).name '.stl'], bodyspine(i).extra(j).extra(k).faces, bodyspine(i).extra(j).extra(k).vertices);
                    bodyspine(i).extra(j).extra(k).path = [outdir bodyspine(i).extra(j).extra(k).part '\' bodyspine(i).extra(j).extra(k).name '.stl'];
                end
                if ~isempty(bodyspine(i).extra(j).extra(k).landmarks)
                    if ~exist([outdir bodyspine(i).extra(j).extra(k).part '\landmarks\'],'dir')
                        mkdir([outdir bodyspine(i).extra(j).extra(k).part '\landmarks\']);
                    end
                    if exist('OCTAVE_VERSION', 'builtin')
                        currentdir = pwd;
                        cd([outdir bodyspine(i).extra(j).extra(k).part '\landmarks\']);
                    end
                    xlswrite([outdir bodyspine(i).extra(j).extra(k).part '\landmarks\' bodyspine(i).extra(j).extra(k).name '_landmarks.xlsx'], bodyspine(i).extra(j).extra(k).landmarks(1:end,:));
                    if exist('OCTAVE_VERSION', 'builtin')
                        cd(currentdir);
                    end
                end
            end
            end
        end
        end
    end
    fprintf('Done.\n');
    
    

end