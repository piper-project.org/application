function []=spineTransformationcs_wp3(sourcePosture, targetPosture, alpha, systemRef, systemRefSacr, systemRefC0, splineRef, splineParameters, outdirusr, gravVectorPath, alignChoice, latFlexTL, latFlexC)

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org
%%

%% Constants
% For the spline fitting
N = 1000; % Number of points composing the splines (the higher the better).
err = 1/100; % Relative max error wanted (the lower the better). Not always respected if it cannot be achieved.

%% Path to the main files
currentdir = pwd;
if ~exist('gravVectorPath','var')
    gravVectorPath = [currentdir '/../tmp/gravity.dat']; % Default spine coordinate system path.
end
A = openfile(gravVectorPath,' ');
gravVector = [str2double(A{2}{1}),str2double(A{2}{2}),str2double(A{2}{3})];

% For the output directory
currentdir = pwd;
if ~exist('outdirusr','var')
    currentdir = pwd;
    outdir = [currentdir '/../TransformedSpine/']; % Default output directory.
else
    outdir = outdirusr;
end

% For the transformation parameters
TransformationParamPath = [currentdir '/../Datas/TransformationParameters.xlsx']; % Path to the transformation parameters xls file.

%% Reference, initial and final posture for regression
% Parameters used to define the final posture.
STANDING_ERECT = 1;         % Numbers must be the same as in the transformation parameters file.
STANDING_SLOUCH = 2;        % Since the released version 1.5, six physiological (based on validated postural parameters)
SEATED_ERECT = 3;           % postures are available. The user can modify/create new postures
SUPINE = 4;                 % by modifying the transformation parameters file.
FWDFLEX = 5;                % Be careful that posture's number must be the same in the file and in this script.
SEATED_DRIVING = 6;

posture_ref = STANDING_ERECT; % Choose the reference posture (this is the posture of the reference PIPER model.
% The positioning_spineOnly.m script allows creating other postures).
% STANDING_ERECT is the default reference posture obtained.

posture_ini = sourcePosture;  % Choose the initial target posture here.

posture_target = targetPosture;  % Choose the final target posture here.

weight = alpha;     % Define the weight (between 0 and 1) for the output posture 
% as a percentage of linear interpolation between the source (initial) and target postures.
% Extrapolation is possible below 0 and above 1. It is advised to keep those within -0.2 and 1.2 


%% Choose the mode and smoothing for the spline's coordinate system
MODE = 0; % For spline2vert function in positioningTest_v3_spineOnly.
% Useless to modify for sagittal transformation.
% 0 to forbid axial rotation of the vertebrae along the
% transformation of the spline due to Frenet's coordinate system.
% 1 to allow them.

SMOOTH = 0; % For spline2vert function in positioningTest_v3_spineOnly.
% Useless to modify for sagittal transformation.
% Number of smothing loops (useful if MODE = 1).


%% Choose the sight vector (direction of the head) (NOT USED ANYMORE)
if exist('sightVectorusr','var')
    sightVector = sightVectorusr;
end

%% Precision of the spline fitting
if ~exist('IKMODE','var')
    IKMODE = 2; % Mode of the inverse kinematics tool (1 : quick search/cervicals only (not precise), 2 : quick search/thoraco-lumbar and cervicals, 3 : precise (long calculations))
end


%% Reading the input spine's CS file. 
% When used as part of the spine predictor tool, i.e. after the
% initialPositioning_light_wp3.m script, this is also by default the chosen
% reference posture.
system0 = systemRef;

%% Check plots n�3 CS before transformation
% figure('Name','System input before transformation')
% hold on
% systemPlot(system0,20)
% systemPlot(systemRefC0,20) % Comment if not present
% systemPlot(systemRefSacr,20) % Comment if not present
% view(3)
% axis equal

%% Definition of the spline for the input posture
Q = splineRef;
sL5 = splineParameters{1};
tL5 = splineParameters{2};
sC7 = splineParameters{3};
tC7 = splineParameters{4};
sC1 = splineParameters{5};
tC1 = splineParameters{6};
htl = splineParameters{7};
hc = splineParameters{8};
thetac = splineParameters{9};
orig = system0{1}(1,:); % Location of L5.
l = lengthcurve(Q); % Total length of the spline.

%% Account for user-chosen alignment of the output
systemGlob{1} = systemRef{1};
systemGlob{1}(2,:) = [1 0 0]; systemGlob{1}(3,:) = [0 1 0]; systemGlob{1}(4,:) = [0 0 1];
switch alignChoice
    case 1
        R0 = splineParameters{10};
        %% If we choose to align the target spline to gravity vector and Ant-Pos L5 vector
        systemGrav{1} = systemRef{1}; % systemGrav will be the CS aligned to gravity vector and Ant-Pos L5 vector
        systemGrav{1}(3,:) = -gravVector/norm(gravVector); % local L5 Y should be aligned to -1 x gravity vector
        if norm(cross(systemRef{1}(2,:),systemGrav{1}(3,:))) < 0.005
            systemGrav{1}(2,:) = cross(systemGrav{1}(3,:),systemGrav{1}(4,:)); % local X
            systemGrav{1}(2,:) = systemGrav{1}(2,:)/norm(systemGrav{1}(2,:));
        elseif norm(cross(systemRef{1}(4,:),systemGrav{1}(3,:))) < 0.005
            systemGrav{1}(4,:) = cross(systemGrav{1}(2,:),systemGrav{1}(3,:)); % local Z
            systemGrav{1}(4,:) = systemGrav{1}(4,:)/norm(systemGrav{1}(4,:));
        else
            systemGrav{1}(4,:) = cross(systemRef{1}(2,:),systemGrav{1}(3,:)); % local Z aligned perpendicular to X and Y
            systemGrav{1}(4,:) = systemGrav{1}(4,:)/norm(systemGrav{1}(4,:));
            systemGrav{1}(2,:) = cross(systemGrav{1}(3,:),systemGrav{1}(4,:)); % local X
            systemGrav{1}(2,:) = systemGrav{1}(2,:)/norm(systemGrav{1}(2,:));
        end
        %% If we choose to align the target spline to gravity vector and Med-Lat L5 vector
%         systemGrav{1} = systemRef{1}; % systemGrav will be the CS aligned to gravity vector and Med-Lat L5 vector
%         systemGrav{1}(3,:) = -gravVector/norm(gravVector); % local L5 Y should be aligned to -1 x gravity vector
%         if norm(cross(systemRef{1}(2,:),systemGrav{1}(3,:))) < 0.005
%             systemGrav{1}(2,:) = cross(systemGrav{1}(3,:),systemGrav{1}(4,:)); % local X
%             systemGrav{1}(2,:) = systemGrav{1}(2,:)/norm(systemGrav{1}(2,:));
%         elseif norm(cross(systemRef{1}(4,:),systemGrav{1}(3,:))) < 0.005
%             systemGrav{1}(4,:) = cross(systemGrav{1}(2,:),systemGrav{1}(3,:)); % local Z
%             systemGrav{1}(4,:) = systemGrav{1}(4,:)/norm(systemGrav{1}(4,:));
%         else
%             systemGrav{1}(2,:) = cross(systemRef{1}(3,:),systemGrav{1}(4,:)); % local X aligned perpendicular to X and Y
%             systemGrav{1}(2,:) = systemGrav{1}(2,:)/norm(systemGrav{1}(2,:));             
%             systemGrav{1}(4,:) = cross(systemGrav{1}(2,:),systemGrav{1}(3,:)); % local Z
%             systemGrav{1}(4,:) = systemGrav{1}(4,:)/norm(systemGrav{1}(4,:));
%             
%         end
        %%
        R = rotMatrix(systemRef{1},systemGrav{1})*R0;
    otherwise % includes case 0; in this case the output CS of L5 is aligned to the input CS of L5
        R = splineParameters{10}; % Rotation between CSs in the L5-C7 X-Y CS and the initial CS
end

%% Transformation of the original spine and rotation/translation of the created spline (or else,
% the first and last point of the spline are aligned with x axis)
% The spline is created so that L5 and C7 are on the x-axis. Then it is
% rotated in function of the original spine and finally it is translated so
% that the transformed spline's origin is the same as the original spline's.
TransformationParam = readTransfoParamxls(TransformationParamPath); % All the parameters needed for the spline transformation from one posture to another.
Qt1 = splineTransfo(sL5,sC7,sC1,tL5,tC7,tC1,htl,hc,l,thetac,weight,posture_ref,posture_ini,posture_target,TransformationParam);
if exist('latFlexTL','var') || exist('latFlexC','var')
    Qt = splineLatFlex(Qt1,latFlexTL,latFlexC);
else
    Qt = Qt1;
end
Qtransf = translation((R*Qt')',orig); 

%% Creation of the vertebrae location on the spline
% The vertebrae location are created in function of the original distance
% between each vertebrae. This distance is conserved by the transformation.
% The local systems are bound to the spline (these are not the local system
% of the vertebrae), to ensure continuity of the spine.
[systemS,] = spline2vert(systemOrig(system0),Q);
if exist('latFlexTL','var') || exist('latFlexC','var')
    [~,inflexpoint] = spline2vert(systemOrig(system0),Qt1); % Detection of inflexion points in the lateral flexion free spline.
    [systemStransf,] = spline2vert(systemOrig(system0),Qtransf,0,0,inflexpoint);
else
    [systemStransf,] = spline2vert(systemOrig(system0),Qtransf,0,0);
end

%% Check plots n�4 Transformed splines before accounting for pelvis angle
% figure('Name','Transformed splines before accounting for pelvis angle')
% hold on
% systemPlot(system0,20)
% view(3)
% axis equal
% plot3(Q(:,1),Q(:,2),Q(:,3),'LineWidth',2)
% systemPlot(systemS,20)
% systemPlot(systemStransf,20)
% plot3(Qt(:,1),Qt(:,2),Qt(:,3),'LineWidth',2)
% plot3(Qt1(:,1),Qt1(:,2),Qt1(:,3),'LineWidth',2)
% plot3(Qtransf(:,1),Qtransf(:,2),Qtransf(:,3),'LineWidth',2)

%% Creation of the transformed vertebrae local systems.
% Based on the spline's local systems. The rotation between the spline's
% and the vertebrae's local systems are the same as the reference posture's
% ones.
Rstand = cell(size(systemS));
for i=1:size(systemS,1)
    Rstand{i} = rotMatrix(systemS{i},system0{i});   % Rotation matrix between the standing spline's local systems
                                                    % and the standing vertebrae local systems.
end
systemOrig1 = system0; % Backup of the original system.
system0 = vert2system (system0,systemS,Rstand);
systemTransf = vert2system(system0,systemStransf,Rstand);

%% Check plots n�5 after reconstruction of vertebrae local CS from spline
% figure('Name','Check after reconstruction of vertebrae local CS from spline')
% hold on
% systemPlot(system0,20)
% view(3)
% axis equal
% systemPlot(systemOrig1,20)
% systemPlot(system0,20)
% systemPlot(systemTransf,20)
% plot3(Qt(:,1),Qt(:,2),Qt(:,3),'LineWidth',2)
% plot3(Qtransf(:,1),Qtransf(:,2),Qtransf(:,3),'LineWidth',2)

%% Y axis rotation correction (For a sagittal plane rotation only) (NOT USED ANYMORE)
% If this is not done, there will probably be some unrealistic rotation
% applied to the vertebrae if the original spine is not aligned perfectly
% in a plane.
system1 = system0;
systemTransf1 = systemTransf;
% for i=1:size(system0,1)
%     system1{i}(4,:) = system{1}(4,:);
%     system1{i}(2,:) = cross(system1{i}(3,:),system1{i}(4,:))/norm(cross(system1{i}(3,:),system1{i}(4,:)));
%     system1{i}(3,:) = cross(system1{i}(4,:),system1{i}(2,:))/norm(cross(system1{i}(4,:),system1{i}(2,:)));
%     systemTransf1{i}(4,:) = system{1}(4,:);
%     systemTransf1{i}(2,:) = cross(systemTransf1{i}(3,:),systemTransf1{i}(4,:))/norm(cross(systemTransf1{i}(3,:),systemTransf1{i}(4,:)));
%     systemTransf1{i}(3,:) = cross(systemTransf1{i}(4,:),systemTransf1{i}(2,:))/norm(cross(systemTransf1{i}(4,:),systemTransf1{i}(2,:)));
% end

%% Axial rotation and lateral flexion with Inverse Kinematics (NOT USED ANYMORE)
if exist('sightVectorusr','var')
    systemTransf1 = pelvisHeadIK(systemTransf1,sightVector,IKMODE);
end

%% Transformation back in L5's initial (from input) local BCS / with correct absolute angle
switch alignChoice
    case 0
        RL5Current2World = rotMatrix(system0{1},systemGlob{1});
        RL5Target2World = rotMatrix(systemTransf{1},systemGlob{1});
        RBack2L5Current = RL5Target2World'*RL5Current2World;
        systemTransflocalL5 = rotSystem(systemTransf,RBack2L5Current');
        vectback2L5Current = systemTransf{1}(1,:) - systemTransflocalL5{1}(1,:);
        systemTransf1 = transSystem(systemTransflocalL5,vectback2L5Current);
%% Wip based on fitting spline 
% Will not work to extract spline postural spline parameters out of the
% sagittal plane (which actually makes sense)
        if exist('latFlexTL','var') || exist('latFlexC','var')
            if latFlexTL ~= 0
                for i=1:24
                    points{1,i} = {systemTransf1{i,1}(1,:)};
                end
                Qtransf = hobbysplines0(points);
            elseif latFlexC ~= 0
                for i=1:24
                    points{1,i} = {systemTransf1{i,1}(1,:)};
                end
                Qtransf = hobbysplines0(points);
            else
                [Qtransf, sL5save, tL5save, sC7save, tC7save, sC1save, tC1save, htl, hc, thetac, R, thetaz, errsave] = fitSpline(systemTransf1, 1000, 0.0001, 18) % Works but slow
            end
        end
    case 1
        PassageGrav = rotMatrix(systemGrav{1},systemGlob{1});
        systemTransfTemp1 = rotSystem(systemTransf,PassageGrav);
        systemTransfTemp2 = rotSystemAngle(systemTransfTemp1,[0,0,-(sL5+TransformationParam(10,posture_ref)-90)*pi/180]);
        systemTransfTemp3 = rotSystem(systemTransfTemp2,PassageGrav');  
        vectback2L5Current = systemTransf{1}(1,:) - systemTransfTemp3{1}(1,:);
        systemTransf1 = transSystem(systemTransfTemp3,vectback2L5Current);        
        CorrectRot = rotMatrix(systemTransf{1},systemTransfTemp3{1});
        Qtransf = translation((CorrectRot*Qtransf')',vectback2L5Current);
end

%% Case of Sacrum and Skull(C0)
if  isempty(systemRefSacr{1}) == false
    RL5_ini2trgt = rotMatrix(systemRef{1},systemTransf1{1});
    vectL52Sacr_ini = systemRefSacr{1}(1,:) - systemRef{1}(1,:);
    systemSacr = rotSystem(systemRefSacr,RL5_ini2trgt);
    systemSacr{1}(1,:) = systemTransf1{1}(1,:) + (RL5_ini2trgt*vectL52Sacr_ini')';
else systemSacr = systemRefSacr;
end

if  isempty(systemRefC0{1}) == false
    RC1_ini2trgt = rotMatrix(systemRef{end},systemTransf1{end});
    vectC12C0_ini = systemRefC0{1}(1,:) - systemRef{end}(1,:);
    systemC0 = rotSystem(systemRefC0,RC1_ini2trgt);
    systemC0{1}(1,:) = systemTransf1{end}(1,:) + (RC1_ini2trgt*vectC12C0_ini')' ;
else systemC0 = systemRefC0;
end

%% Check plots n�6
% figure('Name','system before and after transformation + correction for pelvis angle')
% hold on
% systemPlot(system0,20)
% view(3)
% axis equal
% systemPlot(systemOrig1,20)
% systemPlot(systemTransf1,20)
% systemPlot(systemRefSacr,20)
% systemPlot(systemTransflocalL5,20) % Comment if not present
% systemPlot(systemRefC0,20) % Comment if not present
% systemPlot(systemSacr,20) % Comment if not present
% systemPlot(systemC0,20) % Comment if not present
% if exist('systemGlob','var')
%    systemPlot(systemGlob,100);
% end
% if exist('systemGrav','var')
%    systemPlot(systemGrav,100);
% end
% plot3(Qt(:,1),Qt(:,2),Qt(:,3),'LineWidth',2)
% plot3(Qt1(:,1),Qt1(:,2),Qt1(:,3),'LineWidth',2)
% plot3(Qtransf(:,1),Qtransf(:,2),Qtransf(:,3),'LineWidth',2)
%% Vertebrae, landmarks, etc. writing
systemwrite([outdir 'CS.dat'], systemTransf1, systemSacr, systemC0); % Writing the local CS.
dlmwrite([outdir 'splinePoints.dat'], sampleDist(Qtransf,lengthcurve(Qtransf)/1000)); % Writing of the spline's points to a file.
end


