function TransformationParam = readTransfoParamxls(TransformationParamPath)
% Returns a double array of all the postured spline parameters contained in
% the TransformationParamPath file. The column is the ID of the posture.

% Copyright (C) 2017 UCBL-IFSTTAR
% 
% This file is part of the PIPER Framework
%
% The PIPER Framework is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
% 
% Contributors: 
% Marc Gardegaront, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% Bertrand Fr�ch�de, LBMC, Universit� Claude Bernard Lyon 1 / IFSTTAR
% 
% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
%
% Contact:
% www.piper-project.org

    [~,~,TransformationParamFile] = xlsread(TransformationParamPath);
    
    compte=0;
    for lines=1:size(TransformationParamFile,1)
            if strfind(TransformationParamFile{lines,1},'#') == 1
                compte=compte+1;
            end
    end
    
    TransformationParam = zeros(lines-compte-2,size(TransformationParamFile,2)-1);
    for i=1:lines-compte-2
        for j=1:size(TransformationParamFile,2)-1
            TransformationParam(i,j) = TransformationParamFile{i+compte+2,j+1};
        end
    end
    
end