Use the files in 'Scripts_release' folder.


%%% MAIN SCRIPTS DESCRITPION %%%
A demonstration script is available as main.m.

The 'InitialPositioning_light_wp3.m' script is used to create a positioned spine from a random postured spine. 
With the default values, the initial spine is positioned in a 'standing erect' posture.

The 'spineTransformationcs_wp3.m' script is used to transform a spine in the available postures (described 
in the xlsx file 'TransformationParameters.xlsx') from a spine in a reference posture ('standing erect' for example).
You can create a spine in reference posture from a random postured spine by using the 'InitialPositioning_light_wp3.m' script.

%%% FOLDER DESCRIPTION %%%
'Datas' folder contains the initial spine master file, and the transformation parameter file used for the spine 
transformation toward other posture.

'Doc' folder contains some documentation on the scripts

%%% MAIN FILES DESCRIPTION %%%
The 'TransformationParameters' file is a list of datas corresponding to the postured splines. 
You can modify the existing ones or add new by simply respecting these rules :
	The first column and the two first rows are not important for the scripts. They are the name of the parameters, the name of the posture and the ID of the posture.
	The parameters in the first column corresponds strictly to the parameters needed to create a postured spline.
	All the parameters are determined by fitting a hobbyspline on a list of points describing the vertebrae's center (from L5 to C1).
	Every parameters except for theta are given in the local spine coordinate system defined by:
		L5 is the origin
		L5-C7 defines the x axis
		z axis is orthogonal to the sagittal plane
		The spine is facing up.
	Description of the parameters :
		sL5 = slope (in degrees) at L5.
		tL5 = tension (between 1 (no tension) and 0 ('infinite' tension)) at L5.
		sC7 = slope (in degrees) at C7.
		tC7 = tension (between 1 (no tension) and 0 ('infinite' tension)) at C7.
		sC1 = slope (in degrees) at C1.
		tC1 = tension (between 1 (no tension) and 0 ('infinite' tension)) at C1.
		htl = distance between L5 and C7.
		hc = distance between C7 and C1.
		thetac = angle between the line formed by (C7,C1) and the horizontal
		theta = angle between the upper body and the horizontal (in the absolute coordinate system. Generally, the angle between (L5-C7) and the orthogonal plane to the gravity vector)
See 'HowToAddNewPostures.docx' file if you are interested in adding new postures.

The CS.dat files contains the info of each local CS of the spine from L5 to C1.
Its structure is as this : 'Name of the vertebra' Origin(x-coord y-coord z-coord) X-vector(x-coord y-coord z-coord) Y-vector(x-coord y-coord z-coord) Z-vector(x-coord y-coord z-coord)

The splinePoints.dat files contains around 100 points sampled from the spine's spline.
Its structure is as this : x-coord,y-coord,z-coord.

%%% OBJECTS DESCRIPTION %%%
A 'system' is an array of double of dimension 4x3. First row is the coordinates of the object center.
Second row is the coordinates of the x-vector (normalized), third is the coordinates of the y-vector (normalized),
and fourth is the coordinates of the z-vector (normalized).
Usually, the variable named with 'system' in the scripts are in fact cell array of systems.
The 'systems' of the vertebrae are created with the ISB convention.