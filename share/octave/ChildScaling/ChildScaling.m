% Copyright (C) 2017 KTH, UCBL-Ifsttar

% This file is part of the PIPER Framework.

% The PIPER Framework is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 2 of the License, or
% (at your option) any later version.

% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

% Contributors include : will completed soon

% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).


%
% close all
% clear all
% clc
 
arg_list = argv();

age = str2double(arg_list{1}); 
targetAge = str2double(arg_list{2}); 
targetFile = arg_list{3}; 
InputPointFile = arg_list{4}; 
OutputPointFile = arg_list{5};
jar1 = arg_list{6}; 
jar2 = arg_list{7}; 

%% Uncomment for GUI checking
%age = 6;
%targetFile = 'J:\child_scaling\target_example.ptt';
%InputPointFile = 'J:\child_scaling\controlpointsSource.txt';
%OutputPointFile = 'J:\child_scaling\controlpointsTarget.txt';
%jar1 = 'D:\Users\Wan\DevCpp\01-PIPER\piper\piper-exe2\share\octave\xerces-2_11_0\xercesImpl.jar'; 
%jar2 = 'D:\Users\Wan\DevCpp\01-PIPER\piper\piper-exe2\share\octave\xerces-2_11_0\xml-apis.jar'; 



% add java path for octave xml parser
%javaaddpath(jar1);
%javaaddpath(jar2);

% get target age
%[TargetAge]=getTarget(targetFile, 'Age');
disp(targetAge);
TargetAge.AGE=targetAge

% get AnthropometricDimension targets
%[TargetAnthropometric]=getTarget(targetFile, 'AnthropometricDimension');

% define AnthropometricDimension on child6YO uing GEBOD regression and age
AnthropometricDimension.('HEAD_LENGTH') = interp1([1.5 3 6],[169.5 178 182],age,"linear", "extrap");
AnthropometricDimension.('HEAD_BREADTH') = interp1([1.5 3 6],[129 136 139.5],age,"linear", "extrap");
AnthropometricDimension.('SEATED_HEIGHT') = interp1([1.5 3 6],[505 552 631],age,"linear", "extrap");
AnthropometricDimension.('HEAD_TO_CHIN_HEIGHT') = interp1([1.5 3 6],[158.9 170.2 183.6],age,"linear", "extrap");
AnthropometricDimension.('BUTTOCK_DEPTH') = interp1([1.5 3 6],[127.5 143.6 171.6],age,"linear", "extrap");
AnthropometricDimension.('HIP_BREADTH_STANDING') = interp1([1.5 3 6],[169 191 216.5],age,"linear", "extrap");
AnthropometricDimension.('WAIST_DEPTH') = interp1([1.5 3 6],[138 146.5 150.7],age,"linear", "extrap");
AnthropometricDimension.('WAIST_BREADTH') = interp1([1.5 3 6],[145 156.5 176.5],age,"linear", "extrap");
AnthropometricDimension.('CHEST_DEPTH') = interp1([1.5 3 6],[120.5 129 144],age,"linear", "extrap");
AnthropometricDimension.('CHEST_BREADTH') = interp1([1.5 3 6],[154.5 167 186.5],age,"linear", "extrap");
AnthropometricDimension.('SHOULDER_BREADTH') = interp1([1.5 3 6],[217 236.5 269.5],age,"linear", "extrap");
AnthropometricDimension.('SHOULDER_TO_ELBOW_LENGTH') = interp1([1.5 3 6],[162.5 193 235],age,"linear", "extrap");
AnthropometricDimension.('FOREARM-HAND_LENGTH') = interp1([1.5 3 6],[215.5 253 302.5],age,"linear", "extrap");
AnthropometricDimension.('ELBOW_CIRCUMFERENCE') = interp1([1.5 3 6],[132.4 147.6 171.6],age,"linear", "extrap");
AnthropometricDimension.('BICEPS_CIRCUMFERENCE') = interp1([1.5 3 6],[131.5 146.6 174.6],age,"linear", "extrap");
AnthropometricDimension.('FOREARM_CIRCUMFERENCE') = interp1([1.5 3 6],[138.3 151.5 170.6],age,"linear", "extrap");
AnthropometricDimension.('WRIST_CIRCUMFERENCE') = interp1([1.5 3 6],[102 110.2 121.7],age,"linear", "extrap");
AnthropometricDimension.('KNEE_HEIGHT_SEATED') = interp1([1.5 3 6],[230 281.5 349.5],age,"linear", "extrap");
AnthropometricDimension.('KNEE_CIRCUMFERENCE') = interp1([1.5 3 6],[212.9 241 290.4],age,"linear", "extrap");
AnthropometricDimension.('THIGH_CIRCUMFERENCE') = interp1([1.5 3 6],[251.1 284.3 346.3],age,"linear", "extrap");
AnthropometricDimension.('UPPER_LEG_CIRCUMFERENCE') = interp1([1.5 3 6],[226 258.2 307.4],age,"linear", "extrap");
AnthropometricDimension.('CALF_CIRCUMFERENCE') = interp1([1.5 3 6],[178.5 200.7 231.5],age,"linear", "extrap");
AnthropometricDimension.('ANKLE_CIRCUMFERENCE') = interp1([1.5 3 6],[130.5 144.6 160.7],age,"linear", "extrap");
AnthropometricDimension.('FOOT_BREADTH') = interp1([1.5 3 6],[53 62 72.8],age,"linear", "extrap");
AnthropometricDimension.('FOOT_LENGTH') = interp1([1.5 3 6],[126.5 151.5 181.6],age,"linear", "extrap");
AnthropometricDimension.('NECK_CIRCUMFERENCE') = interp1([1.5 3 6],[219.5 238.5 260],age,"linear", "extrap");

TargetAnthropometricDimension.('HEAD_LENGTH') = interp1([1.5 3 6],[169.5 178 182],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('HEAD_BREADTH') = interp1([1.5 3 6],[129 136 139.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('SEATED_HEIGHT') = interp1([1.5 3 6],[505 552 631],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('HEAD_TO_CHIN_HEIGHT') = interp1([1.5 3 6],[158.9 170.2 183.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('BUTTOCK_DEPTH') = interp1([1.5 3 6],[127.5 143.6 171.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('HIP_BREADTH_STANDING') = interp1([1.5 3 6],[169 191 216.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('WAIST_DEPTH') = interp1([1.5 3 6],[138 146.5 150.7],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('WAIST_BREADTH') = interp1([1.5 3 6],[145 156.5 176.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('CHEST_DEPTH') = interp1([1.5 3 6],[120.5 129 144],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('CHEST_BREADTH') = interp1([1.5 3 6],[154.5 167 186.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('SHOULDER_BREADTH') = interp1([1.5 3 6],[217 236.5 269.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('SHOULDER_TO_ELBOW_LENGTH') = interp1([1.5 3 6],[162.5 193 235],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('FOREARM-HAND_LENGTH') = interp1([1.5 3 6],[215.5 253 302.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('ELBOW_CIRCUMFERENCE') = interp1([1.5 3 6],[132.4 147.6 171.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('BICEPS_CIRCUMFERENCE') = interp1([1.5 3 6],[131.5 146.6 174.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('FOREARM_CIRCUMFERENCE') = interp1([1.5 3 6],[138.3 151.5 170.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('WRIST_CIRCUMFERENCE') = interp1([1.5 3 6],[102 110.2 121.7],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('KNEE_HEIGHT_SEATED') = interp1([1.5 3 6],[230 281.5 349.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('KNEE_CIRCUMFERENCE') = interp1([1.5 3 6],[212.9 241 290.4],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('THIGH_CIRCUMFERENCE') = interp1([1.5 3 6],[251.1 284.3 346.3],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('UPPER_LEG_CIRCUMFERENCE') = interp1([1.5 3 6],[226 258.2 307.4],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('CALF_CIRCUMFERENCE') = interp1([1.5 3 6],[178.5 200.7 231.5],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('ANKLE_CIRCUMFERENCE') = interp1([1.5 3 6],[130.5 144.6 160.7],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('FOOT_BREADTH') = interp1([1.5 3 6],[53 62 72.8],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('FOOT_LENGTH') = interp1([1.5 3 6],[126.5 151.5 181.6],TargetAge.AGE,"linear", "extrap");
TargetAnthropometricDimension.('NECK_CIRCUMFERENCE') = interp1([1.5 3 6],[219.5 238.5 260],TargetAge.AGE,"linear", "extrap");

%AnthropometricDimension.('STANDING_HEIGHT') = 1146;
%AnthropometricDimension.('SHOULDER_HEIGHT') =909;
%AnthropometricDimension.('SHOULDER_CIRCUMFERENCE') =222.3;
%AnthropometricDimension.('WAIST_HEIGHT') =666.6;
%AnthropometricDimension.('SEATED_HEIGHT') =631;
%AnthropometricDimension.('HEAD_LENGTH') =182;
%AnthropometricDimension.('HEAD_BREADTH') =139.5;
%AnthropometricDimension.('HEAD_TO_CHIN_HEIGHT') =183.6;
%AnthropometricDimension.('NECK_CIRCUMFERENCE') =260;
%AnthropometricDimension.('SHOULDER_BREADTH') =269.5;
%AnthropometricDimension.('CHEST_DEPTH') =144;
%AnthropometricDimension.('CHEST_BREADTH') =186.5;
%AnthropometricDimension.('WAIST_DEPTH') =150.7;
%AnthropometricDimension.('WAIST_BREADTH') =176.5;
%AnthropometricDimension.('BUTTOCK_DEPTH') =171.6;
%AnthropometricDimension.('HIP_BREADTH_STANDING') =216.5;
%AnthropometricDimension.('SHOULDER_TO_ELBOW_LENGTH') =235;
%AnthropometricDimension.('FOREARM-HAND_LENGTH') =302.5;
%AnthropometricDimension.('BICEPS_CIRCUMFERENCE') =174.6;
%AnthropometricDimension.('ELBOW_CIRCUMFERENCE') =171.6;
%AnthropometricDimension.('FOREARM_CIRCUMFERENCE') =170.6;
%AnthropometricDimension.('WRIST_CIRCUMFERENCE') =121.7;
%AnthropometricDimension.('KNEE_HEIGHT_SEATED') =349.5;
%AnthropometricDimension.('THIGH_CIRCUMFERENCE') =346.3;
%AnthropometricDimension.('UPPER_LEG_CIRCUMFERENCE') =307.4;
%AnthropometricDimension.('KNEE_CIRCUMFERENCE') =290.4;
%AnthropometricDimension.('CALF_CIRCUMFERENCE') =231.5;
%AnthropometricDimension.('ANKLE_CIRCUMFERENCE') =160.7;

% get source control point
ControlPoints = dlmread(InputPointFile, " ");
% compute tatget point
ControlPointsTarget = ChildScaling_compute(ControlPoints,age,AnthropometricDimension, TargetAge.AGE, TargetAnthropometricDimension);
%write outputfile
dlmwrite(OutputPointFile, ControlPointsTarget(:,2:4), " "); 


