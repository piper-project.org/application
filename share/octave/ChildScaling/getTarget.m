% Copyright (C) 2017 KTH, UCBL-Ifsttar

% This file is part of the PIPER Framework.

% The PIPER Framework is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 2 of the License, or
% (at your option) any later version.

% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

% Contributors include : will completed soon

% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).



%

function [targets]=getTarget(targetFile, targetType)

 parser = javaObject('org.apache.xerces.parsers.DOMParser');
 parser.parse(targetFile); 
 xDoc = parser.getDocument;
 
%elem = xDoc.getElementsByTagName('piper-target').getElementsByTagName('targetList').getElementsByTagName('target').item(0);
pipertarget = xDoc.getElementsByTagName('piper-target').item(0);
targetlist = pipertarget.getElementsByTagName('targetList').item(0);
target = targetlist.getElementsByTagName('target');
% get type attribute target
for i=0:target.getLength()-1
  curTarget = target.item(i);
  att = curTarget.getAttribute('type');
  if  strcmp(att,targetType)
    name = curTarget.getElementsByTagName('name').item(0).getFirstChild.getTextContent
    targets.(name) = str2double(curTarget.getElementsByTagName('value').item(0).getFirstChild.getTextContent);
  end
end
