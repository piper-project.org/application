# Copyright (C) 2017 KTH, UCBL-Ifsttar, CEESAR

# This file is part of the PIPER Framework.

# The PIPER Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# The PIPER Framework is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

# Contributors include : will completed soon

# This work has received funding from the European Union Seventh Framework 
# Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).


#
#See the PIPER documentation for more info.
import sys
import piper.app
import os
import os.path
import shutil
import piper.hbm
import numpy

def printUsage():
    piper.app.logWarning("script expects 2 parameters")

def materialScaling(modelAge, targetAge):
    piper.app.logInfo("Model age= {0} years".format(modelAge/12))
    piper.app.logInfo("Target age= {0} years".format(targetAge/12))
	#shortcut to current hbm, target  and project
    project = piper.app.Context.instance().project()
    model = project.model()
    target = project.target()
    #definition of an example scaling parameter
    ageVector = numpy.arange(18,73,1)
    index = targetAge-18
    index2 = modelAge-18

    ScalingFactor = {}
    
    regression_skull_cortical_modulus = 0.1588 + ageVector*0.0982
    ScalingFactor["skull_cortical_modulus"]=[regression_skull_cortical_modulus[index]/regression_skull_cortical_modulus[index2]]
	
    regression_skull_suture_modulus = -0.0726 + ageVector*0.019
    ScalingFactor["skull_suture_modulus"]=[regression_skull_suture_modulus[index]/regression_skull_suture_modulus[index2]]
	
    regression_skull_duramater_modulus = 0.0072 + ageVector*0.000025
    ScalingFactor["skull_duramater_modulus"]=[regression_skull_duramater_modulus[index]/regression_skull_duramater_modulus[index2]]
	
    regression_vertebrae_trabecular_modulus = 0.0999 + ageVector*3.58e-4
    ScalingFactor["vertebrae_trabecular_modulus"]=[regression_vertebrae_trabecular_modulus[index]/regression_vertebrae_trabecular_modulus[index2]]
	
    regression_vertebrae_cortical_modulus = 0.7029 + ageVector*0.000975
    ScalingFactor["vertebrae_cortical_modulus"]=[regression_vertebrae_cortical_modulus[index]]
	
    regression_sternum_cortical_modulus = 5.268 + ageVector*0.0161
    ScalingFactor["sternum_cortical_modulus"]=[regression_sternum_cortical_modulus[index]/regression_sternum_cortical_modulus[index2]]
	
    regression_ribs_cortical_modulus = 5.7595 + ageVector*0.0141
    ScalingFactor["ribs_cortical_modulus"]=[regression_ribs_cortical_modulus[index]/regression_ribs_cortical_modulus[index2]]
	
    regression_scapula_clavicle_cortical_modulus = 6.2444 + ageVector*0.024325
    ScalingFactor["scapula_clavicle_cortical_modulus"]=[regression_scapula_clavicle_cortical_modulus[index]/regression_scapula_clavicle_cortical_modulus[index2]]
	
    regression_scapula_clavicle_trabecular_modulus = 0.8333 + ageVector*0.00324
    ScalingFactor["scapula_clavicle_trabecular_modulus"]=[regression_scapula_clavicle_trabecular_modulus[index]/regression_scapula_clavicle_trabecular_modulus[index2]]
	
    regression_ribs_trabecular_modulus = 0.0174 + ageVector*5.83e-5
    ScalingFactor["ribs_trabecular_modulus"]=[regression_ribs_trabecular_modulus[index]/regression_ribs_trabecular_modulus[index2]]
	
    regression_ribcage_cartilage = 0.0002 + ageVector*6.66e-5
    ScalingFactor["ribcage_cartilage"]=[regression_ribcage_cartilage[index]/regression_ribcage_cartilage[index2]]
	
    regression_lungs_modulus = 4.296e-6 - ageVector*0.0028e-6
    ScalingFactor["lungs_modulus"]=[regression_lungs_modulus[index]/regression_lungs_modulus[index2]]
	
    regression_pelvis_cortical_modulus = 5.297 + (numpy.log(ageVector)- numpy.log(12))*2.8535
    ScalingFactor["pelvis_cortical_modulus"]=[regression_pelvis_cortical_modulus[index]/regression_pelvis_cortical_modulus[index2]]
	
    regression_hipcapsule_ligament_modulus = 0.1187 + ageVector*6.66e-5
    ScalingFactor["hipcapsule_ligament_modulus"]=[regression_hipcapsule_ligament_modulus[index]/regression_hipcapsule_ligament_modulus[index2]]
	
    regression_humerus_cortical_modulus = 5.7278 - ageVector*0.02228
    ScalingFactor["humerus_cortical_modulus"]=[regression_humerus_cortical_modulus[index]/regression_humerus_cortical_modulus[index2]]
	
    regression_humerus_trabecular_modulus = 0.5214 + ageVector*0.000075
    ScalingFactor["humerus_trabecular_modulus"]=[regression_humerus_trabecular_modulus[index]/regression_humerus_trabecular_modulus[index2]]
	
    regression_femur_trabecular_modulus = 0.5214 + ageVector*0.000075
    ScalingFactor["femur_trabecular_modulus"]=[regression_femur_trabecular_modulus[index]/regression_femur_trabecular_modulus[index2]]
	
    regression_AnnulusFibers_modulus_A = 0.0212 + ageVector*0.00003
    ScalingFactor["AnnulusFibers_modulus_A"]=[regression_AnnulusFibers_modulus_A[index]/regression_AnnulusFibers_modulus_A[index2]]
	
    regression_AnnulusFibers_modulus_B = 0.0039 + ageVector*0.000008
    ScalingFactor["AnnulusFibers_modulus_B"]=[regression_AnnulusFibers_modulus_B[index]/regression_AnnulusFibers_modulus_B[index2]]
	
    regression_AnnulusFibers_modulus_C = 0.0039 + ageVector*0.000008
    ScalingFactor["AnnulusFibers_modulus_C"]=[regression_AnnulusFibers_modulus_C[index]/regression_AnnulusFibers_modulus_C[index2]]

    regression_AnnulusFibers_shear_modulus = 0.0133 + ageVector*0.00002
    ScalingFactor["AnnulusFibers_shear_modulus"]=[regression_AnnulusFibers_shear_modulus[index]/regression_AnnulusFibers_shear_modulus[index2]]
	
    regression_Vertical_cruciate_modulus_A = 0.6532 + ageVector*0.00085
    ScalingFactor["Vertical_cruciate_modulus_A"]=[regression_Vertical_cruciate_modulus_A[index]/regression_Vertical_cruciate_modulus_A[index2]]
	
    regression_Vertical_cruciate_modulus_B = 0.0631 + ageVector*8.33E-5
    ScalingFactor["Vertical_cruciate_modulus_B"]=[regression_Vertical_cruciate_modulus_B[index]/regression_Vertical_cruciate_modulus_B[index2]]
	
    regression_Vertical_cruciate_modulus_C = 0.0631 + ageVector*8.33E-5
    ScalingFactor["Vertical_cruciate_modulus_C"]=[regression_Vertical_cruciate_modulus_C[index]/regression_Vertical_cruciate_modulus_C[index2]]
	
    regression_Transverse_ligament_modulus_A = 0.1913 + ageVector*0.0001
    ScalingFactor["Transverse_ligament_modulus_A"]=[regression_Transverse_ligament_modulus_A[index]/regression_Transverse_ligament_modulus_A[index2]]
	
    regression_Transverse_ligament_modulus_B = 0.0243 + ageVector*2E-5
    ScalingFactor["Transverse_ligament_modulus_B"]=[regression_Transverse_ligament_modulus_B[index]/regression_Transverse_ligament_modulus_B[index2]]
	
    regression_Transverse_ligament_modulus_C = 0.0243 + ageVector*2E-5
    ScalingFactor["Transverse_ligament_modulus_C"]=[regression_Transverse_ligament_modulus_C[index]/regression_Transverse_ligament_modulus_C[index2]]
	
    regression_Pelvis_trabecular_modulus = 0.0325 + ageVector*0.0004 -(ageVector**2)*3e-7
    ScalingFactor["Pelvis_trabecular_modulus"]=[regression_Pelvis_trabecular_modulus[index]/regression_Pelvis_trabecular_modulus[index2]]
	
    regression_Femur_cortical_young_modulus = 2.2515 + ageVector*0.2257 -(ageVector**2)*0.0012
    ScalingFactor["Femur_cortical_young_modulus"]=[regression_Femur_cortical_young_modulus[index]/regression_Femur_cortical_young_modulus[index2]]
	
    regression_Femur_cortical_Yeld_Stress = 0.0525 + ageVector*0.0005
    ScalingFactor["Femur_cortical_Yeld_Stress"]=[regression_Femur_cortical_Yeld_Stress[index]/regression_Femur_cortical_Yeld_Stress[index2]]
	
    regression_Femur_cartilage_Bulk_modulus = 0.031 + ageVector*0.000025
    ScalingFactor["Femur_cartilage_Bulk_modulus"]=[regression_Femur_cartilage_Bulk_modulus[index]/regression_Femur_cartilage_Bulk_modulus[index2]]
	
    regression_Tibia_cortical_young_modulus = -0.9605 + (numpy.log(ageVector))*3.1498
    ScalingFactor["Tibia_cortical_young_modulus"]=[regression_Tibia_cortical_young_modulus[index]/regression_Tibia_cortical_young_modulus[index2]]
	
    regression_Fibula_cortical_young_modulus = -0.9605 + (numpy.log(ageVector))*3.1498
    ScalingFactor["Fibula_cortical_young_modulus"]=[regression_Fibula_cortical_young_modulus[index]/regression_Fibula_cortical_young_modulus[index2]]

    regression_Neck_Ligament_Scale_Factor = 0.8139 + ageVector*0.0006
    ScalingFactor["Neck_Ligament_Scale_Factor"]=[regression_Neck_Ligament_Scale_Factor[index]]

    regression_Neck_Muscle_Scale_Factor = 0.6153 + ageVector*0.0011
    ScalingFactor["Neck_Muscle_Scale_Factor"]=[regression_Neck_Muscle_Scale_Factor[index]]
  
    ScalingFactor["Neck_GroundSubstance_G"]= []
    regression_Neck_GroundSubstance_G1 = 0.0003 + ageVector*4e-7
    ScalingFactor["Neck_GroundSubstance_G"].append(regression_Neck_GroundSubstance_G1[index]/regression_Neck_GroundSubstance_G1[index2])
	
    regression_Neck_GroundSubstance_G2 = 0.0003 + ageVector*4e-7
    ScalingFactor["Neck_GroundSubstance_G"].append(regression_Neck_GroundSubstance_G2[index]/regression_Neck_GroundSubstance_G2[index2])
	
    regression_Neck_GroundSubstance_G3 = 0.0002 + ageVector*4e-7
    ScalingFactor["Neck_GroundSubstance_G"].append(regression_Neck_GroundSubstance_G3[index]/regression_Neck_GroundSubstance_G3[index2])
	
    regression_Neck_GroundSubstance_G4 = 7e-5 + ageVector*1e-7
    ScalingFactor["Neck_GroundSubstance_G"].append(regression_Neck_GroundSubstance_G4[index]/regression_Neck_GroundSubstance_G4[index2])
	
    regression_Neck_GroundSubstance_G5 = 9e-5 + ageVector*1e-7
    ScalingFactor["Neck_GroundSubstance_G"].append(regression_Neck_GroundSubstance_G5[index]/regression_Neck_GroundSubstance_G5[index2])

    ScalingFactor["Neck_GroundSubstance_K"]= []
    regression_Neck_GroundSubstance_K1 = 0.0025 + ageVector*8e-6
    ScalingFactor["Neck_GroundSubstance_K"].append(regression_Neck_GroundSubstance_K1[index]/regression_Neck_GroundSubstance_K1[index2])
	
    regression_Neck_GroundSubstance_K2 = 0.002 + ageVector*2e-6
    ScalingFactor["Neck_GroundSubstance_K"].append(regression_Neck_GroundSubstance_K2[index]/regression_Neck_GroundSubstance_K2[index2])
	
    regression_Neck_GroundSubstance_K3 = 0.001 + ageVector*2e-6
    ScalingFactor["Neck_GroundSubstance_K"].append(regression_Neck_GroundSubstance_K3[index]/regression_Neck_GroundSubstance_K3[index2])
	
    regression_Neck_GroundSubstance_K4 = 0.001 + ageVector*2e-6
    ScalingFactor["Neck_GroundSubstance_K"].append(regression_Neck_GroundSubstance_K4[index]/regression_Neck_GroundSubstance_K4[index2])
    #K5 remains the same (equal to zero)
    ScalingFactor["Neck_GroundSubstance_K"].append(1)

    #define scaling target
    targetlist = piper.hbm.TargetList()
    for scalefactor, value in ScalingFactor.iteritems():
        print scalefactor, value
        newtarget = piper.hbm.ScalingParameterTarget()
        newtarget.setName(scalefactor)
        newtarget.setValue(value)   
        targetlist.add(newtarget)
    
    piper.app.targetChanged()
    piper.app.historyListChanged()
    #apply scaling factors to parameters
    piper.app.applyTargetScalingParameter(project, targetlist)

if (3 != len(sys.argv)):
    printUsage()
else :
    materialScaling(float(sys.argv[1]), float(sys.argv[2]))