% Copyright (C) 2017 KTH, UCBL-Ifsttar

% This file is part of the PIPER Framework.

% The PIPER Framework is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 2 of the License, or
% (at your option) any later version.

% The PIPER Framework is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with the PIPER Framework.  If not, see <http://www.gnu.org/licenses/>.

% Contributors include : will completed soon

% This work has received funding from the European Union Seventh Framework 
% Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).


%
function nodes = ChildScaling_compute(nodes,Source_Age,AnthropometricDimension, Age, TargetAnthropometricDimension, height);


%========== Functions =====================================================
function [ABC,D,R,ptsP]=LeastSquaresPlane(pts)
% Least-squares Plane : Ax + By + Cz + D = 0

% least-squares plane passes through the centroid
ptsC=pts-ones(size(pts,1),1)*mean(pts,1);

[U,S,V]=svd(ptsC,0); %[V,S]=spec(ptsC'*ptsC);

[S,i]=min(diag(S));

% ABC coefficients = coordinates of the unit vector normal to the plane
ABC=V(:,i);
D=-mean(pts,1)*ABC;
R=norm(U(:,i)*S);
ptsP=pts-(pts*ABC+D)*ABC';
endfunction

function W=NaturalCubicSpline(n,V)
% natural cubic spline interpolation
% parametric functions : x=f(t), y=f(t), z=f(t)
% estimated t values (based on distances between successive points)
d=sqrt(sum((V(2:end,:)-V(1:end-1,:)).^2,2)); 
t=[0;cumsum(d)]/sum(d);
tt = reshape((d*ones(1,n+1)/(n+1))',numel(d*ones(1,n+1)/(n+1)),1);
tt=[0;cumsum(tt)]/sum(tt);

%tt=linspace(0,1,n)';

% derivative of the spline at each point
% ('natural' for derivative equal to zero at first and last points)
    for k=1:size(V,2)
        W(:,k)=interp1(t,V(:,k),tt,'spline');
    end
endfunction

%== GEBOD data ============================================================
Head_Length_Source = AnthropometricDimension.('HEAD_LENGTH');
Head_Breadth_Source = AnthropometricDimension.('HEAD_BREADTH');
Seated_Height_Source = AnthropometricDimension.('SEATED_HEIGHT');
Head_to_Chin_Height_Source = AnthropometricDimension.('HEAD_TO_CHIN_HEIGHT');
Buttock_Depth_Source = AnthropometricDimension.('BUTTOCK_DEPTH');
Hipp_Breadth_Source = AnthropometricDimension.('HIP_BREADTH_STANDING');
Waist_Depth_Source = AnthropometricDimension.('WAIST_DEPTH');
Waist_Breadth_Source = AnthropometricDimension.('WAIST_BREADTH');
Chest_Depth_Source = AnthropometricDimension.('CHEST_DEPTH');
Chest_Breadth_Source = AnthropometricDimension.('CHEST_BREADTH');
Shoulder_Breadth_Source = AnthropometricDimension.('SHOULDER_BREADTH');
Shoulder_To_Elbow_Length_Source = AnthropometricDimension.('SHOULDER_TO_ELBOW_LENGTH');
Forearm_Hand_Length_Source = AnthropometricDimension.('FOREARM-HAND_LENGTH');
Elbow_Circumference_Source = AnthropometricDimension.('ELBOW_CIRCUMFERENCE');
Biceps_Circumference_Source = AnthropometricDimension.('BICEPS_CIRCUMFERENCE');
Forearm_Circumference_Source = AnthropometricDimension.('FOREARM_CIRCUMFERENCE');
Wrist_Circumference_Source = AnthropometricDimension.('WRIST_CIRCUMFERENCE');
Knee_Heigth_Source = AnthropometricDimension.('KNEE_HEIGHT_SEATED');
Knee_Circumference_Source = AnthropometricDimension.('KNEE_CIRCUMFERENCE');
Thigh_Circumference_Source = AnthropometricDimension.('THIGH_CIRCUMFERENCE');
Upper_Leg_Circumference_Source = AnthropometricDimension.('UPPER_LEG_CIRCUMFERENCE');
Calf_Circumference_Source = AnthropometricDimension.('CALF_CIRCUMFERENCE');
Ankle_Circumference_Source = AnthropometricDimension.('ANKLE_CIRCUMFERENCE');
Foot_Breadth_Source = AnthropometricDimension.('FOOT_BREADTH');
Foot_Length_Source = AnthropometricDimension.('FOOT_LENGTH');
Neck_Circumference_Source = AnthropometricDimension.('NECK_CIRCUMFERENCE');

%------------------------------
Head_Length = TargetAnthropometricDimension.('HEAD_LENGTH');
Head_Breadth = TargetAnthropometricDimension.('HEAD_BREADTH');
Seated_Height = TargetAnthropometricDimension.('SEATED_HEIGHT');
Head_to_Chin_Height = TargetAnthropometricDimension.('HEAD_TO_CHIN_HEIGHT');
Buttock_Depth = TargetAnthropometricDimension.('BUTTOCK_DEPTH');
Hipp_Breadth = TargetAnthropometricDimension.('HIP_BREADTH_STANDING');
Waist_Depth = TargetAnthropometricDimension.('WAIST_DEPTH');
Waist_Breadth = TargetAnthropometricDimension.('WAIST_BREADTH');
Chest_Depth = TargetAnthropometricDimension.('CHEST_DEPTH');
Chest_Breadth = TargetAnthropometricDimension.('CHEST_BREADTH');
Shoulder_Breadth = TargetAnthropometricDimension.('SHOULDER_BREADTH');
Shoulder_To_Elbow_Length = TargetAnthropometricDimension.('SHOULDER_TO_ELBOW_LENGTH');
Forearm_Hand_Length = TargetAnthropometricDimension.('FOREARM-HAND_LENGTH');
Elbow_Circumference = TargetAnthropometricDimension.('ELBOW_CIRCUMFERENCE');
Biceps_Circumference = TargetAnthropometricDimension.('BICEPS_CIRCUMFERENCE');
Forearm_Circumference = TargetAnthropometricDimension.('FOREARM_CIRCUMFERENCE');
Wrist_Circumference = TargetAnthropometricDimension.('WRIST_CIRCUMFERENCE');
Knee_Heigth = TargetAnthropometricDimension.('KNEE_HEIGHT_SEATED');
Knee_Circumference = TargetAnthropometricDimension.('KNEE_CIRCUMFERENCE');
Thigh_Circumference = TargetAnthropometricDimension.('THIGH_CIRCUMFERENCE');
Upper_Leg_Circumference = TargetAnthropometricDimension.('UPPER_LEG_CIRCUMFERENCE');
Calf_Circumference = TargetAnthropometricDimension.('CALF_CIRCUMFERENCE');
Ankle_Circumference = TargetAnthropometricDimension.('ANKLE_CIRCUMFERENCE');
Foot_Breadth = TargetAnthropometricDimension.('FOOT_BREADTH');
Foot_Length = TargetAnthropometricDimension.('FOOT_LENGTH');
Neck_Circumference = TargetAnthropometricDimension.('NECK_CIRCUMFERENCE');


%----------- Head_Circumference -------------------------------------
%Head_Circumference =[49.5 49.5 49.5 50.1 50.7 51.0 51.3 52.1 52.4 52.8 53.0,...
%    53.3 53.9 54.3 54.8 55.4 55.6 55.8]*10;
%
x_age = [1.5 2 3.5 4.5 5.5 6.5 7.5 8.5 9.5 10.5 11.5 12.5 13.5 14.5 15.5 16.5 17.5 19];
%Head_circ = interp1(x_age,Head_Circumference,Age);
%----------- Head_Breadth -------------------------------------------
Head_Breadth =[13.4 13.4 13.4 13.6 13.7 13.9 14.0 14.1 14.2 14.3 14.4 14.5,...
    14.7 14.8 14.8 15.0 15.0 15.1]*10;

Head_breadth = interp1(x_age,Head_Breadth,Age);
Head_breadth_Source = interp1(x_age,Head_Breadth,Source_Age);
%----------- Head_Length --------------------------------------------
Head_Length =[17.4 17.4 17.4 17.9 18.1 18.1 18.1 18.4 18.6 18.5 18.6 18.8 18.8,...
    19.1 19.2 19.3 19.1 19.4]*10;

Head_length = interp1(x_age,Head_Length,Age);
Head_length_Source = interp1(x_age,Head_Length,Source_Age);
%----------- Lower Face Height --------------------------------------------
Lower_Face_Height =[8.1 8.1 8.1 8.5 8.6 8.9 9.1 9.3 9.6 9.7 10.0 10.1 10.4,...
    10.8 10.9 11.2 11.0 11.3]*10;

Lower_face = interp1(x_age,Lower_Face_Height,Age);
Lower_face_Source = interp1(x_age,Lower_Face_Height,Source_Age);
%----------- Head Heigth --------------------------------------------
% Head_Heigth =[17.3 17.3 17.3 17.7 17.8 18.4 18.7 18.9 19.0 19.5 19.8 19.9 20.2,...
%     20.4 20.7 21.2 21.0 21.3]*10;
% 
% Head_height = interp1(x_age,Head_Heigth,Age);
% Head_height_Source = interp1(x_age,Head_Heigth,Source_Age);
%----------- Tragion to top of Head ---------------------------------------
Tragion_to_Top_Head =[11.1 11.1 11.1 11.6 11.5 11.6 11.8 11.7 11.7 12.0 12.2,...
    12.2 12.2 12.2 12.3 12.5 12.4 12.7]*10;

Trag_Top_Head = interp1(x_age,Tragion_to_Top_Head,Age);
Trag_Top_Head_source = interp1(x_age,Tragion_to_Top_Head,Source_Age);
%----------- Ear-Sellion Depth --------------------------------------------
% Ear_Sellion_Depth =[8.0 8.0 8.0 8.3 8.6 8.6 8.9 8.8 8.9 9.3 9.6 9.3 9.6 9.8,...
%     9.8 9.8 9.8 10.1]*10;
% 
% Ear_sellion = interp1(x_age,Ear_Sellion_Depth,Age);
% Ear_sellion_Source = interp1(x_age,Ear_Sellion_Depth,Source_Age);
%----------- Nose Length --------------------------------------------
% Nose_Length =[2.9 2.9 2.9 3.1 3.3 3.4 3.5 3.7 3.9 4.0 4.2 4.3 4.4 4.6 4.7,...
%     4.7 4.6 5.0]*10;
% 
% Nose_length = interp1(x_age,Nose_Length,Age);
% Nose_length_Source = interp1(x_age,Nose_Length,Source_Age);
%------------------------------------------------------------------------------- 
ID = nodes(:,1); %Node number ID

New_ID = 26000001:26000291;
id = [23:58,60:67,71:87,92:132,134:196,198:216,301001:301083,301088:301111];

for h = 1:length(id)
  nodes(find(ID==New_ID(h)),1) =  id(h);
end

ID = id;

Lower_Body_ID = [find(ID==23):find(ID==58),find(ID==60):find(ID==67),...
    find(ID==71):find(ID==87),find(ID==92):find(ID==196),find(ID==198):find(ID==202)];
%Lower_Body_ID = ID;
node =  nodes(Lower_Body_ID,:);
node_out = node; 
node_out(:,2:end) = 0;

Offs_torso_T1_1 = nodes(find(ID==301111),[2 4])-nodes(find(ID==84),[2 4]);
%======== Scaling Upper body (not neck-head) ==============================
%----- HEAD -----------------------------------------------------
%----------- Head Length -------------------------------------
Head_Nodes = 203:216;
%Head_length_Source = norm(nodes(find(ID==203),2:4)-nodes(find(ID==204),2:4));
Scale_H_Length = Head_length/Head_length_Source;

v_H_length = nodes(find(ID==203),2:4)-nodes(find(ID==204),2:4);
n_H_length = v_H_length/norm(v_H_length);
d = (norm(v_H_length)*Scale_H_Length-norm(v_H_length))/2;

nodes(find(ID==203),2:4) = nodes(find(ID==203),2:4)+n_H_length*d;
nodes(find(ID==208),2:4) = nodes(find(ID==208),2:4)+n_H_length*d;
nodes(find(ID==207),2:4) = nodes(find(ID==207),2:4)+n_H_length*d;
nodes(find(ID==213),2:4) = nodes(find(ID==213),2:4)+n_H_length*d;
nodes(find(ID==214),2:4) = nodes(find(ID==214),2:4)+n_H_length*d;
nodes(find(ID==204),2:4) = nodes(find(ID==204),2:4)-n_H_length*d;
nodes(find(ID==215),2:4) = nodes(find(ID==215),2:4)-n_H_length*d;
nodes(find(ID==216),2:4) = nodes(find(ID==216),2:4)-n_H_length*d;

%----------- Head_Breadth -------------------------------------------
%Head_breadth_Source = norm(nodes(find(ID==206),2:4)-nodes(find(ID==205),2:4));
Scale_H_Breadth = Head_breadth/Head_breadth_Source;

v_H_breadth = nodes(find(ID==206),2:4)-nodes(find(ID==205),2:4);
n_H_breadth = v_H_breadth/norm(v_H_breadth);
d = (norm(v_H_breadth)*Scale_H_Breadth-norm(v_H_breadth))/2;

nodes(find(ID==205),2:4) = nodes(find(ID==205),2:4)-n_H_breadth*d;
nodes(find(ID==214),2:4) = nodes(find(ID==214),2:4)-n_H_breadth*d;
nodes(find(ID==216),2:4) = nodes(find(ID==216),2:4)-n_H_breadth*d;

nodes(find(ID==206),2:4) = nodes(find(ID==206),2:4)+n_H_breadth*d;
nodes(find(ID==213),2:4) = nodes(find(ID==213),2:4)+n_H_breadth*d;
nodes(find(ID==215),2:4) = nodes(find(ID==215),2:4)+n_H_breadth*d;

%----------- Lower Face Height --------------------------------------------
%Lower_face_Source = norm(nodes(find(ID==207),2:4)-nodes(find(ID==208),2:4));
Scale_LFHeight = Lower_face/Lower_face_Source;

v_H_LFHeight = nodes(find(ID==207),2:4)-nodes(find(ID==208),2:4);
n_H_LFHeight = v_H_LFHeight/norm(v_H_LFHeight);
d = (norm(v_H_LFHeight)*Scale_LFHeight-norm(v_H_LFHeight));

%nodes(find(ID==207),2:4) = nodes(find(ID==207),2:4)+n_H_LFHeight*d;
for id = [203:206,208:216]
nodes(find(ID==id),2:4) = nodes(find(ID==id),2:4)-n_H_LFHeight*d;
end

%----------- Nose Length --------------------------------------------
% Nose_length_Source = norm(nodes(find(ID==208),2:4)-nodes(find(ID==203),2:4));
% Scale_Nose_Length = Nose_length/Nose_length_Source;
% 
% v_Nose_Length = nodes(find(ID==208),2:4)-nodes(find(ID==203),2:4);
% n_Nose_Length = v_Nose_Length/norm(v_Nose_Length);
% d = (norm(v_Nose_Length)*Scale_Nose_Length-norm(v_Nose_Length));
% 
% %nodes(find(ID==208),2:4) = nodes(find(ID==208),2:4)+n_Nose_Length*d;
% for id = [203:206,209:216]
% nodes(find(ID==id),2:4) = nodes(find(ID==id),2:4)-n_H_LFHeight*d;
% end

%----------- Head Heigth --------------------------------------------
%Trag_Top_Head_source = (nodes(find(ID==212),4)-nodes(find(ID==205),4));

Scale_Top_H_height = Trag_Top_Head/Trag_Top_Head_source%*0.78378;

n_Top_head_height = [0 0 1];
d = Trag_Top_Head_source*Scale_Top_H_height-Trag_Top_Head_source;

nodes(find(ID==212),2:4) = nodes(find(ID==212),2:4)+n_Top_head_height*d;
nodes(find(ID==209),2:4) = nodes(find(ID==209),2:4)+n_Top_head_height*d;
nodes(find(ID==211),2:4) = nodes(find(ID==211),2:4)+n_Top_head_height*d;
nodes(find(ID==210),2:4) = nodes(find(ID==210),2:4)+n_Top_head_height*d;

%-- Script translated from original LBMC SciLab script --------------------
%----- SPINE -----------------------------------------------------
node_spine = [find(ID==23):find(ID==58),find(ID==71):find(ID==79),find(ID==132)];
%node_spine = [7:59,68:79,132,133]'; % For Original "LineModel_NODES.k"
% node : 132 (origin) -> 301013 (end)
% node : 132 (origin) -> 59 (end) % For Original "LineModel_NODES.k"
% data : seated height - 0.7605 * head to chin height,

spine_height = Seated_Height-0.7605*Head_to_Chin_Height;
spine_height_Source = Seated_Height_Source-0.7605*Head_to_Chin_Height_Source;
scale_factor =  spine_height/spine_height_Source;

D = ones(size(node_spine))'*node(ID==132,2:4);
d_head = nodes(ID==301017,2:4);
node_out(node_spine,2:4) = (node(node_spine,2:4)-D)*scale_factor + D;

%----- TRUNK -----------------------------------------------------
node_trunk = [find(ID==60):find(ID==67),find(ID==80):find(ID==87)];

D = ones(size(node_trunk,2),1)*node(ID==132,2:4);
S72 = scale_factor;
node_out(node_trunk,2:4) = (node(node_trunk,2:4)-D)*S72 + D;

%... and leg ---------------
temp_leg = [find(ID==174),find(ID==166)];

D = ones(size(temp_leg,2),1)*node(ID==132,2:4);
node_out(temp_leg(1),2:4) = (node(temp_leg(1),2:4)-D(1,:))*S72 + D(1,:);
node_out(temp_leg(2),2:4) = (node(temp_leg(2),2:4)-D(1,:))*S72 + D(1,:);

%----- Pelvis : 80 82 (breadth) 81 83 (depth) 79-132 (origin) ----
%pelvis : 80 82 (breadth) 81 83 (depth) 79-132 (origin)
%pelvis : BUTTOCK DEPTH & HIP BREADTH,STANDING
scale_factor = [Buttock_Depth ; Hipp_Breadth]./[Buttock_Depth_Source ; Hipp_Breadth_Source];
vy = [0 1 0]';

[vz,k,e,ptsP]=LeastSquaresPlane(node_out([find(ID==80):find(ID==83)],2:4));

O72 = mean(node_out([find(ID==79),find(ID==132)],2:4),1);
O72 = O72-(O72*vz+k)*vz';
O72 = ones(size(node_out([find(ID==80):find(ID==83)],2:4),1),1)*O72;
vz = sign(vz(3))*vz;
vx = cross(vy',vz')';
R = [vx,vy,vz];

S72b = diag([scale_factor/S72;1]);
node_out([find(ID==80):find(ID==83)],2:4) = (node_out([find(ID==80):find(ID==83)],2:4)-O72)*(R*S72b/R) + O72;
node_out(find(ID==temp_leg(1)),2:4) = (node_out(find(ID==temp_leg(1)),2:4)-O72(1,:))*(R*S72b/R) + O72(1,:);
node_out(find(ID==temp_leg(2)),2:4) = (node_out(find(ID==temp_leg(2)),2:4)-O72(2,:))*(R*S72b/R) + O72(2,:);

% abdo : 62 64 (breadth) 61 63 (depth) 77 (origin)
%  MODIF 63 (origin)
% abdomen : WAIST DEPTH & WAIST BREADTH
scale_factor = [Waist_Depth;Waist_Breadth]./[Waist_Depth_Source;Waist_Breadth_Source];
vy = [0 1 0]';
 
[vz,k,e,ptsP]=LeastSquaresPlane(node_out([find(ID==61):find(ID==64)],2:4));
O72 = node_out(ID==63,2:4);
O72 = O72-(O72*vz+k)*vz';
O72 = ones(size(node_out([find(ID==61):find(ID==64)],2:4),1),1)*O72;
vz = sign(vz(3))*vz;
vx = cross(vy',vz')';
R = [vx,vy,vz];

S72b = diag([scale_factor/S72;1]);
node_out([find(ID==61):find(ID==64)],2:4) = (node_out([find(ID==61):find(ID==64)],2:4)-O72)*(R*S72b/R) + O72; 
% thorax : 65 67 (breadth) 60 66 (depth) 73 (origin)
% MODIF 66 (origin)
% thorax : CHEST DEPTH & CHEST BREADTH
scale_factor = [Chest_Depth;Chest_Breadth]./[Chest_Depth_Source;Chest_Breadth_Source];
vy = [0 1 0]';

[vz,k,e,ptsP]=LeastSquaresPlane(node_out([find(ID==60),find(ID==65):find(ID==67)],2:4));
O72 = node_out(ID==66,2:4);
O72 = O72-(O72*vz+k)*vz';
O72 = ones(size(node_out([find(ID==60),find(ID==65):find(ID==67)],2:4),1),1)*O72;
vz = sign(vz(3))*vz;
vx = cross(vy',vz')';
R = [vx,vy,vz];
 
S72b = diag([scale_factor/S72;1]);
node_out([find(ID==60),find(ID==65):find(ID==67)],2:4) = (node_out([find(ID==60),find(ID==65):find(ID==67)],2:4)-O72)*(R*S72b/R) + O72;

% shoulder : 84 86 (breadth) 85 87 () 70 (origin)
% MODIF 87 (origin)
% shoulder : SHOULDER BREADTH

scale_factor = Shoulder_Breadth./Shoulder_Breadth_Source;
vy = [0 1 0]';

[vz,k,e,ptsP]=LeastSquaresPlane(node_out([find(ID==84):find(ID==87)],2:4));
O72 = node_out(ID==87,2:4);
O72 = O72-(O72*vz+k)*vz';
O72 = ones(size(node_out([find(ID==84):find(ID==87)],2:4),1),1)*O72;
vz = sign(vz(3))*vz;
vx = cross(vy',vz')';
R = [vx,vy,vz];
 
S72b = diag([scale_factor/S72;scale_factor/S72;1]);
node_out([find(ID==84):find(ID==87)],2:4) = (node_out([find(ID==84):find(ID==87)],2:4)-O72)*(R*S72b/R) + O72;

%---- ARMS -----------------------------------------------------
%right arm : 
%   origin - 84
%   length - 84 99 102 (shoulder to elbow) ; 102 107 108 93 92 (forearm-hand)
%   circumference - 84 65 94 95 196 (shoulder) ; 99 96 97 98 195 (biceps) ; 102 100 101 103 194 (elbow) ;
%   circumference - 107 104 105 106 193 (forearm) ; 108 109 110 111 192 (wrist)

node_arm_right = [find(ID==65),find(ID==84),find(ID==92):find(ID==111),find(ID==192):find(ID==196)];
node_arm_left = [find(ID==67),find(ID==86),find(ID==112):find(ID==131),find(ID==198):find(ID==202)];

% 84 65 already scaled -> shoulder circumference
scale_factor = norm(node_out(ID==65,2:4)-node_out(ID==84,2:4))./norm(node(ID==65,2:4)-node(ID==84,2:4));

[vy,k,e,ptsP]=LeastSquaresPlane(node([find(ID==84),find(ID==65),find(ID==94),find(ID==95)],2:4));
vy = sign(vy(2))*vy;
vz = ptsP(1,:)-ptsP(2,:);
vz = sign(vz(3))*vz'/norm(vz);
vx = cross(vy',vz')';
R = [vx,vy,vz];

D = ones(size(node([find(ID==94),find(ID==95),find(ID==196)],2:4),1),1)*node(ID==84,2:4);
O72 = ones(size(node_out([find(ID==94),find(ID==95),find(ID==196)],2:4),1),1)*node_out(ID==84,2:4);

S72 = diag([scale_factor;1;scale_factor]);
node_out([find(ID==94),find(ID==95),find(ID==196)],2:4)=(node([find(ID==94),find(ID==95),find(ID==196)],2:4)-D)*(R*S72/R) + O72;

% arm lengths
scale_factor = [Shoulder_To_Elbow_Length; Forearm_Hand_Length]./[Shoulder_To_Elbow_Length_Source; Forearm_Hand_Length_Source];

D = ones(size(node([find(ID==99),find(ID==102)],2:4),1),1)*node(ID==84,2:4);
O72 = ones(size(node_out([find(ID==99),find(ID==102)],2:4),1),1)*node_out(ID==84,2:4);
node_out([find(ID==99),find(ID==102)],2:4)=(node([find(ID==99),find(ID==102)],2:4)-D)*scale_factor(1) + O72;

D = ones(size(node([find(ID==107),find(ID==108),find(ID==93),find(ID==92)],2:4),1),1)*node(ID==102,2:4);
O72 = ones(size(node_out([find(ID==107),find(ID==108),find(ID==93),find(ID==92)],2:4),1),1)*node_out(ID==102,2:4);
node_out([find(ID==107),find(ID==108),find(ID==93),find(ID==92)],2:4)=(node([find(ID==107),find(ID==108),find(ID==93),find(ID==92)],2:4)-D)*scale_factor(2) + O72;

% biceps circumference : 99 (origin) 96 97 98 195
scale_factor = Biceps_Circumference/Biceps_Circumference_Source;
 
D = ones(size(node([find(ID==96):find(ID==98),find(ID==195)],2:4),1),1)*node(ID==99,2:4);
O72 = ones(size(node_out([find(ID==96):find(ID==98),find(ID==195)],2:4),1),1)*node_out(ID==99,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==99):find(ID==96),find(ID==97),find(ID==98)],2:4));
vx = sign(vx(1))*vx;
vz = ptsP(1,:)-ptsP(2,:);
vz = sign(vz(3))*vz'/norm(vz);
vy = cross(vz',vx')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out([find(ID==96):find(ID==98),find(ID==195)],2:4)=(node([find(ID==96):find(ID==98),find(ID==195)],2:4)-D)*(R*S72/R) + O72;
 
% elbow circumference : 102 (origin) 100 101 103 194
scale_factor = Elbow_Circumference/Elbow_Circumference_Source;

D = ones(size(node([find(ID==100),find(ID==101),find(ID==103),find(ID==194)],2:4),1),1)*node(ID==102,2:4);
O72 = ones(size(node_out([find(ID==100),find(ID==101),find(ID==103),find(ID==194)],2:4),1),1)*node_out(ID==102,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==102),find(ID==100),find(ID==101),find(ID==103)],2:4));
vx = sign(vx(1))*vx;
vz = ptsP(1,:)-ptsP(2,:);
vz = sign(vz(3))*vz'/norm(vz);
vy = cross(vz',vx')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out([find(ID==100),find(ID==101),find(ID==103),find(ID==194)],2:4)=(node([find(ID==100),find(ID==101),find(ID==103),find(ID==194)],2:4)-D)*(R*S72/R) + O72;

% forearm circumference : 107 (origin) 104 105 106 193
scale_factor = Forearm_Circumference/Forearm_Circumference_Source;
%pos = [find(ID==104):find(ID==106),find(ID==193)];
%D = ones(size(node(pos,2:4),1),1)*node(ID==107,2:4);
%O72 = ones(size(node_out(pos,2:4),1),1)*node_out(107,2:4);
D = ones(size(node([find(ID==104),find(ID==105),find(ID==106),find(ID==193)],2:4),1),1)*node(ID==107,2:4);
O72 = ones(size(node_out([find(ID==104),find(ID==105),find(ID==106),find(ID==193)],2:4),1),1)*node_out(ID==107,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==107),find(ID==105),find(ID==104),find(ID==106)],2:4));
vx = sign(vx(1))*vx;
vz = ptsP(1,:)-ptsP(2,:);
vz = sign(vz(3))*vz'/norm(vz);
vy = cross(vz',vx')';
R = [vx,vy,vz];
S72 = diag([1;scale_factor(1);scale_factor(1)]);
%node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;
node_out([find(ID==104),find(ID==105),find(ID==106),find(ID==193)],2:4)=(node([find(ID==104),find(ID==105),find(ID==106),find(ID==193)],2:4)-D)*(R*S72/R) + O72;

% wrist circumference : 108 (origin) 109 110 111 192 
scale_factor = Wrist_Circumference/Wrist_Circumference_Source;

pos = [find(ID==109),find(ID==110),find(ID==111),find(ID==192)];
D = ones(size(node([find(ID==109),find(ID==110),find(ID==111),find(ID==192)],2:4),1),1)*node(ID==108,2:4);
O72 = ones(size(node_out([find(ID==109),find(ID==110),find(ID==111),find(ID==192)],2:4),1),1)*node_out(ID==108,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==108),find(ID==110),find(ID==109),find(ID==111)],2:4));
vx = sign(vx(1))*vx;
vz = ptsP(1,:)-ptsP(2,:);
vz = sign(vz(3))*vz'/norm(vz);
vy = cross(vz',vx')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out([find(ID==109),find(ID==110),find(ID==111),find(ID==192)],2:4)=(node([find(ID==109),find(ID==110),find(ID==111),find(ID==192)],2:4)-D)*(R*S72/R) + O72;

% left arm (symetry): 
node_out(node_arm_left(3:end),2:4) = node_out(node_arm_right(3:end),2:4);
node_out(node_arm_left(3:end),3) = -node_out(node_arm_left(3:end),3);
 
%---- LEGS -----------------------------------------------------
%right leg : 
%   origin - 174
%   length - 174 175 164 (hip to knee) ; 164 151 136 141 (knee to foot)
%   length - 141 139 (foot length) ; 140 138 (foot breadth)
%   circumference - 174 181 180 179 186 (thigh) ; 175 178 177 176 185 (upper leg) ; 164 162 163 165 184 (knee) ;
%   circumference - 151 152 142 153 182 (calf) ; 136 134 135 137 183 (ankle)
node_leg_right = [find(ID==134):find(ID==142),find(ID==151):find(ID==153),find(ID==162):find(ID==165),find(ID==174):find(ID==186)];
node_leg_left = [find(ID==143):find(ID==150),find(ID==156),find(ID==155),find(ID==157),find(ID==154),find(ID==158),find(ID==159),...
    find(ID==160),find(ID==161),find(ID==166):find(ID==173),find(ID==191),find(ID==190),find(ID==189),find(ID==188),find(ID==187)];


% leg lengths : KNEE HEIGHT,SEATED - BUTTOCK DEPTH ; KNEE HEIGHT,SEATED
leg_length = [Knee_Heigth - Buttock_Depth; Knee_Heigth];
leg_length_Source = [Knee_Heigth_Source - Buttock_Depth_Source; Knee_Heigth_Source];
scale_factor = leg_length./leg_length_Source;

pos = [find(ID==175),find(ID==164)];
D = ones(size(node(pos,2:4),1),1)*node(ID==174,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==174,2:4);
node_out(pos,2:4)=(node(pos,2:4)-D)*scale_factor(1) + O72;
 
pos = [find(ID==151),find(ID==136),find(ID==141)];
D = ones(size(node(pos,2:4),1),1)*node(ID==164,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==164,2:4);
node_out(pos,2:4)=(node(pos,2:4)-D)*scale_factor(2) + O72;

% thigh circumference : 174 (origin) 181 180 179 186
scale_factor = Thigh_Circumference/Thigh_Circumference_Source;

pos = [find(ID==181),find(ID==180),find(ID==179),find(ID==186)];
D = ones(size(node(pos,2:4),1),1)*node(ID==174,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==174,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==174),find(ID==181),find(ID==180),find(ID==179)],2:4));
vx = sign(vx(1))*vx;
vy = ptsP(1,:)-ptsP(2,:);
vy = sign(vy(2))*vy'/norm(vy);
vz = cross(vx',vy')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;

% upper leg circumference : 175 (origin) 178 177 176 185
scale_factor = Upper_Leg_Circumference/Upper_Leg_Circumference_Source;

pos = [find(ID==178),find(ID==177),find(ID==176),find(ID==185)];
D = ones(size(node(pos,2:4),1),1)*node(ID==175,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==175,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==175),find(ID==178),find(ID==177),find(ID==176)],2:4));
vx = sign(vx(1))*vx;
vy = ptsP(1,:)-ptsP(2,:);
vy = sign(vy(2))*vy'/norm(vy);
vz = cross(vx',vy')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;
 
% knee circumference : 164 (origin) 162 163 165 184
scale_factor = Knee_Circumference/Knee_Circumference_Source;

pos = [find(ID==162),find(ID==163),find(ID==165),find(ID==184)];
D = ones(size(node(pos,2:4),1),1)*node(ID==164,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==164,2:4);

[vx,k,e,ptsP]=LeastSquaresPlane(node([find(ID==164),find(ID==162),find(ID==163),find(ID==165)],2:4));
vx = sign(vx(1))*vx;
vy = ptsP(1,:)-ptsP(2,:);
vy = sign(vy(2))*vy'/norm(vy);
vz = cross(vx',vy')';
R = [vx,vy,vz];
 
S72 = diag([1;scale_factor;scale_factor]);
node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;

% calf circumference : 151 (origin) 152 142 153 182
scale_factor = Calf_Circumference/Calf_Circumference_Source;

pos = [find(ID==152),find(ID==142),find(ID==153),find(ID==182)];
D = ones(size(node(pos,2:4),1),1)*node(ID==151,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==151,2:4);
 
[vz,k,e,ptsP]=LeastSquaresPlane(node([find(ID==151),find(ID==152),find(ID==142),find(ID==153)],2:4));
vz = sign(vz(3))*vz;
vy = ptsP(1,:)-ptsP(2,:);
vy = sign(vy(2))*vy'/norm(vy);
vx = cross(vy',vz')';
R = [vx,vy,vz];
 
S72 = diag([scale_factor;scale_factor;1]);
node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;

% ankle circumference : 136 (origin) 134 135 137 183
scale_factor = Ankle_Circumference/Ankle_Circumference_Source;

pos = [find(ID==134),find(ID==135),find(ID==137),find(ID==183)];
D = ones(size(node(pos,2:4),1),1)*node(ID==136,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==136,2:4);

[vz,k,e,ptsP]=LeastSquaresPlane(node([find(ID==136),find(ID==134),find(ID==135),find(ID==137)],2:4));
vz = sign(vz(3))*vz;
vy = ptsP(1,:)-ptsP(2,:);
vy = sign(vy(2))*vy'/norm(vy);
vx = cross(vy',vz')';
R = [vx,vy,vz];
 
S72 = diag([scale_factor;scale_factor;1]);
node_out(pos,2:4)=(node(pos,2:4)-D)*(R*S72/R) + O72;

% foot : 141 139 (length) ; 140 138 (breadth)
scale_factor = [Foot_Breadth;Foot_Length]./[Foot_Breadth_Source;Foot_Length_Source];

pos = [find(ID==139),find(ID==140),find(ID==138)];
D = ones(size(node(pos,2:4),1),1)*node(ID==141,2:4);
O72 = ones(size(node_out(pos,2:4),1),1)*node_out(ID==141,2:4);

[vz,k,e,ptsP]=LeastSquaresPlane(node([find(ID==141),find(ID==139),find(ID==140),find(ID==138)],2:4));
vz = sign(vz(3))*vz;
vx = ptsP(1,:)-ptsP(2,:);
vx = sign(vx(1))*vx'/norm(vx);
vy = cross(vz',vx')';
R = [vx,vy,vz];
 
S72 = diag([scale_factor(2);scale_factor(1);1]);
node_out(pos,2:4)=(nodes(pos,2:4)-D)*(R*S72/R) + O72;

% left leg (symetry) : 
node_out(node_leg_left,2:4) = node_out(node_leg_right,2:4);
node_out(node_leg_left,3) = -node_out(node_leg_left,3);

clear i pos vx vy vz k e ptsP D R scale_factor S72;
clear O72 R72 S72b temp leg;
 
nodes(Lower_Body_ID,:) = node_out;

%=========== Scaling Neck =================================================
%--- Vertebral Body diameter [mm] (Kasai et al. 1996) (male) -------
Body_diameter_M =[
    8.79 0.86
    9.77 1.08
    10.95 0.45
    11.32 1.12
    12.05 0.58
    12.33 0.74
    12.83 0.88
    13.30 0.67
    13.78 0.53
    14.13 0.66
    14.68 0.49
    15.70 0.70
    16.56 0.82
    17.60 0.90
    17.73 0.85
    17.77 0.77
    18.01 0.58
    18.23 0.45];

x = 1:18;
Mean_D_Age = interp1(x,Body_diameter_M(:,1),Age);
Mean_D_Age_Source = interp1(x,Body_diameter_M(:,1),Source_Age);

%--- Vertebral Body cevical length [mm] (Kasai et al. 1996) (male) --
Cervical_length_M =[
19.5 1.8
28.2 2.1
34.4 2.3
39.3 1.7
42.9 2.2
45.2 1.4
47.0 1.7
49.1 1.3
50.9 2.3
53.1 1.9
55.0 2.2
57.0 2.0
61.1 1.5
65.9 2.4
68.2 1.5
68.9 1.8
70.2 1.9
70.7 2.7];

Mean_L_Age = interp1(x,Cervical_length_M(:,1),Age);
Mean_L_Age_Source = interp1(x,Cervical_length_M(:,1),Source_Age);

%--- Facet angels [deg] (Kasai et al. 1996) [c7 c6 c5 c4 c3] --------
Facet_Angles_M =[
    66.9 4.2 63.8 3.9 57.1 3.6 53.2 3.0 42.1 2.6
    59.0 4.0 55.9 5.3 53.2 3.2 49.4 3.1 38.3 3.2
    51.9 4.3 50.2 2.1 50.1 3.8 44.9 4.5 34.8 2.6
    48.9 4.9 47.9 3.6 48.3 3.1 42.5 2.0 32.6 2.9
    46.1 2.4 45.4 3.1 46.2 3.8 39.9 2.0 30.4 2.5
    45.0 2.9 43.9 3.4 44.0 3.7 39.1 2.8 29.8 2.3
    43.8 2.8 43.1 3.9 42.9 4.0 38.2 2.3 29.5 3.0
    42.7 2.8 43.2 3.4 43.0 2.3 37.7 2.3 29.1 3.7
    42.0 3.7 42.0 3.9 42.8 3.1 37.4 2.9 28.6 2.8
    41.0 3.1 42.0 3.4 42.0 3.7 37.3 2.8 27.6 3.1
    39.6 3.5 41.1 3.3 42.0 3.6 36.6 3.3 27.3 2.8
    40.8 3.8 40.1 2.2 42.0 3.3 36.8 3.7 26.6 2.8
    39.8 3.0 40.4 2.2 42.1 2.8 37.2 2.8 27.1 3.5
    40.3 4.1 40.8 3.6 42.1 4.9 36.7 3.2 27.3 3.4
    39.9 4.0 41.0 3.5 40.6 3.2 35.1 1.9 27.3 4.0
    40.1 3.5 41.1 3.5 41.2 3.4 35.3 2.0 28.2 3.7
    39.2 3.4 41.2 2.4 40.8 3.1 36.2 3.7 27.4 4.3
    39.5 4.2 41.2 4.1 42.3 2.5 36.1 4.5 27.4 4.0];

Mean_F_Age = interp1(x,Facet_Angles_M(:,1:2:end),Age);
Mean_F_Age_Source = interp1(x,Facet_Angles_M(:,1:2:end),Source_Age);

%--- NECK HEIGHT GEBOD (from old Kriging points) --------------------
Neck_Height_Age = interp1([1.5 3 6],[83.6746 92.04 107.028],Age, "linear", "extrap");
Neck_Height_Source = interp1([1.5 3 6],[83.6746 92.04 107.028],Source_Age,"linear", "extrap");

%--- NECK CIRCUMFERENCE GEBOD ---------------------------------------
Neck_Circ_Age = interp1([1.5 3 6],[219.5 238.5 260],Age,"linear", "extrap");
Neck_Circ_Age_Source = interp1([1.5 3 6],[219.5 238.5 260],Source_Age,"linear", "extrap");

%--- C2 Canal Diameter [mm] (Wang et al. 2001) (male) ---------------
Mean_C2_Canal_Age = interp1([1.5 3 6],[13.6 14.2 15.4],Age,"linear", "extrap");
Mean_C2_Canal_Source = interp1([1.5 3 6],[13.6 14.2 15.4],Source_Age,"linear", "extrap");

%===== Scale Control points ===============================================
%--------------------------------------------------------------------
neck_nodes = [301080 301081 301082 301083 301084 301085 301086 301087];
% _b -> bottow, _t -> topp of vertebrae
% _a -> anterior, _p -> posterior
b_nodes = [301111; 301107; 301103; 301099; 301095; 301091];
t_nodes = [301110; 301106; 301102; 301098; 301094; 301090];
p_nodes = [301108; 301104; 301100; 301096; 301092; 301088];
a_nodes = [301109; 301105; 301101; 301097; 301093; 301089];
at_nodes = [301052; 301056; 301060; 301064; 301068; 301074];
pt_nodes = [301053; 301057; 301061; 301065; 301069; 301071];
ab_nodes = [301050; 301055; 301059; 301063; 301067; 301073];
pb_nodes = [301051; 301054; 301058; 301062; 301066; 301070];
C2_b_Nodes = [301075 301072];
Extra_nodes = [301001 301077 301076 301020 301078 301079 301021 301002
    301003 301004 301022 301023 301024 301025 0 0
    301005 301006 301026 301027 301028 301029 0 0
    301007 301008 301030 301031 301032 301033 0 0
    301009 301010 301034 301035 301036 301037 0 0
    301011 301012 301038 301039 301040 301041 0 0
    301014 301015 301042 301043 301044 301045 0 0];

Extra_nodes2 = [301013 301016 301017 301018 301019 301046 301047 301048 301049];

r_nodes = [301022 301025; 301027 301028; 301030 301031; 301034 301035; 301038 301039];
l_nodes = [301023 301024; 301026 301029; 301032 301033; 301036 301037; 301041 301040];

%------ Measure length of spine on Source control points ------------
Spine_length_Source = norm(nodes(find(ID==301111),2:4)-nodes(find(ID==301013),2:4));

%----------- Neck Circumference -------------------------------------
Neck_Circ_Nodes = 301080:301083;
Neck_F = nodes([find(ID==301080)],2:4);
Neck_B = nodes([find(ID==301081)],2:4);
Neck_R = nodes([find(ID==301082)],2:4);
Neck_L = nodes([find(ID==301083)],2:4);

Depth = (norm(Neck_B(1,:)-Neck_F(1,:)));
Width = (norm(Neck_R(1,:)-Neck_L(1,:)));
a = Width/2;
b = Depth/2;
h = (a-b)^2/(a+b)^2;
%Neck_Circ_FE = pi*(a+b)*(1+3*h/(10+sqrt(4-3*h)));
%scale_Circ = Neck_Circ_FE/Neck_Circ_Age;
scale_Circ = Neck_Circ_Age_Source/Neck_Circ_Age;

FB_u = (Neck_F(1,:)-Neck_B(1,:));
n_depth_u = (FB_u)/norm(FB_u);
scaled_FB_u = norm(FB_u)*scale_Circ;
d = (scaled_FB_u-norm(FB_u))/2;

nodes(find(ID==301080),2:4) = nodes(find(ID==301080),2:4)-n_depth_u*d;
nodes(find(ID==301081),2:4) = nodes(find(ID==301081),2:4)+n_depth_u*d;

RL_u = (Neck_R(1,:)-Neck_L(1,:));
n_width_u = RL_u/norm(RL_u);
scaled_RL_u = norm(RL_u)*scale_Circ;
d = (scaled_RL_u-norm(RL_u))/2;
nodes(find(ID==301082),2:4) = nodes(find(ID==301082),2:4)-n_width_u*d;
nodes(find(ID==301083),2:4) = nodes(find(ID==301083),2:4)+n_width_u*d;

%----------- Vertebral diameter -------------------------------------
for i = 1:6
    p = nodes([find(ID==p_nodes(i))],2:4);
    a = nodes([find(ID==a_nodes(i))],2:4);
    
    vect_dia = p-a;
    dist_dia(i) = norm(vect_dia);
    dir_dia(i,:) = vect_dia/dist_dia(i);
end

p = nodes([find(ID==C2_b_Nodes(2))],2:4);
a = nodes([find(ID==C2_b_Nodes(1))],2:4);

vect_dia = p-a;
dist_dia(7) = norm(vect_dia);
dir_dia(7,:) = vect_dia/dist_dia(7);

mean_D = sum(dist_dia(2:6))/5;
%scale_D = Mean_D_Age/mean_D;
scale_D = Mean_D_Age/Mean_D_Age_Source;
dia_Scaled = dist_dia*scale_D;


for i = 1:7
    if i<7
        nodes(find(ID==p_nodes(i)),2:4) = nodes(find(ID==a_nodes(i)),2:4)+dia_Scaled(i)*dir_dia(i,:);
        
        nodes(find(ID==pt_nodes(i)),2:4) = nodes(find(ID==pt_nodes(i)),2:4)+(dia_Scaled(i)-dist_dia(i))*dir_dia(i,:);
        nodes(find(ID==pb_nodes(i)),2:4) = nodes(find(ID==pb_nodes(i)),2:4)+(dia_Scaled(i)-dist_dia(i))*dir_dia(i,:);
        
        nodes(find(ID==b_nodes(i)),2:4) = nodes(find(ID==b_nodes(i)),2:4)+(dia_Scaled(i)-dist_dia(i))/2*dir_dia(i,:);
        nodes(find(ID==t_nodes(i)),2:4) = nodes(find(ID==t_nodes(i)),2:4)+(dia_Scaled(i)-dist_dia(i))/2*dir_dia(i,:);
    else
        nodes(find(ID==C2_b_Nodes(2)),2:4) = nodes(find(ID==C2_b_Nodes(1)),2:4)+dia_Scaled(i)*dir_dia(i,:);
    end
    if i==1
        for t = 1:size(Extra_nodes,2)
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+(dia_Scaled(i)-dist_dia(i))*dir_dia(i,:);
        end
    elseif i>1
        for t = 1:size(Extra_nodes,2)-2
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+(dia_Scaled(i)-dist_dia(i))*dir_dia(i,:);
        end
    end
end

%----------- Vertebral canal diameter -------------------------------

C2_aCanal = nodes([find(ID==C2_b_Nodes(2))],[2 4]);
C2_pCanal = nodes([find(ID==Extra_nodes(7,1))],[2 4]);

vect_Canal = C2_aCanal-C2_pCanal;
dist_Canal = norm(vect_Canal);
dir_Canal = vect_Canal/dist_Canal;

Canal_Scale = Mean_C2_Canal_Age/Mean_C2_Canal_Source;
Canal_Scaled = dist_Canal*Canal_Scale;
Offset = Canal_Scaled-dist_Canal;

for i = 1:7
    if i==1
        for t = 1:size(Extra_nodes,2)
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+Offset*dir_dia(i,:);
        end
    elseif i>1
        for t = 1:size(Extra_nodes,2)-2
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+Offset*dir_dia(i,:);
        end
    end
end

%----------- Vertebral height ---------------------------------------
for i = 1:6
    b = nodes([find(ID==b_nodes(i,1))],2:4);
    t = nodes([find(ID==t_nodes(i,1))],2:4);
    
    vect_height = t-b;
    length_height(i) = norm(vect_height);
    dir_height(i,:) = vect_height/length_height(i);
end

Length = sum(length_height(2:end));
%Scale_L = Mean_L_Age/Length;
Scale_L = Mean_L_Age/Mean_L_Age_Source;
Length_Scaled = length_height*Scale_L;

for i = 1:6
    nodes(find(ID==t_nodes(i)),2:4) = nodes(find(ID==b_nodes(i)),2:4)+Length_Scaled(i)*dir_height(i,:);
    nodes(find(ID==at_nodes(i)),2:4) = nodes(find(ID==at_nodes(i)),2:4)+(Length_Scaled(i)-length_height(i))*dir_height(i,:);
    nodes(find(ID==pt_nodes(i)),2:4) = nodes(find(ID==pt_nodes(i)),2:4)+(Length_Scaled(i)-length_height(i))*dir_height(i,:);
    nodes(find(ID==a_nodes(i)),2:4) = nodes(find(ID==a_nodes(i)),2:4)+(Length_Scaled(i)-length_height(i))/2*dir_height(i,:);
    nodes(find(ID==p_nodes(i)),2:4) = nodes(find(ID==p_nodes(i)),2:4)+(Length_Scaled(i)-length_height(i))/2*dir_height(i,:);
    if i==1
        for t = 1:size(Extra_nodes,2)
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+(Length_Scaled(i)-length_height(i))*dir_height(i,:);
        end
    elseif i>1
        for t = 1:size(Extra_nodes,2)-2
            nodes(find(ID==Extra_nodes(i,t)),2:4) = nodes(find(ID==Extra_nodes(i,t)),2:4)+(Length_Scaled(i)-length_height(i))*dir_height(i,:);
        end
    end
end

%----------- Facet angles -------------------------------------------
clear b t
for i = 1:5    
    fb(i,:) = nodes([find(ID==r_nodes(i,2))],2:4);
    ft = nodes([find(ID==r_nodes(i,1))],2:4);    
    Facet_vector = fb(i,:)-ft;     
    alpha_f(i) = atand(Facet_vector(1)/Facet_vector(3));
        
    b = nodes([find(ID==b_nodes(i+1,1))],2:4);
    t = nodes([find(ID==t_nodes(i+1,1))],2:4);    
    Body_vector = b-t;
    alpha_body(i) = atand(Body_vector(1)/Body_vector(3));
       
    Facet_vector = Facet_vector([1 3]);
    Body_vector = Body_vector([1 3]); 
    
    d_f(i) = norm(Facet_vector);
    
    Facet_angle(i) = acosd((dot(Facet_vector,Body_vector))/(norm(Facet_vector)*norm(Body_vector)));            
end

%scale_f = fliplr(Mean_F_Age)./Facet_angle;
scale_f = Mean_F_Age/Mean_F_Age_Source;
Scaled_facet = Facet_angle.*scale_f;
Adjust_angle = Facet_angle-Scaled_facet;
alpha_f_new = alpha_f-Adjust_angle;

x1 = d_f.*sind(alpha_f); 
z1 = d_f.*cosd(alpha_f);
x2 = d_f.*sind(alpha_f_new); 
z2 = d_f.*cosd(alpha_f_new);

x = x1-x2;
z = z1-z2;

facets_t_scaled = [fb(:,1)+x' fb(:,3)+z'];

for i = 1:5
    nodes(find(ID==r_nodes(i,2)),2) = facets_t_scaled(i,1);
    nodes(find(ID==r_nodes(i,2)),4) = facets_t_scaled(i,2);
    nodes(find(ID==l_nodes(i,2)),2) = facets_t_scaled(i,1);
    nodes(find(ID==l_nodes(i,2)),4) = facets_t_scaled(i,2);
end

%----------- Cervical Length ----------------------------------------
Scale_lit = Neck_Height_Age/Neck_Height_Source
Spine_Vector = nodes(find(ID==b_nodes(1)),2:4)-nodes(find(ID==Extra_nodes2(1)),2:4);
Spine_current_length = norm(Spine_Vector)
Spine_Direction = -Spine_Vector/Spine_current_length;
Scaled_baseline_length = Spine_length_Source*Scale_lit
Scale_Current = Scaled_baseline_length/Spine_current_length;
%Scaled_Current_length = Scale_Current*Spine_current_length
Scaled_Current_length = Scale_lit*Spine_current_length
Distribut_offset = (Scaled_Current_length-Spine_current_length)/6

for i = 1:6
    for k = 1:7-i
        if i+k<7
            nodes(find(ID==b_nodes(i+k)),2:4) = nodes(find(ID==b_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==t_nodes(i+k)),2:4) = nodes(find(ID==t_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==a_nodes(i+k)),2:4) = nodes(find(ID==a_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==p_nodes(i+k)),2:4) = nodes(find(ID==p_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==at_nodes(i+k)),2:4) = nodes(find(ID==at_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==pt_nodes(i+k)),2:4) = nodes(find(ID==pt_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==ab_nodes(i+k)),2:4) = nodes(find(ID==ab_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
            nodes(find(ID==pb_nodes(i+k)),2:4) = nodes(find(ID==pb_nodes(i+k)),2:4)+Distribut_offset*Spine_Direction;
        end
        for t = 1:size(Extra_nodes,2)
            if isempty(nodes(find(ID==Extra_nodes(i+k,t)),2:4))<1
                nodes(find(ID==Extra_nodes(i+k,t)),2:4) = nodes(find(ID==Extra_nodes(i+k,t)),2:4)+Distribut_offset*Spine_Direction;
            end
        end
    end
    
    for m = 1:length(C2_b_Nodes)
        nodes(find(ID==C2_b_Nodes(m)),2:4) = nodes(find(ID==C2_b_Nodes(m)),2:4)+Distribut_offset*Spine_Direction;
    end
    
    for t = 1:size(Extra_nodes2,2)
        nodes(find(ID==Extra_nodes2(t)),2:4) = nodes(find(ID==Extra_nodes2(t)),2:4)+Distribut_offset*Spine_Direction;
    end
    
    for u = 1:size(Head_Nodes,2)
        nodes(find(ID==Head_Nodes(u)),2:4) = nodes(find(ID==Head_Nodes(u)),2:4)+Distribut_offset*Spine_Direction;
    end   
   
    if i<4
        for v = 1:size(Neck_Circ_Nodes,2)
            nodes(find(ID==Neck_Circ_Nodes(v)),2:4) = nodes(find(ID==Neck_Circ_Nodes(v)),2:4)+Distribut_offset*Spine_Direction;
        end      
    end        
end

%--- Rigid Translation [dx dz] --------------------------------------
Offs_torso_T1_2 = nodes(find(ID==301111),[2 4])-nodes(find(ID==84),[2 4]);
Translation_Age = [Offs_torso_T1_1(1)-Offs_torso_T1_2(1) 0 Offs_torso_T1_1(2)-Offs_torso_T1_2(2)]
Head_Neck_Nodes = [203:216,301001:301083,301088:301111];

for z = 1:size(Head_Neck_Nodes,2)
  nodes(find(ID==Head_Neck_Nodes(z)),2:4) = nodes(find(ID==Head_Neck_Nodes(z)),2:4)+Translation_Age;
end      


endfunction
