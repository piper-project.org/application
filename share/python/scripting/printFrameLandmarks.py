# This script prints the landmarks required to compute frames which are part of an entity.
# If a bibliography entry is given (optionnal) only the frames defined in that entity are printed.
#
# arguments: 
#  name_of_entity [bibliography]
# 
# exemple:
#  Spine ISB_BCS

# author: Thomas Lemaire, date: 2017

import piper.app
import piper.anatomyDB

if len(sys.argv) not in {2,3} :
    raise RuntimeError("Invalid arguments")
else:
    if not piper.anatomyDB.exists(sys.argv[1]):
        piper.app.logInfo("Entity {0} does not exist in anatomyDB".format(sys.argv[1]))
    elif 3 == len(sys.argv) and not piper.anatomyDB.exists(sys.argv[2]):
        piper.app.logInfo("Bibliography {0} does not exist in anatomyDB".format(sys.argv[2]))
    else:
        objectName = sys.argv[1]
        frameList = []
        if piper.anatomyDB.isEntitySubClassOf(objectName, "Frame"):
            frameList.append(objectName)
        frameList += piper.anatomyDB.getSubPartOfList(objectName, "Frame", True)
        outputString = "Query with {0} ({1})".format(objectName, len(frameList))
        if 3 == len(sys.argv):
            bibliography = sys.argv[2]
            frameListTmp = []
            for frame in frameList:
                if piper.anatomyDB.isEntityFromBibliography(frame, bibliography):
                    frameListTmp.append(frame)
            frameList = frameListTmp
            outputString += " - from bibliography {0} ({1})".format(bibliography, len(frameList))
        outputString += ":\n"
        
        ff = piper.anatomyDB.FrameFactory.instance()
        for frame in frameList:
            if ff.isFrameRegistered(frame):
                outputString += frame + ": "
                landmarkList = ff.getFrameRequiredLandmarks(frame)
                for l in landmarkList:
                    outputString += "["+", ".join(l)+"]; "
                outputString += "\n"
        outputString = outputString.replace("\n","<br>\n")
        piper.app.logInfo(outputString)