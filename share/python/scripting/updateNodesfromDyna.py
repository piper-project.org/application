# This script updates the HBM nodes with node coordinadate stored in a dyna .k file 
# To be used together with the exportSelectedGeometryToDyna.py script
#
# arguments:
#  /path/to/mesh.k [history name]
 
# author:  Matthieu Mear date: 2017



import piper.hbm
import piper.app

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not ( len(sys.argv) in {2,3} ) :
    raise RuntimeError("Invalid arguments")
else :
    DynaFilePath = sys.argv[1]
    # reading Dyna vertices and updating the corresponding node
    fem = model.fem() # shortcut
    readingNodes = False #tells us if we are reading Nodes
        # setup model in history
    if len(sys.argv) == 3:
        model.history().addNewHistory(sys.argv[2])

    # Read the updated Dyna file
    with open(DynaFilePath, 'r') as DynaFile:
        piper.app.logInfo("Reading Dyna file {0} ...".format(DynaFilePath))
        for line in DynaFile:
            # we are not reading nodes
            if line.startswith("*ELEMENT_SHELL" or "*END"):
                readingNodes = False
                continue
            # Here we are reading nodes
            if line.startswith("*NODE"):
                readingNodes = True
            # read and update node coordinates, skip if line start with  #END, $# or #NODE
            if readingNodes and not line.startswith("*END") and not line.startswith("$#") and not line.startswith("*NODE"):
                node = fem.getNode(int(line[0:8]))
                node.setCoord(float(line[9:24]), float(line[25:40]), float(line[41:56]))
    piper.app.modelUpdated() # send the appropriate signal to refresh the application
