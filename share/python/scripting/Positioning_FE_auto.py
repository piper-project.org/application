# -*- coding: utf-8 -*-
#This script writes LS-Dyna inputs for positionning a FE model.
#This file (Author: UCBL-Ifsttar) is licensed under a Creative Commons 
#Attribution 4.0 International License. (https://creativecommons.org/licenses/by/4.0/)
#
#This work has received funding from the European Union Seventh Framework 
#Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
#Contributors are Anicet Le Ruyet.
#
#See the PIPER documentation for more info.
#
#Arguments: directory of export (ex: Y://my_dir//);
#name of model (ex: import_model);
#name of controllers (ex: ['Left_knee','gebod_stylion_right_xyz','Pelvis_right_hip_frame_to_B_Right_femur_rx_ry_rz']);
#DOF for each controller (ex:[['nan',-75,'nan'],['nan','nan',200],['nan','nan','nan','nan','nan',-45]]);
#number of positions (ex: 4)



import piper.hbm
import piper.app
import math
	
def fonc_concat_py(N,L):
#    N='%.5f' % N
    N="{0:.10g}".format(N)
    str1=' '*(L-len(N))+N
    return str1
    
    
def fonc_def_nodes_py(nid):
    NODES=dict()
    NODES[0]=['*NODE']
    NODES[1]=['$#   nid               x               y               z      tc      rc']
    j=2
    for i in range(len(nid)):
        NODES[j]=[fonc_concat_py(nid[i][0],8),fonc_concat_py(nid[i][1],16),fonc_concat_py(nid[i][2],16),fonc_concat_py(nid[i][3],16)];
        j+=1
    return NODES
    
def fonc_define_elem_beam_py(eid,pid,n1,n2,local):
    ELEM=dict()    
    ELEM[0]=['*ELEMENT_BEAM']
    ELEM[1]=['$#   eid     pid      n1      n2      n3     rt1     rr1     rt2     rr2   local']
    ELEM[2]=[fonc_concat_py(eid,8),fonc_concat_py(pid,8),fonc_concat_py(n1,8),fonc_concat_py(n2,8),fonc_concat_py(local,48)]
    return ELEM

def fonc_define_CURVE_py(lcid,ABSC,ORD,titre):
    CURVE=dict()
    q=0
    tit=['$ '+titre]
    CURVE[q]=['*DEFINE_CURVE']
    CURVE[q+1]=tit
    CURVE[q+2]=['$     lcid      sidr      scla      sclo      offa      offo']
    CURVE[q+3]=[fonc_concat_py(lcid,10)]
    CURVE[q+4]=['$           abscissa            ordinate']
    p=q+5
    for i in range(len(ABSC)):      
        CURVE[p]=[fonc_concat_py(ABSC[i],20),fonc_concat_py(ORD[i],20)]
        p+=1
    return CURVE
    
def fonc_define_boundary_pre_mot_py(nid,dof,vad,lcid):
    mot=dict()
    mot[0]=['*BOUNDARY_PRESCRIBED_MOTION_NODE']
    mot[1]=['$#     nid       dof       vad      lcid        sf       vid     death     birth']
    mot[2]=[fonc_concat_py(nid,10),fonc_concat_py(dof,10),fonc_concat_py(vad,10),fonc_concat_py(lcid,10)]
    return mot
    
def fonc_define_CNRB(pid,cid,nsid,pnode):
    CNRB=dict()
    CNRB[0]='*CONSTRAINED_NODAL_RIGID_BODY'
    CNRB[1]='$#     pid       cid      nsid     pnode      iprt    drflag    rrflag'
    CNRB[2]=[fonc_concat_py(pid,10),fonc_concat_py(cid,10),fonc_concat_py(nsid,10),fonc_concat_py(pnode,10)]
    return CNRB

def fonc_def_set_node_py(nodeset,num_node_set):
    nset=dict()
    nset[0]='*SET_NODE_LIST_TITLE'
    nset[1]='set node sid='+str(num_node_set)
    nset[2]='$#     sid       da1       da2       da3       da4'
    nset[3]=fonc_concat_py(num_node_set,10)+'     0.000     0.000     0.000     0.000'
    nset[4]='$#    nid1      nid2      nid3      nid4      nid5      nid6      nid7      nid8'
    p=5;q=0
    b=len(nodeset)%8
    for i in range(len(nodeset),len(nodeset)+b,1):
        nodeset.append('0')
    nodeset=[float(i) for i in nodeset]
    while q<len(nodeset)-8:
        nset[p]=[fonc_concat_py(nodeset[q],10),fonc_concat_py(nodeset[q+1],10),fonc_concat_py(nodeset[q+2],10),fonc_concat_py(nodeset[q+3],10),
        fonc_concat_py(nodeset[q+4],10),fonc_concat_py(nodeset[q+5],10),fonc_concat_py(nodeset[q+6],10),fonc_concat_py(nodeset[q+7],10)]
        p+=1;q+=8
    return nset
        
        
    return nset
            
def fonc_export_py(MAT,filename,folder):
    g=open(folder+filename,'w')
    try:
        for j in range(len(MAT)):
            tmp=map(str,MAT[j])
            line="".join(tmp)
            g.write("%s\n" % line)
        g.close()
    finally: 
        g.close()

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step
        
###############################################################################


def printUsage():
	piper.app.logWarning("wrong script parameters, expected: /path/to/export/directory [history name]")

import time
from collections import defaultdict
model = piper.app.Context.instance().project().model()
if model.empty():
    piper.app.logWarning("Model is empty")
elif not simulationController.isAllNodesActivated():
        piper.app.logWarning("\"all nodes\" update is not activated")
else:
    dir_e=sys.argv[1]
    name_model=sys.argv[2]
    controller_input=eval(sys.argv[3])
    ddl=eval(sys.argv[4])
    no_pos_interm=eval(sys.argv[5])

    pos_inter=dict()
    for p in range(no_pos_interm):
        ddl_temp=[]
        for i in range(len(ddl)):
            ddl_temp.append([])
            for j in ddl[i]:
                if j!='nan':
                    ddl_temp[i].append((p+1)*float(j)/no_pos_interm)
                else:
                    ddl_temp[i].append(j)
        pos_inter[p]=ddl_temp

    global cpt,start_time
    if not "cpt" in globals():
        cpt = 0
    if not "start_time" in globals():
        start_time = time.time()
    if cpt<=no_pos_interm-1:
        q=0
        for controllerName in controller_input:
            if controllerName in landmarkController.getControllerNames():
                for i in range(3):
                    if ddl[q][i]!='nan':
                        landmarkController.setTarget(controllerName,i,pos_inter[cpt][q][i])
            elif controllerName in jointController.getControllerNames():
                for i in range(3):
                    if ddl[q][i]!='nan':
                        jointController.setTarget(controllerName,i+3,math.radians(pos_inter[cpt][q][i]))
            elif controllerName in frameController.getControllerNames():
                for i in range(3):
                    if len(ddl[q]) <= 3:
                        # Backward compatibility, with only 3 coordinates (rx, ry, rx)
                        if ddl[q][i]!='nan':
                            frameController.setTarget(controllerName,i+3,math.radians(pos_inter[cpt][q][i]))
                    else:
                        # New frame controller has 6 coordinates (x, y, z, rx, ry, rz)
                        if ddl[q][i+3]!='nan':
                            frameController.setTarget(controllerName,i+3,math.radians(pos_inter[cpt][q][i+3]))
                        if ddl[q][i]!='nan':
                            frameController.setTarget(controllerName,i,pos_inter[cpt][q][i])
            q+=1
        
        vit=simulationController.getMaxCartesianVelocity()
        q=0;bol=1;
        for controllerName in controller_input:
            controller = None
            if controllerName in landmarkController.getControllerNames():
                controller = landmarkController
            elif controllerName in jointController.getControllerNames():
                controller = jointController
            elif controllerName in frameController.getControllerNames():
                controller = frameController
            current_val=controller.getCurrentPosition(controllerName)
            if controller == landmarkController:
                for i in range(3):
                    if pos_inter[cpt][q][i]!='nan':
                        if abs(current_val[i]-pos_inter[cpt][q][i])<2:
                            bol = bol and 1
                        else:
                            bol = bol and 0
            elif controller == frameController:
                for i in range(3):
                    if pos_inter[cpt][q][i]!='nan':
                        if abs(current_val[3+i]-math.radians(pos_inter[cpt][q][i]))<math.radians(2):
                            bol = bol and 1
                        else:
                            bol=bol and 0
            elif controller == jointController:
                for i in range(3):
                    if pos_inter[cpt][q][i]!='nan':
                        if abs(current_val[3+i]-math.radians(pos_inter[cpt][q][i]))<math.radians(2):
                            bol = bol and 1
                        else:
                            bol=bol and 0            
            q+=1
            
        if (vit<=0.05 and bol) or abs(time.time()-start_time)>=20:
            historyName = "model_"+str(cpt)
            piper.app.Context.instance().addNewHistory(historyName)
            simulationController.updateModelNodes()
            start_time=time.time()
            cpt+=1   

    if cpt==no_pos_interm:
        model.history().setCurrentModel(name_model)
        metadata = piper.app.Context.instance().project().model().metadata()
        fem = piper.app.Context.instance().project().model().fem()
        piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();
        no_temp=[[]];x_temp=[[]];y_temp=[[]];z_temp=[[]];
        for (name,entity) in metadata.entities().iteritems() :
            if piper.anatomyDB.isEntitySubClassOf(name, "Bone"):
                ids = entity.getEntityNodesIds(fem)
                for nodeId in ids:
                    coord = fem.getNode(nodeId).get().flatten()
                    no_temp[0].append(piperIdToIdKey[nodeId][1].id)
                    x_temp[0].append(coord[0])
                    y_temp[0].append(coord[1])
                    z_temp[0].append(coord[2])
        for i in range(no_pos_interm):
            model.history().setCurrentModel('model_'+str(i))
            metadata = piper.app.Context.instance().project().model().metadata()
            fem = piper.app.Context.instance().project().model().fem()
            piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();
            no_temp.append([]);x_temp.append([]);y_temp.append([]);z_temp.append([]);
            for (name,entity) in metadata.entities().iteritems() :
                if piper.anatomyDB.isEntitySubClassOf(name, "Bone"):
                    ids = entity.getEntityNodesIds(fem)
                    for nodeId in ids:
                        coord = fem.getNode(nodeId).get().flatten()
                        no_temp[i+1].append(piperIdToIdKey[nodeId][1].id)
                        x_temp[i+1].append(coord[0])
                        y_temp[i+1].append(coord[1])
                        z_temp[i+1].append(coord[2])
    
                        
        nodeset_cnrb=dict();node_cnrb=dict();CNRB=dict();beam_cnrb=dict();nod=list();node=dict();
        ELEM=dict();CURVE_X=dict();CURVE_Y=dict();CURVE_Z=dict();mot_x=dict();mot_y=dict();mot_z=dict();
        nid=dict();CURVE=dict();mot=dict();
        no_t=defaultdict(list);x_t=[[]];y_t=defaultdict(list);z_t=defaultdict(list)
        a=100001;b=200001;c=300001;k=0;
    
        nod=no_temp[0]
        z=0;temp=[];
        for j in range(0,len(nod),10):
            nid[k]=[k+1,x_temp[0][j],y_temp[0][j],z_temp[0][j]] # Noeuds tractés
            ELEM_temp=fonc_define_elem_beam_py(k+1,1,nod[j],nid[k][0],2) # poutres tract-CNRB
            tp=len(ELEM)
            for x in range(len(ELEM_temp)):
                ELEM[x+tp]=ELEM_temp[x]
            curvx=[0];curvy=[0];curvz=[0]
            for i in range(no_pos_interm):
                curvx.append(x_temp[i+1][j]-x_temp[0][j])
                curvy.append(y_temp[i+1][j]-y_temp[0][j])
                curvz.append(z_temp[i+1][j]-z_temp[0][j])
            curvx.append(x_temp[i+1][j]-x_temp[0][j])
            curvy.append(y_temp[i+1][j]-y_temp[0][j])
            curvz.append(z_temp[i+1][j]-z_temp[0][j])
            tps=[0];
            for i in range(no_pos_interm):
                tps.append((i+1)*200/no_pos_interm)
            tps.append(250)
            CURVE_X=fonc_define_CURVE_py(a,tps,curvx,'dispx')
            CURVE_Y=fonc_define_CURVE_py(b,tps,curvy,'dispy')
            CURVE_Z=fonc_define_CURVE_py(c,tps,curvz,'dispz')
            mot_x=fonc_define_boundary_pre_mot_py(k+1,1,2,a)
            mot_y=fonc_define_boundary_pre_mot_py(k+1,2,2,b)
            mot_z=fonc_define_boundary_pre_mot_py(k+1,3,2,c)
            tp=len(CURVE)
            for x in range(len(CURVE_X)):
                CURVE[tp+x]=CURVE_X[x]
            tp=len(CURVE)
            for x in range(len(CURVE_Y)):
                CURVE[tp+x]=CURVE_Y[x]
            tp=len(CURVE)
            for x in range(len(CURVE_Z)):
                CURVE[tp+x]=CURVE_Z[x]
            tp=len(mot)
            for x in range(len(mot_x)):
                mot[tp+x]=mot_x[x]
            tp=len(mot)
            for x in range(len(mot_y)):
                mot[tp+x]=mot_y[x]
            tp=len(mot)
            for x in range(len(mot_z)):
                mot[tp+x]=mot_z[x]
            a+=1;b+=1;c+=1;k+=1;z+=1;
        nodeset_PIPER=fonc_def_set_node_py(nod,1)
    
        fonc_export_py(CURVE,'CURVE.k',dir_e)
        fonc_export_py(mot,'motion.k',dir_e)
        noeuds=fonc_def_nodes_py(nid);
        fonc_export_py(noeuds,'noeuds_extr_beam.k',dir_e);
        fonc_export_py(ELEM,'ele_beam.k',dir_e);
        fonc_export_py(nodeset_PIPER,'nodeset_PIPER.k',dir_e);
        
        # Définition du main
        p=0
        file_main={}
        file_main[p]='*KEYWORD memory=200000000'
        p+=1
        file_main[p]='*CONTROL_TERMINATION'
        p+=1        
        file_main[p]='$#  endtim    endcyc     dtmin    endeng    endmas'      
        p+=1
        file_main[p]='     250.0         0       0.0       0.01.000000E8'
        p+=1
        file_main[p]='*DATABASE_BINARY_D3PLOT'
        p+=1
        file_main[p]='$#      dt      lcdt      beam     npltc    psetid'     
        p+=1
        file_main[p]='       1.0         0         0         0         0'
        p+=1
        file_main[p]='$#   ioopt'     
        p+=1
        file_main[p]='         0'
        p+=1
        file_main[p]='*PART'
        p+=1
        file_main[p]='$#                                                                         title'
        p+=1
        file_main[p]='BEAM'
        p+=1
        file_main[p]='$#     pid     secid       mid     eosid      hgid      grav    adpopt      tmid'
        p+=1
        file_main[p]='         1         1         1         0         0         0         0         0'
        p+=1
        file_main[p]='*SECTION_BEAM_TITLE'
        p+=1
        file_main[p]='beam'
        p+=1
        file_main[p]='$#   secid    elform      shrf   qr/irid       cst     scoor       nsm'   
        p+=1
        file_main[p]='         1         6       1.0         2         0       0.0       0.0'
        p+=1
        file_main[p]='$#     vol      iner       cid        ca    offset     rrcon     srcon     trcon'
        p+=1
        file_main[p]='       1.0       1.0         0       0.0       0.0       1.0       1.0       1.0'
        p+=1
        file_main[p]='*MAT_GENERAL_NONLINEAR_6DOF_DISCRETE_BEAM'
        p+=1
        file_main[p]='$#     mid        ro        kt        kr   unldopt    offset     dampf     iflag'
        p+=1
        file_main[p]='         17.80000E-6       0.0       0.0         0       0.0       0.0         0'
        p+=1
        file_main[p]='$#  lcidtr    lcidts    lcidtt    lcidrr    lcidrs    lcidrt'      
        p+=1
        file_main[p]='         1         1         1         0         0         0'
        p+=1
        file_main[p]='$# lcidtur   lcidtus   lcidtut   lcidrur   lcidrus   lcidrut'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# lcidtdr   lcidtds   lcidtdt   lcidrdr   lcidrds   lcidrdt'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# lcidter   lcidtes   lcidtet   lcidrer   lcidres   lcidret'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# utfailr   utfails   utfailt   wtfailr   wtfails   wtfailt'       
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='$# ucfailr   ucfails   ucfailt   wcfailr   wcfails   wcfailt'       
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='$#     iur       ius       iut       iwr       iws       iwt'   
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='*DEFINE_CURVE'
        p+=1
        file_main[p]='$#    lcid      sidr       sfa       sfo      offa      offo    dattyp     lcint'
        p+=1   
        file_main[p]='         1         0       1.0       1.0       0.0       0.0         0         0'
        p+=1
        file_main[p]='$#                a1                  o1'  
        p+=1
        file_main[p]='                 0.0                 0.0'
        p+=1
        file_main[p]='                 1.0                0.05'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='model.k to include here'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='CURVE.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='ele_beam.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='motion.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='nodeset_PIPER.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='noeuds_extr_beam.k'
        p+=1
        file_main[p]='*END'
    
        fonc_export_py(file_main,'main_pos.dyn',dir_e)
        cpt+=1