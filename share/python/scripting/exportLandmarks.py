# This script exports the landmarks position to a csv file
#
# arguments :
#   /path/to/landmarks.csv

import csv

import piper.hbm
import piper.app
import piper.anatomyDB

model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not ( 2 == len(sys.argv) ) :
    raise RuntimeError("Invalid arguments")
else :
    csvFilePath = sys.argv[1]
    metadata = piper.app.Context.instance().project().model().metadata()
    fem = piper.app.Context.instance().project().model().fem()
    with open(csvFilePath, 'wb') as csvfile:
        csvWriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        csvWriter.writerow(["name", "bone", "x", "y", "z"])
        for (name,landmark) in metadata.landmarks().iteritems() :
            bones = []
            if piper.anatomyDB.exists(name):
                bones = piper.anatomyDB.getPartOfSubClassList(name, "Bone")
            coord = landmark.position(fem).flatten()
            csvWriter.writerow([name, bones[0] if len(bones)>0 else "", coord[0], coord[1], coord[2]])
            
