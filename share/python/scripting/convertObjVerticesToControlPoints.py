﻿# This script reads vertices in an obj file and saves them as a set of control points in a format readable by the kriging module.
# This is intended to be used for transforming an HBM towards a given surface (so this script needs to be ran twice, one for the source surface and once for the target).
#
# arguments:
#  /path/to/mesh.obj /path/to/output_controlPoints.txt

# author: Tomas Janak date: 2017


import piper.app
    

if not len(sys.argv) == 3 :
    raise RuntimeError("Invalid arguments")
else :
    objFilePath = sys.argv[1]
    outputFilePath = sys.argv[2]
    output = open(outputFilePath, 'w')
    with open(objFilePath, 'r') as objFile:
        piper.app.logInfo("Reading obj file {0} ...".format(objFilePath))
        for line in objFile:
            if line.startswith('#'): continue
            values = line.split()
            if not values: continue
            if values[0] == 'v':
                coord = map(float, values[1:4])
                output.write("{0} {1} {2}\n".format(coord[0], coord[1], coord[2]))

    output.close()
        
