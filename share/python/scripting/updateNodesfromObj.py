# This script updates the HBM nodes with an obj file vertices coordinates
# It reads also a json file which stores correspondance between obj vertices and piper nodes id
# To be used together with the exportSelectedGeometryToObj.py script
#
# TODO: make it more robust to invalid data
#
# arguments:
#  /path/to/mesh.obj [history name]

# author: Thomas Lemaire date: 2017


import json

import piper.hbm
import piper.app
    
model = piper.app.Context.instance().project().model()

# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
elif not len(sys.argv) in {2,3} :
    raise RuntimeError("Invalid arguments")
else :
    objFilePath = sys.argv[1]
    objIdFilePath = objFilePath+".json"
    objIndexToNodeId = None
    if len(sys.argv) == 3:
        model.history().addNewHistory(sys.argv[2])    
    # loading the convertion dict from the json file
    with open(objIdFilePath, 'r') as objIdFile:
        piper.app.logInfo("Reading obj id file {0} ...".format(objIdFilePath))
        objIndexToNodeId = json.load(objIdFile)
    if objIndexToNodeId is None:
        piper.app.logError("Missing obj id file {0}".format(objIdFilePath))
    # reading obj vertices and updating the corresponding node
    fem = model.fem() # shortcut
    currentIndex = 1 # obj convention starts at 1
    with open(objFilePath, 'r') as objFile:
        piper.app.logInfo("Reading obj file {0} ...".format(objFilePath))
        for line in objFile:
            if line.startswith('#'): continue
            values = line.split()
            if not values: continue
            if values[0] == 'v':
                coord = map(float, values[1:4])
                if str(currentIndex) in objIndexToNodeId:
                    node = fem.getNode(objIndexToNodeId[str(currentIndex)])
                    node.setCoord(coord[0], coord[1], coord[2])
                else:
                    piper.app.logWarning("No id found for vertex {0}, skipped".format(currentIndex))
                currentIndex+=1
    piper.app.modelUpdated() # send the appropriate signal to refresh the application
        
