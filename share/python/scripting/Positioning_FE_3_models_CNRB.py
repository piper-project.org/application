# -*- coding: utf-8 -*-
#This script writes LS-Dyna inputs for positionning a FE model.
#This file (Author: UCBL-Ifsttar) is licensed under a Creative Commons 
#Attribution 4.0 International License. (https://creativecommons.org/licenses/by/4.0/)
#
#This work has received funding from the European Union Seventh Framework 
#Program ([FP7/2007-2013]) under grant agreement 605544 [PIPER project]).
#Contributors are Anicet Le Ruyet.
#
#See the PIPER documentation for more info.
#
#Arguments: directory of export (ex: Y://my_dir//);
#name of model (ex: import_model);
#name of model (ex: model_new0);
#name of model (ex: model_new1);
# CNRB: 0 (on) or 1 (off)

import piper.hbm
import piper.app
	
def fonc_concat_py(N,L):
#    N='%.5f' % N
    N="{0:.10g}".format(N)
    str1=' '*(L-len(N))+N
    return str1
    
    
def fonc_def_nodes_py(nid):
    NODES=dict()
    NODES[0]=['*NODE']
    NODES[1]=['$#   nid               x               y               z      tc      rc']
    j=2
    for i in range(len(nid)):
        NODES[j]=[fonc_concat_py(nid[i][0],8),fonc_concat_py(nid[i][1],16),fonc_concat_py(nid[i][2],16),fonc_concat_py(nid[i][3],16)];
        j+=1
    return NODES
    
def fonc_define_elem_beam_py(eid,pid,n1,n2,local):
    ELEM=dict()    
    ELEM[0]=['*ELEMENT_BEAM']
    ELEM[1]=['$#   eid     pid      n1      n2      n3     rt1     rr1     rt2     rr2   local']
    ELEM[2]=[fonc_concat_py(eid,8),fonc_concat_py(pid,8),fonc_concat_py(n1,8),fonc_concat_py(n2,8),fonc_concat_py(local,48)]
    return ELEM

def fonc_define_CURVE_py(lcid,ABSC,ORD,titre):
    CURVE=dict()
    q=0
    tit=['$ '+titre]
    CURVE[q]=['*DEFINE_CURVE']
    CURVE[q+1]=tit
    CURVE[q+2]=['$     lcid      sidr      scla      sclo      offa      offo']
    CURVE[q+3]=[fonc_concat_py(lcid,10)]
    CURVE[q+4]=['$           abscissa            ordinate']
    p=q+5
    for i in range(len(ABSC)):      
        CURVE[p]=[fonc_concat_py(ABSC[i],20),fonc_concat_py(ORD[i],20)]
        p+=1
    return CURVE
    
def fonc_define_boundary_pre_mot_py(nid,dof,vad,lcid):
    mot=dict()
    mot[0]=['*BOUNDARY_PRESCRIBED_MOTION_NODE']
    mot[1]=['$#     nid       dof       vad      lcid        sf       vid     death     birth']
    mot[2]=[fonc_concat_py(nid,10),fonc_concat_py(dof,10),fonc_concat_py(vad,10),fonc_concat_py(lcid,10)]
    return mot
    
def fonc_define_CNRB(pid,cid,nsid,pnode):
    CNRB=dict()
    CNRB[0]='*CONSTRAINED_NODAL_RIGID_BODY'
    CNRB[1]='$#     pid       cid      nsid     pnode      iprt    drflag    rrflag'
    CNRB[2]=[fonc_concat_py(pid,10),fonc_concat_py(cid,10),fonc_concat_py(nsid,10),fonc_concat_py(pnode,10)]
    return CNRB

def fonc_def_set_node_py(nodeset,num_node_set):
    nset=dict()
    nset[0]='*SET_NODE_LIST_TITLE'
    nset[1]='set node sid='+str(num_node_set)
    nset[2]='$#     sid       da1       da2       da3       da4'
    nset[3]=fonc_concat_py(num_node_set,10)+'     0.000     0.000     0.000     0.000'
    nset[4]='$#    nid1      nid2      nid3      nid4      nid5      nid6      nid7      nid8'
    p=5;q=0
    b=len(nodeset)%8
    for i in range(len(nodeset),len(nodeset)+b,1):
        nodeset.append('0')
    nodeset=[float(i) for i in nodeset]
    while q<len(nodeset)-8:
        nset[p]=[fonc_concat_py(nodeset[q],10),fonc_concat_py(nodeset[q+1],10),fonc_concat_py(nodeset[q+2],10),fonc_concat_py(nodeset[q+3],10),
        fonc_concat_py(nodeset[q+4],10),fonc_concat_py(nodeset[q+5],10),fonc_concat_py(nodeset[q+6],10),fonc_concat_py(nodeset[q+7],10)]
        p+=1;q+=8
    return nset
        
        
    return nset
            
def fonc_export_py(MAT,filename,folder):
    g=open(folder+filename,'w')
    try:
        for j in range(len(MAT)):
            tmp=map(str,MAT[j])
            line="".join(tmp)
            g.write("%s\n" % line)
        g.close()
    finally: 
        g.close()

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step
        
###############################################################################


def printUsage():
	piper.app.logWarning("wrong script parameters, expected: /path/to/export/directory [history name]")


model = piper.app.Context.instance().project().model()
import time
start_time = time.time()
# if the model is empty, we can do nothing
if model.empty():
    piper.app.logWarning("Model is empty")
#elif not ( len(sys.argv) in {2,3} ) :
#    printUsage()
else :
    # setup model in history
    model.history().setCurrentModel(sys.argv[2])
    metadata = piper.app.Context.instance().project().model().metadata()
    fem = piper.app.Context.instance().project().model().fem()
    piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();

    
    from collections import defaultdict
    p=0;q=0;no_i_t=defaultdict(list);x_i_t=defaultdict(list);y_i_t=defaultdict(list);z_i_t=defaultdict(list);
    for (name,entity) in metadata.entities().iteritems() :
        if piper.anatomyDB.isEntitySubClassOf(name, "Bone"):
            ids = entity.getEntityNodesIds(fem)
            for nodeId in ids:
                coord = fem.getNode(nodeId).get().flatten()
                no_i_t[q].append(piperIdToIdKey[nodeId][1].id)
                x_i_t[q].append(coord[0])
                y_i_t[q].append(coord[1])
                z_i_t[q].append(coord[2])
                p+=1
            q+=1
    
		
    model.history().setCurrentModel(sys.argv[3])
    metadata = piper.app.Context.instance().project().model().metadata()
    fem = piper.app.Context.instance().project().model().fem()
    piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();
    p=0;q=0;no_t_t=defaultdict(list);x_t_t=defaultdict(list);y_t_t=defaultdict(list);z_t_t=defaultdict(list);
    for (name,entity) in metadata.entities().iteritems() :
        if piper.anatomyDB.isEntitySubClassOf(name, "Bone"):
            ids = entity.getEntityNodesIds(fem)
            for nodeId in ids:
                coord = fem.getNode(nodeId).get().flatten()
                no_t_t[q].append(piperIdToIdKey[nodeId][1].id)
                x_t_t[q].append(coord[0])
                y_t_t[q].append(coord[1])
                z_t_t[q].append(coord[2])
                p+=1
            q+=1
            
    model.history().setCurrentModel(sys.argv[4])
    metadata = piper.app.Context.instance().project().model().metadata()
    fem = piper.app.Context.instance().project().model().fem()
    piperIdToIdKey = fem.computePiperIdToIdKeyMapNode();
    p=0;q=0;no_f_t=defaultdict(list);x_f_t=defaultdict(list);y_f_t=defaultdict(list);z_f_t=defaultdict(list);
    for (name,entity) in metadata.entities().iteritems() :
        if piper.anatomyDB.isEntitySubClassOf(name, "Bone"):
            ids = entity.getEntityNodesIds(fem)
            for nodeId in ids:
                coord = fem.getNode(nodeId).get().flatten()
                no_f_t[q].append(piperIdToIdKey[nodeId][1].id)
                x_f_t[q].append(coord[0])
                y_f_t[q].append(coord[1])
                z_f_t[q].append(coord[2])
                p+=1
            q+=1

    print("nodes collected")
    nodeset_cnrb=dict();node_cnrb=dict();CNRB=dict();beam_cnrb=dict();nod=list();node=dict();
    ELEM=dict();CURVE_X=dict();CURVE_Y=dict();CURVE_Z=dict();mot_x=dict();mot_y=dict();mot_z=dict();
    nid=dict();CURVE=dict();mot=dict();
    a=100001;b=200001;c=300001;p=40e6;k=0;m=0;
    
    CNRB_simu=int(sys.argv[5])
    if CNRB_simu==1:    
        for f in range(q):
            no_i=no_i_t[f];x_i=x_i_t[f];y_i=y_i_t[f];z_i=z_i_t[f];
            no_t=no_t_t[f];x_t=x_t_t[f];y_t=y_t_t[f];z_t=z_t_t[f];   
            
            nod=nod+no_i
            z=0;temp=[];
            for j in range(0,len(no_i),len(no_i)/10):
                temp.append(str(p+k))
                node[k]=[p+k,x_i[j],y_i[j],z_i[j]] # CNRB nodes
                nid[k]=[k+1,x_i[j],y_i[j],z_i[j]] # Noeuds tractés
                bcnrb=fonc_define_elem_beam_py(p+k,2,no_i[j],node[k][0],0)
                tp=len(beam_cnrb)
                for x in range(len(bcnrb)):
                    beam_cnrb[x+tp]=bcnrb[x]
                # poutres CNRB-entity   eid,pid,n1,n2,local
                ELEM_temp=fonc_define_elem_beam_py(k+1,1,k+1,p+k,2) # poutres tract-CNRB
                tp=len(ELEM)
                for x in range(len(ELEM_temp)):
                    ELEM[x+tp]=ELEM_temp[x]
                CURVE_X=fonc_define_CURVE_py(a,[0,200,1e3],[0,x_t[j]-x_i[j],x_t[j]-x_i[j]],'dispx')
                CURVE_Y=fonc_define_CURVE_py(b,[0,200,1e3],[0,y_t[j]-y_i[j],y_t[j]-y_i[j]],'dispy')
                CURVE_Z=fonc_define_CURVE_py(c,[0,200,1e3],[0,z_t[j]-z_i[j],z_t[j]-z_i[j]],'dispz')
                mot_x=fonc_define_boundary_pre_mot_py(k+1,1,2,a)
                mot_y=fonc_define_boundary_pre_mot_py(k+1,2,2,b)
                mot_z=fonc_define_boundary_pre_mot_py(k+1,3,2,c)
                tp=len(CURVE)
                for x in range(len(CURVE_X)):
                    CURVE[tp+x]=CURVE_X[x]
                tp=len(CURVE)
                for x in range(len(CURVE_Y)):
                    CURVE[tp+x]=CURVE_Y[x]
                tp=len(CURVE)
                for x in range(len(CURVE_Z)):
                    CURVE[tp+x]=CURVE_Z[x]
                tp=len(mot)
                for x in range(len(mot_x)):
                    mot[tp+x]=mot_x[x]
                tp=len(mot)
                for x in range(len(mot_y)):
                    mot[tp+x]=mot_y[x]
                tp=len(mot)
                for x in range(len(mot_z)):
                    mot[tp+x]=mot_z[x]
                a+=1;b+=1;c+=1;k+=1;z+=1;
            tp=len(nodeset_cnrb)
            nscnrb=fonc_def_set_node_py(temp,p+m)
            for x in range(len(nscnrb)):
                nodeset_cnrb[x+tp]=nscnrb[x]
            CNRB_temp=fonc_define_CNRB(p+m,0,p+m,0)
            tp=len(CNRB)
            for x in range(len(CNRB_temp)):
                CNRB[x+tp]=CNRB_temp[x]
            m+=1
        nodeset_PIPER=fonc_def_set_node_py(nod,1)
        node_cnrb=fonc_def_nodes_py(node)
        
    
        dir_e=sys.argv[1]
        fonc_export_py(CURVE,'CURVE.k',dir_e)
        fonc_export_py(mot,'motion.k',dir_e)
        fonc_export_py(nodeset_cnrb,'nodeset_cnrb.k',dir_e);
        fonc_export_py(node_cnrb,'node_cnrb.k',dir_e)
        fonc_export_py(CNRB,'cnrb.k',dir_e)
        fonc_export_py(beam_cnrb,'beam_cnrb.k',dir_e)
        noeuds=fonc_def_nodes_py(nid);
        fonc_export_py(noeuds,'noeuds_extr_beam.k',dir_e);
        fonc_export_py(ELEM,'ele_beam.k',dir_e);
        fonc_export_py(nodeset_PIPER,'nodeset_PIPER.k',dir_e);
        
        # Définition du main
        p=0
        file_main={}
        file_main[p]='*KEYWORD memory=200000000'
        p+=1
        file_main[p]='*CONTROL_TERMINATION'
        p+=1
        file_main[p]='$#  endtim    endcyc     dtmin    endeng    endmas'      
        p+=1
        file_main[p]='     250.0         0       0.0       0.01.000000E8'
        p+=1
        file_main[p]='*DATABASE_BINARY_D3PLOT'
        p+=1
        file_main[p]='$#      dt      lcdt      beam     npltc    psetid'     
        p+=1
        file_main[p]='       5.0         0         0         0         0'
        p+=1
        file_main[p]='$#   ioopt'     
        p+=1
        file_main[p]='         0'
        p+=1
        file_main[p]='*PART'
        p+=1
        file_main[p]='$#                                                                         title'
        p+=1
        file_main[p]='BEAM'
        p+=1
        file_main[p]='$#     pid     secid       mid     eosid      hgid      grav    adpopt      tmid'
        p+=1
        file_main[p]='         1         1         1         0         0         0         0         0'
        p+=1
        file_main[p]='*PART'
        p+=1
        file_main[p]='$#                                                                         title'
        p+=1
        file_main[p]='BEAM'
        p+=1
        file_main[p]='$#     pid     secid       mid     eosid      hgid      grav    adpopt      tmid'
        p+=1
        file_main[p]='         2         1         2         0         0         0         0         0'
        p+=1
        file_main[p]='*SECTION_BEAM_TITLE'
        p+=1
        file_main[p]='beam'
        p+=1
        file_main[p]='$#   secid    elform      shrf   qr/irid       cst     scoor       nsm'   
        p+=1
        file_main[p]='         1         6       1.0         2         0       0.0       0.0'
        p+=1
        file_main[p]='$#     vol      iner       cid        ca    offset     rrcon     srcon     trcon'
        p+=1
        file_main[p]='      1e-4       1.0         0       0.0       0.0       1.0       1.0       1.0'
        p+=1
        file_main[p]='*MAT_GENERAL_NONLINEAR_1DOF_DISCRETE_BEAM'
        p+=1
        file_main[p]='$#     mid        ro         k   unldopt    offset     dampf'
        p+=1
        file_main[p]='         17.80000E-6       0.0         0       0.0       2.0'
        p+=1
        file_main[p]='$#   lcidt    lcidtu    lcidtd    lcidte'      
        p+=1
        file_main[p]='         1         0         0         0'
        p+=1
        file_main[p]='$#  utfail    ucfail        iu'       
        p+=1
        file_main[p]='       0.0       0.0       0.0'
        p+=1
        file_main[p]='*MAT_GENERAL_NONLINEAR_1DOF_DISCRETE_BEAM'
        p+=1
        file_main[p]='$#     mid        ro         k   unldopt    offset     dampf'
        p+=1
        file_main[p]='         27.80000E-6       0.0         0       0.0       2.0'
        p+=1
        file_main[p]='$#   lcidt    lcidtu    lcidtd    lcidte'      
        p+=1
        file_main[p]='         2         0         0         0'
        p+=1
        file_main[p]='$#  utfail    ucfail        iu'       
        p+=1
        file_main[p]='       0.0       0.0       0.0'
        p+=1
        file_main[p]='*DEFINE_CURVE'
        p+=1
        file_main[p]='$#    lcid      sidr       sfa       sfo      offa      offo    dattyp     lcint'
        p+=1
        file_main[p]='         2         0       1.0       1.0       0.0       0.0         0         0'
        p+=1
        file_main[p]='$#                a1                  o1'  
        p+=1
        file_main[p]='                 0.0                 0.0'
        p+=1
        file_main[p]='                 1.0                1e-3'
        p+=1
        file_main[p]='*DEFINE_CURVE'
        p+=1
        file_main[p]='$#    lcid      sidr       sfa       sfo      offa      offo    dattyp     lcint'
        p+=1
        file_main[p]='         1         0       1.0       1.0       0.0       0.0         0         0'
        p+=1
        file_main[p]='$#                a1                  o1'  
        p+=1
        file_main[p]='                 0.0                 0.0'
        p+=1
        file_main[p]='                 1.0                0.05'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='include main.k here'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='CURVE.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='ele_beam.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='motion.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='nodeset_PIPER.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='noeuds_extr_beam.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='nodeset_cnrb.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='node_cnrb.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='cnrb.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='beam_cnrb.k'
        p+=1
        file_main[p]='*END'
        fonc_export_py(file_main,'main_pos.dyn',dir_e)
        
    else:
        nodeset_cnrb=dict();node_cnrb=dict();CNRB=dict();beam_cnrb=dict();nod=list();node=dict();
        ELEM=dict();CURVE_X=dict();CURVE_Y=dict();CURVE_Z=dict();mot_x=dict();mot_y=dict();mot_z=dict();
        nid=dict();CURVE=dict();mot=dict();
        a=100001;b=200001;c=300001;k=0;m=0;
        for f in range(q):
            no_i=no_i_t[f];x_i=x_i_t[f];y_i=y_i_t[f];z_i=z_i_t[f];
            no_t=no_t_t[f];x_t=x_t_t[f];y_t=y_t_t[f];z_t=z_t_t[f];
            no_f=no_f_t[f];x_f=x_f_t[f];y_f=y_f_t[f];z_f=z_f_t[f];   
            
            nod=nod+no_i
            z=0;temp=[];
            for j in range(0,len(no_i),len(no_i)/10):
                nid[k]=[k+1,x_i[j],y_i[j],z_i[j]] # Noeuds tractés
                ELEM_temp=fonc_define_elem_beam_py(k+1,1,no_i[j],nid[k][0],2) # poutres tract-CNRB
                tp=len(ELEM)
                for x in range(len(ELEM_temp)):
                    ELEM[x+tp]=ELEM_temp[x]
                CURVE_X=fonc_define_CURVE_py(a,[0,100,200,1e3],[0,x_t[j]-x_i[j],x_f[j]-x_i[j],x_f[j]-x_i[j]],'dispx')
                CURVE_Y=fonc_define_CURVE_py(b,[0,100,200,1e3],[0,y_t[j]-y_i[j],y_f[j]-y_i[j],y_f[j]-y_i[j]],'dispy')
                CURVE_Z=fonc_define_CURVE_py(c,[0,100,200,1e3],[0,z_t[j]-z_i[j],z_f[j]-z_i[j],z_f[j]-z_i[j]],'dispz')
                mot_x=fonc_define_boundary_pre_mot_py(k+1,1,2,a)
                mot_y=fonc_define_boundary_pre_mot_py(k+1,2,2,b)
                mot_z=fonc_define_boundary_pre_mot_py(k+1,3,2,c)
                tp=len(CURVE)
                for x in range(len(CURVE_X)):
                    CURVE[tp+x]=CURVE_X[x]
                tp=len(CURVE)
                for x in range(len(CURVE_Y)):
                    CURVE[tp+x]=CURVE_Y[x]
                tp=len(CURVE)
                for x in range(len(CURVE_Z)):
                    CURVE[tp+x]=CURVE_Z[x]
                tp=len(mot)
                for x in range(len(mot_x)):
                    mot[tp+x]=mot_x[x]
                tp=len(mot)
                for x in range(len(mot_y)):
                    mot[tp+x]=mot_y[x]
                tp=len(mot)
                for x in range(len(mot_z)):
                    mot[tp+x]=mot_z[x]
                a+=1;b+=1;c+=1;k+=1;z+=1;
            m+=1
        nodeset_PIPER=fonc_def_set_node_py(nod,1)
        
    
        dir_e=sys.argv[1]
        fonc_export_py(CURVE,'CURVE.k',dir_e)
        fonc_export_py(mot,'motion.k',dir_e)
        noeuds=fonc_def_nodes_py(nid);
        fonc_export_py(noeuds,'noeuds_extr_beam.k',dir_e);
        fonc_export_py(ELEM,'ele_beam.k',dir_e);
        fonc_export_py(nodeset_PIPER,'nodeset_PIPER.k',dir_e);
        
        # Définition du main
        p=0
        file_main={}
        file_main[p]='*KEYWORD memory=200000000'
        p+=1
        file_main[p]='*CONTROL_TERMINATION'
        p+=1        
        file_main[p]='$#  endtim    endcyc     dtmin    endeng    endmas'      
        p+=1
        file_main[p]='     250.0         0       0.0       0.01.000000E8'
        p+=1
        file_main[p]='*DATABASE_BINARY_D3PLOT'
        p+=1
        file_main[p]='$#      dt      lcdt      beam     npltc    psetid'     
        p+=1
        file_main[p]='       1.0         0         0         0         0'
        p+=1
        file_main[p]='$#   ioopt'     
        p+=1
        file_main[p]='         0'
        p+=1
        file_main[p]='*PART'
        p+=1
        file_main[p]='$#                                                                         title'
        p+=1
        file_main[p]='BEAM'
        p+=1
        file_main[p]='$#     pid     secid       mid     eosid      hgid      grav    adpopt      tmid'
        p+=1
        file_main[p]='         1         1         1         0         0         0         0         0'
        p+=1
        file_main[p]='*SECTION_BEAM_TITLE'
        p+=1
        file_main[p]='beam'
        p+=1
        file_main[p]='$#   secid    elform      shrf   qr/irid       cst     scoor       nsm'   
        p+=1
        file_main[p]='         1         6       1.0         2         0       0.0       0.0'
        p+=1
        file_main[p]='$#     vol      iner       cid        ca    offset     rrcon     srcon     trcon'
        p+=1
        file_main[p]='       1.0       1.0         0       0.0       0.0       1.0       1.0       1.0'
        p+=1
        file_main[p]='*MAT_GENERAL_NONLINEAR_6DOF_DISCRETE_BEAM'
        p+=1
        file_main[p]='$#     mid        ro        kt        kr   unldopt    offset     dampf     iflag'
        p+=1
        file_main[p]='         17.80000E-6       0.0       0.0         0       0.0       0.0         0'
        p+=1
        file_main[p]='$#  lcidtr    lcidts    lcidtt    lcidrr    lcidrs    lcidrt'      
        p+=1
        file_main[p]='         1         1         1         0         0         0'
        p+=1
        file_main[p]='$# lcidtur   lcidtus   lcidtut   lcidrur   lcidrus   lcidrut'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# lcidtdr   lcidtds   lcidtdt   lcidrdr   lcidrds   lcidrdt'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# lcidter   lcidtes   lcidtet   lcidrer   lcidres   lcidret'       
        p+=1
        file_main[p]='         0         0         0         0         0         0'
        p+=1
        file_main[p]='$# utfailr   utfails   utfailt   wtfailr   wtfails   wtfailt'       
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='$# ucfailr   ucfails   ucfailt   wcfailr   wcfails   wcfailt'       
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='$#     iur       ius       iut       iwr       iws       iwt'   
        p+=1
        file_main[p]='       0.0       0.0       0.0       0.0       0.0       0.0'
        p+=1
        file_main[p]='*DEFINE_CURVE'
        p+=1
        file_main[p]='$#    lcid      sidr       sfa       sfo      offa      offo    dattyp     lcint'
        p+=1   
        file_main[p]='         1         0       1.0       1.0       0.0       0.0         0         0'
        p+=1
        file_main[p]='$#                a1                  o1'  
        p+=1
        file_main[p]='                 0.0                 0.0'
        p+=1
        file_main[p]='                 1.0                0.05'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='include main.k here'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='CURVE.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='ele_beam.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='motion.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='nodeset_PIPER.k'
        p+=1
        file_main[p]='*INCLUDE'
        p+=1
        file_main[p]='noeuds_extr_beam.k'
        p+=1
        file_main[p]='*END'

        fonc_export_py(file_main,'main_pos.dyn',dir_e)