import sys, traceback, piper.app, string

piper.app.logDebug(str(len(sys.argv)-1) + " arguments recieved by '" + str(sys.argv[0]) + "': " + str(sys.argv))

if len(sys.argv) < 2:
    raise RuntimeError('No script passed.')

source = str(sys.argv[1]) # selecting first argument (= final script to run)
sys.argv = sys.argv[1:] # selecting extra arguments (for the final script)

try:
    execfile(source)
except:
    error_string = traceback.format_exc()
    error_string = string.replace(error_string, ' ', '&nbsp;')
    error_string = string.replace(error_string, '\n', '<br>')
    piper.app.logError("There was an error in the exectution of the script:<br>" + error_string)
    raise RuntimeError("Script '"+source+"' did not executed correctly.")
