Change Log     {#pageChangelog}
==========

v1.1.0 - 08/2020
----------------
New or modified features:
- [kriging] The Kriging module now enables to enter global nugget for individual source control point sets
- [kriging] Added a Surface Registration tool (see Kriging module documentation for details)
- [kriging] The Module Parameters for Kriging now have two new options - "interpolation by displacement" and "use drift". See the Kriging module documentation for details.
- [kriging] Removed the "max nodes per box" Kriging parameter from Module Parameters - large amount of nodes per box is not an issue, only large amount of control points is
- [kriging] Four geodesic distances can now be chosen through the Module Parameters as the topology-aware distance functions for Kriging (until now, Green's distance was always used).
- [kriging] Added homogenous decimation of control points; points can now be decimated not only on intermediate targets, but also on points loaded in the Kriging module
- [kriging] The decimation based on relative displacement is now off by default
- [kriging] Added option to export control points to file (useful for saving transformed CPs)
- [kriging] The "Kriging with intermediate targets" option is shown/hidden by a checkbox in the "Perform Deformation" window. By default, it is shown in the Scaling Constraints module and hidden in the Kriging module.
- [smoothing] The "Use progressive kriging" option of "Kriging in a box" was removed as it was not functioning correctly and could be detrimental to the quality of the smoothing
- [element quality] Number of elements is now reported in the form of a table for individual element types (tria, quad, tetra, hexa)
- [element quality] Added "Volume" as an element quality metric (for tetrahedral and hexahedral elements)
- [selection] Added selection by plane
- [selection] Added export of selected elements
- [selection] Added deselecting bone elements
- [3D viewer] Added buttons for setting camera in negative X/Y/Z direction
- [3D viewer] The "see axes" button will now place the axes in the corner of the model rather than at (0, 0, 0)
- [parser] Added basic format rules for Radioss (imports only geometry, some types of elements might not be imported)

Bug-fixes:
- [kriging] Domain decomposition is now applicable to control points defined through Kriging module as well
- [kriging] Kriging with intermediate targets is now possible when only one of the targets (skin or bones) is defined
- [pre-posi] Fix piper crashing when a positioning python script is not executed succesfully. Now only an error message is printed in the log.
- [element quality] Fixed a bug where boundary values of each range were not counted in either of the ranges. Now the lower bound is part of the range and the upper bound is not, except for the highest range, which includes the upper bound as well -> values above or below the ranges are always strictly above or below.
- [parser] Fixed several corner cases that made very large numbers to be output incorrectly in fixed length formats (overstepping the fixed limit) or imprecisely (truncating everything after decimal point)
- [3D viewer] Fixed Z and Y clipping planes being switched

v1.0.3 - 03/2020
----------------
New or modified features:
- [kriging] The kriging deformation API is now accesible by python scripts (including batch mode)
- [app] Command line arguments are now passed to launching the piper.exe when starting PIPER from Windows command line using the RunPiper.bat. For example, ">RunPiper.bat --batch myScript.py" will start PIPER in batch mode
- [hbm] Support for keywords defined by regular expressions, see the "Multi Finite Element Format Parser" page of the documentation for details
- [hbm] Added option to specify additional include files the .pmr files
- [hbm] More precise error reporting when importing HBM files

Bug-fixes:
- [child scaling] Fixed a bug that caused the first point in the sequence (left side of vertebra near shoulders) to have target x and y coordinates incorrectly equal to 0
- [hbm] Comments in FE files were disappearing after export in some cases, fixed
- [kriging] Fixed source control points not disappearing from display after being removed (introduced in 1.0.2)
- [kriging] When "fix bones" option is turned on when doing kriging with intermediate targets, "use bones as intermediate targets" must be turned on as well. GUI will now ensure this
- [scripting] The Positioning_FE_auto.py script (for exporting positioning data as LS-Dyna simulation) now handles positions in frame controllers and uses floats for interpolation instead of ints


v1.0.2 - 12/2017
----------------
New or modified features:
- [kriging] Added option to toggle visibility of control points through their context menu in the "Target/Source points" windows
- [kriging] Control point parameters (nugget, skin/bone association) can now be defined in the metadata either through a per-point list, or through global values
- [kriging] The Kriging module now enables to enter global skin/bone association parameters for individual source control point sets
- [kriging] Dialogs for loading control points now remember last used folder paths
- [app] Enhanced the RunPiper.bat script for windows to allow starting Piper from a remote network drive

Bug-fixes:
- [pre-posi] The target values of controllers of all types are no longer limited to <-1000;1000>, but can have arbitrary values
- [app] Piper should no longer crash when UI is scaled through operating system - some parts of the GUI can still be scaled improperly though
- [app] Temporarily disabled storing the last used folder for various dialogues if the last used folder was on a network (UNC) path as that currently causes crashes due to a bug in an external library used by PIPER.
- [app] Removed size limitations for xml validation -> large Piper Project files will now correctly validate
- [kriging] Skin/bone associations should correctly from the .pmr files and correctly applied in the Kriging module
- [kriging] Control points defined in metadata with the "target" role will now correctly show up in the "Target Control Points" menu in Kriging module
- [contour] Enhanced hip and legs repositioning


v1.0.1 - 07/2017
----------------
- [framework] The application will now automatically go to check module upon opening a project (as it does upon importing a new model)
- [scaling constraints] Fix crashes when a new HBM is loaded that is incompatible with the currently loaded scalable model
- [contour] Fixes a bug causing contourCL files to be unrecognized by the software
- [anthropo] CCTAnthro-landmark based prediction temporarily removed as it is not yet fully functional
- [anthropo] Fixes a bug that caused the "generate regressions" button to do nothing in some cases

v1.0.0 - 05/2017
----------------

- [pre-posi] some gui enhancement (landmark visibility, reload button always active)
- [pre-posi] fix controllers were not destroyed on reload
- [fine-posi] fix bad affine density value used
- [shape] fix skin young modules value was not set properly from the gui
- [kriging] support for domain decomposition of the model through named metadata
- [smoothing] added a new transformation smoothing method - local displacement average 

v0.99.0 - 04/2017
-----------------

- [shape] new module to shape HBM skin (beta quality)
- [pre-posi] spine predictor have two lateral flexion angles
- [pre-posi] voxel size can be scaled with HBM height
- [pre-posi] custom affine frames can be defined in the metadata
- [pre-posi] add simulation of articular capsules, ligaments, cartilage, when appropriate entities are defined
- [pre-posi] add a distance constraint between tibia and patella
- [pre-posi] add relative motion in the frame controller
- [pre-posi] bone and capsule collisions can be enabled/disabled directly in the module, disabling it speeds up the simulation
- [application] ability to interrupt lengthy process (like Position modules loading)
- [anatomyDB] add some articular capsules, ligaments and meniscus
- [pre-posi] FIX wrong bone collision in rare cases
- [pre-posi] FIX update of some nodes in bone entities
- [application] Environment can now be visualized and transformed (scale, translation and rotation)
- [framework] metadata can be exported and imported.
- [framework] only pmr file is now required to imported HBM from FE file or graphic mesh format (obj).
- [bodysection] refactoring module: anthropometric model can be edited. Handles of type of dimension with relative or absolute taget value
- [kriging] skin transformation now possible using surface distance instead of euclidean
- [kriging] kriging with intermediate bone and skin target

v0.8.0 - 01/2017
----------------
- [pre-posi] improve stability of the spine controller and the spine predictor
- [pre-posi] add control on the user target stiffness
- [pre-posi] add a clipping plane to inspect the 3D model
- [pre-posi] fix flesh attachment bug
- [framework] add generic tooltip
- [framework] the application remembers your last used folders
- [framework] python scripts can be run at any time in the workflow

v0.7.0 - 11/2016
----------------
- [application] add landmarks and nodes exporter to simple ascii files
- [pre-posi] lazy simulation loading, do not reload simulation if it is already ready
- [pre-posi] updated spine predictor, with or without pelvis orientation
- [pre-posi] spine target is reached progressively to improve simulation stability
- [pre-posi] let the user control more simulation parameters (timestep, number of vertices for bone collision)
- [smoothing] allowed selecting multiple entities as targets for optimization by all smoothing techniques
- [3D viewer] Element blanking to allow exploration of inside of the model
- [3D viewer] Display settings: setting camera focal point, resetting camera, switching projection type
- [3D viewer] Highlighting element edges and normals
- [3D viewer] Camera and display settings are now persistent across all modules
- [3D viewer] Improved stability (due to safer handling of interaction between "computational" and rendering threads)

v0.6 - 10/2016 - alpha version
------------------------------
- [pre-posi] new limited mouse interaction with the model
- [pre-posi] new spine predictor
- [pre-posi] new spine controller
- [pre-posi] new anatomical landmark controllers
- [fine-posi] slightly better affine frame spreading in the model
- [pre-posi] soft and hard joint simulation
- [pre-posi] loading of targets
- [pre-posi] bone collision
- [pre-posi] automatic stop of the positioning process
- [app] model-modifying actions now create a history of node coordinates to which the user can roll-back
- [bodysection] new module for scaling the model based on body section dimension targets
- [anthropo-perso] new module for generating anthropometric measurement targets based on statistical regression analysis of a specified database
- [smoothing] crease detection on surfaces
- [smoothing] transformation smoothing using kriging in a box
- [smoothing] surface smoothing using windowed sinc FIR filter
- [smoothing] mechanism for loading baseline models either from file or from history
- [3D viewer] VTK-based 3D viewer - model exploration, visualization of metadata
- [3D viewer] Picking tools: rubber band, box and single-object picking
- [3D viewer] Coloring elements based on quality
- [app] uniform module layout, use of tool windows to gather specific parts of the GUI

v0.5 - 12/2015
--------------
- [smoothing] new smoothing module based on the Mesquite library
- [fine-posi] new physics based module for positioned module deformation, it is based on the [pre-posi]
- [pre-posi] control over frame orientation, ability to remove controllers
- [pre-posi] save some targets
- [laplacian-smoothing] this module is removed
- [app] modules can have parameters
- [framework] target data structure as an input/output for modules, can be saved/loaded to/from an xml file
- [framework] environment can be loaded from FE code files
- [framework] piper application save/load project files which contain the FEModel, targets and environment.
- [iitd] new module to define contours metadata and position hip and knee, personalize the hbm.
- [mesh-optim] new module to analyze mesh quality and improve mesh quality for 3D elements meshes.

v0.4 - 09/2015
--------------

- [pre-posi] positioning of the frames, more display options
- [pre-posi] support contact
- [libhbm] support contact
- [libhbm] split parser files in a format specific file and a model specific file

v0.3 - 07/2015
--------------

- same functionalities as 0.2 but using double precision for floating point numbers: it consumes more memory but sofa-positioning is more stable.

v0.2 - 07/2015
--------------

- package for both windows and linux
- new module physics-positioning

v0.1 - 05/2015
--------------

- initial version of the PIPER application for windows
- it includes the following modules: check, 3d-display, krigging, sofa-smoothing, scaling parameter
