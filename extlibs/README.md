This folder contains links (git sub-modules) to libraries source code repositories required to compile PIPER from scratch.

git sub-modules documentation can be found at https://git-scm.com/book/en/v2/Git-Tools-Submodules

SOFA
----
Generic instructions on compiling SOFA are available at
http://www.sofa-framework.org/

The SOFA framework (in sofa/) and some plugins (in sofa_plugins/) are required. To compile the framework along with these plugins, the following file has to be put at in the sofa/ directory: 

custom.cmake
  add_subdirectory("../sofa-plugins/BranchingImage" "${CMAKE_CURRENT_BINARY_DIR}/BranchingImage")
  add_subdirectory("../sofa-plugins/sofaqtquick" "${CMAKE_CURRENT_BINARY_DIR}/SofaQtQuick")
  add_subdirectory("../sofa-plugins/Anatomy" "${CMAKE_CURRENT_BINARY_DIR}/Anatomy")
  add_subdirectory("../sofa-plugins/LinearSubspace" "${CMAKE_CURRENT_BINARY_DIR}/LinearSubspace")
