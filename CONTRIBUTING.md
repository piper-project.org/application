# Contributing for the first time

The PIPER application is maintained by members of the piper-project.org Open Source project. If you would like to contribute, first you need to create an account at gitlab.inria.fr, then send us an e-mail to contact@piper-project.org containing your username at gitlab.inria.fr and tell us what are planning to contribute (it would also be nice if you can tell us a bit about yourself, what are you using PIPER for etc.). We will then grant you the "developer" privileges. As a "developer", you will be able to create new branches containing your contributions. However, you will not be allowed to make changes to the master branch, that is the branch that is considered the "official release" of PIPER. In order to do that, you will need to create a merge request that will be reviewed by members of the piper-project.org before it will be included in the master branch.

A step-by-step procedure for getting your contributions to the master brach follows. Especially if you are not very familiar with git, we recommend using a graphical interface such as Git Extensions (https://sourceforge.net/projects/gitextensions/) instead of command line.

## Clone the repository:

Using SSH:

    git clone git@gitlab.inria.fr:piper/application.git

Using https:

	git clone https://gitlab.inria.fr/piper/application.git


# Submitting contributions

## Create a new branch and make your changes:

The first step is to create a branch from the HEAD of the master branch.
If your intention is to fix an issue, please name the branch "issueXX".

    git branch your_branch_name
    git checkout your_branch_name

Implement your modifications in this new branch, and then push this branch to the remote (online) repository. If not already existing, write a test that hightlights the fix or improvements.

    git push -f origin your_branch_name
  
(without -f option if the branch already exists online, and you just want to update it).

Especially in case your contribution requires a lot of time and effort, do not be affraid to push your branch to the remote repository even when you are not done yet. This will serve as a backup for you and also others can track your progress, which can avoid duplicated work.

## Make a merge request with master branch

Once you have implemented everything you wanted and you are confident it works, you are ready to submit a merge request with the master branch. 

First check if the master branch haven't changed much since you started your work. If it did, you should first either merge master into your branch or (preferably) rebase your branch on top of master in your local repository. Resolve all conflicts that occured and test that everything still works. 

    git checkout your_branch_name
    git rebase master


**Check that all tests are succesfully passed before submitting a merge request**

**If you implemented new features or changed existing ones, make sure to also update the documentation**

Go to https://gitlab.inria.fr/piper/application/merge_requests and submit a new merge request. Make sure you have actually pushed your changes (not only "commited") to the remote repository before submitting. Choose "your" branch as the source branch, "piper/application master" as the target branch. You can find more details about merge requests here: https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html. Please check the "Remove source branch when merge request is accepted" checkbox before submitting the request in order to keep the repository cleaner.

The merge request title should describe the change you want to make. If your intention is to fix an issue, please add “Fix #xx”. In case of a large contribution, such as some new feature, add a short description please.

