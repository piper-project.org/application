import os
import os.path
import shutil
import piper.anatomyDB

piper.anatomyDB.init(".")

genDocDir = "../generatedDoc"
shutil.rmtree(genDocDir, ignore_errors=True)
os.mkdir(genDocDir)

# appendix_anatomyDB_entity.md
boneList = [entity for entity in piper.anatomyDB.getSubClassOfList("Bone")]
boneList.sort()
capsuleList = [entity for entity in piper.anatomyDB.getSubClassOfList("Articular_capsule")]
capsuleList.sort()
ligamentList = [entity for entity in piper.anatomyDB.getSubClassOfList("Ligament")]
ligamentList.sort()
cartilageList = [entity for entity in piper.anatomyDB.getSubClassOfList("Cartilage")]
cartilageList.sort()

skinList = [entity for entity in piper.anatomyDB.getSubClassOfList("Skin")]
skinList.sort()

entityList = skinList + boneList + capsuleList + ligamentList + cartilageList

with open(os.path.join(genDocDir,"appendix_anatomyDB_entity.md"), 'w') as file:
    file.write("Appendix: List of entities     {#pageAppendixEntity}\n")
    file.write("==========\n")
    
    file.write("| Entity name | Synonyms | Classes | PartOf |  Region |\n")
    file.write("|-------------|----------|---------|--------|---------|\n")
    for entity in entityList:
        entityClasses = [subClass for subClass in piper.anatomyDB.getParentClassList(entity)]
        if "Joint" in entityClasses: # for now, skip joints
            continue
        entityClasses.remove("Anatomical_entity") # do not print "Anatomical_entity" for each entity
        file.write("| {0} | {1} | {2} | {3} | {4} |\n".format(
            entity, 
            ", ".join(piper.anatomyDB.getSynonymList(entity, True)),
            ", ".join(entityClasses),
            ", ".join( piper.anatomyDB.getPartOfSubClassList(entity,"Bone")
                     + piper.anatomyDB.getPartOfSubClassList(entity,"Skin")),
            ", ".join( piper.anatomyDB.getPartOfSubClassList(entity,"Region")) ))

# appendix_anatomyDB_landmark.md
landmarkList = [entity for entity in piper.anatomyDB.getLandmarkList()]
landmarkList.sort()

with open(os.path.join(genDocDir,"appendix_anatomyDB_landmark.md"), 'w') as file:
    file.write("Appendix: List of landmarks     {#pageAppendixLandmark}\n")
    file.write("==========\n")
    
    file.write("| Landmark name | Synonyms |Description | PartOf | Bibliography |\n")
    file.write("|---------------|----------|------------|--------|--------------|\n")
    for landmark in landmarkList:
        file.write("| {0} | {1} | {2} | {3} | {4} |\n".format(
            landmark,
            ", ".join(piper.anatomyDB.getSynonymList(landmark, True)),
            piper.anatomyDB.getEntityDescription(landmark),
            ", ".join(piper.anatomyDB.getPartOfSubClassList(landmark,"Bone")),
            ", ".join(piper.anatomyDB.getEntityBibliographyList(landmark)) ))

# appendix_anatomyDB_frame.md
frameList = [entity for entity in piper.anatomyDB.getSubClassOfList("Frame")]
frameList.sort()

with open(os.path.join(genDocDir,"appendix_anatomyDB_frame.md"), 'w') as file:
    file.write("Appendix: List of frames     {#pageAppendixFrame}\n")
    file.write("==========\n")
    
    file.write("| Frame name | Synonyms |Description | PartOf | Landmarks | Bibliography |\n")
    file.write("|------------|----------|------------|--------|-----------|--------------|\n")
    ff = piper.anatomyDB.FrameFactory.instance()
    for frame in frameList:
        landmarkListStr = ""
        if ff.isFrameRegistered(frame):
            landmarkList = ff.getFrameRequiredLandmarks(frame)
            for l in landmarkList:
                if len(landmarkListStr)>0:
                    landmarkListStr+= " **or** "
                landmarkListStr += "["+", ".join(l)+"]"
        else:
            landmarkListStr = "N/A"
        file.write("| {0} | {1} | {2} | {3} | {4} | {5} |\n".format(
            frame,
            ", ".join(piper.anatomyDB.getSynonymList(frame, True)),
            piper.anatomyDB.getEntityDescription(frame),
            ", ".join(piper.anatomyDB.getPartOfSubClassList(frame,"Bone")),
            landmarkListStr,
            ", ".join(piper.anatomyDB.getEntityBibliographyList(frame)) ))
        
# appendix_anatomyDB_joint.md
jointList = [joint for joint in piper.anatomyDB.getSubClassOfFromBibliographyList("Joint", "FMA")]
jointList.sort()
with open(os.path.join(genDocDir,"appendix_anatomyDB_joint.md"), 'w') as file:
    file.write("Appendix: List of joints     {#pageAppendixJoint}\n")
    file.write("==========\n")
    
    file.write("| Joint name | Description | Frames | Bibliography |\n")
    file.write("|------------|-------------|--------|--------------|\n")
    for joint in jointList:
        frames = piper.anatomyDB.getSubPartOfList(joint,"Frame")
        # in that case, the joint cannot be used as an anatomical joint, skip it for the doc
        if 0 == len(frames):
            continue
        file.write("| {0} | {1} | {2} | {3} |\n".format(
            joint,
            piper.anatomyDB.getEntityDescription(joint),
            ", ".join(frames),
            ", ".join(piper.anatomyDB.getEntityBibliographyList(joint)) ))



