@echo off
pushd %~dp0.
set PIPERDIR=%cd%
set PATH=%PIPERDIR%\bin\;%PIPERDIR%\Python27\;%PIPERDIR%\Octave\bin\;%PATH%
set PYTHONPATH=%PIPERDIR%\Python27\
set PYTHONHOME=%PIPERDIR%\Python27\
set PYTHONUSERBASE=%PIPERDIR%\Python27\
set OCTAVEPIPERPATH=%PIPERDIR%\Octave
"%OCTAVEPIPERPATH%\octavePathSetter.exe" "%OCTAVEPIPERPATH%\share\octave\template_octave_packages" "%OCTAVEPIPERPATH%\share\octave\octave_packages" %OCTAVEPIPERPATH%
"%PIPERDIR%\bin\piper.exe" %* -platform windows:dpiawareness=1
popd