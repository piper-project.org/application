// quick and dirty script to set paths to Octave packages to the specified path and also set the number of characters that are in the path - required by octave to run
//

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ios>

using namespace std;

int main(int argc, char *argv[])
{
    ifstream input;
    stringstream output;
    input.open(argv[1]); // the path to the template octave file
    string replace = argv[3];
    if (input.is_open())
    {
        string line;        
        while (getline(input, line))
        {
            if (line.size() > 9 && line.compare(0, 9, "# length:") == 0) // if the line is legth, the next line can be the path -> load it
            {
                string nextLine;
                if (getline(input, nextLine))
                {
                    if (nextLine.compare(0, 13, "[OCTAVE_ROOT]") == 0)
                    {
                        nextLine.replace(0, 13, replace);
                        line.replace(9, line.size() - 9, " " + to_string(nextLine.size()));
                    }
                    output << line << endl << nextLine << endl;
                }
                else // should never happen...
                    output << line << endl;
            }
            else
                output << line << endl;
        }

        input.close();
        // now rewrite the file by the changed lines
        ofstream outFile;
        outFile.open(argv[2]); // path to the output file
        if (outFile.is_open())
        {
            outFile << output.str();
            outFile.close();
        }
        else
        {
            std::cout << "Octave package list file - " << argv[2] << " - could not be open for writing. Octave-dependent modules will likely not work!";
        }

    }
    else
    {
        std::cout << "Octave package list file - " << argv[1] << " - could not be open for reading. Octave-dependent modules will likely not work!";
    }

	return 0;
}

