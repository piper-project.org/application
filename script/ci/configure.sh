#!/bin/bash

# - CI_BUILD_TYPE             Debug|Release

# Exit on error
set -o errexit

## Checks

usage() {
    echo "Usage: configure.sh <build-dir> <src-dir>"
}

if [[ "$#" = 2 ]]; then
    build_dir="$1"
    src_dir="$(cd "$2" && pwd)"
else
    usage; exit 1
fi

if [[ ! -d "$build_dir" ]]; then
    mkdir -p "$build_dir"
fi
build_dir="$(cd "$build_dir" && pwd)"

## Defaults

if [ -z "$CI_BUILD_TYPE" ]; then CI_BUILD_TYPE="Release"; fi

## CMake options

cmake_options='-DCMAKE_COLOR_MAKEFILE=OFF \
-DCMAKE_BUILD_TYPE=$CI_BUILD_TYPE \
-DQt5_DIR=/local/usr/Qt/5.6/gcc_64/lib/cmake/Qt5 \
-DVTK_DIR=/local/usr/vtk/lib/cmake/vtk-7.0 \
-DMesquite_DIR=/local/usr/trilinos/lib/cmake/Mesquite \
-DSWIG_EXECUTABLE=/local/usr/swig/bin/swig \
-DSOFA_ROOT=/local/usr/sofa '

echo "Calling cmake with the following options:"
echo "$cmake_options" | tr -s '\\' '\n'

cd "$build_dir"
cmake -G"Unix Makefiles" $cmake_options $src_dir
