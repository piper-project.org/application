:: configure
dir %WORKSPACE%
if not exist %WORKSPACE%\build mkdir %WORKSPACE%\build
dir %WORKSPACE%
cd %WORKSPACE%\build
dir %WORKSPACE%
::set PATH=%PATH%
if not defined CI_BUILD_TYPE set CI_BUILD_TYPE="Release"
cmake -D CMAKE_BUILD_TYPE=%CI_BUILD_TYPE% -D PIPER_PACKAGE_DIR=C:\Lib -D OCTAVE_ROOT_DIR=C:\Octave\Octave-4.0.3 -DCMAKE_INSTALL_PREFIX=%WORKSPACE%\build\install -D PYTHON_DEBUG_LIBRARY=C:\Lib\Python27\debug\python27_d.lib -DPIPER_BUNDLEINSTALL=ON -DINSTALL_OCTAVE=OFF -G "Visual Studio 12 2013 Win64" ..\src\

