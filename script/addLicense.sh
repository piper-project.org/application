#!/bin/bash

usage() {
    echo "Usage: addLicense.sh <src-dir> <license_file>"
    echo "  src-dir: all files in this folder will be affected"
    echo "  license_file: file that contains the license header to add"
}

if [[ "$#" = 2 ]]; then
    SRC_DIR="$1"
    LICENSE_FILE="$2"
else
    usage; exit 1
fi

files-to-update() {
    /usr/bin/find "$SRC_DIR" -regex ".*\.\(h\|cpp\|inl\|c\|h\.in\)$"
}

main() {
# TODO improve to be able to update the license when required
    for i in $(files-to-update)
    do
        cat $LICENSE_FILE $i >$i.new && mv $i.new $i
done
}

main